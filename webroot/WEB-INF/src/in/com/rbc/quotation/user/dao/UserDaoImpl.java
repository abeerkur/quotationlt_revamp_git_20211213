/**
 * *****************************************************************************
 * Project Name: Request For Quotation
 * Document Name: UserDaoImpl.java
 * Package: in.com.rbc.quotation.user.dao
 * Desc: 
 * *****************************************************************************
 * @author 100002865 (Shanthi Chinta)
 * *****************************************************************************
 */
package in.com.rbc.quotation.user.dao;

import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.utility.DBUtility;
import in.com.rbc.quotation.common.utility.ListDBColumnUtil;
import in.com.rbc.quotation.common.utility.ListModel;
import in.com.rbc.quotation.common.utility.LoggerUtility;
import in.com.rbc.quotation.user.dto.UserDto;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Hashtable;

public class UserDaoImpl implements UserDao, UserQueries {

    /* (non-Javadoc)
     * @see in.com.rbc.quotation.user.dao.UserDao#getLookupSearchResults(java.sql.Connection, in.com.rbc.quotation.user.dto.UserDto, in.com.rbc.quotation.common.utility.ListModel)
     */
    public Hashtable getLookupSearchResults(Connection conn, 
                                            UserDto oUserDto, 
                                            ListModel oListModel) throws Exception {
        
        /* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;      
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        /*
         * Hashtable that stores the following values, and that is used to display results in search page
         * --> ArrayList containing the rows retrieved from database
         * --> ListModel object that hold the values used to retrieve values and display the same
         * --> String array containing the list of column names to be displayed as column headings
         */
        Hashtable htSearchResults = null;
        
        ArrayList alSearchResultsList = new ArrayList(); // Stores the list of rows retrieved from the database        
        
        int iCursorPosition = 0; // Stores the cursor position from where the records have to be retrieved
        // from database; this is dependent on the page number selected for viewing
        
        String sWhereString = null; // Stores the Where clause string
        String sSqlQueryString = null; // Stores the complete SQL query to be executed
        
        try {
            /* Retrieving the where clause string that is built in the buildWhereClause method */
            sWhereString = buildWhereClause(oUserDto);
            
            /*
             * Executing the SQL query by attaching the where clause string that is built
             * to retrieve the count of rows obtained based on the filter criteria
             * This count is stored in totalRecordCount property of ListModel object
             * If no records were found, this value is set to zero
             */
            pstmt = conn.prepareStatement(UserQueries.FETCH_LOOKUPSEARCH_RESULTS_COUNT + sWhereString);  
            rs = pstmt.executeQuery();
            if (rs.next()) {
                oListModel.setTotalRecordCount(rs.getInt(1));
            } else {
                oListModel.setTotalRecordCount(0);
            }
            oDBUtility.releaseResources(rs, pstmt);
            
            /*
             * Proceeding on if the count of records satisfying the filter criteria
             * is greater than zero
             */             
            UserDto oTempUserDto;
            if (oListModel.getTotalRecordCount() > 0) {
                htSearchResults = new Hashtable();
                
                /* Throwing an exception, if current page could not be retrieved */
                if (oListModel.getCurrentPageInt()<0)
                    throw new Exception("Current Page is negative in search method ");
                
                /* 
                 * Retrieving the cursor position based on the page number selected by the user
                 * to retrieve list of values to be displayed for the request page
                 * If no page is selected, cursor position to display the first page is retrieved
                 */
                iCursorPosition = (oListModel.getCurrentPageInt()-1) * oListModel.getRecordsPerPageInt();
                
                /*
                 * Proceeding only if the cursor position is valid
                 * i.e., the cursor position is less than the total record count
                 */
                if(iCursorPosition < oListModel.getTotalRecordCount()) {
                    sSqlQueryString = new StringBuffer("SELECT * FROM (SELECT Q.*, rownum AS rnum FROM ( ")
                                                        .append(UserQueries.FETCH_LOOKUPSEARCH_RESULTS)
                                                        .append(sWhereString)
                                                        .append(" ORDER BY ")
                                                        .append(ListDBColumnUtil.getlookupSearchResultDBField(oListModel.getSortBy()))
                                                        .append(" " + oListModel.getSortOrderDesc())
                                                        .append(" ) Q WHERE rownum <= (")
                                                        .append(iCursorPosition + " + " + oListModel.getRecordsPerPage() + ")) ")
                                                        .append(" WHERE rnum > " + iCursorPosition + "")
                                                        .toString();
                     
                     /*
                      * Querying the database to retrieve the list of records, which match
                      * the filter criteria specified, and storing them in an ArrayList
                      */            
                     pstmt = conn.prepareStatement(sSqlQueryString, ResultSet.TYPE_SCROLL_INSENSITIVE,
                                                                    ResultSet.CONCUR_READ_ONLY);
                     rs = pstmt.executeQuery();               
                     
                     /* Retrieving values from the ResultSet and storing them in an ArrayList */
                     if (rs.next()) {
                         do {
                             oTempUserDto = new UserDto();
                             oTempUserDto.setUserId(rs.getString("USERID"));
                             oTempUserDto.setFullName(rs.getString("FULLNAME"));
                             oTempUserDto.setFirstName(rs.getString("FIRSTNAME"));
                             oTempUserDto.setLastName(rs.getString("LASTNAME"));
                             oTempUserDto.setMiddleName(rs.getString("MIDDLENAME"));
                             oTempUserDto.setTitle(rs.getString("TITLE"));
                             oTempUserDto.setGender(rs.getString("GENDER"));
                             oTempUserDto.setEmailId(rs.getString("EMAILID"));
                             oTempUserDto.setCompany(rs.getString("COMPANY"));
                             oTempUserDto.setCountry(rs.getString("COUNTRY"));
                             oTempUserDto.setLocation(rs.getString("LOCATION"));
                             
                             alSearchResultsList.add(oTempUserDto);
                        } while (rs.next());
                     }
                }
                /* Putting the search results ArrayList and ListModel object in the Hashtable */
                htSearchResults.put(QuotationConstants.QUOTATION_LIST_RESULTSLIST, alSearchResultsList);
                htSearchResults.put(QuotationConstants.QUOTATION_LIST_LISTMODELCLASS, oListModel);    
            }
        }finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
        
        return htSearchResults;
    }
    
    private String buildWhereClause(UserDto oUserDto) throws Exception {
        StringBuffer sbWhereString = new StringBuffer(" WHERE "); // Stores the Where clause string
       
        if ( (oUserDto.getEmployeeNameFilter() != null) && (oUserDto.getEmployeeNameFilter().trim().length() > 0) ){
            sbWhereString.append(" UPPER(EMP_NAMEFULL) LIKE '%" + oUserDto.getEmployeeNameFilter().trim().toUpperCase() + "%'");
            sbWhereString.append(" AND ");
        }
        
        /*if ( oUserDto.getEmployeeFilter().trim().length()>0 ) {
            if ( oUserDto.getEmployeeFilter().equalsIgnoreCase(QuotationConstants.QUOTATION_LOOKUPEMPFILTER_VP) ) {
                sbWhereString.append(" EMP_SSO IN (SELECT DISTINCT GEO_VICEPRESIDENTSSO FROM " + UserQueries.SCHEMA_QUOTATION + ".ACK_ORGANIZATIONS_MV)");
                sbWhereString.append(" AND ");
            }
            if ( oUserDto.getEmployeeFilter().equalsIgnoreCase(QuotationConstants.QUOTATION_LOOKUPEMPFILTER_HRM) ) {
                sbWhereString.append(" EMP_SSO IN (SELECT DISTINCT EMP_SSO_HRM FROM " + UserQueries.SCHEMA_QUOTATION + ".RFQ_EMPLOYEES_V)");
                sbWhereString.append(" AND ");
            }
        }*/
        
        if ( sbWhereString.toString().trim().equalsIgnoreCase(" WHERE") ) {
            return "";
        } else if (sbWhereString.toString().trim().endsWith(" AND")) {
            return sbWhereString.toString().substring(0, sbWhereString.toString().lastIndexOf(" AND"));
        } else {
            return "";
        }
    }
    
    /* (non-Javadoc)
     * @see in.com.rbc.quotation.user.dao.UserDao#getUserInfo(java.sql.Connection, java.lang.String)
     */
    public UserDto getUserInfo(Connection conn, String userId) throws Exception {
        
        String sMethodName = "getUserInfo";
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        // PreparedStatement object to handle database operations.
        PreparedStatement pstmt = null;
        
        //ResultSet object to store the rows retrieved from database.
        ResultSet rs = null;
        
        // Object of DBUtility class to handle database resource operations.
        DBUtility oDBUtility = new DBUtility();
        
        UserDto oUserDto = null;
        
        try {
            if ( userId!=null && userId.trim().length()!=0 ) {
                oUserDto = new UserDto();
                oUserDto.setUserId(userId.trim());
                
                // Preparing the PreapredStatement by passing query to the Connection object.
                pstmt = conn.prepareStatement(FETCH_USERDETAILS);
                
                // Setting the parameters to the prepared statement.
                pstmt.setString(1, oUserDto.getUserId());
                
                // Executing the prepared Statement and assigning the results to result set.
                rs = pstmt.executeQuery();
                
                if (rs.next()) {
                    LoggerUtility.log("INFO", sClassName, sMethodName, "User information retrieved. Setting in User DTO object.");
                    //Setting user information in UserDto object
                    oUserDto.setUserId(rs.getString("USERID"));
                    oUserDto.setFullName(rs.getString("FULLNAME"));
                    oUserDto.setFirstName(rs.getString("FIRSTNAME"));
                    oUserDto.setLastName(rs.getString("LASTNAME"));
                    oUserDto.setMiddleName(rs.getString("MIDDLENAME"));
                    oUserDto.setTitle(rs.getString("TITLE"));
                    oUserDto.setGender(rs.getString("GENDER"));
                    oUserDto.setEmailId(rs.getString("EMAILID"));
                    oUserDto.setCompany(rs.getString("COMPANY"));
                    oUserDto.setCountry(rs.getString("COUNTRY"));
                    oUserDto.setLocation(rs.getString("LOCATION"));
                    LoggerUtility.log("INFO", sClassName, sMethodName, "SSO Id = " + oUserDto.getUserId());
                    LoggerUtility.log("INFO", sClassName, sMethodName, "Name = " + oUserDto.getFullName());
                }
            }
        } finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        return oUserDto;
    }
    
    /* (non-Javadoc)
     * @see in.com.rbc.quotation.user.dao.UserDao#getUserInfoList(java.sql.Connection, java.util.ArrayList)
     */
    public ArrayList getUserInfoList(Connection conn, ArrayList alUserDtoList) throws Exception {
        //PreparedStatement object to handle database operations.
        PreparedStatement pstmt = null;
        
        //ResultSet object to store the rows retrieved from database.
        ResultSet rs = null;
        
        // Object of DBUtility class to handle database resource operations.
        DBUtility oDBUtility = new DBUtility();
        
        UserDto oTempUserDto;
        StringBuffer sbSsoList = null;
        String inputsString = "";
        try {
            sbSsoList = new StringBuffer("");
            for (int i=0 ; i<alUserDtoList.size() ; i++) {
                sbSsoList.append(((UserDto)alUserDtoList.get(i)).getUserId());
                sbSsoList.append("','");
            }
            inputsString = "'" + sbSsoList.toString();
            inputsString = inputsString.substring(0, (inputsString.lastIndexOf(",")));
            
            String FETCH_MULTIPLEUSERDETAILS = "SELECT " +
                                                    " EMP_SSO USERID, EMP_NAMEFIRST FIRSTNAME, " +
                                                    " EMP_NAMELAST LASTNAME, EMP_NAMEMIDDLE MIDDLENAME, " +
                                                    " EMP_NAMEFULL FULLNAME, EMP_TITLE TITLE, EMP_GENDER GENDER, " +
                                                    " EMP_EMAIL EMAILID, EMP_ORGANIZATION COMPANY, " +
                                                    " EMP_COUNTRY COUNTRY, EMP_LOCATION LOCATION " +
										         " FROM " +
                                                     SCHEMA_QUOTATION + ".RFQ_EMPLOYEES_V" +
										         " WHERE EMP_SSO IN (" + inputsString + ") " +
										         " ORDER BY EMP_SSO";
            
            // Preparing the PreapredStatement by passing query to the Connection object.
            pstmt = conn.prepareStatement(FETCH_MULTIPLEUSERDETAILS);
            
            // Executing the prepared Statement and assigning the results to result set.
            rs = pstmt.executeQuery();
            
            if (rs.next()) {
                alUserDtoList = new ArrayList();
                do {
                    oTempUserDto = new UserDto();
                    
                    //Setting user information in UserDto object
                    oTempUserDto.setUserId(rs.getString("USERID"));
                    oTempUserDto.setFullName(rs.getString("FULLNAME"));
                    oTempUserDto.setFirstName(rs.getString("FIRSTNAME"));
                    oTempUserDto.setLastName(rs.getString("LASTNAME"));
                    oTempUserDto.setMiddleName(rs.getString("MIDDLENAME"));
                    oTempUserDto.setTitle(rs.getString("TITLE"));
                    oTempUserDto.setGender(rs.getString("GENDER"));
                    oTempUserDto.setEmailId(rs.getString("EMAILID"));
                    oTempUserDto.setCompany(rs.getString("COMPANY"));
                    oTempUserDto.setCountry(rs.getString("COUNTRY"));
                    
                    oTempUserDto.setLocation(rs.getString("LOCATION"));
                    alUserDtoList.add(oTempUserDto);
                } while (rs.next());
            }
        }finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
        return alUserDtoList;
    }

}
