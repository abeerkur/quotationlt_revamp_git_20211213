/**
 * *****************************************************************************
 * Project Name: Request For Quotation
 * Document Name: UserForm.java
 * Package: in.com.rbc.quotation.user.form
 * Desc: 
 * *****************************************************************************
 * Author: 100002865 (Shanthi Chinta)
 * Date: Jul 8, 2008
 * *****************************************************************************
 */
package in.com.rbc.quotation.user.form;

import java.util.ArrayList;

import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.utility.CommonUtility;

import org.apache.struts.action.ActionForm;

public class UserForm extends ActionForm {
    private String userId = null;
    private String title = null;
    private String fullName = null;
    private String firstName = null;
    private String lastName = null;
    private String middleName = null;
    private String emailId = null;
    private String location = null;
    private String country = null;
    private String gender = null;
    private String company = null;
    
    private String invoke = null;
    private String selectType = null;
    private String submitted = null;
    private String parentUserIdObj = null;
    private String parentNameObj = null;
    private String parentEmailObj = null;
    private String parentLocObj = null;
    private String parentListObj = null;
    private String employeeFilter = null;
    private String employeeNameFilter = null;

    private ArrayList searchResultsList = null;
    private String sizeOfList = null;
    private String isSingleSelect = null;
    
    /**
     * This function accepts the row index and returns true/false depending on the index
     * @param rowIndex string parameter holding the row index
     * @return true - if row index is an even number; 
     *         false - if row index is an odd number
     */
    public boolean rowStyle(String rowIndex) {
        if ((Integer.parseInt(rowIndex)%2)==0)
            return true;
        else
            return false;
    }
    
    public boolean isSingleSelect() {
        if ( this.selectType.equalsIgnoreCase(QuotationConstants.QUOTATION_LOOKUPSELECTTYPE_SINGLE) )
            return true;
        else if ( this.selectType.equalsIgnoreCase(QuotationConstants.QUOTATION_LOOKUPSELECTTYPE_MULTIPLE) )
            return false;
        else
            return true;
    }

    /**
     * @return Returns the isSingleSelect.
     */
    public String getIsSingleSelect() {
        if ( this.selectType.equalsIgnoreCase(QuotationConstants.QUOTATION_LOOKUPSELECTTYPE_SINGLE) )
            return "true";
        else if ( this.selectType.equalsIgnoreCase(QuotationConstants.QUOTATION_LOOKUPSELECTTYPE_MULTIPLE) )
            return "false";
        else
            return "true";
    }

    /**
     * @param isSingleSelect The isSingleSelect to set.
     */
    public void setIsSingleSelect(String isSingleSelect) {
        this.isSingleSelect = isSingleSelect;
    }
    /**
     * @return Returns the searchResultsList.
     */
    public ArrayList getSearchResultsList() {
        return this.searchResultsList;
    }
    /**
     * @param searchResultsList The searchResultsList to set.
     */
    public void setSearchResultsList(ArrayList searchResultsList) {
        this.searchResultsList = searchResultsList;
    }
    /**
     * @return Returns the sizeOfList.
     */
    public String getSizeOfList() {
        return this.sizeOfList;
    }
    /**
     * @param sizeOfList The sizeOfList to set.
     */
    public void setSizeOfList(String sizeOfList) {
        this.sizeOfList = sizeOfList;
    }
    /**
     * @return Returns the company.
     */
    public String getCompany() {
        return CommonUtility.replaceNull(this.company);
    }
    /**
     * @param company The company to set.
     */
    public void setCompany(String company) {
        this.company = company;
    }
    /**
     * @return Returns the country.
     */
    public String getCountry() {
        return CommonUtility.replaceNull(this.country);
    }
    /**
     * @param country The country to set.
     */
    public void setCountry(String country) {
        this.country = country;
    }
    /**
     * @return Returns the emailId.
     */
    public String getEmailId() {
        return CommonUtility.replaceNull(this.emailId);
    }
    /**
     * @param emailId The emailId to set.
     */
    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }
    /**
     * @return Returns the employeeFilter.
     */
    public String getEmployeeFilter() {
        return CommonUtility.replaceNull(this.employeeFilter);
    }
    /**
     * @param employeeFilter The employeeFilter to set.
     */
    public void setEmployeeFilter(String employeeFilter) {
        this.employeeFilter = employeeFilter;
    }
    /**
     * @return Returns the employeeNameFilter.
     */
    public String getEmployeeNameFilter() {
        return CommonUtility.replaceNull(this.employeeNameFilter);
    }
    /**
     * @param employeeNameFilter The employeeNameFilter to set.
     */
    public void setEmployeeNameFilter(String employeeNameFilter) {
        this.employeeNameFilter = employeeNameFilter;
    }
    /**
     * @return Returns the firstName.
     */
    public String getFirstName() {
        return CommonUtility.replaceNull(this.firstName);
    }
    /**
     * @param firstName The firstName to set.
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    /**
     * @return Returns the fullName.
     */
    public String getFullName() {
        return CommonUtility.replaceNull(this.fullName);
    }
    /**
     * @param fullName The fullName to set.
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
    /**
     * @return Returns the gender.
     */
    public String getGender() {
        return CommonUtility.replaceNull(this.gender);
    }
    /**
     * @param gender The gender to set.
     */
    public void setGender(String gender) {
        this.gender = gender;
    }
    /**
     * @return Returns the invoke.
     */
    public String getInvoke() {
        return CommonUtility.replaceNull(this.invoke);
    }
    /**
     * @param invoke The invoke to set.
     */
    public void setInvoke(String invoke) {
        this.invoke = invoke;
    }
    /**
     * @return Returns the lastName.
     */
    public String getLastName() {
        return CommonUtility.replaceNull(this.lastName);
    }
    /**
     * @param lastName The lastName to set.
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    /**
     * @return Returns the location.
     */
    public String getLocation() {
        return CommonUtility.replaceNull(this.location);
    }
    /**
     * @param location The location to set.
     */
    public void setLocation(String location) {
        this.location = location;
    }
    /**
     * @return Returns the middleName.
     */
    public String getMiddleName() {
        return CommonUtility.replaceNull(this.middleName);
    }
    /**
     * @param middleName The middleName to set.
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
    /**
     * @return Returns the parentEmailObj.
     */
    public String getParentEmailObj() {
        return CommonUtility.replaceNull(this.parentEmailObj);
    }
    /**
     * @param parentEmailObj The parentEmailObj to set.
     */
    public void setParentEmailObj(String parentEmailObj) {
        this.parentEmailObj = parentEmailObj;
    }
    /**
     * @return Returns the parentLocObj.
     */
    public String getParentLocObj() {
        return CommonUtility.replaceNull(this.parentLocObj);
    }
    /**
     * @param parentLocObj The parentLocObj to set.
     */
    public void setParentLocObj(String parentLocObj) {
        this.parentLocObj = parentLocObj;
    }
    /**
     * @return Returns the parentNameObj.
     */
    public String getParentNameObj() {
        return CommonUtility.replaceNull(this.parentNameObj);
    }
    /**
     * @param parentNameObj The parentNameObj to set.
     */
    public void setParentNameObj(String parentNameObj) {
        this.parentNameObj = parentNameObj;
    }
    /**
     * @return Returns the selectType.
     */
    public String getSelectType() {
        return CommonUtility.replaceNull(this.selectType);
    }
    /**
     * @param selectType The selectType to set.
     */
    public void setSelectType(String selectType) {
        this.selectType = selectType;
    }
    /**
     * @return Returns the submitted.
     */
    public String getSubmitted() {
        return CommonUtility.replaceNull(this.submitted);
    }
    /**
     * @param submitted The submitted to set.
     */
    public void setSubmitted(String submitted) {
        this.submitted = submitted;
    }
    /**
     * @return Returns the title.
     */
    public String getTitle() {
        return CommonUtility.replaceNull(this.title);
    }
    /**
     * @param title The title to set.
     */
    public void setTitle(String title) {
        this.title = title;
    }
    /**
     * @return Returns the userId.
     */
    public String getUserId() {
        return CommonUtility.replaceNull(this.userId);
    }
    /**
     * @param userId The userId to set.
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }
    /**
     * @return Returns the parentUserIdObj.
     */
    public String getParentUserIdObj() {
        return CommonUtility.replaceNull(this.parentUserIdObj);
    }
    /**
     * @param parentUserIdObj The parentUserIdObj to set.
     */
    public void setParentUserIdObj(String parentUserIdObj) {
        this.parentUserIdObj = parentUserIdObj;
    }

    /**
     * @return Returns the parentListObj.
     */
    public String getParentListObj() {
        return CommonUtility.replaceNull(this.parentListObj);
    }

    /**
     * @param parentListObj The parentListObj to set.
     */
    public void setParentListObj(String parentListObj) {
        this.parentListObj = parentListObj;
    }
}
