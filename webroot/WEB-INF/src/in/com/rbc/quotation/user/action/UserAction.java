/**
 * *****************************************************************************
 * Project Name: Request For Quotation
 * Document Name: UserAction.java
 * Package: in.com.rbc.quotation.common.utility
 * Desc: User Utility class which is used to retrieve User information like 
 *       user's first name, last name, full name, location etc. 
 *       by passing SSO Id or list of SSO Ids.
 * *****************************************************************************
 * @author 100002865 (Shanthi Chinta)
 * *****************************************************************************
 */
package in.com.rbc.quotation.user.action;

import in.com.rbc.quotation.base.BaseAction;
import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.utility.CommonUtility;
import in.com.rbc.quotation.common.utility.ConverterUtility;
import in.com.rbc.quotation.common.utility.DBUtility;
import in.com.rbc.quotation.common.utility.ExceptionUtility;
import in.com.rbc.quotation.common.utility.ListModel;
import in.com.rbc.quotation.common.utility.LoggerUtility;
import in.com.rbc.quotation.factory.DaoFactory;
import in.com.rbc.quotation.user.dao.UserDao;
import in.com.rbc.quotation.user.dto.UserDto;
import in.com.rbc.quotation.user.form.UserForm;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class UserAction extends BaseAction {
    
    public ActionForward lookupEmployee(ActionMapping actionMapping,
                                        ActionForm actionForm,
                                        HttpServletRequest request,
                                        HttpServletResponse response) throws Exception {
        
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        UserForm oUserForm = null;
        DaoFactory oDaoFactory = null;
        UserDao oUserDao = null;
        UserDto oUserDto = null;
        
        /*
         * Object of Hashtable to store different values that are retrieved 
         * using searchEmployeeVo.getAttributeSearchResults method
         */
        Hashtable htSearchResults = null;
        
        /*
         * Object of ListModel class to store the values necessary to display 
         * the results in attribute search results page with pagination
         */
        ListModel oListModel = null;
        
        Connection conn = null;  // Connection object to store the database connection
        
        try {
            /*
             * Ssetting user's information in request, using the header 
             * information received through request
             */
            setRolesToRequest(request);

            oUserForm = (UserForm)actionForm;
            if ( oUserForm == null ) {
                oUserForm = new UserForm();
                
                oUserForm.setInvoke(CommonUtility.getStringParameter(request, "invoke"));
                oUserForm.setSelectType(CommonUtility.getStringParameter(request, "selectType"));
                oUserForm.setParentUserIdObj(CommonUtility.getStringParameter(request, "parentUserIdObj"));
                oUserForm.setParentNameObj(CommonUtility.getStringParameter(request, "parentNameObj"));
                oUserForm.setParentEmailObj(CommonUtility.getStringParameter(request, "parentEmailObj"));
                oUserForm.setParentLocObj(CommonUtility.getStringParameter(request, "parentLocObj"));
                oUserForm.setParentListObj(CommonUtility.getStringParameter(request, "parentListObj"));
                oUserForm.setEmployeeFilter(CommonUtility.getStringParameter(request, "employeeFilter"));
            }
            if ( oUserForm.getSelectType().length()==0 ) {
                oUserForm.setSelectType(QuotationConstants.QUOTATION_LOOKUPSELECTTYPE_SINGLE);
            }
            
            if ( oUserForm.getSubmitted()!=null 
                    && oUserForm.getSubmitted().length()>0 
                    && oUserForm.getSubmitted().equals("1") ) {
                
                /* Instantiating the DBUtility object */
                oDBUtility = new DBUtility();
                
                /* Creating an instance of of DaoFactory & retrieving UserDao object */
                oDaoFactory = new DaoFactory();
                oUserDao = oDaoFactory.getUserDao();
                
                oUserDto = new UserDto();
                
                /*
                 * Setting values for ListModel that are necessary to retrieve rows from the database
                 * and display them in search results page. It holds information like
                 * --> number of rows to be displayed at once in the page
                 * --> column on which rows have to be sorted
                 * --> order in which rows have to be sorted
                 * --> set of rows to be retrieved depending on the page number selected for viewing
                 * Details of parameters:
                 * 1. Request object
                 * 2. Sort column index
                 * 3. Sort order
                 * 4. Search form
                 * 5. Field that stores the invoke method name
                 * 8. Invoke method name
                 */
                oListModel = new ListModel();
                oListModel.setParams(request, 
                                       "1", 
                                       ListModel.ASCENDING, 
                                       "userForm", 
                                       "userForm.invoke", 
                                       "lookupEmployee");
                
                ConverterUtility.copyProperties(oUserDto, oUserForm);
                
                /* Retrieving the database connection using DBUtility.getDBConnection method */
                conn = oDBUtility.getDBConnection();
                
                htSearchResults = oUserDao.getLookupSearchResults(conn, oUserDto, oListModel);
                
                /*
                 * Storing the information retrieved in browsers request, in case when the 
                 * results are found for the specified search criteria
                 * Following information is stored in the request, in this case
                 * SearchEmployeeVo object: Object of SearchEmployeeVo
                 * Display Columns:  String array containing the list of column names to 
                 *                   be displayed as column headings in results page
                 * No. of columns:   Number of columns that are selected for display
                 * resultsList:      ArrayList containing the rows retrieved from database
                 * ListModel object: ListModel object that hold the values used to retrieve 
                 *                   values and display the same
                 * Size of List:     Size of the list to be displayed i.e., size of resultsList
                 */
                if (htSearchResults != null) {
                    oUserDto.setSearchResultsList((ArrayList)htSearchResults.get(QuotationConstants.QUOTATION_LIST_RESULTSLIST));
                    oUserDto.setSizeOfList(CommonUtility.getStringValue((((ArrayList)htSearchResults.get(QuotationConstants.QUOTATION_LIST_RESULTSLIST)).size())));
                    
                    request.setAttribute(QuotationConstants.QUOTATION_LIST_LISTMODELCLASS, 
                                         htSearchResults.get(QuotationConstants.QUOTATION_LIST_LISTMODELCLASS));
                    request.setAttribute(QuotationConstants.QUOTATION_LIST_RESULTSLIST, 
                                        (ArrayList)htSearchResults.get(QuotationConstants.QUOTATION_LIST_RESULTSLIST));
                }
                
                ConverterUtility.copyProperties(oUserForm, oUserDto);
            } else {
                oUserForm.setSubmitted("0");
            }
        } catch(Exception e) {
            /*
             * Logging any exception that raised during this activity in log files using Log4j
             */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "lookupEmployee", ExceptionUtility.getStackTraceAsString(e));
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        } finally {
            /* Releasing/closing the connection object */
            if (conn!=null)
                oDBUtility.releaseResources(conn);
        }
        
        return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_SUCCESS);
    }
    
    public UserDto getUserInfoForLoginUser(HttpServletRequest request, 
                                           Connection conn) throws Exception {
        
        String sMethodName = "getUserInfoForLoginUser";
        
        DaoFactory oDaoFactory = null;
        UserDao oUserDao = null;
        String sLoginUserId = null;
        UserDto oUserDto = null;
        
        try {
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
            oUserDao = oDaoFactory.getUserDao();
            
            if (request.getSession().getAttribute(QuotationConstants.QUOTAION_SELE_SSOID) != null) {
				sLoginUserId = (String) request.getSession().getAttribute(QuotationConstants.QUOTAION_SELE_SSOID);
			}
            
            if (sLoginUserId == null || sLoginUserId.trim().length() == 0 ) {
            	if (request.getHeader(QuotationConstants.QUOTATION_REQHEADER_UID)!=null) {
                    LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "User SSO retrieved with - " + QuotationConstants.QUOTATION_REQHEADER_UID);
                    sLoginUserId = request.getHeader(QuotationConstants.QUOTATION_REQHEADER_UID);
                }
                if (sLoginUserId == null || sLoginUserId.trim().length()== 0 ) {
                    if ( request.getHeader(QuotationConstants.QUOTATION_REQHEADER_SSOID)!=null ) {
                        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "User SSO retrieved with - " + QuotationConstants.QUOTATION_REQHEADER_SSOID);
                        sLoginUserId = request.getHeader(QuotationConstants.QUOTATION_REQHEADER_SSOID);
                    }
                }
                if (sLoginUserId == null  || sLoginUserId.trim().length()== 0 ) {
                    if ( request.getHeader(QuotationConstants.QUOTATION_REQHEADER_SMUSER)!=null ) {
                        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "User SSO retrieved with - " + QuotationConstants.QUOTATION_REQHEADER_SMUSER);
                        sLoginUserId = request.getHeader(QuotationConstants.QUOTATION_REQHEADER_SMUSER);
                    }
                }
                if (sLoginUserId == null  || sLoginUserId.trim().length()== 0 ) {
                    if (request.getSession().getAttribute("QUOTATION_SELECTED_SSOID") == null) {
                    	sLoginUserId = QuotationConstants.QUOTATION_DEFAULT_SSOID;
                        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "User information not found in request. Setting default user - " + sLoginUserId);
                    } else {
                    	sLoginUserId = (String)request.getSession().getAttribute("QUOTATION_SELECTED_SSOID");
                        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "User information not found in request. Retrieving the user information selected locally - " + sLoginUserId);
                    }
                }
            }
            
            //Retrieving the login User details using UserUtility
            oUserDto = oUserDao.getUserInfo(conn, sLoginUserId);
        } catch(Exception e) {
        	e.printStackTrace();
        }
        
        return oUserDto;
    }
    
    public UserDto getUserInfoForId(Connection conn, String sUserId) throws Exception {
        
        DaoFactory oDaoFactory = null;
        UserDao oUserDao = null;
        UserDto oUserDto = null;
        
        try {
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
            oUserDao = oDaoFactory.getUserDao();
            
            //Retrieving the login User details using UserUtility
            oUserDto = oUserDao.getUserInfo(conn, sUserId);
        } 
        finally {}
        
        return oUserDto;
}
    
    public UserDto getUserInfo(Connection conn,
                               UserDto oUserDto) throws Exception {
        
        DaoFactory oDaoFactory = null;
        UserDao oUserDao = null;
        
        try {
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
            oUserDao = oDaoFactory.getUserDao();
            
            //Retrieving the login User details using UserUtility
            oUserDto = oUserDao.getUserInfo(conn, oUserDto.getUserId());
        } finally {}
        
        return oUserDto;
    }
    
    public ArrayList getUserInfoList(Connection conn,
                                   ArrayList alUserDtoList) throws Exception {
        
        DaoFactory oDaoFactory = null;
        UserDao oUserDao = null;
        
        try {
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
            oUserDao = oDaoFactory.getUserDao();
            
            //Retrieving the login User details using UserUtility
            alUserDtoList = oUserDao.getUserInfoList(conn, alUserDtoList);
        } finally {}
        
        return alUserDtoList;
    }
    
	/**
	 * Retrieves the full name of given SSO id
	 * @param conn Connection object
	 * @param sSsoId User Id
	 * @return returns the full name
	 * @throws Exception
     * @author 100002572 (Sunanda Devulapally)
     * Created on: July 15, 2008
	 */
	public String getUserFullName(Connection conn, String sSsoId) 
				throws Exception {
		
		UserDto oUserDto = new UserDto();
		oUserDto.setUserId(sSsoId);
		oUserDto = getUserInfo(conn, oUserDto);
		
		return oUserDto.getFullName();
	}
	
	
}
