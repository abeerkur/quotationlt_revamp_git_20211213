/**
 * *****************************************************************************
 * Project Name: Request For Quotation
 * Document Name: UserQueries.java
 * Package: in.com.rbc.quotation.user.dao
 * Desc: 
 * *****************************************************************************
 * @author 100002865 (Shanthi Chinta)
 * *****************************************************************************
 */
package in.com.rbc.quotation.user.dao;

import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.utility.PropertyUtility;

public interface UserQueries {
    /**
     * String variable to store the QUOTATION & SHARED schema name.
     * It is retrieved from quotation.properties file
     */
    public final String SCHEMA_QUOTATION = PropertyUtility.getKeyValue(
                                                        QuotationConstants.QUOTATION_PROPERTYFILENAME,
                                                        QuotationConstants.QUOTATION_DB_SCHEMA_QUOTATION);
    
    public final String FETCH_LOOKUPSEARCH_RESULTS_COUNT = "SELECT COUNT(*) AS COUNT FROM " +
                                                            SCHEMA_QUOTATION + ".RFQ_EMPLOYEES_V";
    
    public final String FETCH_LOOKUPSEARCH_RESULTS = "SELECT " +
                                                        " EMP_SSO USERID, EMP_NAMEFIRST FIRSTNAME, " +
                                                        " EMP_NAMELAST LASTNAME, EMP_NAMEMIDDLE MIDDLENAME, " +
                                                        " EMP_NAMEFULL FULLNAME, EMP_TITLE TITLE, EMP_GENDER GENDER, " +
                                                        " EMP_EMAIL EMAILID, EMP_ORGANIZATION COMPANY, " +
                                                        " EMP_COUNTRY COUNTRY, EMP_LOCATION LOCATION " +
                                                     " FROM " +
                                                         SCHEMA_QUOTATION + ".RFQ_EMPLOYEES_V";

    public final String FETCH_USERDETAILS = "SELECT " +
                                                " EMP_SSO USERID, EMP_NAMEFIRST FIRSTNAME, " +
                                                " EMP_NAMELAST LASTNAME, EMP_NAMEMIDDLE MIDDLENAME, " +
                                                " EMP_NAMEFULL FULLNAME, EMP_TITLE TITLE, EMP_GENDER GENDER, " +
                                                " EMP_EMAIL EMAILID, EMP_ORGANIZATION COMPANY, " +
                                                " EMP_COUNTRY COUNTRY, EMP_LOCATION LOCATION " +
                                             " FROM " +
                                                 SCHEMA_QUOTATION + ".RFQ_EMPLOYEES_V" +
                                             " WHERE EMP_SSO=? ";
    
}
