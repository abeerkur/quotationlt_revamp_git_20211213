/**
 * *****************************************************************************
 * Project Name: Request For Quotation
 * Document Name: UserDao.java
 * Package: in.com.rbc.quotation.user.dao
 * Desc: 
 * *****************************************************************************
 * @author 100002865 (Shanthi Chinta)
 * *****************************************************************************
 */
package in.com.rbc.quotation.user.dao;

import in.com.rbc.quotation.common.utility.ListModel;
import in.com.rbc.quotation.user.dto.UserDto;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.servlet.http.HttpServletRequest;

public interface UserDao {
    
    /**
     * 
     * @param conn
     * @param oUserDto
     * @param oListModel
     * @return
     * @throws Exception
     * @author 100002865 (Shanthi Chinta)
     * Created on: Jul 9, 2008
     */
    public Hashtable getLookupSearchResults(Connection conn, UserDto oUserDto, ListModel oListModel) throws Exception;
    
    /**
     * 
     * @param conn
     * @param oUserDto
     * @return
     * @throws Exception
     * @author 100002865 (Shanthi Chinta)
     * Created on: Jul 9, 2008
     */
    public UserDto getUserInfo(Connection conn, String userId) throws Exception;

    /**
     * 
     * @param conn
     * @param arUserDtoList
     * @return
     * @author 100002865 (Shanthi Chinta)
     * Created on: Jul 10, 2008
     */
    public ArrayList getUserInfoList(Connection conn, ArrayList alUserDtoList) throws Exception;
    
    /**
     * Method is used to check if the logged-in user is a normal employee or a manager.
     * 
     * @param conn Connection object to connect to database.
     * @param oUserDto Object of UserDto class, which contains then necessary inputs.
     * @return Returns a boolean value; True, if the employee is a manager. Else,
     *         returns false.
     * @throws Exception Exception if any error occurs during the process
     * @author 100002865 (Shanthi Chinta)
     * Created on: Aug 25, 2008
     */
    /*public boolean isManager(Connection conn, UserDto oUserDto) throws Exception;*/
}
