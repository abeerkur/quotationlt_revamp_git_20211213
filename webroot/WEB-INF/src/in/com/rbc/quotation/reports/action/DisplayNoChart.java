/**
 * *****************************************************************************
 * Project Name: Global Employee Management System
 * Document Name: ImageServlet.java
 * Package: in.com.rbc.gems.employee.action
 * Desc: Servlet retrieves the picture of the employee 
 * *****************************************************************************
 * Author: 100003810 (Subhakar Edeti)
 * *****************************************************************************
 */
package in.com.rbc.quotation.reports.action;

import in.com.rbc.quotation.common.utility.ExceptionUtility;
import in.com.rbc.quotation.common.utility.LoggerUtility;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DisplayNoChart extends HttpServlet {
    
	
	/**
	 * Method to display image in jsp -- invokes doPost
	 * @param request HttpServletRequest object to handle request operations
	 * @param response HttpServletResponse object to handle response operations 
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
	
	/**
	 * Method displays the image in jsp
	 * @param request HttpServletRequest object to handle request operations
	 * @param response HttpServletResponse object to handle response operations
	 */
	public void doPost(HttpServletRequest request, 
                       HttpServletResponse response) throws ServletException, IOException{
		       
		try{       
            /* Setting the content type to image */
			response.setContentType("image/gif");
			
			/* Setting the image to writer to display it on jsp */
			
                LoggerUtility.log("INFO", this.getClass().getName(), "doPost", "Image is not available.");
                PrintWriter out = response.getWriter();
                int data = 0;
                File f = new File(getServletContext().getRealPath(".") +  "/html/images/nodata.gif");
                BufferedInputStream in = new BufferedInputStream(new FileInputStream(f.getAbsolutePath()));
                while ((data = in.read()) != -1) {
                    out.write(data);
                    out.flush();
                }
            
		} catch(Exception e) {
            /*
             * Logging any exception that raised during this activity in log files using Log4j
             */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "doPost", ExceptionUtility.getStackTraceAsString(e));
		} 
	}
}
