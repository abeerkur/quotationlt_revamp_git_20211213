/**
 * ******************************************************************************************
 * Project Name: Quality Tools
 * Document Name: RequestAction
 * Package: in.com.rbc.quality.fracas.reports.action
 * Desc:  Action class that contains all the action methods from Reports area
 * ******************************************************************************************
 * @author 100003810 (Subhakar Edeti)
 * ******************************************************************************************
 */
package in.com.rbc.quotation.reports.action;

import in.com.rbc.quotation.admin.dao.MasterDao;
import in.com.rbc.quotation.base.BaseAction;
import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.utility.CommonUtility;
import in.com.rbc.quotation.common.utility.ConverterUtility;
import in.com.rbc.quotation.common.utility.DBUtility;
import in.com.rbc.quotation.common.utility.ExceptionUtility;
import in.com.rbc.quotation.common.utility.LoggerUtility;
import in.com.rbc.quotation.factory.DaoFactory;
import in.com.rbc.quotation.reports.dao.ReportsDao;
import in.com.rbc.quotation.reports.dto.GraphDto;
import in.com.rbc.quotation.reports.dto.ReportDatasetDto;
import in.com.rbc.quotation.reports.dto.ReportDto;
import in.com.rbc.quotation.reports.form.ReportForm;

import java.awt.Color;
import java.awt.GradientPaint;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.jfree.chart.ChartRenderingInfo;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPosition;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.CategoryLabelWidthType;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.entity.StandardEntityCollection;
import org.jfree.chart.labels.StandardCategoryToolTipGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.servlet.ServletUtilities;
import org.jfree.chart.urls.StandardCategoryURLGenerator;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.text.TextBlockAnchor;
import org.jfree.ui.GradientPaintTransformType;
import org.jfree.ui.RectangleAnchor;
import org.jfree.ui.StandardGradientPaintTransformer;
import org.jfree.ui.TextAnchor;

/**
 * This class is used to display the Graphs depending on the 
 * parameters like business,sub business,product line etc.
 */
public class ReportsAction extends BaseAction {
    
    /**
     * This method is used to display  detail graphs  like business,sub business,
     * product line,status,customer,customer location,defect code.
     * @param actionMapping ActionMapping object to redirect the user to next page depending on the activity
     * @param actionForm ActionForm object to handle the form elements
     * @param request HttpServletRequest object to handle request operations
     * @param response HttpServletResponse object to handle response operations
     * @return returns ActionForward depending on which user is redirected to next page
     * @throws Exception if any error occurs while performing database operations, etc
     * @author 100003810
     * Created on: July 7, 2008
     */ 
    public ActionForward viewGraph(ActionMapping actionMapping,
                                   ActionForm actionForm,
                                   HttpServletRequest request,
                                   HttpServletResponse response) throws Exception {
    	
    	 /*
         * Setting user's information in request, using the header 
         * information received through request
         */
        setRolesToRequest(request);
        
        Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        
        ArrayList alResults=null;
        ArrayList alGraph=new ArrayList();
        String filename=null;
        String sfilename=null;
        String cfilename=null;
        String icfilename=null;    
        String graphURL = request.getContextPath() + "/servlet/DisplayChart?filename=";
        String sNographURL="DisplayNoChart";        
    
        int iRequestGraphType= CommonUtility.getIntParameter(request,"graphtype");
        String sSection= CommonUtility.getStringParameter(request,"section");
        // Get the Form class
        ReportForm oReportForm = (ReportForm)actionForm;
        if (oReportForm == null){
            oReportForm = new ReportForm();    
            }
        ReportDto oReportDto = new ReportDto();
    
        try{
             /* Creating an instance of of DaoFactory */      
             DaoFactory  oDaoFactory = new DaoFactory();
             /* Instantiating the DBUtility object */
             oDBUtility = new DBUtility();
             /* Retrieving the database connection using DBUtility.getDBConnection method */   
             //ReportsDao oReportsDao = oDaoFactory.getReportsDao(); 
             conn = oDBUtility.getDBConnection();  
             //1. Region list
             MasterDao oMasterDao = oDaoFactory.getMasterDao();
             ReportsDao oReportsDao=oDaoFactory.getReportsDao();
             oReportForm.setRegionList(oMasterDao.getRegionList(conn));
             //2. Issue Category list
             oReportForm.setLocationList(oMasterDao.getLocationList(conn));
             oReportForm.setStatusList(oMasterDao.getStatusList(conn));
             oReportForm.setYearList(oMasterDao.getYears(conn));          
             
             ConverterUtility.copyProperties(oReportDto,oReportForm);
             
             if(oReportDto.getCustomerId()> 0 || iRequestGraphType == 3 ){  
            	 
            	 
            	 	if(iRequestGraphType > 0){ 
                     
                     setRequesttoFrom(request,oReportForm,oReportDto);                                      
                     oReportDto.setCustomerId(oReportsDao.getCustomerID(conn,sSection));
                     oReportForm.setCustomerId(oReportDto.getCustomerId());
                     oReportDto.setLocationId(oMasterDao.getLocationID(conn,oReportForm.getCustomerId()));
                     oReportForm.setLocationId(oReportDto.getLocationId());                   
                     
                 }
            	 
            	 if(oReportDto.getSalesManagerId()>0){
            		 
            		 oReportForm.setSalesManagerList(oMasterDao.getSalemangerList(conn,CommonUtility.getStringValue(oReportDto.getRegionId())));
            	 }
            	 oReportForm.setCustomerList(oMasterDao.getCustomerList(conn,CommonUtility.getStringValue(oReportDto.getLocationId())));
            	 
            	 GraphDto oGraphDto= new GraphDto();
                 oReportDto.setGraphType(QuotationConstants.RBC_QUOTATION_CUSTOMER_LOCATIONS_GRAPH);
                 oGraphDto.setAlgraphdataset(oReportsDao.getDataSetForGraph(conn,oReportDto));
                 filename= generateGraph(oGraphDto.getAlgraphdataset(),oReportDto,request,response);
                 oReportDto.setGraphType(QuotationConstants.RBC_QUOTATION_STATUS_GRAPH);
                 oGraphDto.setAlgraphdataset2(oReportsDao.getDataSetForGraph(conn,oReportDto));
                 sfilename=generateGraph(oGraphDto.getAlgraphdataset2(),oReportDto,request,response);
                 
                 
                 if(filename.equalsIgnoreCase("nodata.gif")){
                     oGraphDto.setGraphUrl(sNographURL); 
                 }else{
                 oGraphDto.setGraphUrl(graphURL+filename);
                 }
                 oGraphDto.setGraphName(filename);
                 oGraphDto.setGraphTitle("Customer Locations");
                 oGraphDto.setGraphHrefUrl(generateUrl(oReportDto,5,0));
                 if(sfilename.equalsIgnoreCase("nodata.gif")){
                     oGraphDto.setGraphUrl2(sNographURL); 
                 }else{
                 oGraphDto.setGraphUrl2(graphURL+sfilename);
                 }
                 oGraphDto.setGraphHrefUrl2(generateUrl(oReportDto,4,0));
                 oGraphDto.setGraphName2(sfilename);                 
                 oGraphDto.setGraphTitle2("Status");
                 alGraph.add(oGraphDto);             
            	 
            	 
             }else if(oReportDto.getSalesManagerId()>0 || iRequestGraphType == 2){
            	 
            	 if(iRequestGraphType > 0){
        			 
            		 
                     setRequesttoFrom(request,oReportForm,oReportDto);                                      
                     oReportDto.setSalesManagerId(oReportsDao.getSaleManagerID(conn,sSection));
                     oReportDto.setRegionId(oMasterDao.getRegionIDBySalesManager(conn,sSection));
                                       
                     oReportForm.setSalesManagerId(oReportDto.getSalesManagerId()); 
                     oReportForm.setRegionId(oReportDto.getRegionId());
                 
        	 }
            	 
            	 oReportForm.setSalesManagerList(oMasterDao.getSalemangerList(conn,CommonUtility.getStringValue(oReportDto.getRegionId())));
            	 
            	 
            	 GraphDto oGraphDto= new GraphDto();
                 oReportDto.setGraphType(QuotationConstants.RBC_QUOTATION_CUSTOMER_GRAPH);
                 oGraphDto.setAlgraphdataset(oReportsDao.getDataSetForGraph(conn,oReportDto));
                 filename= generateGraph(oGraphDto.getAlgraphdataset(),oReportDto,request,response);
                 oReportDto.setGraphType(QuotationConstants.RBC_QUOTATION_STATUS_GRAPH);
                 oGraphDto.setAlgraphdataset2(oReportsDao.getDataSetForGraph(conn,oReportDto));
                 sfilename=generateGraph(oGraphDto.getAlgraphdataset2(),oReportDto,request,response);
                 
                 
                 if(filename.equalsIgnoreCase("nodata.gif")){
                     oGraphDto.setGraphUrl(sNographURL); 
                 }else{
                 oGraphDto.setGraphUrl(graphURL+filename);
                 }
                 oGraphDto.setGraphName(filename);
                 oGraphDto.setGraphTitle("Customer");
                 oGraphDto.setGraphHrefUrl(generateUrl(oReportDto,3,0));
                 if(sfilename.equalsIgnoreCase("nodata.gif")){
                     oGraphDto.setGraphUrl2(sNographURL); 
                 }else{
                 oGraphDto.setGraphUrl2(graphURL+sfilename);
                 }
                 oGraphDto.setGraphHrefUrl2(generateUrl(oReportDto,4,0));
                 oGraphDto.setGraphName2(sfilename);                 
                 oGraphDto.setGraphTitle2("Status");
                 alGraph.add(oGraphDto);             
            	 
            	 
             }else if(oReportDto.getRegionId()>0 || iRequestGraphType == 1){
            	 
            
            		 if(iRequestGraphType > 0){
            			 
                         setRequesttoFrom(request,oReportForm,oReportDto);                                      
                         oReportDto.setRegionId(oReportsDao.getRegionID(conn,sSection));
                         oReportForm.setRegionId(oReportDto.getRegionId());                        
                     
            	 }
            	 if(oReportDto.getSalesManagerId()>0){
            		 oReportForm.setSalesManagerList(oMasterDao.getSalemangerList(conn,CommonUtility.getStringValue(oReportDto.getRegionId())));
            		 
            	 }
            	 
            	 GraphDto oGraphDto= new GraphDto();
                 GraphDto oGraphDto1= new GraphDto();
                 
                 oReportDto.setGraphType(QuotationConstants.RBC_QUOTATION_SALES_MANAGER_GRAPH);
                 oGraphDto.setAlgraphdataset(oReportsDao.getDataSetForGraph(conn,oReportDto));
                 filename=generateGraph(oGraphDto.getAlgraphdataset(),oReportDto,request,response);
                 oReportDto.setGraphType(QuotationConstants.RBC_QUOTATION_CUSTOMER_GRAPH);
                 oGraphDto.setAlgraphdataset2(oReportsDao.getDataSetForGraph(conn,oReportDto));
                 sfilename= generateGraph(oGraphDto.getAlgraphdataset2(),oReportDto,request,response);
                 oReportDto.setGraphType(QuotationConstants.RBC_QUOTATION_STATUS_GRAPH);
                 oGraphDto1.setAlgraphdataset(oReportsDao.getDataSetForGraph(conn,oReportDto));
                 cfilename=generateGraph(oGraphDto1.getAlgraphdataset(),oReportDto,request,response);
                 

                 if(filename.equalsIgnoreCase("nodata.gif")){
                     oGraphDto.setGraphUrl(sNographURL); 
                 }else{
                 oGraphDto.setGraphUrl(graphURL+filename);
                 }
                 oGraphDto.setGraphName(filename);
                 oGraphDto.setGraphTitle("Sales Managers");
                 oGraphDto.setGraphHrefUrl(generateUrl(oReportDto,2,0));
                 if(sfilename.equalsIgnoreCase("nodata.gif")){
                     oGraphDto.setGraphUrl2(sNographURL); 
                 }else{
                 oGraphDto.setGraphUrl2(graphURL+sfilename);
                 }
                 oGraphDto.setGraphName2(sfilename);                 
                 oGraphDto.setGraphTitle2("Customer");
                 oGraphDto.setGraphHrefUrl2(generateUrl(oReportDto,3,0));
                 if(cfilename.equalsIgnoreCase("nodata.gif")){
                	 oGraphDto1.setGraphUrl(sNographURL); 
                 }else{
                 oGraphDto1.setGraphUrl(graphURL+cfilename);
                 }
                 oGraphDto1.setGraphName(cfilename);
                 oGraphDto1.setGraphTitle("Status");
                 oGraphDto1.setGraphHrefUrl(generateUrl(oReportDto,4,0));
                 
                 alGraph.add(oGraphDto);   
                 alGraph.add(oGraphDto1);
            	 
            	 
             }else{
            	 
                 GraphDto oGraphDto= new GraphDto();
                 GraphDto oGraphDto1= new GraphDto();
                 oReportDto.setGraphType(QuotationConstants.RBC_QUOTATION_REGION_GRAPH);
                 oGraphDto.setAlgraphdataset(oReportsDao.getDataSetForGraph(conn,oReportDto));
                 filename= generateGraph(oGraphDto.getAlgraphdataset(),oReportDto,request,response);
                 
                 
                 oReportDto.setGraphType(QuotationConstants.RBC_QUOTATION_SALES_MANAGER_GRAPH);
                 oGraphDto.setAlgraphdataset2(oReportsDao.getDataSetForGraph(conn,oReportDto));
                 sfilename=generateGraph(oGraphDto.getAlgraphdataset2(),oReportDto,request,response);
                 
                 oReportDto.setGraphType(QuotationConstants.RBC_QUOTATION_CUSTOMER_GRAPH);
                 oGraphDto1.setAlgraphdataset(oReportsDao.getDataSetForGraph(conn,oReportDto));
                 cfilename=generateGraph(oGraphDto1.getAlgraphdataset(),oReportDto,request,response);
                 oReportDto.setGraphType(QuotationConstants.RBC_QUOTATION_STATUS_GRAPH);
                 oGraphDto1.setAlgraphdataset2(oReportsDao.getDataSetForGraph(conn,oReportDto));
                 icfilename=generateGraph(oGraphDto1.getAlgraphdataset2(),oReportDto,request,response);
                 
                 
                 if(filename.equalsIgnoreCase("nodata.gif")){
                     oGraphDto.setGraphUrl(sNographURL); 
                 }else{
                 oGraphDto.setGraphUrl(graphURL+filename);
                 }
                 oGraphDto.setGraphName(filename);
                 oGraphDto.setGraphTitle("Regions");
                 oGraphDto.setGraphHrefUrl(generateUrl(oReportDto,1,0));
                 if(sfilename.equalsIgnoreCase("nodata.gif")){
                     oGraphDto.setGraphUrl2(sNographURL); 
                 }else{
                 oGraphDto.setGraphUrl2(graphURL+sfilename);
                 }
                 oGraphDto.setGraphName2(sfilename);                 
                 oGraphDto.setGraphTitle2("Sales Managers");
                 oGraphDto.setGraphHrefUrl2(generateUrl(oReportDto,2,0));
                 if(cfilename.equalsIgnoreCase("nodata.gif")){
                	 oGraphDto1.setGraphUrl(sNographURL); 
                 }else{	
                 oGraphDto1.setGraphUrl(graphURL+cfilename);
                 }
                 oGraphDto1.setGraphName(cfilename);
                 oGraphDto1.setGraphTitle("Customer");
                 oGraphDto1.setGraphHrefUrl(generateUrl(oReportDto,3,0));
                 if(icfilename.equalsIgnoreCase("nodata.gif")){
                	 oGraphDto1.setGraphUrl2(sNographURL); 
                 }else{
                 oGraphDto1.setGraphUrl2(graphURL+icfilename);
                 }
                 oGraphDto1.setGraphName2(icfilename);               
                 oGraphDto1.setGraphTitle2("Status");
                 oGraphDto1.setGraphHrefUrl2(generateUrl(oReportDto,4,0));
                 
                 alGraph.add(oGraphDto);   
                 alGraph.add(oGraphDto1);                 
                 
             }
                
             
             request.setAttribute("resultsGraph",alGraph);
        }catch(Exception e) {
            /*
             * Logging any exception that raised during this activity in log files using Log4j
             */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "viewGraph", ExceptionUtility.getStackTraceAsString(e));
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.APP_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.APP_FORWARD_FAILURE);
        } finally {
            /* Releasing Connection */
            oDBUtility.releaseResources(conn);
        }   
        
        return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_SUCCESS);
    }
        
    
    /**
     * This method is used to generating graph based on given dataset  
     * @param alResults ArrayList object to contain collection for dataset
     * @param oReportDto ReportDto object to transfer all the form elements
     * @param request HttpServletRequest object to handle request operations
     * @param response HttpServletResponse object to handle response operations
     * @return returns String  which is file  name for generated graph
     * @author 100003810
     * Created on: July 7, 2008
     */ 
    private String generateGraph(ArrayList alResults,ReportDto oReportDto, HttpServletRequest request,HttpServletResponse response){
        
        String sFilename=null;
        
        String sSeriesname="";
        
        CategoryAxis categoryAxis = new CategoryAxis("");
        ValueAxis valueAxis = new NumberAxis("");
        
        if (alResults.size() == 0) {
            
        	return sFilename="nodata.gif";
        }        
    

        
        try{
        //  Create and populate a CategoryDataset
        Iterator iter = alResults.listIterator();
        BarRenderer renderer = new BarRenderer();
        
        // Setting Gradient to Graph
        GradientPaint gp0 = new GradientPaint( 0.0f, 0.0f, new Color(30,95,143) , 0.0f, 0.0f, new Color(187,250,243) ); 
        renderer.setSeriesPaint(0, gp0);
        renderer.setGradientPaintTransformer( new StandardGradientPaintTransformer( GradientPaintTransformType.CENTER_HORIZONTAL));
        
        switch(oReportDto.getGraphType()){
        
        case QuotationConstants.RBC_QUOTATION_REGION_GRAPH : {
            sSeriesname="Region";         
            //renderer.setItemURLGenerator(new StandardCategoryURLGenerator(generateUrl(oReportDto,1,1),"series","section"));
            renderer.setSeriesItemURLGenerator(0,new StandardCategoryURLGenerator(generateUrl(oReportDto,1,1),"series","section")); 
            break;
        }
        
        case QuotationConstants.RBC_QUOTATION_SALES_MANAGER_GRAPH: {
            sSeriesname="Sales Representives";
            //renderer.setItemURLGenerator(new StandardCategoryURLGenerator(generateUrl(oReportDto,2,1),"series","section"));
            renderer.setSeriesItemURLGenerator(0,new StandardCategoryURLGenerator(generateUrl(oReportDto,2,1),"series","section")); 
            break;
        }
        case QuotationConstants.RBC_QUOTATION_CUSTOMER_GRAPH: {
            sSeriesname="Customers";
            //renderer.setItemURLGenerator(new StandardCategoryURLGenerator(generateUrl(oReportDto,3,1),"series","section"));
            renderer.setSeriesItemURLGenerator(0,new StandardCategoryURLGenerator(generateUrl(oReportDto,3,1),"series","section"));     
            break;
        }
        case QuotationConstants.RBC_QUOTATION_STATUS_GRAPH: {
            sSeriesname="Status";
            //renderer.setItemURLGenerator(new StandardCategoryURLGenerator(generateUrl(oReportDto,4,1),"series","section"));
            renderer.setSeriesItemURLGenerator(0,new StandardCategoryURLGenerator(generateUrl(oReportDto,4,0),"series","section"));     
            break;
        }
        case QuotationConstants.RBC_QUOTATION_CUSTOMER_LOCATIONS_GRAPH: {
            sSeriesname="CustomerLocation";
            //renderer.setItemURLGenerator(new StandardCategoryURLGenerator(generateUrl(oReportDto,5,1),"series","section"));
            renderer.setSeriesItemURLGenerator(0,new StandardCategoryURLGenerator(generateUrl(oReportDto,5,0),"series","section"));         
            break;
        }
        case QuotationConstants.RBC_QUOTATION_MONTHLY_GRAPH: {
            sSeriesname="Monthly";
            CategoryLabelPositions oCategoryLabelPositions1=createUpRotationLabelPositions(45.00);
            categoryAxis.setCategoryLabelPositions(oCategoryLabelPositions1);
            //renderer.setItemURLGenerator(new StandardCategoryURLGenerator(generateUrl(oReportDto,6,0),"series","section"));           
            renderer.setSeriesItemURLGenerator(0,new StandardCategoryURLGenerator(generateUrl(oReportDto,6,0),"series","section"));
            break;
        }
        
        
        }
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        while (iter.hasNext()) {
            
            ReportDatasetDto oReportDatasetDto = (ReportDatasetDto)iter.next();
            dataset.addValue(new Integer(oReportDatasetDto.getOpenCount()), sSeriesname, oReportDatasetDto.getSection());
        }
        //    Create the chart object
        
        //renderer.setToolTipGenerator(new StandardCategoryToolTipGenerator()); 
        renderer.setSeriesToolTipGenerator(0,new StandardCategoryToolTipGenerator());
        Plot plot = new CategoryPlot(dataset, categoryAxis, valueAxis, renderer);
        JFreeChart chart = new JFreeChart("", JFreeChart.DEFAULT_TITLE_FONT, plot, false);
        chart.setBackgroundPaint(java.awt.Color.white);
        
        //  Write the chart image to the temporary directory
        ChartRenderingInfo info = new ChartRenderingInfo(new StandardEntityCollection());
        sFilename = ServletUtilities.saveChartAsPNG(chart, 450, 300, info, request.getSession());
        //Write the image map to the PrintWriter
        PrintWriter pw = response.getWriter();
        ChartUtilities.writeImageMap(pw, sFilename, info,false);            
        pw.flush();        
        
        }        
        catch(Exception e) {
            /*
             * Logging any exception that raised during this activity in log files using Log4j
             */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "generateGraph", ExceptionUtility.getStackTraceAsString(e));            
            
        }       
        return sFilename;
    }
    
    
    
    /**
     * This method is used to populate Form object based on request parameters  
     * @param request HttpServletRequest object to handle request operations
     * @param oReportForm ReportForm object to handle the form elements
     * @param oReportDto ReportDto object to transfer all the form elements
     * @throws Exception if any error occurs while copy property operation
     * @author 100003810
     * Created on: July 7, 2008
     */ 
    private void setRequesttoFrom(HttpServletRequest request,ReportForm oReportForm,ReportDto oReportDto) throws Exception{
    	
    	
    	  int iRegionID= CommonUtility.getIntParameter(request,"regionid");
          int iSalesMangerID= CommonUtility.getIntParameter(request,"salesmangerid");
          int iLocationID= CommonUtility.getIntParameter(request,"locationid");
          int iCustomerID= CommonUtility.getIntParameter(request,"customerid");        
          int iStatusID= CommonUtility.getIntParameter(request,"statusid");
          String sYear= CommonUtility.getStringParameter(request,"year");
          String sMonth=CommonUtility.getStringParameter(request,"month");
     
          
          oReportForm.setRegionId(iRegionID);
          oReportForm.setSalesManagerId(iSalesMangerID);
          oReportForm.setLocationId(iLocationID);
          oReportForm.setCustomerId(iCustomerID);
          oReportForm.setSatusId(iStatusID);
          oReportForm.setYear(sYear);
          oReportForm.setMonth(sMonth);     
          
          ConverterUtility.copyProperties(oReportDto,oReportForm);
    	
    	
    	
    }
        
    /**
     * This method is used to generate url for graphs drill down 
     * @param oReportDto ReportDto object to transfer all the form elements
     * @param iGraphType int for type of graph like business, sub business etc
     * @param iUraltype  int for type of url 1 for drill down and other for search
     * @author 100003810
     * Created on: July 7, 2008
     */ 
    private String generateUrl(ReportDto oReportDto,int iGraphType ,int iUraltype)  {
    	
    	
    	StringBuffer sbUrl = new StringBuffer();
        if(iUraltype == 1)
        {
        sbUrl.append("viewreport.do?invoke=viewGraph");
        }else{
            
         sbUrl.append("searchenquiry.do?invoke=interimSearch");
        }
        sbUrl.append("&regionid="+oReportDto.getRegionId());
        sbUrl.append("&salesmangerid="+oReportDto.getSalesManagerId());
        sbUrl.append("&locationid="+oReportDto.getLocationId());
        sbUrl.append("&customerid="+oReportDto.getCustomerId());
        sbUrl.append("&year="+oReportDto.getYear());
        sbUrl.append("&month="+oReportDto.getMonth());
        sbUrl.append("&statusid="+oReportDto.getSatusId());
        
        sbUrl.append("&graphtype="+iGraphType);     
       
        return  sbUrl.toString();
    }
    
    /**
     * This method is used to generate label position for give angel
     * @param angle double angle for label position
     * @author 100003810
     * Created on: July 7, 2008
     */ 
    private static CategoryLabelPositions createUpRotationLabelPositions(
            double angle) {
        return new CategoryLabelPositions(
            new CategoryLabelPosition(
                RectangleAnchor.BOTTOM, TextBlockAnchor.BOTTOM_LEFT, 
                TextAnchor.BOTTOM_LEFT, -angle,
                CategoryLabelWidthType.RANGE, 0.50f
            ), // TOP
            new CategoryLabelPosition(
                RectangleAnchor.TOP, TextBlockAnchor.TOP_RIGHT, 
                TextAnchor.TOP_RIGHT, -angle,
                CategoryLabelWidthType.RANGE, 0.50f
            ), // BOTTOM
            new CategoryLabelPosition(
                RectangleAnchor.RIGHT, TextBlockAnchor.BOTTOM_RIGHT, 
                TextAnchor.BOTTOM_RIGHT, -angle,
                CategoryLabelWidthType.RANGE, 0.50f
            ), // LEFT
            new CategoryLabelPosition(
                RectangleAnchor.LEFT, TextBlockAnchor.TOP_LEFT, 
                TextAnchor.TOP_LEFT, -angle,
                CategoryLabelWidthType.RANGE, 0.50f
            ) // RIGHT
        );
    }
}
