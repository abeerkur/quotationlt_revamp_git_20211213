   /**
 * ******************************************************************************************
 * Project Name: Quality Tools
 * Document Name: GraphDto
 * Package: in.com.rbc.quality.fracas.reports.dto
 * Desc: DTO class that holds getters and setters of each property for dataset.
 * ******************************************************************************************
 * Author: 100003810 (Subhakar Edeti)
 * ******************************************************************************************
 */
package in.com.rbc.quotation.reports.dto;

import java.util.ArrayList;

/**
 * This Class is used for to transfer Graph information to screen
 */
public class GraphDto {

/**
 * Stroies the graphName  
 */	
 private String graphName=null;
 /**
  * Stroies the graphName2  
  */	
 private String graphName2=null;
 /**
  * Stroies the graphUrl  
  */
 private String graphUrl=null;
 /**
  * Stroies the graphUrl  
  */
 private String graphHrefUrl=null;
 /**
  * Stroies the graphHrefUrl2  
  */
 private String graphHrefUrl2=null;
 /**
  * Stroies the algraphdataset  
  */
 private ArrayList algraphdataset=new ArrayList();
 /**
  * Stroies the algraphdataset2  
  */
 private ArrayList algraphdataset2=new ArrayList();
 /**
  * Stroies the graphUrl2  
  */ 
 private String graphUrl2=null;
 /**
  * Stroies the graphTitle  
  */
 private String graphTitle=null;
 /**
  * Stroies the graphTitle2  
  */
 private String graphTitle2=null;
 
 /**
  * @return Returns the graphName.
  */ 
public String getGraphName() {
	return graphName;
}
/**
 * @param graphName The graphName to set.
 */
public void setGraphName(String graphName) {
	this.graphName = graphName;
}
/**
 * @return Returns the graphName2.
 */
public String getGraphName2() {
	return graphName2;
}
/**
 * @param graphName2 The graphName2 to set.
 */
public void setGraphName2(String graphName2) {
	this.graphName2 = graphName2;
}
/**
 * @return Returns the graphTitle.
 */
public String getGraphTitle() {
	return graphTitle;
}
/**
 * @param graphTitle The graphTitle to set.
 */
public void setGraphTitle(String graphTitle) {
	this.graphTitle = graphTitle;
}
/**
 * @return Returns the graphTitle2.
 */
public String getGraphTitle2() {
	return graphTitle2;
}
/**
 * @param graphTitle2 The graphTitle2 to set.
 */
public void setGraphTitle2(String graphTitle2) {
	this.graphTitle2 = graphTitle2;
}
/**
 * @return Returns the graphUrl.
 */
public String getGraphUrl() {
	return graphUrl;
}
/**
 * @param graphUrl The graphUrl to set.
 */
public void setGraphUrl(String graphUrl) {
	this.graphUrl = graphUrl;
}
/**
 * @return Returns the graphUrl2.
 */
public String getGraphUrl2() {
	return graphUrl2;
}
/**
 * @param graphUrl2 The graphUrl2 to set.
 */
public void setGraphUrl2(String graphUrl2) {
	this.graphUrl2 = graphUrl2;
}
/**
 * @return Returns the algraphdataset.
 */
public ArrayList getAlgraphdataset() {
	return algraphdataset;
}
/**
 * @param algraphdataset The algraphdataset to set.
 */
public void setAlgraphdataset(ArrayList algraphdataset) {
	this.algraphdataset = algraphdataset;
}
/**
 * @return Returns the algraphdataset2.
 */
public ArrayList getAlgraphdataset2() {
	return algraphdataset2;
}
/**
 * @param algraphdataset2 The algraphdataset2 to set.
 */
public void setAlgraphdataset2(ArrayList algraphdataset2) {
	this.algraphdataset2 = algraphdataset2;
}
/**
 * @return Returns the graphHrefUrl.
 */
public String getGraphHrefUrl() {
	return graphHrefUrl;
}
/**
 * @param graphHrefUrl The graphHrefUrl to set.
 */
public void setGraphHrefUrl(String graphHrefUrl) {
	this.graphHrefUrl = graphHrefUrl;
}
/**
 * @return Returns the graphHrefUrl2.
 */
public String getGraphHrefUrl2() {
	return graphHrefUrl2;
}
/**
 * @param graphHrefUrl2 The graphHrefUrl2 to set.
 */
public void setGraphHrefUrl2(String graphHrefUrl2) {
	this.graphHrefUrl2 = graphHrefUrl2;
}
	
}