/**
 * ******************************************************************************************
 * Project Name: Quality Tools
 * Document Name: CreateRequestForm.java
 * Package: in.com.rbc.quality.fracas.request.form
 * Desc: DTO class that holds getters and setters of each property.
 * ******************************************************************************************
 * Author: 100002572(Sunanda Devulapally)
 * ******************************************************************************************
 */
package in.com.rbc.quotation.reports.form;



import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

/**
 * This Class is used for to hold form elements 
 */
public class ReportForm extends ActionForm{
	
    private int regionId  = 0 ;
    private int locationId=0;
    private int salesManagerId=0;
    private int customerId=0;
    private int satusId=0;
    private String year="0";
    private String month="0";
    
    private ArrayList regionList  = new ArrayList();
    private ArrayList locationList  = new ArrayList();
    private ArrayList salesManagerList  = new ArrayList();
    private ArrayList customerList  = new ArrayList();
    private ArrayList statusList  = new ArrayList();
    private ArrayList yearList  = new ArrayList();
    
    
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	public ArrayList getCustomerList() {
		return customerList;
	}
	public void setCustomerList(ArrayList customerList) {
		this.customerList = customerList;
	}
	public int getLocationId() {
		return locationId;
	}
	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}
	public ArrayList getLocationList() {
		return locationList;
	}
	public void setLocationList(ArrayList locationList) {
		this.locationList = locationList;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public int getRegionId() {
		return regionId;
	}
	public void setRegionId(int regionId) {
		this.regionId = regionId;
	}
	public ArrayList getRegionList() {
		return regionList;
	}
	public void setRegionList(ArrayList regionList) {
		this.regionList = regionList;
	}
	public int getSalesManagerId() {
		return salesManagerId;
	}
	public void setSalesManagerId(int salesManagerId) {
		this.salesManagerId = salesManagerId;
	}
	public ArrayList getSalesManagerList() {
		return salesManagerList;
	}
	public void setSalesManagerList(ArrayList salesManagerList) {
		this.salesManagerList = salesManagerList;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public ArrayList getStatusList() {
		return statusList;
	}
	public void setStatusList(ArrayList statusList) {
		this.statusList = statusList;
	}
	public ArrayList getYearList() {
		return yearList;
	}
	public void setYearList(ArrayList yearList) {
		this.yearList = yearList;
	}
	public int getSatusId() {
		return satusId;
	}
	public void setSatusId(int satusId) {
		this.satusId = satusId;
	}
    
	
}

