package in.com.rbc.quotation.reports.dao;

import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.utility.CommonUtility;
import in.com.rbc.quotation.common.utility.DBUtility;
import in.com.rbc.quotation.common.utility.LoggerUtility;
import in.com.rbc.quotation.reports.dto.ReportDatasetDto;
import in.com.rbc.quotation.reports.dto.ReportDto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;


/**
 * This class contains all the database method used for
 * generating graphs.
 */
public class ReportsDaoImpl implements ReportsDao, ReportsQueries {
	
	
	/**
     * Retrieves the list of ReportsDataSetDto objects
     * @param conn Connection object to connect to database
     * @param oReportDto ReportDto object which has Report Input information
     * @return Returns the list of ReportsDataSetDto objects
     * @throws Exception If any error occurs during the process
     * @author 100002668 (Anand Yalla)
     * Created on: July 17, 2008
     */
    public ArrayList getDataSetForGraph(Connection conn, ReportDto oReportDto) throws Exception {

        String sMethod = "getDataSetForGraph";
        
        String iSalesmangerID="0";
        
        LoggerUtility.log("INFO", this.getClass().getName(), sMethod," ReportDaoImpl "+sMethod+"() START ");
     
        /* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        /* ArrayList to store the list of disposition to information */
        ArrayList alDataSet = new ArrayList();
        
        try {
            
        	if(oReportDto.getSalesManagerId()>0){
            	oReportDto.setSalesManagerSso(getSalesManagerSSOID(conn,CommonUtility.getStringValue(oReportDto.getSalesManagerId())));  
            	
            }
            pstmt = conn.prepareStatement(getGraphQuery(oReportDto));
            
                     
            rs = pstmt.executeQuery();
            
            while (rs.next()) {
                ReportDatasetDto oReportDatasetDto = new ReportDatasetDto();               
              
                
                if(oReportDto.getGraphType() == QuotationConstants.RBC_QUOTATION_SALES_MANAGER_GRAPH){
                	iSalesmangerID=rs.getString(1);
                    oReportDatasetDto.setSectionId(getSalesManagerID(conn,iSalesmangerID));                    
                                     
                }else{
                	  oReportDatasetDto.setSectionId(rs.getInt(1));
                }
                oReportDatasetDto.setSection(rs.getString(2));
                oReportDatasetDto.setOpenCount(rs.getInt(3));
                
                alDataSet.add(oReportDatasetDto);
            }
            
        } finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
        
        LoggerUtility.log("INFO", this.getClass().getName(), sMethod," ReportDaoImpl "+sMethod+"() END ");
        
        return alDataSet;
    }
    
    /**
     * Retrieves Query for ReportsDataSetDto objects
     * @param oReportDto ReportDto object which has Report Input information
     * @return Returns the Query for list of ReportsDataSetDto objects,Dataset
     * @throws Exception If any error occurs during the process
     * @author 100002668 (Anand Yalla)
     * Created on: July 23, 2008
     */
    private String getGraphQuery( ReportDto oReportDto ) throws Exception {
        StringBuffer sbQuery = new StringBuffer();
        
        StringBuffer sbWhereQuery = new StringBuffer();
        
        String sColumnName  = "";
        String sColumnId  = "";
        
        
        if(oReportDto.getGraphType() > 0 ){
            switch(oReportDto.getGraphType()) {
                case QuotationConstants.RBC_QUOTATION_REGION_GRAPH: 
                   sColumnName = "QEM_REGIONID";
                   sColumnId = "QEM_REGIONNAME";
                   break;
                case QuotationConstants.RBC_QUOTATION_SALES_MANAGER_GRAPH: 
                    sColumnName = "QEM_CREATEDBY";
                    sColumnId = "QEM_CREATEDBYNAME";
                    break;
                case QuotationConstants.RBC_QUOTATION_CUSTOMER_GRAPH:
                    sColumnName = "QEM_CUSTOMERID";
                    sColumnId = "QEM_CUSTOMERNAME";
                    break;
                case QuotationConstants.RBC_QUOTATION_STATUS_GRAPH: 
                    sColumnName = "QEM_STATUSID";
                    sColumnId = "QEM_STATUSDESC";
                    break;
                case QuotationConstants.RBC_QUOTATION_CUSTOMER_LOCATIONS_GRAPH:
                    sColumnName = "QEM_LOCATIONID";
                    sColumnId = "QEM_LOCATIONNAME";
                    break;
                case QuotationConstants.RBC_QUOTATION_MONTHLY_GRAPH:
                    sColumnName = "";
                    sColumnId = "";
                    break;
             
            }  
            
            if(oReportDto.getRegionId() > 0 )
                sbWhereQuery.append(" AND QEM_REGIONID = "+oReportDto.getRegionId());
            if(oReportDto.getSalesManagerId() > 0 )
                sbWhereQuery.append(" AND QEM_CREATEDBY = "+oReportDto.getSalesManagerSso());
            if(oReportDto.getCustomerId() > 0 )
                sbWhereQuery.append(" AND QEM_CUSTOMERID = "+oReportDto.getCustomerId());
            if(oReportDto.getLocationId() > 0 )
                sbWhereQuery.append(" AND QEM_LOCATIONID = "+oReportDto.getLocationId());
            if(oReportDto.getCustomerId() > 0 )
                sbWhereQuery.append(" AND QEM_CUSTOMERID = "+oReportDto.getCustomerId());
            if(oReportDto.getSatusId() > 0 )
                sbWhereQuery.append(" AND QEM_STATUSID = "+oReportDto.getSatusId());            
            if(!oReportDto.getYear().equalsIgnoreCase("0")){
                sbWhereQuery.append(" AND TO_CHAR(QEM_CREATEDDATE, 'yyyy')  = '"+oReportDto.getYear());
                sbWhereQuery.append("'");            }              
            if(!oReportDto.getMonth().equalsIgnoreCase("0")){
                sbWhereQuery.append(" AND TO_CHAR(QEM_CREATEDDATE, 'mm')  = '"+oReportDto.getMonth());
                sbWhereQuery.append("'");
            }   
            
            
            sbQuery.append(" SELECT Q.* FROM (SELECT ");
            
            sbQuery.append(sColumnName+" , "+sColumnId);
            sbQuery.append(", COUNT(1) ENQUIRY_COUNT FROM ");
            sbQuery.append( SCHEMA_QUOTATION );
            sbQuery.append(".RFQ_ENQUIRES_V WHERE 1=1 ");
            sbQuery.append(sbWhereQuery.toString());
            sbQuery.append(" GROUP BY ");
            sbQuery.append(sColumnId+" , "+sColumnName);
            sbQuery.append(" ) Q WHERE ROWNUM<=10 ");          
          
                       
          
        }       
        
        return sbQuery.toString();
    }		
    
    private int getSalesManagerID(Connection conn,String sSssoID) throws Exception{
    	
    	/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        try{
        	pstmt = conn.prepareStatement(FETCH_SALESMANAGER_ID);
        	pstmt.setString(1,sSssoID);
            
            rs = pstmt.executeQuery();
            if(rs.next()){
            	return rs.getInt(1);	
            }
        }finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
    
    	
    	return 0;
    }
    
 private String getSalesManagerSSOID(Connection conn,String sSssoID) throws Exception{
    	
    	/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        try{
        	pstmt = conn.prepareStatement(FETCH_SALESMANAGER_SSOID);
        	pstmt.setString(1,sSssoID);
            
            rs = pstmt.executeQuery();
            if(rs.next()){
            	return rs.getString(1);	
            }
        }finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
    
    	
    	return "";
    }
 
 /**
	 * Retrieves the RegionID
	 * @param conn Connection object to connect to database
	 * @param sBusidesc String Object
	 * @return Returns BusinessID
	 * @throws Exception If any error occurs during the process
	 * @author 100003810 (Subhakar Edeti)
	 * Created on: July 08, 2008
	 */
	public int getRegionID(Connection conn,String sRegiondesc) throws Exception{
		
		LoggerUtility.log("INFO", this.getClass().getName(), "getRegionID"," ReportDaoImpl getRegionID() START ");
	   	 
     /* PreparedStatement object to handle database operations */
     PreparedStatement pstmt = null ;        
     
     /* ResultSet object to store the rows retrieved from database */
     ResultSet rs = null ;        
   
     /* Object of DBUtility class to handle database operations */
     DBUtility oDBUtility = new DBUtility();
     
     /* ArrayList to store the list of disposition to information */
     int iRegionID = 0;
     try {  
     	
    	 pstmt = conn.prepareStatement(FETCH_REGION_ID);    
    	 pstmt.setString(1,sRegiondesc);
    	 rs = pstmt.executeQuery();
      
    	if(rs.next()){
   	  
    		iRegionID=rs.getInt("QRE_REGIONID");          
 	}
                 
    	
    } finally {
        /* Releasing ResultSet & PreparedStatement objects */
        oDBUtility.releaseResources(rs, pstmt);          
		
    }
    LoggerUtility.log("INFO", this.getClass().getName(), "getRegionID"," RequestDaoImpl getRegionID() END ");
    return iRegionID;	
		
	}
	
	/**
	 * Retrieves the SaleManagerID
	 * @param conn Connection object to connect to database
	 * @param sBusidesc String Object
	 * @return Returns BusinessID
	 * @throws Exception If any error occurs during the process
	 * @author 100003810 (Subhakar Edeti)
	 * Created on: July 08, 2008
	 */
	public int getSaleManagerID(Connection conn,String sSalesMangerdesc) throws Exception{
		
		LoggerUtility.log("INFO", this.getClass().getName(), "getSaleManagerID"," ReportDaoImpl getSaleManagerID() START ");
	   	 
     /* PreparedStatement object to handle database operations */
     PreparedStatement pstmt = null ;        
     
     /* ResultSet object to store the rows retrieved from database */
     ResultSet rs = null ;        
   
     /* Object of DBUtility class to handle database operations */
     DBUtility oDBUtility = new DBUtility();
     
     /* ArrayList to store the list of disposition to information */
     int iSalesManagerID = 0;
     try {  
     	
    	 pstmt = conn.prepareStatement(FETCH_SALESMANAGER_ID);    
    	 pstmt.setString(1,sSalesMangerdesc);
    	 rs = pstmt.executeQuery();
      
    	if(rs.next()){
   	  
    		iSalesManagerID=rs.getInt("SALESMGR_ID");          
 	}
                 
    	
    } finally {
        /* Releasing ResultSet & PreparedStatement objects */
        oDBUtility.releaseResources(rs, pstmt);          
		
    }
    LoggerUtility.log("INFO", this.getClass().getName(), "getSaleManagerID"," RequestDaoImpl getSaleManagerID() END ");
    return iSalesManagerID;	
		
	}
	
	/**
	 * Retrieves the CustomerID
	 * @param conn Connection object to connect to database
	 * @param sBusidesc String Object
	 * @return Returns BusinessID
	 * @throws Exception If any error occurs during the process
	 * @author 100003810 (Subhakar Edeti)
	 * Created on: July 08, 2008
	 */
	public int getCustomerID(Connection conn,String sCustomerdesc) throws Exception{
		
		LoggerUtility.log("INFO", this.getClass().getName(), "getCustomerID"," ReportDaoImpl getCustomerID() START ");
	   	 
     /* PreparedStatement object to handle database operations */
     PreparedStatement pstmt = null ;        
     
     /* ResultSet object to store the rows retrieved from database */
     ResultSet rs = null ;        
   
     /* Object of DBUtility class to handle database operations */
     DBUtility oDBUtility = new DBUtility();
     
     /* ArrayList to store the list of disposition to information */
     int iCustomerID = 0;
     try {  
     	
    	 pstmt = conn.prepareStatement(FETCH_CUSTOMER_ID);    
    	 pstmt.setString(1,sCustomerdesc);
    	 rs = pstmt.executeQuery();
      
    	if(rs.next()){
   	  
    		iCustomerID=rs.getInt("QCU_CUSTOMERID");          
 	}
                 
    	
    } finally {
        /* Releasing ResultSet & PreparedStatement objects */
        oDBUtility.releaseResources(rs, pstmt);          
		
    }
    LoggerUtility.log("INFO", this.getClass().getName(), "getCustomerID"," RequestDaoImpl getCustomerID() END ");
    return iCustomerID;	
		
	}
	
	/**
	 * Retrieves the CustomerID
	 * @param conn Connection object to connect to database
	 * @param sBusidesc String Object
	 * @return Returns BusinessID
	 * @throws Exception If any error occurs during the process
	 * @author 100003810 (Subhakar Edeti)
	 * Created on: July 08, 2008
	 */
	public int getLocationID(Connection conn,String sLocationdesc) throws Exception{
		
		LoggerUtility.log("INFO", this.getClass().getName(), "getLocationID"," ReportDaoImpl getLocationID() START ");
	   	 
     /* PreparedStatement object to handle database operations */
     PreparedStatement pstmt = null ;        
     
     /* ResultSet object to store the rows retrieved from database */
     ResultSet rs = null ;        
   
     /* Object of DBUtility class to handle database operations */
     DBUtility oDBUtility = new DBUtility();
     
     /* ArrayList to store the list of disposition to information */
     int iLocationID = 0;
     try {  
     	
    	 pstmt = conn.prepareStatement(FETCH_LOCATION_ID);    
    	 pstmt.setString(1,sLocationdesc);
    	 rs = pstmt.executeQuery();
      
    	if(rs.next()){
   	  
    		iLocationID=rs.getInt("QLO_LOCATIONID");          
 	}
                 
    	
    } finally {
        /* Releasing ResultSet & PreparedStatement objects */
        oDBUtility.releaseResources(rs, pstmt);          
		
    }
    LoggerUtility.log("INFO", this.getClass().getName(), "getLocationID"," RequestDaoImpl getLocationID() END ");
    return iLocationID;	
		
	}
}