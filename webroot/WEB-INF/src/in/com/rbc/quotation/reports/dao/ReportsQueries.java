package in.com.rbc.quotation.reports.dao;

import in.com.rbc.quotation.common.utility.PropertyUtility;
import in.com.rbc.quotation.common.constants.QuotationConstants;


/**
 * This interface contains all the database queries used for 
 * generating graphs.
 */
public interface ReportsQueries {
	
	/**
     * String variable to store the QUALITY schema name.
     * It is retrieved from fracas.properties file
     */
    public final String SCHEMA_QUOTATION = PropertyUtility.getKeyValue(
    									QuotationConstants.QUOTATION_PROPERTYFILENAME,
    									QuotationConstants.QUOTATION_DB_SCHEMA_QUOTATION);  
    
    /*
     *  Query retrieves the Salesmanager ID 
    */ 
   public final String FETCH_SALESMANAGER_ID="SELECT SALESMGR_ID FROM "+SCHEMA_QUOTATION+".RFQ_SALESMANAGER_V WHERE UPPER(SALESMGR_NAME)=UPPER(?)";
   
   public final String FETCH_SALESMANAGER_SSOID="SELECT QSM_SALESMGR FROM "+SCHEMA_QUOTATION+".RFQ_SALESMANAGER WHERE QSM_SALESMGRID=?";
   
   public final String FETCH_REGION_ID="SELECT QRE_REGIONID FROM "+SCHEMA_QUOTATION+".RFQ_REGION WHERE UPPER(QRE_REGIONNAME)=UPPER(?)"; 
   
   public final String FETCH_CUSTOMER_ID="SELECT QCU_CUSTOMERID FROM "+SCHEMA_QUOTATION+".RFQ_CUSTOMER WHERE UPPER(QCU_NAME) =UPPER(?)";
   
   public final String FETCH_LOCATION_ID ="SELECT QLO_LOCATIONID FROM "+SCHEMA_QUOTATION+".RFQ_LOCATION WHERE  UPPER(QLO_LOCATIONNAME)=UPPER(?)";
   
   
   
	
}
