/**
 * *****************************************************************************
 * Project Name: quotation
 * Document Name: WorkflowDto.java
 * Package: in.com.rbc.quotation.workflow.dto
 * Desc: 
 * *****************************************************************************
 * Author: 100002865 (Shanthi Chinta)
 * Date: Oct 3, 2008
 * *****************************************************************************
 */
package in.com.rbc.quotation.workflow.dto;

import in.com.rbc.quotation.common.utility.CommonUtility;
import in.com.rbc.quotation.enquiry.dto.EnquiryDto;

public class WorkflowDto {
	
    private long enquiryId = 0;
    private String enquiryNo = null;
    private long baseEnquiryId = 0;
    
    private String actionCode = null;
    private String actionDesc = null;
    
    private String workflowStatusCode = null;
    private String workflowStatus = null;
    
    private int statusId = 0;
    private String status = null;
    
    private int oldStatusId = 0;
    private String oldStatus = null;
    
    private int newStatusId = 0;
    private String newStatus = null;
    
    private String nextLevelCode = null;
    private String nextLevelDesc = null;

    private String assignedTo = null;
    private String assignedToName = null;
    private String assignedToEmail = null;
    
    private String loginUserId = null;
    private String loginUserName = null;
    private String loginUserEmail = null;

    private String customerName=null;
    private String customerId=null;
    
    private String attachmentFile = null;
    private String attachmentFileName = null; //This Variable Stores FileNames
    
    private String emailFlag = null;
    private String createdDate = null;
    private String createdByName = null;
    private String lastModifiedDate = null;
    private String remarks = null;
    private String projName = null;
    private String appUrl = null;
    private String equiryOperationType=null;
    private String returnEnquiry=null;
    private String ccEmailTo = null;
    
    private String smNote=null;
    private String dmNote=null;
    private String ceNote=null;
    private String rsmNote=null;
    private String smName=null;
    private String dmSsoid=null;
    private String dmName=null;
    private String ceSsoid=null;
    private String ceName=null;
    private String rsmSsoid=null;
    private String rsmName=null;
    private String cmNote=null;
    private String pageChecking=null;

    private String dmToSmReturn=null;
    private String ceToDmReturn=null;
    private String ceToSmReturn=null;
    private String rsmToCeReturn=null;
    private String rsmToDmReturn=null;
    private String rsmToSmReturn=null;
    private String returnOperation=null;
    private String deToSmReturn=null;
    private String cmToSmReturn=null;
    private String dmToCmReturn=null;
    private String deToCmReturn=null;
    
    EnquiryDto enquiryDto = null;

    //Indent Data Added by Egr on 18-March-2019
    private String savingIndent=null;    
    private String commengToSmReturn=null;
    private String sReturnComment=null;
    private int emailCode=0;
    private String designReference=null;
    
    private String isNewEnquiry=null;
    private String revisionRemarks = null;
    
    public String getDesignReference() {
		return CommonUtility.replaceNull(designReference);
	}
	public void setDesignReference(String designReference) {
		this.designReference = designReference;
	}
	/**
     * @return Returns the actionCode.
     */
    public String getActionCode() {
        return CommonUtility.replaceNull(this.actionCode);
    }
    /**
     * @param actionCode The actionCode to set.
     */
    public void setActionCode(String actionCode) {
        this.actionCode = actionCode;
    }
    /**
     * @return Returns the actionDesc.
     */
    public String getActionDesc() {
        return CommonUtility.replaceNull(this.actionDesc);
    }
    /**
     * @param actionDesc The actionDesc to set.
     */
    public void setActionDesc(String actionDesc) {
        this.actionDesc = actionDesc;
    }
    /**
     * @return Returns the assignedTo.
     */
    public String getAssignedTo() {
        return CommonUtility.replaceNull(this.assignedTo);
    }
    /**
     * @param assignedTo The assignedTo to set.
     */
    public void setAssignedTo(String assignedTo) {
        this.assignedTo = assignedTo;
    }
    /**
     * @return Returns the assignedToName.
     */
    public String getAssignedToName() {
        return CommonUtility.replaceNull(this.assignedToName);
    }
    /**
     * @param assignedToName The assignedToName to set.
     */
    public void setAssignedToName(String assignedToName) {
        this.assignedToName = assignedToName;
    }
    /**
     * @return Returns the createdDate.
     */
    public String getCreatedDate() {
        return CommonUtility.replaceNull(this.createdDate);
    }
    /**
     * @param createdDate The createdDate to set.
     */
    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }
    /**
     * @return Returns the emailFlag.
     */
    public String getEmailFlag() {
        return CommonUtility.replaceNull(this.emailFlag);
    }
    /**
     * @param emailFlag The emailFlag to set.
     */
    public void setEmailFlag(String emailFlag) {
        this.emailFlag = emailFlag;
    }
    /**
     * @return Returns the enquiryId.
     */
    public long getEnquiryId() {
        return this.enquiryId;
    }
    /**
     * @param enquiryId The enquiryId to set.
     */
    public void setEnquiryId(long enquiryId) {
        this.enquiryId = enquiryId;
    }
    /**
     * @return Returns the lastModifiedDate.
     */
    public String getLastModifiedDate() {
        return CommonUtility.replaceNull(this.lastModifiedDate);
    }
    /**
     * @param lastModifiedDate The lastModifiedDate to set.
     */
    public void setLastModifiedDate(String lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }
    /**
     * @return Returns the newStatus.
     */
    public String getNewStatus() {
        return CommonUtility.replaceNull(this.newStatus);
    }
    /**
     * @param newStatus The newStatus to set.
     */
    public void setNewStatus(String newStatus) {
        this.newStatus = newStatus;
    }
    /**
     * @return Returns the newStatusId.
     */
    public int getNewStatusId() {
        return this.newStatusId;
    }
    /**
     * @param newStatusId The newStatusId to set.
     */
    public void setNewStatusId(int newStatusId) {
        this.newStatusId = newStatusId;
    }
    /**
     * @return Returns the nextLevelCode.
     */
    public String getNextLevelCode() {
        return CommonUtility.replaceNull(this.nextLevelCode);
    }
    /**
     * @param nextLevelCode The nextLevelCode to set.
     */
    public void setNextLevelCode(String nextLevelCode) {
        this.nextLevelCode = nextLevelCode;
    }
    /**
     * @return Returns the nextLevelDesc.
     */
    public String getNextLevelDesc() {
        return CommonUtility.replaceNull(this.nextLevelDesc);
    }
    /**
     * @param nextLevelDesc The nextLevelDesc to set.
     */
    public void setNextLevelDesc(String nextLevelDesc) {
        this.nextLevelDesc = nextLevelDesc;
    }
    /**
     * @return Returns the oldStatus.
     */
    public String getOldStatus() {
        return CommonUtility.replaceNull(this.oldStatus);
    }
    /**
     * @param oldStatus The oldStatus to set.
     */
    public void setOldStatus(String oldStatus) {
        this.oldStatus = oldStatus;
    }
    /**
     * @return Returns the oldStatusId.
     */
    public int getOldStatusId() {
        return this.oldStatusId;
    }
    /**
     * @param oldStatusId The oldStatusId to set.
     */
    public void setOldStatusId(int oldStatusId) {
        this.oldStatusId = oldStatusId;
    }
    /**
     * @return Returns the remarks.
     */
    public String getRemarks() {
        return CommonUtility.replaceNull(this.remarks);
    }
    /**
     * @param remarks The remarks to set.
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
    /**
     * @return Returns the workflowStatus.
     */
    public String getWorkflowStatus() {
        return CommonUtility.replaceNull(this.workflowStatus);
    }
    /**
     * @param workflowStatus The workflowStatus to set.
     */
    public void setWorkflowStatus(String workflowStatus) {
        this.workflowStatus = workflowStatus;
    }
    /**
     * @return Returns the workflowStatusCode.
     */
    public String getWorkflowStatusCode() {
        return CommonUtility.replaceNull(this.workflowStatusCode);
    }
    /**
     * @param workflowStatusCode The workflowStatusCode to set.
     */
    public void setWorkflowStatusCode(String workflowStatusCode) {
        this.workflowStatusCode = workflowStatusCode;
    }
    /**
     * @return Returns the enquiryNo.
     */
    public String getEnquiryNo() {
        return CommonUtility.replaceNull(this.enquiryNo);
    }
    /**
     * @param enquiryNo The enquiryNo to set.
     */
    public void setEnquiryNo(String enquiryNo) {
        this.enquiryNo = enquiryNo;
    }
    /**
     * @return Returns the status.
     */
    public String getStatus() {
        return CommonUtility.replaceNull(this.status);
    }
    /**
     * @param status The status to set.
     */
    public void setStatus(String status) {
        this.status = status;
    }
    /**
     * @return Returns the statusId.
     */
    public int getStatusId() {
        return this.statusId;
    }
    /**
     * @param statusId The statusId to set.
     */
    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }
    /**
     * @return Returns the appUrl.
     */
    public String getAppUrl() {
        return CommonUtility.replaceNull(this.appUrl);
    }
    /**
     * @param appUrl The appUrl to set.
     */
    public void setAppUrl(String appUrl) {
        this.appUrl = appUrl;
    }
    /**
     * @return Returns the assignedToEmail.
     */
    public String getAssignedToEmail() {
        return CommonUtility.replaceNull(this.assignedToEmail);
    }
    /**
     * @param assignedToEmail The assignedToEmail to set.
     */
    public void setAssignedToEmail(String assignedToEmail) {
        this.assignedToEmail = assignedToEmail;
    }
    /**
     * @return Returns the loginUserEmail.
     */
    public String getLoginUserEmail() {
        return CommonUtility.replaceNull(this.loginUserEmail);
    }
    /**
     * @param loginUserEmail The loginUserEmail to set.
     */
    public void setLoginUserEmail(String loginUserEmail) {
        this.loginUserEmail = loginUserEmail;
    }
    /**
     * @return Returns the loginUserId.
     */
    public String getLoginUserId() {
        return CommonUtility.replaceNull(this.loginUserId);
    }
    /**
     * @param loginUserId The loginUserId to set.
     */
    public void setLoginUserId(String loginUserId) {
        this.loginUserId = loginUserId;
    }
    /**
     * @return Returns the loginUserName.
     */
    public String getLoginUserName() {
        return CommonUtility.replaceNull(this.loginUserName);
    }
    /**
     * @param loginUserName The loginUserName to set.
     */
    public void setLoginUserName(String loginUserName) {
        this.loginUserName = loginUserName;
    }
    /**
     * @return Returns the enquiryDto.
     */
    public EnquiryDto getEnquiryDto() {
        return this.enquiryDto;
    }
    /**
     * @param enquiryDto The enquiryDto to set.
     */
    public void setEnquiryDto(EnquiryDto enquiryDto) {
        this.enquiryDto = enquiryDto;
    }
    /**
     * @return Returns the attachmentFile.
     */
    public String getAttachmentFile() {
        return CommonUtility.replaceNull(this.attachmentFile);
    }
    /**
     * @param attachmentFile The attachmentFile to set.
     */
    public void setAttachmentFile(String attachmentFile) {
        this.attachmentFile = attachmentFile;
    }
    /**
     * @return Returns the baseEnquiryId.
     */
    public long getBaseEnquiryId() {
        return this.baseEnquiryId;
    }
    /**
     * @param baseEnquiryId The baseEnquiryId to set.
     */
    public void setBaseEnquiryId(long baseEnquiryId) {
        this.baseEnquiryId = baseEnquiryId;
    }
    /**
     * @return Returns the ccEmailTo.
     */
    public String getCcEmailTo() {
        return CommonUtility.replaceNull(this.ccEmailTo);
    }
    /**
     * @param ccEmailTo The ccEmailTo to set.
     */
    public void setCcEmailTo(String ccEmailTo) {
        this.ccEmailTo = ccEmailTo;
    }
	public String getCreatedByName() {
		return CommonUtility.replaceNull(createdByName);
	}
	public void setCreatedByName(String createdByName) {
		this.createdByName = createdByName;
	}
	public String getSmNote() {
		return CommonUtility.replaceNull(smNote);
	}
	public void setSmNote(String smNote) {
		this.smNote = smNote;
	}
	public String getDmNote() {
		return CommonUtility.replaceNull(dmNote);
	}
	public void setDmNote(String dmNote) {
		this.dmNote = dmNote;
	}
	public String getCeNote() {
		return CommonUtility.replaceNull(ceNote);
	}
	public void setCeNote(String ceNote) {
		this.ceNote = ceNote;
	}
	public String getRsmNote() {
		return CommonUtility.replaceNull(rsmNote);
	}
	public void setRsmNote(String rsmNote) {
		this.rsmNote = rsmNote;
	}
	public String getSmName() {
		return CommonUtility.replaceNull(smName);
	}
	public void setSmName(String smName) {
		this.smName = smName;
	}
	public String getDmSsoid() {
		return CommonUtility.replaceNull(dmSsoid);
	}
	public void setDmSsoid(String dmSsoid) {
		this.dmSsoid = dmSsoid;
	}
	public String getDmName() {
		return CommonUtility.replaceNull(dmName);
	}
	public void setDmName(String dmName) {
		this.dmName = dmName;
	}
	public String getCeSsoid() {
		return CommonUtility.replaceNull(ceSsoid);
	}
	public void setCeSsoid(String ceSsoid) {
		this.ceSsoid = ceSsoid;
	}
	public String getCeName() {
		return CommonUtility.replaceNull(ceName);
	}
	public void setCeName(String ceName) {
		this.ceName = ceName;
	}
	public String getRsmSsoid() {
		return CommonUtility.replaceNull(rsmSsoid);
	}
	public void setRsmSsoid(String rsmSsoid) {
		this.rsmSsoid = rsmSsoid;
	}
	public String getRsmName() {
		return CommonUtility.replaceNull(rsmName);
	}
	public void setRsmName(String rsmName) {
		this.rsmName = rsmName;
	}
	public String getPageChecking() {
		return CommonUtility.replaceNull(pageChecking);
	}
	public void setPageChecking(String pageChecking) {
		this.pageChecking = pageChecking;
	}
	public String getDmToSmReturn() {
		return CommonUtility.replaceNull(dmToSmReturn);
	}
	public void setDmToSmReturn(String dmToSmReturn) {
		this.dmToSmReturn = dmToSmReturn;
	}
	public String getCeToDmReturn() {
		return CommonUtility.replaceNull(ceToDmReturn);
	}
	public void setCeToDmReturn(String ceToDmReturn) {
		this.ceToDmReturn = ceToDmReturn;
	}
	public String getCeToSmReturn() {
		return CommonUtility.replaceNull(ceToSmReturn);
	}
	public void setCeToSmReturn(String ceToSmReturn) {
		this.ceToSmReturn = ceToSmReturn;
	}
	public String getRsmToCeReturn() {
		return CommonUtility.replaceNull(rsmToCeReturn);
	}
	public void setRsmToCeReturn(String rsmToCeReturn) {
		this.rsmToCeReturn = rsmToCeReturn;
	}
	public String getRsmToDmReturn() {
		return CommonUtility.replaceNull(rsmToDmReturn);
	}
	public void setRsmToDmReturn(String rsmToDmReturn) {
		this.rsmToDmReturn = rsmToDmReturn;
	}
	public String getRsmToSmReturn() {
		return CommonUtility.replaceNull(rsmToSmReturn);
	}
	public void setRsmToSmReturn(String rsmToSmReturn) {
		this.rsmToSmReturn = rsmToSmReturn;
	}
	public String getReturnOperation() {
		return CommonUtility.replaceNull(returnOperation);
	}
	public void setReturnOperation(String returnOperation) {
		this.returnOperation = returnOperation;
	}
	public String getCustomerName() {
		return CommonUtility.replaceNull(customerName);
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerId() {
		return CommonUtility.replaceNull(customerId);
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getDeToSmReturn() {
		return CommonUtility.replaceNull(deToSmReturn);
	}
	public void setDeToSmReturn(String deToSmReturn) {
		this.deToSmReturn = deToSmReturn;
	}
	public String getEquiryOperationType() {
		return CommonUtility.replaceNull(equiryOperationType);
	}
	public void setEquiryOperationType(String equiryOperationType) {
		this.equiryOperationType = equiryOperationType;
	}
	public String getAttachmentFileName() {
		return CommonUtility.replaceNull(attachmentFileName);
	}
	public void setAttachmentFileName(String attachmentFileName) {
		this.attachmentFileName = attachmentFileName;
	}
	public String getReturnEnquiry() {
		return CommonUtility.replaceNull(returnEnquiry);
	}
	public void setReturnEnquiry(String returnEnquiry) {
		this.returnEnquiry = returnEnquiry;
	}
	public String getCmToSmReturn() {
		return CommonUtility.replaceNull(cmToSmReturn);
	}
	public void setCmToSmReturn(String cmToSmReturn) {
		this.cmToSmReturn = cmToSmReturn;
	}
	public String getCommengToSmReturn() {
		return CommonUtility.replaceNull(commengToSmReturn);
	}
	public void setCommengToSmReturn(String commengToSmReturn) {
		this.commengToSmReturn = commengToSmReturn;
	}
	public String getDmToCmReturn() {
		return CommonUtility.replaceNull(dmToCmReturn);
	}
	public void setDmToCmReturn(String dmToCmReturn) {
		this.dmToCmReturn = dmToCmReturn;
	}
	public String getDeToCmReturn() {
		return CommonUtility.replaceNull(deToCmReturn);
	}
	public void setDeToCmReturn(String deToCmReturn) {
		this.deToCmReturn = deToCmReturn;
	}
	public String getCmNote() {
		return CommonUtility.replaceNull(cmNote);
	}
	public void setCmNote(String cmNote) {
		this.cmNote = cmNote;
	}
	public String getsReturnComment() {
		return CommonUtility.replaceNull(sReturnComment);
	}
	public void setsReturnComment(String sReturnComment) {
		this.sReturnComment = sReturnComment;
	}
	public int getEmailCode() {
		return emailCode;
	}
	public void setEmailCode(int emailCode) {
		this.emailCode = emailCode;
	}
	public String getProjName() {
		return CommonUtility.replaceNull(projName);
	}
	public void setProjName(String projName) {
		this.projName = projName;
	}
	public String getSavingIndent() {
		return CommonUtility.replaceNull(savingIndent);
	}
	public void setSavingIndent(String savingIndent) {
		this.savingIndent = savingIndent;
	}
	public String getIsNewEnquiry() {
		return CommonUtility.replaceNull(isNewEnquiry);
	}
	public void setIsNewEnquiry(String isNewEnquiry) {
		this.isNewEnquiry = isNewEnquiry;
	}
	public String getRevisionRemarks() {
		return CommonUtility.replaceNull(revisionRemarks);
	}
	public void setRevisionRemarks(String revisionRemarks) {
		this.revisionRemarks = revisionRemarks;
	}
	

}
