/**
 * *****************************************************************************
 * Project Name: Request For Quotation
 * Document Name: WorkflowQueries.java
 * Package: in.com.rbc.quotation.workflow.dao
 * Desc: Interface that contains all the SQL queries related to workflow activities.
 * *****************************************************************************
 * Author: 100002865 (Shanthi Chinta)
 * Date: Oct 3, 2008
 * *****************************************************************************
 */
package in.com.rbc.quotation.workflow.dao;

import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.utility.PropertyUtility;

public interface WorkflowQueries {
    /**
     * String variable to store the QUOTATION & SHARED schema name.
     * It is retrieved from quotation.properties file
     */
    public final String SCHEMA_QUOTATION = PropertyUtility.getKeyValue(
                                                        QuotationConstants.QUOTATION_PROPERTYFILENAME,
                                                        QuotationConstants.QUOTATION_DB_SCHEMA_QUOTATION);
    public final String DASHBOARD_RECENTENQUIRYSCOUNT = PropertyUtility.getKeyValue(
                                                        QuotationConstants.QUOTATION_PROPERTYFILENAME,
                                                        QuotationConstants.QUOTATION_DASHBOARD_RECENTENQUIRYSCOUNT);
    public final String DASHBOARD_RELEASEDENQUIRYSCOUNT = PropertyUtility.getKeyValue(
                                                        QuotationConstants.QUOTATION_PROPERTYFILENAME,
                                                        QuotationConstants.QUOTATION_DASHBOARD_RECENTENQUIRYSCOUNT);
    
    public final String FETCH_MYRECENTENQUIRES = "SELECT Q.* FROM ( " +
                                                   " SELECT " +
                                                     " QEM_ENQUIRYID, QEM_ENQUIRYNO, " +
                                                     " QEM_STATUSID, QEM_STATUSDESC, " +
                                                     " QEM_CREATEDBY, QEM_CREATEDBYNAME, " +
                                                     " TO_CHAR(QEM_CREATEDDATE, '" + QuotationConstants.QUOTATION_DATE_FORMAT + "') QEM_CREATEDDATE ," +
                                                     "(SELECT DISTINCT EMP_NAMEFULL   FROM " + SCHEMA_QUOTATION + ".RFQ_EMPLOYEES_V EMP," + SCHEMA_QUOTATION + ".RFQ_WORKFLOW WORK WHERE "+
                                                     " EMP.EMP_SSO=WORK.QWF_ASSIGNTO AND WORK.QWF_WORKFLOWSTATUS='P' AND WORK.QWF_ENQUIRYID=a.QEM_ENQUIRYID) QEM_ASSIGNTO, QEM_CUSTOMERNAME, QEM_PROJECTNAME, QEM_IS_NEWENQUIRY "+
                                                   " FROM " + SCHEMA_QUOTATION + ".RFQ_ENQUIRES_V a" +
                                                   " WHERE QEM_CREATEDBY=? " +
                                                   " AND TO_DATE(TO_CHAR(QEM_CREATEDDATE,'DD-MM-YYYY'), 'DD-MM-YYYY') > TO_DATE(?,'DD-MM-YYYY') " +
                                                   " AND QEM_IS_NEWENQUIRY = 'Y' " +
                                                   " ORDER BY QEM_ENQUIRYID DESC) Q WHERE ROWNUM<=" + DASHBOARD_RECENTENQUIRYSCOUNT;
    
    public final String FETCH_MYAWAITINGAPPROVALENQUIRES = "SELECT " +
                                                             " QEM_ENQUIRYID, QEM_ENQUIRYNO, " +
                                                             " QEM_STATUSID, QEM_STATUSDESC, " +
                                                             " QEM_CREATEDBY, QEM_CREATEDBYNAME, " +
                                                             " TO_CHAR(QEM_CREATEDDATE, '" + QuotationConstants.QUOTATION_DATE_FORMAT + "') QEM_CREATEDDATE, " +
                                                             "(SELECT DISTINCT EMP_NAMEFULL   FROM " + SCHEMA_QUOTATION + ".RFQ_EMPLOYEES_V EMP," + SCHEMA_QUOTATION + ".RFQ_WORKFLOW WORK WHERE "+
                                                             " EMP.EMP_SSO=WORK.QWF_ASSIGNTO AND WORK.QWF_WORKFLOWSTATUS='P' AND WORK.QWF_ENQUIRYID=a.QEM_ENQUIRYID) QEM_ASSIGNTO, QEM_CUSTOMERNAME, QEM_PROJECTNAME, QEM_IS_NEWENQUIRY "+
                                                           " FROM " + SCHEMA_QUOTATION + ".RFQ_ENQUIRES_V a " +
                                                           " WHERE QEM_ENQUIRYID IN (SELECT QWF_ENQUIRYID " +
                                                                                       " FROM " + SCHEMA_QUOTATION + ".RFQ_WORKFLOW " +
                                                                                       " WHERE QWF_ASSIGNTO=? AND QWF_WORKFLOWSTATUS='P') " +
                                                           " AND TO_DATE(TO_CHAR(QEM_CREATEDDATE,'DD-MM-YYYY'), 'DD-MM-YYYY') > TO_DATE(?,'DD-MM-YYYY') " +
                                                           " AND QEM_IS_NEWENQUIRY = 'Y' " +
                                                           " ORDER BY QEM_ENQUIRYID DESC";
    
    public final String FETCH_MYRECENTRELEASEDENQUIRES = "SELECT Q.* FROM ( " +
                                                           " SELECT " +
                                                             " QEM_ENQUIRYID, QEM_ENQUIRYNO, " +
                                                             " QEM_STATUSID, QEM_STATUSDESC, " +
                                                             " QEM_CREATEDBY, QEM_CREATEDBYNAME, " +
                                                             " TO_CHAR(QEM_CREATEDDATE, '" + QuotationConstants.QUOTATION_DATE_FORMAT + "') QEM_CREATEDDATE, QEM_CUSTOMERNAME, QEM_PROJECTNAME, QEM_IS_NEWENQUIRY " +
                                                           " FROM " + SCHEMA_QUOTATION + ".RFQ_ENQUIRES_V " +
                                                           " WHERE QEM_CREATEDBY=? AND QEM_STATUSID IN (6, 7) " +
                                                           " AND TO_DATE(TO_CHAR(QEM_CREATEDDATE,'DD-MM-YYYY'), 'DD-MM-YYYY') > TO_DATE(?,'DD-MM-YYYY') " +
                                                           " AND QEM_IS_NEWENQUIRY = 'Y' " +
                                                           " ORDER BY QEM_ENQUIRYID DESC) Q WHERE ROWNUM<=" + DASHBOARD_RELEASEDENQUIRYSCOUNT;
    
    public final String FETCH_WORKFLOWRULE = "SELECT " + 
                                                " QRL_ACTIONCODE, QRL_ACTIONCODEDESC, " + 
                                                " QRL_CURRSTATUSID, QRL_CURRSTATUSDESC, " + 
                                                " QRL_NEWSTATUSID, QRL_NEWSTATUSDESC, " + 
                                                " QRL_NEXTLEVELCODE, QRL_NEXTLEVELDESC, " + 
                                                " QRL_EMAILFLAG, QRL_EMAILFLAGDESC " + 
                                             " FROM " + SCHEMA_QUOTATION + ".RFQ_WORKFLOWRULES_V " + 
                                             " WHERE QRL_ACTIONCODE = ? AND QRL_CURRSTATUSID = ?";
    
    /**
     * Query to Fetch Records From RFQ_TEAMMEMBERS_V Based On EnquiryId, 
     * This query is used in the following method
     *WorkflowDaoImpl.getNextLevelAssignTo()
     * Added by Gangadhar - Febraury , 2019
     */		
   
    public final String FETCH_NEXTLEVELASSIGNTO = "SELECT " + 
                                                      " AST_ENQUIRYID, AST_ENQUIRYNO, AST_REVISIONNO, " + 
                                                      " AST_NEXTLEVELCODE_SM, AST_SALESMANAGER, AST_SALESMANAGERNAME, AST_SALESMANAGEREMAIL, " + 
                                                      " AST_NEXTLEVELCODE_END, AST_CUSTOMERID, AST_CUSTOMERNAME, AST_CONTACTPERSON, AST_CONTACTPERSONEMAIL, " + 
                                                      " AST_NEXTLEVELCODE_RSM, AST_PRIMARYRSM, AST_PRIMARYRSMNAME, AST_PRIMARYRSMEMAIL, AST_ADDITIONALRSM, AST_ADDITIONALRSMNAME, AST_ADDITIONALRSMEMAIL, " + 
                                                      " AST_NEXTLEVELCODE_CE, AST_CHIEFEXECUTIVE, AST_CHIEFEXECUTIVENAME, AST_CHIEFEXECUTIVEEMAIL, " + 
                                                      " AST_NEXTLEVELCODE_DM, AST_DESIGNMANAGER, AST_DESIGNMANAGERNAME, AST_DESIGNMANAGEREMAIL, " + 
                                                      " AST_NEXTLEVELCODE_CM,AST_COMMERCIALMANAGER,AST_COMMERCIALMANAGERNAME,AST_COMMERCIALMANAGEREMAIL, "+
                                                      " AST_NEXTLEVELCODE_DE, AST_DESIGNENGINEER, AST_DESIGNENGINEERNAME, AST_DESIGNENGINEEREMAIL, " +
                                                      " AST_NEXTLEVELCODE_NSM, AST_NSM, AST_NSMNAME, AST_NSMEMAIL, AST_NEXTLEVELCODE_MH, AST_MH, AST_MHNAME, AST_MHEMAIL, " +
                                                      " AST_NEXTLEVELCODE_BH, AST_BH, AST_BHNAME, AST_BHEMAIL " +
                                                  " FROM " + SCHEMA_QUOTATION + ".RFQ_TEAMMEMBERS_V" + 
                                                  " WHERE AST_ENQUIRYID = ?";
       
    public final String INSERT_WORKFLOWDETAIL = "INSERT INTO " + SCHEMA_QUOTATION + ".RFQ_WORKFLOW ( " + 
                                                    " QWF_ENQUIRYID, QWF_ASSIGNTO, QWF_WORKFLOWSTATUS, " + 
                                                    " QWF_OLDSTATUSID, QWF_NEWSTATUSID, QWF_REMARKS, " + 
                                                    " QWF_CREATEDDATE, QWF_LASTMODIFIEDDATE) " + 
                                                " VALUES ( ?, ?, ?, ?, ?, ?, SYSDATE, SYSDATE)";
    
    public final String UPDATE_WORKFLOWDETAIL = "UPDATE " + SCHEMA_QUOTATION + ".RFQ_WORKFLOW " + 
                                                " SET" + 
                                                    " QWF_WORKFLOWSTATUS   = ?, " + 
                                                    " QWF_NEWSTATUSID      = ?, " + 
                                                    " QWF_REMARKS          = ?, " + 
                                                    " QWF_LASTMODIFIEDDATE = SYSDATE " + 
                                                " WHERE" + 
                                                    "     QWF_ENQUIRYID = ? " + 
                                                    " AND QWF_ASSIGNTO = ? " + 
                                                    " AND QWF_OLDSTATUSID = ? " +
                                                    " AND QWF_WORKFLOWSTATUS = '" + QuotationConstants.QUOTATION_WORKFLOW_ACTION_PENDING + "'";
    
    public final String UPDATE_PENDING_WORKFLOWDETAIL = "UPDATE " + SCHEMA_QUOTATION + ".RFQ_WORKFLOW " + 
    												" SET QWF_ASSIGNTO = ?, QWF_WORKFLOWSTATUS = ?, QWF_OLDSTATUSID = ?, " +
    												" QWF_NEWSTATUSID = ?, QWF_LASTMODIFIEDDATE = SYSDATE  " +
    												" WHERE QWF_ENQUIRYID = ? " +
    												" AND QWF_WORKFLOWSTATUS = '" + QuotationConstants.QUOTATION_WORKFLOW_ACTION_PENDING + "'";
    
    public final String UPDATE_ENQUIRYMASTERWITHSTATUS = "UPDATE " + SCHEMA_QUOTATION + ".RFQ_ENQUIRY_MASTER " + 
                                                         " SET QEM_STATUSID = ?" + 
                                                         " WHERE QEM_ENQUIRYID = ?";
    
    /**
     * Query to UPDATE QEM_STATUSID In RFQ_ENQUIRY_MASTER Table , userrole is dm
     * This query is used in the following method
     *WorkflowDaoImpl.updateEnquiryMasterWithStatus()
     * Added by Gangadhar - January , 2019
     */		
    public final String DM_UPDATE_ENQUIRYMASTERWITHSTATUS = "UPDATE " + SCHEMA_QUOTATION + ".RFQ_ENQUIRY_MASTER " + 
            " SET QEM_STATUSID = ?,QEM_DMSSOID=?,QEM_DMNAME=?,QEM_DMNOTE=?,QEM_DMTOSM_RETURN=?" + 
            " WHERE QEM_ENQUIRYID = ?";
    
    /**
     * Query to UPDATE QEM_STATUSID In RFQ_ENQUIRY_MASTER Table,  userrole is de
     * This query is used in the following method
     *WorkflowDaoImpl.updateEnquiryMasterWithStatus()
     * Added by Gangadhar - January , 2019
     */		
    public final String DE_UPDATE_ENQUIRYMASTERWITHSTATUS = "UPDATE " + SCHEMA_QUOTATION + ".RFQ_ENQUIRY_MASTER " + 
            " SET QEM_STATUSID = ?,QEM_DESSOID=?,QEM_DENAME=?,QEM_DENOTE=?,QEM_DETOSM_RETURN=?" + 
            " WHERE QEM_ENQUIRYID = ?";
    
    /**
     * Query to UPDATE QEM_STATUSID In RFQ_ENQUIRY_MASTER Table,  userrole is  ce
     * This query is used in the following method
     *WorkflowDaoImpl.updateEnquiryMasterWithStatus()
     * Added by Gangadhar - January , 2019
     */		
    public final String CE_UPDATE_ENQUIRYMASTERWITHSTATUS = "UPDATE " + SCHEMA_QUOTATION + ".RFQ_ENQUIRY_MASTER " + 
            " SET QEM_STATUSID = ?,QEM_CESSOID=?,QEM_CENAME=?,QEM_CENOTE=?" + 
            " WHERE QEM_ENQUIRYID = ?";
    
    /**
     * Query to UPDATE QEM_STATUSID In RFQ_ENQUIRY_MASTER Table, userrole is rsm
     * This query is used in the following method
     *WorkflowDaoImpl.updateEnquiryMasterWithStatus()
     * Added by Gangadhar - January , 2019
     */		
    public final String RSM_UPDATE_ENQUIRYMASTERWITHSTATUS = "UPDATE " + SCHEMA_QUOTATION + ".RFQ_ENQUIRY_MASTER " + 
            " SET QEM_STATUSID = ?,QEM_RSMSSOID=?,QEM_RSMNAME=?,QEM_RSMNOTE=?" + 
            " WHERE QEM_ENQUIRYID = ?";

    
    /**
     * Query to UPDATE QEM_STATUSID and RETURN Note In RFQ_ENQUIRY_MASTER Table , userrole is ce
     * 
     * This query is used in the following method
     *WorkflowDaoImpl.updateEnquiryMasterWithStatus()
     * Added by Gangadhar - January , 2019
     */		
    public final String QUOTATION_CE_UPDATE_ENQUIRYMASTERWITHSTATUS = "UPDATE " + SCHEMA_QUOTATION + ".RFQ_ENQUIRY_MASTER " + 
            " SET QEM_STATUSID = ?,QEM_QUOTATIONTOCE_RETURN=?" + 
            " WHERE QEM_ENQUIRYID = ?";
    
    /**
     * Query to UPDATE QEM_STATUSID and RETURN Note In RFQ_ENQUIRY_MASTER Table , userrole is RSM
     * 
     * This query is used in the following method
     *WorkflowDaoImpl.updateEnquiryMasterWithStatus()
     * Added by Gangadhar - January , 2019
     */		
    public final String QUOTATION_RSM_UPDATE_ENQUIRYMASTERWITHSTATUS = "UPDATE " + SCHEMA_QUOTATION + ".RFQ_ENQUIRY_MASTER " + 
            " SET QEM_STATUSID = ?,QEM_QUOTATIONTORSM_RETURN=?" + 
            " WHERE QEM_ENQUIRYID = ?";


    /**
     * Query to UPDATE QEM_STATUSID and RETURN Note In RFQ_ENQUIRY_MASTER Table , userrole is dm
     * 
     * This query is used in the following method
     *WorkflowDaoImpl.updateEnquiryMasterWithStatus()
     * Added by Gangadhar - January , 2019
     */		
    public final String QUOTATION_DM_UPDATE_ENQUIRYMASTERWITHSTATUS = "UPDATE " + SCHEMA_QUOTATION + ".RFQ_ENQUIRY_MASTER " + 
            " SET QEM_STATUSID = ?,QEM_QUOTATIONTODM_RETURN=?" + 
            " WHERE QEM_ENQUIRYID = ?";



    

    /**
     * Query to UPDATE QEM_STATUSID In RFQ_ENQUIRY_MASTER Table ,
     * When Revision is required
     * This query is used in the following method
     *WorkflowDaoImpl.updateEnquiryMasterWithStatus()
     * Added by Gangadhar - January , 2019
     */
    public final String UPDATE_ENQUIRYMASTERWITHSTATUS_REVISION  = "UPDATE " + SCHEMA_QUOTATION + ".RFQ_ENQUIRY_MASTER " + 
    																" SET QEM_STATUSID = ?, QEM_REMARKS = ? " + 
    																" WHERE QEM_ENQUIRYID= ? ";
    public final String GET_APPROVERSINFO_BY_ENQUIRYID = "SELECT DISTINCT " +
                                                         "       USERS.EMP_NAMEFULL, " +
                                                         "       TO_CHAR(WFLOW.QWF_LASTMODIFIEDDATE, '" + QuotationConstants.QUOTATION_DATE_FORMAT + "') AS QWF_LASTMODIFIEDDATE," +
                                                         "       WFLOW.QWF_REMARKS, " +
                                                         "       WFLOW.QWF_WORKFLOWSTATUS " +
                                                         "  FROM " + SCHEMA_QUOTATION + ".RFQ_WORKFLOW WFLOW, " + 
                                                                     SCHEMA_QUOTATION +".RFQ_EMPLOYEES_V USERS " +
                                                         " WHERE WFLOW.QWF_ASSIGNTO = USERS.EMP_SSO " +
                                                         "   AND WFLOW.QWF_ENQUIRYID = ? " +
                                                         "   AND (WFLOW.QWF_WORKFLOWSTATUS = 'SRS' OR WFLOW.QWF_WORKFLOWSTATUS = 'SSM') " +
                                                         " ORDER BY QWF_LASTMODIFIEDDATE";
    
    public final String FETCH_ENQUIRYDETAILS = "SELECT " + 
                                                "   QEM_ENQUIRYID, QEM_ENQUIRYNO, " + 
                                                "   QEM_CUSTOMERID, QEM_CUSTOMERNAME, QEM_CUSTOMERCONTACTPERSON, QEM_CUSTOMEREMAIL, " + 
                                                "   QEM_LOCATIONID, QEM_LOCATIONNAME, " + 
                                                "   QEM_ENQUIRYTYPE, QEM_ENQUIRYTYPEDESC, " + 
                                                "   QEM_CONSULTANTNAME, QEM_CROSSREFERENCE, QEM_DESC, " + 
                                                "   QEM_STATUSID, QEM_STATUSDESC, " + 
                                                "   QEM_CREATEDBY, QEM_CREATEDBYNAME, " +
                                                "   TO_CHAR(QEM_CREATEDDATE, 'MM/DD/YYYY') QEM_CREATEDDATE, " + 
                                                "   QEM_LASTMODIFIEDBY, QEM_LASTMODIFIEDBYNAME, " +
                                                "   TO_CHAR(QEM_LASTMODIFIEDDATE, 'MM/DD/YYYY') QEM_LASTMODIFIEDDATE,QEM_ENDUSER,QEM_ENQUIRYREFERENCENO" + 
                                                " FROM " + SCHEMA_QUOTATION + ".RFQ_ENQUIRES_V" + 
                                                " WHERE QEM_ENQUIRYID = ?";
    
    /**
     * Query to UPDATE QRD_LT_IS_VALIDATED Column In RFQ_LT_RATING_DETAILS Table ,
     * This query is used in the following method
     * WorkflowDaoImpl.updateRatingIsValidated()
     */
 
    public final String UPDATE_IS_VALIDATED_RFQ_RATING_DETAILS="UPDATE " + SCHEMA_QUOTATION + ".RFQ_LT_RATING_DETAILS " 
    															+ "set QRD_LT_IS_VALIDATED=? WHERE QRD_LT_ENQUIRYID=?";
    
    /**
     * Query to DELETE  RFQ_WORKFLOW Table Based On QWF_WORKFLOWSTATUS and QWF_ENQUIRYID ,
     * 
     * This query is used in the following method
     *WorkflowDaoImpl.deletePrevStatusBasedonEnquiry()
     * Added by Gangadhar - Feb 4 , 2019
     */
 
    public final String DELETE_WORKFLOW_BASEDPREVSTATUS="DELETE FROM " + SCHEMA_QUOTATION + ".RFQ_WORKFLOW WHERE QWF_ENQUIRYID=? AND QWF_WORKFLOWSTATUS=? ";

    
    /**
     * Query to UPDATE QEM_STATUSID and RETURN Note In RFQ_ENQUIRY_MASTER Table , userrole is Commercial Design Manager
     * 
     * This query is used in the following method
     *WorkflowDaoImpl.updateEnquiryMasterWithStatus()
     * Added by Gangadhar - Febraury 28 , 2019
     */		
    public final String UPDATE_QUOTATIONCMRETURN_ENQUIRYMASTERSTATUS = "UPDATE " + SCHEMA_QUOTATION + ".RFQ_ENQUIRY_MASTER " + 
            " SET QEM_STATUSID = ?,QEM_CMTOSM_RETURN=?" + 
            " WHERE QEM_ENQUIRYID = ?";
    
    
    /**
     * Query to UPDATE QEM_STATUSID and RETURN Note In RFQ_ENQUIRY_MASTER Table , userrole is Commercial Design Engineer
     * 
     * This query is used in the following method
     *WorkflowDaoImpl.updateEnquiryMasterWithStatus()
     * Added by Gangadhar - March 4 , 2019
     */		
    public final String UPDATE_QUOTATIONCERETURN_ENQUIRYMASTERSTATUS = "UPDATE " + SCHEMA_QUOTATION + ".RFQ_ENQUIRY_MASTER " + 
            " SET QEM_STATUSID = ?,QEM_COMMENGTOSM_RETURN=?" + 
            " WHERE QEM_ENQUIRYID = ?";
    
    
    /**
     * Query to UPDATE QEM_STATUSID and Indent CM Note In RFQ_ENQUIRY_MASTER Table , userrole is Commercial Design Manager
     * 
     * This query is used in the following method
     *WorkflowDaoImpl.updateEnquiryMasterWithStatus()
     * Added by Gangadhar - Febraury 28 , 2019
     */		
    public final String UPDATE_QUOTATION_ENQUIRYMASTERNOTESTATUS = "UPDATE " + SCHEMA_QUOTATION + ".RFQ_ENQUIRY_MASTER " + 
            " SET QEM_STATUSID = ?,QEM_INDENT_CMNOTE=?" + 
            " WHERE QEM_ENQUIRYID = ?";
    
    
    /**
     * Query to UPDATE QEM_STATUSID and RETURN Note In RFQ_ENQUIRY_MASTER Table , userrole is  Design Manager
     * 
     * This query is used in the following method
     *WorkflowDaoImpl.updateEnquiryMasterWithStatus()
     * Added by Gangadhar - March 5 , 2019
     */		
    public final String UPDATE_QUOTATIONDMRETURN_ENQUIRYMASTERSTATUS = "UPDATE " + SCHEMA_QUOTATION + ".RFQ_ENQUIRY_MASTER " + 
            " SET QEM_STATUSID = ?,QEM_DMTOCM_RETURN=?" + 
            " WHERE QEM_ENQUIRYID = ?";


    /**
     * Query to UPDATE QEM_STATUSID and RETURN Note In RFQ_ENQUIRY_MASTER Table , userrole is  Design Manager
     * 
     * This query is used in the following method
     *WorkflowDaoImpl.updateEnquiryMasterWithStatus()
     * Added by Gangadhar - March 7 , 2019
     */		
    public final String UPDATE_QUOTATIONDMRETURNTOSM = "UPDATE " + SCHEMA_QUOTATION + ".RFQ_ENQUIRY_MASTER " + 
            " SET QEM_STATUSID = ?,QEM_INDMTOSM_RETURN=?" + 
            " WHERE QEM_ENQUIRYID = ?";
    /**
     * Query to UPDATE QEM_STATUSID and RETURN Note In RFQ_ENQUIRY_MASTER Table , userrole is  Design Engineer
     * 
     * This query is used in the following method
     *WorkflowDaoImpl.updateEnquiryMasterWithStatus()
     * Added by Gangadhar - March 5 , 2019
     */		
    public final String UPDATE_QUOTATIONDERETURN_ENQUIRYMASTERSTATUS = "UPDATE " + SCHEMA_QUOTATION + ".RFQ_ENQUIRY_MASTER " + 
            " SET QEM_STATUSID = ?,QEM_DETOCM_RETURN=?" + 
            " WHERE QEM_ENQUIRYID = ?";
    
    
    /**
     * Query to UPDATE QEM_STATUSID and RETURN Note In RFQ_ENQUIRY_MASTER Table , userrole is  Design Engineer
     * 
     * This query is used in the following method
     *WorkflowDaoImpl.updateEnquiryMasterWithStatus()
     * Added by Gangadhar - March 7 , 2019
     */		
    public final String UPDATE_QUOTATIONDERETURNTOSM = "UPDATE " + SCHEMA_QUOTATION + ".RFQ_ENQUIRY_MASTER " + 
            " SET QEM_STATUSID = ?,QEM_INDETOSM_RETURN=?" + 
            " WHERE QEM_ENQUIRYID = ?";



    
    
}
