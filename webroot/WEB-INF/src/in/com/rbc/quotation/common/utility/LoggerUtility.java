/**
 * ********************************************************************************************
 * Project Name: Request For Quotation
 * Document Name: LoggerUtility.java
 * Package: in.com.rbc.acmotor.common.utility
 * Desc: Logger Utility class which holds methods that generate the Log4j log.
 * ********************************************************************************************
 * @author 100002865 (Shanthi Chinta)
 * ********************************************************************************************
 */
package in.com.rbc.quotation.common.utility;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class LoggerUtility {
    /*
     * Constants related to Log4j
     */
    public static final Logger APP_DEBUGLOGGER = Logger.getLogger("in.com.rbc.quotation.debug");
    public static final Logger APP_INFOLOGGER = Logger.getLogger("in.com.rbc.quotation.info");
    
    /**
     * Method to generate the Log4j log along with current date.
     * 
     * @param sLogType Log type string i.e., either INFO or DEBUG
     * @param sClassName Name of the class from where this method is being called
     * @param sMethodName Name of the method from where this method is being called
     * @param sLogString  Log string to generate the log
     * @author 100002865 (Shanthi Chinta)
     * Created on: Jun 4, 2008
     */
    public static void log(String sLogType, 
                           String sClassName,
                           String sMethodName,
                           String sLogString) {
        
        String sOutputString = "Date: " + DateUtility.getCurrentDateTime() + " - "
                                   + sClassName + "." + sMethodName + "(): "
                                   + sLogString;
        
        if ( sLogType.equalsIgnoreCase("INFO") )
            APP_INFOLOGGER.log(Level.INFO, sOutputString);
        else if ( sLogType.equalsIgnoreCase("DEBUG") )
            APP_DEBUGLOGGER.log(Level.ERROR, sOutputString);
    }
}
