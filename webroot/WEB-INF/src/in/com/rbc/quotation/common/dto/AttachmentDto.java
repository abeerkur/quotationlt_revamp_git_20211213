package in.com.rbc.quotation.common.dto;

import in.com.rbc.quotation.common.utility.CommonUtility;

public class AttachmentDto {
	
	/*
	 * ASM_ATTACHMENT
	 */
	
	private long attachmentId; //ASM_ATTACHMENT
	private int applicationId; //AAT_APPID
	private String applicationUniqueId; //AAT_APPUNIQUEID
	private String fileName; //AAT_FILENAME
	private String fileDescription; //AAT_FILEDESC
	private String fileType; //AAT_FILETYPE
	private long fileSize; //AAT_FILESIZE 
	private int attachedBy; //AAT_ATTACHEDBY	
	private String attachedDate; //AAT_ATTACHEDDATE
	private String attachedByName;
	private byte[] fileBytes;
	private int attachmentCount;
	
	/*
	 * ASM_APPLICATION - Application related variables
	 */	
	private String applicationName; //AAP_NAME
	private int maxFileSize; //AAP_MAXFILESIZE
	private int maxAttachments; //AAP_MAXATTACHPERREQUEST
	private String fileTypes; //AAP_FILETYPESALLOWED
	private String attachPath; //AAP_ATTACHPATH
	private String overrideDuplicate; //AAP_OVERRIDEDUPLICATION	
	
	private String imageIconName;

	/**
	 * @return Returns the applicationId.
	 */
	public int getApplicationId() {
		return applicationId;
	}

	/**
	 * @param applicationId The applicationId to set.
	 */
	public void setApplicationId(int applicationId) {
		this.applicationId = applicationId;
	}

	/**
	 * @return Returns the applicationName.
	 */
	public String getApplicationName() {
		return CommonUtility.replaceNull(applicationName);
	}

	/**
	 * @param applicationName The applicationName to set.
	 */
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	/**
	 * @return Returns the applicationUniqueId.
	 */
	public String getApplicationUniqueId() {
		return CommonUtility.replaceNull(applicationUniqueId);
	}

	/**
	 * @param applicationUniqueId The applicationUniqueId to set.
	 */
	public void setApplicationUniqueId(String applicationUniqueId) {
		this.applicationUniqueId = applicationUniqueId;
	}

	/**
	 * @return Returns the attachedBy.
	 */
	public int getAttachedBy() {
		return attachedBy;
	}

	/**
	 * @param attachedBy The attachedBy to set.
	 */
	public void setAttachedBy(int attachedBy) {
		this.attachedBy = attachedBy;
	}

	/**
	 * @return Returns the attachedByName.
	 */
	public String getAttachedByName() {
		return CommonUtility.replaceNull(attachedByName);
	}

	/**
	 * @param attachedByName The attachedByName to set.
	 */
	public void setAttachedByName(String attachedByName) {
		this.attachedByName = attachedByName;
	}

	/**
	 * @return Returns the attachedDate.
	 */
	public String getAttachedDate() {
		return CommonUtility.replaceNull(attachedDate);
	}

	/**
	 * @param attachedDate The attachedDate to set.
	 */
	public void setAttachedDate(String attachedDate) {
		this.attachedDate = attachedDate;
	}

	/**
	 * @return Returns the attachmentId.
	 */
	public long getAttachmentId() {
		return attachmentId;
	}

	/**
	 * @param attachmentId The attachmentId to set.
	 */
	public void setAttachmentId(long attachmentId) {
		this.attachmentId = attachmentId;
	}

	/**
	 * @return Returns the attachPath.
	 */
	public String getAttachPath() {
		return CommonUtility.replaceNull(attachPath);
	}

	/**
	 * @param attachPath The attachPath to set.
	 */
	public void setAttachPath(String attachPath) {
		this.attachPath = attachPath;
	}

	/**
	 * @return Returns the fileDescription.
	 */
	public String getFileDescription() {
		return CommonUtility.replaceNull(fileDescription);
	}

	/**
	 * @param fileDescription The fileDescription to set.
	 */
	public void setFileDescription(String fileDescription) {
		this.fileDescription = fileDescription;
	}

	/**
	 * @return Returns the fileName.
	 */
	public String getFileName() {
		return CommonUtility.replaceNull(fileName);
	}

	/**
	 * @param fileName The fileName to set.
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * @return Returns the fileSize.
	 */
	public long getFileSize() {
		return fileSize;
	}

	/**
	 * @param fileSize The fileSize to set.
	 */
	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}

	/**
	 * @return Returns the fileType.
	 */
	public String getFileType() {
		return CommonUtility.replaceNull(fileType);
	}

	/**
	 * @param fileType The fileType to set.
	 */
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	/**
	 * @return Returns the fileTypes.
	 */
	public String getFileTypes() {
		return CommonUtility.replaceNull(fileTypes);
	}

	/**
	 * @param fileTypes The fileTypes to set.
	 */
	public void setFileTypes(String fileTypes) {
		this.fileTypes = fileTypes;
	}

	/**
	 * @return Returns the imageIconName.
	 */
	public String getImageIconName() {
		return CommonUtility.replaceNull(imageIconName);
	}

	/**
	 * @param imageIconName The imageIconName to set.
	 */
	public void setImageIconName(String imageIconName) {
		this.imageIconName = imageIconName;
	}

	/**
	 * @return Returns the maxAttachments.
	 */
	public int getMaxAttachments() {
		return maxAttachments;
	}

	/**
	 * @param maxAttachments The maxAttachments to set.
	 */
	public void setMaxAttachments(int maxAttachments) {
		this.maxAttachments = maxAttachments;
	}

	/**
	 * @return Returns the maxFileSize.
	 */
	public int getMaxFileSize() {
		return maxFileSize;
	}

	/**
	 * @param maxFileSize The maxFileSize to set.
	 */
	public void setMaxFileSize(int maxFileSize) {
		this.maxFileSize = maxFileSize;
	}

	/**
	 * @return Returns the overrideDuplicate.
	 */
	public String getOverrideDuplicate() {
		return CommonUtility.replaceNull(overrideDuplicate);
	}

	/**
	 * @param overrideDuplicate The overrideDuplicate to set.
	 */
	public void setOverrideDuplicate(String overrideDuplicate) {
		this.overrideDuplicate = overrideDuplicate;
	}

	/**
	 * @return Returns the fileBytes.
	 */
	public byte[] getFileBytes() {
		return fileBytes;
	}

	/**
	 * @param fileBytes The fileBytes to set.
	 */
	public void setFileBytes(byte[] fileBytes) {
		this.fileBytes = fileBytes;
	}

	/**
	 * @return Returns the attachmentCount.
	 */
	public int getAttachmentCount() {
		return attachmentCount;
	}

	/**
	 * @param attachmentCount The attachmentCount to set.
	 */
	public void setAttachmentCount(int attachmentCount) {
		this.attachmentCount = attachmentCount;
	}

}
