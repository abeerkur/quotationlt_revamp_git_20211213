/**
 * *****************************************************************************
 * Project Name: Request For Quotation
 * Document Name: ListModel.java
 * Package: in.com.rbc.quotation.common.utility
 * Desc: This model is used to cater listing pages i.e., 
 *  displaying results list along with pagination
 * ********************************************************************************************
 * @author 100002865 (Shanthi Chinta)
 * *****************************************************************************
 */
package in.com.rbc.quotation.common.utility;

import javax.servlet.http.HttpServletRequest;
import in.com.rbc.quotation.common.constants.QuotationConstants;

public class ListModel implements java.io.Serializable {
    private String m_sRecordsPerPage = "";
    private String m_sCurrentPage = ""; 
    private String m_sSortBy = "";
    private String m_sSortOrder = "";
    private String m_sDefaultSortBy = "";
    private String m_sDefaultSortOrder = "";
    private String m_sAscArrowPath = "";
    private String m_sDescArrowPath = "";
    private String m_sFormName = "";
    private String m_sScreenDef = "";
    private String m_sActionName = "";
    
    private int m_nCurrentPageRecordsSize = 0;
    private int m_nTotalRecordCount = 0;
    
    public final static String ASCENDING = "A";
    public final static String DESCENDING = "D";
    
    /**
     * Default constructor of ListModel class 
     */
    public ListModel() {
        super();
    }
    
    /**
     * Get the records per page in string format
     * @return String Records per page
     */
    public String getRecordsPerPage() {
        if(m_sRecordsPerPage.length() == 0)
            return QuotationConstants.QUOTATION_LIST_NUMRECORDSPERPAGE;
        return CommonUtility.replaceNull(m_sRecordsPerPage);
    }
    
    /**
     * Get the records per page in integer
     * @return String Records per page
     */
    public int getRecordsPerPageInt() {
         int nRecordsPerPage = Integer.parseInt(QuotationConstants.QUOTATION_LIST_NUMRECORDSPERPAGE);
         
         if(getRecordsPerPage().length() > 0)
             nRecordsPerPage = Integer.parseInt(getRecordsPerPage());
         
         return nRecordsPerPage;
    }

    /**
     * Set the records per page in string format
     * @param String Records per page in string format
     * @return void
     */
    public void setRecordsPerPage(String sRecordsPerPage) {
        m_sRecordsPerPage = sRecordsPerPage;
    }

    /**
     * Get the current page in string format
     * @return String Current page
     */
    public String getCurrentPage() {
        if (m_sCurrentPage.length() == 0 ) {
            return "1";
        }
        return CommonUtility.replaceNull(m_sCurrentPage);
    }

    /**
     * Get the current page in string format
     * @return String Current page
     */
    public int getCurrentPageInt() {
         int nCurrentPage = 1;
         
         if(getCurrentPage().length() > 0)
           nCurrentPage = Integer.parseInt(getCurrentPage());
           
         return nCurrentPage;
    }

    /**
     * Set the current page in string format
     * @param String Current page in string format
     * @return void
     */
    public void setCurrentPage(String sCurrentPage) {
        m_sCurrentPage = sCurrentPage;
    }

    /**
     * Get the sorting column
     * Sorting column represents the database column name
     * @return String Sort By column name
     */
    public String getSortBy() {
        m_sSortBy = CommonUtility.replaceNull(m_sSortBy);
         
        if(m_sSortBy.length() == 0)
        {
            return getDefaultSortBy();  
        }
           
        return m_sSortBy;
    }

    /**
     * Set the sorting column
     * Sorting column represents the database column name
     * @param String Sort By column name
     * @return void
     */
    public void setSortBy(String sSortBy) 
    {
        m_sSortBy = sSortBy;
    }

    /**
     * Get the sorting order
     * A - Asc, D - Desc
     * @return String Sort order
     */
    public String getSortOrder() {
        m_sSortOrder = CommonUtility.replaceNull(m_sSortOrder);
        
        if(m_sSortOrder.length() == 0) {
           return getDefaultSortOrder();    
        }
        return m_sSortOrder;
    }

    /**
     * Get the sorting order description
     * A - Asc, D - Desc
     * @return String Sort order
     */
    public String getSortOrderDesc() {
         String sSortOrderDesc = "";
         
         if(getSortOrder().equals(ASCENDING))
           sSortOrderDesc = "ASC";
         else if(getSortOrder().equals(DESCENDING))
           sSortOrderDesc = "DESC";
           
         return sSortOrderDesc;
    }

    /**
     * Set the sorting order
     * @param String Sorting order
     * A - Asc, D - Desc
     * @return void
     */
    public void setSortOrder(String sSortOrder) {
        m_sSortOrder = sSortOrder;
    }

    /**
     * Get the default sorting column
     * Sorting column represents the database column name
     * @return String Sort By column name
     */
    public String getDefaultSortBy() {
        return CommonUtility.replaceNull(m_sDefaultSortBy);
    }

    /**
     * Set the default sorting column
      * Sorting column represents the database column name
     * @param String Sort By column name
     * @return void
     */
    public void setDefaultSortBy(String sDefaultSortBy) {
        m_sDefaultSortBy = sDefaultSortBy;
    }

    /**
     * Get the default sorting order
     * A - Asc, D - Desc
     * @return String default sort order
     */
    public String getDefaultSortOrder() {
        return CommonUtility.replaceNull(m_sDefaultSortOrder);
    }

    /**
     * Set the default sorting order
     * @param String Default orting order
     * A - Asc, D - Desc
     * @return void
     */
    public void setDefaultSortOrder(String sDefaultSortOrder) {
        m_sDefaultSortOrder = sDefaultSortOrder;
    }

    /**
     * Get the ascending arrow file image path
     * @return String Ascending Arrow Path
     */
    public String getAscArrowPath() {
        return CommonUtility.replaceNull("html/images/arrowasc.gif");
    }

    /**
     * Set the ascending arrow file image path
     * @param String Ascending arrow file image path
     * @return void
     */
    public void setAscArrowPath(String sAscArrowPath) {
        m_sAscArrowPath = sAscArrowPath;
    }

    /**
     * Get the descending arrow file image path
     * @return String Descending Arrow Path
     */
    public String getDescArrowPath() {
        return CommonUtility.replaceNull("html/images/arrowdesc.gif");
    }

    /**
     * Set the descending arrow file image path
     * @param String Decending arrow file image path
     * @return void
     */
    public void setDescArrowPath(String sDescArrowPath) {
        m_sDescArrowPath = sDescArrowPath;
    }

    /**
     * Get the total number of records
     * @return int Total number of records
     */
    public int getTotalRecordCount() {
        return m_nTotalRecordCount;
    }

    /**
     * Set the total number of records
     * @param int Total number of records
     * @return int
     */
    public void setTotalRecordCount(int nTotalRecordCount) {
        m_nTotalRecordCount = nTotalRecordCount;
    }

    /**
     * Get the total number of pages that contain "RecordPerPage" records
     * @return int nTotalFullPages
     */
    public int getTotalFullPages() {
         int nTotalFullPages = 0;
         nTotalFullPages = getTotalRecordCount()/getRecordsPerPageInt();
         return nTotalFullPages;
    }

    /**
     * Get the number of last page records
     * @return int nLastPageRecords
     */
    public int getLastPageRecords() {
         int nLastPageRecords = 0;
         nLastPageRecords = getTotalRecordCount() % getRecordsPerPageInt();
         return nLastPageRecords;
    }

    /**
     * Get the string indicating the summary of the listing
     * E.g. : 1 to 10 of 23
     * @param int Size of the records vector got from list method
     * @return String <Start Record> to <End Record> of <Total Records>
     */
    public String getListSummary(int nVectorSize) {
         String sStart = (getTotalRecordCount() == 0)?"0":
             String.valueOf(((getCurrentPageInt()-1) * getRecordsPerPageInt())+1);
         int nEnd = ((getCurrentPageInt()-1)*getRecordsPerPageInt()) + nVectorSize;
         return ("Listing" + " "+ sStart + "-" +  nEnd + " of " + getTotalRecordCount());
    }

    /**
     * Get sequence number in the listing (S/No)
     * E.g. : 1,2,3...10 
     * @return int StartSequence
     */
    public int getStartSequence() {
         int nStartSequence = 0;
         
         if(getCurrentPageInt() > 1)
           nStartSequence = (getCurrentPageInt()-1) * getRecordsPerPageInt();
         
         return nStartSequence;
    }

    /**
     * Get page numbers list for listing page
     * @param sFormName Listing form name E.g: document.ListRoleForm
     * @param sScreenName Screen name of the Listing page  E.g: /ekp/ecoll.cm.list.screen
     * @param nSelect int whether the top select or botton. Top = 1 and bottom = 2
     * @return String Page numbers for listing
     */
    public String getPageList(String sFormName, String sActionName, 
            String sScreenName, int nSelect) {
         
        //get the last page records size    
         int nLastPageRecords = getTotalRecordCount() % getRecordsPerPageInt();
         
         //get the total number pages in the list
         //E.g. If there are some last page records then add one page to the
         //getTotalFullPages() method
         int nTotalPages = (nLastPageRecords > 0)?getTotalFullPages()+1:getTotalFullPages();    
         
         StringBuffer sb = new StringBuffer();

         if(getCurrentPageInt() > 1)    
           sb.append("<a href=\"JavaScript:goNextPrev(" + sFormName + "," 
                   + sActionName + ",\'" + sScreenName + "\',\'" + (getCurrentPageInt()-1) 
                   + "\');\"><img src=\"html/images/PrevPage.gif\" alt='Previous' width='17' height='17' border='0' align='absmiddle'></a>&nbsp;");

         sb.append("Page");
         sb.append("<select class=\"mini\" name=\"sCurrentPage" + nSelect + 
                 "\" onChange=\"JavaScript:goToPage(" + sFormName + "," + sActionName + 
                 ",\'" + sScreenName + "\',"+sFormName+".sCurrentPage" + nSelect +");\">");

         for(int i=1;i<=nTotalPages;i++) {
             sb.append("<option value=\"" + i + "\"" 
                     + ((i == getCurrentPageInt())?" SELECTED":"") + ">" + i + "</option>");
         }
         
         sb.append("</select>");
         sb.append(" of ");
         sb.append(nTotalPages);
         
         
         if(!(getCurrentPageInt() == getTotalFullPages() && getLastPageRecords() == 0) 
                 && !(getCurrentPageInt() == getTotalFullPages()+1))    
           sb.append("&nbsp;<a href=\"JavaScript:goNextPrev(" + sFormName 
                   + "," + sActionName + ",\'" + sScreenName + "\',\'" 
                   + (getCurrentPageInt()+1) + "\');\"><img src=\"html/images/NextPage.gif\" alt='Next' width='17' height='17' border='0' align='absmiddle'></a>");

         return sb.toString();
    }

    /**
     * Get page numbers list for listing page
     * @param sFormName Listing form name E.g: document.ListRoleForm
     * @param sScreenName Screen name of the Listing page  E.g: /ekp/ecoll.cm.list.screen
     * @param nSelect int whether the top select or botton. Top = 1 and bottom = 2
     * @return String Page numbers for listing
     */
    public String getPageListDown(String sFormName, String sActionName, 
            String sScreenName, int nSelect) {
        
         //get the last page records size   
         int nLastPageRecords = getTotalRecordCount() % getRecordsPerPageInt();
         
         //get the total number pages in the list
         //E.g. If there are some last page records then add one page to the
         //getTotalFullPages() method
         int nTotalPages = (nLastPageRecords > 0)?getTotalFullPages()+1:
             getTotalFullPages();   
         
         StringBuffer sb = new StringBuffer();

         if(getCurrentPageInt() > 1)    
           sb.append("<a href=\"JavaScript:goNextPrev(" + sFormName + "," 
                   + sActionName + ",\'" + sScreenName + "\',\'" + (getCurrentPageInt()-1) + "\');\"><< Previous</a>&nbsp;");

         sb.append("Page");
         sb.append("<select class=\"mini\" name=\"sCurrentPage" + nSelect + "\" onChange=\"JavaScript:goToPage(" + sFormName + "," + sActionName + ",\'" + sScreenName + "\', this.value);\">");

         for(int i=1;i<=nTotalPages;i++) {
             sb.append("<option value=\"" + i + "\"" + ((i == getCurrentPageInt())?" SELECTED":"") + ">" + i + "</option>");
         }
         
         sb.append("</select>");
         sb.append(" of ");
         sb.append(nTotalPages);
         
         
         if(!(getCurrentPageInt() == getTotalFullPages() && getLastPageRecords() == 0) 
                 && !(getCurrentPageInt() == getTotalFullPages()+1))    
           sb.append("&nbsp;<a href=\"JavaScript:goNextPrev(" + sFormName + "," 
                   + sActionName + ",\'" + sScreenName + "\',\'" 
                   + (getCurrentPageInt()+1) + "\');\">Next >></a>");

         return sb.toString();
    }

    /**
     * Get the Arrow code
     * @return String
     */
    public String getArrow(String sColumnName) {
         String sArrowCode = "";
         if(getSortBy().equals(sColumnName)) {
            if(getSortOrder().equalsIgnoreCase(ASCENDING))
              sArrowCode = "<img src=\"" + getAscArrowPath() + "\" border=\"0\">";
            else if(getSortOrder().equalsIgnoreCase(DESCENDING))
              sArrowCode = "<img src=\"" + getDescArrowPath() + "\" border=\"0\">";
         }
         return sArrowCode;   
    }

    /**
     * Set the parameters for ListModel
     * @param Request
     */
    public void setParams(HttpServletRequest request, String sDefaultSortBy, 
            String sDefaultSortOrder) {
        
        String sNextPrevNo = CommonUtility.replaceNull(request.getParameter("sNextPrevNo"));
        String sCurrentPage = CommonUtility.replaceNull(request.getParameter("sCurrentPage"));
        
        if(sNextPrevNo.length() > 0)
           sCurrentPage = sNextPrevNo;
 
        m_sCurrentPage = sCurrentPage;
        setSortBy(request.getParameter("sSortBy"));
        setSortOrder(request.getParameter("sSortOrder"));
        
        setDefaultSortBy(sDefaultSortBy);
        setDefaultSortOrder(sDefaultSortOrder);
                             
        //set the arrow paths here
        setAscArrowPath(request.getContextPath() + "./html/images/arrowasc.gif");
        setDescArrowPath(request.getContextPath() + "./html/images/arrowdesc.gif");
    }

    /**
     * Set the parameters for ListModel
     * @param Request
     */
    public void setParams(HttpServletRequest request, String sDefaultSortBy, 
            String sDefaultSortOrder, String sFormName,String sActionName, String sScreenDef) {
        
        String sNextPrevNo = CommonUtility.replaceNull(request.getParameter("sNextPrevNo"));
        String sCurrentPage = CommonUtility.replaceNull(request.getParameter("sCurrentPage"));
        

        
        if(sNextPrevNo.length() > 0)
           sCurrentPage = sNextPrevNo;
        
        m_sCurrentPage = sCurrentPage;
        setSortBy(request.getParameter("sSortBy"));
        setSortOrder(request.getParameter("sSortOrder"));
        
        setDefaultSortBy(sDefaultSortBy);
        setDefaultSortOrder(sDefaultSortOrder);
                             
        //set the arrow paths here
        setAscArrowPath(request.getContextPath() + "/html/images/arrowasc.gif");
        setDescArrowPath(request.getContextPath() + "/html/images/arrowdesc.gif");
        //setNonSortedPath(request.getContextPath() + "/html/images/spacer.gif");
        
        //set the formname and screendef of the listing page
        setFormName(sFormName);
        setScreenDef(sScreenDef);
        setActionName(sActionName);
        
        // Set the labels for listing and pagination
        //setI18nTable(request);
    }
    
    /**
     * Get the hidden fields to be created in the form
     * @author 
     */
    public String getHiddenFields() {
         StringBuffer oStringBuffer = new StringBuffer();
         
         oStringBuffer.append("<input type=\"hidden\" name=\"sSortBy\" value=\"" + getSortBy() + "\">");
         oStringBuffer.append("<input type=\"hidden\" name=\"sSortOrder\" value=\"" + getSortOrder()+ "\">");
         oStringBuffer.append("<input type=\"hidden\" name=\"sNextPrevNo\" value=\"\">");
         oStringBuffer.append("<input type=\"hidden\" name=\"sCurrentPage\" value=\"\">");
         
         return oStringBuffer.toString();   
    }
    
    /**
     * Get the lsting form name
     * @return String lsting form name
     */
    public String getFormName() {
        return CommonUtility.replaceNull(m_sFormName);
        //return PropertyUtility.getStringValue(m_sFormName);
    }

    /**
     * Set the lsting form name
     * @param String lsting form name
     * @return void
     */
    public void setFormName(String sFormName) {
        m_sFormName = sFormName;
    }

    /**
     * Get the screen definition of the listing page
     * @return String Sort By column name
     */
    public String getScreenDef() {
         return CommonUtility.replaceNull(m_sScreenDef);
        //return PropertyUtility.getStringValue(m_sScreenDef);
    }

    /**
      * Set the screen definition of the listing page
      * @param String screen definition of the listing page
      * @return void
      */
    public void setScreenDef(String sScreenDef) {
        m_sScreenDef = sScreenDef;
    }
    
    /**
     * Get the screen definition of the listing page
     * @return String Sort By column name
     */
    public String getActionName() {
         return CommonUtility.replaceNull(m_sActionName);
         //return PropertyUtility.getStringValue(m_sActionName);
    }

    /**
     * Set the screen definition of the listing page
     * @param String screen definition of the listing page
     * @return void
     */
    public void setActionName(String sActionName) {
        m_sActionName = sActionName;
    }
    
    /**
     * Get the current page records size
     * @return int current page records size
     */
    public int getCurrentPageRecordsSize() {
        return m_nCurrentPageRecordsSize;
    }
}