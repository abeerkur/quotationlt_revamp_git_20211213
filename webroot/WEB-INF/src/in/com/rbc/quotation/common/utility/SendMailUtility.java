/**
 * *****************************************************************************
 * Project Name: Request For Quotation
 * Document Name: SendMailUtility.java
 * Package: in.com.rbc.quotation.common.utility
 * Desc: Class holds all the methods related to sending email when different  
 *       operation were performed in the application.
 * *****************************************************************************
 * Author: 100002865 (Shanthi Chinta)
 * Date: Oct 8, 2008
 * *****************************************************************************
 */
package in.com.rbc.quotation.common.utility;

import java.util.ArrayList;

import in.com.rbc.quotation.common.constants.MailConstants;
import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.dto.AttachmentDto;
import in.com.rbc.quotation.common.vo.KeyValueVo;
import in.com.rbc.quotation.enquiry.dto.EnquiryDto;
import in.com.rbc.quotation.workflow.dto.WorkflowDto;

public class SendMailUtility {
	
    
    public boolean sendWorkflowEmail(WorkflowDto oWorkflowDto) throws Exception {

        String sMethodName = "sendWorkflowEmail";
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        boolean isMailSent = false;
        String sFinalEnquiryId="";
        String sAttachmentFileName="";
        ArrayList arDmAttachmentList=null;
        KeyValueVo oKeyValueVo=null;
        int iDmApplicationId=0; //Assigning DmApplicationId
        String sAttachmentId=null;
        String sFileName=null;
        String sFileExten=null;
        AttachmentDto oAttachmentDto=null;
        ArrayList arDmAttachmentListPath=null;
        String shouldSendMailStoped = null;
		boolean canSendEmails = true;
        
        String ccEmailTo = oWorkflowDto.getCcEmailTo();
        if ( ccEmailTo==null && ccEmailTo.length()==0 ) {
            ccEmailTo = "";
        }
        
        shouldSendMailStoped = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONLT_SENDMAIL_STOP);
        if (shouldSendMailStoped != null
				&& shouldSendMailStoped.trim().length() != 0
				&& shouldSendMailStoped.equalsIgnoreCase("YES")) {
        	// Set FALSE.
        	canSendEmails = false;
        }
        // TESTING
        //	oWorkflowDto.setLoginUserEmail("anup.beerkur@regalrexnord.com");
        //	oWorkflowDto.setAssignedToEmail("anup.beerkur@regalrexnord.com");
        //	ccEmailTo = "anup.beerkur@regalrexnord.com";
        // TESTING
        	
        	
        EnquiryDto oEnquiryDto = null;
        if ( oWorkflowDto.getAttachmentFile().trim().length()>0 ) {
        	
            /*
             * Case when file information is provided, to send as an attachment 
             * to the email
             */
            LoggerUtility.log("INFO", sClassName, sMethodName, "Attachment File = " + oWorkflowDto.getAttachmentFile());
            LoggerUtility.log("INFO", sClassName, sMethodName, "attachmentFileName = " + sAttachmentFileName);
            LoggerUtility.log("INFO", sClassName, sMethodName, "AssignedToEmail = " + oWorkflowDto.getAssignedToEmail());
            
            if(canSendEmails) {
            	isMailSent = MailUtility.sendMailWithAttachment(oWorkflowDto.getLoginUserEmail(), 
                        oWorkflowDto.getAssignedToEmail(), 
                        getWorkflowEmailSubject(oWorkflowDto),
                        getWorkflowEmailBody(oWorkflowDto),
                        ccEmailTo, "",
                        oWorkflowDto.getAttachmentFile(),
                        oWorkflowDto.getAttachmentFileName());
            }
            
        } else {
        	
            /* Case when email has to be sent without any attachment */
        	if(canSendEmails) {
        		isMailSent = MailUtility.sendMail(oWorkflowDto.getLoginUserEmail(), 
                        oWorkflowDto.getAssignedToEmail(), 
                        getWorkflowEmailSubject(oWorkflowDto),
                        getWorkflowEmailBody(oWorkflowDto),
                        ccEmailTo);
        	}
        	
        }
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        return isMailSent;
    }
    
    private String getWorkflowEmailSubject(WorkflowDto oWorkflowDto) throws Exception {
        String sEmailSubject = null;
        String sCustomer = null;
        String[] oCustomerArray =null;
        
        sCustomer= oWorkflowDto.getCustomerName();
        
        if(sCustomer!=null&&!sCustomer.equals("")){
        	oCustomerArray =sCustomer.split(" ");
        	if(oCustomerArray.length>=QuotationConstants.QUOTATION_LITERAL_2)
        		sCustomer = oCustomerArray[0]+" "+oCustomerArray[1];
        	else
        		sCustomer = oCustomerArray[0];
        } 
        
        if(oWorkflowDto.getNewStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_WONPENDINGCM || oWorkflowDto.getNewStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_WONPENDINGDM){
        	 sEmailSubject = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONEMAILS_PROPERTYFILENAME, 
                     MailConstants.QUOTATION_WON_EMAIL_SUBJECT);
        	 sEmailSubject = CommonUtility.replaceString(sEmailSubject, "IndentNo", oWorkflowDto.getSavingIndent());
        }
        else{
        	sEmailSubject = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONEMAILS_PROPERTYFILENAME, 
                                                    MailConstants.QUOTATION_EMAIL_SUBJECT);
        }
        sEmailSubject = CommonUtility.replaceString(sEmailSubject, "DesignRef", oWorkflowDto.getDesignReference());
        sEmailSubject = CommonUtility.replaceString(sEmailSubject, "Customer", sCustomer);
        sEmailSubject = CommonUtility.replaceString(sEmailSubject, "EnquiryNumber", oWorkflowDto.getEnquiryNo().substring(QuotationConstants.QUOTATION_LITERAL_15));
        sEmailSubject = CommonUtility.replaceString(sEmailSubject, "Status", oWorkflowDto.getNewStatus());
        
        LoggerUtility.log("INFO", this.getClass().getName(), "getWorkflowEmailSubject", "Workflow Email Subject = " + sEmailSubject);
        
        return sEmailSubject;
    }
    
    private String getWorkflowEmailBody(WorkflowDto oWorkflowDto) throws Exception {
        String sEmailBody = null;
        StringBuffer sbEnquiryLink = new StringBuffer("");
        
        sbEnquiryLink.append(oWorkflowDto.getAppUrl());
        sbEnquiryLink.append(QuotationConstants.QUOTATION_SENTMAIL_URL);
        sbEnquiryLink.append(QuotationConstants.QUOTATION_SENTMAIL_INQID_URL + oWorkflowDto.getEnquiryId());
        sbEnquiryLink.append(QuotationConstants.QUOTATION_SENTMAIL_STATUSID_URL+ oWorkflowDto.getNewStatusId());
        
        /*
         * Appending the following
         * -- email head along with style definitions
         * -- email body
         * -- email signature along with auto-generated message
         * -- email footer
         */
        if ( oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTODESIGNOFFICE) ) {
            sEmailBody = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONEMAILS_PROPERTYFILENAME, 
                                                     MailConstants.QUOTATION_EMAIL_BODY_PLACEENQUIRY);
        } 
        else if ( oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_REASSIGNTODE) ) {
            sEmailBody = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONEMAILS_PROPERTYFILENAME, 
                                                     MailConstants.QUOTATION_EMAIL_BODY_REASSIGNTODE);
        } 
        else if ( oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTODM) ) {
            sEmailBody = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONEMAILS_PROPERTYFILENAME, 
                                                     MailConstants.QUOTATION_EMAIL_BODY_SUBMITTODM);
        } 
        else if ( oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTOCE) ) {
            sEmailBody = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONEMAILS_PROPERTYFILENAME, 
                                                     MailConstants.QUOTATION_EMAIL_BODY_SUBMITTOCE);
        } 
        else if ( oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTORSM) ) {
            sEmailBody = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONEMAILS_PROPERTYFILENAME, 
                                                     MailConstants.QUOTATION_EMAIL_BODY_SUBMITTORSM);
        } 
        else if ( oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_RETURNTODM) ) {
            sEmailBody = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONEMAILS_PROPERTYFILENAME, 
                                                     MailConstants.QUOTATION_EMAIL_BODY_RETURNTODM);
        } 
        else if ( oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_RETURNTODE) ) {
            sEmailBody = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONEMAILS_PROPERTYFILENAME, 
                                                     MailConstants.QUOTATION_EMAIL_BODY_RETURNTODE);
        } 
        else if ( oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTOSM) ) {
            sEmailBody = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONEMAILS_PROPERTYFILENAME, 
                                                     MailConstants.QUOTATION_EMAIL_BODY_SUBMITTOSM);
        } 
        else if ( oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_RETURNTOCE) ) {
            sEmailBody = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONEMAILS_PROPERTYFILENAME, 
                                                     MailConstants.QUOTATION_EMAIL_BODY_RETURNTOCE);
        } 
        else if ( oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_RETURNTORSM) ) {
            sEmailBody = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONEMAILS_PROPERTYFILENAME, 
                                                     MailConstants.QUOTATION_EMAIL_BODY_RETURNTORSM);
        } 
        else if ( oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_RELEASETOCUSTOMER) ) {
            sEmailBody = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONEMAILS_PROPERTYFILENAME, 
                                                     MailConstants.QUOTATION_EMAIL_BODY_RELEASEDTOCUSTOMER);
        }
        else if ( oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_RETURNTOSM) ) {
            sEmailBody = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONEMAILS_PROPERTYFILENAME, 
                                                     MailConstants.QUOTATION_EMAIL_BODY_RETURNTOSM);
        }
        else if ( oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTONSM) ) {
        	sEmailBody = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONEMAILS_PROPERTYFILENAME, 
                    								MailConstants.QUOTATION_EMAIL_BODY_SUBMITTONSM);
        }
        else if ( oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTOMH) ) {
        	sEmailBody = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONEMAILS_PROPERTYFILENAME, 
                    								MailConstants.QUOTATION_EMAIL_BODY_SUBMITTOMH);
        }
        else if ( oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTOQA) ) {
        	sEmailBody = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONEMAILS_PROPERTYFILENAME, 
                    								MailConstants.QUOTATION_EMAIL_BODY_SUBMITTOQA);
        }
        else if ( oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTOSCM) ) {
        	sEmailBody = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONEMAILS_PROPERTYFILENAME, 
                    								MailConstants.QUOTATION_EMAIL_BODY_SUBMITTOSCM);
        }
        else if ( oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTOMFG) ) {
        	sEmailBody = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONEMAILS_PROPERTYFILENAME, 
                    								MailConstants.QUOTATION_EMAIL_BODY_SUBMITTOMFG);
        }  
        else if ( oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTOBH) ) {
        	sEmailBody = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONEMAILS_PROPERTYFILENAME, 
                    								MailConstants.QUOTATION_EMAIL_BODY_SUBMITTOBH);
        }
        else if ( oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_DMRETURNED_SUBMITTOBH) ) {
        	sEmailBody = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONEMAILS_PROPERTYFILENAME, 
                    								MailConstants.QUOTATION_EMAIL_BODY_DMRETURN_SUBMITTOBH);
        }
        else if ( oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_DMRETURNED_SUBMITTODM) ) {
        	sEmailBody = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONEMAILS_PROPERTYFILENAME, 
                    								MailConstants.QUOTATION_EMAIL_BODY_DMRETURN_SUBMITTODM);
        }
        else if ( oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_DMRETURNED_SUBMITTOSM) ) {
        	sEmailBody = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONEMAILS_PROPERTYFILENAME, 
                    								MailConstants.QUOTATION_EMAIL_BODY_DMRETURN_SUBMITTOSM);
        }
        
        
        // :::::::::::::::::::: Revise Enquiry Flow. ::::::::::::::::::::::::
        if ( oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SM_REVISE_ENQ) ) {
        	sEmailBody = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONEMAILS_PROPERTYFILENAME, 
                    								MailConstants.QUOTATION_EMAIL_BODY_REVISEENQBYSM);
        }
        else if ( oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_RSM_REVISE_ENQ) ) {
        	sEmailBody = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONEMAILS_PROPERTYFILENAME, 
                    								MailConstants.QUOTATION_EMAIL_BODY_REVISEENQBYRSM);
        }
        else if ( oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_NSM_REVISE_ENQ) ) {
        	sEmailBody = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONEMAILS_PROPERTYFILENAME, 
                    								MailConstants.QUOTATION_EMAIL_BODY_REVISEENQBYNSM);
        }
        else if ( oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_MH_REVISE_ENQ) ) {
        	sEmailBody = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONEMAILS_PROPERTYFILENAME, 
                    								MailConstants.QUOTATION_EMAIL_BODY_REVISEENQBYMH);
        }
        
        
        // :::::::::::::::::::: Regret Enquiry. ::::::::::::::::::::::::
        if ( oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_REGRETMTOENQUIRY) ) {
        	sEmailBody = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONEMAILS_PROPERTYFILENAME, 
                    								MailConstants.QUOTATION_EMAIL_BODY_REGRETMTOENQUIRY);
        }
        
		// Indent Mailing Starts From Here
		if (oWorkflowDto.getActionCode()
				.equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTOCOMMERCIALMGR)
				&& (oWorkflowDto.getEmailCode() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_WONPENDINGSM)) {
			sEmailBody = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONEMAILS_PROPERTYFILENAME,
					MailConstants.QUOTATION_EMAIL_BODY_SUBMITTOCM);
		}
		if (oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTODM)
				&& (oWorkflowDto.getEmailCode() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_WONPENDINGCM)) {
			sEmailBody = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONEMAILS_PROPERTYFILENAME,
					MailConstants.QUOTATION_EMAIL_BODY_CMSUBMITTODM);
		}
		if (oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_WON)
				&& (oWorkflowDto.getEmailCode() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_WONPENDINGDM)) {
			sEmailBody = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONEMAILS_PROPERTYFILENAME,
					MailConstants.QUOTATION_EMAIL_BODY_WON);
		}
		if (oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_LOST)
				&& (oWorkflowDto.getEmailCode() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_RELEASED)) {
			sEmailBody = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONEMAILS_PROPERTYFILENAME,
					MailConstants.QUOTATION_EMAIL_BODY_LOST);
		}
		if (oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_ABONDENT)
				&& (oWorkflowDto.getEmailCode() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_RELEASED)) {
			sEmailBody = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONEMAILS_PROPERTYFILENAME,
					MailConstants.QUOTATION_EMAIL_BODY_ABONDENT);
		}
		if (oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_RETURNTOCM)
				&& (oWorkflowDto.getEmailCode() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_WONPENDINGCM)) {
			sEmailBody = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONEMAILS_PROPERTYFILENAME,
					MailConstants.QUOTATION_EMAIL_BODY_CMRETURNTOSM);
		}
		if (oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_RETURNTOCM)
				&& (oWorkflowDto.getEmailCode() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_WONPENDINGDM)) {
			sEmailBody = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONEMAILS_PROPERTYFILENAME,
					MailConstants.QUOTATION_EMAIL_BODY_DMRETURNTOCM);
		}
		if (oWorkflowDto.getActionCode().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_ACTION_RETURNTOSM)
				&& (oWorkflowDto.getEmailCode() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_WONPENDINGDM)) {
			sEmailBody = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONEMAILS_PROPERTYFILENAME,
					MailConstants.QUOTATION_EMAIL_BODY_DMRETURNTOSM);
		}

		
        sEmailBody = PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONEMAILS_PROPERTYFILENAME, 
                                                 MailConstants.QUOTATION_EMAIL_BODY_HEADER) + " " +
                     sEmailBody + 
                     PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONEMAILS_PROPERTYFILENAME, 
                                                 MailConstants.QUOTATION_EMAIL_AUTOGENARATED_MSG) + " " +
                     PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONEMAILS_PROPERTYFILENAME, 
                                                 MailConstants.QUOTATION_EMAIL_BODY_FOOTER);
        
        
        
        /* Replacing the content area with appropriate enquiry values */
        // to allow special characters in string values using replaceString1 method
        sEmailBody = CommonUtility.replaceString(sEmailBody, "__AssignedToName__",   oWorkflowDto.getAssignedToName());
        sEmailBody = CommonUtility.replaceString1(sEmailBody, "__EnquiryLink__",      sbEnquiryLink.toString());
        sEmailBody = CommonUtility.replaceString(sEmailBody, "__EnquiryNumber__",    oWorkflowDto.getEnquiryNo());
        sEmailBody = CommonUtility.replaceString(sEmailBody, "__EnquiryStatus__",    oWorkflowDto.getNewStatus());
        sEmailBody = CommonUtility.replaceString(sEmailBody, "__CustomerName__",     oWorkflowDto.getEnquiryDto().getCustomerName());
        sEmailBody = CommonUtility.replaceString1(sEmailBody, "__endUserName__",      oWorkflowDto.getEnquiryDto().getEndUser());
        sEmailBody = CommonUtility.replaceString1(sEmailBody, "__CustomerLocation__", oWorkflowDto.getEnquiryDto().getLocation());
        sEmailBody = CommonUtility.replaceString1(sEmailBody, "__ConsultantName__",   oWorkflowDto.getEnquiryDto().getConsultantName());
        sEmailBody = CommonUtility.replaceString1(sEmailBody, "__EnquiryType__",      oWorkflowDto.getEnquiryDto().getEnquiryType());
        sEmailBody = CommonUtility.replaceString(sEmailBody, "__CreatedBy__",        oWorkflowDto.getEnquiryDto().getCreatedBy());
        sEmailBody = CommonUtility.replaceString(sEmailBody, "__CreatedDate__",      oWorkflowDto.getEnquiryDto().getCreatedDate());
        sEmailBody = CommonUtility.replaceString(sEmailBody, "__CrossReference__",   oWorkflowDto.getEnquiryDto().getCrossReference());
        sEmailBody = CommonUtility.replaceString(sEmailBody, "__Description__",      oWorkflowDto.getEnquiryDto().getDescription());
        
        LoggerUtility.log("INFO", this.getClass().getName(), "getWorkflowEmailSubject", "Workflow Email Body = " + sEmailBody);
        return sEmailBody;
    }
}
