/**
 * *****************************************************************************
 * Project Name: Request For Quotation
 * Document Name: KeyValueVo.java
 * Package: in.com.rbc.quotation.common.vo
 * Desc: 
 * *****************************************************************************
 * Author: 100002865 (Shanthi Chinta)
 * Date: Sep 29, 2008
 * *****************************************************************************
 */
package in.com.rbc.quotation.common.vo;

import in.com.rbc.quotation.common.utility.CommonUtility;

public class KeyValueVo {
    private String key = null;
    private String value = null;
    private String value2 = null;

    private String ratingNumber=null;
    private String mcValue=null;
    private String enquiryNumber=null;
    private String unitPriceRsm=null; //To Display Previous Ratings Unit Price
	/**
     * @return Returns the key.
     */
    public String getKey() {
        return CommonUtility.replaceNull(this.key);
    }
    /**
     * @param key The key to set.
     */
    public void setKey(String key) {
        this.key = key;
    }
    /**
     * @return Returns the value.
     */
    public String getValue() {
        return CommonUtility.replaceNull(this.value);
    }
    /**
     * @param value The value to set.
     */
    public void setValue(String value) {
        this.value = value;
    }
    /**
     * @return Returns the value2.
     */
    public String getValue2() {
        return CommonUtility.replaceNull(this.value2);
    }
    /**
     * @param value2 The value2 to set.
     */
    public void setValue2(String value2) {
        this.value2 = value2;
    }
	public String getRatingNumber() {
		return CommonUtility.replaceNull(this.ratingNumber);
	}
	public void setRatingNumber(String ratingNumber) {
		this.ratingNumber = ratingNumber;
	}
    public String getMcValue() {
		return CommonUtility.replaceNull(mcValue);
	}
	public void setMcValue(String mcValue) {
		this.mcValue = mcValue;
	}
	public String getEnquiryNumber() {
		return CommonUtility.replaceNull(enquiryNumber);
	}
	public void setEnquiryNumber(String enquiryNumber) {
		this.enquiryNumber = enquiryNumber;
	}
    /**
     * @return Returns the unitPriceRsm.
     */

	public String getUnitPriceRsm() {
		return CommonUtility.replaceNull(unitPriceRsm);
	}
    /**
     * @param unitPriceRsm The unitPriceRsm to set.
     */

	public void setUnitPriceRsm(String unitPriceRsm) {
		this.unitPriceRsm = unitPriceRsm;
	}

}
