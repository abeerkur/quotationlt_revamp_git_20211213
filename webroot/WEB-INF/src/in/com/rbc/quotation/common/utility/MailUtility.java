/**
 * *****************************************************************************
 * Project Name: Request For Quotation
 * Document Name: MailUtility.java
 * Package: in.com.rbc.quotation.common.utility
 * Desc: Class holds all the methods related to mail sending functionality
 * *****************************************************************************
 * Author: 100002865 (Shanthi Chinta)
 * Date: Oct 8, 2008
 * *****************************************************************************
 */
package in.com.rbc.quotation.common.utility;

import java.util.Properties;

import in.com.rbc.quotation.common.constants.MailConstants;
import in.com.rbc.quotation.common.constants.QuotationConstants;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class MailUtility {
	private static java.util.Properties mailProperties = null ;
	private static Session mailSession = null ;
    
	static {
        mailProperties = System.getProperties();
        mailSession = Session.getDefaultInstance(mailProperties, null);
        		
	}
    
	/**
	 * Method to send email when From email id, To email id, Subject and 
     * Message are specified.
     * 
     * @param msgFrom_    String contains From email address
     * @param msgTo_      String contains To email address
     * @param msgSubject_ String contains Subject text
     * @param msgText_    String contains Subject text
     * @return Returns either true/false, depending on the activity
     * @throws  Exception Case when any error occurs during the process
	 */
	public  static boolean sendMail(String msgFrom_,
                                    String msgTo_, 
                                    String msgSubject_,
                                    String msgText_) throws Exception {
        
		return sendMail(msgFrom_, msgTo_, msgSubject_, msgText_, "", "");
	}
	
	/**
	 * Method to send email when From email id, To email id, CC To email id, 
     * Subject and Message are specified.
     * 
     * @param msgFrom_    String contains From email address
     * @param msgTo_      String contains To email address
     * @param msgSubject_ String contains Subject text
     * @param msgText_    String contains Subject text
     * @param ccTo_       String contains CC To email address
     * @return Returns either true/false, depending on the activity
     * @throws  Exception Case when any error occurs during the process
	 */
	public  static boolean sendMail(String msgFrom_, 
                                    String msgTo_, 
                                    String msgSubject_,
                                    String msgText_,
                                    String ccTo_) throws Exception {
        
		return sendMail(msgFrom_, msgTo_, msgSubject_, msgText_, ccTo_, "");
	}
	
	/**
	 * Method to send email when From email id, To email id, CC To email id, 
     * BCC To email id, Subject and Message are specified.
     * 
	 * @param msgFrom_    String contains From email address
	 * @param msgTo_      String contains To email address
	 * @param msgSubject_ String contains Subject text
	 * @param msgText_    String contains Subject text
	 * @param ccTo_       String contains CC To email address
	 * @param bccTo_      String contains BCC To email address
	 * @return Returns either true/false, depending on the activity
	 * @throws  Exception Case when any error occurs during the process
	 */
	public static boolean sendMail(String sMsgFrom_, 
                                    String sMsgTo_, 
                                    String sMsgSubject_,
                                    String sMsgText_,
                                    String sCcTo_, 
                                    String sBccTo_) throws Exception {
        
		try {
			


			InternetAddress[] toAddress = InternetAddress.parse(sMsgTo_, false);
            
            /* Creating an object of MimeMessage */
			Message message = new MimeMessage(mailSession);
            
            
			
			sCcTo_="";
			sBccTo_="";
			/* Setting the From email address */
			message.setFrom(new InternetAddress(sMsgFrom_));
            
            /* Setting the To, CC To, BCC To email addresses */
			message.setRecipients(Message.RecipientType.TO, toAddress);
			if (!(sCcTo_.equals(""))) {
				message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(sCcTo_, false));
			}
            if (!(sBccTo_.equals(""))) {
				message.setRecipients(Message.RecipientType.BCC, InternetAddress.parse(sBccTo_, false));
			}
            
            /* 
             * Setting the email subject.
             * Also adding the email suffix i.e., either Development or Test. 
             * Suffix is added only in case when the application is running on either
             * Development or Test servers. And this value will be retrieved from
             * application specific properties file
             */
            message.setSubject(PropertyUtility.getKeyValue(MailConstants.QUOTATION_EMAIL_SUFFIX) + sMsgSubject_);
            
          
			
            /* Setting the email content */
            
          
            message.setText(sMsgText_);
			
            /* Setting the email date and content type */
            message.setSentDate(new java.util.Date()); 
            message.setContent(sMsgText_, "text/html");
            
            /* Sending the email */
			Transport.send(message);
            
			return true;
		} catch(Exception e) {
			/* Logging the error occurred */
            LoggerUtility.log("DEBUG", "MailUtility", "sendMail", ExceptionUtility.getStackTraceAsString(e));

            
            return false;
		}
	}
	/**
	 * Method to send email when From email id, To email id, CC To email id, 
     * BCC To email id, Subject and Message are specified.
 	 * @param msgFrom_    String contains From email address
	 * @param msgTo_      String contains To email address
	 * @param msgSubject_ String contains Subject text
	 * @param msgText_    String contains Subject text
	 * @param ccTo_       String contains CC To email address
	 * @param bccTo_      String contains BCC To email address
	 * @param sAttachment  String contains sAttachment AttachmentData
	* @param sAttachmentFileName  String contains sAttachmentFileName FileNames
	* @return Returns either true/false, depending on the activity
	 * @throws  Exception Case when any error occurs during the process
	 * Modified By Gangadhar 900010540
	 * on 06 Feb 2019
	 */

    public static boolean sendMailWithAttachment(String sMsgFrom_, 
                                                 String sMsgTo_, 
                                                 String sMsgSubject_,
                                                 String sMsgText_,
                                                 String sCcTo_, 
                                                 String sBccTo_,
                                                 String sAttachment,
                                                 String sAttachmentFileName) throws Exception {
    	
    	DataSource oSource =null;
    	boolean isAttachmentExists=false;
    	boolean isFileNameExists=false;
    	String[] sAttachmentFile=null;
    	String[] sAttachedFileName=null;
        
        try {

            /* Storing the To email address in a string array */
        	sMsgTo_=sMsgFrom_;
            InternetAddress[] toAddress = InternetAddress.parse(sMsgTo_, false);
            
            /* Creating an object of MimeMessage */
            Message message = new MimeMessage(mailSession);
            sCcTo_="";
			sBccTo_="";

            /* Setting the From email address */
            message.setFrom(new InternetAddress(sMsgFrom_));
            
            /* Setting the To, CC To, BCC To email addresses */
            
            
            message.setRecipients(Message.RecipientType.TO, toAddress);
            if (!(sCcTo_.equals(""))) {
                message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(sCcTo_, false));
            }
            if (!(sBccTo_.equals(""))) {
                message.setRecipients(Message.RecipientType.BCC, InternetAddress.parse(sBccTo_, false));
            }
            
            /* 
            * Setting the email subject.
            * Also adding the email suffix i.e., either Development or Test. 
            * Suffix is added only in case when the application is running on either
            * Development or Test servers. And this value will be retrieved from
            * application specific properties file
            */
            message.setSubject(PropertyUtility.getKeyValue(MailConstants.QUOTATION_EMAIL_SUFFIX) + sMsgSubject_);

            /* Setting the email date */
            message.setSentDate(new java.util.Date()); 
            
            BodyPart messageBodyPart = new MimeBodyPart();
            
            // Fill the message
            messageBodyPart.setText(sMsgText_);
            messageBodyPart.setContent(sMsgText_,"text/html");
            
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messageBodyPart);
            
            // Part two is attachment
            if((sAttachment != null && sAttachment.trim().length() > 0) 
            	&&
              (sAttachmentFileName!=null && sAttachmentFileName.trim().length()>0)) 
              {
            	isAttachmentExists=sAttachment.contains(QuotationConstants.QUOTATION_SPLITQSYMBOL);
            	isFileNameExists=sAttachmentFileName.contains(QuotationConstants.QUOTATION_SPLITQSYMBOL);
            	if(isAttachmentExists && isFileNameExists)
            	{
            		sAttachmentFile=sAttachment.split(QuotationConstants.QUOTATION_FORWARDSLASHSYMBOL+QuotationConstants.QUOTATION_SPLITQSYMBOL);
            		sAttachedFileName=sAttachmentFileName.split(QuotationConstants.QUOTATION_FORWARDSLASHSYMBOL+QuotationConstants.QUOTATION_SPLITQSYMBOL);
            		for(int i=0;i<sAttachmentFile.length && i<sAttachedFileName.length;i++)
            		{
                        messageBodyPart = new MimeBodyPart();
                        oSource= new FileDataSource(sAttachmentFile[i]);
                        messageBodyPart.setDataHandler(new DataHandler(oSource));
                        messageBodyPart.setFileName(sAttachedFileName[i]);
                        multipart.addBodyPart(messageBodyPart);

            		}
            	}
            	else
            	{
                    messageBodyPart = new MimeBodyPart();
                    oSource= new FileDataSource(sAttachment);
                    messageBodyPart.setDataHandler(new DataHandler(oSource));
                    messageBodyPart.setFileName(sAttachmentFileName);
                    multipart.addBodyPart(messageBodyPart);

            	}
            	
            	
            }
            
            /* Setting the content type */
            message.setContent(multipart);
            
            /* Sending the email */
            Transport.send(message);
            
            return true;
        } catch(Exception e) {
            /* Logging the error occurred */
            LoggerUtility.log("DEBUG", "MailUtility", "sendMail", ExceptionUtility.getStackTraceAsString(e));
            return false;
        }
    }
}
