package in.com.rbc.quotation.common.taglib;

import in.com.rbc.quotation.common.constants.QuotationConstants;

public class SecurityUtility {
     public static String getRoleRequestParamName(int iRoleId) {
        String sRoleReqParamName = "" ;
        switch(iRoleId){
            case QuotationConstants.QUOTATION_SECURITY_ADMINISTRATOR: 
                sRoleReqParamName = QuotationConstants.QUOTATION_REQ_ROLE_ADMINISTRATOR;
                break;
        }
        return sRoleReqParamName;
    }
}
