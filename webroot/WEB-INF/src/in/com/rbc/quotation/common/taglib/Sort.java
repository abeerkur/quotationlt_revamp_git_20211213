/**
 * ******************************************************************************************
 * Project Name: Request For Quotation
 * Document Name: Sort.java
 * Package: in.com.rbc.quotation.common.taglib
 * Desc: This class is used while displaying results list along with pagination, in JSP pages
 * ******************************************************************************************
 * @author 100002865 (Shanthi Chinta)
 * ******************************************************************************************
 */
package in.com.rbc.quotation.common.taglib;

import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.utility.ListModel;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTagSupport;

public class Sort extends BodyTagSupport {
    private String dbfieldid;
    
    public void setDbfieldid(String dbfieldid) {
        this.dbfieldid = dbfieldid;
    }

    public int doAfterBody() throws JspException {
        try {
            HttpServletRequest request = (HttpServletRequest)pageContext.getRequest();
            BodyContent oBodyContent = getBodyContent();
            String sBody = oBodyContent.getString();
            JspWriter out = oBodyContent.getEnclosingWriter();
            if (request.getAttribute(QuotationConstants.QUOTATION_LIST_LISTMODELCLASS) != null) {
                ListModel oListModel = (ListModel)request.getAttribute(QuotationConstants.QUOTATION_LIST_LISTMODELCLASS);
                out.println("<a class=\"formLabelSort\"" +
                              " href=\"JavaScript:goSort(" + oListModel.getFormName() + ", " + 
                                                             oListModel.getActionName() + ", \'" + 
                                                             oListModel.getScreenDef() + "\', \'" + 
                                                             dbfieldid + "\', \'" + 
                                                             (oListModel.getSortBy().equals(dbfieldid)? 
                                                                     ((oListModel.getSortOrder().equals(ListModel.ASCENDING))? 
                                                                             ListModel.DESCENDING:ListModel.ASCENDING)
                                                                     :ListModel.ASCENDING) + "\');\" >" + 
                              sBody + "</a>" + oListModel.getArrow(dbfieldid));
            }
        }catch(Exception e){
            //ExceptionLog.logFinestExceptionStackTrace(InfogateConstants.LOG_LISTING, this.getClass().getName(), e.getMessage(), e);
        }
        
        return SKIP_BODY;
    }
}

