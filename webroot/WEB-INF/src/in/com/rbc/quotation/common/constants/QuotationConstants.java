/**
 * *****************************************************************************
 * Project Name: Request For Quotation
 * Document Name: QuotationConstants.java
 * Package: in.com.rbc.quotation.common.constants
 * Desc: This class holds all constants declared across application
 * *****************************************************************************
 * @author 100002865 (Shanthi Chinta)
 * *****************************************************************************
 */
package in.com.rbc.quotation.common.constants;


import in.com.rbc.quotation.common.utility.PropertyUtility;

/*import in.com.rbc.quotation.enquiry.action.another;*/

public interface QuotationConstants {
    /*
     * Constants related to RBC properties file
     */
	public static final String RBC_PROPERTYFILENAME = "rbc";
	public static final String RBC_HOME_URL = "RBC_HOME_URL";
	public static final String RBC_INTRANET_URL = "RBC_INTRANET_URL";
	public static final String RBC_LOGO = "RBC_LOGO";

    /*
     * Constants related to DB (DSN, Schema, etc)
     */
	public static final String QUOTATION_PROPERTYFILENAME = "quotationlttool";
	public static final String QUOTATIONTERMS_PROPERTYFILENAME = "props.quotationterms";
    public static final String QUOTATIONEMAILS_PROPERTYFILENAME = "props.quotationemails";
    
    public static final String QUOTATION_DB_SOURCE = "QUOTATION_DB_SOURCE";
    public static final String QUOTATION_DB_SCHEMA_QUOTATION = "QUOTATION_DB_SCHEMA_QUOTATION";
    public static final String QUOTATION_DB_SCHEMA_SHARED = "QUOTATION_DB_SCHEMA_SHARED";
    
    public static final String QUOTATION_FEEDBACK_LINK = "QUOTATION_FEEDBACK_LINK";
    public static final String QUOTATION_FEEDBACK_SUBJECT = "QUOTATION_FEEDBACK_SUBJECT";
	
    /*
     * Constants related to roles email groups
     */
    public static final String QUOTATION_EMAILGROUP_NAME_ADMINISTRATOR = "QUOTATION_EMAILGROUP_NAME_ADMINISTRATOR";    
    public static final String QUOTATION_EMAILGROUP_MEMBEROF_ADMINISTRATOR = "QUOTATION_EMAILGROUP_MEMBEROF_ADMINISTRATOR";
    /*
     * Constants related to request roles
     */
    public static final String QUOTATION_REQ_ROLE_ADMINISTRATOR = "isAdministrator";
    public static final String QUOTATION_REQ_SECURITY_ADMINISTRATOR = "administrator";
    public static final int QUOTATION_SECURITY_ADMINISTRATOR = 1;
    
    public static final String QUOTATION_ENQUIRYNODIGITCOUNT = "QUOTATION_ENQUIRYNODIGITCOUNT";
    public static final String QUOTATION_MAXIMUMRATINGSCOUNT = "QUOTATION_MAXIMUMRATINGSCOUNT";
    public static final String QUOTATION_DASHBOARD_RECENTENQUIRYSCOUNT = "QUOTATION_DASHBOARD_RECENTENQUIRYSCOUNT";
    public static final String QUOTATION_DASHBOARD_RELEASEDENQUIRYSCOUNT = "QUOTATION_DASHBOARD_RELEASEDENQUIRYSCOUNT";
    
    public static final String QUOTATION_TEMPFOLDER = "QUOTATION_TEMPFOLDER";
    public static final String QUOTATION_ATTACHMENTFILEPREFIX = "QUOTATION_ATTACHMENTFILEPREFIX";
    

    public static final String QUOTATION_DOCUMENT_SUBMISSION = "QUOTATION_DOCUMENT_SUBMISSION";
    public static final String QUOTATION_PACKAGE_FORWARDING = "QUOTATION_PACKAGE_FORWARDING";
    public static final String QUOTATION_Taxes_Duties = "QUOTATION_Taxes_Duties";
    public static final String QUOTATION_SPECIAL_NOTE = "QUOTATION_SPECIAL_NOTE";
    

    public static final String QUOTATION_NOTES_1="QUOTATION_NOTES_1";
    public static final String QUOTATION_NOTES_2="QUOTATION_NOTES_2";
    public static final String QUOTATION_NOTES_3="QUOTATION_NOTES_3";
    public static final String QUOTATION_NOTES_4="QUOTATION_NOTES_4";
    public static final String QUOTATION_NOTES_5="QUOTATION_NOTES_5";
    public static final String QUOTATION_NOTES_6="QUOTATION_NOTES_6";
    public static final String QUOTATION_NOTES_7="QUOTATION_NOTES_7";
    public static final String QUOTATION_NOTES_8="QUOTATION_NOTES_8";
    public static final String QUOTATION_NOTES_9="QUOTATION_NOTES_9";
    public static final String QUOTATION_NOTES_10="QUOTATION_NOTES_10";
    public static final String QUOTATION_NOTES_11="QUOTATION_NOTES_11";
    public static final String QUOTATION_NOTES_12="QUOTATION_NOTES_12";
    public static final String QUOTATION_NOTES_13="QUOTATION_NOTES_13";
    public static final String QUOTATION_NOTES_14="QUOTATION_NOTES_14";
    
    
    
    

    		public static final String QUOTATION_DEFINTIONS="DEFINTIONS";
    		public static final String QUOTATION_VALIDITY="VALIDITY";
    		public static final String QUOTATION_SCOPE="SCOPE";
    		public static final String QUOTATION_PRICEANDBASIS="PRICEANDBASIS";
    		public static final String QUOTATION_TAXES_DUTIES="TAXES_DUTIES";
    		public static final String QUOTATION_STATUTORY_VARIATION="STATUTORY_VARIATION";
    		public static final String QUOTATION_FREIGHT_AND_INSURANCE="FREIGHT_AND_INSURANCE";
    		public static final String QUOTATION_PACKING_AND_FORWARDING="PACKING_AND_FORWARDING";
    		public static final String QUOTATION_TERMS_OF_PAYMENT="TERMS_OF_PAYMENT";
    		public static final String QUOTATION_DELIVERY="DELIVERY";
    		public static final String QUOTATION_DRAWINGANDDOCSAPPROVAL="DRAWING_AND_DOCUMENTS_APPROVAL";
    		public static final String QUOTATION_DELAYMANFCLEARANCE="DELAY_IN_MANUFACTURING_CLEARANCE";
    		public static final String QUOTATION_FORCE_MAJEURE="FORCE_MAJEURE";
    		public static final String QUOTATION_STORAGE="STORAGE";
    		public static final String QUOTATION_WARRANTY="WARRANTY";
    		public static final String QUOTATION_WEIGHTS_AND_DIMENSIONS="WEIGHTS_AND_DIMENSIONS";
    		public static final String QUOTATION_TESTS="TESTS";
    		public static final String QUOTATION_STANDARDS="STANDARDS";
    		public static final String QUOTATION_LIMITATION_OF_LIABILITY="LIMITATION_OF_LIABILITY";
    		public static final String QUOTATION_CONSEQUENTIAL_LOSSES="CONSEQUENTIAL_LOSSES";
    		public static final String QUOTATION_ARBITRATION="ARBITRATION";
    		public static final String QUOTATION_LANGUAGE="LANGUAGE";
    		public static final String QUOTATION_GOVERNING_LAW="GOVERNING_LAW";
    		public static final String QUOTATION_VARIATION_IN_QUANTITY="VARIATION_IN_QUANTITY";
    		public static final String QUOTATION_TRANSFER_OF_TITLE="TRANSFER_OF_TITLE";
    		public static final String QUOTATION_CONFIDENTIAL_TREATMENT_AND_SECRECY="CONFIDENTIAL_TREATMENT_AND_SECRECY";
    		public static final String QUOTATION_PASSING_OF_BENEFIT_AND_RISK="PASSING_OF_BENEFIT_AND_RISK";
    		public static final String QUOTATION_COMPENSATION_DUE_TO_BUYERS_DEFAULT="COMPENSATION_DUE_TO_BUYERS_DEFAULT";
    		
    		public static final String QUOTATION_SUSPENSION ="Suspension";
    		
    		public static final String QUOTATION_BANKRUPTCY ="BANKRUPTCY";
    		public static final String QUOTATION_ACCEPTANCE ="ACCEPTANCE";
    		public static final String QUOTATION_LIMIT_OF_SUPPLY ="LIMIT_OF_SUPPLY";
    		public static final String QUOTATION_CHANGE_IN_SCOPE ="CHANGE_IN_SCOPE";
    		public static final String QUOTATION_CHANNELS_FOR_COMMUNICATION="CHANNELS_FOR_COMMUNICATION";
    		public static final String QUOTATION_GENERAL="GENERAL";

    
    
    
    public static final String QUOTATION_DOCUMENT_SUBMISSIONS = "QUOTATION_DOCUMENT_SUBMISSIONS";
    public static final String QUOTATION_PACKAGE_FORWARDINGS = "QUOTATION_PACKAGE_FORWARDINGS";
    public static final String QUOTATION_TAXES_DUTIESS = "QUOTATION_TAXES_DUTIESS";
    public static final String QUOTATION_SPECIAL_NOTES = "QUOTATION_SPECIAL_NOTES";

    
    
    public static final String QUOTATION_TERMS_1 = "QUOTATION_TERMS_1";
    public static final String QUOTATION_TERMS_2 = "QUOTATION_TERMS_2";
    public static final String QUOTATION_TERMS_3 = "QUOTATION_TERMS_3";
    public static final String QUOTATION_TERMS_4 = "QUOTATION_TERMS_4";
    public static final String QUOTATION_TERMS_5 = "QUOTATION_TERMS_5";
    public static final String QUOTATION_TERMS_6 = "QUOTATION_TERMS_6";
    public static final String QUOTATION_TERMS_7 = "QUOTATION_TERMS_7";
    
    public static final String QUOTATION_TERMS_8 = "QUOTATION_TERMS_8";
    public static final String QUOTATION_TERMS_9 = "QUOTATION_TERMS_9";
    public static final String QUOTATION_TERMS_10 = "QUOTATION_TERMS_10";
    public static final String QUOTATION_TERMS_11 = "QUOTATION_TERMS_11";
    public static final String QUOTATION_TERMS_12 = "QUOTATION_TERMS_12";
    public static final String QUOTATION_TERMS_13 = "QUOTATION_TERMS_13";
    public static final String QUOTATION_TERMS_14 = "QUOTATION_TERMS_14";
    
	/*
     * Constants related to Forwards
	 */
    public static final String QUOTATION_FORWARD_HOME = "home";
    public static final String QUOTATION_FORWARD_SUCCESS = "success";
	public static final String QUOTATION_FORWARD_FAILURE = "failure";
    public static final String QUOTATION_FORWARD_SEARCH = "search";
    public static final String QUOTATION_FORWARD_RESULTS = "results";
    public static final String QUOTATION_FORWARD_DETAILS = "details";
	public static final String QUOTATION_FORWARD_UPDATE = "update";
	public static final String QUOTATION_FORWARD_INSERT = "INSERT";
    public static final String QUOTATION_FORWARD_EXPORT = "export";
    public static final String QUOTATION_FORWARD_PRINT = "print";
    public static final String QUOTATION_FORWARD_VIEW = "view";
    public static final String QUOTATION_FORWARD_CONFIRM = "confirmation";
    public static final String QUOTATION_FORWARD_VIEWENQUIRY = "viewenquiry";
	public static final String QUOTATION_FORWARD_DATASUCCESS = "datapage";
	public static final String QUOTATION_FORWARD_OFFERDATA_VIEW = "viewoffer";
	public static final String QUOTATION_FORWARD_ADDMASTERDATA = "add";
	public static final String QUOTATION_FORWARD_INDENTWON = "indentwon";
	public static final String QUOTATION_FORWARD_MTOENQUIRY_VIEW = "viewmtoenquiry";
	public static final String QUOTATION_FORWARD_ABONDENT_VIEW = "viewabondentsubmit";
	public static final String QUOTATION_FORWARD_LOST_VIEW = "viewlostsubmit";
	public static final String QUOTATION_FORWARD_WON_VIEW = "viewwonsubmit";
	public static final String QUOTATION_FORWARD_UPDATE_PLANTDETAILS_VIEW = "viewupdateplantdetails";
	
    /*
     * Constants related to Exceptions
     */
    public static final String QUOTATION_EXCEPTION = "EXCEPTION";
    /*
     * Constant to store the name of the variable that stores a error message
     */
    public static final String QUOTATION_ERRORMESSAGE = "ERRORMESSAGE";
    public static final String QUOTATION_HEADINGMESSAGE = "HEADINGMESSAGE";
    public static final String QUOTATION_SUCCESSMESSAGE = "SUCCESSMESSAGE";
    public static final String QUOTATION_LINKSMESSAGE = "LINKSMESSAGE";
    
    public static final String QUOTATION_INVALIDTOKENMESSAGE = "Your request could not be processed due to one of the below reasons. " +
                                                               "Please try again." +
                                                               "<ul>" +
                                                               "<li>You clicked on the submit button, more than once, before the response is sent back." +
                                                               "<li>You clicked on the back button in the browser or simply refreshed the page." +
                                                               "<li>You accessesed the web page by returning to a previously bookmarked page." +
                                                               "<li>Your Session Will be Expired." +
                                                               "</ul>";
    
    public static final String QUOTATION_REQHEADER_UID = "uid";
    public static final String QUOTATION_REQHEADER_SSOID = "SSOID";
    public static final String QUOTATION_REQHEADER_SMUSER = "sm-user";
    public static final String QUOTATION_SSOID = "QUOTATION_SSOID";
   // public static final String QUOTATION_DEFAULT_SSOID = "100024594"; //Suprakash Ghosh
   //public static final String QUOTATION_DEFAULT_SSOID = "610093155"; //RSM
   //public static final String QUOTATION_DEFAULT_SSOID = "610061540"; //CE //Dilip Mahadev Sangle
  // public static final String QUOTATION_DEFAULT_SSOID = "100005794";	//DM Gautam Mondal 
  //public static final String QUOTATION_DEFAULT_SSOID = "100002720"; //SM RamMohan
  public static final String QUOTATION_DEFAULT_SSOID = PropertyUtility.getKeyValue(QUOTATION_PROPERTYFILENAME, QUOTATION_SSOID); 
    //public static final String QUOTATION_DEFAULT_SSOID ="100006007"; //Neelam
 // public static final String QUOTATION_DEFAULT_SSOID = "900010540"; // Giving SM Roles To Gangadhar
 //  public static final String QUOTATION_DEFAULT_SSOID = "100005885"; // Giving Roles To Susmitha Dutta D.E
 // public static final String QUOTATION_DEFAULT_SSOID = "610060516"; // Giving Roles To Sourav Saha D.E
// public static final String QUOTATION_DEFAULT_SSOID = "100005826"; // Giving Roles To Sourav Saha D.E   
   
    
   

    public static final String QUOTATION_DEFAULT_HEADERINFO = "RBC_Security_QUOTATION_Administrators";;
    
    public static final String QUOTATION_USERDTO_OBJECT = "userDtoObject";
    public static final String QUOTATION_LOOKUPSELECTTYPE_SINGLE = "SINGLE";
    public static final String QUOTATION_LOOKUPSELECTTYPE_MULTIPLE = "MULTIPLE";
    public static final String QUOTATION_LOOKUPEMPFILTER_VP = "VP";
    public static final String QUOTATION_LOOKUPEMPFILTER_HRM = "HRM";
    
    public static final String QUOTATION_DATE_FORMAT = "MM/dd/yyyy";
    public static final String QUOTATION_YEAR_FORMAT = "yyyy";
    public static final String QUOTATION_MONTH_FORMAT = "MON";
    public static final String QUOTATION_DATE_TOKENIZER = "/";    
    
    /*
     * Constants related to pagination
     */
    public static final String QUOTATION_LIST_NUMRECORDSPERPAGE = "50";
    public static final String QUOTATION_LIST_LISTSIZE = "sizeOfList";
    public static final String QUOTATION_LIST_LISTMODELCLASS = "ListModel";
    public static final String QUOTATION_LIST_RESULTSLIST = "resultsList";
    public static final String QUOTATION_SEARCH_QUERY = "whereQuery";
    public static final String QUOTATION_SEARCH_SORTFIELD = "sortField";
    public static final String QUOTATION_SEARCH_SORTORDER = "sortOrder";
    
    /*
     * Constant related to paging from view request screen
     */
    public static final String QUOTATION_PAGING_DISPLAYPAGING = "displayPaging";
    public static final String QUOTATION_PAGING_CURRENTINDEX = "currentIndex";
    public static final String QUOTATION_PAGING_PREVIOUSID = "previousId";
    public static final String QUOTATION_PAGING_PREVIOUSINDEX = "previousIndex";
    public static final String QUOTATION_PAGING_DISPLAY_MESSAGE = "displayMessage";
    public static final String QUOTATION_PAGING_NEXTID = "nextId";
    public static final String QUOTATION_PAGING_NEXTINDEX = "nextIndex";
    
    public static final String RBC_QUOTATION_CUSTOMER_QUERY = "customerQuery";
    public static final String RBC_QUOTATION_LOCATION_QUERY = "locationQuery";
    public static final String RBC_QUOTATION_LISTPRICE_QUERY = "listPriceQuery";
    public static final String RBC_QUOTATION_CUSTDISCOUNT_QUERY = "custDiscountQuery";
    
    /*
     * Constants related to AJAX look ups
     */
    public static final String QUOTATION_AJAXLOOKUP_TARGETTYPE_COMBO = "comboBox";
    public static final String QUOTATION_AJAXLOOKUP_TARGETTYPE_TEXT = "textData";
    public static final String QUOTATION_AJAXLOOKUP_TARGETTYPE_DEFAULT = "comboBox";
    
    public static final String[][] QUOTATION_ENQUIRYTYPESLIST = {
        {"B", "Budgetary"},
        {"P", "Permanent"}
    };
    
    public static final String[][] QUOTATION_OFFERTYPESLIST = {
        {"P", "Precise"},
        {"D", "Detailed"}
    };
	
    public static final int QUOTATION_LITERAL_HTLT = 1;
    public static final int QUOTATION_LITERAL_POLE = 2;
    public static final int QUOTATION_LITERAL_SCSR = 3;
    public static final int QUOTATION_LITERAL_DUTY = 4;
    public static final int QUOTATION_LITERAL_DEGOFPRO = 5;
    public static final int QUOTATION_LITERAL_TERMBOX = 6;
    public static final int QUOTATION_LITERAL_INSULCLASS = 7;
    public static final int QUOTATION_LITERAL_MOUNTING = 8;
    public static final int QUOTATION_LITERAL_SHAFTEXT = 9;
    public static final int QUOTATION_LITERAL_ENCLOSURE = 10;
    public static final int QUOTATION_LITERAL_DOR = 11;
    public static final int QUOTATION_LITERAL_APPL = 12;
    
    /*
     * Constants related to Workflow
     */
    public static final String QUOTATION_WORKFLOW_ACTION_PENDING = "P";
    public static final String QUOTATION_WORKFLOW_ACTION_SUBMITTODESIGNOFFICE = "SUB";
    public static final String QUOTATION_WORKFLOW_ACTION_REASSIGNTODE = "RAS";
    public static final String QUOTATION_WORKFLOW_ACTION_SUBMITTODM = "SDM";
    
    public static final String QUOTATION_WORKFLOW_ACTION_SUBMITTOCE = "SCE";
    public static final String QUOTATION_WORKFLOW_ACTION_RETURNTOSM = "RSM";
    public static final String QUOTATION_WORKFLOW_ACTION_RETURNTODM = "RDM";
    public static final String QUOTATION_WORKFLOW_ACTION_RETURNTODE = "RDE";
    public static final String QUOTATION_WORKFLOW_ACTION_SUBMITTORSM = "SRS";
    public static final String QUOTATION_WORKFLOW_ACTION_RETURNTOCE = "RCE";
    public static final String QUOTATION_WORKFLOW_ACTION_SUBMITTOSM = "SSM";
    public static final String QUOTATION_WORKFLOW_ACTION_RETURNTORSM = "RRS";
    public static final String QUOTATION_WORKFLOW_ACTION_RELEASETOCUSTOMER = "END";
    public static final String QUOTATION_WORKFLOW_ACTION_CREATEREVISION = "REV";
    public static final String QUOTATION_WORKFLOW_ACTION_ASSIGN_TO_DESIGNOFFICE ="ADM";
    public static final String QUOTATION_WORKFLOW_ACTION_SM_REVISE_ENQ = "SRE";
    public static final String QUOTATION_WORKFLOW_ACTION_RSM_REVISE_ENQ = "RRE";
    public static final String QUOTATION_WORKFLOW_ACTION_NSM_REVISE_ENQ = "NRE";
    public static final String QUOTATION_WORKFLOW_ACTION_MH_REVISE_ENQ = "MRE";
    
    public static final String QUOTATION_WORKFLOW_ACTION_SUBMITTONSM = "SNS";
    public static final String QUOTATION_WORKFLOW_ACTION_SUBMITTOMH = "SMH";
    public static final String QUOTATION_WORKFLOW_ACTION_SUBMITTOBH = "SBH";
    
    public static final String QUOTATION_WORKFLOW_ACTION_SUBMITTOQA = "SQA";
    public static final String QUOTATION_WORKFLOW_ACTION_SUBMITTOSCM = "SSC";
    public static final String QUOTATION_WORKFLOW_ACTION_SUBMITTOMFG = "SMP";
    
    public static final String QUOTATION_WORKFLOW_ACTION_DMRETURNED_SUBMITTODM = "EDD";
    public static final String QUOTATION_WORKFLOW_ACTION_DMRETURNED_SUBMITTOBH = "EDB";
    public static final String QUOTATION_WORKFLOW_ACTION_DMRETURNED_SUBMITTOSM = "EDS";
    
    public static final String QUOTATION_WORKFLOW_ACTION_REGRETMTOENQUIRY = "RME";
    
    public static final String QUOTATION_WORKFLOW_NEXTLEVEL_SM = "SM";
    public static final String QUOTATION_WORKFLOW_NEXTLEVEL_DM = "DM";
    public static final String QUOTATION_WORKFLOW_NEXTLEVEL_DE = "DE";
    public static final String QUOTATION_WORKFLOW_NEXTLEVEL_CE = "CE";
    public static final String QUOTATION_WORKFLOW_NEXTLEVEL_RSM = "RSM";
    public static final String QUOTATION_WORKFLOW_NEXTLEVEL_END = "END";
    public static final String QUOTATION_WORKFLOW_NEXTLEVEL_CE_RSM_DM = "CE|RSM|DM";
    public static final String QUOTATION_WORKFLOW_NEXTLEVEL_NSM = "NSM";
    public static final String QUOTATION_WORKFLOW_NEXTLEVEL_MH = "MH";
    public static final String QUOTATION_WORKFLOW_NEXTLEVEL_BH = "BH";
    public static final String QUOTATION_WORKFLOW_NEXTLEVEL_QA = "QA";
    public static final String QUOTATION_WORKFLOW_NEXTLEVEL_SCM = "SCM";
    public static final String QUOTATION_WORKFLOW_NEXTLEVEL_MFG = "MFG";
    
    public static final int QUOTATION_WORKFLOW_STATUS_DRAFT = 1;
    public static final int QUOTATION_WORKFLOW_STATUS_PENDINGDM = 2;
    public static final int QUOTATION_WORKFLOW_STATUS_PENDINGCE = 3;
    public static final int QUOTATION_WORKFLOW_STATUS_PENDINGRSM = 4;
    public static final int QUOTATION_WORKFLOW_STATUS_PENDINGSM = 5;
    public static final int QUOTATION_WORKFLOW_STATUS_RELEASED = 6;
    public static final int QUOTATION_WORKFLOW_STATUS_SUPERSEDED = 7;
    public static final int QUOTATION_WORKFLOW_STATUS_ReturnSM = 8;
    
    public static final int QUOTATION_WORKFLOW_STATUS_PENDINGNSM = 15;
    public static final int QUOTATION_WORKFLOW_STATUS_PENDINGMH = 16;
    
    public static final int QUOTATION_WORKFLOW_STATUS_PENDINGQA = 17;
    public static final int QUOTATION_WORKFLOW_STATUS_PENDINGSCM = 18;
    public static final int QUOTATION_WORKFLOW_STATUS_REGRET_PENDINGSM = 19;
    public static final int QUOTATION_WORKFLOW_STATUS_PENDINGBH = 20;
    public static final int QUOTATION_WORKFLOW_STATUS_PENDINGMFG = 21;
    
    public static final String QUOTATION_WORKFLOW_REVISION_TECHNICAL = "TechRevisionRequested";
    public static final String QUOTATION_WORKFLOW_REVISION_COMMERCIAL = "CommRevisionRequested";
    
    /*
     * Constant related to Admin success page
     */
    public static final String QUOTATION_ADMIN_SUCCESSMESSAGE = "adminSuccessMessage";
    public static final String QUOTATION_ADMIN_FAILUREMESSAGE = "adminFailureMessage";
    public static final String QUOTATION_ADMIN_BACKTOSEARCH_URL = "backToSearchUrl";
    public static final String QUOTATION_ADMIN_BACKTOSEARCH_MSG = "backToSearchMsg";
    public static final String QUOTATION_ADMIN_HEADERTEXT = "headerText";
    public static final String QUOTATION_INSERTION_FAILED="Data Insertion Failed";
    public static final String QUOTATION_INSERTION_SUCCESS="Data Inserted Sucessfully";
    public static final String QUOTATION_UPDATION_FAILED="Data Updation Failed";
    public static final String QUOTATION_UPDATION_SUCCESS="Data Updated Sucessfully";
    public static final String QUOTATION_DELETED_SUCCESS="Data Deleted Sucessfully";
    public static final String QUOTATION_DELETED_FAILED="Data Deletion Failed";
    
    public static final String QUOTATION_REC_ADDSUCC = " has been added successfully.";
    public static final String QUOTATION_REC_UPDATESUCC = " has been updated successfully.";
    public static final String QUOTATION_REC_DELSUCC = " has been deleted successfully.";
    public static final String QUOTATION_REC_DELFAIL = " record could not be deleted, since it is in Use.";
    public static final String QUOTATION_REC_BULKLOADSUCC = " Bulk Load process has been performed successfully.";
    
    public static final String QUOTATION_ADMIN_EXPORTLIST = "alAdminSearchResultsList";
    public static final String QUOTATION_ADMIN_EXPORTTYPE = "exportType";
    
    public static final String QUOTATION_ADMIN_EXPORT_LISTPRICE = "EXPORT_LISTPRICE";
    public static final String QUOTATION_ADMIN_EXPORT_CUSTDISCOUNT = "EXPORT_CUSTDISCOUNT";
    
    /*
     * Constants for all request/session attributes
     */
    public static final String QUOTATION_REQUEST_ENQUIRYID = "enquiryId";
    public static final String QUOTATION_REQUEST_STATUSID ="statusId";
    public static final String QUOTATION_REQUEST_ENQUIRYNUMBER = "enquiryNumber";
    
    public static final String QUOTATION_REQUEST_RATINGID = "ratingId";
    public static final String QUOTATION_REQUEST_ENQUIRYCODE = "enquiryCode";
    
    public static final String QUOTATION_ADMINLOOKUP_SELECTEDOPTION = "selectedOption";
    public static final String QUOTATION_ADMINLOOKUP_TARGETOBJECTS = "targetObjects";
	public static final String QUOTATION_FORWARD_VIEWQUOTATION = "viewquotation";
	public static final String QUOTATION_FORWARD_VIEWRETURNENQUIRY = "viewreturnenquiry";
	public static final String QUOTATION_FORWARD_SMAPPROVE = "viewsmapprove";
	public static final String QUOTATION_FORWARD_RSMAPPROVE = "viewrsmapprove";
	public static final String QUOTATION_FORWARD_NSMAPPROVE = "viewnsmapprove";
	public static final String QUOTATION_FORWARD_MHAPPROVE = "viewmhapprove";
	
	 /*
     * Constants for all Graphs
     * 
     */
   public static final int RBC_QUOTATION_REGION_GRAPH=1;
   public static final int RBC_QUOTATION_SALES_MANAGER_GRAPH=2;
   public static final int RBC_QUOTATION_CUSTOMER_GRAPH=3;
   public static final int RBC_QUOTATION_STATUS_GRAPH=4;
   public static final int RBC_QUOTATION_CUSTOMER_LOCATIONS_GRAPH=5;
   public static final int RBC_QUOTATION_MONTHLY_GRAPH=6;
   
   /*
    * Constants related to admin look ups
    */
   public static final String APP_ADMINLOOKUP_TARGETOBJECTS = "targetObjects";
   public static final String APP_ADMINLOOKUP_SELECTEDOPTION = "selectedOption";
   public static final String APP_ADMINLOOKUP_SELECTEDOPTION1 = "selectedOption1";
   public static final String APP_ADMINLOOKUP_SELECTEDOPTION2 = "selectedOption2";
   public static final String APP_ADMINLOOKUP_TARGET_SALESMANAGER = "salesmanager";;
   public static final String APP_ADMINLOOKUP_TARGET_CUSTOMER = "customer";
   public static final String APP_ADMINLOOKUP_TARGET_TEXT1 = "text1";
   public static final String APP_ADMINLOOKUP_TARGET_TEXT2 = "text2";
   public static final String APP_ADMINLOOKUP_TARGETTYPE_COMBO = "comboBox";
   public static final String APP_ADMINLOOKUP_TARGETTYPE_TEXT = "textData";
   public static final String APP_ADMINLOOKUP_TARGETTYPE_DEFAULT = "comboBox";
   public static final String APP_REQUEST_CANINCLUDE_INACTIVE="canIncludeInactive";
   
   
   /*
    * Constant to store the name of the variable that stores a error message
    */
   public static final String APP_ERRORMESSAGE = "ERRORMESSAGE";
   public static final String APP_FORWARD_FAILURE = "failure";
   
   public static final String QUOTATION_ATTACHMENT_APPLICATION_URL= "RBC_QUOTATION_ATTACHMENT_APPLICATION_URL";
   public static final String QUOTATION_ATTACHMENT_APPLICATIONID = "RBC_QUOTATION_ATTACHMENT_APPLICATIONID";
   public static final String QUOTATION_DMATTACHMENT_APPLICATIONID ="RBC_QUOTATION_UPLOAD_DM_ATTACHMENT_APPID";
   public static final String QUOTATION_ATTACHMENT_FILESIZEUNITS = "RBC_QUOTATION_ATTACHMENT_FILESIZEUNITS";
   
   public static final String QUOTATION_PRICE_PATTERN= "###,###.#";
   public static final int QUOTATION_PRICE_DECIMALCOUNT = 2;
   public static final String QUOTATION_PRICE_CURRENCYFORMAT = "INR";
   
   public static final String QUOTATION_SESSION_ROLE = "sessionrole";
   public static final String QUOTATION_SESSION_ADMIN_FLAG = "adminflag";
   public static final String QUOTATION_SESSION_ADMIN_FLAG_TRUE= "true";
   public static final String QUOTATION_ADMIN_HEADER_INFO="1";
   public static final String QUOTATION_REQ_ROLES = "roles";
   
   public static final String QUOTATION_ROLE_ADMIN ="RBC_QUOTATION_ROLE_ADMIN";
   public static final String QUOTATION_ROLE_OTHERS ="others";
  public static final String QUOTATION_DEFAULT_LOCATION ="QUOTATION_DEFAULT_LOCATION";
   public static final String QUOTATION_TECHNICAL_DEPT="Engg./";
   
   
   public static final String QUOTATION_STRING_HTLT="1";
   public static final String QUOTATION_STRING_POLE="2";
   public static final String QUOTATION_STRING_SCSR="3";
   public static final String QUOTATION_STRING_DUTY="4";
   public static final String QUOTATION_STRING_DEGPRO="5";
   public static final String QUOTATION_STRING_TERMBOX="6";
   public static final String QUOTATION_STRING_INSULATIONCLASS="7";
   public static final String QUOTATION_STRING_MOUNTING="8";
   public static final String QUOTATION_STRING_SHAFTEXT="9";
   public static final String QUOTATION_STRING_ENCLOSURE="10";
   public static final String QUOTATION_STRING_DOR="11";
   public static final String QUOTATION_STRING_APPL="12";
   public static final String QUOTATION_STRING_RATVOLUN="13";
   public static final String QUOTATION_STRING_RATFREQFN="14";
   public static final String QUOTATION_STRING_STARCURISIN="15";
   public static final String QUOTATION_STRING_TEMPRISE="16";
   public static final String QUOTATION_STRING_AMBTEMP="17";
   public static final String QUOTATION_STRING_CLGSYS="18";
   public static final String QUOTATION_STRING_BEARDENDE="19";
   public static final String QUOTATION_STRING_LUBRI="20";
   public static final String QUOTATION_STRING_SOUNDPRES="21";
   public static final String QUOTATION_STRING_VIBCLASS="22";
   public static final String QUOTATION_STRING_POFMTERBOX="23";
   public static final String QUOTATION_STRING_NEUTERBOX="24";
   public static final String QUOTATION_STRING_CABENTRY="25";
   public static final String QUOTATION_STRING_AUXTERBO="26";
   public static final String QUOTATION_STRING_STATORCONN="27";
   public static final String QUOTATION_STRING_NOTERM="28";
   public static final String QUOTATION_STRING_ROTATION="29";
   public static final String QUOTATION_STRING_SPHEATER="30";
   public static final String QUOTATION_STRING_WINDRTD="31";
   public static final String QUOTATION_STRING_BEARINGRTD="32";
   public static final String QUOTATION_MASTERNAME_METHOFSTART="33";
   public static final String QUOTATION_MASTERNAME_SALESSTAGE="34";
   public static final String QUOTATION_ASDM="ASDM";

   
   /**
    * Required Data Added By Egr on 18-Jan-2019
    * 
    */
   public static final String QUOTATION_QEM_HTLT="QEM_HTLT";
   public static final String QUOTATION_QEM_POLE="QEM_POLE";
   public static final String QUOTATION_QEM_TYPE="QEM_TYPE";
   public static final String QUOTATION_QEM_DUTY="QEM_DUTY";
   public static final String QUOTATION_QEM_DEGPRO="QEM_DEGPRO";
   public static final String QUOTATION_QEM_TERMBOX="QEM_TERMBOX";
   public static final String QUOTATION_QEM_INSULATIONCLASS="QEM_INSULATIONCLASS";
   public static final String QUOTATION_QEM_MOUNTING="QEM_MOUNTING";
   public static final String QUOTATION_QEM_SHAFTEXT="QEM_ShaftExt";
   public static final String QUOTATION_QEM_ENCLOSURE="QEM_ENCLOSURE";
   public static final String QUOTATION_QEM_ROTDIRECT="QEM_ROTDIRECTION";
   public static final String QUOTATION_QEM_APPL="QEM_APPLICATION";
   public static final String QUOTATION_QEM_VOLTS="QEM_VOLTS";
   public static final String QUOTATION_QEM_FREQUENCY="QEM_FREQUENCY";
   public static final String QUOTATION_QEM_STRTGCURRENT="QEM_STRTGCURRENT";
   public static final String QUOTATION_QEM_TEMPRISE="QEM_TEMPERATURERISE";
   public static final String QUOTATION_QEM_AMBIENCE="QEM_AMBIENCE";
   public static final String QUOTATION_QEM_CLGSYSTEM="QEM_CLGSYSTEM";
   public static final String QUOTATION_QEM_BEARINGDENDE="QEM_BEARINGDENDE";
   public static final String QUOTATION_QEM_LUBRI="QEM_LUBRICATION";
   public static final String QUOTATION_QEM_NOISEL="QEM_NOISE_LEVEL";
   public static final String QUOTATION_QEM_VIB="QEM_VIBRATION";
   public static final String QUOTATION_QEM_MAINTERMBOX="QEM_MAINTERMBOXPS";
   public static final String QUOTATION_QEM_NEUTRALTERMBOX="QEM_NEUTRALTERMBOX";
   public static final String QUOTATION_QEM_CABLEENTRY="QEM_CABLEENTRY";
   public static final String QUOTATION_QEM_AUXTERMBOX="QEM_AUXTERMBOX";
   public static final String QUOTATION_QEM_STATORCONN="QEM_STATORCONN";
   public static final String QUOTATION_QEM_BOTERMINALSNO="QEM_BOTERMINALSNO";
   public static final String QUOTATION_QEM_ROTATION="QEM_ROTATION";
   public static final String QUOTATION_QEM_SPACEHEATER="QEM_SPACEHEATER";
   public static final String QUOTATION_QEM_WINDINGRTD="QEM_WINDINGRTD";
   public static final String QUOTATION_QEM_BEARINGRTD="QEM_BEARINGRTD";
   public static final String QUOTATION_QEM_METHODOFSTG="QEM_METHODOFSTG";
   public static final String QUOTATION_QEM_SALESSTAGE="QEM_SALESSTAGE";
   
   public static final String QUOTATION_QEM_LT_PRODGROUP="QEM_LT_PRODGROUP";
   public static final String QUOTATION_QEM_LT_PRODLINE="QEM_LT_PRODLINE";
   public static final String QUOTATION_QEM_LT_MOTORTYPE="QEM_LT_MOTORTYPE";
   public static final String QUOTATION_QEM_LT_KW="QEM_LT_KW";
   public static final String QUOTATION_QEM_LT_POLE="QEM_LT_POLE";
   public static final String QUOTATION_QEM_LT_FRAME="QEM_LT_FRAME";
   public static final String QUOTATION_QEM_LT_MOUNTING="QEM_LT_MOUNTING";
   public static final String QUOTATION_QEM_LT_TBPOS="QEM_LT_TBPOS";
   public static final String QUOTATION_QEM_LT_AMBTEMP="QEM_LT_AMBTEMP";
   public static final String QEM_LT_AMBREMTEMP = "QEM_LT_AMBREMTEMP";
   public static final String QUOTATION_QEM_LT_GASGROUP="QEM_LT_GASGROUP";
   public static final String QEM_LT_HAZARD_ZONE = "QEM_LT_HAZARD_ZONE";
   public static final String QEM_LT_DUST_GROUP = "QEM_LT_DUST_GROUP";
   public static final String QUOTATION_QEM_LT_APPL="QEM_LT_APPLICATION";
   public static final String QUOTATION_QEM_LT_VOLT="QEM_LT_VOLT";
   public static final String QUOTATION_QEM_LT_FREQ="QEM_LT_FREQ";
   public static final String QUOTATION_QEM_LT_COMBVAR="QEM_LT_COMBVAR";
   public static final String QUOTATION_QEM_LT_DUTY="QEM_LT_DUTY";
   public static final String QUOTATION_QEM_LT_CDF="QEM_LT_CDF";
   public static final String QUOTATION_QEM_LT_STARTS="QEM_LT_STARTS";
   
   public static final String QEM_LT_FRAME_SUFFIX = "QEM_LT_FRAME_SUFFIX";
   public static final String QEM_LT_STANDARD = "QEM_LT_STANDARD";
   public static final String QEM_LT_RVRA="QEM_LT_RVRA";
   public static final String QEM_LT_WINDING_TREATMENT="QEM_LT_WINDING_TREATMENT";
   public static final String QEM_LT_LEAD="QEM_LT_LEAD";
   public static final String QEM_LT_VFD_TYPE="QEM_LT_VFD_TYPE";
   public static final String QEM_LT_DUAL_SPEED_TYPE="QEM_LT_DUAL_SPEED_TYPE";
   public static final String QEM_LT_SHAFT_MATERIAL="QEM_LT_SHAFT_MATERIAL";
   public static final String QEM_LT_PAINT_TYPE="QEM_LT_PAINT_TYPE";
   public static final String QEM_LT_PAINT_SHADE="QEM_LT_PAINT_SHADE";
   public static final String QEM_LT_PAINT_THICKNESS="QEM_LT_PAINT_THICKNESS";
   public static final String QEM_LT_IP="QEM_LT_IP";
   
   public static final String QEM_LT_INSULATION_CLASS="QEM_LT_INSULATION_CLASS";
   public static final String QEM_LT_TERMINAL_BOX="QEM_LT_TERMINAL_BOX";
   public static final String QEM_LT_SPREADER_BOX="QEM_LT_SPREADER_BOX";
   public static final String QEM_LT_AUX_TERMINAL_BOX="QEM_LT_AUX_TERMINAL_BOX";
   public static final String QEM_LT_SPACE_HEATER="QEM_LT_SPACE_HEATER";
   public static final String QEM_LT_VIBRATION="QEM_LT_VIBRATION";
   public static final String QEM_LT_FLYING_LEAD="QEM_LT_FLYING_LEAD";
   public static final String QEM_LT_METAL_FAN="QEM_LT_METAL_FAN";
   public static final String QEM_LT_SHAFT_GROUND="QEM_LT_SHAFT_GROUND";
   public static final String QEM_LT_FORCED_COOL="QEM_LT_FORCED_COOL";
   
   public static final String QEM_LT_HARDWARE="QEM_LT_HARDWARE";
   public static final String QEM_LT_GLAND_PLATE="QEM_LT_GLAND_PLATE";
   public static final String QEM_LT_DOUBLE_COMPRESS_GLAND="QEM_LT_DOUBLE_COMPRESS_GLAND";
   public static final String QEM_LT_SPM_MOUNT_PROVISION="QEM_LT_SPM_MOUNT_PROVISION";
   public static final String QEM_LT_ADD_NAME_PLATE="QEM_LT_ADD_NAME_PLATE";
   public static final String QEM_LT_ARROW_PLATE_DIRECTION="QEM_LT_ARROW_PLATE_DIRECTION";
   public static final String QEM_LT_RTD="QEM_LT_RTD";
   public static final String QEM_LT_BTD="QEM_LT_BTD";
   public static final String QEM_LT_THERMISTER="QEM_LT_THERMISTER";
   public static final String QEM_LT_CABLE_SEAL_BOX="QEM_LT_CABLE_SEAL_BOX";
   
   public static final String QEM_LT_BEARING_SYSTEM="QEM_LT_BEARING_SYSTEM";
   public static final String QEM_LT_BEARING_NDE="QEM_LT_BEARING_NDE";
   public static final String QEM_LT_BEARING_DE="QEM_LT_BEARING_DE";
   public static final String QEM_LT_WITNESS_ROUTINE="QEM_LT_WITNESS_ROUTINE";
   public static final String QEM_LT_ADDITIONAL_TEST="QEM_LT_ADDITIONAL_TEST";
   public static final String QEM_LT_TYPE_TEST="QEM_LT_TYPE_TEST";
   public static final String QEM_LT_ULCE="QEM_LT_ULCE";
   public static final String QEM_LT_QAP = "QEM_LT_QAP";
   public static final String QEM_LT_SEAWORTHY_PACKING="QEM_LT_SEAWORTHY_PACKING";
   public static final String QEM_LT_WARRANTY="QEM_LT_WARRANTY";
   public static final String QEM_LT_SPARES = "QEM_LT_SPARES";
   
   public static final String QEM_LT_DELIVERYTYPE = "QEM_LT_DELIVERYTYPE";
   public static final String QEM_LT_PAYMENTTERMS = "QEM_LT_PAYMENTTERMS";
   public static final String QEM_LT_PERCENT_NETORDERVALUE = "QEM_LT_PERCENT_NETORDERVALUE";
   public static final String QEM_LT_PAYMENTDAYS = "QEM_LT_PAYMENTDAYS";
   public static final String QEM_LT_PAYABLETERMS = "QEM_LT_PAYABLETERMS";
   public static final String QEM_LT_INSTRUMENT= "QEM_LT_INSTRUMENT";
   public static final String QEM_LT_WARRANTYMONTHS = "QEM_LT_WARRANTYMONTHS";
   public static final String QEM_LT_ORDERCOMPLETELEADTIME = "QEM_LT_ORDERCOMPLETELEADTIME";
   public static final String QEM_LT_GST = "QEM_LT_GST";
   public static final String QEM_LT_PACKAGING = "QEM_LT_PACKAGING";
   public static final String QEM_LT_LIQUIDATEDDAMAGES = "QEM_LT_LIQUIDATEDDAMAGES";
   public static final String QEM_LT_DELIVERY_LOT = "QEM_LT_DELIVERY_LOT";
   public static final String QEM_LT_DELIVERY_DAMAGES = "QEM_LT_DELIVERY_DAMAGES";
   public static final String QEM_LT_OFFERVALIDITY = "QEM_LT_OFFERVALIDITY";
   public static final String QEM_LT_PRICEVALIDITY = "QEM_LT_PRICEVALIDITY";
   
   public static final String QEM_LT_VOLT_VARIATION = "QEM_LT_VOLT_VARIATION";
   public static final String QEM_LT_FREQ_VARIATION = "QEM_LT_FREQ_VARIATION";
   public static final String QEM_LT_START_DOL_CURRENT = "QEM_LT_START_DOL_CURRENT";
   public static final String QEM_LT_WINDING_CONFIG = "QEM_LT_WINDING_CONFIG";
   public static final String QEM_LT_METHOD_OF_STARTING = "QEM_LT_METHOD_OF_STARTING";
   public static final String QEM_LT_WINDING_WIRE = "QEM_LT_WINDING_WIRE";
   public static final String QEM_LT_VFD_SPEEDRANGE_MIN = "QEM_LT_VFD_SPEEDRANGE_MIN";
   public static final String QEM_LT_VFD_SPEEDRANGE_MAX = "QEM_LT_VFD_SPEEDRANGE_MAX";
   public static final String QEM_LT_OVERLOADING_DUTY = "QEM_LT_OVERLOADING_DUTY";
   public static final String QEM_LT_CONSTANT_EFF_RANGE = "QEM_LT_CONSTANT_EFF_RANGE";
   public static final String QEM_LT_SHAFT_TYPE = "QEM_LT_SHAFT_TYPE";
   public static final String QEM_LT_DIR_OF_ROTATION = "QEM_LT_DIR_OF_ROTATION";
   public static final String QEM_LT_METHOD_OF_COUPLING = "QEM_LT_METHOD_OF_COUPLING";
   public static final String QEM_LT_TECHO_MOUNTING = "QEM_LT_TECHO_MOUNTING";
   public static final String QEM_LT_DATA_SHEET = "QEM_LT_DATA_SHEET";
   public static final String QEM_LT_NON_STD_VOLTAGE = "QEM_LT_NON_STD_VOLTAGE";
   public static final String QEM_LT_HAZARD_AREA = "QEM_LT_HAZARD_AREA";
   public static final String QEM_LT_HAZARD_AREA_TYPE = "QEM_LT_HAZARD_AREA_TYPE";
   public static final String QEM_LT_MFG_LOCATION = "QEM_LT_MFG_LOCATION";
   public static final String QEM_LT_SERVICE_FACTOR = "QEM_LT_SERVICE_FACTOR";
   public static final String QEM_LT_STANDARD_ROTATION = "QEM_LT_STANDARD_ROTATION";
   public static final String QEM_LT_TEMP_RISE = "QEM_LT_TEMP_RISE";
   public static final String QEM_LT_NOISE_LEVEL = "QEM_LT_NOISE_LEVEL";
   public static final String QEM_LT_PERF_CURVES = "QEM_LT_PERF_CURVES";
   public static final String QEM_LT_CABLE_SIZE = "QEM_LT_CABLE_SIZE";
   public static final String QEM_LT_NO_OF_RUNS = "QEM_LT_NO_OF_RUNS";
   public static final String QEM_LT_NO_OF_CORES = "QEM_LT_NO_OF_CORES";
   public static final String QEM_LT_CROSSSECTION_AREA = "QEM_LT_CROSSSECTION_AREA";
   public static final String QEM_LT_CONDUCTOR_MATERIAL = "QEM_LT_CONDUCTOR_MATERIAL";
   public static final String QEM_LT_TEMPERATURE_CLASS = "QEM_LT_TEMPERATURE_CLASS";
   public static final String QEM_LT_EFFICIENCY_CLASS = "QEM_LT_EFFICIENCY_CLASS";
   public static final String QEM_LT_TYPE_OF_GREASE = "QEM_LT_TYPE_OF_GREASE";
   public static final String QEM_LT_MOTOR_GA = "QEM_LT_MOTOR_GA";
   public static final String QEM_LT_TBOX_GA = "QEM_LT_TBOX_GA";
   public static final String QEM_LT_BROUGHTOUTTERMINALS = "QEM_LT_BROUGHTOUTTERMINALS";
   public static final String QEM_LT_MAIN_TB = "QEM_LT_MAIN_TB";
   public static final String QEM_LT_CABLE_ENTRY = "QEM_LT_CABLE_ENTRY";
   public static final String QEM_LT_LUBRICATION = "QEM_LT_LUBRICATION";
   
   public static final String QEM_LT_DEFAULTS = "QEM_LT_DEFAULTS";
   
   public static final String QEM_LT_ABONDENT_REASON = "QEM_LT_ABONDENT_REASON";
   public static final String QEM_LT_LOSS_REASON = "QEM_LT_LOSS_REASON";
   public static final String QEM_LT_LOSS_COMPETITOR = "QEM_LT_LOSS_COMPETITOR";
   public static final String QEM_LT_WON_POOPTIONS = "QEM_LT_WON_POOPTIONS";
   public static final String QEM_LT_WON_REASON = "QEM_LT_WON_REASON";
   
   public static final String QPDF_LT_PRODLINES = "QPDF_LT_PRODLINES";
   public static final String QPDF_LT_REMARKSFIELD = "QPDF_LT_REMARKSFIELD";
   
   public static final String QUOTATION_ZERO="0";
   public static final  String QUOTATION_ONE="1";
   

   public static final int QUOTATION_LITERAL_MINUSONE=-1;
   public static final int QUOTATION_LITERAL_ZERO=0;
   public static final int QUOTATION_LITERAL_ONE=1;
   public static final int QUOTATION_LITERAL_TWO=2;
   public static final int QUOTATION_LITERAL_THREE=3;
   public static final double QUOTATION_DOUBLE_ZERO=0.0;
   
   public static final String QUOTATION_STRING_DOUBLEZERO="0.00";
   public static final String QUOTATION_STRING_ZERO="0";
   public static final String QUOTATION_PERIOD=".";
   public static final String QUOTATION_STRING_DOUBLEDIGZERO="00";
   public static final String QUOTATION_STRING_ONE="1";
   public static final String QUOTATION_STRING_TWO="2";
   public static final String QUOTATION_STRING_THREE="3";
   public static final String QUOTATION_STRING_FOUR="4";
   public static final String QUOTATION_STRING_FIVE="5";
   public static final String QUOTATION_STRING_SIX="6";
   public static final String QUOTATION_STRING_ELEVEN="11";
   public static final String QUOTATION_STRING_QUOTATION="Quotation";
   public static final String QUOTATION_STRING_HTML="html";
   public static final String QUOTATION_STRING_IMAGES="images";
   public static final String QUOTATION_MELOGOIMG="melogo.jpg";
   public static final String QUOTATION_MELOGO_NEW_IMG="melogo_new.jpg";
   public static final String QUOTATION_REGALLOGOIMG="REGAL_Logo_Black.jpg";
   public static final String QUOTATION_REGALLOGO_NEW_IMG="regal_logo_new.jpg";
   public static final String QUOTATION_R1="-R1";
   
   
   
  public static final String QUOTATION_MASTER_EXISTS="The Master data already exsits in database ";
   public static final String QUOTATION_PROB__FECTCHREC=" A problem occurred while retrieving records Please try again.";
   public static final String QUOTATION_PROB_COPYREC=" Problem occurred while copying enquiry details";
   public static final String QUOTATION_MASTER="Master";
   public static final String QUOTATION_EDITMASTERDATA="editMastersData";
   public static final String QUOTATION_ADDMASTERDATA="addMastersData";
   public static final String QUOTATION_EDITMASTER="editMaster";
   public static final String QUOTATION_ADDMASTER="addMaster";

   public static final String QUOTATION_SPAGE="sPage";
   public static final String QUOTATION_SMSG="sMsg";
   public static final String QUOTATION_CUR_NEGINSEARCH="Current Page is negative in search method...";
   public static final String QUOTATION_SESSIONLIST="sessionList";
   public static final String QUOTATION_SALESMGRDTO="oSalesManagerDto";
   public static final String QUOTATION_EMPTY="";
   public static final String QUOTATION_UPDATE_FAILED=" Updation Failed!";
   public static final String QUOTATION_INSERT_FAILED=" Insertion Failed!";
   public static final String QUOTATION_INVALID_ACTION=" Invalid action";
   public static final String QUOTATION_SMALREDAY_EXISTS=" was already registered as a Sales Manager for the same region.  Hence, record could not be added";
   public static final String QUOTATION_SM_ALREADY_EXISTS=" This Sales Manager has already exist for this Region, Please choose some other Sales Manager name";
   public static final String QUOTATION_STRING_NO="no";
   public static final String QUOTATION_STRING_N="N";
   public static final String QUOTATION_STRING_Y="Y";
   public static final String QUOTATION_STRING_P="P";
   public static final String QUOTATION_STRING_D="D";
   public static final String QUOTATION_CUSTOMER=" Customer";
   public static final String QUOTATION_CUSTOMER_ALREADY_EXISTS=" Customer with same name and location already exists";
   public static final String QUOTATION_EXCEP_IN_INSERT=" Exception In Inserting/Updating Customer Data ";
   public static final String QUOTATION_MSG_TOKHAN="Message for Token handling in Deletion of Customer";
   public static final String QUOTATION_MSG_TOKSM="Message for Token handling in Deletion of Sales Manger";
   public static final String QUOTATION_MSG_TOKLOC="Message for Token handling in Deletion of Location";
   public static final String QUOTATION_MSG_TOKDE="Message for Token handling in updation of Design Engineer";
   public static final String QUOTATION_MSG_TOKDE_DELETION="Message for Token handling in Deletion of Design Engineer";
   public static final String QUOTATION_STRING_A="A";
   public static final String QUOTATION_LOCATION=" Location ";
   public static final String QUOTATION_LOC_EXISTS=" This Location has already exists for this Region, Please choose some other location name ";
   public static final String QUOTATION_DE_EXISTS=" Design engineer already exists. Please choose another design engineer ";
   public static final String QUOTATION_CE="Cheif Executive";
   public static final String QUOTATION_DM="Design Manager";
   public static final String QUOTATION_RSM="Regional Sales Manager";
   public static final String QUOTATION_SM="Sales Manager";
   public static final String QUOTATION_DE="Design Engineer";
   public static final String QUOTATION_REVISION="REVISION";
   public static final String QUOTATION_PROJNAME="Project Name";
   public static final String QUOTATION_DESIGNREF="Design Reference";
   public static final String QUOTATION_CM="Commercial Manager";
   public static final String QUOTATION_DATEOFISSUE="Date Of Issue";
   public static final String QUOTATION_STRING_SPACE="  ";
   public static final String QUOTATION_REQ_PROC="Your request could not be processed due to one of the below reasons. " +
           "<ul>" +
           "<li>Old manager do not have any open enquires to be replaced with the new manager." +
           "<li>Some internal problem occurred during the process." +
           "</ul>";
   public static final String QUOTATION_RET_REPMGRS="Return to replace managers";
   public static final String QUOTATION_RET_REPSM="Return To SalesManager";
   public static final String QUOTATION_REPMGR_EXIST="Update Replace Manager Details already exists";
   public static final String QUOTATION_REPMGR_URL="updatemanagers.do?invoke=updateManagers";
   public static final String QUOTATION_RECINQ="arlMyRecentEnquires";
   public static final String QUOTATION_WAITINQ="arlMyAwaitingApprovalEnquires";
   public static final String QUOTATION_RELEINQ="arlMyRecentReleasedEnquires";
   public static final String QUOTATION_USERDTO="oUserDto";
   
   public static final String QUOTATION_ENQUIRYDTO="oEnquiryDto";
   public static final String QUOTATION_CUSTDTO="oCustomerDto";
   public static final String QUOTATION_LOCDTO="oLocationDto";
   public static final String QUOTATION_TDDTO="oTechnicalDataDto";
   
   
   public static final String QUOTATION_LOWER_INQUIRYDTO="enquiryDto";
   public static final String QUOTATION_ATTDTO="oAttachmentDto";
   public static final String QUOTATION_USERSSO="userSSO";
   
   
   public static final String QUOTATION_MSG_UNAUTH="You are not authorized to view this page";
   
   public static final String QUOTATION_INSERT="INSERT";
   public static final String QUOTATION_VIEWSUBMIT="viewSubmit";
   public static final String QUOTATION_SUB_SUCES="Submitted Successfully !";
   
   public static final String QUOTATION_RET_SUCES="Returned Successfully!";
   public static final String QUOTATION_PENDING_DESIGNOFFICE="Pending - Design Office";
   public static final String QUOTATION_PENDING_RSM="Pending - RSM";

   
   
   public static final String QUOTATION_REASSIG_SUCES="Re-assigned Successfully !";
   public static final String QUOTATION_WFS="Workflow Executed Successfully";
   public static final String QUOTATION_WFSEMAILF="Workflow Executed - But email could not be sent. Please send email manually.";
   public static final String QUOTATION_WFF="Workflow Executed Failed";
   public static final String QUOTATION_WFPC="- - - Workflow process completed - - -";
   public static final String QUOTATION_VIEWLINK="Click Here </a>, To view enquiry";
   public static final String QUOTATION_VIEWLINKS="Click here</a>, to view all your enquiries.";
   
   public static final String QUOTATION_PLACELINKS="Click Here </a>, To place another enquiry";
   
   public static final String QUOTATION_RATINGUS="Is Validated Column In RFQ_RATING_DETAILS is Updated Sucessfully";
   public static final String QUOTATION_RATINGUF="Is Validated Column In RFQ_RATING_DETAILS is Updation Failed";
   public static final String QUOTATION_RATINGUC="- - - Is Validated Column In RFQ_RATING_DETAILS Updation process completed - - -";
   
   public static final String QUOTATION_ATTACHMENT1="attachmentMessage1";
   public static final String QUOTATION_ATTACHMENT2="attachmentMessage2";
   public static final String QUOTATION_ATTACHMENT3="attachmentMessage3";
   public static final String QUOTATION_ATTACHMENT4="attachmentMessage4";
   public static final String QUOTATION_ATTACHMENT5="attachmentMessage5";
   public static final String QUOTATION_ATTACHMENT6="attachmentMessage6";
   public static final String QUOTATION_ATTACHMENT7="attachmentMessage7";
   public static final String QUOTATION_ATTACHMENT8="attachmentMessage8";
   public static final String QUOTATION_ATTACHMENT9="attachmentMessage9";
   public static final String QUOTATION_ATTACHMENT10="attachmentMessage10";
   
   public static final String QUOTATION_DRAFT="DRAFT";
   public static final String QUOTATION_SUBMIT="SUBMIT";
   public static final String QUOTATION_RETURN="RETURN";
   public static final String QUOTATION_EDIT="edit";
   public static final String QUOTATION_RETURNDM="RETURNDM";
   public static final String QUOTATION_SAVEONLY="SAVEONLY";
   public static final String QUOTATION_NEW="NEW";
   public static final String QUOTATION_DMRETURN="DM_RETURN";
   public static final String QUOTATION_NEXTRATING="NEXTRATING";
   public static final String QUOTATION_REASSIGN="reassign";
   public static final String QUOTATION_CONSO_VIEW="consolidatedView";
   public static final String QUOTATION_NRM_USER="normalUser";
   public static final String QUOTATION_ADDNEWRATING="ADDNEWRATING";
   public static final String QUOTATION_COPYPREVIOUSRATING="COPYPREVIOUSRATING";
   public static final String QUOTATION_REMOVERATING="REMOVERATING";
   public static final String QUOTATION_REFERDESIGN="REFERDESIGN";
   
   
   public static final String QUOTATION_TRACKNO= "Your enquiry has been submitted successfully.<br>  Your enquiry tracking# : ";
   
   public static final String QUOTATION_ER_INSERT="An Error ocurred while inserting records, please try again!";
   public static final String QUOTATION_ER_DELETE="An Error ocurred while deleting records, please try again!";
   public static final String QUOTATION_CRET_ROWS="Enquiry record could not be retrieved";
   
   public static final String QUOTATION_RAT_NOTDEL="Rating record could be deleted";
   public static final String QUOTATION_ISSALESMGR="isSalesManager";
   public static final String QUOTATION_STRING_TRUE="true";
   public static final String QUOTATION_STRING_FALSE="false";
   public static final String QUOTATION_VIEWCINQ="viewCreateEnquiry";
   public static final String QUOTATION_CINQ="createEnquiry";
   public static final String QUOTATION_COPYINQ="copyEnquiry";
   public static final String QUOTATION_DELINQ="deleteEnquiry";
   public static final String QUOTATION_VIEWOFF="viewOfferData";
   public static final String QUOTATION_VIEWOFFDATA="viewofferdata";
   public static final String QUOTATION_VIEWOFFDATA2="viewofferdata2";
   public static final String QUOTATION_UVIEWOFFDATA="OFFERDATA2";
   public static final String QUOTATION_VIEWOFFDATA1="OFFERDATA";
   public static final String QUOTATION_REFACOFF_DATA="refactorofferdata";
   public static final String QUOTATION_ESTOFFDATA="estimateOfferData";
   
   public static final String QUOTATION_MTHD_SUBMITTORSM = "submitToRSM";
   public static final String QUOTATION_MTHD_APPROVEBYRSM = "approveByRSM";
   public static final String QUOTATION_MTHD_SUBMITTONSM = "submitToNSM";
   public static final String QUOTATION_MTHD_APPROVEBYNSM = "approveByNSM";
   public static final String QUOTATION_MTHD_SUBMITTOMH = "submitToMH";
   public static final String QUOTATION_MTHD_APPROVEBYMH = "approveByMH";
   
   
   public static final String QUOTATION_UPDRATING="updaterating";
   
   public static final String QUOTATION_HIDEROWIND="hidRowIndex";
   
   
   
   public static final String QUOTATION_RATING1="Rating 1";
   public static final String QUOTATION_RATING="Rating ";
   public static final String QUOTATION_STRING_COMMA=",";
   public static final String QUOTATION_NONE="NONE";
   public static final String QUOTATION_SNONE="None";
   /*public static final String QUOTATION_ENQUIRY_CLICKHERE="Click Here </a>,";*/
   public static final String QUOTATION_DMCOMM="dmcomment";
   public static final String QUOTATION_DECOMM="decomment";
   public static final String QUOTATION_ATTACHLIST="attachmentList";
   public static final String QUOTATION_DMATTACHLIST="dmattachmentList";
   public static final String QUOTATION_APPID="applicationId";
   public static final String QUOTATION_GETINQDET="getEnquiryDetails";
   public static final String QUOTATION_COPY="copy";
   public static final String QUOTATION_TYPE="type";
   public static final String QUOTATION_MANAGER="manager";
   public static final String QUOTATION_ENGINEER="engineer";
   public static final String QUOTATION_SMANAGER="salesManager";
   public static final String QUOTATION_QUOTDATA="quotationData";
   public static final String QUOTATION_PAGEHEADTYPE="pageHeadingType";
   
   
   
   public static final String QUOTATION_ESTIMATE="estimate";
   public static final String QUOTATION_VIEWLINK_CAP="viewEnquiry";
   public static final String QUOTATION_RETURN_ENQUIRY="returnenquiry";
   public static final String QUOTATION_OPERATION_TYPE="operationType";
   public static final String QUOTATION_RFQINQ="RFQ";
   
   
   
   
   
   
   public static final String QUOTATION_RETURNFROMDM="returnfromdm";
   
   public static final String QUOTATION_DELRATING="deleteRating";
   
   public static final String QUOTATION_DELINQSUC="The Enquiry Successfully Deleted!";
   
   public static final String QUOTATION_OFFERDASUC="Offer data has been submitted successfully.";
   
   public static final String QUOTATION_OFFERDATARSUC="Offer data has been returned successfully to Sales Manager.";
   public static final String QUOTATION_OFFERDATARDMSUC="Offer data has been returned successfully to Design Manager.";
   public static final String QUOTATION_OFFERDATARDESUC="Offer data has been returned successfully to Design Engineer.";
   public static final String QUOTATION_OFFERDATARCESUC="Offer data has been returned successfully to Chief Executive.";
   public static final String QUOTATION_OFFERDATARSMSUC="Offer data has been returned successfully to Regional Sales Manager.";
   public static final String QUOTATION_DELCONF="Delete Confirmation";
   public static final String QUOTATION_DISPCPERS="displaycontactperson";
   public static final String QUOTATION_ASSIGNED_SUCESS="Enquiry has been successfully re-assigned to: ";
   public static final String QUOTATION_INVALID_RATINGID="Invalid RatingId";
   public static final String QUOTATION_ERROR_UPDATING="Error occurred while updating the data, please try again!";
   public static final String QUOTATION_ERROR_DELETING="Error occurred while deleting the data, please try again!";
   
   public static final String QUOTATION_QUANTITY_MULTIPLIER="quantityMultiplier_";
   public static final String QUOTATION_ROPN_MULTIPLIER="ratedOutputPNMulitplier_";
   public static final String QUOTATION_TYPE_MULTIPLIER="typeMultiplier_";
   public static final String QUOTATION_AREAC_MULTIPLIER="areaCMultiplier_";
   public static final String QUOTATION_FRAME_MULTIPLIER="frameMultiplier_";
   public static final String QUOTATION_APPL_MULTIPLIER="applicationMultiplier_";
   public static final String QUOTATION_FACTOR_MULTIPLIER="factorMultiplier_";
   public static final String QUOTATION_NETMC_MULTIPLIER="netmcMultiplier_";
   public static final String QUOTATION_MARKUP_MULTIPLIER="markupMultiplier_";
   public static final String QUOTATION_TRANSFERCOST_MULTIPLIER="transferCMultiplier_";
   
   
   
   
   
   public static final String QUOTATION_MCORNSRATIO_MULTIPLIER="mcornspratioMultiplier_";
   public static final String QUOTATION_TRANSPORTATION_MULTIPLIER="transportationperunit_rsm_";
   public static final String QUOTATION_WARRANTY_MULTIPLIER="warrantyperunit_rsm_";
   public static final String QUOTATION_UNITPRICE_MULTIPLIER="unitpriceMultiplier_rsm_";
   public static final String QUOTATION_TOTALPRICE_MULTIPLIER="totalpriceMultiplier_rsm_";
   
   public static final String QUOTAION_Q_CE="QUOTATION_CE";
   public static final String QUOTAION_Q_DM="QUOTATION_DM";
   public static final String QUOTAION_Q_RSM="QUOTATION_RSM";
   
   public static final String QUOTATION_UNITPRICE="addOnTypeText";
   public static final String QUOTATION_ADDONQUANTITY="addOnQuantity";
   public static final String QUOTATION_ADDONUNITPRICE="addOnUnitPrice";
   public static final String QUOTATION_QUOTESUC="Quotation has been successfully sent to Sales Engineer / Manager.";
   
   public static final String QUOTAION_EMAILTO_CUSTOMER="emailQuoteToCustomer";
   
   public static final String QUOTAION_VIEWRETURNENQUIRY="viewReturnEnquiry";
   
   public static final String QUOTAION_DOWNLOADATTACHMENT="downloadAttachment";
   
   public static final String QUOTAION_SETROLES="setRoles";
   
   public static final String QUOTAION_ATTACHMENTID="attachmentId";
   public static final String QUOTAION_FILENAME="fileName";
   public static final String QUOTAION_FILETYPE="fileType";
   public static final String QUOTAION_FILEPATH_SEPARATOR="/";
   
   public static final String QUOTAION_SELE_SSOID="QUOTATION_SELECTED_SSOID";
   
   public static final String QUOTAION_SELE_HEADINF="QUOTATION_SELECTED_HEADERINFO";
  
   public static final String QUOTAION_RBCSECQUOTADMIN="RBC_Security_QUOTATION_Administrators";
   
   public static final String QUOTAION_MSG="msg";
   
   public static final String QUOTAION_MSOUTLOOKMSG="MSOUTLOOK.msg";
   
   public static final String QUOTATION_FILEUPLOADF="File upload failed";
   
   public static final String QUOTATION_VALREQACT="validateRequestAction";
   
   public static final String QUOTATION_PDF="Quotation_";
   public static final String QUOTATION_PDFEXT= ".pdf";
   public static final String QUOTATION_ZIP_EXT= ".zip";
   
   public static final String QUOTATION_SAMPLE= "sample";
   
   public static final String QUOTATION_STRING_UNDERSC= "_";
   public static final String QUOTATION_SPLITQSYMBOL="?";
   public static final String QUOTATION_GRSYMBOL=">";
   public static final String QUOTATION_FORWARDSLASHSYMBOL="\\";
   public static final String QUOTATION_STRING_PERCENTAGE="%";
   public static final String QUOTATION_STRING_PIPE="|";
   
   
   public static final String QUOTATION_VIEWSEAR_RES= "viewSearchEnquiryResults";
   
   public static final String QUOTATION_CREATERATING="createRating";
   
   public static final String QUOTATION_BUILDINS_R="buildRatingInsertQuery";
   
   public static final String QUOTATION_RFQ_CUSTOMER="RFQ_CUSTOMER";
   
   public static final String QUOTATION_QCU_CUSID="QCU_CUSTOMERID";
   
   public static final String QUOTATION_QCU_NAME="QCU_NAME";
   
   public static final String QUOTATION_QEM_CUSTTYPE="QEM_CUSTOMERTYPE";
   
   public static final String QUOTATION_QEM_INDUSTRY="QEM_INDUSTRY";
   
   public static final String QUOTATION_QEM_APPROVAL="QEM_APPROVAL";
   
   public static final String QUOTATION_QEM_TARGTD="QEM_TARGETED";
   
   public static final String QUOTATION_QEM_COMPR="QEM_COMPETITOR";
   
   public static final String QUOTATION_QEM_ETYPE="QEM_ENQUIRYTYPE";
   
   public static final String QUOTATION_QEM_SSTAGE="QEM_SALESSTAGE";
   
   public static final String QUOTATION_QEM_DTYPE="QEM_DELIVERYTYPE";
   
   public static final String QUOTATION_QEM_SPA="QEM_SPA";
   
   public static final String QUOTATION_QEM_WIN_COM="QEM_WINNING_COMPETITOR";
   
   public static final String QUOTATION_QEM_RE_WL="QEM_REASON_WINLOSS";
   

   
   public static final String QUOTATION_QEM_WINCH="QEM_WINCHANCE";
   
   
   
   
    public static final String QUOTATION_QEM_AREACL="QEM_AREACLASSIFICATION";
    public static final String QUOTATION_QEM_SERFA="QEM_SERVICEFACTOR";
    
    
	
	public static final String QUOTATION_QEM_LUB="QEM_LUBRICATION";
	public static final String QUOTATION_QEM_BAL="QEM_BALANCING";
	public static final String QUOTATION_QEM_VIBR="QEM_VIBRATION";
	public static final String QUOTATION_QEM_MTBPS="QEM_MAINTERMBOXPS";
	 
    public static final String QUOTATION_QEM_MINSTVOLT="QEM_MINSTARTVOLT";
    
    public static final String QUOTATION_QEM_BEARME="QEM_BEARINGTHERMODT";
    public static final String QUOTATION_QEM_YESNO="QEM_YESNO";
    
    public static final String QUOTATION_QEM_ZONE="QEM_ZONE";
    public static final String QUOTATION_QEM_GASGROUP="QEM_GASGROUP";
    
    public static final String QUOTATION_BUL_TECHOFF="buildTechnicalOfferQuery";
    
    public static final String QUOTATION_GET_OPTATTR="getOptionsbyAttributeList";
    public static final String QUOTATION_BREAK="<br>";
    public static final String QUOTATION_FINALRLIST="finalRatingList";
    
    public static final String QUOTATION_RMAXSIZE="ratingMaxSize";
    
    public static final String QUOTATION_ADMAXSIZE="addonMaxSize";
    
    public static final String QUOTATION_RALIST="ratingList_";
    
    public static final String QUOTATION_NETPRICE="netPrice_";
    public static final String QUOTATION_ADDONNETPRICE="addonnetPrice_";
    
    public static final String QUOTATION_RETURNCINQ="returnEnquiry";
    
    public static final String QUOTATION_EMAILCUSTCONFIRM="Email To Customer Confirmation";
    
    public static final String QUOTATION_PRE_STA="previousStatus";
    public static final String QUOTATION_NEX_STA="nextStatus";
    
    public static final String QUOTATION_INDEX="index";
    
    public static final String QUOTATION_VALID="valid";
    
    public static final String QUOTATION_NULL=null;
    public static final boolean QUOTATION_FALSE=false;
    public static final boolean QUOTATION_TRUE=true;
    
    public static final String QUOTATION_UPDATEMGRS="updateManagers";
    public static final String QUOTATION_SAVE_VALUES="saveValues";
    public static final String QUOTATION_GETMANAGERVALS="getManagerValues";
    public static final String QUOTATION_GETDELIST="getDesignEngineerList";
   public static final String QUOTATION_GETSMLIST="getSalesManagerList";
    public static final String QUOTATION_GETRSMLIST="getReginolSalesManagerList";
    public static final String QUOTATION_UPDATECEVAL="updateChiefExecutiveValues";
    public static final String QUOTATION_UPDATEDES="updateDesignManagerValue";
    public static final String QUOTATION_UPDATERSMVALUES="updateRegionalSalesManagerValue";
    public static final String QUOTATION_UPDATESMVALUES="updateSalesManagerValue";
    public static final String QUOTATION_REPLACEMGRSWF="replaceManagersWorkflow";
    public static final String QUOTATION_VIEWDE="viewDesignEngineer";
    public static final String QUOTATION_DEDTO="oDesignEngineerDto";

    
    /*This Url Constants. Added By Egr on 28-Jan-2019*/
    public static final String QUOTATION_VIEWCUST_URL="searchcustomer.do?invoke=viewcustomer";
    public static final String QUOTATION_VIEWDE_URL="searchdesignengineer.do?invoke=viewDesignEngineer";
    public static final String QUOTATION_VIEWLOCTION_URL="searchlocation.do?invoke=viewlocationsearch";
    public static final String QUOTATION_VIEWMASTERS_URL="masters.do?invoke=viewSearchMasterList";
    public static final String QUOTATION_SEARCHMASTERS_URL="masters.do?invoke=viewSavedSearchMasterList";
    public static final String QUOTATION_MASTERID_URL="mastersdata.do?invoke=viewSearchMastersData&masterId=";
    public static final String QUOTATION_MASTER_URL="&master";
    public static final String QUOTATION_VIEWSM_URL="searchsalesmanager.do?invoke=viewsalesmanagerSearchResult";
    public static final String QUOTATION_VALIDATEREQ_URL="<a href=createEnquiry.do?invoke=validateRequestAction&enquiryId=";
    public static final String QUOTATION_VIEWINQ_VAREQURL="<a href=viewEnquiry.do?invoke=validateRequestAction&enquiryId=";
    public static final String QUOTATION_VIEWCR_ENQ_URL="<a href=createEnquiry.do?invoke=viewCreateEnquiry>";
    public static final String QUOTATION_GET_ENQDET_URL="/createEnquiry.do?invoke=getEnquiryDetails&type=view&enquiryId=";
    public static final String QUOTATION_GET_NEWENQDETCOPY_URL="/newPlaceAnEnquiry.do?invoke=getEnquiryDetails&copy=Y&type=view&enquiryId=";
    public static final String QUOTATION_GET_ENQDETCOPY_URL="/createEnquiry.do?invoke=getEnquiryDetails&copy=Y&type=view&enquiryId=";
    public static final String QUOTATION_VIEWSR_ENQURL="<a href='searchenquiry.do?invoke=viewSearchEnquiryResults'> Click here </a> To Go Search page";
    public static final String QUOTATION_VIEWSR_FVIEW_URL="<a href=searchenquiry.do?invoke=viewSearchEnquiryResults&dispatch=fromview>";
    public static final String QUOTATION_VIEWENQ_REFACT_URL="/viewEnquiry.do?invoke=viewEnquiry&operationType=refactorofferdata&enquiryId=";
    public static final String QUOTATION_VIEWENQ_EST_URL1="/viewEnquiry.do?invoke=viewEnquiry&operationType=estimateOfferData&enquiryId=";
    public static final String QUOTATION_VIEWENQ_EST_URL2="&returnenquiry=yes";
    public static final String QUOTATION_HOME_URL="/home.do?invoke=home";
    public static final String QUOTATION_RETRUNDM_URL="/createEnquiry.do?invoke=getEnquiryDetails&type=returnfromdm&enquiryId=";
    public static final String QUOTATION_VIEWOFFERD_URL="/createEnquiry.do?invoke=viewOfferData&enquiryId=";
    public static final String QUOTATION_VIEWOFFCONSOL_URL="/viewEnquiry.do?invoke=viewEnquiry&operationType=consolidatedView&enquiryId=";
    public static final String QUOTATION_VIEWENQ_URL="/createEnquiry.do?invoke=viewEnquiry&operationType=quotationData&enquiryId=";
    
    public static final String QUOTATION_SENTMAIL_URL="/createEnquiry.do?invoke=validateRequestAction";
    public static final String QUOTATION_SENTMAIL_INQID_URL="&enquiryId=";
    public static final String QUOTATION_SENTMAIL_STATUSID_URL="&statusId=";
    
    public static final String QUOTATION_EXISCUST="existingCustomer";
    public static final String QUOTATION_LOCID="locid";
    public static final String QUOTATION_LOCATIONID="locationId";
    public static final String QUOTATION_OPERATION="operation";
    
    
    public static final String QUOTATION_ENGID="engineeringId";
    public static final String QUOTATION_ACTION="action";
    public static final String QUOTATION_MASTERNAME="mastername";
    
    
    
  
    
    public static final String QUOTATION_CUSTOMERID="customerId";
    public static final String QUOTATION_MASTERID="masterId";
    public static final String QUOTATION_DISPATCH="dispatch";
    public static final String QUOTATION_EXISMASTER="existingMaster";
    public static final String QUOTATION_EDITMASTERVALUE="editmastervalue";
    public static final String QUOTATION_SFUNCTIONTYPE="sFunctionType";
    
    public static final String QUOTATION_REGIONID="regionIds";
    public static final String QUOTATION_SMID="salesManagerId";
    public static final String QUOTATION_OLDSMSSO="oldsalesManagerSSO";
    public static final String QUOTATION_ITEMNO="Item No.";
    public static final String QUOTATION_TAGNUMBER = "Tag Number#";
    public static final String QUOTATION_RATEDOUTPUT="Rated Output";
    public static final String QUOTATION_UNITS="Units";
    public static final String QUOTATION_VOLTAGE="Voltage";
    public static final String QUOTATION_UNITPRICES="Unit Price";
    public static final String QUOTATION_QUANTITY="Quantity";
    public static final String QUOTATION_TOTALPRICE="Total Price";
    public static final String QUOTATION_ADDONNO="Add-on No.";
    public static final String QUOTATION_ADDONTYPE="Add-on Type";
    public static final String QUOTATION_TOTAL="Total";
    public static final String QUOTATION_AMOUNTINWORDS="In Words";
    public static final String QUOTATION_EXPDELIVERYDATE="Expected Delivery Date";
    public static final String QUOTATION_WARRANTYDATEDISP="Warranty from the date of dispatch";
    public static final String QUOTATION_WARRANTYDATECOMM="Warranty from the date of commissioning";
    public static final String QUOTATION_MONTHS=" Months";
    public static final String QUOTATION_ADVPAYMENT="Advance payment";
    public static final String QUOTATION_PAYMENTTERMS="Payment Terms";
    public static final String QUOTATION_DELTYPE="Delivery Type";
    public static final String QUOTATION_BEFOREDISP="% Before Dispatch";
    public static final String QUOTATION_ADDNEWDE="addNewDesignEngineerPage";
    public static final String QUOTATION_EDITDE="editDesignEngineerPage";
    public static final String QUOTATION_DEID="designEngineerId";
    public static final String QUOTATION_UPDATEDE="updateDesinEngineerValue";
    public static final String QUOTATION_EXTDESSO="existingDesignEngineerSSO";
    public static final String QUOTATION_DELDEVAL="deleteDesignEngineerValues";
    public static final String QUOTATION_EXPDEVAL="exportDesignEngineerSearchResult";
    public static final String QUOTATION_FUNCTIONTYPES="functionType";
    public static final String QUOTAION_GETDESEARC="getDesignEngineerSearchResult";
    public static final String QUOTATION_CREATEDE="createDesignEngineer";
    public static final String QUOTATION_GETDEINFO="getDesignEngineerInfo";
    public static final String QUOTATION_SDEVALUE="saveDesignEngineerVlaue";
    public static final String QUOTATION_ISDEALREADYEXISTS="isDEAlreadyExists";
    public static final String QUOTATION_UPDATEEXISTDE="updateExistDE";
    public static final String QUOTATION_DELETEDE="deleteDesignEngineer";
    public static final String QUOTATION_EXPORTDE="exportDesignEngineerSearchResult";
    public static final String QUOTATION_TECHREQ="Technical Requirement - ";
    public static final String QUOTATION_TOCUSTOMER="Quotation To Customer  ";
    public static final String QUOTATION_RFQ="RFQ # : ";
    public static final String QUOTATION_LINE="___________________________________________________________________________________________________";
    public static final String QUOTATION_DOTED_LINE="---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------";
    public static final String QUOTATION_DOTED_LINE_TECH="----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------";
    public static final String QUOTATION_QNTYREQ="Quantity Required";
    public static final String QUOTATION_APPLICATION="Application";
    public static final String QUOTATION_QTYPE="Type";
    public static final String QUOTATION_ARC="Area Classification";
    public static final String QUOTATION_ZONE="Zone";
    public static final String QUOTATION_GASGP="Gas Group";
    public static final String QUOTATION_ENCLOSURE="Enclosure";
    public static final String QUOTATION_FRAME="Frame";
    public static final String QUOTATION_MOUNTING="Mounting";
    public static final String QUOTATION_RATEDOUPTUPN="Rated output PN";
    public static final String QUOTATION_SERVICEFACTOR="Service Factor";
    public static final String QUOTATION_BKW="BKW";
    public static final String QUOTATION_KW="kW";
    public static final String QUOTATION_DUTY="Duty";
    public static final String QUOTATION_RATEDVOTUN="Rated Voltage UN";
    public static final String QUOTATION_RATEDFREQUN="Rated Frequency fN";
    public static final String QUOTATION_HZ="Hz.";
    public static final String QUOTATION_POLE="Pole";
    public static final String QUOTATION_RATEDSPEEDNN="Rated Speed nN";
    public static final String QUOTATION_RMIN="r/min";
    public static final String QUOTATION_RATEDCURRENTIN="Rated Current IN";
    
    public static final String QUOTATION_STRING_C="C";
    public static final String QUOTATION_NOLOADCURRENT="No-load Current";
    public static final String QUOTATION_STARCURENTISIN="Starting Current Is/IN";
    public static final String QUOTATION_NOMTORQUETN="Nominal torque TN";
    public static final String QUOTATION_NM="Nm";
    public static final String QUOTATION_LOCKEDROTOR="Locked Rotor Torque TS/TN";
    public static final String QUOTATION_MAXIMUMTORQUE="Maximum torque Tmax/TN";
    public static final String QUOTATION_RV="RV (Slip Ring Motor)";
    public static final String QUOTATION_RA="RA (Slip Ring Motor)	";
    public static final String QUOTATION_LOAD="Load characteristics (IEC 60034-2-1:2014)::::::";
    public static final String QUOTATION_LOADPER="Load %";
    public static final String QUOTATION_CURRENTA="Current A";
    public static final String QUOTATION_EFFICIENCY="Efficiency %";
    public static final String QUOTATION_POWERFACTOR="Power Factor";
    public static final String QUOTATION_PLL="PLL determined from residual loss:::: ";
    public static final String QUOTATION_STRING_100="100";
    public static final String QUOTATION_STRING_75="75";
    public static final String QUOTATION_STRING_50="50";
    public static final String QUOTATION_START="START";
    public static final String QUOTATION_STRING_DUTYPOINT="Duty Point";
    public static final String QUOTATION_HOT="Safe Stall Time (Hot)";
    public static final String QUOTATION_S="s";
    public static final String QUOTATION_COLD="Safe Stall Time (Cold)";
    public static final String QUOTATION_STARTTIME_RATEDV="Starting time at rated voltage";
    public static final String QUOTATION_STARTTIME_RATEDV_80="Starting time at 80% of rated voltage";
    public static final String QUOTATION_INSULATION_CLASS="Insulation Class";
    public static final String QUOTATION_TEMPERATURE_RISE="Temperature Rise";
    public static final String QUOTATION_AMBIENT_TEMP="Ambient temperature";
    public static final String QUOTATION_ALTITUDE="Altitude";
    public static final String QUOATION_CUST_NAME="Customer Name";
    public static final String QUOATION_ENDUSER_NAME="End User Name";
    
    public static final String QUOTATION_MASL="m.a.s.l.";
    public static final String QUOTATION_DP="Degree of Protection";
    public static final String QUOTATION_CS="Cooling System";
    public static final String QUOTATION_BDENDE="Bearing DE/NDE";
    public static final String QUOTATION_LUBRICATION="Lubrication";
    public static final String QUOTATION_SOUNDPRESSURE="Sound pressure level(noload) (LP dB(A) 1m)";
    public static final String QUOTATION_DBA="dB(A)";
    public static final String QUOTATION_KGM2="kg-m2";
    public static final String QUOTATION_BALANCING="Balancing";
    public static final String QUOTATION_ISOGRADE="2.5 grade as per ISO 1940";
    public static final String QUOTATION_VIBECLASSS="Vibration class(no load)";
    public static final String QUOTATION_MAINTERM="Position of main terminal box";
    public static final String QUOTATION_NEUTRTBOX="Neutral Terminal Box";
    public static final String QUOTATION_CABLE="Cable Entry";
    public static final String QUOTATION_AUXTERMINAL="Aux Terminal Box";
    public static final String QUOTATION_STATORCONN="Stator connection";
    public static final String QUOTATION_NOTERM="Number of brought out terminals";
    public static final String QUOTATION_ROT="Rotation";
    public static final String QUOTATION_DROT="Direction of rotation";
    public static final String QUOTATION_TOTWGT="Total weight of motor";
    public static final String QUOTATION_KG=" Kg";
    public static final String QUOTATION_SH="Space Heater";
    public static final String QUOTATION_WINDING="Winding RTD`s No. & type";
    public static final String QUOTATION_BEARING="Bearing RTD`s , No. & type";
    public static final String QUOTATION_MINSTART="Minimum Starting Voltage";
    public static final String QUOTATION_METHODSTART="Method of starting";
    public static final String QUOTATION_MAINTERMBOX="Main Term Box";
    public static final String QUOTATION_DIALTYPEBEARING="Dial type bearing";
    public static final String QUOTATION_AIRTHERMO="Air thermometer";
    public static final String QUOTATION_VIBPROB="Vibration probe mounting pads";
    public static final String QUOTATION_ENCODER="Encoder";
    public static final String QUOTATION_SPEEDSWITCH="Speed Switch";
    public static final String QUOTATION_SURGESUPRESSOR="Surge Suppressors";
    public static final String QUOTATION_CURRENTTRANSFORMER="Current Transformers";
    public static final String QUOTATION_VIBRPROBES="Vibration Probes";
    public static final String QUOTATION_SPMNIPPLE="SPM nipple";
    public static final String QUOTATION_KEYPHASOR="Key phasor";
    public static final String QUOTATION_SHAFTEXT="Shaft Extension";
    public static final String QUOTATION_PAINTTYPE="Paint Type";
    public static final String QUOTATION_PAINTCOLOR="Paint Colours";
    public static final String QUOTATION_DEVCLAR="Deviation And Clarification";
    public static final String QUOTATION_COMOFFER="Commercial Offer ";
    public static final String QUOTATION_NOTES="Notes:";
    public static final String QUOTATION_SGTCO="Standard General terms & Conditions";
    public static final String QUOTATION_DEF="DEFINTIONS";
    public static final String QUOTATION_DEF1="In these Conditions of supply";
    public static final String QUOTATION_DEF2="SELLER 	MARATHON ELECTRIC INDIA PVT. LTD. Limited, INDIA is called the Seller.";
    public static final String QUOTATION_DEF3="BUYER 	The order issuing agency for buying the equipment/services against this offer will be called the Buyer.";
    
    public static final String QUOTATION_VALIDITYS="VALIDITY";
    public static final String QUOTATION_VALIDITYS1="Unless otherwise specified, the offer will be valid for a period of 30 days from the issue date of the offer and thereafter it will be subject to Seller";
    public static final String QUOTATION_VALIDITYS2="s confirmation / revision.";
    
    public static final String QUOTATION_SCOPES="SCOPE";
    
    public static final String QUOTATION_SCOPES1="The scope will include Design, Manufacturing and packing of items as quoted in the offer. Item or work not expressly referred to therein shall be charged for separately. Seller will organise for Dispatch as per agreed terms but unloading at site will not be in the Seller";
    public static final String QUOTATION_SCOPES2="s scope.";
    public static final String QUOTATION_PRICEANDBASISAS="PRICE AND TAXES";
    
    public static final String QUOTATION_PRICEANDBASISAS1="Unless otherwise mentioned the prices quoted are Ex-works basis and inclusive of suitable packing. The prices are exclusive of all taxes and duties, which will be extra, at the rates prevailing at the time of supply.";
    public static final String QUOTATION_TAXDUTY="TAXES & DUTIES";
    public static final String QUOTATION_TAXDUTY1="The prices quoted are exclusive of any taxes or duties such as Excise duty, Education Cess, CST, Local VAT, Octroi, Works contract tax, Entry Tax etc. All these taxes and duties shall be charged extra at the rates prevailing at the time of despatch and shall be payable by buyer at actual.  If Form C is not provided, the full CST as applicable shall be charged. Wherever Way Bill/Road Permit is applicable, the same shall be issued by the buyer. ";
    
    public static final String QUOTATION_TAXDUTY2="If the supplies are to EPCG holders, terminal ED shall be reimbursed to seller & the buyer will avail the Cenvat Credit. Seller will not claim the refund from DGFT. Wherever, seller is entitled to any exemptions/concessions on Tax & Duty, it shall be subject to the issue of necessary documentary evidences by buyer as prescribed under the governing provisions of the law.";
    
    public static final String QUOTATION_TAXDUTY3="Further should there be any new levies / duties imposed by the central government / state government / other statutory authorities, the same shall be reimbursed by buyer to seller at actual.";
    
    public static final String QUOTATION_SATVAR="STATUTORY VARIATION";
    public static final String QUOTATION_SATVAR1="The Prices quoted are based on the present rates of various Government Taxes, duties as applicable on the  motors. Should there be any statutory variation in these rates later on, the same will be adjusted extra to Buyer";
    public static final String QUOTATION_SATVAR2="s account.";
    public static final String QUOTATION_FRIN="FREIGHT AND INSURANCE";
    public static final String QUOTATION_FRIN1="If required, Seller can undertake the responsibility of dispatching the equipment by road through their standard transporter upto site as per pre-agreed rates and terms. Any type of civil work for transportation purposes, whatsoever, is excluded from Seller";
    public static final String QUOTATION_FRIN2="s scope.";
    public static final String QUOTATION_FRIN3="Any requirement of transport through water ways is excluded from Seller";
    public static final String QUOTATION_FRIN4="s scope unless specifically confirmed and quoted in the offer.";
    public static final String QUOTATION_PACKING="PACKING AND FORWARDING";
    public static final String QUOTATION_PACKING1="Offered equipment will be dispatched suitably crated/packed as per Sellers factory standard. The fitting/assembling of loose parts/accessories at site is not in Sellers scope.";
    public static final String QUOTATION_TERMSOFPAY="TERMS OF PAYMENT";
    public static final String QUOTATION_TERMSOFPAY1="Unless otherwise mentioned in the enclosed offer following Payment Terms will be applicable.";
    public static final String QUOTATION_TERMSOFPAY2="30% interest free advance along with issue of order. Balance 70% together with full excise duty & sales tax, price variation amount, freight, insurance as well as testing charges, if any, against proforma invoice before despatch of material.";
    public static final String QUOTATION_TERMSOFPAY3="In case of late payment,  the price of each invoice may be increased by 2 % per month on account of interest.";
    public static final String QUOTATION_DELIVERYS="DELIVERY";
    public static final String QUOTATION_DELIVERYS1="The delivery period quoted in the offer is on Ex-Works basis & subject to force majeure conditions mentioned herein. Delivery period will be reckoned from the date of receipt of technically and commercially clear purchase order containing agreed terms and the advance as per the payment term. Date of successful completion of agreed tests at Seller";
    public static final String QUOTATION_DELIVERYS2="s works shall be deemed as the date of delivery for contractual purposes.";
    public static final String QUOTATION_DELIVERYS3="These delivery times could be modified during negotiation taking into account Buyer";
    public static final String QUOTATION_DELIVERYS4="s actual need and the load of the Seller";
    public static final String QUOTATION_DELIVERYS5="s factory.";
    public static final String QUOTATION_FORCEMEASURE="FORCE MAJEURE";
    public static final String QUOTATION_FORCEMEASURE1="Seller  shall  be  under no  liability  if  performance  of contract on their part is prevented or delayed further in whole  or in part due to any of the causes  beyond  their reasonable control such as but not limited to acts of God, acts of Government, acts of public enemy, war hostility, civil commotion, sabotage, fires, flood, explosions, epidemics, strike and lawful lock-out, then provided notice of happening of any such eventuality is given by the affected party to the other party within 10 days from the date of occurrence and cessation of the Force Majeure, the period of Force Majeure shall be excluded accordingly.";
    public static final String QUOTATION_FORCEMEASURE2="If the Force Majeure event(s) continue beyond the period of three months, that parties shall hold consultation to chalk out the further course of action.";
    public static final String QUOTATION_FORCEMEASURE3="Neither party can claim any compensation from the other party on account of Force Majeure.";
    public static final String QUOTATION_STORAGES="STORAGE";
    public static final String QUOTATION_STORAGES1="If the Buyer does not take delivery of goods within thirty (30) days of notification that they are ready for delivery the Seller shall be entitled on behalf of the Buyer to put the goods into storage at the Buyer expenses.";
    public static final String QUOTATION_WARRANTYS="WARRANTY";
    public static final String QUOTATION_WARRANTYS1="The equipment to be supplied in accordance with the technical part of the offer shall be warranted for a period of 12 (twelve) months from the date of commissioning, but in no case whatsoever longer than for a period of 18 (eighteen) months from the date of dispatch.";
    public static final String QUOTATION_WAIT="WEIGHTS AND DIMENSIONS";
    public static final String QUOTATION_WAIT1="All weights and dimensions given in the technical data sheets are tentative. Final weights and dimensions will be given after design finalisation in the event of an order.";
    
    public static final String QUOTATION_TEST="TESTS";
    public static final String QUOTATION_TEST1="Routine tests as per relevant IS as applicable will be conducted on the equipment before dispatch at Seller";
    
    public static final String QUOTATION_TEST2="s works. Type test/ special test, if agreed, will be carried out on first unit of each rating only. All the above tests shall be witnessed at extra cost if agreed at the time of order. For major bought out items, suppliers test certificates shall be furnished, wherever available.";
    
    public static final String QUOTATION_STANDARD="STANDARDS";
    public static final String QUOTATION_STANDARD1="The equipment to be supplied in accordance with the technical part of the offer shall be manufactured and tested in accordance with IS recommendations.";
    
    public static final String QUOTATION_LIMITEDLIABILITY="LIMITATION OF LIABILITY";
    public static final String QUOTATION_LIMITEDLIABILITY1="Notwithstanding anything contained in this AGREEMENT, its Appendices or orders to the contrary, with respect to any and all claims arising out of the performance or non-performance of obligations under this AGREEMENT or purchase orders, whether arising in contract, tort, warranty, strict liability or otherwise, Seller";
    public static final String QUOTATION_LIMITEDLIABILITY2="s liability shall not exceed in the 100% of this order value or payments received against this order whatever is lower.";
     
    public static final String QUOTATION_CONSEQ_LOSS="CONSEQUENTIAL LOSSES";
    
    public static final String QUOTATION_CONSEQ_LOSS1="Seller shall in no event be liable for loss of profit, loss of revenues, loss of use, loss of production, costs of capital or costs connected with interruption of operation, loss of anticipated savings or for any special, indirect or consequential damage or loss of any nature whatsoever.";
    
    
    public static final String QUOTATION_ARB="ARBITRATION";
    
    public static final String QUOTATION_ARB1="All disputes arising in connection with this Agreement / Purchaser Order shall be finally settled and governed by the provisions of Arbitration and Conciliation Act, 1996. The arbitration panel shall consist of three arbitrators, one to be appointed by each Party and the third arbitrator shall be appointed by the two appointed arbitrators. The third arbitrator shall serve as a chairman. The award of the arbitral tribunal shall be final and binding on both Parties. The place of arbitration shall be Kolkata.  The proceedings shall be conducted in English language.";
    
    public static final String QUOTATION_LNG="LANGUAGE";
    
    public static final String QUOTATION_LNG1="English shall be the language of the Tender and the Contract and will prevail over any translation, if any.";
   
    public static final String QUOTATION_GL="GOVERNING LAW";
    public static final String QUOTATION_GL1="This Offer shall be governed and construed in accordance with the laws of India.";
    public static final String QUOTATION_VQ="VARIATION IN QUANTITY";
    public static final String QUOTATION_VQ1="Any Variation i.e. increase or decrease, in the ordered quantity of equipment";

    public static final String QUOTATION_VQ2="s and components will not be binding on the Seller unless mutually agreed between the Buyer and the Seller in writing.";
    public static final String QUOTATION_TRAN_TITL="TRANSFER OF TITLE";
    public static final String QUOTATION_TRAN_TITL1="Title (legal and beneficial ownership) in the goods shall pass to the Buyer upon loading on board the transportation vehicle at the Seller";
    public static final String QUOTATION_TRAN_TITL2="s works.";
    public static final String QUOTATION_TRAN_TITL3="The Buyer agrees not to transform, capitalise, pledge or resell the equipment sold until the price thereof has been fully paid, except in case of prior express permission granted by the Seller.";
   
    public static final String QUOTATION_CONFID="CONFIDENTIAL TREATMENT AND SECRECY";
    public static final String QUOTATION_CONFID1="Seller shall retain the ownership of it";
    public static final String QUOTATION_CONFID2="s studies, drawings, models and any documents issued/ communicated to Buyer, or of which Buyer may have had knowledge in fulfilment of this contract. Such information and documents may be used only by the Buyer exclusively for execution of the contract. These documents and information shall be treated as confidential and shall not be distributed, published or generally communicated to any third parties without prior express permission in writing by the Seller.";
   
    public static final String QUOTATION_PASSBEFRISK="PASSING OF BENEFIT AND RISK";
    public static final String QUOTATION_PASSBEFRISK1="Risk associated with the equipment shall be transferred upon disptach as per agreed terms. The Buyer shall provide the insurance required for the coverage of said risks immediately upon thereof. ";
    public static final String QUOTATION_PASSBEFRISK2="COMPENSATION DUE TO BUYER'S DEFAULT";
    public static final String QUOTATION_PASSBEFRISK3="S DEFAULT";
    public static final String QUOTATION_PASSBEFRISK4="The Seller shall be suitably compensated for any delay or default not attributable to him during the execution of contract. Such compensation shall include but not limited to Price escalation claims. Increase in Freight, Storage & Port handling charges & re-fixation of contractual delivery etc. ";
   
    public static final String QUOTATION_SUSPEN="SUSPENSION AND TERMINATION";
    public static final String QUOTATION_SUSPEN1="If the Buyer fails to make any payment when due or perform on time any of its other obligations under the contract:";
    public static final String QUOTATION_SUSPEN2="The Seller shall be entitled to suspend performance of the contract until the failure is remedied;";
    public static final String QUOTATION_SUSPEN3="The time for performance of the contract by the Seller shall be extended accordingly ;";
    public static final String QUOTATION_SUSPEN4="Any cost (including financial cost and storage) thereby incurred by the Seller shall be paid by the Buyer.";
    public static final String QUOTATION_SUSPEN5="If performance of the contract is for any reason suspended and such suspension continues for more than 3 months the Seller shall be entitled at any time during that continued suspension by not less than 30 days written notice to terminate the contract forthwith, in which event the provisions of the termination clause below shall apply";
   
    public static final String QUOTATION_TERMINATION="Termination";
    public static final String QUOTATION_TERMINATION1="In case of termination of the contract in whole or in part under any sub-clause of these Conditions of Supply the Buyer shall pay to the Seller without prejudice to any other remedy the Seller may have:";
    public static final String QUOTATION_TERMINATION2="The outstanding balance of the contract value of the goods and services which have been duly delivered or performed ;";
    public static final String QUOTATION_TERMINATION3="The cost incurred by the Seller up to the date of termination in performing work on goods and services which are not then in a deliverable status, plus a reasonable sum to compensate the loss of profit.";

    public static final String QUOTATION_BANKRUPTCYS="BANKRUPTCY";
    public static final String QUOTATION_BANKRUPTCYS1="If the Buyer becomes bankrupt or insolvent or makes any agreement with its creditors compounding debts or if, being a limited company, any proceedings are begun in respect of it applying for the appointment of a liquidator, administrator, receiver or similar official for it or all or any substantial part of its assets or seeking an order or relief against it as debtor or under any law relating to insolvency, readjustment of debt, reorganisation, administration or liquidation, the Seller may at any time by written notice terminate the contract forthwith, in which event the provisions of the termination clause above shall apply.";
    public static final String QUOTATION_ACCEPTANCES="ACCEPTANCE";
    public static final String QUOTATION_ACCEPTANCES1="The  acceptance of the equipment by the Buyer  shall be deemed complete on successful testing of the same at the plant of the Seller";
    public static final String QUOTATION_ACCEPTANCES2="s or it";
    public static final String QUOTATION_ACCEPTANCES3="s agents suppliers or subcontractors , before dispatch to the site.";

    public static final String QUOTATION_LIMITS="LIMIT OF SUPPLY";
    public static final String QUOTATION_LIMITS1="The equipment will be supplied complete with fittings and accessories stated in the offer to MARATHON ELECTRIC MOTORS (INDIA) LTD. standard and generally confirming to Buyer";
    public static final String QUOTATION_LIMITS2="s Specification.";
    
    public static final String QUOTATION_CHSCOPE="CHANGE IN SCOPE";
    public static final String QUOTATION_CHSCOPE1="The prices quoted are in accordance with the scope of work specified in our Technical Scope enclosed. If subsequent to the tender evaluation and placement of order, changes in the specification alter the quoted scope of supply and services, seller reserve the right to re-negotiate the price.";
    public static final String QUOTATION_CHSCOPE2="Any change in our scope of work shall be compensated by buyer. Seller shall maintain a record of such changes. Any increase or decrease in the price shall be mutually discussed and agreed before seller undertakes the manufacturing of the corresponding equipments.";
    public static final String QUOTATION_CHCOMM="CHANNELS FOR COMMUNICATION";
    
    public static final String QUOTATION_CHCOMM1="The Buyer is requested to direct his communication at the following address, in addition to Seller";
    public static final String QUOTATION_CHCOMM2="s  branch office / local office(if any).";
    public static final String QUOTATION_CHCOMM3="MARATHON ELECTRIC MOTORS (INDIA) PVT. LTD.";
    public static final String QUOTATION_CHCOMM4="1, Taratala Road,";
    public static final String QUOTATION_CHCOMM5="s  branch office / local office(if any).";
    public static final String QUOTATION_CHCOMM6="Kolkata  -700024,";
    public static final String QUOTATION_CHCOMM7="West Bengal,Fax  	: 	91-33-2469 6988";
    
    public static final String QUOTATION_CHCOMM8="India,Tel	:	91-33-4403 0529";
    
    public static final String QUOTATION_GENERALS="GENERAL";
    
    public static final String QUOTATION_GENERAL1="These Standard Conditions of supply  will be supplementary to any other terms and conditions mentioned in the enclosed offer. In case of any contradiction the relevant terms and condition of the enclosed offer will prevail.";
    public static final String QUOTATION_GENERAL2="Both the parties are under obligation to maintain secrecy of the documents, data, information, IP right etc. supplied by either of the parties.";
    public static final String QUOTATION_GENERAL3="We trust that the above quotation and the enclosed documentation will be sufficient for the evaluation of our quotation and await with interest to hear further from you. Should you need any further information or assistance, please do not hesitate to contact us.";
    public static final String QUOTATION_MOMENT1="Moment of inertia J =1/4 GD2 (Motor)";
    public static final String QUOTATION_MOMENT2="Moment of inertia J =1/4 GD2 (Load)";
    public static final int QUOTATION_NUMBER1=8226;
    public static final int QUOTATION_NUMBER2=39;
    
    //Indent Data Added By Gangadhar On 15-March-2019
    public static final String QUOTATION_UPDRATINGINDENTWON="updateratingwonindentpage";
    public static final String QUOTATION_UPDRATINGINDENTLOST="updateratinglostindentpage";
    public static final String QUOTATION_UPDRATINGABONDENTINDENTLOST="updateratingabondentindentpage";
    public static final String QUOTATION_WONTOESTIMATE="wontoestimate";
    public static final String QUOTATION_LOSTTOESTIMATE="losttoestimate";
    public static final String QUOTATION_ABONDEMENTTOESTIMATE="abondmenttoestimate";
    public static final String QUOTATION_WON ="WON";
    public static final String QUOTATION_LOST ="LOST";
    public static final String QUOTATION_ABONDED ="ABONDED";
    public static final String QUOTATION_WONPENDINGSALES ="Won-Pending Sales";
    public static final String QUOTATION_LOSTPENDINGSALES ="Lost-Pending Sales";
    public static final String QUOTATION_DMWON ="DMWON";
    public static final String QUOTATION_DEWON ="DEWON";
    public static final String QUOTATION_CMWON ="CMWON";
    public static final String QUOTATION_CEWON ="CEWON";
    public static final String QUOTATION_SMINDENT="SMINDENT";
    public static final String QUOTATION_CE_EXISTS=" Commercial engineer already exists. Please choose another commercial engineer ";
    public static final String QUOTATION_COMMERCIALENGINEER="Commercial Engineer";
    public static final String QUOTATION_DELIVERYLOCATION="deliveryLocation";
    public static final String QUOTATION_DELIVERYADDRESS="deliveryAddress";
    public static final String QUOTATION_NOOFMOTORS="noOfMotors";
    public static final String QUOTATION_SALESMANAGERS_LIST="salesManagersList";
    
    public static final String QUOTATION_WORKFLOW_NEXTLEVEL_CM = "CM";
   
    
    public static final String QUOTATION_WORKFLOW_NEXTLEVEL_WON="WON";
    
    public static final String QUOTATION_WORKFLOW_ACTION_SUBEND ="P,SUB,END";
    public static final String QUOTATION_WORKFLOW_ACTION_PEND ="P,END";
    
    public static final String QUOATION_PAGECHECKING_DMWON="dmwon";
    public static final String QUOATION_PAGECHECKING_DEWON="dewon";
    public static final String QUOATION_PAGECHECKING_CMWON="cmwon";
    public static final String QUOATION_PAGECHECKING_CEWON="cewon";
    public static final String QUOTATION_NA ="NA";
    public static final String QUOTATION_VIEWCE_URL="searchcommercialengineer.do?invoke=viewCommercialEngineer";
    public static final String QUOTATION_WORKFLOW_ACTION_SUBMITTOCOMMERCIALMGR = "SCM";
    public static final String QUOTATION_WORKFLOW_ACTION_LOST = "LST";
    public static final String QUOTATION_WORKFLOW_ACTION_WON = "WON";
    public static final String QUOTATION_WORKFLOW_ACTION_ABONDENT = "ABD";
   public static final int QUOTATION_WORKFLOW_STATUS_WONPENDINGSM = 9;
   public static final int QUOTATION_WORKFLOW_STATUS_WONPENDINGCM = 10;
   public static final int QUOTATION_WORKFLOW_STATUS_WONPENDINGDM = 11;
   public static final int QUOTATION_WORKFLOW_STATUS_WON = 12;
   public static final int QUOTATION_WORKFLOW_STATUS_LOST = 13;
   public static final int QUOTATION_WORKFLOW_STATUS_ABONDENT = 14;
   public static final String QUOTATION_INDENTDATA2="INDENTDATA2";
   public static final String QUOTATION_INDENTDATA1="INDENTDATA";
   public static final String QUOTATION_INTENDATTACHLIST="intendattachmentList";
   public static final String QUOTATION_INTENDATTACHMENT_APPLICATIONID="RBC_QUOTATION_UPLOAD_INTEND_ATTACHMENT_APPID";
   public static final String QUOTATION_FORWARD_INDENTLOST = "indentlost";
   public static final String QUOTATION_FORWARD_INDENTABONDED = "indentabonded";
   public static final String QUOTATION_INDENTPAGEVIEW="IndentPageView";
   public static final String QUOTATION_FORWARD_INDENTPAGEVIEW = "indentview";
   public static final String QUOTATION_RETURN_TYPE="returnType";
   public static final String QUOTATION_VIEWINDENTOFFERD_URL="/createEnquiry.do?invoke=viewIndentOfferData&enquiryId=";
   public static final String QUOTATION_VIEWINDENTDESIGNOFFERD_URL="/createEnquiry.do?invoke=viewIndentDesignOfferData&enquiryId=";
   public static final String QUOTATION_VIEWOFFINDENTPAGE_URL="/viewEnquiry.do?invoke=viewEnquiry&operationType=IndentPageView&enquiryId=";
   public static final String QUOTATION_COMMMANAGER="commercialmanager";
   public static final String QUOTATION_COMMENGINEER="commercialengineer";
   public static final String QUOTATION_COMMERCIALMANAGER="commercialmanager";
   public static final String QUOTATION_FORWARD_VIEWINDENTOFFER ="viewindentoffer";
   public static final String QUOTATION_FORWARD_VIEWDESIGNINDENTOFFER ="viewindentdesignoffer";
   public static final String QUOTATION_WORKFLOW_ACTION_RETURNTOCM = "RCM";
   public static final String QUOTATION_DMINDENTRETURN="Indent data has been returned successfully to Sales Manager.";
   public static final String QUOTATION_OFFERDATARCMSUC="Indent data has been returned successfully to Commercial Manager.";
   public static final String QUOTATION_VIEW="VIEW";
   public static final String QUOTATION_OFFERDATA="OFFERDATA";
   public static final String QUOTATION_INDENTDATA="INDENTDATA";
   public static final String QUOTATION_OFFERDATA2="OFFERDATA2";
   public static final String QUOTATION_INR="INR";
   public static final String QUOTATION_SPACE="\\s";
   public static final String QUOTATION_PDFHEADER="QuotHeader";
   public static final String QUOTATION_PDFFOOTER="QuotFooter";
   public static final String QUOTATION_MOTOR1IMG="quotmotor1.jpg";
   public static final String QUOTATION_MOTOR2IMG="quotmotor2.jpg";
   public static final String QUOTATION_REGARDS="Regards";
   public static final String QUOTATION_MEMI="Marathon Electric Motors India Limited";
   public static final String QUOTATION_EMAIL="Email: ";
   public static final String QUOTATION_CONTACT="Contact: +91 ";
   public static final String QUOTATION_SLASH="\\";
   public static final int QUOTATION_LITERAL_TEN=10;
   public static final int QUOTATION_LITERAL_M350=-350;
   public static final int QUOTATION_LITERAL_M300=-300;
   public static final int QUOTATION_LITERAL_M250=-250;
   public static final int QUOTATION_LITERAL_250=250;
   public static final int QUOTATION_LITERAL_150=150;
   public static final int QUOTATION_LITERAL_50=50;
   public static final int QUOTATION_LITERAL_20=20;
   public static final int QUOTATION_LITERAL_780=780;
   public static final int QUOTATION_LITERAL_485=485;
   public static final int QUOTATION_LITERAL_8=8;
   public static final int QUOTATION_LITERAL_5=5;
   public static final int QUOTATION_LITERAL_2=2;  
   public static final int QUOTATION_LITERAL_15=15;  
   public static final String QUOTATION_YES="YES";
   public static final String QUOTATION_NO="NO";
   public static final int QUOTATION_LITERAL_40=40;
   public static final int QUOTATION_LITERAL_60=60;
   public static final int QUOTATION_LITERAL_80=80;
   public static final int QUOTATION_LITERAL_100=100;
   public static final String QUOTATION_DASHR="-R";
   public static final String QUOTATION_APP_URL="QUOTATION_APP_URL";
   public static final String QUOTATION_APP_NAME = "quotationlttool";
   public static final String QUOTATION_NEWLINE="\n";
   public static final String QUOTATION_TAB="\t";
   public static final String QUOTATION_NBSP="&nbsp;";
   public static final String QUOTATION_QUOTEDFLAG="quotedFlag";
   
   public static final String CECHECK = "(" + QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGCE +")";
   public static final String DMCHECK = "(" +  QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGDM +","+QuotationConstants.QUOTATION_WORKFLOW_STATUS_WONPENDINGDM +")";
   public static final String RSMCHECK = "(" +  QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGRSM +")";
   public static final String SMCHECK = "(" +  QuotationConstants.QUOTATION_WORKFLOW_STATUS_DRAFT+","+QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGSM +","+QuotationConstants.QUOTATION_WORKFLOW_STATUS_ReturnSM +","+QuotationConstants.QUOTATION_WORKFLOW_STATUS_WONPENDINGSM +")";
   public static final String DECHECK = "(" +  QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGDM +")";
   public static final String CMCHECK = "(" +  QuotationConstants.QUOTATION_WORKFLOW_STATUS_WONPENDINGCM +")";
   
   public static final String QUOTATION_TECHNICADATAID="TECHNICAL_DATA_SERIAL_ID";
   public static final String QUOTATION_TECHNICALDATA_URL="technicaldata.do?invoke=viewbulkdata";
   public static final String QUOTATION_TECHNICALDATA=" Technical Data ";
   
   public static final String QUOTATION_CDDTO="oCustomerDiscountDto";
   public static final String QUOTATION_CUSTOMERDISCOUNT_URL="customerdiscount.do?invoke=viewdiscount";
   public static final String QUOTATION_LPDTO="oListPriceDto";
   public static final String QUOTATION_LISTPRICE_URL="listprice.do?invoke=viewListPrice";
   public static final String QUOTATION_SERIAL_ID="serial_id";
   
   public static final String QUOTATION_RFQ_LOCATION="RFQ_LOCATION";
   public static final String QUOTATION_QLO_LOCATIONID="QLO_LOCATIONID";
   public static final String QUOTATION_QLO_LOCATIONNAME="QLO_LOCATIONNAME";

   public static final String QUOTATION_QUANTITY_VALUE = "quantityVal";
   public static final String QUOTATION_MFGLOCATION_VALUE = "mfgLocationVal";
   public static final String QUOTATION_PRODGROUP_VALUE = "prodGroupVal";
   public static final String QUOTATION_PRODLINE_VALUE = "prodLineVal";
   public static final String QUOTATION_KW_VALUE = "kwVal";
   public static final String QUOTATION_FRAME_VALUE = "frameVal";
   public static final String QUOTATION_POLE_VALUE = "poleVal";
   public static final String QUOTATION_MOUNTING_VALUE = "mountingVal";
   public static final String QUOTATION_TBPOSITION_VALUE = "tbPositionVal";
   public static final String QUOTATION_CUSTOMERID_VALUE = "customerIdVal";
   
   public static final String QUOTATION_NONSTDVOLTAGE_VALUE = "nonStdVoltageVal";
   public static final String QUOTATION_WINDINGTREATMENT_VALUE = "windingTreatmentVal";
   public static final String QUOTATION_SHAFTTYPE_VALUE = "shaftTypeVal";
   public static final String QUOTATION_SHAFTMATERIAL_VALUE = "shaftMaterialVal";
   public static final String QUOTATION_SPACEHEATER_VALUE = "spaceHeaterVal";
   public static final String QUOTATION_HARDWARE_VALUE = "hardwareVal";
   public static final String QUOTATION_DOUBLECOMPRESSGLAND_VALUE = "doubleCompressGlandVal";
   
   public static final String QUOTATION_LT_GET_ENQDET_URL="/newPlaceAnEnquiry.do?invoke=getEnquiryDetails&type=view&enquiryId=";
   public static final String QUOTATION_LT_RETRUN_BY_DM_URL="/newPlaceAnEnquiry.do?invoke=getEnquiryDetails&type=edit&enquiryId=";
   public static final String QUOTATION_LT_BHAPPROVE_URL="/newPlaceAnEnquiry.do?invoke=getEnquiryDetails&type=edit&enquiryId=";
   public static final String QUOTATION_LT_MFGAPPROVAL_URL="/newPlaceAnEnquiry.do?invoke=getEnquiryDetails&type=edit&enquiryId=";
   public static final String QUOTATION_LT_VIEWOFFERD_URL="/newMTOEnquiry.do?invoke=viewMTOEnquiryDetails&enquiryId=";
   public static final String QUOTATION_LT_VIEWOFFCONSOL_URL="/newViewEnquiry.do?invoke=viewEnquiry&operationType=consolidatedView&enquiryId=";
   public static final String QUOTATION_LT_VIEWENQ_EST_URL1="/newViewEnquiry.do?invoke=viewEnquiry&operationType=estimateOfferData&enquiryId=";
   public static final String QUOTATION_LT_VIEWENQ_REFACT_URL="/newViewEnquiry.do?invoke=viewEnquiry&operationType=refactorofferdata&enquiryId=";
   public static final String QUOTATION_LT_VIEWENQ_URL="/newPlaceAnEnquiry.do?invoke=viewEnquiry&operationType=quotationData&enquiryId=";
   public static final String QUOTATION_LT_VIEWOFFERQA_URL="/newMTOEnquiry.do?invoke=viewMTOEnquiryDetails&enquiryId=";
   public static final String QUOTATION_LT_VIEWOFFERSCM_URL="/newMTOEnquiry.do?invoke=viewMTOEnquiryDetails&enquiryId=";
   public static final String QUOTATION_LT_GET_REVENQDET_URL="/newPlaceAnEnquiry.do?invoke=getRevisionEnquiryDetails&enquiryId=";
   
   public static final String QUOTATION_LT_VIEWINDENTOFFERD_URL="/newPlaceAnEnquiry.do?invoke=viewIndentOfferData&enquiryId=";
   public static final String QUOTATION_LT_VIEWOFFINDENTPAGE_URL="/newViewEnquiry.do?invoke=viewEnquiry&operationType=IndentPageView&enquiryId=";
   
   public static final String QUOTATION_LT_VALIDATEREQ_URL="<a href=newPlaceAnEnquiry.do?invoke=validateRequestAction&enquiryId=";
   public static final String QUOTATION_LT_VIEWINQ_VAREQURL="<a href=newViewEnquiry.do?invoke=validateRequestAction&enquiryId=";
   public static final String QUOTATION_LT_VIEWCR_ENQ_URL="<a href=newPlaceAnEnquiry.do?invoke=viewCreateEnquiry>";
   
   public static final String QUOTATION_LOWER_NEW_INQUIRYDTO ="newEnquiryDto";
   public static final String QUOTATION_LOWER_NEW_RATINGDTO = "ratingObj";
   
   public static final String QUOTATION_DELIVERY_TERM="Delivery Term";
   public static final String QUOTATION_ORD_COMPLETE_LEAD_TIME="Order Completion Lead Time";
   public static final String QUOTATION_GST = "GST";
   public static final String QUOTATION_PACKAGING = "Packaging";
   public static final String QUOTATION_DELIVERY_LOT="Delivery";
   public static final String QUOTATION_LIQUIDATED_DAMAGES="Liquidated Damages due to delayed deliveries";
   
   public static final String QUOTATION_PDF_PAY_TERMS = "Payment Terms";
   public static final String QUOTATION_PDF_PERCENT_NET_ORD_VALUE = "%age amount of Nett. order value";
   public static final String QUOTATION_PDF_PAY_DAYS = "Payment Days";
   public static final String QUOTATION_PDF_PAYABLE_TERMS = "Payable Terms";
   public static final String QUOTATION_PDF_INSTRUMENT = "Instrument";
   
   public static final String QUOTATION_PDF_PAYMENT_DETAILS = "Payment Details";
   
   public static final String QUOTATION_PDF_ADVANCE_PAYMENT1 = "Advance Payment 1";
   public static final String QUOTATION_PDF_ADVANCE_PAYMENT2 = "Advance Payment 2";
   public static final String QUOTATION_PDF_MAIN_PAYMENT = "Main Payment";
   public static final String QUOTATION_PDF_RETENTION_PAYMENT1 = "Retention Payment 1";
   public static final String QUOTATION_PDF_RETENTION_PAYMENT2 = "Retention Payment 2";
   
   public static final String QUOTATION_PAYMENT_IMMEDIATELY = "Immediately";
   
   public static final String QUOTATION_CUST_CONCERNED_PERSON = "Contact Person";
   public static final String QUOTATION_CUST_ENQ_REFERENCE = "Customer Enquiry Reference";
   public static final String QUOTATION_ENQ_RECEIPT_DATE = "Enquiry Receipt date";
   
   public static final String QUOTATION_PDF_ADDRESSTO = "Dear Sir / Madam, ";
   
   public static final String QUOTATION_ATTACHMENT_COMMPURCHASESPEC="attachmentCommPurchaseSpec";
   
   public static final String QUOTATION_PDF_MODEL_NUM = "Model No. : ";
   
   public static final String QUOTATION_PDF_REMARKS = "Remarks: ";
   public static final String QUOTATION_PDF_ADDL_COMMENTS = "Additional Comments: ";
   public static final String QUOTATION_PDF_NOTES = "Notes: ";
   
   public static final String QUOTATIONLT_MOTOR1_IMG="quotlt_memotor1.jpg";
   
   public static final String QUOTATIONLT_MOTOR2_IMG="quotlt_memotor2.jpg";
   
   public static final String QUOTATION_RETURNHOME_URL="<a href=home.do?invoke=home";
   public static final String QUOTATION_VIEWLINK_RETURNHOME="Click Here </a>, To Return to Home Page.";
   
   public static final String QUOTATION_SUBMITTORSM_TRACKNO= "Your enquiry has been Submitted to RSM successfully.<br>  Your enquiry tracking# : ";
   public static final String QUOTATION_APPROVEBYRSM_TRACKNO= "Your enquiry has been Approved By RSM successfully.<br>  Your enquiry tracking# : ";
   public static final String QUOTATION_SUBMITTONSM_TRACKNO= "Your enquiry has been Submitted to NSM successfully.<br>  Your enquiry tracking# : ";
   public static final String QUOTATION_APPROVEBYNSM_TRACKNO= "Your enquiry has been Approved By NSM successfully.<br>  Your enquiry tracking# : ";
   public static final String QUOTATION_SUBMITTOMH_TRACKNO= "Your enquiry has been Submitted to MH successfully.<br>  Your enquiry tracking# : ";
   public static final String QUOTATION_APPROVEBYMH_TRACKNO= "Your enquiry has been Approved By MH successfully.<br>  Your enquiry tracking# : ";
   
   public static final String QUOTATION_REVISEBYSM_TRACKNO = "Your enquiry has been Returned to Design successfully.<br>  Your enquiry tracking# : ";
   public static final String QUOTATION_REVISEBYRSM_TRACKNO = "Your enquiry has been Returned to SM successfully.<br>  Your enquiry tracking# : ";
   public static final String QUOTATION_REVISEBYNSM_TRACKNO = "Your enquiry has been Returned to RSM successfully.<br>  Your enquiry tracking# : ";
   public static final String QUOTATION_REVISEBYMH_TRACKNO = "Your enquiry has been Returned to NSM successfully.<br>  Your enquiry tracking# : ";
   
   public static final String RATING_DEFAULT_VOLT="VOLT";
   public static final String RATING_DEFAULT_VOLTADDVAR="VOLTADDVAR";
   public static final String RATING_DEFAULT_VOLTREMVAR="VOLTREMVAR";
   public static final String RATING_DEFAULT_FREQ="FREQ";
   public static final String RATING_DEFAULT_FREQADDVAR="FREQADDVAR";
   public static final String RATING_DEFAULT_FREQREMVAR="FREQREMVAR";
   public static final String RATING_DEFAULT_COMBVAR="COMBVAR";
   public static final String RATING_DEFAULT_SERVICEFACTOR="SERVICEFACTOR";
   public static final String RATING_DEFAULT_MTHDOFSTART="MTHDOFSTART";
   public static final String RATING_DEFAULT_DOLSTART="DOLSTART";
   public static final String RATING_DEFAULT_TBPOS="TBPOS";
   public static final String RATING_DEFAULT_AMBTEMP="AMBTEMP";
   public static final String RATING_DEFAULT_TEMPRISE="TEMPRISE";
   public static final String RATING_DEFAULT_INSCLASS="INSCLASS";
   public static final String RATING_DEFAULT_MTHDOFCOOL="MTHDOFCOOL";
   public static final String RATING_DEFAULT_HAZLOC="HAZLOC";
   public static final String RATING_DEFAULT_HAZAREA="HAZAREA";
   public static final String RATING_DEFAULT_GASGROUP="GASGROUP";
   public static final String RATING_DEFAULT_DUTY="DUTY";
   public static final String RATING_DEFAULT_REPLACEMOTOR="REPLACEMOTOR";
   public static final String RATING_DEFAULT_WINDTREATMENT="WINDTREATMENT";
   public static final String RATING_DEFAULT_WINDWIRE="WINDWIRE";
   public static final String RATING_DEFAULT_LEAD="LEAD";
   public static final String RATING_DEFAULT_WINDCONFIG="WINDCONFIG";
   public static final String RATING_DEFAULT_OVERLOADDUTY="OVERLOADDUTY";
   public static final String RATING_DEFAULT_CONSTEFFRANGE="CONSTEFFRANGE";
   public static final String RATING_DEFAULT_SHAFTTYPE="SHAFTTYPE";
   public static final String RATING_DEFAULT_SHAFTMATERIAL="SHAFTMATERIAL";
   public static final String RATING_DEFAULT_ROTATIONDIR="ROTATIONDIR";
   public static final String RATING_DEFAULT_MTHDOFCOUPLING="MTHDOFCOUPLING";
   public static final String RATING_DEFAULT_PAINTTYPE="PAINTTYPE";
   public static final String RATING_DEFAULT_PAINTSHADE="PAINTSHADE";
   public static final String RATING_DEFAULT_PAINTTHICKNESS="PAINTTHICKNESS";
   public static final String RATING_DEFAULT_CABLESIZE="CABLESIZE";
   public static final String RATING_DEFAULT_TBOXSIZE="TBOXSIZE";
   public static final String RATING_DEFAULT_SPREADERBOX="SPREADERBOX";
   public static final String RATING_DEFAULT_SPACEHEATER="SPACEHEATER";
   public static final String RATING_DEFAULT_VIBRATION="VIBRATION";
   public static final String RATING_DEFAULT_FLYLEADWITHOUTTB="FLYLEADWITHOUTTB";
   public static final String RATING_DEFAULT_FLYLEADWITHTB="FLYLEADWITHTB";
   public static final String RATING_DEFAULT_METALFAN="METALFAN";
   public static final String RATING_DEFAULT_TECHOMOUNTING="TECHOMOUNTING";
   public static final String RATING_DEFAULT_SHAFTGROUNDING="SHAFTGROUNDING";
   public static final String RATING_DEFAULT_HARDWARE="HARDWARE";
   public static final String RATING_DEFAULT_GLANDPLATE="GLANDPLATE";
   public static final String RATING_DEFAULT_DOUBLECOMPRESSGLAND="DOUBLECOMPRESSGLAND";
   public static final String RATING_DEFAULT_SPMMOUNTPROVISION="SPMMOUNTPROVISION";
   public static final String RATING_DEFAULT_ADDLNAMEPLATE="ADDLNAMEPLATE";
   public static final String RATING_DEFAULT_ARROWPLATEDIR="ARROWPLATEDIR";
   public static final String RATING_DEFAULT_RTD="RTD";
   public static final String RATING_DEFAULT_BTD="BTD";
   public static final String RATING_DEFAULT_THERMISTER="THERMISTER";
   public static final String RATING_DEFAULT_AUXTBOX="AUXTBOX";
   public static final String RATING_DEFAULT_CABLESEALBOX="CABLESEALBOX";
   public static final String RATING_DEFAULT_BEARINGSYSTEM="BEARINGSYSTEM";
   public static final String RATING_DEFAULT_BEARINGNDE="BEARINGNDE";
   public static final String RATING_DEFAULT_BEARINGDE="BEARINGDE";
   public static final String RATING_DEFAULT_WITNESS="WITNESS";
   public static final String RATING_DEFAULT_ADDLTEST1="ADDLTEST1";
   public static final String RATING_DEFAULT_ADDLTEST2="ADDLTEST2";
   public static final String RATING_DEFAULT_ADDLTEST3="ADDLTEST3";
   public static final String RATING_DEFAULT_TYPETEST="TYPETEST";
   public static final String RATING_DEFAULT_CERTIFICATION="CERTIFICATION";
   public static final String RATING_DEFAULT_SPARES="SPARES";
   public static final String RATING_DEFAULT_NOISELEVEL="NOISELEVEL";
   public static final String RATING_DEFAULT_QAP = "QAP";
   public static final String RATING_DEFAULT_DATASHEET = "DATASHEET";
   public static final String RATING_DEFAULT_MOTORGA = "MOTORGA";
   public static final String RATING_DEFAULT_PERFCURVE = "PERFCURVE";
   public static final String RATING_DEFAULT_TBOXGA = "TBOXGA";
   
   
   public static final String RATING_DEFAULT_ALTITUDE = "1000";
   
   public static final String QUOTATIONLT_STRING_STANDARD = "STANDARD";
   
   public static final String QUOTATIONLT_APPROVAL_TYPE = "APPROVALTYPE";
   
   public static final long QUOTATIONLT_MANDAY_SUPERVISE_CHARGES = 16500;
   
   public static final String QUOTATIONLT_SUPERVISION_CHARGES = "Supervision and Commisioning charges";
   
   public static final String QUOTATION_LT_WON = "WON";
   public static final String QUOTATION_LT_LOST ="LOST";
   public static final String QUOTATION_LT_ABONDENT ="ABONDENT";
 
   public static final String QUOTATION_ABONDENT_TRACKNO= "Your enquiry has been moved to ABONDENT.<br>  Your enquiry tracking# : ";
   public static final String QUOTATION_LOST_TRACKNO= "Your enquiry has been moved to LOST.<br>  Your enquiry tracking# : ";
   public static final String QUOTATION_WON_TRACKNO= "Your enquiry has been moved to WON.<br>  Your enquiry tracking# : ";
   
   public static final String QUOTATION_LD_TRACKNO = "Liquidated Damages for the Enquiry has been processed successfully! <br>  Your enquiry tracking# : ";
   
   public static final String QUOTATION_DELETED_TRACKNO= "Your enquiry has been deleted successfully.<br>  Your enquiry tracking# : ";
   
   public static final String QUOTATION_NEWREVISION_TRACKNO = "An Enquiry with New Revision has been created against your Current Enquiry.<br>  Your enquiry tracking# : ";
   
   public static final String QUOTATION_LT_DELETEDRAFT_ENQ = "deleteDraftEnquiry";
   
   public static final String QUOTATIONLT_BULKLOAD_MAX_ROWCOUNT = "QUOTATIONLT_BULKLOAD_MAX_ROWCOUNT";
   
   public static final String QUOTATIONLT_LISTPRICE_BULKUPLOAD_COLUMNCOUNT = "QUOTATIONLT_LISTPRICE_BULKUPLOAD_COLUMNCOUNT";
   public static final String QUOTATIONLT_CUSTOMERDISCOUNT_BULKUPLOAD_COLUMNCOUNT = "QUOTATIONLT_CUSTOMERDISCOUNT_BULKUPLOAD_COLUMNCOUNT";
   
   public static final int QUOTATION_LISTPRICE_VALIDCOLUMNCOUNT = 11;
   public static final int QUOTATION_CUSTOMERDISCOUNT_VALIDCOLUMNCOUNT = 14;
   
   public static final String REGAL_REXNORD_LOGO = "REGAL_REXNORD_LOGO";
   
   public static final String QUOTATION_LT_LASTRATINGNUM = "lastRatingNum";
   public static final String QUOTATION_LT_PREVIDVAL = "prevIdVal";
   public static final String QUOTATION_LT_SHOWREASSIGN_FLAG = "showReAssignSectionFlag";
   
   public static final String QUOTATIONLT_SHOW_SWITCH_USER = "QUOTATIONLT_SHOW_SWITCH_USER";
   public static final String QUOTATIONLT_LOGINUSER_FULLNAME = "loginusername";
   public static final String QUOTATIONLT_SESSION_USERINFO = "sessionuserinfo";
   
   public static final String QUOTATIONLT_ADDONPAGETYPE_CREATE = "CREATE";
   public static final String QUOTATIONLT_ADDONPAGETYPE_DESIGN = "DESIGN";
   
   public static final String QUOTATIONLT_REASSIGNTO_ID = "reassignToId";
   
   public static final String QUOTATIONLT_REASSIGNTO_DE = "DE";
   public static final String QUOTATIONLT_REASSIGNTO_DM = "DM";
   public static final String QUOTATIONLT_REASSIGNTO_QA = "QA";
   public static final String QUOTATIONLT_REASSIGNTO_SCM = "SCM";
   public static final String QUOTATIONLT_REASSIGNTO_MFG = "MFG";
   
   public static final String QUOTATION_REASSIGN_TRACKNO1 = "Your enquiry has been Re-assigned to ";
   public static final String QUOTATION_REASSIGN_TRACKNO2 = ".<br>  Your enquiry tracking# : ";
   
   public static final String QUOTATION_DMUPDATE_RETURN = "DMRETURN";
   public static final String QUOTATION_DMUPDATE_SUBMIT = "DMSUBMIT";
   public static final String QUOTATION_DMUPDATE_REGRET = "DMREGRET";
   
   public static final String QUOTATIONLT_ADDON_PERCENT = "PERCENT";
   public static final String QUOTATIONLT_ADDON_ROUNDED = "ROUNDED";
 
   public static final String QUOTATION_COMMENTSDEVIATIONS = "Comments & Deviations ";
   
   public static final String QUOTATIONLT_SLIPRING_KS = "KS";
   public static final String QUOTATIONLT_SLIPRING_KSCMR = "KS-CMR";
   
   public static final String QUOTATIONLT_LIQUIDATEDDAMAGE_APPROVE = "APPROVE";
   public static final String QUOTATIONLT_LIQUIDATEDDAMAGE_REJECT = "REJECT";
 
   public static final String QUOTATIONLT_NOTIFIER_CHECK_STATUSES = "2,4,5,8,15,16,17,18,19,20,21";
   
   public static final String QUOTATIONLT_SENDMAIL_STOP = "QUOTATIONLT_SENDMAIL_STOP";
   
   public static final String QUOTATIONLT_CUSTOMER_DEALER_LINK = "In the event of Order, Kindly Place the PO on our Authorized Dealer. We will provide the Dealer information.";
   
   public static final String QUOTATIONLT_PDFTYPE_COMMERCIAL = "COMMERCIAL";
   public static final String QUOTATIONLT_PDFTYPE_TECHNICAL = "TECHNICAL";
   
   
}