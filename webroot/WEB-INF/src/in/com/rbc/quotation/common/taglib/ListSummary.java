/**
 * *****************************************************************************
 * Project Name: Request For Quotation
 * Document Name: ListSummary.java
 * Package: in.com.rbc.quotation.common.taglib
 * Desc: This class is used while displaying results list along with pagination, in JSP pages
 * *****************************************************************************
 * @author 100002865 (Shanthi Chinta)
 * *****************************************************************************
 */
package in.com.rbc.quotation.common.taglib;

import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.utility.ListModel;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

public class ListSummary extends TagSupport {
    public int doStartTag() {
        try {
            JspWriter out = pageContext.getOut();
            HttpServletRequest request = (HttpServletRequest)pageContext.getRequest();
            
            if(request.getAttribute(QuotationConstants.QUOTATION_LIST_LISTMODELCLASS) != null) {
                ListModel oListModel = (ListModel)request.getAttribute(QuotationConstants.QUOTATION_LIST_LISTMODELCLASS);
                ArrayList resultList = (ArrayList)request.getAttribute(QuotationConstants.QUOTATION_LIST_RESULTSLIST);
                if (resultList != null && resultList.size() > 0) {
                     out.print(oListModel.getListSummary(resultList.size()));
                }
            }
        } catch(Exception e) {
            //ExceptionLog.logFinestExceptionStackTrace(InfogateConstants.LOG_LISTING, this.getClass().getName(), e.getMessage(), e);
        }
        return SKIP_BODY;
    }
}
