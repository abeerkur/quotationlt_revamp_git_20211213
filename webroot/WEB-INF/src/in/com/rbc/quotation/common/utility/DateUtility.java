/**
 * *****************************************************************************
 * Project Name: Request For Quotation
 * Document Name: DateUtility.java
 * Package: in.com.rbc.acmotor.common.utility
 * Desc: Date Utility class which hold common date functions that are used 
 *       across the project.
 * *****************************************************************************
 * @author 100002865 (Shanthi Chinta)
 * *****************************************************************************
 */
package in.com.rbc.quotation.common.utility;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;

import in.com.rbc.common.utility.ExceptionUtility;

public class DateUtility {
   
    private static Calendar oCalendar;
    private Date oDate;

    /**
     * Constructor for the DateUtility object
     *
     * @param date 
     */
    public DateUtility(Date oDate) {
        this.oDate = oDate;
        oCalendar = Calendar.getInstance();
        // for the moment, don't worry about validating the incoming date
        oCalendar.setLenient(false);
        oCalendar.setTime(oDate);
    }
    
    /**
     * Method to retrieve current date and time, format the date in the format, 
     * MM/dd/yyyy hh:mm:ss, and return the formated date & time string.
     * 
     * @return Returns current date & time string in MM/dd/yyyy hh:mm:ss format.
     * @author 100002865 (Shanthi Chinta)
     * Created on: Jul 2, 2008
     */
    public static String getCurrentDateTime() {
        StringBuffer buff = new StringBuffer(10);
        Date oDate = new Date();
        Calendar c = getCalRef();
        c.setTime(oDate);
        buff.append(pad(c.get(Calendar.MONTH) + 1));
        buff.append("/");
        buff.append(pad(c.get(Calendar.DATE)));
        buff.append("/");
        buff.append(c.get(Calendar.YEAR));
        buff.append(" ");
        buff.append(c.get(Calendar.HOUR_OF_DAY));
        buff.append(":");
        buff.append(c.get(Calendar.MINUTE));
        buff.append(":");
        buff.append(c.get(Calendar.SECOND));
        return buff.toString();
    }

    /*
     * Method gives the Current date (short) in RBC standard format i.e.,
     * MM/dd/yyyy.
     * 
     * @return Returns the date string, formatted in MM/dd/yyyy format, which 
     * is the RBC standard date format
     */
    public static String getRBCStandardDate() {
        StringBuffer buff = new StringBuffer(10);
        Date oDate = new Date();
        Calendar c = getCalRef();
        c.setTime(oDate);
        buff.append(pad(c.get(Calendar.MONTH) + 1));
        buff.append("/");
        buff.append(pad(c.get(Calendar.DATE)));
        buff.append("/");
        buff.append(c.get(Calendar.YEAR));
        return buff.toString();
    }
    
    public static String getCurrentYear() {
        Date oDate = new Date();
        Calendar c = getCalRef();
        c.setTime(oDate);
        return ""+c.get(Calendar.YEAR);
    }
    
    public static String getCurrentMonth() {
        Date oDate = new Date();
        Calendar c = getCalRef();
        c.setTime(oDate);
        return ""+pad(c.get(Calendar.MONTH) + 1);
    }
    
    public static String getCurrentDay() {
        Date oDate = new Date();
        Calendar c = getCalRef();
        c.setTime(oDate);
        return ""+pad(c.get(Calendar.DATE));
    }
    
    /**
     * Method to format the specified date in RBC standard format i.e.,
     * MM/dd/yyyy.
     * 
     * @param oDate Date object that has to be formatted.
     * @return Returns the date string, formatted in MM/dd/yyyy format, 
     * which is the RBC standard date format.
     * @author 100002865 (Shanthi Chinta)
     */
    public static String getRBCStandardDate(Date oDate) {
        StringBuffer buff = new StringBuffer(10);
        Calendar c = getCalRef();
        c.setTime(oDate);
        buff.append(pad(c.get(Calendar.MONTH) + 1));
        buff.append("/");
        buff.append(pad(c.get(Calendar.DATE)));
        buff.append("/");
        buff.append(c.get(Calendar.YEAR));
        return buff.toString();
    }

    /**
     * Method to format the specified date in RBC standard format i.e.,
     * MM/dd/yyyy.
     * 
     * @param oDate Date string that has to be formatted.
     * @return Returns the date string, formatted in MM/dd/yyyy format, 
     * which is the RBC standard date format.
     * @author 100002865 (Shanthi Chinta)
     */
    public static String getRBCStandardDate(String sDate) {
        StringBuffer buff = new StringBuffer(10);
        try {
            Date oDate = new SimpleDateFormat("MM/dd/yyyy").parse(sDate);
            Calendar c = getCalRef();
            c.setTime(oDate);
            buff.append(pad(c.get(Calendar.MONTH) + 1));
            buff.append("/");
            buff.append(pad(c.get(Calendar.DATE)));
            buff.append("/");
            buff.append(c.get(Calendar.YEAR));
            return buff.toString();
        } catch (Exception ex) {
            return sDate;
        }
    }
    
    /**
     * This method accepts two date strings, compares them and returns an integer.
     * 
     * @param sDate1 Date string for comparision
     * @param sDate2 Date string for comparision
     * @return Returns an integer, depending on the comparision, as below
     *         <br>-1: if date1 is less than date2
     *         <br>1: if date1 is greater than date2
     *         <br>0: if date1 is equal to date2
     *         <br>2: if there is an exception
     * @author 100002865 (Shanthi Chinta)
     * Created on: Apr 16, 2008
     */
    private static int compareDates(String sDate1, String sDate2) {
        int iDate1 = 0;
        int iDate2 = 0;
        int iReturnValue = 2;
        
        try {
            /*
             * Formatting the first date string in MM/dd/yyyy format, retrieving 
             * the date, month and year values and building a string without
             * any separation characters like yyyyMMdd
             */
            Date oDate1 = new SimpleDateFormat("MM/dd/yyyy").parse(sDate1);
            Calendar c1 = getCalRef();
            c1.setTime(oDate1);
            iDate1 = Integer.parseInt(c1.get(Calendar.YEAR) + pad(c1.get(Calendar.MONTH) + 1) + pad(c1.get(Calendar.DATE)));

            /*
             * Formatting the second date string in MM/dd/yyyy format, retrieving 
             * the date, month and year values and building a string without
             * any separation characters like yyyyMMdd
             */
            Date oDate2 = new SimpleDateFormat("MM/dd/yyyy").parse(sDate2);
            Calendar c2 = getCalRef();
            c2.setTime(oDate2);
            iDate2 = Integer.parseInt(c2.get(Calendar.YEAR) + pad(c2.get(Calendar.MONTH) + 1) + pad(c2.get(Calendar.DATE)));
            
            /* Comparing the date values and retrieving the return value */
            // if date1 is less than date2 then return -1
            if ( iDate1 < iDate2 ) {
                iReturnValue = -1;
            }
            // if date1 is greater than date2 then return 1
            if ( iDate1 > iDate2 ) {
                iReturnValue= 1;
            }
            // if date1 and date2 are equal return 0;
            if ( iDate1 == iDate2 ) {
                iReturnValue= 0;
            }
        }
        catch (Exception ex) {
            return iReturnValue;
        }
        
        return iReturnValue;
    }
    /**
     * This method accepts a date string, compares it with current date and returns an integer.
     * @param sDate Date string for comparision
     * @return returns an integer, depending on the comparision, as below
     *         <br>-1: if current date is less than specified date
     *         <br>1: if current date is greater than specified date
     *         <br>0: if current date is equal to specified date
     *         <br>2: if there is an exception
     * @author 100002865 (Shanthi Chinta)
     * Created on: Apr 16, 2008
     */
    private static int compareDateWithToday(String sDate) {
        int iDate1 = 0;
        int iDate2 = 0;
        int iReturnValue = 2;
        
        try {
            Date oDate1 = new Date();
            Calendar c1 = getCalRef();
            c1.setTime(oDate1);
            iDate1 = Integer.parseInt(c1.get(Calendar.YEAR) + pad(c1.get(Calendar.MONTH) + 1) + pad(c1.get(Calendar.DATE)));
            
            Date oDate2 = new SimpleDateFormat("MM/dd/yyyy").parse(sDate);
            Calendar c2 = getCalRef();
            c2.setTime(oDate2);
            iDate2 = Integer.parseInt(c2.get(Calendar.YEAR) + pad(c2.get(Calendar.MONTH) + 1) + pad(c2.get(Calendar.DATE)));
            
            // if date1 is less than date2 then return -1
            if ( iDate1 < iDate2 ) {
                iReturnValue = -1;
            }
            // if date1 is greater than date2 then return 1
            if ( iDate1 > iDate2 ) {
                iReturnValue= 1;
            }
            // if date1 and date2 are equal return 0;
            if ( iDate1 == iDate2 ) {
                iReturnValue= 0;
            }
        }
        catch (Exception ex) {
            return iReturnValue;
        }
        
        return iReturnValue;
    }
    
    public static String getEffectiveDate(String sEffectiveDate) {
        if ( compareDateWithToday(sEffectiveDate)==-1 || compareDateWithToday(sEffectiveDate)==0 ) {
            return getRBCStandardDate(sEffectiveDate);
        }
        else {
            return getRBCStandardDate();
        }
    }
    
    /**
     * formatDate() returns a String representing the given Date
     * in the form <day>, yyyy.mm.dd
     */
    public static String formatDate(Date oDate, String sDateFormat) {

        return (new SimpleDateFormat (sDateFormat)).format(oDate);

    }

    /**
     * Return the incoming date in Oracle Format: MM-DD-YYYY uses date specified
     * upon construction
     *
     * @param oDate  
     * @return The oracleDate value
     * @params java.util.Date
     * @returns String
     */
    public static String getOracleDate(Date oDate) {
        StringBuffer buff = new StringBuffer(10);
        Calendar c = getCalRef();
        c.setTime(oDate);
        buff.append(pad(c.get(Calendar.MONTH) + 1));
        buff.append("-");
        buff.append(pad(c.get(Calendar.DATE)));
        buff.append("-");
        buff.append(c.get(Calendar.YEAR));
        return buff.toString();
    }
    
    /**
     * Gets the current oracle date
     * @return The currentOracleDate value
     */
    public static String getCurrentOracleDate() {
        return getOracleDate(new Date());
    }

    /**
     * Return the incoming date in Month and Year Format: MM-YYYY uses date
     * specified upon construction
     * @param oDate  
     * @return The monthYear value
     * @params java.util.Date
     * @returns String
     */
    public static String getMonthYear(Date oDate) {
        StringBuffer buff = new StringBuffer(10);
        Calendar c = getCalRef();
        c.setTime(oDate);
        buff.append(pad(c.get(Calendar.MONTH) + 1));
        buff.append("-");
        buff.append(c.get(Calendar.YEAR));
        return buff.toString();
    }

    /**
     * Gets the current month year
     * @return The currentMonthYear value
     */
    public static String getCurrentMonthYear() {
        return getMonthYear(new Date());
    }

    /**
     * Return the incoming in "short" form, as defined by the Locale in the
     * DateFormat construction: for US, the form is MM/DD/YY, e.g. 12/5/01
     * @param oDate  
     * @return The shortDate value
     * @params java.util.Date
     * @returns String
     */
    public static String getShortDate(Date oDate) {
        return DateFormat.getDateInstance(DateFormat.SHORT, Locale.US).format(oDate);
    }

    /**
     * Gets the current short date
     * @return The currentShortDate value
     */
    public static String getCurrentShortDate() {
        return getShortDate(new Date());
    }

    /**
     * Return the incoming in "medium" form, as defined by the Locale in the
     * DateFormat construction. For US, the form is, e.g., Dec 5, 2001
     * @param oDate  
     * @return The mediumDate value
     * @params java.util.Date
     * @returns String
     */
    public static String getMediumDate(Date oDate) {
        return DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.US).format(oDate);
    }

    /**
     * This method is used to validate the date.
     * First it validates for the date format and then checks for the value.
     * 
     * @param dateValue - Date string, that has to be validated
     * @param dateFormat - Date format string, against which the date value should be validated
     * @param dateTokenizer - Separator between the date fields
     * @returns Returns a boolean value, either true/false depending on the validation
     * Author: 100002668 (Anand Yalla) (Rajkiran Attuluri)
     * Created on: Jun 3, 2008
     */
    public static boolean validateDate(String dateValue, 
                                       String dateFormat, 
                                       String dateTokenizer) {
        
    	boolean isValidDate = false;
    	
        try{
    		if (dateValue != null && dateFormat != null && dateTokenizer != null){
    			/*
    			 * tokenize both dateValue and dateFormat using dateTokenizer value 
    			 */
    			StringTokenizer stToken = new StringTokenizer(dateValue, dateTokenizer);
    			StringTokenizer stFormatToken = new StringTokenizer(dateFormat, dateTokenizer);
    			
                if (stToken != null && stFormatToken != null){
    				//token length should be same
    				if (stToken.countTokens() == stFormatToken.countTokens()){
    					while (stToken.hasMoreTokens() && stFormatToken.hasMoreTokens()){
    						String sTokenValue = stToken.nextToken();
    						String sFormatTokenValue = stFormatToken.nextToken();
    						
    						if ( sTokenValue.trim().length() == sFormatTokenValue.trim().length() ) {
    							isValidDate = true;
    						} else {
    							isValidDate = false;
    						}
    					}
    				}
    			}
    		}
            if (isValidDate) {
                SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
                sdf.setLenient(false);
                sdf.parse(dateValue);
            }
    	} catch (ParseException e) {
            LoggerUtility.log("DEBUG", "DateUtility", "validateDate", "ParseException: " + e);
            isValidDate = false;
        } catch (IllegalArgumentException e) {
            LoggerUtility.log("DEBUG", "DateUtility", "validateDate", "IllegalArgumentException: " + e);
            isValidDate = false;
        } catch (Exception e) {
            LoggerUtility.log("DEBUG", "DateUtility", "validateDate", "Exception: " + e);
            isValidDate = false;
        }
        return isValidDate;
    }

    /**
     * Gets the current long date
     * @return The currentLongDate value
     */
    public static String getCurrentLongDate() {
        return getLongDate(new Date());
    }

    
    /**
     * Return the incoming date as number of milliseconds since 00:00:00.00 GMT
     * Jan 1, 1970
     * @param oDate 
     * @return 
     * @params java.util.Date
     * @returns long
     */
    public static long dateToLong(Date oDate) {
       
        Calendar c = getCalRef();
        c.setTime(oDate);
        return c.getTime().getTime();
    }
    
    /**
     * Return the incoming in "long" form, as defined by the Locale in the
     * DateFormat construction. For US, the form is, e.g., December 5, 2001
     * @param oDate  
     * @return The longDate value
     * @params java.util.Date
     * @returns String
     */
    public static String getLongDate(Date oDate) {
        return DateFormat.getDateInstance(DateFormat.LONG, Locale.US).format(oDate);
    }

   /**
     * private method getting the reference to a Calendar object This object is
     * either created with the object is constructed, or at the time of the
     * static call.
     * @return The calRef value
     * @params none
     * @returns Calendar
     */
    private static Calendar getCalRef() {
        if (oCalendar == null) {
            return Calendar.getInstance();
        } else {
            return oCalendar;
        }
    }

    /**
     * private method for padding a 0 to a single digit
     * @param value  
     * @return 
     * @params int
     * @returns String
     */
    private static String pad(int value) {
        if (value > 9) {
            return Integer.toString(value);
        } else {
            return "0" + Integer.toString(value);
        }
    }

    /**
     * private method for padding a 0 to a decimal value < 10.000
     * @param str  
     * @return 
     * @params int
     * @returns String
     */
    private static String pad(String str) {
        int pos = str.indexOf(".");
        if (1 == pos) {
            return "0" + str;
        } else {
            return str;
        }
    }
    
    public static long differenceInDays(String start_date, String end_date) throws Exception {
    	long difference_In_Days = 0;
    	
    	try {
    		Date d1 = new SimpleDateFormat("MM/dd/yyyy").parse(start_date);
    		Date d2 = new SimpleDateFormat("MM/dd/yyyy").parse(end_date);
    		
    		// Calucalte time difference in milliseconds
    		long difference_In_Time = d2.getTime() - d1.getTime();
    		
    		difference_In_Days = TimeUnit.MILLISECONDS.toDays(difference_In_Time) % 365;
    		
    	} catch(Exception e) {
    		System.out.println("DateUtility.differenceInDays ... Exception :: " + ExceptionUtility.getStackTraceAsString(e));
    		throw e;
    	}
    	
    	return difference_In_Days;
    }
}

