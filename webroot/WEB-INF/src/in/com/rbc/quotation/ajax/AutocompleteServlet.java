/**
 * ******************************************************************************************
 * Project Name: Request For Quotation
 * Document Name: AutocompleteServlet.java
 * Package: in.com.rbc.quotation.ajax
 * Desc:  Servlet used for autocomplete functionality 
 * ******************************************************************************************
 * @author 100006126
 * ******************************************************************************************
 */

package in.com.rbc.quotation.ajax;


import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import in.com.rbc.quotation.admin.dao.CustomerDao;
import in.com.rbc.quotation.admin.dao.MasterDao;
import in.com.rbc.quotation.admin.dto.CustomerDto;
import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.utility.CommonUtility;
import in.com.rbc.quotation.common.utility.DBUtility;
import in.com.rbc.quotation.common.utility.ExceptionUtility;
import in.com.rbc.quotation.common.utility.LoggerUtility;
import in.com.rbc.quotation.common.vo.KeyValueVo;
import in.com.rbc.quotation.factory.DaoFactory;
import in.com.rbc.quotation.user.action.UserAction;
import in.com.rbc.quotation.user.dto.UserDto;




/*
 * This class is mainly used for generating and returning the XML content based on input provided in the text field, 
 * returned XML content will be parsed and the fields will be displayed.
 * 
 * Standard XML format:
 * 
 *  	<list>
 *  		<item value="1">Item1</item>
 *  		<item value="2">Item2</item>
 *  	</list>
 *  
 *  The above format has to be maintained, since ajaxtags code expects in this format. *   
 *  
 * 
 */
public final class AutocompleteServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException  {
		doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LoggerUtility.log("INFO", this.getClass().getName(), "doPost()", "START");
		response.setContentType("text/xml;charset=UTF-8");
		
		/*
		 * If the values to be fetched differs functionality wise then this variable will be used		 *  
		 */
		String sType = CommonUtility.getStringParameter(request,"type");
		String sFieldType = CommonUtility.getStringParameter(request,"fieldtype");
		LoggerUtility.log("INFO", this.getClass().getName(), "doPost()", "Type = "+sType);
		
		/* Search parameter value */
		String sSearchFragment = CommonUtility.getStringParameter(request, "customer");
		
		LoggerUtility.log("INFO", this.getClass().getName(), "doPost()", "sSearchFragment == "+sSearchFragment);
		
		/*
		 * Get the value type parameter from request 
		 * This parameter will specify what should go into value attribute
		 * Like: In some cases we need customerId as the value then pass "id" as parameter 
		 */
		String sValueType = CommonUtility.getStringParameter(request, "valueType");		
		
		LoggerUtility.log("INFO", this.getClass().getName(), "doPost()", "sValueType == "+sValueType);
		
		CustomerDto oCustomerDto = new CustomerDto();		
		Connection conn = null;  // Connection object to store the database connection
		DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
		ArrayList alSearchResult ;
		
		 /* Instantiating the UserAction object */
        UserAction oUserAction = new UserAction();
		
		
		try {
			/* Instantiating the DBUtility object */
			oDBUtility = new DBUtility();
			/* Creating an instance of of DaoFactory  */
			DaoFactory oDaoFactory = new DaoFactory();
			/* Retrieving the database connection using DBUtility.getDBConnection method */
			conn = oDBUtility.getDBConnection();
			
			CustomerDao oCustomerDao = oDaoFactory.getCustomerDao();
			MasterDao oMasterDao = oDaoFactory.getMasterDao();
			
			oCustomerDto.setCustomer(sSearchFragment);
			
			if (sType.equalsIgnoreCase("placeenquiry")){
				 /* Retrieving the logged-in user information */
	            UserDto oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
	            
	            if (oUserDto != null){
	            	oCustomerDto.setRegionId(oMasterDao.getRegionIDBySalesManagerId(conn, oUserDto.getUserId()));	            	
	            	oCustomerDto.setInvoke("placeenquiry");
	            }else{
	            	return;
	            }
			}
			
			if("locationOfInstallation".equalsIgnoreCase(sFieldType)) {
				alSearchResult = oCustomerDao.getLocationBySearchString(conn,oCustomerDto);
			} else {
				alSearchResult = oCustomerDao.getCustomerName(conn,oCustomerDto);
			}
			
			LoggerUtility.log("INFO",this.getClass().getName(),"AutoComplete-doPost","ArraList Size :--"+alSearchResult.size());
			StringBuffer xml = new StringBuffer("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
			xml.append("<list>");	
			
			if("locationOfInstallation".equalsIgnoreCase(sFieldType)) {
				for(int i=0 ; i < alSearchResult.size() ; i++ ) {
					CustomerDto oTempCustomerDto = (CustomerDto)alSearchResult.get(i);
					if (sValueType.trim().equalsIgnoreCase("id"))
						xml.append("<item value=\""+oTempCustomerDto.getLocationId()+"\">");
					else
						xml.append("<item value=\""+oTempCustomerDto.getLocationname()+"\">");
					
					xml.append(oTempCustomerDto.getLocationname());
					
					xml.append("</item>");
				}
			} else {
				for(int i=0 ; i < alSearchResult.size() ; i++ ) {
					CustomerDto oTempCUstomerDto = (CustomerDto)alSearchResult.get(i);
					if (sValueType.trim().equalsIgnoreCase("id"))
						xml.append("<item value=\""+oTempCUstomerDto.getCustomerId()+"\">");
					else
						xml.append("<item value=\""+oTempCUstomerDto.getCustomer()+"\">");
					
					xml.append(oTempCUstomerDto.getCustomer());
					
					xml.append("</item>");
				}
			}
			
			
			xml.append("</list>");			
			

			//xml.append(oTempCustomerVo.getCustomerInfo().replaceAll("&", "~12345~12345~"));
			String sFinalString = CommonUtility.replaceNull(xml.toString().replaceAll("&", "~12345~12345~"));
			
			PrintWriter out = response.getWriter();
			LoggerUtility.log("INFO", this.getClass().getName(), "doPost()", "XML == "+sFinalString);
			LoggerUtility.log("INFO", this.getClass().getName(), "doPost()", "END");
			out.print(sFinalString);
			out.close();
			
		}catch(Exception e) {
			
			/*
			* Logging any exception that raised during this activity in log files using Log4j
			*/
			LoggerUtility.log("DEBUG", this.getClass().getName(), "AutoComplete -- doPost()","getCusotmerName Search Result."+ExceptionUtility.getStackTraceAsString(e));
			
			/* Setting error details to request to display the same in Exception page */
			request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
			
		} finally {
            
            /*
             * Releasing the resources
             */
			if (oDBUtility != null && conn != null){
				try {
					oDBUtility.releaseResources(conn);
				}catch(SQLException se) {
					/*
					* Logging any exception that raised during this activity in log files using Log4j
					*/
					LoggerUtility.log("DEBUG", this.getClass().getName(), "AutoComplete -- doPost()", "SQLException Error  While closing the connection ."+ExceptionUtility.getStackTraceAsString(se));
					
				}
				
			}
		}
		/*
		
		if (sSearchFragment != null){
			
			xml.append("<item value=\"1\">");
			xml.append("Raj");
			xml.append("</item>");
			
			xml.append("<item value=\"2\">");
			xml.append("Kiran");
			xml.append("</item>");
			
			xml.append("<item value=\"3\">");
			xml.append("Raj Kiran");
			xml.append("</item>");
			
		}
		*/
		
	}
}
