/**
 * *****************************************************************************
 * Project Name: Request For Quotation
 * Document Name: BaseAction.java
 * Package: in.com.rbc.quotation.base
 * Desc: Base Action class extends the DispatchAction class, which in-turn will
 *       be extended by all other action classes in the application.
 * *****************************************************************************
 * Author: 100002865 (Shanthi Chinta)
 * *****************************************************************************
 */
package in.com.rbc.quotation.base;

import java.net.URL;
import java.util.Hashtable;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.xml.namespace.QName;
import javax.xml.rpc.ParameterMode;

import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.struts.actions.DispatchAction;

import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.utility.AttachmentUtility;
import in.com.rbc.quotation.common.utility.CommonUtility;
import in.com.rbc.quotation.common.utility.ExceptionUtility;
import in.com.rbc.quotation.common.utility.LoggerUtility;
import in.com.rbc.quotation.common.utility.PropertyUtility;
import in.com.rbc.common.dto.UserDto;

public class BaseAction extends DispatchAction {
	
    /**
     * This method accepts the HttpServletRequest object and String sGroupName,
     * check if user details are available in specified email group and returns true/false accordingly.
     * @param 	request HttpServletRequest object
     * @return 	Returns a boolean value, true/false, depending on the validation.
     * <br>Returns True 	- if the user is a member of the specified email group.
     * <br>Returns False 	- if the user is not a memeber of the specified email group.
     */
    public static boolean isMemberOfGroup(HttpServletRequest request, String sRoleId){
        String sGroupOutput = "";
        Hashtable htRoleOutput = null;
       
        if (request.getSession().getAttribute(QuotationConstants.QUOTATION_SESSION_ROLE) == null)
        	setRolesToRequest(request);
        
        if (request.getSession().getAttribute(QuotationConstants.QUOTATION_SESSION_ROLE) != null){
        	htRoleOutput = (Hashtable) request.getSession().getAttribute(QuotationConstants.QUOTATION_SESSION_ROLE);
        	if (htRoleOutput != null){
        		sGroupOutput = (String) htRoleOutput.get(sRoleId);
        		if (sGroupOutput == null)
        			sGroupOutput = "";
        	}
        }
        
        return sGroupOutput.equalsIgnoreCase(sRoleId)? true : false;
    }
    
    /**
     * This method accepts the HttpServletRequest object, check if user details
     * are available in the Administrator email group, in request 
     * or not and returns true/false, accordingly
     * 
     * @param request HttpServletRequest object
     * @return Returns a boolean value, true/false, depending on the validation.
     * <br>Returns True - if the user is a member of Administrator group.
     * <br>Returns False - if the user is not a member of Administrator group.
     */
    public static boolean isMemberOfAdministratorGroup(HttpServletRequest request) {
        return isMemberOfGroup(request, CommonUtility.getStringValue(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_ROLE_ADMIN)));
    }
    
    /**
     * This method accepts the HttpServletRequest object, check if user details
     * are available in the Administrator email group, in request 
     * or not and stores true/false, in the request.
     * <br>Stores True - if the user is a member of Administrator group.
     * <br>Stores False - if the user is not a member of Administrator group.
     * <br>This value is stored in request in a variable with name, isAdminsitrator.
     * 
     * @param request HttpServletRequest object
     */
    public static void setAdministratorRoleToRequest(HttpServletRequest request) {
        request.setAttribute(QuotationConstants.QUOTATION_REQ_ROLE_ADMINISTRATOR,
                             isMemberOfAdministratorGroup(request)+ "");
    }
    
    /**
     * This method accepts the HttpServletRequest object, check if user details
     * are available in either Super User email group or Administrator email group, 
     * in request and sets either true/false, different request variables.
     * 
     * @param request HttpServletRequest object
     */
    public static void setRolesToRequest(HttpServletRequest request) {
    	Hashtable htRoleOutput = null; 
    	String sRoleOutput = "";    	
    	/*
    	 * check if the roles are already set in the session for the logged-in user
    	 * get the new roles only when the roles are found in the session 
    	 */  
    	if (request.getSession().getAttribute(QuotationConstants.QUOTATION_SESSION_ROLE) == null) {
    		//System.out.println("Inside setRolesToRequest : ");
             try {
            	 
            	 String sUserSSOId = getLoginUserSSOId(request);
                 //System.out.println("Value from getLoginUserSSOId(request) : sUserSSOId : " + sUserSSOId);
            	 try {
            		 
                	 sRoleOutput = AttachmentUtility.getUserRoles(sUserSSOId);
                	 //System.out.println("sRoleOutput : " + sRoleOutput);
                	 
                 } catch(Exception e) {
                	 e.printStackTrace();
                 }
                 
                 if(sRoleOutput == null || (sRoleOutput != null && sRoleOutput.trim().length() == 0)){
                	 //System.out.println("sRoleOutput : Null or Empty");
                	 htRoleOutput = new Hashtable();
                	 htRoleOutput.put(""+QuotationConstants.QUOTATION_ROLE_OTHERS, ""+QuotationConstants.QUOTATION_ROLE_OTHERS);
                	 //System.out.println("sRoleOutput : Set Role OTHERS : ");
                	 request.getSession().setAttribute(QuotationConstants.QUOTATION_SESSION_ROLE, htRoleOutput);
                 }else{
                	 StringTokenizer stToken = new StringTokenizer(sRoleOutput.trim(), ",");
                	 if (stToken != null && stToken.countTokens() > 0){
                		 //System.out.println("sRoleOutput : Print Role Tokens");
                		 htRoleOutput = new Hashtable();
                		 while (stToken.hasMoreTokens()) {
                			 String sTokenValue = stToken.nextToken();
                			 //System.out.println(" : sTokenValue : " + sTokenValue);
                			 if (sTokenValue != null && sTokenValue.trim().length() > 0)
                				 htRoleOutput.put(sTokenValue, sTokenValue);
                		 }                		 
                		 request.getSession().setAttribute(QuotationConstants.QUOTATION_SESSION_ROLE, htRoleOutput);
                	 }
                 }
             } catch(Exception e) { 
            	 //System.out.println("ERROR :: setRolesToRequest : " + ExceptionUtility.getStackTraceAsString(e));
                 LoggerUtility.log("DEBUG", "BaseAction", "setRolesToRequest", ExceptionUtility.getStackTraceAsString(e));

             } 
    	} else {
    		//System.out.println("Inside setRolesToRequest : Roles already set to Session.");
    	}
       
    }
    
    public static boolean isMemberOfManagerGroup(HttpServletRequest request) {
        boolean isMemberOfManagerGroup = false;
        
        if ( ((String)request
                .getAttribute(QuotationConstants.QUOTATION_REQ_ROLE_ADMINISTRATOR))
                .equalsIgnoreCase("TRUE") ) {
            isMemberOfManagerGroup = true;
        }
        
        return isMemberOfManagerGroup;
    }
    
    public UserDto getLoggedInUserDetails(HttpServletRequest request) throws Exception {
    	
		String sMethodName = "getLoggedInUserDetails";
		String sLoginUserId = null;
		UserDto oUserDto = null;

		try {
			//System.out.println("Inside getLoggedInUserDetails : ");
			if (request.getSession().getAttribute(QuotationConstants.QUOTATIONLT_SESSION_USERINFO) == null) {
				sLoginUserId = getLoginUserSSOId(request);
				//System.out.println("Value from getLoginUserSSOId(request) : sLoginUserId : " + sLoginUserId);
				LoggerUtility.log("INFO", "BaseAction", sMethodName, "sLoginUserId = " + sLoginUserId);

				// Retrieving the login User details using UserUtility
				oUserDto = getUserInfo(sLoginUserId, PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_ATTACHMENT_APPLICATION_URL));

				if (oUserDto != null) {
					//System.out.println("oUserDto : " +oUserDto.toString());
					request.getSession().setAttribute(QuotationConstants.QUOTATIONLT_SESSION_USERINFO, oUserDto);
				} else {
					//System.out.println("oUserDto : null");
				}
				
			} else {
				oUserDto = (UserDto) request.getSession().getAttribute(QuotationConstants.QUOTATIONLT_SESSION_USERINFO);
				if (oUserDto == null) {
					LoggerUtility.log("INFO", "BaseAction", sMethodName, "After WebService Call... UserDto = NULL");
				} else {
					LoggerUtility.log("INFO", "BaseAction", sMethodName, "After WebService Call... User Name = " + oUserDto.getFullName());
				}
			}
		} finally {
			
		}

		return oUserDto;
	}
    
    
    public static String getLoginUserSSOId(HttpServletRequest request) throws Exception {
    	
		String sMethodName = "getLoginUserSSOId";
		String sClassName = "BaseAction";
		LoggerUtility.log("INFO", sClassName, sMethodName, "START");

		String sLoginUserId = null, sPassedAt = null;
		//System.out.println("Inside getLoginUserSSOId : ");
		try {
			if (request.getSession().getAttribute(QuotationConstants.QUOTAION_SELE_SSOID) != null) {
				sLoginUserId = (String) request.getSession().getAttribute(QuotationConstants.QUOTAION_SELE_SSOID);
				sPassedAt = "Loop 1";
			}

			if (sLoginUserId == null || sLoginUserId.trim().length() == 0) {
				if (sLoginUserId == null && request.getHeader(QuotationConstants.QUOTATION_REQHEADER_UID) != null) {
					LoggerUtility.log("INFO", sClassName, sMethodName, "User SSO retrieved with - " + QuotationConstants.QUOTATION_REQHEADER_UID);
					sLoginUserId = request.getHeader(QuotationConstants.QUOTATION_REQHEADER_UID);
					sPassedAt = "Loop 2";
				}
				if (sLoginUserId == null || sLoginUserId.trim().length() == 0) {
					if (request.getHeader(QuotationConstants.QUOTATION_REQHEADER_SSOID) != null) {
						LoggerUtility.log("INFO", sClassName, sMethodName, "User SSO retrieved with - " + QuotationConstants.QUOTATION_REQHEADER_SSOID);
						sLoginUserId = request.getHeader(QuotationConstants.QUOTATION_REQHEADER_SSOID);
						sPassedAt = "Loop 3";
					}
				}
				if (sLoginUserId == null || sLoginUserId.trim().length() == 0) {
					if (request.getHeader(QuotationConstants.QUOTATION_REQHEADER_SMUSER) != null) {
						LoggerUtility.log("INFO", sClassName, sMethodName, "User SSO retrieved with - " + QuotationConstants.QUOTATION_REQHEADER_SMUSER);
						sLoginUserId = request.getHeader(QuotationConstants.QUOTATION_REQHEADER_SMUSER);
						sPassedAt = "Loop 4";
					}
				}
				if (sLoginUserId == null || sLoginUserId.trim().length() == 0) {
					if (request.getSession().getAttribute(QuotationConstants.QUOTAION_SELE_SSOID) == null) {
						sLoginUserId = PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_SSOID);
						sPassedAt = "Loop 5 : from properties file";
						LoggerUtility.log("INFO", sClassName, sMethodName, "User information not found in request. Setting default user - " + sLoginUserId);
					} else {
						sLoginUserId = (String) request.getSession().getAttribute(QuotationConstants.QUOTAION_SELE_SSOID);
						sPassedAt = "Loop 6 : session";
						LoggerUtility.log("INFO", sClassName, sMethodName, "User information not found in request. Retrieving the user information selected locally - " + sLoginUserId);
					}
				}
			}
			//System.out.println("sLoginUserId  set value Passed at : " + sPassedAt);
			//System.out.println("sLoginUserId  : " + sLoginUserId);
		} catch (Exception ex) {
			LoggerUtility.log("DEBUG", "BaseAction", "getLoginUserSSOId", ex.getMessage());
			
		} finally {

		}
		return sLoginUserId;
	}
    
    
    public static UserDto getUserInfo(String sUserSSOId, String sServiceURL)
			throws Exception {
		UserDto oUserDto = null;
		LoggerUtility.log("INFO", "BaseAction", "getUserInfo", "START");

		if (sUserSSOId.trim().length() > 0) {

			// now start the process.
			URL url = new URL(sServiceURL);
			Service service = new Service();
			
			Call call = (Call) service.createCall();
			call.setTargetEndpointAddress(url);
			call.setOperationName(new QName("AttachmentService", "getUserDetails"));

			QName qn = new QName("urn:"+CommonUtility.getStringValue(sServiceURL), "UserDto");
			QName bean_qn = new QName("myNS:UserDto", "UserDto");

			call.addParameter("userId", org.apache.axis.encoding.XMLType.XSD_STRING, ParameterMode.IN);

			call.registerTypeMapping(UserDto.class, qn, new org.apache.axis.encoding.ser.BeanSerializerFactory(UserDto.class, qn),
														new org.apache.axis.encoding.ser.BeanDeserializerFactory(UserDto.class, qn));

			call.setReturnType(bean_qn, UserDto.class);

			oUserDto = (UserDto) call.invoke(new Object[] {sUserSSOId});
		}
		
		return oUserDto;
	}
    
    
}
