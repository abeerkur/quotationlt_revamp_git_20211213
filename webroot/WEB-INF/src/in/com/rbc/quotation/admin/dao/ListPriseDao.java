package in.com.rbc.quotation.admin.dao;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Hashtable;

import in.com.rbc.quotation.admin.dto.ListPriceBulkLoadDto;
import in.com.rbc.quotation.admin.dto.ListPriceDto;
import in.com.rbc.quotation.common.utility.ListModel;
import in.com.rbc.quotation.common.vo.KeyValueVo;

public interface ListPriseDao {
	
	/**
	 * 
	 * @param conn
	 * @param oListPriseDto
	 * @param oListModel
	 * @return
	 * @throws Exception
	 * @author 900008798 (Abhilash Moola)
	 * Created on: Dec 03, 2020
	 */
	public Hashtable getListPriseSearchResult(Connection conn, ListPriceDto oListPriceDto, ListModel oListModel) throws Exception ;
	
	 /**
		 * Gets all the look up values
		 * @param conn Connection object to connect to database
		 * @param oListPriceDto ListPriceDto object having enquiry details
		 * @return Returns the ListPriceDto object
		 * @throws Exception If any error occurs during the process
		 * @author 900008798 (Abhilash Moola)
		 * Created on: Dec 3, 2020
		 */
	 public ListPriceDto loadLookUpValues(Connection conn, ListPriceDto oListPriceDto) throws Exception ;
	 
	 /**
		 * Gets all the look up values
		 * @param conn Connection object to connect to database
		 * @param oListPriceDto ListPriceDto object having enquiry details
		 * @return Returns the ListPriceDto object
		 * @throws Exception If any error occurs during the process
		 * @author 900008798 (Abhilash Moola)
		 * Created on: Dec 3, 2020
		 */
	 public boolean addListPrice(Connection conn, ListPriceDto oListPriceDto) throws Exception;
	 
	 /**
		 * Gets all the look up values
		 * @param conn Connection object to connect to database
		 * @param oListPriceDto ListPriceDto object having enquiry details
		 * @return Returns the ListPriceDto object
		 * @throws Exception If any error occurs during the process
		 * @author 900008798 (Abhilash Moola)
		 * Created on: Dec 3, 2020
		 */
	 public boolean updateListPrice(Connection conn, ListPriceDto oListPriceDto) throws Exception ;
	 
	 /**
		 * Gets all the look up values
		 * @param conn Connection object to connect to database
		 * @param oListPriceDto ListPriceDto object having enquiry details
		 * @return Returns the ListPriceDto object
		 * @throws Exception If any error occurs during the process
		 * @author 900008798 (Abhilash Moola)
		 * Created on: Dec 3, 2020
		 */
	 public ListPriceDto getListPrice(Connection conn, ListPriceDto oListPriceDto) throws Exception;
	 
	 /**
		 * Gets all the look up values
		 * @param conn Connection object to connect to database
		 * @param oListPriceDto ListPriceDto object having enquiry details
		 * @return Returns the ListPriceDto object
		 * @throws Exception If any error occurs during the process
		 * @author 900008798 (Abhilash Moola)
		 * Created on: Dec 3, 2020
		 */
	 public boolean deleteListPrice(Connection conn, ListPriceDto oListPriceDto) throws Exception;
	 

	 /**
	  * This Method is used for Fetching the List Price - Based on Selected Parameters in Enquiry Page
	  * @param conn
	  * @param oListPriceDto
	  * @return
	  * @throws Exception
	  */
	 public ListPriceDto fetchListPrice(Connection conn, ListPriceDto oListPriceDto) throws Exception;
	 
	 /**
	  * This Method is used to fetch the list of values in Ascending Order from ENQUIRY PICKLIST table for given lookup field
	  * @param conn Connection object to connect to database
	  * @param lookupfield string - query parameter
	  * @return Returns ArrayList object
	  * @throws Exception If any error occurs during the process
	  */
	 public ArrayList<KeyValueVo> getAscendingOptionsbyAttributeList(Connection conn,String sLookupField) throws Exception;
	 
	 /**
	  * This Method is used to check if List Price value already Exists in DB or not (For specific List Price Params).
	  * @param conn Connection object to connect to database
	  * @param oListPriceBulkLoadDto ListPriceBulkLoadDto that contains the Price Value and related parameters.
	  * @return Return KeyValueVo - To Indicate if ListPrice with matching parameters is already present or not.
	  * @throws Exception If any error occurs during the process
	  */
	 public KeyValueVo isListPriceExists(Connection conn, ListPriceBulkLoadDto oListPriceBulkLoadDto) throws Exception;
	 
	 /**
	  * 
	  * @param conn Connection object to connect to database
	  * @param oListPriseDto ListPriceDto object that contains the required values.
	  * @param sWhereQuery String where condition Query.
	  * @param sSortFilter String Sort Filter parameter.
	  * @param sSortOrder String Sort Order value.
	  * @return Returns ArrayList of ListPrice records, that would be exported to Excel
	  * @throws Exception If any error occurs during the process
	  */
	 public ArrayList exportListPriceSearchResult(Connection conn, ListPriceDto oListPriseDto, String sWhereQuery, String sSortFilter, String sSortOrder) throws Exception;
	 
} 
