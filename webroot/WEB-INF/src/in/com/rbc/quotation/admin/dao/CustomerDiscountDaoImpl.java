package in.com.rbc.quotation.admin.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Hashtable;

import org.apache.commons.lang.StringUtils;

import in.com.rbc.quotation.admin.dto.CustomerDiscountBulkLoadDto;
import in.com.rbc.quotation.admin.dto.CustomerDiscountDto;
import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.utility.DBUtility;
import in.com.rbc.quotation.common.utility.ListDBColumnUtil;
import in.com.rbc.quotation.common.utility.ListModel;
import in.com.rbc.quotation.common.utility.LoggerUtility;
import in.com.rbc.quotation.common.vo.KeyValueVo;
import in.com.rbc.quotation.enquiry.dao.EnquiryQueries;
import in.com.rbc.quotation.factory.DaoFactory;

public class CustomerDiscountDaoImpl implements CustomerDiscountDao,AdminQueries,EnquiryQueries {
	
	public Hashtable getCustomerDiscountSearchResult(Connection conn, CustomerDiscountDto oCustomerDiscountDto, ListModel oListModel) throws Exception
	{
		/*
    	* Setting the Method Name as generic for logger Utility purpose . 
    	*/
		String sMethodName = "getCustomerDiscountSearchResult Method" ;
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null;
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null;      
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
       
        /*
         * Hashtable that stores the following values, and that is used to display results in search page
         * --> ArrayList containing the rows retrieved from database
         * --> ListModel object that hold the values used to retrieve values and display the same
         * --> String array containing the list of column names to be displayed as column headings
         */
        Hashtable htSearchResults = null;
        
        ArrayList alSearchResultsList = new ArrayList(); // Stores the list of rows retrieved from the database
        
        int iCursorPosition = 0; // Stores the cursor position from where the records have to be retrieved // from database; this is dependent on the page number selected for viewing
        
        String sWhereString = null; // Stores the Where clause condition
        String sSqlQueryString = null; // Stores the complete SQL query to be executed
        CustomerDiscountDto oTempCustomerDiscountDto = null ;// Create the Temporary Location Object for storing the table column values 
        int iSalesMgrId = 0;
        
        try {
        	
        	sWhereString = buildWhereClause(oCustomerDiscountDto);
        	sSqlQueryString = COUNT_CUSTOMERDISCOUNT_SEARCHRESULT + sWhereString;
        	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName + "..Search getCustomerDiscountSearchResult data Count Query",sSqlQueryString);
        	
        	pstmt = conn.prepareCall(sSqlQueryString);
        	
        	/* Execute the Query for counting the number records*/
        	rs = pstmt.executeQuery();
        	
        	if(rs.next()) {
        		oListModel.setTotalRecordCount(rs.getInt(1));//set total number of records available in table depond upon the where conditions .
        	} else {
        		oListModel.setTotalRecordCount(0);// if the Query return no records , it's set the Total recordcount  "0" as default
        	}
        	oDBUtility.releaseResources(rs, pstmt);
        	 
        	if(oListModel.getTotalRecordCount()>0) {
        		htSearchResults = new Hashtable();
        		 
        		if(oListModel.getCurrentPageInt()<0)
        			throw new Exception("Current Page is negative in search method...");
        		 
        		iCursorPosition = (oListModel.getCurrentPageInt()-1) * oListModel.getRecordsPerPageInt();
        		 
        		if(iCursorPosition < oListModel.getTotalRecordCount()) {
             		
					/*
					 * Select the values from the table Using FETCH_SEARCHLocation_RESULTS Query depond upon the 
					 * sWhereString condition and return into the sSqlQueryString as StringBuffer 
					 * and the data's are ordered by Location DBFiled as by default
					 * 
					 */
        			sSqlQueryString = new StringBuffer("SELECT * FROM (SELECT Q.*, rownum AS rnum FROM ( ")
 						                    .append(FETCH_CUSTOMERDISCOUNT_LIST)
 						                    .append(sWhereString)
 						                    .append(" ORDER BY ")
 						                    .append(ListDBColumnUtil.getTechnicalDataResultDBField(oListModel.getSortBy()))
 						                    .append(" " + oListModel.getSortOrderDesc())
 						                    .append(" ) Q WHERE rownum <= (")
 						                    .append(iCursorPosition + " + " + oListModel.getRecordsPerPage() + ")) ")
 						                    .append(" WHERE rnum > " + iCursorPosition + "")
 						                    .toString();
        			
             		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName+"..SearchLocation Retrive Query",sSqlQueryString );
             		pstmt = conn.prepareStatement(sSqlQueryString, ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
             		
             		rs = pstmt.executeQuery(); // Execute the Search Query to fetch Cust. Discount Values .
             		if (rs.next()) {
                         do {
                        	 oTempCustomerDiscountDto = new CustomerDiscountDto();
                        	 oTempCustomerDiscountDto.setSerial_id(Integer.parseInt(rs.getString("SERIAL_ID")));
                        	 oTempCustomerDiscountDto.setManufacturing_location(getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_MFG_LOCATION, rs.getString("MANUFACTURING_LOCATION")));
                        	 oTempCustomerDiscountDto.setProduct_group(getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_PRODGROUP, rs.getString("PRODUCT_GROUP")));	
                        	 oTempCustomerDiscountDto.setProduct_line(getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_PRODLINE, rs.getString("PRODUCT_LINE")));	
                        	 oTempCustomerDiscountDto.setStandard(rs.getString("STANDARD"));	
                        	 oTempCustomerDiscountDto.setPower_rating_kw(getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_KW, rs.getString("POWER_RATING_KW")));
                        	 oTempCustomerDiscountDto.setFrame_type(getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_FRAME, rs.getString("FRAME_TYPE")));	
                        	 oTempCustomerDiscountDto.setNumber_of_poles(Integer.parseInt(getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_POLE, rs.getString("NUMBER_OF_POLES"))));
                        	 oTempCustomerDiscountDto.setMounting_type(getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_MOUNTING, rs.getString("MOUNTING_TYPE")));
                        	 oTempCustomerDiscountDto.setTb_position(getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_TBPOS, rs.getString("TB_POSITION")));
                        	 oTempCustomerDiscountDto.setCustomer_name(rs.getString("QCU_NAME"));
                        	 oTempCustomerDiscountDto.setSapcode(rs.getString("SAPCODE"));
                        	 oTempCustomerDiscountDto.setOraclecode(rs.getString("ORACLECODE"));
                        	 oTempCustomerDiscountDto.setGstnumber(rs.getString("GSTNUMBER"));
                        	 if(StringUtils.isNotBlank(rs.getString("SALESENGINEERID"))) {
         						iSalesMgrId = Integer.parseInt(rs.getString("SALESENGINEERID"));
         					 } else {
         						iSalesMgrId = 0;
         					 }
                        	 oTempCustomerDiscountDto.setSalesengineer(getSalesManagerById(conn, iSalesMgrId));
                        	 oTempCustomerDiscountDto.setCustomer_discount(rs.getString("CUSTOMER_DISCOUNT"));
                        	 oTempCustomerDiscountDto.setSm_discount(rs.getString("SM_DISCOUNT"));
                        	 oTempCustomerDiscountDto.setRsm_discount(rs.getString("RSM_DISCOUNT"));
                        	 oTempCustomerDiscountDto.setNsm_discount(rs.getString("NSM_DISCOUNT"));
                        	 
                        	 alSearchResultsList.add(oTempCustomerDiscountDto);
                        	 
                        } while (rs.next());
                     }
               }
        		 
        		 htSearchResults.put("resultsList", alSearchResultsList);// put the SearchresultList of arraylist value into Hashtable 
                 htSearchResults.put("ListModel", oListModel); // put the ListModel values into hashtable 
                 htSearchResults.put("custDiscountQuery", sWhereString);//keep the Where condition for using in Export to Excel purpose .
             }
        	 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END.." );
        }finally {
        	 /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
        return htSearchResults ;
		
		
	}
	
	private String buildWhereClause(CustomerDiscountDto oCustomerDiscountDto) {
		
		String sMethodName = "buildWhereClause";
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
        
        StringBuffer sbWhereString = new StringBuffer(" AND");
        
        // Customer ID
        if(oCustomerDiscountDto.getCustomerId() != null && oCustomerDiscountDto.getCustomerId().trim().length() > 0) {
        	sbWhereString.append(" CD.CUSTOMERID = '" + oCustomerDiscountDto.getCustomerId() + "'");
        	sbWhereString.append(" AND");
        }
        
        // Manufacturing Location 
        if (oCustomerDiscountDto.getManufacturing_location() != null && oCustomerDiscountDto.getManufacturing_location().trim().length() > 0 && !QuotationConstants.QUOTATION_ZERO.equals(oCustomerDiscountDto.getManufacturing_location().trim()) ){
        	sbWhereString.append(" CD.MANUFACTURING_LOCATION = '"+ oCustomerDiscountDto.getManufacturing_location() + "'");
        	sbWhereString.append(" AND");
        }
        
        // Product Group
        if (oCustomerDiscountDto.getProduct_group() != null && oCustomerDiscountDto.getProduct_group().trim().length() > 0 && !QuotationConstants.QUOTATION_ZERO.equals(oCustomerDiscountDto.getProduct_group().trim()) ){
        	sbWhereString.append(" CD.PRODUCT_GROUP = '" + oCustomerDiscountDto.getProduct_group().trim().toUpperCase() +"'");
        	sbWhereString.append(" AND");
        }
        
        // Product Line
        if (oCustomerDiscountDto.getProduct_line() != null && oCustomerDiscountDto.getProduct_line().trim().length() > 0 && !QuotationConstants.QUOTATION_ZERO.equals(oCustomerDiscountDto.getProduct_line().trim()) ){
        	sbWhereString.append(" CD.PRODUCT_LINE = '" + oCustomerDiscountDto.getProduct_line().trim().toUpperCase() + "'");
        	sbWhereString.append(" AND");
        }
        
        // Frame Type
        if (oCustomerDiscountDto.getFrame_type() != null && oCustomerDiscountDto.getFrame_type().trim().length() > 0 && !QuotationConstants.QUOTATION_ZERO.equals(oCustomerDiscountDto.getFrame_type().trim()) ){
        	sbWhereString.append(" CD.FRAME_TYPE = '" + oCustomerDiscountDto.getFrame_type().trim().toUpperCase() + "'");
        	sbWhereString.append(" AND");
        }
        
        // Power Rating (KW)
        if (oCustomerDiscountDto.getPower_rating_kw() != null && oCustomerDiscountDto.getPower_rating_kw().trim().length() > 0 && !QuotationConstants.QUOTATION_ZERO.equals(oCustomerDiscountDto.getPower_rating_kw().trim()) ){
        	sbWhereString.append(" CD.POWER_RATING_KW = '" + oCustomerDiscountDto.getPower_rating_kw().trim().toUpperCase() + "'");
        	sbWhereString.append(" AND");
        }
        
        if ( sbWhereString.toString().trim().equalsIgnoreCase(" AND") )
            return "";
        else if (sbWhereString.toString().trim().endsWith(" AND"))        	
            return sbWhereString.toString().substring(0, sbWhereString.toString().lastIndexOf("AND"));        
        else
        	return "";
		
	}

	public CustomerDiscountDto loadLookUpValues(Connection conn, CustomerDiscountDto oCustomerDiscountDto) throws Exception {
		oCustomerDiscountDto.setMaufacturingList(getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_MFG_LOCATION));
		oCustomerDiscountDto.setProductGroupList(getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LT_PRODGROUP));
		oCustomerDiscountDto.setProductLineList(getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LT_PRODLINE));
		oCustomerDiscountDto.setStandardList(getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_STANDARD));
		oCustomerDiscountDto.setPowerratingkwList(getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LT_KW));
		oCustomerDiscountDto.setFrametypelist(getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LT_FRAME));
		oCustomerDiscountDto.setNumberofpoleslist(getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LT_POLE));
		oCustomerDiscountDto.setMountingyypelist(getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LT_MOUNTING));
		oCustomerDiscountDto.setTbpositionlist(getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LT_TBPOS));

		return oCustomerDiscountDto;
	}
	
	public ArrayList<KeyValueVo> getOptionsbyAttributeListByLookup(Connection conn,String sLookupField) throws Exception {

		/**
		 * Setting the Method Name as generic for logger Utility purpose .
		 */
		String sMethodName = "getOptionsbyAttributeListByLookup";
		String sClassName = this.getClass().getName();

		LoggerUtility.log("INFO", sClassName, sMethodName,
				"************************START******************");

		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null;

		/* ResultSet object to store the rows retrieved from database */
		ResultSet rs = null;

		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();

		ArrayList<KeyValueVo> arlOptions = new ArrayList<KeyValueVo>();		
		KeyValueVo oKeyValueVo = null;

		try {
			
			switch(sLookupField) {
			case "MANUFACTURING_LOCATION":
			pstmt = conn.prepareStatement(FETCH_TD_MANUFACTURING);
			rs = pstmt.executeQuery();
			if (rs.next()) {				
				do {
					oKeyValueVo = new KeyValueVo();
					oKeyValueVo.setKey(rs.getString(sLookupField));
					oKeyValueVo.setValue(rs.getString(sLookupField));
					arlOptions.add(oKeyValueVo);
				} while (rs.next());
			}
			break;
			
			case "PRODUCT_GROUP":
				pstmt = conn.prepareStatement(FETCH_TD_PRODUCT_GROUP);
				rs = pstmt.executeQuery();
				if (rs.next()) {				
					do {
						oKeyValueVo = new KeyValueVo();
						oKeyValueVo.setKey(rs.getString(sLookupField));
						oKeyValueVo.setValue(rs.getString(sLookupField));
						arlOptions.add(oKeyValueVo);
					} while (rs.next());
				}
				break;
				
			case "PRODUCT_LINE":
				pstmt = conn.prepareStatement(FETCH_TD_PRODUCT_LINE);
				rs = pstmt.executeQuery();
				if (rs.next()) {				
					do {
						oKeyValueVo = new KeyValueVo();
						oKeyValueVo.setKey(rs.getString(sLookupField));
						oKeyValueVo.setValue(rs.getString(sLookupField));
						arlOptions.add(oKeyValueVo);
					} while (rs.next());
				}
				break;
				
			case "STANDARD":
				pstmt = conn.prepareStatement(FETCH_TD_STANDARD);
				rs = pstmt.executeQuery();
				if (rs.next()) {				
					do {
						oKeyValueVo = new KeyValueVo();
						oKeyValueVo.setKey(rs.getString(sLookupField));
						oKeyValueVo.setValue(rs.getString(sLookupField));
						arlOptions.add(oKeyValueVo);
					} while (rs.next());
				}
				break;
				
			case "POWER_RATING_KW":
				pstmt = conn.prepareStatement(FETCH_TD_POWER_RATING_KW);
				rs = pstmt.executeQuery();
				if (rs.next()) {				
					do {
						oKeyValueVo = new KeyValueVo();
						oKeyValueVo.setKey(rs.getString(sLookupField));
						oKeyValueVo.setValue(rs.getString(sLookupField));
						arlOptions.add(oKeyValueVo);
					} while (rs.next());
				}
				break;
				
				case "FRAME_TYPE":
					pstmt = conn.prepareStatement(FETCH_TD_FRAME_TYPE);
					rs = pstmt.executeQuery();
					if (rs.next()) {				
						do {
							oKeyValueVo = new KeyValueVo();
							oKeyValueVo.setKey(rs.getString(sLookupField));
							oKeyValueVo.setValue(rs.getString(sLookupField));
							arlOptions.add(oKeyValueVo);
						} while (rs.next());
					}
					break;
					
				case "NUMBER_OF_POLES":
					pstmt = conn.prepareStatement(FETCH_TD_NUMBER_OF_POLES);
					rs = pstmt.executeQuery();
					if (rs.next()) {				
						do {
							oKeyValueVo = new KeyValueVo();
							oKeyValueVo.setKey(rs.getString(sLookupField));
							oKeyValueVo.setValue(rs.getString(sLookupField));
							arlOptions.add(oKeyValueVo);
						} while (rs.next());
					}
					break;
					
				case "MOUNTING_TYPE":
					pstmt = conn.prepareStatement(FETCH_TD_MOUNTING_TYPE);
					rs = pstmt.executeQuery();
					if (rs.next()) {				
						do {
							oKeyValueVo = new KeyValueVo();
							oKeyValueVo.setKey(rs.getString(sLookupField));
							oKeyValueVo.setValue(rs.getString(sLookupField));
							arlOptions.add(oKeyValueVo);
						} while (rs.next());
					}
					break;
					
				case "TB_POSITION":
					pstmt = conn.prepareStatement(FETCH_TD_TB_POSITION);
					rs = pstmt.executeQuery();
					if (rs.next()) {				
						do {
							oKeyValueVo = new KeyValueVo();
							oKeyValueVo.setKey(rs.getString(sLookupField));
							oKeyValueVo.setValue(rs.getString(sLookupField));
							arlOptions.add(oKeyValueVo);
						} while (rs.next());
					}
					break;
			case "":
				break;
			}
			
		} finally {
			/* Releasing ResultSet & PreparedStatement objects */
			oDBUtility.releaseResources(rs, pstmt);
		}

		LoggerUtility.log("INFO", sClassName, sMethodName,
				"************************END******************");

		return arlOptions;

	}
	
	public ArrayList getCustomerNamesList(Connection conn)throws Exception{
		/*
    	 * Setting the Method Name as generic for logger Utility purpose . 
    	 */
    		String sMethodName = "getCustomerNamesList" ;
    		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
    		
    		/* PreparedStatement object to handle database operations */
            	PreparedStatement pstmt = null ;
            
            /* ResultSet object to store the rows retrieved from database */
            	ResultSet rs = null ;      
            
            /* Object of DBUtility class to handle database operations */
            	DBUtility oDBUtility = new DBUtility();
            /*
             * Querying the database to retrieve the cusomter description, storing the
             * details in KeyValueVo object, storing these object in an ArrayList 
             * and returning the same
             */
            	KeyValueVo oKeyValueVo = null;
            	String sSqlQuery=null;
           
             /* ArrayList to store the list of request status */
            	ArrayList alSearchResultsList = new ArrayList(); // Stores the list of rows retrieved from the database
            try {
            	sSqlQuery=FETCH_CUSTOMERNAME_LIST;
            	pstmt = conn.prepareStatement(sSqlQuery);
            	rs = pstmt.executeQuery();
            	while(rs.next()) {
            		oKeyValueVo = new KeyValueVo();
            		oKeyValueVo.setKey(rs.getString("QCU_CUSTOMERID"));
            		oKeyValueVo.setValue(rs.getString("QCU_NAME"));
            		alSearchResultsList.add(oKeyValueVo);
            	}
            	
            }finally {
                /* Releasing ResultSet & PreparedStatement objects */
            	oDBUtility.releaseResources(rs,pstmt);
            }
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );		
		return alSearchResultsList;
	}
	
	public ArrayList getSapcodelist(Connection conn)throws Exception
	{
		/*
    	 * Setting the Method Name as generic for logger Utility purpose . 
    	 */
    		String sMethodName = "getSapcodelist" ;
    		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
    		
    		/* PreparedStatement object to handle database operations */
            	PreparedStatement pstmt = null ;
            
            /* ResultSet object to store the rows retrieved from database */
            	ResultSet rs = null ;      
            
            /* Object of DBUtility class to handle database operations */
            	DBUtility oDBUtility = new DBUtility();
            /*
             * Querying the database to retrieve the cusomter description, storing the
             * details in KeyValueVo object, storing these object in an ArrayList 
             * and returning the same
             */
            	KeyValueVo oKeyValueVo = null;
            	String sSqlQuery=null;
           
             /* ArrayList to store the list of request status */
            	ArrayList alSearchResultsList = new ArrayList(); // Stores the list of rows retrieved from the database
            try {
            	sSqlQuery=FETCH_SAPCODE_LIST;
            	pstmt = conn.prepareStatement(sSqlQuery);
            	rs = pstmt.executeQuery();
            	while(rs.next()) {
            		if(rs.getString("QCU_SAPCODE")!=null && rs.getString("QCU_SAPCODE")!="") {
            			oKeyValueVo = new KeyValueVo();
                		oKeyValueVo.setKey(rs.getString("QCU_SAPCODE"));
                		oKeyValueVo.setValue(rs.getString("QCU_SAPCODE"));
                		alSearchResultsList.add(oKeyValueVo);
            		}
            		
            	}
            	
            }finally {
                /* Releasing ResultSet & PreparedStatement objects */
            	oDBUtility.releaseResources(rs,pstmt);
            }
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );		
		return alSearchResultsList;
	}
	
	
	public ArrayList getOraclecodelist(Connection conn)throws Exception
	{
		/*
    	 * Setting the Method Name as generic for logger Utility purpose . 
    	 */
    		String sMethodName = "getOraclecodelist" ;
    		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
    		
    		/* PreparedStatement object to handle database operations */
            	PreparedStatement pstmt = null ;
            
            /* ResultSet object to store the rows retrieved from database */
            	ResultSet rs = null ;      
            
            /* Object of DBUtility class to handle database operations */
            	DBUtility oDBUtility = new DBUtility();
            /*
             * Querying the database to retrieve the cusomter description, storing the
             * details in KeyValueVo object, storing these object in an ArrayList 
             * and returning the same
             */
            	KeyValueVo oKeyValueVo = null;
            	String sSqlQuery=null;
           
             /* ArrayList to store the list of request status */
            	ArrayList alSearchResultsList = new ArrayList(); // Stores the list of rows retrieved from the database
            try {
            	sSqlQuery=FETCH_ORACLECODE_LIST;
            	pstmt = conn.prepareStatement(sSqlQuery);
            	rs = pstmt.executeQuery();
            	while(rs.next()) {
            		if(rs.getString("QCU_ORACLECODE")!=null && rs.getString("QCU_ORACLECODE")!="") {
            			oKeyValueVo = new KeyValueVo();
                		oKeyValueVo.setKey(rs.getString("QCU_ORACLECODE"));
                		oKeyValueVo.setValue(rs.getString("QCU_ORACLECODE"));
                		alSearchResultsList.add(oKeyValueVo);
            		}
            		
            	}
            	
            }finally {
                /* Releasing ResultSet & PreparedStatement objects */
            	oDBUtility.releaseResources(rs,pstmt);
            }
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );		
		return alSearchResultsList;
	}
	
	public ArrayList getGstnumberlist(Connection conn)throws Exception
	{
		/*
    	 * Setting the Method Name as generic for logger Utility purpose . 
    	 */
    		String sMethodName = "getGstnumberlist" ;
    		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
    		
    		/* PreparedStatement object to handle database operations */
            	PreparedStatement pstmt = null ;
            
            /* ResultSet object to store the rows retrieved from database */
            	ResultSet rs = null ;      
            
            /* Object of DBUtility class to handle database operations */
            	DBUtility oDBUtility = new DBUtility();
            /*
             * Querying the database to retrieve the cusomter description, storing the
             * details in KeyValueVo object, storing these object in an ArrayList 
             * and returning the same
             */
            	KeyValueVo oKeyValueVo = null;
            	String sSqlQuery=null;
           
             /* ArrayList to store the list of request status */
            	ArrayList alSearchResultsList = new ArrayList(); // Stores the list of rows retrieved from the database
            try {
            	sSqlQuery=FETCH_GSTNUMBER_LIST;
            	pstmt = conn.prepareStatement(sSqlQuery);
            	rs = pstmt.executeQuery();
            	while(rs.next()) {
            		if(rs.getString("QCU_GSTNUMBER")!=null && rs.getString("QCU_GSTNUMBER")!="") {
            			oKeyValueVo = new KeyValueVo();
                		oKeyValueVo.setKey(rs.getString("QCU_GSTNUMBER"));
                		oKeyValueVo.setValue(rs.getString("QCU_GSTNUMBER"));
                		alSearchResultsList.add(oKeyValueVo);
            		}
            		
            	}
            	
            }finally {
                /* Releasing ResultSet & PreparedStatement objects */
            	oDBUtility.releaseResources(rs,pstmt);
            }
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );		
		return alSearchResultsList;
	}
	
	public ArrayList getSalesManagersList(Connection conn) throws Exception {

		String sMethodName = "getSalesManagersList";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");

		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null;
		/* ResultSet object to store the rows retrieved from database */
		ResultSet rs = null;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();
		
		/*
		 * Querying the database to retrieve the cusomter description, storing the
		 * details in KeyValueVo object, storing these object in an ArrayList and
		 * returning the same
		 */
		KeyValueVo oKeyValueVo = null;
		String sSqlQuery = null;

		/* ArrayList to store the list of request status */
		ArrayList alSearchResultsList = new ArrayList(); // Stores the list of rows retrieved from the database
		
		try {
			sSqlQuery = FETCH_SALESMANGER_LIST;
			pstmt = conn.prepareStatement(sSqlQuery);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				oKeyValueVo = new KeyValueVo();
				oKeyValueVo.setKey(rs.getString("QSM_SALESMGR"));
				oKeyValueVo.setValue(rs.getString("SALESMANAGER_NAME"));
				alSearchResultsList.add(oKeyValueVo);
			}

		} finally {
			/* Releasing ResultSet & PreparedStatement objects */
			oDBUtility.releaseResources(rs, pstmt);
		}
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
		return alSearchResultsList;
	}
	
	public String getSalesManagerById(Connection conn, int salesManagerId) throws Exception {
		String sMethodName = "getSalesManagersList";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");

		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null;
		/* ResultSet object to store the rows retrieved from database */
		ResultSet rs = null;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();
		
		/*
		 * Querying the database to retrieve the cusomter description, storing the
		 * details in KeyValueVo object, storing these object in an ArrayList and
		 * returning the same
		 */
		KeyValueVo oKeyValueVo = null;
		String sSqlQuery = null;
		
		String sSalesManagerName = "";
		
		try {
			sSqlQuery = FETCH_SALESMANGER_BY_ID;
			pstmt = conn.prepareStatement(sSqlQuery);
			pstmt.setInt(1, salesManagerId);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				sSalesManagerName = rs.getString("SALESMANAGER_NAME");
			}

		} finally {
			/* Releasing ResultSet & PreparedStatement objects */
			oDBUtility.releaseResources(rs, pstmt);
		}
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
		return sSalesManagerName;
	}
	
	public boolean addCustomerDiscount(Connection conn, CustomerDiscountDto oCustomerDiscountDto) throws Exception {
		
		String sMethodName = "addCustomerDiscount" ;
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
		boolean isUpdate = false;
		int serial_id = 0;
		
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null ;
		PreparedStatement pstmt1 = null ;
		/* ResultSet object to store the rows retrieved from database */
		ResultSet rs = null ;      
      
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();
		
		try {
			/**
			 * Getting the technical data serial id from the table
			 */
			pstmt1 = conn.prepareStatement(CREATE_CUSTOMERDISCOUNT_ID);
			rs = pstmt1.executeQuery();// Execute the creating the Query
      	
	      	if(rs.next()) {
	      		serial_id = rs.getInt("MAX_VAL");
	      	}
      	
	      	pstmt = conn.prepareStatement(INSERT_CUSTOMERDISCOUNT);
	      	pstmt.setInt(1, serial_id);
	      	pstmt.setString(2, oCustomerDiscountDto.getManufacturing_location());
	      	pstmt.setString(3, oCustomerDiscountDto.getProduct_group());
	      	pstmt.setString(4, oCustomerDiscountDto.getProduct_line());
	      	pstmt.setString(5, oCustomerDiscountDto.getStandard());
	      	pstmt.setString(6, oCustomerDiscountDto.getPower_rating_kw());
	      	pstmt.setString(7, oCustomerDiscountDto.getFrame_type());
	      	pstmt.setInt(8, oCustomerDiscountDto.getNumber_of_poles());
	      	pstmt.setString(9, oCustomerDiscountDto.getMounting_type());
	      	//pstmt.setString(10, oCustomerDiscountDto.getTb_position());
	      	pstmt.setString(10, oCustomerDiscountDto.getCustomer_name());
	      	pstmt.setString(11, oCustomerDiscountDto.getSapcode());
	      	pstmt.setString(12, oCustomerDiscountDto.getOraclecode());
	      	pstmt.setString(13, oCustomerDiscountDto.getGstnumber());
	      	pstmt.setString(14, String.valueOf(oCustomerDiscountDto.getSalesengineerid()));
	      	pstmt.setString(15, oCustomerDiscountDto.getCustomer_discount());
	      	pstmt.setString(16, oCustomerDiscountDto.getSm_discount());
	      	pstmt.setString(17, oCustomerDiscountDto.getRsm_discount());
	      	pstmt.setString(18, oCustomerDiscountDto.getNsm_discount());
	      	
	      	int iUpdatedCount = pstmt.executeUpdate();
	      	if(iUpdatedCount > 0 ) {
	    	   isUpdate = true;
	      	}
   	   
		} finally {
      		/* Releasing PreparedStatement objects */
          oDBUtility.releaseResources( rs,pstmt1);
          oDBUtility.releaseResources(pstmt);
		}
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );
      
		return isUpdate;
	}
	
	public boolean updateCustomerDiscount(Connection conn, CustomerDiscountDto oCustomerDiscountDto) throws Exception
	{
		/*
		 * Setting the Method Name as generic for logger Utility purpose . 
		 */
		String sMethodName = "updateCustomerDiscount" ;
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
		boolean isUpdate = false;
		
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null ;
     
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();
		
    	try {
	     	pstmt = conn.prepareStatement(UPDATE_CUSTOMERDISCOUNT);
	     	pstmt.setString(1, oCustomerDiscountDto.getManufacturing_location());
	     	pstmt.setString(2, oCustomerDiscountDto.getProduct_group());
	     	pstmt.setString(3, oCustomerDiscountDto.getProduct_line());
	     	pstmt.setString(4, oCustomerDiscountDto.getStandard());
	     	pstmt.setString(5, oCustomerDiscountDto.getPower_rating_kw());
	     	pstmt.setString(6, oCustomerDiscountDto.getFrame_type());
	     	pstmt.setInt(7, oCustomerDiscountDto.getNumber_of_poles());
	     	pstmt.setString(8, oCustomerDiscountDto.getMounting_type());
	     	pstmt.setString(9, oCustomerDiscountDto.getTb_position());
	     	pstmt.setString(10, oCustomerDiscountDto.getCustomer_name());
	     	pstmt.setString(11, oCustomerDiscountDto.getSapcode());
	     	pstmt.setString(12, oCustomerDiscountDto.getOraclecode());
	     	pstmt.setString(13, oCustomerDiscountDto.getGstnumber());
	     	pstmt.setString(14, String.valueOf(oCustomerDiscountDto.getSalesengineerid()));
	     	pstmt.setString(15, oCustomerDiscountDto.getCustomer_discount());
	     	pstmt.setString(16, oCustomerDiscountDto.getSm_discount());
	     	pstmt.setString(17, oCustomerDiscountDto.getRsm_discount());
	     	pstmt.setString(18, oCustomerDiscountDto.getNsm_discount());
	     	pstmt.setInt(19, oCustomerDiscountDto.getSerial_id());
	     	
	     	int iUpdatedCount = pstmt.executeUpdate();
	     	if(iUpdatedCount > 0 ) {
	     		isUpdate = true;
	     	}
  	   
    	} finally {
    		/* Releasing PreparedStatement objects */
    		oDBUtility.releaseResources(pstmt);
    	}
    	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );
     
		return isUpdate;
	}
	
	public CustomerDiscountDto getCustomerDiscount(Connection conn, CustomerDiscountDto oCustomerDiscountDto) throws Exception
	{
		/*
    	* Setting the Method Name as generic for logger Utility purpose . 
    	*/
		String sMethodName = "getCustomerDiscountSearchResult Mehotd" ;
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;      
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
       
        try {
             		pstmt = conn.prepareStatement(FETCH_CUSTOMERDISCOUNT);
             		pstmt.setInt(1, oCustomerDiscountDto.getSerial_id());
             		
             		rs =pstmt.executeQuery();
             		if (rs.next()) {
             			oCustomerDiscountDto.setSerial_id(rs.getInt("SERIAL_ID"));
             			oCustomerDiscountDto.setManufacturing_location(rs.getString("MANUFACTURING_LOCATION"));
             			oCustomerDiscountDto.setProduct_group(rs.getString("PRODUCT_GROUP"));	
             			oCustomerDiscountDto.setProduct_line(rs.getString("PRODUCT_LINE"));	
             			oCustomerDiscountDto.setStandard(rs.getString("STANDARD"));	
             			oCustomerDiscountDto.setPower_rating_kw(rs.getString("POWER_RATING_KW"));
             			oCustomerDiscountDto.setFrame_type(rs.getString("FRAME_TYPE"));	
             			oCustomerDiscountDto.setNumber_of_poles(rs.getInt("NUMBER_OF_POLES"));
             			oCustomerDiscountDto.setMounting_type(rs.getString("MOUNTING_TYPE"));
             			oCustomerDiscountDto.setTb_position(rs.getString("TB_POSITION"));
             			oCustomerDiscountDto.setCustomer_name(rs.getString("CUSTOMERID"));
             			oCustomerDiscountDto.setSapcode(rs.getString("SAPCODE"));
             			oCustomerDiscountDto.setOraclecode(rs.getString("ORACLECODE"));
             			oCustomerDiscountDto.setGstnumber(rs.getString("GSTNUMBER"));
             			oCustomerDiscountDto.setSalesengineer(rs.getString("SALESENGINEERID"));
             			oCustomerDiscountDto.setCustomer_discount(rs.getString("CUSTOMER_DISCOUNT"));
             			oCustomerDiscountDto.setSm_discount(rs.getString("SM_DISCOUNT"));
             			oCustomerDiscountDto.setRsm_discount(rs.getString("RSM_DISCOUNT"));
             			oCustomerDiscountDto.setNsm_discount(rs.getString("NSM_DISCOUNT"));
                        } 
        	 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END.." );
        }finally {
        	 /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
        return oCustomerDiscountDto ;
	}
	
	public boolean deleteCustomerDiscount(Connection conn, CustomerDiscountDto oCustomerDiscountDto) throws Exception
	{
		boolean isDeleted = false;
		/*
    	* Setting the Method Name as generic for logger Utility purpose . 
    	*/
		String sMethodName = "deleteCustomerDiscount Mehotd" ;
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        int iDelete=0;
        try {
             		pstmt = conn.prepareStatement(DELETE_CUSTOMERDISCOUNT);
             		pstmt.setInt(1, oCustomerDiscountDto.getSerial_id());
        			iDelete = pstmt.executeUpdate();
        	        	if(iDelete > 0) {
        	        		isDeleted = true ;
        	        	}
             		
        	 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END.." );
        }finally {
        	 /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(pstmt);
        }
        return isDeleted ;
	}

	public double fetchCustomerDiscount(Connection conn, CustomerDiscountDto  oCustomerDiscountDto) throws Exception {
		String sMethodName = "fetchCustomerDiscount" ;
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
		double customerDiscountValue = 0;
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;      
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
		try {
			if(QuotationConstants.QUOTATION_EMPTY.equals(oCustomerDiscountDto.getProduct_line()) 
					|| QuotationConstants.QUOTATION_EMPTY.equals(oCustomerDiscountDto.getFrame_type()) 
					|| oCustomerDiscountDto.getNumber_of_poles() == 0 
					|| QuotationConstants.QUOTATION_EMPTY.equals(oCustomerDiscountDto.getCustomerId()) ) {
				
				return customerDiscountValue;
				
			} else {
				pstmt = conn.prepareStatement(FETCH_CUSTOMERDISCOUNT_FOR_ENQUIRY);
				pstmt.setString(1, oCustomerDiscountDto.getProduct_line().trim());
        		pstmt.setString(2, oCustomerDiscountDto.getFrame_type().trim());
        		pstmt.setInt(3, oCustomerDiscountDto.getNumber_of_poles());
        		pstmt.setString(4, oCustomerDiscountDto.getCustomerId().trim());
        		
        		rs = pstmt.executeQuery();
        		
        		if (rs.next()) {
        			customerDiscountValue = Double.parseDouble(rs.getString("CUSTOMER_DISCOUNT").trim());
        		}
			}
		} finally {
        	/* Releasing PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
		
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );
		return customerDiscountValue;
	}
	
	public KeyValueVo fetchRSMDiscountApprovalLimit(Connection conn, CustomerDiscountDto oCustomerDiscountDto) throws Exception {
		String sMethodName = "fetchRSMDiscountApprovalLimit" ;
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;      
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        KeyValueVo oKeyValueVo = null;
        
        try {
        	pstmt = conn.prepareStatement(FETCH_APPROVAL_LIMIT);
    		pstmt.setString(1, oCustomerDiscountDto.getProduct_line().trim());
    		pstmt.setString(2, oCustomerDiscountDto.getFrame_type().trim());
    		pstmt.setString(3, oCustomerDiscountDto.getCustomerId().trim());
    		rs = pstmt.executeQuery();
    		if(rs.next()) {
        		oKeyValueVo = new KeyValueVo();
        		oKeyValueVo.setKey(rs.getString("RSM_DISCOUNT"));
        		oKeyValueVo.setValue(rs.getString("RSM_DISCOUNT"));
    		}
    		
        } finally {
            /* Releasing ResultSet & PreparedStatement objects */
        	oDBUtility.releaseResources(rs,pstmt);
        }
		
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );
		return oKeyValueVo;
	}
	
	public KeyValueVo fetchNSMDiscountApprovalLimit(Connection conn, CustomerDiscountDto oCustomerDiscountDto) throws Exception {
		String sMethodName = "fetchNSMDiscountApprovalLimit" ;
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;      
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        KeyValueVo oKeyValueVo = null;
        
        try {
        	pstmt = conn.prepareStatement(FETCH_APPROVAL_LIMIT);
    		pstmt.setString(1, oCustomerDiscountDto.getProduct_line().trim());
    		pstmt.setString(2, oCustomerDiscountDto.getFrame_type().trim());
    		pstmt.setString(3, oCustomerDiscountDto.getCustomerId().trim());
    		rs = pstmt.executeQuery();
    		if(rs.next()) {
        		oKeyValueVo = new KeyValueVo();
        		oKeyValueVo.setKey(rs.getString("NSM_DISCOUNT"));
        		oKeyValueVo.setValue(rs.getString("NSM_DISCOUNT"));
    		}
    		
        } finally {
            /* Releasing ResultSet & PreparedStatement objects */
        	oDBUtility.releaseResources(rs,pstmt);
        }
		
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );
		return oKeyValueVo;
	}
	
	/**
	 * To fetch the list of values in Ascending Order from ENQUIRY PICKLIST table for given lookup field
	 * @param conn Connection object to connect to database
	 * @param lookupfield string - query parameter
	 * @return Returns ArrayList object
	 * @throws Exception If any error occurs during the process
	 */
	public ArrayList<KeyValueVo> getAscendingOptionsbyAttributeList(Connection conn,String sLookupField) throws Exception {
		String sMethodName = "getAscendingOptionsbyAttributeList";
		String sClassName = this.getClass().getName();
		LoggerUtility.log("INFO", sClassName, sMethodName, "************************START******************");

		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null;
		/* ResultSet object to store the rows retrieved from database */
		ResultSet rs = null;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();

		ArrayList<KeyValueVo> arlOptions = new ArrayList<KeyValueVo>();		
		KeyValueVo oKeyValueVo = null;

		try {			
			pstmt = conn.prepareStatement(FETCH_ENQUIRY_LIST_ASC);
			pstmt.setString(1, sLookupField);			
			rs = pstmt.executeQuery();
			if (rs.next()) {				
				do {
					oKeyValueVo = new KeyValueVo();
					oKeyValueVo.setKey(rs.getString("QEM_KEY"));
					oKeyValueVo.setValue(rs.getString("QEM_VALUE"));
					arlOptions.add(oKeyValueVo);
				} while (rs.next());
			}
		} finally {
			/* Releasing ResultSet & PreparedStatement objects */
			oDBUtility.releaseResources(rs, pstmt);
		}

		LoggerUtility.log("INFO", sClassName, sMethodName, "************************END******************");
		return arlOptions;
	}

	public String getOptionsbyAttributeKey(Connection conn, String sLookupField, String sLookupKey) throws Exception {
		String sMethodName = "getOptionsbyAttributeKey";
		String sClassName = this.getClass().getName();

		LoggerUtility.log("INFO", sClassName, sMethodName, "************************START******************");
		
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null;
		/* ResultSet object to store the rows retrieved from database */
		ResultSet rs = null;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();
		ArrayList<KeyValueVo> arlOptions = new ArrayList<KeyValueVo>();		
		String sValue = "";
		
		try {
			pstmt = conn.prepareStatement(FETCH_ENQUIRY_LIST_BY_ATTRIBUTE_KEY);
			pstmt.setString(1, sLookupField);
			pstmt.setString(2, sLookupKey);
			rs = pstmt.executeQuery();
			
			if (rs.next()) {
				sValue = rs.getString("QEM_VALUE");
			}
		} finally {
			/* Releasing ResultSet & PreparedStatement objects */
			oDBUtility.releaseResources(rs, pstmt);
		}
		
		LoggerUtility.log("INFO", sClassName, sMethodName, "************************END******************");
		return sValue;
	}

	public String getOptionsbyAttributeValue(Connection conn, String sLookupField, String sLookupValue) throws Exception {
		String sMethodName = "getOptionsbyAttributeValue";
		String sClassName = this.getClass().getName();

		LoggerUtility.log("INFO", sClassName, sMethodName, "************************START******************");
		
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null;
		/* ResultSet object to store the rows retrieved from database */
		ResultSet rs = null;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();
		ArrayList<KeyValueVo> arlOptions = new ArrayList<KeyValueVo>();		
		String sValue = "";
		
		try {
			pstmt = conn.prepareStatement(FETCH_ENQUIRY_LIST_BY_ATTRIBUTE_VALUE);
			pstmt.setString(1, sLookupField);
			pstmt.setString(2, sLookupValue.trim());
			rs = pstmt.executeQuery();
			
			if (rs.next()) {
				sValue = rs.getString("QEM_KEY");
			}
		} finally {
			/* Releasing ResultSet & PreparedStatement objects */
			oDBUtility.releaseResources(rs, pstmt);
		}
		
		LoggerUtility.log("INFO", sClassName, sMethodName, "************************END******************");
		return sValue;
	}
	
	public KeyValueVo isCustomerDiscountExists(Connection conn, CustomerDiscountBulkLoadDto oCustomerDiscountBulkLoadDto, String sCustomerIdVal) throws Exception {
		LoggerUtility.log("INFO", this.getClass().getName(), "isCustomerDiscountExists", "Start");

		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null;
		/* ResultSet object to store the rows retrieved from database */
		ResultSet rs = null;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();

		KeyValueVo oKeyValueVo = null;

		try {
			DaoFactory oDaoFactory = new DaoFactory();
			CustomerDao oCustomerDao = oDaoFactory.getCustomerDao();
			
			pstmt = conn.prepareStatement(FETCH_CUSTOMERDISCOUNT_FOR_ENQUIRY);
			//pstmt.setString(1, getOptionsbyAttributeValue(conn, QuotationConstants.QUOTATION_QEM_LT_PRODGROUP, oCustomerDiscountBulkLoadDto.getProductGroup()));
			pstmt.setString(1, getOptionsbyAttributeValue(conn, QuotationConstants.QUOTATION_QEM_LT_PRODLINE, oCustomerDiscountBulkLoadDto.getProductLine()));
			pstmt.setString(2, getOptionsbyAttributeValue(conn, QuotationConstants.QUOTATION_QEM_LT_FRAME, oCustomerDiscountBulkLoadDto.getFrameType()));
			pstmt.setInt(3, Integer.parseInt(getOptionsbyAttributeValue(conn, QuotationConstants.QUOTATION_QEM_LT_POLE, String.valueOf(oCustomerDiscountBulkLoadDto.getNoOfPoles()))));
			pstmt.setString(4, sCustomerIdVal);
			rs = pstmt.executeQuery();
            if (rs.next()) {
            	oKeyValueVo = new KeyValueVo();
                oKeyValueVo.setKey(rs.getString("SERIAL_ID"));
                oKeyValueVo.setValue(rs.getString("CUSTOMER_DISCOUNT"));
            }
		} finally {
			/* Releasing ResultSet & PreparedStatement objects */
			oDBUtility.releaseResources(rs, pstmt);
		}
		
		LoggerUtility.log("INFO", this.getClass().getName(), "isCustomerDiscountExists", "End");
        return oKeyValueVo;
	}

	public ArrayList exportCustDiscountSearchResult(Connection conn, CustomerDiscountDto oCustomerDiscountDto, String sWhereQuery, String sSortFilter, String sSortOrder) throws Exception {
		
		LoggerUtility.log("INFO", this.getClass().getName(), "exportCustDiscountSearchResult", "Start");
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;      
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        ArrayList alSearchResultsList = new ArrayList(); 		// Stores the list of rows retrieved from the database
        String sSqlQueryString = null; 							// Stores the complete SQL query to be executed
        CustomerDiscountDto oTempCustomerDiscountDto = null ;	// Create the Temporary Location Object for storing the table column values
        int iSalesMgrId = 0;
        
        try {
        	sSqlQueryString = FETCH_CUSTDISCOUNT_LIST;
        	
			if (sWhereQuery != null) {
				sSqlQueryString = sSqlQueryString + " " + sWhereQuery;
			}
			if (sSortFilter != null && sSortFilter.trim().length() > 0) {
				sSqlQueryString = sSqlQueryString + " ORDER BY " + sSortFilter;
				if (sSortOrder != null && sSortOrder.trim().length() > 0)
					sSqlQueryString = sSqlQueryString + " " + sSortOrder;
			}
            
        	LoggerUtility.log("INFO", this.getClass().getName(), "exportCustDiscountSearchResult : "+"..Search CustDiscount Count Query in Expert..", sSqlQueryString+" : WHERE QUERY : "+sWhereQuery);
        	
        	pstmt = conn.prepareCall(sSqlQueryString);
        	rs = pstmt.executeQuery();// Execute the Search Query to retriuve the Location Values 
        	
        	if (rs.next()) {
				do {
					oTempCustomerDiscountDto = new CustomerDiscountDto();
					oTempCustomerDiscountDto.setSerial_id(Integer.parseInt(rs.getString("SERIAL_ID")));
					oTempCustomerDiscountDto.setManufacturing_location(getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_MFG_LOCATION, rs.getString("MANUFACTURING_LOCATION")));
					oTempCustomerDiscountDto.setProduct_group(getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_PRODGROUP, rs.getString("PRODUCT_GROUP")));
					oTempCustomerDiscountDto.setProduct_line(getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_PRODLINE, rs.getString("PRODUCT_LINE")));
					oTempCustomerDiscountDto.setStandard(rs.getString("STANDARD"));
					oTempCustomerDiscountDto.setPower_rating_kw(getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_KW, rs.getString("POWER_RATING_KW")));
					oTempCustomerDiscountDto.setFrame_type(getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_FRAME, rs.getString("FRAME_TYPE")));
					oTempCustomerDiscountDto.setNumber_of_poles(Integer.parseInt(getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_POLE, rs.getString("NUMBER_OF_POLES"))));
					oTempCustomerDiscountDto.setMounting_type(getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_MOUNTING, rs.getString("MOUNTING_TYPE")));
					oTempCustomerDiscountDto.setTb_position(getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_TBPOS, rs.getString("TB_POSITION")));
					oTempCustomerDiscountDto.setCustomer_name(rs.getString("QCU_NAME"));
					oTempCustomerDiscountDto.setSapcode(rs.getString("SAPCODE"));
					oTempCustomerDiscountDto.setOraclecode(rs.getString("ORACLECODE"));
					oTempCustomerDiscountDto.setGstnumber(rs.getString("GSTNUMBER"));
					if(StringUtils.isNotBlank(rs.getString("SALESENGINEERID"))) {
						iSalesMgrId = Integer.parseInt(rs.getString("SALESENGINEERID"));
					} else {
						iSalesMgrId = 0;
					}
					oTempCustomerDiscountDto.setSalesengineer(getSalesManagerById(conn, iSalesMgrId));
					oTempCustomerDiscountDto.setCustomer_discount(rs.getString("CUSTOMER_DISCOUNT"));
					oTempCustomerDiscountDto.setSm_discount(rs.getString("SM_DISCOUNT"));
					oTempCustomerDiscountDto.setRsm_discount(rs.getString("RSM_DISCOUNT"));
					oTempCustomerDiscountDto.setNsm_discount(rs.getString("NSM_DISCOUNT"));

					alSearchResultsList.add(oTempCustomerDiscountDto);

				} while (rs.next());
        	}
        } finally {
        	/* Releasing PreparedStatement objects */
            oDBUtility.releaseResources( rs,pstmt);
        }
        
        LoggerUtility.log("INFO", this.getClass().getName(), "exportCustDiscountSearchResult", "End");
        return alSearchResultsList;
	}
	
}
