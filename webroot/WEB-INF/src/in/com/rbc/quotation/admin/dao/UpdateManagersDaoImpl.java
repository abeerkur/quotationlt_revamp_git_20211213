/**
 * *****************************************************************************
 * Project Name: quotation
 * Document Name: UpdateManagersDaoImpl.java
 * Package: in.com.rbc.quotation.admin.dao
 * Desc: 
 * *****************************************************************************
 * Author: 100005701 (Suresh)
 * Date: Oct 13, 2008
 * *****************************************************************************
 */
package in.com.rbc.quotation.admin.dao;

import in.com.rbc.quotation.admin.dto.UpdateManagersDto;
import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.utility.DBUtility;
import in.com.rbc.quotation.common.utility.LoggerUtility;
import in.com.rbc.quotation.common.vo.KeyValueVo;
import in.com.rbc.quotation.workflow.dao.WorkflowQueries;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import org.apache.commons.beanutils.PropertyUtils;

/**
 * @author 100005701 (Suresh Shanmugam)
 *
 */
public class UpdateManagersDaoImpl implements UpdateManagersDao, AdminQueries {
	
	
	public UpdateManagersDto getManagerValues(Connection conn , UpdateManagersDto oUpdateManagersDto)throws Exception {
		/*
    	 * Setting the Method Name as generic for logger Utility purpose . 
    	 */
		String sMethodName = "getManagerValues";
		String sClassName=this.getClass().getName();
		LoggerUtility.log("INFO", sClassName, sMethodName,"START" );
    		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;      
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        UpdateManagersDto oOutputUpdateManagersDto = new UpdateManagersDto();
        
        try {
            PropertyUtils.copyProperties(oOutputUpdateManagersDto, oUpdateManagersDto);
            
        	/*
        	 * getting the manager value from table and set into Dto object.
        	 */
            pstmt = conn.prepareStatement(FETCH_MANAGER_VALUE);
            LoggerUtility.log("INFO", sClassName, sMethodName,"FETCH Manager Query ::: " + FETCH_MANAGER_VALUE);
            
			rs = pstmt.executeQuery();
            if ( rs.next() ) {
                do {
                    if ( rs.getString("RLE_MGRTYPE").equalsIgnoreCase("CE") ) {
                        oOutputUpdateManagersDto.setCheifExecutiveOldSSO(rs.getString("RLE_MGRSSO"));
                        oOutputUpdateManagersDto.setCheifExecutiveOldName(rs.getString("EMP_NAMEFULL"));
                    }
                    if (rs.getString("RLE_MGRTYPE").equalsIgnoreCase("DM") ) {
                        oOutputUpdateManagersDto.setDesignManagerOldSSO(rs.getString("RLE_MGRSSO"));
                        oOutputUpdateManagersDto.setDesignManagerOldName(rs.getString("EMP_NAMEFULL"));
                    }
                    if ( rs.getString("RLE_MGRTYPE").equalsIgnoreCase("DE") ) {
                        oOutputUpdateManagersDto.setDesignManagerOldSSO(rs.getString("RLE_MGRSSO"));
                        oOutputUpdateManagersDto.setDesignManagerOldName(rs.getString("EMP_NAMEFULL"));
                    }
                    if ( rs.getString("RLE_MGRTYPE").equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_CM) ) {
                        oOutputUpdateManagersDto.setCommercialManagerOldSSO(rs.getString("RLE_MGRSSO"));
                        oOutputUpdateManagersDto.setCommercialManagerOldName(rs.getString("EMP_NAMEFULL"));
                    }
                } while (rs.next());
            }
        } finally {
        	/* Releasing ResultSet & PreparedStatement objects */
    		oDBUtility.releaseResources(rs,pstmt);
        }
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
		return oOutputUpdateManagersDto;
	}
	public ArrayList getDesignEngineerList(Connection conn)throws Exception{
		/*
    	 * Setting the Method Name as generic for logger Utility purpose . 
    	 */
    	String sMethodName = "getDesignEngineerList";
    	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
    		
		/* PreparedStatement object to handle database operations */
        	PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        	ResultSet rs = null ;      
        
        /* Object of DBUtility class to handle database operations */
        	DBUtility oDBUtility = new DBUtility();
            
        /*
         * Querying the database to retrieve the Location description, storing the
         * details in KeyValueVo object, storing these object in an ArrayList 
         * and returning the same
         */
        	KeyValueVo oKeyValueVo = null;
        	String sQuery=null;
           
         /* ArrayList to store the list of request status */
        	ArrayList arlSearchResultsList = new ArrayList(); // Stores the list of rows retrieved from the database
        try {
        	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName," Query for retrving the getDesignEngineerList List := "+FETCH_DE_LIST);
        	sQuery=FETCH_DE_LIST;
        	pstmt = conn.prepareStatement(sQuery);
        	rs = pstmt.executeQuery();
        	while(rs.next()) {
        		oKeyValueVo = new KeyValueVo();
        		oKeyValueVo.setKey(rs.getString("QDE_ENGINEERID")+","+rs.getString("QDE_ENGINEERSSO"));
        		oKeyValueVo.setValue(rs.getString("QDE_ENGINEERNAME"));
        		arlSearchResultsList.add(oKeyValueVo);
        		
        		
        	}
        	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"Size of regional sales Manager Array Lsit Size := "+arlSearchResultsList.size());
        }finally {
                /* Releasing ResultSet & PreparedStatement objects */
            	oDBUtility.releaseResources(rs,pstmt);
            }
            LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );		
		return arlSearchResultsList;
	}
	public ArrayList getSalesManagerList(Connection conn)throws Exception{
		/*
    	 * Setting the Method Name as generic for logger Utility purpose . 
    	 */
    	String sMethodName = "getSalesManagerList";
    	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
    		
		/* PreparedStatement object to handle database operations */
        	PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        	ResultSet rs = null ;      
        
        /* Object of DBUtility class to handle database operations */
        	DBUtility oDBUtility = new DBUtility();
            
        /*
         * Querying the database to retrieve the Location description, storing the
         * details in KeyValueVo object, storing these object in an ArrayList 
         * and returning the same
         */
        	KeyValueVo oKeyValueVo = null;
        	String sQuery=null;
           
         /* ArrayList to store the list of request status */
        	ArrayList arlSearchResultsList = new ArrayList(); // Stores the list of rows retrieved from the database
        try {
        	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName," Query for retrving the Sales Manager List := "+FETCH_SALESMANAGER_LIST);
        	sQuery=FETCH_SALESMANAGER_LIST;
        	pstmt = conn.prepareStatement(sQuery);
        	rs = pstmt.executeQuery();
        	while(rs.next()) {
        		oKeyValueVo = new KeyValueVo();
        		oKeyValueVo.setKey(rs.getString("QSM_SALESMGRID")+","+rs.getString("QSM_SALESMGR"));
        		oKeyValueVo.setValue(rs.getString("QSM_RSMNAME"));
        		arlSearchResultsList.add(oKeyValueVo);
        	}
        	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"Size of regional sales Manager Array Lsit Size := "+arlSearchResultsList.size());
        }finally {
                /* Releasing ResultSet & PreparedStatement objects */
            	oDBUtility.releaseResources(rs,pstmt);
            }
            LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );		
		return arlSearchResultsList;
	}
	
	public ArrayList getReginolSalesManagerList(Connection conn) throws Exception{
		/*
    	 * Setting the Method Name as generic for logger Utility purpose . 
    	 */
    	String sMethodName ="getReginolSalesManagerList"; 
    	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
    		
		/* PreparedStatement object to handle database operations */
        	PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        	ResultSet rs = null ;      
        
        /* Object of DBUtility class to handle database operations */
        	DBUtility oDBUtility = new DBUtility();
            
        /*
         * Querying the database to retrieve the Location description, storing the
         * details in KeyValueVo object, storing these object in an ArrayList 
         * and returning the same
         */
        	KeyValueVo oKeyValueVo = null;
        	String sQuery=null;
           
         /* ArrayList to store the list of request status */
        	ArrayList arlSearchResultsList = new ArrayList(); // Stores the list of rows retrieved from the database
        try {
        	sQuery=FETCH_RSM_LIST;
        	pstmt = conn.prepareStatement(sQuery);
        	rs = pstmt.executeQuery();
        	while(rs.next()) {
        		oKeyValueVo = new KeyValueVo();
        		oKeyValueVo.setKey(rs.getString("QSM_RSM"));
        		oKeyValueVo.setValue(rs.getString("QSM_RSMNAME"));
        		arlSearchResultsList.add(oKeyValueVo);
        	}
        	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"Size of regional Regional sales Manager Array Lsit Size := "+arlSearchResultsList.size());
        }finally {
                /* Releasing ResultSet & PreparedStatement objects */
            	oDBUtility.releaseResources(rs,pstmt);
            }
            LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );		
		return arlSearchResultsList;
	}
	
	public boolean updateChiefExecutiveValues(Connection conn , UpdateManagersDto oUpdateManagersDto)throws Exception {
		/*
    	* Setting the Method Name as generic for logger Utility purpose . 
        */
		String sMethodName ="updateChiefExecutiveValues"; 
		String sClassName=this.getClass().getName();
		LoggerUtility.log("INFO", sClassName, sMethodName,"START" );
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        PreparedStatement pstmt1 = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        boolean isUpdated = false ;
        int iUpdatedCount = 0;
        int iCreatedCount =0 ;

        try {
	        	LoggerUtility.log("INFO", sClassName, sMethodName,"Before of Update Query"+UPDATE_MANAGER_VALUE );
	        	pstmt = conn.prepareStatement(UPDATE_MANAGER_VALUE);
	        	pstmt.setString(1,oUpdateManagersDto.getCheifExecutiveSSO());
	        	pstmt.setString(2,oUpdateManagersDto.getOperation());
	    		iUpdatedCount = pstmt.executeUpdate();
	        	LoggerUtility.log("INFO", sClassName, sMethodName,"iUpdatedCount Value : "+iUpdatedCount);
        	if(iUpdatedCount > 0 ) {
        		isUpdated = true;
        		LoggerUtility.log("INFO", sClassName, sMethodName," Upadted Value in Manager table  : "+isUpdated);	
            }else {
            	LoggerUtility.log("INFO", sClassName, sMethodName,"Inside Else condition for inserting new value into the table : Insert Query "+INSERT_MANAGER_VALUE );
            	pstmt1 = conn.prepareStatement(INSERT_MANAGER_VALUE);
            	pstmt1.setString(1,oUpdateManagersDto.getCheifExecutiveSSO());
            	pstmt1.setString(2,oUpdateManagersDto.getOperation());
            	iCreatedCount = pstmt1.executeUpdate();
            	LoggerUtility.log("INFO", sClassName, sMethodName,"Insert into manager table  : "+iCreatedCount);
            	if(iCreatedCount > 0) {
                	isUpdated = true;
            	}
            }
        }finally {
        	/* Releasing PreparedStatement objects */
        	oDBUtility.releaseResources(pstmt);
        	oDBUtility.releaseResources(pstmt1);
        }
        LoggerUtility.log("INFO", sClassName, sMethodName,"END" );
        return isUpdated;
	}
	
	public boolean updateDesignManagerValue(Connection conn , UpdateManagersDto oUpdateManagersDto)throws Exception{
		/*
    	* Setting the Method Name as generic for logger Utility purpose . 
        */
		String sMethodName ="updateDesignManagerValue";
		String sClassName=this.getClass().getName();
		LoggerUtility.log("INFO", sClassName, sMethodName,"START" );
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        PreparedStatement pstmt1 = null ;
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
       
        boolean isUpdated = false ;
        int iUpdatedCount = 0;
        int iCreatedCount =0 ;
        try {
	        	LoggerUtility.log("INFO", sClassName, sMethodName,"Before of Update Query"+UPDATE_MANAGER_VALUE );
	        	pstmt = conn.prepareStatement(UPDATE_MANAGER_VALUE);
	        	pstmt.setString(1,oUpdateManagersDto.getDesignManagerSSO());
	        	pstmt.setString(2,oUpdateManagersDto.getOperation());
	    		iUpdatedCount = pstmt.executeUpdate();
	    		LoggerUtility.log("INFO", sClassName, sMethodName,"iUpdatedCount Value : "+iUpdatedCount);
        	
        	if(iUpdatedCount > 0 ) {
        		isUpdated = true;
        		LoggerUtility.log("INFO", sClassName, sMethodName,"Upadted Value in Manager table  : "+isUpdated);	
            }else {
            	LoggerUtility.log("INFO", sClassName, sMethodName,"Inside Else condition for inserting new value into the table : Insert Query "+INSERT_MANAGER_VALUE );
            	pstmt1 = conn.prepareStatement(INSERT_MANAGER_VALUE);
            	pstmt1.setString(1,oUpdateManagersDto.getDesignManagerSSO());
            	pstmt1.setString(2,oUpdateManagersDto.getOperation());
            	iCreatedCount = pstmt1.executeUpdate();
            	LoggerUtility.log("INFO", sClassName, sMethodName,"Insert into manager table  : "+iCreatedCount);
            	if(iCreatedCount > 0) {
                	isUpdated = true;
            	}
            }
        }finally {
        	/* Releasing PreparedStatement objects */
        	oDBUtility.releaseResources(pstmt);
        	oDBUtility.releaseResources(pstmt1);
        }
        LoggerUtility.log("INFO", sClassName, sMethodName,"END" );
        return isUpdated;
	}
	
	public boolean updateRegionalSalesManagerValue(Connection conn , UpdateManagersDto oUpdateManagersDto)throws Exception{
		/*
    	* Setting the Method Name as generic for logger Utility purpose . 
        */
		String sMethodName ="updateRegionalSalesManagerValue";
		String sClassName=this.getClass().getName();
		LoggerUtility.log("INFO", sClassName, sMethodName,"START" );
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        PreparedStatement pstmt1 = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
       
        boolean isUpdated = false ;
        int iUpdatePRSMCount = 0;
        int iUpdateARSMCount =0 ;
        try {
        	
        	LoggerUtility.log("INFO", sClassName, sMethodName,"Update Query of Primary RSM :="+UPDATE_PRIMARYRSM_VALUE );
        	pstmt = conn.prepareStatement(UPDATE_PRIMARYRSM_VALUE);
        	pstmt.setString(1,oUpdateManagersDto.getRegionalSalesManagerSSO());
        	pstmt.setString(2,oUpdateManagersDto.getRegionalSalesManagerID());
        	iUpdatePRSMCount = pstmt.executeUpdate();
        	LoggerUtility.log("INFO", sClassName, sMethodName,"isUpdatePRSMCount Value : "+iUpdatePRSMCount);
        	
        	LoggerUtility.log("INFO", sClassName, sMethodName,"Update Query of Additional RSM :="+UPDATE_ADDITIONALRSM_VALUE );
        	pstmt1 = conn.prepareStatement(UPDATE_ADDITIONALRSM_VALUE);
        	pstmt1.setString(1,oUpdateManagersDto.getRegionalSalesManagerSSO());
        	pstmt1.setString(2,oUpdateManagersDto.getRegionalSalesManagerID());
        	iUpdateARSMCount = pstmt1.executeUpdate();
        	LoggerUtility.log("INFO", sClassName, sMethodName,"isUpdateARSMCount Value : "+iUpdateARSMCount);
        	if(iUpdatePRSMCount > 0 || iUpdateARSMCount > 0) {
        		isUpdated = true;
        		LoggerUtility.log("INFO", sClassName, sMethodName,"Upadted Value in Sales Maneger table : "+isUpdated);	
            }
        }finally {
        	/* Releasing PreparedStatement objects */
        	oDBUtility.releaseResources(pstmt);
            oDBUtility.releaseResources(pstmt1);
        }
        LoggerUtility.log("INFO", sClassName, sMethodName,"END" );
        return isUpdated;
	}
	
	public boolean updateSalesManagerValue(Connection conn , UpdateManagersDto oUpdateManagersDto)throws Exception{
		/*
    	* Setting the Method Name as generic for logger Utility purpose . 
        */
		String sMethodName = "updateSalesManagerValue";
		String sClassName=this.getClass().getName();
		LoggerUtility.log("INFO", sClassName, sMethodName,"START" );
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        PreparedStatement pstmt1 = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
       
        boolean isUpdated = false ;
        int iUpdateSMCount = 0;
        int iUpdateARSMCount =0 ;
        String[] sExistSalesManagerSSO =null;
        String[] sNewSalesManagerSSO=null;
        try {
        	sExistSalesManagerSSO = oUpdateManagersDto.getOldSalesManagerSSO().split(",");
        	sNewSalesManagerSSO=oUpdateManagersDto.getNewSalesManagerSSO().split(",");
			LoggerUtility.log("INFO", sClassName, sMethodName,"Old Sales Manger ID :="+sExistSalesManagerSSO[1] );
			LoggerUtility.log("INFO", sClassName, sMethodName,"New Sales Manager SSO :="+sNewSalesManagerSSO[1] );
        	LoggerUtility.log("INFO", sClassName, sMethodName,"Update Query of Sales Manager :="+UPDATE_SALESMANGER_VALUE+" QEM_ENQUIRYNO LIKE '"+sExistSalesManagerSSO[1]+"%' " );
        	
        	pstmt = conn.prepareStatement(UPDATE_SALESMANGER_VALUE+" QEM_ENQUIRYNO LIKE '"+sExistSalesManagerSSO[1]+"%'");
        	pstmt.setString(1,sNewSalesManagerSSO[1]);
        	pstmt.setString(2,sExistSalesManagerSSO[1]);
        	pstmt.setString(3,sNewSalesManagerSSO[1]);
        	iUpdateSMCount = pstmt.executeUpdate();
        	LoggerUtility.log("INFO", sClassName, sMethodName,"isUpdateSMCount Value : "+iUpdateSMCount);
        	if(iUpdateSMCount > 0 || iUpdateARSMCount > 0 ) {
        		isUpdated = true;
        		LoggerUtility.log("INFO", sClassName, sMethodName,"Upadted Value in Sales Maneger table : "+isUpdated);	
            }
        }finally {
        	/* Releasing PreparedStatement objects */
        	oDBUtility.releaseResources(pstmt);
            oDBUtility.releaseResources(pstmt1);
        }
        LoggerUtility.log("INFO", sClassName, sMethodName,"END" );
        return isUpdated;
	}
	
	public boolean replaceManagersWorkflow(Connection conn, 
										   UpdateManagersDto oUpdateManagersDto)throws Exception {
		
        String sClassName = this.getClass().getName();
        String sMethodName = "replaceManagersWorkflow";
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        /* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        String sQuery = "";
        boolean isDatabaseUpdated = false;
        String[] sExistSalesManagerSSO=null;
        String[] sNewSalesManagerSSO=null;
        String[] sExistDesignEngineerSSO=null;
        String[] sNewDesignEngineerSSO=null;        
        int iRowCount =0;
        
        try {
            /*
             * update in Workflow table
             */
        	sQuery = UPDATE_MANAGERVALUE_WORKFLOW_TABLE;
        	
            if(oUpdateManagersDto.getOperation().equalsIgnoreCase("CE")) {
    			LoggerUtility.log("INFO", sClassName, sMethodName, "Inside CE loop and  old SSO : "+oUpdateManagersDto.getCheifExecutiveOldSSO());	
    			LoggerUtility.log("INFO", sClassName, sMethodName, "New SSO ID :="+oUpdateManagersDto.getCheifExecutiveSSO());	
    			sQuery = sQuery +QuotationConstants.CECHECK;
    			pstmt = conn.prepareStatement(sQuery);
    			pstmt.setString(1,oUpdateManagersDto.getCheifExecutiveSSO());
    			pstmt.setString(2,oUpdateManagersDto.getCheifExecutiveOldSSO());
    			
    		}
            if(oUpdateManagersDto.getOperation().equalsIgnoreCase("DM")) {
            	LoggerUtility.log("INFO", sClassName, sMethodName,"Inside DM loop and old SSO   : "+oUpdateManagersDto.getDesignManagerOldSSO()+"  New SSO ID :="+oUpdateManagersDto.getDesignManagerSSO());	
            	sQuery = sQuery +QuotationConstants.DMCHECK;
    			pstmt = conn.prepareStatement(sQuery);
            	pstmt.setString(1,oUpdateManagersDto.getDesignManagerSSO());
            	pstmt.setString(2,oUpdateManagersDto.getDesignManagerOldSSO());
    		}
            
            if(oUpdateManagersDto.getOperation().equalsIgnoreCase("RSM")) {
            	LoggerUtility.log("INFO", sClassName, sMethodName,"Inside RSM loop and old SSO   : "+oUpdateManagersDto.getRegionalSalesManagerID()+"  New SSO ID :="+oUpdateManagersDto.getRegionalSalesManagerSSO());	
            	sQuery = sQuery +QuotationConstants.RSMCHECK;
    			pstmt = conn.prepareStatement(sQuery);
            	pstmt.setString(1,oUpdateManagersDto.getRegionalSalesManagerSSO());
            	pstmt.setString(2,oUpdateManagersDto.getRegionalSalesManagerID());
    		}
            if(oUpdateManagersDto.getOperation().equalsIgnoreCase("SM")) {
            	sExistSalesManagerSSO= oUpdateManagersDto.getOldSalesManagerSSO().split(",");
            	sNewSalesManagerSSO=oUpdateManagersDto.getNewSalesManagerSSO().split(",");
            	LoggerUtility.log("INFO", sClassName, sMethodName,"Inside Sales Manager loop and old SSO   : "+sExistSalesManagerSSO[1]+"  New SSO ID :="+sNewSalesManagerSSO[1]);	
            	sQuery = sQuery +QuotationConstants.SMCHECK;
    			pstmt = conn.prepareStatement(sQuery);
            	pstmt.setString(1,sNewSalesManagerSSO[1]);
    			pstmt.setString(2,sExistSalesManagerSSO[1]);
    		}
            
            if(oUpdateManagersDto.getOperation().equalsIgnoreCase("DE")) {
            	sQuery = sQuery +QuotationConstants.DECHECK;
    			pstmt = conn.prepareStatement(sQuery);
            	 sExistDesignEngineerSSO= oUpdateManagersDto.getOldDesignEngineerSSO().split(",");
            	 sNewDesignEngineerSSO=oUpdateManagersDto.getNewDesignEngineerSSO().split(",");
            	LoggerUtility.log("INFO", sClassName, sMethodName,"Inside Sales Manager loop and old SSO   : "+sExistDesignEngineerSSO[1]+"  New SSO ID :="+sNewDesignEngineerSSO[1]);	
    			
            	pstmt.setString(1,sNewDesignEngineerSSO[1]);
            	pstmt.setString(2,sExistDesignEngineerSSO[1]);
    		}
            
            if(oUpdateManagersDto.getOperation().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_CM)) {
            	sQuery = sQuery + QuotationConstants.CMCHECK;
    			pstmt = conn.prepareStatement(sQuery);
            	pstmt.setString(1,oUpdateManagersDto.getCommercialManagerSSO());
            	pstmt.setString(2,oUpdateManagersDto.getCommercialManagerOldSSO());
   		}
            iRowCount = pstmt.executeUpdate();
            
            LoggerUtility.log("INFO", sClassName, sMethodName, "Updated Row Count = " + iRowCount);
            
            
            
            
            
            if ( iRowCount > 0 )
                isDatabaseUpdated = true;
            LoggerUtility.log("INFO", sClassName, sMethodName,"Upadted Value in Work Flow table  : "+isDatabaseUpdated);
        } finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(pstmt);
        }
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        return isDatabaseUpdated;
	}
	
	/**
	 * To update Commercial Manager in Admin Replace Managers page
	 * @param conn Connection object to connect to database	
	 * @param oUpdateManagersDto UpdateManagersDto object which has Commercial Manager data
	 * @return Returns boolean true if update success else false
	 * @throws Exception If any error occurs during the process
	 * @author 610092227 (Kalyani Uppalapati)
	 * Created on: 28 June 2019
	 */
	public boolean updateCommercialManagerValue(Connection conn, 
			   UpdateManagersDto oUpdateManagersDto)throws Exception{
		/*
    	* Setting the Method Name as generic for logger Utility purpose . 
        */
		String sMethodName ="updateCommercialManagerValue";
		String sClassName=this.getClass().getName();
		LoggerUtility.log("INFO", sClassName, sMethodName,"START" );
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        PreparedStatement pstmt1 = null ;
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
       
        boolean isUpdated = QuotationConstants.QUOTATION_FALSE ;
        int iUpdatedCount = QuotationConstants.QUOTATION_LITERAL_ZERO;
        int iCreatedCount = QuotationConstants.QUOTATION_LITERAL_ZERO ;
        try {
	        	LoggerUtility.log("INFO", sClassName, sMethodName,"Before of Update Query"+UPDATE_MANAGER_VALUE );
	        	pstmt = conn.prepareStatement(UPDATE_MANAGER_VALUE);
	        	pstmt.setString(1,oUpdateManagersDto.getCommercialManagerSSO());
	        	pstmt.setString(2,oUpdateManagersDto.getOperation());
	    		iUpdatedCount = pstmt.executeUpdate();
	    		LoggerUtility.log("INFO", sClassName, sMethodName,"iUpdatedCount Value : "+iUpdatedCount);
        	
        	if(iUpdatedCount > QuotationConstants.QUOTATION_LITERAL_ZERO ) {
        		isUpdated =QuotationConstants.QUOTATION_TRUE;
        		LoggerUtility.log("INFO", sClassName, sMethodName,"Upadted Value in Manager table  : "+isUpdated);	
            }else {
            	LoggerUtility.log("INFO", sClassName, sMethodName,"Inside Else condition for inserting new value into the table : Insert Query "+INSERT_MANAGER_VALUE );
            	pstmt1 = conn.prepareStatement(INSERT_MANAGER_VALUE);
            	pstmt1.setString(1,oUpdateManagersDto.getCommercialManagerSSO());
            	pstmt1.setString(2,oUpdateManagersDto.getOperation());
            	iCreatedCount = pstmt1.executeUpdate();
            	LoggerUtility.log("INFO", sClassName, sMethodName,"Insert into manager table  : "+iCreatedCount);
            	if(iCreatedCount > QuotationConstants.QUOTATION_LITERAL_ZERO) {
                	isUpdated = QuotationConstants.QUOTATION_TRUE;
            	}
            }
        }finally {
        	/* Releasing PreparedStatement objects */
        	oDBUtility.releaseResources(pstmt);
        	oDBUtility.releaseResources(pstmt1);
        }
        LoggerUtility.log("INFO", sClassName, sMethodName,"END" );
        return isUpdated;
		
	}
	
	
}
