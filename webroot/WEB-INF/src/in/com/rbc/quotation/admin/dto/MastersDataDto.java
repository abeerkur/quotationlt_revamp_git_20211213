/**
 * *****************************************************************************
 * Project Name: quotation
 * Document Name: CustomerDto.java
 * Package: in.com.rbc.quotation.admin.dto
 * Desc: 
 * *****************************************************************************
 * Author: 100005701 (Suresh)
 * Date: Sep 30, 2008
 * *****************************************************************************
 */
package in.com.rbc.quotation.admin.dto;

import in.com.rbc.quotation.common.utility.CommonUtility;

import java.util.ArrayList;

/**
 * @author 100005701 (Suresh Shanmugam)
 *
 */
public class MastersDataDto {
	
	private String invoke ;
	private String masterId;
	private String master;
	private String engineeringId;
	private String engineeringName;
	private String status;
	private String addOrUpdate;
	private String editmastervalue;
	
	public String getInvoke() {
		return CommonUtility.replaceNull(invoke);
	}
	public void setInvoke(String invoke) {
		this.invoke = invoke;
	}
	public String getMaster() {
		return CommonUtility.replaceNull(master);
	}
	public void setMaster(String master) {
		this.master = master;
	}
	public String getMasterId() {
		return CommonUtility.replaceNull(masterId);
	}
	public void setMasterId(String masterId) {
		this.masterId = masterId;
	}
	public String getAddOrUpdate() {
		return CommonUtility.replaceNull(addOrUpdate);
	}
	public void setAddOrUpdate(String addOrUpdate) {
		this.addOrUpdate = addOrUpdate;
	}
	public String getEngineeringId() {
		return CommonUtility.replaceNull(engineeringId);
	}
	public void setEngineeringId(String engineeringId) {
		this.engineeringId = engineeringId;
	}
	public String getEngineeringName() {
		return CommonUtility.replaceNull(engineeringName);
	}
	public void setEngineeringName(String engineeringName) {
		this.engineeringName = engineeringName;
	}
	public String getStatus() {
		return CommonUtility.replaceNull(status);
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getEditmastervalue() {
		return CommonUtility.replaceNull(editmastervalue);
	}
	public void setEditmastervalue(String editmastervalue) {
		this.editmastervalue = editmastervalue;
	}
}
