/**
 * *****************************************************************************
 * Project Name: quotation
 * Document Name: LocationDaoImpl.java
 * Package: in.com.rbc.quotation.admin.dao
 * Desc: 
 * *****************************************************************************
 * Author: 100005701 (Suresh)
 * Date: Oct 3, 2008
 * *****************************************************************************
 */
package in.com.rbc.quotation.admin.dao;

import in.com.rbc.quotation.admin.dto.LocationDto;

import in.com.rbc.quotation.common.utility.DBUtility;
import in.com.rbc.quotation.common.utility.ListDBColumnUtil;
import in.com.rbc.quotation.common.utility.ListModel;
import in.com.rbc.quotation.common.utility.LoggerUtility;
import in.com.rbc.quotation.common.vo.KeyValueVo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Hashtable;

/*import com.sun.org.apache.bcel.internal.generic.ISUB;*/

/**
 * @author 100005701 (Suresh Shanmugam)
 *
 */
public class LocationDaoImpl implements LocationDao ,AdminQueries {

	
	public ArrayList getRegionList(Connection conn)throws Exception {
		
		/*
    	 * Setting the Method Name as generic for logger Utility purpose . 
    	 */
    		String sMethodName = "getRegionList" ;
    		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
    		
    		/* PreparedStatement object to handle database operations */
            	PreparedStatement pstmt = null ;
            
            /* ResultSet object to store the rows retrieved from database */
            	ResultSet rs = null ;      
            
            /* Object of DBUtility class to handle database operations */
            	DBUtility oDBUtility = new DBUtility();
            
            /*
             * Querying the database to retrieve the Location description, storing the
             * details in KeyValueVo object, storing these object in an ArrayList 
             * and returning the same
             */
            	KeyValueVo oKeyValueVo = null;
           
             /* ArrayList to store the list of request status */
            	ArrayList alSearchResultsList = new ArrayList(); // Stores the list of rows retrieved from the database
            	
            	String sSqlQuery="";
            try {
            	sSqlQuery=FETCH_REGION_LIST;
            	pstmt = conn.prepareStatement(sSqlQuery);
            	rs = pstmt.executeQuery();
            	while(rs.next()) {
            		oKeyValueVo = new KeyValueVo();
            		oKeyValueVo.setKey(rs.getString("QRE_REGIONID"));
            		oKeyValueVo.setValue(rs.getString("QRE_REGIONNAME"));
            		alSearchResultsList.add(oKeyValueVo);
            	}
            	
            }finally {
                /* Releasing ResultSet & PreparedStatement objects */
            	oDBUtility.releaseResources(rs,pstmt);
            }
            LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );		
		return alSearchResultsList;
	}
	
	public Hashtable getLocationSearchResult(Connection conn, LocationDto oLocationDto, ListModel oListModel) throws Exception {
		
		   /*
	    	* Setting the Method Name as generic for logger Utility purpose . 
	    	*/
			String sMethodName = "getLocationSearchResult Mehotd" ;
			LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
			
    		/* PreparedStatement object to handle database operations */
            PreparedStatement pstmt = null ;
            
            /* ResultSet object to store the rows retrieved from database */
            ResultSet rs = null ;      
            
            /* Object of DBUtility class to handle database operations */
            DBUtility oDBUtility = new DBUtility();
           
            /*
             * Hashtable that stores the following values, and that is used to display results in search page
             * --> ArrayList containing the rows retrieved from database
             * --> ListModel object that hold the values used to retrieve values and display the same
             * --> String array containing the list of column names to be displayed as column headings
             */
            Hashtable htSearchResults = null;
            
            ArrayList alSearchResultsList = new ArrayList(); // Stores the list of rows retrieved from the database
            
            
            int iCursorPosition = 0; // Stores the cursor position from where the records have to be retrieved // from database; this is dependent on the page number selected for viewing
            
            String sWhereString = null; // Stores the Where clause condition
            String sSqlQueryString = null; // Stores the complete SQL query to be executed
        	LocationDto oTempLocationDto ;// Create the Temporary Location Object for storing the table column values 

            try {
            	
            	sWhereString = buildWhereClause(oLocationDto);
            	sSqlQueryString = COUNT_LOCATION_SEARCHRESULT + sWhereString;
            	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName+"..Search Location Count Query",sSqlQueryString);
            	
            	pstmt = conn.prepareCall(sSqlQueryString);
            	
            	/* Execute the Query for counting the number records*/
            	rs=pstmt.executeQuery();
            	
            	if(rs.next()) {
            		oListModel.setTotalRecordCount(rs.getInt(1));//set total number of records available in table depond upon the where conditions .
            	}else {
            		oListModel.setTotalRecordCount(0);// if the Query return no records , it's set the Total recordcount  "0" as default
            	}
            	oDBUtility.releaseResources(rs, pstmt);
            	 
            	 if(oListModel.getTotalRecordCount()>0) {
            		 htSearchResults = new Hashtable();
            		 
            		 if(oListModel.getCurrentPageInt()<0)
            			 throw new Exception("Current Page is negative in search method...");
            		 
            		 iCursorPosition = (oListModel.getCurrentPageInt()-1) * oListModel.getRecordsPerPageInt();
            		 
            		 if(iCursorPosition < oListModel.getTotalRecordCount()) {
                 		
    			 /*
    			  * Select the values from the table Using FETCH_SEARCHLocation_RESULTS Query depond upon the 
    			  * sWhereString condition and return into the sSqlQueryString as StringBuffer 
    			  * and the data's are ordered by Location DBFiled as by default
    			  * 
    			  */
                 		sSqlQueryString = new StringBuffer("SELECT * FROM (SELECT Q.*, rownum AS rnum FROM ( ")
     						                    .append(FETCH_LOCATION_SEARCHLIST)
     						                    .append(sWhereString)
     						                    .append(" ORDER BY ")
     						                    .append(ListDBColumnUtil.getLocationSearchResultDBField(oListModel.getSortBy()))
     						                    .append(" " + oListModel.getSortOrderDesc())
     						                    .append(" ) Q WHERE rownum <= (")
     						                    .append(iCursorPosition + " + " + oListModel.getRecordsPerPage() + ")) ")
     						                    .append(" WHERE rnum > " + iCursorPosition + "")
     						                    .toString();
                 		

                 		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName+"..SearchLocation Retrive Query",sSqlQueryString );
                 		pstmt = conn.prepareStatement(sSqlQueryString, ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
                 		
                 		rs =pstmt.executeQuery();// Execute the Search Query to retriuve the Location Values .
                 		if (rs.next()) {
                             do {
                            	 oTempLocationDto = new LocationDto();
                            	 oTempLocationDto.setLocationName(rs.getString("QLO_LOCATIONNAME"));
                            	 oTempLocationDto.setRegionName(rs.getString("QRE_REGIONNAME"));
                            	 oTempLocationDto.setStatus(rs.getString("QLO_ISACTIVE"));
                            	 oTempLocationDto.setLocationId(rs.getInt("QLO_LOCATIONID"));
                            	 alSearchResultsList.add(oTempLocationDto);
                            } while (rs.next());
                         }
                   }
            		 
            		 htSearchResults.put("resultsList", alSearchResultsList);// put the SearchresultList of arraylist value into Hashtable 
                     htSearchResults.put("ListModel", oListModel); // put the ListModel values into hashtable 
                     htSearchResults.put("locationQuery", sWhereString);//keep the Where condition for using in Export to Excel purpose .
                 }
            	 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END.." );
            }finally {
            	 /* Releasing ResultSet & PreparedStatement objects */
                oDBUtility.releaseResources(rs, pstmt);
            }
            return htSearchResults ;
	}
	
	private String buildWhereClause(LocationDto oLocationDto) {
		
		String sMethodName = "buildWhereClause";
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
        
        StringBuffer sbWhereString = new StringBuffer(" AND");
        
        /* Location Name */
        if (oLocationDto.getLocationName() != null && oLocationDto.getLocationName().trim().length() > 0){
        	sbWhereString.append(" UPPER(LOC.QLO_LOCATIONNAME) LIKE UPPER('%"+oLocationDto.getLocationName().trim().toUpperCase()+"%')");
        	sbWhereString.append(" AND");
        }
        
        /* Region Id */
        if ( oLocationDto.getRegionId()  > 0){
        	sbWhereString.append(" UPPER(LOC.QLO_REGIONID)= "+oLocationDto.getRegionId());
        	sbWhereString.append(" AND");
        }
        
        if ( sbWhereString.toString().trim().equalsIgnoreCase(" AND") )
            return "";
        else if (sbWhereString.toString().trim().endsWith(" AND"))        	
            return sbWhereString.toString().substring(0, sbWhereString.toString().lastIndexOf("AND"));        
        else
        	return "";
	}
	
	public boolean createLocation(Connection conn, LocationDto oLocationDto)throws Exception {
	   /*
    	* Setting the Method Name as generic for logger Utility purpose . 
        */
		String sMethodName = "createLocation" ;
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        PreparedStatement pstmt1 = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;      
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        /*getting the newlly Customer ID from the Customer sequence */
        int iGetLocationID = 0;
        boolean isCreated = false ;
        try {
        	/**
        	 * Getting the Customer id from the Customer Sequence for
        	 */
        	pstmt1 = conn.prepareStatement(CREATE_LOCATIONID);
        	rs = pstmt1.executeQuery();// Execute the creating the Customer Sequence Query,
        	
        	if(rs.next()) {
        		iGetLocationID = rs.getInt("LOCATION_ID");
        	}
        	pstmt = conn.prepareStatement(INSERT_LOCATION_VALUES);
        	pstmt.setInt(1, iGetLocationID);
        	pstmt.setString(2, oLocationDto.getLocationName());
        	pstmt.setInt(3, oLocationDto.getRegionId());
        	pstmt.setString(4, oLocationDto.getStatus());
        	pstmt.setString(5,oLocationDto.getCreatedById());
        	pstmt.setString(6,oLocationDto.getLastUpdatedById());
            int iUpdatedCount = pstmt.executeUpdate();
            if(iUpdatedCount > 0 ) {
            	isCreated = true;
            }
        }finally {
        	/* Releasing PreparedStatement objects */
            oDBUtility.releaseResources( rs,pstmt);
            oDBUtility.releaseResources(pstmt1);
        }
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );
        return isCreated;
		
	}
	
	public LocationDto getLocationInfo(Connection conn , LocationDto oLocationDto)throws Exception {
		/*
    	 * Setting the Method Name as generic for logger Utility purpose . 
    	 */
		String sMethodName = "getLocationInfo" ;
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
    		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;      
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        try {
        	pstmt = conn.prepareStatement(FETCH_LOCATION_INFO);
        	pstmt.setInt(1,oLocationDto.getLocationId());
        	rs = pstmt.executeQuery();
			while(rs.next()) {
				oLocationDto.setLocationName(rs.getString("QLO_LOCATIONNAME"));
				oLocationDto.setLocationId(rs.getInt("QLO_LOCATIONID"));
				oLocationDto.setRegionId(rs.getInt("QLO_REGIONID"));
				oLocationDto.setStatus(rs.getString("QLO_ISACTIVE"));
			}
		}finally {
			/* Releasing ResultSet & PreparedStatement objects */
        		oDBUtility.releaseResources(rs,pstmt);
		}
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );		
		return oLocationDto;
	}
	
	public boolean saveLocationValue(Connection conn , LocationDto oLocationDto)throws Exception {
		/*
    	 * Setting the Method Name as generic for logger Utility purpose . 
    	 */
		String sMethodName = "saveLocationValue" ;
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
    	/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
		boolean isUpdated = false ;
		int iUdate=0;
        try {
        	pstmt = conn.prepareStatement(UPDATE_LOCATION_VALUE);
        	pstmt.setString(1, oLocationDto.getLocationName());
        	pstmt.setInt(2, oLocationDto.getRegionId());
        	pstmt.setString(3, oLocationDto.getStatus());
        	pstmt.setString(4, oLocationDto.getLastUpdatedById());
        	pstmt.setInt(5, oLocationDto.getLocationId());
        	iUdate = pstmt.executeUpdate();
        	if(iUdate > 0) {
        		isUpdated = true ;
        	}
        }finally {
        	/* Releasing PreparedStatement objects */
            oDBUtility.releaseResources( pstmt);
        }
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );
		return isUpdated;
	}
	
	public boolean deleteLocationValue(Connection conn , LocationDto oLocationDto)throws Exception{
     	/*
    	 * Setting the Method Name as generic for logger Utility purpose . 
    	 */
		String sMethodName = "deleteLocationValue" ;
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );

    	/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
		boolean isDeleted = false ;
		int iDelete=0;
		try {
			pstmt = conn.prepareStatement(DELETE_LOCATION_VALUE);
			pstmt.setInt(1, oLocationDto.getLocationId());
			iDelete = pstmt.executeUpdate();
	        	if(iDelete > 0) {
	        		isDeleted = true ;
	        	}
	        }finally {
	        	/* Releasing PreparedStatement objects */
	            oDBUtility.releaseResources( pstmt);
	        }
	        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );
		return isDeleted;
	}
	
	public ArrayList exportLocationSearchResult(Connection conn , LocationDto oLocationDto,String sWhereQuery, String sSortFilter, String sSortType)throws Exception {
		
		/*
    	 * Setting the Method Name as generic for logger Utility purpose . 
    	 */
    		String sMethodName = "getCustomer Mehotd" ;
    		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
    		
    		/* PreparedStatement object to handle database operations */
            PreparedStatement pstmt = null ;
            
            /* ResultSet object to store the rows retrieved from database */
            ResultSet rs = null ;      
            
            /* Object of DBUtility class to handle database operations */
            DBUtility oDBUtility = new DBUtility();
            
            ArrayList alSearchResultsList = new ArrayList(); // Stores the list of rows retrieved from the database
            
            String sSqlQueryString = null; // Stores the complete SQL query to be executed
           
            LocationDto oTempLocationDto=null ;// Create the Temporary Location Object for storing the table column values
            try {
            	
            	sSqlQueryString = FETCH_LOCATION_SEARCHLIST;
            	
            	if (sWhereQuery != null){
            		sSqlQueryString = sSqlQueryString + " "+sWhereQuery;            		
            	}
            	
            	if (sSortFilter != null && sSortFilter.trim().length() > 0){
            		sSqlQueryString = sSqlQueryString + " ORDER BY "+ sSortFilter;
            		if (sSortType != null && sSortType.trim().length() > 0)
            			sSqlQueryString = sSqlQueryString + " " +sSortType;
            	}
                
            	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName+"..Search Location Count Query in Expert..",sSqlQueryString+" WHERE QUERY "+sWhereQuery);
            	
            	pstmt = conn.prepareCall(sSqlQueryString);
            	rs = pstmt.executeQuery();// Execute the Search Query to retriuve the Location Values .
            	
            	 
         		if (rs.next()) {
                     do {
                    	 oTempLocationDto = new LocationDto();
                    	 oTempLocationDto.setLocationName(rs.getString("QLO_LOCATIONNAME"));
                    	 oTempLocationDto.setRegionName(rs.getString("QRE_REGIONNAME"));
                    	 oTempLocationDto.setStatus(rs.getString("QLO_ISACTIVE"));
                    	 oTempLocationDto.setLocationId(rs.getInt("QLO_LOCATIONID"));
                    	 alSearchResultsList.add(oTempLocationDto);
                    } while (rs.next());
                 }
            }finally {
            	/* Releasing PreparedStatement objects */
                oDBUtility.releaseResources( rs,pstmt);
            }
		
		return alSearchResultsList;
	}
	
	public boolean updateLocationStatusValue(Connection conn, LocationDto oLocationDto)throws Exception {
		/*
    	 * Setting the Method Name as generic for logger Utility purpose . 
    	 */
		String sMethodName = "updateLocationStatusValue" ;
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
    	/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
		boolean isUpdated = false ;
		int iUdate=0;
		
        try {
        	pstmt = conn.prepareStatement(UPDATE_LOCATION_STATUS);
        	pstmt.setString(1, oLocationDto.getStatus());
        	pstmt.setString(2, oLocationDto.getLastUpdatedById());
        	pstmt.setInt(3, oLocationDto.getLocationId());
        	
        	iUdate = pstmt.executeUpdate();
        	if(iUdate > 0) {
        		isUpdated = true ;
        	}
        }finally {
        	/* Releasing PreparedStatement objects */
            oDBUtility.releaseResources( pstmt);
        }
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );
		return isUpdated;
	}
}
