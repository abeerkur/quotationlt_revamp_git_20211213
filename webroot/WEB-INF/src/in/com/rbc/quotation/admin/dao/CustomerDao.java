/**
 * *****************************************************************************
 * Project Name: quotation
 * Document Name: CustomerDao.java
 * Package: in.com.rbc.quotation.admin.dao
 * Desc: 
 * *****************************************************************************
 * Author: 100005701 (Suresh)
 * Date: Sep 30, 2008
 * *****************************************************************************
 */
package in.com.rbc.quotation.admin.dao;

import in.com.rbc.quotation.admin.dto.CustomerDto;
import in.com.rbc.quotation.admin.form.CustomerForm;
import in.com.rbc.quotation.common.utility.ListModel;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * @author 100005701 (Suresh Shanmugam)
 *
 */
public interface CustomerDao {
	
	/**
	 * 
	 * @param conn
	 * @param oCustomerDto
	 * @param oListModel
	 * @return
	 * @throws Exception
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Sep 30, 2008
	 */
	public Hashtable getCustomer(Connection conn, CustomerDto oCustomerDto, ListModel oListModel) throws Exception ;

	/**
	 * 
	 * @param conn
	 * @return
	 * @throws Exception
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Sep 30, 2008
	 */
	public ArrayList getLocationList(Connection conn)throws Exception;
	
	
	/**
	 * 
	 * @param conn
	 * @param oCustomerDto
	 * @return
	 * @throws Exception
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 7, 2008
	 */
	public boolean createCustomer(Connection conn, CustomerDto oCustomerDto)throws Exception ;
	
	/**
	 * 
	 * @param conn
	 * @param oCustomerDto
	 * @return
	 * @throws Exception
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 7, 2008
	 */
	public CustomerDto getCustomerInfo(Connection conn , CustomerDto oCustomerDto)throws Exception ;
	
	/**
	 * 
	 * @param conn
	 * @param oCustomerDto
	 * @return
	 * @throws Exception
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 7, 2008
	 */
	public boolean saveCustomerValue(Connection conn , CustomerDto oCustomerDto)throws Exception ;
	
	/**
	 * 
	 * @param conn
	 * @param oCustomerDto
	 * @return
	 * @throws Exception
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 7, 2008
	 */
	public boolean deleteCustomerValue(Connection conn , CustomerDto oCustomerDto)throws Exception;
	
	/**
	 * 
	 * @param conn
	 * @param oCustomerDto
	 * @return
	 * @throws Exception
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 7, 2008
	 */
	public ArrayList exportCustomerSearchResult(Connection conn , CustomerDto oCustomerDto,String sWhereQuery , String sSortOrder, String sSortType)throws Exception ;
	
	
	/**
	 * 
	 * @param conn
	 * @param oCustomerDto
	 * @return
	 * @throws Exception
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 7, 2008
	 */
	public ArrayList getCustomerName(Connection conn , CustomerDto oCustomerDto)throws Exception ;
	
	/**
	 * 
	 * @param conn
	 * @param oCustomerDto
	 * @return
	 * @throws Exception
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 8, 2008
	 */
	public ArrayList getLocationsByCustomerName(Connection conn , CustomerDto oCustomerDto, String displayContactPerson)throws Exception ;
    
    /**
     * 
     * @param conn
     * @param oCustomerDto
     * @return
     * @throws Exception
     * @author 100005701 (Suresh Shanmugam)
     * Created on: Feb 10, 2009
     */
    public boolean canDeleteCustomer(Connection conn, CustomerDto oCustomerDto) throws Exception;
    
 
	/**
	 * 
	 * @param conn
	 * @param oCustomerDto
	 * @return
	 * @throws Exception
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Feb 10, 2009
	 */
	public int isCustomerAlreadyExists(Connection conn ,CustomerDto oCustomerDto) throws Exception ;
	

	/**
	 * 
	 * @param conn
	 * @param oCustomerDto
	 * @return
	 * @throws Exception
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Feb 10, 2009
	 */
	public boolean updateExistCustomer(Connection conn , CustomerDto oCustomerDto) throws Exception ;
	
	/**
	 * 
	 * @param conn
	 * @return
	 * @throws Exception
	 * @author 900008798 (Abhilash Moola)
	 * Created on: December 1, 2020
	 */
	public ArrayList getSalesManagersList(Connection conn)throws Exception;
	
	public ArrayList getLocationName(Connection conn, CustomerDto oCustomerDto) throws Exception;
	
	public ArrayList getLocationBySearchString(Connection conn, CustomerDto oCustomerDto) throws Exception;
	
	public ArrayList getCustomerIndustryName(Connection conn, CustomerDto oCustomerDto) throws Exception;
	
	public ArrayList fetchLocationsByCustomerId(Connection conn, CustomerDto oCustomerDto) throws Exception;
	
	public ArrayList fetchContactsByCustomerId(Connection conn, CustomerDto oCustomerDto) throws Exception;

	public String fetchCustomerIdByName(Connection conn, String sCustomerName) throws Exception;
	
	public String fetchCustomerTypeById(Connection conn, CustomerDto oCustomerDto) throws Exception;
	
	
}
