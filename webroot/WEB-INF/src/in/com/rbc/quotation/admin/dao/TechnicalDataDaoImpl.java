/**
 * *****************************************************************************
 * Project Name: quotation
 * Document Name: TechnicalDataDaoImpl.java
 * Package: in.com.rbc.quotation.admin.dao
 * Desc: 
 * *****************************************************************************
 * Author: 900008798 (Abhilash Moola)
 * Date: Nov 20, 2020
 * *****************************************************************************
 */
package in.com.rbc.quotation.admin.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import in.com.rbc.quotation.admin.dto.LocationDto;
import in.com.rbc.quotation.admin.dto.TechnicalDataDto;
import in.com.rbc.quotation.common.utility.DBUtility;
import in.com.rbc.quotation.common.utility.ListDBColumnUtil;
import in.com.rbc.quotation.common.utility.ListModel;
import in.com.rbc.quotation.common.utility.LoggerUtility;
import in.com.rbc.quotation.common.vo.KeyValueVo;

/**
 * @author 900008798 (Abhilash Moola)
 *
 */
public class TechnicalDataDaoImpl implements TechnicalDataDao,AdminQueries {
	
	public Hashtable getTechnicalSearchResult(Connection conn, TechnicalDataDto oTechnicalDataDto, ListModel oListModel) throws Exception {
		
		/*
    	* Setting the Method Name as generic for logger Utility purpose . 
    	*/
		String sMethodName = "getLocationSearchResult Mehotd" ;
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;      
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
       
        /*
         * Hashtable that stores the following values, and that is used to display results in search page
         * --> ArrayList containing the rows retrieved from database
         * --> ListModel object that hold the values used to retrieve values and display the same
         * --> String array containing the list of column names to be displayed as column headings
         */
        Hashtable htSearchResults = null;
        
        ArrayList alSearchResultsList = new ArrayList(); // Stores the list of rows retrieved from the database
        
        
        int iCursorPosition = 0; // Stores the cursor position from where the records have to be retrieved // from database; this is dependent on the page number selected for viewing
        
        String sWhereString = null; // Stores the Where clause condition
        String sSqlQueryString = null; // Stores the complete SQL query to be executed
        TechnicalDataDto oTempTechnicalDataDto ;// Create the Temporary Location Object for storing the table column values 

        try {
        	
        	sWhereString = buildWhereClause(oTechnicalDataDto);
        	sSqlQueryString = COUNT_TECHNICALDATA_SEARCHRESULT + sWhereString;
        	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName+"..Search Technical data Count Query",sSqlQueryString);
        	
        	pstmt = conn.prepareCall(sSqlQueryString);
        	
        	/* Execute the Query for counting the number records*/
        	rs=pstmt.executeQuery();
        	
        	if(rs.next()) {
        		oListModel.setTotalRecordCount(rs.getInt(1));//set total number of records available in table depond upon the where conditions .
        	}else {
        		oListModel.setTotalRecordCount(0);// if the Query return no records , it's set the Total recordcount  "0" as default
        	}
        	oDBUtility.releaseResources(rs, pstmt);
        	 
        	 if(oListModel.getTotalRecordCount()>0) {
        		 htSearchResults = new Hashtable();
        		 
        		 if(oListModel.getCurrentPageInt()<0)
        			 throw new Exception("Current Page is negative in search method...");
        		 
        		 iCursorPosition = (oListModel.getCurrentPageInt()-1) * oListModel.getRecordsPerPageInt();
        		 
        		 if(iCursorPosition < oListModel.getTotalRecordCount()) {
             		
			 /*
			  * Select the values from the table Using FETCH_SEARCHLocation_RESULTS Query depond upon the 
			  * sWhereString condition and return into the sSqlQueryString as StringBuffer 
			  * and the data's are ordered by Location DBFiled as by default
			  * 
			  */
             		sSqlQueryString = new StringBuffer("SELECT * FROM (SELECT Q.*, rownum AS rnum FROM ( ")
 						                    .append(FETCH_TECHNICALDATA_LIST)
 						                    .append(sWhereString)
 						                    .append(" ORDER BY ")
 						                    .append(ListDBColumnUtil.getTechnicalDataResultDBField(oListModel.getSortBy()))
 						                    .append(" " + oListModel.getSortOrderDesc())
 						                    .append(" ) Q WHERE rownum <= (")
 						                    .append(iCursorPosition + " + " + oListModel.getRecordsPerPage() + ")) ")
 						                    .append(" WHERE rnum > " + iCursorPosition + "")
 						                    .toString();
             		

             		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName+"..SearchLocation Retrive Query",sSqlQueryString );
             		pstmt = conn.prepareStatement(sSqlQueryString, ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
             		
             		rs =pstmt.executeQuery();// Execute the Search Query to retriuve the Location Values .
             		if (rs.next()) {
                         do {
                        	 oTempTechnicalDataDto = new TechnicalDataDto();
                        	 oTempTechnicalDataDto.setSERIAL_ID(rs.getInt("SERIAL_ID"));
                        	 oTempTechnicalDataDto.setMANUFACTURING_LOCATION(rs.getString("MANUFACTURING_LOCATION"));
                        	 oTempTechnicalDataDto.setPRODUCT_GROUP(rs.getString("PRODUCT_GROUP"));	
                        	 oTempTechnicalDataDto.setPRODUCT_LINE(rs.getString("PRODUCT_LINE"));	
                        	 oTempTechnicalDataDto.setSTANDARD(rs.getString("STANDARD"));	
                        	 oTempTechnicalDataDto.setPOWER_RATING_KW(rs.getString("POWER_RATING_KW"));
                        	 oTempTechnicalDataDto.setFRAME_TYPE(rs.getString("FRAME_TYPE"));
                        	 oTempTechnicalDataDto.setFRAME_SUFFIX(rs.getString("FRAME_SUFFIX"));
                        	 oTempTechnicalDataDto.setNUMBER_OF_POLES(rs.getString("NUMBER_OF_POLES"));
                        	 oTempTechnicalDataDto.setMOUNTING_TYPE(rs.getString("MOUNTING_TYPE"));
                        	 oTempTechnicalDataDto.setTB_POSITION(rs.getString("TB_POSITION"));
                        	 oTempTechnicalDataDto.setMODEL_NO(rs.getString("MODEL_NO"));
                        	 oTempTechnicalDataDto.setVOLTAGE_V(rs.getString("VOLTAGE_V"));
                        	 oTempTechnicalDataDto.setPOWER_RATING_HP(rs.getString("POWER_RATING_HP"));	
                        	 oTempTechnicalDataDto.setFREQUENCY_HZ(rs.getString("FREQUENCY_HZ"));
                        	 oTempTechnicalDataDto.setCURRENT_A(rs.getString("CURRENT_A"));	
                        	 oTempTechnicalDataDto.setSPEED_MAX(rs.getString("SPEED_MAX"));
                        	 oTempTechnicalDataDto.setPOWER_FACTOR(rs.getString("POWER_FACTOR"));	
                        	 oTempTechnicalDataDto.setEFFICIENCY(rs.getString("EFFICIENCY"));
                        	 oTempTechnicalDataDto.setELECTRICAL_TYPE(rs.getString("ELECTRICAL_TYPE"));
                        	 oTempTechnicalDataDto.setIP_CODE(rs.getString("IP_CODE"));
                        	 oTempTechnicalDataDto.setINSULATION_CLASS(rs.getString("INSULATION_CLASS"));	
                        	 oTempTechnicalDataDto.setSERVICE_FACTOR(rs.getString("SERVICE_FACTOR"));
                        	 oTempTechnicalDataDto.setDUTY(rs.getString("DUTY"));
                        	 oTempTechnicalDataDto.setNUMBER_OF_PHASES(rs.getString("NUMBER_OF_PHASES"));
                        	 oTempTechnicalDataDto.setMAX_AMBIENT_C(rs.getString("MAX_AMBIENT_C"));	
                        	 oTempTechnicalDataDto.setDE_BEARING_SIZE(rs.getString("DE_BEARING_SIZE"));	
                        	 oTempTechnicalDataDto.setDE_BEARING_TYPE(rs.getString("DE_BEARING_TYPE"));	
                        	 oTempTechnicalDataDto.setODE_BEARING_SIZE(rs.getString("ODE_BEARING_SIZE"));	
                        	 oTempTechnicalDataDto.setODE_BEARING_TYPE(rs.getString("ODE_BEARING_TYPE"));	
                        	 oTempTechnicalDataDto.setENCLOSURE_TYPE(rs.getString("ENCLOSURE_TYPE"));	
                        	 oTempTechnicalDataDto.setMOTOR_ORIENTATION(rs.getString("MOTOR_ORIENTATION"));	
                        	 oTempTechnicalDataDto.setFRAME_MATERIAL(rs.getString("FRAME_MATERIAL"));	
                        	 oTempTechnicalDataDto.setFRAME_LENGTH(rs.getString("FRAME_LENGTH"));
                        	 oTempTechnicalDataDto.setROTATION(rs.getString("ROTATION"));	
                        	 oTempTechnicalDataDto.setNUMBER_OF_SPEEDS(rs.getString("NUMBER_OF_SPEEDS"));	
                        	 oTempTechnicalDataDto.setSHAFT_TYPE(rs.getString("SHAFT_TYPE"));	
                        	 oTempTechnicalDataDto.setSHAFT_DIAMETER_MM(rs.getString("SHAFT_DIAMETER_MM"));	
                        	 oTempTechnicalDataDto.setSHAFT_EXTENSION_MM(rs.getString("SHAFT_EXTENSION_MM"));
                        	 oTempTechnicalDataDto.setOUTLINE_DWG_NO(rs.getString("OUTLINE_DWG_NO"));
                        	 oTempTechnicalDataDto.setCONNECTION_DRAWING_NO(rs.getString("CONNECTION_DRAWING_NO"));	
                        	 oTempTechnicalDataDto.setOVERALL_LENGTH_MM(rs.getString("OVERALL_LENGTH_MM"));	
                        	 oTempTechnicalDataDto.setSTARTING_TYPE(rs.getString("STARTING_TYPE"));	
                        	 oTempTechnicalDataDto.setTHRU_BOLTS_EXTENSION(rs.getString("THRU_BOLTS_EXTENSION"));	
                        	 oTempTechnicalDataDto.setTYPE_OF_OVERLOAD_PROTECTION(rs.getString("TYPE_OF_OVERLOAD_PROTECTION"));	
                        	 oTempTechnicalDataDto.setCE(rs.getString("CE"));	
                        	 oTempTechnicalDataDto.setCSA(rs.getString("CSA"));
                        	 oTempTechnicalDataDto.setUL(rs.getString("UL"));
                        	 		
                        	 		
                        	 
                        	 
                        	 
                        	 alSearchResultsList.add(oTempTechnicalDataDto);
                        } while (rs.next());
                     }
               }
        		 
        		 htSearchResults.put("resultsList", alSearchResultsList);// put the SearchresultList of arraylist value into Hashtable 
                 htSearchResults.put("ListModel", oListModel); // put the ListModel values into hashtable 
                 htSearchResults.put("locationQuery", sWhereString);//keep the Where condition for using in Export to Excel purpose .
             }
        	 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END.." );
        }finally {
        	 /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
        return htSearchResults ;
	}
	
	private String buildWhereClause(TechnicalDataDto oTechnicalDataDto) {
		
		/*String sMethodName = "buildWhereClause";
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
        
        StringBuffer sbWhereString = new StringBuffer(" AND");
        
         Location Name 
        if (oLocationDto.getLocationName() != null && oLocationDto.getLocationName().trim().length() > 0){
        	sbWhereString.append(" UPPER(LOC.QLO_LOCATIONNAME) LIKE UPPER('%"+oLocationDto.getLocationName().trim().toUpperCase()+"%')");
        	sbWhereString.append(" AND");
        }
        
         Region Id 
        if ( oLocationDto.getRegionId()  > 0){
        	sbWhereString.append(" UPPER(LOC.QLO_REGIONID)= "+oLocationDto.getRegionId());
        	sbWhereString.append(" AND");
        }
        
        if ( sbWhereString.toString().trim().equalsIgnoreCase(" AND") )
            return "";
        else if (sbWhereString.toString().trim().endsWith(" AND"))        	
            return sbWhereString.toString().substring(0, sbWhereString.toString().lastIndexOf("AND"));        
        else
        	return "";*/
		
		return "";
	}
	
	public boolean addNewTechnicalData(Connection conn, TechnicalDataDto oTechnicalDataDto) throws Exception {
		
		boolean isUpdate = false;
		 /*
    	* Setting the Method Name as generic for logger Utility purpose . 
        */
		String sMethodName = "addNewTechnicalData" ;
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        PreparedStatement pstmt1 = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;      
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        int serial_id = 0;
        try {
        	/**
        	 * Getting the technical data serial id from the table
        	 */
        	pstmt1 = conn.prepareStatement(CREATE_TECHNICALdATA_ID);
        	rs = pstmt1.executeQuery();// Execute the creating the Query
        	
        	if(rs.next()) {
        		serial_id = rs.getInt("MAX_VAL");
        	}
        	
        	pstmt = conn.prepareStatement(INSERT_TECHNICAL_DATA);
        	pstmt.setInt(1, serial_id);
        	pstmt.setString(2,oTechnicalDataDto.getMANUFACTURING_LOCATION());
        	pstmt.setString(3,oTechnicalDataDto.getPRODUCT_GROUP());
        	pstmt.setString(4,oTechnicalDataDto.getPRODUCT_LINE());
        	pstmt.setString(5,oTechnicalDataDto.getSTANDARD());
        	pstmt.setString(6,oTechnicalDataDto.getPOWER_RATING_KW());
        	pstmt.setString(7,oTechnicalDataDto.getFRAME_TYPE());
        	pstmt.setString(8,oTechnicalDataDto.getNUMBER_OF_POLES());
        	pstmt.setString(9,oTechnicalDataDto.getMOUNTING_TYPE());
        	pstmt.setString(10,oTechnicalDataDto.getTB_POSITION());
        	pstmt.setString(11,oTechnicalDataDto.getMODEL_NO());
        	pstmt.setString(12,oTechnicalDataDto.getVOLTAGE_V());
        	pstmt.setString(13,oTechnicalDataDto.getPOWER_RATING_HP());
        	pstmt.setString(14,oTechnicalDataDto.getFREQUENCY_HZ());
        	pstmt.setString(15,oTechnicalDataDto.getCURRENT_A());
        	pstmt.setString(16,oTechnicalDataDto.getSPEED_MAX());
        	pstmt.setString(17,oTechnicalDataDto.getPOWER_FACTOR());	
        	pstmt.setString(18,oTechnicalDataDto.getEFFICIENCY());
        	pstmt.setString(19,oTechnicalDataDto.getELECTRICAL_TYPE());
        	pstmt.setString(20,oTechnicalDataDto.getIP_CODE());
        	pstmt.setString(21,oTechnicalDataDto.getINSULATION_CLASS());
        	pstmt.setString(22,oTechnicalDataDto.getSERVICE_FACTOR());
        	pstmt.setString(23,oTechnicalDataDto.getDUTY());
        	pstmt.setString(24,oTechnicalDataDto.getNUMBER_OF_PHASES());
        	pstmt.setString(25,oTechnicalDataDto.getMAX_AMBIENT_C());
        	pstmt.setString(26,oTechnicalDataDto.getDE_BEARING_SIZE());
        	pstmt.setString(27,oTechnicalDataDto.getDE_BEARING_TYPE());
        	pstmt.setString(28,oTechnicalDataDto.getODE_BEARING_SIZE());	
        	pstmt.setString(29,oTechnicalDataDto.getODE_BEARING_TYPE());	
        	pstmt.setString(30,oTechnicalDataDto.getENCLOSURE_TYPE());
        	pstmt.setString(31,oTechnicalDataDto.getMOTOR_ORIENTATION());
        	pstmt.setString(32,oTechnicalDataDto.getFRAME_MATERIAL());	
        	pstmt.setString(33,oTechnicalDataDto.getFRAME_LENGTH());
        	pstmt.setString(34,oTechnicalDataDto.getROTATION());
        	pstmt.setString(35,oTechnicalDataDto.getNUMBER_OF_SPEEDS());
        	pstmt.setString(36,oTechnicalDataDto.getSHAFT_TYPE());
        	pstmt.setString(37,oTechnicalDataDto.getSHAFT_DIAMETER_MM());
        	pstmt.setString(38,oTechnicalDataDto.getSHAFT_EXTENSION_MM());
        	pstmt.setString(39,oTechnicalDataDto.getOUTLINE_DWG_NO());
        	pstmt.setString(40,oTechnicalDataDto.getCONNECTION_DRAWING_NO());
        	pstmt.setString(41,oTechnicalDataDto.getOVERALL_LENGTH_MM());
        	pstmt.setString(42,oTechnicalDataDto.getSTARTING_TYPE());
        	pstmt.setString(43,oTechnicalDataDto.getTHRU_BOLTS_EXTENSION());
        	pstmt.setString(44,oTechnicalDataDto.getTYPE_OF_OVERLOAD_PROTECTION());
        	pstmt.setString(45,oTechnicalDataDto.getCE());
        	pstmt.setString(46,oTechnicalDataDto.getCSA());
        	pstmt.setString(47,oTechnicalDataDto.getUL());
        	pstmt.setString(48,oTechnicalDataDto.getFRAME_SUFFIX());
            int iUpdatedCount = pstmt.executeUpdate();
            if(iUpdatedCount > 0 ) {
            	isUpdate = true;
            }
        }finally {
        	/* Releasing PreparedStatement objects */
            oDBUtility.releaseResources( rs,pstmt);
            oDBUtility.releaseResources(pstmt1);
        }
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );
		
		return isUpdate;
	}
	
	public boolean updateTechnicalData(Connection conn, TechnicalDataDto oTechnicalDataDto) throws Exception {
		
		boolean isUpdate = false;
		 /*
    	* Setting the Method Name as generic for logger Utility purpose . 
        */
		String sMethodName = "addNewTechnicalData" ;
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        PreparedStatement pstmt1 = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;      
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        try {
        	/**
        	 * Getting the technical data serial id from the table
        	 */
        	
        	pstmt = conn.prepareStatement(UPDATE_TECHNICAL_DATA);
        	pstmt.setString(1,oTechnicalDataDto.getMANUFACTURING_LOCATION());
        	pstmt.setString(2,oTechnicalDataDto.getPRODUCT_GROUP());
        	pstmt.setString(3,oTechnicalDataDto.getPRODUCT_LINE());
        	pstmt.setString(4,oTechnicalDataDto.getSTANDARD());
        	pstmt.setString(5,oTechnicalDataDto.getPOWER_RATING_KW());
        	pstmt.setString(6,oTechnicalDataDto.getFRAME_TYPE());
        	pstmt.setString(7,oTechnicalDataDto.getNUMBER_OF_POLES());
        	pstmt.setString(8,oTechnicalDataDto.getMOUNTING_TYPE());
        	pstmt.setString(9,oTechnicalDataDto.getTB_POSITION());
        	pstmt.setString(10,oTechnicalDataDto.getMODEL_NO());
        	pstmt.setString(11,oTechnicalDataDto.getVOLTAGE_V());
        	pstmt.setString(12,oTechnicalDataDto.getPOWER_RATING_HP());
        	pstmt.setString(13,oTechnicalDataDto.getFREQUENCY_HZ());
        	pstmt.setString(14,oTechnicalDataDto.getCURRENT_A());
        	pstmt.setString(15,oTechnicalDataDto.getSPEED_MAX());
        	pstmt.setString(16,oTechnicalDataDto.getPOWER_FACTOR());	
        	pstmt.setString(17,oTechnicalDataDto.getEFFICIENCY());
        	pstmt.setString(18,oTechnicalDataDto.getELECTRICAL_TYPE());
        	pstmt.setString(19,oTechnicalDataDto.getIP_CODE());
        	pstmt.setString(20,oTechnicalDataDto.getINSULATION_CLASS());
        	pstmt.setString(21,oTechnicalDataDto.getSERVICE_FACTOR());
        	pstmt.setString(22,oTechnicalDataDto.getDUTY());
        	pstmt.setString(23,oTechnicalDataDto.getNUMBER_OF_PHASES());
        	pstmt.setString(24,oTechnicalDataDto.getMAX_AMBIENT_C());
        	pstmt.setString(25,oTechnicalDataDto.getDE_BEARING_SIZE());
        	pstmt.setString(26,oTechnicalDataDto.getDE_BEARING_TYPE());
        	pstmt.setString(27,oTechnicalDataDto.getODE_BEARING_SIZE());	
        	pstmt.setString(28,oTechnicalDataDto.getODE_BEARING_TYPE());	
        	pstmt.setString(29,oTechnicalDataDto.getENCLOSURE_TYPE());
        	pstmt.setString(30,oTechnicalDataDto.getMOTOR_ORIENTATION());
        	pstmt.setString(31,oTechnicalDataDto.getFRAME_MATERIAL());	
        	pstmt.setString(32,oTechnicalDataDto.getFRAME_LENGTH());
        	pstmt.setString(33,oTechnicalDataDto.getROTATION());
        	pstmt.setString(34,oTechnicalDataDto.getNUMBER_OF_SPEEDS());
        	pstmt.setString(35,oTechnicalDataDto.getSHAFT_TYPE());
        	pstmt.setString(36,oTechnicalDataDto.getSHAFT_DIAMETER_MM());
        	pstmt.setString(37,oTechnicalDataDto.getSHAFT_EXTENSION_MM());
        	pstmt.setString(38,oTechnicalDataDto.getOUTLINE_DWG_NO());
        	pstmt.setString(39,oTechnicalDataDto.getCONNECTION_DRAWING_NO());
        	pstmt.setString(40,oTechnicalDataDto.getOVERALL_LENGTH_MM());
        	pstmt.setString(41,oTechnicalDataDto.getSTARTING_TYPE());
        	pstmt.setString(42,oTechnicalDataDto.getTHRU_BOLTS_EXTENSION());
        	pstmt.setString(43,oTechnicalDataDto.getTYPE_OF_OVERLOAD_PROTECTION());
        	pstmt.setString(44,oTechnicalDataDto.getCE());
        	pstmt.setString(45,oTechnicalDataDto.getCSA());
        	pstmt.setString(46,oTechnicalDataDto.getUL());
        	pstmt.setString(47, oTechnicalDataDto.getFRAME_SUFFIX());
        	pstmt.setInt(48, oTechnicalDataDto.getSERIAL_ID());
            int iUpdatedCount = pstmt.executeUpdate();
            if(iUpdatedCount > 0 ) {
            	isUpdate = true;
            }
        }finally {
        	/* Releasing PreparedStatement objects */
            oDBUtility.releaseResources( rs,pstmt);
            oDBUtility.releaseResources(pstmt1);
        }
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );
		
		return isUpdate;
	}
	
	public TechnicalDataDto getTechnicalDataInfo(Connection conn, TechnicalDataDto oTechnicalDataDto) throws Exception{
		
		/*
    	* Setting the Method Name as generic for logger Utility purpose . 
    	*/
		String sMethodName = "getTechnicalDataInfo Mehotd" ;
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;      
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        try {
        	
        	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName+"..getTechnicalDataInfo Retrive Query","" );
     		pstmt = conn.prepareStatement(FETCH_TECHNICALDATA_LIST+" WHERE SERIAL_ID=? ");
     		pstmt.setInt(1, oTechnicalDataDto.getSERIAL_ID());
     		rs =pstmt.executeQuery();// Execute the Search Query to retriuve the Location Values .
     		if (rs.next()) {
                	 oTechnicalDataDto = new TechnicalDataDto();
                	 oTechnicalDataDto.setSERIAL_ID(rs.getInt("SERIAL_ID"));
                	 oTechnicalDataDto.setMANUFACTURING_LOCATION(rs.getString("MANUFACTURING_LOCATION"));
                	 oTechnicalDataDto.setPRODUCT_GROUP(rs.getString("PRODUCT_GROUP"));	
                	 oTechnicalDataDto.setPRODUCT_LINE(rs.getString("PRODUCT_LINE"));	
                	 oTechnicalDataDto.setSTANDARD(rs.getString("STANDARD"));	
                	 oTechnicalDataDto.setPOWER_RATING_KW(rs.getString("POWER_RATING_KW"));
                	 oTechnicalDataDto.setFRAME_TYPE(rs.getString("FRAME_TYPE"));
                	 oTechnicalDataDto.setFRAME_SUFFIX(rs.getString("FRAME_SUFFIX"));
                	 oTechnicalDataDto.setNUMBER_OF_POLES(rs.getString("NUMBER_OF_POLES"));
                	 oTechnicalDataDto.setMOUNTING_TYPE(rs.getString("MOUNTING_TYPE"));
                	 oTechnicalDataDto.setTB_POSITION(rs.getString("TB_POSITION"));
                	 oTechnicalDataDto.setMODEL_NO(rs.getString("MODEL_NO"));
                	 oTechnicalDataDto.setVOLTAGE_V(rs.getString("VOLTAGE_V"));
                	 oTechnicalDataDto.setPOWER_RATING_HP(rs.getString("POWER_RATING_HP"));	
                	 oTechnicalDataDto.setFREQUENCY_HZ(rs.getString("FREQUENCY_HZ"));
                	 oTechnicalDataDto.setCURRENT_A(rs.getString("CURRENT_A"));	
                	 oTechnicalDataDto.setSPEED_MAX(rs.getString("SPEED_MAX"));
                	 oTechnicalDataDto.setPOWER_FACTOR(rs.getString("POWER_FACTOR"));	
                	 oTechnicalDataDto.setEFFICIENCY(rs.getString("EFFICIENCY"));
                	 oTechnicalDataDto.setELECTRICAL_TYPE(rs.getString("ELECTRICAL_TYPE"));
                	 oTechnicalDataDto.setIP_CODE(rs.getString("IP_CODE"));
                	 oTechnicalDataDto.setINSULATION_CLASS(rs.getString("INSULATION_CLASS"));	
                	 oTechnicalDataDto.setSERVICE_FACTOR(rs.getString("SERVICE_FACTOR"));
                	 oTechnicalDataDto.setDUTY(rs.getString("DUTY"));
                	 oTechnicalDataDto.setNUMBER_OF_PHASES(rs.getString("NUMBER_OF_PHASES"));
                	 oTechnicalDataDto.setMAX_AMBIENT_C(rs.getString("MAX_AMBIENT_C"));	
                	 oTechnicalDataDto.setDE_BEARING_SIZE(rs.getString("DE_BEARING_SIZE"));	
                	 oTechnicalDataDto.setDE_BEARING_TYPE(rs.getString("DE_BEARING_TYPE"));	
                	 oTechnicalDataDto.setODE_BEARING_SIZE(rs.getString("ODE_BEARING_SIZE"));	
                	 oTechnicalDataDto.setODE_BEARING_TYPE(rs.getString("ODE_BEARING_TYPE"));	
                	 oTechnicalDataDto.setENCLOSURE_TYPE(rs.getString("ENCLOSURE_TYPE"));	
                	 oTechnicalDataDto.setMOTOR_ORIENTATION(rs.getString("MOTOR_ORIENTATION"));	
                	 oTechnicalDataDto.setFRAME_MATERIAL(rs.getString("FRAME_MATERIAL"));	
                	 oTechnicalDataDto.setFRAME_LENGTH(rs.getString("FRAME_LENGTH"));
                	 oTechnicalDataDto.setROTATION(rs.getString("ROTATION"));	
                	 oTechnicalDataDto.setNUMBER_OF_SPEEDS(rs.getString("NUMBER_OF_SPEEDS"));	
                	 oTechnicalDataDto.setSHAFT_TYPE(rs.getString("SHAFT_TYPE"));	
                	 oTechnicalDataDto.setSHAFT_DIAMETER_MM(rs.getString("SHAFT_DIAMETER_MM"));	
                	 oTechnicalDataDto.setSHAFT_EXTENSION_MM(rs.getString("SHAFT_EXTENSION_MM"));
                	 oTechnicalDataDto.setOUTLINE_DWG_NO(rs.getString("OUTLINE_DWG_NO"));
                	 oTechnicalDataDto.setCONNECTION_DRAWING_NO(rs.getString("CONNECTION_DRAWING_NO"));	
                	 oTechnicalDataDto.setOVERALL_LENGTH_MM(rs.getString("OVERALL_LENGTH_MM"));	
                	 oTechnicalDataDto.setSTARTING_TYPE(rs.getString("STARTING_TYPE"));	
                	 oTechnicalDataDto.setTHRU_BOLTS_EXTENSION(rs.getString("THRU_BOLTS_EXTENSION"));	
                	 oTechnicalDataDto.setTYPE_OF_OVERLOAD_PROTECTION(rs.getString("TYPE_OF_OVERLOAD_PROTECTION"));	
                	 oTechnicalDataDto.setCE(rs.getString("CE"));	
                	 oTechnicalDataDto.setCSA(rs.getString("CSA"));
                	 oTechnicalDataDto.setUL(rs.getString("UL"));
                	 		
                } 
     		 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END.." );
        }finally {
        	 /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
		
		return oTechnicalDataDto;
	}
	
	public boolean deleteTechnicalDataValue(Connection conn, TechnicalDataDto oTechnicalDataDto) throws Exception{
		/*
    	 * Setting the Method Name as generic for logger Utility purpose . 
    	 */
		String sMethodName = "deleteTechnicalDataValue" ;
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );

    	/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
		boolean isDeleted = false ;
		int iDelete=0;
		try {
			pstmt = conn.prepareStatement(DELETE_TECHNICALDATA_VALUE);
			pstmt.setInt(1, oTechnicalDataDto.getSERIAL_ID());
			iDelete = pstmt.executeUpdate();
	        	if(iDelete > 0) {
	        		isDeleted = true ;
	        	}
	        }finally {
	        	/* Releasing PreparedStatement objects */
	            oDBUtility.releaseResources( pstmt);
	        }
	        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );
		return isDeleted;
	}
	
	public boolean uploadBulkTechnicalData(Connection conn, List<TechnicalDataDto> oListTechnicalDataDto) throws Exception {
		
		boolean isUpdate = false;
		 /*
    	* Setting the Method Name as generic for logger Utility purpose . 
        */
		String sMethodName = "addNewTechnicalData" ;
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
		List<TechnicalDataDto> oAListTechnicalDataDto = oListTechnicalDataDto;
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        PreparedStatement pstmt1 = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;      
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        int serial_id = 0;
        try {
        	/**
        	 * Getting the technical data serial id from the table
        	 */
        	pstmt1 = conn.prepareStatement(CREATE_TECHNICALdATA_ID);
        	rs = pstmt1.executeQuery();// Execute the creating the Query
        	
        	if(rs.next()) {
        		serial_id = rs.getInt("MAX_VAL");
        	}
        	
        	Iterator<TechnicalDataDto> oiTechnicalDataDto = oListTechnicalDataDto.iterator();
        	TechnicalDataDto oTechnicalDataDto = null;
        	while(oiTechnicalDataDto.hasNext()) {
        		oTechnicalDataDto = oiTechnicalDataDto.next();
        		
        		pstmt = conn.prepareStatement(INSERT_TECHNICAL_DATA);
            	pstmt.setInt(1, serial_id);
            	pstmt.setString(2,oTechnicalDataDto.getMANUFACTURING_LOCATION());
            	pstmt.setString(3,oTechnicalDataDto.getPRODUCT_GROUP());
            	pstmt.setString(4,oTechnicalDataDto.getPRODUCT_LINE());
            	pstmt.setString(5,oTechnicalDataDto.getSTANDARD());
            	pstmt.setString(6,oTechnicalDataDto.getPOWER_RATING_KW());
            	pstmt.setString(7,oTechnicalDataDto.getFRAME_TYPE());
            	pstmt.setString(8,oTechnicalDataDto.getNUMBER_OF_POLES());
            	pstmt.setString(9,oTechnicalDataDto.getMOUNTING_TYPE());
            	pstmt.setString(10,oTechnicalDataDto.getTB_POSITION());
            	pstmt.setString(11,oTechnicalDataDto.getMODEL_NO());
            	pstmt.setString(12,oTechnicalDataDto.getVOLTAGE_V());
            	pstmt.setString(13,oTechnicalDataDto.getPOWER_RATING_HP());
            	pstmt.setString(14,oTechnicalDataDto.getFREQUENCY_HZ());
            	pstmt.setString(15,oTechnicalDataDto.getCURRENT_A());
            	pstmt.setString(16,oTechnicalDataDto.getSPEED_MAX());
            	pstmt.setString(17,oTechnicalDataDto.getPOWER_FACTOR());	
            	pstmt.setString(18,oTechnicalDataDto.getEFFICIENCY());
            	pstmt.setString(19,oTechnicalDataDto.getELECTRICAL_TYPE());
            	pstmt.setString(20,oTechnicalDataDto.getIP_CODE());
            	pstmt.setString(21,oTechnicalDataDto.getINSULATION_CLASS());
            	pstmt.setString(22,oTechnicalDataDto.getSERVICE_FACTOR());
            	pstmt.setString(23,oTechnicalDataDto.getDUTY());
            	pstmt.setString(24,oTechnicalDataDto.getNUMBER_OF_PHASES());
            	pstmt.setString(25,oTechnicalDataDto.getMAX_AMBIENT_C());
            	pstmt.setString(26,oTechnicalDataDto.getDE_BEARING_SIZE());
            	pstmt.setString(27,oTechnicalDataDto.getDE_BEARING_TYPE());
            	pstmt.setString(28,oTechnicalDataDto.getODE_BEARING_SIZE());	
            	pstmt.setString(29,oTechnicalDataDto.getODE_BEARING_TYPE());	
            	pstmt.setString(30,oTechnicalDataDto.getENCLOSURE_TYPE());
            	pstmt.setString(31,oTechnicalDataDto.getMOTOR_ORIENTATION());
            	pstmt.setString(32,oTechnicalDataDto.getFRAME_MATERIAL());	
            	pstmt.setString(33,oTechnicalDataDto.getFRAME_LENGTH());
            	pstmt.setString(34,oTechnicalDataDto.getROTATION());
            	pstmt.setString(35,oTechnicalDataDto.getNUMBER_OF_SPEEDS());
            	pstmt.setString(36,oTechnicalDataDto.getSHAFT_TYPE());
            	pstmt.setString(37,oTechnicalDataDto.getSHAFT_DIAMETER_MM());
            	pstmt.setString(38,oTechnicalDataDto.getSHAFT_EXTENSION_MM());
            	pstmt.setString(39,oTechnicalDataDto.getOUTLINE_DWG_NO());
            	pstmt.setString(40,oTechnicalDataDto.getCONNECTION_DRAWING_NO());
            	pstmt.setString(41,oTechnicalDataDto.getOVERALL_LENGTH_MM());
            	pstmt.setString(42,oTechnicalDataDto.getSTARTING_TYPE());
            	pstmt.setString(43,oTechnicalDataDto.getTHRU_BOLTS_EXTENSION());
            	pstmt.setString(44,oTechnicalDataDto.getTYPE_OF_OVERLOAD_PROTECTION());
            	pstmt.setString(45,oTechnicalDataDto.getCE());
            	pstmt.setString(46,oTechnicalDataDto.getCSA());
            	pstmt.setString(47,oTechnicalDataDto.getUL());
                int iUpdatedCount = pstmt.executeUpdate();
                if(iUpdatedCount > 0 ) {
                	isUpdate = true;
                	serial_id=serial_id+1;
                	oDBUtility.releaseResources(pstmt);
                }
        		
        	}
        	
        	
        }finally {
        	/* Releasing PreparedStatement objects */
            oDBUtility.releaseResources( rs,pstmt1);
            oDBUtility.releaseResources(pstmt);
        }
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );
		
		return isUpdate;
	}

}
