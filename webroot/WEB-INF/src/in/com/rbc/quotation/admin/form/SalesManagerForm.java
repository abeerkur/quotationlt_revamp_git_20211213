/**
 * *****************************************************************************
 * Project Name: quotation
 * Document Name: SalesManagerForm.java
 * Package: in.com.rbc.quotation.admin.form
 * Desc: 
 * *****************************************************************************
 * Author: 100005701 (Suresh)
 * Date: Oct 6, 2008
 * *****************************************************************************
 */
package in.com.rbc.quotation.admin.form;

import java.util.ArrayList;

import in.com.rbc.quotation.common.utility.CommonUtility;

import org.apache.struts.action.ActionForm;

/**
 * @author 100005701 (Suresh Shanmugam)
 *
 */
public class SalesManagerForm extends ActionForm{
	
	private String invoke ;
	private String operation;
	private String salesManager ;
	private String salesManagerSSO ;
	private String primaryRSM;
	private String primaryRSMSSO ;
	private String additionalRSM;
	private String additionalRSMSSO ;
	private String regionName ;
	private int regionId = 0;
	private int salesMgrId =0 ;
	
	private String createdById;
	private String lastUpdatedById ;
	private String createdDate ;
	private String lastUpdatedDate ;
	
	private String smContactNo;
	
	
	private ArrayList regionList = null;
	private ArrayList SearchResultsList = null;
	
	
	/**
	 * @return Returns the additionalRSM.
	 */
	public String getAdditionalRSM() {
		return CommonUtility.replaceNull(this.additionalRSM);
	}
	/**
	 * @param additionalRSM The additionalRSM to set.
	 */
	public void setAdditionalRSM(String additionalRSM) {
		this.additionalRSM = additionalRSM;
	}
	/**
	 * @return Returns the additionalRSMSSO.
	 */
	public String getAdditionalRSMSSO() {
		return CommonUtility.replaceNull(this.additionalRSMSSO);
	}
	/**
	 * @param additionalRSMSSO The additionalRSMSSO to set.
	 */
	public void setAdditionalRSMSSO(String additionalRSMSSO) {
		this.additionalRSMSSO = additionalRSMSSO;
	}
	/**
	 * @return Returns the createdById.
	 */
	public String getCreatedById() {
		return CommonUtility.replaceNull(this.createdById);
	}
	/**
	 * @param createdById The createdById to set.
	 */
	public void setCreatedById(String createdById) {
		this.createdById = createdById;
	}
	/**
	 * @return Returns the createdDate.
	 */
	public String getCreatedDate() {
		return CommonUtility.replaceNull(this.createdDate);
	}
	/**
	 * @param createdDate The createdDate to set.
	 */
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	/**
	 * @return Returns the invoke.
	 */
	public String getInvoke() {
		return CommonUtility.replaceNull(this.invoke);
	}
	/**
	 * @param invoke The invoke to set.
	 */
	public void setInvoke(String invoke) {
		this.invoke = invoke;
	}
	/**
	 * @return Returns the lastUpdatedById.
	 */
	public String getLastUpdatedById() {
		return CommonUtility.replaceNull(this.lastUpdatedById);
	}
	/**
	 * @param lastUpdatedById The lastUpdatedById to set.
	 */
	public void setLastUpdatedById(String lastUpdatedById) {
		this.lastUpdatedById = lastUpdatedById;
	}
	/**
	 * @return Returns the lastUpdatedDate.
	 */
	public String getLastUpdatedDate() {
		return CommonUtility.replaceNull(this.lastUpdatedDate);
	}
	/**
	 * @param lastUpdatedDate The lastUpdatedDate to set.
	 */
	public void setLastUpdatedDate(String lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	/**
	 * @return Returns the operation.
	 */
	public String getOperation() {
		return CommonUtility.replaceNull(this.operation);
	}
	/**
	 * @param operation The operation to set.
	 */
	public void setOperation(String operation) {
		this.operation = operation;
	}
	/**
	 * @return Returns the primaryRSM.
	 */
	public String getPrimaryRSM() {
		return CommonUtility.replaceNull(this.primaryRSM);
	}
	/**
	 * @param primaryRSM The primaryRSM to set.
	 */
	public void setPrimaryRSM(String primaryRSM) {
		this.primaryRSM = primaryRSM;
	}
	/**
	 * @return Returns the primaryRSMSSO.
	 */
	public String getPrimaryRSMSSO() {
		return CommonUtility.replaceNull(this.primaryRSMSSO);
	}
	/**
	 * @param primaryRSMSSO The primaryRSMSSO to set.
	 */
	public void setPrimaryRSMSSO(String primaryRSMSSO) {
		this.primaryRSMSSO = primaryRSMSSO;
	}
	/**
	 * @return Returns the regionId.
	 */
	public int getRegionId() {
		return  this.regionId;
	}
	/**
	 * @param regionId The regionId to set.
	 */
	public void setRegionId(int regionId) {
		this.regionId = regionId;
	}
	/**
	 * @return Returns the regionList.
	 */
	public ArrayList getRegionList() {
		return  this.regionList;
	}
	/**
	 * @param regionList The regionList to set.
	 */
	public void setRegionList(ArrayList regionList) {
		this.regionList = regionList;
	}
	/**
	 * @return Returns the regionName.
	 */
	public String getRegionName() {
		return CommonUtility.replaceNull(this.regionName);
	}
	/**
	 * @param regionName The regionName to set.
	 */
	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}
	/**
	 * @return Returns the salesManager.
	 */
	public String getSalesManager() {
		return CommonUtility.replaceNull(this.salesManager);
	}
	/**
	 * @param salesManager The salesManager to set.
	 */
	public void setSalesManager(String salesManager) {
		this.salesManager = salesManager;
	}
	/**
	 * @return Returns the salesManagerSSO.
	 */
	public String getSalesManagerSSO() {
		return CommonUtility.replaceNull(this.salesManagerSSO);
	}
	/**
	 * @param salesManagerSSO The salesManagerSSO to set.
	 */
	public void setSalesManagerSSO(String salesManagerSSO) {
		this.salesManagerSSO = salesManagerSSO;
	}
	/**
	 * @return Returns the salesMgrId.
	 */
	public int getSalesMgrId() {
		return  this.salesMgrId;
	}
	/**
	 * @param salesMgrId The salesMgrId to set.
	 */
	public void setSalesMgrId(int salesMgrId) {
		this.salesMgrId = salesMgrId;
	}
	/**
	 * @return Returns the searchResultsList.
	 */
	public ArrayList getSearchResultsList() {
		return  this.SearchResultsList;
	}
	/**
	 * @param searchResultsList The searchResultsList to set.
	 */
	public void setSearchResultsList(ArrayList searchResultsList) {
		this.SearchResultsList = searchResultsList;
	}
	
	public String getSmContactNo() {
		return CommonUtility.replaceNull(smContactNo);
	}
	public void setSmContactNo(String smContactNo) {
		this.smContactNo = smContactNo;
	}
}