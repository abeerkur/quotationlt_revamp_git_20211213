/**
 * *****************************************************************************
 * Project Name: quotation
 * Document Name: DesignEngineerDaoImpl.java
 * Package: in.com.rbc.quotation.admin.dao
 * Desc: 
 * *****************************************************************************
 * Author: 100005701 (Suresh)
 * Date: Oct 17, 2008
 * *****************************************************************************
 */
package in.com.rbc.quotation.admin.dao;

import in.com.rbc.quotation.admin.dto.DesignEngineerDto;

import in.com.rbc.quotation.common.utility.DBUtility;
import in.com.rbc.quotation.common.utility.ListDBColumnUtil;
import in.com.rbc.quotation.common.utility.ListModel;
import in.com.rbc.quotation.common.utility.LoggerUtility;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * @author 100005701 (Suresh Shanmugam)
 *
 */
public class DesignEngineerDaoImpl implements DesignEngineerDao , AdminQueries {
	
	/* (non-Javadoc)
	 * @see in.com.rbc.quotation.admin.dao.DesignEngineerDao#getDesignEngineerSearchResult(java.sql.Connection, in.com.rbc.quotation.admin.dto.DesignEngineerDto, in.com.rbc.quotation.common.utility.ListModel)
	 */
	public Hashtable getDesignEngineerSearchResult(Connection conn, DesignEngineerDto oDesignEngineerDto, ListModel oListModel) throws Exception {
	   /*
    	* Setting the Method Name as generic for logger Utility purpose . 
    	*/
		String sMethodName = "getDesignEngineerSearchResult";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
			
 		/* PreparedStatement object to handle database operations */
         PreparedStatement pstmt = null ;
         
         /* ResultSet object to store the rows retrieved from database */
         ResultSet rs = null ;      
         
         /* Object of DBUtility class to handle database operations */
         DBUtility oDBUtility = new DBUtility();
        
         /*
          * Hashtable that stores the following values, and that is used to display results in search page
          * --> ArrayList containing the rows retrieved from database
          * --> ListModel object that hold the values used to retrieve values and display the same
          * --> String array containing the list of column names to be displayed as column headings
          */
         Hashtable htSearchResults = null;
         
         ArrayList alSearchResultsList = new ArrayList(); // Stores the list of rows retrieved from the database
         
         
         int iCursorPosition = 0; // Stores the cursor position from where the records have to be retrieved // from database; this is dependent on the page number selected for viewing
         
         String sWhereString = null; // Stores the Where clause condition
         String sSqlQueryString = null; // Stores the complete SQL query to be executed
         
         try {
        	 sWhereString = " AND QDE_ENGINEERSSO LIKE '"+oDesignEngineerDto.getDesignEngineerSSO()+"%'";
         	 sSqlQueryString = COUNT_DESING_ENGINEER+sWhereString ;
         	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName+"..Search Desing Engineer Count Query",sSqlQueryString);
         	pstmt = conn.prepareCall(sSqlQueryString);
         	
         	/* Execute the Query for counting the number records*/
         	rs=pstmt.executeQuery();
         	
         	if(rs.next()) {
         		oListModel.setTotalRecordCount(rs.getInt(1));//set total number of records available in table depond upon the where conditions .
         	}else {
         		oListModel.setTotalRecordCount(0);// if the Query return no records , it's set the Total recordcount  "0" as default
         	}
         	oDBUtility.releaseResources(rs, pstmt);
         	DesignEngineerDto oTempDesignEngineerDto ;// Create the Temporary Location Object for storing the table column values 
         	 
         	 if(oListModel.getTotalRecordCount()>0) {
         		 htSearchResults = new Hashtable();
         		 
         		 if(oListModel.getCurrentPageInt()<0)
         			 throw new Exception("Current Page is negative in search method...");
         	
         		 iCursorPosition = (oListModel.getCurrentPageInt()-1) * oListModel.getRecordsPerPageInt();
         		 
         		 if(iCursorPosition < oListModel.getTotalRecordCount()) {
	 			 /*
	 			  * Select the values from the table Using FETCH_SEARCHLocation_RESULTS Query depond upon the 
	 			  * sWhereString condition and return into the sSqlQueryString as StringBuffer 
	 			  * and the data's are ordered by Location DBFiled as by default
	 			  * 
	 			  */
              		sSqlQueryString = new StringBuffer("SELECT * FROM (SELECT Q.*, rownum AS rnum FROM ( ")
  						                    .append(FETCH_DESIGN_ENGINEER)
  						                    .append(sWhereString)
  						                    .append(" ORDER BY ")
  						                    .append(ListDBColumnUtil.getDesignEngineerSearchResultDBField(oListModel.getSortBy()))
  						                    .append(" " + oListModel.getSortOrderDesc())
  						                    .append(" ) Q WHERE rownum <= (")
  						                    .append(iCursorPosition + " + " + oListModel.getRecordsPerPage() + ")) ")
  						                    .append(" WHERE rnum > " + iCursorPosition + "")
  						                    .toString();
              		
              		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName+"..Search Desing Engineer Retrive Query",sSqlQueryString );
              		pstmt = conn.prepareStatement(sSqlQueryString, ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
              		
              		rs= pstmt.executeQuery();// Execute the Search Query to retriuve the Location Values .
              		if (rs.next()) {
                          do {
                         	 oTempDesignEngineerDto = new DesignEngineerDto();
                         	oTempDesignEngineerDto.setDesignEngineerId(rs.getInt("QDE_ENGINEERID"));
                         	oTempDesignEngineerDto.setDesignEngineerName(rs.getString("DESIGNENGINEER_NAME"));
                         	oTempDesignEngineerDto.setDesignEngineerSSO(rs.getString("QDE_ENGINEERSSO"));
                         	 alSearchResultsList.add(oTempDesignEngineerDto);
                         } while (rs.next());
                      }
                }
         		 htSearchResults.put("resultsList", alSearchResultsList);// put the SearchresultList of arraylist value into Hashtable 
                  htSearchResults.put("ListModel", oListModel); // put the ListModel values into hashtable 
                  htSearchResults.put("locationQuery", sWhereString);//keep the Where condition for using in Export to Excel purpose .
              }
         	 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END.." );
         }finally {
         	 /* Releasing ResultSet & PreparedStatement objects */
             oDBUtility.releaseResources(rs, pstmt);
         }
         return htSearchResults ;
	}
	
	/* (non-Javadoc)
	 * @see in.com.rbc.quotation.admin.dao.DesignEngineerDao#createDesignEngineer(java.sql.Connection, in.com.rbc.quotation.admin.dto.DesignEngineerDto)
	 */
	public boolean createDesignEngineer(Connection conn, DesignEngineerDto oDesignEngineerDto)throws Exception {
		   /*
	    	* Setting the Method Name as generic for logger Utility purpose . 
	        */
			String sMethodName ="createDesignEngineer"; 
			LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
			
			/* PreparedStatement object to handle database operations */
	        PreparedStatement pstmt = null ;
	        PreparedStatement pstmt1 = null ;
	        
	        /* ResultSet object to store the rows retrieved from database */
	        ResultSet rs = null ;      
	        
	        /* Object of DBUtility class to handle database operations */
	        DBUtility oDBUtility = new DBUtility();
	        
	        /*getting the newlly Customer ID from the Customer sequence */
	        int iGetDesingengineerID = 0;
	        boolean isCreated = false ;
	        int iCreatedCount=0;
	        try {
	        	
		        	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"Create design Engineer ID query :="+ CREATE_DESIGNENGINEER_ID);
		        	pstmt1 = conn.prepareStatement(CREATE_DESIGNENGINEER_ID);
		        	rs = pstmt1.executeQuery();// Execute the creating the Customer Sequence Query,
		        	
		        	if(rs.next()) {
		        		iGetDesingengineerID = rs.getInt("DESIGNENGINEER_ID");
		        	}
		        	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"Insert design Engineer Vlues query :="+ INSERT_DESIGNENGINEER_VALUE);
		        	
		        	pstmt = conn.prepareStatement(INSERT_DESIGNENGINEER_VALUE);
		        	
		        	pstmt.setInt(1, iGetDesingengineerID);
		        	pstmt.setString(2,oDesignEngineerDto.getDesignEngineerSSO());
		        	pstmt.setString(3,oDesignEngineerDto.getCreatedById());
		        	pstmt.setString(4,oDesignEngineerDto.getLastUpdatedById());
		        	iCreatedCount= pstmt.executeUpdate();
		            if(iCreatedCount > 0 ) {
		            	isCreated = true;
		            }
	        	
	        	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"is design engineer created isCreated :="+ isCreated);
	            
	        }finally {
	        	/* Releasing PreparedStatement objects */
	            oDBUtility.releaseResources( rs,pstmt);
	            oDBUtility.releaseResources( pstmt1);
	        }
	        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );
	        return isCreated;
			
		}

	
	/* (non-Javadoc)
	 * @see in.com.rbc.quotation.admin.dao.DesignEngineerDao#getDesignEngineerInfo(java.sql.Connection, in.com.rbc.quotation.admin.dto.DesignEngineerDto)
	 */
	public DesignEngineerDto getDesignEngineerInfo(Connection conn , DesignEngineerDto oDesignEngineerDto)throws Exception {
		/*
    	 * Setting the Method Name as generic for logger Utility purpose . 
    	 */
		String sMethodName = "getDesignEngineerInfo";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
    		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs= null ;      
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();

        try {
        	pstmt = conn.prepareStatement(FETCH_DESIGNENGINEER_INFO);
        	pstmt.setInt(1,oDesignEngineerDto.getDesignEngineerId());
        	rs = pstmt.executeQuery();
			
			while(rs.next()) {
				oDesignEngineerDto.setDesignEngineerName(rs.getString("DESIGNENGINEER_NAME"));
				oDesignEngineerDto.setDesignEngineerSSO(rs.getString("QDE_ENGINEERSSO"));
			}
		}finally {
			/* Releasing ResultSet & PreparedStatement objects */
        		oDBUtility.releaseResources(rs,pstmt);
		}
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );		
		return oDesignEngineerDto;
	}
	//public boolean saveDesignEngineerVlaue(Connection conn , DesignEngineerDto oDesignEngineerDto)throws Exception 
	
	/* (non-Javadoc)
	 * @see in.com.rbc.quotation.admin.dao.DesignEngineerDao#saveDesignEngineerVlaue(java.sql.Connection, in.com.rbc.quotation.admin.dto.DesignEngineerDto)
	 */
	public boolean saveDesignEngineerVlaue(Connection conn , DesignEngineerDto oDesignEngineerDto)throws Exception  {
		/*
    	 * Setting the Method Name as generic for logger Utility purpose . 
    	 */
		String sMethodName = "saveDesignEngineerVlaue";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
    	/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
		boolean isUpdated = false ;

        try {
        	LoggerUtility.log("INFO",this.getClass().getName(),sMethodName,"Design Engineer ID :="+oDesignEngineerDto.getDesignEngineerId());
        	LoggerUtility.log("INFO",this.getClass().getName(),sMethodName,"Design Engineer Update Query  :="+UPDATE_DESIGNENGINEER_VALUE);
        	pstmt = conn.prepareStatement(UPDATE_DESIGNENGINEER_VALUE);
        	pstmt.setString(1, oDesignEngineerDto.getDesignEngineerSSO());
        	pstmt.setString(2, oDesignEngineerDto.getLastUpdatedById());
        	pstmt.setInt(3, oDesignEngineerDto.getDesignEngineerId());
            int isUdateRowCount = pstmt.executeUpdate();
        	if(isUdateRowCount > 0) {
        		isUpdated = true ;
        	}
        }finally {
        	/* Releasing PreparedStatement objects */
            oDBUtility.releaseResources( pstmt);
        }
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );
		return isUpdated;
	}
	
	
	public int isDEAlreadyExists(Connection conn , DesignEngineerDto oDesignEngineerDto) throws Exception  {
		
		/*
    	* Setting the Method Name as generic for logger Utility purpose . 
        */
		String sMethodName = "isDEAlreadyExists";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;      
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        /*getting the newlly Customer ID from the Customer sequence */
        int iExists = 0;
        
        try {
        	LoggerUtility.log("INFO",this.getClass().getName(),sMethodName,"FETCH_EXISTS_DE :="+FETCH_EXISTS_DE);
        	pstmt = conn.prepareStatement(FETCH_EXISTS_DE);
        	pstmt.setString(1,oDesignEngineerDto.getDesignEngineerSSO());
        	rs = pstmt.executeQuery();
			
			while(rs.next()) {
				if(rs.getInt("engcount")>0)
				{
					iExists = 1;
				}
			}
		}finally {
			/* Releasing ResultSet & PreparedStatement objects */
        		oDBUtility.releaseResources(rs,pstmt);
		}
		
		return iExists;
	}
	
	public boolean updateExistDE(Connection conn , DesignEngineerDto oDesignEngineerDto) throws Exception {
		/*
    	 * Setting the Method Name as generic for logger Utility purpose . 
    	 */
		String sMethodName = "updateExistDE";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
    	/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
		boolean isUpdated = false ;
		int iUdateRowCount=0;

        try {
        	LoggerUtility.log("INFO",this.getClass().getName(),sMethodName,"Design Engineer Update Query  :="+UPDATE_DESIGNENGINEER_EXISTS  +"Exists DE SSO :="+oDesignEngineerDto.getDesignEngineerSSO());
        	pstmt = conn.prepareStatement(UPDATE_DESIGNENGINEER_EXISTS);
        	pstmt.setString(2, oDesignEngineerDto.getDesignEngineerSSO());
        	pstmt.setString(1, oDesignEngineerDto.getLastUpdatedById());
           iUdateRowCount = pstmt.executeUpdate();
        	if(iUdateRowCount > 0) {
        		
        		isUpdated = true ;
        	}
        }finally {
        	/* Releasing PreparedStatement objects */
            oDBUtility.releaseResources( pstmt);
        }
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );
		return isUpdated;
	}
	
	
	
	/* (non-Javadoc)
	 * @see in.com.rbc.quotation.admin.dao.DesignEngineerDao#deleteDesignEngineer(java.sql.Connection, in.com.rbc.quotation.admin.dto.DesignEngineerDto)
	 */
	public boolean deleteDesignEngineer(Connection conn , DesignEngineerDto oDesignEngineerDto)throws Exception{
     	/*
    	 * Setting the Method Name as generic for logger Utility purpose . 
    	 */
		String sMethodName ="deleteDesignEngineer"; 
		String sClassName=this.getClass().getName();
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );

    	/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        PreparedStatement pstmt1 = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ; 
        
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
		boolean isDeleted = false ;
		int iCountRow = -1 ;
		int iUpdateRow =0;
		 try {
			 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"Delete Desing Engineer Query 	 ::=Enginner ID :="+oDesignEngineerDto.getDesignEngineerId());
			 pstmt = conn.prepareStatement(CHECK_DESIGNENGINEER_VALUE);
	        	
			 pstmt.setString(1, oDesignEngineerDto.getDesignEngineerSSO());
			 rs = pstmt.executeQuery();
	        	
        	   if(rs.next()) {
		        	iCountRow = rs.getInt(1);
		        	LoggerUtility.log("INFO", sClassName, sMethodName,"After Execute Query the number of records avaialble iCountRow in Design Engineer Value:="+iCountRow);
		        }		        
			        if(iCountRow ==0) {// if the count ==0 then the cursor will go inside of if loop ..
			        	LoggerUtility.log("INFO", sClassName, sMethodName,"Update Query for isDelete Column in Slaes Manager table :="+UPDATE_SALESMANAGER_DELETE_COLUMN);
			        	pstmt1 = conn.prepareStatement(UPDATE_DESIGNENGINEER_DELETE_COLUMN);
			        	pstmt1.setString(1,oDesignEngineerDto.getDesignEngineerSSO());
			        	iUpdateRow = pstmt1.executeUpdate();
			        	LoggerUtility.log("INFO", sClassName, sMethodName,"After Execute Update Query isUpdateRow :="+iUpdateRow);
			        	if(iUpdateRow > 0) {
			        		isDeleted = true ;
			        	}
			        }
	        	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"Afetr deletion design engineer value : result isDeleted :="+isDeleted );	
	        }finally {
	        	/* Releasing PreparedStatement objects */
	        	oDBUtility.releaseResources(pstmt1);
	            oDBUtility.releaseResources(rs, pstmt);
	        }
	        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );
		return isDeleted;
	}
	
	/* (non-Javadoc)
	 * @see in.com.rbc.quotation.admin.dao.DesignEngineerDao#exportDesignEngineerSearchResult(java.sql.Connection, in.com.rbc.quotation.admin.dto.DesignEngineerDto, java.lang.String)
	 */
	public ArrayList exportDesignEngineerSearchResult(Connection conn , DesignEngineerDto oDesignEngineerDto,String sWhereQuery, String sSortFilter, String sSortType)throws Exception{
		
		/*
    	 * Setting the Method Name as generic for logger Utility purpose . 
    	 */
    		String sMethodName ="exportDesignEngineerSearchResult"; 
    		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
    		
    		/* PreparedStatement object to handle database operations */
            PreparedStatement pstmt = null ;
            
            /* ResultSet object to store the rows retrieved from database */
            ResultSet rs = null ;      
            
            /* Object of DBUtility class to handle database operations */
            DBUtility oDBUtility = new DBUtility();
            
            ArrayList alSearchResultsList = new ArrayList(); // Stores the list of rows retrieved from the database
            
            String sSqlQueryString = null; // Stores the complete SQL query to be executed
            
            DesignEngineerDto oTempDesignEngineerDto=null ;// Create the Temporary Location Object for storing the table column values 
            try {

            	sSqlQueryString = FETCH_DESIGN_ENGINEER;
            	
            	if (sWhereQuery != null){
            		sSqlQueryString = sSqlQueryString + " "+sWhereQuery;
            	}
            	
            	if (sSortFilter != null && sSortFilter.trim().length() > 0){
            		sSqlQueryString = sSqlQueryString + " ORDER BY "+ sSortFilter;
            		if (sSortType != null && sSortType.trim().length() > 0)
            			sSqlQueryString = sSqlQueryString + " " +sSortType;
            	}
            	
            	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName+"..Search Design Engineer Query ",sSqlQueryString);
            	pstmt = conn.prepareCall(sSqlQueryString);
            	rs = pstmt.executeQuery();// Execute the Search Query to retriuve the Location Values .
            	
            	
         		if (rs.next()) {
                     do {
                    	 oTempDesignEngineerDto = new DesignEngineerDto();
                    	 oTempDesignEngineerDto.setDesignEngineerId(rs.getInt("QDE_ENGINEERID"));
                    	 oTempDesignEngineerDto.setDesignEngineerName(rs.getString("DESIGNENGINEER_NAME"));
                    	 oTempDesignEngineerDto.setDesignEngineerSSO(rs.getString("QDE_ENGINEERSSO"));
                    	 alSearchResultsList.add(oTempDesignEngineerDto);
                    } while (rs.next());
                 }
            }finally {
            	/* Releasing PreparedStatement objects */
                oDBUtility.releaseResources( rs,pstmt);
            }
		
		return alSearchResultsList;
	}
}
