/**
 * *****************************************************************************
 * Project Name: quotation
 * Document Name: CustomerAction.java
 * Package: in.com.rbc.quotation.admin.action
 * Desc: Action class which performs the necessary action depending on the request
 * *****************************************************************************
 * Author: 100005701 (Suresh)
 * Date: Sep 30, 2008
 * *****************************************************************************
 */
package in.com.rbc.quotation.admin.action;

import in.com.rbc.quotation.admin.dao.CustomerDao;
import in.com.rbc.quotation.admin.dto.CustomerDto;
import in.com.rbc.quotation.admin.form.CustomerForm;
import in.com.rbc.quotation.base.BaseAction;
import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.utility.CommonUtility;
import in.com.rbc.quotation.common.utility.DBUtility;
import in.com.rbc.quotation.common.utility.ExceptionUtility;
import in.com.rbc.quotation.common.utility.ListDBColumnUtil;
import in.com.rbc.quotation.common.utility.ListModel;
import in.com.rbc.quotation.common.utility.LoggerUtility;
import in.com.rbc.quotation.factory.DaoFactory;
import in.com.rbc.quotation.user.action.UserAction;
import in.com.rbc.quotation.user.dto.UserDto;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * <h3>Class Name:-CustomerAction</h3> 
 * <br> This class contains the all Business functionality  
 * <br>	-->viewcustomer , 
 * <br>	-->Add new Customer ,
 * <br>	-->Upate Customer , 
 * <br>	-->Delete Customer , 
 * <br>	-->export Customer into Excel , and 
 * <br>	-->lookup Customer Values .
 * <br>This Class is Extends from BaseAction Class .
 * @author 100005701 (Suresh Shanmugam)
 *
 */
public class CustomerAction extends BaseAction {
	
	/**
	 * <h3>Method Name:-viewcustomer</h3> 
	 * <br>Method is Displaying the Customer Search result, by default it'll dispaly the all Values from the table. 
	 * <br>By Calling the ListModel class the columns are sorting  
	 * @param  actionForm     ActionForm object to handle the form elements
	 * @param  request  HttpServletRequest object to handle request operations
	 * @param  response HttpServletResponse object to handle response operations
	 * @return returns    ActionForward depending on which user is redirected to next page
	 * @return to QUOTATION_FORWARD_VIEW page
	 * @throws Exception 
	 * 				if any error occurs while performing database operations, etc
	 * @return QUOTATION_FORWARD_FAILURE when any exception occured
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 21, 2008
	 */
	public ActionForward viewcustomer(ActionMapping actionMapping, ActionForm actionForm,
									  HttpServletRequest request , HttpServletResponse response)throws Exception {
		//Method name Set for log file usage ;
    	String sMethodname = "viewcustomer";
    	String sClassName = this.getClass().getName();
   	 	LoggerUtility.log("INFO",sClassName,sMethodname,"START");
		/*
         * Setting user's information in request, using the header 
         * information received through request
         */
        setRolesToRequest(request);
    	 // Get Form objects from the actionForm
    	 CustomerForm oCustomerForm =(CustomerForm)actionForm;
    	 if(oCustomerForm ==  null) {
    		 oCustomerForm = new CustomerForm();
    	 }
    	 Connection conn = null;  // Connection object to store the database connection
       	 DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
       	DaoFactory oDaoFactory =null;  /* Creating an instance of of DaoFactory  */
       	CustomerDao oCustomerDao =null;
       	CustomerDto oCustomerDto=null;
       	 HttpSession session= request.getSession();
  	     try {
  	    	saveToken(request);// To avoid entering the duplicate value into database. 
		 	/*
		 	 * Object of Hashtable to store different values 
		 	 */
       		 Hashtable htSearchResultsList = new Hashtable();
			   
			/*
             * Object of ListModel class to store the values necessary to display 
             * the results in attribute search results page with pagination
             */
		   		ListModel oListModel = new ListModel();
	   		/*
             * Setting values for ListModel that are necessary to retrieve rows from the database
             * and display them in search results page. It holds information like
             * --> number of rows to be displayed at once in the page
             * --> column on which rows have to be sorted
             * --> order in which rows have to be sorted
             * --> set of rows to be retrieved depending on the page number selected for viewing
             * Details of parameters:
             * 1. Request object
             * 2. Sort column index
             * 3. Sort order
             * 4. Search form
             * 5. Field that stores the invoke method name
             * 8. Invoke method name
             */
		   		oListModel.setParams(request, 
                       "1", 
                       ListModel.ASCENDING, 
                       "customerForm", 
                       "customerForm.invoke", 
                       "viewcustomer");
			
		   	/* Instantiating the DBUtility object */
       		oDBUtility = new DBUtility();
       		
            oDaoFactory = new DaoFactory();
            oCustomerDao = oDaoFactory.getCustomerDao();
            /* Retrieving the database connection using DBUtility.getDBConnection method */
	        conn = oDBUtility.getDBConnection();
	        oCustomerDto = new CustomerDto();
	            
	        LoggerUtility.log("INFO",sClassName,sMethodname,"Cusotmer Name:"+oCustomerForm.getCustomer());
    	            
            /* Copying the Form Object into DTO Object via Propertyutils*/
	        PropertyUtils.copyProperties(oCustomerDto , oCustomerForm);
    	            
            /* Retrieving the search results using CustomerDaoImpl method */
	        htSearchResultsList = oCustomerDao.getCustomer(conn,oCustomerDto,oListModel);

            if(htSearchResultsList != null ) {
	        	  oCustomerDto.setSearchResultsList((ArrayList)htSearchResultsList.get(QuotationConstants.QUOTATION_LIST_RESULTSLIST));
		          request.setAttribute(QuotationConstants.QUOTATION_CUSTDTO,oCustomerDto);                 
		          request.setAttribute(QuotationConstants.QUOTATION_LIST_RESULTSLIST,(ArrayList)htSearchResultsList.get(QuotationConstants.QUOTATION_LIST_RESULTSLIST));
		          request.setAttribute(QuotationConstants.QUOTATION_LIST_LISTMODELCLASS,htSearchResultsList.get(QuotationConstants.QUOTATION_LIST_LISTMODELCLASS));
		          request.setAttribute(QuotationConstants.QUOTATION_LIST_LISTSIZE,new Integer(((ArrayList)htSearchResultsList.get(QuotationConstants.QUOTATION_LIST_RESULTSLIST)).size()));
		          session.setAttribute(QuotationConstants.QUOTATION_SESSIONLIST,(ArrayList)htSearchResultsList.get(QuotationConstants.QUOTATION_LIST_RESULTSLIST));
		          session.setAttribute(QuotationConstants.RBC_QUOTATION_CUSTOMER_QUERY,(String)htSearchResultsList.get(QuotationConstants.RBC_QUOTATION_CUSTOMER_QUERY));
		          session.setAttribute(QuotationConstants.QUOTATION_SEARCH_SORTFIELD, ListDBColumnUtil.getCustomerSearchResultDBField(oListModel.getSortBy()));
				  session.setAttribute(QuotationConstants.QUOTATION_SEARCH_SORTORDER, oListModel.getSortOrderDesc());
				 
				  LoggerUtility.log("INFO",sClassName,sMethodname,"	RBC_QUOTATION_CUSTOMER_QUERY :="+(String) request.getSession().getAttribute(QuotationConstants.RBC_QUOTATION_CUSTOMER_QUERY));	
            }else {
            	request.setAttribute(QuotationConstants.QUOTATION_LIST_LISTSIZE, new Integer(0));
            	
            	// remove the values that are set in the session
				session.removeAttribute(QuotationConstants.RBC_QUOTATION_LOCATION_QUERY);
				session.removeAttribute(QuotationConstants.QUOTATION_SEARCH_SORTFIELD);
				session.removeAttribute(QuotationConstants.QUOTATION_SEARCH_SORTORDER);
            }
  	     }catch(Exception e) {
       		 /*
             * Logging any exception that raised during this activity in log files using Log4j
             */
        		LoggerUtility.log("DEBUG",sClassName,sMethodname, "viewCustomer Search Result."+ExceptionUtility.getStackTraceAsString(e));
            /* Setting error details to request to display the same in Exception page */
            	request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            	
            /* Forwarding request to Error page */
            	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
  	     }finally {
  	       	 /* Releasing/closing the connection object */
             	oDBUtility.releaseResources(conn);
  	     }
  	       LoggerUtility.log("INFO",sClassName,sMethodname,"END");
  	       return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_VIEW);
	}
	
	/**
	 * <h3>Method Name:-addNewCustomerpage</h3> 
	 * <br>Method Used for to open the add new Customer form Page
	 * <br>while opening this page its clear the all form values .
	 * @param  actionForm     ActionForm object to handle the form elements
	 * @param  request  HttpServletRequest object to handle request operations
	 * @param  response HttpServletResponse object to handle response operations
	 * @return returns    ActionForward depending on which user is redirected to next page
	 * @return to QUOTATION_FORWARD_ADDMASTERDATA page
	 * @throws Exception if any error occurs while performing database operations, etc
	 * @return QUOTATION_FORWARD_FAILURE when any exception occured
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 21, 2008
	 */
	public ActionForward addNewCustomerpage(ActionMapping actionMapping, ActionForm actionForm,
			  HttpServletRequest request , HttpServletResponse response)throws Exception {
		//Method name Set for log file usage ;
    	String sMethodname = "addNewCustomerpage";
    	LoggerUtility.log("INFO", this.getClass().getName(),sMethodname, "START");
		/*
	     * Setting user's information in request, using the header 
	     * information received through request
	     */
        	setRolesToRequest(request);
        
       // Get Form objects from the actionForm
    	 CustomerForm oCustomerForm =(CustomerForm)actionForm;
    	 if(oCustomerForm ==  null) {
    		 oCustomerForm = new CustomerForm();
    	 }
    	 
    	 Connection conn = null;  // Connection object to store the database connection
       	 DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
       	DaoFactory oDaoFactory =null;  //Creating an instance of of DaoFactory
       	CustomerDao oCustomerDao =null;
       	CustomerDto oCustomerDto =null;
       	 try {
	   		 	saveToken(request);// To avoid entering the duplicate value into database. 
		  	    /* Instantiating the DBUtility object */
		  	    oDBUtility = new DBUtility();
		  	    
		  	    oDaoFactory = new DaoFactory();
		  	    oCustomerDao = oDaoFactory.getCustomerDao();
		  	    /* Retrieving the database connection using DBUtility.getDBConnection method */
		  	    conn = oDBUtility.getDBConnection();
		  	    oCustomerDto = new CustomerDto();
		  	    oCustomerForm.setCustomerName(""); 
		  	    oCustomerForm.setCustomer("");
		    	/*
		    	* setLocationList to dispaly the drop down values in location field with appropriate value  
		    	*/
		  	    oCustomerForm.setLocationList(oCustomerDao.getLocationList(conn));
		  	    
		  	  oCustomerForm.setSalesengineerList(oCustomerDao.getSalesManagersList(conn));
       	 }catch(Exception e){
		    	/*
		    	 * Logging any exception that raised during this activity in log files using Log4j
		    	 */
		    	LoggerUtility.log("DEBUG", this.getClass().getName(), "add new customer page Result.", ExceptionUtility.getStackTraceAsString(e));
		    	/* Setting error details to request to display the same in Exception page */
		    	request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
		    	
		        /* Forwarding request to Error page */
		    	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
  	     }finally {
  	       	 /* Releasing/closing the connection object */
	         oDBUtility.releaseResources(conn);  	       		 
  	     }
  	    LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"END");
		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_ADDMASTERDATA);
	}
	
	/**
	 * <h3>Method Name:-editCustomerPage</h3> 
	 * <br>Method Used for to open the Update exists Customer form Page
	 * <br>while opening this page its load the all form values .
	 * @param  actionForm     ActionForm object to handle the form elements
	 * @param  request  HttpServletRequest object to handle request operations
	 * @param  response HttpServletResponse object to handle response operations
	 * @return returns    ActionForward depending on which user is redirected to next page
	 * @return to QUOTATION_FORWARD_ADDMASTERDATA page
	 * @throws Exception if any error occurs while performing database operations, etc
	 * @return QUOTATION_FORWARD_FAILURE when any exception occured
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 21, 2008
	 */
	public ActionForward editCustomerPage(ActionMapping actionMapping, ActionForm actionForm,
			  HttpServletRequest request , HttpServletResponse response) throws Exception {
		//Method name Set for log file usage ;
		String sMethodname = "editCustomerPage";
		LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"START");
	   /*
        * Setting user's information in request, using the header 
        * information received through request
        */
    	setRolesToRequest(request);
    	// Get Form objects from the actionForm
		CustomerForm oCustomerForm =(CustomerForm)actionForm;
    	if(oCustomerForm ==  null) {
    		oCustomerForm = new CustomerForm();
    	}
    	Connection conn = null;  // Connection object to store the database connection
    	DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
    	UserDto oUserDto = null; // Object of UserDto class to store the logged in user details
    	DaoFactory oDaoFactory =null; //Creating an instance of of DaoFactory
    	CustomerDao oCustomerDao =null;
    	CustomerDto oCustomerDto=null;
    	UserAction oUserAction =null;
    	
    	try {
	    	// To avoid entering the duplicate value into database. 
	    	saveToken(request);
	    	/* Instantiating the DBUtility object */
    		oDBUtility = new DBUtility();
    		
			oDaoFactory = new DaoFactory();
			oCustomerDao = oDaoFactory.getCustomerDao();
            conn = oDBUtility.getDBConnection();
            oCustomerDto = new CustomerDto();
            oDBUtility = new DBUtility();
            
            oUserAction = new UserAction();
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            if (oUserDto == null){
	      	   oUserDto = new UserDto();
	        }

            /* Copying the Form Object into DTO Object via Propertyutils*/
	        PropertyUtils.copyProperties( oCustomerDto,oCustomerForm);
	        oCustomerDto.setCustomerId(CommonUtility.getIntParameter(request,QuotationConstants.QUOTATION_CUSTOMERID));
	    	oCustomerDto = oCustomerDao.getCustomerInfo(conn,oCustomerDto);
	    	
	    	/*
	    	 * setLocationList to display the drop down values in location field with appropriate value  
	    	 * setOperation is used to set the operation values as Update for diff in JSP files to dispaly diff button and header label
	    	 */
	    	oCustomerDto.setLocationList(oCustomerDao.getLocationList(conn));
	    	oCustomerDto.setSalesengineerList(oCustomerDao.getSalesManagersList(conn));
	    	oCustomerDto.setOperation(QuotationConstants.QUOTATION_FORWARD_UPDATE);
	    	
	    	/* Copying the Form Object into DTO Object via Propertyutils*/
	        PropertyUtils.copyProperties( oCustomerForm ,oCustomerDto);
	        
	    } catch(Exception e) {
	    	 /*
	         * Logging any exception that raised during this activity in log files using Log4j
	         */
	    	LoggerUtility.log("DEBUG", this.getClass().getName(), "edit customer page Result.", ExceptionUtility.getStackTraceAsString(e));
	        /* Setting error details to request to display the same in Exception page */
	        request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
	        
	        /* Forwarding request to Error page */
	        return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
       	} finally {
  	       	 /* Releasing/closing the connection object */
	         oDBUtility.releaseResources(conn);  	      
       	}
        
    	LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"END");
        return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_ADDMASTERDATA);
	}
	
	/**
	 * <h3>Method Name:-updateCustomer</h3> 
	 * <br>Method Used for to open the Update exists Customer form Page
	 * @param  actionForm     ActionForm object to handle the form elements
	 * @param  request  HttpServletRequest object to handle request operations
	 * @param  response HttpServletResponse object to handle response operations
	 * @return returns    ActionForward depending on which user is redirected to next page
	 * @return to QUOTATION_FORWARD_SUCCESS page
	 * @throws Exception if any error occurs while performing database operations, etc
	 * @return QUOTATION_FORWARD_FAILURE when any exception occured
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 21, 2008
	 */
	public ActionForward updateCustomer(ActionMapping actionMapping, ActionForm actionForm,
			  HttpServletRequest request , HttpServletResponse response)throws Exception  {
		//Method name Set for log file usage ;
    	String sMethodname = "updateCustomer";
    	LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"START");
		/*
         * Setting user's information in request, using the header 
         * information received through request
         */
        setRolesToRequest(request);
        // Get Form objects from the actionForm
    	CustomerForm oCustomerForm =(CustomerForm)actionForm;
    	if(oCustomerForm ==  null) {
    		oCustomerForm = new CustomerForm();
    	}
		 Connection conn = null;  // Connection object to store the database connection
	   	 DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
	   	 UserDto oUserDto = null; // Object of UserDto class to store the logged in user details
	   	 boolean isSuccess = false ;
	   	 String operation = null;
	   	DaoFactory oDaoFactory  = null;
	   	CustomerDto oCustomerDto = null;
	    UserAction oUserAction = null;
	    CustomerDao oCustomerDao =null;
	    String sExistingCustomer=null;
	    int isExists=0;
  	       	 
  	     try {
				/* Instantiating the DBUtility object */
				oDBUtility = new DBUtility();
				/* Creating an instance of of DaoFactory  */
			 	oDaoFactory = new DaoFactory();
			 	oCustomerDao = oDaoFactory.getCustomerDao();
			    oCustomerDto = new CustomerDto();
			    
	         	if(isTokenValid(request)){// IF loop to check the if user clicked the referesh button or anyother things...
	         	    resetToken(request);// Reset the Token Request ...
		         	/* Retrieving the database connection using DBUtility.getDBConnection method */
		            conn = oDBUtility.getDBConnection();
		            /* Copying the Form Object into DTO Object via Propertyutils*/
		            PropertyUtils.copyProperties(oCustomerDto , oCustomerForm);
		            oUserAction = new UserAction();
		            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
		            if (oUserDto == null){
		          	   oUserDto = new UserDto();
		            }
		            operation = CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_OPERATION);
		            sExistingCustomer=CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_EXISCUST);
		            oDBUtility.setAutoCommitFalse(conn);// Set Auto commit functionality as False ...

	            	if(operation.equals(QuotationConstants.QUOTATION_FORWARD_UPDATE)){
		            	 oCustomerDto.setLastUpdatedById(oUserDto.getUserId());
		            	 if(oCustomerDto.getCustomer().equals(sExistingCustomer))
		            	 {
			            	 isSuccess = oCustomerDao.saveCustomerValue(conn,oCustomerDto);
			            	 if(isSuccess)
			            		 request.setAttribute(QuotationConstants.QUOTATION_ADMIN_SUCCESSMESSAGE, QuotationConstants.QUOTATION_CUSTOMER+" "+QuotationConstants.QUOTATION_REC_UPDATESUCC);

					   	      if(isSuccess) {
					   	    	  oDBUtility.commit(conn);
					   	    	  request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_URL, QuotationConstants.QUOTATION_VIEWCUST_URL); 
					   	      }else {
					   	    	  oDBUtility.rollback(conn);
					   	    	  return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
					   	      }

		            	 } 
		            	 else
		            	 {
		            		
				 	            PropertyUtils.copyProperties(oCustomerDto , oCustomerForm);
				 	            
						 	    isExists =  oCustomerDao.isCustomerAlreadyExists(conn,oCustomerDto);

				 	            if(isExists == QuotationConstants.QUOTATION_LITERAL_ONE) {
				 	            LoggerUtility.log("DEBUG", this.getClass().getName(), sMethodname,QuotationConstants.QUOTATION_CUSTOMER_ALREADY_EXISTS);
				 				request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_CUSTOMER_ALREADY_EXISTS);
				 	            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
				 	             }
				 	            else
				 	            {
				 	            	isSuccess = oCustomerDao.saveCustomerValue(conn,oCustomerDto);
					            	 if(isSuccess)
					            		 request.setAttribute(QuotationConstants.QUOTATION_ADMIN_SUCCESSMESSAGE, QuotationConstants.QUOTATION_CUSTOMER+" "+QuotationConstants.QUOTATION_REC_UPDATESUCC);

							   	      if(isSuccess) {
							   	    	  oDBUtility.commit(conn);
							   	    	  request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_URL, QuotationConstants.QUOTATION_VIEWCUST_URL); 
							   	      }else {
							   	    	  oDBUtility.rollback(conn);
							   	    	  return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
							   	      }

				 	            }

		            	 }
		             }else {
		            	
		 	            PropertyUtils.copyProperties(oCustomerDto , oCustomerForm);
			 	            
				 	    isExists =  oCustomerDao.isCustomerAlreadyExists(conn,oCustomerDto);

		 	            if(isExists == QuotationConstants.QUOTATION_LITERAL_ONE) {
		 	            LoggerUtility.log("DEBUG", this.getClass().getName(), sMethodname,QuotationConstants.QUOTATION_CUSTOMER_ALREADY_EXISTS);
		 				request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_CUSTOMER_ALREADY_EXISTS);
		 	            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
		 	             }
		 	            else if(isExists==QuotationConstants.QUOTATION_LITERAL_ZERO)
		 	             {
		            	 oCustomerDto.setCreatedById(oUserDto.getUserId());
		                 oCustomerDto.setLastUpdatedById(oUserDto.getUserId());
		                 isSuccess = oCustomerDao.createCustomer(conn,oCustomerDto);
		                 
		                 if(isSuccess)
		                	 request.setAttribute(QuotationConstants.QUOTATION_ADMIN_SUCCESSMESSAGE, "\""+oCustomerDto.getCustomerName()+"\" "+QuotationConstants.QUOTATION_REC_ADDSUCC);
		 	              }
				   	      if(isSuccess) {
				   	    	  oDBUtility.commit(conn);
				   	    	  request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_URL, QuotationConstants.QUOTATION_VIEWCUST_URL); 
				   	      }else {
				   	    	  oDBUtility.rollback(conn);
				   	    	  return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
				   	      }

		 	                }
	            	/*
		              * Depond upon the Success value , the page is re-directed with message 
		              */
					oDBUtility.setAutoCommitTrue(conn);
	         	}else {
	            	LoggerUtility.log("INFO", this.getClass().getName(), sMethodname,QuotationConstants.QUOTATION_MSG_TOKSM);
	 				request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_INVALIDTOKENMESSAGE);
	 	            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
	         	}
  	       	 }catch (SQLException se) {
  	       		
 				/*
 				 * if the SQL Exception is Unique Constrain error then customer won't be delete and it'll return the SQL Exception .
 				 */
  	       		 LoggerUtility.log("DEBUG", this.getClass().getName(), "Customer delete Result Error Code .", String.valueOf(se.getErrorCode()));
 				/*
 				 * if the Error is 00001 , then the forward to Error Failure page and dispaly the message as customer details already exists
 				 */
  	       		   
	 	            LoggerUtility.log("DEBUG", this.getClass().getName(), sMethodname,QuotationConstants.QUOTATION_EXCEP_IN_INSERT+se.getMessage());
	 				request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_EXCEP_IN_INSERT+se.getMessage());
	 	            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);

        	}catch(Exception e) {
  	       		 /*
   	             * Logging any exception that raised during this activity in log files using Log4j
   	             */
   	        	LoggerUtility.log("DEBUG", this.getClass().getName(), "update customer Result.", ExceptionUtility.getStackTraceAsString(e));
   	            /* Setting error details to request to display the same in Exception page */
   	            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
   	            
   	            /* Forwarding request to Error page */
   	            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
  	       	 }finally {
  	  	       	 /* Releasing/closing the connection object */
  		         oDBUtility.releaseResources(conn);  	      
  	       	 }
  	    LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"END");
		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_SUCCESS);
	}
	
	/**
	 * <h3>Method Name:-deleteCustomer</h3> 
	 * <br>Method Used for to Delete th exists Customer
	 * @param  actionForm     ActionForm object to handle the form elements
	 * @param  request  HttpServletRequest object to handle request operations
	 * @param  response HttpServletResponse object to handle response operations
	 * @return returns    ActionForward depending on which user is redirected to next page
	 * @return to QUOTATION_FORWARD_SUCCESS page
	 * @throws Exception if any error occurs while performing database operations, etc
	 * @return QUOTATION_FORWARD_FAILURE when any exception occured
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 21, 2008
	 */
	public ActionForward deleteCustomer(ActionMapping actionMapping, 
                                        ActionForm actionForm,
			                            HttpServletRequest request, 
                                        HttpServletResponse response) throws Exception {
        
		//Method name Set for log file usage ;
		String sMethodname = "deleteCustomer";
		LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"START");
		
        /*
         * Setting user's information in request, using the header 
         * information received through request
         */
    	setRolesToRequest(request);
    	
        // Get Form objects from the actionForm
    	CustomerForm oCustomerForm =(CustomerForm)actionForm;
    	if (oCustomerForm ==  null) {
    	    oCustomerForm = new CustomerForm();
        }
        
    	Connection conn = null;  // Connection object to store the database connection
	    DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
	    CustomerDto oCustomerDto = null;
	    DaoFactory oDaoFactory =null; // Creating an instance of of DaoFactory
	    CustomerDao oCustomerDao =null;
	    
        boolean isdeleted = false ;
        boolean isDeleteCustomer = false;
	   	
        try {
       		/* Instantiating the DBUtility object */
    		oDBUtility = new DBUtility();
            
    		
    		oDaoFactory = new DaoFactory();
            
    		oCustomerDao = oDaoFactory.getCustomerDao();
            
    		oCustomerDto = new CustomerDto();
            
    		if(isTokenValid(request)){// IF loop to check the if user clicked the refresh button or another things...
	         	resetToken(request);// Reset the Token Request ...
                
	    		/* Retrieving the database connection using DBUtility.getDBConnection method */
	    		conn = oDBUtility.getDBConnection();
                
	    		/* Copying the Form Object into DTO Object via Propertyutils*/
	    		PropertyUtils.copyProperties(oCustomerDto , oCustomerForm);
                
	    		isDeleteCustomer = oCustomerDao.canDeleteCustomer(conn, oCustomerDto);
                LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"Before entering the if loop canDeleteCustomer :="+isDeleteCustomer);
                if ( isDeleteCustomer ) {
                    oDBUtility.setAutoCommitFalse(conn);
                    isdeleted =    oCustomerDao.deleteCustomerValue(conn,oCustomerDto);
                    LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"isdeleted :="+isdeleted);
                    if(isdeleted) {
                        oDBUtility.commit(conn);
                        
                        LoggerUtility.log("INFOR", this.getClass().getName(), sMethodname, "Successfully" );
                        
                        request.setAttribute(QuotationConstants.QUOTATION_ADMIN_SUCCESSMESSAGE, "\""+oCustomerDto.getCustomer()+"\" "+QuotationConstants.QUOTATION_REC_DELSUCC);
                        request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_URL, QuotationConstants.QUOTATION_VIEWCUST_URL); 
                    } else {
                        oDBUtility.rollback(conn);
                    }
                    oDBUtility.setAutoCommitTrue(conn);
                } else {
                    LoggerUtility.log("INFOR", this.getClass().getName(), sMethodname, "Customer could not be deleted since there are open enquires for the customer." );
                    
                    request.setAttribute(QuotationConstants.QUOTATION_ADMIN_HEADERTEXT, QuotationConstants.QUOTATION_DELETED_FAILED);
                    request.setAttribute(QuotationConstants.QUOTATION_ADMIN_SUCCESSMESSAGE, 
                                         "\"" + oCustomerDto.getCustomer() + "\" " + QuotationConstants.QUOTATION_REC_DELFAIL);
                    request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_URL, QuotationConstants.QUOTATION_VIEWCUST_URL); 
                }
    		} else {
    			LoggerUtility.log("INFO", this.getClass().getName(),sMethodname, QuotationConstants.QUOTATION_MSG_TOKHAN );
 				request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_INVALIDTOKENMESSAGE);
 	            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
    		}
	   	 } catch (SQLException se) {
			/*
			 * if the SQL Exception is Unique Constrain error then customer won't be delete and it'll return the SQL Exception .
			 */
	   		 LoggerUtility.log("DEBUG", this.getClass().getName(), "Customer delete Result Error Code .", String.valueOf(se.getErrorCode()));
			/*
			 * if the Error is 2292 , then the forward to Success page and dispaly the message as customer couldn't be deleted
			 */
			if ( se.getErrorCode() == 2292 ) {
				request.setAttribute(QuotationConstants.QUOTATION_ADMIN_HEADERTEXT,"");
				request.setAttribute(QuotationConstants.QUOTATION_ADMIN_SUCCESSMESSAGE, "\""+oCustomerDto.getCustomer()+"\" "+QuotationConstants.QUOTATION_REC_DELFAIL);
                request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_URL, QuotationConstants.QUOTATION_VIEWCUST_URL); 
                return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_SUCCESS);
			}
       	} catch(Exception e) {
       		/*
             * Logging any exception that raised during this activity in log files using Log4j
             */
        	LoggerUtility.log("DEBUG", this.getClass().getName(), "Customer delete result ", ExceptionUtility.getStackTraceAsString(e));
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
       	} finally {
  	       	 /* Releasing/closing the connection object */
	         oDBUtility.releaseResources(conn);
       	}
       	LoggerUtility.log("INFO", this.getClass().getName(), sMethodname, "END");
       	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_SUCCESS);
	}
	
	/**
	 * 
	 * <h3>Class Name :- exportCustomerSearch </h3>
	 * <br> get the Values from arraylist Object and fetch into Excel Sheet .
	 * @param actionMapping ActionMapping object to redirect the user to next page depending on the activity
     * @param actionForm ActionForm object to handle the form elements
     * @param request HttpServletRequest object to handle request operations
     * @param response HttpServletResponse object to handle response operations
     * @return returns ActionForward depending on which user is redirected to next page
     * @throws Exception if any error occurs while performing database operations, etc
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 21, 2008
	 */
	public ActionForward exportCustomerSearch(ActionMapping actionMapping, ActionForm actionForm,
			  HttpServletRequest request , HttpServletResponse response)throws Exception {
		//Method name Set for log file usage ;
		String sMethodname = "exportCustomerSearch";
		LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"START");
		/*
         * Setting user's information in request, using the header 
         * information received through request
         */
    	setRolesToRequest(request);
    	// Get Form objects from the actionForm
		CustomerForm oCustomerForm =(CustomerForm)actionForm;
		if(oCustomerForm ==  null) {
			oCustomerForm = new CustomerForm();
		}
    	Connection conn = null;  // Connection object to store the database connection
	    DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
	    CustomerDto oCustomerDto = null;
	    String sOperation = null; 
	    String sSortFilter = null;
	    String sSortOrder = null;
	    String sWhereQuery = null;
	    DaoFactory oDaoFactory =null; //Creating an instance of of DaoFactory
	    CustomerDao oCustomerDao =null;
	    ArrayList alSearchResultsList =null; //Retrieving the search results using oHelpDto.getHelpList method
	    
	    try {
	    	/* Instantiating the DBUtility object */
 			oDBUtility = new DBUtility();
	 		
 			oDaoFactory = new DaoFactory();
 			oCustomerDao = oDaoFactory.getCustomerDao();
	      	/* Retrieving the database connection using DBUtility.getDBConnection method */
 			conn = oDBUtility.getDBConnection();
 			oCustomerDto = new CustomerDto();
 			sOperation = CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_FUNCTIONTYPES);
 			LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"Operation value"+sOperation);
 			
 			if (request.getSession().getAttribute(QuotationConstants.QUOTATION_SEARCH_SORTFIELD) != null)
 				sSortFilter = (String) request.getSession().getAttribute(QuotationConstants.QUOTATION_SEARCH_SORTFIELD);
 			
 			if (request.getSession().getAttribute(QuotationConstants.QUOTATION_SEARCH_SORTORDER) != null)
 				sSortOrder = (String) request.getSession().getAttribute(QuotationConstants.QUOTATION_SEARCH_SORTORDER);
 			
 			if(request.getSession().getAttribute(QuotationConstants.RBC_QUOTATION_CUSTOMER_QUERY)!= null)
 				sWhereQuery = (String) request.getSession().getAttribute(QuotationConstants.RBC_QUOTATION_CUSTOMER_QUERY);
 			/* Copying the Form Object into DTO Object via Propertyutils*/
 			PropertyUtils.copyProperties(oCustomerDto,oCustomerForm);       
	           
 			/* Retrieving the search results using oHelpDto.getHelpList method */
 			alSearchResultsList = oCustomerDao.exportCustomerSearchResult(conn, oCustomerDto,sWhereQuery, sSortFilter, sSortOrder);  
            request.setAttribute(QuotationConstants.QUOTATION_ADMIN_EXPORTLIST, alSearchResultsList); 
        }catch(Exception e) {
    	 	/*
             * Logging any exception that raised during this activity in log files using Log4j
             */
        	LoggerUtility.log("DEBUG", this.getClass().getName(), "customer Export result Search Result.", ExceptionUtility.getStackTraceAsString(e));
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
           
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
	     }finally {
	    	 /* Releasing/closing the connection object */
          	 oDBUtility.releaseResources(conn);  	  
	     }
	     LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"END");
	     if(sOperation.equalsIgnoreCase(QuotationConstants.QUOTATION_FORWARD_EXPORT))
	    	 return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_EXPORT);
	     else if(sOperation.equalsIgnoreCase(QuotationConstants.QUOTATION_FORWARD_PRINT))
	    	 return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_PRINT);
	     else 
	    	 return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
	}
	
	/**
	 * 
	 * <h3>Class Name :- exportCustomerSearch </h3>
	 * <br> get the Values from arraylist Object and fetch into Excel Sheet .
	 * @param actionMapping ActionMapping object to redirect the user to next page depending on the activity
     * @param actionForm ActionForm object to handle the form elements
     * @param request HttpServletRequest object to handle request operations
     * @param response HttpServletResponse object to handle response operations
     * @return returns ActionForward depending on which user is redirected to next page
     * @throws Exception if any error occurs while performing database operations, etc
	 * @author 900008798 (Abhilash Moola)
	 * Created on: Dec 9, 2020
	 */
	public ActionForward customerbulkUpload(ActionMapping actionMapping, ActionForm actionForm,
			  HttpServletRequest request , HttpServletResponse response)throws Exception {
		
		// Method name Set for log file usage ;
		String sMethodname = "updateCustomer";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodname, "START");
		/*
		 * Setting user's information in request, using the header information received
		 * through request
		 */
		setRolesToRequest(request);
		// Get Form objects from the actionForm
		CustomerForm oCustomerForm = (CustomerForm) actionForm;
		if (oCustomerForm == null) {
			oCustomerForm = new CustomerForm();
		}
		Connection conn = null; // Connection object to store the database connection
		DBUtility oDBUtility = null; // Object of DBUtility class to handle DB connection objects
		UserDto oUserDto = null; // Object of UserDto class to store the logged in user details
		CustomerDto oCustomerDto = null;
		try {
			saveToken(request);

			oDBUtility = new DBUtility();

			// oDaoFactory = new DaoFactory();
			// oCustomerDao = oDaoFactory.getCustomerDao();
			/* Retrieving the database connection using DBUtility.getDBConnection method */
			// conn = oDBUtility.getDBConnection();
			oCustomerDto = new CustomerDto();

		} catch (Exception e) {
			/*
			 * Logging any exception that raised during this activity in log files using
			 * Log4j
			 */
			LoggerUtility.log("DEBUG", this.getClass().getName(), "Customer delete result ",
					ExceptionUtility.getStackTraceAsString(e));

			/* Setting error details to request to display the same in Exception page */
			request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));

			/* Forwarding request to Error page */
			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
		} finally {
			/* Releasing/closing the connection object */
			oDBUtility.releaseResources(conn);
		}
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodname, "END");

		request.setAttribute(QuotationConstants.QUOTATION_ADMIN_SUCCESSMESSAGE,
				"Excel upload " + QuotationConstants.QUOTATION_REC_ADDSUCC);
		request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_URL,
				QuotationConstants.QUOTATION_VIEWCUST_URL);

		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_SUCCESS);
	}
}
