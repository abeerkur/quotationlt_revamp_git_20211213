package in.com.rbc.quotation.admin.form;

import in.com.rbc.quotation.common.utility.CommonUtility;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

public class CommercialEngineerForm extends ActionForm{

	/**
	 * @author 900010540 (Gangadhar)
	 *
	 */

	private String designEngineerName= null;
	private String designEngineerSSO=null;
	private int designEngineerId = 0;
	private String invoke = null;
	private String operation;
	
	private ArrayList searchResultsList = null ;

	/**
	 * @return Returns the designEngineerId.
	 */
	public int getDesignEngineerId() {
		return this.designEngineerId;
	}

	/**
	 * @param designEngineerId The designEngineerId to set.
	 */
	public void setDesignEngineerId(int designEngineerId) {
		this.designEngineerId = designEngineerId;
	}

	/**
	 * @return Returns the designEngineerName.
	 */
	public String getDesignEngineerName() {
		return CommonUtility.replaceNull(this.designEngineerName);
	}

	/**
	 * @param designEngineerName The designEngineerName to set.
	 */
	public void setDesignEngineerName(String designEngineerName) {
		this.designEngineerName = designEngineerName;
	}

	/**
	 * @return Returns the designEngineerSSO.
	 */
	public String getDesignEngineerSSO() {
		return CommonUtility.replaceNull(this.designEngineerSSO);
	}

	/**
	 * @param designEngineerSSO The designEngineerSSO to set.
	 */
	public void setDesignEngineerSSO(String designEngineerSSO) {
		this.designEngineerSSO = designEngineerSSO;
	}

	/**
	 * @return Returns the invoke.
	 */
	public String getInvoke() {
		return CommonUtility.replaceNull(this.invoke);
	}

	/**
	 * @param invoke The invoke to set.
	 */
	public void setInvoke(String invoke) {
		this.invoke = invoke;
	}

	/**
	 * @return Returns the operation.
	 */
	public String getOperation() {
		return CommonUtility.replaceNull(this.operation);
	}

	/**
	 * @param operation The operation to set.
	 */
	public void setOperation(String operation) {
		this.operation = operation;
	}

	/**
	 * @return Returns the searchResultsList.
	 */
	public ArrayList getSearchResultsList() {
		return this.searchResultsList;
	}

	/**
	 * @param searchResultsList The searchResultsList to set.
	 */
	public void setSearchResultsList(ArrayList searchResultsList) {
		this.searchResultsList = searchResultsList;
	}
	
}
