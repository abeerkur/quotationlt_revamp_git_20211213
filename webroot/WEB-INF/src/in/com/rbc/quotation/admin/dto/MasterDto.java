/**
 * *****************************************************************************
 * Project Name: quotation
 * Document Name: CustomerDto.java
 * Package: in.com.rbc.quotation.admin.dto
 * Desc: 
 * *****************************************************************************
 * Author: 100005701 (Suresh)
 * Date: Sep 30, 2008
 * *****************************************************************************
 */
package in.com.rbc.quotation.admin.dto;

import in.com.rbc.quotation.common.utility.CommonUtility;

import java.util.ArrayList;

/**
 * @author 100005701 (Suresh Shanmugam)
 *
 */
public class MasterDto {
	
	private String invoke ;
	private String masterId;
	private String master;
	private String addOrUpdate;
	private String dispatch;
	private String existingMaster;
	/**
	 * @return Returns the dispatch.
	 */
	public String getDispatch() {
		return CommonUtility.replaceNull(dispatch);
	}
	/**
	 * @param dispatch The dispatch to set.
	 */
	public void setDispatch(String dispatch) {
		this.dispatch = dispatch;
	}
	/**
	 * @return Returns the addOrUpdate.
	 */
	public String getAddOrUpdate() {
		return CommonUtility.replaceNull(addOrUpdate);
	}
	/**
	 * @param addOrUpdate The addOrUpdate to set.
	 */
	public void setAddOrUpdate(String addOrUpdate) {
		this.addOrUpdate = addOrUpdate;
	}
	/**
	 * @return Returns the invoke.
	 */
	public String getInvoke() {
		return invoke;
	}
	/**
	 * @param invoke The invoke to set.
	 */
	public void setInvoke(String invoke) {
		this.invoke = invoke;
	}
	/**
	 * @return Returns the master.
	 */
	public String getMaster() {
		return CommonUtility.replaceNull(master);
	}
	/**
	 * @param master The master to set.
	 */
	public void setMaster(String master) {
		this.master = master;
	}
	/**
	 * @return Returns the masterId.
	 */
	public String getMasterId() {
		return CommonUtility.replaceNull(masterId);
	}
	/**
	 * @param masterId The masterId to set.
	 */
	public void setMasterId(String masterId) {
		this.masterId = masterId;
	}
	public String getExistingMaster() {
		return CommonUtility.replaceNull(existingMaster);
	}
	public void setExistingMaster(String existingMaster) {
		this.existingMaster = existingMaster;
	}


	
	
	
}
