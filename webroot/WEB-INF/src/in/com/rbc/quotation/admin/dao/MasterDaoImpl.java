/**
 * *****************************************************************************
 * Project Name: quotation
 * Document Name: MasterDaoImpl.java
 * Package: in.com.rbc.quotation.admin.dao
 * Desc: 
 * *****************************************************************************
 * Author: 100002865 (Shanthi Chinta)
 * Date: Oct 1, 2008
 * *****************************************************************************
 */
package in.com.rbc.quotation.admin.dao;


import in.com.rbc.quotation.admin.dto.MasterDto;
import in.com.rbc.quotation.admin.dto.MastersDataDto;

import in.com.rbc.quotation.common.utility.DBUtility;
import in.com.rbc.quotation.common.utility.ListDBColumnUtil;
import in.com.rbc.quotation.common.utility.ListModel;
import in.com.rbc.quotation.common.utility.LoggerUtility;
import in.com.rbc.quotation.common.vo.KeyValueVo;
import in.com.rbc.quotation.user.dto.UserDto;
import in.com.rbc.quotation.common.utility.PropertyUtility;
import in.com.rbc.quotation.common.constants.QuotationConstants;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.StringTokenizer;


/**
 * @author 100002865 (Shanthi Chinta)
 *
 */
public class MasterDaoImpl implements MasterDao,AdminQueries {
	
	  public ArrayList getLocationList(Connection conn) throws Exception{  

	        
	        String sClassName = this.getClass().getName();
	        String sMethodName = "getLocationList";
	        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
	        
	        /* PreparedStatement object to handle database operations */
	        PreparedStatement pstmt = null ;
	        
	        /* ResultSet object to store the rows retrieved from database */
	        ResultSet rs = null ;
	        
	        /* Object of DBUtility class to handle database operations */
	        DBUtility oDBUtility = new DBUtility();
	        
	        /* ArrayList to store the list of request status */
	        ArrayList arlLocationList = null;
	        
	        /*  This Class is used to maintain Key Value Pair*/
	        KeyValueVo oKeyValueVo =null;
	        
	        try {
	            /*
	             * Querying the database to retrieve the lookup values, storing the
	             * details in KeyValueVo object, storing these object in an ArrayList 
	             * and returning the same
	             */
	            LoggerUtility.log("INFO", sClassName, sMethodName, "Query to retrieve Status list ::: " + AdminQueries.FETCH_LOCATION_LIST);
	            pstmt = conn.prepareStatement(AdminQueries.FETCH_LOCATION_LIST);
	            rs = pstmt.executeQuery();
	            if ( rs.next() ) {
	            	arlLocationList = new ArrayList();
	                do {
	                    oKeyValueVo = new KeyValueVo();
	                    oKeyValueVo.setKey(rs.getString("QLO_LOCATIONID"));
	                    oKeyValueVo.setValue(rs.getString("QLO_LOCATIONNAME"));
	                    arlLocationList.add(oKeyValueVo);
	                } while (rs.next());
	            }
	        } finally {
	            /* Releasing ResultSet & PreparedStatement objects */
	            oDBUtility.releaseResources(rs, pstmt);
	        }
	        
	        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
	        return arlLocationList;
	        
	  }
	 
	
	public ArrayList getCustomerList(Connection conn,String sLocation) throws Exception{ 

	
        String sClassName = this.getClass().getName();
        String sMethodName = "getCustomerList";
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        /* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        /* ArrayList to store the list of request status */
        ArrayList arlCustomerList = null;
        
        /*  This is Used to Store Key Value Pair*/
        KeyValueVo oKeyValueVo=null;
        
        try {
            /*
             * Querying the database to retrieve the lookup values, storing the
             * details in KeyValueVo object, storing these object in an ArrayList 
             * and returning the same
             */
            LoggerUtility.log("INFO", sClassName, sMethodName, "Query to retrieve Status list ::: " + AdminQueries.FETCH_CUSTOMER_LIST);
            pstmt = conn.prepareStatement(AdminQueries.FETCH_CUSTOMER_LIST);
            pstmt.setString(1,sLocation);
            rs = pstmt.executeQuery();
            if (rs.next() ) {
            	
            	arlCustomerList = new ArrayList();
                do {
                    oKeyValueVo = new KeyValueVo();
                    oKeyValueVo.setKey(rs.getString("QCU_CUSTOMERID"));
                    oKeyValueVo.setValue(rs.getString("QCU_NAME"));
                    arlCustomerList.add(oKeyValueVo);
                } while (rs.next());
            }
        } finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        return arlCustomerList;	   
 
		
	}
	
	public ArrayList getSalemangerList(Connection conn,String sRegion) throws Exception{

        
        String sClassName = this.getClass().getName();
        String sMethodName = "getSalemangerList";
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        /* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        /* ArrayList to store the list of request status */
        ArrayList arlSalemangerList = new ArrayList();
        /* KeyValueVo Stores Key Value Pairs*/
        KeyValueVo oKeyValueVo=null;
        
        try {
            /*
             * Querying the database to retrieve the lookup values, storing the
             * details in KeyValueVo object, storing these object in an ArrayList 
             * and returning the same
             */
            LoggerUtility.log("INFO", sClassName, sMethodName, "Query to retrieve Status list ::: " + AdminQueries.FETCH_SALESMANAGERNAME_LIST);
            pstmt = conn.prepareStatement(AdminQueries.FETCH_SALESMANAGERNAME_LIST);
            pstmt.setString(1,sRegion);
            
            rs = pstmt.executeQuery();
            if (rs.next() ) {
            	arlSalemangerList = new ArrayList();
                do {
                	oKeyValueVo= new KeyValueVo();
                    oKeyValueVo.setKey(rs.getString("SALESMGR_ID"));
                    oKeyValueVo.setValue(rs.getString("SALESMGR_NAME"));
                    arlSalemangerList.add(oKeyValueVo);
                } while (rs.next());
            }
        } finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        return arlSalemangerList;
		
	}
	
	 public ArrayList getRegionList(Connection conn) throws Exception{
		 

	        
	        String sClassName = this.getClass().getName();
	        String sMethodName = "getRegionList";
	        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
	        
	        /* PreparedStatement object to handle database operations */
	        PreparedStatement pstmt= null ;
	        
	        /* ResultSet object to store the rows retrieved from database */
	        ResultSet rs = null ;
	        
	        /* Object of DBUtility class to handle database operations */
	        DBUtility oDBUtility = new DBUtility();
	        
	        /* ArrayList to store the list of request status */
	        ArrayList arlRegionList = null;
	        /*KeyValueVo Stores Key Value Pairs*/
	        KeyValueVo oKeyValueVo=null;
	        
	        try {
	            /*
	             * Querying the database to retrieve the lookup values, storing the
	             * details in KeyValueVo object, storing these object in an ArrayList 
	             * and returning the same
	             */
	            LoggerUtility.log("INFO", sClassName, sMethodName, "Query to retrieve Status list ::: " + AdminQueries.FETCH_REGION_LIST);
	            pstmt = conn.prepareStatement(AdminQueries.FETCH_REGION_LIST);
	            rs = pstmt.executeQuery();
	            if ( rs.next() ) {
	            	arlRegionList = new ArrayList();
	                do {
	                    oKeyValueVo = new KeyValueVo();
	                    oKeyValueVo.setKey(rs.getString("QRE_REGIONID"));
	                    oKeyValueVo.setValue(rs.getString("QRE_REGIONNAME"));
	                    arlRegionList.add(oKeyValueVo);
	                } while (rs.next());
	            }
	        } finally {
	            /* Releasing ResultSet & PreparedStatement objects */
	            oDBUtility.releaseResources(rs, pstmt);
	        }
	        
	        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
	        return arlRegionList;	    
		 
	 }
    
    public ArrayList getStatusList(Connection conn) throws Exception {
        
        String sClassName = this.getClass().getName();
        String sMethodName = "getStatusList";
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        /* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        /* ArrayList to store the list of request status */
        ArrayList arlStatusList = null;
        /* KeyValueVo Stores Key Value Pair*/
        KeyValueVo oKeyValueVo=null;
        
        try {
            /*
             * Querying the database to retrieve the lookup values, storing the
             * details in KeyValueVo object, storing these object in an ArrayList 
             * and returning the same
             */
            LoggerUtility.log("INFO", sClassName, sMethodName, "Query to retrieve Status list ::: " + AdminQueries.FETCH_STATUSLIST);
            pstmt = conn.prepareStatement(AdminQueries.FETCH_STATUSLIST);
            rs = pstmt.executeQuery();
            if ( rs.next() ) {
                arlStatusList = new ArrayList();
                do {
                    oKeyValueVo = new KeyValueVo();
                    oKeyValueVo.setKey(rs.getString("QST_STATUSID"));
                    oKeyValueVo.setValue(rs.getString("QST_STATUSDESC"));
                    arlStatusList.add(oKeyValueVo);
                } while (rs.next());
            }
        } finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        return arlStatusList;
    }
    
    public ArrayList getMastersList(Connection conn) throws Exception {
        
        String sClassName = this.getClass().getName();
        String sMethodName = "getMastersList";
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        /* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs= null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        /* ArrayList to store the list of request status */
        ArrayList arlOptions = null;
        
        /*KeyValueVo Stores Key Value Pair List*/
        KeyValueVo oKeyValueVo=null;
        
        try {
            /*
             * Querying the database to retrieve the lookup values, storing the
             * details in KeyValueVo object, storing these object in an ArrayList 
             * and returning the same
             */
            LoggerUtility.log("INFO", sClassName, sMethodName, "Query to retrieve Masters list ::: " + AdminQueries.FETCH_MASTERSLIST);
            pstmt = conn.prepareStatement(AdminQueries.FETCH_MASTERSLIST);
            rs = pstmt.executeQuery();
            if ( rs.next() ) {
                arlOptions = new ArrayList();
                do {
                    oKeyValueVo = new KeyValueVo();
                    oKeyValueVo.setKey(rs.getString("QEM_ENGMID"));
                    oKeyValueVo.setValue(rs.getString("QEM_DESC"));
                    arlOptions.add(oKeyValueVo);
                } while (rs.next());
            }
        } finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        return arlOptions;
    }
    
    public ArrayList getOptionsList(Connection conn, 
                                     int masterTypeId, 
                                     boolean canIncludeInActive) throws Exception {
        
        String sClassName = this.getClass().getName();
        String sMethodName = "getOptionsList";
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        /* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        /* ArrayList to store the list of request status */
        ArrayList arlOptions = null;
        String sSqlQuery = null;
        /*KeyValueVo Stores Key Value Pair*/
        KeyValueVo oKeyValueVo=null;
        
        try {
            /*
             * Querying the database to retrieve the lookup values, storing the
             * details in KeyValueVo object, storing these object in an ArrayList 
             * and returning the same
             */
            
            if (canIncludeInActive) {
                sSqlQuery = AdminQueries.FETCH_OPTIONSLIST_ALL;
            } else {
                sSqlQuery = AdminQueries.FETCH_OPTIONSLIST_ONLYACTIVE;
            }
            LoggerUtility.log("INFO", sClassName, sMethodName, "Query to retrieve lookup values ::: " + sSqlQuery);
            LoggerUtility.log("INFO", sClassName, sMethodName, "Master Type Id = " + masterTypeId);
            
            pstmt = conn.prepareStatement(sSqlQuery);
            pstmt.setInt(1, masterTypeId);
            rs= pstmt.executeQuery();
            if ( rs.next() ) {
                arlOptions = new ArrayList();
                do {
                    oKeyValueVo = new KeyValueVo();
                    oKeyValueVo.setKey(rs.getString("QED_ENGDID"));
                    oKeyValueVo.setValue(rs.getString("QED_DESC"));
                    oKeyValueVo.setValue2(rs.getString("QED_ISACTIVE"));
                    arlOptions.add(oKeyValueVo);
                } while (rs.next());
            }
        } finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        return arlOptions;
    }

	public Hashtable getMasterListResults(Connection conn, MasterDto masterDto, ListModel oListModel) throws Exception {
	    String sClassName = this.getClass().getName();
        String sMethodName = "getStatusList";
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
    	/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
      
        int iCursorPosition = 0; // Stores the cursor position from where the records have to be retrieved
        // from database; this is dependent on the page number selected for viewing
        /*
         * Hashtable that stores the following values, and that is used to display results in search page
         * --> ArrayList containing the rows retrieved from database
         * --> ListModel object that hold the values used to retrieve values and display the same
         * --> String array containing the list of column names to be displayed as column headings
         */
        Hashtable htSearchMasterResults = null;
        KeyValueVo oKeyValueVo = null;
        String sWhereString = null; // Stores the Where clause string
        String sSqlQueryString = null; // Stores the complete SQL query to be executed
        StringBuffer sbCountofRecords=null;
        ArrayList alSearchMasterResults = null;
        try 
        {
        	
        	sWhereString = buildWhereClause(masterDto);
        	alSearchMasterResults = new ArrayList() ;
            pstmt = conn.prepareStatement(FETCH_MASTERLIST);
            /*
             * This bolck of code will construct and execute the query, which will bring the no of total records
             */
            sbCountofRecords = new StringBuffer();
            sbCountofRecords.append("SELECT count(*) FROM ");
            sbCountofRecords.append("("+FETCH_MASTERLIST+" "+sWhereString+") q");
            
            pstmt = conn.prepareStatement(sbCountofRecords.toString());
            rs = pstmt.executeQuery();
                
            if (rs.next()) {
                oListModel.setTotalRecordCount(rs.getInt(1));
            } else {
                oListModel.setTotalRecordCount(0);
            }
            oDBUtility.releaseResources(rs, pstmt);
            /*
             * Proceeding on if the count of records statisfying the filter criteria
             * is greater than zero
             */      
            htSearchMasterResults = new Hashtable();
            if (oListModel.getTotalRecordCount() > 0) 
            {
              
                /* Throwing an exception, if current page could not be retrieved */
                    if (oListModel.getCurrentPageInt()<0)
                        throw new Exception("Current Page is negative in search method...");
                    
                    /* 
                     * Retrieving the cursor position based on the page number selected by the user
                     * to retrieve list of values to be displayed for the request page
                     * If no page is selected, cursor position to display the first page is retrieved
                     */
                     iCursorPosition = (oListModel.getCurrentPageInt()-1) * oListModel.getRecordsPerPageInt();
                   
                     /*
                      * Proceeding only if the cursor position is valid
                      * i.e., the cursor position is less than the total record count
                      */
                     if(iCursorPosition < oListModel.getTotalRecordCount()) 
                     { 
                    	 /* Placing the cursor in appropriate position in the ResultSet */
                    	     sSqlQueryString = new StringBuffer("SELECT * FROM (SELECT Q.*, rownum AS rnum FROM ( ")
                                                             .append(FETCH_MASTERLIST)
                                                             .append(sWhereString)
                                                             .append(" ORDER BY ")
                                                             .append(ListDBColumnUtil.getSearchMasterDBField(oListModel.getSortBy()))
                                                             .append(" " + oListModel.getSortOrderDesc())
                                                             .append(" ) Q WHERE rownum <= (")
                                                             .append(iCursorPosition + " + " + oListModel.getRecordsPerPage() + ")) ")
                                                             .append(" WHERE rnum > " + iCursorPosition + "")
                                                             .toString();
                          LoggerUtility.log("INFO", this.getClass().getName(), "getMasterResults", "Master Query --> " + sSqlQueryString); 
                       
                          /*
                           * Querying the database to retrieve the list of records, which match
                           * the filter criteria specified, and storing them in an ArrayList
                           */      
                          pstmt = conn.prepareStatement(sSqlQueryString, ResultSet.TYPE_SCROLL_INSENSITIVE,
                                                                       ResultSet.CONCUR_READ_ONLY);
                          rs = pstmt.executeQuery();               
                          
                          /* Retrieving values from the ResultSet and storing them in an ArrayList */
                          
                          if (rs.next()) 
                          {
                              do {
                            	  	oKeyValueVo = new KeyValueVo();
                            	  	oKeyValueVo.setKey(rs.getString("QEM_ENGMID"));
                            	  	
                            	  	oKeyValueVo.setValue(rs.getString("QEM_DESC"));
                              	 	
                              	
                            	  	alSearchMasterResults.add(oKeyValueVo);
                             } while (rs.next());
                          }
                         
                          
                     }
            }      /* Putting the search results arraylist and ListModel object in the Hashtable */
            htSearchMasterResults.put("resultsList", alSearchMasterResults);
            htSearchMasterResults.put("ListModel", oListModel);  
            htSearchMasterResults.put("whereQuery",FETCH_MASTERLIST +sWhereString );
                        
                   
                }
                finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }       
       
    
        return htSearchMasterResults;
	}

	private String buildWhereClause(MasterDto masterDto) {
   
	String sWhereString =" ";
		
		if(masterDto.getMaster()!=null && !masterDto.getMaster().trim().equals("") )
		{
			sWhereString = " WHERE UPPER(a.QEM_DESC) like '%"+masterDto.getMaster().toUpperCase()+"%'";
		}
		
		if(masterDto.getMasterId()!=null && !masterDto.getMasterId().trim().equals("") )
		{
			sWhereString = " WHERE a.QEM_ENGMID = "+masterDto.getMasterId()+"";
		}
		
		
		return sWhereString;
	}

	public MasterDto getMasterDetails(Connection conn, MasterDto masterDto) throws Exception {
		/* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
    	/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
      
   
        MasterDto oMasterDto = null;
        String sWhereString = null; // Stores the Where clause string
        String sSqlQueryString = null; // Stores the complete SQL query to be executed
        try {
        		sWhereString = buildWhereClause(masterDto);
   
            	pstmt = conn.prepareStatement(FETCH_MASTERLIST);
                          	
   
                sSqlQueryString = new StringBuffer("")
                                                             .append(FETCH_MASTERLIST)
                                                             .append(""+sWhereString)
                                                             .toString();
                
              
                if(!sWhereString.trim().equalsIgnoreCase(""))
                {
                	pstmt = conn.prepareStatement(sSqlQueryString, ResultSet.TYPE_SCROLL_INSENSITIVE,
                                                                   ResultSet.CONCUR_READ_ONLY);
                    rs = pstmt.executeQuery();               
                    /* Retrieving values from the ResultSet and storing them in an SoftwareVo object */
                          
                    if (rs.next()) 
                    {
                    	oMasterDto = new MasterDto();
                    	oMasterDto.setMasterId(rs.getString("QEM_ENGMID"));
                    	oMasterDto.setMaster(rs.getString("QEM_DESC"));
                	}
                   
                 }
                 else
                 {
                	 oMasterDto= new MasterDto();
                 }
            }
        	finally {
        			/* Releasing ResultSet & PreparedStatement objects */
        			oDBUtility.releaseResources(rs, pstmt);
            }       
            return oMasterDto;
	}

	public String deleteMaster(Connection conn, MasterDto masterDto) throws Exception {

		String sMsg="";
		
		/* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
    	/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        try 
        {
         				pstmt = conn.prepareStatement(DELETE_MASTER);
        				pstmt.setString(1,masterDto.getMasterId());
        				if(pstmt.executeUpdate()>0)
        				{
        					sMsg = ""+masterDto.getMaster()+QuotationConstants.QUOTATION_REC_DELSUCC;
        				}
        				else
        					sMsg=QuotationConstants.QUOTATION_REC_DELFAIL;	
        	
        }
        finally
        {
        	oDBUtility.releaseResources(pstmt);        	
        }
		return sMsg;
	}

	public String addOrUpdateMaster(Connection conn, MasterDto oMasterDto,UserDto oUserDto) throws Exception {
		
		String sMsg="";
		String sSoftwareGroupID="";
		/* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
    	/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        int iMaxMasterId=0;
        String sExistingMaster=null;
        try {
        	/*
        	 * This IF statement is used to check the operation type is add software or not
        	 */
        	if(oMasterDto.getAddOrUpdate()!=null && oMasterDto.getAddOrUpdate().trim().equalsIgnoreCase("add"))
        	{	
        		/*
        		 * This block of statements is used to check the perticuler Software is already exsists or not
        		 */
        		pstmt = conn.prepareStatement(FETCH_MASTER);
        		
        		pstmt.setString(1,oMasterDto.getMaster().trim());
        		rs = pstmt.executeQuery();
        		if(rs.next()) {
        			sMsg = QuotationConstants.QUOTATION_MASTER_EXISTS;
        			oDBUtility.releaseResources(rs,pstmt);
        		} else {
        			
        			
        			
        			pstmt = conn.prepareStatement(FETCH_MAX_MASTERID);
        			rs=pstmt.executeQuery();
        			while(rs.next())
        			{
        				iMaxMasterId=rs.getInt("MAXENGID");
        			}
        			
        				pstmt = conn.prepareStatement(INSERT_MASTER);
	        			pstmt.setInt(1,iMaxMasterId);
	        			pstmt.setString(2,oMasterDto.getMaster());
	        			pstmt.setString(3,oUserDto.getUserId());
	        			pstmt.setString(4,oUserDto.getUserId());
	        			pstmt.setString(5,oMasterDto.getMaster());
	        			
	            		if(pstmt.executeUpdate()>0) {
	            			sMsg = ""+oMasterDto.getMaster()+QuotationConstants.QUOTATION_REC_ADDSUCC;
	            		}
        		}
        	} else {
        		sExistingMaster=oMasterDto.getExistingMaster();
        		if(sExistingMaster.equalsIgnoreCase(oMasterDto.getMaster()))
        		{
            		pstmt = conn.prepareStatement(UPDATE_MASTER);
        			
        			pstmt.setString(1,oMasterDto.getMaster());
        			pstmt.setString(2,oUserDto.getUserId());        			
        			pstmt.setString(3,oMasterDto.getMaster());
        			pstmt.setString(4,oMasterDto.getMasterId());
        			if(pstmt.executeUpdate()>0) {
        				sMsg = ""+oMasterDto.getMaster()+QuotationConstants.QUOTATION_REC_UPDATESUCC;
            		}

        		}
        		else
        		{
            		pstmt = conn.prepareStatement(FETCH_MASTER);

            		pstmt.setString(1,oMasterDto.getMaster().trim());
            		rs = pstmt.executeQuery();
            		if(rs.next()) {
            			sMsg = QuotationConstants.QUOTATION_MASTER_EXISTS;
            			oDBUtility.releaseResources(rs,pstmt);
            		}
            		else
            		{
                		pstmt = conn.prepareStatement(UPDATE_MASTER);
            			
            			pstmt.setString(1,oMasterDto.getMaster());
            			pstmt.setString(2,oUserDto.getUserId());
            			pstmt.setString(3,oMasterDto.getMaster());
            			pstmt.setString(4,oMasterDto.getMasterId());
            			if(pstmt.executeUpdate()>0) {
            				sMsg = ""+oMasterDto.getMaster()+QuotationConstants.QUOTATION_REC_UPDATESUCC;
                		}

            		}
        		}
        		
        	}
        } finally {
        	oDBUtility.releaseResources(rs,pstmt);        	
        }
        
		return sMsg;
	}
	public ArrayList getMasterList(Connection conn, MasterDto masterDto,String sQuery) throws Exception {
	   
		String sClassName = this.getClass().getName();
        String sMethodName = "getStatusList";
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
    	/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
      
        ArrayList alSearchMasterResults =null;
        KeyValueVo oKeyValueVo = null;
        try 
        {
        	alSearchMasterResults = new ArrayList() ;
        	
            pstmt = conn.prepareStatement(sQuery, ResultSet.TYPE_SCROLL_INSENSITIVE,
                                                                       ResultSet.CONCUR_READ_ONLY);
            rs = pstmt.executeQuery();               
            /* Retrieving values from the ResultSet and storing them in an ArrayList */
            if (rs.next()) 
            {
              do {
            	  	oKeyValueVo = new KeyValueVo();
            	  	oKeyValueVo.setKey(rs.getString("QEM_ENGMID"));
            	  	oKeyValueVo.setValue(rs.getString("QEM_DESC"));
            	  	alSearchMasterResults.add(oKeyValueVo);
              	 } while (rs.next());
            }
        }
        finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }       
        return alSearchMasterResults;
	}
	/**
     * This method is used to get  QEM Attribute from ENG MASTER by masterId to add options     * 
     * @param masterName   masterName is String value we will get masterId From Jsp
     *            			depending on the masterId Will Return QEM Attribute
     * @return returns sQemAttribute it is String value  of QEM Attribute
     * @author 900010540 (Gangadhara Ra0) 
     
     * created  on 10/01/2019           
      */

	public static String getQemAttributeValue(Connection conn, String masterName) throws Exception
	{
		String sQemAttribute="";
      
     
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null ;
		/* ResultSet object to store the rows retrieved from database */
		ResultSet rs = null ;
		
		try {
	      	/*
	      	 * This IF statement is used to check the operation type is add software or not
	      	 */
	      	if(masterName!=null && masterName!="" )
	      	{	
	      		/*
	      		 * This block of statements is used to check the perticuler Software is already exsists or not
	      		 */
	      		pstmt = conn.prepareStatement(FETCH_MASTER_ATTRIBUTE);
	      		
	      		pstmt.setInt(1,Integer.parseInt(masterName));
	      		rs = pstmt.executeQuery();
	      		if(rs.next()) {
	      			sQemAttribute = rs.getString("QEM_ATTRIBUTE");
	      			
	      		} 
	      	}
	      
  	
		}
		finally{
			oDBUtility.releaseResources(rs,pstmt);
		}
      
      
      return sQemAttribute;
		
	}
	
	public Hashtable getMastersDataList(Connection conn, MastersDataDto mastersDataDto, ListModel oListModel) throws Exception {
		   String sClassName = this.getClass().getName();
	        String sMethodName = "getMastersDataList";
	        String sQemAttribute=null; //Stores EngineeringMasterName

	        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
	        
	        /* Object of DBUtility class to handle database operations */
	        DBUtility oDBUtility = new DBUtility();
	    	/* PreparedStatement object to handle database operations */
	        PreparedStatement pstmt = null ;
	        /* ResultSet object to store the rows retrieved from database */
	        ResultSet rs = null ;
	        
	        
	    	

	    	
	      
	        int iCursorPosition = 0; // Stores the cursor position from where the records have to be retrieved
	        // from database; this is dependent on the page number selected for viewing
	        /*
	         * Hashtable that stores the following values, and that is used to display results in search page
	         * --> ArrayList containing the rows retrieved from database
	         * --> ListModel object that hold the values used to retrieve values and display the same
	         * --> String array containing the list of column names to be displayed as column headings
	         */
	        Hashtable htSearchMasterResults = null;
	        MastersDataDto oMastersDataDto = null;
	        String sWhereString = null; // Stores the Where clause string
	        String sSqlQueryString = null; // Stores the complete SQL query to be executed
	        ArrayList alSearchMasterResults =null;
	        StringBuffer sbCountofRecords = null;
	        

	        try 
	        {
	        	sQemAttribute=MasterDaoImpl.getQemAttributeValue(conn, mastersDataDto.getMasterId());
	        	
	        	sWhereString = buildWhereClause(mastersDataDto);
	        	alSearchMasterResults = new ArrayList() ;
	            pstmt = conn.prepareStatement(FETCH_MASTERSDATALIST);
	            /*
	             * This bolck of code will construct and execute the query, which will bring the no of total records
	             */
	            sbCountofRecords = new StringBuffer();
	            sbCountofRecords.append("SELECT count(*) FROM ");
	            sbCountofRecords.append("("+FETCH_MASTERSDATALIST+" "+sWhereString+") q");
	            
	            pstmt = conn.prepareStatement(sbCountofRecords.toString());
	          
	            pstmt.setString(1,sQemAttribute);
	            rs = pstmt.executeQuery();
	                
	            if (rs.next()) {
	                oListModel.setTotalRecordCount(rs.getInt(1));
	            } else {
	                oListModel.setTotalRecordCount(0);
	            }
	            oDBUtility.releaseResources(rs, pstmt);
	            /*
	             * Proceeding on if the count of records statisfying the filter criteria
	             * is greater than zero
	             */      
	            htSearchMasterResults = new Hashtable();
	            if (oListModel.getTotalRecordCount() > 0) 
	            {
	              
	                /* Throwing an exception, if current page could not be retrieved */
	                    if (oListModel.getCurrentPageInt()<0)
	                        throw new Exception("Current Page is negative in search method...");
	                    
	                    /* 
	                     * Retrieving the cursor position based on the page number selected by the user
	                     * to retrieve list of values to be displayed for the request page
	                     * If no page is selected, cursor position to display the first page is retrieved
	                     */
	                     iCursorPosition = (oListModel.getCurrentPageInt()-1) * oListModel.getRecordsPerPageInt();
	                   
	                     /*
	                      * Proceeding only if the cursor position is valid
	                      * i.e., the cursor position is less than the total record count
	                      */
	                     if(iCursorPosition < oListModel.getTotalRecordCount()) 
	                     { 
	                    	 /* Placing the cursor in appropriate position in the ResultSet */
	                    	     sSqlQueryString = new StringBuffer("SELECT * FROM (SELECT Q.*, rownum AS rnum FROM ( ")
	                                                             .append(FETCH_MASTERSDATALIST)
	                                                             .append(sWhereString)
	                                                             .append(" ORDER BY ")
	                                                             .append(ListDBColumnUtil.getMasterDataDBField(oListModel.getSortBy()))
	                                                             .append(" " + oListModel.getSortOrderDesc())
	                                                             .append(" ) Q WHERE rownum <= (")
	                                                             .append(iCursorPosition + " + " + oListModel.getRecordsPerPage() + ")) ")
	                                                             .append(" WHERE rnum > " + iCursorPosition + "")
	                                                             .toString();
	                          LoggerUtility.log("INFO", this.getClass().getName(), "FETCH_MASTERSDATALIST", "Master Query --> " + sSqlQueryString); 
	                       
	                          /*
	                           * Querying the database to retrieve the list of records, which match
	                           * the filter criteria specified, and storing them in an ArrayList
	                           */      
	                         
	                          pstmt = conn.prepareStatement(sSqlQueryString, ResultSet.TYPE_SCROLL_INSENSITIVE,
	                                                                       ResultSet.CONCUR_READ_ONLY);
	                          pstmt.setString(1,sQemAttribute);
	                          
	                          rs = pstmt.executeQuery();               
	                          
	                          /* Retrieving values from the ResultSet and storing them in an ArrayList */
	                          
	                          if (rs.next()) 
	                          {
	                              do {
	                            	  
	                            	  oMastersDataDto = new MastersDataDto();
	                            	  oMastersDataDto.setEngineeringId(rs.getString("QEM_SORTORDER"));
	                            	  oMastersDataDto.setEngineeringName(rs.getString("QEM_VALUE"));
	                            	  oMastersDataDto.setStatus(rs.getString("QEM_ISACTIVE")==null?"":rs.getString("QEM_ISACTIVE"));
	                              	 	
	                              	
	                            	  	alSearchMasterResults.add(oMastersDataDto);
	                             } while (rs.next());
	                          }
	                         
	                          
	                     }
	            }      /* Putting the search results arraylist and ListModel object in the Hashtable */
	            htSearchMasterResults.put("resultsList", alSearchMasterResults);
	            htSearchMasterResults.put("ListModel", oListModel);  
	            htSearchMasterResults.put("whereQuery",FETCH_MASTERSDATALIST +sWhereString );
	                        
	                   
	                }
	                finally {
	            /* Releasing ResultSet & PreparedStatement objects */
	            oDBUtility.releaseResources(rs, pstmt);
	        }       
	       
	    
	        return htSearchMasterResults;
	}

	private String buildWhereClause(MastersDataDto mastersDataDto) 
	{
		
			String sWhereString =" ";
			
		
			if(mastersDataDto.getEngineeringId()!=null && !mastersDataDto.getEngineeringId().trim().equals("") )
			{
				sWhereString = " AND QEM_SORTORDER = "+mastersDataDto.getEngineeringId()+"";
			}
			if(mastersDataDto.getEngineeringName()!=null && !mastersDataDto.getEngineeringName().trim().equals("") )
			{
				sWhereString = " AND UPPER(QEM_VALUE) like '%"+mastersDataDto.getEngineeringName().toUpperCase()+"%'";
			}
					
			
		return sWhereString;
	}

	public MastersDataDto getMastersDataDetails(Connection conn, MastersDataDto mastersDataDto) throws Exception {
		/* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
    	/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        String sQemAttribute=null; //Stores EngineeringMasterName

        
   
        MastersDataDto oMastersDataDto = null;
        String sWhereString = null; // Stores the Where clause string
        String sSqlQueryString = null; // Stores the complete SQL query to be executed
        try {
        	
        	
        	sQemAttribute=MasterDaoImpl.getQemAttributeValue(conn, mastersDataDto.getMasterId());
        		sWhereString = buildWhereClause(mastersDataDto);
   
            	pstmt = conn.prepareStatement(FETCH_MASTERSDATALIST);
                          	
   
                sSqlQueryString = new StringBuffer("")
                                                             .append(FETCH_MASTERSDATALIST)
                                                             .append(""+sWhereString)
                                                             .toString();
                
              
                if(!sWhereString.trim().equalsIgnoreCase(""))
                {
                	pstmt = conn.prepareStatement(sSqlQueryString, ResultSet.TYPE_SCROLL_INSENSITIVE,
                                                                   ResultSet.CONCUR_READ_ONLY);

                	pstmt.setString(1,sQemAttribute);
                    rs = pstmt.executeQuery();               
                    /* Retrieving values from the ResultSet and storing them in an SoftwareVo object */
                          
                    if (rs.next()) 
                    {
                    	oMastersDataDto = new MastersDataDto();
                    	oMastersDataDto.setEngineeringId(rs.getString("QEM_SORTORDER"));
                    	oMastersDataDto.setEngineeringName(rs.getString("QEM_VALUE"));
                	}
                   
                 }
                 else
                 {
                	 oMastersDataDto= new MastersDataDto();
                 }
            }
        	finally {
        			/* Releasing ResultSet & PreparedStatement objects */
        			oDBUtility.releaseResources(rs, pstmt);
            }       
            return oMastersDataDto;
	}

	public String addOrUpdateMasterData(Connection conn, MastersDataDto oMastersDataDto, UserDto userDto) throws Exception {
		String sMsg="";
		String sSoftwareGroupID="";
		String sMaxKeyValue="";
		String sQemKey="";
		String sQemAttribute=null; //Stores EngineeringMasterName 
		String sEditMasterValue="";
		/* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
    	/* PreparedStatement object to handle database operations */
        
        PreparedStatement pstmt = null ;
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
 
        
        try 
        {
        	
        	/*
        	 * This IF statement is used to check the operation type is add software or not
        	 */
        	
        	
        	sQemAttribute=MasterDaoImpl.getQemAttributeValue(conn, oMastersDataDto.getMasterId());
        	if(oMastersDataDto.getAddOrUpdate()!=null && oMastersDataDto.getAddOrUpdate().trim().equalsIgnoreCase("add"))
        	{	
        		/*
        		 * This block of statements is used to check the particular Software is already exists or not
        		 */
        		
        		
        		
        		pstmt = conn.prepareStatement(FETCH_MASTERDATA);
        		pstmt.setString(1,oMastersDataDto.getEngineeringName());

        		pstmt.setString(2, sQemAttribute);
    			
        		rs = pstmt.executeQuery();
        		if(rs.next())
        		{
        			sMsg = QuotationConstants.QUOTATION_MASTER_EXISTS;        			
        			
        		}
        		else
        		{
        			oDBUtility.releaseResources(rs,pstmt);
        			
        			//Assign Value Of QEM_Value
        			
        			//Get Maximum QEM_KEY if it is Following Types
            		if(sQemAttribute.equalsIgnoreCase(QuotationConstants.QUOTATION_QEM_ENCLOSURE) || 
            				sQemAttribute.equalsIgnoreCase(QuotationConstants.QUOTATION_QEM_MOUNTING) || 
            				sQemAttribute.equalsIgnoreCase(QuotationConstants.QUOTATION_QEM_DEGPRO) ||
            				sQemAttribute.equalsIgnoreCase(QuotationConstants.QUOTATION_QEM_TYPE)	|| 
            				sQemAttribute.equalsIgnoreCase(QuotationConstants.QUOTATION_QEM_INSULATIONCLASS) ||
            				sQemAttribute.equalsIgnoreCase(QuotationConstants.QUOTATION_QEM_ROTDIRECT) || 
            				sQemAttribute.equalsIgnoreCase(QuotationConstants.QUOTATION_QEM_APPL) || 
            				sQemAttribute.equalsIgnoreCase(QuotationConstants.QUOTATION_QEM_DUTY) || 
            				sQemAttribute.equalsIgnoreCase(QuotationConstants.QUOTATION_QEM_TEMPRISE) ||
            				sQemAttribute.equalsIgnoreCase(QuotationConstants.QUOTATION_QEM_TERMBOX) ||
            				sQemAttribute.equalsIgnoreCase(QuotationConstants.QUOTATION_QEM_HTLT) ||
            				sQemAttribute.equalsIgnoreCase(QuotationConstants.QUOTATION_QEM_SHAFTEXT) )
                	{
        	

            		pstmt = conn.prepareStatement(FETCH_MAXQEM_KEY);
            		pstmt.setString(1,sQemAttribute);
            		rs=pstmt.executeQuery();
            		while(rs.next())
            		{
            			sMaxKeyValue=rs.getString("maxkey")==null?"0":rs.getString("maxkey");
            		}
            		oDBUtility.releaseResources(rs,pstmt);
                	}
            		else
            		{
            			sMaxKeyValue=oMastersDataDto.getEngineeringName();
            		}
        			
        			
        				pstmt = conn.prepareStatement(INSERT_MASTERDATA);
	        			
	        			pstmt.setString(1,sQemAttribute);
	        			pstmt.setString(2,sMaxKeyValue);
	        			pstmt.setString(3,oMastersDataDto.getEngineeringName());
	        			pstmt.setString(4,userDto.getUserId());
	        			pstmt.setString(5,userDto.getUserId());
	        			
	        			
	            		if(pstmt.executeUpdate()>0)
	            		{
	            			sMsg = ""+oMastersDataDto.getEngineeringName()+QuotationConstants.QUOTATION_REC_ADDSUCC;
	            		
	            		}
	            		
	            		oDBUtility.releaseResources(rs,pstmt);
        		}
        	}
        	else
        	{
        		
        		sQemKey="0";
        		
        		if(sQemAttribute.equalsIgnoreCase(QuotationConstants.QUOTATION_QEM_ENCLOSURE) || 
        				sQemAttribute.equalsIgnoreCase(QuotationConstants.QUOTATION_QEM_MOUNTING) || 
        				sQemAttribute.equalsIgnoreCase(QuotationConstants.QUOTATION_QEM_DEGPRO) ||
        				sQemAttribute.equalsIgnoreCase(QuotationConstants.QUOTATION_QEM_TYPE)	|| 
        				sQemAttribute.equalsIgnoreCase(QuotationConstants.QUOTATION_QEM_INSULATIONCLASS) ||
        				sQemAttribute.equalsIgnoreCase(QuotationConstants.QUOTATION_QEM_ROTDIRECT) || 
        				sQemAttribute.equalsIgnoreCase(QuotationConstants.QUOTATION_QEM_APPL) || 
        				sQemAttribute.equalsIgnoreCase(QuotationConstants.QUOTATION_QEM_DUTY) || 
        				sQemAttribute.equalsIgnoreCase(QuotationConstants.QUOTATION_QEM_TEMPRISE) ||
        				sQemAttribute.equalsIgnoreCase(QuotationConstants.QUOTATION_QEM_TERMBOX) ||
        				sQemAttribute.equalsIgnoreCase(QuotationConstants.QUOTATION_QEM_HTLT) ||
        				sQemAttribute.equalsIgnoreCase(QuotationConstants.QUOTATION_QEM_SHAFTEXT) )
            	{
        			
        			sQemKey="1";
            	}
        		
        		
        		 sEditMasterValue=oMastersDataDto.getEditmastervalue();
        		if(sEditMasterValue!=null && sEditMasterValue!="" )
        		{
        			if(!sEditMasterValue.equalsIgnoreCase(oMastersDataDto.getEngineeringName()))
        			{
                		pstmt = conn.prepareStatement(FETCH_MASTERDATA);
                		pstmt.setString(1,oMastersDataDto.getEngineeringName());
                		
                		pstmt.setString(2, sQemAttribute);
            			
                		rs = pstmt.executeQuery();
                		if(rs.next())
                		{
                			sMsg = QuotationConstants.QUOTATION_MASTER_EXISTS;               			
                			
                		}
                		else
                		{
                			oDBUtility.releaseResources(rs,pstmt); 
                			if(sQemKey.equalsIgnoreCase("1"))
                			{
                				
                    		pstmt = conn.prepareStatement(UPDATE_MASTERDATA);
                			pstmt.setString(1,oMastersDataDto.getEngineeringName());
                			pstmt.setString(2,userDto.getUserId());
                			pstmt.setString(3,sQemAttribute);
                			pstmt.setString(4,oMastersDataDto.getEngineeringId());

                			}
                			else
                			{
                				
                    		pstmt = conn.prepareStatement(UPDATE_MASTERDATA_TWO);
                    		pstmt.setString(1,oMastersDataDto.getEngineeringName());
                			pstmt.setString(2,oMastersDataDto.getEngineeringName());
                			pstmt.setString(3,userDto.getUserId());
                			pstmt.setString(4,sQemAttribute);
                			pstmt.setString(5,oMastersDataDto.getEngineeringId());

                			}

                    		
                			
                			if(pstmt.executeUpdate()>0)
                    		{
                				sMsg = ""+oMastersDataDto.getEngineeringName()+QuotationConstants.QUOTATION_REC_UPDATESUCC;
                    		}
                			

                		}
                		
	
        			}
        			else
        			{
        				
            			
            			if(sQemKey.equalsIgnoreCase("1"))
            			{
            				
                		pstmt = conn.prepareStatement(UPDATE_MASTERDATA);
            			pstmt.setString(1,oMastersDataDto.getEngineeringName());
            			pstmt.setString(2,userDto.getUserId());
            			pstmt.setString(3,sQemAttribute);
            			pstmt.setString(4,oMastersDataDto.getEngineeringId());

            			}
            			else
            			{
            				
                		pstmt = conn.prepareStatement(UPDATE_MASTERDATA_TWO);
                		pstmt.setString(1,oMastersDataDto.getEngineeringName());
            			pstmt.setString(2,oMastersDataDto.getEngineeringName());
            			pstmt.setString(3,userDto.getUserId());
            			pstmt.setString(4,sQemAttribute);
            			pstmt.setString(5,oMastersDataDto.getEngineeringId());

            			}

            			
            			if(pstmt.executeUpdate()>0)
                		{
            				sMsg = ""+oMastersDataDto.getEngineeringName()+QuotationConstants.QUOTATION_REC_UPDATESUCC;
                		}

        			}
        		}
        		
        	}
        	
        }
        finally
        {
        	oDBUtility.releaseResources(rs,pstmt);        	
        }
        
		return sMsg;
	}

	public String deleteMasterData(Connection conn, MastersDataDto mastersDataDto) throws Exception {
		String sMsg="";
		String sQemAttribute=null; //Stores EngineeringMasterName

		

		
		/* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
    	/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        try 
        {
        				sQemAttribute=MasterDaoImpl.getQemAttributeValue(conn,mastersDataDto.getMasterId());
         				pstmt = conn.prepareStatement(DELETE_MASTERDATA);

         				pstmt.setString(1,mastersDataDto.getEngineeringName());
         				pstmt.setString(2,sQemAttribute);
        				pstmt.setString(3,mastersDataDto.getEngineeringId());
        				
        				
        				if(pstmt.executeUpdate()>0)
        				{
        					sMsg = ""+mastersDataDto.getEngineeringName()+QuotationConstants.QUOTATION_REC_DELSUCC;
        				}
        				else
        					sMsg=QuotationConstants.QUOTATION_REC_DELFAIL;	
        	
        }
        finally
        {
        	oDBUtility.releaseResources(pstmt);        	
        }
		return sMsg;
	}

	public ArrayList getMasterDataList(Connection conn, MastersDataDto mastersDataDto, String sSqlQueryString) throws Exception {
		  String sClassName = this.getClass().getName();
	        String sMethodName = "getMastersDataList";
	        String sQemAttribute=null; //Stores EngineeringMasterName

	        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
	        
	        /* Object of DBUtility class to handle database operations */
	        DBUtility oDBUtility = new DBUtility();
	    	/* PreparedStatement object to handle database operations */
	        PreparedStatement pstmt = null ;
	        /* ResultSet object to store the rows retrieved from database */
	        ResultSet rs = null ;
	        
	        MastersDataDto oMastersDataDto = null;
	    	ArrayList alSearchMasterResults = new ArrayList() ;
	      
	        try 
	        {
	             /*
               * Querying the database to retrieve the list of records, which match
               * the filter criteria specified, and storing them in an ArrayList
               */    
	         sQemAttribute=MasterDaoImpl.getQemAttributeValue(conn,mastersDataDto.getMasterId());
	        	
              pstmt = conn.prepareStatement(sSqlQueryString, ResultSet.TYPE_SCROLL_INSENSITIVE,
                                                           ResultSet.CONCUR_READ_ONLY);
              pstmt.setString(1,sQemAttribute);
              
              rs = pstmt.executeQuery();               
              
              /* Retrieving values from the ResultSet and storing them in an ArrayList */
              
              if (rs.next()) 
              {
                  do {
                	  
                	  oMastersDataDto = new MastersDataDto();
                	  oMastersDataDto.setEngineeringId(rs.getString("QEM_SORTORDER"));
                
                	  oMastersDataDto.setEngineeringName(rs.getString("QEM_VALUE"));
                	  oMastersDataDto.setStatus(rs.getString("QEM_ISACTIVE")==null?"":rs.getString("QEM_ISACTIVE").equalsIgnoreCase("A")?"Active":"Inactive");
                  	 	
                  	
                	  	alSearchMasterResults.add(oMastersDataDto);
                 } while (rs.next());
              }
	        }
	                finally {
	            /* Releasing ResultSet & PreparedStatement objects */
	            oDBUtility.releaseResources(rs, pstmt);
	        }       
	       
	    
	        return alSearchMasterResults;
	}

	public boolean updateMasterDataStatusValue(Connection conn, MastersDataDto mastersDataDto, UserDto userDto) throws Exception {
		/*
    	 * Setting the Method Name as generic for logger Utility purpose . 
    	 */
		String sMethodName = "updateMasterDataStatusValue" ;
		String sQemAttribute=null; //Stores EngineeringMasterName

		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
    	/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        

        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
		boolean isUpdated = false ;
		
        try {
        	
        	sQemAttribute=MasterDaoImpl.getQemAttributeValue(conn, mastersDataDto.getMasterId());
        	
        	pstmt = conn.prepareStatement(UPDATE_MASTERSDATA_STATUS);
        	pstmt.setString(1, mastersDataDto.getStatus());
        	pstmt.setString(2, userDto.getUserId());
        	pstmt.setString(3, sQemAttribute);
        	pstmt.setString(4, mastersDataDto.getEngineeringId());
            
            
            
        	
            int isUdate = pstmt.executeUpdate();
        	if(isUdate > 0) {
        		isUpdated = true ;
        	}
        }finally {
        	/* Releasing PreparedStatement objects */
            oDBUtility.releaseResources( pstmt);
        }
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );
		return isUpdated;
	}	
	
	public ArrayList getYears(Connection conn) throws Exception{
		
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        /* ArrayList to store the list of request status */
        ArrayList arlOptions = null;   
        KeyValueVo oKeyValueVo = null;
        
        try {
            /*
             * Querying the database to retrieve the Status description, storing the
             * details in KeyValueVo object, storing these object in an ArrayList 
             * and returning the same
             */
        	arlOptions = new ArrayList();
        	pstmt = conn.prepareStatement(FECTH_YEARS);        
        	rs = pstmt.executeQuery();
            
            if (rs.next()) {            	
                do {
                	oKeyValueVo = new KeyValueVo();
                    oKeyValueVo.setKey(rs.getString(1));
                    oKeyValueVo.setValue(rs.getString(2));                
                    arlOptions.add(oKeyValueVo);
                } while (rs.next());
            
            }
        	
        } finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }    
       
        return arlOptions;		
		
		
	}

	public Hashtable getMasterListResults(Connection conn, MasterDto masterDto, ListModel oListModel, String sQuery) throws Exception{
		 String sClassName = this.getClass().getName();
	        String sMethodName = "getStatusList";
	        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
	        
	        /* Object of DBUtility class to handle database operations */
	        DBUtility oDBUtility = new DBUtility();
	    	/* PreparedStatement object to handle database operations */
	        PreparedStatement pstmt = null ;
	        /* ResultSet object to store the rows retrieved from database */
	        ResultSet rs = null ;
	      
	        int iCursorPosition = 0; // Stores the cursor position from where the records have to be retrieved
	        // from database; this is dependent on the page number selected for viewing
	        /*
	         * Hashtable that stores the following values, and that is used to display results in search page
	         * --> ArrayList containing the rows retrieved from database
	         * --> ListModel object that hold the values used to retrieve values and display the same
	         * --> String array containing the list of column names to be displayed as column headings
	         */
	        Hashtable htSearchMasterResults = null;
	        KeyValueVo oKeyValueVo = null;
	        String sWhereString = null; // Stores the Where clause string
	        String sSqlQueryString = null; // Stores the complete SQL query to be executed
	        ArrayList alSearchMasterResults = null;
	        try 
	        {
	        	
	        	sWhereString = buildWhereClause(masterDto);
	        	alSearchMasterResults = new ArrayList() ;
	       
	            
	            pstmt = conn.prepareStatement(sQuery);
	            rs = pstmt.executeQuery();
	                
	            if (rs.next()) {
	                oListModel.setTotalRecordCount(rs.getInt(1));
	            } else {
	                oListModel.setTotalRecordCount(0);
	            }
	            oDBUtility.releaseResources(rs, pstmt);
	            /*
	             * Proceeding on if the count of records statisfying the filter criteria
	             * is greater than zero
	             */      
	            htSearchMasterResults = new Hashtable();
	            if (oListModel.getTotalRecordCount() > 0) 
	            {
	              
	                /* Throwing an exception, if current page could not be retrieved */
	                    if (oListModel.getCurrentPageInt()<0)
	                        throw new Exception("Current Page is negative in search method...");
	                    
	                    /* 
	                     * Retrieving the cursor position based on the page number selected by the user
	                     * to retrieve list of values to be displayed for the request page
	                     * If no page is selected, cursor position to display the first page is retrieved
	                     */
	                     iCursorPosition = (oListModel.getCurrentPageInt()-1) * oListModel.getRecordsPerPageInt();
	                   
	                     /*
	                      * Proceeding only if the cursor position is valid
	                      * i.e., the cursor position is less than the total record count
	                      */
	                     if(iCursorPosition < oListModel.getTotalRecordCount()) 
	                     { 
	                    	 /* Placing the cursor in appropriate position in the ResultSet */
	                    	     sSqlQueryString = new StringBuffer("SELECT * FROM (SELECT Q.*, rownum AS rnum FROM ( ")
	                                                             .append(sQuery)
	                                                             .append(" ORDER BY ")
	                                                             .append(ListDBColumnUtil.getSearchMasterDBField(oListModel.getSortBy()))
	                                                             .append(" " + oListModel.getSortOrderDesc())
	                                                             .append(" ) Q WHERE rownum <= (")
	                                                             .append(iCursorPosition + " + " + oListModel.getRecordsPerPage() + ")) ")
	                                                             .append(" WHERE rnum > " + iCursorPosition + "")
	                                                             .toString();
	                          LoggerUtility.log("INFO", this.getClass().getName(), "getMasterResults", "Master Query --> " + sSqlQueryString); 
	                       
	                          /*
	                           * Querying the database to retrieve the list of records, which match
	                           * the filter criteria specified, and storing them in an ArrayList
	                           */      
	                          pstmt = conn.prepareStatement(sSqlQueryString, ResultSet.TYPE_SCROLL_INSENSITIVE,
	                                                                       ResultSet.CONCUR_READ_ONLY);
	                          rs = pstmt.executeQuery();               
	                          
	                          /* Retrieving values from the ResultSet and storing them in an ArrayList */
	                          
	                          if (rs.next()) 
	                          {
	                              do {
	                            	  	oKeyValueVo = new KeyValueVo();
	                            	  	oKeyValueVo.setKey(rs.getString("QEM_ENGMID"));
	                            	  	
	                            	  	oKeyValueVo.setValue(rs.getString("QEM_DESC"));
	                              	 	
	                              	
	                            	  	alSearchMasterResults.add(oKeyValueVo);
	                             } while (rs.next());
	                          }
	                         
	                          
	                     }
	            }      /* Putting the search results arraylist and ListModel object in the Hashtable */
	            htSearchMasterResults.put("resultsList", alSearchMasterResults);
	            htSearchMasterResults.put("ListModel", oListModel);  
	            htSearchMasterResults.put("whereQuery",FETCH_MASTERLIST +sWhereString );
	                        
	                   
	                }
	                finally {
	            /* Releasing ResultSet & PreparedStatement objects */
	            oDBUtility.releaseResources(rs, pstmt);
	        }       
	       
	    
	        return htSearchMasterResults;
	}
	
	
	public int getRegionIDBySalesManager(Connection conn,String sSalesManager)throws Exception{
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        /* ArrayList to store the list of request status */
        int  iRegionId = 0;      
        
        try {
            /*
             * Querying the database to retrieve the Status description, storing the
             * details in KeyValueVo object, storing these object in an ArrayList 
             * and returning the same
             */
        	
        	pstmt = conn.prepareStatement(FETCH_REGION_BYSALESMANAGER);
        	pstmt.setString(1,sSalesManager);
        	rs = pstmt.executeQuery();
            
            if (rs.next()) {            	
                
            	iRegionId=rs.getInt("REGIONID");
            }
        	
        } finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }    
        
        return iRegionId;		
	}
	
	public int getRegionIDBySalesManagerId(Connection conn,String sSalesManager)throws Exception{
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        /* ArrayList to store the list of request status */
        int  iRegionId = 0;      
        
        try {
            /*
             * Querying the database to retrieve the Status description, storing the
             * details in KeyValueVo object, storing these object in an ArrayList 
             * and returning the same
             */
        	
        	pstmt = conn.prepareStatement(FETCH_REGION_BYSALESMANAGERID);
        	pstmt.setString(1,sSalesManager);
        	rs = pstmt.executeQuery();
            
            if (rs.next()) {            	
                
            	iRegionId=rs.getInt("QSM_REGIONID");
            }
        	
        } finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }    
        
        return iRegionId;		
	}
	
public int getLocationID(Connection conn,int iCustomerID)throws Exception{
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs= null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        /* ArrayList to store the list of request status */
        int  iRegionId = 0;      
        
        try {
            /*
             * Querying the database to retrieve the Status description, storing the
             * details in KeyValueVo object, storing these object in an ArrayList 
             * and returning the same
             */
        	
        	pstmt = conn.prepareStatement(FETCH_LOCATION_ID);
        	pstmt.setInt(1,iCustomerID);
        	rs = pstmt.executeQuery();
            
            if (rs.next()) {            	
                
            	iRegionId=rs.getInt("QCU_LOCATIONID");
            }
        	
        } finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }    
        
        return iRegionId;		
	}
}
