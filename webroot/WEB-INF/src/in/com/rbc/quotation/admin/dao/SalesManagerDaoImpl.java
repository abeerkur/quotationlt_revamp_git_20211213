/**
 * *****************************************************************************
 * Project Name: quotation
 * Document Name: SalesManagerDaoImpl.java
 * Package: in.com.rbc.quotation.admin.dao
 * Desc: 
 * *****************************************************************************
 * Author: 100005701 (Suresh)
 * Date: Oct 6, 2008
 * *****************************************************************************
 */
package in.com.rbc.quotation.admin.dao;

import in.com.rbc.quotation.admin.dto.CustomerDto;
import in.com.rbc.quotation.admin.dto.SalesManagerDto;

import in.com.rbc.quotation.common.utility.DBUtility;
import in.com.rbc.quotation.common.utility.ListDBColumnUtil;
import in.com.rbc.quotation.common.utility.ListModel;
import in.com.rbc.quotation.common.utility.LoggerUtility;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * @author 100005701 (Suresh Shanmugam)
 *
 */
public class SalesManagerDaoImpl implements SalesManagerDao, AdminQueries {
	
	/* (non-Javadoc)
	 * @see in.com.rbc.quotation.admin.dao.SalesManagerDao#getSalesManagerSearchResult(java.sql.Connection, in.com.rbc.quotation.admin.dto.SalesManagerDto, in.com.rbc.quotation.common.utility.ListModel)
	 */
	public Hashtable getSalesManagerSearchResult(Connection conn, SalesManagerDto oSalesManagerDto, ListModel oListModel) throws Exception {
		/*
    	* Setting the Method Name as generic for logger Utility purpose . 
    	*/
		String sMethodName = "getSalesManagerSearchResult Mehotd" ;
		String sClassName = this.getClass().getName();
		LoggerUtility.log("INFO", sClassName, sMethodName,"START" );
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;      
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
       
        /*
         * Hashtable that stores the following values, and that is used to display results in search page
         * --> ArrayList containing the rows retrieved from database
         * --> ListModel object that hold the values used to retrieve values and display the same
         * --> String array containing the list of column names to be displayed as column headings
         */
        Hashtable htSearchResults = null;
        
        ArrayList alSearchResultsList = new ArrayList(); // Stores the list of rows retrieved from the database
        
        
        int iCursorPosition = 0; // Stores the cursor position from where the records have to be retrieved // from database; this is dependent on the page number selected for viewing
        
        String sWhereString = null; // Stores the Where clause condition
        String sSqlQueryString = null; // Stores the complete SQL query to be executed
        
        try {
        	
        	sWhereString = buildWhereClause(oSalesManagerDto);
        	sSqlQueryString = COUNT_SALESMANAGER_SEARCHRESULT + sWhereString;
        	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName+"..Search Location Count Query",sSqlQueryString);
        	pstmt = conn.prepareCall(sSqlQueryString);
        	
        	/* Execute the Query for counting the number records*/
        	rs=pstmt.executeQuery();
        	
        	if(rs.next()) {
        		oListModel.setTotalRecordCount(rs.getInt(1));//set total number of records available in table depond upon the where conditions .
        	}else {
        		oListModel.setTotalRecordCount(0);// if the Query return no records , it's set the Total recordcount  "0" as default
        	}
        	oDBUtility.releaseResources(rs, pstmt);
        	SalesManagerDto oTempSalesManagerDto ;// Create the Temporary Location Object for storing the table column values 
        	 
        	 if(oListModel.getTotalRecordCount()>0) {
        		 htSearchResults = new Hashtable();
        		 
        		 if(oListModel.getCurrentPageInt()<0)
        			 throw new Exception("Current Page is negative in search method...");
        		 
        		 iCursorPosition = (oListModel.getCurrentPageInt()-1) * oListModel.getRecordsPerPageInt();
        		 
        		 if(iCursorPosition < oListModel.getTotalRecordCount()) {
             		
			 /*
			  * Select the values from the table Using FETCH_SEARCHLocation_RESULTS Query depond upon the 
			  * sWhereString condition and return into the sSqlQueryString as StringBuffer 
			  * and the data's are ordered by Location DBFiled as by default
			  * 
			  */
             		sSqlQueryString = new StringBuffer("SELECT * FROM (SELECT Q.*, rownum AS rnum FROM ( ")
 						                    .append(FETCH_SALESMANAGER_SEARCHRESULT)
 						                    .append(sWhereString)
 						                    .append(" ORDER BY ")
 						                    .append(ListDBColumnUtil.getSalesManagerSearchResultDBField(oListModel.getSortBy()))
 						                    .append(" " + oListModel.getSortOrderDesc())
 						                    .append(" ) Q WHERE rownum <= (")
 						                    .append(iCursorPosition + " + " + oListModel.getRecordsPerPage() + ")) ")
 						                    .append(" WHERE rnum > " + iCursorPosition + "")
 						                    .toString();
             		
             		
             		LoggerUtility.log("INFO", sClassName, sMethodName+"..SearchLocation Retrive Query",sSqlQueryString );
             		
             		
             		pstmt = conn.prepareStatement(sSqlQueryString, ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
             		
             		rs = pstmt.executeQuery();// Execute the Search Query to retriuve the Location Values .
             		if (rs.next()) {
                         do {
                        	 oTempSalesManagerDto = new SalesManagerDto();
                        	 oTempSalesManagerDto.setSalesMgrId(rs.getInt("QSM_SALESMGRID"));
                        	 oTempSalesManagerDto.setSalesManager(rs.getString("SALESMANAGER_NAME"));
                        	 oTempSalesManagerDto.setSalesManagerSSO(rs.getString("QSM_SALESMGR"));
                        	 oTempSalesManagerDto.setRegionName(rs.getString("QRE_REGIONNAME"));
                        	 oTempSalesManagerDto.setPrimaryRSM(rs.getString("PRIMARYRSM_NAME"));
                        	 oTempSalesManagerDto.setPrimaryRSMSSO(rs.getString("QSM_PRIMARYRSM"));
                        	 oTempSalesManagerDto.setAdditionalRSM(rs.getString("ADDITIONALRSM_NAME"));
                        	 oTempSalesManagerDto.setAdditionalRSMSSO(rs.getString("QSM_ADDITIONALRSM"));
                        	 oTempSalesManagerDto.setSmContactNo(rs.getString("QSM_PHONENO"));
                        	 alSearchResultsList.add(oTempSalesManagerDto);
                        } while (rs.next());
                     }
               }
        		 htSearchResults.put("resultsList", alSearchResultsList);// put the SearchresultList of arraylist value into Hashtable 
                 htSearchResults.put("ListModel", oListModel); // put the ListModel values into hashtable 
                 htSearchResults.put("locationQuery", sWhereString);//keep the Where condition for using in Export to Excel purpose .
             }
        	 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END.." );
        }finally {
        	 /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
        return htSearchResults ;
	}
	
	/**
	 * 
	 * @param oSalesManagerDto
	 * @return
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 17, 2008
	 */
	private String buildWhereClause(SalesManagerDto oSalesManagerDto) {
		
		String sMethodName = "buildWhereClause";
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
        
        StringBuffer sbWhereString = new StringBuffer(" AND QSM_ISDELETED ='N' AND ");
        
        
        if (oSalesManagerDto.getSalesManagerSSO() != null && oSalesManagerDto.getSalesManagerSSO().trim().length() > 0){
        	sbWhereString.append(" SAL.QSM_SALESMGR = '"+oSalesManagerDto.getSalesManagerSSO()+"'");
        	sbWhereString.append(" AND");
        }
        if(oSalesManagerDto.getRegionId()> 0) {
        	sbWhereString.append(" SAL.QSM_REGIONID = "+oSalesManagerDto.getRegionId()+"");
        	sbWhereString.append(" AND");
        }
        if (oSalesManagerDto.getPrimaryRSMSSO() != null && oSalesManagerDto.getPrimaryRSMSSO().trim().length() > 0){
        	sbWhereString.append(" SAL.QSM_PRIMARYRSM = '"+oSalesManagerDto.getPrimaryRSMSSO()+"'");
        	sbWhereString.append(" AND");
        }
        if (oSalesManagerDto.getAdditionalRSMSSO() != null && oSalesManagerDto.getAdditionalRSMSSO().trim().length() > 0){
        	sbWhereString.append(" SAL.QSM_ADDITIONALRSM = '"+oSalesManagerDto.getAdditionalRSMSSO()+"'");
        	sbWhereString.append(" AND");
        }
        
        if ( sbWhereString.toString().trim().equalsIgnoreCase(" AND") )
            return "";
        else if (sbWhereString.toString().trim().endsWith(" AND"))        	
            return sbWhereString.toString().substring(0, sbWhereString.toString().lastIndexOf("AND"));        
        else
        	return "";
	}
	
	/* (non-Javadoc)
	 * @see in.com.rbc.quotation.admin.dao.SalesManagerDao#createSalesManager(java.sql.Connection, in.com.rbc.quotation.admin.dto.SalesManagerDto)
	 */
	public boolean createSalesManager(Connection conn, SalesManagerDto oSalesManagerDto)throws Exception {
	
	   /*
		* Setting the Method Name as generic for logger Utility purpose . 
	    */
		String sClassName = this.getClass().getName();
		String sMethodName = "createSalesManager" ;
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
		/* PreparedStatement object to handle database operations */
	    PreparedStatement pstmt = null ;
	    PreparedStatement pstmt1 = null ;
	    
	    /* ResultSet object to store the rows retrieved from database */
	    ResultSet rs = null ;      
	    
	    /* Object of DBUtility class to handle database operations */
	    DBUtility oDBUtility = new DBUtility();
	    
	    /*getting the newlly Customer ID from the Customer sequence */
	    int iGetSalesManagerID = 0;
	    boolean isCreated = false ;
	
	    try {
	    	/**
	    	 * Getting the Customer id from the Customer Sequence for
	    	 */
	    	pstmt1 = conn.prepareStatement(CREATE_SALESMANAGER_ID);
	    	rs = pstmt1.executeQuery();// Execute the creating the Customer Sequence Query,
	    	
	    	LoggerUtility.log("INFO", sClassName, sMethodName, "Sales Mangeer ID creation Query -- " +CREATE_SALESMANAGER_ID);
	    	
	    	if(rs.next()) {
	    		iGetSalesManagerID = rs.getInt("SALESMANAGER_ID");
	    	}
	    	
	    	pstmt = conn.prepareStatement(CREATE_SALESMANAGER_VALUES);
	    	pstmt.setInt(1, iGetSalesManagerID);
	    	pstmt.setString(2,oSalesManagerDto.getSalesManagerSSO());
	    	pstmt.setInt(3,oSalesManagerDto.getRegionId());
	    	pstmt.setString(4,oSalesManagerDto.getPrimaryRSMSSO());
	    	pstmt.setString(5,oSalesManagerDto.getAdditionalRSMSSO());
	    	pstmt.setString(6,oSalesManagerDto.getCreatedById());
	    	pstmt.setString(7,oSalesManagerDto.getLastUpdatedById());
	    	pstmt.setString(8,oSalesManagerDto.getSmContactNo());
	        
	    	LoggerUtility.log("INFO", sClassName, sMethodName, "Sales Mangeer  creation Query -- " +CREATE_SALESMANAGER_VALUES);
	        int iUpdatedCount = pstmt.executeUpdate();

	        if(iUpdatedCount > 0 ) {
	        	isCreated = true;
	        }
	        LoggerUtility.log("INFO", sClassName, sMethodName, "Sales Mangeer  creation result -- " +isCreated);
	    }finally {
	    	/* Releasing PreparedStatement objects */
	        oDBUtility.releaseResources( rs,pstmt);
	        oDBUtility.releaseResources( pstmt1);
	    }
	    LoggerUtility.log("INFO",sClassName, sMethodName,"END" );
	    return isCreated;
	}
	
	/* (non-Javadoc)
	 * @see in.com.rbc.quotation.admin.dao.SalesManagerDao#getSalesManagerInfo(java.sql.Connection, in.com.rbc.quotation.admin.dto.SalesManagerDto)
	 */
	public SalesManagerDto getSalesManagerInfo(Connection conn, SalesManagerDto oSalesManagerDto)throws Exception {
		/*
		 * Setting the Method Name as generic for logger Utility purpose . 
		 */
		String sMethodName = "getSalesManagerInfo" ;
		String sClassName = this.getClass().getName();
		LoggerUtility.log("INFO", sClassName, sMethodName,"START" );
    		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;      
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();

        try {
        	LoggerUtility.log("INFO", sClassName, sMethodName,"Get the Salesmanager info Query :-"+ FETCH_SALESMANGER_INFO+" :-- Sales Manager ID :--"+oSalesManagerDto.getSalesMgrId());
        	pstmt = conn.prepareStatement(FETCH_SALESMANGER_INFO);
        	pstmt.setInt(1,oSalesManagerDto.getSalesMgrId());
        	rs = pstmt.executeQuery();
			
			while(rs.next()) {
				oSalesManagerDto.setSalesManagerSSO(rs.getString("QSM_SALESMGR"));
				oSalesManagerDto.setPrimaryRSMSSO(rs.getString("QSM_PRIMARYRSM"));
				oSalesManagerDto.setAdditionalRSMSSO(rs.getString("QSM_ADDITIONALRSM"));
				oSalesManagerDto.setSalesManager(rs.getString("SALESMANAGER_NAME"));
				oSalesManagerDto.setRegionId(rs.getInt("QSM_REGIONID"));
				oSalesManagerDto.setPrimaryRSM(rs.getString("PRIMARYRSM_NAME"));
				oSalesManagerDto.setAdditionalRSM(rs.getString("ADDITIONALRSM_NAME"));
				oSalesManagerDto.setSmContactNo(rs.getString("QSM_PHONENO"));
				oSalesManagerDto.setSmEmail(rs.getString("SALESMANAGER_EMAIL"));
			}
			
		}finally {
			/* Releasing ResultSet & PreparedStatement objects */
        		oDBUtility.releaseResources(rs,pstmt);
		}
		LoggerUtility.log("INFO", sClassName, sMethodName,"END");
		return oSalesManagerDto;
	}
	
	/* (non-Javadoc)
	 * @see in.com.rbc.quotation.admin.dao.SalesManagerDao#saveSalesManagerValue(java.sql.Connection, in.com.rbc.quotation.admin.dto.SalesManagerDto)
	 */
	public boolean saveSalesManagerValue(Connection conn, SalesManagerDto oSalesManagerDto)throws Exception {
		
		/*
    	 * Setting the Method Name as generic for logger Utility purpose . 
    	 */
		String sMethodName = "saveSalesManagerValue" ;
		String sClassName = this.getClass().getName();
		LoggerUtility.log("INFO", sClassName, sMethodName,"START" );
		
    	/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
		boolean isUpdated = false ;
		int iUpdate =0;

        try {
        	LoggerUtility.log("INFO", sClassName, sMethodName,"Upadte the Salesmanager Query :-"+ UPDATE_SALESMANAGER_VALUES+" :-- Sales Manager ID :--"+oSalesManagerDto.getSalesMgrId());
        	pstmt = conn.prepareStatement(UPDATE_SALESMANAGER_VALUES);
        	pstmt.setString(1,oSalesManagerDto.getSalesManagerSSO());
        	pstmt.setInt(2,oSalesManagerDto.getRegionId());
        	pstmt.setString(3,oSalesManagerDto.getPrimaryRSMSSO());
        	pstmt.setString(4,oSalesManagerDto.getAdditionalRSMSSO());
        	pstmt.setString(5,oSalesManagerDto.getLastUpdatedById());
        	pstmt.setString(6,oSalesManagerDto.getSmContactNo());
        	pstmt.setInt(7,oSalesManagerDto.getSalesMgrId());
        	iUpdate= pstmt.executeUpdate();
        	if(iUpdate > 0) {
        		isUpdated = true ;
        	}
        }finally {
        	/* Releasing PreparedStatement objects */
            oDBUtility.releaseResources( pstmt);
        }
        LoggerUtility.log("INFO", sClassName, sMethodName,"END");
		return isUpdated;
	}
	
	/* (non-Javadoc)
	 * @see in.com.rbc.quotation.admin.dao.SalesManagerDao#deleteSalesManagerValue(java.sql.Connection, in.com.rbc.quotation.admin.dto.SalesManagerDto)
	 */
	public boolean deleteSalesManagerValue(Connection conn, SalesManagerDto oSalesManagerDto)throws Exception {
		/*
    	 * Setting the Method Name as generic for logger Utility purpose . 
    	 */
		String sMethodName = "deleteSalesManagerValue" ;
		String sClassName = this.getClass().getName();
		LoggerUtility.log("INFO", sClassName, sMethodName,"START" );

    	/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        PreparedStatement pstmt1 = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ; 
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
		boolean isDeleted = false ;
		int iUpdateRow=0;
		int iCountRow = -1;
		try {
			/*
			 * this method is not deleting the whole record form the table , but its make update the isDelete column as 'Y',its mean , the sales manager is cant use anywhere .
			 */
				LoggerUtility.log("INFO", sClassName, sMethodName,"Checking the sales manager value :- "+ CHECK_SALESMANGER_VALUE +" :-- Sales Manager SSO :="+oSalesManagerDto.getSalesManagerSSO());
				/*
				 * Query to check with particular Sales Mangere has any pending status in WorkFlow table or
				 * to check in enquery table whether the user Enquery status is Releassed or pending ..
				 * query return the number records availale from table .
				 * if the query return count as 0, then its update the sales manager table isDelete coilumn as 'Y'. i.e., 
				 * the sales manager hasn't contain any records under with him.
				 * if the count return the count as greater than 1 , then the particular sales manager attached with some other records.
				 */

				pstmt = conn.prepareStatement(CHECK_SALESMANGER_VALUE); 
				pstmt.setString(1, oSalesManagerDto.getSalesManagerSSO());
		        rs = pstmt.executeQuery();
		        if(rs.next()) {
		        	iCountRow = rs.getInt(1);
		        	LoggerUtility.log("INFO", sClassName, sMethodName,"After Execute Query the number of records avaialble iCountRow :="+iCountRow);
		        }		        
		        if(iCountRow ==0) {
		        	LoggerUtility.log("INFO", sClassName, sMethodName,"Update Query for isDelete Column in Slaes Manager table :="+UPDATE_SALESMANAGER_DELETE_COLUMN);
		        	pstmt1 = conn.prepareStatement(DELETE_SALESMANAGER_DELETE_COLUMN);
		        	pstmt1.setString(1,oSalesManagerDto.getSalesManagerSSO());
		        	pstmt1.setInt(2,oSalesManagerDto.getRegionId());
		        	iUpdateRow = pstmt1.executeUpdate();
		        	LoggerUtility.log("INFO", sClassName, sMethodName,"After Execute Update Query isUpdateRow :="+iUpdateRow);
		        	if(iUpdateRow > 0) {
		        		isDeleted = true ;
		        	}
		        }
	        }finally {
	        	/* Releasing PreparedStatement objects */
	            oDBUtility.releaseResources(rs, pstmt);
	            oDBUtility.releaseResources( pstmt1);
	        }
	        LoggerUtility.log("INFO", sClassName, sMethodName,"END" );    
		return isDeleted;
	}
	
	/* (non-Javadoc)
	 * @see in.com.rbc.quotation.admin.dao.SalesManagerDao#exportSalesManagerSearchResult(java.sql.Connection, in.com.rbc.quotation.admin.dto.SalesManagerDto, java.lang.String)
	 */
	public ArrayList exportSalesManagerSearchResult(Connection conn, SalesManagerDto oSalesManagerDto,String sWhereQuery, String sSortFilter, String sSortType)throws Exception {
		
		/*
    	 * Setting the Method Name as generic for logger Utility purpose . 
    	 */
    	String sMethodName = "exportSalesManagerSearchResult Mehotd" ;
    	String sClassName = this.getClass().getName();
    	LoggerUtility.log("INFO", sClassName, sMethodName,"START" );
    		
    	/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
            
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;     
        
        SalesManagerDto oSalesManagerDtoo=null;// Create the Temporary Location Object for storing the table column values
            
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
       ArrayList alSearchResultsList = new ArrayList(); // Stores the list of rows retrieved from the database
       String sSqlQueryString = null; // Stores the complete SQL query to be executed
            
       try {
    	   				
            	sSqlQueryString = FETCH_SALESMANAGER_SEARCHRESULT;
            	
            	if (sWhereQuery != null && sWhereQuery.trim().length() > 0){
            		sSqlQueryString = sSqlQueryString + " "+ sWhereQuery;
            	}
            	
            	if (sSortFilter != null && sSortFilter.trim().length() > 0){
            		sSqlQueryString = sSqlQueryString + " ORDER BY "+ sSortFilter;
            		if (sSortType != null && sSortType.trim().length() > 0)
            			sSqlQueryString = sSqlQueryString + " " +sSortType;
            	}
            	
            	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName+"..Search Location Count Query",sSqlQueryString);
            	
            	pstmt = conn.prepareCall(sSqlQueryString);
            	rs= pstmt.executeQuery();
            	
            	 
         		if (rs.next()) {
                     do {
                    	 oSalesManagerDtoo = new SalesManagerDto();
                    	 oSalesManagerDtoo.setSalesMgrId(rs.getInt("QSM_SALESMGRID"));
                    	 oSalesManagerDtoo.setSalesManager(rs.getString("SALESMANAGER_NAME"));
                    	 oSalesManagerDtoo.setRegionName(rs.getString("QRE_REGIONNAME"));
                    	 oSalesManagerDtoo.setPrimaryRSM(rs.getString("PRIMARYRSM_NAME"));
                    	 oSalesManagerDtoo.setAdditionalRSM(rs.getString("ADDITIONALRSM_NAME"));
                    	 alSearchResultsList.add(oSalesManagerDtoo);
                    } while (rs.next());
                 }
            }finally {
            	/* Releasing PreparedStatement objects */
                oDBUtility.releaseResources( rs,pstmt);
            }
		
		return alSearchResultsList;
	}
	
	public int doesSalesManagerExist(Connection conn, SalesManagerDto oSalesManagerDto)throws Exception{
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        ResultSet rs = null;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
		int iExists = 0;

        try {
        	LoggerUtility.log("INFO",this.getClass().getName(),"doesSalesManagerExist","Sales Mangere Check Exists Query  :="+DOES_SALESMANAGER_EXIST  +"Exists  oSalesManagerDto.getSalesManagerSSO() :="+oSalesManagerDto.getSalesManagerSSO());
        	
        	pstmt = conn.prepareStatement(DOES_SALESMANAGER_EXIST);
        	pstmt.setString(1,oSalesManagerDto.getSalesManagerSSO());
	    	
        	rs = pstmt.executeQuery();
     		
     		while(rs.next()){
     			if(rs.getString("QSM_ISDELETED").equalsIgnoreCase("N"))
     				iExists = -1;
     			else
     				iExists =0;
     		}
        }finally {
        	/* Releasing PreparedStatement objects */
            oDBUtility.releaseResources( rs, pstmt);
        }
        
		return iExists;
	}
	
	public boolean updateExistSM(Connection conn ,SalesManagerDto oSalesManagerDto) throws Exception {
		
		/*
    	 * Setting the Method Name as generic for logger Utility purpose . 
    	 */
		String sMethodName = "updateExistCustomer" ;
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
    	/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
		boolean isUpdated = false ;
		int iUdateRowCount =0;

        try {
        	LoggerUtility.log("INFO",this.getClass().getName(),sMethodName,"Sales Manager Exists Update Query  :="+UPDATE_EXISTS_SM  +"Exists  oSalesManagerDto.getSalesManagerSSO() :="+oSalesManagerDto.getSalesManagerSSO());
        	pstmt = conn.prepareStatement(UPDATE_EXISTS_SM);
        	pstmt.setString(1,oSalesManagerDto.getSalesManagerSSO());
        	pstmt.setInt(2,oSalesManagerDto.getRegionId());
        	iUdateRowCount = pstmt.executeUpdate();
        	if(iUdateRowCount > 0) {
        		isUpdated = true ;
        	}
        }finally {
        	/* Releasing PreparedStatement objects */
            oDBUtility.releaseResources( pstmt);
        }
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );
		return isUpdated;
	}

}
