/**
 * *****************************************************************************
 * Project Name: quotation
 * Document Name: ListPriseAction.java
 * Package: in.com.rbc.quotation.admin.action
 * Desc: 
 * *****************************************************************************
 * Author: 900008798 (Abhilash Moola)
 * Date: Dec 03, 2020
 * *****************************************************************************
 */
package in.com.rbc.quotation.admin.action;

import java.io.InputStream;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

import in.com.rbc.quotation.admin.dao.ListPriseDao;
import in.com.rbc.quotation.admin.dto.ListPriceDto;
import in.com.rbc.quotation.admin.form.ListPriceForm;
import in.com.rbc.quotation.admin.utility.ListPriceBulkLoadManager;
import in.com.rbc.quotation.base.BaseAction;
import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.utility.CommonUtility;
import in.com.rbc.quotation.common.utility.DBUtility;
import in.com.rbc.quotation.common.utility.ExceptionUtility;
import in.com.rbc.quotation.common.utility.ListDBColumnUtil;
import in.com.rbc.quotation.common.utility.ListModel;
import in.com.rbc.quotation.common.utility.LoggerUtility;
import in.com.rbc.quotation.factory.DaoFactory;
import in.com.rbc.quotation.user.action.UserAction;
import in.com.rbc.quotation.user.dto.UserDto;

/**
 * <h3>Class Name:-ListPriceAction</h3> 
 * <br> This class upload bulk data 
 * <br>	-->viewListPrice , 
 * <br>	-->addNewListPrice
 * <br>	-->updateListPrice
 * <br>	--> and 
 * <br>This Class is Extends from BaseAction Class .
 * @author 900008798 (Abhilash Moola)
 *
 */
public class ListPriceAction extends BaseAction {
	
	public ActionForward viewListPrice(ActionMapping actionMapping, ActionForm actionForm,
			  HttpServletRequest request , HttpServletResponse response)throws Exception {
		/*
		* Setting user's information in request, using the header 
		* information received through request
		*/
		setRolesToRequest(request);
		
		// Method name Set for log file usage ;
		String sMethodname = "viewListPrice";
		LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"START");
		
		ListPriceForm oListPriceForm = (ListPriceForm) actionForm;
		if(oListPriceForm ==  null) {
			oListPriceForm = new ListPriceForm();
		}
		
		Connection conn = null;  // Connection object to store the database connection
		DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
		Hashtable htSearchResultsList =null; //Object of Hashtable to store different values
		ListModel oListModel=null;
		ListPriceDto oListPriseDto = null;
		DaoFactory oDaoFactory=null;
		ListPriseDao oListPriseDao = null;
		HttpSession session= request.getSession();
		
		try {
			saveToken(request);// To avoid entering the duplicate value into database.
			
			/*
			* Object of ListModel class to store the values necessary to display 
			* the results in attribute search results page with pagination
			*/
			oListModel = new ListModel();
			/*
			* Setting values for ListModel that are necessary to retrieve rows from the database
			* and display them in search results page. It holds information like
			* --> number of rows to be displayed at once in the page
			* --> column on which rows have to be sorted
			* --> order in which rows have to be sorted
			* --> set of rows to be retrieved depending on the page number selected for viewing
			* Details of parameters:
			* 1. Request object
			* 2. Sort column index
			* 3. Sort order
			* 4. Search form
			* 5. Field that stores the invoke method name
			* 8. Invoke method name
			*/
			oListModel.setParams(request, 
			 "1", 
			 ListModel.ASCENDING, 
			 "listPriceForm", 
			 "listPriceForm.invoke", 
			 "viewListPrice");
			
			/* Instantiating the DBUtility object */
			oDBUtility = new DBUtility();
			/* Creating an instance of of DaoFactory  */
			oDaoFactory = new DaoFactory();
			oListPriseDao = oDaoFactory.getListPriseDao();
			/* Retrieving the database connection using DBUtility.getDBConnection method */
			conn = oDBUtility.getDBConnection();
			oListPriseDto = new ListPriceDto();
			
			/* Copying the Form Object into DTO Object via Propertyutils*/
			PropertyUtils.copyProperties(oListPriseDto , oListPriceForm);
			
			/* Retrieving the search results using LocationDaoImpl.getLocationSearchResult method */
			htSearchResultsList = oListPriseDao.getListPriseSearchResult(conn,oListPriseDto,oListModel);
			
			
			if(htSearchResultsList != null ) {
				oListPriseDto.setSearchResultsList((ArrayList)htSearchResultsList.get(QuotationConstants.QUOTATION_LIST_RESULTSLIST));
				request.setAttribute(QuotationConstants.QUOTATION_LPDTO,oListPriseDto); 
				request.setAttribute(QuotationConstants.QUOTATION_LIST_LISTSIZE,new Integer(((ArrayList)htSearchResultsList.get(QuotationConstants.QUOTATION_LIST_RESULTSLIST)).size()));
				request.setAttribute(QuotationConstants.QUOTATION_LIST_RESULTSLIST,(ArrayList)htSearchResultsList.get(QuotationConstants.QUOTATION_LIST_RESULTSLIST));
		        request.setAttribute(QuotationConstants.QUOTATION_LIST_LISTMODELCLASS,htSearchResultsList.get(QuotationConstants.QUOTATION_LIST_LISTMODELCLASS));
		        session.setAttribute(QuotationConstants.QUOTATION_SESSIONLIST,(ArrayList)htSearchResultsList.get(QuotationConstants.QUOTATION_LIST_RESULTSLIST));
				session.setAttribute(QuotationConstants.RBC_QUOTATION_LISTPRICE_QUERY,(String)htSearchResultsList.get(QuotationConstants.RBC_QUOTATION_LISTPRICE_QUERY));
				session.setAttribute(QuotationConstants.QUOTATION_SEARCH_SORTFIELD, ListDBColumnUtil.getTechnicalDataResultDBField(oListModel.getSortBy()));
				session.setAttribute(QuotationConstants.QUOTATION_SEARCH_SORTORDER, oListModel.getSortOrderDesc());
			}else {
				request.setAttribute(QuotationConstants.QUOTATION_LIST_LISTSIZE, new Integer(0));
			}
			
			oListPriseDto = oListPriseDao.loadLookUpValues(conn, oListPriseDto);
			
			/* Copying the DTO Object to Form Object via Propertyutils */
			PropertyUtils.copyProperties(oListPriceForm, oListPriseDto);
		}catch(Exception e) {
			/*
			* Logging any exception that raised during this activity in log files using Log4j
			*/
			LoggerUtility.log("DEBUG", this.getClass().getName(), "viewListPrice  Result.", ExceptionUtility.getStackTraceAsString(e));
			
			/* Setting error details to request to display the same in Exception page */
			request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
			/* Forwarding request to Error page */
			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
		}finally {
		/* Releasing/closing the connection object */
		oDBUtility.releaseResources(conn);
		}
		LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"END");
				
		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_VIEW);
		
	}
	
	public ActionForward addNewListPrice(ActionMapping actionMapping, ActionForm actionForm,
			  HttpServletRequest request , HttpServletResponse response)throws Exception {
		/*
		* Setting user's information in request, using the header 
		* information received through request
		*/
		setRolesToRequest(request);
		
		// Method name Set for log file usage ;
		String sMethodname = "addNewListPrice";
		LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"START");
		
		ListPriceForm oListPriceForm = (ListPriceForm) actionForm;
		if(oListPriceForm ==  null) {
			oListPriceForm = new ListPriceForm();
		}
		
		Connection conn = null;  // Connection object to store the database connection
		DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
		ListPriceDto oListPriseDto = null;
		DaoFactory oDaoFactory=null;
		ListPriseDao oListPriseDao = null;
		
		try {
			saveToken(request); 	// To avoid entering the duplicate value into database.
			
			/* Instantiating the DBUtility object */
			oDBUtility = new DBUtility();
			/* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            /* Creating an instance of of DaoFactory  */
			oDaoFactory = new DaoFactory();
			
			oListPriseDao = oDaoFactory.getListPriseDao();
			oListPriseDto = new ListPriceDto();
			oListPriseDto = oListPriseDao.loadLookUpValues(conn, oListPriseDto);
			
			PropertyUtils.copyProperties(oListPriceForm , oListPriseDto);
			
		} catch(Exception e) {
			/*
			* Logging any exception that raised during this activity in log files using Log4j
			*/
			LoggerUtility.log("DEBUG", this.getClass().getName(), "addNewListPrice  Result.", ExceptionUtility.getStackTraceAsString(e));
			/* Setting error details to request to display the same in Exception page */
			request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
			/* Forwarding request to Error page */
			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
		} finally {
			/* Releasing/closing the connection object */
			oDBUtility.releaseResources(conn);
		}
		LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"END");
				
		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_ADDMASTERDATA);
	}
	
	public ActionForward addupdateListPrice(ActionMapping actionMapping, ActionForm actionForm,
			  HttpServletRequest request , HttpServletResponse response)throws Exception {
				
		// Method name Set for log file usage ;
		String sMethodname = "addupdateListPrice";
		LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"START");
		
		Connection conn = null;  // Connection object to store the database connection
		DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
		DaoFactory oDaoFactory = null;
		ListPriseDao oListPriseDao = null;
		String operation = null;
		String sSuccessMsg = "";
		boolean isUpdated = false;
		ListPriceDto oListPriseDto = null;
		
		try {
			saveToken(request);// To avoid entering the duplicate value into database.
			
			// Setting user's information in request, using the header 
			setRolesToRequest(request);
			
			/* Instantiating the DBUtility object */
			oDBUtility = new DBUtility();
			/* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
          	/* Creating an instance of of DaoFactory  */
			oDaoFactory = new DaoFactory();
			oListPriseDao = oDaoFactory.getListPriseDao();
			
			ListPriceForm oListPriceForm = (ListPriceForm) actionForm;
			if(oListPriceForm ==  null) {
				oListPriceForm = new ListPriceForm();
			}
			oListPriseDto = new ListPriceDto();
			
			/* Copying the Form Object into DTO Object via Propertyutils*/
			PropertyUtils.copyProperties(oListPriseDto , oListPriceForm);
			
			operation = CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_OPERATION);
			
			if(operation.equals(QuotationConstants.QUOTATION_FORWARD_UPDATE)){
				isUpdated = oListPriseDao.updateListPrice(conn, oListPriseDto);
				sSuccessMsg = QuotationConstants.QUOTATION_REC_UPDATESUCC;
			} else {
				isUpdated = oListPriseDao.addListPrice(conn, oListPriseDto);
				sSuccessMsg = QuotationConstants.QUOTATION_REC_ADDSUCC;
			}
			
			if(isUpdated)
				request.setAttribute(QuotationConstants.QUOTATION_ADMIN_SUCCESSMESSAGE, "List Price "+ sSuccessMsg);
			
			request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_URL, QuotationConstants.QUOTATION_LISTPRICE_URL); 
			
		} catch(Exception e) {
			/*
			* Logging any exception that raised during this activity in log files using Log4j
			*/
			LoggerUtility.log("DEBUG", this.getClass().getName(), "addupdateListPrice  Result.", ExceptionUtility.getStackTraceAsString(e));
			/* Setting error details to request to display the same in Exception page */
			request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
			/* Forwarding request to Error page */
			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
		} finally {
			/* Releasing/closing the connection object */
			oDBUtility.releaseResources(conn);
		}
		LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"END");
				
		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_SUCCESS);
	}
	
	public ActionForward editListPrice(ActionMapping actionMapping, ActionForm actionForm,
			  HttpServletRequest request , HttpServletResponse response)throws Exception {
		
		// Method name Set for log file usage ;
		String sMethodname = "viewListPrice";
		LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"START");
		
		Connection conn = null;  // Connection object to store the database connection
		DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
		ListPriceDto oListPriseDto = null;
		DaoFactory oDaoFactory=null;
		ListPriseDao oListPriseDao = null;
		
		try {
			saveToken(request);// To avoid entering the duplicate value into database.
			
			// Setting user's information in request, using the header information received through request
			setRolesToRequest(request);
			
			/* Instantiating the DBUtility object */
			oDBUtility = new DBUtility();
			/* Retrieving the database connection using DBUtility.getDBConnection method */
			conn = oDBUtility.getDBConnection();
			/* Creating an instance of of DaoFactory  */
			oDaoFactory = new DaoFactory();
			oListPriseDao = oDaoFactory.getListPriseDao();
			
			ListPriceForm oListPriceForm = (ListPriceForm) actionForm;
			if(oListPriceForm ==  null) {
				oListPriceForm = new ListPriceForm();
			}
			oListPriseDto = new ListPriceDto();
			
			/* Copying the Form Object into DTO Object via Propertyutils*/
			PropertyUtils.copyProperties(oListPriseDto , oListPriceForm);
			oListPriseDto.setSerial_id(CommonUtility.getIntParameter(request,QuotationConstants.QUOTATION_SERIAL_ID));
			oListPriseDto = oListPriseDao.getListPrice(conn, oListPriseDto);
			oListPriseDto.setOperation(QuotationConstants.QUOTATION_FORWARD_UPDATE);
			oListPriseDto=oListPriseDao.loadLookUpValues(conn, oListPriseDto);
			PropertyUtils.copyProperties(oListPriceForm,oListPriseDto);
			
		} catch(Exception e) {
			/*
			* Logging any exception that raised during this activity in log files using Log4j
			*/
			LoggerUtility.log("DEBUG", this.getClass().getName(), "viewListPrice  Result.", ExceptionUtility.getStackTraceAsString(e));
			/* Setting error details to request to display the same in Exception page */
			request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
			/* Forwarding request to Error page */
			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
		} finally {
			/* Releasing/closing the connection object */
			oDBUtility.releaseResources(conn);
		}
		LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"END");
				
		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_ADDMASTERDATA);
	}
	
	public ActionForward deleteListPrice(ActionMapping actionMapping, ActionForm actionForm,
			  HttpServletRequest request , HttpServletResponse response)throws Exception {
		
		// Method name Set for log file usage ;
		String sMethodname = "deleteListPrice";
		LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"START");
		
		Connection conn = null;  // Connection object to store the database connection
		DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
		ListPriceDto oListPriseDto = null;
		DaoFactory oDaoFactory=null;
		ListPriseDao oListPriseDao = null;
		boolean isUpdated = false;
		
		try {
			saveToken(request);// To avoid entering the duplicate value into database.
			
			// Setting user's information in request, using the header information received through request
			setRolesToRequest(request);
			
			/* Instantiating the DBUtility object */
			oDBUtility = new DBUtility();
			/* Retrieving the database connection using DBUtility.getDBConnection method */
			conn = oDBUtility.getDBConnection();
			/* Creating an instance of of DaoFactory  */
			oDaoFactory = new DaoFactory();
			oListPriseDao = oDaoFactory.getListPriseDao();
			
			
			ListPriceForm oListPriceForm = (ListPriceForm) actionForm;
			if(oListPriceForm ==  null) {
				oListPriceForm = new ListPriceForm();
			}
			oListPriseDto = new ListPriceDto();
			oListPriseDto.setSerial_id(CommonUtility.getIntParameter(request,QuotationConstants.QUOTATION_SERIAL_ID));
			
			PropertyUtils.copyProperties(oListPriseDto , oListPriceForm);
			
			isUpdated = oListPriseDao.deleteListPrice(conn, oListPriseDto);
			
			PropertyUtils.copyProperties(oListPriceForm,oListPriseDto);
			
			if(isUpdated)
				request.setAttribute(QuotationConstants.QUOTATION_ADMIN_SUCCESSMESSAGE, "Selected List Price " + QuotationConstants.QUOTATION_REC_DELSUCC);
				
			request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_URL, QuotationConstants.QUOTATION_LISTPRICE_URL); 
				
		} catch(Exception e) {
			/*
			* Logging any exception that raised during this activity in log files using Log4j
			*/
			LoggerUtility.log("DEBUG", this.getClass().getName(), "viewListPrice  Result.", ExceptionUtility.getStackTraceAsString(e));
			/* Setting error details to request to display the same in Exception page */
			request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
			/* Forwarding request to Error page */
			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
		} finally {
			/* Releasing/closing the connection object */
			oDBUtility.releaseResources(conn);
		}
		LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"END");
				
		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_SUCCESS);
	}
	
	/**
	 * Method to Perform Export to Excel of the List Prices Search Result.
	 * @param actionMapping ActionMapping object to redirect the user to next page depending on the activity
     * @param actionForm 	ActionForm object to handle the form elements
     * @param request 		HttpServletRequest object to handle request operations
     * @param response 		HttpServletResponse object to handle response operations
     * @return returns 		ActionForward depending on which user is redirected to next page
     * @throws Exception if any error occurs while performing database operations, etc
	 */
	public ActionForward exportListPriceSearch(ActionMapping actionMapping, ActionForm actionForm,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		String sMethodname = "exportListPriceSearch";
		String sClassname = this.getClass().getName();
		LoggerUtility.log("INFO", sClassname, sMethodname, "START");

		// Setting user's information in request, using the header information received
		// through request
		setRolesToRequest(request);

		// Get Form objects from the actionForm
		ListPriceForm oListPriceForm = (ListPriceForm) actionForm;
		if (oListPriceForm == null) {
			oListPriceForm = new ListPriceForm();
		}

		Connection conn = null; // Connection object to store the database connection
		DBUtility oDBUtility = null; // Object of DBUtility class to handle DB connection objects
		HttpSession session = request.getSession();
		String sOperation = null;
		String sWhereQuery = null;
		String sSortFilter = null;
		String sSortOrder = null;
		DaoFactory oDaoFactory = null;
		ListPriseDao oListPriseDao = null;
		ListPriceDto oListPriseDto = null;
		ArrayList alSearchResultsList = new ArrayList();
		
		try {
			/* Instantiating the DBUtility object */
			oDBUtility = new DBUtility();
			/* Creating an instance of of DaoFactory */
			oDaoFactory = new DaoFactory();
			/* Retrieving the database connection using DBUtility.getDBConnection method */
			conn = oDBUtility.getDBConnection();
			
			oListPriseDao = oDaoFactory.getListPriseDao();
			oListPriseDto = new ListPriceDto();
			
			sWhereQuery = (String) session.getAttribute(QuotationConstants.RBC_QUOTATION_LISTPRICE_QUERY);
			sOperation = CommonUtility.getStringParameter(request, QuotationConstants.QUOTATION_FUNCTIONTYPES);

			if (session.getAttribute(QuotationConstants.QUOTATION_SEARCH_SORTFIELD) != null)
				sSortFilter = (String) session.getAttribute(QuotationConstants.QUOTATION_SEARCH_SORTFIELD);

			if (session.getAttribute(QuotationConstants.QUOTATION_SEARCH_SORTORDER) != null)
				sSortOrder = (String) session.getAttribute(QuotationConstants.QUOTATION_SEARCH_SORTORDER);

			/* Copying the Form Object into DTO Object via Propertyutils */
			PropertyUtils.copyProperties(oListPriseDto, oListPriceForm);
			
			/* Retrieving the search results using oHelpDto.getHelpList method */
			alSearchResultsList = oListPriseDao.exportListPriceSearchResult(conn, oListPriseDto, sWhereQuery, sSortFilter, sSortOrder);  
			
			request.setAttribute(QuotationConstants.QUOTATION_ADMIN_EXPORTLIST, alSearchResultsList);
			request.setAttribute(QuotationConstants.QUOTATION_ADMIN_EXPORTTYPE, QuotationConstants.QUOTATION_ADMIN_EXPORT_LISTPRICE);
			
		} catch (Exception e) {
			/* Logging any exception that raised during this activity in log files using Log4j */
			LoggerUtility.log("DEBUG", this.getClass().getName(), "export ListPrice Search Result.", ExceptionUtility.getStackTraceAsString(e));
			/* Setting error details to request to display the same in Exception page */
			request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
			/* Forwarding request to Error page */
			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
		} finally {
			/* Releasing/closing the connection object */
			oDBUtility.releaseResources(conn);
		}

		LoggerUtility.log("INFO", sClassname, sMethodname, "END");

		if (sOperation.equalsIgnoreCase(QuotationConstants.QUOTATION_FORWARD_EXPORT))
			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_EXPORT);
		else if (sOperation.equalsIgnoreCase(QuotationConstants.QUOTATION_FORWARD_PRINT))
			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_PRINT);
		else
			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
	}
	
	/**
	 * Method to Process the BulkUpload Sheet for List Prices.
	 * @param actionMapping ActionMapping object to redirect the user to next page depending on the activity
     * @param actionForm 	ActionForm object to handle the form elements
     * @param request 		HttpServletRequest object to handle request operations
     * @param response 		HttpServletResponse object to handle response operations
     * @return returns 		ActionForward depending on which user is redirected to next page
     * @throws Exception if any error occurs while performing database operations, etc
	 */
	public ActionForward processBulkUploadListPrice(ActionMapping actionMapping, ActionForm actionForm,
			  HttpServletRequest request , HttpServletResponse response)throws Exception {
		
		String sMethodName = "processBulkUploadListPrice";
		String sClassName = this.getClass().getName();
		LoggerUtility.log("INFO", sClassName, sMethodName, "Start");
		
		Connection conn = null;  // Connection object to store the database connection
		DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
		
		String sPath = QuotationConstants.QUOTATION_FORWARD_FAILURE;
		ArrayList priceList = null;
		Hashtable htBulkLoadReport = null;
		InputStream ipStrListPriceLoadFile = null;
		UserAction oUserAction = null; //Instantiating the UserAction object
		
		try {
			/*
			 * Setting user's information in request, using the header 
			 * information received through request
			 */
			setRolesToRequest(request);
			
			/* Instantiating the DBUtility object */
			oDBUtility = new DBUtility();
			/* Retrieving the database connection using DBUtility.getDBConnection method */
			conn = oDBUtility.getDBConnection();
			/* Instantiating the UserAction object */
            oUserAction = new UserAction();
            
            //getting the Logged-in User ID
            UserDto oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
			String loggedInUserSSOId = oUserDto.getUserId();
            
			/* Retrieving the Form object */
			ListPriceForm oListPriceForm = (ListPriceForm)actionForm;
			if ( oListPriceForm == null ) {
				sPath = QuotationConstants.APP_FORWARD_FAILURE;
				
			} else {
				LoggerUtility.log("INFO", sClassName, sMethodName, " List Price Bulk Load : Activity Start....");
				
				// Process the List Price Bulk Load File.
				if (oListPriceForm.getIsFileLoaded() != null ) {
					if (isTokenValid(request)) {
						resetToken(request);
						sPath = "report";
						
						ListPriceBulkLoadManager oLPBulkLoadManager = new ListPriceBulkLoadManager();
						// Retrieving the List price Details from the Uploaded File.
						FormFile ffListPriceLoadFile = oListPriceForm.getListpriceBulkupload();
						if (ffListPriceLoadFile != null) {
							ipStrListPriceLoadFile = ffListPriceLoadFile.getInputStream();
							
							priceList = oLPBulkLoadManager.retrieveFromExcel(request, ipStrListPriceLoadFile);
							if ( priceList != null ) {
								LoggerUtility.log("INFO", sClassName, sMethodName, "List Price Bulk Load : After retrieving records from Excel :: Records count = " + priceList.size());
							}
						}
						
						if (priceList != null) {
							htBulkLoadReport = oLPBulkLoadManager.processListPriceBulkLoadList(conn, priceList, loggedInUserSSOId);
							
							if (htBulkLoadReport != null) {
								LoggerUtility.log("INFO", sClassName, sMethodName, "List Price Bulk Load : After Processing Price List : "
										+ " Result = " + (String) htBulkLoadReport.get("RESULT") + " : "
										+ " Records Count = " + (Integer)htBulkLoadReport.get("RECORDCOUNT") + " : "
										+ " Inserted Records Count = " + ((ArrayList) htBulkLoadReport.get("INSERTEDRECORDS")).size() + " : "
										+ " Invalid Records Count = " + ((ArrayList) htBulkLoadReport.get("INVALIDRECORDS")).size() + " : "
										+ " Existing Records Count = " + ((ArrayList) htBulkLoadReport.get("EXISTINGRECORDS")).size() );
								
								/* Retrieving bulk load report details from the Hashtable and setting in the request, to display in confirmation page */
								if (((String) htBulkLoadReport.get("RESULT")).length() > 0) {
									request.setAttribute("sResultString", (String) htBulkLoadReport.get("RESULT"));
									request.setAttribute(QuotationConstants.QUOTATION_ADMIN_SUCCESSMESSAGE, "ListPrice "+QuotationConstants.QUOTATION_REC_BULKLOADSUCC);
								}
								request.setAttribute("recordCount", (Integer)htBulkLoadReport.get("RECORDCOUNT"));
								if (((ArrayList) htBulkLoadReport.get("INSERTEDRECORDS")).size() > 0) {
									request.setAttribute("arlInsertedRecordsList", (ArrayList) htBulkLoadReport.get("INSERTEDRECORDS"));
									request.setAttribute("insertedRecordsCount", ((ArrayList) htBulkLoadReport.get("INSERTEDRECORDS")).size());
								} else {
									request.setAttribute("insertedRecordsCount", 0);
								}
								if (((ArrayList) htBulkLoadReport.get("INVALIDRECORDS")).size() > 0) {
									request.setAttribute("arlInvalidRecordsList", (ArrayList) htBulkLoadReport.get("INVALIDRECORDS"));
									request.setAttribute("invalidRecordsCount", ((ArrayList) htBulkLoadReport.get("INVALIDRECORDS")).size());
								} else {
									request.setAttribute("invalidRecordsCount", 0);
								}
								if (((ArrayList) htBulkLoadReport.get("EXISTINGRECORDS")).size() > 0) {
									request.setAttribute("arlExistingRecordsList", (ArrayList) htBulkLoadReport.get("EXISTINGRECORDS"));
									request.setAttribute("existingsRecordsCount", ((ArrayList) htBulkLoadReport.get("EXISTINGRECORDS")).size());
								} else {
									request.setAttribute("existingsRecordsCount", 0);
								}
								
								request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_URL, QuotationConstants.QUOTATION_LISTPRICE_URL); 
							}
						}
						
					} else {
						/* Setting error details to request to display the same in Exception page */ 
						request.setAttribute(QuotationConstants.APP_ERRORMESSAGE, 
						              "Your request could not be processed due to one of the below reasons. Please try again." +
						              "<ul>" +
						              "<li>You clicked on the submit button, more than once, before the response is sent back." +
						              "<li>You clicked on the back button in the browser or simply refreshed the page." +
						              "<li>You accessesed the web page by returning to a previously bookmarked page." +
						              "</ul>");
						sPath = QuotationConstants.APP_FORWARD_FAILURE;
					}
				} else {
					sPath = "report";
				}
			}
			LoggerUtility.log("INFO", sClassName, sMethodName, " List Price Bulk Load : Activity End....");
			
		} catch (Exception e) {
			/*
			* Logging any exception that raised during this activity in log files using Log4j
			*/
			LoggerUtility.log("DEBUG", sClassName, sMethodName, ExceptionUtility.getStackTraceAsString(e));
			
			/* Setting error details to request to display the same in Exception page */
			request.setAttribute(QuotationConstants.APP_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
			
			/* Forwarding request to Error page */
			return actionMapping.findForward(sPath);
		} finally {
			if(ipStrListPriceLoadFile != null){
				ipStrListPriceLoadFile.close();
			}
			/* Releasing/closing the connection object */
			oDBUtility.releaseResources(conn);
		}
		
		LoggerUtility.log("INFO", sClassName, sMethodName, "End");
		return actionMapping.findForward(sPath);
	}

}
