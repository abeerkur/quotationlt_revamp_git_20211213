/**
 * *****************************************************************************
 * Project Name: quotation
 * Document Name: CustomerForm.java
 * Package: in.com.rbc.quotation.admin.form
 * Desc: 
 * *****************************************************************************
 * Author: 100005701 (Suresh)
 * Date: Sep 30, 2008
 * *****************************************************************************
 */
package in.com.rbc.quotation.admin.form;

import java.util.ArrayList;

import in.com.rbc.quotation.common.utility.CommonUtility;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

/**
 * @author 100005701 (Suresh Shanmugam)
 *
 */
public class CustomerForm extends ActionForm {
	
	private String invoke ;
	private String customer ;
	private String customerName;
	private String contactperson ;
	private String locationname ;
	private int locationId =0;
	private int customerId=0;
	private String email;
	private String phone;
	private String address;
	private String fax;
	private String mobile;
	private String operation ;
	private int salesengineerId;
	private String salesengineer;
	private ArrayList salesengineerList = null;
	private String customerType;
	private String industry;
	private String sapcode;
	private String oraclecode;
	private String gstnumber;
	private String state;
	private String country;
	
	private String customerRSM;
	private String customerIndustry;
	private String customerSalesGroup;
	private String customerClass;
	
	FormFile customerBulkuploadFile = null;
	
	private ArrayList searchResultsList = null;
	private ArrayList locationList = null;

	/**
	 * @return Returns the locationList.
	 */
	public ArrayList getLocationList() {
		return this.locationList;
	}


	/**
	 * @param locationList The locationList to set.
	 */
	public void setLocationList(ArrayList locationList) {
		this.locationList = locationList;
	}


	/**
	 * @return Returns the contactperson.
	 */
	public String getContactperson() {
		return CommonUtility.replaceNull(this.contactperson);
	}


	/**
	 * @param contactperson The contactperson to set.
	 */
	public void setContactperson(String contactperson) {
		this.contactperson = contactperson;
	}


	/**
	 * @return Returns the customer.
	 */
	public String getCustomer() {
		return CommonUtility.replaceNull(this.customer);
	}


	/**
	 * @param customer The customer to set.
	 */
	public void setCustomer(String customer) {
		this.customer = customer;
	}


	/**
	 * @return Returns the email.
	 */
	public String getEmail() {
		return CommonUtility.replaceNull(this.email);
	}


	/**
	 * @param email The email to set.
	 */
	public void setEmail(String email) {
		this.email = email;
	}


	/**
	 * @return Returns the invoke.
	 */
	public String getInvoke() {
		return CommonUtility.replaceNull(this.invoke);
	}


	/**
	 * @param invoke The invoke to set.
	 */
	public void setInvoke(String invoke) {
		this.invoke = invoke;
	}


	/**
	 * @return Returns the locationId.
	 */
	public int getLocationId() {
		return this.locationId;
	}


	/**
	 * @param locationId The locationId to set.
	 */
	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}


	/**
	 * @return Returns the locationname.
	 */
	public String getLocationname() {
		return CommonUtility.replaceNull(this.locationname);
	}


	/**
	 * @param locationname The locationname to set.
	 */
	public void setLocationname(String locationname) {
		this.locationname = locationname;
	}


	/**
	 * @return Returns the phone.
	 */
	public String getPhone() {
		return CommonUtility.replaceNull(this.phone);
	}


	/**
	 * @param phone The phone to set.
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}


	/**
	 * @return Returns the searchResultsList.
	 */
	public ArrayList getSearchResultsList() {
		return this.searchResultsList;
	}


	/**
	 * @param searchResultsList The searchResultsList to set.
	 */
	public void setSearchResultsList(ArrayList searchResultsList) {
		this.searchResultsList = searchResultsList;
	}


	/**
	 * @return Returns the customerId.
	 */
	public int getCustomerId() {
		return this.customerId;
	}


	/**
	 * @param customerId The customerId to set.
	 */
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}


	/**
	 * @return Returns the address.
	 */
	public String getAddress() {
		return CommonUtility.replaceNull(this.address);
	}


	/**
	 * @param address The address to set.
	 */
	public void setAddress(String address) {
		this.address = address;
	}


	/**
	 * @return Returns the fax.
	 */
	public String getFax() {
		return CommonUtility.replaceNull(this.fax);
	}


	/**
	 * @param fax The fax to set.
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}


	/**
	 * @return Returns the mobile.
	 */
	public String getMobile() {
		return CommonUtility.replaceNull(this.mobile);
	}


	/**
	 * @param mobile The mobile to set.
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}


	/**
	 * @return Returns the operation.
	 */
	public String getOperation() {
		return CommonUtility.replaceNull(this.operation);
	}


	/**
	 * @param operation The operation to set.
	 */
	public void setOperation(String operation) {
		this.operation = operation;
	}


	/**
	 * @return Returns the customerName.
	 */
	public String getCustomerName() {
		return CommonUtility.replaceNull(this.customerName);
	}


	/**
	 * @param customerName The customerName to set.
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public int getSalesengineerId() {
		return salesengineerId;
	}

	public void setSalesengineerId(int salesengineerId) {
		this.salesengineerId = salesengineerId;
	}

	public String getSalesengineer() {
		return CommonUtility.replaceNull(salesengineer);
	}

	public void setSalesengineer(String salesengineer) {
		this.salesengineer = salesengineer;
	}
	
	public ArrayList getSalesengineerList() {
		return salesengineerList;
	}

	public void setSalesengineerList(ArrayList salesengineerList) {
		this.salesengineerList = salesengineerList;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getSapcode() {
		return sapcode;
	}

	public void setSapcode(String sapcode) {
		this.sapcode = sapcode;
	}

	public String getOraclecode() {
		return oraclecode;
	}

	public void setOraclecode(String oraclecode) {
		this.oraclecode = oraclecode;
	}

	public String getGstnumber() {
		return gstnumber;
	}

	public void setGstnumber(String gstnumber) {
		this.gstnumber = gstnumber;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public FormFile getCustomerBulkuploadFile() {
		return customerBulkuploadFile;
	}


	public void setCustomerBulkuploadFile(FormFile customerBulkuploadFile) {
		this.customerBulkuploadFile = customerBulkuploadFile;
	}
	
	public String getCustomerRSM() {
		return CommonUtility.replaceNull(customerRSM);
	}
	
	public void setCustomerRSM(String customerRSM) {
		this.customerRSM = customerRSM;
	}

	public String getCustomerIndustry() {
		return CommonUtility.replaceNull(customerIndustry);
	}

	public void setCustomerIndustry(String customerIndustry) {
		this.customerIndustry = customerIndustry;
	}

	public String getCustomerSalesGroup() {
		return CommonUtility.replaceNull(customerSalesGroup);
	}

	public void setCustomerSalesGroup(String customerSalesGroup) {
		this.customerSalesGroup = customerSalesGroup;
	}

	public String getCustomerClass() {
		return CommonUtility.replaceNull(customerClass);
	}

	public void setCustomerClass(String customerClass) {
		this.customerClass = customerClass;
	}
	
}
