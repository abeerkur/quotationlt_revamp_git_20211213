/**
 * ******************************************************************************************
 * Project Name: Request For Quotation
 * Document Name: SearchAction.java
 * Package: in.com.rbc.quotation.requst.action
 * Desc:  Action class that contains all the action methods for Search
 * ******************************************************************************************
 * @author 100002865 (Shanthi Chinta)
 * ******************************************************************************************
 */
package in.com.rbc.quotation.admin.action;


import in.com.rbc.quotation.admin.dao.LocationDao;
import in.com.rbc.quotation.admin.dao.MasterDao;
import in.com.rbc.quotation.admin.dto.LocationDto;
import in.com.rbc.quotation.admin.dto.MasterDto;
import in.com.rbc.quotation.admin.dto.MastersDataDto;
import in.com.rbc.quotation.admin.form.MasterForm;
import in.com.rbc.quotation.admin.form.MastersDataForm;
import in.com.rbc.quotation.base.BaseAction;
import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.utility.CommonUtility;
import in.com.rbc.quotation.common.utility.DBUtility;
import in.com.rbc.quotation.common.utility.ExceptionUtility;
import in.com.rbc.quotation.common.utility.ListModel;
import in.com.rbc.quotation.common.utility.LoggerUtility;
import in.com.rbc.quotation.factory.DaoFactory;
import in.com.rbc.quotation.user.action.UserAction;
import in.com.rbc.quotation.user.dto.UserDto;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class MasterAction extends BaseAction {
	/** 
	 * This method shows the results of master List Search
	 * @param actionMapping ActionMapping object to redirect the user to next page depending on the activity
     * @param actionForm ActionForm object to handle the form elements
     * @param request HttpServletRequest object to handle request operations
     * @param response HttpServletResponse object to handle response operations
     * @return returns ActionForward depending on which user is redirected to next page
     * @throws Exception if any error occurs while performing database operations, etc
     * @author 100006718
     *  Date: August 26, 2008
	 */
	
	public ActionForward viewSearchMasterList(ActionMapping actionMapping,
            ActionForm actionForm,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception 
    {
		setRolesToRequest(request);
		
		String sMethodName = "viewSearchMasterList";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		String sPath = QuotationConstants.QUOTATION_FORWARD_SUCCESS ;
		String sDipatch = CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_DISPATCH);
		sDipatch = sDipatch==null?"":sDipatch;
		/*
		 * Object of Hashtable to store different values that are retrieved 
		 * using searchEmployeeDto.getAttributeSearchResults method
		 */
		Hashtable htSearchMasterResultsList = null;
		
		/*
         * Object of ListModel class to store the values necessary to display 
         * the results in attribute search results page with pagination
         */
        ListModel oListModel = null;

        MasterDao oMasterDao =null;
		
		
		
		Connection oConnection = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        MasterDto oMasterDto = new MasterDto();
        HttpSession session;
        MasterForm oMasterForm = (MasterForm)actionForm;
        if (oMasterForm == null){
        	oMasterForm = new MasterForm();
        }
        /*Saving Request into a Token*/
        saveToken(request);
       
        session = request.getSession();
        try {
         
        	    /*
        		 * Object of Hashtable to store different values that are retrieved 
        		 * using searchEmployeeDto.getAttributeSearchResults method
        		 */
        		htSearchMasterResultsList = new Hashtable();
        		/*
                * Object of ListModel class to store the values necessary to display 
                * the results in attribute search results page with pagination
                */
                oListModel = new ListModel();
           	
           	    /*
                * Setting values for ListModel that are necessary to retrieve rows from the database
                * and display them in search results page. It holds information like
                * --> number of rows to be displayed at once in the page
                * --> column on which rows have to be sorted
                * --> order in which rows have to be sorted
                * --> set of rows to be retrieved depending on the page number selected for viewing
                * Details of parameters:
                * 1. Request object
                * 2. Sort column index
                * 3. Sort order
                * 4. Search form
                * 5. Field that stores the invoke method name
                * 8. Invoke method name
                */
               oListModel.setParams(request, 
                                      "1", 
                                      ListModel.ASCENDING, 
                                      "MasterForm", 
                                      "MasterForm.invoke", 
                                      "viewSearchMasterList");
               
               /* Instantiating the DBUtility object */
                oDBUtility = new DBUtility();
                    
               /* Creating an instance of of DaoFactory & retrieving AttributeSearchDao object */
                DaoFactory oDaoFactory = new DaoFactory();
               /* Retrieving the database connection using DBUtility.getDBConnection method */
                oConnection = oDBUtility.getDBConnection();
                oMasterDao = oDaoFactory.getMasterDao();
                PropertyUtils.copyProperties( oMasterDto, oMasterForm);
                
                htSearchMasterResultsList = oMasterDao.getMasterListResults(oConnection,oMasterDto,oListModel);
                if (htSearchMasterResultsList != null) {
                 
                	request.setAttribute(QuotationConstants.QUOTATION_LIST_RESULTSLIST,
                                              (ArrayList)htSearchMasterResultsList
                                              .get(QuotationConstants.QUOTATION_LIST_RESULTSLIST));
                    request.setAttribute(QuotationConstants.QUOTATION_LIST_LISTMODELCLASS,
                    		htSearchMasterResultsList
                                               .get(QuotationConstants.QUOTATION_LIST_LISTMODELCLASS));
                    request.setAttribute(QuotationConstants.QUOTATION_LIST_LISTSIZE,
                                               new Integer(((ArrayList)htSearchMasterResultsList
                                               .get(QuotationConstants.QUOTATION_LIST_RESULTSLIST))
                                           .size()));   	
                   
                    session.setAttribute(QuotationConstants.QUOTATION_SEARCH_QUERY,(String)htSearchMasterResultsList.get(QuotationConstants.QUOTATION_SEARCH_QUERY));
                }
            }
                    
      
        catch (Exception e){
        	  /*
             * Logging any exception that raised during this activity in log files using Log4j
             */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "viewSearchMasterList", ExceptionUtility.getStackTraceAsString(e));
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
      	
        }
        finally{
        	
        	if(oConnection != null ) oDBUtility.releaseResources(oConnection);
        	
        }
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
        return actionMapping.findForward(sPath);
    }
	
	/** 
	 * This method shows the results of master List Search
	 * @param actionMapping ActionMapping object to redirect the user to next page depending on the activity
     * @param actionForm ActionForm object to handle the form elements
     * @param request HttpServletRequest object to handle request operations
     * @param response HttpServletResponse object to handle response operations
     * @return returns ActionForward depending on which user is redirected to next page
     * @throws Exception if any error occurs while performing database operations, etc
     * @author 100006718
     *  Date: August 26, 2008
	 */
	
	public ActionForward viewSavedSearchMasterList(ActionMapping actionMapping,
            ActionForm actionForm,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception 
    {
		
		String sMethodName = "viewSearchMasterList";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		String sPath = QuotationConstants.QUOTATION_FORWARD_SUCCESS ;
		String sDispatch = CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_DISPATCH);
		sDispatch = sDispatch==null?"":sDispatch;
		
	    /*
		 * Object of Hashtable to store different values that are retrieved 
		 * using searchEmployeeDto.getAttributeSearchResults method
		 */
		Hashtable htSearchMasterResultsList = null;
		/*
         * Object of ListModel class to store the values necessary to display 
         * the results in attribute search results page with pagination
         */
        ListModel oListModel = null;
        MasterDao oMasterDao =null;

		
		
		Connection oConnection = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        MasterDto oMasterDto = new MasterDto();
        HttpSession session;
        MasterForm oMasterForm = (MasterForm)actionForm;
        if (oMasterForm == null){
        	oMasterForm = new MasterForm();
        }
        /*Saving Request into a Token*/
        saveToken(request);
       
        session = request.getSession();
        
        setRolesToRequest(request);
        try {
         
        	    /*
        		 * Object of Hashtable to store different values that are retrieved 
        		 * using searchEmployeeDto.getAttributeSearchResults method
        		 */
        		 htSearchMasterResultsList = new Hashtable();
        		/*
                * Object of ListModel class to store the values necessary to display 
                * the results in attribute search results page with pagination
                */
                oListModel = new ListModel();
           	
           	    /*
                * Setting values for ListModel that are necessary to retrieve rows from the database
                * and display them in search results page. It holds information like
                * --> number of rows to be displayed at once in the page
                * --> column on which rows have to be sorted
                * --> order in which rows have to be sorted
                * --> set of rows to be retrieved depending on the page number selected for viewing
                * Details of parameters:
                * 1. Request object
                * 2. Sort column index
                * 3. Sort order
                * 4. Search form
                * 5. Field that stores the invoke method name
                * 8. Invoke method name
                */
               oListModel.setParams(request, 
                                      "1", 
                                      ListModel.ASCENDING, 
                                      "MasterForm", 
                                      "MasterForm.invoke", 
                                      "viewSearchMasterList");
               
               /* Instantiating the DBUtility object */
                oDBUtility = new DBUtility();
                    
               /* Creating an instance of of DaoFactory & retrieving AttributeSearchDao object */
                DaoFactory oDaoFactory = new DaoFactory();
               /* Retrieving the database connection using DBUtility.getDBConnection method */
                oConnection = oDBUtility.getDBConnection();
                oMasterDao = oDaoFactory.getMasterDao();
                PropertyUtils.copyProperties( oMasterDto, oMasterForm);
                
                htSearchMasterResultsList = oMasterDao.getMasterListResults(oConnection,oMasterDto,oListModel,(String)session.getAttribute(QuotationConstants.QUOTATION_SEARCH_QUERY));
                if (htSearchMasterResultsList != null) {
                 
                	request.setAttribute(QuotationConstants.QUOTATION_LIST_RESULTSLIST,
                                              (ArrayList)htSearchMasterResultsList
                                              .get(QuotationConstants.QUOTATION_LIST_RESULTSLIST));
                    request.setAttribute(QuotationConstants.QUOTATION_LIST_LISTMODELCLASS,
                    		htSearchMasterResultsList
                                               .get(QuotationConstants.QUOTATION_LIST_LISTMODELCLASS));
                    request.setAttribute(QuotationConstants.QUOTATION_LIST_LISTSIZE,
                                               new Integer(((ArrayList)htSearchMasterResultsList
                                               .get(QuotationConstants.QUOTATION_LIST_RESULTSLIST))
                                           .size()));   	
                   
                    session.setAttribute(QuotationConstants.QUOTATION_SEARCH_QUERY,(String)htSearchMasterResultsList.get(QuotationConstants.QUOTATION_SEARCH_QUERY));
                }
            }
                    
      
        catch (Exception e){
        	  /*
             * Logging any exception that raised during this activity in log files using Log4j
             */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "viewSearchMasterList", ExceptionUtility.getStackTraceAsString(e));
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
      	
        }
        finally{
        	
        	if(oConnection != null ) oDBUtility.releaseResources(oConnection);
        	
        }
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
        return actionMapping.findForward(sPath);
    }
	
	/**
	 * This method get the list of masters for Print or Export to Excel
	 * @param actionMapping ActionMapping object to redirect the user to next page depending on the activity
     * @param actionForm ActionForm object to handle the form elemets
     * @param request HttpServletRequest object to handle request operations
     * @param response HttpServletResponse object to handle response operations
     * @return returns ActionForward depending on which user is redirected to next page
     * @throws Exception if any error occurs while performing database operations, etc
     * @author 100006718
     *  Date: August 26, 2008
	 */
	
	public ActionForward printOrExportMasterResults(ActionMapping actionMapping,
            ActionForm actionForm,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception 
    {
		
		String sMethodName = "printOrExportMasterResults";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		/* Getting the funtion Type from Request object*/
		String sFunctionType =CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_FUNCTIONTYPES); 
		if (sFunctionType == null || sFunctionType.trim().length() < 1){
			sFunctionType =QuotationConstants.QUOTATION_SNONE; 
		}
		request.setAttribute(QuotationConstants.QUOTATION_SFUNCTIONTYPE,sFunctionType); 
		
		Connection oConnection = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        MasterDto oMasterDto = new MasterDto();
        
        /* Creating an instance of of DaoFactory & retrieving AttributeSearchDao object */
        DaoFactory oDaoFactory = null;
        /* geting MasterDao Instance from Factory Class*/
        MasterDao oMasterDao = null;
        String sQuery =null;
        /*Storing SearchMasterResults Set*/
        ArrayList alSearchMasterResults = null;


        HttpSession session;
        MasterForm oMasterForm = (MasterForm)actionForm;
        if (oMasterForm == null){
        	oMasterForm = new MasterForm();
        }
        session = request.getSession();
        try {
         
        	    oDBUtility = new DBUtility();
                    
               /* Creating an instance of of DaoFactory & retrieving AttributeSearchDao object */
                oDaoFactory = new DaoFactory();
               /* Retrieving the database connection using DBUtility.getDBConnection method */
                oConnection = oDBUtility.getDBConnection();
               /* geting MasterDao Instance from Factory Class*/
                oMasterDao = oDaoFactory.getMasterDao();
                /* Coping the properties from ActionForm object to Dto Object*/
                PropertyUtils.copyProperties( oMasterDto, oMasterForm);
                /*Geting the Qruey from Session */
                sQuery = "" + session.getAttribute(QuotationConstants.QUOTATION_SEARCH_QUERY);
                alSearchMasterResults = new ArrayList();
                alSearchMasterResults = oMasterDao.getMasterList(oConnection,oMasterDto,sQuery);
                
                if (alSearchMasterResults != null) {
                    request.setAttribute(QuotationConstants.QUOTATION_LIST_RESULTSLIST,alSearchMasterResults);
                    request.setAttribute(QuotationConstants.QUOTATION_LIST_LISTSIZE,alSearchMasterResults.size());   	
                }
            }
        catch (Exception e){
        	  /*
             * Logging any exception that raised during this activity in log files using Log4j
             */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "printOrExportMasterResults", ExceptionUtility.getStackTraceAsString(e));
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
      	
        }
        finally{
        	
        	if(oConnection != null ) oDBUtility.releaseResources(oConnection);
        	
        }
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
       /*
        *For wording the request  
        */
        if (sFunctionType.equalsIgnoreCase(QuotationConstants.QUOTATION_FORWARD_EXPORT)){
			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_EXPORT);
		}else if (sFunctionType.equalsIgnoreCase(QuotationConstants.QUOTATION_FORWARD_PRINT)){
			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_PRINT);
		}else{
			request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_PROB__FECTCHREC);
       	 	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
		}
    }
	/**
	 * This method gets the data for Master list Diplay Page
	 * @param actionMapping ActionMapping object to redirect the user to next page depending on the activity
     * @param actionForm ActionForm object to handle the form elements
     * @param request HttpServletRequest object to handle request operations
     * @param response HttpServletResponse object to handle response operations
     * @return returns ActionForward depending on which user is redirected to next page
     * @throws Exception if any error occurs while performing database operations, etc
     * @author 100006718
     *  Date: August 26, 2008
	 */
	
	public ActionForward viewMasterList(ActionMapping actionMapping,
            ActionForm actionForm,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception 
    {
		
		String sMethodName = "viewMasterList";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		String sPath = QuotationConstants.QUOTATION_FORWARD_SUCCESS ;
		
		Connection oConnection = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        MastersDataDto oMastersDataDto = new MastersDataDto();
        //Storing Data In MastersList
        ArrayList alMasterList = null;
        //MasterDao get Interfaces List
        MasterDao oMasterDao = null;
      
        MastersDataForm oMastersDataForm = (MastersDataForm)actionForm;
        if (oMastersDataForm == null){
        	oMastersDataForm = new MastersDataForm();
        }
        
        setRolesToRequest(request);
      
        try {
        		alMasterList = new ArrayList();
        		   
               /* Instantiating the DBUtility object */
                oDBUtility = new DBUtility();
                    
               /* Creating an instance of of DaoFactory & retrieving AttributeSearchDao object */
                DaoFactory oDaoFactory = new DaoFactory();
               /* Retrieving the database connection using DBUtility.getDBConnection method */
                oConnection = oDBUtility.getDBConnection();
                /*getting MasterDao Instance from Factory Class*/
                oMasterDao = oDaoFactory.getMasterDao();
                /*cpoing properties from Action From object to Dto Class*/
                PropertyUtils.copyProperties( oMastersDataDto, oMastersDataForm);
                
                alMasterList = oMasterDao.getMastersList(oConnection);
                if (alMasterList != null) {

                    request.setAttribute(QuotationConstants.QUOTATION_LIST_RESULTSLIST,
                    		alMasterList);
                    
                    request.setAttribute(QuotationConstants.QUOTATION_LIST_LISTSIZE,
                                               new Integer(alMasterList.size())); 
                }
            }  catch (Exception e){
        	  /*
             * Logging any exception that raised during this activity in log files using Log4j
             */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "viewMasterList", ExceptionUtility.getStackTraceAsString(e));
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
      	
        }
        finally{
        	
        	if(oConnection != null ) oDBUtility.releaseResources(oConnection);
        	
        }
    	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");   
        return actionMapping.findForward(sPath);
    }
	/**
	 * This method gets the information for a perticuler Master
	 * @param actionMapping ActionMapping object to redirect the user to next page depending on the activity
     * @param actionForm ActionForm object to handle the form elements
     * @param request HttpServletRequest object to handle request operations
     * @param response HttpServletResponse object to handle response operations
     * @return returns ActionForward depending on which user is redirected to next page
     * @throws Exception if any error occurs while performing database operations, etc
     * @author 100006718
     *  Date: August 26, 2008
	 */
    public ActionForward viewMaster(ActionMapping actionMapping,
            ActionForm actionForm,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
    	
    	String sMethodName = "viewMaster";
    	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
    	
    	String sPath = QuotationConstants.QUOTATION_FORWARD_VIEW ;
    
    	Connection oConnection = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
       
        MasterDto oMasterDto = new MasterDto();
        MasterDao oMasterDao =null;
        MasterForm oMasterForm = (MasterForm)actionForm;
        if (oMasterForm == null)
        {
        	oMasterForm = new MasterForm();
        }
        /*Saving Request into a Token*/
        saveToken(request);
        
        setRolesToRequest(request);
        
        if(oMasterForm.getMasterId()!=null && !oMasterForm.getMasterId().trim().equalsIgnoreCase(""))
	        	request.setAttribute(QuotationConstants.QUOTATION_SPAGE,QuotationConstants.QUOTATION_EDITMASTER);
		else
        		request.setAttribute(QuotationConstants.QUOTATION_SPAGE,QuotationConstants.QUOTATION_ADDMASTER);
        try
        {
         
                /* Instantiating the DBUtility object */
                 oDBUtility = new DBUtility();
                 /* Creating an instance of of DaoFactory & retrieving AttributeSearchDao object */
                 DaoFactory oDaoFactory = new DaoFactory();
                 /* Retrieving the database connection using DBUtility.getDBConnection method */
                 oConnection = oDBUtility.getDBConnection();
                 //Getting Master Dao Factroy
                 oMasterDao = oDaoFactory.getMasterDao();
                 PropertyUtils.copyProperties( oMasterDto, oMasterForm);
                 oMasterDto = oMasterDao.getMasterDetails(oConnection,oMasterDto);
                 oMasterDto = oMasterDto == null ? new MasterDto():oMasterDto;         
                 PropertyUtils.copyProperties(oMasterForm, oMasterDto);
            
        }
        catch(Exception e)
        {
        	/*
             * Logging any exception that raised during this activity in log files using Log4j
             */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "viewMaster", ExceptionUtility.getStackTraceAsString(e));
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        	
        }
        finally
        {
        	if(oConnection != null ) oDBUtility.releaseResources(oConnection);
        	
        }
    	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
       return actionMapping.findForward(sPath);
       
   } 
    /**
	 * This method delete the information about selected Master
	 * @param actionMapping ActionMapping object to redirect the user to next page depending on the activity
     * @param actionForm ActionForm object to handle the form elements
     * @param request HttpServletRequest object to handle request operations
     * @param response HttpServletResponse object to handle response operations
     * @return returns ActionForward depending on which user is redirected to next page
     * @throws Exception if any error occurs while performing database operations, etc
     * @author 100006718
     *  Date: August 26, 2008
	 */
    public ActionForward deleteMaster(ActionMapping actionMapping,
            ActionForm actionForm,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
    	
    	String sMethodName = "deleteMaster";
    	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
    	
    	String sPath = QuotationConstants.QUOTATION_FORWARD_CONFIRM ;
    	String sMsg = "";
    	Connection oConnection = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        MasterDto oMasterDto = new MasterDto();
        DaoFactory oDaoFactory=null;
        
        MasterForm oMasterForm = (MasterForm)actionForm;
      
     
        if (oMasterForm == null)
        	oMasterForm = new MasterForm();
     
        PropertyUtils.copyProperties( oMasterDto, oMasterForm);
        
        setRolesToRequest(request);
       
        try
        {
        	if(isTokenValid(request)) 
        	{
        		resetToken(request);

                /* Instantiating the DBUtility object */
                 oDBUtility = new DBUtility();
                 /* Creating an instance of of DaoFactory & retrieving AttributeSearchDao object */
                 oDaoFactory = new DaoFactory();
                 /* Retrieving the database connection using DBUtility.getDBConnection method */
                 oConnection = oDBUtility.getDBConnection();
                 MasterDao oMasterDao = oDaoFactory.getMasterDao();
    	         sMsg = oMasterDao.deleteMaster(oConnection, oMasterDto);
    	         request.setAttribute(QuotationConstants.QUOTATION_SMSG,sMsg);
    	         request.setAttribute(QuotationConstants.QUOTATION_SPAGE,QuotationConstants.QUOTATION_MASTER);
    	          

    	         request.setAttribute(QuotationConstants.QUOTATION_ADMIN_SUCCESSMESSAGE,sMsg);
    	         request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_URL,QuotationConstants.QUOTATION_VIEWMASTERS_URL); 
        	}
        	else
        	{
        		
        		   request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE,QuotationConstants.QUOTATION_INVALIDTOKENMESSAGE);
                   
                   /* Forwarding request to Error page */
                   return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
               
        		
        	}
        }
        catch (SQLException se) {
        	/*
        	* if the SQL Exception is Unique Constrain error then Location won't be delete and it'll return the SQL Exception .
        	*/
        	LoggerUtility.log("DEBUG", this.getClass().getName(), "Location delete Result Error Code .", String.valueOf(se.getErrorCode()));
        	/*
        	* if the Error is 2292 , then the forward to Success page and dispaly the message as location couldn't be deleted
        	*/
        	
        	
        	if ( se.getErrorCode() == 2292 ) {
        	request.setAttribute(QuotationConstants.QUOTATION_ADMIN_HEADERTEXT,"");
        	request.setAttribute(QuotationConstants.QUOTATION_ADMIN_SUCCESSMESSAGE,QuotationConstants.QUOTATION_REC_DELFAIL);
        	request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_URL, QuotationConstants.QUOTATION_VIEWMASTERS_URL); 
        	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);
        	}
        }

        catch(Exception e)
        {
        	/*
             * Logging any exception that raised during this activity in log files using Log4j
             */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "deleteMaster", ExceptionUtility.getStackTraceAsString(e));
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        	
        }
        finally
        {
        	if(oConnection != null ) oDBUtility.releaseResources(oConnection);
        	
        }
    	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
       return actionMapping.findForward(sPath);
	}
    /**
	 * This method inserts or Updates the information about selected Models
	 * @param actionMapping ActionMapping object to redirect the user to next page depending on the activity
     * @param actionForm ActionForm object to handle the form elements
     * @param request HttpServletRequest object to handle request operations
     * @param response HttpServletResponse object to handle response operations
     * @return returns ActionForward depending on which user is redirected to next page
     * @throws Exception if any error occurs while performing database operations, etc
     * @author 100006718
     *  Date: August 26, 2008
	 */
	public ActionForward addorUpdateMaster(ActionMapping actionMapping,
            ActionForm actionForm,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
    	String sMethodName="addorUpdateMaster";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
    	String sPath = QuotationConstants.QUOTATION_FORWARD_CONFIRM ;
    	String sMsg = "";
    	Connection oConnection = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        UserDto oUserDto = null;
        UserAction oUserAction=null;
        DaoFactory oDaoFactory=null;
        MasterDao oMasterDao =null;
        MasterDto oMasterDto = new MasterDto();
        String sExistingMaster=null;
       
        MasterForm oMasterForm = (MasterForm)actionForm;
        if (oMasterForm == null)
        	oMasterForm = new MasterForm();
        PropertyUtils.copyProperties( oMasterDto, oMasterForm);
        
        setRolesToRequest(request);
       
        try
        {      
        	if(isTokenValid(request)) 
        	{
        		resetToken(request);

        		/* Instantiating the UserAction object */
            	oUserAction = new UserAction();
        
                /* Instantiating the DBUtility object */
                 oDBUtility = new DBUtility();
                 /* Creating an instance of of DaoFactory & retrieving AttributeSearchDao object */
                 oDaoFactory = new DaoFactory();
                 /* Retrieving the database connection using DBUtility.getDBConnection method */
                 oConnection = oDBUtility.getDBConnection();
                 /* Retrieving the logged-in user information*/
                 oUserDto = oUserAction.getUserInfoForLoginUser(request, oConnection);
                 oMasterDao = oDaoFactory.getMasterDao();
                 sExistingMaster=CommonUtility.getStringParameter(request, QuotationConstants.QUOTATION_EXISMASTER);
                 oMasterDto.setExistingMaster(sExistingMaster);
    	         sMsg = oMasterDao.addOrUpdateMaster(oConnection, oMasterDto,oUserDto);
    	         request.setAttribute(QuotationConstants.QUOTATION_SMSG,sMsg);

    	         request.setAttribute(QuotationConstants.QUOTATION_ADMIN_SUCCESSMESSAGE,sMsg);
    	         request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_URL,QuotationConstants.QUOTATION_SEARCHMASTERS_URL);
        	}
        	else
        	{
        		
        		   request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE,QuotationConstants.QUOTATION_INVALIDTOKENMESSAGE);
                   
                   /* Forwarding request to Error page */
                   return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
               
        		
        	}
        		
           
        }
        catch (SQLException se) {
        	/*
        	* if the SQL Exception is Unique Constrain error then Location won't be delete and it'll return the SQL Exception .
        	*/
        	LoggerUtility.log("DEBUG", this.getClass().getName(), "Location delete Result Error Code .", String.valueOf(se.getErrorCode()));
        	/*
        	* if the Error is 2292 , then the forward to Success page and dispaly the message as location couldn't be deleted
        	*/
        	
        
        	if ( se.getErrorCode() == 1 ) {
        	request.setAttribute(QuotationConstants.QUOTATION_ADMIN_HEADERTEXT,"");
        	request.setAttribute(QuotationConstants.QUOTATION_ADMIN_SUCCESSMESSAGE,QuotationConstants.QUOTATION_MASTER_EXISTS);
        	request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_URL, QuotationConstants.QUOTATION_VIEWMASTERS_URL); 
        	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);
        	}
        }
        catch(Exception e)
        {
        	/*
             * Logging any exception that raised during this activity in log files using Log4j
             */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "AddorUpdateMaster", ExceptionUtility.getStackTraceAsString(e));
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        	
        }
        finally
        {
        	if(oConnection != null ) oDBUtility.releaseResources(oConnection);
        	
        }
    	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
       return actionMapping.findForward(sPath);
       
   }    
	/**
	 * This method gets data list for  selected master
	 * @param actionMapping ActionMapping object to redirect the user to next page depending on the activity
     * @param actionForm ActionForm object to handle the form elements
     * @param request HttpServletRequest object to handle request operations
     * @param response HttpServletResponse object to handle response operations
     * @return returns ActionForward depending on which user is redirected to next page
     * @throws Exception if any error occurs while performing database operations, etc
     * @author 100006718
     *  Date: August 26, 2008
	 */
	
	public ActionForward viewSearchMastersData(ActionMapping actionMapping,
            ActionForm actionForm,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception 
    {
		
		String sMethodName = "viewSearchMastersData";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		String sPath = QuotationConstants.QUOTATION_FORWARD_DATASUCCESS ;
		
		Connection oConnection = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        Hashtable htSearchMasterDataList =null; //Object of Hashtable to store different values that are retrieved using searchEmployeeDto.getAttributeSearchResults method
        ListModel oListModel =null;
        DaoFactory oDaoFactory =null;
        MasterDao oMasterDao=null;
        MastersDataDto oMastersDataDto = new MastersDataDto();
        HttpSession session;
        MastersDataForm oMastersDataForm = (MastersDataForm)actionForm;
        if (oMastersDataForm == null){
        	oMastersDataForm = new MastersDataForm();
        }
        
        /*Saving Request into a Token*/
        saveToken(request);
        setRolesToRequest(request);
        
        session = request.getSession();
        try {
         
        	    /*
        		 * Object of Hashtable to store different values that are retrieved 
        		 * using searchEmployeeDto.getAttributeSearchResults method
        		 */
        		htSearchMasterDataList = new Hashtable();
        		/*
                * Object of ListModel class to store the values necessary to display 
                * the results in attribute search results page with pagination
                */
                oListModel = new ListModel();
           	
           	    /*
                * Setting values for ListModel that are necessary to retrieve rows from the database
                * and display them in search results page. It holds information like
                * --> number of rows to be displayed at once in the page
                * --> column on which rows have to be sorted
                * --> order in which rows have to be sorted
                * --> set of rows to be retrieved depending on the page number selected for viewing
                * Details of parameters:
                * 1. Request object
                * 2. Sort column index
                * 3. Sort order
                * 4. Search form
                * 5. Field that stores the invoke method name
                * 8. Invoke method name
                */
               oListModel.setParams(request, 
                                      "1", 
                                      ListModel.ASCENDING, 
                                      "MastersDataForm", 
                                      "MastersDataForm.invoke", 
                                      "viewSearchMastersData");
               
               /* Instantiating the DBUtility object */
                oDBUtility = new DBUtility();
                    
               /* Creating an instance of of DaoFactory & retrieving AttributeSearchDao object */
               oDaoFactory = new DaoFactory();
               /* Retrieving the database connection using DBUtility.getDBConnection method */
                oConnection = oDBUtility.getDBConnection();
                oMasterDao = oDaoFactory.getMasterDao();
                PropertyUtils.copyProperties( oMastersDataDto, oMastersDataForm);
                
                htSearchMasterDataList = oMasterDao.getMastersDataList(oConnection,oMastersDataDto,oListModel);
                if (htSearchMasterDataList != null) {
                    
                    //oSearchDto.setSearchResultsList((ArrayList)htSearchResultsList
                        //              .get(FracasConstants.LIST_RESULTSLIST));

                   // request.setAttribute("searchDto", oSearchDto);                 
                    
                    request.setAttribute(QuotationConstants.QUOTATION_LIST_RESULTSLIST,
                                              (ArrayList)htSearchMasterDataList
                                              .get(QuotationConstants.QUOTATION_LIST_RESULTSLIST));
                    request.setAttribute(QuotationConstants.QUOTATION_LIST_LISTMODELCLASS,
                    		htSearchMasterDataList
                                               .get(QuotationConstants.QUOTATION_LIST_LISTMODELCLASS));
                    request.setAttribute(QuotationConstants.QUOTATION_LIST_LISTSIZE,
                                               new Integer(((ArrayList)htSearchMasterDataList
                                               .get(QuotationConstants.QUOTATION_LIST_RESULTSLIST))
                                           .size()));   	
                   
                    session.setAttribute(QuotationConstants.QUOTATION_SEARCH_QUERY,(String)htSearchMasterDataList.get(QuotationConstants.QUOTATION_SEARCH_QUERY));
                }
                PropertyUtils.copyProperties(oMastersDataForm,oMastersDataDto );
              
            }
                    
      
        catch (Exception e){
        	  /*
             * Logging any exception that raised during this activity in log files using Log4j
             */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "viewSearchMasterList", ExceptionUtility.getStackTraceAsString(e));
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
      	
        }
        finally{
        	
        	if(oConnection != null ) oDBUtility.releaseResources(oConnection);
        	
        }
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
        return actionMapping.findForward(sPath);
    }
	
	/**
	 * This method get the information of selected Engineering Data
	 * @param actionMapping ActionMapping object to redirect the user to next page depending on the activity
     * @param actionForm ActionForm object to handle the form elements
     * @param request HttpServletRequest object to handle request operations
     * @param response HttpServletResponse object to handle response operations
     * @return returns ActionForward depending on which user is redirected to next page
     * @throws Exception if any error occurs while performing database operations, etc
     * @author 100006718
     *  Date: August 26, 2008
	 */
    public ActionForward viewMastersData(ActionMapping actionMapping,
            ActionForm actionForm,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
    	
    	String sMethodName="viewMastersData";
    	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");

    	String sPath = QuotationConstants.QUOTATION_FORWARD_ADDMASTERDATA;
        MastersDataDto oMastersDataDto = new MastersDataDto();
        MastersDataForm oMastersDataForm = (MastersDataForm)actionForm;
        DBUtility oDBUtility=null;
        Connection oConnection = null;
        DaoFactory oDaoFactory =null;
        MasterDao oMasterDao=null;
        String sMasterId =null,sMasterName=null;
       
        if (oMastersDataForm == null){
        	
        	oMastersDataForm = new MastersDataForm();
        }
        /*Saving Request into a Token*/
        saveToken(request);
        setRolesToRequest(request);
        if(oMastersDataForm.getEngineeringId()!=null && !oMastersDataForm.getEngineeringId().trim().equalsIgnoreCase(""))

        	request.setAttribute(QuotationConstants.QUOTATION_SPAGE, QuotationConstants.QUOTATION_EDITMASTERDATA);
		else
			request.setAttribute(QuotationConstants.QUOTATION_SPAGE, QuotationConstants.QUOTATION_ADDMASTERDATA);
        try
        {
         
                /* Instantiating the DBUtility object */
                 oDBUtility = new DBUtility();
                 /* Creating an instance of of DaoFactory & retrieving AttributeSearchDao object */
                 oDaoFactory = new DaoFactory();
                 /* Retrieving the database connection using DBUtility.getDBConnection method */
                 oConnection = oDBUtility.getDBConnection();
                 oMasterDao = oDaoFactory.getMasterDao();
                 sMasterId = oMastersDataForm.getMasterId();
                 sMasterName = oMastersDataForm.getMaster();
                 
                /* Coping the properties from Action Form oblect to Dto Object*/ 
                 PropertyUtils.copyProperties( oMastersDataDto, oMastersDataForm);
                 
                 oMastersDataDto = oMasterDao.getMastersDataDetails(oConnection,oMastersDataDto);
                 oMastersDataDto = oMastersDataDto == null ? new MastersDataDto():oMastersDataDto;         
                 PropertyUtils.copyProperties(oMastersDataForm, oMastersDataDto);
                 oMastersDataForm.setMasterId(sMasterId);
                 oMastersDataForm.setMaster(sMasterName);
            
                 
        }
        catch(Exception e)
        {
        	/*
             * Logging any exception that raised during this activity in log files using Log4j
             */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "viewMastersData", ExceptionUtility.getStackTraceAsString(e));
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        	
        }
        finally
        {
        	if(oConnection != null ) oDBUtility.releaseResources(oConnection);
        	
        }
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
       return actionMapping.findForward(sPath);
       
   } 
    
    /**
	 * This method inserts or update the information about selected Engineering Data
	 * @param actionMapping ActionMapping object to redirect the user to next page depending on the activity
     * @param actionForm ActionForm object to handle the form elements
     * @param request HttpServletRequest object to handle request operations
     * @param response HttpServletResponse object to handle response operations
     * @return returns ActionForward depending on which user is redirected to next page
     * @throws Exception if any error occurs while performing database operations, etc
     * @author 100006718
     *  Date: August 26, 2008
	 */
	public ActionForward addorUpdateMasterData(ActionMapping actionMapping,
            ActionForm actionForm,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
		
		String sMethodName="addorUpdateMasterData";
    	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
    	
    	String sPath = QuotationConstants.QUOTATION_FORWARD_CONFIRM ;
    	String sEditMasterValue=CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_EDITMASTERVALUE);
    	String sMsg = "";
    	Connection oConnection = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        UserDto oUserDto = null;
        UserAction oUserAction =null;
        DaoFactory oDaoFactory=null;
        MasterDao oMasterDao=null;
        MastersDataDto oMastersDataDto = new MastersDataDto();
       
        MastersDataForm oMastersDataForm = (MastersDataForm)actionForm;
        if (oMastersDataForm == null)
        	oMastersDataForm = new MastersDataForm();
        PropertyUtils.copyProperties( oMastersDataDto, oMastersDataForm);
        setRolesToRequest(request);
         
        
       
        try
        {	
        	if(isTokenValid(request)) 
        	{
        		resetToken(request);
        	  	/* Instantiating the UserAction object */
            	oUserAction = new UserAction();
        
                /* Instantiating the DBUtility object */
                 oDBUtility = new DBUtility();
                 /* Creating an instance of of DaoFactory & retrieving AttributeSearchDao object */
                 oDaoFactory = new DaoFactory();
                 /* Retrieving the database connection using DBUtility.getDBConnection method */
                 oConnection = oDBUtility.getDBConnection();
                 /* Retrieving the logged-in user information*/
                 oUserDto = oUserAction.getUserInfoForLoginUser(request, oConnection);
                 oMasterDao = oDaoFactory.getMasterDao();
                 oMastersDataDto.setEditmastervalue(sEditMasterValue);
                 
    	         sMsg = oMasterDao.addOrUpdateMasterData(oConnection, oMastersDataDto,oUserDto);
    	         request.setAttribute(QuotationConstants.QUOTATION_SMSG,sMsg);
    	      
    	         request.setAttribute(QuotationConstants.QUOTATION_ADMIN_SUCCESSMESSAGE,sMsg);
    	         request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_URL,QuotationConstants.QUOTATION_MASTERID_URL+oMastersDataDto.getMasterId()+QuotationConstants.QUOTATION_MASTER_URL+oMastersDataDto.getMaster()+"");
    	         
        	}
        	else
        	{
        		
        		   request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE,QuotationConstants.QUOTATION_INVALIDTOKENMESSAGE);
                   
                   /* Forwarding request to Error page */
                   return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
               
        		
        	}     
        }
        catch(Exception e)
        {
        	/*
             * Logging any exception that raised during this activity in log files using Log4j
             */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "AddorUpdateMaster", ExceptionUtility.getStackTraceAsString(e));
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        	
        }
        finally
        {
        	if(oConnection != null ) oDBUtility.releaseResources(oConnection);
        	
        }
    	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
       return actionMapping.findForward(sPath);
       
   } 
	 /**
	 * This method delete the information about selected Engineering Data
	 * @param actionMapping ActionMapping object to redirect the user to next page depending on the activity
     * @param actionForm ActionForm object to handle the form elements
     * @param request HttpServletRequest object to handle request operations
     * @param response HttpServletResponse object to handle response operations
     * @return returns ActionForward depending on which user is redirected to next page
     * @throws Exception if any error occurs while performing database operations, etc
     * @author 100006718
     *  Date: August 26, 2008
	 */
    public ActionForward deleteMasterData(ActionMapping actionMapping,
            ActionForm actionForm,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
    	
    	String sMethodName = "deleteMaster";
    	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
    	
    	String sPath = QuotationConstants.QUOTATION_FORWARD_CONFIRM ;
    	String sMsg = "";
    	Connection oConnection = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        DaoFactory oDaoFactory =null;
        MasterDao oMasterDao =null;
        MastersDataDto oMastersDataDto = new MastersDataDto();
        
        MastersDataForm oMastersDataForm = (MastersDataForm)actionForm;
      
     
        if (oMastersDataForm == null)
        	oMastersDataForm = new MastersDataForm();
     
        PropertyUtils.copyProperties( oMastersDataDto, oMastersDataForm);
        setRolesToRequest(request);
        try
        {
        	if(isTokenValid(request)) 
        	{
        		resetToken(request);
                /* Instantiating the DBUtility object */
                 oDBUtility = new DBUtility();
                 /* Creating an instance of of DaoFactory & retrieving AttributeSearchDao object */
                 oDaoFactory = new DaoFactory();
                 /* Retrieving the database connection using DBUtility.getDBConnection method */
                 oConnection = oDBUtility.getDBConnection();
                 oMasterDao = oDaoFactory.getMasterDao();
    	         sMsg = oMasterDao.deleteMasterData(oConnection, oMastersDataDto);
    	         request.setAttribute(QuotationConstants.QUOTATION_SMSG,sMsg);
    	         request.setAttribute(QuotationConstants.QUOTATION_SPAGE,QuotationConstants.QUOTATION_MASTER);

    	         request.setAttribute(QuotationConstants.QUOTATION_ADMIN_SUCCESSMESSAGE,sMsg);
    	         request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_URL,QuotationConstants.QUOTATION_MASTERID_URL+oMastersDataDto.getMasterId()+QuotationConstants.QUOTATION_MASTER_URL+oMastersDataDto.getMaster()+"");
    	     
        	}
        	else
        	{
        		
        		   request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE,QuotationConstants.QUOTATION_INVALIDTOKENMESSAGE);
                   
                   /* Forwarding request to Error page */
                   return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
               
        		
        	}         
        }
        catch (SQLException se) {
        	/*
        	* if the SQL Exception is Unique Constrain error then Location won't be delete and it'll return the SQL Exception .
        	*/
        	LoggerUtility.log("DEBUG", this.getClass().getName(), "Location delete Result Error Code .", String.valueOf(se.getErrorCode()));
        	/*
        	* if the Error is 2292 , then the forward to Success page and dispaly the message as location couldn't be deleted
        	*/
        	if ( se.getErrorCode() == 2292 ) {

        	request.setAttribute(QuotationConstants.QUOTATION_ADMIN_SUCCESSMESSAGE,QuotationConstants.QUOTATION_REC_DELFAIL);
        	request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_URL, QuotationConstants.QUOTATION_VIEWMASTERS_URL); 
        	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_SUCCESS);
        	}
        }

        catch(Exception e)
        {
        	/*
             * Logging any exception that raised during this activity in log files using Log4j
             */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "deleteMaster", ExceptionUtility.getStackTraceAsString(e));
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        	
        }
        finally
        {
        	if(oConnection != null ) oDBUtility.releaseResources(oConnection);
        	
        }
    	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
       return actionMapping.findForward(sPath);
	}	/**
	 * This method get the list of master's Data for Print or Export to Excel
	 * @param actionMapping ActionMapping object to redirect the user to next page depending on the activity
     * @param actionForm ActionForm object to handle the form elemets
     * @param request HttpServletRequest object to handle request operations
     * @param response HttpServletResponse object to handle response operations
     * @return returns ActionForward depending on which user is redirected to next page
     * @throws Exception if any error occurs while performing database operations, etc
     * @author 100006718
     *  Date: October 13, 2008
	 */
	
	public ActionForward printOrExportMasterData(ActionMapping actionMapping,
            ActionForm actionForm,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception 
    {
		
		String sMethodName = "printOrExportMasterData";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		/* Getting the funtion Type from Request object*/
		String sFunctionType =CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_FUNCTIONTYPES);
		if (sFunctionType == null || sFunctionType.trim().length() < 1){
			sFunctionType = QuotationConstants.QUOTATION_SNONE;
		}
		request.setAttribute(QuotationConstants.QUOTATION_SFUNCTIONTYPE,sFunctionType);
		
		Connection oConnection = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        DaoFactory oDaoFactory=null;
        MasterDao oMasterDao=null;
        String sQuery=null;
        ArrayList alSearchMasterDataResults =null;
        MastersDataDto oMastersDataDto = new MastersDataDto();
        HttpSession session;
        MastersDataForm oMastersDataForm = (MastersDataForm)actionForm;
        if (oMastersDataForm == null){
        	oMastersDataForm = new MastersDataForm();
        }
        session = request.getSession();
        try {
         
        	    oDBUtility = new DBUtility();
                    
               /* Creating an instance of of DaoFactory & retrieving AttributeSearchDao object */
                oDaoFactory = new DaoFactory();
               /* Retrieving the database connection using DBUtility.getDBConnection method */
                oConnection = oDBUtility.getDBConnection();
               /* geting MasterDao Instance from Factory Class*/
                oMasterDao = oDaoFactory.getMasterDao();
                /*getting Master Details from Request*/
                oMastersDataForm.setMasterId(CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_MASTERID));
                oMastersDataForm.setMaster(CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_MASTER.toLowerCase()));
                /* Coping the properties from ActionForm object to Dto Object*/
                PropertyUtils.copyProperties( oMastersDataDto, oMastersDataForm);
                /*Geting the Qruey from Session */
                sQuery = "" + session.getAttribute(QuotationConstants.QUOTATION_SEARCH_QUERY);
                
                alSearchMasterDataResults = new ArrayList();
                alSearchMasterDataResults = oMasterDao.getMasterDataList(oConnection,oMastersDataDto,sQuery);
                
                if (alSearchMasterDataResults != null) {
                    request.setAttribute(QuotationConstants.QUOTATION_LIST_RESULTSLIST,alSearchMasterDataResults);
                    request.setAttribute(QuotationConstants.QUOTATION_LIST_LISTSIZE,alSearchMasterDataResults.size());   	
                }
            }
        catch (Exception e){
        	  /*
             * Logging any exception that raised during this activity in log files using Log4j
             */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "printOrExportMasterResults", ExceptionUtility.getStackTraceAsString(e));
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
      	
        }
        finally{
        	
        	if(oConnection != null ) oDBUtility.releaseResources(oConnection);
        	
        }
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
       /*
        *For wording the request  
        */
        if (sFunctionType.equalsIgnoreCase(QuotationConstants.QUOTATION_FORWARD_EXPORT)){
			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_EXPORT);
		}else if (sFunctionType.equalsIgnoreCase(QuotationConstants.QUOTATION_FORWARD_PRINT)){
			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_PRINT);
		}else{
			request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, 
                    QuotationConstants.QUOTATION_PROB__FECTCHREC);
       	 	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
		}
    }
	
	public ActionForward updateMastersDataStatus(ActionMapping actionMapping,
			ActionForm actionForm,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception {  

	  	// Method name Set for log file usage ;
  		String sMethodname = "updateMastersDataStatus";
  		String sClassName = this.getClass().getName();
  		LoggerUtility.log("INFO", sClassName, sMethodname, "START");
		
	   /*
        * Setting user's information in request, using the header 
        * information received through request
        */
  		setRolesToRequest(request);
	  	
        // Object of DBUtility class to handle DB connection objects
		DBUtility oDBUtility = new DBUtility();
		
		// Connection object to store the database connection
		Connection conn = null;
		
		UserDto oUserDto = null;
		/* StringBuffer to hold the XML */
		StringBuffer sbBuiltXML = null;
		/* String that holds the targetObjects values that we get from the request */
		String sTargetObjects = null;
		String sAction = null;
		int iengneeringId = 0;
		String sMasterName=null;
		String sMasterId=null;
		
		boolean isUpdated = false ;
		DaoFactory oDaoFactory =null;
		MasterDao oMasterDao =null;
		MastersDataDto oMastersDataDto=null;
		UserAction oUserAction=null;


		
		try {			
    		// setting content type to create xml  
    		response.setContentType("text/xml;charset=utf-8");
    		PrintWriter out = response.getWriter();
    		
    		
    		/* StringBuffer to hold the XML */
    		sbBuiltXML = new StringBuffer();
    		
    		/* Instantiating the DBUtility object */
 			oDBUtility = new DBUtility();
 			/* Creating an instance of of DaoFactory  */
 			oDaoFactory = new DaoFactory();
 			oMasterDao = oDaoFactory.getMasterDao();
 			/* Retrieving the database connection using DBUtility.getDBConnection method */
 			conn = oDBUtility.getDBConnection();
 			oMastersDataDto= new MastersDataDto();
 			
 			oUserAction = new UserAction();
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
             
            if (oUserDto == null){
          	   oUserDto = new UserDto();
            }
    		
            iengneeringId = CommonUtility.getIntParameter(request, QuotationConstants.QUOTATION_ENGID);
            sAction = CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_ACTION );
            sMasterName=CommonUtility.getStringParameter(request, QuotationConstants.QUOTATION_MASTERNAME);
            sMasterId=CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_MASTERID );
    		sTargetObjects = CommonUtility.getStringParameter(request, QuotationConstants.QUOTATION_ADMINLOOKUP_TARGETOBJECTS);
    		
    		oMastersDataDto.setEngineeringId(iengneeringId+"");
    		oMastersDataDto.setStatus(sAction);
    		
    		
    		oDBUtility.setAutoCommitFalse(conn);
    		oMastersDataDto.setMaster(sMasterName);
    		oMastersDataDto.setMasterId(sMasterId);
    		isUpdated = oMasterDao.updateMasterDataStatusValue(conn, oMastersDataDto,oUserDto);
    
    		LoggerUtility.log("INFO", sClassName, sMethodname, "Location Status Updated -- " + isUpdated);
    		if ( isUpdated ) {
    			oDBUtility.commit(conn);
    		} else {
    			oDBUtility.rollback(conn);
    		}
    		oDBUtility.setAutoCommitTrue(conn);
    		
			//Setting the status code 200 
            response.setStatus(200, "Get Options (XML) Successful");
            
            //Creating xml
            sbBuiltXML.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"); 
            sbBuiltXML.append("<result>");
    		sbBuiltXML.append("<params>");
    		sbBuiltXML.append("<objType>statusupdate</objType>");
    		sbBuiltXML.append("<target>" + sTargetObjects + "</target>");
    		sbBuiltXML.append("<updated>" + isUpdated + "</updated>");
    		sbBuiltXML.append("<action>" + sAction + "</action>");
    		sbBuiltXML.append("</params>");			             
            sbBuiltXML.append("</result>");		 
            
            LoggerUtility.log("INFO", sClassName, sMethodname, "sbBuiltXML --> " + sbBuiltXML);
            out.print(sbBuiltXML);
		}catch(Exception e) {
			oDBUtility.rollback(conn);
			
			/*
	           * Logging any exception that raised during this activity in log files using Log4j
	           */
	      		LoggerUtility.log("DEBUG", this.getClass().getName(), "updateMaster's Data status Result.", ExceptionUtility.getStackTraceAsString(e));
	         
	          /* Setting error details to request to display the same in Exception page */
	          	request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
	          	
	          /* Forwarding request to Error page */
	          	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
		} finally {
            
            /*
             * Releasing the resources
             */
			if (oDBUtility != null && conn != null){
				oDBUtility.releaseResources(conn);
			}
        }
		
		LoggerUtility.log("INFO", sClassName, sMethodname, "END");
		return null;
	}
	
	/**
	 * This method retrieves the list for the selected SalesMangerLookup 
	 * @param actionMapping ActionMapping object to redirect the user to next page depending on the activity
     * @param actionForm ActionForm object to handle the form elements
     * @param request HttpServletRequest object to handle request operations
     * @param response HttpServletResponse object to handle response operations
     * @return returns ActionForward depending on which user is redirected to next page
     * @throws Exception if any error occurs while performing database operations, etc
     * @author 100006126
     *  Date: July 9, 2008
	 */
	public ActionForward loadSalesMangerlookup(ActionMapping actionMapping,
			ActionForm actionForm,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception { 		
	
		
		LoggerUtility.log("INFO", this.getClass().getName(), "loadSalesMangerlookup", "retrieveLocationList Start");
		
        // Object of DBUtility class to handle DB connection objects
		DBUtility oDBUtility = new DBUtility();
		
		// Connection object to store the database connection
		Connection conn = null;
		
		ArrayList arlProductLineOptionsList =null;
		StringBuffer sbBuiltXML =null;
		/* String that holds the targetObjects values that we get from the request */
		String sTargetObjects = null;
		String[] sTargetName = null;
		DaoFactory oDaoFactory=null;
		MasterDao oMasterDao=null;
		boolean isIncludeInActive =false;

		
		setRolesToRequest(request);
		try {
			
    		
    		// setting content type to create xml  
    		response.setContentType("text/xml;charset=utf-8");
    		PrintWriter out = response.getWriter();
    		
    		/* Object of ArrayList to hold the retrieved list of options*/
    		arlProductLineOptionsList = new ArrayList();
    	
    		
    		/* StringBuffer to hold the XML */
    		sbBuiltXML = new StringBuffer();
    		
    		
    		/* Creating an instance of of DaoFactory & retrieving EmployeeDao object */
            oDaoFactory = new DaoFactory();    
    		
    		/* Instantiating the DBUtility object */
    		oDBUtility = new DBUtility();
    		
    		/* Retrieving the database connection using DBUtility.getDBConnection method */
    		conn = oDBUtility.getDBConnection();
    		
    		// get the product line objects
    		 oMasterDao = oDaoFactory.getMasterDao();
    		
    		
    		sTargetObjects = CommonUtility.getStringParameter(request, QuotationConstants.APP_ADMINLOOKUP_TARGETOBJECTS);
    		
    		isIncludeInActive = CommonUtility.getBooleanValue(request.getParameter(QuotationConstants.APP_REQUEST_CANINCLUDE_INACTIVE));
    		
    		if (CommonUtility.getStringParameter(request, QuotationConstants.APP_ADMINLOOKUP_SELECTEDOPTION).trim().length() > QuotationConstants.QUOTATION_LITERAL_ZERO){
    	   		    			
    			arlProductLineOptionsList = oMasterDao.getSalemangerList(conn, CommonUtility.getStringParameter(request, QuotationConstants.APP_ADMINLOOKUP_SELECTEDOPTION));
    			    			    			
    		}   
    		
			//  setting the status code 200 
            response.setStatus(200, "Get Options (XML) Successful");
            
            //	creating xml
            sbBuiltXML.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"); 
            sbBuiltXML.append("<result>");
            
            if (arlProductLineOptionsList!=null && arlProductLineOptionsList.size() > QuotationConstants.QUOTATION_LITERAL_ZERO){	

       		 request.setAttribute(QuotationConstants.QUOTATION_SALESMANAGERS_LIST, arlProductLineOptionsList);

            	sbBuiltXML.append(CommonUtility.generateOptionsXML(arlProductLineOptionsList, "", sTargetObjects, 
            			QuotationConstants.APP_ADMINLOOKUP_TARGET_SALESMANAGER));
            } 
            
           
            sbBuiltXML.append("</result>");            
            out.print(sbBuiltXML);         
    		    		
		}catch(Exception e) {
			/*
			 * Logging any exception that raised during this activity in 
			 * log files using Log4j
			 */
			LoggerUtility.log("DEBUG", this.getClass().getName(), "retrieveLocationList", 
					ExceptionUtility.getStackTraceAsString(e));
			
			/*
			 * Setting error details to request to show in the View.
			 */
			request.setAttribute(QuotationConstants.APP_ERRORMESSAGE, 
					ExceptionUtility.getStackTraceAsString(e));
			
			/*
			 * Forwarding request to Error page.
			 */
			return actionMapping.findForward(QuotationConstants.APP_FORWARD_FAILURE);
		} finally {
            
            /*
             * Releasing the resources
             */
			if (oDBUtility != null && conn != null){
				oDBUtility.releaseResources(conn);
			}
        }		
		return null;
	}	
	
	
	/**
	 * This method retrieves the list for the selected SalesMangerLookup 
	 * @param actionMapping ActionMapping object to redirect the user to next page depending on the activity
     * @param actionForm ActionForm object to handle the form elements
     * @param request HttpServletRequest object to handle request operations
     * @param response HttpServletResponse object to handle response operations
     * @return returns ActionForward depending on which user is redirected to next page
     * @throws Exception if any error occurs while performing database operations, etc
     * @author 100006126
     *  Date: July 9, 2008
	 */
	public ActionForward loadCustomerlookup(ActionMapping actionMapping,
			ActionForm actionForm,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception { 
		
		LoggerUtility.log("INFO", this.getClass().getName(), "loadSalesMangerlookup", "retrieveLocationList Start");
		
        // Object of DBUtility class to handle DB connection objects
		DBUtility oDBUtility = new DBUtility();
		
		// Connection object to store the database connection
		Connection conn = null;
		
		ArrayList arlCustomerOptionsList =null;
		StringBuffer sbBuiltXML =null;
		/* String that holds the targetObjects values that we get from the request */
		String sTargetObjects = null;
		String[] sTargetName = null;
		MasterDao oMasterDao=null;
		DaoFactory oDaoFactory=null;

		
		setRolesToRequest(request);
		try {
			
    		
    		// setting content type to create xml  
    		response.setContentType("text/xml;charset=utf-8");
    		PrintWriter out = response.getWriter();
    		
    		/* Object of ArrayList to hold the retrieved list of options*/
    		arlCustomerOptionsList = new ArrayList();
    	
    		
    		/* StringBuffer to hold the XML */
    		sbBuiltXML = new StringBuffer();
    		
    		
    		/* Creating an instance of of DaoFactory & retrieving EmployeeDao object */
             oDaoFactory = new DaoFactory();    
    		
    		/* Instantiating the DBUtility object */
    		oDBUtility = new DBUtility();
    		
    		/* Retrieving the database connection using DBUtility.getDBConnection method */
    		conn = oDBUtility.getDBConnection();
    		
    		// get the product line objects
    		 oMasterDao = oDaoFactory.getMasterDao();
    		
    		
    		sTargetObjects = CommonUtility.getStringParameter(request, QuotationConstants.APP_ADMINLOOKUP_TARGETOBJECTS);
    		
    		//boolean canIncludeInActive = CommonUtility.getBooleanValue(request.getParameter(QuotationConstants.APP_REQUEST_CANINCLUDE_INACTIVE));
    		
    		if (CommonUtility.getStringParameter(request, QuotationConstants.APP_ADMINLOOKUP_SELECTEDOPTION).trim().length() > QuotationConstants.QUOTATION_LITERAL_ZERO){
    	   		    			
    			arlCustomerOptionsList = oMasterDao.getCustomerList(conn, CommonUtility.getStringParameter(request, QuotationConstants.APP_ADMINLOOKUP_SELECTEDOPTION));
    		    
    		}   
    		
			//  setting the status code 200 
            response.setStatus(200, "Get Options (XML) Successful");
            
            //	creating xml
            sbBuiltXML.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"); 
            sbBuiltXML.append("<result>");
            
            if (arlCustomerOptionsList!=null && arlCustomerOptionsList.size() > QuotationConstants.QUOTATION_LITERAL_ZERO){	
            	sbBuiltXML.append(CommonUtility.generateOptionsXML(arlCustomerOptionsList, "", sTargetObjects, 
            			QuotationConstants.APP_ADMINLOOKUP_TARGET_CUSTOMER));
            } 
            
           
            sbBuiltXML.append("</result>");  
            
          
            out.print(sbBuiltXML);         
    		    		
		}catch(Exception e) {
			/*
			 * Logging any exception that raised during this activity in 
			 * log files using Log4j
			 */
			LoggerUtility.log("DEBUG", this.getClass().getName(), "retrieveLocationList", 
					ExceptionUtility.getStackTraceAsString(e));
			
			/*
			 * Setting error details to request to show in the View.
			 */
			request.setAttribute(QuotationConstants.APP_ERRORMESSAGE, 
					ExceptionUtility.getStackTraceAsString(e));
			
			/*
			 * Forwarding request to Error page.
			 */
			return actionMapping.findForward(QuotationConstants.APP_FORWARD_FAILURE);
		} finally {
            
            /*
             * Releasing the resources
             */
			if (oDBUtility != null && conn != null){
				oDBUtility.releaseResources(conn);
			}
        }		
		return null;
	}	

}
