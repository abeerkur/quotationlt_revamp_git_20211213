/**
 * *****************************************************************************
 * Project Name: quotation
 * Document Name: UpdateManagersForm.java
 * Package: in.com.rbc.quotation.admin.form
 * Desc: 
 * *****************************************************************************
 * Author: 100005701 (Suresh)
 * Date: Oct 13, 2008
 * *****************************************************************************
 */
package in.com.rbc.quotation.admin.form;

import java.util.ArrayList;

import in.com.rbc.quotation.common.utility.CommonUtility;

import org.apache.struts.action.ActionForm;

/**
 * @author 100005701 (Suresh Shanmugam)
 *
 */
public class UpdateManagersForm extends ActionForm {
	
	private String invoke ;
	private String operation;

	private String createdById;
	private String lastUpdatedById ;
	private String createdDate ;
	private String lastUpdatedDate ;
	
	
	private String managertype;
	private String cheifExecutiveName ;
	private String cheifExecutiveSSO= "";
	private String designManagerName;
	private String designManagerSSO="";
	
	private String regionalSalesManagerName ;
	private String regionalSalesManagerSSO="";
	private String salesManagerName;
	private String salesManagerSSO="";
	
	
	
	private String cheifExecutiveOldSSO ="";
    private String cheifExecutiveOldName ="";
    private String designManagerOldName ="";
	private String designManagerOldSSO ="";
	
	private String regionalSalesManagerID = "";
	private String oldSalesManagerSSO ="";
	private String newSalesManagerSSO="";
	
	private String oldDesignEngineerSSO ="";
	private String newDesignEngineerSSO="";
	private String designEngineer;
	private String designEngineerSSO="";
	
	private String commercialManagerName;
	private String commercialManagerSSO="";
	private String commercialManagerOldSSO ="";
	private String commercialManagerOldName ="";
	
	
	private ArrayList regionalSalesManagerList = null;
	private ArrayList salesManagerList = null;
	private ArrayList designEngineerList = null;
	private ArrayList commercialManagersList = null;

	
	/**
	 * @return Returns the designEngineerList.
	 */
	public ArrayList getDesignEngineerList() {
		return designEngineerList;
	}
	/**
	 * @param designEngineerList The designEngineerList to set.
	 */
	public void setDesignEngineerList(ArrayList designEngineerList) {
		this.designEngineerList = designEngineerList;
	}
	/**
	 * @return Returns the cheifExecutiveName.
	 */
	public String getCheifExecutiveName() {
		return CommonUtility.replaceNull(this.cheifExecutiveName);
	}
	/**
	 * @param cheifExecutiveName The cheifExecutiveName to set.
	 */
	public void setCheifExecutiveName(String cheifExecutiveName) {
		this.cheifExecutiveName = cheifExecutiveName;
	}
	/**
	 * @return Returns the cheifExecutiveOldSSO.
	 */
	public String getCheifExecutiveOldSSO() {
		return CommonUtility.replaceNull(this.cheifExecutiveOldSSO);
	}
	/**
	 * @param cheifExecutiveOldSSO The cheifExecutiveOldSSO to set.
	 */
	public void setCheifExecutiveOldSSO(String cheifExecutiveOldSSO) {
		this.cheifExecutiveOldSSO = cheifExecutiveOldSSO;
	}
	/**
	 * @return Returns the cheifExecutiveSSO.
	 */
	public String getCheifExecutiveSSO() {
		return CommonUtility.replaceNull(this.cheifExecutiveSSO);
	}
	/**
	 * @param cheifExecutiveSSO The cheifExecutiveSSO to set.
	 */
	public void setCheifExecutiveSSO(String cheifExecutiveSSO) {
		this.cheifExecutiveSSO = cheifExecutiveSSO;
	}
	/**
	 * @return Returns the createdById.
	 */
	public String getCreatedById() {
		return CommonUtility.replaceNull(this.createdById);
	}
	/**
	 * @param createdById The createdById to set.
	 */
	public void setCreatedById(String createdById) {
		this.createdById = createdById;
	}
	/**
	 * @return Returns the createdDate.
	 */
	public String getCreatedDate() {
		return CommonUtility.replaceNull(this.createdDate);
	}
	/**
	 * @param createdDate The createdDate to set.
	 */
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	/**
	 * @return Returns the designManagerName.
	 */
	public String getDesignManagerName() {
		return CommonUtility.replaceNull(this.designManagerName);
	}
	/**
	 * @param designManagerName The designManagerName to set.
	 */
	public void setDesignManagerName(String designManagerName) {
		this.designManagerName = designManagerName;
	}
	/**
	 * @return Returns the designManagerOldSSO.
	 */
	public String getDesignManagerOldSSO() {
		return CommonUtility.replaceNull(this.designManagerOldSSO);
	}
	/**
	 * @param designManagerOldSSO The designManagerOldSSO to set.
	 */
	public void setDesignManagerOldSSO(String designManagerOldSSO) {
		this.designManagerOldSSO = designManagerOldSSO;
	}
	/**
	 * @return Returns the designManagerSSO.
	 */
	public String getDesignManagerSSO() {
		return CommonUtility.replaceNull(this.designManagerSSO);
	}
	/**
	 * @param designManagerSSO The designManagerSSO to set.
	 */
	public void setDesignManagerSSO(String designManagerSSO) {
		this.designManagerSSO = designManagerSSO;
	}
	/**
	 * @return Returns the invoke.
	 */
	public String getInvoke() {
		return CommonUtility.replaceNull(this.invoke);
	}
	/**
	 * @param invoke The invoke to set.
	 */
	public void setInvoke(String invoke) {
		this.invoke = invoke;
	}
	/**
	 * @return Returns the lastUpdatedById.
	 */
	public String getLastUpdatedById() {
		return CommonUtility.replaceNull(this.lastUpdatedById);
	}
	/**
	 * @param lastUpdatedById The lastUpdatedById to set.
	 */
	public void setLastUpdatedById(String lastUpdatedById) {
		this.lastUpdatedById = lastUpdatedById;
	}
	/**
	 * @return Returns the lastUpdatedDate.
	 */
	public String getLastUpdatedDate() {
		return CommonUtility.replaceNull(this.lastUpdatedDate);
	}
	/**
	 * @param lastUpdatedDate The lastUpdatedDate to set.
	 */
	public void setLastUpdatedDate(String lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	/**
	 * @return Returns the managertype.
	 */
	public String getManagertype() {
		return CommonUtility.replaceNull(this.managertype);
	}
	/**
	 * @param managertype The managertype to set.
	 */
	public void setManagertype(String managertype) {
		this.managertype = managertype;
	}
	/**
	 * @return Returns the operation.
	 */
	public String getOperation() {
		return CommonUtility.replaceNull(this.operation);
	}
	/**
	 * @param operation The operation to set.
	 */
	public void setOperation(String operation) {
		this.operation = operation;
	}
	
	/**
	 * @return Returns the regionalSalesManagerID.
	 */
	public String getRegionalSalesManagerID() {
		return CommonUtility.replaceNull(this.regionalSalesManagerID);
	}
	/**
	 * @param regionalSalesManagerID The regionalSalesManagerID to set.
	 */
	public void setRegionalSalesManagerID(String regionalSalesManagerID) {
		this.regionalSalesManagerID = regionalSalesManagerID;
	}
	/**
	 * @return Returns the regionalSalesManagerList.
	 */
	public ArrayList getRegionalSalesManagerList() {
		return this.regionalSalesManagerList;
	}
	/**
	 * @param regionalSalesManagerList The regionalSalesManagerList to set.
	 */
	public void setRegionalSalesManagerList(ArrayList regionalSalesManagerList) {
		this.regionalSalesManagerList = regionalSalesManagerList;
	}
	/**
	 * @return Returns the regionalSalesManagerName.
	 */
	public String getRegionalSalesManagerName() {
		return CommonUtility.replaceNull(this.regionalSalesManagerName);
	}
	/**
	 * @param regionalSalesManagerName The regionalSalesManagerName to set.
	 */
	public void setRegionalSalesManagerName(String regionalSalesManagerName) {
		this.regionalSalesManagerName = regionalSalesManagerName;
	}
	/**
	 * @return Returns the regionalSalesManagerSSO.
	 */
	public String getRegionalSalesManagerSSO() {
		return CommonUtility.replaceNull(this.regionalSalesManagerSSO);
	}
	/**
	 * @param regionalSalesManagerSSO The regionalSalesManagerSSO to set.
	 */
	public void setRegionalSalesManagerSSO(String regionalSalesManagerSSO) {
		this.regionalSalesManagerSSO = regionalSalesManagerSSO;
	}
	/**
	 * @return Returns the salesManagerList.
	 */
	public ArrayList getSalesManagerList() {
		return this.salesManagerList;
	}
	/**
	 * @param salesManagerList The salesManagerList to set.
	 */
	public void setSalesManagerList(ArrayList salesManagerList) {
		this.salesManagerList = salesManagerList;
	}
	/**
	 * @return Returns the salesManagerName.
	 */
	public String getSalesManagerName() {
		return CommonUtility.replaceNull(this.salesManagerName);
	}
	/**
	 * @param salesManagerName The salesManagerName to set.
	 */
	public void setSalesManagerName(String salesManagerName) {
		this.salesManagerName = salesManagerName;
	}
	/**
	 * @return Returns the salesManagerSSO.
	 */
	public String getSalesManagerSSO() {
		return CommonUtility.replaceNull(this.salesManagerSSO);
	}
	/**
	 * @param salesManagerSSO The salesManagerSSO to set.
	 */
	public void setSalesManagerSSO(String salesManagerSSO) {
		this.salesManagerSSO = salesManagerSSO;
	}
	/**
	 * @return Returns the newSalesManagerSSO.
	 */
	public String getNewSalesManagerSSO() {
		return CommonUtility.replaceNull(this.newSalesManagerSSO);
	}
	/**
	 * @param newSalesManagerSSO The newSalesManagerSSO to set.
	 */
	public void setNewSalesManagerSSO(String newSalesManagerSSO) {
		this.newSalesManagerSSO = newSalesManagerSSO;
	}
	/**
	 * @return Returns the oldSalesManagerSSO.
	 */
	public String getOldSalesManagerSSO() {
		return CommonUtility.replaceNull(this.oldSalesManagerSSO);
	}
	/**
	 * @param oldSalesManagerSSO The oldSalesManagerSSO to set.
	 */
	public void setOldSalesManagerSSO(String oldSalesManagerSSO) {
		this.oldSalesManagerSSO = oldSalesManagerSSO;
	}
    /**
     * @return Returns the cheifExecutiveOldName.
     */
    public String getCheifExecutiveOldName() {
        return CommonUtility.replaceNull(this.cheifExecutiveOldName);
    }
    /**
     * @param cheifExecutiveOldName The cheifExecutiveOldName to set.
     */
    public void setCheifExecutiveOldName(String cheifExecutiveOldName) {
        this.cheifExecutiveOldName = cheifExecutiveOldName;
    }
    /**
     * @return Returns the designManagerOldName.
     */
    public String getDesignManagerOldName() {
        return CommonUtility.replaceNull(this.designManagerOldName);
    }
    /**
     * @param designManagerOldName The designManagerOldName to set.
     */
    public void setDesignManagerOldName(String designManagerOldName) {
        this.designManagerOldName = designManagerOldName;
    }
	/**
	 * @return Returns the designEngineer.
	 */
	public String getDesignEngineer() {
		return CommonUtility.replaceNull(designEngineer);
	}
	/**
	 * @param designEngineer The designEngineer to set.
	 */
	public void setDesignEngineer(String designEngineer) {
		this.designEngineer = designEngineer;
	}
	/**
	 * @return Returns the designEngineerSSO.
	 */
	public String getDesignEngineerSSO() {
		return CommonUtility.replaceNull(designEngineerSSO);
	}
	/**
	 * @param designEngineerSSO The designEngineerSSO to set.
	 */
	public void setDesignEngineerSSO(String designEngineerSSO) {
		this.designEngineerSSO = designEngineerSSO;
	}
	/**
	 * @return Returns the newDesignEngineerSSO.
	 */
	public String getNewDesignEngineerSSO() {
		return CommonUtility.replaceNull(newDesignEngineerSSO);
	}
	/**
	 * @param newDesignEngineerSSO The newDesignEngineerSSO to set.
	 */
	public void setNewDesignEngineerSSO(String newDesignEngineerSSO) {
		this.newDesignEngineerSSO = newDesignEngineerSSO;
	}
	/**
	 * @return Returns the oldDesignEngineerSSO.
	 */
	public String getOldDesignEngineerSSO() {
		return CommonUtility.replaceNull(oldDesignEngineerSSO);
	}
	/**
	 * @param oldDesignEngineerSSO The oldDesignEngineerSSO to set.
	 */
	public void setOldDesignEngineerSSO(String oldDesignEngineerSSO) {
		this.oldDesignEngineerSSO = oldDesignEngineerSSO;
	}
	public ArrayList getCommercialManagersList() {
		return commercialManagersList;
	}
	public void setCommercialManagersList(ArrayList commercialManagersList) {
		this.commercialManagersList = commercialManagersList;
	}	
	
	public String getCommercialManagerName() {
		return  CommonUtility.replaceNull(commercialManagerName);
	}
	public void setCommercialManagerName(String commercialManagerName) {
		this.commercialManagerName = commercialManagerName;
	}
	public String getCommercialManagerSSO() {
		return  CommonUtility.replaceNull(commercialManagerSSO);
	}
	public void setCommercialManagerSSO(String commercialManagerSSO) {
		this.commercialManagerSSO = commercialManagerSSO;
	}
	public String getCommercialManagerOldSSO() {
		return  CommonUtility.replaceNull(commercialManagerOldSSO);
	}
	public void setCommercialManagerOldSSO(String commercialManagerOldSSO) {
		this.commercialManagerOldSSO = commercialManagerOldSSO;
	}
	public String getCommercialManagerOldName() {
		return  CommonUtility.replaceNull(commercialManagerOldName);
	}
	public void setCommercialManagerOldName(String commercialManagerOldName) {
		this.commercialManagerOldName = commercialManagerOldName;
	}


			
}
