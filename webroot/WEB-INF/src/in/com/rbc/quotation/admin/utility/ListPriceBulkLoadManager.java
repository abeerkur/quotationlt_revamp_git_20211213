package in.com.rbc.quotation.admin.utility;

import java.io.InputStream;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import in.com.rbc.quotation.admin.dao.ListPriseDao;
import in.com.rbc.quotation.admin.dto.ListPriceBulkLoadDto;
import in.com.rbc.quotation.admin.dto.ListPriceDto;
import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.utility.CommonUtility;
import in.com.rbc.quotation.common.utility.LoggerUtility;
import in.com.rbc.quotation.common.utility.PropertyUtility;
import in.com.rbc.quotation.common.vo.KeyValueVo;
import in.com.rbc.quotation.factory.DaoFactory;

public class ListPriceBulkLoadManager {

	/**
	 * This method retrieves the information from the excel sheet.
	 * @param request HttpServletRequest object to handle request operations
	 * @param xlsPath String variable holding the excel file name & path.
	 * @return ArrayList holding the ListPriceBulkLoadDto objects.
	 * @throws Exception Throws exception to the calling method, in case of any error.
	 */
	public ArrayList retrieveFromExcel(HttpServletRequest request, InputStream ipStrListPriceLoadFile) throws Exception {
		String sMethodName = "retrieveFromExcel";
	    String sClassName = this.getClass().getName();
	    LoggerUtility.log("INFO", sClassName, sMethodName, "START : ");
	    
	    InputStream inputStream = null;
	    POIFSFileSystem fileSystem = null;
	    ListPriceBulkLoadDto oListPriceBulkLoadDto = null;
	    ArrayList arlPriceList = null;
	    
	    try {
	    	//getting the uploaded file
	        inputStream = ipStrListPriceLoadFile;
	        
	        if ( inputStream != null ) {
	        	/* Retrieving the FileSystem object */
	            fileSystem = new POIFSFileSystem(inputStream);
	            
	            if (fileSystem != null) {
	            	/* Retrieving the workbook object */
	                HSSFWorkbook workBook = new HSSFWorkbook(fileSystem);
	                /* Retrieving the work sheet object */
	                HSSFSheet sheet = workBook.getSheetAt(0);
	                /* Retrieving the rows collection */
	                Iterator rows = sheet.rowIterator();                   
	                /* Initializing the ListPrice ArrayList object */
	                arlPriceList = new ArrayList();
	                
	                /* 
	                 * Starting of loop to parse through the collection of rows
	                 * and retrieve all the ListPrice information from the sheet.
	                 */
	                while (rows.hasNext()) {
	                	/* Retrieving the Excel row object holding an List Price information */
	                    HSSFRow row = (HSSFRow)rows.next();
	                    LoggerUtility.log("INFO", sClassName, sMethodName, "Row No.: " + row.getRowNum());
	                    
	                    /*
	                     * Checking if the file specified for bulk load is valid or not.
	                     * If valid, skipping the first row, since it contains the column headings.
	                     */
	                    if (row.getRowNum() == 1) {
	                    	Iterator cells = row.cellIterator();
	                        int cellCount = 0;
	                        while (cells.hasNext()) {
	                            /* Retrieving the cell object */
	                            HSSFCell cell = (HSSFCell)cells.next();
	                            cellCount++;
	                        }
	                        LoggerUtility.log("INFO", sClassName, sMethodName, "Cell Count = " + cellCount);
	                       
	                        if (cellCount != Integer.parseInt(PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONLT_LISTPRICE_BULKUPLOAD_COLUMNCOUNT))) {
	                            /*
	                             * Case when the number of columns specified in the sheet are not equal to the count required 
	                             * i.e., when not all information is specified.
	                             * Setting error message in request, which has to be displayed in the confirmation/report page.
	                             */
	                            request.setAttribute(QuotationConstants.APP_ERRORMESSAGE, "Please specify valid excel sheet to Bulk Load the List Prices");
	                            LoggerUtility.log("INFO", sClassName, sMethodName, "Please specify valid excel sheet to Bulk Load the List Prices");
	                            
	                            arlPriceList = null;
	                            break;
	                        }
	                        /* Skipping the first row, containing the column headings */
	                        continue;
	                    }
	                    
	                    /*
	                     * Retrieving the no of records defined in properties file for Bulk Upload
	                     * Break the loop : if record count is more than the count defined in the properties file.
	                     * This is done to handle the performance of the functionality.
	                     */
	                    if ( row.getRowNum() > (Integer.parseInt(PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONLT_BULKLOAD_MAX_ROWCOUNT))+2 )) {
	                    	/* Setting error details to request to display the same in Exception page */
							request.setAttribute(QuotationConstants.APP_ERRORMESSAGE,
								"The maximum number of records to be uploaded can be "
								+ PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONLT_BULKLOAD_MAX_ROWCOUNT)
								+ " only. Hence, records more than "
								+ PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONLT_BULKLOAD_MAX_ROWCOUNT)
								+ " were ignored during the process."
								+ " Please specify rest of the List Price Details in a separate input file, and reload the records.");
	                        break;
	                    }
	                    
	                    if(!CommonUtility.checkIfRowHasEmptyValues(row, QuotationConstants.QUOTATION_LISTPRICE_VALIDCOLUMNCOUNT)) {
	                    	if (row.getRowNum() > 1) {
								/* Initializing the ListPriceBulkLoadDto object */
								oListPriceBulkLoadDto = new ListPriceBulkLoadDto();

								HSSFRow rowsVals = sheet.getRow(row.getRowNum());
								
								HSSFCell cellVals = rowsVals.getCell(0);
								if ("".equals(String.valueOf(cellVals)) || cellVals == null) {
									oListPriceBulkLoadDto.setMfgLocation("");
								} else {
									oListPriceBulkLoadDto.setMfgLocation(String.valueOf(cellVals));
								}
								
								cellVals = rowsVals.getCell(1);
								if ("".equals(String.valueOf(cellVals)) || cellVals == null) {
									oListPriceBulkLoadDto.setProductGroup("");
								} else {
									oListPriceBulkLoadDto.setProductGroup(String.valueOf(cellVals));
								}
								
								cellVals = rowsVals.getCell(2);
								if ("".equals(String.valueOf(cellVals)) || cellVals == null) {
									oListPriceBulkLoadDto.setProductLine("");
								} else {
									oListPriceBulkLoadDto.setProductLine(String.valueOf(cellVals));
								}
								
								cellVals = rowsVals.getCell(3);
								if ("".equals(String.valueOf(cellVals)) || cellVals == null) {
									oListPriceBulkLoadDto.setStandard("");
								} else {
									if("Yes".equals(String.valueOf(cellVals))){
										oListPriceBulkLoadDto.setStandard("Y");
									} else {
										oListPriceBulkLoadDto.setStandard("N");
									}
								}
								
								cellVals = rowsVals.getCell(4);
								if ("".equals(String.valueOf(cellVals)) || cellVals == null) {
									oListPriceBulkLoadDto.setPowerRatingKW("");
								} else {
									oListPriceBulkLoadDto.setPowerRatingKW(String.valueOf(cellVals));
								}
								
								cellVals = rowsVals.getCell(5);
								if ("".equals(String.valueOf(cellVals)) || cellVals == null) {
									oListPriceBulkLoadDto.setFrameType("");
								} else {
									oListPriceBulkLoadDto.setFrameType(String.valueOf(cellVals));
								}
								
								cellVals = rowsVals.getCell(6);
								if ("".equals(String.valueOf(cellVals)) || cellVals == null) {
									oListPriceBulkLoadDto.setFrameSuffix("");
								} else {
									oListPriceBulkLoadDto.setFrameSuffix(String.valueOf(cellVals));
								}
								
								cellVals = rowsVals.getCell(7);
								if ("".equals(String.valueOf(cellVals)) || cellVals == null) {
									oListPriceBulkLoadDto.setNoOfPoles(0);
								} else {
									oListPriceBulkLoadDto.setNoOfPoles(Integer.parseInt(String.valueOf(cellVals)));
								}
								
								cellVals = rowsVals.getCell(8);
								if ("".equals(String.valueOf(cellVals)) || cellVals == null) {
									oListPriceBulkLoadDto.setMountType("");
								} else {
									oListPriceBulkLoadDto.setMountType(String.valueOf(cellVals));
								}
								
								cellVals = rowsVals.getCell(9);
								if ("".equals(String.valueOf(cellVals)) || cellVals == null) {
									oListPriceBulkLoadDto.setTbPosition("");
								} else {
									oListPriceBulkLoadDto.setTbPosition(String.valueOf(cellVals));
								}
								
								/* cellVals = rowsVals.getCell(10);
								if ("".equals(String.valueOf(cellVals)) || cellVals == null) {
									oListPriceBulkLoadDto.setListPrice("");
								} else {
									if(cellVals.getCellType() == HSSFCell.CELL_TYPE_FORMULA) {
										double cellValDouble = cellVals.getNumericCellValue();
	                                    DecimalFormat df = new DecimalFormat("##");
	                                    oListPriceBulkLoadDto.setListPrice(df.format(cellValDouble).toString())
									} else {
										oListPriceBulkLoadDto.setListPrice("");
									}
								} */
								cellVals = rowsVals.getCell(10);
								if ("".equals(String.valueOf(cellVals)) || cellVals == null) {
									oListPriceBulkLoadDto.setListPrice("");
								} else {
									oListPriceBulkLoadDto.setListPrice(String.valueOf(cellVals));
								}
								
								cellVals = rowsVals.getCell(11);
								if ("".equals(String.valueOf(cellVals)) || cellVals == null) {
									oListPriceBulkLoadDto.setMaterialCost("");
								} else {
									oListPriceBulkLoadDto.setMaterialCost(String.valueOf(cellVals));
								}
								
								cellVals = rowsVals.getCell(12);
								if ("".equals(String.valueOf(cellVals)) || cellVals == null) {
									oListPriceBulkLoadDto.setLoh("");
								} else {
									oListPriceBulkLoadDto.setLoh(String.valueOf(cellVals));
								}
								
								/* Adding each Composition's information in the ArrayList */
								arlPriceList.add(oListPriceBulkLoadDto);
							}
	                    }
	                }
	            }
	        }
	    } finally {
	    	if(inputStream != null){
	    		inputStream.close();
	    	}
	        fileSystem = null;
	    }
	    
	    LoggerUtility.log("INFO", sClassName, sMethodName, "END : ");
	    return arlPriceList;
	}
	
	/**
	 * This method loops through the Price List, retrieved from the excel sheet
	 * Validates the information, inserts valid List Price information in
	 * LIST_PRISE table and generates report based on the activity.
	 * 
	 * @param conn Connection object to perform database objects
	 * @param arlPriceList ArrayList holding the ListPriceBulkLoadDto objects of all Prices listed in the excel sheet.
	 * @return Hashtable object which contains different list for inserted records count, invalid records count, existing records count, etc
	 */
	public Hashtable processListPriceBulkLoadList(Connection conn, ArrayList arlPriceList, String loggedInUserId) throws Exception {
		String sMethodName = "processListPriceBulkLoadList";
	    String sClassName = this.getClass().getName();
	    LoggerUtility.log("INFO", sClassName, sMethodName, "-START");
	    
	    String sBlankSpaceChar =  "";
	    if (sBlankSpaceChar == null)
	    	sBlankSpaceChar = "";
	    
	    // Hashtable object : To store the List Price Bulk Load report/results.
	    Hashtable htBulkLoadReport = null;
	    // ArrayList to store the list of records that are valid and inserted.
	    ArrayList arlInsertedRecordsList = null;
	    // ArrayList to store the list of records that could not be processed.
	    ArrayList arlExistingRecordsList = null;
	    // ArrayList to store the list of records that could not be processed, since their format is invalid or any other reason.
	    ArrayList arlInvalidRecordsList = null;
	    
	    // String to store the result message to be displayed in bulk load activity report/confirmation page.
	    String sResultString = null;
	    // KeyValueVo object to store ListPrice information in the report, which is used during the process
	    KeyValueVo oKeyValueVo = null;
	    // Boolean FLag to Indicate if List Price can be inserted to DB or not.
	    boolean canProceed = false;
	    
	    ListPriceBulkLoadDto oListPriceBulkLoadDto  = new ListPriceBulkLoadDto();
	    
	    try {
	    	DaoFactory oDaoFactory = new DaoFactory();
	    	ListPriseDao oListPriseDao = oDaoFactory.getListPriseDao();
	    	
			if (arlPriceList.size() > 0) {
				/* Initializing the objects */
				sResultString = "";
				arlInsertedRecordsList = new ArrayList();
				arlExistingRecordsList = new ArrayList();
				arlInvalidRecordsList = new ArrayList();
				
				// Looping through the Price ArrayList to validate and update database accordingly.
				Iterator it = arlPriceList.iterator();
				
				int i=0;
	            int recordCount = 0;
	            
	            while(it.hasNext()) {
	            	oListPriceBulkLoadDto = (ListPriceBulkLoadDto)it.next();
	            	
	            	// If ListPrice value is Null/Empty/NaN - Skip the record, add to Invalid Records List.
	            	String sLPValue = oListPriceBulkLoadDto.getListPrice();
	            	if(StringUtils.isBlank(sLPValue) || CommonUtility.isNaN(sLPValue)) {
	            		recordCount++;
	            		i++;
	            		// Updating the Invalid Records list.
	            		arlInvalidRecordsList = updateReportList(arlInvalidRecordsList, sLPValue, "Line No." + (i+2), "This Record contains Invalid List Price value.");
	            		LoggerUtility.log("INFO", sClassName, sMethodName, "Line No." + (i+2) + " - This Record contains Invalid List Price value.");
	            		continue;
	            	}
	            	
	            	// If Required Parameters for LIST_PRICE Table are missing - Skip the record, add to Invalid Records List.
	            	if(StringUtils.isBlank(oListPriceBulkLoadDto.getMfgLocation()) 
	            			|| StringUtils.isBlank(oListPriceBulkLoadDto.getProductGroup())
	            			|| StringUtils.isBlank(oListPriceBulkLoadDto.getProductLine()) 
	            			|| StringUtils.isBlank(oListPriceBulkLoadDto.getStandard()) 
	            			|| StringUtils.isBlank(oListPriceBulkLoadDto.getPowerRatingKW())
	            			|| StringUtils.isBlank(oListPriceBulkLoadDto.getFrameType()) 
	            			|| StringUtils.isBlank(oListPriceBulkLoadDto.getFrameSuffix()) 
	            			|| oListPriceBulkLoadDto.getNoOfPoles() == 0 ) {
	            		recordCount++;
	            		i++;
	            		// Updating the Invalid Records list.
	            		arlInvalidRecordsList = updateReportList(arlInvalidRecordsList, sLPValue, "Line No." + (i+2), "Required Parameters for List Price are missing.");
	            		LoggerUtility.log("INFO", sClassName, sMethodName, "Line No." + (i+2) + " - Required Parameters for List Price are missing.");
	            		continue;
	            	}
	            	
	            	// If List Price already present (With matching parameters) - Skip the record, add to Existing Records List.
	            	String isLPExists = "No";
	            	oKeyValueVo = oListPriseDao.isListPriceExists(conn, oListPriceBulkLoadDto);
					if (oKeyValueVo != null) {
						recordCount++;
	                	i++;
	                	isLPExists="Yes";
	                	oListPriceBulkLoadDto.setIsListPriceExists("Y");
	                	canProceed = false;
	                	// Updating the Existing Records list.
	                	arlExistingRecordsList = updateReportList(arlExistingRecordsList, sLPValue, "Line No." + (i+2), "List Price : " + sLPValue + " with given Product Line and Frame paramters already exists!");
					} else {
						LoggerUtility.log("INFO", sClassName, sMethodName, " if oKeyValueVo == null. List Price record is not in DB... Can be inserted.");
	                    // Case when List Price record does not exist.
						oListPriceBulkLoadDto.setIsListPriceExists("N");
	                    canProceed = true;
					}
					
					// If all the conditions are satisfied : Insert the List Price.
					if (canProceed) {
						String isRecordInserted = null;     
	                    boolean oListPriceCreated = false;
	                    
	                    if("N".equals(oListPriceBulkLoadDto.getIsListPriceExists())){
	                    	ListPriceDto oListPriceDto = new ListPriceDto();
	                    	oListPriceDto.setManufacturing_location(oListPriceBulkLoadDto.getMfgLocation());
	                    	oListPriceDto.setProduct_group(oListPriceBulkLoadDto.getProductGroup());
	                    	oListPriceDto.setProduct_line(oListPriceBulkLoadDto.getProductLine());
	                    	oListPriceDto.setStandard(oListPriceBulkLoadDto.getStandard());
	                    	oListPriceDto.setPower_rating_kw(oListPriceBulkLoadDto.getPowerRatingKW());
	                    	oListPriceDto.setFrame_type(oListPriceBulkLoadDto.getFrameType());
	                    	oListPriceDto.setFrame_suffix(oListPriceBulkLoadDto.getFrameSuffix());
	                    	oListPriceDto.setNumber_of_poles(oListPriceBulkLoadDto.getNoOfPoles());
	                    	oListPriceDto.setMounting_type(oListPriceBulkLoadDto.getMountType());
	                    	oListPriceDto.setTb_position(oListPriceBulkLoadDto.getTbPosition());
	                    	oListPriceDto.setList_price(sLPValue);
	                    	oListPriceDto.setMaterial_cost(oListPriceBulkLoadDto.getMaterialCost());
	                    	oListPriceDto.setLoh(oListPriceBulkLoadDto.getLoh());
	                    	
	                    	oListPriceCreated = oListPriseDao.addListPrice(conn, oListPriceDto);
	                    	
							if (oListPriceCreated == true) {
								LoggerUtility.log("INFO", sClassName, sMethodName, "Record inserted - " + isRecordInserted);
                                /* Updating the Inserted Records list */
                                arlInsertedRecordsList = updateReportList(arlInsertedRecordsList, sLPValue, "Line No." + (i+3), sLPValue);
                                recordCount++;
                                i++;
							} else {
								LoggerUtility.log("INFO", sClassName, sMethodName, "Record could not be inserted - " + isRecordInserted);
								/* Updating the Invalid Records list */
								arlInvalidRecordsList = updateReportList(arlInvalidRecordsList, sLPValue, "Line No." + (i+3), " ListPrice could not be created. Error occurred during the process.");
								recordCount++;
                                i++;
							}
	                    }
					} else {
	                    LoggerUtility.log("INFO", sClassName, sMethodName, "- - - List Price record not processed - - -");
	                }
					
	            }
	            
	            //System.out.println("Bulk Load Process : Value of  i : " + i);
				//System.out.println("Bulk Load Process : arlPriceList size : " + arlPriceList.size());
				if (i == arlPriceList.size()) {
					sResultString = "Success";
                }
				
	            /* Initializing the Hashtable and setting report information */
	            htBulkLoadReport = new Hashtable();
	            htBulkLoadReport.put("RESULT", sResultString);
	            htBulkLoadReport.put("RECORDCOUNT", recordCount);
	            htBulkLoadReport.put("INSERTEDRECORDS", arlInsertedRecordsList);
	            htBulkLoadReport.put("EXISTINGRECORDS", arlExistingRecordsList);
	            htBulkLoadReport.put("INVALIDRECORDS", arlInvalidRecordsList);
			}
	    	
		} finally {
			// releasing the ArrayList objects
			arlInsertedRecordsList = null;
			arlExistingRecordsList = null;
			arlInvalidRecordsList = null;
			oKeyValueVo = null;
		}
	    
	    LoggerUtility.log("INFO", sClassName, sMethodName, "END");
	    return htBulkLoadReport;
	}
	
	
	/**
	 * Method to set values in Key-Value format, in an object and update it in the specified ArrayList
	 * 
	 * @param arlReportList ArrayList that has to be updated
	 * @param sKey Key value to set
	 * @param sValue1 Value1 to set
	 * @param sValue2 Value2 to set
	 * @return Returns the updated ArrayList
	 */
	private ArrayList updateReportList(ArrayList arlReportList, String sKey, String sValue1, String sValue2) {
		KeyValueVo oKeyValueVo = new KeyValueVo();
		oKeyValueVo.setKey(sKey);
		oKeyValueVo.setValue(sValue1);
		oKeyValueVo.setValue2(sValue2);
		arlReportList.add(oKeyValueVo);
		LoggerUtility.log("INFO", this.getClass().getName(), "updateReportList",
				"AdminAction.updateReportList(): Key = " + sKey + " ::: Value 1 = " + sValue1 + " ::: Value 2 = " + sValue2);
		return arlReportList;
	}
	
}
