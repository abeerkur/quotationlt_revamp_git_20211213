/**
 * *****************************************************************************
 * Project Name: quotation
 * Document Name: CustomerForm.java
 * Package: in.com.rbc.quotation.admin.form
 * Desc: 
 * *****************************************************************************
 * Author: 900008798 (Abhilash Moola)
 * Date: Dec 02, 2020
 * *****************************************************************************
 */
package in.com.rbc.quotation.admin.form;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

public class CustomerDiscountForm extends ActionForm {
	
	private String invoke ;
	private int serial_id;
	private String customerId;
	private String manufacturing_location;
	private String product_group;
	private String product_line;
	private String standard;
	private String power_rating_kw;
	private String frame_type;
	private int number_of_poles;
	private String mounting_type;
	private String tb_position;
	private String customer_name;
	private String sapcode;
	private String oraclecode;
	private String gstnumber;
	private int salesengineerid;
	private String salesengineer;
	private String customer_discount;
	private String sm_discount;
	private String rsm_discount;
	private String nsm_discount;
	private ArrayList searchResultsList = null;
	private String operation;
	
	private ArrayList maufacturingList = new ArrayList();
	private ArrayList productGroupList = new ArrayList();
	private ArrayList productLineList = new ArrayList();
	private ArrayList standardList = new ArrayList();
	private ArrayList powerratingkwList = new ArrayList();
	private ArrayList frametypelist = new ArrayList();
	private ArrayList numberofpoleslist = new ArrayList();
	private ArrayList mountingyypelist = new ArrayList();
	private ArrayList tbpositionlist = new ArrayList();
	private ArrayList customerlist = new ArrayList();
	private ArrayList sapcodelist = new ArrayList();
	private ArrayList oraclecodelist = new ArrayList();
	private ArrayList gstnumberlist = new ArrayList();
	private ArrayList salesengineerList = new ArrayList();
	
	FormFile custDiscountBulkupload = null;
	String isFileLoaded = null;
	
	public String getInvoke() {
		return invoke;
	}
	public void setInvoke(String invoke) {
		this.invoke = invoke;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getManufacturing_location() {
		return manufacturing_location;
	}
	public void setManufacturing_location(String manufacturing_location) {
		this.manufacturing_location = manufacturing_location;
	}
	public String getProduct_group() {
		return product_group;
	}
	public void setProduct_group(String product_group) {
		this.product_group = product_group;
	}
	public String getProduct_line() {
		return product_line;
	}
	public void setProduct_line(String product_line) {
		this.product_line = product_line;
	}
	public String getStandard() {
		return standard;
	}
	public void setStandard(String standard) {
		this.standard = standard;
	}
	public String getPower_rating_kw() {
		return power_rating_kw;
	}
	public void setPower_rating_kw(String power_rating_kw) {
		this.power_rating_kw = power_rating_kw;
	}
	public String getFrame_type() {
		return frame_type;
	}
	public void setFrame_type(String frame_type) {
		this.frame_type = frame_type;
	}
	public int getNumber_of_poles() {
		return number_of_poles;
	}
	public void setNumber_of_poles(int number_of_poles) {
		this.number_of_poles = number_of_poles;
	}
	public String getMounting_type() {
		return mounting_type;
	}
	public void setMounting_type(String mounting_type) {
		this.mounting_type = mounting_type;
	}
	public String getTb_position() {
		return tb_position;
	}
	public void setTb_position(String tb_position) {
		this.tb_position = tb_position;
	}
	public String getCustomer_name() {
		return customer_name;
	}
	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}
	public String getSapcode() {
		return sapcode;
	}
	public void setSapcode(String sapcode) {
		this.sapcode = sapcode;
	}
	public String getOraclecode() {
		return oraclecode;
	}
	public void setOraclecode(String oraclecode) {
		this.oraclecode = oraclecode;
	}
	public String getGstnumber() {
		return gstnumber;
	}
	public void setGstnumber(String gstnumber) {
		this.gstnumber = gstnumber;
	}
	public int getSalesengineerid() {
		return salesengineerid;
	}
	public void setSalesengineerid(int salesengineerid) {
		this.salesengineerid = salesengineerid;
	}
	public String getSalesengineer() {
		return salesengineer;
	}
	public void setSalesengineer(String salesengineer) {
		this.salesengineer = salesengineer;
	}
	public String getCustomer_discount() {
		return customer_discount;
	}
	public void setCustomer_discount(String customer_discount) {
		this.customer_discount = customer_discount;
	}
	public String getSm_discount() {
		return sm_discount;
	}
	public void setSm_discount(String sm_discount) {
		this.sm_discount = sm_discount;
	}
	public String getRsm_discount() {
		return rsm_discount;
	}
	public void setRsm_discount(String rsm_discount) {
		this.rsm_discount = rsm_discount;
	}
	public String getNsm_discount() {
		return nsm_discount;
	}
	public void setNsm_discount(String nsm_discount) {
		this.nsm_discount = nsm_discount;
	}
	public ArrayList getSearchResultsList() {
		return searchResultsList;
	}
	public void setSearchResultsList(ArrayList searchResultsList) {
		this.searchResultsList = searchResultsList;
	}
	public int getSerial_id() {
		return serial_id;
	}
	public void setSerial_id(int serial_id) {
		this.serial_id = serial_id;
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public ArrayList getMaufacturingList() {
		return maufacturingList;
	}
	public void setMaufacturingList(ArrayList maufacturingList) {
		this.maufacturingList = maufacturingList;
	}
	public ArrayList getProductGroupList() {
		return productGroupList;
	}
	public void setProductGroupList(ArrayList productGroupList) {
		this.productGroupList = productGroupList;
	}
	public ArrayList getProductLineList() {
		return productLineList;
	}
	public void setProductLineList(ArrayList productLineList) {
		this.productLineList = productLineList;
	}
	public ArrayList getStandardList() {
		return standardList;
	}
	public void setStandardList(ArrayList standardList) {
		this.standardList = standardList;
	}
	public ArrayList getPowerratingkwList() {
		return powerratingkwList;
	}
	public void setPowerratingkwList(ArrayList powerratingkwList) {
		this.powerratingkwList = powerratingkwList;
	}
	public ArrayList getFrametypelist() {
		return frametypelist;
	}
	public void setFrametypelist(ArrayList frametypelist) {
		this.frametypelist = frametypelist;
	}
	public ArrayList getNumberofpoleslist() {
		return numberofpoleslist;
	}
	public void setNumberofpoleslist(ArrayList numberofpoleslist) {
		this.numberofpoleslist = numberofpoleslist;
	}
	public ArrayList getMountingyypelist() {
		return mountingyypelist;
	}
	public void setMountingyypelist(ArrayList mountingyypelist) {
		this.mountingyypelist = mountingyypelist;
	}
	public ArrayList getTbpositionlist() {
		return tbpositionlist;
	}
	public void setTbpositionlist(ArrayList tbpositionlist) {
		this.tbpositionlist = tbpositionlist;
	}
	public ArrayList getCustomerlist() {
		return customerlist;
	}
	public void setCustomerlist(ArrayList customerlist) {
		this.customerlist = customerlist;
	}
	public ArrayList getSapcodelist() {
		return sapcodelist;
	}
	public void setSapcodelist(ArrayList sapcodelist) {
		this.sapcodelist = sapcodelist;
	}
	public ArrayList getOraclecodelist() {
		return oraclecodelist;
	}
	public void setOraclecodelist(ArrayList oraclecodelist) {
		this.oraclecodelist = oraclecodelist;
	}
	public ArrayList getGstnumberlist() {
		return gstnumberlist;
	}
	public void setGstnumberlist(ArrayList gstnumberlist) {
		this.gstnumberlist = gstnumberlist;
	}
	public ArrayList getSalesengineerList() {
		return salesengineerList;
	}
	public void setSalesengineerList(ArrayList salesengineerList) {
		this.salesengineerList = salesengineerList;
	}
	public FormFile getCustDiscountBulkupload() {
		return custDiscountBulkupload;
	}
	public void setCustDiscountBulkupload(FormFile custDiscountBulkupload) {
		this.custDiscountBulkupload = custDiscountBulkupload;
	}
	public String getIsFileLoaded() {
		return isFileLoaded;
	}
	public void setIsFileLoaded(String isFileLoaded) {
		this.isFileLoaded = isFileLoaded;
	}
	
	
}
