/**
 * *****************************************************************************
 * Project Name: quotation
 * Document Name: DesignEngineerDto.java
 * Package: in.com.rbc.quotation.admin.dto
 * Desc: 
 * *****************************************************************************
 * Author: 100005701 (Suresh)
 * Date: Oct 17, 2008
 * *****************************************************************************
 */
package in.com.rbc.quotation.admin.dto;

import in.com.rbc.quotation.common.utility.CommonUtility;

import java.util.ArrayList;

/**
 * @author 100005701 (Suresh Shanmugam)
 *
 */
public class DesignEngineerDto {
	
	private String designEngineerName= null;
	private String designEngineerSSO=null;
	private int designEngineerId = 0;
	private String invoke = null;
	private String operation;
	
	private ArrayList searchResultsList = null ;
	
	private String createdById;
	private String lastUpdatedById ;
	
	private String createdDate ;
	private String lastUpdatedDate ;


	/**
	 * @return Returns the createdById.
	 */
	public String getCreatedById() {
		return CommonUtility.replaceNull(this.createdById);
	}

	/**
	 * @param createdById The createdById to set.
	 */
	public void setCreatedById(String createdById) {
		this.createdById = createdById;
	}

	/**
	 * @return Returns the createdDate.
	 */
	public String getCreatedDate() {
		return CommonUtility.replaceNull(this.createdDate);
	}

	/**
	 * @param createdDate The createdDate to set.
	 */
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return Returns the lastUpdatedById.
	 */
	public String getLastUpdatedById() {
		return CommonUtility.replaceNull(this.lastUpdatedById);
	}

	/**
	 * @param lastUpdatedById The lastUpdatedById to set.
	 */
	public void setLastUpdatedById(String lastUpdatedById) {
		this.lastUpdatedById = lastUpdatedById;
	}

	/**
	 * @return Returns the lastUpdatedDate.
	 */
	public String getLastUpdatedDate() {
		return CommonUtility.replaceNull(this.lastUpdatedDate);
	}

	/**
	 * @param lastUpdatedDate The lastUpdatedDate to set.
	 */
	public void setLastUpdatedDate(String lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	/**
	 * @return Returns the designEngineerId.
	 */
	public int getDesignEngineerId() {
		return this.designEngineerId;
	}

	/**
	 * @param designEngineerId The designEngineerId to set.
	 */
	public void setDesignEngineerId(int designEngineerId) {
		this.designEngineerId = designEngineerId;
	}

	/**
	 * @return Returns the designEngineerName.
	 */
	public String getDesignEngineerName() {
		return CommonUtility.replaceNull(this.designEngineerName);
	}

	/**
	 * @param designEngineerName The designEngineerName to set.
	 */
	public void setDesignEngineerName(String designEngineerName) {
		this.designEngineerName = designEngineerName;
	}

	/**
	 * @return Returns the designEngineerSSO.
	 */
	public String getDesignEngineerSSO() {
		return CommonUtility.replaceNull(this.designEngineerSSO);
	}

	/**
	 * @param designEngineerSSO The designEngineerSSO to set.
	 */
	public void setDesignEngineerSSO(String designEngineerSSO) {
		this.designEngineerSSO = designEngineerSSO;
	}

	/**
	 * @return Returns the invoke.
	 */
	public String getInvoke() {
		return CommonUtility.replaceNull(this.invoke);
	}

	/**
	 * @param invoke The invoke to set.
	 */
	public void setInvoke(String invoke) {
		this.invoke = invoke;
	}

	/**
	 * @return Returns the operation.
	 */
	public String getOperation() {
		return CommonUtility.replaceNull(this.operation);
	}

	/**
	 * @param operation The operation to set.
	 */
	public void setOperation(String operation) {
		this.operation = operation;
	}

	/**
	 * @return Returns the searchResultsList.
	 */
	public ArrayList getSearchResultsList() {
		return this.searchResultsList;
	}

	/**
	 * @param searchResultsList The searchResultsList to set.
	 */
	public void setSearchResultsList(ArrayList searchResultsList) {
		this.searchResultsList = searchResultsList;
	}

}
