/**
 * *****************************************************************************
 * Project Name: quotation
 * Document Name: CustomerDiscountAction.java
 * Package: in.com.rbc.quotation.admin.action
 * Desc: 
 * *****************************************************************************
 * Author: 900008798 (Abhilash Moola)
 * Date: Dec 02, 2020
 * *****************************************************************************
 */
package in.com.rbc.quotation.admin.action;

import java.io.InputStream;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

import in.com.rbc.quotation.admin.dao.CustomerDiscountDao;
import in.com.rbc.quotation.admin.dao.ListPriseDao;
import in.com.rbc.quotation.admin.dto.CustomerDiscountDto;
import in.com.rbc.quotation.admin.dto.ListPriceDto;
import in.com.rbc.quotation.admin.form.CustomerDiscountForm;
import in.com.rbc.quotation.admin.form.ListPriceForm;
import in.com.rbc.quotation.admin.utility.CustomerDiscountBulkLoadManager;
import in.com.rbc.quotation.base.BaseAction;
import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.utility.CommonUtility;
import in.com.rbc.quotation.common.utility.DBUtility;
import in.com.rbc.quotation.common.utility.ExceptionUtility;
import in.com.rbc.quotation.common.utility.ListDBColumnUtil;
import in.com.rbc.quotation.common.utility.ListModel;
import in.com.rbc.quotation.common.utility.LoggerUtility;
import in.com.rbc.quotation.factory.DaoFactory;
import in.com.rbc.quotation.user.action.UserAction;
import in.com.rbc.quotation.user.dto.UserDto;

/**
 * <h3>Class Name:-CustomerDiscountAction</h3> 
 * <br> This class upload bulk data 
 * <br>	-->viewbulkdata , 
 * <br>	--> and 
 * <br>This Class is Extends from BaseAction Class .
 * @author 900008798 (Abhilash Moola)
 *
 */
public class CustomerDiscountAction extends BaseAction {
	
	public ActionForward viewdiscount(ActionMapping actionMapping, ActionForm actionForm,
			  HttpServletRequest request , HttpServletResponse response)throws Exception {
		/*
		* Setting user's information in request, using the header 
		* information received through request
		*/
		setRolesToRequest(request);
		
		// Method name Set for log file usage ;
		String sMethodname = "viewdiscount";
		LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"START");
		
		CustomerDiscountForm oCustomerDiscountForm = (CustomerDiscountForm) actionForm;
		if(oCustomerDiscountForm ==  null) {
			oCustomerDiscountForm = new CustomerDiscountForm();
		}
		
		Connection conn = null;  // Connection object to store the database connection
		DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
		Hashtable htSearchResultsList =null; //Object of Hashtable to store different values
		ListModel oListModel=null;
		CustomerDiscountDto oCustomerDiscountDto = null;
		DaoFactory oDaoFactory=null;
		CustomerDiscountDao oCustomerDiscountDao = null;
		HttpSession session= request.getSession();
		
		try {
			saveToken(request);// To avoid entering the duplicate value into database.
			
			/*
			* Object of ListModel class to store the values necessary to display 
			* the results in attribute search results page with pagination
			*/
			oListModel = new ListModel();
			/*
			* Setting values for ListModel that are necessary to retrieve rows from the database
			* and display them in search results page. It holds information like
			* --> number of rows to be displayed at once in the page
			* --> column on which rows have to be sorted
			* --> order in which rows have to be sorted
			* --> set of rows to be retrieved depending on the page number selected for viewing
			* Details of parameters:
			* 1. Request object
			* 2. Sort column index
			* 3. Sort order
			* 4. Search form
			* 5. Field that stores the invoke method name
			* 8. Invoke method name
			*/
			oListModel.setParams(request, 
			 "1", 
			 ListModel.ASCENDING, 
			 "customerDiscountForm", 
			 "customerDiscountForm.invoke", 
			 "viewdiscount");
			
			/* Instantiating the DBUtility object */
			oDBUtility = new DBUtility();
			/* Creating an instance of of DaoFactory  */
			oDaoFactory = new DaoFactory();
			oCustomerDiscountDao = oDaoFactory.getCustomerDiscountDao();
			/* Retrieving the database connection using DBUtility.getDBConnection method */
			conn = oDBUtility.getDBConnection();
			oCustomerDiscountDto = new CustomerDiscountDto();
			
			/* Copying the Form Object into DTO Object via Propertyutils*/
			PropertyUtils.copyProperties(oCustomerDiscountDto , oCustomerDiscountForm);
			
			/* Retrieving the search results using LocationDaoImpl.getLocationSearchResult method */
			htSearchResultsList = oCustomerDiscountDao.getCustomerDiscountSearchResult(conn,oCustomerDiscountDto,oListModel);
			
			if(htSearchResultsList != null ) {
				oCustomerDiscountDto.setSearchResultsList((ArrayList)htSearchResultsList.get(QuotationConstants.QUOTATION_LIST_RESULTSLIST));
				request.setAttribute(QuotationConstants.QUOTATION_CDDTO,oCustomerDiscountDto); 
				request.setAttribute(QuotationConstants.QUOTATION_LIST_LISTSIZE,new Integer(((ArrayList)htSearchResultsList.get(QuotationConstants.QUOTATION_LIST_RESULTSLIST)).size()));
				request.setAttribute(QuotationConstants.QUOTATION_LIST_RESULTSLIST,(ArrayList)htSearchResultsList.get(QuotationConstants.QUOTATION_LIST_RESULTSLIST));
		        request.setAttribute(QuotationConstants.QUOTATION_LIST_LISTMODELCLASS,htSearchResultsList.get(QuotationConstants.QUOTATION_LIST_LISTMODELCLASS));
		        session.setAttribute(QuotationConstants.QUOTATION_SESSIONLIST,(ArrayList)htSearchResultsList.get(QuotationConstants.QUOTATION_LIST_RESULTSLIST));
				session.setAttribute(QuotationConstants.RBC_QUOTATION_CUSTDISCOUNT_QUERY,(String)htSearchResultsList.get(QuotationConstants.RBC_QUOTATION_CUSTDISCOUNT_QUERY));
				session.setAttribute(QuotationConstants.QUOTATION_SEARCH_SORTFIELD, ListDBColumnUtil.getTechnicalDataResultDBField(oListModel.getSortBy()));
				session.setAttribute(QuotationConstants.QUOTATION_SEARCH_SORTORDER, oListModel.getSortOrderDesc());
			} else {
				request.setAttribute(QuotationConstants.QUOTATION_LIST_LISTSIZE, new Integer(0));
			}
			
			oCustomerDiscountDto = oCustomerDiscountDao.loadLookUpValues(conn, oCustomerDiscountDto);
			
			/* Copying the DTO Object to Form Object via Propertyutils */
			PropertyUtils.copyProperties(oCustomerDiscountForm, oCustomerDiscountDto);
		} catch(Exception e) {
			/*
			* Logging any exception that raised during this activity in log files using Log4j
			*/
			LoggerUtility.log("DEBUG", this.getClass().getName(), "viewdiscount  Result.", ExceptionUtility.getStackTraceAsString(e));
			
			/* Setting error details to request to display the same in Exception page */
			request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
			/* Forwarding request to Error page */
			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
		}finally {
		/* Releasing/closing the connection object */
		oDBUtility.releaseResources(conn);
		}
		LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"END");
				
		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_VIEW);
		
	}
	
	public ActionForward addNewCustomerDiscount(ActionMapping actionMapping, ActionForm actionForm,
			  HttpServletRequest request , HttpServletResponse response)throws Exception {
		/*
		* Setting user's information in request, using the header 
		* information received through request
		*/
		setRolesToRequest(request);
		
		// Method name Set for log file usage ;
		String sMethodname = "addNewCustomerDiscount";
		LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"START");
		
		CustomerDiscountForm oCustomerDiscountForm = (CustomerDiscountForm) actionForm;
		if(oCustomerDiscountForm ==  null) {
			oCustomerDiscountForm = new CustomerDiscountForm();
		}
		
		Connection conn = null;  // Connection object to store the database connection
		DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
		CustomerDiscountDto oCustomerDiscountDto = null;
		DaoFactory oDaoFactory=null;
		CustomerDiscountDao oCustomerDiscountDao = null;
		try {
			saveToken(request);		// To avoid entering the duplicate value into database.
			
			/* Instantiating the DBUtility object */
			oDBUtility = new DBUtility();
			/* Creating an instance of of DaoFactory  */
			oDaoFactory = new DaoFactory();
			/* Retrieving the database connection using DBUtility.getDBConnection method */
			conn = oDBUtility.getDBConnection();
			
			oCustomerDiscountDao = oDaoFactory.getCustomerDiscountDao();
			oCustomerDiscountDto = new CustomerDiscountDto();
			oCustomerDiscountDto = oCustomerDiscountDao.loadLookUpValues(conn, oCustomerDiscountDto);
			
			oCustomerDiscountDto.setCustomerlist(oCustomerDiscountDao.getCustomerNamesList(conn));
			oCustomerDiscountDto.setSapcodelist(oCustomerDiscountDao.getSapcodelist(conn));
			oCustomerDiscountDto.setOraclecodelist(oCustomerDiscountDao.getOraclecodelist(conn));
			oCustomerDiscountDto.setGstnumberlist(oCustomerDiscountDao.getGstnumberlist(conn));
			oCustomerDiscountDto.setSalesengineerList(oCustomerDiscountDao.getSalesManagersList(conn));
			
			/* Copying the Form Object into DTO Object via Propertyutils*/
			PropertyUtils.copyProperties(oCustomerDiscountForm,oCustomerDiscountDto);
			
		} catch(Exception e) {
			/*
			* Logging any exception that raised during this activity in log files using Log4j
			*/
			LoggerUtility.log("DEBUG", this.getClass().getName(), "addNewCustomerDiscount  Result.", ExceptionUtility.getStackTraceAsString(e));
			/* Setting error details to request to display the same in Exception page */
			request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
			/* Forwarding request to Error page */
			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
		}finally {
			/* Releasing/closing the connection object */
			oDBUtility.releaseResources(conn);
		}
		LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"END");
				
		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_ADDMASTERDATA);
		
	}
	
	public ActionForward addupdateCustomerDiscount(ActionMapping actionMapping, ActionForm actionForm,
			  HttpServletRequest request , HttpServletResponse response)throws Exception {
		
		// Method name Set for log file usage ;
		String sMethodname = "addupdateCustomerDiscount";
		LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"START");
		
		Connection conn = null;  // Connection object to store the database connection
		DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
		CustomerDiscountDto oCustomerDiscountDto = null;
		DaoFactory oDaoFactory = null;
		CustomerDiscountDao oCustomerDiscountDao = null;
		String operation = null;
		String sSuccessMsg = "";
		boolean isUpdated = false;
		
		try {
			saveToken(request);// To avoid entering the duplicate value into database.
			
			// Setting user's information in request, using the header 
			setRolesToRequest(request);
			
			/* Instantiating the DBUtility object */
			oDBUtility = new DBUtility();
			/* Retrieving the database connection using DBUtility.getDBConnection method */
			conn = oDBUtility.getDBConnection();
			/* Creating an instance of of DaoFactory  */
			oDaoFactory = new DaoFactory();
			oCustomerDiscountDao = oDaoFactory.getCustomerDiscountDao();
			
			CustomerDiscountForm oCustomerDiscountForm = (CustomerDiscountForm) actionForm;
			if(oCustomerDiscountForm ==  null) {
				oCustomerDiscountForm = new CustomerDiscountForm();
			}
			oCustomerDiscountDto = new CustomerDiscountDto();
			
			/* Copying the Form Object into DTO Object via PropertyUtils */
			PropertyUtils.copyProperties(oCustomerDiscountDto , oCustomerDiscountForm);
			
			operation = CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_OPERATION);
			
			if(operation.equals(QuotationConstants.QUOTATION_FORWARD_UPDATE)){
				isUpdated = oCustomerDiscountDao.updateCustomerDiscount(conn, oCustomerDiscountDto);
				sSuccessMsg = QuotationConstants.QUOTATION_REC_UPDATESUCC;
			} else {
				isUpdated = oCustomerDiscountDao.addCustomerDiscount(conn, oCustomerDiscountDto);
				sSuccessMsg = QuotationConstants.QUOTATION_REC_ADDSUCC;
			}
			
			if(isUpdated)
				request.setAttribute(QuotationConstants.QUOTATION_ADMIN_SUCCESSMESSAGE, "Customer Discount " + sSuccessMsg);
			
			request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_URL, QuotationConstants.QUOTATION_CUSTOMERDISCOUNT_URL); 
			
		} catch(Exception e) {
			/*
			* Logging any exception that raised during this activity in log files using Log4j
			*/
			LoggerUtility.log("DEBUG", this.getClass().getName(), "addupdateCustomerDiscount  Result.", ExceptionUtility.getStackTraceAsString(e));
			/* Setting error details to request to display the same in Exception page */
			request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
			/* Forwarding request to Error page */
			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
		} finally {
			/* Releasing/closing the connection object */
			oDBUtility.releaseResources(conn);
		}
		LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"END");
				
		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_SUCCESS);
		
	}
	
	public ActionForward editcustomerdiscount(ActionMapping actionMapping, ActionForm actionForm,
			  HttpServletRequest request , HttpServletResponse response)throws Exception {
		/*
		* Setting user's information in request, using the header 
		* information received through request
		*/
		setRolesToRequest(request);
		
		// Method name Set for log file usage ;
		String sMethodname = "editcustomerdiscount";
		LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"START");
		
		CustomerDiscountForm oCustomerDiscountForm = (CustomerDiscountForm) actionForm;
		if(oCustomerDiscountForm ==  null) {
			oCustomerDiscountForm = new CustomerDiscountForm();
		}
		
		Connection conn = null;  // Connection object to store the database connection
		DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
		CustomerDiscountDto oCustomerDiscountDto = null;
		DaoFactory oDaoFactory=null;
		CustomerDiscountDao oCustomerDiscountDao = null;
		
		try {
			saveToken(request);// To avoid entering the duplicate value into database.
			
			/* Instantiating the DBUtility object */
			oDBUtility = new DBUtility();
			/* Retrieving the database connection using DBUtility.getDBConnection method */
			conn = oDBUtility.getDBConnection();
			/* Creating an instance of of DaoFactory  */
			oDaoFactory = new DaoFactory();
			oCustomerDiscountDao = oDaoFactory.getCustomerDiscountDao();
			
			oCustomerDiscountDto = new CustomerDiscountDto();
			oCustomerDiscountDto.setSerial_id(CommonUtility.getIntParameter(request,QuotationConstants.QUOTATION_SERIAL_ID));
			oCustomerDiscountDto = oCustomerDiscountDao.getCustomerDiscount(conn,oCustomerDiscountDto);
			
			oCustomerDiscountDto = oCustomerDiscountDao.loadLookUpValues(conn, oCustomerDiscountDto);
			
			oCustomerDiscountDto.setCustomerlist(oCustomerDiscountDao.getCustomerNamesList(conn));
			oCustomerDiscountDto.setSapcodelist(oCustomerDiscountDao.getSapcodelist(conn));
			oCustomerDiscountDto.setOraclecodelist(oCustomerDiscountDao.getOraclecodelist(conn));
			oCustomerDiscountDto.setGstnumberlist(oCustomerDiscountDao.getGstnumberlist(conn));
			oCustomerDiscountDto.setSalesengineerList(oCustomerDiscountDao.getSalesManagersList(conn));
			oCustomerDiscountDto.setOperation(QuotationConstants.QUOTATION_FORWARD_UPDATE);
			PropertyUtils.copyProperties(oCustomerDiscountForm,oCustomerDiscountDto);
			
		} catch(Exception e) {
			/*
			* Logging any exception that raised during this activity in log files using Log4j
			*/
			LoggerUtility.log("DEBUG", this.getClass().getName(), "viewdiscount  Result.", ExceptionUtility.getStackTraceAsString(e));
			/* Setting error details to request to display the same in Exception page */
			request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
			/* Forwarding request to Error page */
			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
		} finally {
			/* Releasing/closing the connection object */
			oDBUtility.releaseResources(conn);
		}
		LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"END");
				
		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_ADDMASTERDATA);
	}
	
	public ActionForward deleteCustomerDiscount(ActionMapping actionMapping, ActionForm actionForm,
			  HttpServletRequest request , HttpServletResponse response)throws Exception {
		
		// Method name Set for log file usage ;
		String sMethodname = "deleteCustomerDiscount";
		LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"START");
		
		Connection conn = null;  // Connection object to store the database connection
		DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
		CustomerDiscountDto oCustomerDiscountDto = null;
		DaoFactory oDaoFactory=null;
		CustomerDiscountDao oCustomerDiscountDao = null;
		boolean isUpdated = false;
		
		try {
			saveToken(request);// To avoid entering the duplicate value into database.
			
			// Setting user's information in request, using the header information received through request
			setRolesToRequest(request);
			
			/* Instantiating the DBUtility object */
			oDBUtility = new DBUtility();
			/* Retrieving the database connection using DBUtility.getDBConnection method */
			conn = oDBUtility.getDBConnection();
			/* Creating an instance of of DaoFactory  */
			oDaoFactory = new DaoFactory();
			oCustomerDiscountDao = oDaoFactory.getCustomerDiscountDao();
			
			CustomerDiscountForm oCustomerDiscountForm = (CustomerDiscountForm) actionForm;
			if(oCustomerDiscountForm ==  null) {
				oCustomerDiscountForm = new CustomerDiscountForm();
			}
			oCustomerDiscountDto = new CustomerDiscountDto();
			oCustomerDiscountDto.setSerial_id(CommonUtility.getIntParameter(request,QuotationConstants.QUOTATION_SERIAL_ID));
			
			isUpdated = oCustomerDiscountDao.deleteCustomerDiscount(conn, oCustomerDiscountDto);
			
			if(isUpdated)
				request.setAttribute(QuotationConstants.QUOTATION_ADMIN_SUCCESSMESSAGE, " Selected Customer Discount " + QuotationConstants.QUOTATION_REC_DELSUCC);
			
			request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_URL, QuotationConstants.QUOTATION_CUSTOMERDISCOUNT_URL); 
			
		} catch(Exception e) {
			/*
			* Logging any exception that raised during this activity in log files using Log4j
			*/
			LoggerUtility.log("DEBUG", this.getClass().getName(), "deleteCustomerDiscount .", ExceptionUtility.getStackTraceAsString(e));
			/* Setting error details to request to display the same in Exception page */
			request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
			/* Forwarding request to Error page */
			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
		} finally {
			/* Releasing/closing the connection object */
			oDBUtility.releaseResources(conn);
		}
		LoggerUtility.log("INFO", this.getClass().getName(),sMethodname,"END");
				
		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_SUCCESS);
	}
	
	/**
	 * Method to Process the BulkUpload Sheet for Customer Discounts.
	 * @param actionMapping ActionMapping object to redirect the user to next page depending on the activity
     * @param actionForm 	ActionForm object to handle the form elements
     * @param request 		HttpServletRequest object to handle request operations
     * @param response 		HttpServletResponse object to handle response operations
     * @return returns 		ActionForward depending on which user is redirected to next page
     * @throws Exception if any error occurs while performing database operations, etc
	 */
	public ActionForward processBulkUploadCustDiscount(ActionMapping actionMapping, ActionForm actionForm,
			  HttpServletRequest request , HttpServletResponse response)throws Exception {
		
		String sMethodName = "processBulkUploadCustDiscount";
		String sClassName = this.getClass().getName();
		LoggerUtility.log("INFO", sClassName, sMethodName, "Start");
		
		Connection conn = null;  // Connection object to store the database connection
		DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
		
		String sPath = QuotationConstants.QUOTATION_FORWARD_FAILURE;
		ArrayList customerDiscountsList = null;
		Hashtable htBulkLoadReport = null;
		InputStream ipStrCustDiscountLoadFile = null;
		UserAction oUserAction = null; //Instantiating the UserAction object
		
		try {
			/*
			 * Setting user's information in request, using the header 
			 * information received through request
			 */
			setRolesToRequest(request);
			
			/* Instantiating the DBUtility object */
			oDBUtility = new DBUtility();
			/* Retrieving the database connection using DBUtility.getDBConnection method */
			conn = oDBUtility.getDBConnection();
			/* Instantiating the UserAction object */
            oUserAction = new UserAction();
            
            //getting the Logged-in User ID
            UserDto oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
			String loggedInUserSSOId = oUserDto.getUserId();
			
			/* Retrieving the Form object */
			CustomerDiscountForm oCustomerDiscountForm = (CustomerDiscountForm)actionForm;
			if ( oCustomerDiscountForm == null ) {
				sPath = QuotationConstants.APP_FORWARD_FAILURE;
				
			} else {
				LoggerUtility.log("INFO", sClassName, sMethodName, " Customer Discount Bulk Load : Activity Start....");
				
				// Process the Customer Discount Bulk Load File.
				if (oCustomerDiscountForm.getIsFileLoaded() != null ) {
					if (isTokenValid(request)) {
						resetToken(request);
						sPath = "report";
						
						CustomerDiscountBulkLoadManager oCustomerDiscountBulkLoadManager = new CustomerDiscountBulkLoadManager();
						// Retrieving the Customer Discount Details from the Uploaded File.
						FormFile ffCustDiscountLoadFile = oCustomerDiscountForm.getCustDiscountBulkupload();
						if (ffCustDiscountLoadFile != null) {
							ipStrCustDiscountLoadFile = ffCustDiscountLoadFile.getInputStream();
							
							customerDiscountsList = oCustomerDiscountBulkLoadManager.retrieveFromExcel(request, ipStrCustDiscountLoadFile);
							if ( customerDiscountsList != null ) {
								LoggerUtility.log("INFO", sClassName, sMethodName, "Customer Discount Bulk Load : After retrieving records from Excel :: Records count = " + customerDiscountsList.size());
							}
						}
						
						if (customerDiscountsList != null) {
							htBulkLoadReport = oCustomerDiscountBulkLoadManager.processCustDiscountBulkLoadList(conn, customerDiscountsList, loggedInUserSSOId);
							
							if (htBulkLoadReport != null) {
								LoggerUtility.log("INFO", sClassName, sMethodName, "Customer Discount Bulk Load : After Processing CustomerDiscount List : "
										+ " Result = " + (String) htBulkLoadReport.get("RESULT") + " : "
										+ " Records Count = " + (Integer)htBulkLoadReport.get("RECORDCOUNT") + " : "
										+ " Inserted Records Count = " + ((ArrayList) htBulkLoadReport.get("INSERTEDRECORDS")).size() + " : "
										+ " Invalid Records Count = " + ((ArrayList) htBulkLoadReport.get("INVALIDRECORDS")).size() + " : "
										+ " Existing Records Count = " + ((ArrayList) htBulkLoadReport.get("EXISTINGRECORDS")).size() );
								
								/* Retrieving bulk load report details from the Hashtable and setting in the request, to display in confirmation page */
								if (((String) htBulkLoadReport.get("RESULT")).length() > 0) {
									request.setAttribute("sResultString", (String) htBulkLoadReport.get("RESULT"));
									request.setAttribute(QuotationConstants.QUOTATION_ADMIN_SUCCESSMESSAGE, "CustomerDiscount "+QuotationConstants.QUOTATION_REC_BULKLOADSUCC);
								}
								request.setAttribute("recordCount", (Integer)htBulkLoadReport.get("RECORDCOUNT"));
								if (((ArrayList) htBulkLoadReport.get("INSERTEDRECORDS")).size() > 0) {
									request.setAttribute("arlInsertedRecordsList", (ArrayList) htBulkLoadReport.get("INSERTEDRECORDS"));
									request.setAttribute("insertedRecordsCount", ((ArrayList) htBulkLoadReport.get("INSERTEDRECORDS")).size());
								} else {
									request.setAttribute("insertedRecordsCount", 0);
								}
								if (((ArrayList) htBulkLoadReport.get("INVALIDRECORDS")).size() > 0) {
									request.setAttribute("arlInvalidRecordsList", (ArrayList) htBulkLoadReport.get("INVALIDRECORDS"));
									request.setAttribute("invalidRecordsCount", ((ArrayList) htBulkLoadReport.get("INVALIDRECORDS")).size());
								} else {
									request.setAttribute("invalidRecordsCount", 0);
								}
								if (((ArrayList) htBulkLoadReport.get("EXISTINGRECORDS")).size() > 0) {
									request.setAttribute("arlExistingRecordsList", (ArrayList) htBulkLoadReport.get("EXISTINGRECORDS"));
									request.setAttribute("existingsRecordsCount", ((ArrayList) htBulkLoadReport.get("EXISTINGRECORDS")).size());
								} else {
									request.setAttribute("existingsRecordsCount", 0);
								}
								
								request.setAttribute(QuotationConstants.QUOTATION_ADMIN_BACKTOSEARCH_URL, QuotationConstants.QUOTATION_CUSTOMERDISCOUNT_URL); 
							}
						}
						
					} else {
						/* Setting error details to request to display the same in Exception page */ 
						request.setAttribute(QuotationConstants.APP_ERRORMESSAGE, 
						              "Your request could not be processed due to one of the below reasons. Please try again." +
						              "<ul>" +
						              "<li>You clicked on the submit button, more than once, before the response is sent back." +
						              "<li>You clicked on the back button in the browser or simply refreshed the page." +
						              "<li>You accessesed the web page by returning to a previously bookmarked page." +
						              "</ul>");
						sPath = QuotationConstants.APP_FORWARD_FAILURE;
					}
				} else {
					sPath = "report";
				}
			}
			LoggerUtility.log("INFO", sClassName, sMethodName, " Customer Discount Bulk Load : Activity Start....");
			
		} catch (Exception e) {
			e.printStackTrace();
			/*
			* Logging any exception that raised during this activity in log files using Log4j
			*/
			LoggerUtility.log("DEBUG", sClassName, sMethodName, ExceptionUtility.getStackTraceAsString(e));
			
			/* Setting error details to request to display the same in Exception page */
			request.setAttribute(QuotationConstants.APP_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
			
			/* Forwarding request to Error page */
			return actionMapping.findForward(sPath);
		} finally {
			if(ipStrCustDiscountLoadFile != null){
				ipStrCustDiscountLoadFile.close();
			}
			/* Releasing/closing the connection object */
			oDBUtility.releaseResources(conn);
		}
		
		LoggerUtility.log("INFO", sClassName, sMethodName, "End");
		return actionMapping.findForward(sPath);
	}
	
	/**
	 * Method to Perform Export to Excel of the Customer Discount Search Result.
	 * @param actionMapping ActionMapping object to redirect the user to next page depending on the activity
     * @param actionForm 	ActionForm object to handle the form elements
     * @param request 		HttpServletRequest object to handle request operations
     * @param response 		HttpServletResponse object to handle response operations
     * @return returns 		ActionForward depending on which user is redirected to next page
     * @throws Exception if any error occurs while performing database operations, etc
	 */
	public ActionForward exportCustDiscountSearch(ActionMapping actionMapping, ActionForm actionForm,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		String sMethodname = "exportCustDiscountSearch";
		String sClassname = this.getClass().getName();
		LoggerUtility.log("INFO", sClassname, sMethodname, "START");

		// Setting user's information in request, using the header information received
		// through request
		setRolesToRequest(request);

		// Get Form objects from the actionForm
		CustomerDiscountForm oCustomerDiscountForm = (CustomerDiscountForm)actionForm;
		if (oCustomerDiscountForm == null) {
			oCustomerDiscountForm = new CustomerDiscountForm();
		}

		Connection conn = null; // Connection object to store the database connection
		DBUtility oDBUtility = null; // Object of DBUtility class to handle DB connection objects
		HttpSession session = request.getSession();
		String sOperation = null;
		String sWhereQuery = null;
		String sSortFilter = null;
		String sSortOrder = null;
		DaoFactory oDaoFactory = null;
		CustomerDiscountDao oCustomerDiscountDao = null;
		CustomerDiscountDto oCustomerDiscountDto = null;
		ArrayList alSearchResultsList = new ArrayList();
		
		try {
			/* Instantiating the DBUtility object */
			oDBUtility = new DBUtility();
			/* Creating an instance of of DaoFactory */
			oDaoFactory = new DaoFactory();
			/* Retrieving the database connection using DBUtility.getDBConnection method */
			conn = oDBUtility.getDBConnection();
			
			oCustomerDiscountDao = oDaoFactory.getCustomerDiscountDao();
			oCustomerDiscountDto = new CustomerDiscountDto();
			
			sWhereQuery = (String) session.getAttribute(QuotationConstants.RBC_QUOTATION_CUSTDISCOUNT_QUERY);
			sOperation = CommonUtility.getStringParameter(request, QuotationConstants.QUOTATION_FUNCTIONTYPES);

			if (session.getAttribute(QuotationConstants.QUOTATION_SEARCH_SORTFIELD) != null)
				sSortFilter = (String) session.getAttribute(QuotationConstants.QUOTATION_SEARCH_SORTFIELD);

			if (session.getAttribute(QuotationConstants.QUOTATION_SEARCH_SORTORDER) != null)
				sSortOrder = (String) session.getAttribute(QuotationConstants.QUOTATION_SEARCH_SORTORDER);
			
			/* Copying the Form Object into DTO Object via Propertyutils */
			PropertyUtils.copyProperties(oCustomerDiscountDto, oCustomerDiscountForm);
			
			/* Retrieving the search results using oHelpDto.getHelpList method */
			alSearchResultsList = oCustomerDiscountDao.exportCustDiscountSearchResult(conn, oCustomerDiscountDto, sWhereQuery, sSortFilter, sSortOrder);  
			
			request.setAttribute(QuotationConstants.QUOTATION_ADMIN_EXPORTLIST, alSearchResultsList);
			request.setAttribute(QuotationConstants.QUOTATION_ADMIN_EXPORTTYPE, QuotationConstants.QUOTATION_ADMIN_EXPORT_CUSTDISCOUNT);
			
		} catch (Exception e) {
			/* Logging any exception that raised during this activity in log files using Log4j */
			LoggerUtility.log("DEBUG", this.getClass().getName(), "export CustDiscount Search Result.", ExceptionUtility.getStackTraceAsString(e));
			/* Setting error details to request to display the same in Exception page */
			request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
			/* Forwarding request to Error page */
			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
		} finally {
			/* Releasing/closing the connection object */
			oDBUtility.releaseResources(conn);
		}

		LoggerUtility.log("INFO", sClassname, sMethodname, "END");

		if (sOperation.equalsIgnoreCase(QuotationConstants.QUOTATION_FORWARD_EXPORT))
			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_EXPORT);
		else if (sOperation.equalsIgnoreCase(QuotationConstants.QUOTATION_FORWARD_PRINT))
			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_PRINT);
		else
			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
	}
	
}
