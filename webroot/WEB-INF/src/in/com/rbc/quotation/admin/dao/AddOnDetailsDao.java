package in.com.rbc.quotation.admin.dao;

import java.sql.Connection;
import java.util.ArrayList;

import in.com.rbc.quotation.admin.dto.AddOnDetailsDto;
import in.com.rbc.quotation.common.vo.KeyValueVo;
import in.com.rbc.quotation.enquiry.dto.NewRatingDto;

public interface AddOnDetailsDao {

	public AddOnDetailsDto fetchAddOnPriceDetails(Connection conn, AddOnDetailsDto oAddOnDetailsDto) throws Exception;
	
	public AddOnDetailsDto fetchAddOnDetails(Connection conn, AddOnDetailsDto oAddOnDetailsDto) throws Exception;
	
	public AddOnDetailsDto fetchMTODetailsList(Connection conn, AddOnDetailsDto oAddOnDetailsDto) throws Exception;
	
	public void updateAddOnMTODetails(Connection conn, NewRatingDto oNewRatingDto, String loggedInUserId) throws Exception;
	
}
