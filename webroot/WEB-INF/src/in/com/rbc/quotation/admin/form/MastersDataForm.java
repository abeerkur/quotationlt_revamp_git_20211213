/**
 * *****************************************************************************
 * Project Name: quotation
 * Document Name: CustomerForm.java
 * Package: in.com.rbc.quotation.admin.form
 * Desc: 
 * *****************************************************************************
 * Author: 100005701 (Suresh)
 * Date: Sep 30, 2008
 * *****************************************************************************
 */
package in.com.rbc.quotation.admin.form;

import java.util.ArrayList;

import in.com.rbc.quotation.common.utility.CommonUtility;

import org.apache.struts.action.ActionForm;

/**
 * @author 100005701 (Suresh Shanmugam)
 *
 */
public class MastersDataForm extends ActionForm {
	
	
	private String invoke ;
	private String masterId;
	private String master;
	private String addOrUpdate;
	private String engineeringId;
	private String engineeringName;
	private String status;
	
	public String getAddOrUpdate() {
		return CommonUtility.replaceNull(addOrUpdate);
	}
	public void setAddOrUpdate(String addOrUpdate) {
		this.addOrUpdate = addOrUpdate;
	}
	public String getInvoke() {
		return CommonUtility.replaceNull(invoke);
	}
	public void setInvoke(String invoke) {
		this.invoke = invoke;
	}
	public String getMaster() {
		return CommonUtility.replaceNull(master);
	}
	public void setMaster(String master) {
		this.master = master;
	}
	public String getMasterId() {
		return CommonUtility.replaceNull(masterId);
	}
	public void setMasterId(String masterId) {
		this.masterId = masterId;
	}
	public String getEngineeringId() {
		return CommonUtility.replaceNull(engineeringId);
	}
	public void setEngineeringId(String engineeringId) {
		this.engineeringId = engineeringId;
	}
	public String getEngineeringName() {
		return CommonUtility.replaceNull(engineeringName);
	}
	public void setEngineeringName(String engineeringName) {
		this.engineeringName = engineeringName;
	}
	public String getStatus() {
		return CommonUtility.replaceNull(status);
	}
	public void setStatus(String status) {
		this.status = status;
	}}
