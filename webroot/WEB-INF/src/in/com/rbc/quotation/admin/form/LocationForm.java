/**
 * *****************************************************************************
 * Project Name: quotation
 * Document Name: LocationForm.java
 * Package: in.com.rbc.quotation.admin.form
 * Desc: 
 * *****************************************************************************
 * Author: 100005701 (Suresh)
 * Date: Oct 3, 2008
 * *****************************************************************************
 */
package in.com.rbc.quotation.admin.form;

import in.com.rbc.quotation.common.utility.CommonUtility;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

/**
 * @author 100005701 (Suresh Shanmugam)
 *
 */
public class LocationForm extends ActionForm {
	
	private String locationName= null;
	private String regionName=null;
	private String status ;
	private int locationId = 0;
	private int regionId = 0;
	private String invoke = null;
	private String operation;
	
	private ArrayList regionList = null;
	private ArrayList searchResultsList = null ;

	

	/**
	 * @return Returns the searchResultsList.
	 */
	public ArrayList getSearchResultsList() {
		return this.searchResultsList;
	}

	/**
	 * @param searchResultsList The searchResultsList to set.
	 */
	public void setSearchResultsList(ArrayList searchResultsList) {
		this.searchResultsList = searchResultsList;
	}

	/**
	 * @return Returns the invoke.
	 */
	public String getInvoke() {
		return CommonUtility.replaceNull(this.invoke);
	}

	/**
	 * @param invoke The invoke to set.
	 */
	public void setInvoke(String invoke) {
		this.invoke = invoke;
	}

	/**
	 * @return Returns the locationId.
	 */
	public int getLocationId() {
		return this.locationId;
	}

	/**
	 * @param locationId The locationId to set.
	 */
	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}

	/**
	 * @return Returns the locationName.
	 */
	public String getLocationName() {
		return CommonUtility.replaceNull(this.locationName);
	}

	/**
	 * @param locationName The locationName to set.
	 */
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	/**
	 * @return Returns the operation.
	 */
	public String getOperation() {
		return CommonUtility.replaceNull(this.operation);
	}

	/**
	 * @param operation The operation to set.
	 */
	public void setOperation(String operation) {
		this.operation = operation;
	}

	/**
	 * @return Returns the regionId.
	 */
	public int getRegionId() {
		return this.regionId;
	}

	/**
	 * @param regionId The regionId to set.
	 */
	public void setRegionId(int regionId) {
		this.regionId = regionId;
	}

	/**
	 * @return Returns the regionList.
	 */
	public ArrayList getRegionList() {
		return this.regionList;
	}

	/**
	 * @param regionList The regionList to set.
	 */
	public void setRegionList(ArrayList regionList) {
		this.regionList = regionList;
	}

	/**
	 * @return Returns the regionName.
	 */
	public String getRegionName() {
		return CommonUtility.replaceNull(this.regionName);
	}

	/**
	 * @param regionName The regionName to set.
	 */
	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	/**
	 * @return Returns the status.
	 */
	public String getStatus() {
		return CommonUtility.replaceNull(this.status);
	}

	/**
	 * @param status The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

}
