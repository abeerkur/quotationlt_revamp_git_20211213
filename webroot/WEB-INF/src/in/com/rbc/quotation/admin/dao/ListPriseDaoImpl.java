package in.com.rbc.quotation.admin.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Hashtable;

import in.com.rbc.quotation.admin.dto.CustomerDiscountBulkLoadDto;
import in.com.rbc.quotation.admin.dto.ListPriceBulkLoadDto;
import in.com.rbc.quotation.admin.dto.ListPriceDto;
import in.com.rbc.quotation.admin.dto.LocationDto;
import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.utility.DBUtility;
import in.com.rbc.quotation.common.utility.ListDBColumnUtil;
import in.com.rbc.quotation.common.utility.ListModel;
import in.com.rbc.quotation.common.utility.LoggerUtility;
import in.com.rbc.quotation.common.vo.KeyValueVo;
import in.com.rbc.quotation.enquiry.dao.EnquiryQueries;

public class ListPriseDaoImpl implements ListPriseDao,AdminQueries,EnquiryQueries {
	
	public Hashtable getListPriseSearchResult(Connection conn, ListPriceDto oListPriceDto, ListModel oListModel) throws Exception{
		
		/*
    	* Setting the Method Name as generic for logger Utility purpose . 
    	*/
		String sMethodName = "getListPriseSearchResult Method" ;
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;      
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
       
        /*
         * Hashtable that stores the following values, and that is used to display results in search page
         * --> ArrayList containing the rows retrieved from database
         * --> ListModel object that hold the values used to retrieve values and display the same
         * --> String array containing the list of column names to be displayed as column headings
         */
        Hashtable htSearchResults = null;
        
        ArrayList alSearchResultsList = new ArrayList(); // Stores the list of rows retrieved from the database
        
        
        int iCursorPosition = 0; // Stores the cursor position from where the records have to be retrieved // from database; this is dependent on the page number selected for viewing
        
        String sWhereString = null; // Stores the Where clause condition
        String sSqlQueryString = null; // Stores the complete SQL query to be executed
        ListPriceDto oTempListPriceDto = null ;// Create the Temporary Location Object for storing the table column values 

        try {
        	
        	sWhereString = buildWhereClause(oListPriceDto);
        	sSqlQueryString = COUNT_LISTPRISE_SEARCHRESULT + sWhereString;
        	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName+"..Search getListPriseSearchResult data Count Query",sSqlQueryString);
        	
        	pstmt = conn.prepareCall(sSqlQueryString);
        	
        	/* Execute the Query for counting the number records*/
        	rs = pstmt.executeQuery();
        	
        	if(rs.next()) {
        		oListModel.setTotalRecordCount(rs.getInt(1));//set total number of records available in table depond upon the where conditions .
        	} else {
        		oListModel.setTotalRecordCount(0);// if the Query return no records , it's set the Total recordcount  "0" as default
        	}
        	oDBUtility.releaseResources(rs, pstmt);
        	 
        	if(oListModel.getTotalRecordCount()>0) {
        		htSearchResults = new Hashtable();
        		 
        		if(oListModel.getCurrentPageInt()<0)
        			throw new Exception("Current Page is negative in search method...");
        		 
        		iCursorPosition = (oListModel.getCurrentPageInt()-1) * oListModel.getRecordsPerPageInt();
        		 
        		if(iCursorPosition < oListModel.getTotalRecordCount()) {
             		
					 /*
					  * Select the values from the table Using FETCH_SEARCHLocation_RESULTS Query depond upon the 
					  * sWhereString condition and return into the sSqlQueryString as StringBuffer 
					  * and the data's are ordered by Location DBFiled as by default
					  * 
					  */
             		sSqlQueryString = new StringBuffer("SELECT * FROM (SELECT Q.*, rownum AS rnum FROM ( ")
 						                    .append(FETCH_LISTPRISECOUNT_LIST)
 						                    .append(sWhereString)
 						                    .append(" ORDER BY ")
 						                    .append(ListDBColumnUtil.getTechnicalDataResultDBField(oListModel.getSortBy()))
 						                    .append(" " + oListModel.getSortOrderDesc())
 						                    .append(" ) Q WHERE rownum <= (")
 						                    .append(iCursorPosition + " + " + oListModel.getRecordsPerPage() + ")) ")
 						                    .append(" WHERE rnum > " + iCursorPosition + "")
 						                    .toString();
             		

             		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName+"..Search List Price Retrive Query",sSqlQueryString );
             		pstmt = conn.prepareStatement(sSqlQueryString, ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
             		
             		rs =pstmt.executeQuery();// Execute the Search Query to retriuve the Location Values .
             		if (rs.next()) {
                         do {
                        	 oTempListPriceDto = new ListPriceDto();
                        	 oTempListPriceDto.setSerial_id(rs.getInt("SERIAL_ID"));
                        	 oTempListPriceDto.setManufacturing_location(rs.getString("MANUFACTURING_LOCATION"));
                        	 oTempListPriceDto.setProduct_group(rs.getString("PRODUCT_GROUP"));	
                        	 oTempListPriceDto.setProduct_line(rs.getString("PRODUCT_LINE"));	
                        	 oTempListPriceDto.setStandard(rs.getString("STANDARD"));	
                        	 oTempListPriceDto.setPower_rating_kw(rs.getString("POWER_RATING_KW"));
                        	 oTempListPriceDto.setFrame_type(rs.getString("FRAME_TYPE"));
                        	 oTempListPriceDto.setFrame_suffix(rs.getString("FRAME_SUFFIX"));
                        	 oTempListPriceDto.setNumber_of_poles(rs.getInt("NUMBER_OF_POLES"));
                        	 oTempListPriceDto.setMounting_type(rs.getString("MOUNTING_TYPE"));
                        	 oTempListPriceDto.setTb_position(rs.getString("TB_POSITION"));
                        	 oTempListPriceDto.setList_price(rs.getString("LIST_PRISE"));
                        	 oTempListPriceDto.setMaterial_cost(rs.getString("MATERIAL_COST"));
                        	 oTempListPriceDto.setLoh(rs.getString("LOH"));
                        	 
                        	 alSearchResultsList.add(oTempListPriceDto);
                        } while (rs.next());
                     }
               }
        		 
        		 htSearchResults.put("resultsList", alSearchResultsList);	// put the SearchresultList of arraylist value into Hashtable 
                 htSearchResults.put("ListModel", oListModel); 				// put the ListModel values into hashtable 
                 htSearchResults.put("listPriceQuery", sWhereString); 		// keep the Where condition for using in Export to Excel purpose .
             }
        	 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END.." );
        }finally {
        	 /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
        return htSearchResults ;
		
	}
	
	private String buildWhereClause(ListPriceDto oListPriceDto) {
		
		String sMethodName = "buildWhereClause";
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
        
        StringBuffer sbWhereString = new StringBuffer(" AND");
        
        // Manufacturing Location 
        if (oListPriceDto.getManufacturing_location() != null && oListPriceDto.getManufacturing_location().trim().length() > 0 && !QuotationConstants.QUOTATION_ZERO.equals(oListPriceDto.getManufacturing_location().trim()) ){
        	sbWhereString.append(" UPPER(LP.MANUFACTURING_LOCATION) LIKE UPPER('%"+oListPriceDto.getManufacturing_location().trim().toUpperCase()+"%')");
        	sbWhereString.append(" AND");
        }
        
        // Power Rating (KW)
        if (oListPriceDto.getPower_rating_kw() != null && oListPriceDto.getPower_rating_kw().trim().length() > 0 && !QuotationConstants.QUOTATION_ZERO.equals(oListPriceDto.getPower_rating_kw().trim()) ){
        	sbWhereString.append(" UPPER(LP.POWER_RATING_KW) LIKE UPPER('%"+oListPriceDto.getPower_rating_kw().trim().toUpperCase()+"%')");
        	sbWhereString.append(" AND");
        }
        
        // Product Group
        if (oListPriceDto.getProduct_group() != null && oListPriceDto.getProduct_group().trim().length() > 0 && !QuotationConstants.QUOTATION_ZERO.equals(oListPriceDto.getProduct_group().trim()) ){
        	sbWhereString.append(" UPPER(LP.PRODUCT_GROUP) LIKE UPPER('%"+oListPriceDto.getProduct_group().trim().toUpperCase()+"%')");
        	sbWhereString.append(" AND");
        }
        
        // Product Line
        if (oListPriceDto.getProduct_line() != null && oListPriceDto.getProduct_line().trim().length() > 0 && !QuotationConstants.QUOTATION_ZERO.equals(oListPriceDto.getProduct_line().trim()) ){
        	sbWhereString.append(" UPPER(LP.PRODUCT_LINE) LIKE UPPER('%"+oListPriceDto.getProduct_line().trim().toUpperCase()+"%')");
        	sbWhereString.append(" AND");
        }
        
        // Frame Type
        if (oListPriceDto.getFrame_type() != null && oListPriceDto.getFrame_type().trim().length() > 0 && !QuotationConstants.QUOTATION_ZERO.equals(oListPriceDto.getFrame_type().trim()) ){
        	sbWhereString.append(" UPPER(LP.FRAME_TYPE) LIKE UPPER('%"+oListPriceDto.getFrame_type().trim().toUpperCase()+"%')");
        	sbWhereString.append(" AND");
        }
        
        // Frame Suffix
        if (oListPriceDto.getFrame_suffix() != null && oListPriceDto.getFrame_suffix().trim().length() > 0 && !QuotationConstants.QUOTATION_ZERO.equals(oListPriceDto.getFrame_suffix().trim()) ){
        	sbWhereString.append(" UPPER(LP.FRAME_SUFFIX) LIKE UPPER('%"+oListPriceDto.getFrame_suffix().trim().toUpperCase()+"%')");
        	sbWhereString.append(" AND");
        }
        
        if ( sbWhereString.toString().trim().equalsIgnoreCase(" AND") )
            return "";
        else if (sbWhereString.toString().trim().endsWith(" AND"))        	
            return sbWhereString.toString().substring(0, sbWhereString.toString().lastIndexOf("AND"));        
        else
        	return "";
		
	}
	
	public ListPriceDto loadLookUpValues(Connection conn, ListPriceDto oListPriceDto) throws Exception
	{
		oListPriceDto.setMaufacturingList(getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_MFG_LOCATION));
		oListPriceDto.setProductGroupList(getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LT_PRODGROUP));
		oListPriceDto.setProductLineList(getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LT_PRODLINE));
		oListPriceDto.setStandardList(getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_STANDARD));
		oListPriceDto.setPowerratingkwList(getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LT_KW));
		oListPriceDto.setFrametypelist(getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LT_FRAME));
		oListPriceDto.setFramesuffixlist(getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_FRAME_SUFFIX));
		oListPriceDto.setNumberofpoleslist(getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LT_POLE));
		oListPriceDto.setMountingyypelist(getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LT_MOUNTING));
		oListPriceDto.setTbpositionlist(getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LT_TBPOS));
		
		return oListPriceDto;
	}
	
	
	public ArrayList<KeyValueVo> getOptionsbyAttributeListByLookup(Connection conn,String sLookupField) throws Exception {

		/**
		 * Setting the Method Name as generic for logger Utility purpose .
		 */
		String sMethodName = "getOptionsbyAttributeList";
		String sClassName = this.getClass().getName();

		LoggerUtility.log("INFO", sClassName, sMethodName,
				"************************START******************");

		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null;

		/* ResultSet object to store the rows retrieved from database */
		ResultSet rs = null;

		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();

		ArrayList<KeyValueVo> arlOptions = new ArrayList<KeyValueVo>();		
		KeyValueVo oKeyValueVo = null;

		try {
			
			switch(sLookupField) {
			case "MANUFACTURING_LOCATION":
			pstmt = conn.prepareStatement(FETCH_TD_MANUFACTURING);
			rs = pstmt.executeQuery();
			if (rs.next()) {				
				do {
					oKeyValueVo = new KeyValueVo();
					oKeyValueVo.setKey(rs.getString(sLookupField));
					oKeyValueVo.setValue(rs.getString(sLookupField));
					arlOptions.add(oKeyValueVo);
				} while (rs.next());
			}
			break;
			
			case "PRODUCT_GROUP":
				pstmt = conn.prepareStatement(FETCH_TD_PRODUCT_GROUP);
				rs = pstmt.executeQuery();
				if (rs.next()) {				
					do {
						oKeyValueVo = new KeyValueVo();
						oKeyValueVo.setKey(rs.getString(sLookupField));
						oKeyValueVo.setValue(rs.getString(sLookupField));
						arlOptions.add(oKeyValueVo);
					} while (rs.next());
				}
				break;
				
			case "PRODUCT_LINE":
				pstmt = conn.prepareStatement(FETCH_TD_PRODUCT_LINE);
				rs = pstmt.executeQuery();
				if (rs.next()) {				
					do {
						oKeyValueVo = new KeyValueVo();
						oKeyValueVo.setKey(rs.getString(sLookupField));
						oKeyValueVo.setValue(rs.getString(sLookupField));
						arlOptions.add(oKeyValueVo);
					} while (rs.next());
				}
				break;
				
			case "STANDARD":
				pstmt = conn.prepareStatement(FETCH_TD_STANDARD);
				rs = pstmt.executeQuery();
				if (rs.next()) {				
					do {
						oKeyValueVo = new KeyValueVo();
						oKeyValueVo.setKey(rs.getString(sLookupField));
						oKeyValueVo.setValue(rs.getString(sLookupField));
						arlOptions.add(oKeyValueVo);
					} while (rs.next());
				}
				break;
				
			case "POWER_RATING_KW":
				pstmt = conn.prepareStatement(FETCH_TD_POWER_RATING_KW);
				rs = pstmt.executeQuery();
				if (rs.next()) {				
					do {
						oKeyValueVo = new KeyValueVo();
						oKeyValueVo.setKey(rs.getString(sLookupField));
						oKeyValueVo.setValue(rs.getString(sLookupField));
						arlOptions.add(oKeyValueVo);
					} while (rs.next());
				}
				break;
				
				case "FRAME_TYPE":
					pstmt = conn.prepareStatement(FETCH_TD_FRAME_TYPE);
					rs = pstmt.executeQuery();
					if (rs.next()) {				
						do {
							oKeyValueVo = new KeyValueVo();
							oKeyValueVo.setKey(rs.getString(sLookupField));
							oKeyValueVo.setValue(rs.getString(sLookupField));
							arlOptions.add(oKeyValueVo);
						} while (rs.next());
					}
					break;
					
				case "NUMBER_OF_POLES":
					pstmt = conn.prepareStatement(FETCH_TD_NUMBER_OF_POLES);
					rs = pstmt.executeQuery();
					if (rs.next()) {				
						do {
							oKeyValueVo = new KeyValueVo();
							oKeyValueVo.setKey(rs.getString(sLookupField));
							oKeyValueVo.setValue(rs.getString(sLookupField));
							arlOptions.add(oKeyValueVo);
						} while (rs.next());
					}
					break;
					
				case "MOUNTING_TYPE":
					pstmt = conn.prepareStatement(FETCH_TD_MOUNTING_TYPE);
					rs = pstmt.executeQuery();
					if (rs.next()) {				
						do {
							oKeyValueVo = new KeyValueVo();
							oKeyValueVo.setKey(rs.getString(sLookupField));
							oKeyValueVo.setValue(rs.getString(sLookupField));
							arlOptions.add(oKeyValueVo);
						} while (rs.next());
					}
					break;
					
				case "TB_POSITION":
					pstmt = conn.prepareStatement(FETCH_TD_TB_POSITION);
					rs = pstmt.executeQuery();
					if (rs.next()) {				
						do {
							oKeyValueVo = new KeyValueVo();
							oKeyValueVo.setKey(rs.getString(sLookupField));
							oKeyValueVo.setValue(rs.getString(sLookupField));
							arlOptions.add(oKeyValueVo);
						} while (rs.next());
					}
					break;
			case "":
				LoggerUtility.log("INFO", sClassName, sMethodName, "****** EMPTY ******");
				//System.out.println("empty..");
			}
			
		} finally {
			/* Releasing ResultSet & PreparedStatement objects */
			oDBUtility.releaseResources(rs, pstmt);
		}

		LoggerUtility.log("INFO", sClassName, sMethodName,
				"************************END******************");

		return arlOptions;

	}
	
	public boolean addListPrice(Connection conn, ListPriceDto oListPriceDto) throws Exception
	{
		/*
		 * Setting the Method Name as generic for logger Utility purpose .
		 */
		String sMethodName = "addNewTechnicalData" ;
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
		boolean isUpdate = false;
		int serial_id = 0;
		
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null ;
		PreparedStatement pstmt1 = null ;
		/* ResultSet object to store the rows retrieved from database */
		ResultSet rs = null ;      
       
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();
		
		try {
    	   /**
    	    * Getting the technical data serial id from the table
       	 	*/
			pstmt1 = conn.prepareStatement(CREATE_LIST_PRISE_ID);
			rs = pstmt1.executeQuery();// Execute the creating the Query
       	
	       	if(rs.next()) {
	       		serial_id = rs.getInt("MAX_VAL");
	       	}
       	
	       	pstmt = conn.prepareStatement(INSERT_LIST_PRISE);
	       	pstmt.setInt(1, serial_id);
	       	pstmt.setString(2, oListPriceDto.getManufacturing_location());
	       	pstmt.setString(3, oListPriceDto.getProduct_group());
	       	pstmt.setString(4, oListPriceDto.getProduct_line());
	       	pstmt.setString(5, oListPriceDto.getStandard());
	       	pstmt.setString(6, oListPriceDto.getPower_rating_kw());
	       	pstmt.setString(7, oListPriceDto.getFrame_type());
	       	pstmt.setString(8, oListPriceDto.getFrame_suffix());
	       	pstmt.setInt(9, oListPriceDto.getNumber_of_poles());
	       	pstmt.setString(10, oListPriceDto.getMounting_type());
	       	pstmt.setString(11, oListPriceDto.getTb_position());
	       	pstmt.setString(12, oListPriceDto.getList_price());
	       	pstmt.setString(13, oListPriceDto.getMaterial_cost());
	       	pstmt.setString(14, oListPriceDto.getLoh());
       	
	        int iUpdatedCount = pstmt.executeUpdate();
	        if(iUpdatedCount > 0 ) {
	        	isUpdate = true;
	        }
    	   
		} finally {
			/* Releasing PreparedStatement objects */
           oDBUtility.releaseResources( rs,pstmt1);
           oDBUtility.releaseResources(pstmt);
		}
       LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );
       
	   return isUpdate;
	}
	
	public boolean updateListPrice(Connection conn, ListPriceDto oListPriceDto) throws Exception {
	
		/*
		 * Setting the Method Name as generic for logger Utility purpose . 
		 */
		String sMethodName = "updateListPrice" ;
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
		boolean isUpdate = false;
		
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null ;
       
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();
		
		try {
	       	pstmt = conn.prepareStatement(UPDATE_LIST_PRISE);
	       	pstmt.setString(1, oListPriceDto.getManufacturing_location());
	       	pstmt.setString(2, oListPriceDto.getProduct_group());
	       	pstmt.setString(3, oListPriceDto.getProduct_line());
	       	pstmt.setString(4, oListPriceDto.getStandard());
	       	pstmt.setString(5, oListPriceDto.getPower_rating_kw());
	       	pstmt.setString(6, oListPriceDto.getFrame_type());
	       	pstmt.setString(7, oListPriceDto.getFrame_suffix());
	       	pstmt.setInt(8, oListPriceDto.getNumber_of_poles());
	       	pstmt.setString(9, oListPriceDto.getMounting_type());
	       	pstmt.setString(10, oListPriceDto.getTb_position());
	       	pstmt.setString(11, oListPriceDto.getList_price());
	       	pstmt.setString(12, oListPriceDto.getMaterial_cost());
	       	pstmt.setString(13, oListPriceDto.getLoh());
	       	pstmt.setInt(14, oListPriceDto.getSerial_id());
	       	
	        int iUpdatedCount = pstmt.executeUpdate();
	        if(iUpdatedCount > 0 ) {
	        	isUpdate = true;
	        }
    	   
       } finally {
       		/* Releasing PreparedStatement objects */
           	oDBUtility.releaseResources(pstmt);
       }
       LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );
       
       return isUpdate;
	}
	
	public ListPriceDto getListPrice(Connection conn, ListPriceDto oListPriceDto) throws Exception {
		
		/*
    	* Setting the Method Name as generic for logger Utility purpose . 
    	*/
		String sMethodName = "getListPrice Mehotd" ;
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;      
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();

        try {
             		pstmt = conn.prepareStatement(FETCH_LIST_PRISE);
             		pstmt.setInt(1, oListPriceDto.getSerial_id());
             		
             		rs =pstmt.executeQuery();
             		if (rs.next()) {
                        
             			oListPriceDto.setSerial_id(rs.getInt("SERIAL_ID"));
             			oListPriceDto.setManufacturing_location(rs.getString("MANUFACTURING_LOCATION"));
             			oListPriceDto.setProduct_group(rs.getString("PRODUCT_GROUP"));	
             			oListPriceDto.setProduct_line(rs.getString("PRODUCT_LINE"));	
             			oListPriceDto.setStandard(rs.getString("STANDARD"));	
             			oListPriceDto.setPower_rating_kw(rs.getString("POWER_RATING_KW"));
             			oListPriceDto.setFrame_type(rs.getString("FRAME_TYPE"));	
             			oListPriceDto.setFrame_suffix(rs.getString("FRAME_SUFFIX"));
             			oListPriceDto.setNumber_of_poles(rs.getInt("NUMBER_OF_POLES"));
             			oListPriceDto.setMounting_type(rs.getString("MOUNTING_TYPE"));
             			oListPriceDto.setTb_position(rs.getString("TB_POSITION"));
             			oListPriceDto.setList_price(rs.getString("LIST_PRISE"));
             			oListPriceDto.setMaterial_cost(rs.getString("MATERIAL_COST"));
             			oListPriceDto.setLoh(rs.getString("LOH"));
                     }
        		 
        	 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END.." );
        }finally {
        	 /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
        return oListPriceDto ;
		
	}
	
	public boolean deleteListPrice(Connection conn, ListPriceDto oListPriceDto) throws Exception{
		/*
    	 * Setting the Method Name as generic for logger Utility purpose . 
    	 */
		String sMethodName = "deleteListPrice" ;
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );

    	/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
		boolean isDeleted = false ;
		int iDelete=0;
		try {
			pstmt = conn.prepareStatement(DELETE_LISTPRICE);
			pstmt.setInt(1, oListPriceDto.getSerial_id());
			iDelete = pstmt.executeUpdate();
	        	if(iDelete > 0) {
	        		isDeleted = true ;
	        	}
	        }finally {
	        	/* Releasing PreparedStatement objects */
	            oDBUtility.releaseResources(pstmt);
	        }
	        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );
		return isDeleted;
	}

	@Override
	public ListPriceDto fetchListPrice(Connection conn, ListPriceDto oListPriceDto) throws Exception {
		String sMethodName = "fetchListPrice" ;
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
		Double listPriceValue = 0d;
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;      
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        /* Set Default List Price to '0'. */
        oListPriceDto.setListPriceValue(listPriceValue);
        
        try {
        	if(QuotationConstants.QUOTATION_EMPTY.equals(oListPriceDto.getProduct_line())
        			|| QuotationConstants.QUOTATION_EMPTY.equals(oListPriceDto.getPower_rating_kw())
        			|| QuotationConstants.QUOTATION_EMPTY.equals(oListPriceDto.getFrame_type())
        			|| oListPriceDto.getNumber_of_poles() == 0
        			|| QuotationConstants.QUOTATION_EMPTY.equals(oListPriceDto.getMounting_type())
        			|| QuotationConstants.QUOTATION_EMPTY.equals(oListPriceDto.getTb_position()) ) {
        		
        		return oListPriceDto;
        	
        	} else {
        		pstmt = conn.prepareStatement(FETCH_LIST_PRISE_FOR_ENQUIRY);
        		pstmt.setString(1, oListPriceDto.getProduct_line().trim());
        		pstmt.setString(2, oListPriceDto.getPower_rating_kw().trim());
        		pstmt.setString(3, oListPriceDto.getFrame_type().trim());
        		pstmt.setString(4, oListPriceDto.getFrame_suffix().trim());
        		pstmt.setInt(5, oListPriceDto.getNumber_of_poles());
        		pstmt.setString(6, oListPriceDto.getMounting_type().trim());
        		pstmt.setString(7, oListPriceDto.getTb_position().trim());
        		
        		rs = pstmt.executeQuery();
        		
        		if (rs.next()) {
        			listPriceValue = Double.parseDouble(rs.getString("LIST_PRISE").trim());
        			oListPriceDto.setListPriceValue(listPriceValue);
        			// Set MC and LOH.
        			oListPriceDto.setMaterial_cost(rs.getString("MATERIAL_COST"));
        			oListPriceDto.setLoh(rs.getString("LOH"));
        		}
        		
        	}
        } finally {
        	/* Releasing PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
        
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );
		return oListPriceDto;
	}
	
	/**
	 * To fetch the list of values in Ascending Order from ENQUIRY PICKLIST table for given lookup field
	 * @param conn Connection object to connect to database
	 * @param lookupfield string - query parameter
	 * @return Returns ArrayList object
	 * @throws Exception If any error occurs during the process
	 */
	public ArrayList<KeyValueVo> getAscendingOptionsbyAttributeList(Connection conn,String sLookupField) throws Exception {
		String sMethodName = "getAscendingOptionsbyAttributeList";
		String sClassName = this.getClass().getName();
		LoggerUtility.log("INFO", sClassName, sMethodName, "************************START******************");

		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null;
		/* ResultSet object to store the rows retrieved from database */
		ResultSet rs = null;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();

		ArrayList<KeyValueVo> arlOptions = new ArrayList<KeyValueVo>();		
		KeyValueVo oKeyValueVo = null;

		try {			
			pstmt = conn.prepareStatement(FETCH_ENQUIRY_LIST_ASC);
			pstmt.setString(1, sLookupField);			
			rs = pstmt.executeQuery();
			if (rs.next()) {				
				do {
					oKeyValueVo = new KeyValueVo();
					oKeyValueVo.setKey(rs.getString("QEM_KEY"));
					oKeyValueVo.setValue(rs.getString("QEM_VALUE"));
					arlOptions.add(oKeyValueVo);
				} while (rs.next());
			}
		} finally {
			/* Releasing ResultSet & PreparedStatement objects */
			oDBUtility.releaseResources(rs, pstmt);
		}

		LoggerUtility.log("INFO", sClassName, sMethodName, "************************END******************");
		return arlOptions;
	}
	
	public KeyValueVo isListPriceExists(Connection conn, ListPriceBulkLoadDto oListPriceBulkLoadDto) throws Exception {

		LoggerUtility.log("INFO", this.getClass().getName(), "isListPriceExists", "Start");

		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null;
		/* ResultSet object to store the rows retrieved from database */
		ResultSet rs = null;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();

		KeyValueVo oKeyValueVo = null;
		
		try {
            pstmt = conn.prepareStatement(FETCH_LIST_PRISE_WITH_PRICEVALUE);
            pstmt.setString(1, oListPriceBulkLoadDto.getMfgLocation().trim());
    		pstmt.setString(2, oListPriceBulkLoadDto.getProductGroup().trim());
    		pstmt.setString(3, oListPriceBulkLoadDto.getProductLine().trim());
    		pstmt.setString(4, oListPriceBulkLoadDto.getPowerRatingKW().trim());
    		pstmt.setString(5, oListPriceBulkLoadDto.getFrameType().trim());
    		pstmt.setString(6, oListPriceBulkLoadDto.getFrameSuffix().trim());
    		pstmt.setInt(7, oListPriceBulkLoadDto.getNoOfPoles());
    		pstmt.setString(8, oListPriceBulkLoadDto.getMountType().trim());
    		pstmt.setString(9, oListPriceBulkLoadDto.getTbPosition().trim());
    		pstmt.setString(10, oListPriceBulkLoadDto.getListPrice().trim());
            rs = pstmt.executeQuery();
            if (rs.next()) {
                oKeyValueVo = new KeyValueVo();
                oKeyValueVo.setKey(rs.getString("SERIAL_ID"));
                oKeyValueVo.setValue(rs.getString("LIST_PRISE"));
            }
		} finally {
			/* Releasing ResultSet & PreparedStatement objects */
			oDBUtility.releaseResources(rs, pstmt);
		}
		
		LoggerUtility.log("INFO", this.getClass().getName(), "isListPriceExists", "End");
        return oKeyValueVo;
	}
	
	public ArrayList exportListPriceSearchResult(Connection conn, ListPriceDto oListPriseDto, String sWhereQuery, String sSortFilter, String sSortOrder) throws Exception {
		
		LoggerUtility.log("INFO", this.getClass().getName(), "exportListPriceSearchResult", "Start");
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;      
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        ArrayList alSearchResultsList = new ArrayList(); // Stores the list of rows retrieved from the database
        String sSqlQueryString = null; 					// Stores the complete SQL query to be executed
        ListPriceDto oTempListPriceDto = null ;			// Create the Temporary Location Object for storing the table column values
        
        try { 
        	sSqlQueryString = FETCH_LISTPRISECOUNT_LIST;
        	
			if (sWhereQuery != null) {
				sSqlQueryString = sSqlQueryString + " " + sWhereQuery;
			}
			if (sSortFilter != null && sSortFilter.trim().length() > 0) {
				sSqlQueryString = sSqlQueryString + " ORDER BY " + sSortFilter;
				if (sSortOrder != null && sSortOrder.trim().length() > 0)
					sSqlQueryString = sSqlQueryString + " " + sSortOrder;
			}
            
        	LoggerUtility.log("INFO", this.getClass().getName(), "exportListPriceSearchResult : "+"..Search Location Count Query in Expert..", sSqlQueryString+" : WHERE QUERY : "+sWhereQuery);
        	
        	pstmt = conn.prepareCall(sSqlQueryString);
        	rs = pstmt.executeQuery();// Execute the Search Query to retriuve the Location Values 
        	
			if (rs.next()) {
				do {
					oTempListPriceDto = new ListPriceDto();
					oTempListPriceDto.setManufacturing_location(rs.getString("MANUFACTURING_LOCATION"));
					oTempListPriceDto.setProduct_group(rs.getString("PRODUCT_GROUP"));
					oTempListPriceDto.setProduct_line(rs.getString("PRODUCT_LINE"));
					oTempListPriceDto.setStandard(rs.getString("STANDARD"));
					oTempListPriceDto.setPower_rating_kw(rs.getString("POWER_RATING_KW"));
					oTempListPriceDto.setFrame_type(rs.getString("FRAME_TYPE"));
					oTempListPriceDto.setFrame_suffix(rs.getString("FRAME_SUFFIX"));
					oTempListPriceDto.setNumber_of_poles(rs.getInt("NUMBER_OF_POLES"));
					oTempListPriceDto.setMounting_type(rs.getString("MOUNTING_TYPE"));
					oTempListPriceDto.setTb_position(rs.getString("TB_POSITION"));
					oTempListPriceDto.setList_price(rs.getString("LIST_PRISE"));
					oTempListPriceDto.setMaterial_cost(rs.getString("MATERIAL_COST"));
					oTempListPriceDto.setLoh(rs.getString("LOH"));

					alSearchResultsList.add(oTempListPriceDto);

				} while (rs.next());
			}
        } finally {
        	/* Releasing PreparedStatement objects */
            oDBUtility.releaseResources( rs,pstmt);
        }
        
        LoggerUtility.log("INFO", this.getClass().getName(), "exportListPriceSearchResult", "End");
        return alSearchResultsList;
	}

	
}