/**
 * *****************************************************************************
 * Project Name: quotation
 * Document Name: LocationDao.java
 * Package: in.com.rbc.quotation.admin.dao
 * Desc: 
 * *****************************************************************************
 * Author: 100005701 (Suresh)
 * Date: Oct 3, 2008
 * *****************************************************************************
 */
package in.com.rbc.quotation.admin.dao;

import in.com.rbc.quotation.admin.dto.LocationDto;
import in.com.rbc.quotation.common.utility.ListModel;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * @author 100005701 (Suresh Shanmugam)
 *
 */
public interface LocationDao {
	
	/**
	 * 
	 * @param conn
	 * @return
	 * @throws Exception
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 3, 2008
	 */
	public ArrayList getRegionList(Connection conn)throws Exception ;
	
	/**
	 * 
	 * @param conn
	 * @param oLocationDto
	 * @param oListModel
	 * @return
	 * @throws Exception
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 3, 2008
	 */
	public Hashtable getLocationSearchResult(Connection conn, LocationDto oLocationDto, ListModel oListModel) throws Exception ;
	
	/**
	 * 
	 * @param conn
	 * @param oLocationDto
	 * @return
	 * @throws Exception
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 5, 2008
	 */
	public boolean createLocation(Connection conn, LocationDto oLocationDto)throws Exception ;
	
	/**
	 * 
	 * @param conn
	 * @param oLocationDto
	 * @return
	 * @throws Exception
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 5, 2008
	 */
	public LocationDto getLocationInfo(Connection conn , LocationDto oLocationDto)throws Exception ;
	
	/**
	 * 
	 * @param conn
	 * @param oLocationDto
	 * @return
	 * @throws Exception
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 5, 2008
	 */
	public boolean saveLocationValue(Connection conn , LocationDto oLocationDto)throws Exception ;
	
	/**
	 * 
	 * @param conn
	 * @param oLocationDto
	 * @return
	 * @throws Exception
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 5, 2008
	 */
	public boolean deleteLocationValue(Connection conn , LocationDto oLocationDto)throws Exception;
	
	/**
	 * 
	 * @param conn
	 * @param oLocationDto
	 * @param sWhereQuery
	 * @return
	 * @throws Exception
	 * @author 100005701 (Suresh Shanmugam)
	 * Created on: Oct 6, 2008
	 */
	public ArrayList exportLocationSearchResult(Connection conn , LocationDto oLocationDto,String sWhereQuery, String sSortFilter, String sSortType)throws Exception ;

	public boolean updateLocationStatusValue(Connection conn , LocationDto oLocationDto)throws Exception ;
}
