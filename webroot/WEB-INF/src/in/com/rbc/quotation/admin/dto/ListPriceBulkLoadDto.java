package in.com.rbc.quotation.admin.dto;

public class ListPriceBulkLoadDto {

	String invoke = null;
	private String isListPriceExists = null;
	private long serialId = 0;
	private String mfgLocation;
	private String productGroup;
	private String productLine;
	private String standard;
	private String powerRatingKW;
	private String frameType;
	private String frameSuffix;
	private int noOfPoles;
	private String mountType;
	private String tbPosition;
	private String listPrice;
	private String materialCost;
	private String loh;
	
	
	public String getInvoke() {
		return invoke;
	}
	public void setInvoke(String invoke) {
		this.invoke = invoke;
	}
	public String getIsListPriceExists() {
		return isListPriceExists;
	}
	public void setIsListPriceExists(String isListPriceExists) {
		this.isListPriceExists = isListPriceExists;
	}
	public long getSerialId() {
		return serialId;
	}
	public void setSerialId(long serialId) {
		this.serialId = serialId;
	}
	public String getMfgLocation() {
		return mfgLocation;
	}
	public void setMfgLocation(String mfgLocation) {
		this.mfgLocation = mfgLocation;
	}
	public String getProductGroup() {
		return productGroup;
	}
	public void setProductGroup(String productGroup) {
		this.productGroup = productGroup;
	}
	public String getProductLine() {
		return productLine;
	}
	public void setProductLine(String productLine) {
		this.productLine = productLine;
	}
	public String getStandard() {
		return standard;
	}
	public void setStandard(String standard) {
		this.standard = standard;
	}
	public String getPowerRatingKW() {
		return powerRatingKW;
	}
	public void setPowerRatingKW(String powerRatingKW) {
		this.powerRatingKW = powerRatingKW;
	}
	public String getFrameType() {
		return frameType;
	}
	public void setFrameType(String frameType) {
		this.frameType = frameType;
	}
	public String getFrameSuffix() {
		return frameSuffix;
	}
	public void setFrameSuffix(String frameSuffix) {
		this.frameSuffix = frameSuffix;
	}
	public int getNoOfPoles() {
		return noOfPoles;
	}
	public void setNoOfPoles(int noOfPoles) {
		this.noOfPoles = noOfPoles;
	}
	public String getMountType() {
		return mountType;
	}
	public void setMountType(String mountType) {
		this.mountType = mountType;
	}
	public String getTbPosition() {
		return tbPosition;
	}
	public void setTbPosition(String tbPosition) {
		this.tbPosition = tbPosition;
	}
	public String getListPrice() {
		return listPrice;
	}
	public void setListPrice(String listPrice) {
		this.listPrice = listPrice;
	}
	public String getMaterialCost() {
		return materialCost;
	}
	public void setMaterialCost(String materialCost) {
		this.materialCost = materialCost;
	}
	public String getLoh() {
		return loh;
	}
	public void setLoh(String loh) {
		this.loh = loh;
	}
	
	
}
