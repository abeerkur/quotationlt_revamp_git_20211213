package in.com.rbc.quotation.admin.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import in.com.rbc.quotation.admin.dto.AddOnDetailsDto;
import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.utility.DBUtility;
import in.com.rbc.quotation.common.utility.LoggerUtility;
import in.com.rbc.quotation.common.vo.KeyValueVo;
import in.com.rbc.quotation.enquiry.dto.NewMTOAddOnDto;
import in.com.rbc.quotation.enquiry.dto.NewRatingDto;

public class AddOnDetailsDaoImpl implements AddOnDetailsDao, AdminQueries {

	@Override
	public AddOnDetailsDto fetchAddOnPriceDetails(Connection conn, AddOnDetailsDto oAddOnDetailsDto) throws Exception {
		String sMethodName = "fetchAddOnPriceDetails" ;
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;      
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        String sAddOnQuery = "", sAddOnType = "";
        double totalAddOnPercent = 0d, totalAddOnCashExtra = 0d;

        try {
        	if(QuotationConstants.QUOTATION_EMPTY.equals(oAddOnDetailsDto.getAddOnProdLine())
        			|| QuotationConstants.QUOTATION_EMPTY.equals(oAddOnDetailsDto.getAddOnFrameSize())
        			|| oAddOnDetailsDto.getAddOnSpecialFeatureMap().size() == 0 ) {
        		
        		oAddOnDetailsDto.setTotalAddOnPercentValue(0);
        		oAddOnDetailsDto.setTotalAddOnCashExtraValue(0);
        		return oAddOnDetailsDto;
        		
        	} else {
        		oAddOnDetailsDto.setFetchAddOnList(false);
        		sAddOnQuery = buildAddOnDetailsQuery(oAddOnDetailsDto);
        		pstmt = conn.prepareStatement(sAddOnQuery);
        		
        		rs = pstmt.executeQuery();
        		
        		while(rs.next()) {
        			if("PERCENT".equalsIgnoreCase(rs.getString("RAD_CALC_TYPE"))) {
        				totalAddOnPercent = rs.getDouble("SUM(RAD_VALUE)");
        			} else if("ROUNDED".equalsIgnoreCase(rs.getString("RAD_CALC_TYPE"))) {
        				totalAddOnCashExtra = rs.getDouble("SUM(RAD_VALUE)");
        			}
        		}
        		oAddOnDetailsDto.setTotalAddOnPercentValue(totalAddOnPercent);
        		oAddOnDetailsDto.setTotalAddOnCashExtraValue(totalAddOnCashExtra);
        	}
        	
        } finally {
        	/* Releasing PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
		
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );
		return oAddOnDetailsDto;
	}
	
	@Override
	public AddOnDetailsDto fetchAddOnDetails(Connection conn, AddOnDetailsDto oAddOnDetailsDto) throws Exception {
		String sMethodName = "fetchAddOnDetails" ;
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;      
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        String sAddOnQuery = "", sAddOnType = "";
        double totalAddOnPercent = 0d, totalAddOnCashExtra = 0d;
        ArrayList<NewMTOAddOnDto> newMTOAddOnsList = new ArrayList<NewMTOAddOnDto>();
        
        try {
        	if(QuotationConstants.QUOTATION_EMPTY.equals(oAddOnDetailsDto.getAddOnProdLine())
        			|| QuotationConstants.QUOTATION_EMPTY.equals(oAddOnDetailsDto.getAddOnFrameSize())
        			|| oAddOnDetailsDto.getAddOnSpecialFeatureMap().size() == 0 ) {
        		
        		oAddOnDetailsDto.setTotalAddOnPercentValue(0);
        		oAddOnDetailsDto.setTotalAddOnCashExtraValue(0);
        		return oAddOnDetailsDto;
        		
        	} else {
        		oAddOnDetailsDto.setFetchAddOnList(true);
        		sAddOnQuery = buildAddOnDetailsQuery(oAddOnDetailsDto);
        		pstmt = conn.prepareStatement(sAddOnQuery);
        		
        		rs = pstmt.executeQuery();
        		
        		while(rs.next()) {
        			NewMTOAddOnDto oNewMTOAddOnDto = new NewMTOAddOnDto();
        			sAddOnType = rs.getString("RAD_SPECIAL_FEATURE") + " - " + rs.getString("RAD_SPECIAL_VALUE");
        			oNewMTOAddOnDto.setMtoAddOnType(sAddOnType);
        			oNewMTOAddOnDto.setMtoAddOnQty(new Integer(1));
        			oNewMTOAddOnDto.setMtoAddOnValue(String.valueOf(rs.getDouble("RAD_VALUE")));
        			oNewMTOAddOnDto.setMtoAddOnCalcType(rs.getString("RAD_CALC_TYPE"));
        			
        			if("PERCENT".equalsIgnoreCase(rs.getString("RAD_CALC_TYPE"))) {
        				totalAddOnPercent = totalAddOnPercent + rs.getDouble("RAD_VALUE");
        			} else if("ROUNDED".equalsIgnoreCase(rs.getString("RAD_CALC_TYPE"))) {
        				totalAddOnCashExtra = totalAddOnCashExtra + rs.getDouble("RAD_VALUE");
        			}
        			
        			newMTOAddOnsList.add(oNewMTOAddOnDto);
        		}
        		oAddOnDetailsDto.setTotalAddOnPercentValue(totalAddOnPercent);
        		oAddOnDetailsDto.setTotalAddOnCashExtraValue(totalAddOnCashExtra);
        		oAddOnDetailsDto.setNewMTOAddOnsList(newMTOAddOnsList);
        	}
        	
        } finally {
        	/* Releasing PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
        
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );
		return oAddOnDetailsDto;
	}

	/**
	 * Method to Build the Query for "fetchAddOnDetails"(With AddOn List) method / "fetchAddOnPriceDetails" method. 
	 * @param oAddOnDetailsDto
	 * @return
	 */
	private String buildAddOnDetailsQuery(AddOnDetailsDto oAddOnDetailsDto) {
		String sMethodName = "buildAddOnDetailsQuery";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		int iIndex = 0;
		StringBuffer sbAddOnQuery = new StringBuffer();
		String sQuery = "SELECT RAD_SPECIAL_FEATURE, RAD_SPECIAL_VALUE, RAD_VALUE, RAD_CALC_TYPE FROM " + AdminQueries.SCHEMA_QUOTATION + ".RFQ_ADDONLT_DETAILS WHERE ";
		if(!oAddOnDetailsDto.isFetchAddOnList()) {
			sbAddOnQuery.append("SELECT  SUM(RAD_VALUE),RAD_CALC_TYPE FROM ( ");
		}
		sbAddOnQuery.append(sQuery);
		
		HashMap<String, String> addOnSpecialFeaturesMap = oAddOnDetailsDto.getAddOnSpecialFeatureMap();
		String sMapKey = "", sMapValue = "";
		
		Iterator<String> itr = addOnSpecialFeaturesMap.keySet().iterator();
		while(itr.hasNext()) {
			sMapKey = itr.next();
			if(sMapKey.contains("Additional Testing")) { sMapKey = "Additional Testing"; }
			sMapValue = addOnSpecialFeaturesMap.get(sMapKey);
			
			if(iIndex > 0) {
				sbAddOnQuery.append(" UNION ").append(sQuery);
			}
			// PRODUCT LINE
			sbAddOnQuery.append("RAD_PRODUCT_LINE = '");
			sbAddOnQuery.append(oAddOnDetailsDto.getAddOnProdLine() + "' ");
			// FRAME TYPE
			sbAddOnQuery.append("AND RAD_FRAME_SIZE = '");
			sbAddOnQuery.append(oAddOnDetailsDto.getAddOnFrameSize() + "' ");
			// SPECIAL FEATURE
			sbAddOnQuery.append("AND RAD_SPECIAL_FEATURE = '");
			sbAddOnQuery.append(sMapKey + "' ");
			// SPECIAL FEATURE VALUE
			sbAddOnQuery.append("AND RAD_SPECIAL_VALUE = '");
			sbAddOnQuery.append(sMapValue + "' ");
			
			// ::::::::::::::::::::::::::::::::::::::::::::::::::  Logic for Multiple Checks ::::::::::::::::::::::::::::::::::::::::::::::::::
			// ::::::::::::::::::::::::::::::::::::::::::::::::::  Verify for Each "Special Feature" Name :::::::::::::::::::::::::::::::::::::
			if("Paint Thickness".equalsIgnoreCase(sMapKey)) {
				if("C4".equalsIgnoreCase(sMapValue) && "C5".equalsIgnoreCase(sMapValue)) {
					sbAddOnQuery.append("AND RAD_MFGLOC = '");
					sbAddOnQuery.append(oAddOnDetailsDto.getAddOnMfgLoc() + "' ");
				}
			}
			if("Terminal Box Size".equalsIgnoreCase(sMapKey)) {
				sbAddOnQuery.append("AND RAD_TBPOS = '");
				sbAddOnQuery.append(oAddOnDetailsDto.getAddOnTBPos() + "' ");
			}
			
			// ::::::::::::::::::::::::::::::::::::::::::::::::::  Logic for Multiple Checks ::::::::::::::::::::::::::::::::::::::::::::::::::
			
			// CALCULATION TYPE
			sbAddOnQuery.append("AND RAD_CALC_TYPE IN ('PERCENT','ROUNDED') ");
			
			iIndex++;
		}
		if(!oAddOnDetailsDto.isFetchAddOnList()) {
			sbAddOnQuery.append(" ) GROUP BY RAD_CALC_TYPE ");
		}
		
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
		//System.out.println("Query = = " + sbAddOnQuery.toString());
		return sbAddOnQuery.toString();
	}
	
	/**
	 * Method to Fetch MTO Details List.
	 */
	public AddOnDetailsDto fetchMTODetailsList(Connection conn, AddOnDetailsDto oAddOnDetailsDto) throws Exception {
		String sMethodName = "fetchMTODetailsList" ;
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;      
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        List<AddOnDetailsDto> newMTOList = new ArrayList<AddOnDetailsDto>();
        ArrayList<KeyValueVo> newMTOTechDetailsList = new ArrayList<KeyValueVo>();
        String tempAltitude = "", tempLoadGD2 = "", tempRVvalue = "", tempRAvalue = "";
        int iIndex = 0;
        
        try {
        	if(QuotationConstants.QUOTATION_EMPTY.equals(oAddOnDetailsDto.getAddOnProdLine())
        			|| QuotationConstants.QUOTATION_EMPTY.equals(oAddOnDetailsDto.getAddOnFrameSize())
        			|| QuotationConstants.QUOTATION_ZERO.equals(oAddOnDetailsDto.getAddOnKW())
        			|| QuotationConstants.QUOTATION_ZERO.equals(oAddOnDetailsDto.getAddOnPole())
        			|| QuotationConstants.QUOTATION_ZERO.equals(oAddOnDetailsDto.getAddOnFrameSuffix())
        			|| QuotationConstants.QUOTATION_ZERO.equals(oAddOnDetailsDto.getAddOnMounting())
        			|| QuotationConstants.QUOTATION_ZERO.equals(oAddOnDetailsDto.getAddOnTBPos())
        			|| QuotationConstants.QUOTATION_ZERO.equals(oAddOnDetailsDto.getAddOnMfgLoc())
        			|| oAddOnDetailsDto.getAddOnSpecialFeatureMap().size() == 0 ) {
        		
        		oAddOnDetailsDto.setNewMTOTechDetailsList(newMTOTechDetailsList);
        		return oAddOnDetailsDto;
        		
        	} else {
        		StringBuffer sMTODetailsQuery = new StringBuffer();
        		String sQuery = " SELECT * FROM " + AdminQueries.SCHEMA_QUOTATION + ".RFQ_ADDONLT_DETAILS WHERE ";
        		sMTODetailsQuery.append(sQuery);
        		
        		HashMap<String, String> addOnSpecialFeaturesMap = oAddOnDetailsDto.getAddOnSpecialFeatureMap();
        		String sMapKey = "", sMapValue = "";
        		
        		Iterator<String> itr = addOnSpecialFeaturesMap.keySet().iterator();
        		while(itr.hasNext()) {
        			sMapKey = itr.next();
        			sMapValue = addOnSpecialFeaturesMap.get(sMapKey);
        			
        			if(iIndex > 0) {
        				sMTODetailsQuery.append(" UNION ").append(sQuery);
        			}
        			// SPECIAL FEATURE
        			sMTODetailsQuery.append("RAD_SPECIAL_FEATURE = '");
        			sMTODetailsQuery.append(sMapKey + "' ");
        			
        			// SPECIAL FEATURE - VALUE
        			if(!"Altitude".equalsIgnoreCase(sMapKey) && !"Load GD2".equalsIgnoreCase(sMapKey) && !"RV".equalsIgnoreCase(sMapKey) && !"RA".equalsIgnoreCase(sMapKey)) {
        				sMTODetailsQuery.append("AND RAD_SPECIAL_VALUE = '");
            			sMTODetailsQuery.append(sMapValue + "' ");
        			} else {
        				if("Altitude".equalsIgnoreCase(sMapKey)) { tempAltitude = sMapValue; } 
        				if("Load GD2".equalsIgnoreCase(sMapKey)) { tempLoadGD2 = sMapValue; }
        				if("RV".equalsIgnoreCase(sMapKey)) { tempRVvalue = sMapValue; }
        				if("RA".equalsIgnoreCase(sMapKey)) { tempRAvalue = sMapValue; }
        			}
        			
        			// ::::::::::::::::::::::::::::::::::::::::::::::::::  Logic for Multiple Checks ::::::::::::::::::::::::::::::::::::::::::::::::::
        			// ::::::::::::::::::::::::::::::::::::::::::::::::::  Verify for Each "Special Feature" Name :::::::::::::::::::::::::::::::::::::
        			
        			if("Method of Starting".equalsIgnoreCase(sMapKey)) {
        				if("ASR / LSR".equalsIgnoreCase(sMapValue)) {
        					sMTODetailsQuery.append("AND RAD_PRODUCT_LINE = '");
        					sMTODetailsQuery.append(oAddOnDetailsDto.getAddOnProdLine() + "' ");
        				}
        			}
        			if("Constant Efficiency Range".equalsIgnoreCase(sMapKey)) {
        				if("75% to 100%".equalsIgnoreCase(sMapValue)) {
        					sMTODetailsQuery.append("AND RAD_PRODUCT_LINE = '");
        					sMTODetailsQuery.append(oAddOnDetailsDto.getAddOnProdLine() + "' ");
        				}
        			}
        			if("Shaft Material".equalsIgnoreCase(sMapKey)) {
        				sMTODetailsQuery.append("AND RAD_POLE_VALUE = '");
    					sMTODetailsQuery.append(oAddOnDetailsDto.getAddOnPole() + "' ");
        			}
        			if("Paint Thickness".equalsIgnoreCase(sMapKey)) {
        				sMTODetailsQuery.append("AND RAD_MFGLOC = '");
    					sMTODetailsQuery.append(oAddOnDetailsDto.getAddOnMfgLoc() + "' ");
        			}
        			if("Terminal Box Size".equalsIgnoreCase(sMapKey)) {
        				if("One Size Higher".equalsIgnoreCase(sMapValue)) {
        					sMTODetailsQuery.append("AND RAD_TBPOS = '");
        					sMTODetailsQuery.append(oAddOnDetailsDto.getAddOnTBPos() + "' ");
        					
            				sMTODetailsQuery.append("AND RAD_MFGLOC = '");
        					sMTODetailsQuery.append(oAddOnDetailsDto.getAddOnMfgLoc() + "' ");
        				}
        			}
        			if("Space Heater".equalsIgnoreCase(sMapKey)) {
        				if("KF-IE1".equalsIgnoreCase(oAddOnDetailsDto.getAddOnProdLine()) || "KF-IE2".equalsIgnoreCase(oAddOnDetailsDto.getAddOnProdLine())) {
        					sMTODetailsQuery.append("AND RAD_PRODUCT_LINE = '");
        					sMTODetailsQuery.append(oAddOnDetailsDto.getAddOnProdLine() + "' ");
        					
            				sMTODetailsQuery.append("AND RAD_FRAME_SIZE = '");
        					sMTODetailsQuery.append(oAddOnDetailsDto.getAddOnFrameSize() + "' ");
        				} else {
        					sMTODetailsQuery.append("AND RAD_FRAME_SIZE = '");
        					sMTODetailsQuery.append(oAddOnDetailsDto.getAddOnFrameSize() + "' ");
        				}
        			}
        			if("Encoder Mounting".equalsIgnoreCase(sMapKey)) {
    					if("Hollow Shaft".equalsIgnoreCase(sMapValue)) {
    						sMTODetailsQuery.append("AND RAD_MFGLOC = '");
        					sMTODetailsQuery.append(oAddOnDetailsDto.getAddOnMfgLoc() + "' ");
    					}
        			}
        			if("Hardware".equalsIgnoreCase(sMapKey)) {
        				if("SCA".equalsIgnoreCase(oAddOnDetailsDto.getAddOnProdLine()) || "TCA".equalsIgnoreCase(oAddOnDetailsDto.getAddOnProdLine())) {
        					sMTODetailsQuery.append("AND RAD_PRODUCT_LINE = '");
        					sMTODetailsQuery.append(oAddOnDetailsDto.getAddOnProdLine() + "' ");
        					
        					sMTODetailsQuery.append("AND RAD_MFGLOC = '");
        					sMTODetailsQuery.append(oAddOnDetailsDto.getAddOnMfgLoc() + "' ");
        				} else {
        					sMTODetailsQuery.append("AND RAD_PRODUCT_LINE = '");
        					sMTODetailsQuery.append(oAddOnDetailsDto.getAddOnProdLine() + "' ");
        				}
        			}
        			if("RTD".equalsIgnoreCase(sMapKey) || "BTD".equalsIgnoreCase(sMapKey)) {
        				sMTODetailsQuery.append("AND RAD_FRAME_SIZE = '");
    					sMTODetailsQuery.append(oAddOnDetailsDto.getAddOnFrameSize() + "' ");
        			}
        			if("Bearing System".equalsIgnoreCase(sMapKey)) {
        				sMTODetailsQuery.append("AND RAD_PRODUCT_LINE = '");
    					sMTODetailsQuery.append(oAddOnDetailsDto.getAddOnProdLine() + "' ");
    					
    					sMTODetailsQuery.append("AND RAD_FRAME_SIZE = '");
    					sMTODetailsQuery.append(oAddOnDetailsDto.getAddOnFrameSize() + "' ");
        			}
        			if("RV".equalsIgnoreCase(sMapKey) || "RA".equalsIgnoreCase(sMapKey)) {
        				sMTODetailsQuery.append("AND RAD_PRODUCT_LINE = '");
    					sMTODetailsQuery.append(oAddOnDetailsDto.getAddOnProdLine() + "' ");
        			}
        			
        			// ::::::::::::::::::::::::::::::::::::::::::::::::::  Logic for Multiple Checks ::::::::::::::::::::::::::::::::::::::::::::::::::
        			
        			// CALCULATION TYPE
        			sMTODetailsQuery.append("AND RAD_CALC_TYPE = 'RE' ");
        			
        			iIndex++;
        		}
        		
        		pstmt = conn.prepareStatement(sMTODetailsQuery.toString());
        		
        		rs = pstmt.executeQuery();
        		while(rs.next()) {
        			AddOnDetailsDto tempAddOnDetailsDto = new AddOnDetailsDto();
        			tempAddOnDetailsDto.setAddOnProdLine(rs.getString("RAD_PRODUCT_LINE"));
        			tempAddOnDetailsDto.setAddOnKW(rs.getString("RAD_KW_VALUE"));
        			tempAddOnDetailsDto.setAddOnPole(rs.getString("RAD_POLE_VALUE"));
        			tempAddOnDetailsDto.setAddOnFrameSize(rs.getString("RAD_FRAME_SIZE"));
        			tempAddOnDetailsDto.setAddOnFrameSuffix(rs.getString("RAD_FRAME_SUFFIX"));
        			tempAddOnDetailsDto.setAddOnMounting(rs.getString("RAD_MOUNTING"));
        			tempAddOnDetailsDto.setAddOnTBPos(rs.getString("RAD_TBPOS"));
        			tempAddOnDetailsDto.setAddOnMfgLoc(rs.getString("RAD_MFGLOC"));
        			tempAddOnDetailsDto.setAddOnSpecialFeature(rs.getString("RAD_SPECIAL_FEATURE"));
        			if(tempAddOnDetailsDto.getAddOnSpecialFeature().equalsIgnoreCase("Altitude")) {
        				tempAddOnDetailsDto.setAddOnSpecialFeatureValue(tempAltitude);
        			} else if(tempAddOnDetailsDto.getAddOnSpecialFeature().equalsIgnoreCase("Load GD2")) { 
        				tempAddOnDetailsDto.setAddOnSpecialFeatureValue(tempLoadGD2);
        			} else {
        				tempAddOnDetailsDto.setAddOnSpecialFeatureValue(rs.getString("RAD_SPECIAL_VALUE"));
        			}
        			        			
        			newMTOList.add(tempAddOnDetailsDto);
        		}
        		oDBUtility.releaseResources(rs, pstmt);
        		
        		if(newMTOList.size() > 0) {
        			oAddOnDetailsDto = processMTORules(oAddOnDetailsDto, newMTOList);
        		}
        	}
        } finally {
        	/* Releasing PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
        
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );
		return oAddOnDetailsDto;
	}
	
	private AddOnDetailsDto processMTORules(AddOnDetailsDto oAddOnDetailsDto, List<AddOnDetailsDto> newMTOList) {
		String sMethodName = "processMTORules" ;
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
		ArrayList<KeyValueVo> newMTOTechDetailsList = new ArrayList<KeyValueVo>();
		KeyValueVo newKeyValueVo = null;
		AddOnDetailsDto tempAddOnDetailsDto = null;
		boolean vfdType_IsMTO = false;
		String sFeatureValue = "";
		StringBuffer sbMTOHighlightFlag = new StringBuffer();
		
		try {
			Iterator<AddOnDetailsDto> itr = newMTOList.iterator();
			while(itr.hasNext()) {
				// Get Initial Highlight Flag value from "oAddOnDetailsDto".
				sbMTOHighlightFlag.append(oAddOnDetailsDto.getMtoHighlightFlag());
				
				tempAddOnDetailsDto = itr.next();
				sFeatureValue = tempAddOnDetailsDto.getAddOnSpecialFeatureValue();
				
				if("Pole".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim())) {
					newKeyValueVo = new KeyValueVo();
					newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
					newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
					if(sbMTOHighlightFlag.toString().length() > 0) {
						sbMTOHighlightFlag.append(QuotationConstants.QUOTATION_STRING_PIPE).append("1");
					} else {
						sbMTOHighlightFlag.append("1");
					}
				}
				if("Service Factor".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim())) {
					newKeyValueVo = new KeyValueVo();
					newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
					newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
					if(sbMTOHighlightFlag.toString().length() > 0) {
						sbMTOHighlightFlag.append(QuotationConstants.QUOTATION_STRING_PIPE).append("2");
					} else {
						sbMTOHighlightFlag.append("2");
					}
				}
				if("Method of Starting".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim())) {
					if("ASR / LSR".equalsIgnoreCase(sFeatureValue)) {
						// Condition Check for "Slip Ring" Motor.
						String sProdLine = oAddOnDetailsDto.getAddOnProdLine();
						if("KS".equalsIgnoreCase(sProdLine) || "KS-CMR".equalsIgnoreCase(sProdLine) ) {
							newKeyValueVo = new KeyValueVo();
							newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
							newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
						}
					} else {
						newKeyValueVo = new KeyValueVo();
						newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
						newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
					}
					if(sbMTOHighlightFlag.toString().length() > 0) {
						sbMTOHighlightFlag.append(QuotationConstants.QUOTATION_STRING_PIPE).append("3");
					} else {
						sbMTOHighlightFlag.append("3");
					}
				}
				if("DOL Starting Current".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim())) {
					newKeyValueVo = new KeyValueVo();
					newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
					newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
					if(sbMTOHighlightFlag.toString().length() > 0) {
						sbMTOHighlightFlag.append(QuotationConstants.QUOTATION_STRING_PIPE).append("4");
					} else {
						sbMTOHighlightFlag.append("4");
					}
				}
				if("Ambient Temp Rem".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim())) {
					newKeyValueVo = new KeyValueVo();
					newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
					newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
					if(sbMTOHighlightFlag.toString().length() > 0) {
						sbMTOHighlightFlag.append(QuotationConstants.QUOTATION_STRING_PIPE).append("5");
					} else {
						sbMTOHighlightFlag.append("5");
					}
				}
				if("Altitude".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim())) {
					newKeyValueVo = new KeyValueVo();
					newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
					newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
					if(sbMTOHighlightFlag.toString().length() > 0) {
						sbMTOHighlightFlag.append(QuotationConstants.QUOTATION_STRING_PIPE).append("6");
					} else {
						sbMTOHighlightFlag.append("6");
					}
				}
				if("Dust Group".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim())) {
					newKeyValueVo = new KeyValueVo();
					newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
					newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
					if(sbMTOHighlightFlag.toString().length() > 0) {
						sbMTOHighlightFlag.append(QuotationConstants.QUOTATION_STRING_PIPE).append("7");
					} else {
						sbMTOHighlightFlag.append("7");
					}
				}
				if("Noise Level".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim())) {
					newKeyValueVo = new KeyValueVo();
					newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
					newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
					if(sbMTOHighlightFlag.toString().length() > 0) {
						sbMTOHighlightFlag.append(QuotationConstants.QUOTATION_STRING_PIPE).append("8");
					} else {
						sbMTOHighlightFlag.append("8");
					}
				}
				if("Replacement Motor".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim())) {
					newKeyValueVo = new KeyValueVo();
					newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
					newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
					if(sbMTOHighlightFlag.toString().length() > 0) {
						sbMTOHighlightFlag.append(QuotationConstants.QUOTATION_STRING_PIPE).append("9");
					} else {
						sbMTOHighlightFlag.append("9");
					}
				}
				if("Leads".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim())) {
					newKeyValueVo = new KeyValueVo();
					newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
					newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
					if(sbMTOHighlightFlag.toString().length() > 0) {
						sbMTOHighlightFlag.append(QuotationConstants.QUOTATION_STRING_PIPE).append("10");
					} else {
						sbMTOHighlightFlag.append("10");
					}
				}
				if("Load GD2".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim())) {
					if(StringUtils.isNotBlank(sFeatureValue) && sFeatureValue.trim().length() > 0
							&& Integer.parseInt(sFeatureValue.trim()) > 0 ) {
						newKeyValueVo = new KeyValueVo();
						newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
						newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
						if(sbMTOHighlightFlag.toString().length() > 0) {
							sbMTOHighlightFlag.append(QuotationConstants.QUOTATION_STRING_PIPE).append("11");
						} else {
							sbMTOHighlightFlag.append("11");
						}
					}
				}
				if("VFD Type".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim())) {
					newKeyValueVo = new KeyValueVo();
					newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
					newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
					if("Constant Torque".equalsIgnoreCase(sFeatureValue) || "Intermittent Duty".equalsIgnoreCase(sFeatureValue)) {
						vfdType_IsMTO = true;
					}
					if(sbMTOHighlightFlag.toString().length() > 0) {
						sbMTOHighlightFlag.append(QuotationConstants.QUOTATION_STRING_PIPE).append("12");
					} else {
						sbMTOHighlightFlag.append("12");
					}
				}
				if("VFD Min Speed".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim())) {
					if(vfdType_IsMTO) {
						newKeyValueVo = new KeyValueVo();
						newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
						newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
						if(sbMTOHighlightFlag.toString().length() > 0) {
							sbMTOHighlightFlag.append(QuotationConstants.QUOTATION_STRING_PIPE).append("13");
						} else {
							sbMTOHighlightFlag.append("13");
						}
					}
				}
				if("VFD Max Speed".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim())) {
					newKeyValueVo = new KeyValueVo();
					newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
					newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
					if(sbMTOHighlightFlag.toString().length() > 0) {
						sbMTOHighlightFlag.append(QuotationConstants.QUOTATION_STRING_PIPE).append("14");
					} else {
						sbMTOHighlightFlag.append("14");
					}
				}
				if("Overloading Duty".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim())) {
					newKeyValueVo = new KeyValueVo();
					newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
					newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
					if(sbMTOHighlightFlag.toString().length() > 0) {
						sbMTOHighlightFlag.append(QuotationConstants.QUOTATION_STRING_PIPE).append("15");
					} else {
						sbMTOHighlightFlag.append("15");
					}
				}
				if("Constant Efficiency Range".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim())) {
					if("75% to 100%".equalsIgnoreCase(sFeatureValue)) {
						String sProdLine = oAddOnDetailsDto.getAddOnProdLine();
						if("SE IE2".equalsIgnoreCase(sProdLine) || "KS".equalsIgnoreCase(sProdLine) || "KD".equalsIgnoreCase(sProdLine)
								|| "VKD".equalsIgnoreCase(sProdLine) || "VD".equalsIgnoreCase(sProdLine) || "IE1".equalsIgnoreCase(sProdLine)
								|| "KF-IE1".equalsIgnoreCase(sProdLine) || "KF-IE2".equalsIgnoreCase(sProdLine) || "PTSC-I".equalsIgnoreCase(sProdLine)
								|| "PTSC-I-FF".equalsIgnoreCase(sProdLine) || "KS-CMR".equalsIgnoreCase(sProdLine) || "SZ250".equalsIgnoreCase(sProdLine)
								|| "SZ300".equalsIgnoreCase(sProdLine) || "SZ400".equalsIgnoreCase(sProdLine) ) {
							
							newKeyValueVo = new KeyValueVo();
							newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
							newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
						}
					
					} else {
						newKeyValueVo = new KeyValueVo();
						newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
						newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
					}
					if(sbMTOHighlightFlag.toString().length() > 0) {
						sbMTOHighlightFlag.append(QuotationConstants.QUOTATION_STRING_PIPE).append("16");
					} else {
						sbMTOHighlightFlag.append("16");
					}
				}
				if("Shaft Material".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim())) {
					if("1".equals(oAddOnDetailsDto.getAddOnPole())) {
						newKeyValueVo = new KeyValueVo();
						newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
						newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
					}
					if(sbMTOHighlightFlag.toString().length() > 0) {
						sbMTOHighlightFlag.append(QuotationConstants.QUOTATION_STRING_PIPE).append("17");
					} else {
						sbMTOHighlightFlag.append("17");
					}
				}
				if("Method of Coupling".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim())) {
					newKeyValueVo = new KeyValueVo();
					newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
					newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
					if(sbMTOHighlightFlag.toString().length() > 0) {
						sbMTOHighlightFlag.append(QuotationConstants.QUOTATION_STRING_PIPE).append("18");
					} else {
						sbMTOHighlightFlag.append("18");
					}
				}
				if("Paint Shade".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim())) {
					newKeyValueVo = new KeyValueVo();
					newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
					newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
					if(sbMTOHighlightFlag.toString().length() > 0) {
						sbMTOHighlightFlag.append(QuotationConstants.QUOTATION_STRING_PIPE).append("19");
					} else {
						sbMTOHighlightFlag.append("19");
					}
				}
				if("Paint Thickness".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim())) {
					if("1".equalsIgnoreCase(oAddOnDetailsDto.getAddOnMfgLoc())) {
						newKeyValueVo = new KeyValueVo();
						newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
						newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
					}
					if(sbMTOHighlightFlag.toString().length() > 0) {
						sbMTOHighlightFlag.append(QuotationConstants.QUOTATION_STRING_PIPE).append("20");
					} else {
						sbMTOHighlightFlag.append("20");
					}
				}
				if("IP".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim())) {
					newKeyValueVo = new KeyValueVo();
					newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
					newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
					if(sbMTOHighlightFlag.toString().length() > 0) {
						sbMTOHighlightFlag.append(QuotationConstants.QUOTATION_STRING_PIPE).append("21");
					} else {
						sbMTOHighlightFlag.append("21");
					}
				}
				if("Terminal Box Size".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim())) {
					if("One Size Higher".equalsIgnoreCase(sFeatureValue)) {
						if("2".equalsIgnoreCase(oAddOnDetailsDto.getAddOnTBPos()) && "2".equalsIgnoreCase(oAddOnDetailsDto.getAddOnMfgLoc())) {
							newKeyValueVo = new KeyValueVo();
							newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
							newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
						}
					} else {
						newKeyValueVo = new KeyValueVo();
						newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
						newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
					}
					if(sbMTOHighlightFlag.toString().length() > 0) {
						sbMTOHighlightFlag.append(QuotationConstants.QUOTATION_STRING_PIPE).append("22");
					} else {
						sbMTOHighlightFlag.append("22");
					}
				}
				if("Space Heater".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim())) {
					if("KF-IE1".equalsIgnoreCase(oAddOnDetailsDto.getAddOnProdLine()) || "KF-IE2".equalsIgnoreCase(oAddOnDetailsDto.getAddOnProdLine())) {
						if("112".equalsIgnoreCase(oAddOnDetailsDto.getAddOnFrameSize()) || "132".equalsIgnoreCase(oAddOnDetailsDto.getAddOnFrameSize())) {
							newKeyValueVo = new KeyValueVo();
							newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
							newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
						}
					} else {
						if("80".equalsIgnoreCase(oAddOnDetailsDto.getAddOnFrameSize()) || "90".equalsIgnoreCase(oAddOnDetailsDto.getAddOnFrameSize())
								|| "100".equalsIgnoreCase(oAddOnDetailsDto.getAddOnFrameSize()) ) {
							newKeyValueVo = new KeyValueVo();
							newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
							newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
						}
					}
					if(sbMTOHighlightFlag.toString().length() > 0) {
						sbMTOHighlightFlag.append(QuotationConstants.QUOTATION_STRING_PIPE).append("23");
					} else {
						sbMTOHighlightFlag.append("23");
					}
				}
				if("Flying Lead Without TB".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim())) {
					newKeyValueVo = new KeyValueVo();
					newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
					newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
					if(sbMTOHighlightFlag.toString().length() > 0) {
						sbMTOHighlightFlag.append(QuotationConstants.QUOTATION_STRING_PIPE).append("24");
					} else {
						sbMTOHighlightFlag.append("24");
					}
				}
				if("Flying Lead With TB".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim())) {
					newKeyValueVo = new KeyValueVo();
					newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
					newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
					if(sbMTOHighlightFlag.toString().length() > 0) {
						sbMTOHighlightFlag.append(QuotationConstants.QUOTATION_STRING_PIPE).append("25");
					} else {
						sbMTOHighlightFlag.append("25");
					}
				}
				if("Metal Fan".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim())) {
					newKeyValueVo = new KeyValueVo();
					newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
					newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
					if(sbMTOHighlightFlag.toString().length() > 0) {
						sbMTOHighlightFlag.append(QuotationConstants.QUOTATION_STRING_PIPE).append("26");
					} else {
						sbMTOHighlightFlag.append("26");
					}
				}
				if("Encoder Mounting".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim())) {
					if("Hollow Shaft".equalsIgnoreCase(sFeatureValue)) {
						if("1".equalsIgnoreCase(oAddOnDetailsDto.getAddOnMfgLoc())) {
							newKeyValueVo = new KeyValueVo();
							newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
							newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
						}
					} else {
						newKeyValueVo = new KeyValueVo();
						newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
						newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
					}
					if(sbMTOHighlightFlag.toString().length() > 0) {
						sbMTOHighlightFlag.append(QuotationConstants.QUOTATION_STRING_PIPE).append("27");
					} else {
						sbMTOHighlightFlag.append("27");
					}
				}
				if("Shaft Grounding".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim())) {
					newKeyValueVo = new KeyValueVo();
					newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
					newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
					if(sbMTOHighlightFlag.toString().length() > 0) {
						sbMTOHighlightFlag.append(QuotationConstants.QUOTATION_STRING_PIPE).append("28");
					} else {
						sbMTOHighlightFlag.append("28");
					}
				}
				if("Hardware".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim())) {
					if("SCA".equalsIgnoreCase(oAddOnDetailsDto.getAddOnProdLine()) || "TCA".equalsIgnoreCase(oAddOnDetailsDto.getAddOnProdLine())) {
						if("2".equalsIgnoreCase(oAddOnDetailsDto.getAddOnMfgLoc())) {
							newKeyValueVo = new KeyValueVo();
							newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
							newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
						}
					} else {
						newKeyValueVo = new KeyValueVo();
						newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
						newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
					}
					if(sbMTOHighlightFlag.toString().length() > 0) {
						sbMTOHighlightFlag.append(QuotationConstants.QUOTATION_STRING_PIPE).append("29");
					} else {
						sbMTOHighlightFlag.append("29");
					}
				}
				if("Gland Plate".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim())) {
					newKeyValueVo = new KeyValueVo();
					newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
					newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
					if(sbMTOHighlightFlag.toString().length() > 0) {
						sbMTOHighlightFlag.append(QuotationConstants.QUOTATION_STRING_PIPE).append("30");
					} else {
						sbMTOHighlightFlag.append("30");
					}
				}
				if("SPM Mounting".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim())) {
					newKeyValueVo = new KeyValueVo();
					newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
					newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
					if(sbMTOHighlightFlag.toString().length() > 0) {
						sbMTOHighlightFlag.append(QuotationConstants.QUOTATION_STRING_PIPE).append("31");
					} else {
						sbMTOHighlightFlag.append("31");
					}
				}
				if("RTD".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim())) {
					String sAddOnFrameSize = oAddOnDetailsDto.getAddOnFrameSize();
					if("80".equalsIgnoreCase(sAddOnFrameSize) || "90".equalsIgnoreCase(sAddOnFrameSize) || "100".equalsIgnoreCase(sAddOnFrameSize)
							|| "112".equalsIgnoreCase(sAddOnFrameSize) || "132".equalsIgnoreCase(sAddOnFrameSize) || "160".equalsIgnoreCase(sAddOnFrameSize)
							|| "180".equalsIgnoreCase(sAddOnFrameSize) || "200".equalsIgnoreCase(sAddOnFrameSize) || "225".equalsIgnoreCase(sAddOnFrameSize)
							|| "250".equalsIgnoreCase(sAddOnFrameSize)) {
						newKeyValueVo = new KeyValueVo();
						newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
						newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
					}
					if(sbMTOHighlightFlag.toString().length() > 0) {
						sbMTOHighlightFlag.append(QuotationConstants.QUOTATION_STRING_PIPE).append("32");
					} else {
						sbMTOHighlightFlag.append("32");
					}
				}
				if("BTD".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim())) {
					String sAddOnFrameSize = oAddOnDetailsDto.getAddOnFrameSize();
					if("80".equalsIgnoreCase(sAddOnFrameSize) || "90".equalsIgnoreCase(sAddOnFrameSize) || "100".equalsIgnoreCase(sAddOnFrameSize)
							|| "112".equalsIgnoreCase(sAddOnFrameSize) || "132".equalsIgnoreCase(sAddOnFrameSize) || "160".equalsIgnoreCase(sAddOnFrameSize)
							|| "180".equalsIgnoreCase(sAddOnFrameSize) || "200".equalsIgnoreCase(sAddOnFrameSize) || "225".equalsIgnoreCase(sAddOnFrameSize)
							|| "250".equalsIgnoreCase(sAddOnFrameSize)) {
						newKeyValueVo = new KeyValueVo();
						newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
						newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
					}
					if(sbMTOHighlightFlag.toString().length() > 0) {
						sbMTOHighlightFlag.append(QuotationConstants.QUOTATION_STRING_PIPE).append("33");
					} else {
						sbMTOHighlightFlag.append("33");
					}
				}
				if("Thermister".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim())) {
					newKeyValueVo = new KeyValueVo();
					newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
					newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
					if(sbMTOHighlightFlag.toString().length() > 0) {
						sbMTOHighlightFlag.append(QuotationConstants.QUOTATION_STRING_PIPE).append("34");
					} else {
						sbMTOHighlightFlag.append("34");
					}
				}
				if("Bearing System".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim())) {
					if("SCA".equalsIgnoreCase(oAddOnDetailsDto.getAddOnProdLine()) || "TCA".equalsIgnoreCase(oAddOnDetailsDto.getAddOnProdLine())) {
						if("160".equalsIgnoreCase(oAddOnDetailsDto.getAddOnFrameSize()) || "180".equalsIgnoreCase(oAddOnDetailsDto.getAddOnFrameSize())) {
							newKeyValueVo = new KeyValueVo();
							newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
							newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
						}
					} else {
						if("132".equalsIgnoreCase(oAddOnDetailsDto.getAddOnFrameSize()) || "160".equalsIgnoreCase(oAddOnDetailsDto.getAddOnFrameSize())
								|| "180".equalsIgnoreCase(oAddOnDetailsDto.getAddOnFrameSize()) || "200".equalsIgnoreCase(oAddOnDetailsDto.getAddOnFrameSize()) ) {
							newKeyValueVo = new KeyValueVo();
							newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
							newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
						}
					}
					if(sbMTOHighlightFlag.toString().length() > 0) {
						sbMTOHighlightFlag.append(QuotationConstants.QUOTATION_STRING_PIPE).append("35");
					} else {
						sbMTOHighlightFlag.append("35");
					}
				}
				if("Bearing DE".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim())) {
					newKeyValueVo = new KeyValueVo();
					newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
					newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
					if(sbMTOHighlightFlag.toString().length() > 0) {
						sbMTOHighlightFlag.append(QuotationConstants.QUOTATION_STRING_PIPE).append("36");
					} else {
						sbMTOHighlightFlag.append("36");
					}
				}
				if("Combined Variation".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim())) {
					newKeyValueVo = new KeyValueVo();
					newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
					newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
					if(sbMTOHighlightFlag.toString().length() > 0) {
						sbMTOHighlightFlag.append(QuotationConstants.QUOTATION_STRING_PIPE).append("37");
					} else {
						sbMTOHighlightFlag.append("37");
					}
				}
				if("Performance Curves".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim())
						|| "Datasheet".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim())
						|| "Motor GA".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim())
						|| "Tbox GA".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim()) ) {
					
					newKeyValueVo = new KeyValueVo();
					newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
					newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
					
					if("Performance Curves".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim()) ) {
						if(sbMTOHighlightFlag.toString().length() > 0) {  sbMTOHighlightFlag.append(QuotationConstants.QUOTATION_STRING_PIPE);  }
						sbMTOHighlightFlag.append("38");
					} else if("Datasheet".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim()) ) {
						if(sbMTOHighlightFlag.toString().length() > 0) {  sbMTOHighlightFlag.append(QuotationConstants.QUOTATION_STRING_PIPE);  }
						sbMTOHighlightFlag.append("39");
					} else if("Motor GA".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim()) ) {
						if(sbMTOHighlightFlag.toString().length() > 0) {  sbMTOHighlightFlag.append(QuotationConstants.QUOTATION_STRING_PIPE);  }
						sbMTOHighlightFlag.append("40");
					} else if("Tbox GA".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim()) ) {
						if(sbMTOHighlightFlag.toString().length() > 0) {  sbMTOHighlightFlag.append(QuotationConstants.QUOTATION_STRING_PIPE);  }
						sbMTOHighlightFlag.append("41");
					}
				}
				if("RV".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim()) || "RA".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim())) {
					// Condition Check for "Slip Ring" Motor.
					String sProdLine = oAddOnDetailsDto.getAddOnProdLine();
					if("KS".equalsIgnoreCase(sProdLine) || "KS-CMR".equalsIgnoreCase(sProdLine) ) {
						newKeyValueVo = new KeyValueVo();
						newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
						newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
						sbMTOHighlightFlag.append("42");
					}
				}
				if("Method Of Cooling".equalsIgnoreCase(tempAddOnDetailsDto.getAddOnSpecialFeature().trim())) {
					newKeyValueVo = new KeyValueVo();
					newKeyValueVo.setKey(tempAddOnDetailsDto.getAddOnSpecialFeature());
					newKeyValueVo.setValue(tempAddOnDetailsDto.getAddOnSpecialFeatureValue());
					sbMTOHighlightFlag.append("43");
				}
				
				if(newKeyValueVo != null) {
					newMTOTechDetailsList.add(newKeyValueVo);
				}
			}
			
			oAddOnDetailsDto.setNewMTOTechDetailsList(newMTOTechDetailsList);
			oAddOnDetailsDto.setMtoHighlightFlag(sbMTOHighlightFlag.toString());
			
		} catch(Exception e) {
			//System.out.println(" ::::::::::::::::::  Error while Processing MTO Rules ::::::::::::::::: ");
			e.printStackTrace();
		}
		
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );
		return oAddOnDetailsDto;
	}
	
	@Override
	public void updateAddOnMTODetails(Connection conn, NewRatingDto oNewRatingDto, String loggedInUserId) throws Exception {
		String sMethodName = "updateAddOnMTODetails" ;
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        PreparedStatement pstmt1 = null;
        PreparedStatement pstmt2 = null;
        /* ResultSet object to store the rows retrieved from database */
		ResultSet rs = null;
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        int iMTOAddOnId = 0, iMTORatingId = 0, iResult = 0;
        String sCheckQuery = "";
        String sDeleteQuery = "DELETE FROM " + AdminQueries.SCHEMA_QUOTATION +".RFQ_LT_MTOADDON_DETAILS WHERE QRD_LT_MTOENQUIRYID = ? AND QRD_LT_MTORATINGNO = ? ";
        
        try {
        	// Deleting Existing MTO & Add-On Records - Based on EnquiryId and RatingNo.
        	pstmt = conn.prepareStatement(sDeleteQuery);
        	pstmt.setInt(1, oNewRatingDto.getEnquiryId());
        	pstmt.setInt(2, oNewRatingDto.getRatingNo());
        	pstmt.executeUpdate();
        	
        	// RFQ_LT_MTOADDON_DETAILS : Inserting Add-Ons - Based on EnquiryId and RatingNo.
        	if(CollectionUtils.isNotEmpty(oNewRatingDto.getNewMTOAddOnsList())) {
        		for(NewMTOAddOnDto oAddOnDto : oNewRatingDto.getNewMTOAddOnsList()) {
            		pstmt1 = conn.prepareStatement(FETCH_LT_MTOADDONID_NEXTVAL);	    
    	    		rs = pstmt1.executeQuery();
    	    		if (rs.next()) {
    	    			iMTOAddOnId = Integer.parseInt(rs.getString("MTOADDON_ID"));
    	    		}
    	    		oDBUtility.releaseResources(rs, pstmt1);
    	    		
    	    		pstmt1 = conn.prepareStatement(INSERT_LT_MTOADDON_DETAILS);
    	    		pstmt1.setInt(1, iMTOAddOnId);
    	    		pstmt1.setInt(2, oNewRatingDto.getEnquiryId());
    	    		pstmt1.setInt(3, oNewRatingDto.getRatingNo());
    	    		pstmt1.setString(4, oAddOnDto.getMtoAddOnType());
    	    		pstmt1.setInt(5, oAddOnDto.getMtoAddOnQty());
    	    		pstmt1.setDouble(6, Double.parseDouble(oAddOnDto.getMtoAddOnValue()));
    	    		pstmt1.setString(7, oAddOnDto.getMtoAddOnCalcType());
    	    		pstmt1.setString(8, oNewRatingDto.getIsReferDesign());
    	    		pstmt1.setString(9, QuotationConstants.QUOTATIONLT_ADDONPAGETYPE_CREATE);
    	    		pstmt1.setString(10, loggedInUserId);
    	    		iResult = pstmt1.executeUpdate();
    	    		
    	    		oDBUtility.releaseResources(pstmt1);
            	}
        	}
        	// RFQ_LT_MTOADDON_DETAILS : Inserting MTOs Details - Based on EnquiryId and RatingNo.
        	if(CollectionUtils.isNotEmpty(oNewRatingDto.getNewMTOTechDetailsList()) || QuotationConstants.QUOTATION_STRING_Y.equalsIgnoreCase(oNewRatingDto.getIsReferDesign())) {
        		
        		// RFQ_LT_MTOADDON_DETAILS : Inserting MTOs - Based on EnquiryId and RatingNo.
        		if( CollectionUtils.isNotEmpty(oNewRatingDto.getNewMTOTechDetailsList()) ) {
        			for(KeyValueVo oKeyValue : oNewRatingDto.getNewMTOTechDetailsList()) {
                		pstmt1 = conn.prepareStatement(FETCH_LT_MTOADDONID_NEXTVAL);	    
        	    		rs = pstmt1.executeQuery();
        	    		if (rs.next()) {
        	    			iMTOAddOnId = Integer.parseInt(rs.getString("MTOADDON_ID"));
        	    		}
        	    		oDBUtility.releaseResources(rs, pstmt1);
        	    		
        	    		pstmt1 = conn.prepareStatement(INSERT_LT_MTOADDON_DETAILS);
        	    		pstmt1.setInt(1, iMTOAddOnId);
        	    		pstmt1.setInt(2, oNewRatingDto.getEnquiryId());
        	    		pstmt1.setInt(3, oNewRatingDto.getRatingNo());
        	    		pstmt1.setString(4, oKeyValue.getKey() + " - " + oKeyValue.getValue());
        	    		pstmt1.setInt(5, new Integer(1));
        	    		pstmt1.setString(6, oKeyValue.getValue());
        	    		pstmt1.setString(7, "RE");
        	    		pstmt1.setString(8, oNewRatingDto.getIsReferDesign());
        	    		pstmt1.setString(9, QuotationConstants.QUOTATIONLT_ADDONPAGETYPE_CREATE);
        	    		pstmt1.setString(10, loggedInUserId);
        	    		iResult = pstmt1.executeUpdate();
        	    		oDBUtility.releaseResources(pstmt1);
                	}
        		}
        		
            	// RFQ_LT_MTORATING_DETAILS : Inserting MTO Ratings with Mandatory Field Details - Based on EnquiryId and RatingNo. 
        		oDBUtility.releaseResources(rs, pstmt1);
        		sCheckQuery = "SELECT * FROM " + AdminQueries.SCHEMA_QUOTATION + ".RFQ_LT_MTORATING_DETAILS WHERE QRD_LT_MTOENQUIRYID = ? AND QRD_LT_MTORATINGNO = ? ";
        		pstmt1 = conn.prepareStatement(sCheckQuery);
	    		pstmt1.setInt(1, oNewRatingDto.getEnquiryId());
	    		pstmt1.setInt(2, oNewRatingDto.getRatingNo());
	    		rs = pstmt1.executeQuery();
	    		
	    		if (rs.next()) { 
	        		pstmt2 = conn.prepareStatement(UPDATE_LT_MTORATING_DETAILS);
	        		pstmt2.setInt(1, oNewRatingDto.getQuantity());
	        		pstmt2.setString(2, oNewRatingDto.getLtProdLineId());
	        		pstmt2.setString(3, oNewRatingDto.getLtKWId());
	        		pstmt2.setInt(4, Integer.parseInt(oNewRatingDto.getLtPoleId()));
	        		pstmt2.setString(5, oNewRatingDto.getLtFrameId());
	        		pstmt2.setString(6, oNewRatingDto.getLtFrameSuffixId());
	        		pstmt2.setString(7, oNewRatingDto.getLtMountingId());
	        		pstmt2.setString(8, oNewRatingDto.getLtTBPositionId());
	        		pstmt2.setString(9, oNewRatingDto.getTagNumber());
	        		pstmt2.setString(10, oNewRatingDto.getLtManufacturingLocation());
	        		pstmt2.setString(11, oNewRatingDto.getLtTotalAddonPercent());
	        		pstmt2.setString(12, oNewRatingDto.getLtTotalCashExtra());
	        		pstmt2.setInt(13, oNewRatingDto.getEnquiryId());
	        		pstmt2.setInt(14, oNewRatingDto.getRatingNo());
	        		iResult = pstmt2.executeUpdate();
	        		
	    		} else {
	    			pstmt2 = conn.prepareStatement(FETCH_LT_MTORATING_NEXTVAL);	    
	        		rs = pstmt2.executeQuery();
	        		if (rs.next()) {
	        			iMTORatingId = Integer.parseInt(rs.getString("MTORATING_ID"));
	        		}
	        		oDBUtility.releaseResources(rs, pstmt2);
	        		
	        		pstmt2 = conn.prepareStatement(INSERT_LT_MTORATING_DETAILS);
	        		pstmt2.setInt(1, iMTORatingId);
	        		pstmt2.setInt(2, oNewRatingDto.getEnquiryId());
	        		pstmt2.setInt(3, oNewRatingDto.getRatingNo());
	        		pstmt2.setInt(4, oNewRatingDto.getQuantity());
	        		pstmt2.setString(5, oNewRatingDto.getLtProdLineId());
	        		pstmt2.setString(6, oNewRatingDto.getLtKWId());
	        		pstmt2.setInt(7, Integer.parseInt(oNewRatingDto.getLtPoleId()));
	        		pstmt2.setString(8, oNewRatingDto.getLtFrameId());
	        		pstmt2.setString(9, oNewRatingDto.getLtFrameSuffixId());
	        		pstmt2.setString(10, oNewRatingDto.getLtMountingId());
	        		pstmt2.setString(11, oNewRatingDto.getLtTBPositionId());
	        		pstmt2.setString(12, oNewRatingDto.getTagNumber());
	        		pstmt2.setString(13, oNewRatingDto.getLtManufacturingLocation());
	        		pstmt2.setString(14, oNewRatingDto.getLtTotalAddonPercent());
	        		pstmt2.setString(15, oNewRatingDto.getLtTotalCashExtra());
	        		iResult = pstmt2.executeUpdate();
	    		}
	    		
        		// RFQ_LT_RATING_DETAILS - Update the MTO Highlight Flag.
	    		if(StringUtils.isNotBlank(oNewRatingDto.getMtoHighlightFlag())) {
	    			oDBUtility.releaseResources(rs, pstmt2);
		    		pstmt2 = conn.prepareStatement(UPDATE_LT_MTO_HIGHLIGHTFLAG);
		    		pstmt2.setString(1, oNewRatingDto.getMtoHighlightFlag());
		    		pstmt2.setInt(2, oNewRatingDto.getEnquiryId());
	        		pstmt2.setInt(3, oNewRatingDto.getRatingNo());
	        		pstmt2.executeUpdate();
	    		}
	    		
        	}
        	
        } finally {
        	/* Releasing PreparedStatement objects */
            oDBUtility.releaseResources(pstmt);
            oDBUtility.releaseResources(pstmt1);
            oDBUtility.releaseResources(pstmt2);
        }
        
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );
	}
	
}
