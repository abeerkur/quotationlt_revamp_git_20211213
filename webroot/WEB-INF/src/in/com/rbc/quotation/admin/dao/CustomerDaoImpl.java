/**
 * *****************************************************************************
 * Project Name: quotation
 * Document Name: CustomerDaoImpl.java
 * Package: in.com.rbc.quotation.admin.dao
 * Desc: 
 * *****************************************************************************
 * Author: 100005701 (Suresh)
 * Date: Sep 30, 2008
 * *****************************************************************************
 */
package in.com.rbc.quotation.admin.dao;

import in.com.rbc.quotation.admin.dto.CustomerDto;
import in.com.rbc.quotation.admin.dto.DesignEngineerDto;

import in.com.rbc.quotation.common.utility.DBUtility;
import in.com.rbc.quotation.common.utility.ListDBColumnUtil;
import in.com.rbc.quotation.common.utility.ListModel;
import in.com.rbc.quotation.common.utility.LoggerUtility;
import in.com.rbc.quotation.common.vo.KeyValueVo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Hashtable;

import org.apache.commons.lang.StringUtils;



/**
 * @author 100005701 (Suresh Shanmugam)
 *
 */
public class CustomerDaoImpl implements CustomerDao,AdminQueries {
	
	public Hashtable getCustomer(Connection conn, CustomerDto oCustomerDto, ListModel oListModel) throws Exception {
			/*
	    	 * Setting the Method Name as generic for logger Utility purpose . 
	    	 */
    		String sMethodName = "getCustomer" ;
    		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
    		
    		/* PreparedStatement object to handle database operations */
            PreparedStatement pstmt = null ;
            
            /* ResultSet object to store the rows retrieved from database */
            ResultSet rs = null ;      
            
            /* Object of DBUtility class to handle database operations */
            DBUtility oDBUtility = new DBUtility();
            /*
             * Hashtable that stores the following values, and that is used to display results in search page
             * --> ArrayList containing the rows retrieved from database
             * --> ListModel object that hold the values used to retrieve values and display the same
             * --> String array containing the list of column names to be displayed as column headings
             */
            Hashtable htSearchResults = null;
            ArrayList alSearchResultsList = new ArrayList(); // Stores the list of rows retrieved from the database
            int iCursorPosition = 0; // Stores the cursor position from where the records have to be retrieved // from database; this is dependent on the page number selected for viewing
            String sWhereString = null; // Stores the Where clause condition
            String sSqlQueryString = null; // Stores the complete SQL query to be executed
            CustomerDto oTempCustomerDto=null ;// Create the Temporary cusomter Object for storing the table column values
            
            try {
            	
            	sWhereString = " AND UPPER(CUS.QCU_NAME) LIKE UPPER('"+oCustomerDto.getCustomerName()+"%')";
            	sSqlQueryString = COUNT_CUSTOMER_SEARCHRESULT + sWhereString;
            	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName+"..Search Customer Count Query",sSqlQueryString);
            	
            	pstmt = conn.prepareCall(sSqlQueryString);
            	
            	/* Execute the Query for counting the number records*/
            	rs=pstmt.executeQuery();
            	
            	if(rs.next()) {
            		oListModel.setTotalRecordCount(rs.getInt(1));//set total number of records available in table depond upon the where conditions .
            	} else {
            		oListModel.setTotalRecordCount(0);// if the Query return no records , it's set the Total recordcount  "0" as default
            	}
            	oDBUtility.releaseResources(rs, pstmt);
            	
            	if(oListModel.getTotalRecordCount()>0) {
            		 htSearchResults = new Hashtable();
            		 
	    		 if(oListModel.getCurrentPageInt()<0)
	    			 throw new Exception("Current Page is negative in search method...");
            		 
            		 iCursorPosition = (oListModel.getCurrentPageInt()-1) * oListModel.getRecordsPerPageInt();
            		 
            		 if(iCursorPosition < oListModel.getTotalRecordCount()) {
                 		
    			 /*
    			  * Select the values from the table Using FETCH_SEARCHcusomter_RESULTS Query depond upon the 
    			  * sWhereString condition and return into the sSqlQueryString as StringBuffer 
    			  * and the data's are ordered by cusomter DBFiled as by default
    			  * 
    			  */
                 		sSqlQueryString = new StringBuffer("SELECT * FROM (SELECT Q.*, rownum AS rnum FROM ( ")
     						                    .append(FETCH_CUSTOMER_SEARCHRESULT)
     						                    .append(sWhereString)
     						                    .append(" ORDER BY ")
     						                    .append(ListDBColumnUtil.getCustomerSearchResultDBField(oListModel.getSortBy()))
     						                    .append(" " + oListModel.getSortOrderDesc())
     						                    .append(" ) Q WHERE rownum <= (")
     						                    .append(iCursorPosition + " + " + oListModel.getRecordsPerPage() + ")) ")
     						                    .append(" WHERE rnum > " + iCursorPosition + "")
     						                    .toString();
                 		

                 		
                 		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName+"..SearchCustomer Retrive Query",sSqlQueryString );
                 		pstmt = conn.prepareStatement(sSqlQueryString, ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
                 		
                 		rs = pstmt.executeQuery();// Execute the Search Query to retriuve the cusomter Values .
                 		if (rs.next()) {
                             do {
                            	 oTempCustomerDto = new CustomerDto();
                            	 oTempCustomerDto.setCustomer(rs.getString("QCU_NAME"));
                            	 oTempCustomerDto.setCustomerId(rs.getInt("QCU_CUSTOMERID"));
                            	 oTempCustomerDto.setContactperson(rs.getString("QCU_CONTACTPERSON"));
                            	 oTempCustomerDto.setLocationname(rs.getString("QLO_LOCATIONNAME"));
                            	 oTempCustomerDto.setEmail(rs.getString("QCU_EMAIL"));
                            	 oTempCustomerDto.setPhone(rs.getString("QCU_PHONE"));
                            	 oTempCustomerDto.setSalesengineerId(rs.getInt("QCU_SALESENGINEERID"));
                            	 oTempCustomerDto.setCustomerType(rs.getString("QCU_CUSTOMERTYPE"));
                            	 oTempCustomerDto.setIndustry(rs.getString("QCU_INDUSTRY"));
                            	 oTempCustomerDto.setSapcode(rs.getString("QCU_SAPCODE"));
                            	 oTempCustomerDto.setOraclecode(rs.getString("QCU_ORACLECODE"));
                            	 oTempCustomerDto.setGstnumber(rs.getString("QCU_GSTNUMBER"));
                            	 oTempCustomerDto.setState(rs.getString("QCU_STATE"));
                            	 oTempCustomerDto.setCountry(rs.getString("QCU_COUNTRY"));
                            	 oTempCustomerDto.setCustomerRSM(rs.getString("QCU_CUSTOMER_RSM"));
                            	 oTempCustomerDto.setCustomerIndustry(rs.getString("QCU_CUSTOMERINDUSTRY"));
                            	 oTempCustomerDto.setCustomerSalesGroup(rs.getString("QCU_SALESGROUP"));
                            	 oTempCustomerDto.setCustomerClass(rs.getString("QCU_CUSTOMER_CLASS"));
                            	 
                            	 alSearchResultsList.add(oTempCustomerDto);
                            } while (rs.next());
                 		}
                   }
            		 
            		 htSearchResults.put("resultsList", alSearchResultsList);// put the SearchresultList of arraylist value into Hashtable 
                     htSearchResults.put("ListModel", oListModel); // put the ListModel values into hashtable 
                     htSearchResults.put("customerQuery", sWhereString);//keep the Where condition for using in Export to Excel purpose .
                 }
            	
            } finally {
            	 /* Releasing ResultSet & PreparedStatement objects */
                oDBUtility.releaseResources(rs, pstmt);
            }
            LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END.." );
            return htSearchResults ;
	}
	
	public ArrayList getLocationList(Connection conn) throws Exception  {
			/*
	    	 * Setting the Method Name as generic for logger Utility purpose . 
	    	 */
    		String sMethodName = "getLocationList" ;
    		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
    		
    		/* PreparedStatement object to handle database operations */
            PreparedStatement pstmt = null ;
            
            /* ResultSet object to store the rows retrieved from database */
            ResultSet rs = null ;      
            
            /* Object of DBUtility class to handle database operations */
            DBUtility oDBUtility = new DBUtility();
            
            /*
             * Querying the database to retrieve the cusomter description, storing the
             * details in KeyValueVo object, storing these object in an ArrayList 
             * and returning the same
             */
            KeyValueVo oKeyValueVo = null;
            String sSqlQuery=null;
           
             /* ArrayList to store the list of request status */
            ArrayList alSearchResultsList = new ArrayList(); // Stores the list of rows retrieved from the database
            
            try {
            	sSqlQuery=FETCH_LOCATION_LIST;
            	pstmt = conn.prepareStatement(sSqlQuery);
            	rs = pstmt.executeQuery();
            	while(rs.next()) {
            		oKeyValueVo = new KeyValueVo();
            		oKeyValueVo.setKey(rs.getString("QLO_LOCATIONID"));
            		oKeyValueVo.setValue(rs.getString("QLO_LOCATIONNAME"));
            		alSearchResultsList.add(oKeyValueVo);
            	}
            	
            } finally {
                /* Releasing ResultSet & PreparedStatement objects */
            	oDBUtility.releaseResources(rs,pstmt);
            }
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );		
		return alSearchResultsList;
	}
	
	public ArrayList getSalesManagersList(Connection conn)throws Exception{
		
			/*
	    	 * Setting the Method Name as generic for logger Utility purpose . 
	    	 */
    		String sMethodName = "getSalesManagersList" ;
    		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
    		
    		/* PreparedStatement object to handle database operations */
            PreparedStatement pstmt = null ;
            
            /* ResultSet object to store the rows retrieved from database */
            ResultSet rs = null ;      
            
            /* Object of DBUtility class to handle database operations */
            DBUtility oDBUtility = new DBUtility();
            
            /*
             * Querying the database to retrieve the cusomter description, storing the
             * details in KeyValueVo object, storing these object in an ArrayList 
             * and returning the same
             */
            KeyValueVo oKeyValueVo = null;
            String sSqlQuery=null;
           
            /* ArrayList to store the list of request status */
            ArrayList alSearchResultsList = new ArrayList(); // Stores the list of rows retrieved from the database
            
            try {
            	sSqlQuery=FETCH_SALESMANGER_LIST;
            	pstmt = conn.prepareStatement(sSqlQuery);
            	rs = pstmt.executeQuery();
            	while(rs.next()) {
            		oKeyValueVo = new KeyValueVo();
            		oKeyValueVo.setKey(rs.getString("QSM_SALESMGR"));
            		oKeyValueVo.setValue(rs.getString("SALESMANAGER_NAME"));
            		alSearchResultsList.add(oKeyValueVo);
            	}
            	
            } finally {
                /* Releasing ResultSet & PreparedStatement objects */
            	oDBUtility.releaseResources(rs,pstmt);
            }
            
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );		
		
        return alSearchResultsList;
	}
	
	public ArrayList getLocationsByCustomerName(Connection conn, 
                                                CustomerDto oCustomerDto, 
                                                String displayContactPerson) throws Exception {
		
        /*
    	 * Setting the Method Name as generic for logger Utility purpose . 
    	 */
		String sMethodName = "getLocationsByCustomerName" ;
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;      
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        /*
         * Querying the database to retrieve the cusomter description, storing the
         * details in KeyValueVo object, storing these object in an ArrayList 
         * and returning the same
         */
        KeyValueVo oKeyValueVo = null;
        String sSQLWhereQuery =null;
        String sSqlQuery =null;
       
        /* ArrayList to store the list of request status */
        ArrayList alSearchResultsList = new ArrayList(); // Stores the list of rows retrieved from the database
        
        try {
        	sSQLWhereQuery = " AND UPPER(QCU_NAME) = UPPER('"+oCustomerDto.getCustomer()+"') ";           	
        	sSqlQuery = FETCH_LOCATIONS_BYCUSTOMERNAME+sSQLWhereQuery;
        	
        	pstmt = conn.prepareStatement(sSqlQuery);
        	rs = pstmt.executeQuery();
        	while(rs.next()) {
        		oKeyValueVo = new KeyValueVo();
        		oKeyValueVo.setKey(rs.getString("QLO_LOCATIONID"));
                if ( displayContactPerson.equalsIgnoreCase("YES") ) {
                    oKeyValueVo.setValue(rs.getString("QLO_LOCATIONNAME") + " - " + rs.getString("QCU_CONTACTPERSON"));
                } else {
                    oKeyValueVo.setValue(rs.getString("QLO_LOCATIONNAME"));
                }
                oKeyValueVo.setValue2(rs.getString("QCU_CUSTOMERID"));
        		alSearchResultsList.add(oKeyValueVo);
        	}
        } finally {
            /* Releasing ResultSet & PreparedStatement objects */
        	oDBUtility.releaseResources(rs,pstmt);
        }
        
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );		
		return alSearchResultsList;
	}
	
	public CustomerDto getCustomerInfo(Connection conn , CustomerDto oCustomerDto)throws Exception {
		/*
    	 * Setting the Method Name as generic for logger Utility purpose . 
    	 */
    		String sMethodName = "getCustomerInfo" ;
    		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
    		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;      
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();

        try {
        	pstmt = conn.prepareStatement(FETCH_CUSTOMER_VALUE);
        	pstmt.setInt(1,oCustomerDto.getCustomerId());
        	rs = pstmt.executeQuery();
			while(rs.next()) {
				oCustomerDto.setCustomerName(rs.getString("QCU_NAME"));
				oCustomerDto.setCustomer(rs.getString("QCU_NAME"));
				oCustomerDto.setLocationId(rs.getInt("QCU_LOCATIONID"));
				oCustomerDto.setContactperson(rs.getString("QCU_CONTACTPERSON"));
				oCustomerDto.setEmail(rs.getString("QCU_EMAIL"));
				oCustomerDto.setPhone(rs.getString("QCU_PHONE"));
				oCustomerDto.setMobile(rs.getString("QCU_MOBILE"));
				oCustomerDto.setFax(rs.getString("QCU_FAX"));
				oCustomerDto.setAddress(rs.getString("QCU_ADDRESS"));
				
				oCustomerDto.setSalesengineerId(rs.getInt("QCU_SALESENGINEERID"));
				oCustomerDto.setCustomerType(rs.getString("QCU_CUSTOMERTYPE"));
				oCustomerDto.setIndustry(rs.getString("QCU_INDUSTRY"));
				oCustomerDto.setSapcode(rs.getString("QCU_SAPCODE"));
				oCustomerDto.setOraclecode(rs.getString("QCU_ORACLECODE"));
				oCustomerDto.setGstnumber(rs.getString("QCU_GSTNUMBER"));
				oCustomerDto.setState(rs.getString("QCU_STATE"));
				oCustomerDto.setCountry(rs.getString("QCU_COUNTRY"));
				oCustomerDto.setCustomerRSM(rs.getString("QCU_CUSTOMER_RSM"));
				oCustomerDto.setCustomerIndustry(rs.getString("QCU_CUSTOMERINDUSTRY"));
				oCustomerDto.setCustomerSalesGroup(rs.getString("QCU_SALESGROUP"));
				oCustomerDto.setCustomerClass(rs.getString("QCU_CUSTOMER_CLASS"));
			}
		} finally {
			/* Releasing ResultSet & PreparedStatement objects */
        	oDBUtility.releaseResources(rs,pstmt);
		}
        
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );		
		
		return oCustomerDto;
	}
	
	public boolean createCustomer(Connection conn, CustomerDto oCustomerDto)throws Exception {
		/*
    	 * Setting the Method Name as generic for logger Utility purpose . 
    	 */
    	String sMethodName = "createCustomer" ;
    	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        PreparedStatement pstmt1 = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;      
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        /*getting the newlly Customer ID from the Customer sequence */
        int iGetCustomerID = 0;
        boolean isCreated = false ;

        try {
        	/**
        	 * Getting the Customer id from the Customer Sequence for
        	 */
        	pstmt1 = conn.prepareStatement(FETCH_CUSTOEMRID_MAXNEXTVAL);
        	rs = pstmt1.executeQuery();// Execute the creating the Customer Sequence Query,
        	
        	if(rs.next()) {
        		iGetCustomerID = rs.getInt("QCU_CUSTOMERID");
        		iGetCustomerID = iGetCustomerID + 1;
        	}

        	pstmt = conn.prepareStatement(INSERT_CUSTOMER_VALUE);
        	pstmt.setInt(1, iGetCustomerID);
        	pstmt.setString(2, oCustomerDto.getCustomerName());
        	pstmt.setInt(3, oCustomerDto.getLocationId());
        	pstmt.setString(4, oCustomerDto.getContactperson());
        	pstmt.setString(5, oCustomerDto.getEmail());
        	pstmt.setString(6, oCustomerDto.getPhone());
        	pstmt.setString(7, oCustomerDto.getMobile());
        	pstmt.setString(8, oCustomerDto.getFax());
        	pstmt.setString(9, oCustomerDto.getAddress());
        	pstmt.setString(10, oCustomerDto.getCreatedById());
        	pstmt.setString(11, oCustomerDto.getLastUpdatedById());
        	
        	pstmt.setLong(12, oCustomerDto.getSalesengineerId());
        	pstmt.setString(13, oCustomerDto.getCustomerType());
        	pstmt.setString(14, oCustomerDto.getIndustry());
        	pstmt.setString(15, oCustomerDto.getSapcode());
        	pstmt.setString(16, oCustomerDto.getOraclecode());
        	pstmt.setString(17, oCustomerDto.getGstnumber());
        	pstmt.setString(18, oCustomerDto.getState());
        	pstmt.setString(19, oCustomerDto.getCountry());
        	
            int iUpdatedCount = pstmt.executeUpdate();
            if(iUpdatedCount > 0 )
                isCreated = true;
        } finally {
        	/* Releasing PreparedStatement objects */
            oDBUtility.releaseResources( rs,pstmt);
            oDBUtility.releaseResources(pstmt1);
        }
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );
        return isCreated;
	}
	
	public boolean saveCustomerValue(Connection conn , CustomerDto oCustomerDto)throws Exception{
		/*
    	 * Setting the Method Name as generic for logger Utility purpose . 
    	 */
    		String sMethodName = "saveCustomerValue" ;
    		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );

    	/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt= null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
		boolean isUpdated = false ;
        try {
        	pstmt = conn.prepareStatement(UPDATE_CUSTOMER_VALUE);

        	pstmt.setString(1, oCustomerDto.getCustomerName());
        	pstmt.setInt(2, oCustomerDto.getLocationId());
        	pstmt.setString(3, oCustomerDto.getContactperson());
        	pstmt.setString(4, oCustomerDto.getEmail());
        	pstmt.setString(5, oCustomerDto.getPhone());
        	pstmt.setString(6, oCustomerDto.getMobile());
        	pstmt.setString(7, oCustomerDto.getFax());
        	pstmt.setString(8, oCustomerDto.getAddress());
        	pstmt.setString(9, oCustomerDto.getLastUpdatedById());          
        	
        	pstmt.setInt(10, oCustomerDto.getSalesengineerId());
        	pstmt.setString(11, oCustomerDto.getCustomerType());
        	pstmt.setString(12, oCustomerDto.getIndustry());
        	pstmt.setString(13, oCustomerDto.getSapcode());
        	pstmt.setString(14, oCustomerDto.getOraclecode());
        	pstmt.setString(15, oCustomerDto.getGstnumber());
        	pstmt.setString(16, oCustomerDto.getState());
        	pstmt.setString(17, oCustomerDto.getCountry());
        	pstmt.setInt(18, oCustomerDto.getCustomerId());
        	
        	int isUdate = pstmt.executeUpdate();
        	
        	if(isUdate > 0) {
        		isUpdated = true ;
        	}
        } finally {
        	/* Releasing PreparedStatement objects */
            oDBUtility.releaseResources( pstmt);
        }
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );
		return isUpdated;
	}
	
	public boolean deleteCustomerValue(Connection conn , CustomerDto oCustomerDto)throws Exception{
		/*
    	 * Setting the Method Name as generic for logger Utility purpose . 
    	 */
    		String sMethodName = "deleteCustomerValue" ;
    		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );

       /* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
		boolean isDeleted = false ;
		int iDeleteCount=0;
		 try {
			 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"Upate Customer table DELETE_CUSTOMER_VALUE:="+DELETE_CUSTOMER_VALUE );
			 pstmt = conn.prepareStatement(DELETE_CUSTOMER_VALUE);
			 pstmt.setInt(1, oCustomerDto.getCustomerId());
			 iDeleteCount= pstmt.executeUpdate();
	        	if(iDeleteCount > 0) {
	        		isDeleted = true ;
	        	}
	        	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"isDeleted :="+isDeleted );
	        }finally {
	        	/* Releasing PreparedStatement objects */
	            oDBUtility.releaseResources( pstmt);
	        }
	        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );
		return isDeleted;
	}
	
    public boolean canDeleteCustomer(Connection conn, 
                                     CustomerDto oCustomerDto) throws Exception {
        /*
         * Setting the Method Name as generic for logger Utility purpose. 
         */
        String sMethodName = "canDeleteCustomer";
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
        
        /* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null;
        ResultSet rs= null;
        int iCountRow = -1 ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        boolean canDeleteCustomer = false;
        try {
        	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName," Before Update the Customer table check with Workflow table:-"+CHECK_CUSTOMER_ISACTIVE+"	oCustomerDto.getCustomerId() :-"+oCustomerDto.getCustomerId() );
        	pstmt = conn.prepareStatement(CHECK_CUSTOMER_ISACTIVE);
        	pstmt.setInt(1, oCustomerDto.getCustomerId());
        	rs = pstmt.executeQuery();
            if(rs.next()) {
	        	iCountRow = rs.getInt(1);
	        	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"After Execute Query the number of records avaialble iCountRow :="+iCountRow);
	        }
            
            if ( iCountRow==0 ) {
                canDeleteCustomer = true;
            } else { 
                canDeleteCustomer = false;
            }
            LoggerUtility.log("INFO", this.getClass().getName(),sMethodName,"canDeleteCustomer :="+canDeleteCustomer);
        } finally {
            /* Releasing PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
        return canDeleteCustomer;
    }
    
	public ArrayList exportCustomerSearchResult(Connection conn , CustomerDto oCustomerDto, String sWhereQuery, String sSortFilter, String sSortType)throws Exception{
		/*
    	 * Setting the Method Name as generic for logger Utility purpose . 
    	 */
    		String sMethodName = "exportCustomerSearchResult " ;
    		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
    		
    		/* PreparedStatement object to handle database operations */
            PreparedStatement pstmt = null ;
            
            /* ResultSet object to store the rows retrieved from database */
            ResultSet rs = null ;      
            
            /* Object of DBUtility class to handle database operations */
            DBUtility oDBUtility = new DBUtility();
            
            ArrayList alSearchResultsList = new ArrayList(); // Stores the list of rows retrieved from the database
            
            String sSqlQueryString = null; // Stores the complete SQL query to be executed
            
            CustomerDto oTempCustomerDto ;// Create the Temporary cusomter Object for storing the table column values
            
            try {
            	
            	sSqlQueryString = FETCH_CUSTOMER_SEARCHRESULT + sWhereQuery;
            	
            	if (sSortFilter != null && sSortFilter.trim().length() > 0){
            		sSqlQueryString = sSqlQueryString + " ORDER BY "+ sSortFilter;
            		if (sSortType != null && sSortType.trim().length() > 0)
            			sSqlQueryString = sSqlQueryString + " " +sSortType;
            	}
                
            	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName+"..Search Customer Count Query",sSqlQueryString);
            	
            	pstmt = conn.prepareCall(sSqlQueryString);
            	rs = pstmt.executeQuery();// Execute the Search Query to retriuve the cusomter Values .
            	
            	 
         		if (rs.next()) {
                     do {
                    	 oTempCustomerDto = new CustomerDto();
                    	 oTempCustomerDto.setCustomer(rs.getString("QCU_NAME"));
                    	 oTempCustomerDto.setCustomerId(rs.getInt("QCU_CUSTOMERID"));
                    	 oTempCustomerDto.setContactperson(rs.getString("QCU_CONTACTPERSON"));
                    	 oTempCustomerDto.setLocationname(rs.getString("QLO_LOCATIONNAME"));
                    	 oTempCustomerDto.setEmail(rs.getString("QCU_EMAIL"));
                    	 oTempCustomerDto.setPhone(rs.getString("QCU_PHONE"));
                    	 oTempCustomerDto.setMobile(rs.getString("QCU_MOBILE"));
                    	 oTempCustomerDto.setFax(rs.getString("QCU_FAX"));
                    	 oTempCustomerDto.setAddress(rs.getString("QCU_ADDRESS"));
                    	 
                    	 oTempCustomerDto.setSalesengineerId(rs.getInt("QCU_SALESENGINEERID"));
                    	 oTempCustomerDto.setCustomerType(rs.getString("QCU_CUSTOMERTYPE"));
                    	 oTempCustomerDto.setIndustry(rs.getString("QCU_INDUSTRY"));
                    	 oTempCustomerDto.setSapcode(rs.getString("QCU_SAPCODE"));
                    	 oTempCustomerDto.setOraclecode(rs.getString("QCU_ORACLECODE"));
                    	 oTempCustomerDto.setGstnumber(rs.getString("QCU_GSTNUMBER"));
                    	 oTempCustomerDto.setState(rs.getString("QCU_STATE"));
                    	 oTempCustomerDto.setCountry(rs.getString("QCU_COUNTRY"));
                    	 
                    	 alSearchResultsList.add(oTempCustomerDto);
                    } while (rs.next());
                 }
            } finally {
            	/* Releasing PreparedStatement objects */
                oDBUtility.releaseResources( rs,pstmt);
            }
		return alSearchResultsList;
	}
	
	public ArrayList getCustomerName(Connection conn , CustomerDto oCustomerDto)throws Exception {
	/*
	 * Setting the Method Name as generic for logger Utility purpose . 
	 */
		String sMethodName = "getCustomerName" ;
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
		/* PreparedStatement object to handle database operations */
        	PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        	ResultSet rs= null ;      
        
        /* Object of DBUtility class to handle database operations */
        	DBUtility oDBUtility = new DBUtility();
        	
    	/* ArrayList to store the list of request status */
    	ArrayList alSearchResultsList = new ArrayList(); // Stores the list of rows retrieved from the database
    	CustomerDto oTempCustomerDto ;// Create the Temporary Location Object for storing the table column values 
    	String sSQLWhereQuery =null;
    	try {
    		sSQLWhereQuery = " WHERE UPPER(QCU_NAME) LIKE UPPER('"+oCustomerDto.getCustomer()+"%') AND QCU_ISDELETED='N' AND QCU_LOCATIONISACTIVE = 'A'";    		
    		pstmt = conn.prepareStatement(FETCH_CUSTOMER_NAME+sSQLWhereQuery);
    		LoggerUtility.log("INFO",this.getClass().getName(),sMethodName,"FETCH CUSTOMER QUERY :="+FETCH_CUSTOMER_NAME);
    		rs = pstmt.executeQuery();
    		while (rs.next()) {
    			oTempCustomerDto = new CustomerDto();
    			oTempCustomerDto.setCustomer(rs.getString("QCU_NAME"));
    			oTempCustomerDto.setCustomerId(rs.getInt("QCU_CUSTOMERID"));
    			oTempCustomerDto.setCustomerIndustryName(rs.getString("QCU_CUSTOMERINDUSTRY"));
    			alSearchResultsList.add(oTempCustomerDto);
    		}
    	}finally {
    		/* Releasing ResultSet & PreparedStatement objects */
        	oDBUtility.releaseResources(rs,pstmt);
    	}
    	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );				
		return alSearchResultsList;
	}
	
	public ArrayList getLocationName(Connection conn , CustomerDto oCustomerDto)throws Exception {
		String sMethodName = "getLocationName" ;
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs= null ;      
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        	
    	/* ArrayList to store the list of request status */
    	ArrayList alSearchResultsList = new ArrayList(); // Stores the list of rows retrieved from the database
    	CustomerDto oTempCustomerDto ;// Create the Temporary Location Object for storing the table column values 
    	String sSQLWhereQuery = null;
    	
    	try {
    		sSQLWhereQuery = " WHERE UPPER(QCU_LOCATIONNAME) LIKE UPPER('"+oCustomerDto.getCustomer()+"%') AND QCU_ISDELETED='N' AND QCU_LOCATIONISACTIVE = 'A'";    		
    		pstmt = conn.prepareStatement(FETCH_LOCATION_NAME+sSQLWhereQuery);
    		LoggerUtility.log("INFO",this.getClass().getName(),sMethodName,"FETCH LOCATION QUERY :="+FETCH_LOCATION_NAME);
    		rs = pstmt.executeQuery();
    		while (rs.next()) {
    			oTempCustomerDto = new CustomerDto();
    			oTempCustomerDto.setLocationname(rs.getString("QCU_LOCATIONNAME"));
    			oTempCustomerDto.setLocationId(rs.getInt("QCU_LOCATIONID"));
    			alSearchResultsList.add(oTempCustomerDto);
    		}
    	} finally {
    		/* Releasing ResultSet & PreparedStatement objects */
        	oDBUtility.releaseResources(rs,pstmt);
    	}
    	
    	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );				
		return alSearchResultsList;
	}
	
	public ArrayList getLocationBySearchString(Connection conn , CustomerDto oCustomerDto) throws Exception {
		String sMethodName = "getLocationName" ;
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs= null ;      
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        	
    	/* ArrayList to store the list of request status */
    	ArrayList alSearchResultsList = new ArrayList(); // Stores the list of rows retrieved from the database
    	CustomerDto oTempCustomerDto ;// Create the Temporary Location Object for storing the table column values 
    	String sSQLWhereQuery = null;
    	
    	try {
    		sSQLWhereQuery = " WHERE UPPER(QLO_LOCATIONNAME) LIKE UPPER('"+oCustomerDto.getCustomer()+"%') AND QLO_ISACTIVE = 'A'";    		
    		pstmt = conn.prepareStatement(FETCH_LOCATION_BY_SEARCHSTR+sSQLWhereQuery);
    		LoggerUtility.log("INFO",this.getClass().getName(),sMethodName,"FETCH LOCATION QUERY := "+FETCH_LOCATION_BY_SEARCHSTR);
    		rs = pstmt.executeQuery();
    		while (rs.next()) {
    			oTempCustomerDto = new CustomerDto();
    			oTempCustomerDto.setLocationname(rs.getString("QLO_LOCATIONNAME"));
    			oTempCustomerDto.setLocationId(rs.getInt("QLO_LOCATIONID"));
    			alSearchResultsList.add(oTempCustomerDto);
    		}
    	} finally {
    		/* Releasing ResultSet & PreparedStatement objects */
        	oDBUtility.releaseResources(rs,pstmt);
    	}
    	
    	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );				
		return alSearchResultsList;
	}
	
	public ArrayList getCustomerIndustryName(Connection conn , CustomerDto oCustomerDto)throws Exception {
		String sMethodName = "getLocationName" ;
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs= null ;      
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        	
    	/* ArrayList to store the list of request status */
    	ArrayList alSearchResultsList = new ArrayList(); // Stores the list of rows retrieved from the database
    	CustomerDto oTempCustomerDto ;// Create the Temporary Location Object for storing the table column values 
    	String sSQLWhereQuery = null;
    	
    	try {
    		sSQLWhereQuery = " WHERE UPPER(QCU_INDUSTRY) LIKE UPPER('"+oCustomerDto.getCustomer()+"%') AND QCU_ISDELETED='N' ";
    		pstmt = conn.prepareStatement(FETCH_CUSTOMER_INDUSTRY_NAME+sSQLWhereQuery);
    		LoggerUtility.log("INFO",this.getClass().getName(),sMethodName,"FETCH LOCATION QUERY :="+FETCH_CUSTOMER_INDUSTRY_NAME);
    		rs = pstmt.executeQuery();
    		
    		while (rs.next()) {
    			oTempCustomerDto = new CustomerDto();
    			oTempCustomerDto.setCustomerIndustryName(rs.getString("QCU_INDUSTRY"));
    			alSearchResultsList.add(oTempCustomerDto);
    		} 		
    	} finally {
    		/* Releasing ResultSet & PreparedStatement objects */
        	oDBUtility.releaseResources(rs,pstmt);
    	}
    	
    	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );				
		return alSearchResultsList;
	}
	
	public int isCustomerAlreadyExists(Connection conn , CustomerDto oCustomerDto) throws Exception {
		
		/*
    	* Setting the Method Name as generic for logger Utility purpose . 
        */
		String sMethodName = "isCustomerAlreadyExists" ;
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs= null ;      
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        /*getting the newlly Customer ID from the Customer sequence */
        int iExists = 0;
        
        try {
        	pstmt = conn.prepareStatement(FETCH_EXISTS_CUSTOMER);
			LoggerUtility.log("INFO",this.getClass().getName(),sMethodName,"FETCH_EXISTS_CUSTOMER  :="+FETCH_EXISTS_CUSTOMER);
			pstmt.setString(1,oCustomerDto.getCustomerName());
			pstmt.setInt(2,oCustomerDto.getLocationId());
			pstmt.setString(3,oCustomerDto.getContactperson());
			rs = pstmt.executeQuery();
			
			while(rs.next()) {

				if(rs.getInt(1)>0)
					iExists = 1;
			}
		}finally {
			/* Releasing ResultSet & PreparedStatement objects */
        		oDBUtility.releaseResources(rs,pstmt);
		}

		return iExists;
	}
	
	public boolean updateExistCustomer(Connection conn , CustomerDto oCustomerDto) throws Exception {
		
		/*
    	 * Setting the Method Name as generic for logger Utility purpose . 
    	 */
		String sMethodName = "updateExistCustomer" ;
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
    	/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
		boolean isUpdated = false ;
		int iUpdatedRowCount =0;

        try {
        	LoggerUtility.log("INFO",this.getClass().getName(),sMethodName,"Customer Update Query  :="+UPDATE_EXISTS_CUSTOMER  +"Exists  oCustomerDto.getCustomerName() :="+ oCustomerDto.getCustomerName());
        	pstmt = conn.prepareStatement(UPDATE_EXISTS_CUSTOMER);
        	pstmt.setString(1, oCustomerDto.getLastUpdatedById());
        	pstmt.setString(2, oCustomerDto.getCustomerName());
        	pstmt.setInt(3, oCustomerDto.getLocationId());
        	pstmt.setString(4, oCustomerDto.getContactperson());
            iUpdatedRowCount = pstmt.executeUpdate();
        	if(iUpdatedRowCount > 0) {
        		
        		isUpdated = true ;
        	}
        }finally {
        	/* Releasing PreparedStatement objects */
            oDBUtility.releaseResources( pstmt);
        }
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );
		return isUpdated;
	}
	
	@Override
	public ArrayList fetchLocationsByCustomerId(Connection conn , CustomerDto oCustomerDto) throws Exception {
		/* Setting the Method Name as generic for logger Utility purpose . */
		String sMethodName = "fetchLocationsByCustomerId" ;
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;      
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        /*
         * Querying the database to retrieve the cusomter description, storing the
         * details in KeyValueVo object, storing these object in an ArrayList 
         * and returning the same
         */
        KeyValueVo oKeyValueVo = null;
        String sSQLWhereQuery =null;
        String sSqlQuery =null;
       
        /* ArrayList to store the list of request status */
        ArrayList alSearchResultsList = new ArrayList(); // Stores the list of rows retrieved from the database
        
        try {
        	sSQLWhereQuery = " AND CUS.QCU_CUSTOMERID = "+oCustomerDto.getCustomerId()+" ";           	
        	sSqlQuery = FETCH_LOCATIONS_BYCUSTOMERNAME + sSQLWhereQuery;
        	
        	pstmt = conn.prepareStatement(sSqlQuery);
        	rs = pstmt.executeQuery();
        	while(rs.next()) {
        		oKeyValueVo = new KeyValueVo();
        		oKeyValueVo.setKey(rs.getString("QLO_LOCATIONID"));
        		oKeyValueVo.setValue(rs.getString("QLO_LOCATIONNAME"));
        		alSearchResultsList.add(oKeyValueVo);
        	}
        	
        } finally {
            /* Releasing ResultSet & PreparedStatement objects */
        	oDBUtility.releaseResources(rs,pstmt);
        }
        
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );		
		return alSearchResultsList;
	}

	public ArrayList fetchContactsByCustomerId(Connection conn , CustomerDto oCustomerDto) throws Exception {
		/* Setting the Method Name as generic for logger Utility purpose . */
		String sMethodName = "fetchContactsByCustomerId" ;
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;      
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        /*
         * Querying the database to retrieve the cusomter description, storing the
         * details in KeyValueVo object, storing these object in an ArrayList 
         * and returning the same
         */
        KeyValueVo oKeyValueVo = null;
        String sSQLWhereQuery =null;
        String sSqlQuery =null;
       
        /* ArrayList to store the list of request status */
        ArrayList alSearchResultsList = new ArrayList(); // Stores the list of rows retrieved from the database
        
        try {
        	sSQLWhereQuery = " QCU_CUSTOMERID = "+oCustomerDto.getCustomerId()+" ";           	
        	sSqlQuery = FETCH_CONTACTS_BYCUSTOMERID + sSQLWhereQuery;
        	
        	pstmt = conn.prepareStatement(sSqlQuery);
        	rs = pstmt.executeQuery();
        	if(rs.next()) {
        		oKeyValueVo = new KeyValueVo();
        		oKeyValueVo.setKey(rs.getString("QCU_CONTACTPERSON"));
        		oKeyValueVo.setValue(rs.getString("QCU_CONTACTPERSON"));
        		alSearchResultsList.add(oKeyValueVo);
        		
        		String res = "";
        		for(int i=2; i<=5; i++) {
        			res = rs.getString("QCU_CONTACTPERSON"+i);
        			if(StringUtils.isNotBlank(res)) {
        				oKeyValueVo = new KeyValueVo();
        				oKeyValueVo.setKey(res);
            			oKeyValueVo.setValue(res);
            			alSearchResultsList.add(oKeyValueVo);
        			}
        		}
        	}
        } finally {
            /* Releasing ResultSet & PreparedStatement objects */
        	oDBUtility.releaseResources(rs,pstmt);
        }
        
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );		
		return alSearchResultsList;
	}
	
	public String fetchCustomerIdByName(Connection conn, String sCustomerName) throws Exception {
		String sMethodName = "fetchCustomerIdByName" ;
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;      
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        String sCustomerId = "0";
        String sSQLWhereQuery =null;
        String sSqlQuery =null;
        
        try {
        	sSQLWhereQuery = " UPPER(QCU_NAME) LIKE UPPER('" + sCustomerName.trim() + "')";           	
        	sSqlQuery = FETCH_CUSTOMER_BY_CUSTOMERNAME + sSQLWhereQuery;
        	
        	pstmt = conn.prepareStatement(sSqlQuery);
        	rs = pstmt.executeQuery();
        	if(rs.next()) {
        		sCustomerId = rs.getString("QCU_CUSTOMERID");
        	}
        } finally {
            /* Releasing ResultSet & PreparedStatement objects */
        	oDBUtility.releaseResources(rs,pstmt);
        }
        
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );		
		return sCustomerId;
	}
	
	public String fetchCustomerTypeById(Connection conn, CustomerDto oCustomerDto) throws Exception {
		String sMethodName = "fetchCustomerTypeById" ;
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"START" );
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;      
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        String sCustomerType = "0";
        String sSQLWhereQuery =null;
        String sSqlQuery =null;
        
        try {
        	sSQLWhereQuery = " QCU_CUSTOMERID = " + oCustomerDto.getCustomerId() ;           	
        	sSqlQuery = FETCH_CUSTOMER_BY_CUSTOMERNAME + sSQLWhereQuery;
        	
        	pstmt = conn.prepareStatement(sSqlQuery);
        	rs = pstmt.executeQuery();
        	if(rs.next()) {
        		sCustomerType = rs.getString("QCU_CUSTOMERTYPE");
        	}
        } finally {
            /* Releasing ResultSet & PreparedStatement objects */
        	oDBUtility.releaseResources(rs,pstmt);
        }
        
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName,"END" );		
		return sCustomerType;
	}
	
}
