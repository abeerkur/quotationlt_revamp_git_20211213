/**
 * *****************************************************************************
 * Project Name: quotation
 * Document Name: TechnicalDataForm.java
 * Package: in.com.rbc.quotation.admin.form
 * Desc: 
 * *****************************************************************************
 * Author: 900008798 (Abhilash Moola)
 * Date: November 19, 2020
 * *****************************************************************************
 */
package in.com.rbc.quotation.admin.form;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

/**
 * @author 900008798 (Abhilash Moola)
 */
public class TechnicalDataForm extends ActionForm {
	
	private String invoke = null;
	FormFile technicalDataFile = null;
	private String operation;
	private int SERIAL_ID;
	private String MANUFACTURING_LOCATION;
	private String PRODUCT_GROUP;
	private String PRODUCT_LINE;
	private String STANDARD;
	private String POWER_RATING_KW;
	private String FRAME_TYPE;
	private String NUMBER_OF_POLES;
	private String MOUNTING_TYPE;
	private String TB_POSITION;
	private String MODEL_NO;
	private String VOLTAGE_V;
	private String POWER_RATING_HP;
	private String FREQUENCY_HZ;
	private String CURRENT_A;
	private String SPEED_MAX;
	private String POWER_FACTOR;
	private String EFFICIENCY;
	private String ELECTRICAL_TYPE;
	private String IP_CODE;
	private String INSULATION_CLASS;
	private String SERVICE_FACTOR;
	private String DUTY;
	private String NUMBER_OF_PHASES;
	private String MAX_AMBIENT_C;
	private String DE_BEARING_SIZE;
	private String DE_BEARING_TYPE;
	private String ODE_BEARING_SIZE;
	private String ODE_BEARING_TYPE;
	private String ENCLOSURE_TYPE;
	private String MOTOR_ORIENTATION;
	private String FRAME_MATERIAL;
	private String FRAME_LENGTH;
	private String ROTATION;
	private String NUMBER_OF_SPEEDS;
	private String SHAFT_TYPE;
	private String SHAFT_DIAMETER_MM;
	private String SHAFT_EXTENSION_MM;
	private String OUTLINE_DWG_NO;
	private String CONNECTION_DRAWING_NO;
	private String OVERALL_LENGTH_MM;
	private String STARTING_TYPE;
	private String THRU_BOLTS_EXTENSION;
	private String TYPE_OF_OVERLOAD_PROTECTION;
	private String CE;
	private String CSA;
	private String UL;
	
	public String getMANUFACTURING_LOCATION() {
		return MANUFACTURING_LOCATION;
	}
	public void setMANUFACTURING_LOCATION(String mANUFACTURING_LOCATION) {
		MANUFACTURING_LOCATION = mANUFACTURING_LOCATION;
	}
	public String getPRODUCT_GROUP() {
		return PRODUCT_GROUP;
	}
	public void setPRODUCT_GROUP(String pRODUCT_GROUP) {
		PRODUCT_GROUP = pRODUCT_GROUP;
	}
	public String getPRODUCT_LINE() {
		return PRODUCT_LINE;
	}
	public void setPRODUCT_LINE(String pRODUCT_LINE) {
		PRODUCT_LINE = pRODUCT_LINE;
	}
	public String getSTANDARD() {
		return STANDARD;
	}
	public void setSTANDARD(String sTANDARD) {
		STANDARD = sTANDARD;
	}
	public String getPOWER_RATING_KW() {
		return POWER_RATING_KW;
	}
	public void setPOWER_RATING_KW(String pOWER_RATING_KW) {
		POWER_RATING_KW = pOWER_RATING_KW;
	}
	public String getFRAME_TYPE() {
		return FRAME_TYPE;
	}
	public void setFRAME_TYPE(String fRAME_TYPE) {
		FRAME_TYPE = fRAME_TYPE;
	}
	public String getNUMBER_OF_POLES() {
		return NUMBER_OF_POLES;
	}
	public void setNUMBER_OF_POLES(String nUMBER_OF_POLES) {
		NUMBER_OF_POLES = nUMBER_OF_POLES;
	}
	public String getMOUNTING_TYPE() {
		return MOUNTING_TYPE;
	}
	public void setMOUNTING_TYPE(String mOUNTING_TYPE) {
		MOUNTING_TYPE = mOUNTING_TYPE;
	}
	public String getTB_POSITION() {
		return TB_POSITION;
	}
	public void setTB_POSITION(String tB_POSITION) {
		TB_POSITION = tB_POSITION;
	}
	public String getMODEL_NO() {
		return MODEL_NO;
	}
	public void setMODEL_NO(String mODEL_NO) {
		MODEL_NO = mODEL_NO;
	}
	public String getVOLTAGE_V() {
		return VOLTAGE_V;
	}
	public void setVOLTAGE_V(String vOLTAGE_V) {
		VOLTAGE_V = vOLTAGE_V;
	}
	public String getPOWER_RATING_HP() {
		return POWER_RATING_HP;
	}
	public void setPOWER_RATING_HP(String pOWER_RATING_HP) {
		POWER_RATING_HP = pOWER_RATING_HP;
	}
	public String getFREQUENCY_HZ() {
		return FREQUENCY_HZ;
	}
	public void setFREQUENCY_HZ(String fREQUENCY_HZ) {
		FREQUENCY_HZ = fREQUENCY_HZ;
	}
	public String getCURRENT_A() {
		return CURRENT_A;
	}
	public void setCURRENT_A(String cURRENT_A) {
		CURRENT_A = cURRENT_A;
	}
	public String getSPEED_MAX() {
		return SPEED_MAX;
	}
	public void setSPEED_MAX(String sPEED_MAX) {
		SPEED_MAX = sPEED_MAX;
	}
	public String getPOWER_FACTOR() {
		return POWER_FACTOR;
	}
	public void setPOWER_FACTOR(String pOWER_FACTOR) {
		POWER_FACTOR = pOWER_FACTOR;
	}
	public String getEFFICIENCY() {
		return EFFICIENCY;
	}
	public void setEFFICIENCY(String eFFICIENCY) {
		EFFICIENCY = eFFICIENCY;
	}
	public String getELECTRICAL_TYPE() {
		return ELECTRICAL_TYPE;
	}
	public void setELECTRICAL_TYPE(String eLECTRICAL_TYPE) {
		ELECTRICAL_TYPE = eLECTRICAL_TYPE;
	}
	public String getIP_CODE() {
		return IP_CODE;
	}
	public void setIP_CODE(String iP_CODE) {
		IP_CODE = iP_CODE;
	}
	public String getINSULATION_CLASS() {
		return INSULATION_CLASS;
	}
	public void setINSULATION_CLASS(String iNSULATION_CLASS) {
		INSULATION_CLASS = iNSULATION_CLASS;
	}
	public String getSERVICE_FACTOR() {
		return SERVICE_FACTOR;
	}
	public void setSERVICE_FACTOR(String sERVICE_FACTOR) {
		SERVICE_FACTOR = sERVICE_FACTOR;
	}
	public String getDUTY() {
		return DUTY;
	}
	public void setDUTY(String dUTY) {
		DUTY = dUTY;
	}
	public String getNUMBER_OF_PHASES() {
		return NUMBER_OF_PHASES;
	}
	public void setNUMBER_OF_PHASES(String nUMBER_OF_PHASES) {
		NUMBER_OF_PHASES = nUMBER_OF_PHASES;
	}
	public String getMAX_AMBIENT_C() {
		return MAX_AMBIENT_C;
	}
	public void setMAX_AMBIENT_C(String mAX_AMBIENT_C) {
		MAX_AMBIENT_C = mAX_AMBIENT_C;
	}
	public String getDE_BEARING_SIZE() {
		return DE_BEARING_SIZE;
	}
	public void setDE_BEARING_SIZE(String dE_BEARING_SIZE) {
		DE_BEARING_SIZE = dE_BEARING_SIZE;
	}
	public String getDE_BEARING_TYPE() {
		return DE_BEARING_TYPE;
	}
	public void setDE_BEARING_TYPE(String dE_BEARING_TYPE) {
		DE_BEARING_TYPE = dE_BEARING_TYPE;
	}
	public String getODE_BEARING_SIZE() {
		return ODE_BEARING_SIZE;
	}
	public void setODE_BEARING_SIZE(String oDE_BEARING_SIZE) {
		ODE_BEARING_SIZE = oDE_BEARING_SIZE;
	}
	public String getODE_BEARING_TYPE() {
		return ODE_BEARING_TYPE;
	}
	public void setODE_BEARING_TYPE(String oDE_BEARING_TYPE) {
		ODE_BEARING_TYPE = oDE_BEARING_TYPE;
	}
	public String getENCLOSURE_TYPE() {
		return ENCLOSURE_TYPE;
	}
	public void setENCLOSURE_TYPE(String eNCLOSURE_TYPE) {
		ENCLOSURE_TYPE = eNCLOSURE_TYPE;
	}
	public String getMOTOR_ORIENTATION() {
		return MOTOR_ORIENTATION;
	}
	public void setMOTOR_ORIENTATION(String mOTOR_ORIENTATION) {
		MOTOR_ORIENTATION = mOTOR_ORIENTATION;
	}
	public String getFRAME_MATERIAL() {
		return FRAME_MATERIAL;
	}
	public void setFRAME_MATERIAL(String fRAME_MATERIAL) {
		FRAME_MATERIAL = fRAME_MATERIAL;
	}
	public String getFRAME_LENGTH() {
		return FRAME_LENGTH;
	}
	public void setFRAME_LENGTH(String fRAME_LENGTH) {
		FRAME_LENGTH = fRAME_LENGTH;
	}
	public String getROTATION() {
		return ROTATION;
	}
	public void setROTATION(String rOTATION) {
		ROTATION = rOTATION;
	}
	public String getNUMBER_OF_SPEEDS() {
		return NUMBER_OF_SPEEDS;
	}
	public void setNUMBER_OF_SPEEDS(String nUMBER_OF_SPEEDS) {
		NUMBER_OF_SPEEDS = nUMBER_OF_SPEEDS;
	}
	public String getSHAFT_TYPE() {
		return SHAFT_TYPE;
	}
	public void setSHAFT_TYPE(String sHAFT_TYPE) {
		SHAFT_TYPE = sHAFT_TYPE;
	}
	public String getSHAFT_DIAMETER_MM() {
		return SHAFT_DIAMETER_MM;
	}
	public void setSHAFT_DIAMETER_MM(String sHAFT_DIAMETER_MM) {
		SHAFT_DIAMETER_MM = sHAFT_DIAMETER_MM;
	}
	public String getSHAFT_EXTENSION_MM() {
		return SHAFT_EXTENSION_MM;
	}
	public void setSHAFT_EXTENSION_MM(String sHAFT_EXTENSION_MM) {
		SHAFT_EXTENSION_MM = sHAFT_EXTENSION_MM;
	}
	public String getOUTLINE_DWG_NO() {
		return OUTLINE_DWG_NO;
	}
	public void setOUTLINE_DWG_NO(String oUTLINE_DWG_NO) {
		OUTLINE_DWG_NO = oUTLINE_DWG_NO;
	}
	public String getCONNECTION_DRAWING_NO() {
		return CONNECTION_DRAWING_NO;
	}
	public void setCONNECTION_DRAWING_NO(String cONNECTION_DRAWING_NO) {
		CONNECTION_DRAWING_NO = cONNECTION_DRAWING_NO;
	}
	public String getOVERALL_LENGTH_MM() {
		return OVERALL_LENGTH_MM;
	}
	public void setOVERALL_LENGTH_MM(String oVERALL_LENGTH_MM) {
		OVERALL_LENGTH_MM = oVERALL_LENGTH_MM;
	}
	public String getSTARTING_TYPE() {
		return STARTING_TYPE;
	}
	public void setSTARTING_TYPE(String sTARTING_TYPE) {
		STARTING_TYPE = sTARTING_TYPE;
	}
	public String getTHRU_BOLTS_EXTENSION() {
		return THRU_BOLTS_EXTENSION;
	}
	public void setTHRU_BOLTS_EXTENSION(String tHRU_BOLTS_EXTENSION) {
		THRU_BOLTS_EXTENSION = tHRU_BOLTS_EXTENSION;
	}
	public String getTYPE_OF_OVERLOAD_PROTECTION() {
		return TYPE_OF_OVERLOAD_PROTECTION;
	}
	public void setTYPE_OF_OVERLOAD_PROTECTION(String tYPE_OF_OVERLOAD_PROTECTION) {
		TYPE_OF_OVERLOAD_PROTECTION = tYPE_OF_OVERLOAD_PROTECTION;
	}
	public String getCE() {
		return CE;
	}
	public void setCE(String cE) {
		CE = cE;
	}
	public String getCSA() {
		return CSA;
	}
	public void setCSA(String cSA) {
		CSA = cSA;
	}
	public String getUL() {
		return UL;
	}
	public void setUL(String uL) {
		UL = uL;
	}
	public String getInvoke() {
		return invoke;
	}
	public void setInvoke(String invoke) {
		this.invoke = invoke;
	}
	public FormFile getTechnicalDataFile() {
		return technicalDataFile;
	}
	public void setTechnicalDataFile(FormFile technicalDataFile) {
		this.technicalDataFile = technicalDataFile;
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public int getSERIAL_ID() {
		return SERIAL_ID;
	}
	public void setSERIAL_ID(int sERIAL_ID) {
		SERIAL_ID = sERIAL_ID;
	}
	
	
	
	
}
