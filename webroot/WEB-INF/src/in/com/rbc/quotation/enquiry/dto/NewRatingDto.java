package in.com.rbc.quotation.enquiry.dto;

import java.util.ArrayList;

import in.com.rbc.quotation.common.utility.CommonUtility;
import in.com.rbc.quotation.common.vo.KeyValueVo;

public class NewRatingDto {

	private int ratingId;
	private int enquiryId;
	private int ratingNo;
	private int quantity;
	private String ratingTitle;
	private String ratingComments;
	private String isValidated;
	private String tagNumber;
	private String modelNumber;
	
	private String ratingCreatedBy;
	private String ratingCreatedBySSO;
	
	private boolean isFinalPriceCheck;
	private int htltId = 0;
	
	private String ltProdGroupId_MTO;
	private String ltProdLineId_MTO;
	private String ltKWId_MTO;
	private String ltPoleId_MTO;
	private String ltFrameId_MTO;
	private String ltFrameSuffixId_MTO;
	private String ltMountingId_MTO;
	private String ltTBPositionId_MTO;
	private String ltPercentCopper_MTO;
	private String ltPercentStamping_MTO;
	private String ltPercentAluminium_MTO;
	private String ltSpecialAddOn_MTO;
	private String ltSpecialCashExtra_MTO;
	private String ltTotalAddOn_MTO;
	private String ltTotalCashExtra_MTO;
	private String ltMaterialCost_MTO;
	
	// properties to fetch Id values in VIEW mode.
	private String ltProdGroupIdVal;
	private String ltProdLineIdVal;
	private String ltKWIdVal;
	private String ltPoleIdVal;
	private String ltFrameIdVal;
	private String ltFrameSuffixIdVal;
	private String ltMountingIdVal;
	private String ltTBPositionIdVal;
	
	private String ltProdGroupId;
	private String ltProdLineId;
	private String ltMotorTypeId;
	private String ltKWId;
	private String ltPoleId;
	private String ltFrameId;
	private String ltFrameSuffixId;
	private String ltMountingId;
	private String ltTBPositionId;
	private String ltAmbTempId;
	private String ltGasGroupId;
	private String ltApplicationId;
	private String ltEffClass;
	private String ltVoltId;
	private String ltVoltAddVariation;
	private String ltVoltRemoveVariation;
	private String ltFreqId;
	private String ltFreqAddVariation;
	private String ltFreqRemoveVariation;
	private String ltCombVariation;
	private String ltDutyId;
	private String ltCDFId;
	private String ltStartsId;
	private String ltTotalAddonPercent;
	private String ltTotalCashExtra;
	private String ltLPPerUnit;
	private String ltLPWithAddonPerUnit;
	private String ltStandardDiscount;
	private String ltPricePerUnit;
	private String ltTotalOrderPrice;
	private String ltAdditionalComments;
	private String ltEarlierMotorSerialNo;
	private String ltStandardDeliveryWks;
	private String ltCustReqDeliveryWks;
	private String ltRVId;
	private String ltRAId;
	private String ltWindingTreatmentId;
	private String ltWindingWire;
	private String ltLeadId;
	private String ltStartingCurrentOnDOLStart;
	private String ltWindingConfig;
	private String ltLoadGD2Value;
	private String ltMethodOfStarting;
	private String ltVFDApplTypeId;
	private String ltVFDMotorSpeedRangeMin;
	private String ltVFDMotorSpeedRangeMax;
	private String ltDualSpeedId;
	private String ltOverloadingDuty;
	private String ltConstantEfficiencyRange;
	private String ltShaftMaterialId;
	private String ltDirectionOfRotation;
	private String ltStandardRotation;
	private String ltMethodOfCoupling;
	private String ltPaintingTypeId;
	private String ltPaintShadeId;
	private String ltPaintThicknessId;
	private String ltIPId;
	private String ltInsulationClassId;
	private String ltTerminalBoxSizeId;
	private String ltSpreaderBoxId;
	private String ltAuxTerminalBoxId;
	private String ltSpaceHeaterId;
	private String ltVibrationId;
	private String ltFlyingLeadWithoutTDId;
	private String ltFlyingLeadWithTDId;
	private String ltMetalFanId;
	private String ltTechoMounting;
	private String ltShaftGroundingId;
	private String ltForcedCoolingId;
	private String ltHardware;
	private String ltGlandPlateId;
	private String ltDoubleCompressionGlandId;
	private String ltSPMMountingProvisionId;
	private String ltAddNamePlateId;
	private String ltDirectionArrowPlateId;
	private String ltRTDId;
	private String ltBTDId;
	private String ltThermisterId;
	private String ltCableSealingBoxId;
	private String ltBearingSystemId;
	private String ltBearingNDEId;
	private String ltBearingDEId;
	private String ltWitnessRoutineId;
	private String ltAdditionalTest1Id;
	private String ltAdditionalTest2Id;
	private String ltAdditionalTest3Id;
	private String ltTypeTestId;
	private String ltULCEId;
	private String ltQAPId;
	private String ltSeaworthyPackingId;
	private String ltWarrantyId;
	private String ltManufacturingLocation;
	private String ltSpares1;
	private String ltSpares2;
	private String ltSpares3;
	private String ltSpares4;
	private String ltSpares5;
	private String ltDataSheet;
	private String ltAddDrawingId;
	private String ltNonStdVoltageId;
	private String ltShaftTypeId;
	private String ltHazardAreaId;
	private String ltCustomerId;
	private String ltServiceFactor;
	private String ltRequestedDiscount_SM;
	private String ltRequestedPricePerUnit_SM;
	private String ltRequestedTotalPrice_SM;
	private String ltRequestedDiscount_RSM;
	private String ltRequestedPricePerUnit_RSM;
	private String ltRequestedTotalPrice_RSM;
	private String ltRequestedDiscount_NSM;
	private String ltRequestedPricePerUnit_NSM;
	private String ltRequestedTotalPrice_NSM;
	private String ltApprovedDiscount_RSM;
	private String ltApprovedPricePerunit_RSM;
	private String ltApprovedTotalPrice_RSM;
	private String ltApprovedDiscount_NSM;
	private String ltApprovedPricePerunit_NSM;
	private String ltApprovedTotalPrice_NSM;
	private String ltApprovedDiscount_MH;
	private String ltApprovedPricePerunit_MH;
	private String ltApprovedTotalPrice_MH;
	private String ltQuotedDiscount;
	private String ltQuotedPricePerUnit;
	private String ltQuotedTotalPrice;
	private String ltLostPricePerUnit;
	private String ltLostTotalPrice;
	private String ltWonPricePerUnit;
	private String ltWonTotalPrice;
	
	private String ltPaintShadeValue;
	private String ltReplacementMotor;
	private String ltHazardAreaTypeId;
	private String ltTempRise;
	private String ltNoiseLevel;
	private String ltMotorGA;
	private String ltPerfCurves;
	private String ltTBoxGA;
	private String ltHazardZoneId;
	private String ltDustGroupId;
	private String ltTempClass;
	
	private String flg_PriceFields_MTO;
	private String flg_Addon_Leads_MTO;
	private String flg_Addon_TBoxSize_MTO;
	private String flg_Addon_BearingDE_MTO;
	private String flg_Addon_CableEntry_MTO;
	private String flg_Txt_MotorNo_MTO;
	private String flg_Txt_RV_MTO;
	private String flg_Txt_RA_MTO;
	private String flg_Txt_LoadGD2_MTO;
	private String flg_Txt_NonSTDPaint_MTO;
	
	private String isReferDesign;
	private String isDesignComplete;
	
	private String ltCableSizeId;
	private String ltNoOfRuns;
	private String ltNoOfCores;
	private String ltCrossSectionAreaId;
	private String ltConductorMaterialId;
	private String ltCableDiameter;
	private String ltAltitude;
	private String ltAmbTempRemoveId;
	
	// Newly Added for Design.
	private String ltEnclosure;
	private String ltGreaseType;
	private String ltBroughtOutTerminals;
	private String ltMainTB;
	private String ltBearingNDESize;
	private String ltBearingDESize;
	private String ltCableEntry;
	private String ltLubrication;
	private String ltMotorGD2Value;
	private String ltMotorWeight;
	private String ltVibrationProbe;
	
	// Design - Performance parameters.
	private String ltPerfRatedSpeed;
	private String ltPerfRatedCurr;
	private String ltPerfNoLoadCurr;
	private String ltPerfNominalTorque;
	private String ltPerfMaxTorque;
	private String ltPerfLockRotorCurr;
	private String ltPerfLockRotorTorque;
	private String ltPerfSSTHot;
	private String ltPerfSSTCold;
	private String ltPerfStartTimeRatedVolt;
	private String ltPerfStartTime80RatedVolt;
	
	// Load Characteristics;
	private String ltLoadEffPercent100Above;
	private String ltLoadEffPercentAt100;
	private String ltLoadEffPercentAt75;
	private String ltLoadEffPercentAt50;
	private String ltLoadPFPercentAt100;
	private String ltLoadPFPercentAt75;
	private String ltLoadPFPercentAt50;
	
	private String ltIAIN;
	private String ltTATN;
	private String ltTKTN;
	
	private String mtoHighlightFlag;
	
	// MC - NSP
	private String ratingMaterialCost;
	private String ratingLOH;
	private String ratingTotalCost;
	private String ratingProfitMargin;
	private String ratingMC_ByDesign;
	private String ratingNSPPercent;
	private String ratingUnitPrice_MCNSP;
	
	private NewTechnicalOfferDto newTechnicalOfferDto = null;
	
	private RemarksDto remarksDto = null;
	
	private ArrayList<KeyValueVo> alRemarksList = new ArrayList<>();
	
	private ArrayList addOnList = new ArrayList();
	
	private ArrayList<KeyValueVo> newMTOTechDetailsList = new ArrayList<KeyValueVo>();
	private ArrayList<NewMTOAddOnDto> newMTOAddOnsList = new ArrayList<NewMTOAddOnDto>();
	
	
	public int getRatingId() {
		return ratingId;
	}
	public void setRatingId(int ratingId) {
		this.ratingId = ratingId;
	}
	public int getEnquiryId() {
		return enquiryId;
	}
	public void setEnquiryId(int enquiryId) {
		this.enquiryId = enquiryId;
	}
	public int getRatingNo() {
		return ratingNo;
	}
	public void setRatingNo(int ratingNo) {
		this.ratingNo = ratingNo;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getRatingTitle() {
		return CommonUtility.replaceNull(ratingTitle);
	}
	public void setRatingTitle(String ratingTitle) {
		this.ratingTitle = ratingTitle;
	}
	public String getRatingComments() {
		return CommonUtility.replaceNull(ratingComments);
	}
	public void setRatingComments(String ratingComments) {
		this.ratingComments = ratingComments;
	}
	public String getIsValidated() {
		return CommonUtility.replaceNull(isValidated);
	}
	public void setIsValidated(String isValidated) {
		this.isValidated = isValidated;
	}
	public String getTagNumber() {
		return CommonUtility.replaceNull(tagNumber);
	}
	public void setTagNumber(String tagNumber) {
		this.tagNumber = tagNumber;
	}
	public String getModelNumber() {
		return CommonUtility.replaceNull(modelNumber);
	}
	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}
	public String getRatingCreatedBy() {
		return CommonUtility.replaceNull(ratingCreatedBy);
	}
	public void setRatingCreatedBy(String ratingCreatedBy) {
		this.ratingCreatedBy = ratingCreatedBy;
	}
	public String getRatingCreatedBySSO() {
		return CommonUtility.replaceNull(ratingCreatedBySSO);
	}
	public void setRatingCreatedBySSO(String ratingCreatedBySSO) {
		this.ratingCreatedBySSO = ratingCreatedBySSO;
	}
	public boolean isFinalPriceCheck() {
		return isFinalPriceCheck;
	}
	public void setFinalPriceCheck(boolean isFinalPriceCheck) {
		this.isFinalPriceCheck = isFinalPriceCheck;
	}
	public int getHtltId() {
		return htltId;
	}
	public void setHtltId(int htltId) {
		this.htltId = htltId;
	}
	public String getLtProdGroupId() {
		return CommonUtility.replaceNull(ltProdGroupId);
	}
	public void setLtProdGroupId(String ltProdGroupId) {
		this.ltProdGroupId = ltProdGroupId;
	}
	public String getLtProdLineId() {
		return CommonUtility.replaceNull(ltProdLineId);
	}
	public void setLtProdLineId(String ltProdLineId) {
		this.ltProdLineId = ltProdLineId;
	}
	public String getLtMotorTypeId() {
		return CommonUtility.replaceNull(ltMotorTypeId);
	}
	public void setLtMotorTypeId(String ltMotorTypeId) {
		this.ltMotorTypeId = ltMotorTypeId;
	}
	public String getLtKWId() {
		return CommonUtility.replaceNull(ltKWId);
	}
	public void setLtKWId(String ltKWId) {
		this.ltKWId = ltKWId;
	}
	public String getLtPoleId() {
		return CommonUtility.replaceNull(ltPoleId);
	}
	public void setLtPoleId(String ltPoleId) {
		this.ltPoleId = ltPoleId;
	}
	public String getLtFrameId() {
		return CommonUtility.replaceNull(ltFrameId);
	}
	public void setLtFrameId(String ltFrameId) {
		this.ltFrameId = ltFrameId;
	}
	public String getLtFrameSuffixId() {
		return CommonUtility.replaceNull(ltFrameSuffixId);
	}
	public void setLtFrameSuffixId(String ltFrameSuffixId) {
		this.ltFrameSuffixId = ltFrameSuffixId;
	}
	public String getLtMountingId() {
		return CommonUtility.replaceNull(ltMountingId);
	}
	public void setLtMountingId(String ltMountingId) {
		this.ltMountingId = ltMountingId;
	}
	public String getLtTBPositionId() {
		return CommonUtility.replaceNull(ltTBPositionId);
	}
	public void setLtTBPositionId(String ltTBPositionId) {
		this.ltTBPositionId = ltTBPositionId;
	}
	public String getLtAmbTempId() {
		return CommonUtility.replaceNull(ltAmbTempId);
	}
	public void setLtAmbTempId(String ltAmbTempId) {
		this.ltAmbTempId = ltAmbTempId;
	}
	
	public String getLtGasGroupId() {
		return CommonUtility.replaceNull(ltGasGroupId);
	}
	public void setLtGasGroupId(String ltGasGroupId) {
		this.ltGasGroupId = ltGasGroupId;
	}
	public String getLtApplicationId() {
		return CommonUtility.replaceNull(ltApplicationId);
	}
	public void setLtApplicationId(String ltApplicationId) {
		this.ltApplicationId = ltApplicationId;
	}
	public String getLtEffClass() {
		return CommonUtility.replaceNull(ltEffClass);
	}
	public void setLtEffClass(String ltEffClass) {
		this.ltEffClass = ltEffClass;
	}
	public String getLtVoltId() {
		return CommonUtility.replaceNull(ltVoltId);
	}
	public void setLtVoltId(String ltVoltId) {
		this.ltVoltId = ltVoltId;
	}
	public String getLtVoltAddVariation() {
		return CommonUtility.replaceNull(ltVoltAddVariation);
	}
	public void setLtVoltAddVariation(String ltVoltAddVariation) {
		this.ltVoltAddVariation = ltVoltAddVariation;
	}
	public String getLtVoltRemoveVariation() {
		return CommonUtility.replaceNull(ltVoltRemoveVariation);
	}
	public void setLtVoltRemoveVariation(String ltVoltRemoveVariation) {
		this.ltVoltRemoveVariation = ltVoltRemoveVariation;
	}
	public String getLtFreqId() {
		return CommonUtility.replaceNull(ltFreqId);
	}
	public void setLtFreqId(String ltFreqId) {
		this.ltFreqId = ltFreqId;
	}
	public String getLtFreqAddVariation() {
		return CommonUtility.replaceNull(ltFreqAddVariation);
	}
	public void setLtFreqAddVariation(String ltFreqAddVariation) {
		this.ltFreqAddVariation = ltFreqAddVariation;
	}
	public String getLtFreqRemoveVariation() {
		return CommonUtility.replaceNull(ltFreqRemoveVariation);
	}
	public void setLtFreqRemoveVariation(String ltFreqRemoveVariation) {
		this.ltFreqRemoveVariation = ltFreqRemoveVariation;
	}
	public String getLtCombVariation() {
		return CommonUtility.replaceNull(ltCombVariation);
	}
	public void setLtCombVariation(String ltCombVariation) {
		this.ltCombVariation = ltCombVariation;
	}
	public String getLtDutyId() {
		return CommonUtility.replaceNull(ltDutyId);
	}
	public void setLtDutyId(String ltDutyId) {
		this.ltDutyId = ltDutyId;
	}
	public String getLtCDFId() {
		return CommonUtility.replaceNull(ltCDFId);
	}
	public void setLtCDFId(String ltCDFId) {
		this.ltCDFId = ltCDFId;
	}
	public String getLtStartsId() {
		return CommonUtility.replaceNull(ltStartsId);
	}
	public void setLtStartsId(String ltStartsId) {
		this.ltStartsId = ltStartsId;
	}
	public String getLtTotalAddonPercent() {
		return CommonUtility.replaceNull(ltTotalAddonPercent);
	}
	public void setLtTotalAddonPercent(String ltTotalAddonPercent) {
		this.ltTotalAddonPercent = ltTotalAddonPercent;
	}
	public String getLtTotalCashExtra() {
		return CommonUtility.replaceNull(ltTotalCashExtra);
	}
	public void setLtTotalCashExtra(String ltTotalCashExtra) {
		this.ltTotalCashExtra = ltTotalCashExtra;
	}
	public String getLtLPPerUnit() {
		return CommonUtility.replaceNull(ltLPPerUnit);
	}
	public void setLtLPPerUnit(String ltLPPerUnit) {
		this.ltLPPerUnit = ltLPPerUnit;
	}
	public String getLtLPWithAddonPerUnit() {
		return CommonUtility.replaceNull(ltLPWithAddonPerUnit);
	}
	public void setLtLPWithAddonPerUnit(String ltLPWithAddonPerUnit) {
		this.ltLPWithAddonPerUnit = ltLPWithAddonPerUnit;
	}
	public String getLtStandardDiscount() {
		return CommonUtility.replaceNull(ltStandardDiscount);
	}
	public void setLtStandardDiscount(String ltStandardDiscount) {
		this.ltStandardDiscount = ltStandardDiscount;
	}
	public String getLtPricePerUnit() {
		return CommonUtility.replaceNull(ltPricePerUnit);
	}
	public void setLtPricePerUnit(String ltPricePerUnit) {
		this.ltPricePerUnit = ltPricePerUnit;
	}
	public String getLtTotalOrderPrice() {
		return CommonUtility.replaceNull(ltTotalOrderPrice);
	}
	public void setLtTotalOrderPrice(String ltTotalOrderPrice) {
		this.ltTotalOrderPrice = ltTotalOrderPrice;
	}
	public String getLtAdditionalComments() {
		return CommonUtility.replaceNull(ltAdditionalComments);
	}
	public void setLtAdditionalComments(String ltAdditionalComments) {
		this.ltAdditionalComments = ltAdditionalComments;
	}
	public String getLtEarlierMotorSerialNo() {
		return CommonUtility.replaceNull(ltEarlierMotorSerialNo);
	}
	public void setLtEarlierMotorSerialNo(String ltEarlierMotorSerialNo) {
		this.ltEarlierMotorSerialNo = ltEarlierMotorSerialNo;
	}
	public String getLtStandardDeliveryWks() {
		return CommonUtility.replaceNull(ltStandardDeliveryWks);
	}
	public void setLtStandardDeliveryWks(String ltStandardDeliveryWks) {
		this.ltStandardDeliveryWks = ltStandardDeliveryWks;
	}
	public String getLtCustReqDeliveryWks() {
		return CommonUtility.replaceNull(ltCustReqDeliveryWks);
	}
	public void setLtCustReqDeliveryWks(String ltCustReqDeliveryWks) {
		this.ltCustReqDeliveryWks = ltCustReqDeliveryWks;
	}
	public String getLtRVId() {
		return CommonUtility.replaceNull(ltRVId);
	}
	public void setLtRVId(String ltRVId) {
		this.ltRVId = ltRVId;
	}
	public String getLtRAId() {
		return CommonUtility.replaceNull(ltRAId);
	}
	public void setLtRAId(String ltRAId) {
		this.ltRAId = ltRAId;
	}
	public String getLtWindingTreatmentId() {
		return CommonUtility.replaceNull(ltWindingTreatmentId);
	}
	public void setLtWindingTreatmentId(String ltWindingTreatmentId) {
		this.ltWindingTreatmentId = ltWindingTreatmentId;
	}
	public String getLtWindingWire() {
		return CommonUtility.replaceNull(ltWindingWire);
	}
	public void setLtWindingWire(String ltWindingWire) {
		this.ltWindingWire = ltWindingWire;
	}
	public String getLtLeadId() {
		return CommonUtility.replaceNull(ltLeadId);
	}
	public void setLtLeadId(String ltLeadId) {
		this.ltLeadId = ltLeadId;
	}
	public String getLtStartingCurrentOnDOLStart() {
		return CommonUtility.replaceNull(ltStartingCurrentOnDOLStart);
	}
	public void setLtStartingCurrentOnDOLStart(String ltStartingCurrentOnDOLStart) {
		this.ltStartingCurrentOnDOLStart = ltStartingCurrentOnDOLStart;
	}
	public String getLtWindingConfig() {
		return CommonUtility.replaceNull(ltWindingConfig);
	}
	public void setLtWindingConfig(String ltWindingConfig) {
		this.ltWindingConfig = ltWindingConfig;
	}
	public String getLtLoadGD2Value() {
		return CommonUtility.replaceNull(ltLoadGD2Value);
	}
	public void setLtLoadGD2Value(String ltLoadGD2Value) {
		this.ltLoadGD2Value = ltLoadGD2Value;
	}
	public String getLtMethodOfStarting() {
		return CommonUtility.replaceNull(ltMethodOfStarting);
	}
	public void setLtMethodOfStarting(String ltMethodOfStarting) {
		this.ltMethodOfStarting = ltMethodOfStarting;
	}
	public String getLtVFDApplTypeId() {
		return CommonUtility.replaceNull(ltVFDApplTypeId);
	}
	public void setLtVFDApplTypeId(String ltVFDApplTypeId) {
		this.ltVFDApplTypeId = ltVFDApplTypeId;
	}
	public String getLtVFDMotorSpeedRangeMin() {
		return CommonUtility.replaceNull(ltVFDMotorSpeedRangeMin);
	}
	public void setLtVFDMotorSpeedRangeMin(String ltVFDMotorSpeedRangeMin) {
		this.ltVFDMotorSpeedRangeMin = ltVFDMotorSpeedRangeMin;
	}
	public String getLtVFDMotorSpeedRangeMax() {
		return CommonUtility.replaceNull(ltVFDMotorSpeedRangeMax);
	}
	public void setLtVFDMotorSpeedRangeMax(String ltVFDMotorSpeedRangeMax) {
		this.ltVFDMotorSpeedRangeMax = ltVFDMotorSpeedRangeMax;
	}
	public String getLtDualSpeedId() {
		return CommonUtility.replaceNull(ltDualSpeedId);
	}
	public void setLtDualSpeedId(String ltDualSpeedId) {
		this.ltDualSpeedId = ltDualSpeedId;
	}
	public String getLtOverloadingDuty() {
		return CommonUtility.replaceNull(ltOverloadingDuty);
	}
	public void setLtOverloadingDuty(String ltOverloadingDuty) {
		this.ltOverloadingDuty = ltOverloadingDuty;
	}
	public String getLtConstantEfficiencyRange() {
		return CommonUtility.replaceNull(ltConstantEfficiencyRange);
	}
	public void setLtConstantEfficiencyRange(String ltConstantEfficiencyRange) {
		this.ltConstantEfficiencyRange = ltConstantEfficiencyRange;
	}
	public String getLtShaftMaterialId() {
		return CommonUtility.replaceNull(ltShaftMaterialId);
	}
	public void setLtShaftMaterialId(String ltShaftMaterialId) {
		this.ltShaftMaterialId = ltShaftMaterialId;
	}
	public String getLtDirectionOfRotation() {
		return CommonUtility.replaceNull(ltDirectionOfRotation);
	}
	public void setLtDirectionOfRotation(String ltDirectionOfRotation) {
		this.ltDirectionOfRotation = ltDirectionOfRotation;
	}
	public String getLtStandardRotation() {
		return CommonUtility.replaceNull(ltStandardRotation);
	}
	public void setLtStandardRotation(String ltStandardRotation) {
		this.ltStandardRotation = ltStandardRotation;
	}
	public String getLtMethodOfCoupling() {
		return CommonUtility.replaceNull(ltMethodOfCoupling);
	}
	public void setLtMethodOfCoupling(String ltMethodOfCoupling) {
		this.ltMethodOfCoupling = ltMethodOfCoupling;
	}
	public String getLtPaintingTypeId() {
		return CommonUtility.replaceNull(ltPaintingTypeId);
	}
	public void setLtPaintingTypeId(String ltPaintingTypeId) {
		this.ltPaintingTypeId = ltPaintingTypeId;
	}
	public String getLtPaintShadeId() {
		return CommonUtility.replaceNull(ltPaintShadeId);
	}
	public void setLtPaintShadeId(String ltPaintShadeId) {
		this.ltPaintShadeId = ltPaintShadeId;
	}
	public String getLtPaintThicknessId() {
		return CommonUtility.replaceNull(ltPaintThicknessId);
	}
	public void setLtPaintThicknessId(String ltPaintThicknessId) {
		this.ltPaintThicknessId = ltPaintThicknessId;
	}
	public String getLtIPId() {
		return CommonUtility.replaceNull(ltIPId);
	}
	public void setLtIPId(String ltIPId) {
		this.ltIPId = ltIPId;
	}
	public String getLtInsulationClassId() {
		return CommonUtility.replaceNull(ltInsulationClassId);
	}
	public void setLtInsulationClassId(String ltInsulationClassId) {
		this.ltInsulationClassId = ltInsulationClassId;
	}
	public String getLtTerminalBoxSizeId() {
		return CommonUtility.replaceNull(ltTerminalBoxSizeId);
	}
	public void setLtTerminalBoxSizeId(String ltTerminalBoxSizeId) {
		this.ltTerminalBoxSizeId = ltTerminalBoxSizeId;
	}
	public String getLtSpreaderBoxId() {
		return CommonUtility.replaceNull(ltSpreaderBoxId);
	}
	public void setLtSpreaderBoxId(String ltSpreaderBoxId) {
		this.ltSpreaderBoxId = ltSpreaderBoxId;
	}
	public String getLtAuxTerminalBoxId() {
		return CommonUtility.replaceNull(ltAuxTerminalBoxId);
	}
	public void setLtAuxTerminalBoxId(String ltAuxTerminalBoxId) {
		this.ltAuxTerminalBoxId = ltAuxTerminalBoxId;
	}
	public String getLtSpaceHeaterId() {
		return CommonUtility.replaceNull(ltSpaceHeaterId);
	}
	public void setLtSpaceHeaterId(String ltSpaceHeaterId) {
		this.ltSpaceHeaterId = ltSpaceHeaterId;
	}
	public String getLtVibrationId() {
		return CommonUtility.replaceNull(ltVibrationId);
	}
	public void setLtVibrationId(String ltVibrationId) {
		this.ltVibrationId = ltVibrationId;
	}
	public String getLtFlyingLeadWithoutTDId() {
		return CommonUtility.replaceNull(ltFlyingLeadWithoutTDId);
	}
	public void setLtFlyingLeadWithoutTDId(String ltFlyingLeadWithoutTDId) {
		this.ltFlyingLeadWithoutTDId = ltFlyingLeadWithoutTDId;
	}
	public String getLtFlyingLeadWithTDId() {
		return CommonUtility.replaceNull(ltFlyingLeadWithTDId);
	}
	public void setLtFlyingLeadWithTDId(String ltFlyingLeadWithTDId) {
		this.ltFlyingLeadWithTDId = ltFlyingLeadWithTDId;
	}
	public String getLtMetalFanId() {
		return CommonUtility.replaceNull(ltMetalFanId);
	}
	public void setLtMetalFanId(String ltMetalFanId) {
		this.ltMetalFanId = ltMetalFanId;
	}
	public String getLtTechoMounting() {
		return CommonUtility.replaceNull(ltTechoMounting);
	}
	public void setLtTechoMounting(String ltTechoMounting) {
		this.ltTechoMounting = ltTechoMounting;
	}
	public String getLtShaftGroundingId() {
		return CommonUtility.replaceNull(ltShaftGroundingId);
	}
	public void setLtShaftGroundingId(String ltShaftGroundingId) {
		this.ltShaftGroundingId = ltShaftGroundingId;
	}
	public String getLtForcedCoolingId() {
		return CommonUtility.replaceNull(ltForcedCoolingId);
	}
	public void setLtForcedCoolingId(String ltForcedCoolingId) {
		this.ltForcedCoolingId = ltForcedCoolingId;
	}
	public String getLtHardware() {
		return CommonUtility.replaceNull(ltHardware);
	}
	public void setLtHardware(String ltHardware) {
		this.ltHardware = ltHardware;
	}
	public String getLtGlandPlateId() {
		return CommonUtility.replaceNull(ltGlandPlateId);
	}
	public void setLtGlandPlateId(String ltGlandPlateId) {
		this.ltGlandPlateId = ltGlandPlateId;
	}
	public String getLtDoubleCompressionGlandId() {
		return CommonUtility.replaceNull(ltDoubleCompressionGlandId);
	}
	public void setLtDoubleCompressionGlandId(String ltDoubleCompressionGlandId) {
		this.ltDoubleCompressionGlandId = ltDoubleCompressionGlandId;
	}
	public String getLtSPMMountingProvisionId() {
		return CommonUtility.replaceNull(ltSPMMountingProvisionId);
	}
	public void setLtSPMMountingProvisionId(String ltSPMMountingProvisionId) {
		this.ltSPMMountingProvisionId = ltSPMMountingProvisionId;
	}
	public String getLtAddNamePlateId() {
		return CommonUtility.replaceNull(ltAddNamePlateId);
	}
	public void setLtAddNamePlateId(String ltAddNamePlateId) {
		this.ltAddNamePlateId = ltAddNamePlateId;
	}
	public String getLtDirectionArrowPlateId() {
		return CommonUtility.replaceNull(ltDirectionArrowPlateId);
	}
	public void setLtDirectionArrowPlateId(String ltDirectionArrowPlateId) {
		this.ltDirectionArrowPlateId = ltDirectionArrowPlateId;
	}
	public String getLtRTDId() {
		return CommonUtility.replaceNull(ltRTDId);
	}
	public void setLtRTDId(String ltRTDId) {
		this.ltRTDId = ltRTDId;
	}
	public String getLtBTDId() {
		return CommonUtility.replaceNull(ltBTDId);
	}
	public void setLtBTDId(String ltBTDId) {
		this.ltBTDId = ltBTDId;
	}
	public String getLtThermisterId() {
		return CommonUtility.replaceNull(ltThermisterId);
	}
	public void setLtThermisterId(String ltThermisterId) {
		this.ltThermisterId = ltThermisterId;
	}
	public String getLtCableSealingBoxId() {
		return CommonUtility.replaceNull(ltCableSealingBoxId);
	}
	public void setLtCableSealingBoxId(String ltCableSealingBoxId) {
		this.ltCableSealingBoxId = ltCableSealingBoxId;
	}
	public String getLtBearingSystemId() {
		return CommonUtility.replaceNull(ltBearingSystemId);
	}
	public void setLtBearingSystemId(String ltBearingSystemId) {
		this.ltBearingSystemId = ltBearingSystemId;
	}
	public String getLtBearingNDEId() {
		return CommonUtility.replaceNull(ltBearingNDEId);
	}
	public void setLtBearingNDEId(String ltBearingNDEId) {
		this.ltBearingNDEId = ltBearingNDEId;
	}
	public String getLtBearingDEId() {
		return CommonUtility.replaceNull(ltBearingDEId);
	}
	public void setLtBearingDEId(String ltBearingDEId) {
		this.ltBearingDEId = ltBearingDEId;
	}
	public String getLtWitnessRoutineId() {
		return CommonUtility.replaceNull(ltWitnessRoutineId);
	}
	public void setLtWitnessRoutineId(String ltWitnessRoutineId) {
		this.ltWitnessRoutineId = ltWitnessRoutineId;
	}
	public String getLtAdditionalTest1Id() {
		return CommonUtility.replaceNull(ltAdditionalTest1Id);
	}
	public void setLtAdditionalTest1Id(String ltAdditionalTest1Id) {
		this.ltAdditionalTest1Id = ltAdditionalTest1Id;
	}
	public String getLtAdditionalTest2Id() {
		return CommonUtility.replaceNull(ltAdditionalTest2Id);
	}
	public void setLtAdditionalTest2Id(String ltAdditionalTest2Id) {
		this.ltAdditionalTest2Id = ltAdditionalTest2Id;
	}
	public String getLtAdditionalTest3Id() {
		return CommonUtility.replaceNull(ltAdditionalTest3Id);
	}
	public void setLtAdditionalTest3Id(String ltAdditionalTest3Id) {
		this.ltAdditionalTest3Id = ltAdditionalTest3Id;
	}
	public String getLtTypeTestId() {
		return CommonUtility.replaceNull(ltTypeTestId);
	}
	public void setLtTypeTestId(String ltTypeTestId) {
		this.ltTypeTestId = ltTypeTestId;
	}
	public String getLtULCEId() {
		return CommonUtility.replaceNull(ltULCEId);
	}
	public void setLtULCEId(String ltULCEId) {
		this.ltULCEId = ltULCEId;
	}
	public String getLtQAPId() {
		return CommonUtility.replaceNull(ltQAPId);
	}
	public void setLtQAPId(String ltQAPId) {
		this.ltQAPId = ltQAPId;
	}
	public String getLtSeaworthyPackingId() {
		return CommonUtility.replaceNull(ltSeaworthyPackingId);
	}
	public void setLtSeaworthyPackingId(String ltSeaworthyPackingId) {
		this.ltSeaworthyPackingId = ltSeaworthyPackingId;
	}
	public String getLtWarrantyId() {
		return CommonUtility.replaceNull(ltWarrantyId);
	}
	public void setLtWarrantyId(String ltWarrantyId) {
		this.ltWarrantyId = ltWarrantyId;
	}
	public String getLtManufacturingLocation() {
		return CommonUtility.replaceNull(ltManufacturingLocation);
	}
	public void setLtManufacturingLocation(String ltManufacturingLocation) {
		this.ltManufacturingLocation = ltManufacturingLocation;
	}
	public String getLtSpares1() {
		return CommonUtility.replaceNull(ltSpares1);
	}
	public void setLtSpares1(String ltSpares1) {
		this.ltSpares1 = ltSpares1;
	}
	public String getLtSpares2() {
		return CommonUtility.replaceNull(ltSpares2);
	}
	public void setLtSpares2(String ltSpares2) {
		this.ltSpares2 = ltSpares2;
	}
	public String getLtSpares3() {
		return CommonUtility.replaceNull(ltSpares3);
	}
	public void setLtSpares3(String ltSpares3) {
		this.ltSpares3 = ltSpares3;
	}
	public String getLtSpares4() {
		return CommonUtility.replaceNull(ltSpares4);
	}
	public void setLtSpares4(String ltSpares4) {
		this.ltSpares4 = ltSpares4;
	}
	public String getLtSpares5() {
		return CommonUtility.replaceNull(ltSpares5);
	}
	public void setLtSpares5(String ltSpares5) {
		this.ltSpares5 = ltSpares5;
	}
	public String getLtDataSheet() {
		return CommonUtility.replaceNull(ltDataSheet);
	}
	public void setLtDataSheet(String ltDataSheet) {
		this.ltDataSheet = ltDataSheet;
	}
	public String getLtAddDrawingId() {
		return CommonUtility.replaceNull(ltAddDrawingId);
	}
	public void setLtAddDrawingId(String ltAddDrawingId) {
		this.ltAddDrawingId = ltAddDrawingId;
	}
	public String getLtNonStdVoltageId() {
		return CommonUtility.replaceNull(ltNonStdVoltageId);
	}
	public void setLtNonStdVoltageId(String ltNonStdVoltageId) {
		this.ltNonStdVoltageId = ltNonStdVoltageId;
	}
	public String getLtShaftTypeId() {
		return CommonUtility.replaceNull(ltShaftTypeId);
	}
	public void setLtShaftTypeId(String ltShaftTypeId) {
		this.ltShaftTypeId = ltShaftTypeId;
	}
	public String getLtHazardAreaId() {
		return CommonUtility.replaceNull(ltHazardAreaId);
	}
	public void setLtHazardAreaId(String ltHazardAreaId) {
		this.ltHazardAreaId = ltHazardAreaId;
	}
	public String getLtCustomerId() {
		return CommonUtility.replaceNull(ltCustomerId);
	}
	public void setLtCustomerId(String ltCustomerId) {
		this.ltCustomerId = ltCustomerId;
	}
	
	public NewTechnicalOfferDto getNewTechnicalOfferDto() {
		return newTechnicalOfferDto;
	}
	public void setNewTechnicalOfferDto(NewTechnicalOfferDto newTechnicalOfferDto) {
		this.newTechnicalOfferDto = newTechnicalOfferDto;
	}
	public RemarksDto getRemarksDto() {
		return remarksDto;
	}
	public void setRemarksDto(RemarksDto remarksDto) {
		this.remarksDto = remarksDto;
	}
	public ArrayList<KeyValueVo> getAlRemarksList() {
		return alRemarksList;
	}
	public void setAlRemarksList(ArrayList<KeyValueVo> alRemarksList) {
		this.alRemarksList = alRemarksList;
	}
	public String getLtServiceFactor() {
		return CommonUtility.replaceNull(ltServiceFactor);
	}
	public void setLtServiceFactor(String ltServiceFactor) {
		this.ltServiceFactor = ltServiceFactor;
	}
	
	public String getLtQuotedDiscount() {
		return CommonUtility.replaceNull(ltQuotedDiscount);
	}
	public void setLtQuotedDiscount(String ltQuotedDiscount) {
		this.ltQuotedDiscount = ltQuotedDiscount;
	}
	public String getLtQuotedPricePerUnit() {
		return CommonUtility.replaceNull(ltQuotedPricePerUnit);
	}
	public void setLtQuotedPricePerUnit(String ltQuotedPricePerUnit) {
		this.ltQuotedPricePerUnit = ltQuotedPricePerUnit;
	}
	public String getLtQuotedTotalPrice() {
		return CommonUtility.replaceNull(ltQuotedTotalPrice);
	}
	public void setLtQuotedTotalPrice(String ltQuotedTotalPrice) {
		this.ltQuotedTotalPrice = ltQuotedTotalPrice;
	}
	public String getLtLostPricePerUnit() {
		return CommonUtility.replaceNull(ltLostPricePerUnit);
	}
	public void setLtLostPricePerUnit(String ltLostPricePerUnit) {
		this.ltLostPricePerUnit = ltLostPricePerUnit;
	}
	public String getLtLostTotalPrice() {
		return CommonUtility.replaceNull(ltLostTotalPrice);
	}
	public void setLtLostTotalPrice(String ltLostTotalPrice) {
		this.ltLostTotalPrice = ltLostTotalPrice;
	}
	public String getLtWonPricePerUnit() {
		return CommonUtility.replaceNull(ltWonPricePerUnit);
	}
	public void setLtWonPricePerUnit(String ltWonPricePerUnit) {
		this.ltWonPricePerUnit = ltWonPricePerUnit;
	}
	public String getLtWonTotalPrice() {
		return CommonUtility.replaceNull(ltWonTotalPrice);
	}
	public void setLtWonTotalPrice(String ltWonTotalPrice) {
		this.ltWonTotalPrice = ltWonTotalPrice;
	}
	public String getLtRequestedDiscount_SM() {
		return CommonUtility.replaceNull(ltRequestedDiscount_SM);
	}
	public void setLtRequestedDiscount_SM(String ltRequestedDiscount_SM) {
		this.ltRequestedDiscount_SM = ltRequestedDiscount_SM;
	}
	public String getLtRequestedPricePerUnit_SM() {
		return CommonUtility.replaceNull(ltRequestedPricePerUnit_SM);
	}
	public void setLtRequestedPricePerUnit_SM(String ltRequestedPricePerUnit_SM) {
		this.ltRequestedPricePerUnit_SM = ltRequestedPricePerUnit_SM;
	}
	public String getLtRequestedTotalPrice_SM() {
		return CommonUtility.replaceNull(ltRequestedTotalPrice_SM);
	}
	public void setLtRequestedTotalPrice_SM(String ltRequestedTotalPrice_SM) {
		this.ltRequestedTotalPrice_SM = ltRequestedTotalPrice_SM;
	}
	public String getLtRequestedDiscount_RSM() {
		return CommonUtility.replaceNull(ltRequestedDiscount_RSM);
	}
	public void setLtRequestedDiscount_RSM(String ltRequestedDiscount_RSM) {
		this.ltRequestedDiscount_RSM = ltRequestedDiscount_RSM;
	}
	public String getLtRequestedPricePerUnit_RSM() {
		return CommonUtility.replaceNull(ltRequestedPricePerUnit_RSM);
	}
	public void setLtRequestedPricePerUnit_RSM(String ltRequestedPricePerUnit_RSM) {
		this.ltRequestedPricePerUnit_RSM = ltRequestedPricePerUnit_RSM;
	}
	public String getLtRequestedTotalPrice_RSM() {
		return CommonUtility.replaceNull(ltRequestedTotalPrice_RSM);
	}
	public void setLtRequestedTotalPrice_RSM(String ltRequestedTotalPrice_RSM) {
		this.ltRequestedTotalPrice_RSM = ltRequestedTotalPrice_RSM;
	}
	public String getLtRequestedDiscount_NSM() {
		return CommonUtility.replaceNull(ltRequestedDiscount_NSM);
	}
	public void setLtRequestedDiscount_NSM(String ltRequestedDiscount_NSM) {
		this.ltRequestedDiscount_NSM = ltRequestedDiscount_NSM;
	}
	public String getLtRequestedPricePerUnit_NSM() {
		return CommonUtility.replaceNull(ltRequestedPricePerUnit_NSM);
	}
	public void setLtRequestedPricePerUnit_NSM(String ltRequestedPricePerUnit_NSM) {
		this.ltRequestedPricePerUnit_NSM = ltRequestedPricePerUnit_NSM;
	}
	public String getLtRequestedTotalPrice_NSM() {
		return CommonUtility.replaceNull(ltRequestedTotalPrice_NSM);
	}
	public void setLtRequestedTotalPrice_NSM(String ltRequestedTotalPrice_NSM) {
		this.ltRequestedTotalPrice_NSM = ltRequestedTotalPrice_NSM;
	}
	public String getLtApprovedDiscount_RSM() {
		return CommonUtility.replaceNull(ltApprovedDiscount_RSM);
	}
	public void setLtApprovedDiscount_RSM(String ltApprovedDiscount_RSM) {
		this.ltApprovedDiscount_RSM = ltApprovedDiscount_RSM;
	}
	public String getLtApprovedPricePerunit_RSM() {
		return CommonUtility.replaceNull(ltApprovedPricePerunit_RSM);
	}
	public void setLtApprovedPricePerunit_RSM(String ltApprovedPricePerunit_RSM) {
		this.ltApprovedPricePerunit_RSM = ltApprovedPricePerunit_RSM;
	}
	public String getLtApprovedTotalPrice_RSM() {
		return CommonUtility.replaceNull(ltApprovedTotalPrice_RSM);
	}
	public void setLtApprovedTotalPrice_RSM(String ltApprovedTotalPrice_RSM) {
		this.ltApprovedTotalPrice_RSM = ltApprovedTotalPrice_RSM;
	}
	public String getLtApprovedDiscount_NSM() {
		return CommonUtility.replaceNull(ltApprovedDiscount_NSM);
	}
	public void setLtApprovedDiscount_NSM(String ltApprovedDiscount_NSM) {
		this.ltApprovedDiscount_NSM = ltApprovedDiscount_NSM;
	}
	public String getLtApprovedPricePerunit_NSM() {
		return CommonUtility.replaceNull(ltApprovedPricePerunit_NSM);
	}
	public void setLtApprovedPricePerunit_NSM(String ltApprovedPricePerunit_NSM) {
		this.ltApprovedPricePerunit_NSM = ltApprovedPricePerunit_NSM;
	}
	public String getLtApprovedTotalPrice_NSM() {
		return CommonUtility.replaceNull(ltApprovedTotalPrice_NSM);
	}
	public void setLtApprovedTotalPrice_NSM(String ltApprovedTotalPrice_NSM) {
		this.ltApprovedTotalPrice_NSM = ltApprovedTotalPrice_NSM;
	}
	public String getLtApprovedDiscount_MH() {
		return CommonUtility.replaceNull(ltApprovedDiscount_MH);
	}
	public void setLtApprovedDiscount_MH(String ltApprovedDiscount_MH) {
		this.ltApprovedDiscount_MH = ltApprovedDiscount_MH;
	}
	public String getLtApprovedPricePerunit_MH() {
		return CommonUtility.replaceNull(ltApprovedPricePerunit_MH);
	}
	public void setLtApprovedPricePerunit_MH(String ltApprovedPricePerunit_MH) {
		this.ltApprovedPricePerunit_MH = ltApprovedPricePerunit_MH;
	}
	public String getLtApprovedTotalPrice_MH() {
		return CommonUtility.replaceNull(ltApprovedTotalPrice_MH);
	}
	public void setLtApprovedTotalPrice_MH(String ltApprovedTotalPrice_MH) {
		this.ltApprovedTotalPrice_MH = ltApprovedTotalPrice_MH;
	}
	public String getLtPaintShadeValue() {
		return CommonUtility.replaceNull(ltPaintShadeValue);
	}
	public void setLtPaintShadeValue(String ltPaintShadeValue) {
		this.ltPaintShadeValue = ltPaintShadeValue;
	}
	public String getLtReplacementMotor() {
		return CommonUtility.replaceNull(ltReplacementMotor);
	}
	public void setLtReplacementMotor(String ltReplacementMotor) {
		this.ltReplacementMotor = ltReplacementMotor;
	}
	public String getLtHazardAreaTypeId() {
		return CommonUtility.replaceNull(ltHazardAreaTypeId);
	}
	public void setLtHazardAreaTypeId(String ltHazardAreaTypeId) {
		this.ltHazardAreaTypeId = ltHazardAreaTypeId;
	}
	public String getLtTempRise() {
		return CommonUtility.replaceNull(ltTempRise);
	}
	public void setLtTempRise(String ltTempRise) {
		this.ltTempRise = ltTempRise;
	}
	public String getLtNoiseLevel() {
		return CommonUtility.replaceNull(ltNoiseLevel);
	}
	public void setLtNoiseLevel(String ltNoiseLevel) {
		this.ltNoiseLevel = ltNoiseLevel;
	}
	public String getLtMotorGA() {
		return CommonUtility.replaceNull(ltMotorGA);
	}
	public void setLtMotorGA(String ltMotorGA) {
		this.ltMotorGA = ltMotorGA;
	}
	public String getLtPerfCurves() {
		return CommonUtility.replaceNull(ltPerfCurves);
	}
	public void setLtPerfCurves(String ltPerfCurves) {
		this.ltPerfCurves = ltPerfCurves;
	}
	public String getLtTBoxGA() {
		return CommonUtility.replaceNull(ltTBoxGA);
	}
	public void setLtTBoxGA(String ltTBoxGA) {
		this.ltTBoxGA = ltTBoxGA;
	}
	public String getLtProdGroupId_MTO() {
		return CommonUtility.replaceNull(ltProdGroupId_MTO);
	}
	public void setLtProdGroupId_MTO(String ltProdGroupId_MTO) {
		this.ltProdGroupId_MTO = ltProdGroupId_MTO;
	}
	public String getLtProdLineId_MTO() {
		return CommonUtility.replaceNull(ltProdLineId_MTO);
	}
	public void setLtProdLineId_MTO(String ltProdLineId_MTO) {
		this.ltProdLineId_MTO = ltProdLineId_MTO;
	}
	public String getLtKWId_MTO() {
		return CommonUtility.replaceNull(ltKWId_MTO);
	}
	public void setLtKWId_MTO(String ltKWId_MTO) {
		this.ltKWId_MTO = ltKWId_MTO;
	}
	public String getLtFrameId_MTO() {
		return CommonUtility.replaceNull(ltFrameId_MTO);
	}
	public void setLtFrameId_MTO(String ltFrameId_MTO) {
		this.ltFrameId_MTO = ltFrameId_MTO;
	}
	public String getLtPoleId_MTO() {
		return CommonUtility.replaceNull(ltPoleId_MTO);
	}
	public void setLtPoleId_MTO(String ltPoleId_MTO) {
		this.ltPoleId_MTO = ltPoleId_MTO;
	}
	public String getLtFrameSuffixId_MTO() {
		return CommonUtility.replaceNull(ltFrameSuffixId_MTO);
	}
	public void setLtFrameSuffixId_MTO(String ltFrameSuffixId_MTO) {
		this.ltFrameSuffixId_MTO = ltFrameSuffixId_MTO;
	}
	public String getLtMountingId_MTO() {
		return CommonUtility.replaceNull(ltMountingId_MTO);
	}
	public void setLtMountingId_MTO(String ltMountingId_MTO) {
		this.ltMountingId_MTO = ltMountingId_MTO;
	}
	public String getLtTBPositionId_MTO() {
		return CommonUtility.replaceNull(ltTBPositionId_MTO);
	}
	public void setLtTBPositionId_MTO(String ltTBPositionId_MTO) {
		this.ltTBPositionId_MTO = ltTBPositionId_MTO;
	}
	public String getLtPercentCopper_MTO() {
		return CommonUtility.replaceNull(ltPercentCopper_MTO);
	}
	public void setLtPercentCopper_MTO(String ltPercentCopper_MTO) {
		this.ltPercentCopper_MTO = ltPercentCopper_MTO;
	}
	public String getLtPercentStamping_MTO() {
		return CommonUtility.replaceNull(ltPercentStamping_MTO);
	}
	public void setLtPercentStamping_MTO(String ltPercentStamping_MTO) {
		this.ltPercentStamping_MTO = ltPercentStamping_MTO;
	}
	public String getLtPercentAluminium_MTO() {
		return CommonUtility.replaceNull(ltPercentAluminium_MTO);
	}
	public void setLtPercentAluminium_MTO(String ltPercentAluminium_MTO) {
		this.ltPercentAluminium_MTO = ltPercentAluminium_MTO;
	}
	public String getLtSpecialAddOn_MTO() {
		return CommonUtility.replaceNull(ltSpecialAddOn_MTO);
	}
	public void setLtSpecialAddOn_MTO(String ltSpecialAddOn_MTO) {
		this.ltSpecialAddOn_MTO = ltSpecialAddOn_MTO;
	}
	public String getLtSpecialCashExtra_MTO() {
		return CommonUtility.replaceNull(ltSpecialCashExtra_MTO);
	}
	public void setLtSpecialCashExtra_MTO(String ltSpecialCashExtra_MTO) {
		this.ltSpecialCashExtra_MTO = ltSpecialCashExtra_MTO;
	}
	public String getLtTotalAddOn_MTO() {
		return CommonUtility.replaceNull(ltTotalAddOn_MTO);
	}
	public void setLtTotalAddOn_MTO(String ltTotalAddOn_MTO) {
		this.ltTotalAddOn_MTO = ltTotalAddOn_MTO;
	}
	public String getLtTotalCashExtra_MTO() {
		return CommonUtility.replaceNull(ltTotalCashExtra_MTO);
	}
	public void setLtTotalCashExtra_MTO(String ltTotalCashExtra_MTO) {
		this.ltTotalCashExtra_MTO = ltTotalCashExtra_MTO;
	}
	public String getLtMaterialCost_MTO() {
		return CommonUtility.replaceNull(ltMaterialCost_MTO);
	}
	public void setLtMaterialCost_MTO(String ltMaterialCost_MTO) {
		this.ltMaterialCost_MTO = ltMaterialCost_MTO;
	}
	public String getFlg_PriceFields_MTO() {
		return CommonUtility.replaceNull(flg_PriceFields_MTO);
	}
	public void setFlg_PriceFields_MTO(String flg_PriceFields_MTO) {
		this.flg_PriceFields_MTO = flg_PriceFields_MTO;
	}
	public String getFlg_Addon_Leads_MTO() {
		return CommonUtility.replaceNull(flg_Addon_Leads_MTO);
	}
	public void setFlg_Addon_Leads_MTO(String flg_Addon_Leads_MTO) {
		this.flg_Addon_Leads_MTO = flg_Addon_Leads_MTO;
	}
	public String getFlg_Addon_TBoxSize_MTO() {
		return CommonUtility.replaceNull(flg_Addon_TBoxSize_MTO);
	}
	public void setFlg_Addon_TBoxSize_MTO(String flg_Addon_TBoxSize_MTO) {
		this.flg_Addon_TBoxSize_MTO = flg_Addon_TBoxSize_MTO;
	}
	public String getFlg_Addon_BearingDE_MTO() {
		return CommonUtility.replaceNull(flg_Addon_BearingDE_MTO);
	}
	public void setFlg_Addon_BearingDE_MTO(String flg_Addon_BearingDE_MTO) {
		this.flg_Addon_BearingDE_MTO = flg_Addon_BearingDE_MTO;
	}
	public String getFlg_Addon_CableEntry_MTO() {
		return CommonUtility.replaceNull(flg_Addon_CableEntry_MTO);
	}
	public void setFlg_Addon_CableEntry_MTO(String flg_Addon_CableEntry_MTO) {
		this.flg_Addon_CableEntry_MTO = flg_Addon_CableEntry_MTO;
	}
	public String getFlg_Txt_MotorNo_MTO() {
		return CommonUtility.replaceNull(flg_Txt_MotorNo_MTO);
	}
	public void setFlg_Txt_MotorNo_MTO(String flg_Txt_MotorNo_MTO) {
		this.flg_Txt_MotorNo_MTO = flg_Txt_MotorNo_MTO;
	}
	public String getFlg_Txt_RV_MTO() {
		return CommonUtility.replaceNull(flg_Txt_RV_MTO);
	}
	public void setFlg_Txt_RV_MTO(String flg_Txt_RV_MTO) {
		this.flg_Txt_RV_MTO = flg_Txt_RV_MTO;
	}
	public String getFlg_Txt_RA_MTO() {
		return CommonUtility.replaceNull(flg_Txt_RA_MTO);
	}
	public void setFlg_Txt_RA_MTO(String flg_Txt_RA_MTO) {
		this.flg_Txt_RA_MTO = flg_Txt_RA_MTO;
	}
	public String getFlg_Txt_LoadGD2_MTO() {
		return CommonUtility.replaceNull(flg_Txt_LoadGD2_MTO);
	}
	public void setFlg_Txt_LoadGD2_MTO(String flg_Txt_LoadGD2_MTO) {
		this.flg_Txt_LoadGD2_MTO = flg_Txt_LoadGD2_MTO;
	}
	public String getFlg_Txt_NonSTDPaint_MTO() {
		return CommonUtility.replaceNull(flg_Txt_NonSTDPaint_MTO);
	}
	public void setFlg_Txt_NonSTDPaint_MTO(String flg_Txt_NonSTDPaint_MTO) {
		this.flg_Txt_NonSTDPaint_MTO = flg_Txt_NonSTDPaint_MTO;
	}
	public String getIsReferDesign() {
		return CommonUtility.replaceNull(isReferDesign);
	}
	public void setIsReferDesign(String isReferDesign) {
		this.isReferDesign = isReferDesign;
	}
	public String getIsDesignComplete() {
		return CommonUtility.replaceNull(isDesignComplete);
	}
	public void setIsDesignComplete(String isDesignComplete) {
		this.isDesignComplete = isDesignComplete;
	}
	public String getLtCableSizeId() {
		return CommonUtility.replaceNull(ltCableSizeId);
	}
	public void setLtCableSizeId(String ltCableSizeId) {
		this.ltCableSizeId = ltCableSizeId;
	}
	public String getLtNoOfRuns() {
		return CommonUtility.replaceNull(ltNoOfRuns);
	}
	public void setLtNoOfRuns(String ltNoOfRuns) {
		this.ltNoOfRuns = ltNoOfRuns;
	}
	public String getLtNoOfCores() {
		return CommonUtility.replaceNull(ltNoOfCores);
	}
	public void setLtNoOfCores(String ltNoOfCores) {
		this.ltNoOfCores = ltNoOfCores;
	}
	public String getLtCrossSectionAreaId() {
		return CommonUtility.replaceNull(ltCrossSectionAreaId);
	}
	public void setLtCrossSectionAreaId(String ltCrossSectionAreaId) {
		this.ltCrossSectionAreaId = ltCrossSectionAreaId;
	}
	public String getLtConductorMaterialId() {
		return CommonUtility.replaceNull(ltConductorMaterialId);
	}
	public void setLtConductorMaterialId(String ltConductorMaterialId) {
		this.ltConductorMaterialId = ltConductorMaterialId;
	}
	public String getLtCableDiameter() {
		return CommonUtility.replaceNull(ltCableDiameter);
	}
	public void setLtCableDiameter(String ltCableDiameter) {
		this.ltCableDiameter = ltCableDiameter;
	}
	public String getLtAltitude() {
		return CommonUtility.replaceNull(ltAltitude);
	}
	public void setLtAltitude(String ltAltitude) {
		this.ltAltitude = ltAltitude;
	}
	
	// Add-on List for NewRatingDto
	public ArrayList getAddOnList() {
		return addOnList;
	}
	public void setAddOnList(ArrayList addOnList) {
		this.addOnList = addOnList;
	}
	
	public String getLtProdGroupIdVal() {
		return CommonUtility.replaceNull(ltProdGroupIdVal);
	}
	public void setLtProdGroupIdVal(String ltProdGroupIdVal) {
		this.ltProdGroupIdVal = ltProdGroupIdVal;
	}
	public String getLtProdLineIdVal() {
		return CommonUtility.replaceNull(ltProdLineIdVal);
	}
	public void setLtProdLineIdVal(String ltProdLineIdVal) {
		this.ltProdLineIdVal = ltProdLineIdVal;
	}
	public String getLtKWIdVal() {
		return CommonUtility.replaceNull(ltKWIdVal);
	}
	public void setLtKWIdVal(String ltKWIdVal) {
		this.ltKWIdVal = ltKWIdVal;
	}
	public String getLtPoleIdVal() {
		return CommonUtility.replaceNull(ltPoleIdVal);
	}
	public void setLtPoleIdVal(String ltPoleIdVal) {
		this.ltPoleIdVal = ltPoleIdVal;
	}
	public String getLtFrameIdVal() {
		return CommonUtility.replaceNull(ltFrameIdVal);
	}
	public void setLtFrameIdVal(String ltFrameIdVal) {
		this.ltFrameIdVal = ltFrameIdVal;
	}
	public String getLtFrameSuffixIdVal() {
		return CommonUtility.replaceNull(ltFrameSuffixIdVal);
	}
	public void setLtFrameSuffixIdVal(String ltFrameSuffixIdVal) {
		this.ltFrameSuffixIdVal = ltFrameSuffixIdVal;
	}
	public String getLtMountingIdVal() {
		return CommonUtility.replaceNull(ltMountingIdVal);
	}
	public void setLtMountingIdVal(String ltMountingIdVal) {
		this.ltMountingIdVal = ltMountingIdVal;
	}
	public String getLtTBPositionIdVal() {
		return CommonUtility.replaceNull(ltTBPositionIdVal);
	}
	public void setLtTBPositionIdVal(String ltTBPositionIdVal) {
		this.ltTBPositionIdVal = ltTBPositionIdVal;
	}
	public String getLtHazardZoneId() {
		return CommonUtility.replaceNull(ltHazardZoneId);
	}
	public void setLtHazardZoneId(String ltHazardZoneId) {
		this.ltHazardZoneId = ltHazardZoneId;
	}
	public String getLtDustGroupId() {
		return CommonUtility.replaceNull(ltDustGroupId);
	}
	public void setLtDustGroupId(String ltDustGroupId) {
		this.ltDustGroupId = ltDustGroupId;
	}
	public String getLtTempClass() {
		return CommonUtility.replaceNull(ltTempClass);
	}
	public void setLtTempClass(String ltTempClass) {
		this.ltTempClass = ltTempClass;
	}
	public String getLtAmbTempRemoveId() {
		return CommonUtility.replaceNull(ltAmbTempRemoveId);
	}
	public void setLtAmbTempRemoveId(String ltAmbTempRemoveId) {
		this.ltAmbTempRemoveId = ltAmbTempRemoveId;
	}
	public ArrayList<KeyValueVo> getNewMTOTechDetailsList() {
		return newMTOTechDetailsList;
	}
	public void setNewMTOTechDetailsList(ArrayList<KeyValueVo> newMTOTechDetailsList) {
		this.newMTOTechDetailsList = newMTOTechDetailsList;
	}
	public ArrayList<NewMTOAddOnDto> getNewMTOAddOnsList() {
		return newMTOAddOnsList;
	}
	public void setNewMTOAddOnsList(ArrayList<NewMTOAddOnDto> newMTOAddOnsList) {
		this.newMTOAddOnsList = newMTOAddOnsList;
	}
	public String getLtEnclosure() {
		return CommonUtility.replaceNull(ltEnclosure);
	}
	public void setLtEnclosure(String ltEnclosure) {
		this.ltEnclosure = ltEnclosure;
	}
	public String getLtGreaseType() {
		return CommonUtility.replaceNull(ltGreaseType);
	}
	public void setLtGreaseType(String ltGreaseType) {
		this.ltGreaseType = ltGreaseType;
	}
	public String getLtBroughtOutTerminals() {
		return CommonUtility.replaceNull(ltBroughtOutTerminals);
	}
	public void setLtBroughtOutTerminals(String ltBroughtOutTerminals) {
		this.ltBroughtOutTerminals = ltBroughtOutTerminals;
	}
	public String getLtMainTB() {
		return CommonUtility.replaceNull(ltMainTB);
	}
	public void setLtMainTB(String ltMainTB) {
		this.ltMainTB = ltMainTB;
	}
	public String getLtBearingNDESize() {
		return CommonUtility.replaceNull(ltBearingNDESize);
	}
	public void setLtBearingNDESize(String ltBearingNDESize) {
		this.ltBearingNDESize = ltBearingNDESize;
	}
	public String getLtBearingDESize() {
		return CommonUtility.replaceNull(ltBearingDESize);
	}
	public void setLtBearingDESize(String ltBearingDESize) {
		this.ltBearingDESize = ltBearingDESize;
	}
	public String getLtCableEntry() {
		return CommonUtility.replaceNull(ltCableEntry);
	}
	public void setLtCableEntry(String ltCableEntry) {
		this.ltCableEntry = ltCableEntry;
	}
	public String getLtLubrication() {
		return CommonUtility.replaceNull(ltLubrication);
	}
	public void setLtLubrication(String ltLubrication) {
		this.ltLubrication = ltLubrication;
	}
	public String getLtMotorGD2Value() {
		return CommonUtility.replaceNull(ltMotorGD2Value);
	}
	public void setLtMotorGD2Value(String ltMotorGD2Value) {
		this.ltMotorGD2Value = ltMotorGD2Value;
	}
	public String getLtMotorWeight() {
		return CommonUtility.replaceNull(ltMotorWeight);
	}
	public void setLtMotorWeight(String ltMotorWeight) {
		this.ltMotorWeight = ltMotorWeight;
	}
	public String getLtVibrationProbe() {
		return CommonUtility.replaceNull(ltVibrationProbe);
	}
	public void setLtVibrationProbe(String ltVibrationProbe) {
		this.ltVibrationProbe = ltVibrationProbe;
	}
	public String getLtPerfRatedSpeed() {
		return CommonUtility.replaceNull(ltPerfRatedSpeed);
	}
	public void setLtPerfRatedSpeed(String ltPerfRatedSpeed) {
		this.ltPerfRatedSpeed = ltPerfRatedSpeed;
	}
	public String getLtPerfRatedCurr() {
		return CommonUtility.replaceNull(ltPerfRatedCurr);
	}
	public void setLtPerfRatedCurr(String ltPerfRatedCurr) {
		this.ltPerfRatedCurr = ltPerfRatedCurr;
	}
	public String getLtPerfNoLoadCurr() {
		return CommonUtility.replaceNull(ltPerfNoLoadCurr);
	}
	public void setLtPerfNoLoadCurr(String ltPerfNoLoadCurr) {
		this.ltPerfNoLoadCurr = ltPerfNoLoadCurr;
	}
	public String getLtPerfNominalTorque() {
		return CommonUtility.replaceNull(ltPerfNominalTorque);
	}
	public void setLtPerfNominalTorque(String ltPerfNominalTorque) {
		this.ltPerfNominalTorque = ltPerfNominalTorque;
	}
	public String getLtPerfMaxTorque() {
		return CommonUtility.replaceNull(ltPerfMaxTorque);
	}
	public void setLtPerfMaxTorque(String ltPerfMaxTorque) {
		this.ltPerfMaxTorque = ltPerfMaxTorque;
	}
	public String getLtPerfLockRotorCurr() {
		return CommonUtility.replaceNull(ltPerfLockRotorCurr);
	}
	public void setLtPerfLockRotorCurr(String ltPerfLockRotorCurr) {
		this.ltPerfLockRotorCurr = ltPerfLockRotorCurr;
	}
	public String getLtPerfLockRotorTorque() {
		return CommonUtility.replaceNull(ltPerfLockRotorTorque);
	}
	public void setLtPerfLockRotorTorque(String ltPerfLockRotorTorque) {
		this.ltPerfLockRotorTorque = ltPerfLockRotorTorque;
	}
	public String getLtPerfSSTHot() {
		return CommonUtility.replaceNull(ltPerfSSTHot);
	}
	public void setLtPerfSSTHot(String ltPerfSSTHot) {
		this.ltPerfSSTHot = ltPerfSSTHot;
	}
	public String getLtPerfSSTCold() {
		return CommonUtility.replaceNull(ltPerfSSTCold);
	}
	public void setLtPerfSSTCold(String ltPerfSSTCold) {
		this.ltPerfSSTCold = ltPerfSSTCold;
	}
	public String getLtPerfStartTimeRatedVolt() {
		return CommonUtility.replaceNull(ltPerfStartTimeRatedVolt);
	}
	public void setLtPerfStartTimeRatedVolt(String ltPerfStartTimeRatedVolt) {
		this.ltPerfStartTimeRatedVolt = ltPerfStartTimeRatedVolt;
	}
	public String getLtPerfStartTime80RatedVolt() {
		return CommonUtility.replaceNull(ltPerfStartTime80RatedVolt);
	}
	public void setLtPerfStartTime80RatedVolt(String ltPerfStartTime80RatedVolt) {
		this.ltPerfStartTime80RatedVolt = ltPerfStartTime80RatedVolt;
	}
	public String getLtLoadEffPercent100Above() {
		return CommonUtility.replaceNull(ltLoadEffPercent100Above);
	}
	public void setLtLoadEffPercent100Above(String ltLoadEffPercent100Above) {
		this.ltLoadEffPercent100Above = ltLoadEffPercent100Above;
	}
	public String getLtLoadEffPercentAt100() {
		return CommonUtility.replaceNull(ltLoadEffPercentAt100);
	}
	public void setLtLoadEffPercentAt100(String ltLoadEffPercentAt100) {
		this.ltLoadEffPercentAt100 = ltLoadEffPercentAt100;
	}
	public String getLtLoadEffPercentAt75() {
		return CommonUtility.replaceNull(ltLoadEffPercentAt75);
	}
	public void setLtLoadEffPercentAt75(String ltLoadEffPercentAt75) {
		this.ltLoadEffPercentAt75 = ltLoadEffPercentAt75;
	}
	public String getLtLoadEffPercentAt50() {
		return CommonUtility.replaceNull(ltLoadEffPercentAt50);
	}
	public void setLtLoadEffPercentAt50(String ltLoadEffPercentAt50) {
		this.ltLoadEffPercentAt50 = ltLoadEffPercentAt50;
	}
	public String getLtLoadPFPercentAt100() {
		return CommonUtility.replaceNull(ltLoadPFPercentAt100);
	}
	public void setLtLoadPFPercentAt100(String ltLoadPFPercentAt100) {
		this.ltLoadPFPercentAt100 = ltLoadPFPercentAt100;
	}
	public String getLtLoadPFPercentAt75() {
		return CommonUtility.replaceNull(ltLoadPFPercentAt75);
	}
	public void setLtLoadPFPercentAt75(String ltLoadPFPercentAt75) {
		this.ltLoadPFPercentAt75 = ltLoadPFPercentAt75;
	}
	public String getLtLoadPFPercentAt50() {
		return CommonUtility.replaceNull(ltLoadPFPercentAt50);
	}
	public void setLtLoadPFPercentAt50(String ltLoadPFPercentAt50) {
		this.ltLoadPFPercentAt50 = ltLoadPFPercentAt50;
	}
	public String getLtIAIN() {
		return CommonUtility.replaceNull(ltIAIN);
	}
	public void setLtIAIN(String ltIAIN) {
		this.ltIAIN = ltIAIN;
	}
	public String getLtTATN() {
		return CommonUtility.replaceNull(ltTATN);
	}
	public void setLtTATN(String ltTATN) {
		this.ltTATN = ltTATN;
	}
	public String getLtTKTN() {
		return CommonUtility.replaceNull(ltTKTN);
	}
	public void setLtTKTN(String ltTKTN) {
		this.ltTKTN = ltTKTN;
	}
	public String getMtoHighlightFlag() {
		return CommonUtility.replaceNull(mtoHighlightFlag);
	}
	public void setMtoHighlightFlag(String mtoHighlightFlag) {
		this.mtoHighlightFlag = mtoHighlightFlag;
	}
	public String getRatingMaterialCost() {
		return CommonUtility.replaceNull(ratingMaterialCost);
	}
	public void setRatingMaterialCost(String ratingMaterialCost) {
		this.ratingMaterialCost = ratingMaterialCost;
	}
	public String getRatingLOH() {
		return CommonUtility.replaceNull(ratingLOH);
	}
	public void setRatingLOH(String ratingLOH) {
		this.ratingLOH = ratingLOH;
	}
	public String getRatingTotalCost() {
		return ratingTotalCost;
	}
	public void setRatingTotalCost(String ratingTotalCost) {
		this.ratingTotalCost = ratingTotalCost;
	}
	public String getRatingProfitMargin() {
		return CommonUtility.replaceNull(ratingProfitMargin);
	}
	public void setRatingProfitMargin(String ratingProfitMargin) {
		this.ratingProfitMargin = ratingProfitMargin;
	}
	public String getRatingMC_ByDesign() {
		return CommonUtility.replaceNull(ratingMC_ByDesign);
	}
	public void setRatingMC_ByDesign(String ratingMC_ByDesign) {
		this.ratingMC_ByDesign = ratingMC_ByDesign;
	}
	public String getRatingNSPPercent() {
		return CommonUtility.replaceNull(ratingNSPPercent);
	}
	public void setRatingNSPPercent(String ratingNSPPercent) {
		this.ratingNSPPercent = ratingNSPPercent;
	}
	public String getRatingUnitPrice_MCNSP() {
		return CommonUtility.replaceNull(ratingUnitPrice_MCNSP);
	}
	public void setRatingUnitPrice_MCNSP(String ratingUnitPrice_MCNSP) {
		this.ratingUnitPrice_MCNSP = ratingUnitPrice_MCNSP;
	}
	
	
}
