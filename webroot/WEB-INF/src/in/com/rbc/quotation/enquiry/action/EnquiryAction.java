package in.com.rbc.quotation.enquiry.action;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Connection;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.namespace.QName;
import javax.xml.rpc.ParameterMode;

import org.apache.axis.client.Call;
import org.apache.axis.client.Service;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

import in.com.rbc.quotation.admin.dao.CustomerDao;
import in.com.rbc.quotation.admin.dao.MasterDao;
import in.com.rbc.quotation.admin.dto.CustomerDto;
import in.com.rbc.quotation.base.BaseAction;
import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.dto.AttachmentDto;
import in.com.rbc.quotation.common.utility.AttachmentUtility;
import in.com.rbc.quotation.common.utility.CommonUtility;
import in.com.rbc.quotation.common.utility.DBUtility;
import in.com.rbc.quotation.common.utility.DateUtility;
import in.com.rbc.quotation.common.utility.ExceptionUtility;
import in.com.rbc.quotation.common.utility.LoggerUtility;
import in.com.rbc.quotation.common.utility.PDFUtility;
import in.com.rbc.quotation.common.utility.PropertyUtility;
import in.com.rbc.quotation.common.vo.KeyValueVo;
import in.com.rbc.quotation.enquiry.dao.QuotationDao;
import in.com.rbc.quotation.enquiry.dao.SearchEnquiryDao;
import in.com.rbc.quotation.enquiry.dao.SearchEnquiryDaoImpl;
import in.com.rbc.quotation.enquiry.dto.AddOnDto;
import in.com.rbc.quotation.enquiry.dto.DeliveryDetailsDto;
import in.com.rbc.quotation.enquiry.dto.EnquiryDto;
import in.com.rbc.quotation.enquiry.dto.RatingDto;
import in.com.rbc.quotation.enquiry.dto.SearchEnquiryDto;
import in.com.rbc.quotation.enquiry.dto.TeamMemberDto;
import in.com.rbc.quotation.enquiry.dto.TechnicalOfferDto;
import in.com.rbc.quotation.enquiry.form.EnquiryForm;
import in.com.rbc.quotation.factory.DaoFactory;
import in.com.rbc.quotation.user.action.UserAction;
import in.com.rbc.quotation.user.dto.UserDto;
import in.com.rbc.quotation.workflow.dao.WorkflowDao;
import in.com.rbc.quotation.workflow.dto.WorkflowDto;
import in.com.rbc.quotation.workflow.utility.WorkflowUtility;

public class EnquiryAction extends BaseAction {
	
	 /**
     * This method is used to display the home page.
     * @param actionMapping	ActionMapping object to redirect the user to next page depending on the activity
     * @param actionForm	ActionForm object to handle the form elements
     * @param request		HttpServletRequest object to handle request operations
     * @param response		HttpServletResponse object to handle response operations
     * @return returns 		ActionForward depending on which user is redirected to next page
     * @throws Exception	if any error occurs while performing database operations, etc
     */
    public ActionForward home(ActionMapping actionMapping,
                              ActionForm actionForm, 
                              HttpServletRequest request,
                              HttpServletResponse response) throws Exception {
    	
    	String sMethodName = QuotationConstants.QUOTATION_FORWARD_HOME;    	 
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
    	Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects        
        UserDto oUserDto = null;
        
        ArrayList arlMyRecentEnquires = null;
        ArrayList arlMyAwaitingApprovalEnquires = null;
        ArrayList arlMyRecentReleasedEnquires = null;
        UserAction oUserAction =null; //Instantiating the UserAction object
        DaoFactory oDaoFactory =null; //Creating an instance of of DaoFactory & retrieving UserDao object
        WorkflowDao oWorkflowDao =null; //Retrieving the WorkflowDao object
        EnquiryForm oEnquiryForm =null;
        SearchEnquiryDao oSearchEnquiryDao =null;
    	try {
    		//System.out.println(" Inside home method : " );
    		/*
             * Setting user's information in request, using the header information
             * received through request
             */
    		//System.out.println("Calling : setRolesToRequest(request)  : " );
            setRolesToRequest(request);
            //System.out.println("Finished : setRolesToRequest(request)  : " );
            
            LoggerUtility.log("INFO", sClassName, sMethodName, "Roles set to request.");
            
            oEnquiryForm = (EnquiryForm) actionForm;
            if (oEnquiryForm == null)
            	oEnquiryForm = new EnquiryForm();
            
            /* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Instantiating the UserAction object */
            oUserAction = new UserAction();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
            /* Retrieving the WorkflowDao object */
            oWorkflowDao = oDaoFactory.getWorkflowDao();
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            /* Retrieving the logged-in user information */
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            
            /* Retrieving the enquires list to be displayed in home page */
            arlMyRecentEnquires = oWorkflowDao.getMyRecentEnquires(conn, oUserDto.getUserId()); 
            arlMyAwaitingApprovalEnquires = oWorkflowDao.getMyAwaitingApprovalEnquires(conn, oUserDto.getUserId()); 
            arlMyRecentReleasedEnquires = oWorkflowDao.getMyRecentReleasedEnquires(conn, oUserDto.getUserId());
            
            request.setAttribute(QuotationConstants.QUOTATION_RECINQ, arlMyRecentEnquires);
            request.setAttribute(QuotationConstants.QUOTATION_WAITINQ, arlMyAwaitingApprovalEnquires);
            request.setAttribute(QuotationConstants.QUOTATION_RELEINQ, arlMyRecentReleasedEnquires);
            request.setAttribute(QuotationConstants.QUOTATION_USERDTO, oUserDto);
            
            // Now set the Login User FullName - To session variable.
            request.getSession().setAttribute(QuotationConstants.QUOTATIONLT_LOGINUSER_FULLNAME, oUserDto.getFullName());
            
            // check if the logged-in user is a SM or not
            oSearchEnquiryDao = oDaoFactory.getSearchEnquiryDao();
            if (oSearchEnquiryDao.isSalesManager(conn, oUserDto.getUserId())){
            	request.getSession().setAttribute(QuotationConstants.QUOTATION_ISSALESMGR, QuotationConstants.QUOTATION_STRING_TRUE);
            } else {
            	request.getSession().removeAttribute(QuotationConstants.QUOTATION_ISSALESMGR);
            }
    	} catch(Exception e) {
            /*
             * Logging any exception that raised during this activity in log files using Log4j
             */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "home", ExceptionUtility.getStackTraceAsString(e));
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        } finally {
            /* Releasing/closing the connection object */
        	if (conn != null)
        		oDBUtility.releaseResources(conn);
        }
    	
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
    	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_SUCCESS);
    }
    
    
    /**
     * This method is used to get look up fields data required in the create page.
     * @param actionMapping	ActionMapping object to redirect the user to next page depending on the activity
     * @param actionForm	ActionForm object to handle the form elements
     * @param request		HttpServletRequest object to handle request operations
     * @param response		HttpServletResponse object to handle response operations
     * @return returns 		ActionForward depending on which user is redirected to next page
     * @throws Exception	if any error occurs while performing database operations, etc
     */
    public ActionForward viewCreateEnquiry(ActionMapping actionMapping,
                              ActionForm actionForm, 
                              HttpServletRequest request,
                              HttpServletResponse response) throws Exception {
    	
    	String sMethodName = QuotationConstants.QUOTATION_VIEWCINQ;
    	Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        
        UserDto oUserDto = null;
        UserAction oUserAction =null; //Instantiating the UserAction object
        DaoFactory oDaoFactory =null; //Creating an instance of of DaoFactory & retrieving UserDao object
        SearchEnquiryDao oSearchEnquiryDao =null; //Check if the logged-in user is a sales manager or not, if not re-direct the user to the exeption page
        EnquiryDto oEnquiryDto =null; //Creating an instance of of EnquiryDto
        QuotationDao oQuotationDao =null;// Creating an instance of QuotationDao
        EnquiryForm oEnquiryForm =null;
        KeyValueVo oKeyValueVo =null;
        AttachmentDto oAttachmentDto =null;
        ArrayList alRatingList =new ArrayList(); //Creating an Instance of Rating List
        
    	try {
    		oEnquiryForm = (EnquiryForm) actionForm;
    		if (oEnquiryForm == null)
    			oEnquiryForm = new EnquiryForm();
    		
    		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
    		
    		/*
             * Ssetting user's information in request, using the header information
             * received through request
             */
            setRolesToRequest(request);
            LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "Roles set to request.");
            
            
            /* To avoid duplicate form submission (using Synchronizer Token Pattern)
    		 * This token will be validated while saving the request
    		 */
    		saveToken(request);
            
            /* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Instantiating the UserAction object */
            oUserAction = new UserAction();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
    		
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            
            /* Retrieving the logged-in user information */
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            
            /* Check if the logged-in user is a sales manager or not, if not re-direct the user to the exeption page*/
            oSearchEnquiryDao = oDaoFactory.getSearchEnquiryDao();
            
            if(! oSearchEnquiryDao.isSalesManager(conn,oUserDto.getUserId())){      	
        	 /* Setting error details to request to display the same in Exception page */
            	request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_MSG_UNAUTH);
              
             /* Forwarding request to Error page */
               return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);  
        	
            }
            
            oEnquiryDto = new EnquiryDto();
            oQuotationDao = oDaoFactory.getQuotationDao();
            PropertyUtils.copyProperties(oEnquiryDto,oEnquiryForm);
            oEnquiryDto = oQuotationDao.loadLookUpValues(conn, oEnquiryDto);
            PropertyUtils.copyProperties(oEnquiryForm, oEnquiryDto);
            
            oEnquiryForm = loadLookUpValues(conn, oEnquiryForm);
            
            alRatingList = oEnquiryForm.getRatingList();
            
			oEnquiryForm = checkMaxRatingLimit(oEnquiryForm);
					
			oEnquiryForm.setRatingNo(QuotationConstants.QUOTATION_LITERAL_ONE);
			
			oKeyValueVo = new KeyValueVo();
			oKeyValueVo.setKey("");
			oKeyValueVo.setValue(QuotationConstants.QUOTATION_RATING1);
			oKeyValueVo.setValue2(QuotationConstants.QUOTATION_STRING_N);		
			alRatingList.add(oKeyValueVo);
		
			oEnquiryForm.setRatingList(alRatingList);
                 
			oEnquiryDto.setCreatedBySSO(oUserDto.getUserId());
            oEnquiryForm.setCreatedBy(oUserDto.getFullName());           
            oEnquiryForm.setCreatedBySSO(oUserDto.getUserId());
            oEnquiryForm.setCreatedDate(DateUtility.getRBCStandardDate());
            oEnquiryForm.setRevisionNumber(QuotationConstants.QUOTATION_LITERAL_ONE); 
            oEnquiryForm.setRatingNo(QuotationConstants.QUOTATION_LITERAL_ONE); 
            oEnquiryForm.setRatingTitle(QuotationConstants.QUOTATION_RATING+oEnquiryForm.getRevisionNumber());
            oEnquiryForm.setEnquiryOperationType(QuotationConstants.QUOTATION_INSERT);
            oEnquiryForm.setRatingOperationType(QuotationConstants.QUOTATION_INSERT);
            
            if (request.getSession().getAttribute(QuotationConstants.QUOTATION_ATTDTO) == null) 
            {
            	// get the attachment details and put them in session
                oAttachmentDto = AttachmentUtility.getApplicationData();
                if (oAttachmentDto != null)
                	request.getSession().setAttribute(QuotationConstants.QUOTATION_ATTDTO, oAttachmentDto);
            }
            	
    	} catch(Exception e) {
            /*
             * Logging any exception that raised during this activity in log files using Log4j
             */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "home", ExceptionUtility.getStackTraceAsString(e));
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        } finally {
            /* Releasing/closing the connection object */
        	if (conn != null)
        		oDBUtility.releaseResources(conn);	
        }
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
    	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_VIEW);
    }
    
    /**
     * This method is used to create an enquiry.
     * @param actionMapping
     *            ActionMapping object to redirect the user to next page
     *            depending on the activity
     * @param actionForm
     *            ActionForm object to handle the form elements
     * @param request
     *            HttpServletRequest object to handle request operations
     * @param response
     *            HttpServletResponse object to handle response operations
     * @return returns ActionForward depending on which user is redirected to
     *         next page
     * @throws Exception
     *             if any error occurs while performing database operations, etc
     * @author 100006126 <br>
     * Created on: Sep 30, 2008
     */
    public ActionForward createEnquiry(ActionMapping actionMapping,
                              ActionForm actionForm, 
                              HttpServletRequest request,
                              HttpServletResponse response) throws Exception {
    	
    	String sMethodName = QuotationConstants.QUOTATION_CINQ; 
        String sClassName = this.getClass().getName();
    	LoggerUtility.log("INFO", sClassName, sMethodName, "START");
    	Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        
        UserDto oUserDto = null;
      
        String sOperationType = QuotationConstants.QUOTATION_NONE; 
        String sDispatch = null;
        String sCommentOperation=null;
        UserAction oUserAction =null; //Instantiating the UserAction object
        DaoFactory oDaoFactory =null; //Creating an instance of of DaoFactory & retrieving UserDao object
        QuotationDao oQuotationDao =null;// Creating an Instance for QuotationDao
        EnquiryDto oEnquiryDto = null;
        RatingDto oRatingDto = null;
        String sMsg =null;
        String sLinkMsg1 =null;
        String sLinkMsg2 =null;
        String sLinkMsg =null;
        int iWorkflowExecuteStatus =0;
        boolean isUpdatedRating=QuotationConstants.QUOTATION_FALSE;
		String sEnquiryOperationType=null;
		String sDmComment=null;
		String sDeComment=null;
		RatingDto oTempRatingDto =null;
		ArrayList alRatingList =null;
		int iMaxId =QuotationConstants.QUOTATION_LITERAL_ZERO;
		EnquiryForm oEnquiryForm =null;
		WorkflowUtility oWorkflowUtility =null;
		WorkflowDto oWorkflowDto = null;
		WorkflowDao oWorkFlowDao =null;
		KeyValueVo oKeyValueVo =null;


    	
    	try{
    		
    		oEnquiryForm = (EnquiryForm) actionForm;
    		if (oEnquiryForm == null)
    			oEnquiryForm = new EnquiryForm();
    		
    		sOperationType = oEnquiryForm.getOperationType();
    		
    		sDispatch = oEnquiryForm.getDispatch(); 
    		sCommentOperation=oEnquiryForm.getCommentOperation();
            
    	
    		/*
             * Ssetting user's information in request, using the header information
             * received through request
             */
            setRolesToRequest(request);
            LoggerUtility.log("INFO", sClassName, sMethodName, "Roles set to request.");
            
            /* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Instantiating the UserAction object */
            oUserAction = new UserAction();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
            
            oQuotationDao = oDaoFactory.getQuotationDao();
    		
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
           
            if (! isTokenValid(request)) {
           	 /* Setting error details to request to display the same in Exception page */ 
               request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_INVALIDTOKENMESSAGE);
               /* Forwarding request to Error page */
               return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
           }          
           
            
            oDBUtility.setAutoCommitFalse(conn);
            
            /* Retrieving the logged-in user information */
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            
            if (oUserDto == null){
            	oUserDto = new UserDto();
            }
            
            oEnquiryDto = new EnquiryDto();
            oRatingDto = new RatingDto();
            
            
            PropertyUtils.copyProperties(oEnquiryDto, oEnquiryForm);  
            
          
            
            LoggerUtility.log("INFO", sClassName, sMethodName, "iEnquiryId -  "+oEnquiryDto.getEnquiryId());
            
            /*
             * This block will be executed when the enquiry is submitted from view-draft page.
             * 
             */
            if (sDispatch.equalsIgnoreCase(QuotationConstants.QUOTATION_VIEWSUBMIT)){
            	LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Initiating Workflow process - - -");
                
                oWorkflowUtility = new WorkflowUtility();
                oWorkflowDto = new WorkflowDto();               
                
                oWorkflowDto.setEnquiryId(oEnquiryDto.getEnquiryId());
                oWorkflowDto.setEnquiryNo(oEnquiryDto.getEnquiryNumber());
                oWorkflowDto.setLoginUserId(oUserDto.getUserId());
                oWorkflowDto.setLoginUserName(oUserDto.getFullName());
                oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
                oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTODESIGNOFFICE);
                oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_DRAFT);
               
                LoggerUtility.log("INFO", sClassName, sMethodName, "1 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
                iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
                
                oDBUtility.commit(conn);
        		oDBUtility.setAutoCommitTrue(conn);
        		
                if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE ) {
                    LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFS);
                } else  if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ZERO ){
                    LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFSEMAILF);
                } else {
                    LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFF);
                }
                
                LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFPC);
                
                sMsg = QuotationConstants.QUOTATION_TRACKNO+oEnquiryDto.getEnquiryNumber();
                sLinkMsg1 = QuotationConstants.QUOTATION_VALIDATEREQ_URL+oEnquiryDto.getEnquiryId()+QuotationConstants.QUOTATION_GRSYMBOL+QuotationConstants.QUOTATION_VIEWLINK; 
                sLinkMsg2 = QuotationConstants.QUOTATION_VIEWCR_ENQ_URL+QuotationConstants.QUOTATION_PLACELINKS; 
                
                sLinkMsg = sLinkMsg1+QuotationConstants.QUOTATION_BREAK+sLinkMsg2; 
                request.setAttribute(QuotationConstants.QUOTATION_HEADINGMESSAGE,QuotationConstants.QUOTATION_SUB_SUCES);
                request.setAttribute(QuotationConstants.QUOTATION_SUCCESSMESSAGE,sMsg);
                request.setAttribute(QuotationConstants.QUOTATION_LINKSMESSAGE,sLinkMsg);
    			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);	
    			
            }else{            	
            	
            	
            	
            
            	oEnquiryDto = oQuotationDao.createEnquiry(conn, oEnquiryDto, oEnquiryForm.getEnquiryOperationType());
            
            	
            	if (oEnquiryDto.getEnquiryId() > QuotationConstants.QUOTATION_LITERAL_ZERO){            	
            		oEnquiryForm.setEnquiryId(oEnquiryDto.getEnquiryId());
            		PropertyUtils.copyProperties(oRatingDto, oEnquiryForm);  

            		oRatingDto = oQuotationDao.createRating(conn, oRatingDto, oEnquiryForm.getRatingOperationType());
            	
            		LoggerUtility.log("INFO", sClassName, sMethodName, "iRatingId -  "+oRatingDto.getRatingId());            	
            	}
            	oDBUtility.setAutoCommitFalse(conn);

            	if (oEnquiryDto.getEnquiryId() > QuotationConstants.QUOTATION_LITERAL_ZERO && oRatingDto.getRatingId() > QuotationConstants.QUOTATION_LITERAL_ZERO){
            		oDBUtility.commit(conn);
            		oDBUtility.setAutoCommitTrue(conn);
            		
            		// now start the attachment piece
            		
            		String sAttach1 = addAttachment( oEnquiryForm.getTheFile1(),oEnquiryForm.getFileDescription1(),oEnquiryForm.getEnquiryId(), oUserDto);
            		request.setAttribute(QuotationConstants.QUOTATION_ATTACHMENT1, sAttach1);
            		oEnquiryForm.setFileDescription1("");
            		String sAttach2 = addAttachment( oEnquiryForm.getTheFile2(),oEnquiryForm.getFileDescription2(),oEnquiryForm.getEnquiryId(), oUserDto);
            		request.setAttribute(QuotationConstants.QUOTATION_ATTACHMENT2, sAttach2);
            		oEnquiryForm.setFileDescription2("");
            		String sAttach3 = addAttachment( oEnquiryForm.getTheFile3(),oEnquiryForm.getFileDescription3(),oEnquiryForm.getEnquiryId(), oUserDto);
            		request.setAttribute(QuotationConstants.QUOTATION_ATTACHMENT3, sAttach3);
            		oEnquiryForm.setFileDescription3("");
            		String sAttach4 = addAttachment( oEnquiryForm.getTheFile4(),oEnquiryForm.getFileDescription4(),oEnquiryForm.getEnquiryId(), oUserDto);
            		request.setAttribute(QuotationConstants.QUOTATION_ATTACHMENT4, sAttach4);
            		oEnquiryForm.setFileDescription4("");
            		String sAttach5 = addAttachment( oEnquiryForm.getTheFile5(),oEnquiryForm.getFileDescription5(),oEnquiryForm.getEnquiryId(), oUserDto);
            		request.setAttribute(QuotationConstants.QUOTATION_ATTACHMENT5, sAttach5);
            		oEnquiryForm.setFileDescription5("");
            		
            		
            	
				
            		/* now check where to forward the request to
            		 *  if  sOperationType = 'DRAFT' - then forward the request to view page
            		 *  if  sOperationType = 'ADDNEWRATING' - then forward the request to create page 
            		 *  if  sOperationType = 'SUBMIT' - then forward the request to success page
            		 */
				
            		request.setAttribute(QuotationConstants.QUOTATION_REQUEST_ENQUIRYID, ""+oEnquiryDto.getEnquiryId());
            		request.setAttribute(QuotationConstants.QUOTATION_REQUEST_RATINGID, ""+oRatingDto.getRatingId());
            		request.setAttribute(QuotationConstants.QUOTATION_REQUEST_ENQUIRYCODE,oEnquiryDto.getEnquiryNumber());
            		LoggerUtility.log("INFO", sClassName, sMethodName, "sOperationType -  "+sOperationType);
            		if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_DRAFT)){
            			//reset the token
            			resetToken(request);
            			return new ActionForward (QuotationConstants.QUOTATION_GET_ENQDET_URL+oEnquiryDto.getEnquiryId(),true);
            		}else if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_SUBMIT)){ 
            			
            			resetToken(request);
            		    LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Initiating Workflow process - - -");
            		    oWorkFlowDao = oDaoFactory.getWorkflowDao();
                        oWorkflowUtility = new WorkflowUtility();
                        oWorkflowDto = new WorkflowDto();
                        
                        
                        oWorkflowDto.setEnquiryId(oEnquiryDto.getEnquiryId());
                        oWorkflowDto.setEnquiryNo(oEnquiryDto.getEnquiryNumber());
                     
                        oWorkflowDto.setLoginUserId(oUserDto.getUserId());
                        oWorkflowDto.setLoginUserName(oUserDto.getFullName());
                        oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
                        oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTODESIGNOFFICE); 
                        oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_DRAFT); 
                        
                        LoggerUtility.log("INFO", sClassName, sMethodName, "2 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
                        
                        
                        iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
                        
                        isUpdatedRating=oWorkflowUtility.updateRatingIsValidated(conn,oWorkFlowDao,oWorkflowDto);
                       
                        if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE ) {
                            LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFS);
                        } else  if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ZERO){ 
                            LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFSEMAILF);
                        } else {
                            LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFF);
                        }
                        
                        LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFPC);
                        
                        if(isUpdatedRating)
                        {
                            LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_RATINGUS);
	
                        }
                        else
                        {
                            LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_RATINGUF);
	
                        }
                        LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_RATINGUC);

                        sMsg = QuotationConstants.QUOTATION_TRACKNO+oEnquiryDto.getEnquiryNumber();
                        sLinkMsg1 = QuotationConstants.QUOTATION_VALIDATEREQ_URL+oEnquiryDto.getEnquiryId()+QuotationConstants.QUOTATION_GRSYMBOL+QuotationConstants.QUOTATION_VIEWLINK; 
                        sLinkMsg2 = QuotationConstants.QUOTATION_VIEWCR_ENQ_URL+QuotationConstants.QUOTATION_PLACELINKS; 
                        
                        sLinkMsg = sLinkMsg1+QuotationConstants.QUOTATION_BREAK+sLinkMsg2;
                        request.setAttribute(QuotationConstants.QUOTATION_HEADINGMESSAGE,QuotationConstants.QUOTATION_SUB_SUCES);
                        request.setAttribute(QuotationConstants.QUOTATION_SUCCESSMESSAGE,sMsg);
                        request.setAttribute(QuotationConstants.QUOTATION_LINKSMESSAGE,sLinkMsg);
            			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);					
            		}//Ending Of Submit
            		else if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_RETURN)){ 
            			
            			resetToken(request);
            		    LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Initiating Workflow process - - -");
            		    oWorkFlowDao = oDaoFactory.getWorkflowDao();
                        oWorkflowUtility = new WorkflowUtility();
                        oWorkflowDto = new WorkflowDto();
                        
                        
                        oWorkflowDto.setEnquiryId(oEnquiryDto.getEnquiryId());
                        oWorkflowDto.setEnquiryNo(oEnquiryDto.getEnquiryNumber());
                     
                        oWorkflowDto.setLoginUserId(oUserDto.getUserId());
                        oWorkflowDto.setLoginUserName(oUserDto.getFullName());
                        oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
                        oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_ASSIGN_TO_DESIGNOFFICE); 
                        oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_ReturnSM); 
                        
                        LoggerUtility.log("INFO", sClassName, sMethodName, "2 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
                        
                        
                       iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
                        
                       isUpdatedRating=oWorkflowUtility.updateRatingIsValidated(conn,oWorkFlowDao,oWorkflowDto);
                       
                        if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE ){
                            LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFS);
                        } else  if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ZERO){
                            LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFSEMAILF);
                        } else {
                            LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFF);
                        }
                        
                        LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFPC);
                        
                        if(isUpdatedRating)
                        {
                            LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_RATINGUS);
	
                        }
                        else
                        {
                            LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_RATINGUF);
	
                        }
                        LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_RATINGUC);

                        sMsg = QuotationConstants.QUOTATION_TRACKNO+oEnquiryDto.getEnquiryNumber();
                        sLinkMsg1 = QuotationConstants.QUOTATION_VALIDATEREQ_URL+oEnquiryDto.getEnquiryId()+QuotationConstants.QUOTATION_GRSYMBOL+QuotationConstants.QUOTATION_VIEWLINK; 
                        sLinkMsg2 = QuotationConstants.QUOTATION_VIEWCR_ENQ_URL+QuotationConstants.QUOTATION_PLACELINKS; 
                        
                        sLinkMsg = sLinkMsg1+QuotationConstants.QUOTATION_BREAK+sLinkMsg2;
                        request.setAttribute(QuotationConstants.QUOTATION_HEADINGMESSAGE,QuotationConstants.QUOTATION_SUB_SUCES);
                        request.setAttribute(QuotationConstants.QUOTATION_SUCCESSMESSAGE,sMsg);
                        request.setAttribute(QuotationConstants.QUOTATION_LINKSMESSAGE,sLinkMsg);
            			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);					
            		}//Ending Of Return

            		else if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_ADDNEWRATING)){	
            			//	get the attchment details
                        request.setAttribute(QuotationConstants.QUOTATION_ATTACHLIST, AttachmentUtility.getAttachmentDetails(oEnquiryDto));
                        request.setAttribute(QuotationConstants.QUOTATION_APPID, ""+QuotationConstants.QUOTATION_ATTACHMENT_APPLICATIONID); 
            				
            			sEnquiryOperationType=QuotationConstants.QUOTATION_FORWARD_UPDATE.toUpperCase();
            			sDmComment=CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_DMCOMM); 
            			sDeComment=CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_DECOMM);
					
					
            			if (oEnquiryForm.getRatingOperation().equalsIgnoreCase(QuotationConstants.QUOTATION_NEW)) 
            			{						
            				PropertyUtils.copyProperties(oEnquiryForm, new RatingDto());
            			}
					
            			oEnquiryDto = oQuotationDao.loadLookUpValues(conn, oEnquiryDto);
            			PropertyUtils.copyProperties(oEnquiryForm, oEnquiryDto);
		            
            			oEnquiryForm = loadLookUpValues(conn, oEnquiryForm);
		            
            			
            			// get all the revisions applicable to this Enquiry
            			oEnquiryForm.setRatingList(oQuotationDao.getRatingListByEnquiryId(conn, oEnquiryDto)); 
            			
            			
            			
            			
            			if (oEnquiryForm.getRatingOperationId().trim().length() > QuotationConstants.QUOTATION_LITERAL_ZERO){						
						
            				oTempRatingDto = new RatingDto();
            				oRatingDto.setRatingId((Integer.parseInt(oEnquiryForm.getRatingOperationId())));
            				oTempRatingDto = oQuotationDao.getRatingDetails(conn, oRatingDto);
            				PropertyUtils.copyProperties(oEnquiryForm, oTempRatingDto);
            				oEnquiryForm.setRatingOperationType(QuotationConstants.QUOTATION_FORWARD_UPDATE.toUpperCase());						
            				oEnquiryForm.setRatingNo(oTempRatingDto.getRatingNo());	
            				oEnquiryForm.setCurrentRating(oEnquiryForm.getRatingOperationId());
            				
            			}else{
            				alRatingList = oEnquiryForm.getRatingList();
            				oEnquiryForm.setRatingOperationType(QuotationConstants.QUOTATION_FORWARD_INSERT); 
						
            				//  set the rating title
            				
							iMaxId = oEnquiryForm.getRatingList().size();
							
							
							iMaxId = iMaxId + 1;
							
							oEnquiryForm.setRatingNo(iMaxId);
							
							oKeyValueVo = new KeyValueVo();
							oKeyValueVo.setKey("");
							oKeyValueVo.setValue(QuotationConstants.QUOTATION_RATING+iMaxId); 
							oKeyValueVo.setValue2(QuotationConstants.QUOTATION_STRING_N);							
							alRatingList.add(oKeyValueVo);							
							oEnquiryForm.setRatingList(alRatingList);
							oEnquiryForm.setCurrentRating("");
							
							
							oEnquiryForm.setRatingId(oRatingDto.getRatingId());
							
            			}
            			
            			// Check for maxRatings
            			oEnquiryForm = checkMaxRatingLimit(oEnquiryForm);
            			
						// set the error message
						oEnquiryForm = buildRatingErrorMessage(oEnquiryForm);		
					

						// to tell that there is not next rating
						oEnquiryForm.setIsLast(QuotationConstants.QUOTATION_STRING_TRUE); 
						
						oEnquiryForm.setRatingOperationId("");

						if(sCommentOperation!=null && sCommentOperation.equalsIgnoreCase(QuotationConstants.QUOTATION_DMRETURN)) 
			            {
						oEnquiryForm.setReturnstatement(QuotationConstants.QUOTATION_RETURNDM);
		            	oEnquiryForm.setReturnstatus(QuotationConstants.QUOTATION_RET_REPSM);
		            	oEnquiryForm.setDmToSmReturn(sDmComment);
		            	oEnquiryForm.setDeToSmReturn(sDeComment);
            		   }
			            else
			            {
							oEnquiryForm.setReturnstatement(QuotationConstants.QUOTATION_EDIT);

			            }
			            oEnquiryForm.setEnquiryOperationType(QuotationConstants.QUOTATION_FORWARD_UPDATE.toUpperCase()); 

					
					
			            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_VIEW);
            		}
            		
            		else if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_SAVEONLY)){
        			// get the attchment details
                        request.setAttribute(QuotationConstants.QUOTATION_ATTACHLIST, AttachmentUtility.getAttachmentDetails(oEnquiryDto)); 
                        request.setAttribute(QuotationConstants.QUOTATION_APPID, ""+QuotationConstants.QUOTATION_ATTACHMENT_APPLICATIONID); 
						oEnquiryDto = oQuotationDao.loadLookUpValues(conn, oEnquiryDto);
			            PropertyUtils.copyProperties(oEnquiryForm, oEnquiryDto);
			            sDmComment=CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_DMCOMM); 
			            sDeComment=CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_DECOMM); 
			            
			            oEnquiryForm = loadLookUpValues(conn, oEnquiryForm);
			            
			            // get all the revisions applicable to this Enquiry
						oEnquiryForm.setRatingList(oQuotationDao.getRatingListByEnquiryId(conn, oEnquiryDto));
						oEnquiryForm.setCurrentRating(""+oRatingDto.getRatingId());
						oEnquiryForm.setRatingId(oRatingDto.getRatingId());
						
						// Check for maxRatings
            			oEnquiryForm = checkMaxRatingLimit(oEnquiryForm);
            			
						// set the error message
						oEnquiryForm = buildRatingErrorMessage(oEnquiryForm);		
						
						//check for next rating
						oEnquiryForm = checkForNextRating(oEnquiryForm);						
						
						oEnquiryForm.setRatingOperationType(QuotationConstants.QUOTATION_FORWARD_UPDATE.toUpperCase()); 
						
			            if(sCommentOperation!=null && sCommentOperation.equalsIgnoreCase(QuotationConstants.QUOTATION_DMRETURN)) 
			            {
						oEnquiryForm.setReturnstatement(QuotationConstants.QUOTATION_RETURNDM); 
		            	oEnquiryForm.setReturnstatus(QuotationConstants.QUOTATION_RET_REPSM);
		            	oEnquiryForm.setDmToSmReturn(sDmComment);
		            	oEnquiryForm.setDeToSmReturn(sDeComment);
            		   }
			            else
			            {
							oEnquiryForm.setReturnstatement(QuotationConstants.QUOTATION_EDIT);

			            }


						
						return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_VIEW);					
            		}else if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_NEXTRATING)) 
            		{
            			
            			sDmComment=CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_DMCOMM);  
            			sDeComment=CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_DECOMM); 
										
						
						//get the attchment details
                        request.setAttribute(QuotationConstants.QUOTATION_ATTACHLIST, AttachmentUtility.getAttachmentDetails(oEnquiryDto)); 
                        request.setAttribute(QuotationConstants.QUOTATION_APPID, ""+QuotationConstants.QUOTATION_ATTACHMENT_APPLICATIONID); 
						
						oEnquiryDto = oQuotationDao.loadLookUpValues(conn, oEnquiryDto);
			            PropertyUtils.copyProperties(oEnquiryForm, oEnquiryDto);
			            
			            oEnquiryForm = loadLookUpValues(conn, oEnquiryForm);
			            
			            // get all the revisions applicable to this Enquiry
						oEnquiryForm.setRatingList(oQuotationDao.getRatingListByEnquiryId(conn, oEnquiryDto));
						
						if (oEnquiryForm.getNextRatingId().trim().length() >QuotationConstants.QUOTATION_LITERAL_ZERO ){						
							
							oTempRatingDto = new RatingDto();
							oRatingDto.setRatingId((Integer.parseInt(oEnquiryForm.getNextRatingId())));
							oTempRatingDto = oQuotationDao.getRatingDetails(conn, oRatingDto);
							PropertyUtils.copyProperties(oEnquiryForm, oTempRatingDto);
							oEnquiryForm.setRatingOperationType(QuotationConstants.QUOTATION_FORWARD_UPDATE.toUpperCase()); 						
							oEnquiryForm.setRatingNo(oTempRatingDto.getRatingNo());	
							oEnquiryForm.setCurrentRating(oEnquiryForm.getNextRatingId());
							
						}
						
						// set the error message
						oEnquiryForm = buildRatingErrorMessage(oEnquiryForm);		
						
						oEnquiryForm = checkForNextRating(oEnquiryForm);
						
						oEnquiryForm.setRatingOperationId("");
						oEnquiryForm.setCreatedBy(oUserDto.getFullName());
			            oEnquiryForm.setCreatedBySSO(oUserDto.getUserId());
			            oEnquiryForm.setCreatedDate(DateUtility.getRBCStandardDate());	
			            
			            if(sCommentOperation!=null && sCommentOperation.equalsIgnoreCase(QuotationConstants.QUOTATION_DMRETURN)) 
			            {
						oEnquiryForm.setReturnstatement(QuotationConstants.QUOTATION_RETURNDM); 
		            	oEnquiryForm.setReturnstatus(QuotationConstants.QUOTATION_RET_REPSM);
		            	oEnquiryForm.setDmToSmReturn(sDmComment);
		            	oEnquiryForm.setDeToSmReturn(sDeComment);
            		   }
			            else
			            {
							oEnquiryForm.setReturnstatement(QuotationConstants.QUOTATION_EDIT);

			            }

			            oEnquiryForm.setEnquiryOperationType(QuotationConstants.QUOTATION_FORWARD_UPDATE.toUpperCase());
						
						return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_VIEW);					
            		}//Next Rating Ending
            		
            	}else{
            	
            		request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE,QuotationConstants.QUOTATION_ER_INSERT);
            	
            		/* Forwarding request to Error page */
            		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
            	
            	}
            }//Else Part Ending
           
    	}catch(Exception e) {
            /*
             * Logging any exception that raised during this activity in log files using Log4j
             */
            LoggerUtility.log("DEBUG", sClassName, "home", ExceptionUtility.getStackTraceAsString(e));
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        } finally {
            /* Releasing/closing the connection object */
            if ( conn!=null )
                oDBUtility.releaseResources(conn);
        }
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
    	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_HOME);
    }
    
    /**
     * This method is used to get the enquiry and rating details based on enquiryId
     * 
     * @param actionMapping
     *            ActionMapping object to redirect the user to next page
     *            depending on the activity
     * @param actionForm
     *            ActionForm object to handle the form elements
     * @param request
     *            HttpServletRequest object to handle request operations
     * @param response
     *            HttpServletResponse object to handle response operations
     * @return returns ActionForward depending on which user is redirected to
     *         next page
     * @throws Exception
     *             if any error occurs while performing database operations, etc
     * @author 100006126 <br>
     * Created on: Oct 06, 2008
     */
    public ActionForward getEnquiryDetails(ActionMapping actionMapping,
                              ActionForm actionForm, 
                              HttpServletRequest request,
                              HttpServletResponse response) throws Exception {
    	
    	String sMethodName = QuotationConstants.QUOTATION_GETINQDET;
    	Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        
        UserDto oUserDto = null;
        String sOperationType = null;
        String sEnquiryCopied = null;
        
        int iRatingId = QuotationConstants.QUOTATION_LITERAL_ZERO;
        UserAction oUserAction =null;
        DaoFactory oDaoFactory =null;
        EnquiryDto oEnquiryDto = null;
        QuotationDao oQuotationDao =null;
        ArrayList alRatingList =null;
        KeyValueVo oKeyValueVo = null;
        RatingDto oTempRatingDto =null;
        EnquiryForm oEnquiryForm =null;
        
    	try{
    		oEnquiryForm = (EnquiryForm) actionForm;
    		if (oEnquiryForm == null)
    			oEnquiryForm = new EnquiryForm();
    		
    		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
    		
            sEnquiryCopied = CommonUtility.getStringParameter(request, QuotationConstants.QUOTATION_COPY); 
            if (sEnquiryCopied.trim().length() > QuotationConstants.QUOTATION_LITERAL_ZERO) 
            	request.setAttribute(QuotationConstants.QUOTATION_COPY, QuotationConstants.QUOTATION_STRING_Y); 
            
    		sOperationType = CommonUtility.getStringParameter(request, QuotationConstants.QUOTATION_TYPE); 
    		if (sOperationType.equalsIgnoreCase(""))
    			sOperationType = oEnquiryForm.getOperationType();
    		
    		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "sOperationType == "+sOperationType);
    		/*
             * Ssetting user's information in request, using the header information
             * received through request
             */
            setRolesToRequest(request);
            LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "Roles set to request.");
            
            oEnquiryForm.setEnquiryId(CommonUtility.getIntParameter(request, QuotationConstants.QUOTATION_REQUEST_ENQUIRYID));
            
            if (oEnquiryForm.getEnquiryId() < QuotationConstants.QUOTATION_LITERAL_ONE){
            	
            	 /* Setting error details to request to display the same in Exception page */
                request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_CRET_ROWS);
                
                /* Forwarding request to Error page */
                return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);            	
            }
            
            /* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Instantiating the UserAction object */
            oUserAction = new UserAction();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
    		
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            
            /* Retrieving the logged-in user information */
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            
            oEnquiryDto = new EnquiryDto();
            
            PropertyUtils.copyProperties(oEnquiryDto, oEnquiryForm);          
            oQuotationDao = oDaoFactory.getQuotationDao();
            
            oEnquiryDto = oQuotationDao.getEnquiryDetails(conn, oEnquiryDto, sOperationType);           
            PropertyUtils.copyProperties(oEnquiryForm, oEnquiryDto);
            
            /*
             * build the rating error message - submit button has to be submitted only when this message is empty
             * as this validation has to be done for both edit and view - the below method call has been placed outside the edit condition
             */ 
            
            oEnquiryForm = buildRatingErrorMessage(oEnquiryForm); 
           
           
            //get the attchment details
            request.setAttribute(QuotationConstants.QUOTATION_ATTACHLIST, AttachmentUtility.getAttachmentDetails(oEnquiryDto)); 
            request.setAttribute(QuotationConstants.QUOTATION_APPID, ""+QuotationConstants.QUOTATION_ATTACHMENT_APPLICATIONID);
           
            saveToken(request);

            if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_EDIT.toUpperCase()))
            {
            	
            	oEnquiryForm.setEnquiryOperationType(QuotationConstants.QUOTATION_FORWARD_UPDATE.toUpperCase());
            	oEnquiryForm.setDispatch(QuotationConstants.QUOTATION_EDIT);  
            	oEnquiryForm.setReturnstatement(QuotationConstants.QUOTATION_EDIT);
            	
            	iRatingId = CommonUtility.getIntParameter(request, QuotationConstants.QUOTATION_REQUEST_RATINGID);
            	
            	oEnquiryDto = oQuotationDao.loadLookUpValues(conn, oEnquiryDto);
	            PropertyUtils.copyProperties(oEnquiryForm, oEnquiryDto);
	            
	            oEnquiryForm = loadLookUpValues(conn, oEnquiryForm);
	            
	            if (CommonUtility.getStringParameter(request, QuotationConstants.QUOTATION_REQUEST_RATINGID) != null 
	            			&& CommonUtility.getStringParameter(request, QuotationConstants.QUOTATION_REQUEST_RATINGID).trim().length() > QuotationConstants.QUOTATION_LITERAL_ZERO) 
	            {	            	
	            	
	            	
	            	if (iRatingId == QuotationConstants.QUOTATION_LITERAL_ZERO) 
	            	{        		
	            		// this block will be executed when all the ratings are deleted and when edit button is clicked from draft view page
	            		iRatingId = QuotationConstants.QUOTATION_LITERAL_ONE;       		
	            		alRatingList = new ArrayList();
	            		
	            		oKeyValueVo = new KeyValueVo();
	        			oKeyValueVo.setKey("");
	        			oKeyValueVo.setValue(QuotationConstants.QUOTATION_RATING1); 
	        			oKeyValueVo.setValue2(QuotationConstants.QUOTATION_STRING_N);		
	        			alRatingList.add(oKeyValueVo);
	        		
	        			oEnquiryForm.setRatingList(alRatingList);
	        			
	        			oEnquiryForm.setRevisionNumber(QuotationConstants.QUOTATION_LITERAL_ONE); 
	        	        oEnquiryForm.setRatingNo(QuotationConstants.QUOTATION_LITERAL_ONE); 
	        	        oEnquiryForm.setRatingOperationType(QuotationConstants.QUOTATION_INSERT); 
	            	}else{
	            		oEnquiryForm.setRatingOperationId(""+iRatingId);
	            		oEnquiryForm.setCurrentRating(""+iRatingId);
	            	}
	            }else{
	            	oEnquiryForm.setCurrentRating(oEnquiryForm.getRatingOperationId());
	            }
	            
				if (oEnquiryForm.getRatingOperationId().trim().length() > QuotationConstants.QUOTATION_LITERAL_ZERO) 
				{						
					
					oTempRatingDto = new RatingDto();
					oTempRatingDto.setRatingId((Integer.parseInt(oEnquiryForm.getRatingOperationId())));
					oTempRatingDto = oQuotationDao.getRatingDetails(conn, oTempRatingDto);
					PropertyUtils.copyProperties(oEnquiryForm, oTempRatingDto);
					oEnquiryForm.setRatingOperationType(QuotationConstants.QUOTATION_FORWARD_UPDATE.toUpperCase());						
					oEnquiryForm.setRatingNo(oTempRatingDto.getRatingNo());	
					oEnquiryForm.setRatingTitle(QuotationConstants.QUOTATION_RATING+oTempRatingDto.getRatingNo());
				}
				
				oEnquiryForm = checkForNextRating(oEnquiryForm);
				
				//  Check for maxRatings
    			oEnquiryForm = checkMaxRatingLimit(oEnquiryForm);
    			
				oEnquiryForm.setRatingOperationId("");
				oEnquiryForm.setCreatedBy(oEnquiryForm.getCreatedBy());
	            oEnquiryForm.setCreatedBySSO(oEnquiryForm.getCreatedBySSO());
	            oEnquiryForm.setCreatedDate(oEnquiryForm.getCreatedDate());	
	            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_VIEW);
            	
            }
            
            
            else  if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_RETURNFROMDM))
            {
            	
            	oEnquiryForm.setEnquiryOperationType(QuotationConstants.QUOTATION_FORWARD_UPDATE.toUpperCase());
            	oEnquiryForm.setDispatch(QuotationConstants.QUOTATION_EDIT); 
            	oEnquiryForm.setReturnstatement(QuotationConstants.QUOTATION_RETURNDM);
            	oEnquiryForm.setReturnstatus(QuotationConstants.QUOTATION_RET_REPSM);
            	
            	
            	iRatingId = CommonUtility.getIntParameter(request, QuotationConstants.QUOTATION_REQUEST_RATINGID);
            	
            	oEnquiryDto = oQuotationDao.loadLookUpValues(conn, oEnquiryDto);
	            PropertyUtils.copyProperties(oEnquiryForm, oEnquiryDto);
	            

	            
	            oEnquiryForm = loadLookUpValues(conn, oEnquiryForm);
	            
	            if (CommonUtility.getStringParameter(request, QuotationConstants.QUOTATION_REQUEST_RATINGID) != null 
	            			&& CommonUtility.getStringParameter(request, QuotationConstants.QUOTATION_REQUEST_RATINGID).trim().length() >QuotationConstants.QUOTATION_LITERAL_ZERO) 
	            {	            	
	            	
	            	
	            	if (iRatingId == QuotationConstants.QUOTATION_LITERAL_ZERO)
	            	{        		
	            		// this block will be executed when all the ratings are deleted and when edit button is clicked from draft view page
	            		iRatingId = QuotationConstants.QUOTATION_LITERAL_ONE;       		
	            		alRatingList = new ArrayList();
	            		
	            		oKeyValueVo = new KeyValueVo();
	        			oKeyValueVo.setKey("");
	        			oKeyValueVo.setValue(QuotationConstants.QUOTATION_RATING1); 
	        			oKeyValueVo.setValue2(QuotationConstants.QUOTATION_STRING_N);		
	        			alRatingList.add(oKeyValueVo);
	        		
	        			oEnquiryForm.setRatingList(alRatingList);
	        			
	        			oEnquiryForm.setRevisionNumber(QuotationConstants.QUOTATION_LITERAL_ONE); 
	        	        oEnquiryForm.setRatingNo(QuotationConstants.QUOTATION_LITERAL_ONE); 
	        	        oEnquiryForm.setRatingOperationType(QuotationConstants.QUOTATION_INSERT);
	            	}else{
	            		oEnquiryForm.setRatingOperationId(""+iRatingId);
	            		oEnquiryForm.setCurrentRating(""+iRatingId);
	            	}
	            }else{
	            	oEnquiryForm.setCurrentRating(oEnquiryForm.getRatingOperationId());
	            }
	            
				if (oEnquiryForm.getRatingOperationId().trim().length() > QuotationConstants.QUOTATION_LITERAL_ZERO) 
				{						
					
					oTempRatingDto = new RatingDto();
					oTempRatingDto.setRatingId((Integer.parseInt(oEnquiryForm.getRatingOperationId())));
					oTempRatingDto = oQuotationDao.getRatingDetails(conn, oTempRatingDto);
					PropertyUtils.copyProperties(oEnquiryForm, oTempRatingDto);
					oEnquiryForm.setRatingOperationType(QuotationConstants.QUOTATION_FORWARD_UPDATE.toUpperCase());						
					oEnquiryForm.setRatingNo(oTempRatingDto.getRatingNo());	
					oEnquiryForm.setRatingTitle(QuotationConstants.QUOTATION_RATING+oTempRatingDto.getRatingNo()); 
				}
				
				oEnquiryForm = checkForNextRating(oEnquiryForm);
				
				//  Check for maxRatings
    			oEnquiryForm = checkMaxRatingLimit(oEnquiryForm);
    			
    			
    			
				oEnquiryForm.setRatingOperationId("");
				oEnquiryForm.setCreatedBy(oEnquiryForm.getCreatedBy());
	            oEnquiryForm.setCreatedBySSO(oEnquiryForm.getCreatedBySSO());
	            oEnquiryForm.setCreatedDate(oEnquiryForm.getCreatedDate());	
	            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_VIEW);
            	
            }

    	}catch(Exception e) {
            /*
             * Logging any exception that raised during this activity in log files using Log4j
             */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "home", ExceptionUtility.getStackTraceAsString(e));
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        } finally {
            /* Releasing/closing the connection object */
            if ( conn!=null )
                oDBUtility.releaseResources(conn);
        }
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
    	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_VIEWENQUIRY);
    }
    
    
    /**
     * This method is used to delete the rating object
     * 
     * @param actionMapping
     *            ActionMapping object to redirect the user to next page
     *            depending on the activity
     * @param actionForm
     *            ActionForm object to handle the form elements
     * @param request
     *            HttpServletRequest object to handle request operations
     * @param response
     *            HttpServletResponse object to handle response operations
     * @return returns ActionForward depending on which user is redirected to
     *         next page
     * @throws Exception
     *             if any error occurs while performing database operations, etc
     * @author 100006126 <br>
     * Created on: Oct 06, 2008
     */
    public ActionForward deleteRating(ActionMapping actionMapping,
                              ActionForm actionForm, 
                              HttpServletRequest request,
                              HttpServletResponse response) throws Exception {
    	
    	String sMethodName = QuotationConstants.QUOTATION_DELRATING;
    	Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        
        boolean isRatingDeleted = QuotationConstants.QUOTATION_FALSE;
    	int iEnquiryId = QuotationConstants.QUOTATION_LITERAL_ZERO;
		int iRatingId = QuotationConstants.QUOTATION_LITERAL_ZERO;
		UserAction oUserAction = null; //Instantiating the UserAction object
		DaoFactory oDaoFactory =null; //Creating an instance of of DaoFactory & retrieving UserDao object
		QuotationDao oQuotationDao =null;
		RatingDto oRatingDto =null;
		EnquiryForm oEnquiryForm =null;
       
    	try{
    		
    		oEnquiryForm = (EnquiryForm) actionForm;
    		if (oEnquiryForm == null)
    			oEnquiryForm = new EnquiryForm();
    		
    		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
    		
    		iEnquiryId = CommonUtility.getIntParameter(request, QuotationConstants.QUOTATION_REQUEST_ENQUIRYID);
    		iRatingId = CommonUtility.getIntParameter(request, QuotationConstants.QUOTATION_REQUEST_RATINGID);
    		
    		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "iEnquiryId == "+iEnquiryId);
    		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "iRatingId == "+iRatingId);
    		
    		/*
             * Ssetting user's information in request, using the header information
             * received through request
             */
            setRolesToRequest(request);
            LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "Roles set to request.");            
            
            
            /* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Instantiating the UserAction object */
            oUserAction = new UserAction();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
    		
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();  
            
            oQuotationDao = oDaoFactory.getQuotationDao();
            
            if (iRatingId > QuotationConstants.QUOTATION_LITERAL_ZERO && iEnquiryId > QuotationConstants.QUOTATION_LITERAL_ZERO) 
            {
            	oRatingDto = new RatingDto();
            	oRatingDto.setRatingId(iRatingId);
            	
            	isRatingDeleted = oQuotationDao.deleteRating(conn, oRatingDto);
            	
            	if (! isRatingDeleted){

            		/* Setting error details to request to display the same in Exception page */
                    request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_RAT_NOTDEL);
                    
                    /* Forwarding request to Error page */
                    return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);  
            	}
            }else{
            	
            	 /* Setting error details to request to display the same in Exception page */
                request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_RAT_NOTDEL);
                
                /* Forwarding request to Error page */
                return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);            	
            }
    	}catch(Exception e) {
            /*
             * Logging any exception that raised during this activity in log files using Log4j
             */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "home", ExceptionUtility.getStackTraceAsString(e));
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        } finally {
            /* Releasing/closing the connection object */
            if ( conn!=null )
                oDBUtility.releaseResources(conn);
        }
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
        
        return new ActionForward(QuotationConstants.QUOTATION_GET_ENQDET_URL+iEnquiryId); 
    }
    
    /* (non-Javadoc)
     * @see in.com.rbc.quotation.enquiry.form.EnquiryForm#loadLookUpValues(java.sql.Connection, in.com.rbc.quotation.enquiry.form.EnquiryForm)
     * This method is used for loading the look up values
     */
    private EnquiryForm loadLookUpValues(Connection conn, EnquiryForm oEnquiryForm) throws Exception{
    	
    	
    	/* Creating an instance of of DaoFactory & retrieving UserDao object */
        DaoFactory oDaoFactory = new DaoFactory();
		MasterDao oMasterDao = oDaoFactory.getMasterDao();
		QuotationDao oQuotationDao = oDaoFactory.getQuotationDao();
		
		
		oEnquiryForm.setHtltList(oQuotationDao.getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_HTLT)); 
		oEnquiryForm.setPoleList(oQuotationDao.getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_POLE));		
		oEnquiryForm.setScsrList(oQuotationDao.getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_TYPE)); 
		oEnquiryForm.setDutyList(oQuotationDao.getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_DUTY)); 
		oEnquiryForm.setDegproList(oQuotationDao.getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_DEGPRO)); 
		oEnquiryForm.setTermBoxList(oQuotationDao.getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_TERMBOX)); 
		oEnquiryForm.setInsulationList(oQuotationDao.getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_INSULATIONCLASS)); 
		oEnquiryForm.setMountingList(oQuotationDao.getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_MOUNTING)); 
		oEnquiryForm.setShaftList(oQuotationDao.getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_SHAFTEXT)); 
		oEnquiryForm.setEnclosureList(oQuotationDao.getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_ENCLOSURE)); 
		oEnquiryForm.setRotDirectionList(oQuotationDao.getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_ROTDIRECT)); 
		oEnquiryForm.setApplicationList(oQuotationDao.getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_APPL)); 
		oEnquiryForm.setAlFrequencyList(oQuotationDao.getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_FREQUENCY));	
		oEnquiryForm.setAlTempRaiseList(oQuotationDao.getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_TEMPRISE)); 
		oEnquiryForm.setAlAmbienceList(oQuotationDao.getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_AMBIENCE));
	    oEnquiryForm.setAlnoiseLevelList(oQuotationDao.getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_NOISEL));

		
	
    	return oEnquiryForm;
    }
    
    /* (non-Javadoc)
     * @see in.com.rbc.quotation.enquiry.form.EnquiryForm#buildRatingErrorMessage(in.com.rbc.quotation.enquiry.form.EnquiryForm)
     * This method is used to generate the error message - an enquiry will be submitted only when this message is empty ( Save as draft and Save & add new rating cases only)
     */
    private EnquiryForm buildRatingErrorMessage (EnquiryForm oEnquiryForm){
    	//  check if all the ratings are validated or not and build the appropriate error message
    	StringBuffer sbValidationMessage =null;
    	int iCount = QuotationConstants.QUOTATION_LITERAL_ONE;	
    	KeyValueVo oKeyValueVo =null;

    	
		if (oEnquiryForm.getRatingList().size() > QuotationConstants.QUOTATION_LITERAL_ZERO) 
		{
			sbValidationMessage = new StringBuffer();
			/*
			 * This varible is for tracking the rating count mentionded in the title - Rating "1", Rating "2" 
			 */
						for (int i=0; i<oEnquiryForm.getRatingList().size(); i++){
				oKeyValueVo = (KeyValueVo) oEnquiryForm.getRatingList().get(i);
				if (oKeyValueVo != null){
					//Do not add the error message for the rating that is currently in edit mode
					if ((! oEnquiryForm.getRatingOperationId().trim().equalsIgnoreCase(oKeyValueVo.getKey()))){

						if (! oKeyValueVo.getValue2().equalsIgnoreCase(QuotationConstants.QUOTATION_STRING_Y)) 
						{
							sbValidationMessage.append(QuotationConstants.QUOTATION_RATING+iCount);  
							sbValidationMessage.append(QuotationConstants.QUOTATION_STRING_COMMA); 
							iCount++;
						}else{
							iCount++;
						}
					}else{
						iCount++;
					}
				}
			}
			if (sbValidationMessage.toString().trim().endsWith(QuotationConstants.QUOTATION_STRING_COMMA))       	
				oEnquiryForm.setRatingsValidationMessage(sbValidationMessage.toString().substring(0, sbValidationMessage.toString().lastIndexOf(QuotationConstants.QUOTATION_STRING_COMMA)));
			else
				oEnquiryForm.setRatingsValidationMessage(sbValidationMessage.toString());	
			


		}
		return oEnquiryForm;
    }
    
    /* (non-Javadoc)
     * @see in.com.rbc.quotation.enquiry.form.EnquiryForm#checkMaxRatingLimit(in.com.rbc.quotation.enquiry.form.EnquiryForm)
     * This method checks the max ratings for each enquiry
     */
    private EnquiryForm checkMaxRatingLimit (EnquiryForm oEnquiryForm){
    	
    	int iMaxCount = QuotationConstants.QUOTATION_LITERAL_ZERO;
    	
    	if (PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_PROPERTYFILENAME, 
    				QuotationConstants.QUOTATION_MAXIMUMRATINGSCOUNT) != null){
    		iMaxCount = Integer.parseInt(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_PROPERTYFILENAME, 
    				QuotationConstants.QUOTATION_MAXIMUMRATINGSCOUNT));
    		
    		
    	}    	
    	if (iMaxCount == oEnquiryForm.getRatingList().size()){
    		oEnquiryForm.setIsMaxRating(QuotationConstants.QUOTATION_STRING_TRUE); 
    	}else{
    		oEnquiryForm.setIsMaxRating(QuotationConstants.QUOTATION_STRING_FALSE); 
    	}    	
    	return oEnquiryForm;
    }
    
    
    /* (non-Javadoc)
     * @see in.com.rbc.quotation.enquiry.form.EnquiryForm#checkMaxRatingLimit(in.com.rbc.quotation.enquiry.form.EnquiryForm)
     * This method checks if the next rating exists or not - will be used for "Save & Continue with next rating"
     */
    private EnquiryForm checkForNextRating(EnquiryForm oEnquiryForm){

    	int iCount = QuotationConstants.QUOTATION_LITERAL_ZERO; 
    	KeyValueVo oKeyValueVo =null;

    	if (oEnquiryForm.getRatingList().size() > QuotationConstants.QUOTATION_LITERAL_ZERO){    		
    		for (int i=0; i<oEnquiryForm.getRatingList().size(); i++){
    			iCount++;
    			
    			oKeyValueVo = (KeyValueVo) oEnquiryForm.getRatingList().get(i);

    			if (oKeyValueVo != null && oEnquiryForm.getCurrentRating().equalsIgnoreCase(oKeyValueVo.getKey())){
    				
    				
    				if (iCount == oEnquiryForm.getRatingList().size()){
    					oEnquiryForm.setIsLast(QuotationConstants.QUOTATION_STRING_TRUE); 

    					oEnquiryForm.setNextRatingId(oKeyValueVo.getKey());
        				
    				}else{
    					KeyValueVo oKeyValueVo1 = (KeyValueVo) oEnquiryForm.getRatingList().get(i+1);
    					if (oKeyValueVo1 != null){
    						oEnquiryForm.setNextRatingId(oKeyValueVo1.getKey());
    					}
        				

    				}
    			}	//If Loop Ending
    		}//For Loop Ending   
    		
    		
    	}
    	
    	return oEnquiryForm;
    }
    

    /**
     * This method is used to copy the Enquiry.
     * 
     * @param actionMapping
     *            ActionMapping object to redirect the user to next page
     *            depending on the activity
     * @param actionForm
     *            ActionForm object to handle the form elements
     * @param request
     *            HttpServletRequest object to handle request operations
     * @param response
     *            HttpServletResponse object to handle response operations
     * @return returns ActionForward depending on which user is redirected to
     *         next page
     * @throws Exception
     *             if any error occurs while performing database operations, etc
     * @author 100006126 <br>
     * Created on: Sep 30, 2008
     */
    public ActionForward copyEnquiry(ActionMapping actionMapping,
                              ActionForm actionForm, 
                              HttpServletRequest request,
                              HttpServletResponse response) throws Exception {
    	
    	String sMethodName = QuotationConstants.QUOTATION_COPYINQ;
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        EnquiryDto oEnquiryDto = null;
    	Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        
        UserDto oUserDto = null;
        UserAction oUserAction =null; //Instantiating the UserAction object
        DaoFactory oDaoFactory = null; //Creating an instance of of DaoFactory & retrieving UserDao object
        QuotationDao oQuotationDao =null;
    	EnquiryForm oEnquiryForm = (EnquiryForm)actionForm;
		if (oEnquiryForm == null){
			oEnquiryForm = new EnquiryForm();
		}
    	try {
    		if(isTokenValid(request)) {
        		resetToken(request);
        		
        		/*
                 * Ssetting user's information in request, using the header information
                 * received through request
                 */
                setRolesToRequest(request);
                LoggerUtility.log("INFO", sClassName, sMethodName, "Roles set to request.");
                
                /* Instantiating the DBUtility object */
                oDBUtility = new DBUtility();
                /* Instantiating the UserAction object */
                oUserAction = new UserAction();
                /* Creating an instance of of DaoFactory & retrieving UserDao object */
                oDaoFactory = new DaoFactory();
        		
                /* Retrieving the database connection using DBUtility.getDBConnection method */
                conn = oDBUtility.getDBConnection();
                
                /* Retrieving the logged-in user information */
                oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
                
                oQuotationDao = oDaoFactory.getQuotationDao();
                oEnquiryDto = new EnquiryDto();
                PropertyUtils.copyProperties(oEnquiryDto, oEnquiryForm);
                oEnquiryDto = oQuotationDao.copyEnquiry(conn,oEnquiryDto,oUserDto);
                request.setAttribute(QuotationConstants.QUOTATION_ENQUIRYDTO,oEnquiryDto); 
                request.setAttribute(QuotationConstants.QUOTATION_USERDTO, oUserDto); 
                
                if (oEnquiryDto == null){
                	/* Setting error details to request to display the same in Exception page */
                    request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_PROB_COPYREC);
                    
                    /* Forwarding request to Error page */
                    return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
                }
        	} else {
        		   request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_INVALIDTOKENMESSAGE);
                   
                   /* Forwarding request to Error page */
                   return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        	}
    	} catch(Exception e) {
            /*
             * Logging any exception that raised during this activity in log files using Log4j
             */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "home", ExceptionUtility.getStackTraceAsString(e));
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        } finally {
            /* Releasing/closing the connection object */
            if ( conn!=null )
                oDBUtility.releaseResources(conn);
        }
    	
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        return new ActionForward(QuotationConstants.QUOTATION_GET_ENQDETCOPY_URL+oEnquiryDto.getEnquiryId()); 
    }
    
    /**
     * This method is used to deletes the Enquiry.
     * 
     * @param actionMapping
     *            ActionMapping object to redirect the user to next page
     *            depending on the activity
     * @param actionForm
     *            ActionForm object to handle the form elements
     * @param request
     *            HttpServletRequest object to handle request operations
     * @param response
     *            HttpServletResponse object to handle response operations
     * @return returns ActionForward depending on which user is redirected to
     *         next page
     * @throws Exception
     *             if any error occurs while performing database operations, etc
     * @author 100006126 <br>
     * Created on: Sep 30, 2008
     */
    public ActionForward deleteEnquiry(ActionMapping actionMapping,
                              ActionForm actionForm, 
                              HttpServletRequest request,
                              HttpServletResponse response) throws Exception {
    	
    	String sMethodName = QuotationConstants.QUOTATION_DELINQ;
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        EnquiryDto oEnquiryDto = null;
    	Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        
        UserDto oUserDto = null;
        UserAction oUserAction =null;
        DaoFactory oDaoFactory =null;
        QuotationDao oQuotationDao =null;
        boolean isDeleted =QuotationConstants.QUOTATION_FALSE;
        String sMsg = null;
    	EnquiryForm oEnquiryForm = (EnquiryForm)actionForm;
		if (oEnquiryForm == null){
			oEnquiryForm = new EnquiryForm();
		}
    	try {
    		
    		if(isTokenValid(request)) 
        	{
        		resetToken(request);

    		/*
             * Ssetting user's information in request, using the header information
             * received through request
             */
            setRolesToRequest(request);
            LoggerUtility.log("INFO", sClassName, sMethodName, "Roles set to request.");
            
            /* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Instantiating the UserAction object */
            oUserAction = new UserAction();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
    		
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            
            /* Retrieving the logged-in user information */
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            
            oQuotationDao = oDaoFactory.getQuotationDao();
            oEnquiryDto = new EnquiryDto();
            PropertyUtils.copyProperties(oEnquiryDto, oEnquiryForm);
            isDeleted = oQuotationDao.deleteEnquiry(conn,oEnquiryDto);
            if (isDeleted){
            	//delete the attachments
            	isDeleted = AttachmentUtility.deleteAttachment(""+oEnquiryDto.getEnquiryId());
            	
            	sMsg = QuotationConstants.QUOTATION_DELINQSUC;
            	request.setAttribute(QuotationConstants.QUOTATION_HEADINGMESSAGE,QuotationConstants.QUOTATION_DELCONF);
            	request.setAttribute(QuotationConstants.QUOTATION_SUCCESSMESSAGE,sMsg);
            	request.setAttribute(QuotationConstants.QUOTATION_LINKSMESSAGE,QuotationConstants.QUOTATION_VIEWSR_ENQURL); 
            	request.setAttribute(QuotationConstants.QUOTATION_USERDTO, oUserDto); 
            }
        	
        	}else
        	{
        		   request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE,QuotationConstants.QUOTATION_INVALIDTOKENMESSAGE);
                   
                   /* Forwarding request to Error page */
                   return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
               
        		
        	}} catch(Exception e) {
            /*
             * Logging any exception that raised during this activity in log files using Log4j
             */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "home", ExceptionUtility.getStackTraceAsString(e));
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        } finally {
            /* Releasing/closing the connection object */
            if ( conn!=null )
                oDBUtility.releaseResources(conn);
        }
    	
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
    	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);
    }
    
    
    /**
	 * This method retrieves the list for the selected customer name 
	 * @param actionMapping ActionMapping object to redirect the user to next page depending on the activity
     * @param actionForm ActionForm object to handle the form elements
     * @param request HttpServletRequest object to handle request operations
     * @param response HttpServletResponse object to handle response operations
     * @return returns ActionForward depending on which user is redirected to next page
     * @throws Exception if any error occurs while performing database operations, etc
     * @author 100006126
     *  Date: Oct 8, 2008
	 */
	public ActionForward loadCustomerLookUp(ActionMapping actionMapping,
			ActionForm actionForm,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception {	
		
		LoggerUtility.log("INFO", this.getClass().getName(), "loadCustomerLookUp", "loadCustomerLookUp Start");
		
		// Object of DBUtility class to handle DB connection objects
		DBUtility oDBUtility = new DBUtility();
		
		// Connection object to store the database connection
		Connection conn = null;
		ArrayList arlCustomerLocationOptionsList =null;
		StringBuffer sbBuiltXML =null;
		/* String that holds the targetObjects values that we get from the request */
		String sTargetObjects = null;
		String sTargetName = null;
		DaoFactory oDaoFactory =null;//Creating an instance of of DaoFactory & retrieving EmployeeDao object
		CustomerDao oCustomerDao =null;
		CustomerDto oCustomerDto =null;
		String sDisplayContactPerson =null;
		
		try {
    		// setting content type to create xml  
    		response.setContentType("text/xml;charset=utf-8");
    		PrintWriter out = response.getWriter();
    		
    		/* Object of ArrayList to hold the retrieved list of options*/
    		arlCustomerLocationOptionsList = new ArrayList();
    		
    		/* StringBuffer to hold the XML */
    		sbBuiltXML = new StringBuffer();
    		
    		
    		/* Creating an instance of of DaoFactory & retrieving EmployeeDao object */
            oDaoFactory = new DaoFactory();    
    		
    		/* Instantiating the DBUtility object */
    		oDBUtility = new DBUtility();
    		
    		/* Retrieving the database connection using DBUtility.getDBConnection method */
    		conn = oDBUtility.getDBConnection();
    		
    		// get the customer objects
    		oCustomerDao = oDaoFactory.getCustomerDao();
    		oCustomerDto = new CustomerDto();
            
    		sDisplayContactPerson = CommonUtility.getStringParameter(request, QuotationConstants.QUOTATION_DISPCPERS);
    		
    		if (CommonUtility.getStringParameter(request, QuotationConstants.QUOTATION_ADMINLOOKUP_SELECTEDOPTION).trim().length() > QuotationConstants.QUOTATION_LITERAL_ZERO){
    			oCustomerDto.setCustomer(CommonUtility.getStringParameter(request, QuotationConstants.QUOTATION_ADMINLOOKUP_SELECTEDOPTION));   			
    			   			
    			arlCustomerLocationOptionsList = oCustomerDao.getLocationsByCustomerName(conn, oCustomerDto, sDisplayContactPerson);		
    		}    
    		
    			//  setting the status code 200 
	            response.setStatus(200, "Get Options (XML) Successful");
	            
	            //	creating xml
	            sbBuiltXML.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>"); 
	            sbBuiltXML.append("<result>");
	            for(int i = 0; i < arlCustomerLocationOptionsList.size(); i++) {
	            	KeyValueVo oKeyValueVo = (KeyValueVo)arlCustomerLocationOptionsList.get(i);
	                sbBuiltXML.append("<options-location><opname>"
	                + URLEncoder.encode(oKeyValueVo.getValue()+"&&"+oKeyValueVo.getKey()+"&&"+oKeyValueVo.getValue2(), "UTF-8")
	                + "</opname></options-location>\r\n");
	                
	             }
	             sbBuiltXML.append("<params>");
	             sbBuiltXML.append("<target>location</target>");
	             sbBuiltXML.append("<objType>comboBox</objType>");
	             sbBuiltXML.append("</params>");
	             sbBuiltXML.append("</result>");	             
	            
	             out.print(sbBuiltXML);         
    		  		
		}catch(Exception e) {
			/*
			 * Logging any exception that raised during this activity in 
			 * log files using Log4j
			 */
			LoggerUtility.log("DEBUG", this.getClass().getName(), "loadCustomerLookUp", 
					ExceptionUtility.getStackTraceAsString(e));
			
			/*
			 * Setting error details to request to show in the View.
			 */
			request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, 
					ExceptionUtility.getStackTraceAsString(e));
			
			/*
			 * Forwarding request to Error page.
			 */
			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
		} finally {
            /* Releasing the resources */
			if (oDBUtility != null && conn != null){
				oDBUtility.releaseResources(conn);
			}
        }		
		return null;
	}
	
	
	/**
     * This method is used for setting up the offer data page.
     * 
     * @param actionMapping
     *            ActionMapping object to redirect the user to next page
     *            depending on the activity
     * @param actionForm
     *            ActionForm object to handle the form elements
     * @param request
     *            HttpServletRequest object to handle request operations
     * @param response
     *            HttpServletResponse object to handle response operations
     * @return returns ActionForward depending on which user is redirected to
     *         next page
     * @throws Exception
     *             if any error occurs while performing database operations, etc
     * @author 100006126 <br>
     * Created on: Oct 8, 2008
     * 
     */
    public ActionForward viewOfferData(ActionMapping actionMapping,
                              ActionForm actionForm, 
                              HttpServletRequest request,
                              HttpServletResponse response) throws Exception {
    	
    	String sMethodName =QuotationConstants.QUOTATION_VIEWOFF; 
    	String sClassName = this.getClass().getName();
    	
    	Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        
        UserDto oUserDto = null;
        RatingDto oRatingDto = null;
        
        String sOperationType = null;
        String sOperationType2 = null;

        
        WorkflowUtility oWorkflowUtility = new WorkflowUtility();
        WorkflowDto oWorkflowDto = null;
        UserAction oUserAction =null; //Instantiating the UserAction object
        DaoFactory oDaoFactory =null; //Creating an instance of of DaoFactory & retrieving UserDao object
        EnquiryDto oEnquiryDto = null;
        QuotationDao oQuotationDao = null;
        String sMsg =null;
        String sLinkMsg1=null;
        String sLinkMsg2=null;
        String sLinkMsg=null;
        TechnicalOfferDto oTechnicalOfferDto =null;
        boolean isUpdated =QuotationConstants.QUOTATION_FALSE;
        int iHidRowIndex =QuotationConstants.QUOTATION_LITERAL_ZERO;
        int iRatingId = QuotationConstants.QUOTATION_LITERAL_ZERO;
        String sDeptAndAuth = null;
        ArrayList alRatingList =null;
        String sCurrentRateId=null;
        String sFullName = null;
        String[] saSections=null;
        StringBuffer oAddOnTypeNames = null;
    	AddOnDto oAddOnDto = null;
    	TeamMemberDto  oTeamMemberDto = null;
    	List arlSalesManagerList=null;
    	KeyValueVo okeyvaluevo=null;
    	EnquiryForm oEnquiryForm =null;
    	int iWorkflowExecuteStatus =QuotationConstants.QUOTATION_LITERAL_ZERO;
    	boolean isDeletePreviousSatatus=false;

      
    	
    	try{
    		
    		oEnquiryForm = (EnquiryForm) actionForm;
    		if (oEnquiryForm == null)
    			oEnquiryForm = new EnquiryForm();
    		
    		
    		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
    		
    		sOperationType = oEnquiryForm.getOperationType();  		
    		
    		/*
             * Ssetting user's information in request, using the header information
             * received through request
             */
            setRolesToRequest(request);
            LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "Roles set to request.");
            
            
            
            /* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Instantiating the UserAction object */
            oUserAction = new UserAction();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
    		
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            
            /*
    		 * This method will be invoked for all the funtionalities there in offer data page  - start To end
    		 * Now the question is - when do we set the token?
    		 * When sOperationType == "" thats when offer data process starts - may be we can set the token there
    		 * this seems to work - check more and see if any issues comes up
    		 */
    		if (sOperationType.trim().length() == QuotationConstants.QUOTATION_LITERAL_ZERO) 
    		{
    			sOperationType = QuotationConstants.QUOTATION_VIEWOFFDATA;    			
    			saveToken(request);    			
    		}else{
    			//for other cases validate token
    			if (! isTokenValid(request)) {
	           	 /* Setting error details to request to display the same in Exception page */ 
	               request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_INVALIDTOKENMESSAGE);
	               /* Forwarding request to Error page */
	               return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
	           }      
    		}
            
            /* Retrieving the logged-in user information */
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            
            oEnquiryDto = new EnquiryDto();
            oQuotationDao = oDaoFactory.getQuotationDao();
            
            // if its reassign the initiate the work flow and the forward the request to respective page
            if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_REASSIGN)) 
            {      	 
            	
            	//reset the token
            	resetToken(request);
            	
            	 oWorkflowDto = new WorkflowDto();
                 
                 oWorkflowDto.setEnquiryId(oEnquiryForm.getEnquiryId());
                 oWorkflowDto.setEnquiryNo(oEnquiryForm.getEnquiryNumber());
                 oWorkflowDto.setLoginUserId(oUserDto.getUserId());
                 oWorkflowDto.setLoginUserName(oUserDto.getFullName());
                 oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
                 oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_REASSIGNTODE);
                 oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGDM);
                 oWorkflowDto.setAssignedTo(oEnquiryForm.getAssignToId());
            
                 oWorkflowDto.setRemarks(oEnquiryForm.getAssignRemarks());
                 oWorkflowDto.setDmToSmReturn(oEnquiryForm.getReturnToRemarks());
                 
                 LoggerUtility.log("INFO", sClassName, sMethodName, "3 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
                 iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
                
                 if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE ) 
                 {
                     LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Successfully");
                 } else  if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ZERO  )  
                 {
                     LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed - But email could not be sent. Please send email manually.");
                 } else {
                     LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Failed");
                 }
                 
                 LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Workflow process completed - - -");
                 
                  sMsg = QuotationConstants.QUOTATION_ASSIGNED_SUCESS+oEnquiryForm.getAssignToName();
                  sLinkMsg = QuotationConstants.QUOTATION_VALIDATEREQ_URL+oEnquiryForm.getEnquiryId()+QuotationConstants.QUOTATION_GRSYMBOL+QuotationConstants.QUOTATION_VIEWLINK;   
                 
                
                 request.setAttribute(QuotationConstants.QUOTATION_HEADINGMESSAGE,QuotationConstants.QUOTATION_REASSIG_SUCES);
                 request.setAttribute(QuotationConstants.QUOTATION_SUCCESSMESSAGE,sMsg);
                 request.setAttribute(QuotationConstants.QUOTATION_LINKSMESSAGE,sLinkMsg);
     			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);	
            }
            
            if (oEnquiryForm.getEnquiryId() < QuotationConstants.QUOTATION_LITERAL_ONE) 
            	oEnquiryForm.setEnquiryId(CommonUtility.getIntParameter(request, QuotationConstants.QUOTATION_REQUEST_ENQUIRYID));          
            
           
            if (oEnquiryForm.getEnquiryId() < QuotationConstants.QUOTATION_LITERAL_ONE) 
            {
            	
            	 /* Setting error details to request to display the same in Exception page */
                request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_CRET_ROWS); 
                
                /* Forwarding request to Error page */
                return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);            	
            }
        	
            if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_UPDRATING)) 
            {
            	// update the rating record with technical info
            	
            	oDBUtility.setAutoCommitFalse(conn);
            	oRatingDto = new RatingDto();
            	            	
            	if (oEnquiryForm.getRatingId() < QuotationConstants.QUOTATION_LITERAL_ONE) 
            	{
            		 /* Setting error details to request to display the same in Exception page */
                    request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_INVALID_RATINGID); 
                    
                    /* Forwarding request to Error page */
                    return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
            	}
            	
            	oRatingDto.setRatingId(oEnquiryForm.getRatingId());
            	oTechnicalOfferDto = new TechnicalOfferDto();
            	PropertyUtils.copyProperties(oTechnicalOfferDto, oEnquiryForm);         
            	PropertyUtils.copyProperties(oRatingDto, oEnquiryForm);
            	PropertyUtils.copyProperties(oEnquiryDto, oEnquiryForm);
            	
            
            	/* Retrieving the logged-in user information */
                oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
                
                if (oUserDto == null)
                	oUserDto = new UserDto();
                
                oTechnicalOfferDto.setCreatedBy(oUserDto.getUserId());
                oTechnicalOfferDto.setCreatedByName(oUserDto.getFullName());                
                
               
                isUpdated = oQuotationDao.updatingTechincalOfferData(conn, oEnquiryDto, oRatingDto, oTechnicalOfferDto);
                if (isUpdated){
                	oDBUtility.commit(conn);
                }
                if (! isUpdated){
                	oDBUtility.rollback(conn);
                	 /* Setting error details to request to display the same in Exception page */
                    request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_ERROR_UPDATING);
                    
                    /* Forwarding request to Error page */
                    return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
                }
                
                if ((isUpdated) && (! oEnquiryForm.getCommercialOfferType().equalsIgnoreCase(QuotationConstants.QUOTATION_STRING_P)) ) 
                {
                	
                	
                	
                	iHidRowIndex = CommonUtility.getIntParameter(request, QuotationConstants.QUOTATION_HIDEROWIND); 
                	
                	/*
    				 * Retrieves the add on details from the form and sets
    				 * in arraylist
    				 */
                	oRatingDto.setAddOnList(getAddOnDetails(iHidRowIndex, oEnquiryForm.getRatingId(), request));                	
                	
                	// first delete all existing records
                	isUpdated = oQuotationDao.deleteAllAddOnByRatingId(conn, oRatingDto.getRatingId());                
                	
                    if (isUpdated){
                    	oDBUtility.commit(conn);
                    }
                    if (! isUpdated){
                    	oDBUtility.rollback(conn);
                    	 /* Setting error details to request to display the same in Exception page */
                        request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_ERROR_DELETING);
                        
                        /* Forwarding request to Error page */
                        return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
                    }

                	if (isUpdated)
                		isUpdated = oQuotationDao.updateAddOn(conn, oRatingDto);
                	
                	
                    if (isUpdated){
                    	oDBUtility.commit(conn);
                    }

                }else if ((isUpdated) && (oEnquiryForm.getCommercialOfferType().equalsIgnoreCase(QuotationConstants.QUOTATION_STRING_P))){
                	// if offer type has been changed to "Precise" from "Detail" then delete all add-ons
                	

                	isUpdated = oQuotationDao.deleteAllAddOnByRatingId(conn, oRatingDto.getRatingId()); 
                	
                	
                }
                
                sOperationType = QuotationConstants.QUOTATION_VIEWOFFDATA;                
                sOperationType2=QuotationConstants.QUOTATION_VIEWOFFDATA2;
                
                
                if (oEnquiryForm.getDispatch().equalsIgnoreCase(QuotationConstants.QUOTATION_MANAGER)) 
                {
                	
                	// 	reset the token
                	resetToken(request);
                	
                	oWorkflowDto = new WorkflowDto();
                    
                    oWorkflowDto.setEnquiryId(oEnquiryForm.getEnquiryId());
                    oWorkflowDto.setEnquiryNo(oEnquiryForm.getEnquiryNumber());
                    oWorkflowDto.setLoginUserId(oUserDto.getUserId());
                    oWorkflowDto.setLoginUserName(oUserDto.getFullName());
                    oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
                    oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTODM); 
                    oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGDM); 
                    oWorkflowDto.setDmName(oEnquiryForm.getCreatedBy());
                    oWorkflowDto.setDmSsoid(oEnquiryForm.getCreatedBySSO());
                    oWorkflowDto.setDmNote(oEnquiryForm.getDmnote());
                    oWorkflowDto.setPageChecking(oEnquiryForm.getPagechecking());

                    oWorkflowDto.setDmToSmReturn(oEnquiryForm.getReturnToRemarks());

                    oWorkflowDto.setEquiryOperationType(oEnquiryForm.getEnquiryOperationType());
                    
                    //Delete Previous Status If It is SDM Based On Enquiry
                    isDeletePreviousSatatus=oWorkflowUtility.deletePrevStatusBasedonEnquiry(conn, oWorkflowDto);
                    
                    LoggerUtility.log("INFO", sClassName, sMethodName, "4 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
                    iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
                                        
                    if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE ){
                    	oDBUtility.commit(conn);
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Successfully");
                    } else  if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ZERO ){
                    	oDBUtility.commit(conn);
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed - But email could not be sent. Please send email manually.");                        
                    } else {
                    	oDBUtility.rollback(conn);
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Failed");
                    }
                    
                    LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Workflow process completed - - -");
                    
                    
                    sMsg = QuotationConstants.QUOTATION_OFFERDASUC;
                    sLinkMsg1 = QuotationConstants.QUOTATION_VIEWINQ_VAREQURL+oEnquiryForm.getEnquiryId()+QuotationConstants.QUOTATION_GRSYMBOL+QuotationConstants.QUOTATION_VIEWLINK; 
                    sLinkMsg2 = QuotationConstants.QUOTATION_VIEWSR_FVIEW_URL+QuotationConstants.QUOTATION_VIEWLINKS; 
                    
                    sLinkMsg = sLinkMsg1+QuotationConstants.QUOTATION_BREAK+sLinkMsg2;
                    request.setAttribute(QuotationConstants.QUOTATION_HEADINGMESSAGE,QuotationConstants.QUOTATION_SUB_SUCES);
                    request.setAttribute(QuotationConstants.QUOTATION_SUCCESSMESSAGE,sMsg);
                    request.setAttribute(QuotationConstants.QUOTATION_LINKSMESSAGE,sLinkMsg);
        			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);				
                }else if (oEnquiryForm.getDispatch().equalsIgnoreCase(QuotationConstants.QUOTATION_ESTIMATE)){
                	// reset the token
                	resetToken(request);
                	
                	oWorkflowDto = new WorkflowDto();
                	
                    
                    oWorkflowDto.setEnquiryId(oEnquiryForm.getEnquiryId());
                    oWorkflowDto.setEnquiryNo(oEnquiryForm.getEnquiryNumber());
                    oWorkflowDto.setLoginUserId(oUserDto.getUserId());
                    oWorkflowDto.setLoginUserName(oUserDto.getFullName());
                    oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
                   
                  //TODO Bipass the Flow From DM TO CE
                  /*  oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTOCE);
                    oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGDM); 
*/                  //TODO Bipass the Flow From DM TO RSM
                    oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTORSM);
				    oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGCE); 
                 
                    oWorkflowDto.setDmName(oEnquiryForm.getCreatedBy());
                    oWorkflowDto.setDmSsoid(oEnquiryForm.getCreatedBySSO());
                    oWorkflowDto.setDmNote(oEnquiryForm.getDmnote());
                    oWorkflowDto.setPageChecking(oEnquiryForm.getPagechecking());

                    oWorkflowDto.setDmToSmReturn(oEnquiryForm.getReturnToRemarks());

                    oWorkflowDto.setEquiryOperationType(oEnquiryForm.getEnquiryOperationType());
                    
                    

                    
                    LoggerUtility.log("INFO", sClassName, sMethodName, "5 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
                   
                    //Delete Previous Status If It is SCE Based On Enquiry
                    isDeletePreviousSatatus=oWorkflowUtility.deletePrevStatusBasedonEnquiry(conn, oWorkflowDto);

                    
                    iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
                    if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE){
                    	oDBUtility.commit(conn);
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Successfully");                      
                    } else  if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ZERO ){
                    	oDBUtility.commit(conn);
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed - But email could not be sent. Please send email manually.");
                    } else {
                    	oDBUtility.rollback(conn);
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Failed");
                    }
                    
                    LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Workflow process completed - - -");
                    
                    sMsg = QuotationConstants.QUOTATION_OFFERDASUC;
                    sLinkMsg1 = QuotationConstants.QUOTATION_VIEWINQ_VAREQURL+oEnquiryForm.getEnquiryId()+QuotationConstants.QUOTATION_GRSYMBOL+QuotationConstants.QUOTATION_VIEWLINK; 
                    sLinkMsg2 = QuotationConstants.QUOTATION_VIEWSR_FVIEW_URL+QuotationConstants.QUOTATION_VIEWLINKS; 
                    
                    sLinkMsg = sLinkMsg1+QuotationConstants.QUOTATION_BREAK+sLinkMsg2;
                    request.setAttribute(QuotationConstants.QUOTATION_HEADINGMESSAGE,QuotationConstants.QUOTATION_SUB_SUCES);
                    request.setAttribute(QuotationConstants.QUOTATION_SUCCESSMESSAGE,sMsg);
                    request.setAttribute(QuotationConstants.QUOTATION_LINKSMESSAGE,sLinkMsg);
        			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);	
                }            	
            }//Is updaterating Ending
          
           
            
            
            if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_VIEWOFFDATA))
            { 
            	
            	iRatingId = QuotationConstants.QUOTATION_LITERAL_ZERO;            	
            	sDeptAndAuth = QuotationConstants.QUOTATION_TECHNICAL_DEPT;
            	PropertyUtils.copyProperties(oEnquiryDto, oEnquiryForm);    
            	
            	// load the attachment list
                request.setAttribute(QuotationConstants.QUOTATION_ATTACHLIST, AttachmentUtility.getAttachmentDetails(oEnquiryDto)); 
              
                //load the dm uploaded attachmentlist 
                request.setAttribute(QuotationConstants.QUOTATION_DMATTACHLIST, AttachmentUtility.getDMUploadedAttachmentDetails(oEnquiryDto));
                
            	if (oEnquiryForm.getRatingId() > QuotationConstants.QUOTATION_LITERAL_ZERO) 
            		oEnquiryDto.setRatingOperationId(""+oEnquiryForm.getRatingId());
            
              if(sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_VIEWOFFDATA)) 
              {
            	oEnquiryDto = oQuotationDao.getEnquiryDetails(conn, oEnquiryDto, QuotationConstants.QUOTATION_UVIEWOFFDATA); 
              }
            	if(sOperationType2!=null && sOperationType2.equalsIgnoreCase(QuotationConstants.QUOTATION_VIEWOFFDATA2)) 
            	{
                	oEnquiryDto = oQuotationDao.getEnquiryDetails(conn, oEnquiryDto, ""); 

            	}
            	
            	oEnquiryDto = oQuotationDao.loadLookUpValues(conn, oEnquiryDto);           
            	PropertyUtils.copyProperties(oEnquiryForm, oEnquiryDto);
            	
            	

            	
            	
	            oEnquiryForm = loadLookUpValues(conn, oEnquiryForm);
	           
	             sFullName = oEnquiryForm.getCreatedBy();
	            saSections = sFullName.split(" ");
	           
	            for(String sPart:saSections){
	            	sDeptAndAuth= sDeptAndAuth+sPart.substring(0,1);
	            }
	                   
	            oEnquiryForm.setDeptAndAuth(sDeptAndAuth);
            	alRatingList = oEnquiryForm.getRatingList();
            	
            	
            	          
            	if (oEnquiryForm.getCurrentRating().trim().length() > QuotationConstants.QUOTATION_LITERAL_ZERO) 
            	{            
            		oRatingDto = (RatingDto) oEnquiryForm.getCurrentRatingObject().get(0);
        			

            		oEnquiryForm.setCurrentRating(""+oRatingDto.getRatingId());
            		oEnquiryForm.setRatingOperationId(""+oRatingDto.getRatingId());
            		
            		iRatingId = oRatingDto.getRatingId();            		
            		
            	}else{
            		
            		if (oEnquiryForm.getRatingObjects().size() > QuotationConstants.QUOTATION_LITERAL_ZERO){
            			oRatingDto = (RatingDto) oEnquiryForm.getRatingObjects().get(0);
            			
            			oEnquiryForm.setCurrentRating(""+oRatingDto.getRatingId());
            			iRatingId = oRatingDto.getRatingId();
            			oEnquiryForm.setRatingOperationId(""+oRatingDto.getRatingId());
            			
            		}
            	}  
            	if (oRatingDto ==  null)
	            	oRatingDto = new RatingDto();
            	
            	oQuotationDao.loadOfferPageLookUpValues(conn, oRatingDto);
            	
		        PropertyUtils.copyProperties(oEnquiryForm, oRatingDto);
		        
		        //get technical offer details 
		        oTechnicalOfferDto = oRatingDto.getTechnicalDto();
		        if (oTechnicalOfferDto != null){
		        	
		        	 PropertyUtils.copyProperties(oEnquiryForm, oTechnicalOfferDto);		        	 
		        	 if (oTechnicalOfferDto.getCreatedBy().trim().length() > QuotationConstants.QUOTATION_LITERAL_ZERO){
		        		 oEnquiryForm.setUpdateCreateBy(QuotationConstants.QUOTATION_STRING_FALSE); 
		        	 }
		        }		        
		       
  
		        //	get addon details
		       
		        
		        if (iRatingId > QuotationConstants.QUOTATION_LITERAL_ZERO)
		        	oEnquiryForm.setAddOnList(oQuotationDao.getAddOnList(conn, oRatingDto.getRatingId()));
		        
		        /*
		         * get all the addon-type names with some separator, this will be used
		         * for checking duplicate addon type values on the client side
		         */		        
		        if (oEnquiryForm.getAddOnList().size() > QuotationConstants.QUOTATION_LITERAL_ZERO){
		        	oAddOnTypeNames = new StringBuffer();
		        	oAddOnDto = null;
		        	
		        	for (int i=0; i<oEnquiryForm.getAddOnList().size(); i++){
		        		oAddOnDto = (AddOnDto) oEnquiryForm.getAddOnList().get(i);
		        		if (oAddOnDto != null)
		        			
		        			oAddOnTypeNames.append(oAddOnDto.getAddOnTypeText());
		        	}
		        	
		        }
		       //Concate All Rates To Show In Jsp Page
		        
		        
		        
		        oEnquiryForm = checkForNextRating(oEnquiryForm);

		        // now check the permissions - let it happen only once
		        if (oEnquiryForm.getEnquiryOperationType().trim().length() < QuotationConstants.QUOTATION_LITERAL_ONE) 
		        {
		        	oTeamMemberDto = new TeamMemberDto();
		        	oTeamMemberDto.setEnquiryId(oEnquiryForm.getEnquiryId());
		        	oTeamMemberDto = oQuotationDao.getTeamMemberDetails(conn, oTeamMemberDto);
		        	
		        	if (oUserDto.getUserId().equalsIgnoreCase(oTeamMemberDto.getDesignManager())){
		        		oEnquiryForm.setEnquiryOperationType(QuotationConstants.QUOTATION_MANAGER); 
		        	}else if (oUserDto.getUserId().equalsIgnoreCase(oTeamMemberDto.getDesignEngineer())){
		        		
		        		oEnquiryForm.setDesignEngineerName(oUserDto.getFullName());
		        		oEnquiryForm.setEnquiryOperationType(QuotationConstants.QUOTATION_ENGINEER); 
		        	}		        	
		        }
		        
		        //get the direct managers list		        
		        oEnquiryForm.setDirectManagersList(oQuotationDao.getManagersList(conn, QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_DM, oUserDto.getUserId())); 
		        

		        oTeamMemberDto = new TeamMemberDto();
        		oTeamMemberDto.setEnquiryId(oEnquiryForm.getEnquiryId());
        		oTeamMemberDto.setMembertype(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_DM);
        		oTeamMemberDto = oQuotationDao.getTeamMemberDetails(conn, oTeamMemberDto);

        		arlSalesManagerList=oTeamMemberDto.getTeamMemberList();
        		for(int i=0;i<arlSalesManagerList.size();i++)
        		{
        			okeyvaluevo=(KeyValueVo) arlSalesManagerList.get(i);
        			
        		}

        		oEnquiryForm.setTeamMemberList(oTeamMemberDto.getTeamMemberList());
        		
             // build rating error message
		      oEnquiryForm = buildRatingErrorMessage(oEnquiryForm);

		        
		        
            	oEnquiryForm.setCreatedBy(oUserDto.getFullName());
	            oEnquiryForm.setCreatedBySSO(oUserDto.getUserId());

	           

	            
		        if (oEnquiryForm.getCommercialOfferType().trim().length() < QuotationConstants.QUOTATION_LITERAL_ONE) 
		        	oEnquiryForm.setCommercialOfferType(QuotationConstants.QUOTATION_STRING_D);
		        
            }//eNDING
    	}catch(Exception e) {
            /*
             * Logging any exception that raised during this activity in log files using Log4j
             */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "home", ExceptionUtility.getStackTraceAsString(e));
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        } finally {
            /* Releasing/closing the connection object */
            if ( conn!=null )
                oDBUtility.releaseResources(conn);
        }
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
    	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_OFFERDATA_VIEW);
    }
    
    /**
     * This method is used for setting up the Indent offer data page.
     * 
     * @param actionMapping
     *            ActionMapping object to redirect the user to next page
     *            depending on the activity
     * @param actionForm
     *            ActionForm object to handle the form elements
     * @param request
     *            HttpServletRequest object to handle request operations
     * @param response
     *            HttpServletResponse object to handle response operations
     * @return returns ActionForward depending on which user is redirected to
     *         next page
     * @throws Exception
     *             if any error occurs while performing database operations, etc
     * @author 900010540 <br>
     * Created on: March 4, 2019
     * 
     */
    public ActionForward viewIndentOfferData(ActionMapping actionMapping,
                              ActionForm actionForm, 
                              HttpServletRequest request,
                              HttpServletResponse response) throws Exception {
    	
    	String sMethodName ="viewIndentOfferData"; 
    	String sClassName = this.getClass().getName();
    	
    	Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        
        UserDto oUserDto = null;
        RatingDto oRatingDto = null;
        
        String sOperationType = null;
        String sOperationType2 = null;

        
        WorkflowUtility oWorkflowUtility = new WorkflowUtility();
        WorkflowDto oWorkflowDto = null;
        UserAction oUserAction =null; //Instantiating the UserAction object
        DaoFactory oDaoFactory =null; //Creating an instance of of DaoFactory & retrieving UserDao object
        EnquiryDto oEnquiryDto = null;
        QuotationDao oQuotationDao = null;
        String sMsg =null;
        String sLinkMsg1=null;
        String sLinkMsg2=null;
        String sLinkMsg=null;
        TechnicalOfferDto oTechnicalOfferDto =null;
        boolean isUpdated =QuotationConstants.QUOTATION_FALSE;
        int iHidRowIndex =QuotationConstants.QUOTATION_LITERAL_ZERO;
        int iRatingId = QuotationConstants.QUOTATION_LITERAL_ZERO;
        String sDeptAndAuth = null;
        ArrayList alRatingList =null;
        String sCurrentRateId=null;
        String sFullName = null;
        String[] saSections=null;
        StringBuffer oAddOnTypeNames = null;
    	AddOnDto oAddOnDto = null;
    	TeamMemberDto  oTeamMemberDto = null;
    	List arlSalesManagerList=null;
    	KeyValueVo okeyvaluevo=null;
    	EnquiryForm oEnquiryForm =null;
    	int iWorkflowExecuteStatus =QuotationConstants.QUOTATION_LITERAL_ZERO;
    	boolean isDeletePreviousSatatus=false;
    	List alIndentAttachmentList=null;

      
    	
    	try{
    		
    		oEnquiryForm = (EnquiryForm) actionForm;
    		if (oEnquiryForm == null)
    			oEnquiryForm = new EnquiryForm();
    		
    		
    		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
    		
    		sOperationType = oEnquiryForm.getOperationType();  		
    		
    		/*
             * Ssetting user's information in request, using the header information
             * received through request
             */
            setRolesToRequest(request);
            LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "Roles set to request.");
            
            
            
            /* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Instantiating the UserAction object */
            oUserAction = new UserAction();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
    		
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            
            /*
    		 * This method will be invoked for all the funtionalities there in offer data page  - start To end
    		 * Now the question is - when do we set the token?
    		 * When sOperationType == "" thats when offer data process starts - may be we can set the token there
    		 * this seems to work - check more and see if any issues comes up
    		 */
    		if (sOperationType.trim().length() == QuotationConstants.QUOTATION_LITERAL_ZERO) 
    		{
    			sOperationType = QuotationConstants.QUOTATION_VIEWOFFDATA;    			
    			saveToken(request);    			
    		}else{
    			//for other cases validate token
    			if (! isTokenValid(request)) {
	           	 /* Setting error details to request to display the same in Exception page */ 
	               request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_INVALIDTOKENMESSAGE);
	               /* Forwarding request to Error page */
	               return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
	           }      
    		}
            /* Retrieving the logged-in user information */
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            
            oEnquiryDto = new EnquiryDto();
            oQuotationDao = oDaoFactory.getQuotationDao();
            
            // if its reassign the initiate the work flow and the forward the request to respective page In Case of Reassign to Commercial Engineer
            if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_REASSIGN)) 
            {      	 
            	
            	//reset the token
            	resetToken(request);
            	
            	 oWorkflowDto = new WorkflowDto();
                 
                 oWorkflowDto.setEnquiryId(oEnquiryForm.getEnquiryId());
                 oWorkflowDto.setEnquiryNo(oEnquiryForm.getEnquiryNumber());
                 oWorkflowDto.setLoginUserId(oUserDto.getUserId());
                 oWorkflowDto.setLoginUserName(oUserDto.getFullName());
                 oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
                 oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_REASSIGNTODE); //RAS
                 oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_WONPENDINGCM); //2
                 oWorkflowDto.setAssignedTo(oEnquiryForm.getAssignToId());
                 oWorkflowDto.setSavingIndent(oEnquiryForm.getSavingIndent());
            
                 oWorkflowDto.setRemarks(oEnquiryForm.getAssignRemarks());
                 oWorkflowDto.setDmToSmReturn(oEnquiryForm.getReturnToRemarks());
                 
                 LoggerUtility.log("INFO", sClassName, sMethodName, "10 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
                 iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
                
                 if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE ) 
                 {
                     LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Successfully");
                 } else  if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ZERO  )  
                 {
                     LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed - But email could not be sent. Please send email manually.");
                 } else {
                     LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Failed");
                 }
                 
                 LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Workflow process completed - - -");
                 
                  sMsg = QuotationConstants.QUOTATION_ASSIGNED_SUCESS+oEnquiryForm.getAssignToName();
                  sLinkMsg = QuotationConstants.QUOTATION_VALIDATEREQ_URL+oEnquiryForm.getEnquiryId()+QuotationConstants.QUOTATION_GRSYMBOL+QuotationConstants.QUOTATION_VIEWLINK;   
                 
                
                 request.setAttribute(QuotationConstants.QUOTATION_HEADINGMESSAGE,QuotationConstants.QUOTATION_REASSIG_SUCES);
                 request.setAttribute(QuotationConstants.QUOTATION_SUCCESSMESSAGE,sMsg);
                 request.setAttribute(QuotationConstants.QUOTATION_LINKSMESSAGE,sLinkMsg);
     			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);	
            }


            
            if (oEnquiryForm.getEnquiryId() < QuotationConstants.QUOTATION_LITERAL_ONE) 
            	oEnquiryForm.setEnquiryId(CommonUtility.getIntParameter(request, QuotationConstants.QUOTATION_REQUEST_ENQUIRYID));          
            
           
            if (oEnquiryForm.getEnquiryId() < QuotationConstants.QUOTATION_LITERAL_ONE) 
            {
            	
            	 /* Setting error details to request to display the same in Exception page */
                request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_CRET_ROWS); 
                
                /* Forwarding request to Error page */
                return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);            	
            }
        	
            if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_UPDRATING)) 
            {
            	// update the rating record with technical info EGRCheck
            	
            	oDBUtility.setAutoCommitFalse(conn);
            	oRatingDto = new RatingDto();
            	            	
            	oRatingDto.setRatingId(oEnquiryForm.getRatingId());
            	oTechnicalOfferDto = new TechnicalOfferDto();
            	PropertyUtils.copyProperties(oTechnicalOfferDto, oEnquiryForm);         
            	PropertyUtils.copyProperties(oRatingDto, oEnquiryForm);
            	PropertyUtils.copyProperties(oEnquiryDto, oEnquiryForm);
            	
            
            	/* Retrieving the logged-in user information */
                oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
                
                if (oUserDto == null)
                	oUserDto = new UserDto();
                
                oTechnicalOfferDto.setCreatedBy(oUserDto.getUserId());
                oTechnicalOfferDto.setCreatedByName(oUserDto.getFullName());                
                
                 
            	
                
                sOperationType = QuotationConstants.QUOTATION_VIEWOFFDATA;                
                sOperationType2=QuotationConstants.QUOTATION_VIEWOFFDATA2;
                
                
                if (oEnquiryForm.getDispatch().equalsIgnoreCase(QuotationConstants.QUOTATION_COMMERCIALMANAGER)) 
                {
                	
                	// 	reset the token
                	resetToken(request);
                	
                	oWorkflowDto = new WorkflowDto();
                    
                    oWorkflowDto.setEnquiryId(oEnquiryForm.getEnquiryId());
                    oWorkflowDto.setEnquiryNo(oEnquiryForm.getEnquiryNumber());
                    oWorkflowDto.setLoginUserId(oUserDto.getUserId());
                    oWorkflowDto.setLoginUserName(oUserDto.getFullName());
                    oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
                    oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTOCOMMERCIALMGR); 
                    oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_WONPENDINGCM); 
                    oWorkflowDto.setDmName(oEnquiryForm.getCreatedBy());
                    oWorkflowDto.setDmSsoid(oEnquiryForm.getCreatedBySSO());
                    oWorkflowDto.setDmNote(oEnquiryForm.getDmnote());
                    oWorkflowDto.setPageChecking(oEnquiryForm.getPagechecking());

                    oWorkflowDto.setDmToSmReturn(oEnquiryForm.getReturnToRemarks());
                    oEnquiryForm.setReturnEnquiry(QuotationConstants.QUOTATION_WON);

                    oWorkflowDto.setEquiryOperationType(oEnquiryForm.getEnquiryOperationType());
                    
                    //Delete Previous Status If It is SDM Based On Enquiry
                    isDeletePreviousSatatus=oWorkflowUtility.deletePrevStatusBasedonEnquiry(conn, oWorkflowDto);
                    
                    LoggerUtility.log("INFO", sClassName, sMethodName, "5 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
                    iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
                                        
                    if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE ){
                    	oDBUtility.commit(conn);
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Successfully");
                    } else  if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ZERO ){
                    	oDBUtility.commit(conn);
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed - But email could not be sent. Please send email manually.");                        
                    } else {
                    	oDBUtility.rollback(conn);
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Failed");
                    }
                    
                    LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Workflow process completed - - -");
                    
                    
                    sMsg = QuotationConstants.QUOTATION_OFFERDASUC;
                    sLinkMsg1 = QuotationConstants.QUOTATION_VIEWINQ_VAREQURL+oEnquiryForm.getEnquiryId()+QuotationConstants.QUOTATION_GRSYMBOL+QuotationConstants.QUOTATION_VIEWLINK; 
                    sLinkMsg2 = QuotationConstants.QUOTATION_VIEWSR_FVIEW_URL+QuotationConstants.QUOTATION_VIEWLINKS; 
                    
                    sLinkMsg = sLinkMsg1+QuotationConstants.QUOTATION_BREAK+sLinkMsg2;
                    request.setAttribute(QuotationConstants.QUOTATION_HEADINGMESSAGE,QuotationConstants.QUOTATION_SUB_SUCES);
                    request.setAttribute(QuotationConstants.QUOTATION_SUCCESSMESSAGE,sMsg);
                    request.setAttribute(QuotationConstants.QUOTATION_LINKSMESSAGE,sLinkMsg);
        			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);				
                }
                else if (oEnquiryForm.getDispatch().equalsIgnoreCase(QuotationConstants.QUOTATION_ESTIMATE)){
                	// reset the token
                	resetToken(request);
                	
                	oWorkflowDto = new WorkflowDto();
                	
                    
                    oWorkflowDto.setEnquiryId(oEnquiryForm.getEnquiryId());
                    oWorkflowDto.setEnquiryNo(oEnquiryForm.getEnquiryNumber());
                    oWorkflowDto.setLoginUserId(oUserDto.getUserId());
                    oWorkflowDto.setLoginUserName(oUserDto.getFullName());
                    oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
                   
                    oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTODM);//SDM
				    oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_WONPENDINGCM); 
                 
                    oWorkflowDto.setDmName(oEnquiryForm.getCreatedBy());
                    oWorkflowDto.setDmSsoid(oEnquiryForm.getCreatedBySSO());
                    oWorkflowDto.setDmNote(oEnquiryForm.getDmnote());
                    oWorkflowDto.setPageChecking(oEnquiryForm.getPagechecking());

                    oWorkflowDto.setDmToSmReturn(oEnquiryForm.getReturnToRemarks());

                    oWorkflowDto.setEquiryOperationType(oEnquiryForm.getEnquiryOperationType());
                    oWorkflowDto.setCmNote(oEnquiryForm.getCmNote());
                    
                    oWorkflowDto.setEmailCode(QuotationConstants.QUOTATION_WORKFLOW_STATUS_WONPENDINGCM);

                    
                    LoggerUtility.log("INFO", sClassName, sMethodName, "6 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
                   
                    
                    iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
                    
                    if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE){
                    	oDBUtility.commit(conn);
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Successfully");                      
                    } else  if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ZERO ){
                    	oDBUtility.commit(conn);
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed - But email could not be sent. Please send email manually.");
                    } else {
                    	oDBUtility.rollback(conn);
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Failed");
                    }
                    
                    LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Workflow process completed - - -");
                    
                    sMsg = QuotationConstants.QUOTATION_OFFERDASUC;
                    sLinkMsg1 = QuotationConstants.QUOTATION_VIEWINQ_VAREQURL+oEnquiryForm.getEnquiryId()+QuotationConstants.QUOTATION_GRSYMBOL+QuotationConstants.QUOTATION_VIEWLINK; 
                    sLinkMsg2 = QuotationConstants.QUOTATION_VIEWSR_FVIEW_URL+QuotationConstants.QUOTATION_VIEWLINKS; 
                    
                    sLinkMsg = sLinkMsg1+QuotationConstants.QUOTATION_BREAK+sLinkMsg2;
                    request.setAttribute(QuotationConstants.QUOTATION_HEADINGMESSAGE,QuotationConstants.QUOTATION_SUB_SUCES);
                    request.setAttribute(QuotationConstants.QUOTATION_SUCCESSMESSAGE,sMsg);
                    request.setAttribute(QuotationConstants.QUOTATION_LINKSMESSAGE,sLinkMsg);
        			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);	
                }  

                
                
            }//Is updaterating Ending
          
            
            
            if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_VIEWOFFDATA))
            { 
            	
              //Indent Commercial Operation Starts From Here	
            	
            	iRatingId = QuotationConstants.QUOTATION_LITERAL_ZERO;            	
            	sDeptAndAuth = QuotationConstants.QUOTATION_TECHNICAL_DEPT;
            	PropertyUtils.copyProperties(oEnquiryDto, oEnquiryForm);    
            	
            	// load the attachment list
                request.setAttribute(QuotationConstants.QUOTATION_ATTACHLIST, AttachmentUtility.getAttachmentDetails(oEnquiryDto)); 
              
                //load the dm uploaded attachmentlist 
                request.setAttribute(QuotationConstants.QUOTATION_DMATTACHLIST, AttachmentUtility.getDMUploadedAttachmentDetails(oEnquiryDto));

                //load the Intend uploaded AttachmentList
                request.setAttribute(QuotationConstants.QUOTATION_INTENDATTACHLIST, AttachmentUtility.getIntendUploadedAttachmentDetails(oEnquiryDto));
                

                
                
            	if (oEnquiryForm.getRatingId() > QuotationConstants.QUOTATION_LITERAL_ZERO) 
            		oEnquiryDto.setRatingOperationId(""+oEnquiryForm.getRatingId());

            /*
             * Fetching All Required Details along with Delivery Details
             * 
             */
            	oEnquiryDto = oQuotationDao.getEnquiryDetails(conn, oEnquiryDto, QuotationConstants.QUOTATION_FORWARD_VIEW.toUpperCase()); 
            	
            	PropertyUtils.copyProperties(oEnquiryForm, oEnquiryDto);
     
		        // now check the permissions - let it happen only once
		        if (oEnquiryForm.getEnquiryOperationType().trim().length() < QuotationConstants.QUOTATION_LITERAL_ONE) 
		        {
		        	oTeamMemberDto = new TeamMemberDto();
		        	oTeamMemberDto.setEnquiryId(oEnquiryForm.getEnquiryId());
		        	oTeamMemberDto = oQuotationDao.getTeamMemberDetails(conn, oTeamMemberDto);
		        	
		        	/*Setting Commercial Manager and Commercial Engineer*/
		        	 if (oUserDto.getUserId().equalsIgnoreCase(oTeamMemberDto.getCommercialManager())){
		        		
		        		oEnquiryForm.setDesignEngineerName(oUserDto.getFullName());
		        		oEnquiryForm.setEnquiryOperationType(QuotationConstants.QUOTATION_COMMMANAGER); 
		        	}
		        	else if (oUserDto.getUserId().equalsIgnoreCase(oTeamMemberDto.getCommercialEngineer())){
		        		
		        		oEnquiryForm.setDesignEngineerName(oUserDto.getFullName());
		        		oEnquiryForm.setEnquiryOperationType(QuotationConstants.QUOTATION_COMMENGINEER); 
		        	}

		        }
		        
		        //Retrieving Sales Manager List For Return Purpose
		        oTeamMemberDto = new TeamMemberDto();
        		oTeamMemberDto.setEnquiryId(oEnquiryForm.getEnquiryId());
        		oTeamMemberDto.setMembertype(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_DM);
        		oTeamMemberDto = oQuotationDao.getTeamMemberDetails(conn, oTeamMemberDto);

        		arlSalesManagerList=oTeamMemberDto.getTeamMemberList();
        		for(int i=0;i<arlSalesManagerList.size();i++)
        		{
        			okeyvaluevo=(KeyValueVo) arlSalesManagerList.get(i);
        			
        		}

        		oEnquiryForm.setTeamMemberList(oTeamMemberDto.getTeamMemberList());
  		        
            	oEnquiryForm.setCreatedBy(oUserDto.getFullName());
	            oEnquiryForm.setCreatedBySSO(oUserDto.getUserId());
		    	 
			    //Setting Commercial Engineers List    
		        oEnquiryForm.setCommercialManagersList(oQuotationDao.getCommercialManagersList(conn, QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_CM, oUserDto.getUserId())); 


			        
		    	 
		        
            
            }//eNDING
    	}catch(Exception e) {
            /*
             * Logging any exception that raised during this activity in log files using Log4j
             */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "home", ExceptionUtility.getStackTraceAsString(e));
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        } finally {
            /* Releasing/closing the connection object */
            if ( conn!=null )
                oDBUtility.releaseResources(conn);
        }
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
   	 return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_VIEWINDENTOFFER);	
    }

    /**
     * This method is used for setting up the Indent Design offer data page.
     * 
     * @param actionMapping
     *            ActionMapping object to redirect the user to next page
     *            depending on the activity
     * @param actionForm
     *            ActionForm object to handle the form elements
     * @param request
     *            HttpServletRequest object to handle request operations
     * @param response
     *            HttpServletResponse object to handle response operations
     * @return returns ActionForward depending on which user is redirected to
     *         next page
     * @throws Exception
     *             if any error occurs while performing database operations, etc
     * @author 900010540 <br>
     * Created on: March 5, 2019
     * 
     */
    public ActionForward viewIndentDesignOfferData(ActionMapping actionMapping,
                              ActionForm actionForm, 
                              HttpServletRequest request,
                              HttpServletResponse response) throws Exception {
    	
    	String sMethodName ="viewIndentDesignOfferData"; 
    	String sClassName = this.getClass().getName();
    	
    	Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        
        UserDto oUserDto = null;
        RatingDto oRatingDto = null;
        
        String sOperationType = null;
        String sOperationType2 = null;

        
        WorkflowUtility oWorkflowUtility = new WorkflowUtility();
        WorkflowDto oWorkflowDto = null;
        UserAction oUserAction =null; //Instantiating the UserAction object
        DaoFactory oDaoFactory =null; //Creating an instance of of DaoFactory & retrieving UserDao object
        EnquiryDto oEnquiryDto = null;
        QuotationDao oQuotationDao = null;
        String sMsg =null;
        String sLinkMsg1=null;
        String sLinkMsg2=null;
        String sLinkMsg=null;
        TechnicalOfferDto oTechnicalOfferDto =null;
        boolean isUpdated =QuotationConstants.QUOTATION_FALSE;
        int iHidRowIndex =QuotationConstants.QUOTATION_LITERAL_ZERO;
        int iRatingId = QuotationConstants.QUOTATION_LITERAL_ZERO;
        String sDeptAndAuth = null;
        ArrayList alRatingList =null;
        String sCurrentRateId=null;
        String sFullName = null;
        String[] saSections=null;
        StringBuffer oAddOnTypeNames = null;
    	AddOnDto oAddOnDto = null;
    	TeamMemberDto  oTeamMemberDto = null;
    	List arlSalesManagerList=null;
    	KeyValueVo okeyvaluevo=null;
    	EnquiryForm oEnquiryForm =null;
    	int iWorkflowExecuteStatus =QuotationConstants.QUOTATION_LITERAL_ZERO;
    	boolean isDeletePreviousSatatus=false;

      
    	
    	try{
    		
    		oEnquiryForm = (EnquiryForm) actionForm;
    		if (oEnquiryForm == null)
    			oEnquiryForm = new EnquiryForm();
    		
    		
    		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
    		
    		sOperationType = oEnquiryForm.getOperationType();  		
    		
    		/*
             * Ssetting user's information in request, using the header information
             * received through request
             */
            setRolesToRequest(request);
            LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "Roles set to request.");
            
            
            
            /* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Instantiating the UserAction object */
            oUserAction = new UserAction();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
    		
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            
            /*
    		 * This method will be invoked for all the funtionalities there in offer data page  - start To end
    		 * Now the question is - when do we set the token?
    		 * When sOperationType == "" thats when offer data process starts - may be we can set the token there
    		 * this seems to work - check more and see if any issues comes up
    		 */
    		if (sOperationType.trim().length() == QuotationConstants.QUOTATION_LITERAL_ZERO) 
    		{
    			sOperationType = QuotationConstants.QUOTATION_VIEWOFFDATA;    			
    			saveToken(request);    			
    		}else{
    			//for other cases validate token
    			if (! isTokenValid(request)) {
	           	 /* Setting error details to request to display the same in Exception page */ 
	               request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_INVALIDTOKENMESSAGE);
	               /* Forwarding request to Error page */
	               return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
	           }      
    		}
            
            /* Retrieving the logged-in user information */
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            
            oEnquiryDto = new EnquiryDto();
            oQuotationDao = oDaoFactory.getQuotationDao();
            
            // if its reassign the initiate the work flow and the forward the request to respective page In Case of Reassign to Commercial Engineer
            if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_REASSIGN)) 
            {      	 
            	
            	//reset the token
            	resetToken(request);
            	
            	 oWorkflowDto = new WorkflowDto();
                 
                 oWorkflowDto.setEnquiryId(oEnquiryForm.getEnquiryId());
                 oWorkflowDto.setEnquiryNo(oEnquiryForm.getEnquiryNumber());
                 oWorkflowDto.setLoginUserId(oUserDto.getUserId());
                 oWorkflowDto.setLoginUserName(oUserDto.getFullName());
                 oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
                 oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_REASSIGNTODE); 
                 oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_WONPENDINGDM); 
                 oWorkflowDto.setAssignedTo(oEnquiryForm.getAssignToId());
                 oWorkflowDto.setSavingIndent(oEnquiryForm.getSavingIndent());
            
                 oWorkflowDto.setRemarks(oEnquiryForm.getAssignRemarks());
                 oWorkflowDto.setDmToSmReturn(oEnquiryForm.getReturnToRemarks());
                 
                 LoggerUtility.log("INFO", sClassName, sMethodName, "10 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
                 iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
                
                 if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE ) 
                 {
                     LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Successfully");
                 } else  if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ZERO  )  
                 {
                     LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed - But email could not be sent. Please send email manually.");
                 } else {
                     LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Failed");
                 }
                 
                 LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Workflow process completed - - -");
                 
                  sMsg = QuotationConstants.QUOTATION_ASSIGNED_SUCESS+oEnquiryForm.getAssignToName();
                  sLinkMsg = QuotationConstants.QUOTATION_VALIDATEREQ_URL+oEnquiryForm.getEnquiryId()+QuotationConstants.QUOTATION_GRSYMBOL+QuotationConstants.QUOTATION_VIEWLINK;   
                 
                
                 request.setAttribute(QuotationConstants.QUOTATION_HEADINGMESSAGE,QuotationConstants.QUOTATION_REASSIG_SUCES);
                 request.setAttribute(QuotationConstants.QUOTATION_SUCCESSMESSAGE,sMsg);
                 request.setAttribute(QuotationConstants.QUOTATION_LINKSMESSAGE,sLinkMsg);
     			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);	
            }


            
            if (oEnquiryForm.getEnquiryId() < QuotationConstants.QUOTATION_LITERAL_ONE) 
            	oEnquiryForm.setEnquiryId(CommonUtility.getIntParameter(request, QuotationConstants.QUOTATION_REQUEST_ENQUIRYID));          
            
           
            if (oEnquiryForm.getEnquiryId() < QuotationConstants.QUOTATION_LITERAL_ONE) 
            {
            	
            	 /* Setting error details to request to display the same in Exception page */
                request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_CRET_ROWS); 
                
                /* Forwarding request to Error page */
                return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);            	
            }
        	
            if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_UPDRATING)) 
            {
            	// update the rating record with technical info EGRCheck
            	
            	oDBUtility.setAutoCommitFalse(conn);
            	oRatingDto = new RatingDto();
            	            	
            	
            	oRatingDto.setRatingId(oEnquiryForm.getRatingId());
            	oTechnicalOfferDto = new TechnicalOfferDto();
            	PropertyUtils.copyProperties(oTechnicalOfferDto, oEnquiryForm);         
            	PropertyUtils.copyProperties(oRatingDto, oEnquiryForm);
            	PropertyUtils.copyProperties(oEnquiryDto, oEnquiryForm);
            	
            
            	/* Retrieving the logged-in user information */
                oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
                
                if (oUserDto == null)
                	oUserDto = new UserDto();
                
                oTechnicalOfferDto.setCreatedBy(oUserDto.getUserId());
                oTechnicalOfferDto.setCreatedByName(oUserDto.getFullName());                
                
               
                sOperationType = QuotationConstants.QUOTATION_VIEWOFFDATA;                
                sOperationType2=QuotationConstants.QUOTATION_VIEWOFFDATA2;
                
                
                if (oEnquiryForm.getDispatch().equalsIgnoreCase(QuotationConstants.QUOTATION_MANAGER)) 
                {
                	
                	// 	reset the token
                	resetToken(request);
                	
                	oWorkflowDto = new WorkflowDto();
                    
                    oWorkflowDto.setEnquiryId(oEnquiryForm.getEnquiryId());
                    oWorkflowDto.setEnquiryNo(oEnquiryForm.getEnquiryNumber());
                    oWorkflowDto.setLoginUserId(oUserDto.getUserId());
                    oWorkflowDto.setLoginUserName(oUserDto.getFullName());
                    oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
                    oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTODM); 
                    oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_WONPENDINGDM); 
                    oWorkflowDto.setDmName(oEnquiryForm.getCreatedBy());
                    oWorkflowDto.setDmSsoid(oEnquiryForm.getCreatedBySSO());
                    oWorkflowDto.setDmNote(oEnquiryForm.getDmnote());
                    oWorkflowDto.setPageChecking(oEnquiryForm.getPagechecking());

                    oWorkflowDto.setDmToSmReturn(oEnquiryForm.getReturnToRemarks());
                    oEnquiryForm.setReturnEnquiry(QuotationConstants.QUOTATION_WON);

                    oWorkflowDto.setEquiryOperationType(oEnquiryForm.getEnquiryOperationType());
                    
                    //Delete Previous Status If It is SDM Based On Enquiry
                    isDeletePreviousSatatus=oWorkflowUtility.deletePrevStatusBasedonEnquiry(conn, oWorkflowDto);
                    
                    LoggerUtility.log("INFO", sClassName, sMethodName, "5 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
                    iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
                                        
                    if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE ){
                    	oDBUtility.commit(conn);
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Successfully");
                    } else  if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ZERO ){
                    	oDBUtility.commit(conn);
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed - But email could not be sent. Please send email manually.");                        
                    } else {
                    	oDBUtility.rollback(conn);
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Failed");
                    }
                    
                    LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Workflow process completed - - -");
                    
                    
                    sMsg = QuotationConstants.QUOTATION_OFFERDASUC;
                    sLinkMsg1 = QuotationConstants.QUOTATION_VIEWINQ_VAREQURL+oEnquiryForm.getEnquiryId()+QuotationConstants.QUOTATION_GRSYMBOL+QuotationConstants.QUOTATION_VIEWLINK; 
                    sLinkMsg2 = QuotationConstants.QUOTATION_VIEWSR_FVIEW_URL+QuotationConstants.QUOTATION_VIEWLINKS; 
                    
                    sLinkMsg = sLinkMsg1+QuotationConstants.QUOTATION_BREAK+sLinkMsg2;
                    request.setAttribute(QuotationConstants.QUOTATION_HEADINGMESSAGE,QuotationConstants.QUOTATION_SUB_SUCES);
                    request.setAttribute(QuotationConstants.QUOTATION_SUCCESSMESSAGE,sMsg);
                    request.setAttribute(QuotationConstants.QUOTATION_LINKSMESSAGE,sLinkMsg);
        			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);				
                }
                else if (oEnquiryForm.getDispatch().equalsIgnoreCase(QuotationConstants.QUOTATION_ESTIMATE)){
                	// reset the token
                	resetToken(request);
                	
                	oWorkflowDto = new WorkflowDto();
                	
                    
                    oWorkflowDto.setEnquiryId(oEnquiryForm.getEnquiryId());
                    oWorkflowDto.setEnquiryNo(oEnquiryForm.getEnquiryNumber());
                    oWorkflowDto.setLoginUserId(oUserDto.getUserId());
                    oWorkflowDto.setLoginUserName(oUserDto.getFullName());
                    oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
                   
                    oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_WON);
				    oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_WONPENDINGDM); 
                 
                    oWorkflowDto.setDmName(oEnquiryForm.getCreatedBy());
                    oWorkflowDto.setDmSsoid(oEnquiryForm.getCreatedBySSO());
                    oWorkflowDto.setDmNote(oEnquiryForm.getDmnote());
                    oWorkflowDto.setPageChecking(oEnquiryForm.getPagechecking());

                    oWorkflowDto.setDmToSmReturn(oEnquiryForm.getReturnToRemarks());

                    oWorkflowDto.setEquiryOperationType(oEnquiryForm.getEnquiryOperationType());
                    
                    

                    
                    LoggerUtility.log("INFO", sClassName, sMethodName, "6 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
                   
                    oWorkflowDto.setEmailCode(QuotationConstants.QUOTATION_WORKFLOW_STATUS_WONPENDINGDM);
                    iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
                    if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE){
                    	oDBUtility.commit(conn);
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Successfully");                      
                    } else  if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ZERO ){
                    	oDBUtility.commit(conn);
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed - But email could not be sent. Please send email manually.");
                    } else {
                    	oDBUtility.rollback(conn);
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Failed");
                    }
                    
                    LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Workflow process completed - - -");
                    
                    sMsg = QuotationConstants.QUOTATION_OFFERDASUC;
                    sLinkMsg1 = QuotationConstants.QUOTATION_VIEWINQ_VAREQURL+oEnquiryForm.getEnquiryId()+QuotationConstants.QUOTATION_GRSYMBOL+QuotationConstants.QUOTATION_VIEWLINK; 
                    sLinkMsg2 = QuotationConstants.QUOTATION_VIEWSR_FVIEW_URL+QuotationConstants.QUOTATION_VIEWLINKS; 
                    
                    sLinkMsg = sLinkMsg1+QuotationConstants.QUOTATION_BREAK+sLinkMsg2;
                    request.setAttribute(QuotationConstants.QUOTATION_HEADINGMESSAGE,QuotationConstants.QUOTATION_SUB_SUCES);
                    request.setAttribute(QuotationConstants.QUOTATION_SUCCESSMESSAGE,sMsg);
                    request.setAttribute(QuotationConstants.QUOTATION_LINKSMESSAGE,sLinkMsg);
        			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);	
                }  

                
                
            }//Is updaterating Ending
          
            
            
            if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_VIEWOFFDATA))
            { 
            	
            	iRatingId = QuotationConstants.QUOTATION_LITERAL_ZERO;            	
            	sDeptAndAuth = QuotationConstants.QUOTATION_TECHNICAL_DEPT;
            	PropertyUtils.copyProperties(oEnquiryDto, oEnquiryForm);    
            	
             	// load the attachment list
                request.setAttribute(QuotationConstants.QUOTATION_ATTACHLIST, AttachmentUtility.getAttachmentDetails(oEnquiryDto)); 
              
                //load the dm uploaded attachmentlist 
                request.setAttribute(QuotationConstants.QUOTATION_DMATTACHLIST, AttachmentUtility.getDMUploadedAttachmentDetails(oEnquiryDto));

                //load the Intend uploaded AttachmentList
                request.setAttribute(QuotationConstants.QUOTATION_INTENDATTACHLIST, AttachmentUtility.getIntendUploadedAttachmentDetails(oEnquiryDto));
                
            	if (oEnquiryForm.getRatingId() > QuotationConstants.QUOTATION_LITERAL_ZERO) 
            		oEnquiryDto.setRatingOperationId(""+oEnquiryForm.getRatingId());

            /*
             * Fetching All Required Details along with Delivery Details
             * 
             */
            	oEnquiryDto = oQuotationDao.getEnquiryDetails(conn, oEnquiryDto, QuotationConstants.QUOTATION_FORWARD_VIEW.toUpperCase()); 
            	PropertyUtils.copyProperties(oEnquiryForm, oEnquiryDto);
            	
       
		        // now check the permissions - let it happen only once
		        if (oEnquiryForm.getEnquiryOperationType().trim().length() < QuotationConstants.QUOTATION_LITERAL_ONE) 
		        {
		        	oTeamMemberDto = new TeamMemberDto();
		        	oTeamMemberDto.setEnquiryId(oEnquiryForm.getEnquiryId());
		        	oTeamMemberDto = oQuotationDao.getTeamMemberDetails(conn, oTeamMemberDto);
		        	
		        	/*Setting Commercial Manager and Commercial Engineer*/
		        	 if (oUserDto.getUserId().equalsIgnoreCase(oTeamMemberDto.getDesignManager())){
		        		
		        		oEnquiryForm.setDesignEngineerName(oUserDto.getFullName());
		        		oEnquiryForm.setEnquiryOperationType(QuotationConstants.QUOTATION_MANAGER); 
		        	}
		        	else if (oUserDto.getUserId().equalsIgnoreCase(oTeamMemberDto.getDesignEngineer())){
		        		
		        		oEnquiryForm.setDesignEngineerName(oUserDto.getFullName());
		        		oEnquiryForm.setEnquiryOperationType(QuotationConstants.QUOTATION_ENGINEER); 
		        	}

		        }
		        

		        oTeamMemberDto = new TeamMemberDto();
        		oTeamMemberDto.setEnquiryId(oEnquiryForm.getEnquiryId());
        		oTeamMemberDto.setMembertype(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_CM);
        		oTeamMemberDto = oQuotationDao.getTeamMemberDetails(conn, oTeamMemberDto);

        		arlSalesManagerList=oTeamMemberDto.getTeamMemberList();
        		for(int i=0;i<arlSalesManagerList.size();i++)
        		{
        			okeyvaluevo=(KeyValueVo) arlSalesManagerList.get(i);
        			
        		}

        		oEnquiryForm.setTeamMemberList(oTeamMemberDto.getTeamMemberList());
        		
             // build rating error message
		      oEnquiryForm = buildRatingErrorMessage(oEnquiryForm);

		        
		        
            	oEnquiryForm.setCreatedBy(oUserDto.getFullName());
	            oEnquiryForm.setCreatedBySSO(oUserDto.getUserId());

	           

	            
		    	 
			    //Setting Commercial Engineers List    
		        oEnquiryForm.setDirectManagersList(oQuotationDao.getManagersList(conn, QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_DM, oUserDto.getUserId())); 

		    	 
			        
		    	 
		        
            
            }//eNDING
    	}catch(Exception e) {
            /*
             * Logging any exception that raised during this activity in log files using Log4j
             */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "home", ExceptionUtility.getStackTraceAsString(e));
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        } finally {
            /* Releasing/closing the connection object */
            if ( conn!=null )
                oDBUtility.releaseResources(conn);
        }
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
   	 return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_VIEWDESIGNINDENTOFFER);	
    }

    

    
    /**
     * This method is used for the enquiry details
     * 
     * @param actionMapping
     *            ActionMapping object to redirect the user to next page
     *            depending on the activity
     * @param actionForm
     *            ActionForm object to handle the form elements
     * @param request
     *            HttpServletRequest object to handle request operations
     * @param response
     *            HttpServletResponse object to handle response operations
     * @return returns ActionForward depending on which user is redirected to
     *         next page
     * @throws Exception
     *             if any error occurs while performing database operations, etc
     * @author 100006126 <br>
     * Created on: Oct 13, 2008
     */
    public ActionForward viewEnquiry(ActionMapping actionMapping,
                              ActionForm actionForm, 
                              HttpServletRequest request,
                              HttpServletResponse response) throws Exception {
    	
    	
    	
    	String sMethodName = QuotationConstants.QUOTATION_VIEWLINK_CAP;
    	Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        
        UserDto oUserDto = null;
        RatingDto oRatingDto = null;
        TeamMemberDto oTeamMemberDto = null;
        WorkflowDto oWorkFlowDto = null;
        
        String sOperationType = null;
        boolean isValidUser = QuotationConstants.QUOTATION_FALSE;
        String sOperationType2=null;
        String returnenquiry=QuotationConstants.QUOTATION_STRING_NO;
        UserAction oUserAction =null;
        DaoFactory oDaoFactory=null;
        EnquiryDto oEnquiryDto =null;
        QuotationDao oQuotationDao =null;
        EnquiryForm oEnquiryForm =null;
        ArrayList alDmAttachmentList=null;
        KeyValueVo oKeyValueVo=null;
        String sAttachmentId=null;
        String sFileName=null;
        String sFileExtension=null;
        List alIndentAttachmentList=null;
    	
    	try{
    		 /*Saving Request into a Token*/
            saveToken(request);
           
    		oEnquiryForm = (EnquiryForm) actionForm;
    		if (oEnquiryForm == null)
    			oEnquiryForm = new EnquiryForm();
    		
    		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
    		
    		sOperationType = CommonUtility.getStringParameter(request, QuotationConstants.QUOTATION_OPERATION_TYPE); 
    		
    		if (sOperationType.trim().length() == QuotationConstants.QUOTATION_LITERAL_ZERO)
    			sOperationType = oEnquiryForm.getOperationType();    		
    			
    		
    		if (sOperationType.trim().length() == QuotationConstants.QUOTATION_LITERAL_ZERO)
    			sOperationType = QuotationConstants.QUOTATION_VIEWLINK_CAP;
    		
    		oEnquiryForm.setOperationType(sOperationType);
    		
    		
    		
    	
    		
    		/*
             * Ssetting user's information in request, using the header information
             * received through request
             */
            setRolesToRequest(request);
            LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "Roles set to request.");
            
            /* To avoid duplicate form submission (using Synchronizer Token Pattern)
    		 * This token will be validated while saving the request
    		 */
    		saveToken(request);
            
            /* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Instantiating the UserAction object */
            oUserAction = new UserAction();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
    		
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            
            /* Retrieving the logged-in user information */
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            
            oEnquiryDto = new EnquiryDto();
            oQuotationDao = oDaoFactory.getQuotationDao();            
            
            if (oEnquiryForm.getEnquiryId() < QuotationConstants.QUOTATION_LITERAL_ONE) 
            	oEnquiryForm.setEnquiryId(CommonUtility.getIntParameter(request, QuotationConstants.QUOTATION_REQUEST_ENQUIRYID));          
            
           
            if (oEnquiryForm.getEnquiryId() < QuotationConstants.QUOTATION_LITERAL_ONE) 
            {
            	
            	 /* Setting error details to request to display the same in Exception page */
                request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_CRET_ROWS);
                
                /* Forwarding request to Error page */
                return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);            	
            }
            
            // Now check if the logged-in user has access to view this page
            oWorkFlowDto = new WorkflowDto(); 
            oWorkFlowDto.setAssignedTo(oUserDto.getUserId());
            oWorkFlowDto.setWorkflowStatus(QuotationConstants.QUOTATION_WORKFLOW_ACTION_PENDING);
            oWorkFlowDto.setEnquiryId(oEnquiryForm.getEnquiryId());
        	
            if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_REFACOFF_DATA))
            {
            	oWorkFlowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGRSM);
            	
            	
            }else if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_ESTOFFDATA)) 
            {
            	oWorkFlowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGCE);
            	 returnenquiry=CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_RETURN_ENQUIRY); 
            	
            }
            
            if( ( new SearchEnquiryDaoImpl().isSalesManager(conn,oUserDto.getUserId())))
            {
            	request.setAttribute(QuotationConstants.QUOTATION_SMANAGER,QuotationConstants.QUOTATION_SESSION_ADMIN_FLAG_TRUE);
            	
            }
           
            
            request.setAttribute(QuotationConstants.QUOTATION_USERSSO,oUserDto.getUserId());
            
            isValidUser = oQuotationDao.validateUser(conn, oWorkFlowDto);
            
            PropertyUtils.copyProperties(oEnquiryDto, oEnquiryForm);    
            
            //load the attachments for this enquiry
            request.setAttribute(QuotationConstants.QUOTATION_ATTACHLIST, AttachmentUtility.getAttachmentDetails(oEnquiryDto));          	
        
          //load the dm uploaded attachmentlist 
            alDmAttachmentList =AttachmentUtility.getDMUploadedAttachmentDetails(oEnquiryDto);
            request.setAttribute(QuotationConstants.QUOTATION_DMATTACHLIST, alDmAttachmentList);
            
            //load the Intend uploaded AttachmentList
            alIndentAttachmentList=AttachmentUtility.getIntendUploadedAttachmentDetails(oEnquiryDto);
            request.setAttribute(QuotationConstants.QUOTATION_INTENDATTACHLIST, alIndentAttachmentList);

        	
        	if (oEnquiryForm.getRatingId() > QuotationConstants.QUOTATION_LITERAL_ZERO) 
        		oEnquiryDto.setRatingOperationId(""+oEnquiryForm.getRatingId());
        
        	oEnquiryDto = oQuotationDao.getEnquiryDetails(conn, oEnquiryDto, QuotationConstants.QUOTATION_FORWARD_VIEW.toUpperCase()); 
        	calculateQuotedPrice(conn, oEnquiryDto);
        	// Consolidated View QUOTATION_CONSO_VIEW
        	if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_CONSO_VIEW)) 
        	{
        		
        		// get approvers comments
        		oWorkFlowDto = new WorkflowDto();
        		WorkflowDao oWorkFlowDao = oDaoFactory.getWorkflowDao();
        		oWorkFlowDto.setEnquiryId(oEnquiryForm.getEnquiryId());
        		oEnquiryDto.setApproverCommentsList(oWorkFlowDao.getApproversCommentsList(conn, oWorkFlowDto));
        		
        		oTeamMemberDto = new TeamMemberDto();
        		oTeamMemberDto.setEnquiryId(oEnquiryForm.getEnquiryId());        		
        		oTeamMemberDto = oQuotationDao.getTeamMemberDetails(conn, oTeamMemberDto);
        		
        		//  now check the type of the logged-in user and set the type appropriately
        		if (oUserDto.getUserId().equalsIgnoreCase(oTeamMemberDto.getDesignEngineer()) ||
        				oUserDto.getUserId().equalsIgnoreCase(oTeamMemberDto.getDesignManager())){
        			//	design level user
        			oTeamMemberDto.setUserType(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_DM);
        			oEnquiryDto.setDmFlag(QuotationConstants.QUOTATION_STRING_TRUE);//added to know if user is DM/DE/RSM
        		}else if (oUserDto.getUserId().equalsIgnoreCase(oTeamMemberDto.getChiefExecutive())){
        			// CE level user
        			oTeamMemberDto.setUserType(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_CE);        			
        		}else if (oUserDto.getUserId().equalsIgnoreCase(oTeamMemberDto.getPrimaryRSM()) 
        				|| oUserDto.getUserId().equalsIgnoreCase(oTeamMemberDto.getAdditionalRSM()) ){
        			// RSM level user
        			oEnquiryDto.setDmFlag(QuotationConstants.QUOTATION_STRING_TRUE);//added to know if user is DM/DE/RSM
        			oTeamMemberDto.setUserType(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_RSM);        			
        		}else if (oUserDto.getUserId().equalsIgnoreCase(oTeamMemberDto.getSalesManager())){
        			// SM level user
        			oTeamMemberDto.setUserType(QuotationConstants.QUOTATION_NRM_USER);      			
        		}else{
        			// normal user
        			oTeamMemberDto.setUserType(QuotationConstants.QUOTATION_NRM_USER);         			
        		}  
        		oEnquiryDto.setTeamMemberDto(oTeamMemberDto);
        	}
        	
        	
        	//This block fetches the user details to be displayed in return users pulldown
        	if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_ESTOFFDATA)) 
        	{
        		oTeamMemberDto = new TeamMemberDto();
        		oTeamMemberDto.setEnquiryId(oEnquiryForm.getEnquiryId());
        		oTeamMemberDto.setMembertype(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_CE);
        		oTeamMemberDto = oQuotationDao.getTeamMemberDetails(conn, oTeamMemberDto);
        		oEnquiryDto.setTeamMemberList(oTeamMemberDto.getTeamMemberList());  

        		oTeamMemberDto = new TeamMemberDto();
        		oTeamMemberDto.setEnquiryId(oEnquiryForm.getEnquiryId());
        		oTeamMemberDto.setMembertype(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_DM);
        		oTeamMemberDto = oQuotationDao.getTeamMemberDetails(conn, oTeamMemberDto);
        		oEnquiryDto.setSalesManagerList(oTeamMemberDto.getTeamMemberList());
        		
        	}
        	
        	PropertyUtils.copyProperties(oEnquiryForm, oEnquiryDto);
        	request.setAttribute(QuotationConstants.QUOTATION_LOWER_INQUIRYDTO, oEnquiryDto); 
        	
        	oEnquiryForm.setCreatedBy(oUserDto.getFullName());
        	oEnquiryForm.setCreatedBySSO(oUserDto.getUserId());
        	
        	

           if(sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_ESTOFFDATA))
           {
        	   oEnquiryForm.setPagechecking(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_CE.toLowerCase()); 
           }
           if(sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_REFACOFF_DATA))
           {
        	   
        	   oEnquiryForm.setPagechecking(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_RSM.toLowerCase());   
           }
          
    	}catch(Exception e) {
            /*
             * Logging any exception that raised during this activity in log files using Log4j
             */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "home", ExceptionUtility.getStackTraceAsString(e));
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        } finally {
            /* Releasing/closing the connection object */
            if ( conn!=null )
                oDBUtility.releaseResources(conn);
        }
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");

        request.setAttribute(QuotationConstants.QUOTATION_RETURN_ENQUIRY, returnenquiry); 
        if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_ESTOFFDATA)) 
        {
        	request.setAttribute(QuotationConstants.QUOTATION_PAGEHEADTYPE, QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_CE); 

        	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_VIEWENQUIRY);        	
        }else if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_REFACOFF_DATA)) 
        {
        	request.setAttribute(QuotationConstants.QUOTATION_PAGEHEADTYPE, QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_RSM);
        	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_OFFERDATA_VIEW);    
        }else if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_QUOTDATA)) 
        {
        	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_VIEWQUOTATION);        	
        }
        else if(sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_INDENTPAGEVIEW))
        {        	
        	request.setAttribute(QuotationConstants.QUOTATION_PAGEHEADTYPE, QuotationConstants.QUOTATION_FORWARD_VIEW); 
        	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_INDENTPAGEVIEW);
	
        }

        else{
        	request.setAttribute(QuotationConstants.QUOTATION_PAGEHEADTYPE, QuotationConstants.QUOTATION_FORWARD_VIEW); 
        	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_VIEW);
        }
    }
    
    /**
     * This method is used update the factor details
     * 
     * @param actionMapping
     *            ActionMapping object to redirect the user to next page
     *            depending on the activity
     * @param actionForm
     *            ActionForm object to handle the form elements
     * @param request
     *            HttpServletRequest object to handle request operations
     * @param response
     *            HttpServletResponse object to handle response operations
     * @return returns ActionForward depending on which user is redirected to
     *         next page
     * @throws Exception
     *             if any error occurs while performing database operations, etc
     * @author 100006126 <br>
     * Created on: Oct 14, 2008
     */
    public ActionForward updateFactor(ActionMapping actionMapping,
                              ActionForm actionForm, 
                              HttpServletRequest request,
                              HttpServletResponse response) throws Exception {
    	
    	
    	String sMethodName = "updateFactor";
    	String sClassName = this.getClass().getName();
    	Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        
        UserDto oUserDto = null;
        RatingDto oRatingDto = null;
        WorkflowUtility oWorkflowUtility = new WorkflowUtility();
        WorkflowDto oWorkflowDto = null;
        
        String sOperationType = null;
        UserAction oUserAction =null;
        DaoFactory oDaoFactory =null;
        EnquiryDto oEnquiryDto =null;
        EnquiryDto oEnquiryDtos =null;
        QuotationDao oQuotationDao=null;
        HashMap<String,EnquiryDto> hmFinalMap=null;
		String sRatedOutputPNMulitplier="",
				sTypeMultiplier="",
				sAreaCMultiplier="",
				sFrameMultiplier="",
				sApplicationMultiplier="",
				sFactorMultiplier="",
				sNetmcMultiplier="",
				sMarkupMultiplier="",
				sTransferCMultiplier="";  //List Price
		String sMc_Or_NspratioMultiplier="",  //Discount
			   sTransportationPerUnitMultiplier="", //addon percent
			   sWarrantyPerUnitMultiplier="", //addon value
			   sUnitPriceMultiplier="",
			   sTotalPriceMultiplier="";
		String sMsg="",sLinkMsg="",sLinkMsg1="",sLinkMsg2="";
		int iRatingId=0;
		TechnicalOfferDto oTechnicalOfferDto=null;

		String sFinalRatingList=null;
		boolean isUpdated =QuotationConstants.QUOTATION_FALSE;
		String[] sRatings=null;
		String sQuantity=null;
		List alRatingList=null;
		//Added BY Gangadar For Indent Page
		
		String sOperationType2=null;
		StringBuffer oAddOnTypeNames=null;
		DeliveryDetailsDto oDeliveryDetails=null;
		boolean isDeletePreviousStatus=false;
		int iWorkflowExecuteStatus=0;
		String[] saActionCodes=null;
		List alRatingObjectList=null;
		String sTotalPrice="0";
		List alDmAttachmentList=null;
		List alIndentAttachmentList=null;
		String sReturnType=null;
		int iHidRowIndex=0;
		String sActionCode=null;
		TechnicalOfferDto oTechnicalDto=null;

	
		BigDecimal oBigDecimal=null;
		BigDecimal oTotalPrice=new BigDecimal("0");
		DecimalFormat oDataFormat = new DecimalFormat("###,###");

		

        EnquiryForm oEnquiryForm = (EnquiryForm) actionForm;
		if (oEnquiryForm == null)
			oEnquiryForm = new EnquiryForm();
		
    	try{    		
    		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
    		
    		sOperationType = CommonUtility.getStringParameter(request, QuotationConstants.QUOTATION_OPERATION_TYPE);
    		sReturnType=CommonUtility.getStringParameter(request, QuotationConstants.QUOTATION_RETURN_TYPE);

    		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "sOperationType = " + sOperationType);
    		
    		if (sOperationType.trim().length() == QuotationConstants.QUOTATION_LITERAL_ZERO)
    			sOperationType = oEnquiryForm.getOperationType();
    		if(sReturnType.trim().length()==QuotationConstants.QUOTATION_LITERAL_ZERO )
    			sReturnType=QuotationConstants.QUOTATION_NA;

    		if (sOperationType.trim().length() == QuotationConstants.QUOTATION_LITERAL_ZERO)
    			sOperationType = QuotationConstants.QUOTATION_VIEWLINK_CAP;		
    	
    		
    		/*
             * Ssetting user's information in request, using the header information
             * received through request
             */
            setRolesToRequest(request);
            LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "Roles set to request.");
            
            /* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Instantiating the UserAction object */
            oUserAction = new UserAction();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
    		
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            
            //check for token validity
    		if ((sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_WON) 
        			|| 
        		sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_UPDRATINGINDENTWON)
        			||
        		sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_LOST)
        			||
        		sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_ABONDED)
    				) 
        			&&
        		(oEnquiryForm.getDispatch()!=null 
        		    && 
        		 (!oEnquiryForm.getDispatch().equalsIgnoreCase(QuotationConstants.QUOTATION_WONTOESTIMATE)
        			||
        		 !oEnquiryForm.getDispatch().equalsIgnoreCase(QuotationConstants.QUOTATION_LOSTTOESTIMATE) 
        		  )
                )
        		)
        		{
        			saveToken(request);
        		}
        		else
        		{
        		if (! isTokenValid(request)) {
                 	 /* Setting error details to request to display the same in Exception page */ 
                     request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_INVALIDTOKENMESSAGE);
                     /* Forwarding request to Error page */
                     return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
               }else{
               	resetToken(request);
               }
        		}

            /* Retrieving the logged-in user information */
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            
            /* Retrieving the logged-in user information */
            
            oEnquiryDto = new EnquiryDto();
            oQuotationDao = oDaoFactory.getQuotationDao();            
            
            if (oEnquiryForm.getEnquiryId() < QuotationConstants.QUOTATION_LITERAL_ONE) 
            	oEnquiryForm.setEnquiryId(CommonUtility.getIntParameter(request, QuotationConstants.QUOTATION_REQUEST_ENQUIRYID));          
            
            if (oEnquiryForm.getEnquiryId() < QuotationConstants.QUOTATION_LITERAL_ONE) 
            {
            	
            	 /* Setting error details to request to display the same in Exception page */
                request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_CRET_ROWS);
                
                /* Forwarding request to Error page */
                return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);            	
            } 
        	PropertyUtils.copyProperties(oEnquiryDto, oEnquiryForm);   

        	hmFinalMap=new HashMap<String,EnquiryDto>();

        	
        	if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_CE)){  
        		sFinalRatingList=CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_FINALRLIST);



        		
        		if(sFinalRatingList!=null)
        		{
        			sRatings=sFinalRatingList.split(",");
        			

        			for(int j=0;j<sRatings.length;j++)
        			{
        				oEnquiryDtos = new EnquiryDto();

        				sQuantity=CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_QUANTITY_MULTIPLIER+sRatings[j]).equals("")?QuotationConstants.QUOTATION_STRING_DOUBLEZERO:CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_QUANTITY_MULTIPLIER+sRatings[j]);
		
        				sRatedOutputPNMulitplier=CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_ROPN_MULTIPLIER+sRatings[j]).equals("")?QuotationConstants.QUOTATION_STRING_DOUBLEZERO:CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_ROPN_MULTIPLIER+sRatings[j]);
        				sTypeMultiplier=CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_TYPE_MULTIPLIER+sRatings[j]).equals("")?QuotationConstants.QUOTATION_STRING_DOUBLEZERO:CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_TYPE_MULTIPLIER+sRatings[j]);
        				sAreaCMultiplier=CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_AREAC_MULTIPLIER+sRatings[j]).equals("")?QuotationConstants.QUOTATION_STRING_DOUBLEZERO:CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_AREAC_MULTIPLIER+sRatings[j]);
        				sFrameMultiplier=CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_FRAME_MULTIPLIER+sRatings[j]).equals("")?QuotationConstants.QUOTATION_STRING_DOUBLEZERO:CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_FRAME_MULTIPLIER+sRatings[j]);
        				sApplicationMultiplier=CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_APPL_MULTIPLIER+sRatings[j]).equals("")?QuotationConstants.QUOTATION_STRING_DOUBLEZERO:CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_APPL_MULTIPLIER+sRatings[j]);
        				sFactorMultiplier=CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_FACTOR_MULTIPLIER+sRatings[j]).equals("")?QuotationConstants.QUOTATION_STRING_DOUBLEZERO:CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_FACTOR_MULTIPLIER+sRatings[j]);
        				sNetmcMultiplier=CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_NETMC_MULTIPLIER+sRatings[j]).equals("")?QuotationConstants.QUOTATION_STRING_ZERO:CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_NETMC_MULTIPLIER+sRatings[j]);
        				sMarkupMultiplier=CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_MARKUP_MULTIPLIER+sRatings[j]).equals("")?QuotationConstants.QUOTATION_STRING_DOUBLEZERO:CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_MARKUP_MULTIPLIER+sRatings[j]);	
        				sTransferCMultiplier=CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_TRANSFERCOST_MULTIPLIER+sRatings[j]).equals("")?QuotationConstants.QUOTATION_STRING_ZERO:CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_TRANSFERCOST_MULTIPLIER+sRatings[j]);	
        				oEnquiryDtos.setQuantityMultiplier(sQuantity);
        				oEnquiryDtos.setRatedOutputPNMulitplier(sRatedOutputPNMulitplier);
        				oEnquiryDtos.setTypeMultiplier(sTypeMultiplier);
        				oEnquiryDtos.setAreaCMultiplier(sAreaCMultiplier);
        				oEnquiryDtos.setFrameMultiplier(sFrameMultiplier);

        				oEnquiryDtos.setApplicationMultiplier(sApplicationMultiplier);
        				oEnquiryDtos.setFactorMultiplier(sFactorMultiplier);
        				oEnquiryDtos.setNetmcMultiplier(sNetmcMultiplier);
        				oEnquiryDtos.setMarkupMultiplier(sMarkupMultiplier);
        				oEnquiryDtos.setTransferCMultiplier(sTransferCMultiplier);
       	
        				hmFinalMap.put(sRatings[j], oEnquiryDtos);
        				
        			}
        		}

        		oEnquiryDto.setMultiplierMap(hmFinalMap);


        		oEnquiryDto = getPriceListDataFromRequest(request, oEnquiryDto);

        		isUpdated = oQuotationDao.updateFactorDetails(conn, oEnquiryDto, sOperationType);

        		if (isUpdated){
        			oWorkflowDto = new WorkflowDto();
                    
                    oWorkflowDto.setEnquiryId(oEnquiryForm.getEnquiryId());
                    oWorkflowDto.setEnquiryNo(oEnquiryForm.getEnquiryNumber());
                    oWorkflowDto.setLoginUserId(oUserDto.getUserId());
                    oWorkflowDto.setLoginUserName(oUserDto.getFullName());
                    oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
                    oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTORSM);
                    oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGCE);     
                    oWorkflowDto.setRemarks(oEnquiryForm.getDiscountRemarks());
                    oWorkflowDto.setCeSsoid(oEnquiryForm.getCreatedBySSO());
                    oWorkflowDto.setCeName(oEnquiryForm.getCreatedBy());
                    oWorkflowDto.setCeNote(oEnquiryForm.getCenote());
                    oWorkflowDto.setPageChecking(oEnquiryForm.getPagechecking());
                    
                    LoggerUtility.log("INFO", sClassName, sMethodName, "6 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
                    iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
                    
                    if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE ) {
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Successfully");
                    } else  if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ZERO ) {
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed - But email could not be sent. Please send email manually.");
                    } else {
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Failed");
                    }
                    
                    LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Workflow process completed - - -");
                    
        		}
        	} else if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_RSM)){ 
        		
        		
        		sFinalRatingList=CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_FINALRLIST);

        		

        		
        		oEnquiryDtos=null;
        		if(sFinalRatingList!=null)
        		{
        			sRatings=sFinalRatingList.split(",");        			
        			
        			for(int j=0;j<sRatings.length;j++)        			{
        				
        				oEnquiryDtos = new EnquiryDto();
        				
        				sTransferCMultiplier=CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_TRANSFERCOST_MULTIPLIER+sRatings[j]).equals("")?QuotationConstants.QUOTATION_STRING_ZERO:CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_TRANSFERCOST_MULTIPLIER+sRatings[j]);
        				sMc_Or_NspratioMultiplier=CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_MCORNSRATIO_MULTIPLIER+sRatings[j]).equals("")?QuotationConstants.QUOTATION_STRING_DOUBLEZERO:CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_MCORNSRATIO_MULTIPLIER+sRatings[j]);
        				sTransportationPerUnitMultiplier=CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_TRANSPORTATION_MULTIPLIER+sRatings[j]).equals("")?QuotationConstants.QUOTATION_STRING_ZERO:CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_TRANSPORTATION_MULTIPLIER+sRatings[j]);
        				sWarrantyPerUnitMultiplier=CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_WARRANTY_MULTIPLIER+sRatings[j]).equals("")?QuotationConstants.QUOTATION_STRING_ZERO:CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_WARRANTY_MULTIPLIER+sRatings[j]);
        				sUnitPriceMultiplier=CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_UNITPRICE_MULTIPLIER+sRatings[j]).equals("")?QuotationConstants.QUOTATION_STRING_ZERO:CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_UNITPRICE_MULTIPLIER+sRatings[j]);
        				sTotalPriceMultiplier=CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_TOTALPRICE_MULTIPLIER+sRatings[j]).equals("")?QuotationConstants.QUOTATION_STRING_ZERO:CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_TOTALPRICE_MULTIPLIER+sRatings[j]);
        				oEnquiryDtos.setTransferCMultiplier(sTransferCMultiplier);
        				oEnquiryDtos.setMcornspratioMultiplier_rsm(sMc_Or_NspratioMultiplier);
        				oEnquiryDtos.setTransportationperunit_rsm(sTransportationPerUnitMultiplier);
        				oEnquiryDtos.setWarrantyperunit_rsm(sWarrantyPerUnitMultiplier);
        				oEnquiryDtos.setUnitpriceMultiplier_rsm(sUnitPriceMultiplier);
        				oEnquiryDtos.setTotalpriceMultiplier_rsm(sTotalPriceMultiplier);
        				hmFinalMap.put(sRatings[j], oEnquiryDtos);
        				
        			}
        		}

        		oEnquiryDto.setMultiplierMap(hmFinalMap);
        		oEnquiryDto = getPriceListDataFromRequest(request, oEnquiryDto);

        		
        		isUpdated = oQuotationDao.updateFactorDetails(conn, oEnquiryDto, sOperationType);   
        		if (isUpdated){
        			oWorkflowDto = new WorkflowDto();
                    
                    oWorkflowDto.setEnquiryId(oEnquiryForm.getEnquiryId());
                    oWorkflowDto.setEnquiryNo(oEnquiryForm.getEnquiryNumber());
                    oWorkflowDto.setLoginUserId(oUserDto.getUserId());
                    oWorkflowDto.setLoginUserName(oUserDto.getFullName());
                    oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
                    oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTOSM);
                    oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGRSM); 
                    oWorkflowDto.setRemarks(oEnquiryForm.getDiscountRemarks());
                    oWorkflowDto.setRsmSsoid(oEnquiryForm.getCreatedBySSO());
                    oWorkflowDto.setRsmName(oEnquiryForm.getCreatedBy());
                    oWorkflowDto.setRsmNote(oEnquiryForm.getRsmnote());
                    oWorkflowDto.setPageChecking(oEnquiryForm.getPagechecking());
                    
                    LoggerUtility.log("INFO", sClassName, sMethodName, "7 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
                    iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
                    
                    if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE ) {
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Successfully");
                    } else  if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ZERO) {
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed - But email could not be sent. Please send email manually.");
                    } else {
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Failed");
                    }
                    
                    LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Workflow process completed - - -");
        		}
       	}        if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_UPDRATINGINDENTWON))
       	{
       		
        	oDBUtility.setAutoCommitFalse(conn);
        	oRatingDto = new RatingDto();
        	            	
        	if (oEnquiryForm.getRatingId() < QuotationConstants.QUOTATION_LITERAL_ONE) 
        	{
        		 /* Setting error details to request to display the same in Exception page */
                request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_INVALID_RATINGID); 
                
                /* Forwarding request to Error page */
                return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        	}
        	
        	oRatingDto.setRatingId(oEnquiryForm.getRatingId());
        	oTechnicalOfferDto = new TechnicalOfferDto();
        	/*PropertyUtils.copyProperties(oTechnicalOfferDto, oEnquiryForm);*/         
        	PropertyUtils.copyProperties(oRatingDto, oEnquiryForm);
        	PropertyUtils.copyProperties(oEnquiryDto, oEnquiryForm);
        	//Manually Setting Value to The RatingDto
        	oRatingDto.setUnitPriceMultiplierRsm(oEnquiryForm.getUnitpriceMultiplier_rsm());
        
        	/* Retrieving the logged-in user information */
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            
            if (oUserDto == null)
            	oUserDto = new UserDto();
            
            oTechnicalOfferDto.setCreatedBy(oUserDto.getUserId());
            oTechnicalOfferDto.setCreatedByName(oUserDto.getFullName());  
            oEnquiryDto.setIndentOperation(QuotationConstants.QUOTATION_SMINDENT); //To Store Indent Operation
            oEnquiryDto.setIndentOperationCheck(QuotationConstants.QUOTATION_WON); //To Store Indent Type                   
            isUpdated = oQuotationDao.updatingIntendTechnicalData(conn, oEnquiryDto, oRatingDto, oTechnicalOfferDto);
            if (isUpdated){
            	oDBUtility.commit(conn);
            	oEnquiryDto = oQuotationDao.getEnquiryDetails(conn, oEnquiryDto, QuotationConstants.QUOTATION_INDENTDATA1);  
         	  	calculateSaveTmpPkgPrice(conn,oEnquiryDto);
           
            }
            else{
            	oDBUtility.rollback(conn);
            	 /* Setting error details to request to display the same in Exception page */
                request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_ERROR_UPDATING);
                
                /* Forwarding request to Error page */
                return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
            }
            if(isUpdated)
            {
            	iHidRowIndex = CommonUtility.getIntParameter(request, QuotationConstants.QUOTATION_HIDEROWIND); 
            	
            	
            	/* Retrieves the add on details from the form and sets
				  in arraylist
            	 */				 
            	
            	oRatingDto.setDeliveryDetailsList(getDeliveryDetails(iHidRowIndex, oEnquiryForm.getRatingId(), request));                	

            	// first delete all existing records
            	isUpdated = oQuotationDao.deleteAllDelDetailsByRatingId(conn, oRatingDto.getRatingId());                
            	
                if (isUpdated){
                	
                	oDBUtility.commit(conn);
                	isUpdated = oQuotationDao.updateDeliveryDetails(conn, oRatingDto);
                	if (isUpdated){
                     	oDBUtility.commit(conn);
                     	
                     }
                	 else{
                		 isUpdated=true;
                	 }                	
                }
                else{
                	oDBUtility.rollback(conn);
                	  //Setting error details to request to display the same in Exception page 
                    request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_ERROR_DELETING);
                    
                     //Forwarding request to Error page 
                    return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
                }
            	/*TODO if (isUpdated)
            		isUpdated = oQuotationDao.updateDeliveryDetails(conn, oRatingDto);
            	
                if (isUpdated){
                	oDBUtility.commit(conn);
                	oDBUtility.setAutoCommitTrue(conn);
                }
                if (! isUpdated){
                	isUpdated=true;
                }*/
 
                if(isUpdated && oEnquiryForm.getDispatch().equalsIgnoreCase(QuotationConstants.QUOTATION_WONTOESTIMATE))
                {
                	
                	oDBUtility.setAutoCommitFalse(conn);
                	oWorkflowDto = new WorkflowDto();
                	
                    
                	oWorkflowDto.setEnquiryId(oEnquiryDto.getEnquiryId());
                    oWorkflowDto.setEnquiryNo(oEnquiryDto.getEnquiryNumber());
                 
                    oWorkflowDto.setLoginUserId(oUserDto.getUserId());
                    oWorkflowDto.setLoginUserName(oUserDto.getFullName());
                    oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
                    
                    //oWorkflowDto.setActionCode(sActionCode); 
                    
                    LoggerUtility.log("INFO", sClassName, sMethodName, "10 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
                 
                    
                    sActionCode=QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBEND;
                    
                    if((sActionCode.contains(",") && sActionCode.split(",").length==QuotationConstants.QUOTATION_LITERAL_THREE) 
                    		&& 
                    	oEnquiryForm.getReturnEnquiry().equalsIgnoreCase(QuotationConstants.QUOTATION_NA))
                    {
                    saActionCodes=sActionCode.split(",");
                    for(int idx=0;idx<saActionCodes.length;idx++)
                    {
                    	

                    	oWorkflowDto.setActionCode(saActionCodes[idx].toString());
                    isDeletePreviousStatus=oWorkflowUtility.deletePrevStatusBasedonEnquiry(conn, oWorkflowDto);
                    
                    }
                   
                    if(isDeletePreviousStatus)
                    {
                    	
                    	LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Previous Status Deleted Successfully");
                    }
                    else
                    {
                    	LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Previous Status Deletion Faied or Recored Doesnt Exists");
        	
                    }
                    oWorkflowDto.setReturnEnquiry(QuotationConstants.QUOTATION_NA);
                }
                 else if(oEnquiryForm.getReturnEnquiry().equalsIgnoreCase(QuotationConstants.QUOTATION_WON))
                  {
                	 oWorkflowDto.setReturnEnquiry(QuotationConstants.QUOTATION_WON);	
                  }
                    
                    oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTOCOMMERCIALMGR); //SCM
                    oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_WONPENDINGSM);  //9
                    oWorkflowDto.setEmailCode(QuotationConstants.QUOTATION_WORKFLOW_STATUS_WONPENDINGSM);
                    iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
                    
                   
                    if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE ) {
                    	oDBUtility.commit(conn);
                    	oDBUtility.setAutoCommitTrue(conn);
                    	
                        LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFS);
                    } else  if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ZERO){ 
                    	oDBUtility.commit(conn);
                    	oDBUtility.setAutoCommitTrue(conn);
                    	
                    	LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFSEMAILF);
                    } else {
                       
                    	LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFF);
                    }
                    
                    LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFPC);
                    

                    sMsg = QuotationConstants.QUOTATION_TRACKNO+oEnquiryDto.getEnquiryNumber();
                    sLinkMsg1 = QuotationConstants.QUOTATION_VALIDATEREQ_URL+oEnquiryDto.getEnquiryId()+QuotationConstants.QUOTATION_GRSYMBOL+QuotationConstants.QUOTATION_VIEWLINK; 
                    sLinkMsg2 = QuotationConstants.QUOTATION_VIEWCR_ENQ_URL+QuotationConstants.QUOTATION_PLACELINKS; 
                    
                    sLinkMsg = sLinkMsg1+QuotationConstants.QUOTATION_BREAK+sLinkMsg2;
                    request.setAttribute(QuotationConstants.QUOTATION_HEADINGMESSAGE,QuotationConstants.QUOTATION_SUB_SUCES);
                    request.setAttribute(QuotationConstants.QUOTATION_SUCCESSMESSAGE,sMsg);
                    request.setAttribute(QuotationConstants.QUOTATION_LINKSMESSAGE,sLinkMsg);
        			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);					
        		}//Ending Of Submit

                    
            
               
                
                
            
            } //If Loop Ending
       		
       		
            sOperationType2=QuotationConstants.QUOTATION_INDENTDATA1;  		
       		sOperationType=QuotationConstants.QUOTATION_WON; //To For Indent Page
       		
       		
       	}
       	
       	if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_UPDRATINGINDENTLOST))
       	{
       		
        	oDBUtility.setAutoCommitFalse(conn);
        	oRatingDto = new RatingDto();
        	            	
        	
            PropertyUtils.copyProperties(oEnquiryDto, oEnquiryForm);
        
        	/* Retrieving the logged-in user information */
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            
            if (oUserDto == null)
            	oUserDto = new UserDto();
            
            oEnquiryDto.setLossChecking(QuotationConstants.QUOTATION_LOST);
            isUpdated = oQuotationDao.updatingLostIntendTechnicalData(conn, oEnquiryDto);
            
            if (isUpdated){
            	oDBUtility.commit(conn);
            }
            else{
            	oDBUtility.rollback(conn);
            	 /* Setting error details to request to display the same in Exception page */
                request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_ERROR_UPDATING);
                
                /* Forwarding request to Error page */
                return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
            }
 
            if(isUpdated && oEnquiryForm.getDispatch().equalsIgnoreCase(QuotationConstants.QUOTATION_LOSTTOESTIMATE))
            {
                	
                	oDBUtility.setAutoCommitFalse(conn);
                	oWorkflowDto = new WorkflowDto();
                	
                    
                	oWorkflowDto.setEnquiryId(oEnquiryDto.getEnquiryId());
                    oWorkflowDto.setEnquiryNo(oEnquiryDto.getEnquiryNumber());
                 
                    oWorkflowDto.setLoginUserId(oUserDto.getUserId());
                    oWorkflowDto.setLoginUserName(oUserDto.getFullName());
                    oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
                    
                    //oWorkflowDto.setActionCode(sActionCode); 
                    sActionCode=QuotationConstants.QUOTATION_WORKFLOW_ACTION_PEND;
                    
                    if((sActionCode.contains(",") && sActionCode.split(",").length==QuotationConstants.QUOTATION_LITERAL_TWO))
                    {
                    saActionCodes=sActionCode.split(",");
                    for(int idx=0;idx<saActionCodes.length;idx++)
                    {
                    	

                    	oWorkflowDto.setActionCode(saActionCodes[idx].toString());
                    isDeletePreviousStatus=oWorkflowUtility.deletePrevStatusBasedonEnquiry(conn, oWorkflowDto);
                    }
                   
                    if(isDeletePreviousStatus)
                    {
                    	
                    	LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Previous Status Deleted Successfully");
                    }
                    else
                    {
                    	LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Previous Status Deletion Faied or Recored Doesnt Exists");
        	
                    }
                    oWorkflowDto.setReturnEnquiry(QuotationConstants.QUOTATION_LOST);
                    }
                    
                    LoggerUtility.log("INFO", sClassName, sMethodName, "10 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
                 
                    
                    oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_LOST); //LST
                    oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_RELEASED);  //6
                    iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
                    
                     oWorkflowDto.setEmailCode(QuotationConstants.QUOTATION_WORKFLOW_STATUS_RELEASED);
                    if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE ) {
                    	oDBUtility.commit(conn);
                    	oDBUtility.setAutoCommitTrue(conn);
                    	
                        LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFS);
                    } else  if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ZERO){ 
                    	oDBUtility.commit(conn);
                    	oDBUtility.setAutoCommitTrue(conn);
                    	
                    	LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFSEMAILF);
                    } else {
                       
                    	LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFF);
                    }
                    
                    LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFPC);
                    

                    sMsg = QuotationConstants.QUOTATION_TRACKNO+oEnquiryDto.getEnquiryNumber();
                    sLinkMsg1 = QuotationConstants.QUOTATION_VALIDATEREQ_URL+oEnquiryDto.getEnquiryId()+QuotationConstants.QUOTATION_GRSYMBOL+QuotationConstants.QUOTATION_VIEWLINK; 
                    sLinkMsg2 = QuotationConstants.QUOTATION_VIEWCR_ENQ_URL+QuotationConstants.QUOTATION_PLACELINKS; 
                    
                    sLinkMsg = sLinkMsg1+QuotationConstants.QUOTATION_BREAK+sLinkMsg2;
                    request.setAttribute(QuotationConstants.QUOTATION_HEADINGMESSAGE,QuotationConstants.QUOTATION_SUB_SUCES);
                    request.setAttribute(QuotationConstants.QUOTATION_SUCCESSMESSAGE,sMsg);
                    request.setAttribute(QuotationConstants.QUOTATION_LINKSMESSAGE,sLinkMsg);
        			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);					
        		}//Ending Of Submit

            
       	}
       	
       	if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_UPDRATINGABONDENTINDENTLOST))
       	{
       		
        	oDBUtility.setAutoCommitFalse(conn);
        	oRatingDto = new RatingDto();
        	            	
        	
            PropertyUtils.copyProperties(oEnquiryDto, oEnquiryForm);
        
        	/* Retrieving the logged-in user information */
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            
            if (oUserDto == null)
            	oUserDto = new UserDto();
            
            oEnquiryDto.setLossChecking(QuotationConstants.QUOTATION_ABONDED);
            isUpdated = oQuotationDao.updatingLostIntendTechnicalData(conn, oEnquiryDto);

            if (isUpdated){
            	oDBUtility.commit(conn);
            }
            else{
            	oDBUtility.rollback(conn);
            	 /* Setting error details to request to display the same in Exception page */
                request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_ERROR_UPDATING);
                
                /* Forwarding request to Error page */
                return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
            }
 
                if(isUpdated && oEnquiryForm.getDispatch().equalsIgnoreCase(QuotationConstants.QUOTATION_ABONDEMENTTOESTIMATE))
                {
                	
                	oDBUtility.setAutoCommitFalse(conn);
                	oWorkflowDto = new WorkflowDto();
                	
                    
                	oWorkflowDto.setEnquiryId(oEnquiryDto.getEnquiryId());
                    oWorkflowDto.setEnquiryNo(oEnquiryDto.getEnquiryNumber());
                 
                    oWorkflowDto.setLoginUserId(oUserDto.getUserId());
                    oWorkflowDto.setLoginUserName(oUserDto.getFullName());
                    oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
                    
                    //oWorkflowDto.setActionCode(sActionCode); 
                    sActionCode=QuotationConstants.QUOTATION_WORKFLOW_ACTION_PEND;
                    
                    if((sActionCode.contains(",") && sActionCode.split(",").length==QuotationConstants.QUOTATION_LITERAL_TWO))
                    {
                    saActionCodes=sActionCode.split(",");
                    for(int idx=0;idx<saActionCodes.length;idx++)
                    {
                    	

                    	oWorkflowDto.setActionCode(saActionCodes[idx].toString());
                    isDeletePreviousStatus=oWorkflowUtility.deletePrevStatusBasedonEnquiry(conn, oWorkflowDto);

                    }
                   
                    if(isDeletePreviousStatus)
                    {
                    	
                    	LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Previous Status Deleted Successfully");
                    }
                    else
                    {
                    	LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Previous Status Deletion Faied or Recored Doesnt Exists");
        	
                    }
                    oWorkflowDto.setReturnEnquiry(QuotationConstants.QUOTATION_ABONDED);
                }
                    
                    LoggerUtility.log("INFO", sClassName, sMethodName, "10 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
                 
                    
                    oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_ABONDENT); //LST
                    oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_RELEASED);  //6
                    iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
                    
                   
                    if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE ) {
                    	oDBUtility.commit(conn);
                    	oDBUtility.setAutoCommitTrue(conn);
                    	
                        LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFS);
                    } else  if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ZERO){ 
                    	oDBUtility.commit(conn);
                    	oDBUtility.setAutoCommitTrue(conn);
                    	
                    	LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFSEMAILF);
                    } else {
                       
                    	LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFF);
                    }
                    
                    LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFPC);
                    

                    sMsg = QuotationConstants.QUOTATION_TRACKNO+oEnquiryDto.getEnquiryNumber();
                    sLinkMsg1 = QuotationConstants.QUOTATION_VALIDATEREQ_URL+oEnquiryDto.getEnquiryId()+QuotationConstants.QUOTATION_GRSYMBOL+QuotationConstants.QUOTATION_VIEWLINK; 
                    sLinkMsg2 = QuotationConstants.QUOTATION_VIEWCR_ENQ_URL+QuotationConstants.QUOTATION_PLACELINKS; 
                    
                    sLinkMsg = sLinkMsg1+QuotationConstants.QUOTATION_BREAK+sLinkMsg2;
                    request.setAttribute(QuotationConstants.QUOTATION_HEADINGMESSAGE,QuotationConstants.QUOTATION_SUB_SUCES);
                    request.setAttribute(QuotationConstants.QUOTATION_SUCCESSMESSAGE,sMsg);
                    request.setAttribute(QuotationConstants.QUOTATION_LINKSMESSAGE,sLinkMsg);
        			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);					
        		}//Ending Of Submit

            
       	}

       	
       	if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_WON))
       	{
       		
       	
       		/*
       		 * Adding Ratings On Indent Page WON
       		 */
        	iRatingId = QuotationConstants.QUOTATION_LITERAL_ZERO;            	
        	
        	PropertyUtils.copyProperties(oEnquiryDto, oEnquiryForm);    
        	if (oEnquiryForm.getRatingId() > QuotationConstants.QUOTATION_LITERAL_ZERO) 
        		oEnquiryDto.setRatingOperationId(""+oEnquiryForm.getRatingId());
        	sOperationType=QuotationConstants.QUOTATION_VIEWOFFDATA;
        	
        
        	if (oEnquiryForm.getRatingId() > QuotationConstants.QUOTATION_LITERAL_ZERO) 
        		oEnquiryDto.setRatingOperationId(""+oEnquiryForm.getRatingId());
        
          if(sOperationType2!=null && sOperationType2.equalsIgnoreCase(QuotationConstants.QUOTATION_INDENTDATA1))
          {
        	  oEnquiryDto = oQuotationDao.getEnquiryDetails(conn, oEnquiryDto, sOperationType2);  
        	 // calculateSaveTmpPkgPrice(conn,oEnquiryDto);
          }
          else if(sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_VIEWOFFDATA)) 
          {
        	oEnquiryDto = oQuotationDao.getEnquiryDetails(conn, oEnquiryDto, QuotationConstants.QUOTATION_INDENTDATA2); 
          }
          
          //Getting Total Price In Form Of ArrayList
          alRatingObjectList=oEnquiryDto.getRatingObjects();
          oRatingDto=new RatingDto();
          
          for(int i=0;i<alRatingObjectList.size();i++)
          {
        	  oRatingDto=(RatingDto) alRatingObjectList.get(i);
        	  oTechnicalDto=oRatingDto.getTechnicalDto();
        	    if(oTechnicalDto.getTotalpriceMultiplier_rsm()!="" && oTechnicalDto.getTotalpriceMultiplier_rsm()!=QuotationConstants.QUOTATION_STRING_ZERO)
        	    {
        	        if(oTechnicalDto.getTotalpriceMultiplier_rsm().indexOf(",") != QuotationConstants.QUOTATION_LITERAL_MINUSONE){
        	        sTotalPrice=oTechnicalDto.getTotalpriceMultiplier_rsm().replaceAll(",", "");
        	        oBigDecimal=new BigDecimal(sTotalPrice);
        	        oTotalPrice=oTotalPrice.add(oBigDecimal);
        	        }

        	    	
        	    }

          }
          
          if(oTotalPrice.toString()!="" && oTotalPrice.toString()!=QuotationConstants.QUOTATION_STRING_ZERO)
          {
        	  sTotalPrice=oDataFormat.format(oTotalPrice);
          }
          
        	oEnquiryDto = oQuotationDao.loadLookUpValues(conn, oEnquiryDto);           
        	PropertyUtils.copyProperties(oEnquiryForm, oEnquiryDto);
        	
        	

        	
        	
            oEnquiryForm = loadLookUpValues(conn, oEnquiryForm);
           
        	alRatingList = oEnquiryForm.getRatingList();
        	
        	
        	       
        	if (oEnquiryForm.getCurrentRating().trim().length() > QuotationConstants.QUOTATION_LITERAL_ZERO) 
        	{            
        		oRatingDto = (RatingDto) oEnquiryForm.getCurrentRatingObject().get(0);
    			

        		oEnquiryForm.setCurrentRating(""+oRatingDto.getRatingId());
        		oEnquiryForm.setRatingOperationId(""+oRatingDto.getRatingId());
        		
        		iRatingId = oRatingDto.getRatingId();            		
        		
        	}else{
        		
        		if (oEnquiryForm.getRatingObjects().size() > QuotationConstants.QUOTATION_LITERAL_ZERO){
        			oRatingDto = (RatingDto) oEnquiryForm.getRatingObjects().get(0);
        			
        			oEnquiryForm.setCurrentRating(""+oRatingDto.getRatingId());
        			iRatingId = oRatingDto.getRatingId();
        			oEnquiryForm.setRatingOperationId(""+oRatingDto.getRatingId());
        			
        		}
        	}  
        	
        	if (oRatingDto ==  null)
            	oRatingDto = new RatingDto();

                  
        	oQuotationDao.loadOfferPageLookUpValues(conn, oRatingDto);
        	
	        PropertyUtils.copyProperties(oEnquiryForm, oRatingDto);
	        
	        //get technical offer details 
	        oTechnicalOfferDto = oRatingDto.getTechnicalDto();
	        if (oTechnicalOfferDto != null){
	        	
	        	 PropertyUtils.copyProperties(oEnquiryForm, oTechnicalOfferDto);		        	 
	        	 if (oTechnicalOfferDto.getCreatedBy().trim().length() > QuotationConstants.QUOTATION_LITERAL_ZERO){
	        		 oEnquiryForm.setUpdateCreateBy(QuotationConstants.QUOTATION_STRING_FALSE); 
	        	 }
	        }		        
	       
	        oEnquiryForm = checkForNextRating(oEnquiryForm);
	        
            // build rating error message
		    oEnquiryForm = buildRatingErrorMessage(oEnquiryForm);

		        
		        
           	oEnquiryForm.setCreatedBy(oUserDto.getFullName());
	        oEnquiryForm.setCreatedBySSO(oUserDto.getUserId());
    
	            /*
	       		 * Adding Ratings On Indent Page WON Ending
	       		 */
	
	        //	get addon details
		       
	        if (iRatingId > QuotationConstants.QUOTATION_LITERAL_ZERO)
	        	oEnquiryForm.setDeliveryDetailsList(oQuotationDao.getDeliveryDetailsList(conn, oRatingDto.getRatingId()));
	        

	        
	        
       	   oEnquiryForm.setDispatch(QuotationConstants.QUOTATION_WON);
       	   oEnquiryForm.setStatusName(QuotationConstants.QUOTATION_WONPENDINGSALES);
   
       		//Setting Total Price For Indent Page
       	 //TODO  oEnquiryForm.setTotalMotorPrice(sTotalPrice);
       	   
       	   //setting Return Enquiry to Indent Page
       	   if(oEnquiryForm.getReturnEnquiry()!=null && oEnquiryForm.getReturnEnquiry().equalsIgnoreCase(QuotationConstants.QUOTATION_WON))
       	   {
       		oEnquiryForm.setReturnEnquiry(QuotationConstants.QUOTATION_WON);
       	   }else
       	   {
       		oEnquiryForm.setReturnEnquiry(sReturnType); 
       	   }
       	
          
       	   
          
       	   //load the attachments for this enquiry
           request.setAttribute(QuotationConstants.QUOTATION_ATTACHLIST, AttachmentUtility.getAttachmentDetails(oEnquiryDto));          	
       
         //load the dm uploaded attachmentlist 
           alDmAttachmentList =AttachmentUtility.getDMUploadedAttachmentDetails(oEnquiryDto);
           request.setAttribute(QuotationConstants.QUOTATION_DMATTACHLIST, alDmAttachmentList);
           
           //load the Intend uploaded AttachmentList
           alIndentAttachmentList=AttachmentUtility.getIntendUploadedAttachmentDetails(oEnquiryDto);
           request.setAttribute(QuotationConstants.QUOTATION_INTENDATTACHLIST, alIndentAttachmentList);
       	   
       	   
            /* Forwarding request to Indent  page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_INDENTWON);            	

       		
       		
       	}
       	if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_LOST))
       	{
       		
       		/*
       		 * Adding Ratings On Indent Page WON
       		 */
        	iRatingId = QuotationConstants.QUOTATION_LITERAL_ZERO;            	
        	
        	PropertyUtils.copyProperties(oEnquiryDto, oEnquiryForm);    
        	if (oEnquiryForm.getRatingId() > QuotationConstants.QUOTATION_LITERAL_ZERO) 
        		oEnquiryDto.setRatingOperationId(""+oEnquiryForm.getRatingId());
        	sOperationType=QuotationConstants.QUOTATION_VIEWOFFDATA;
        	
        
        	if (oEnquiryForm.getRatingId() > QuotationConstants.QUOTATION_LITERAL_ZERO) 
        		oEnquiryDto.setRatingOperationId(""+oEnquiryForm.getRatingId());
        
          if(sOperationType2!=null && sOperationType2.equalsIgnoreCase(QuotationConstants.QUOTATION_INDENTDATA1))
          {
        	  oEnquiryDto = oQuotationDao.getEnquiryDetails(conn, oEnquiryDto, sOperationType2);  
          }
          else if(sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_VIEWOFFDATA)) 
          {
        	oEnquiryDto = oQuotationDao.getEnquiryDetails(conn, oEnquiryDto, QuotationConstants.QUOTATION_INDENTDATA2); 
          }
      	oEnquiryDto = oQuotationDao.loadLookUpValues(conn, oEnquiryDto);           
      	PropertyUtils.copyProperties(oEnquiryForm, oEnquiryDto);
      	
      	

      	
      	
      	
          
        	
        	
        	oEnquiryForm.setDispatch(QuotationConstants.QUOTATION_LOST);
        	oEnquiryForm.setStatusName(QuotationConstants.QUOTATION_LOSTPENDINGSALES);
       		
            /* Forwarding request to Indent  page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_INDENTLOST);            	

       	}
       	if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_ABONDED))
       	{
       		
       		
       		/*
       		 * Adding Ratings On Indent Page WON
       		 */
        	iRatingId = QuotationConstants.QUOTATION_LITERAL_ZERO;            	
        	
        	PropertyUtils.copyProperties(oEnquiryDto, oEnquiryForm);    
        	if (oEnquiryForm.getRatingId() > QuotationConstants.QUOTATION_LITERAL_ZERO) 
        		oEnquiryDto.setRatingOperationId(""+oEnquiryForm.getRatingId());
        	sOperationType=QuotationConstants.QUOTATION_VIEWOFFDATA;
        	
        
        	if (oEnquiryForm.getRatingId() > QuotationConstants.QUOTATION_LITERAL_ZERO) 
        		oEnquiryDto.setRatingOperationId(""+oEnquiryForm.getRatingId());
        
        	if(sOperationType2!=null && sOperationType2.equalsIgnoreCase(QuotationConstants.QUOTATION_INDENTDATA1))
          	{
        	  oEnquiryDto = oQuotationDao.getEnquiryDetails(conn, oEnquiryDto, sOperationType2);  
          	}
          	else if(sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_VIEWOFFDATA)) 
          	{
        	oEnquiryDto = oQuotationDao.getEnquiryDetails(conn, oEnquiryDto, QuotationConstants.QUOTATION_INDENTDATA2); 
          	}
      		oEnquiryDto = oQuotationDao.loadLookUpValues(conn, oEnquiryDto);           
      		PropertyUtils.copyProperties(oEnquiryForm, oEnquiryDto);
        	oEnquiryForm.setDispatch(QuotationConstants.QUOTATION_ABONDED);
        	oEnquiryForm.setStatusName(QuotationConstants.QUOTATION_ABONDED);
       		
            /* Forwarding request to Indent  page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_INDENTABONDED);            	


       	}	

    	}catch(Exception e) {
            /*
             * Logging any exception that raised during this activity in log files using Log4j
             */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "home", ExceptionUtility.getStackTraceAsString(e));
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        } finally {
            /* Releasing/closing the connection object */
            if ( conn!=null )
                oDBUtility.releaseResources(conn);
        }
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
        
        sMsg = QuotationConstants.QUOTATION_OFFERDASUC;
        sLinkMsg1 = QuotationConstants.QUOTATION_VIEWINQ_VAREQURL+oEnquiryForm.getEnquiryId()+QuotationConstants.QUOTATION_GRSYMBOL+QuotationConstants.QUOTATION_VIEWLINK; 
        sLinkMsg2 = QuotationConstants.QUOTATION_VIEWSR_FVIEW_URL+QuotationConstants.QUOTATION_VIEWLINKS; 
        
        sLinkMsg = sLinkMsg1+QuotationConstants.QUOTATION_BREAK+sLinkMsg2;
        request.setAttribute(QuotationConstants.QUOTATION_HEADINGMESSAGE,QuotationConstants.QUOTATION_SUB_SUCES);
        request.setAttribute(QuotationConstants.QUOTATION_SUCCESSMESSAGE,sMsg);
        request.setAttribute(QuotationConstants.QUOTATION_LINKSMESSAGE,sLinkMsg);
              

	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);

    }
    
    /**
     * This method is used to return an enquiry
     * 
     * @param actionMapping
     *            ActionMapping object to redirect the user to next page
     *            depending on the activity
     * @param actionForm
     *            ActionForm object to handle the form elements
     * @param request
     *            HttpServletRequest object to handle request operations
     * @param response
     *            HttpServletResponse object to handle response operations
     * @return returns ActionForward depending on which user is redirected to
     *         next page
     * @throws Exception
     *             if any error occurs while performing database operations, etc
     * @author 100006126 <br>
     * Created on: Oct 13, 2008
     */
    public ActionForward returnEnquiry(ActionMapping actionMapping,
                              ActionForm actionForm, 
                              HttpServletRequest request,
                              HttpServletResponse response) throws Exception {
    	
    	String sMethodName =QuotationConstants.QUOTATION_RETURNCINQ;
    	String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
        
    	Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        
        UserDto oUserDto = null;
        RatingDto oRatingDto = null;
        WorkflowUtility oWorkflowUtility = new WorkflowUtility();
        WorkflowDto oWorkflowDto = null;
        
        String sOperationType = null;
        String sMsg = "";
        UserAction oUserAction =null;
        DaoFactory oDaoFactory =null;
        QuotationDao oQuotationDao = null;
        String sMail =null;
        String sReturnoperation=null;
        StringTokenizer stToken=null;
        int iWorkflowExecuteStatus =0;
        String sTypeValue=null;
        String pageChecking=null;
        String sLinkMsg =null;
        String sLinkMsg1=null;
        String sLinkMsg2=null;
        String sReturnComment=null;
        
        EnquiryForm oEnquiryForm = (EnquiryForm) actionForm;
		if (oEnquiryForm == null)
			oEnquiryForm = new EnquiryForm();
		
		
    	
    	try {
    		sOperationType = CommonUtility.getStringParameter(request, QuotationConstants.QUOTATION_OPERATION_TYPE);
    		if (sOperationType.trim().length() == QuotationConstants.QUOTATION_LITERAL_ZERO)
    			sOperationType = oEnquiryForm.getOperationType();
    		if (sOperationType.trim().length() == QuotationConstants.QUOTATION_LITERAL_ZERO)
    			sOperationType = QuotationConstants.QUOTATION_VIEWLINK_CAP;		
    		
    		/*
             * Ssetting user's information in request, using the header information
             * received through request
             */
    		
        	

            setRolesToRequest(request);
            LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "Roles set to request.");
            
            /* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Instantiating the UserAction object */
            oUserAction = new UserAction();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
    		
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            
            //  check for token validity
            if ( isTokenValid(request) ) {
                resetToken(request);
                
                /* Retrieving the logged-in user information */
                oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);            
               
                oQuotationDao = oDaoFactory.getQuotationDao();            
                
                if (oEnquiryForm.getEnquiryId() < QuotationConstants.QUOTATION_LITERAL_ONE) 
                    oEnquiryForm.setEnquiryId(CommonUtility.getIntParameter(request, QuotationConstants.QUOTATION_REQUEST_ENQUIRYID));          
               
                if (oEnquiryForm.getEnquiryId() < QuotationConstants.QUOTATION_LITERAL_ONE) 
                {
                     /* Setting error details to request to display the same in Exception page */
                    request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_CRET_ROWS);
                    
                    /* Forwarding request to Error page */
                    return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);             
                } 
               
                if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_DM)){
                    oWorkflowDto = new WorkflowDto();
                    
                    oWorkflowDto.setEnquiryId(oEnquiryForm.getEnquiryId());
                    oWorkflowDto.setEnquiryNo(oEnquiryForm.getEnquiryNumber());
                    oWorkflowDto.setLoginUserId(oUserDto.getUserId());
                    oWorkflowDto.setLoginUserName(oUserDto.getFullName());
                    oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());

                    oWorkflowDto.setDmToSmReturn(oEnquiryForm.getReturnToRemarks());
                    oWorkflowDto.setPageChecking(oEnquiryForm.getPagechecking());
                  
                    stToken = new StringTokenizer(oEnquiryForm.getReturnTo(), QuotationConstants.QUOTATION_STRING_PIPE);
                    if (stToken != null){
                        if (stToken.countTokens() == QuotationConstants.QUOTATION_LITERAL_THREE){
                            oWorkflowDto.setAssignedTo(stToken.nextToken());
                            sMail = stToken.nextToken();
                            if (stToken.nextToken().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_SM)) 
                            {
                                 oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_RETURNTOSM); 
                                 oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGDM); 
                                 sMsg = QuotationConstants.QUOTATION_OFFERDATARSUC;
                            }
                            }
                    }

                    LoggerUtility.log("INFO", sClassName, sMethodName, "7.5 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
                     iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
                    
                    if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE ) {
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Successfully");
                    } else  if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ZERO ) {
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed - But email could not be sent. Please send email manually.");
                    } else {
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Failed");
                    }
                    
                    LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Workflow process completed - - -");           
                    
                }
                if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_DE)){
                    oWorkflowDto = new WorkflowDto();
                    
                    oWorkflowDto.setEnquiryId(oEnquiryForm.getEnquiryId());
                    oWorkflowDto.setEnquiryNo(oEnquiryForm.getEnquiryNumber());
                    oWorkflowDto.setLoginUserId(oUserDto.getUserId());
                    oWorkflowDto.setLoginUserName(oUserDto.getFullName());
                    oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());

                    oWorkflowDto.setDeToSmReturn(oEnquiryForm.getReturnToRemarks());
                    oWorkflowDto.setPageChecking(oEnquiryForm.getPagechecking());
                  
                    stToken = new StringTokenizer(oEnquiryForm.getReturnTo(), QuotationConstants.QUOTATION_STRING_PIPE);
                    if (stToken != null){
                        if (stToken.countTokens() == QuotationConstants.QUOTATION_LITERAL_THREE){
                            oWorkflowDto.setAssignedTo(stToken.nextToken());
                             sMail = stToken.nextToken();
                            if (stToken.nextToken().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_SM)) 
                            {
                                 oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_RETURNTOSM); 
                                 oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGDM); 
                                 sMsg =QuotationConstants.QUOTATION_OFFERDATARSUC; 
                            }
                            }
                    }
                    LoggerUtility.log("INFO", sClassName, sMethodName, "7.6 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
                    iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
                    
                    if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE ) {
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Successfully");
                    } else  if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ZERO ) {
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed - But email could not be sent. Please send email manually.");
                    } else {
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Failed");
                    }
                    
                    LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Workflow process completed - - -");           
                    
                }

                if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_CMWON)){
                    oWorkflowDto = new WorkflowDto();
                    
                    oWorkflowDto.setEnquiryId(oEnquiryForm.getEnquiryId());
                    oWorkflowDto.setEnquiryNo(oEnquiryForm.getEnquiryNumber());
                    oWorkflowDto.setLoginUserId(oUserDto.getUserId());
                    oWorkflowDto.setLoginUserName(oUserDto.getFullName());
                    oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());

                    oWorkflowDto.setCmToSmReturn(oEnquiryForm.getReturnToRemarks());
                    oWorkflowDto.setPageChecking(oEnquiryForm.getPagechecking());
                  
                    stToken = new StringTokenizer(oEnquiryForm.getReturnTo(), QuotationConstants.QUOTATION_STRING_PIPE);
                    if (stToken != null){
                        if (stToken.countTokens() == QuotationConstants.QUOTATION_LITERAL_THREE){
                            oWorkflowDto.setAssignedTo(stToken.nextToken());
                             sMail = stToken.nextToken();
                            if (stToken.nextToken().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_SM)) 
                            {
                                oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_RETURNTOCM);
                                oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_WONPENDINGCM);

                                 sMsg =QuotationConstants.QUOTATION_DMINDENTRETURN; 
                            }
                            
                            
                            }
                    }
                    oWorkflowDto.setEmailCode(QuotationConstants.QUOTATION_WORKFLOW_STATUS_WONPENDINGCM);
                    LoggerUtility.log("INFO", sClassName, sMethodName, "7.7 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
                    iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
                    
                    if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE ) {
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Successfully");
                    } else  if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ZERO ) {
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed - But email could not be sent. Please send email manually.");
                    } else {
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Failed");
                    }
                    
                    LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Workflow process completed - - -");           
                    
                }
                if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_CEWON)){
                  
                	oWorkflowDto = new WorkflowDto();
                    
                    oWorkflowDto.setEnquiryId(oEnquiryForm.getEnquiryId());
                    oWorkflowDto.setEnquiryNo(oEnquiryForm.getEnquiryNumber());
                    oWorkflowDto.setLoginUserId(oUserDto.getUserId());
                    oWorkflowDto.setLoginUserName(oUserDto.getFullName());
                    oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());

                    oWorkflowDto.setCommengToSmReturn(oEnquiryForm.getReturnToRemarks());
                    oWorkflowDto.setPageChecking(oEnquiryForm.getPagechecking());
                  
                    stToken = new StringTokenizer(oEnquiryForm.getReturnTo(), QuotationConstants.QUOTATION_STRING_PIPE);
                    if (stToken != null){
                        if (stToken.countTokens() == QuotationConstants.QUOTATION_LITERAL_THREE){
                            oWorkflowDto.setAssignedTo(stToken.nextToken());
                             sMail = stToken.nextToken();
                            if (stToken.nextToken().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_SM)) 
                            {
                                oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_RETURNTOCM);
                                oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_WONPENDINGCM);

                                 sMsg =QuotationConstants.QUOTATION_DMINDENTRETURN; 
                            }
                            
                            
                            }
                    }
                    oWorkflowDto.setEmailCode(QuotationConstants.QUOTATION_WORKFLOW_STATUS_WONPENDINGCM);
                    LoggerUtility.log("INFO", sClassName, sMethodName, "7.7 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
                    iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
                    
                    if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE ) {
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Successfully");
                    } else  if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ZERO ) {
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed - But email could not be sent. Please send email manually.");
                    } else {
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Failed");
                    }
                    
                    LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Workflow process completed - - -");           
                    
                }

                if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_DMWON)){
                    oWorkflowDto = new WorkflowDto();
                    
                    oWorkflowDto.setEnquiryId(oEnquiryForm.getEnquiryId());
                    oWorkflowDto.setEnquiryNo(oEnquiryForm.getEnquiryNumber());
                    oWorkflowDto.setLoginUserId(oUserDto.getUserId());
                    oWorkflowDto.setLoginUserName(oUserDto.getFullName());
                    oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());

                    oWorkflowDto.setDmToCmReturn(oEnquiryForm.getReturnToRemarks());
                    oWorkflowDto.setPageChecking(oEnquiryForm.getPagechecking());
                  
                    stToken = new StringTokenizer(oEnquiryForm.getReturnTo(), QuotationConstants.QUOTATION_STRING_PIPE);
                    if (stToken != null){
                        if (stToken.countTokens() == QuotationConstants.QUOTATION_LITERAL_THREE){
                            oWorkflowDto.setAssignedTo(stToken.nextToken());
                            sMail = stToken.nextToken();
                            if (stToken.nextToken().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_CM)){
                                 oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_RETURNTOCM); 
                                 oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_WONPENDINGDM); 
                                 sReturnComment=QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_CM;
                                 sMsg = QuotationConstants.QUOTATION_OFFERDATARCMSUC;
                                 oWorkflowDto.setEmailCode(QuotationConstants.QUOTATION_WORKFLOW_STATUS_WONPENDINGDM);
                            }
                            else
                            {
                                oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_RETURNTOSM); 
                                oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_WONPENDINGDM); 
                                sReturnComment=QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_SM;
                                sMsg = QuotationConstants.QUOTATION_DMINDENTRETURN;
                                oWorkflowDto.setEmailCode(QuotationConstants.QUOTATION_WORKFLOW_STATUS_WONPENDINGDM);

                            }
                        }
                    }
                    LoggerUtility.log("INFO", sClassName, sMethodName, "7.8 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
                    oWorkflowDto.setsReturnComment(sReturnComment);
                    iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
                    
                    if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE ) {
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Successfully");
                    } else  if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ZERO ) {
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed - But email could not be sent. Please send email manually.");
                    } else {
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Failed");
                    }
                    
                    LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Workflow process completed - - -");           
                    
                }
                if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_DEWON)){
                   
                	oWorkflowDto = new WorkflowDto();
                    oWorkflowDto.setEnquiryId(oEnquiryForm.getEnquiryId());
                    oWorkflowDto.setEnquiryNo(oEnquiryForm.getEnquiryNumber());
                    oWorkflowDto.setLoginUserId(oUserDto.getUserId());
                    oWorkflowDto.setLoginUserName(oUserDto.getFullName());
                    oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());

                    oWorkflowDto.setDeToCmReturn(oEnquiryForm.getReturnToRemarks());
                    oWorkflowDto.setPageChecking(oEnquiryForm.getPagechecking());
                  
                    stToken = new StringTokenizer(oEnquiryForm.getReturnTo(), QuotationConstants.QUOTATION_STRING_PIPE);
                    if (stToken != null){
                        if (stToken.countTokens() == QuotationConstants.QUOTATION_LITERAL_THREE){
                            oWorkflowDto.setAssignedTo(stToken.nextToken());
                            sMail = stToken.nextToken();
                            if (stToken.nextToken().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_DM)){
                                oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_RETURNTOCM); 
                                oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_WONPENDINGDM); 
                                sReturnComment=QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_CM;
                                sMsg = QuotationConstants.QUOTATION_OFFERDATARCMSUC;
                                oWorkflowDto.setEmailCode(QuotationConstants.QUOTATION_WORKFLOW_STATUS_WONPENDINGDM);

                            }else{
                                oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_RETURNTOSM); 
                                oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_WONPENDINGDM); 
                                sReturnComment=QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_SM;
                                sMsg = QuotationConstants.QUOTATION_DMINDENTRETURN;
                                oWorkflowDto.setEmailCode(QuotationConstants.QUOTATION_WORKFLOW_STATUS_WONPENDINGDM);

                            }
                        }
                    }
                    LoggerUtility.log("INFO", sClassName, sMethodName, "7.9 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
                    iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
                    
                    if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE ) {
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Successfully");
                    } else  if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ZERO ) {
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed - But email could not be sent. Please send email manually.");
                    } else {
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Failed");
                    }
                    
                    LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Workflow process completed - - -");           
                    
                }

                if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_CE)){
                	
                	sReturnoperation=oEnquiryForm.getReturnOperation();
                	
                    oWorkflowDto = new WorkflowDto();
                    
                    oWorkflowDto.setEnquiryId(oEnquiryForm.getEnquiryId());
                    oWorkflowDto.setEnquiryNo(oEnquiryForm.getEnquiryNumber());
                    oWorkflowDto.setLoginUserId(oUserDto.getUserId());
                    oWorkflowDto.setLoginUserName(oUserDto.getFullName());
                    oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
                    
                   stToken = new StringTokenizer(oEnquiryForm.getReturnTo(), QuotationConstants.QUOTATION_STRING_PIPE);
                    if (stToken != null){
                        if (stToken.countTokens() == QuotationConstants.QUOTATION_LITERAL_THREE){
                            oWorkflowDto.setAssignedTo(stToken.nextToken());
                            sMail = stToken.nextToken();
                            if (stToken.nextToken().equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_DM)){
                                 oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_RETURNTODM); 
                                 oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGCE); 
                                 sMsg = QuotationConstants.QUOTATION_OFFERDATARDMSUC;
                            }else{
                                 oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_RETURNTODE); 
                                 oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGCE); 
                                 sMsg = QuotationConstants.QUOTATION_OFFERDATARDESUC;
                            }
                        }
                    }

                    oWorkflowDto.setRemarks(oEnquiryForm.getReturnToRemarks());
                    oWorkflowDto.setReturnOperation(sReturnoperation);
                    LoggerUtility.log("INFO", sClassName, sMethodName, "8 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
                    iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
                    
                    if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE ) {
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Successfully");
                    } else  if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ZERO ) {
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed - But email could not be sent. Please send email manually.");
                    } else {
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Failed");
                    }
                    
                    LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Workflow process completed - - -");  
                
                
                    
                } else if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_RSM)){  
                	
                	
                    oWorkflowDto = new WorkflowDto();
                    oWorkflowDto.setEnquiryId(oEnquiryForm.getEnquiryId());
                    oWorkflowDto.setEnquiryNo(oEnquiryForm.getEnquiryNumber());
                    oWorkflowDto.setLoginUserId(oUserDto.getUserId());
                    oWorkflowDto.setLoginUserName(oUserDto.getFullName());
                    oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
                    /*TODO pass The Flow From RSM TO CE*/
                  /*oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_RETURNTOCE); 
                    oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGRSM);  
                  */   
                    /*TODO Bipass The Flow From RSM TO DM*/
                    oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_RETURNTODM); 
                    oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGCE);           

                    oWorkflowDto.setRemarks(oEnquiryForm.getReturnToRemarks());
                   /*TODO Bipass The Flow From RSM TO DM
                    * 
                    *  sMsg = QuotationConstants.QUOTATION_OFFERDATARCESUC;*/
                    sMsg = QuotationConstants.QUOTATION_OFFERDATARDMSUC;
                    LoggerUtility.log("INFO", sClassName, sMethodName, "9 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
                    iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
                    
                    if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE ) {
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Successfully");
                    } else  if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ZERO ) {
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed - But email could not be sent. Please send email manually.");
                    } else {
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Failed");
                    }
                    
                    LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Workflow process completed - - -");               
                    
                } else if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_RFQINQ)){
                    oWorkflowDto = new WorkflowDto();
                    
                    oWorkflowDto.setEnquiryId(oEnquiryForm.getEnquiryId());
                    oWorkflowDto.setEnquiryNo(oEnquiryForm.getEnquiryNumber());
                    oWorkflowDto.setLoginUserId(oUserDto.getUserId());
                    oWorkflowDto.setLoginUserName(oUserDto.getFullName());
                    oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
                    
                    stToken = new StringTokenizer(oEnquiryForm.getReturnTo(), QuotationConstants.QUOTATION_STRING_PIPE);
                    pageChecking= "";
                    if (stToken != null){
                        if (stToken.countTokens() == QuotationConstants.QUOTATION_LITERAL_THREE){
                            oWorkflowDto.setAssignedTo(stToken.nextToken());
                            sMail = stToken.nextToken();
                            sTypeValue = stToken.nextToken();
                             if (sTypeValue.equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_CE)){
                                 oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_RETURNTOCE);
                                 sMsg = QuotationConstants.QUOTATION_OFFERDATARCESUC;
                                 pageChecking=QuotationConstants.QUOTAION_Q_CE;
                            }else if (sTypeValue.equalsIgnoreCase(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_DM)){
                                oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_RETURNTODM);
                                sMsg = QuotationConstants.QUOTATION_OFFERDATARDMSUC;
                                pageChecking=QuotationConstants.QUOTAION_Q_DM;
                            }else{
                                 oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_RETURNTORSM);
                                 sMsg = QuotationConstants.QUOTATION_OFFERDATARSMSUC;
                                 pageChecking=QuotationConstants.QUOTAION_Q_RSM;
                            }
                        }
                    }
                    oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGSM); 
                    
                    oWorkflowDto.setRemarks(oEnquiryForm.getReturnToRemarks());
                    oWorkflowDto.setPageChecking(pageChecking);
                    LoggerUtility.log("INFO", sClassName, sMethodName, "10 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
                   iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
                    
                    if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE ) {
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Successfully");
                    } else  if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ZERO ) {
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed - But email could not be sent. Please send email manually.");
                    } else {
                        LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Failed");
                    }
                    LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Workflow process completed - - -");
                }//RFQ Loop Ends
            } else {
                /* Setting error details to request to display the same in Exception page */ 
                request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_INVALIDTOKENMESSAGE);
                
                /* Forwarding request to Error page */
                return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
            }
    	} catch(Exception e) {
            /*
             * Logging any exception that raised during this activity in log files using Log4j
             */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "home", ExceptionUtility.getStackTraceAsString(e));
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        } finally {
            /* Releasing/closing the connection object */
            if ( conn!=null )
                oDBUtility.releaseResources(conn);
        }
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
        
        sLinkMsg2 = QuotationConstants.QUOTATION_VIEWSR_FVIEW_URL+QuotationConstants.QUOTATION_VIEWLINKS;        
        sLinkMsg1 = QuotationConstants.QUOTATION_VIEWINQ_VAREQURL+oEnquiryForm.getEnquiryId()+QuotationConstants.QUOTATION_GRSYMBOL+QuotationConstants.QUOTATION_VIEWLINK; 
        
        sLinkMsg = sLinkMsg1 + QuotationConstants.QUOTATION_BREAK + sLinkMsg2;
        request.setAttribute(QuotationConstants.QUOTATION_HEADINGMESSAGE, QuotationConstants.QUOTATION_RET_SUCES);
        request.setAttribute(QuotationConstants.QUOTATION_SUCCESSMESSAGE, sMsg);
        request.setAttribute(QuotationConstants.QUOTATION_LINKSMESSAGE, sLinkMsg);
        
		
        
         return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM); 
    }
    
    /* (non-Javadoc)
     * @see in.com.rbc.quotation.enquiry.dto.EnquiryDto#getPriceListDataFromRequest(javax.servlet.http.HttpServletRequest, in.com.rbc.quotation.enquiry.dto.EnquiryDto)
     * This method is used to for getting the calculated price details from the request
     */
    private EnquiryDto getPriceListDataFromRequest(HttpServletRequest request, EnquiryDto oEnquiryDto){
    	//    	 get the max request size
		int iMaxRatingSize = CommonUtility.getIntParameter(request, QuotationConstants.QUOTATION_RMAXSIZE);       		
		ArrayList arlRatingFactorList =null;
		KeyValueVo oKeyValue = null;
		int iAddOnMaxSize =QuotationConstants.QUOTATION_LITERAL_ZERO;
		String sAddOnPrice =null;
		StringBuffer sbAddOnPriceValues =null;
		if (iMaxRatingSize > QuotationConstants.QUOTATION_LITERAL_ZERO){
			arlRatingFactorList = new ArrayList();
			oKeyValue = null;
			for (int i = 1; i <= iMaxRatingSize; i++) {        				
				oKeyValue = new KeyValueVo();        				
				oKeyValue.setKey(CommonUtility.getStringParameter(request, QuotationConstants.QUOTATION_RALIST+i));
				oKeyValue.setValue(CommonUtility.getStringParameter(request, QuotationConstants.QUOTATION_NETPRICE+i));
				
				
				iAddOnMaxSize =  CommonUtility.getIntParameter(request, QuotationConstants.QUOTATION_ADMAXSIZE);        				
				
				sbAddOnPriceValues = new StringBuffer();
				for (int j = 1; j <= iAddOnMaxSize; j++) {
					sAddOnPrice = CommonUtility.getStringParameter(request, QuotationConstants.QUOTATION_ADDONNETPRICE+i+"_"+j);
					if (sAddOnPrice.trim().length() > QuotationConstants.QUOTATION_LITERAL_ZERO){
						sbAddOnPriceValues.append(sAddOnPrice);
						sbAddOnPriceValues.append(QuotationConstants.QUOTATION_STRING_COMMA);
						
					}       					
				}
				
				if (sbAddOnPriceValues.toString().trim().endsWith(QuotationConstants.QUOTATION_STRING_COMMA))        	
					sbAddOnPriceValues = new StringBuffer(sbAddOnPriceValues.toString().trim().substring(0, sbAddOnPriceValues.toString().trim().lastIndexOf(QuotationConstants.QUOTATION_STRING_COMMA)));       
			    
				oKeyValue.setValue2(sbAddOnPriceValues.toString());
				arlRatingFactorList.add(oKeyValue);    				
			}
			oEnquiryDto.setRatingFactorList(arlRatingFactorList);
		}
    	return oEnquiryDto;
    }
    
    /* (non-Javadoc)
     * @see java.util.ArrayList#getAddOnDetails(int, int, javax.servlet.http.HttpServletRequest)
     * This method is used to for getting add on detail field values from the request
     */
    private ArrayList getAddOnDetails(int iRowSize, int iRatingId, HttpServletRequest request) {
		ArrayList arlAddOnList = new ArrayList();
		String sAddOnType ="";
		int iQtyRejected =QuotationConstants.QUOTATION_LITERAL_ZERO;
		double dUnitPrice =0.0;
		AddOnDto oAddOnDto =null;
		for (int i = 1; i <= iRowSize; i++) {
			 sAddOnType = CommonUtility.getStringParameter(request, QuotationConstants.QUOTATION_UNITPRICE+i); 
			iQtyRejected = CommonUtility.getIntParameter(request, QuotationConstants.QUOTATION_ADDONQUANTITY+i); 
			dUnitPrice = CommonUtility.getDoubleParameter(request, QuotationConstants.QUOTATION_ADDONUNITPRICE+i); 			
			
			
			if (sAddOnType.trim().length() > QuotationConstants.QUOTATION_LITERAL_ZERO || 
				iQtyRejected > QuotationConstants.QUOTATION_LITERAL_ZERO || 
				dUnitPrice > QuotationConstants.QUOTATION_LITERAL_ZERO) {
				oAddOnDto = new AddOnDto();
				oAddOnDto.setAddOnRatingId(iRatingId);
				oAddOnDto.setAddOnTypeText(sAddOnType);
				oAddOnDto.setAddOnQuantity(iQtyRejected);
				oAddOnDto.setAddOnUnitPrice(dUnitPrice);
				
				arlAddOnList.add(oAddOnDto);
			}
		}

		return arlAddOnList;
	}
    /**
     * This method is used to update term values of Quote and release it to customer.
     * 
     * @param actionMapping
     *            ActionMapping object to redirect the user to next page
     *            depending on the activity
     * @param actionForm
     *            ActionForm object to handle the form elements
     * @param request
     *            HttpServletRequest object to handle request operations
     * @param response
     *            HttpServletResponse object to handle response operations
     * @return returns ActionForward depending on which user is redirected to
     *         next page
     * @throws Exception
     *             if any error occurs while performing database operations, etc
     * @author 100006718 <br>
     * Created on: OCT 15, 2008
     */
    public ActionForward emailQuoteToCustomer(ActionMapping actionMapping,
                              ActionForm actionForm, 
                              HttpServletRequest request,
                              HttpServletResponse response) throws Exception {
    	
    	String sMethodName = QuotationConstants.QUOTAION_EMAILTO_CUSTOMER;
    	String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        EnquiryDto oEnquiryDto = null;
    	EnquiryForm oEnquiryForm = (EnquiryForm) actionForm;
		if (oEnquiryForm == null)
			oEnquiryForm = new EnquiryForm();
		
    	Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        
        UserDto oUserDto = null;
        boolean isUpdated = QuotationConstants.QUOTATION_FALSE;
        UserAction oUserAction =null;
        DaoFactory oDaoFactory =null;
        QuotationDao oQuotationDao =null;
        PDFUtility oPDFUtility =null;
        String sFilePath =null;
        WorkflowUtility oWorkflowUtility =null;
        WorkflowDto oWorkflowDto =null;
        String sMsg=null;
        ArrayList alDmAttachmentList=null;
        KeyValueVo oKeyValueVo=null;
        String sAttachmentId=null;
        String sFileName=null;
        String sFileExten=null;
        String sAttachmentFileName=null;
        String sFinalEnquiryId=null;

        int iWorkflowExecuteStatus =QuotationConstants.QUOTATION_LITERAL_ZERO;
        
    	try {
    		if(isTokenValid(request)) 
        	{
        		resetToken(request);

       	
    		
    		/*
             * Ssetting user's information in request, using the header information
             * received through request
             */
            setRolesToRequest(request);
            LoggerUtility.log("INFO", sClassName, sMethodName, "Roles set to request.");
            
            /* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            
            /* Instantiating the UserAction object */
            oUserAction = new UserAction();
            
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
            
            /* Retrieving the WorkflowDao object */
            oQuotationDao = oDaoFactory.getQuotationDao();
    		
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            
            /* Retrieving the logged-in user information */
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            
            /* Creating an instance of of oEnquiryDto */
            oEnquiryDto = new EnquiryDto();
             
            /*Copying Properties from ActionForm To Dto Object*/
            PropertyUtils.copyProperties(oEnquiryDto, oEnquiryForm);    
            
            /*Setting Loged in UserID As LastModifiedBy */
            oEnquiryDto.setLastModifiedBy(oUserDto.getUserId());
            
            /*Updating Term Attribute of Quotation */
            isUpdated = oQuotationDao.updateQuotationTerms(conn,oEnquiryDto);
            
            /* Getting Complete Quotation Details From Databse*/
        	oEnquiryDto = oQuotationDao.getEnquiryDetails(conn, oEnquiryDto, QuotationConstants.QUOTATION_FORWARD_VIEW.toUpperCase());
        	
        	/*Checking whether the Quotation Term Details are Updated or Not */


            if(isUpdated) {
                oPDFUtility = new PDFUtility();
                
                sFilePath = oPDFUtility.generateQuotationPDF(oEnquiryDto,request);
                sFinalEnquiryId =QuotationConstants.QUOTATION_STRING_DOUBLEDIGZERO+oEnquiryDto.getEnquiryId()+QuotationConstants.QUOTATION_R1;
                sAttachmentFileName=QuotationConstants.QUOTATION_STRING_QUOTATION+QuotationConstants.QUOTATION_STRING_UNDERSC+
            			sFinalEnquiryId+QuotationConstants.QUOTATION_STRING_UNDERSC+
            			oEnquiryDto.getCustomerName()+QuotationConstants.QUOTATION_STRING_UNDERSC
                		+DateUtility.getCurrentYear() +QuotationConstants.QUOTATION_STRING_UNDERSC 
                		+ DateUtility.getCurrentMonth()+QuotationConstants.QUOTATION_PDFEXT;
            	
                /*
            	 * Get Dm Uploaded Attachment List
            	 * 
            	 */ 
                alDmAttachmentList=AttachmentUtility.getDMUploadedAttachmentDetails(oEnquiryDto);
            	if(alDmAttachmentList.size()>0)
            	{
            	 for(int i=0;i<alDmAttachmentList.size();i++)
            	 {
            		oKeyValueVo=(KeyValueVo)alDmAttachmentList.get(i); 
            		sAttachmentId=oKeyValueVo.getKey(); //Attachment Id
            		sFileName=oKeyValueVo.getValue(); //File Name
            		sFileExten=oKeyValueVo.getValue2(); //Extension
            		oEnquiryDto.setAttachmentFileName(sFileName+QuotationConstants.QUOTATION_PERIOD+sFileExten);
            		oEnquiryDto.setAttachmentId(Integer.parseInt(sAttachmentId.equals(null)?QuotationConstants.QUOTATION_STRING_ZERO:sAttachmentId));
            		sFilePath =sFilePath+QuotationConstants.QUOTATION_SPLITQSYMBOL+oPDFUtility.generateDmUploadedAttachments(oEnquiryDto,request);
            	
            		sAttachmentFileName=sAttachmentFileName+QuotationConstants.QUOTATION_SPLITQSYMBOL+sFileName+QuotationConstants.QUOTATION_PERIOD+sFileExten;
            	 }
            	}
                
                

                /*
                 * #############################################################
                 * Workflow process initiated
                 * #############################################################
                 */
                LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Initiating Workflow process - - -");

                oWorkflowUtility = new WorkflowUtility();
                oWorkflowDto = new WorkflowDto();
                
                oWorkflowDto.setEnquiryId(oEnquiryDto.getEnquiryId());
                oWorkflowDto.setEnquiryNo(oEnquiryDto.getEnquiryNumber());
                oWorkflowDto.setLoginUserId(oUserDto.getUserId());
                oWorkflowDto.setLoginUserName(oUserDto.getFullName());
                oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
                oWorkflowDto.setCcEmailTo(oUserDto.getEmailId());
                oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_RELEASETOCUSTOMER);
                oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGSM);
                oWorkflowDto.setAttachmentFile(sFilePath);
                oWorkflowDto.setAttachmentFileName(sAttachmentFileName);
                
                LoggerUtility.log("INFO", sClassName, sMethodName, "11 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
                iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
                if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE ){
                    LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Successfully");
                } else  if ( iWorkflowExecuteStatus== QuotationConstants.QUOTATION_LITERAL_ZERO){
                    LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed - But email could not be sent. Please send email manually.");
                } else {
                    LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Failed");
                }
                LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Workflow process completed - - -");
                /*
                 * #############################################################
                 * Workflow process completed
                 * #############################################################
                 */
                
                oPDFUtility.deletePDF(sFilePath);

                sMsg = QuotationConstants.QUOTATION_QUOTESUC;
                request.setAttribute(QuotationConstants.QUOTATION_HEADINGMESSAGE,QuotationConstants.QUOTATION_EMAILCUSTCONFIRM);
                request.setAttribute(QuotationConstants.QUOTATION_SUCCESSMESSAGE,sMsg);
                request.setAttribute(QuotationConstants.QUOTATION_LINKSMESSAGE,"");
            }
            
    	}else
    	{
    		   request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE,QuotationConstants.QUOTATION_INVALIDTOKENMESSAGE);
               
               /* Forwarding request to Error page */
               return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
           
    		
    	}
    	} catch(Exception e) {

            /*
             * Logging any exception that raised during this activity in log files using Log4j
             */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "home", ExceptionUtility.getStackTraceAsString(e));
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        } finally {
            /* Releasing/closing the connection object */
            if ( conn!=null ) 
                oDBUtility.releaseResources(conn);
        }
    	
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
    	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);
    }
	
    /**
     * This method is used to display the home page.
     * 
     * @param actionMapping
     *            ActionMapping object to redirect the user to next page
     *            depending on the activity
     * @param actionForm
     *            ActionForm object to handle the form elements
     * @param request
     *            HttpServletRequest object to handle request operations
     * @param response
     *            HttpServletResponse object to handle response operations
     * @return returns ActionForward depending on which user is redirected to
     *         next page
     * @throws Exception
     *             if any error occurs while performing database operations, etc
     * @author 100006126 <br>
     * Created on: Sep 30, 2008
     */
    public ActionForward viewReturnEnquiry(ActionMapping actionMapping,
                              ActionForm actionForm, 
                              HttpServletRequest request,
                              HttpServletResponse response) throws Exception {
    	
    	String sMethodName =QuotationConstants.QUOTAION_VIEWRETURNENQUIRY; 
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
    	Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        
        UserDto oUserDto = null;
        UserAction oUserAction =null;
        DaoFactory oDaoFactory =null;
        EnquiryForm oEnquiryForm = (EnquiryForm)actionForm;
        if(oEnquiryForm == null)
        	oEnquiryForm = new EnquiryForm();
      	
        EnquiryDto oEnquiryDto = null;
        QuotationDao oQuotationDao =null;
    	try {
            saveToken(request);
            
    		/*
             * Ssetting user's information in request, using the header information
             * received through request
             */
            setRolesToRequest(request);
            LoggerUtility.log("INFO", sClassName, sMethodName, "Roles set to request.");
            
            /* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            
            /* Instantiating the UserAction object */
            oUserAction = new UserAction();
            
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
            
            /* Retrieving the QuotationDao object */
            oQuotationDao = oDaoFactory.getQuotationDao();
    		
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            
            /* Retrieving the logged-in user information */
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            
            oEnquiryDto = new EnquiryDto();
            PropertyUtils.copyProperties(oEnquiryDto, oEnquiryForm);    
            
            TeamMemberDto oTeamMemberDto = new TeamMemberDto();            
    		oTeamMemberDto.setEnquiryId(oEnquiryForm.getEnquiryId());
    		oTeamMemberDto.setMembertype(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_CE_RSM_DM);
    		oTeamMemberDto = oQuotationDao.getTeamMemberDetails(conn, oTeamMemberDto);    		
    		oEnquiryForm.setTeamMemberList(oTeamMemberDto.getTeamMemberList());    
         } catch(Exception e) {
            /*
             * Logging any exception that raised during this activity in log files using Log4j
             */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "home", ExceptionUtility.getStackTraceAsString(e));
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        } finally {
            /* Releasing/closing the connection object */
            if ( conn!=null )
                oDBUtility.releaseResources(conn);
        }
    	
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
    	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_VIEWRETURNENQUIRY);
    }
    
    /**
     * This method is used to set the roles
     * @param actionMapping	ActionMapping object to redirect the user to next page depending on the activity
     * @param actionForm	ActionForm object to handle the form elements
     * @param request		HttpServletRequest object to handle request operations
     * @param response		HttpServletResponse object to handle response operations
     * @return returns 		ActionForward depending on which user is redirected to next page
     * @throws Exception	if any error occurs while performing database operations, etc
     */
    public ActionForward setRoles(ActionMapping actionMapping,
                              ActionForm actionForm, 
                              HttpServletRequest request,
                              HttpServletResponse response) throws Exception {
    	
    	String sMethodName = QuotationConstants.QUOTAION_SETROLES;
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        EnquiryForm oEnquiryForm = (EnquiryForm)actionForm;
        String sAssignToId = null;
        String sRole = null;
        
        if(oEnquiryForm == null)
        	oEnquiryForm = new EnquiryForm();
      	
    	try {
    		sAssignToId = CommonUtility.getStringParameter(request, "accesstoId");
    		//System.out.println("sAssignToId : " + sAssignToId);
    		sRole = oEnquiryForm.getOperationType();
    		//System.out.println("sRole = oEnquiryForm.getOperationType() : " + sRole);
    		
    		if (sAssignToId.trim().length() > QuotationConstants.QUOTATION_LITERAL_ZERO) {
    			// Set "sAssignToId" value as the "QUOTATION_SELECTED_SSOID" - To session variable
    			request.getSession().setAttribute(QuotationConstants.QUOTAION_SELE_SSOID, sAssignToId);
    			// Remove "sessionrole" from session variable
    			request.getSession().removeAttribute(QuotationConstants.QUOTATION_SESSION_ROLE);
    			// Remove "sessionuserinfo" from session variable
    			request.getSession().removeAttribute(QuotationConstants.QUOTATIONLT_SESSION_USERINFO);
    			// Remove Login User FullName from session variable
    			request.getSession().removeAttribute(QuotationConstants.QUOTATIONLT_LOGINUSER_FULLNAME);
    			
    			//System.out.println("Calling : setRolesToRequest");
    			// Invoke SetRoles - BaseAction - This value entered in "sAssignToId" will be set as the User and corresponding Roles will be loaded. 
    			setRolesToRequest(request);
    			//System.out.println("Finished : setRolesToRequest");
    			
    			// Now set the Login User FullName - To session variable.
    			//System.out.println("Calling : getLoggedInUserDetails(request)");
    			in.com.rbc.common.dto.UserDto oUserDto = getLoggedInUserDetails(request);
    			//System.out.println("Finished : getLoggedInUserDetails(request)");
    			//System.out.println("Values from UserDto  : " + oUserDto.toString());
    			//System.out.println(" UserDto - getFullName  : " + oUserDto.getFullName());
    			request.getSession().setAttribute(QuotationConstants.QUOTATIONLT_LOGINUSER_FULLNAME, oUserDto.getFullName());
    		}
    		
    		if (sRole.equalsIgnoreCase(QuotationConstants.QUOTATION_STRING_ONE)) {
    			request.getSession().setAttribute(QuotationConstants.QUOTAION_SELE_HEADINF, QuotationConstants.QUOTAION_RBCSECQUOTADMIN);
    		} else if (sRole.equalsIgnoreCase(QuotationConstants.QUOTATION_STRING_TWO)) {
    			request.getSession().setAttribute(QuotationConstants.QUOTAION_SELE_HEADINF, "");
    		}
         } catch(Exception e) {
            /*
             * Logging any exception that raised during this activity in log files using Log4j
             */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "home", ExceptionUtility.getStackTraceAsString(e));
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        } finally {}
    	
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
    	return new ActionForward(QuotationConstants.QUOTATION_HOME_URL); 
    }

    /**
    * This method is used to copy the Enquiry.
    * 
    * @param actionMapping
    *            ActionMapping object to redirect the user to next page
    *            depending on the activity
    * @param actionForm
    *            ActionForm object to handle the form elements
    * @param request
    *            HttpServletRequest object to handle request operations
    * @param response
    *            HttpServletResponse object to handle response operations
    * @return returns ActionForward depending on which user is redirected to
    *         next page
    * @throws Exception
    *             if any error occurs while performing database operations, etc
    * @author 100006126 <br>
    * Created on: Sep 30, 2008
    */
   public ActionForward createRevision(ActionMapping actionMapping,
                             ActionForm actionForm, 
                             HttpServletRequest request,
                             HttpServletResponse response) throws Exception {
   	    
   	    String sMethodName = "createRevision";
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        EnquiryDto oEnquiryDto = null;
        EnquiryDto oTempEnquiryDto = null;
   	    Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        
        int sOldEnquiryId = QuotationConstants.QUOTATION_LITERAL_ZERO;
        int sNewEnquiryId = QuotationConstants.QUOTATION_LITERAL_ZERO;
        
        UserDto oUserDto = null;
        UserAction oUserAction =null;
        DaoFactory oDaoFactory =null;
        QuotationDao oQuotationDao =null;
        WorkflowDto oWorkflowDto = null;
        WorkflowUtility oWorkflowUtility =null;
        int iWorkflowExecuteStatus =QuotationConstants.QUOTATION_LITERAL_ZERO;
   	    EnquiryForm oEnquiryForm = (EnquiryForm)actionForm;
		if (oEnquiryForm == null){
			oEnquiryForm = new EnquiryForm();
		}
		
        try {
       		if(isTokenValid(request)) {
        		resetToken(request);
        		
       		   /*
                * Ssetting user's information in request, using the header information
                * received through request
                */
               setRolesToRequest(request);
               LoggerUtility.log("INFO", sClassName, sMethodName, "Roles set to request.");
               
               /* Instantiating the DBUtility object */
               oDBUtility = new DBUtility();
               /* Instantiating the UserAction object */
               oUserAction = new UserAction();
               /* Creating an instance of of DaoFactory & retrieving UserDao object */
               oDaoFactory = new DaoFactory();
       		   
               /* Retrieving the database connection using DBUtility.getDBConnection method */
               conn = oDBUtility.getDBConnection();
               
               /* Retrieving the logged-in user information */
               oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
               
               oQuotationDao = oDaoFactory.getQuotationDao();
               oEnquiryDto = new EnquiryDto();
               
               PropertyUtils.copyProperties(oEnquiryDto, oEnquiryForm);
               
               sOldEnquiryId = oEnquiryDto.getEnquiryId(); //Old Enquiry Id
               
               
               oEnquiryDto = oQuotationDao.createRevision(conn, oEnquiryDto, oUserDto);
               
               request.setAttribute(QuotationConstants.QUOTATION_ENQUIRYDTO, oEnquiryDto); 
               request.setAttribute(QuotationConstants.QUOTATION_USERDTO, oUserDto); 
               
               if (oEnquiryDto == null) {
               	   /* Setting error details to request to display the same in Exception page */
                   request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_PROB_COPYREC);
                   
                   /* Forwarding request to Error page */
                   return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
               } else {
                   sNewEnquiryId = oEnquiryDto.getEnquiryId();
                   
                   /*
                    * #############################################################
                    * Workflow process initiated
                    * #############################################################
                    */
                   LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Initiating Workflow process - - -");
                   oWorkflowUtility = new WorkflowUtility();
                   oWorkflowDto = new WorkflowDto();
                   
                   oWorkflowDto.setEnquiryId(sNewEnquiryId);
                   oWorkflowDto.setBaseEnquiryId(sOldEnquiryId);
                   oWorkflowDto.setLoginUserId(oUserDto.getUserId());
                   oWorkflowDto.setLoginUserName(oUserDto.getFullName());
                   oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
                   oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_CREATEREVISION);
                   oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_RELEASED);
                   
                   LoggerUtility.log("INFO", sClassName, sMethodName, "12 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
                    iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
                   
                   if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE ) {
                       LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Successfully");
                   } else  if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ZERO ) {
                       LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed - But email could not be sent. Please send email manually.");
                   } else {
                       LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Failed");
                   }
                   
                   LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Workflow process completed - - -");
                   /*
                    * #############################################################
                    * Workflow process completed
                    * #############################################################
                    */
               }
       		} else {
                    request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE,QuotationConstants.QUOTATION_INVALIDTOKENMESSAGE);
                    
                    /* Forwarding request to Error page */
                    return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
               
       		}
        } catch(Exception e) {
           /*
            * Logging any exception that raised during this activity in log files using Log4j
            */
           LoggerUtility.log("DEBUG", this.getClass().getName(), "home", ExceptionUtility.getStackTraceAsString(e));
           
           /* Setting error details to request to display the same in Exception page */
           request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
           
           /* Forwarding request to Error page */
           return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
       } finally {
           /* Releasing/closing the connection object */
           if ( conn!=null )
               oDBUtility.releaseResources(conn);
       }
   	   
       LoggerUtility.log("INFO", sClassName, sMethodName, "END");
       return new ActionForward(QuotationConstants.QUOTATION_GET_ENQDET_URL+oEnquiryDto.getEnquiryId()); 
   }
   
   /**
    * This method is used download the attachment
    * 
    * @param actionMapping
    *            ActionMapping object to redirect the user to next page
    *            depending on the activity
    * @param actionForm
    *            ActionForm object to handle the form elements
    * @param request
    *            HttpServletRequest object to handle request operations
    * @param response
    *            HttpServletResponse object to handle response operations
    * @return returns ActionForward depending on which user is redirected to
    *         next page
    * @throws Exception
    *             if any error occurs while performing database operations, etc
    * @author 100006126 <br>
    * Created on: Sep 30, 2008
    */
   public ActionForward downloadAttachment(ActionMapping actionMapping,
           ActionForm actionForm, 
           HttpServletRequest request,
           HttpServletResponse response) throws Exception {  	

String sMethodName = QuotationConstants.QUOTAION_DOWNLOADATTACHMENT;
String sClassName = this.getClass().getName();
LoggerUtility.log("INFO", sClassName, sMethodName, "START");

Connection conn = null;  // Connection object to store the database connection
DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects 

int iAttachmentId = QuotationConstants.QUOTATION_LITERAL_ZERO;
DaoFactory oDaoFactory =null;

try {

/* Instantiating the DBUtility object */
oDBUtility = new DBUtility();


/* Creating an instance of of DaoFactory & retrieving UserDao object */
oDaoFactory = new DaoFactory();

/* Retrieving the database connection using DBUtility.getDBConnection method */
conn = oDBUtility.getDBConnection();

iAttachmentId = CommonUtility.getIntParameter(request, QuotationConstants.QUOTAION_ATTACHMENTID);

if (iAttachmentId > QuotationConstants.QUOTATION_LITERAL_ZERO){
	// set content-type to binary stream
	response.setContentType("application/octet-stream");
	response.setHeader("Content-disposition", "attachment; filename="+ CommonUtility.getStringParameter(request, "fileName")+"."+CommonUtility.getStringParameter(request, "fileType"));
	
	URL url = new URL(CommonUtility.getStringValue(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_ATTACHMENT_APPLICATION_URL)));
	Service service = new Service();            
	Call call = (Call) service.createCall();
	call.setTargetEndpointAddress(url);
	call.setOperationName(new QName("AttachmentService", "getAttachmentByBytes"));   				
	call.addParameter("attachmentId",org.apache.axis.encoding.XMLType.XSD_INT,ParameterMode.IN);
	call.setReturnType( org.apache.axis.encoding.XMLType.XSD_BASE64 );
	
	byte[] returnBytes = (byte[])call.invoke( new Object[] {iAttachmentId});
	
	if (returnBytes != null && returnBytes.length > QuotationConstants.QUOTATION_LITERAL_ZERO){   					
	 OutputStream outStream = response.getOutputStream();
	 if (outStream != null) {
	outStream.write(returnBytes);
	outStream.flush(); 
	outStream.close();
	 }
	} 				
}

} catch(Exception e) {
/*
* Logging any exception that raised during this activity in log files using Log4j
*/
LoggerUtility.log("DEBUG", this.getClass().getName(), "home", ExceptionUtility.getStackTraceAsString(e));

/* Setting error details to request to display the same in Exception page */
request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));

/* Forwarding request to Error page */
return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
} finally {
/* Releasing/closing the connection object */
if ( conn!=null )
oDBUtility.releaseResources(conn);
}

LoggerUtility.log("INFO", sClassName, sMethodName, "END");
return null;
}
   
   
   /**
    * This method checks whether to forward the user to consolidated enquiry view page or to forward to other pages based on status id
    * 
    * @param actionMapping
    *            ActionMapping object to redirect the user to next page
    *            depending on the activity
    * @param actionForm
    *            ActionForm object to handle the form elements
    * @param request
    *            HttpServletRequest object to handle request operations
    * @param response
    *            HttpServletResponse object to handle response operations
    * @return returns ActionForward depending on which user is redirected to
    *         next page
    * @throws Exception
    *             if any error occurs while performing database operations, etc
    * @author 100006126 <br>
    * Created on: Sep 30, 2008
    */
   public ActionForward validateRequestAction(ActionMapping actionMapping,
                             ActionForm actionForm, 
                             HttpServletRequest request,
                             HttpServletResponse response) throws Exception {
	   
	   
	   	String sMethodName = QuotationConstants.QUOTATION_VALREQACT;
	    String sClassName = this.getClass().getName();
	    LoggerUtility.log("INFO", sClassName, sMethodName, "START");

	   	Connection conn = null;  // Connection object to store the database connection
	    DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
	    
	    UserDto oUserDto = null;
	    EnquiryForm oEnquiryForm = (EnquiryForm)actionForm;
	    if(oEnquiryForm == null)
	    	oEnquiryForm = new EnquiryForm();
     	
        EnquiryDto oEnquiryDto = null;
        WorkflowDto oWorkflowDto = null;
        UserAction oUserAction =null;
        DaoFactory oDaoFactory =null;
        QuotationDao oQuotationDao =null;
        boolean isValidUser = QuotationConstants.QUOTATION_FALSE;
        int iIndex =QuotationConstants.QUOTATION_LITERAL_ZERO;
        String sPreviousId=null;
        String sPreviousStatus=null;
        StringBuffer sBuffer = null;
        String sNextId=null;
        String sNextStatus=null;
        ArrayList arlSessionList =new ArrayList();
        try {
        	HttpSession session = request.getSession();
           /*
            * Ssetting user's information in request, using the header information
            * received through request
            */
           setRolesToRequest(request);
           LoggerUtility.log("INFO", sClassName, sMethodName, "Roles set to request.");
           
           /* Instantiating the DBUtility object */
           oDBUtility = new DBUtility();
           
           /* Instantiating the UserAction object */
           oUserAction = new UserAction();
           
           /* Creating an instance of of DaoFactory & retrieving UserDao object */
           oDaoFactory = new DaoFactory();
           
           /* Retrieving the QuotationDao object */
           oQuotationDao = oDaoFactory.getQuotationDao();
   		
           /* Retrieving the database connection using DBUtility.getDBConnection method */
           conn = oDBUtility.getDBConnection();
           
           /* Retrieving the logged-in user information */
           oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
           
           // get the enquiryId, statusId from the request and set to the form object
           oEnquiryForm.setEnquiryId(CommonUtility.getIntParameter(request, QuotationConstants.QUOTATION_REQUEST_ENQUIRYID));
           oEnquiryForm.setStatusId(CommonUtility.getIntParameter(request, QuotationConstants.QUOTATION_REQUEST_STATUSID)); 
           
           oEnquiryDto = new EnquiryDto();
           PropertyUtils.copyProperties(oEnquiryDto, oEnquiryForm);
           
           
           oWorkflowDto = new WorkflowDto();
		   oWorkflowDto.setEnquiryId(oEnquiryForm.getEnquiryId());
		   oWorkflowDto.setAssignedTo(oUserDto.getUserId());
		   oWorkflowDto.setWorkflowStatus(QuotationConstants.QUOTATION_WORKFLOW_ACTION_PENDING);		   
		   
		   if (oEnquiryDto.getStatusId() ==QuotationConstants.QUOTATION_LITERAL_ZERO ){
			   oEnquiryDto = oQuotationDao.getEnquiryStatus(conn, oEnquiryDto);		
			   oEnquiryForm.setStatusId(oEnquiryDto.getStatusId());
		   }
		   
           /*
            * Code for displaying the paging part
            */
           arlSessionList = (ArrayList) session.getAttribute(QuotationConstants.QUOTATION_LIST_RESULTSLIST);
         
           if (arlSessionList != null && CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_INDEX) != null )
           {     
           	
           	   // display the paging part only if the below attribute exists in the request
               request.setAttribute(QuotationConstants.QUOTATION_PAGING_DISPLAYPAGING ,QuotationConstants.QUOTATION_SESSION_ADMIN_FLAG_TRUE); 

		   
               iIndex = CommonUtility.getIntParameter(request,QuotationConstants.QUOTATION_INDEX);                    
           request.setAttribute(QuotationConstants.QUOTATION_PAGING_CURRENTINDEX,""+iIndex);                    

           if (iIndex > QuotationConstants.QUOTATION_LITERAL_ZERO && arlSessionList.get(iIndex-1) != null){	
           	sPreviousId = ""+((SearchEnquiryDto ) arlSessionList.get(iIndex-1)).getEnquiryId();  
           	sPreviousStatus = ""+((SearchEnquiryDto ) arlSessionList.get(iIndex-1)).getStatusId();   
           	request.setAttribute(QuotationConstants.QUOTATION_PAGING_PREVIOUSID,sPreviousId);
           	request.setAttribute(QuotationConstants.QUOTATION_PRE_STA,sPreviousStatus); 
           	request.setAttribute(QuotationConstants.QUOTATION_PAGING_PREVIOUSINDEX, ""+(iIndex-1));
           }

           sBuffer = new StringBuffer();
           sBuffer.append("Request ");
           sBuffer.append(iIndex+1);
           sBuffer.append(" / ");
           sBuffer.append(arlSessionList.size());              

           request.setAttribute(QuotationConstants.QUOTATION_PAGING_DISPLAY_MESSAGE,sBuffer.toString());

           if ( ((iIndex + 1)  != arlSessionList.size()) && (arlSessionList.get(iIndex+1) != null) ){                    	
            sNextId = ""+((SearchEnquiryDto ) arlSessionList.get(iIndex+1)).getEnquiryId();                	
           	sNextStatus = ""+((SearchEnquiryDto ) arlSessionList.get(iIndex+1)).getStatusId();  
           	request.setAttribute(QuotationConstants.QUOTATION_PAGING_NEXTID,sNextId);
           	request.setAttribute(QuotationConstants.QUOTATION_NEX_STA,sNextStatus); 
           	request.setAttribute(QuotationConstants.QUOTATION_PAGING_NEXTINDEX, ""+(iIndex+1));
           }                	
      }
           
           if (oEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_DRAFT){
        	   /*
        	    * now check for logged-in user role, in this case the user must be of SM type
        	    * If the user is SM then forward the request to draft view page else to consolidated view page
        	    */ 
        	   isValidUser = oQuotationDao.checkUserRole(conn, oEnquiryDto, oUserDto, QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_SM);
        	 
        	   
        	   if (isValidUser){
        		   return new ActionForward(QuotationConstants.QUOTATION_GET_ENQDET_URL+oEnquiryForm.getEnquiryId()); 
        	   }else{
        		   /* Setting error details to request to display the same in Exception page */
                   request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_MSG_UNAUTH);
                   
                   /* Forwarding request to Error page */
                   return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        	   }        	   
           }
           //Send Retunenquriy to Dm
           else if (oEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_ReturnSM){
        	   /*
        	    * now check for logged-in user role, in this case the user must be of SM type
        	    * If the user is SM then forward the request to draft view page else to consolidated view page
        	    */ 
        	   isValidUser = oQuotationDao.checkUserRole(conn, oEnquiryDto, oUserDto, QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_SM);
        	 
        	   
        	   if (isValidUser){
        		   return new ActionForward(QuotationConstants.QUOTATION_RETRUNDM_URL+oEnquiryForm.getEnquiryId()); 
        	   }else{
        		   /* Setting error details to request to display the same in Exception page */
                   request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_MSG_UNAUTH);
                   
                   /* Forwarding request to Error page */
                   return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        	   }        	   
           }
           
           
           else if (oEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGDM){
        	   /*
        	    * now check for logged-in user role, in this case the user must be of DM/DE type
        	    * If the user is DM/DE then check if the workflow status of this user for this enquiry is Pending or not 
        	    * In both the checks on success the request will be forwarded to offer data page else to consolidated view page
        	    */ 
        	   isValidUser = oQuotationDao.checkUserRole(conn, oEnquiryDto, oUserDto, QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_DM);        	  
        	   if (isValidUser){
        		   isValidUser = oQuotationDao.checkForWorkflowPendingStatus(conn, oWorkflowDto);
        		   
        		   
        		   if (isValidUser){
        			   return new ActionForward(QuotationConstants.QUOTATION_VIEWOFFERD_URL+oEnquiryForm.getEnquiryId()); 
        		   }else{
        			   return new ActionForward(QuotationConstants.QUOTATION_VIEWOFFCONSOL_URL+oEnquiryForm.getEnquiryId()); 
        		   }        		   
        	   }else{
        		  return new ActionForward(QuotationConstants.QUOTATION_VIEWOFFCONSOL_URL+oEnquiryForm.getEnquiryId());  
        	   }        	   
           } else if (oEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGCE){
        	   /*
        	    * now check for logged-in user role, in this case the user must be of CE type
        	    * If the user is CE then check if the workflow status of this user for this enquiry is Pending or not 
        	    * In both the checks on success the request will be forwarded to estimate offer data page else to consolidated view page
        	    */ 
        	   isValidUser = oQuotationDao.checkUserRole(conn, oEnquiryDto, oUserDto, QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_CE);
        	   if (isValidUser){       		   
        		   isValidUser = oQuotationDao.checkForWorkflowPendingStatus(conn, oWorkflowDto);
        		   
        		   if (isValidUser){
        			   return new ActionForward(QuotationConstants.QUOTATION_VIEWENQ_EST_URL1+oEnquiryForm.getEnquiryId()); 
        		   }else{
        			   return new ActionForward(QuotationConstants.QUOTATION_VIEWOFFCONSOL_URL+oEnquiryForm.getEnquiryId()); 
        		   }        		   
        	   }else{
        		  return new ActionForward(QuotationConstants.QUOTATION_VIEWOFFCONSOL_URL+oEnquiryForm.getEnquiryId()); 
        	   }        	   
           } else if (oEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGRSM){
        	   /*
        	    * now check for logged-in user role, in this case the user must be of RSM type
        	    * If the user is RSM then check if the workflow status of this user for this enquiry is Pending or not 
        	    * In both the checks on success the request will be forwarded to refactor offer data page else to consolidated view page
        	    */ 
        	   isValidUser = oQuotationDao.checkUserRole(conn, oEnquiryDto, oUserDto, QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_RSM);
        	   if (isValidUser){       		   
        		   isValidUser = oQuotationDao.checkForWorkflowPendingStatus(conn, oWorkflowDto);
        		   if (isValidUser){
        			   return new ActionForward(QuotationConstants.QUOTATION_VIEWENQ_REFACT_URL+oEnquiryForm.getEnquiryId()); 
        		   }else{
        			   return new ActionForward(QuotationConstants.QUOTATION_VIEWOFFCONSOL_URL+oEnquiryForm.getEnquiryId()); 
        		   }        		   
        	   }else{
        		  return new ActionForward(QuotationConstants.QUOTATION_VIEWOFFCONSOL_URL+oEnquiryForm.getEnquiryId()); 
        	   }        	   
           } else if (oEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGSM){
        	   /*
        	    * now check for logged-in user role, in this case the user must be of SM type
        	    * If the user is SM then check if the workflow status of this user for this enquiry is Pending or not 
        	    * In both the checks on success the request will be forwarded to refactor offer data page else to consolidated view page
        	    */ 
        	   isValidUser = oQuotationDao.checkUserRole(conn, oEnquiryDto, oUserDto, QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_SM);
        	   if (isValidUser){       		   
        		   isValidUser = oQuotationDao.checkForWorkflowPendingStatus(conn, oWorkflowDto);
        		   if (isValidUser){
        			   return new ActionForward(QuotationConstants.QUOTATION_VIEWENQ_URL+oEnquiryForm.getEnquiryId()); 
        		   }else{
        			   return new ActionForward(QuotationConstants.QUOTATION_VIEWOFFCONSOL_URL+oEnquiryForm.getEnquiryId()); 
        		   }        		   
        	   }else{
        		  return new ActionForward(QuotationConstants.QUOTATION_VIEWOFFCONSOL_URL+oEnquiryForm.getEnquiryId()); 
        	   }        	   
           } else if ( (oEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_RELEASED) 
        		   		|| (oEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_SUPERSEDED)){
        	   
        	   oEnquiryDto.setAlWinChanceList(oQuotationDao.getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_WINCH));
        	   oEnquiryDto.setAlTargetedList(oQuotationDao.getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_TARGTD));
        	   PropertyUtils.copyProperties(oEnquiryForm, oEnquiryDto);
        	   /*
        	    * Directly forward the request to consolidated view page
        	    */
        	   return new ActionForward(QuotationConstants.QUOTATION_VIEWOFFCONSOL_URL+oEnquiryForm.getEnquiryId());        	         	   
           }
           /*By Checking the Status it will goes to Commercial Stage */
           else if (oEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_WONPENDINGSM){
        	   /*
        	    * now check for logged-in user role, in this case the user must be of DM/DE type
        	    * If the user is DM/DE then check if the workflow status of this user for this enquiry is Pending or not 
        	    * In both the checks on success the request will be forwarded to offer data page else to consolidated view page
        	    */ 

        	   isValidUser = oQuotationDao.checkUserRole(conn, oEnquiryDto, oUserDto, QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_CM);        	  
        	   if (isValidUser){
        		   isValidUser = oQuotationDao.checkForWorkflowPendingStatus(conn, oWorkflowDto);
        		   if (isValidUser){
        			   return new ActionForward(QuotationConstants.QUOTATION_VIEWINDENTOFFERD_URL+oEnquiryForm.getEnquiryId()); 
        		   }else{
        			   return new ActionForward(QuotationConstants.QUOTATION_VIEWOFFINDENTPAGE_URL+oEnquiryForm.getEnquiryId()); 
        		   }        		   
        	   }else{
        		  return new ActionForward(QuotationConstants.QUOTATION_VIEWOFFINDENTPAGE_URL+oEnquiryForm.getEnquiryId());  
        	   }        	   
           }
           /*By Checking the Status it will goes to Commercial Stage */
           else if (oEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_WONPENDINGCM)
           {
        	   /*
        	    * now check for logged-in user role, in this case the user must be of DM/DE type
        	    * If the user is DM/DE then check if the workflow status of this user for this enquiry is Pending or not 
        	    * In both the checks on success the request will be forwarded to offer data page else to consolidated view page
        	    */ 

        	   isValidUser = oQuotationDao.checkUserRole(conn, oEnquiryDto, oUserDto, QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_CM);        	  
        	   if (isValidUser){
        		   isValidUser = oQuotationDao.checkForWorkflowPendingStatus(conn, oWorkflowDto);
        		   if (isValidUser){
        			   return new ActionForward(QuotationConstants.QUOTATION_VIEWINDENTOFFERD_URL+oEnquiryForm.getEnquiryId()); 
        		   }else{
        			   return new ActionForward(QuotationConstants.QUOTATION_VIEWOFFINDENTPAGE_URL+oEnquiryForm.getEnquiryId()); 
        		   }        		   
        	   }else{
        		  return new ActionForward(QuotationConstants.QUOTATION_VIEWOFFINDENTPAGE_URL+oEnquiryForm.getEnquiryId());  
        	   }        	   
   
           }

           
           /*By Checking the Status it will goes to Design Stage */
           else if (oEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_WONPENDINGDM)
           {
        	   /*
        	    * now check for logged-in user role, in this case the user must be of DM/DE type
        	    * If the user is DM/DE then check if the workflow status of this user for this enquiry is Pending or not 
        	    * In both the checks on success the request will be forwarded to offer data page else to consolidated view page
        	    */ 

        	   isValidUser = oQuotationDao.checkUserRole(conn, oEnquiryDto, oUserDto, QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_DM);        	  
        	   if (isValidUser){
        		   isValidUser = oQuotationDao.checkForWorkflowPendingStatus(conn, oWorkflowDto);
        		   if (isValidUser){
        			   return new ActionForward(QuotationConstants.QUOTATION_VIEWINDENTDESIGNOFFERD_URL+oEnquiryForm.getEnquiryId()); 
        		   }else{
        			   return new ActionForward(QuotationConstants.QUOTATION_VIEWOFFINDENTPAGE_URL+oEnquiryForm.getEnquiryId()); 
        		   }        		   
        	   }else{
        		  return new ActionForward(QuotationConstants.QUOTATION_VIEWOFFINDENTPAGE_URL+oEnquiryForm.getEnquiryId());  
        	   }        	   
   
           }
           /*By Checking the Status it will goes to View Won Details  */
           else if (oEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_WON)
           {

        		  return new ActionForward(QuotationConstants.QUOTATION_VIEWOFFINDENTPAGE_URL+oEnquiryForm.getEnquiryId());  
        	        	   
   
           }
           /*By Checking the Status it will goes to View Won Details  */
           else if (oEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_LOST)
           {

        		  return new ActionForward(QuotationConstants.QUOTATION_VIEWOFFINDENTPAGE_URL+oEnquiryForm.getEnquiryId());  
        	        	   
   
           }

           /*By Checking the Status it will goes to View Won Details  */
           else if (oEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_ABONDENT)
           {

        		  return new ActionForward(QuotationConstants.QUOTATION_VIEWOFFINDENTPAGE_URL+oEnquiryForm.getEnquiryId());  
        	        	   
   
           }



           
        } catch(Exception e) {
           /*
            * Logging any exception that raised during this activity in log files using Log4j
            */
           LoggerUtility.log("DEBUG", this.getClass().getName(), "home", ExceptionUtility.getStackTraceAsString(e));
           
           /* Setting error details to request to display the same in Exception page */
           request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
           
           /* Forwarding request to Error page */
           return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
       } finally {
           /* Releasing/closing the connection object */
           if ( conn!=null )
               oDBUtility.releaseResources(conn);
       }
   	
       LoggerUtility.log("INFO", sClassName, sMethodName, "END");
       /* Setting error details to request to display the same in Exception page */
       request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_MSG_UNAUTH);
       
       /* Forwarding request to Error page */
       return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
   }//Ending Validate Action
   
   private String addAttachment(FormFile oImageFile, String sFileDesc, int iEnquiryId, UserDto oUserDto) throws Exception{
	   
	   String sValidateAttachment =null;
		String sFileName=null;
		String sFileType=null;
		String[] sFileTypes = null;

	   
	   if (oImageFile != null && oImageFile.getFileSize() > QuotationConstants.QUOTATION_LITERAL_ZERO){
		
		   
			// step1. validate this attachment
			sValidateAttachment = AttachmentUtility.validateAttachment(iEnquiryId, oImageFile);
			
			if (sValidateAttachment == null)
				sValidateAttachment = "";
			
			//validation failed
			if ( (sValidateAttachment.trim().length() == QuotationConstants.QUOTATION_LITERAL_ZERO) || 
					(! sValidateAttachment.equalsIgnoreCase(QuotationConstants.QUOTATION_VALID)) ) 
			{
				return sValidateAttachment;

			}else{            				
				// go for upload
				
				
				sFileName="";
				sFileType="";
				sFileTypes = oImageFile.getFileName().split("\\.");
				if (sFileTypes.length ==QuotationConstants.QUOTATION_LITERAL_TWO ){
					sFileType = sFileTypes[1]; 
				}
				if(sFileType.equals(QuotationConstants.QUOTAION_MSG))
				{
					sFileName=QuotationConstants.QUOTAION_MSOUTLOOKMSG;
				}
				else
				{
					sFileName=oImageFile.getFileName();
				}


				
				//set all the required properties to dto object
				AttachmentDto oAttachmentDto = new AttachmentDto();
				
                oAttachmentDto.setApplicationId(CommonUtility.getIntValue(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_ATTACHMENT_APPLICATIONID)));

                oAttachmentDto.setFileName(sFileName);
                oAttachmentDto.setFileBytes(oImageFile.getFileData());
				oAttachmentDto.setFileSize(oImageFile.getFileSize());
				
				oAttachmentDto.setApplicationUniqueId(""+iEnquiryId);
				oAttachmentDto.setAttachedBy(Integer.parseInt(oUserDto.getUserId()));
				oAttachmentDto.setAttachedByName(oUserDto.getFullName());            				
				oAttachmentDto.setFileDescription(sFileDesc);
				
				boolean isUploaded = AttachmentUtility.uploadAttachment(oAttachmentDto);
				if (! isUploaded){
					return QuotationConstants.QUOTATION_FILEUPLOADF;
				}
			}
		}
	   return "";
	   }
   /* (non-Javadoc)
    * @see java.util.ArrayList#getDeliveryDetails(int, int, javax.servlet.http.HttpServletRequest)
    * This method is used to for getting Delivery  details field values from the request
    */
   private ArrayList getDeliveryDetails(int iRowSize, int iRatingId, HttpServletRequest request) {
		ArrayList alDeliveryDetailsList = new ArrayList();
		String sDeliveryLocation ="";
		String sDeliveryAddress="";
	    int iNoOfMotors=QuotationConstants.QUOTATION_LITERAL_ZERO;
		int iQtyRejected =QuotationConstants.QUOTATION_LITERAL_ZERO;
		double dUnitPrice =QuotationConstants.QUOTATION_DOUBLE_ZERO;
		DeliveryDetailsDto oDeliveryDetails =null;
		for (int idx = 1; idx <= iRowSize; idx++) {
			sDeliveryLocation = CommonUtility.getStringParameter(request, QuotationConstants.QUOTATION_DELIVERYLOCATION+idx); 
			sDeliveryAddress = CommonUtility.getStringParameter(request, QuotationConstants.QUOTATION_DELIVERYADDRESS+idx); 
			iNoOfMotors = CommonUtility.getIntParameter(request, QuotationConstants.QUOTATION_NOOFMOTORS+idx); 			
			
			if (sDeliveryLocation.trim().length() > QuotationConstants.QUOTATION_LITERAL_ZERO && 
				sDeliveryAddress.trim().length() > QuotationConstants.QUOTATION_LITERAL_ZERO && 
				iNoOfMotors > QuotationConstants.QUOTATION_LITERAL_ZERO) {
				oDeliveryDetails = new DeliveryDetailsDto();
				oDeliveryDetails.setDeliveryRatingId(iRatingId);
				oDeliveryDetails.setDeliveryLocation(sDeliveryLocation);
				oDeliveryDetails.setDeliveryAddress(sDeliveryAddress);
				oDeliveryDetails.setNoofMotors(iNoOfMotors);
				
				alDeliveryDetailsList.add(oDeliveryDetails);
			}
		}
		return alDeliveryDetailsList;
	}
   
   /**
    * This method is used update the requestion information in released to Customer page
    * 
    * @param sEnquiryId
    *             String Object which contains sEnquiryId value
    * @param sOrderClosingDt
    *            String Object which contains OrderClosingDt value
    * @param sTargeted
    *            String Object which contains Targeted value 
    * @param sWinningChance
    *            String Object which contains WinningChance value 
    * @return returns String Object 'YES' if update success
    *         
    * @throws Exception
    *             if any error occurs while performing database operations, etc
    * @author 610092227 <br>
    * Created on: July 3, 2019
    */
   public String updateBasicInfo(String sEnquiryId, String sOrderClosingDt, String sTargeted, String sWinningChance ) throws Exception {
	   
   		String sMethodName = "updateBasicInfo";
   		String sClassName = this.getClass().getName();
   		Connection conn = null;  // Connection object to store the database connection
   		DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
       
   		String sOperationType = null;       
   		DaoFactory oDaoFactory = null;
   		EnquiryDto oEnquiryDto = null;      
   		QuotationDao oQuotationDao = null;
       
   		try{
    	   oEnquiryDto =  new EnquiryDto();
    	   // Getting request parameters and setting to the oEnquiryDto object.
    	   if(sEnquiryId!=null &&sEnquiryId !="" )
    		   oEnquiryDto.setEnquiryId(Integer.parseInt(sEnquiryId));          
           oEnquiryDto.setOrderClosingMonth(sOrderClosingDt);
           oEnquiryDto.setWinningChance(sWinningChance);           
           oEnquiryDto.setTargeted(sTargeted);
           
           oDaoFactory = new DaoFactory();
   		
   		   /* Instantiating the DBUtility object */
           oDBUtility = new DBUtility();
           /* Retrieving the database connection using DBUtility.getDBConnection method */
           conn = oDBUtility.getDBConnection();
           
           oQuotationDao = oDaoFactory.getQuotationDao();
           
           if( oQuotationDao.updateEnquiryBasicInfo(conn,oEnquiryDto))
        	  return QuotationConstants.QUOTATION_YES;
           else
        	  return QuotationConstants.QUOTATION_NO;
           
   		} catch(Exception e) {
           /* Logging any exception that raised during this activity in log files using Log4j */
           LoggerUtility.log("DEBUG", this.getClass().getName(), "home", ExceptionUtility.getStackTraceAsString(e));
           
           return QuotationConstants.QUOTATION_NO;
           
   		} finally {
           /* Releasing/closing the connection object */
           if ( conn!=null )
               oDBUtility.releaseResources(conn);
   		}
       
	

   }
   
   /**
	* This method is used to save the calculated total motor, package price and also include & extra for enquiry, rating in indent page
	* @param conn Connection object to connect to database
	* @param EnquiryDto oEnquiryDto object having Enquiry details
	* @return Returns none
	* @author 610092227
	* Created on: 05 Sept 2019
	*/
   
   private void calculateSaveTmpPkgPrice(Connection conn, EnquiryDto oEnquiryDto){
	   
	  ArrayList arlRatingObjList = null;
	  RatingDto oRatingDto = null;
	  TechnicalOfferDto oTechnicalOfferDto = null;
	  int iQty = 0;
	  double dRatingTotalPrice = 0.0;
	  double dRatingTotalMotorPrice = 0.0;
	  double dRatingTotalPkgPrice = 0.0;	 
	  double dEnqTotalMotorPrice = 0.0;
	  double dEnqTotalPkgPrice = 0.0;
	  double dEnqIncFrieght = 0.0;
	  double dEnqIncExWrnty = 0.0;
	  double dEnqIncSupCom = 0.0;
	  double dEnqIncTypeTestChrgs = 0.0;
	  double dEnqExcFrieght = 0.0;
	  double dEnqExcExWrnty = 0.0;
	  double dEnqExcSupCom = 0.0;
	  double dEnqExcTypeTestChrgs = 0.0;
	  double dRaIncFrieght = 0.0;
	  double dRaIncExWrnty = 0.0;
	  double dRaIncSupCom = 0.0;
	  double dRaIncTypeTestChrgs = 0.0;
	  double dRaExcFrieght = 0.0;
	  double dRaExcExWrnty = 0.0;
	  double dRaExcSupCom = 0.0;
	  double dRaExcTypeTestChrgs = 0.0;
	  
	 DecimalFormat df1 = new DecimalFormat("#0");
	  DecimalFormat df = new DecimalFormat("###,###");
	  
	  DaoFactory oDaoFactory = null;          
      QuotationDao oQuotationDao = null;
      try{  	  
      
	  
	  
      if(oEnquiryDto.getRatingObjects()!=null)
      {
			 arlRatingObjList = oEnquiryDto.getRatingObjects();
				if(arlRatingObjList.size()>QuotationConstants.QUOTATION_LITERAL_ZERO)
				{
					oDaoFactory = new DaoFactory();
					oQuotationDao = oDaoFactory.getQuotationDao();
				
					for(int i = 0;i<arlRatingObjList.size();i++)
					{	

						oRatingDto = (RatingDto)arlRatingObjList.get(i);
						oTechnicalOfferDto = oRatingDto.getTechnicalDto();
						iQty = oRatingDto.getQuantity();
						if(oTechnicalOfferDto.getUnitpriceMultiplier_rsm().indexOf(",") != QuotationConstants.QUOTATION_LITERAL_MINUSONE){
							dRatingTotalPrice=Double.parseDouble(oTechnicalOfferDto.getUnitpriceMultiplier_rsm().replaceAll(",", ""));
						}		
						else{
							dRatingTotalPrice=Double.parseDouble(oTechnicalOfferDto.getUnitpriceMultiplier_rsm());
						}
						dRaIncFrieght = Double.parseDouble((oTechnicalOfferDto.getPriceincludeFrieghtVal()==""?"0":oTechnicalOfferDto.getPriceincludeFrieghtVal()));
						dRaIncExWrnty = Double.parseDouble((oTechnicalOfferDto.getPriceincludeExtraWarnVal()== ""?"0":oTechnicalOfferDto.getPriceincludeExtraWarnVal()));
						dRaIncSupCom = Double.parseDouble((oTechnicalOfferDto.getPriceincludeSupervisionCostVal()== ""?"0":oTechnicalOfferDto.getPriceincludeSupervisionCostVal()));
						dRaIncTypeTestChrgs = Double.parseDouble((oTechnicalOfferDto.getPriceincludetypeTestChargesVal()== ""?"0":oTechnicalOfferDto.getPriceincludetypeTestChargesVal()));
						dRaExcFrieght = Double.parseDouble((oTechnicalOfferDto.getPriceextraFrieghtVal()== ""?"0":oTechnicalOfferDto.getPriceextraFrieghtVal()));
						dRaExcExWrnty = Double.parseDouble((oTechnicalOfferDto.getPriceextraExtraWarnVal()== ""?"0":oTechnicalOfferDto.getPriceextraExtraWarnVal()));
						dRaExcSupCom = Double.parseDouble((oTechnicalOfferDto.getPriceextraSupervisionCostVal()== ""?"0":oTechnicalOfferDto.getPriceextraSupervisionCostVal()));
						dRaExcTypeTestChrgs = Double.parseDouble((oTechnicalOfferDto.getPriceextratypeTestChargesVal()== ""?"0":oTechnicalOfferDto.getPriceextratypeTestChargesVal()));

						
						dRatingTotalMotorPrice = iQty *(dRatingTotalPrice);
						dRatingTotalPkgPrice = iQty *(dRatingTotalPrice+dRaExcFrieght+dRaExcExWrnty+dRaExcSupCom+dRaExcTypeTestChrgs);
						dEnqTotalMotorPrice = dEnqTotalMotorPrice + dRatingTotalMotorPrice;
						dEnqTotalPkgPrice = dEnqTotalPkgPrice + dRatingTotalPkgPrice;
						dEnqIncFrieght = dEnqIncFrieght+ (iQty *dRaIncFrieght);
						dEnqIncExWrnty = dEnqIncExWrnty + (iQty *dRaIncExWrnty);
						dEnqIncSupCom = dEnqIncSupCom + (iQty *dRaIncSupCom);
						dEnqIncTypeTestChrgs = dEnqIncTypeTestChrgs + (iQty *dRaIncTypeTestChrgs);
						dEnqExcFrieght = dEnqExcFrieght + (iQty *dRaExcFrieght);
						dEnqExcExWrnty = dEnqExcExWrnty + (iQty *dRaExcExWrnty);
						dEnqExcSupCom = dEnqExcSupCom + (iQty *dRaExcSupCom);
						dEnqExcTypeTestChrgs = dEnqExcTypeTestChrgs + (iQty *dRaExcTypeTestChrgs);	
						
						oRatingDto.setTotalMotorPrice(df.format(dRatingTotalMotorPrice));
						oRatingDto.setTotalPackagePrice(df.format(dRatingTotalPkgPrice));						
						oQuotationDao.saveRatingMotorPkgPrice(conn,oRatingDto);
					}
					
					oEnquiryDto.setIncludeFrieght(df1.format(dEnqIncFrieght));
					oEnquiryDto.setIncludeExtraWarrnt(df1.format(dEnqIncExWrnty));
					oEnquiryDto.setIncludeSupervisionCost(df1.format(dEnqIncSupCom));
					oEnquiryDto.setIncludetypeTestCharges(df1.format(dEnqIncTypeTestChrgs));
					oEnquiryDto.setExtraFrieght(df1.format(dEnqExcFrieght));
					oEnquiryDto.setExtraExtraWarrnt(df1.format(dEnqExcExWrnty));
					oEnquiryDto.setExtraSupervisionCost(df1.format(dEnqExcSupCom));
					oEnquiryDto.setExtratypeTestCharges(df1.format(dEnqExcTypeTestChrgs));
					oEnquiryDto.setTotalMotorPrice(df.format(dEnqTotalMotorPrice));
					oEnquiryDto.setTotalPkgPrice(df1.format(dEnqTotalPkgPrice));						
					oQuotationDao.saveEnquiryMotorPkgPrice(conn,oEnquiryDto);
					
					
		   
	   }
      }
      }catch(Exception e) {
          /*
           * Logging any exception that raised during this activity in log files using Log4j
           */
          LoggerUtility.log("DEBUG", this.getClass().getName(), "calculateSaveTmpPkgPrice", ExceptionUtility.getStackTraceAsString(e));    
                  
          
      } finally {
          
      } 
  }
   

   /**
	* This method is used to save the calculated quoted price enquiry and rating in quotation page
	* @param conn Connection object to connect to database
	* @param EnquiryDto oEnquiryDto object having Enquiry details
	* @return Returns none
	* @author 610092227
	* Created on: 05 Sept 2019
	*/
   
   private void  calculateQuotedPrice(Connection conn, EnquiryDto oEnquiryDto){
	   
	   	  ArrayList arlRatingObjList = null;
		  RatingDto oRatingDto = null;
		  TechnicalOfferDto oTechnicalOfferDto = null;
		  int iQty = 0;
		  double dRatingTotalPrice = 0.0;
		  double dEnquiryQuotedPrice = 0.0;
		  String sRatingQuotedPrice = null;
		  String sEnquiryQuotedPrice = null;
		  
		  DecimalFormat df1 = new DecimalFormat("#0");
		  DecimalFormat df = new DecimalFormat("###,###");
		  
		  DaoFactory oDaoFactory = null;          
	      QuotationDao oQuotationDao = null;
	      try{	  
		  
	      if(oEnquiryDto.getRatingObjects()!=null)
	      {
				 arlRatingObjList = oEnquiryDto.getRatingObjects();
					if(arlRatingObjList.size()>QuotationConstants.QUOTATION_LITERAL_ZERO)
					{
						oDaoFactory = new DaoFactory();
						oQuotationDao = oDaoFactory.getQuotationDao();
					
						for(int i = 0;i<arlRatingObjList.size();i++)
						{	

							oRatingDto = (RatingDto)arlRatingObjList.get(i);
							oTechnicalOfferDto = oRatingDto.getTechnicalDto();
							iQty = oRatingDto.getQuantity();
							if(oTechnicalOfferDto.getTotalpriceMultiplier_rsm().indexOf(",") != QuotationConstants.QUOTATION_LITERAL_MINUSONE){
								dRatingTotalPrice=Double.parseDouble(oTechnicalOfferDto.getTotalpriceMultiplier_rsm().replaceAll(",", ""));
							}		
							else{
								dRatingTotalPrice=Double.parseDouble(oTechnicalOfferDto.getTotalpriceMultiplier_rsm());
							}
							
							sRatingQuotedPrice = df.format(dRatingTotalPrice);
							oRatingDto.setQuotedPrice(sRatingQuotedPrice);
							oQuotationDao.saveRatingQuotedPrice(conn,oRatingDto);
							dEnquiryQuotedPrice = dEnquiryQuotedPrice + dRatingTotalPrice;					
							
						}
						
						oEnquiryDto.setEnqQuotedPrice(df.format(dEnquiryQuotedPrice));
						oQuotationDao.saveEnquiryQuotedPrice(conn,oEnquiryDto);
					}
	      }
	      
	      
	      }catch(Exception e) {
	       /*
	        * Logging any exception that raised during this activity in log files using Log4j
	        */
	       LoggerUtility.log("DEBUG", this.getClass().getName(), "calculateQuotedPrice", ExceptionUtility.getStackTraceAsString(e));    
	       
	       
       
		   } finally {
		       
		   } 
   }
}
	
      
   

	   
	   
	
   

   

