package in.com.rbc.quotation.enquiry.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.com.rbc.quotation.common.utility.CommonUtility;
import in.com.rbc.quotation.common.vo.KeyValueVo;

public class NewMTORatingDto {

	private int mtoIndexId;
	private int mtoRatingId;
	private int mtoEnquiryId;
	private int mtoRatingNo;
	private int mtoQuantity;
	
	private String mtoProdGroupId;
	private String mtoProdLineId;
	private String mtoKWId;
	private String mtoPoleId;
	private String mtoFrameId;
	private String mtoFrameSuffixId;
	private String mtoMountingId;
	private String mtoTBPositionId;
	private String mtoTagNumber;
	private String mtoMfgLocation;
	
	private String mtoPercentCopper;
	private String mtoPercentStamping;
	private String mtoPercentAluminium;
	private String mtoStandardAddOnPercent;
	private String mtoStandardCashExtra;
	private String mtoSpecialAddOnPercent;
	private String mtoSpecialCashExtra;
	private String mtoTotalAddOnPercent;
	private String mtoTotalCashExtra;
	private String mtoMaterialCost;
	private String mtoDesignComplete;
	
	private String mtoSpecialFeatures;
	
	private String actualMfgLocation;
	private String actualProdLineId;
	private String actualKWId;
	private String actualPoleId;
	private String actualFrameId;
	private String actualFrameSuffixId;
	private String actualMountingId;
	private String actualTBPositionId;
	
	private ArrayList<KeyValueVo> newMTOTechDetailsList = new ArrayList<KeyValueVo>();
	private ArrayList<NewMTOAddOnDto> newMTOAddOnsList = new ArrayList<NewMTOAddOnDto>();
	
	private List<KeyValueVo> newCompleteAddOnsList = new ArrayList<>();
	
	
	public int getMtoIndexId() {
		return mtoIndexId;
	}
	public void setMtoIndexId(int mtoIndexId) {
		this.mtoIndexId = mtoIndexId;
	}
	public int getMtoRatingId() {
		return mtoRatingId;
	}
	public void setMtoRatingId(int mtoRatingId) {
		this.mtoRatingId = mtoRatingId;
	}
	public int getMtoEnquiryId() {
		return mtoEnquiryId;
	}
	public void setMtoEnquiryId(int mtoEnquiryId) {
		this.mtoEnquiryId = mtoEnquiryId;
	}
	public int getMtoRatingNo() {
		return mtoRatingNo;
	}
	public void setMtoRatingNo(int mtoRatingNo) {
		this.mtoRatingNo = mtoRatingNo;
	}
	public int getMtoQuantity() {
		return mtoQuantity;
	}
	public void setMtoQuantity(int mtoQuantity) {
		this.mtoQuantity = mtoQuantity;
	}
	public String getMtoProdGroupId() {
		return CommonUtility.replaceNull(mtoProdGroupId);
	}
	public void setMtoProdGroupId(String mtoProdGroupId) {
		this.mtoProdGroupId = mtoProdGroupId;
	}
	public String getMtoProdLineId() {
		return CommonUtility.replaceNull(mtoProdLineId);
	}
	public void setMtoProdLineId(String mtoProdLineId) {
		this.mtoProdLineId = mtoProdLineId;
	}
	public String getMtoKWId() {
		return CommonUtility.replaceNull(mtoKWId);
	}
	public void setMtoKWId(String mtoKWId) {
		this.mtoKWId = mtoKWId;
	}
	public String getMtoPoleId() {
		return CommonUtility.replaceNull(mtoPoleId);
	}
	public void setMtoPoleId(String mtoPoleId) {
		this.mtoPoleId = mtoPoleId;
	}
	public String getMtoFrameId() {
		return CommonUtility.replaceNull(mtoFrameId);
	}
	public void setMtoFrameId(String mtoFrameId) {
		this.mtoFrameId = mtoFrameId;
	}
	public String getMtoFrameSuffixId() {
		return CommonUtility.replaceNull(mtoFrameSuffixId);
	}
	public void setMtoFrameSuffixId(String mtoFrameSuffixId) {
		this.mtoFrameSuffixId = mtoFrameSuffixId;
	}
	public String getMtoMountingId() {
		return CommonUtility.replaceNull(mtoMountingId);
	}
	public void setMtoMountingId(String mtoMountingId) {
		this.mtoMountingId = mtoMountingId;
	}
	public String getMtoTBPositionId() {
		return CommonUtility.replaceNull(mtoTBPositionId);
	}
	public void setMtoTBPositionId(String mtoTBPositionId) {
		this.mtoTBPositionId = mtoTBPositionId;
	}
	public String getMtoTagNumber() {
		return CommonUtility.replaceNull(mtoTagNumber);
	}
	public void setMtoTagNumber(String mtoTagNumber) {
		this.mtoTagNumber = mtoTagNumber;
	}
	public String getMtoMfgLocation() {
		return CommonUtility.replaceNull(mtoMfgLocation);
	}
	public void setMtoMfgLocation(String mtoMfgLocation) {
		this.mtoMfgLocation = mtoMfgLocation;
	}
	public String getMtoPercentCopper() {
		return CommonUtility.replaceNull(mtoPercentCopper);
	}
	public void setMtoPercentCopper(String mtoPercentCopper) {
		this.mtoPercentCopper = mtoPercentCopper;
	}
	public String getMtoPercentStamping() {
		return CommonUtility.replaceNull(mtoPercentStamping);
	}
	public void setMtoPercentStamping(String mtoPercentStamping) {
		this.mtoPercentStamping = mtoPercentStamping;
	}
	public String getMtoPercentAluminium() {
		return CommonUtility.replaceNull(mtoPercentAluminium);
	}
	public void setMtoPercentAluminium(String mtoPercentAluminium) {
		this.mtoPercentAluminium = mtoPercentAluminium;
	}
	public String getMtoStandardAddOnPercent() {
		return CommonUtility.replaceNull(mtoStandardAddOnPercent);
	}
	public void setMtoStandardAddOnPercent(String mtoStandardAddOnPercent) {
		this.mtoStandardAddOnPercent = mtoStandardAddOnPercent;
	}
	public String getMtoStandardCashExtra() {
		return CommonUtility.replaceNull(mtoStandardCashExtra);
	}
	public void setMtoStandardCashExtra(String mtoStandardCashExtra) {
		this.mtoStandardCashExtra = mtoStandardCashExtra;
	}
	public String getMtoSpecialAddOnPercent() {
		return CommonUtility.replaceNull(mtoSpecialAddOnPercent);
	}
	public void setMtoSpecialAddOnPercent(String mtoSpecialAddOnPercent) {
		this.mtoSpecialAddOnPercent = mtoSpecialAddOnPercent;
	}
	public String getMtoSpecialCashExtra() {
		return CommonUtility.replaceNull(mtoSpecialCashExtra);
	}
	public void setMtoSpecialCashExtra(String mtoSpecialCashExtra) {
		this.mtoSpecialCashExtra = mtoSpecialCashExtra;
	}
	public String getMtoTotalAddOnPercent() {
		return CommonUtility.replaceNull(mtoTotalAddOnPercent);
	}
	public void setMtoTotalAddOnPercent(String mtoTotalAddOnPercent) {
		this.mtoTotalAddOnPercent = mtoTotalAddOnPercent;
	}
	public String getMtoTotalCashExtra() {
		return CommonUtility.replaceNull(mtoTotalCashExtra);
	}
	public void setMtoTotalCashExtra(String mtoTotalCashExtra) {
		this.mtoTotalCashExtra = mtoTotalCashExtra;
	}
	public String getMtoMaterialCost() {
		return CommonUtility.replaceNull(mtoMaterialCost);
	}
	public void setMtoMaterialCost(String mtoMaterialCost) {
		this.mtoMaterialCost = mtoMaterialCost;
	}
	public String getMtoDesignComplete() {
		return CommonUtility.replaceNull(mtoDesignComplete);
	}
	public void setMtoDesignComplete(String mtoDesignComplete) {
		this.mtoDesignComplete = mtoDesignComplete;
	}
	public String getMtoSpecialFeatures() {
		return CommonUtility.replaceNull(mtoSpecialFeatures);
	}
	public void setMtoSpecialFeatures(String mtoSpecialFeatures) {
		this.mtoSpecialFeatures = mtoSpecialFeatures;
	}
	
	public ArrayList<KeyValueVo> getNewMTOTechDetailsList() {
		return newMTOTechDetailsList;
	}

	public void setNewMTOTechDetailsList(ArrayList<KeyValueVo> newMTOTechDetailsList) {
		this.newMTOTechDetailsList = newMTOTechDetailsList;
	}

	public ArrayList<NewMTOAddOnDto> getNewMTOAddOnsList() {
		return newMTOAddOnsList;
	}

	public void setNewMTOAddOnsList(ArrayList<NewMTOAddOnDto> newMTOAddOnsList) {
		this.newMTOAddOnsList = newMTOAddOnsList;
	}
	
	public String getActualMfgLocation() {
		return CommonUtility.replaceNull(actualMfgLocation);
	}
	public void setActualMfgLocation(String actualMfgLocation) {
		this.actualMfgLocation = actualMfgLocation;
	}
	public String getActualProdLineId() {
		return CommonUtility.replaceNull(actualProdLineId);
	}
	public void setActualProdLineId(String actualProdLineId) {
		this.actualProdLineId = actualProdLineId;
	}
	public String getActualKWId() {
		return CommonUtility.replaceNull(actualKWId);
	}
	public void setActualKWId(String actualKWId) {
		this.actualKWId = actualKWId;
	}
	public String getActualPoleId() {
		return CommonUtility.replaceNull(actualPoleId);
	}
	public void setActualPoleId(String actualPoleId) {
		this.actualPoleId = actualPoleId;
	}
	public String getActualFrameId() {
		return CommonUtility.replaceNull(actualFrameId);
	}
	public void setActualFrameId(String actualFrameId) {
		this.actualFrameId = actualFrameId;
	}
	public String getActualFrameSuffixId() {
		return CommonUtility.replaceNull(actualFrameSuffixId);
	}
	public void setActualFrameSuffixId(String actualFrameSuffixId) {
		this.actualFrameSuffixId = actualFrameSuffixId;
	}
	public String getActualMountingId() {
		return CommonUtility.replaceNull(actualMountingId);
	}
	public void setActualMountingId(String actualMountingId) {
		this.actualMountingId = actualMountingId;
	}
	public String getActualTBPositionId() {
		return CommonUtility.replaceNull(actualTBPositionId);
	}
	public void setActualTBPositionId(String actualTBPositionId) {
		this.actualTBPositionId = actualTBPositionId;
	}
	public List<KeyValueVo> getNewCompleteAddOnsList() {
		return newCompleteAddOnsList;
	}
	public void setNewCompleteAddOnsList(List<KeyValueVo> newCompleteAddOnsList) {
		this.newCompleteAddOnsList = newCompleteAddOnsList;
	}
	
	
}
