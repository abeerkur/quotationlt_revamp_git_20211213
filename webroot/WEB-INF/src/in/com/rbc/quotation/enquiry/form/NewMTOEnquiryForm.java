package in.com.rbc.quotation.enquiry.form;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

import in.com.rbc.quotation.common.utility.CommonUtility;
import in.com.rbc.quotation.common.vo.KeyValueVo;
import in.com.rbc.quotation.enquiry.dto.NewMTORatingDto;
import in.com.rbc.quotation.enquiry.dto.NewRatingDto;

public class NewMTOEnquiryForm extends ActionForm {

	private String dispatch; // This variable is for diffrentiating between create and edit
	private String operationType; // This variable is to check whether INSERT OR UPDATE operation has to be done for enquiry 
	private String enquiryOperationType; // This variable is to diffrentiate between the button actions Manager or engineer
	private String ratingOperationType; // This variable is to check whether INSERT OR UPDATE operation has to be done for rating
	private String ratingsValidationMessage; // This variable will have the validation message - when submit button in clicked we display an error message if all the ratings are not validated
	private String currentRating; // This variable will be used to display the current tab in bold
	private String commentOperation;
	private String reviseUserType;
	private String ratingOperationId; // This variable is used for storing the ratingId that has to fetched for editing - Design Office
	
	private String isLast; // This variable is to check whether the current rating is the last rating or not in edit page
	private String nextRatingId; // This variable will store the next rating id
	private String isMaxRating; // Based on this variable value "Add New rating button will be displayed" 
	
	private int enquiryId;
	private int revisionNumber; 
	private String enquiryNumber;
	private int statusId;
	private String statusName;
	private String createdBy;
	private String createdBySSO;
	private String createdDate;
	private String lastModifiedBy;
	private String lastModifiedDate;
	private String designReferenceNumber;
	private int ratingId;
	private int ratingNo;
	
	private FormFile theFile1 = null;
	private String fileDescription1;	
	private FormFile theFile2 = null;
	private String fileDescription2;
	private FormFile theFile3 = null;
	private String fileDescription3;
	private FormFile theFile4 = null;
	private String fileDescription4;
	private FormFile theFile5 = null;
	private String fileDescription5;
	private FormFile theFile6 = null;
	private String fileDescription6;
	private FormFile theFile7 = null;
	private String fileDescription7;
	private FormFile theFile8 = null;
	private String fileDescription8;
	private FormFile theFile9 = null;
	private String fileDescription9;
	private FormFile theFile10 = null;
	private String fileDescription10;
	
	// --------------------    Request Information	-------------------- 
	private int customerId;
	private String customerName;
	private String customerType;
	private int locationId;
	private String location;
	private String customerIndustry;
	private String customerIndustryId;
	private String concernedPerson;
	private String concernedPersonHdn;
	private String projectName;
	private String endUser;
	private String endUserIndustry;
	
	// --------------------    Workflow Related -------------------------    
	private String assignToId;
	private String assignToName;
	private String assignRemarks;	
	private String returnTo;
	private String returnToRemarks;
	private String sm_name;
	private String smEmail;
	private String smContactNo;
	private String deptAndAuth;
	private String designEngineerName;
	private String pagechecking;
	
	private ArrayList customerLocationList = new ArrayList();
	
	// --------------------    Design MTO Search Related -----------------
	private String searchMfgLocation;
	private String searchProdLine;
	private String searchKW;
	private String searchPole;
	private String searchFrame;
	private String searchFrameSuffix;
	private String searchMounting;
	private String searchTBPosition;
	
	private String isReferDesign;
	
	// --------------------    Notes Section Related ---------------------
	private String smNote;
	private String rsmNote;
	private String nsmNote;
	private String mhNote;
	private String designNote;
	private String bhNote;
	private String mfgNote;
	private String qaNote;
	private String scmNote;
	
	private String commentsDeviations;
	private String reassignRemarks;
	
	private ArrayList<NewRatingDto> newRatingsList = new ArrayList<NewRatingDto>();
	private ArrayList<NewMTORatingDto> newMTORatingsList = new ArrayList<NewMTORatingDto>();

	private List<KeyValueVo> newCompleteAddOnsList = new ArrayList<>();
	
	private List<KeyValueVo> mtoReassignUsersList = new ArrayList<>();
	
	// --------------------		Load Lists -------------------------------
	private List<KeyValueVo> ltMfgLocationList = new ArrayList<>();
	private List<KeyValueVo> ltProductLineList = new ArrayList<>();
	private List<KeyValueVo> ltKWList = new ArrayList<>();
	private List<KeyValueVo> ltPoleList = new ArrayList<>();
	private List<KeyValueVo> ltFrameList = new ArrayList<>();
	private List<KeyValueVo> ltFrameSuffixList = new ArrayList<>();
	private List<KeyValueVo> ltMountingList = new ArrayList<>();
	private List<KeyValueVo> ltTBPositionList = new ArrayList<>();
	
	private List<KeyValueVo> ltAmbientTemperatureList = new ArrayList<>();
	private List<KeyValueVo> ltAmbientRemTempList = new ArrayList<>();
	private List<KeyValueVo> ltGasGroupList = new ArrayList<>();
	private List<KeyValueVo> ltHazardZoneList = new ArrayList<>();
	private List<KeyValueVo> ltDustGroupList = new ArrayList<>();
	private List<KeyValueVo> ltApplicationList = new ArrayList<>();
	private List<KeyValueVo> ltVoltageList = new ArrayList<>();
	private List<KeyValueVo> ltFrequencyList = new ArrayList<>();
	private List<KeyValueVo> ltCombinedVariationList = new ArrayList<>();
	private List<KeyValueVo> ltDutyList = new ArrayList<>();
	private List<KeyValueVo> ltCDFList = new ArrayList<>();
	private List<KeyValueVo> ltStartsList = new ArrayList<>();
	private List<KeyValueVo> ltRVRAList = new ArrayList<>();
	private List<KeyValueVo> ltWindingTreatmentList = new ArrayList<>();
	private List<KeyValueVo> ltLeadList = new ArrayList<>();
	private List<KeyValueVo> ltVFDTypeList = new ArrayList<>();
	private List<KeyValueVo> ltDualSpeedTypeList = new ArrayList<>();
	private List<KeyValueVo> ltShaftMaterialTypeList = new ArrayList<>();
	private List<KeyValueVo> ltPaintingTypeList = new ArrayList<>();
	private List<KeyValueVo> ltPaintShadeList = new ArrayList<>();
	private List<KeyValueVo> ltPaintThicknessList = new ArrayList<>();
	private List<KeyValueVo> ltIPList = new ArrayList<>();
	private List<KeyValueVo> ltInsulationClassList = new ArrayList<>();
	private List<KeyValueVo> ltTerminalBoxList = new ArrayList<>();
	private List<KeyValueVo> ltSpreaderBoxList = new ArrayList<>();
	private List<KeyValueVo> ltAuxTerminalBoxList = new ArrayList<>();
	private List<KeyValueVo> ltSpaceHeaterList = new ArrayList<>();
	private List<KeyValueVo> ltVibrationList = new ArrayList<>();
	private List<KeyValueVo> ltFlyingLeadList = new ArrayList<>();
	private List<KeyValueVo> ltMetalFanList = new ArrayList<>();
	private List<KeyValueVo> ltShaftGroundingList = new ArrayList<>();
	private List<KeyValueVo> ltForcedCoolingList = new ArrayList<>();
	private List<KeyValueVo> ltHardwareList = new ArrayList<>();
	private List<KeyValueVo> ltGlandPlateList = new ArrayList<>();
	private List<KeyValueVo> ltDoubleCompressionGlandList = new ArrayList<>();
	private List<KeyValueVo> ltSPMMountingProvisionList = new ArrayList<>();
	private List<KeyValueVo> ltAddNamePlateList = new ArrayList<>();
	private List<KeyValueVo> ltArrowPlateDirectionList = new ArrayList<>();
	private List<KeyValueVo> ltRTDList = new ArrayList<>();
	private List<KeyValueVo> ltBTDList = new ArrayList<>();
	private List<KeyValueVo> ltThermisterList = new ArrayList<>();
	private List<KeyValueVo> ltCableSealingBoxList = new ArrayList<>();
	private List<KeyValueVo> ltBearingSystemList = new ArrayList<>();
	private List<KeyValueVo> ltBearingNDEList = new ArrayList<>();
	private List<KeyValueVo> ltBearingDEList = new ArrayList<>();
	private List<KeyValueVo> ltWitnessRoutineList = new ArrayList<>();
	private List<KeyValueVo> ltAdditionalTestList = new ArrayList<>();
	private List<KeyValueVo> ltTypeTestList = new ArrayList<>();
	private List<KeyValueVo> ltULCEList = new ArrayList<>();
	private List<KeyValueVo> ltQAPList = new ArrayList<>();
	private List<KeyValueVo> ltSeaworthyPackingList = new ArrayList<>();
	private List<KeyValueVo> ltWarrantyList = new ArrayList<>();
	private List<KeyValueVo> ltVoltVariationList = new ArrayList<>();
	private List<KeyValueVo> ltFreqVariationList = new ArrayList<>();
	private List<KeyValueVo> ltStartingDOLCurrentList = new ArrayList<>();
	private List<KeyValueVo> ltWindingConfigurationList = new ArrayList<>();
	private List<KeyValueVo> ltMethodOfStartingList = new ArrayList<>();
	private List<KeyValueVo> ltWindingWireList = new ArrayList<>();
	private List<KeyValueVo> ltVFDSpeedRangeMinList = new ArrayList<>();
	private List<KeyValueVo> ltVFDSpeedRangeMaxList = new ArrayList<>();
	private List<KeyValueVo> ltOverloadingDutyList = new ArrayList<>();
	private List<KeyValueVo> ltConstantEffRangeList = new ArrayList<>();
	private List<KeyValueVo> ltShaftTypeList = new ArrayList<>();
	private List<KeyValueVo> ltDirectionOfRotationList = new ArrayList<>();
	private List<KeyValueVo> ltMethodOfCouplingList = new ArrayList<>();
	private List<KeyValueVo> ltTechoMountingList = new ArrayList<>();
	private List<KeyValueVo> ltDataSheetList = new ArrayList<>();
	private List<KeyValueVo> ltNonStandardVoltageList = new ArrayList<>();
	private List<KeyValueVo> ltHazAreaList = new ArrayList<>();
	private List<KeyValueVo> ltHazAreaTypeList = new ArrayList<>();
	private List<KeyValueVo> ltServiceFactorList = new ArrayList<>();
	private List<KeyValueVo> ltStandardRotationList = new ArrayList<>();
	private List<KeyValueVo> ltTemperatureRiseList = new ArrayList<>();
	private List<KeyValueVo> ltNoiseLevelList = new ArrayList<>();
	private List<KeyValueVo> ltPerfCurvesList = new ArrayList<>();
	private List<KeyValueVo> ltCableSizeList = new ArrayList<>();
	private List<KeyValueVo> ltNoOfRunsList = new ArrayList<>();
	private List<KeyValueVo> ltNoOfCoresList = new ArrayList<>();
	private List<KeyValueVo> ltCrossSectionAreaList = new ArrayList<>();
	private List<KeyValueVo> ltConductorMaterialList = new ArrayList<>();
	private List<KeyValueVo> ltTemperatureClassList = new ArrayList<>();
	private List<KeyValueVo> ltEfficiencyClassList = new ArrayList<>();
	private List<KeyValueVo> ltTypeOfGreaseList = new ArrayList<>();
	private List<KeyValueVo> ltSparesList = new ArrayList<>();
	private List<KeyValueVo> ltMotorGAList = new ArrayList<>();
	private List<KeyValueVo> ltTBoxGAList = new ArrayList<>();
	
	private List<KeyValueVo> ltBroughtOutTerminalsList = new ArrayList<>();
	private List<KeyValueVo> ltMainTBList = new ArrayList<>();
	private List<KeyValueVo> ltCableEntryList = new ArrayList<>();
	private List<KeyValueVo> ltLubricationList = new ArrayList<>();
	
	private ArrayList alVoltList = new ArrayList();
	private ArrayList alMainTermBoxPSList = new ArrayList();
	private ArrayList alNeutralTBList = new ArrayList();
	private ArrayList alCableEntryList = new ArrayList();
	private ArrayList alBoTerminalsNoList = new ArrayList();
	private ArrayList alLubricationList = new ArrayList();
	private ArrayList alnoiseLevelList = new ArrayList();
	private ArrayList alMinStartVoltList = new ArrayList();
	private ArrayList alBalancingList = new ArrayList();
	private ArrayList alBearingThermoDtList = new ArrayList();
	private ArrayList degproList = new ArrayList();
	private ArrayList termBoxList = new ArrayList();
	private ArrayList enclosureList = new ArrayList();
	
	private ArrayList alTargetedList = new ArrayList();
	private ArrayList alIndustryList = new ArrayList();
	private ArrayList alEnquiryTypeList = new ArrayList();
	private ArrayList alWinChanceList = new ArrayList();
	
	private int totalRatingIndex = 0;
	private int mtoTotalRatingIndex = 0;
	
	private String isDesignCompleteCheck;
	
	public String getDispatch() {
		return CommonUtility.replaceNull(dispatch);
	}

	public void setDispatch(String dispatch) {
		this.dispatch = dispatch;
	}

	public String getOperationType() {
		return CommonUtility.replaceNull(operationType);
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	public String getEnquiryOperationType() {
		return CommonUtility.replaceNull(enquiryOperationType);
	}

	public void setEnquiryOperationType(String enquiryOperationType) {
		this.enquiryOperationType = enquiryOperationType;
	}

	public String getRatingOperationType() {
		return CommonUtility.replaceNull(ratingOperationType);
	}

	public void setRatingOperationType(String ratingOperationType) {
		this.ratingOperationType = ratingOperationType;
	}

	public String getRatingsValidationMessage() {
		return CommonUtility.replaceNull(ratingsValidationMessage);
	}

	public void setRatingsValidationMessage(String ratingsValidationMessage) {
		this.ratingsValidationMessage = ratingsValidationMessage;
	}

	public String getCurrentRating() {
		return CommonUtility.replaceNull(currentRating);
	}

	public void setCurrentRating(String currentRating) {
		this.currentRating = currentRating;
	}

	public String getCommentOperation() {
		return CommonUtility.replaceNull(commentOperation);
	}

	public void setCommentOperation(String commentOperation) {
		this.commentOperation = commentOperation;
	}

	public String getReviseUserType() {
		return CommonUtility.replaceNull(reviseUserType);
	}

	public void setReviseUserType(String reviseUserType) {
		this.reviseUserType = reviseUserType;
	}

	public String getRatingOperationId() {
		return CommonUtility.replaceNull(ratingOperationId);
	}

	public void setRatingOperationId(String ratingOperationId) {
		this.ratingOperationId = ratingOperationId;
	}

	public String getIsLast() {
		return CommonUtility.replaceNull(isLast);
	}

	public void setIsLast(String isLast) {
		this.isLast = isLast;
	}

	public String getNextRatingId() {
		return CommonUtility.replaceNull(nextRatingId);
	}

	public void setNextRatingId(String nextRatingId) {
		this.nextRatingId = nextRatingId;
	}

	public String getIsMaxRating() {
		return CommonUtility.replaceNull(isMaxRating);
	}

	public void setIsMaxRating(String isMaxRating) {
		this.isMaxRating = isMaxRating;
	}

	public int getEnquiryId() {
		return enquiryId;
	}

	public void setEnquiryId(int enquiryId) {
		this.enquiryId = enquiryId;
	}

	public int getRevisionNumber() {
		return revisionNumber;
	}

	public void setRevisionNumber(int revisionNumber) {
		this.revisionNumber = revisionNumber;
	}

	public String getEnquiryNumber() {
		return CommonUtility.replaceNull(enquiryNumber);
	}

	public void setEnquiryNumber(String enquiryNumber) {
		this.enquiryNumber = enquiryNumber;
	}

	public int getStatusId() {
		return statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	public String getStatusName() {
		return CommonUtility.replaceNull(statusName);
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public String getCreatedBy() {
		return CommonUtility.replaceNull(createdBy);
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedBySSO() {
		return CommonUtility.replaceNull(createdBySSO);
	}

	public void setCreatedBySSO(String createdBySSO) {
		this.createdBySSO = createdBySSO;
	}

	public String getCreatedDate() {
		return CommonUtility.replaceNull(createdDate);
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getLastModifiedBy() {
		return CommonUtility.replaceNull(lastModifiedBy);
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public String getLastModifiedDate() {
		return CommonUtility.replaceNull(lastModifiedDate);
	}

	public void setLastModifiedDate(String lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getDesignReferenceNumber() {
		return CommonUtility.replaceNull(designReferenceNumber);
	}

	public void setDesignReferenceNumber(String designReferenceNumber) {
		this.designReferenceNumber = designReferenceNumber;
	}

	public int getRatingId() {
		return ratingId;
	}

	public void setRatingId(int ratingId) {
		this.ratingId = ratingId;
	}

	public int getRatingNo() {
		return ratingNo;
	}

	public void setRatingNo(int ratingNo) {
		this.ratingNo = ratingNo;
	}

	public FormFile getTheFile1() {
		return theFile1;
	}

	public void setTheFile1(FormFile theFile1) {
		this.theFile1 = theFile1;
	}

	public String getFileDescription1() {
		return CommonUtility.replaceNull(fileDescription1);
	}

	public void setFileDescription1(String fileDescription1) {
		this.fileDescription1 = fileDescription1;
	}

	public FormFile getTheFile2() {
		return theFile2;
	}

	public void setTheFile2(FormFile theFile2) {
		this.theFile2 = theFile2;
	}

	public String getFileDescription2() {
		return CommonUtility.replaceNull(fileDescription2);
	}

	public void setFileDescription2(String fileDescription2) {
		this.fileDescription2 = fileDescription2;
	}

	public FormFile getTheFile3() {
		return theFile3;
	}

	public void setTheFile3(FormFile theFile3) {
		this.theFile3 = theFile3;
	}

	public String getFileDescription3() {
		return CommonUtility.replaceNull(fileDescription3);
	}

	public void setFileDescription3(String fileDescription3) {
		this.fileDescription3 = fileDescription3;
	}

	public FormFile getTheFile4() {
		return theFile4;
	}

	public void setTheFile4(FormFile theFile4) {
		this.theFile4 = theFile4;
	}

	public String getFileDescription4() {
		return CommonUtility.replaceNull(fileDescription4);
	}

	public void setFileDescription4(String fileDescription4) {
		this.fileDescription4 = fileDescription4;
	}

	public FormFile getTheFile5() {
		return theFile5;
	}

	public void setTheFile5(FormFile theFile5) {
		this.theFile5 = theFile5;
	}

	public String getFileDescription5() {
		return CommonUtility.replaceNull(fileDescription5);
	}

	public void setFileDescription5(String fileDescription5) {
		this.fileDescription5 = fileDescription5;
	}

	public FormFile getTheFile6() {
		return theFile6;
	}

	public void setTheFile6(FormFile theFile6) {
		this.theFile6 = theFile6;
	}

	public String getFileDescription6() {
		return CommonUtility.replaceNull(fileDescription6);
	}

	public void setFileDescription6(String fileDescription6) {
		this.fileDescription6 = fileDescription6;
	}

	public FormFile getTheFile7() {
		return theFile7;
	}

	public void setTheFile7(FormFile theFile7) {
		this.theFile7 = theFile7;
	}

	public String getFileDescription7() {
		return CommonUtility.replaceNull(fileDescription7);
	}

	public void setFileDescription7(String fileDescription7) {
		this.fileDescription7 = fileDescription7;
	}

	public FormFile getTheFile8() {
		return theFile8;
	}

	public void setTheFile8(FormFile theFile8) {
		this.theFile8 = theFile8;
	}

	public String getFileDescription8() {
		return CommonUtility.replaceNull(fileDescription8);
	}

	public void setFileDescription8(String fileDescription8) {
		this.fileDescription8 = fileDescription8;
	}

	public FormFile getTheFile9() {
		return theFile9;
	}

	public void setTheFile9(FormFile theFile9) {
		this.theFile9 = theFile9;
	}

	public String getFileDescription9() {
		return CommonUtility.replaceNull(fileDescription9);
	}

	public void setFileDescription9(String fileDescription9) {
		this.fileDescription9 = fileDescription9;
	}

	public FormFile getTheFile10() {
		return theFile10;
	}

	public void setTheFile10(FormFile theFile10) {
		this.theFile10 = theFile10;
	}

	public String getFileDescription10() {
		return CommonUtility.replaceNull(fileDescription10);
	}

	public void setFileDescription10(String fileDescription10) {
		this.fileDescription10 = fileDescription10;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return CommonUtility.replaceNull(customerName);
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerType() {
		return CommonUtility.replaceNull(customerType);
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public int getLocationId() {
		return locationId;
	}

	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}

	public String getLocation() {
		return CommonUtility.replaceNull(location);
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getCustomerIndustry() {
		return CommonUtility.replaceNull(customerIndustry);
	}

	public void setCustomerIndustry(String customerIndustry) {
		this.customerIndustry = customerIndustry;
	}

	public String getCustomerIndustryId() {
		return CommonUtility.replaceNull(customerIndustryId);
	}

	public void setCustomerIndustryId(String customerIndustryId) {
		this.customerIndustryId = customerIndustryId;
	}

	public String getConcernedPerson() {
		return CommonUtility.replaceNull(concernedPerson);
	}

	public void setConcernedPerson(String concernedPerson) {
		this.concernedPerson = concernedPerson;
	}

	public String getConcernedPersonHdn() {
		return CommonUtility.replaceNull(concernedPersonHdn);
	}

	public void setConcernedPersonHdn(String concernedPersonHdn) {
		this.concernedPersonHdn = concernedPersonHdn;
	}

	public String getProjectName() {
		return CommonUtility.replaceNull(projectName);
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getEndUser() {
		return CommonUtility.replaceNull(endUser);
	}

	public void setEndUser(String endUser) {
		this.endUser = endUser;
	}

	public String getEndUserIndustry() {
		return CommonUtility.replaceNull(endUserIndustry);
	}

	public void setEndUserIndustry(String endUserIndustry) {
		this.endUserIndustry = endUserIndustry;
	}

	public String getAssignToId() {
		return CommonUtility.replaceNull(assignToId);
	}

	public void setAssignToId(String assignToId) {
		this.assignToId = assignToId;
	}

	public String getAssignToName() {
		return CommonUtility.replaceNull(assignToName);
	}

	public void setAssignToName(String assignToName) {
		this.assignToName = assignToName;
	}

	public String getAssignRemarks() {
		return CommonUtility.replaceNull(assignRemarks);
	}

	public void setAssignRemarks(String assignRemarks) {
		this.assignRemarks = assignRemarks;
	}

	public String getReturnTo() {
		return CommonUtility.replaceNull(returnTo);
	}

	public void setReturnTo(String returnTo) {
		this.returnTo = returnTo;
	}

	public String getReturnToRemarks() {
		return CommonUtility.replaceNull(returnToRemarks);
	}

	public void setReturnToRemarks(String returnToRemarks) {
		this.returnToRemarks = returnToRemarks;
	}

	public String getSm_name() {
		return CommonUtility.replaceNull(sm_name);
	}

	public void setSm_name(String sm_name) {
		this.sm_name = sm_name;
	}

	public String getSmEmail() {
		return CommonUtility.replaceNull(smEmail);
	}

	public void setSmEmail(String smEmail) {
		this.smEmail = smEmail;
	}

	public String getSmContactNo() {
		return CommonUtility.replaceNull(smContactNo);
	}

	public void setSmContactNo(String smContactNo) {
		this.smContactNo = smContactNo;
	}

	public String getDeptAndAuth() {
		return CommonUtility.replaceNull(deptAndAuth);
	}

	public void setDeptAndAuth(String deptAndAuth) {
		this.deptAndAuth = deptAndAuth;
	}

	public String getDesignEngineerName() {
		return CommonUtility.replaceNull(designEngineerName);
	}

	public void setDesignEngineerName(String designEngineerName) {
		this.designEngineerName = designEngineerName;
	}

	public String getPagechecking() {
		return CommonUtility.replaceNull(pagechecking);
	}

	public void setPagechecking(String pagechecking) {
		this.pagechecking = pagechecking;
	}

	public ArrayList getCustomerLocationList() {
		return customerLocationList;
	}

	public void setCustomerLocationList(ArrayList customerLocationList) {
		this.customerLocationList = customerLocationList;
	}

	public String getSearchMfgLocation() {
		return CommonUtility.replaceNull(searchMfgLocation);
	}

	public void setSearchMfgLocation(String searchMfgLocation) {
		this.searchMfgLocation = searchMfgLocation;
	}

	public String getSearchProdLine() {
		return CommonUtility.replaceNull(searchProdLine);
	}

	public void setSearchProdLine(String searchProdLine) {
		this.searchProdLine = searchProdLine;
	}

	public String getSearchKW() {
		return CommonUtility.replaceNull(searchKW);
	}

	public void setSearchKW(String searchKW) {
		this.searchKW = searchKW;
	}

	public String getSearchPole() {
		return CommonUtility.replaceNull(searchPole);
	}

	public void setSearchPole(String searchPole) {
		this.searchPole = searchPole;
	}

	public String getSearchFrame() {
		return CommonUtility.replaceNull(searchFrame);
	}

	public void setSearchFrame(String searchFrame) {
		this.searchFrame = searchFrame;
	}

	public String getSearchFrameSuffix() {
		return CommonUtility.replaceNull(searchFrameSuffix);
	}

	public void setSearchFrameSuffix(String searchFrameSuffix) {
		this.searchFrameSuffix = searchFrameSuffix;
	}

	public String getSearchMounting() {
		return CommonUtility.replaceNull(searchMounting);
	}

	public void setSearchMounting(String searchMounting) {
		this.searchMounting = searchMounting;
	}

	public String getSearchTBPosition() {
		return CommonUtility.replaceNull(searchTBPosition);
	}

	public void setSearchTBPosition(String searchTBPosition) {
		this.searchTBPosition = searchTBPosition;
	}

	public ArrayList<NewMTORatingDto> getNewMTORatingsList() {
		return newMTORatingsList;
	}

	public void setNewMTORatingsList(ArrayList<NewMTORatingDto> newMTORatingsList) {
		this.newMTORatingsList = newMTORatingsList;
	}

	public List<KeyValueVo> getNewCompleteAddOnsList() {
		return newCompleteAddOnsList;
	}

	public void setNewCompleteAddOnsList(List<KeyValueVo> newCompleteAddOnsList) {
		this.newCompleteAddOnsList = newCompleteAddOnsList;
	}
	
	public List<KeyValueVo> getMtoReassignUsersList() {
		return mtoReassignUsersList;
	}

	public void setMtoReassignUsersList(List<KeyValueVo> mtoReassignUsersList) {
		this.mtoReassignUsersList = mtoReassignUsersList;
	}

	public String getIsReferDesign() {
		return CommonUtility.replaceNull(isReferDesign);
	}

	public void setIsReferDesign(String isReferDesign) {
		this.isReferDesign = isReferDesign;
	}

	public String getSmNote() {
		return CommonUtility.replaceNull(smNote);
	}
	public void setSmNote(String smNote) {
		this.smNote = smNote;
	}
	public String getRsmNote() {
		return CommonUtility.replaceNull(rsmNote);
	}
	public void setRsmNote(String rsmNote) {
		this.rsmNote = rsmNote;
	}
	public String getNsmNote() {
		return CommonUtility.replaceNull(nsmNote);
	}
	public void setNsmNote(String nsmNote) {
		this.nsmNote = nsmNote;
	}
	public String getMhNote() {
		return CommonUtility.replaceNull(mhNote);
	}
	public void setMhNote(String mhNote) {
		this.mhNote = mhNote;
	}
	public String getDesignNote() {
		return CommonUtility.replaceNull(designNote);
	}
	public void setDesignNote(String designNote) {
		this.designNote = designNote;
	}
	public String getBhNote() {
		return CommonUtility.replaceNull(bhNote);
	}
	public void setBhNote(String bhNote) {
		this.bhNote = bhNote;
	}
	public String getMfgNote() {
		return CommonUtility.replaceNull(mfgNote);
	}
	public void setMfgNote(String mfgNote) {
		this.mfgNote = mfgNote;
	}
	public String getQaNote() {
		return CommonUtility.replaceNull(qaNote);
	}
	public void setQaNote(String qaNote) {
		this.qaNote = qaNote;
	}
	public String getScmNote() {
		return CommonUtility.replaceNull(scmNote);
	}
	public void setScmNote(String scmNote) {
		this.scmNote = scmNote;
	}

	public String getCommentsDeviations() {
		return CommonUtility.replaceNull(commentsDeviations);
	}

	public void setCommentsDeviations(String commentsDeviations) {
		this.commentsDeviations = commentsDeviations;
	}

	public String getReassignRemarks() {
		return CommonUtility.replaceNull(reassignRemarks);
	}

	public void setReassignRemarks(String reassignRemarks) {
		this.reassignRemarks = reassignRemarks;
	}

	public ArrayList<NewRatingDto> getNewRatingsList() {
		return newRatingsList;
	}

	public void setNewRatingsList(ArrayList<NewRatingDto> newRatingsList) {
		this.newRatingsList = newRatingsList;
	}

	public List<KeyValueVo> getLtMfgLocationList() {
		return ltMfgLocationList;
	}

	public void setLtMfgLocationList(List<KeyValueVo> ltMfgLocationList) {
		this.ltMfgLocationList = ltMfgLocationList;
	}

	public List<KeyValueVo> getLtProductLineList() {
		return ltProductLineList;
	}

	public void setLtProductLineList(List<KeyValueVo> ltProductLineList) {
		this.ltProductLineList = ltProductLineList;
	}

	public List<KeyValueVo> getLtKWList() {
		return ltKWList;
	}

	public void setLtKWList(List<KeyValueVo> ltKWList) {
		this.ltKWList = ltKWList;
	}

	public List<KeyValueVo> getLtPoleList() {
		return ltPoleList;
	}

	public void setLtPoleList(List<KeyValueVo> ltPoleList) {
		this.ltPoleList = ltPoleList;
	}

	public List<KeyValueVo> getLtFrameList() {
		return ltFrameList;
	}

	public void setLtFrameList(List<KeyValueVo> ltFrameList) {
		this.ltFrameList = ltFrameList;
	}

	public List<KeyValueVo> getLtFrameSuffixList() {
		return ltFrameSuffixList;
	}

	public void setLtFrameSuffixList(List<KeyValueVo> ltFrameSuffixList) {
		this.ltFrameSuffixList = ltFrameSuffixList;
	}

	public List<KeyValueVo> getLtMountingList() {
		return ltMountingList;
	}

	public void setLtMountingList(List<KeyValueVo> ltMountingList) {
		this.ltMountingList = ltMountingList;
	}

	public List<KeyValueVo> getLtTBPositionList() {
		return ltTBPositionList;
	}

	public void setLtTBPositionList(List<KeyValueVo> ltTBPositionList) {
		this.ltTBPositionList = ltTBPositionList;
	}
	
	public List<KeyValueVo> getLtAmbientTemperatureList() {
		return ltAmbientTemperatureList;
	}

	public void setLtAmbientTemperatureList(List<KeyValueVo> ltAmbientTemperatureList) {
		this.ltAmbientTemperatureList = ltAmbientTemperatureList;
	}

	public List<KeyValueVo> getLtAmbientRemTempList() {
		return ltAmbientRemTempList;
	}

	public void setLtAmbientRemTempList(List<KeyValueVo> ltAmbientRemTempList) {
		this.ltAmbientRemTempList = ltAmbientRemTempList;
	}

	public List<KeyValueVo> getLtGasGroupList() {
		return ltGasGroupList;
	}

	public void setLtGasGroupList(List<KeyValueVo> ltGasGroupList) {
		this.ltGasGroupList = ltGasGroupList;
	}

	public List<KeyValueVo> getLtHazardZoneList() {
		return ltHazardZoneList;
	}

	public void setLtHazardZoneList(List<KeyValueVo> ltHazardZoneList) {
		this.ltHazardZoneList = ltHazardZoneList;
	}

	public List<KeyValueVo> getLtDustGroupList() {
		return ltDustGroupList;
	}

	public void setLtDustGroupList(List<KeyValueVo> ltDustGroupList) {
		this.ltDustGroupList = ltDustGroupList;
	}

	public List<KeyValueVo> getLtApplicationList() {
		return ltApplicationList;
	}

	public void setLtApplicationList(List<KeyValueVo> ltApplicationList) {
		this.ltApplicationList = ltApplicationList;
	}

	public List<KeyValueVo> getLtVoltageList() {
		return ltVoltageList;
	}

	public void setLtVoltageList(List<KeyValueVo> ltVoltageList) {
		this.ltVoltageList = ltVoltageList;
	}

	public List<KeyValueVo> getLtFrequencyList() {
		return ltFrequencyList;
	}

	public void setLtFrequencyList(List<KeyValueVo> ltFrequencyList) {
		this.ltFrequencyList = ltFrequencyList;
	}

	public List<KeyValueVo> getLtCombinedVariationList() {
		return ltCombinedVariationList;
	}

	public void setLtCombinedVariationList(List<KeyValueVo> ltCombinedVariationList) {
		this.ltCombinedVariationList = ltCombinedVariationList;
	}

	public List<KeyValueVo> getLtDutyList() {
		return ltDutyList;
	}

	public void setLtDutyList(List<KeyValueVo> ltDutyList) {
		this.ltDutyList = ltDutyList;
	}

	public List<KeyValueVo> getLtCDFList() {
		return ltCDFList;
	}

	public void setLtCDFList(List<KeyValueVo> ltCDFList) {
		this.ltCDFList = ltCDFList;
	}

	public List<KeyValueVo> getLtStartsList() {
		return ltStartsList;
	}

	public void setLtStartsList(List<KeyValueVo> ltStartsList) {
		this.ltStartsList = ltStartsList;
	}

	public List<KeyValueVo> getLtRVRAList() {
		return ltRVRAList;
	}

	public void setLtRVRAList(List<KeyValueVo> ltRVRAList) {
		this.ltRVRAList = ltRVRAList;
	}

	public List<KeyValueVo> getLtWindingTreatmentList() {
		return ltWindingTreatmentList;
	}

	public void setLtWindingTreatmentList(List<KeyValueVo> ltWindingTreatmentList) {
		this.ltWindingTreatmentList = ltWindingTreatmentList;
	}

	public List<KeyValueVo> getLtLeadList() {
		return ltLeadList;
	}

	public void setLtLeadList(List<KeyValueVo> ltLeadList) {
		this.ltLeadList = ltLeadList;
	}

	public List<KeyValueVo> getLtVFDTypeList() {
		return ltVFDTypeList;
	}

	public void setLtVFDTypeList(List<KeyValueVo> ltVFDTypeList) {
		this.ltVFDTypeList = ltVFDTypeList;
	}

	public List<KeyValueVo> getLtDualSpeedTypeList() {
		return ltDualSpeedTypeList;
	}

	public void setLtDualSpeedTypeList(List<KeyValueVo> ltDualSpeedTypeList) {
		this.ltDualSpeedTypeList = ltDualSpeedTypeList;
	}

	public List<KeyValueVo> getLtShaftMaterialTypeList() {
		return ltShaftMaterialTypeList;
	}

	public void setLtShaftMaterialTypeList(List<KeyValueVo> ltShaftMaterialTypeList) {
		this.ltShaftMaterialTypeList = ltShaftMaterialTypeList;
	}

	public List<KeyValueVo> getLtPaintingTypeList() {
		return ltPaintingTypeList;
	}

	public void setLtPaintingTypeList(List<KeyValueVo> ltPaintingTypeList) {
		this.ltPaintingTypeList = ltPaintingTypeList;
	}

	public List<KeyValueVo> getLtPaintShadeList() {
		return ltPaintShadeList;
	}

	public void setLtPaintShadeList(List<KeyValueVo> ltPaintShadeList) {
		this.ltPaintShadeList = ltPaintShadeList;
	}

	public List<KeyValueVo> getLtPaintThicknessList() {
		return ltPaintThicknessList;
	}

	public void setLtPaintThicknessList(List<KeyValueVo> ltPaintThicknessList) {
		this.ltPaintThicknessList = ltPaintThicknessList;
	}

	public List<KeyValueVo> getLtIPList() {
		return ltIPList;
	}

	public void setLtIPList(List<KeyValueVo> ltIPList) {
		this.ltIPList = ltIPList;
	}

	public List<KeyValueVo> getLtInsulationClassList() {
		return ltInsulationClassList;
	}

	public void setLtInsulationClassList(List<KeyValueVo> ltInsulationClassList) {
		this.ltInsulationClassList = ltInsulationClassList;
	}

	public List<KeyValueVo> getLtTerminalBoxList() {
		return ltTerminalBoxList;
	}

	public void setLtTerminalBoxList(List<KeyValueVo> ltTerminalBoxList) {
		this.ltTerminalBoxList = ltTerminalBoxList;
	}

	public List<KeyValueVo> getLtSpreaderBoxList() {
		return ltSpreaderBoxList;
	}

	public void setLtSpreaderBoxList(List<KeyValueVo> ltSpreaderBoxList) {
		this.ltSpreaderBoxList = ltSpreaderBoxList;
	}

	public List<KeyValueVo> getLtAuxTerminalBoxList() {
		return ltAuxTerminalBoxList;
	}

	public void setLtAuxTerminalBoxList(List<KeyValueVo> ltAuxTerminalBoxList) {
		this.ltAuxTerminalBoxList = ltAuxTerminalBoxList;
	}

	public List<KeyValueVo> getLtSpaceHeaterList() {
		return ltSpaceHeaterList;
	}

	public void setLtSpaceHeaterList(List<KeyValueVo> ltSpaceHeaterList) {
		this.ltSpaceHeaterList = ltSpaceHeaterList;
	}

	public List<KeyValueVo> getLtVibrationList() {
		return ltVibrationList;
	}

	public void setLtVibrationList(List<KeyValueVo> ltVibrationList) {
		this.ltVibrationList = ltVibrationList;
	}

	public List<KeyValueVo> getLtFlyingLeadList() {
		return ltFlyingLeadList;
	}

	public void setLtFlyingLeadList(List<KeyValueVo> ltFlyingLeadList) {
		this.ltFlyingLeadList = ltFlyingLeadList;
	}

	public List<KeyValueVo> getLtMetalFanList() {
		return ltMetalFanList;
	}

	public void setLtMetalFanList(List<KeyValueVo> ltMetalFanList) {
		this.ltMetalFanList = ltMetalFanList;
	}

	public List<KeyValueVo> getLtShaftGroundingList() {
		return ltShaftGroundingList;
	}

	public void setLtShaftGroundingList(List<KeyValueVo> ltShaftGroundingList) {
		this.ltShaftGroundingList = ltShaftGroundingList;
	}

	public List<KeyValueVo> getLtForcedCoolingList() {
		return ltForcedCoolingList;
	}

	public void setLtForcedCoolingList(List<KeyValueVo> ltForcedCoolingList) {
		this.ltForcedCoolingList = ltForcedCoolingList;
	}

	public List<KeyValueVo> getLtHardwareList() {
		return ltHardwareList;
	}

	public void setLtHardwareList(List<KeyValueVo> ltHardwareList) {
		this.ltHardwareList = ltHardwareList;
	}

	public List<KeyValueVo> getLtGlandPlateList() {
		return ltGlandPlateList;
	}

	public void setLtGlandPlateList(List<KeyValueVo> ltGlandPlateList) {
		this.ltGlandPlateList = ltGlandPlateList;
	}

	public List<KeyValueVo> getLtDoubleCompressionGlandList() {
		return ltDoubleCompressionGlandList;
	}

	public void setLtDoubleCompressionGlandList(List<KeyValueVo> ltDoubleCompressionGlandList) {
		this.ltDoubleCompressionGlandList = ltDoubleCompressionGlandList;
	}

	public List<KeyValueVo> getLtSPMMountingProvisionList() {
		return ltSPMMountingProvisionList;
	}

	public void setLtSPMMountingProvisionList(List<KeyValueVo> ltSPMMountingProvisionList) {
		this.ltSPMMountingProvisionList = ltSPMMountingProvisionList;
	}

	public List<KeyValueVo> getLtAddNamePlateList() {
		return ltAddNamePlateList;
	}

	public void setLtAddNamePlateList(List<KeyValueVo> ltAddNamePlateList) {
		this.ltAddNamePlateList = ltAddNamePlateList;
	}

	public List<KeyValueVo> getLtArrowPlateDirectionList() {
		return ltArrowPlateDirectionList;
	}

	public void setLtArrowPlateDirectionList(List<KeyValueVo> ltArrowPlateDirectionList) {
		this.ltArrowPlateDirectionList = ltArrowPlateDirectionList;
	}

	public List<KeyValueVo> getLtRTDList() {
		return ltRTDList;
	}

	public void setLtRTDList(List<KeyValueVo> ltRTDList) {
		this.ltRTDList = ltRTDList;
	}

	public List<KeyValueVo> getLtBTDList() {
		return ltBTDList;
	}

	public void setLtBTDList(List<KeyValueVo> ltBTDList) {
		this.ltBTDList = ltBTDList;
	}

	public List<KeyValueVo> getLtThermisterList() {
		return ltThermisterList;
	}

	public void setLtThermisterList(List<KeyValueVo> ltThermisterList) {
		this.ltThermisterList = ltThermisterList;
	}

	public List<KeyValueVo> getLtCableSealingBoxList() {
		return ltCableSealingBoxList;
	}

	public void setLtCableSealingBoxList(List<KeyValueVo> ltCableSealingBoxList) {
		this.ltCableSealingBoxList = ltCableSealingBoxList;
	}

	public List<KeyValueVo> getLtBearingSystemList() {
		return ltBearingSystemList;
	}

	public void setLtBearingSystemList(List<KeyValueVo> ltBearingSystemList) {
		this.ltBearingSystemList = ltBearingSystemList;
	}

	public List<KeyValueVo> getLtBearingNDEList() {
		return ltBearingNDEList;
	}

	public void setLtBearingNDEList(List<KeyValueVo> ltBearingNDEList) {
		this.ltBearingNDEList = ltBearingNDEList;
	}

	public List<KeyValueVo> getLtBearingDEList() {
		return ltBearingDEList;
	}

	public void setLtBearingDEList(List<KeyValueVo> ltBearingDEList) {
		this.ltBearingDEList = ltBearingDEList;
	}

	public List<KeyValueVo> getLtWitnessRoutineList() {
		return ltWitnessRoutineList;
	}

	public void setLtWitnessRoutineList(List<KeyValueVo> ltWitnessRoutineList) {
		this.ltWitnessRoutineList = ltWitnessRoutineList;
	}

	public List<KeyValueVo> getLtAdditionalTestList() {
		return ltAdditionalTestList;
	}

	public void setLtAdditionalTestList(List<KeyValueVo> ltAdditionalTestList) {
		this.ltAdditionalTestList = ltAdditionalTestList;
	}

	public List<KeyValueVo> getLtTypeTestList() {
		return ltTypeTestList;
	}

	public void setLtTypeTestList(List<KeyValueVo> ltTypeTestList) {
		this.ltTypeTestList = ltTypeTestList;
	}

	public List<KeyValueVo> getLtULCEList() {
		return ltULCEList;
	}

	public void setLtULCEList(List<KeyValueVo> ltULCEList) {
		this.ltULCEList = ltULCEList;
	}

	public List<KeyValueVo> getLtQAPList() {
		return ltQAPList;
	}

	public void setLtQAPList(List<KeyValueVo> ltQAPList) {
		this.ltQAPList = ltQAPList;
	}

	public List<KeyValueVo> getLtSeaworthyPackingList() {
		return ltSeaworthyPackingList;
	}

	public void setLtSeaworthyPackingList(List<KeyValueVo> ltSeaworthyPackingList) {
		this.ltSeaworthyPackingList = ltSeaworthyPackingList;
	}

	public List<KeyValueVo> getLtWarrantyList() {
		return ltWarrantyList;
	}

	public void setLtWarrantyList(List<KeyValueVo> ltWarrantyList) {
		this.ltWarrantyList = ltWarrantyList;
	}

	public List<KeyValueVo> getLtVoltVariationList() {
		return ltVoltVariationList;
	}

	public void setLtVoltVariationList(List<KeyValueVo> ltVoltVariationList) {
		this.ltVoltVariationList = ltVoltVariationList;
	}

	public List<KeyValueVo> getLtFreqVariationList() {
		return ltFreqVariationList;
	}

	public void setLtFreqVariationList(List<KeyValueVo> ltFreqVariationList) {
		this.ltFreqVariationList = ltFreqVariationList;
	}

	public List<KeyValueVo> getLtStartingDOLCurrentList() {
		return ltStartingDOLCurrentList;
	}

	public void setLtStartingDOLCurrentList(List<KeyValueVo> ltStartingDOLCurrentList) {
		this.ltStartingDOLCurrentList = ltStartingDOLCurrentList;
	}

	public List<KeyValueVo> getLtWindingConfigurationList() {
		return ltWindingConfigurationList;
	}

	public void setLtWindingConfigurationList(List<KeyValueVo> ltWindingConfigurationList) {
		this.ltWindingConfigurationList = ltWindingConfigurationList;
	}

	public List<KeyValueVo> getLtMethodOfStartingList() {
		return ltMethodOfStartingList;
	}

	public void setLtMethodOfStartingList(List<KeyValueVo> ltMethodOfStartingList) {
		this.ltMethodOfStartingList = ltMethodOfStartingList;
	}

	public List<KeyValueVo> getLtWindingWireList() {
		return ltWindingWireList;
	}

	public void setLtWindingWireList(List<KeyValueVo> ltWindingWireList) {
		this.ltWindingWireList = ltWindingWireList;
	}

	public List<KeyValueVo> getLtVFDSpeedRangeMinList() {
		return ltVFDSpeedRangeMinList;
	}

	public void setLtVFDSpeedRangeMinList(List<KeyValueVo> ltVFDSpeedRangeMinList) {
		this.ltVFDSpeedRangeMinList = ltVFDSpeedRangeMinList;
	}

	public List<KeyValueVo> getLtVFDSpeedRangeMaxList() {
		return ltVFDSpeedRangeMaxList;
	}

	public void setLtVFDSpeedRangeMaxList(List<KeyValueVo> ltVFDSpeedRangeMaxList) {
		this.ltVFDSpeedRangeMaxList = ltVFDSpeedRangeMaxList;
	}

	public List<KeyValueVo> getLtOverloadingDutyList() {
		return ltOverloadingDutyList;
	}

	public void setLtOverloadingDutyList(List<KeyValueVo> ltOverloadingDutyList) {
		this.ltOverloadingDutyList = ltOverloadingDutyList;
	}

	public List<KeyValueVo> getLtConstantEffRangeList() {
		return ltConstantEffRangeList;
	}

	public void setLtConstantEffRangeList(List<KeyValueVo> ltConstantEffRangeList) {
		this.ltConstantEffRangeList = ltConstantEffRangeList;
	}

	public List<KeyValueVo> getLtShaftTypeList() {
		return ltShaftTypeList;
	}

	public void setLtShaftTypeList(List<KeyValueVo> ltShaftTypeList) {
		this.ltShaftTypeList = ltShaftTypeList;
	}

	public List<KeyValueVo> getLtDirectionOfRotationList() {
		return ltDirectionOfRotationList;
	}

	public void setLtDirectionOfRotationList(List<KeyValueVo> ltDirectionOfRotationList) {
		this.ltDirectionOfRotationList = ltDirectionOfRotationList;
	}

	public List<KeyValueVo> getLtMethodOfCouplingList() {
		return ltMethodOfCouplingList;
	}

	public void setLtMethodOfCouplingList(List<KeyValueVo> ltMethodOfCouplingList) {
		this.ltMethodOfCouplingList = ltMethodOfCouplingList;
	}

	public List<KeyValueVo> getLtTechoMountingList() {
		return ltTechoMountingList;
	}

	public void setLtTechoMountingList(List<KeyValueVo> ltTechoMountingList) {
		this.ltTechoMountingList = ltTechoMountingList;
	}

	public List<KeyValueVo> getLtDataSheetList() {
		return ltDataSheetList;
	}

	public void setLtDataSheetList(List<KeyValueVo> ltDataSheetList) {
		this.ltDataSheetList = ltDataSheetList;
	}

	public List<KeyValueVo> getLtNonStandardVoltageList() {
		return ltNonStandardVoltageList;
	}

	public void setLtNonStandardVoltageList(List<KeyValueVo> ltNonStandardVoltageList) {
		this.ltNonStandardVoltageList = ltNonStandardVoltageList;
	}

	public List<KeyValueVo> getLtHazAreaList() {
		return ltHazAreaList;
	}

	public void setLtHazAreaList(List<KeyValueVo> ltHazAreaList) {
		this.ltHazAreaList = ltHazAreaList;
	}

	public List<KeyValueVo> getLtHazAreaTypeList() {
		return ltHazAreaTypeList;
	}

	public void setLtHazAreaTypeList(List<KeyValueVo> ltHazAreaTypeList) {
		this.ltHazAreaTypeList = ltHazAreaTypeList;
	}

	public List<KeyValueVo> getLtServiceFactorList() {
		return ltServiceFactorList;
	}

	public void setLtServiceFactorList(List<KeyValueVo> ltServiceFactorList) {
		this.ltServiceFactorList = ltServiceFactorList;
	}

	public List<KeyValueVo> getLtStandardRotationList() {
		return ltStandardRotationList;
	}

	public void setLtStandardRotationList(List<KeyValueVo> ltStandardRotationList) {
		this.ltStandardRotationList = ltStandardRotationList;
	}

	public List<KeyValueVo> getLtTemperatureRiseList() {
		return ltTemperatureRiseList;
	}

	public void setLtTemperatureRiseList(List<KeyValueVo> ltTemperatureRiseList) {
		this.ltTemperatureRiseList = ltTemperatureRiseList;
	}

	public List<KeyValueVo> getLtNoiseLevelList() {
		return ltNoiseLevelList;
	}

	public void setLtNoiseLevelList(List<KeyValueVo> ltNoiseLevelList) {
		this.ltNoiseLevelList = ltNoiseLevelList;
	}

	public List<KeyValueVo> getLtPerfCurvesList() {
		return ltPerfCurvesList;
	}

	public void setLtPerfCurvesList(List<KeyValueVo> ltPerfCurvesList) {
		this.ltPerfCurvesList = ltPerfCurvesList;
	}

	public List<KeyValueVo> getLtCableSizeList() {
		return ltCableSizeList;
	}

	public void setLtCableSizeList(List<KeyValueVo> ltCableSizeList) {
		this.ltCableSizeList = ltCableSizeList;
	}

	public List<KeyValueVo> getLtNoOfRunsList() {
		return ltNoOfRunsList;
	}

	public void setLtNoOfRunsList(List<KeyValueVo> ltNoOfRunsList) {
		this.ltNoOfRunsList = ltNoOfRunsList;
	}

	public List<KeyValueVo> getLtNoOfCoresList() {
		return ltNoOfCoresList;
	}

	public void setLtNoOfCoresList(List<KeyValueVo> ltNoOfCoresList) {
		this.ltNoOfCoresList = ltNoOfCoresList;
	}

	public List<KeyValueVo> getLtCrossSectionAreaList() {
		return ltCrossSectionAreaList;
	}

	public void setLtCrossSectionAreaList(List<KeyValueVo> ltCrossSectionAreaList) {
		this.ltCrossSectionAreaList = ltCrossSectionAreaList;
	}

	public List<KeyValueVo> getLtConductorMaterialList() {
		return ltConductorMaterialList;
	}

	public void setLtConductorMaterialList(List<KeyValueVo> ltConductorMaterialList) {
		this.ltConductorMaterialList = ltConductorMaterialList;
	}

	public List<KeyValueVo> getLtTemperatureClassList() {
		return ltTemperatureClassList;
	}

	public void setLtTemperatureClassList(List<KeyValueVo> ltTemperatureClassList) {
		this.ltTemperatureClassList = ltTemperatureClassList;
	}

	public List<KeyValueVo> getLtEfficiencyClassList() {
		return ltEfficiencyClassList;
	}

	public void setLtEfficiencyClassList(List<KeyValueVo> ltEfficiencyClassList) {
		this.ltEfficiencyClassList = ltEfficiencyClassList;
	}

	public List<KeyValueVo> getLtTypeOfGreaseList() {
		return ltTypeOfGreaseList;
	}

	public void setLtTypeOfGreaseList(List<KeyValueVo> ltTypeOfGreaseList) {
		this.ltTypeOfGreaseList = ltTypeOfGreaseList;
	}

	public List<KeyValueVo> getLtSparesList() {
		return ltSparesList;
	}

	public void setLtSparesList(List<KeyValueVo> ltSparesList) {
		this.ltSparesList = ltSparesList;
	}

	public List<KeyValueVo> getLtMotorGAList() {
		return ltMotorGAList;
	}

	public void setLtMotorGAList(List<KeyValueVo> ltMotorGAList) {
		this.ltMotorGAList = ltMotorGAList;
	}

	public List<KeyValueVo> getLtTBoxGAList() {
		return ltTBoxGAList;
	}

	public void setLtTBoxGAList(List<KeyValueVo> ltTBoxGAList) {
		this.ltTBoxGAList = ltTBoxGAList;
	}

	public ArrayList getAlVoltList() {
		return alVoltList;
	}

	public void setAlVoltList(ArrayList alVoltList) {
		this.alVoltList = alVoltList;
	}

	public ArrayList getAlMainTermBoxPSList() {
		return alMainTermBoxPSList;
	}

	public void setAlMainTermBoxPSList(ArrayList alMainTermBoxPSList) {
		this.alMainTermBoxPSList = alMainTermBoxPSList;
	}

	public ArrayList getAlNeutralTBList() {
		return alNeutralTBList;
	}

	public void setAlNeutralTBList(ArrayList alNeutralTBList) {
		this.alNeutralTBList = alNeutralTBList;
	}

	public ArrayList getAlCableEntryList() {
		return alCableEntryList;
	}

	public void setAlCableEntryList(ArrayList alCableEntryList) {
		this.alCableEntryList = alCableEntryList;
	}

	public ArrayList getAlBoTerminalsNoList() {
		return alBoTerminalsNoList;
	}

	public void setAlBoTerminalsNoList(ArrayList alBoTerminalsNoList) {
		this.alBoTerminalsNoList = alBoTerminalsNoList;
	}

	public ArrayList getAlLubricationList() {
		return alLubricationList;
	}

	public void setAlLubricationList(ArrayList alLubricationList) {
		this.alLubricationList = alLubricationList;
	}

	public ArrayList getAlnoiseLevelList() {
		return alnoiseLevelList;
	}

	public void setAlnoiseLevelList(ArrayList alnoiseLevelList) {
		this.alnoiseLevelList = alnoiseLevelList;
	}

	public ArrayList getAlMinStartVoltList() {
		return alMinStartVoltList;
	}

	public void setAlMinStartVoltList(ArrayList alMinStartVoltList) {
		this.alMinStartVoltList = alMinStartVoltList;
	}

	public ArrayList getAlBalancingList() {
		return alBalancingList;
	}

	public void setAlBalancingList(ArrayList alBalancingList) {
		this.alBalancingList = alBalancingList;
	}

	public ArrayList getAlBearingThermoDtList() {
		return alBearingThermoDtList;
	}

	public void setAlBearingThermoDtList(ArrayList alBearingThermoDtList) {
		this.alBearingThermoDtList = alBearingThermoDtList;
	}

	public ArrayList getDegproList() {
		return degproList;
	}

	public void setDegproList(ArrayList degproList) {
		this.degproList = degproList;
	}

	public ArrayList getTermBoxList() {
		return termBoxList;
	}

	public void setTermBoxList(ArrayList termBoxList) {
		this.termBoxList = termBoxList;
	}

	public ArrayList getEnclosureList() {
		return enclosureList;
	}

	public void setEnclosureList(ArrayList enclosureList) {
		this.enclosureList = enclosureList;
	}

	public ArrayList getAlTargetedList() {
		return alTargetedList;
	}

	public void setAlTargetedList(ArrayList alTargetedList) {
		this.alTargetedList = alTargetedList;
	}

	public ArrayList getAlIndustryList() {
		return alIndustryList;
	}

	public void setAlIndustryList(ArrayList alIndustryList) {
		this.alIndustryList = alIndustryList;
	}

	public ArrayList getAlEnquiryTypeList() {
		return alEnquiryTypeList;
	}

	public void setAlEnquiryTypeList(ArrayList alEnquiryTypeList) {
		this.alEnquiryTypeList = alEnquiryTypeList;
	}

	public ArrayList getAlWinChanceList() {
		return alWinChanceList;
	}

	public void setAlWinChanceList(ArrayList alWinChanceList) {
		this.alWinChanceList = alWinChanceList;
	}

	public int getTotalRatingIndex() {
		return totalRatingIndex;
	}

	public void setTotalRatingIndex(int totalRatingIndex) {
		this.totalRatingIndex = totalRatingIndex;
	}
	
	// QuotationLT - mtoTotalRatingIndex - To indicate number of MTO Ratings in Design(MTO) section.
	public int getMtoTotalRatingIndex() {
		return mtoTotalRatingIndex;
	}
	public void setMtoTotalRatingIndex(int mtoTotalRatingIndex) {
		this.mtoTotalRatingIndex = mtoTotalRatingIndex;
	}

	public List<KeyValueVo> getLtBroughtOutTerminalsList() {
		return ltBroughtOutTerminalsList;
	}

	public void setLtBroughtOutTerminalsList(List<KeyValueVo> ltBroughtOutTerminalsList) {
		this.ltBroughtOutTerminalsList = ltBroughtOutTerminalsList;
	}

	public List<KeyValueVo> getLtMainTBList() {
		return ltMainTBList;
	}

	public void setLtMainTBList(List<KeyValueVo> ltMainTBList) {
		this.ltMainTBList = ltMainTBList;
	}

	public List<KeyValueVo> getLtCableEntryList() {
		return ltCableEntryList;
	}

	public void setLtCableEntryList(List<KeyValueVo> ltCableEntryList) {
		this.ltCableEntryList = ltCableEntryList;
	}

	public List<KeyValueVo> getLtLubricationList() {
		return ltLubricationList;
	}

	public void setLtLubricationList(List<KeyValueVo> ltLubricationList) {
		this.ltLubricationList = ltLubricationList;
	}

	public String getIsDesignCompleteCheck() {
		return CommonUtility.replaceNull(isDesignCompleteCheck);
	}

	public void setIsDesignCompleteCheck(String isDesignCompleteCheck) {
		this.isDesignCompleteCheck = isDesignCompleteCheck;
	}
	
	
}
