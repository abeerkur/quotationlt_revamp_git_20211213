package in.com.rbc.quotation.enquiry.dto;

import in.com.rbc.quotation.common.utility.CommonUtility;

public class UserDetailsDto {
	
	private String userDetailsId;
	private String userDetailsSSO;
	private String userDetailsType;
	private String userDetailsIsDeleted;
	
	public String getUserDetailsId() {
		return CommonUtility.replaceNull(userDetailsId);
	}
	public void setUserDetailsId(String userDetailsId) {
		this.userDetailsId = userDetailsId;
	}
	public String getUserDetailsSSO() {
		return CommonUtility.replaceNull(userDetailsSSO);
	}
	public void setUserDetailsSSO(String userDetailsSSO) {
		this.userDetailsSSO = userDetailsSSO;
	}
	public String getUserDetailsType() {
		return CommonUtility.replaceNull(userDetailsType);
	}
	public void setUserDetailsType(String userDetailsType) {
		this.userDetailsType = userDetailsType;
	}
	public String getUserDetailsIsDeleted() {
		return CommonUtility.replaceNull(userDetailsIsDeleted);
	}
	public void setUserDetailsIsDeleted(String userDetailsIsDeleted) {
		this.userDetailsIsDeleted = userDetailsIsDeleted;
	}
	
}
