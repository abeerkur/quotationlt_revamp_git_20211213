package in.com.rbc.quotation.enquiry.utility;

import java.sql.Connection;
import java.util.ArrayList;

import org.apache.commons.lang.StringUtils;

import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.utility.CommonUtility;
import in.com.rbc.quotation.common.utility.LoggerUtility;
import in.com.rbc.quotation.enquiry.dao.MTOQuotationDao;
import in.com.rbc.quotation.enquiry.dao.QuotationDao;
import in.com.rbc.quotation.enquiry.dto.NewEnquiryDto;
import in.com.rbc.quotation.enquiry.dto.NewRatingDto;
import in.com.rbc.quotation.enquiry.dto.NewTechnicalOfferDto;
import in.com.rbc.quotation.factory.DaoFactory;

public class NewPDFUtilityManager {

	
	public ArrayList<NewTechnicalOfferDto> fetchRatingPDFList(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception {
		String sMethodName = "fetchRatingPDFList";
    	String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        // Initialize NewTechnicalOfferDTO List.
        ArrayList<NewTechnicalOfferDto> alPDFRatingsList = new ArrayList<>();
        // NewTechnicalOfferDto - Build the Object as per PDF Rules.
        NewTechnicalOfferDto oTempTechnicalOfferDto = null;
        DaoFactory oDaoFactory = null;
        QuotationDao oQuotationDao = null;
        MTOQuotationDao oMTOQuotationDao = null;
        
        try {
        	/* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
            /* Retrieving QuotationDao object */
            oQuotationDao = oDaoFactory.getQuotationDao();
            /* Retrieving the MTOQuotationDao object */
            oMTOQuotationDao = oDaoFactory.getMTOQuotationDao();
            
            // Fetch PDF ProdLines Info.
            ArrayList<String> alPDFProdLinesList = oMTOQuotationDao.getPDFProdLinesList(conn);
            // Fetch the Complete Ratings List.
            ArrayList<NewRatingDto> alNewRatingsList = oMTOQuotationDao.fetchMTORatingDetailsForView(conn, oNewEnquiryDto);
            
            // Process Ratings List : For Each Rating, Check if Technical Offer DTO needs to be constructed.
            for(NewRatingDto oNewRatingDto : alNewRatingsList) {
            	if(alPDFProdLinesList.contains(oNewRatingDto.getLtProdLineId())) {
            		
            		// Fetch Standard Technical Offer Details.
            		NewTechnicalOfferDto oStandardTechOfferDto = oQuotationDao.buildNewTechnicalOfferDto(conn, oNewRatingDto); 
            		
            		// Initialize oTempTechnicalOfferDto - And Set the Appropriate values - As per PDF Rules.
            		oTempTechnicalOfferDto = new NewTechnicalOfferDto();
            		
            		// Set EnquiryId, RatingNo, Quantity - Basic Fields.
            		oTempTechnicalOfferDto.setEnquiryId(oNewRatingDto.getEnquiryId());
            		oTempTechnicalOfferDto.setRatingNo(oNewRatingDto.getRatingNo());
            		oTempTechnicalOfferDto.setQuantity(oNewRatingDto.getQuantity());
            		
            		// Rule 1 : Set Mandatory Field values from Rating Table : This Rule works for Both Standard and MTO Orders.
            		oTempTechnicalOfferDto.setMfgLocation(oNewRatingDto.getLtManufacturingLocation());
            		oTempTechnicalOfferDto.setProductLine(oNewRatingDto.getLtProdLineId());
            		oTempTechnicalOfferDto.setPowerRating_KW(oNewRatingDto.getLtKWId());
            		oTempTechnicalOfferDto.setFrameType(oNewRatingDto.getLtFrameId());
            		oTempTechnicalOfferDto.setFrameSuffix(oNewRatingDto.getLtFrameSuffixId());
            		oTempTechnicalOfferDto.setNoOfPoles(Integer.parseInt(oNewRatingDto.getLtPoleId()));
            		oTempTechnicalOfferDto.setMountingType(oNewRatingDto.getLtMountingId());
            		oTempTechnicalOfferDto.setTbPosition(oNewRatingDto.getLtTBPositionId());
            		
            		// Rule 2: Use the Voltage and Frequency values from Rating Table.
            		oTempTechnicalOfferDto.setVoltage(oNewRatingDto.getLtVoltId());
            		oTempTechnicalOfferDto.setFrequency(oNewRatingDto.getLtFreqId());
            		
            		// Rule 3 : If Technical Offer Present - Compare and Apply appropriate values.
            		if(oStandardTechOfferDto != null) {
            			// Fields Not in DM Page Rating.
            			oTempTechnicalOfferDto.setProductGroup(oStandardTechOfferDto.getProductGroup());
            			oTempTechnicalOfferDto.setPowerRating_HP(oStandardTechOfferDto.getPowerRating_HP());
            			oTempTechnicalOfferDto.setFrameMaterial(oStandardTechOfferDto.getFrameMaterial());
            			oTempTechnicalOfferDto.setDesign(oStandardTechOfferDto.getDesign());
            			oTempTechnicalOfferDto.setRotortype(oStandardTechOfferDto.getRotortype());
            			oTempTechnicalOfferDto.setRotorConnection(oStandardTechOfferDto.getRotorConnection());
            			oTempTechnicalOfferDto.setStatorConnection(oStandardTechOfferDto.getStatorConnection());
            			oTempTechnicalOfferDto.setNoOfStarts(oStandardTechOfferDto.getNoOfStarts());
            			oTempTechnicalOfferDto.setLrWithstandTime(oStandardTechOfferDto.getLrWithstandTime());
            			oTempTechnicalOfferDto.setCableSize(oStandardTechOfferDto.getCableSize());
            			
            			// If MTO Rating? - Clarification.
            			oTempTechnicalOfferDto.setModelNumber(oStandardTechOfferDto.getModelNumber());
            			
            			// Check Voltage and Frequency - If Matching Fetch TechnicalData Table values / Else Rating Table values.
            			String sTechOfferVoltage = CommonUtility.fetchBaseFrameType(oStandardTechOfferDto.getVoltage());
                		String sRatingDataVoltage = CommonUtility.fetchBaseFrameType(oNewRatingDto.getLtVoltId());
                		String sTechOfferFrequency = CommonUtility.fetchBaseFrameType(oStandardTechOfferDto.getFrequency());
                		String sRatingDataFrequency = CommonUtility.fetchBaseFrameType(oNewRatingDto.getLtFreqId());
                		
                		if(sTechOfferVoltage.equalsIgnoreCase(sRatingDataVoltage) && sTechOfferFrequency.equalsIgnoreCase(sRatingDataFrequency)) {
                			
                			// Common Fields : DM Page Rating and Standard TechOffer - Compare and Set values.
                			/*if(StringUtils.isBlank(oNewRatingDto.getLtPerfRatedCurr()) || (StringUtils.isNotBlank(oNewRatingDto.getLtPerfRatedCurr()) && CommonUtility.compareDecimalValues(oStandardTechOfferDto.getCurrent_A(), oNewRatingDto.getLtPerfRatedCurr()) == 0 ) ) {
                				if(StringUtils.isNotBlank(oStandardTechOfferDto.getCurrent_A())) {
                					oTempTechnicalOfferDto.setCurrent_A(CommonUtility.round(Double.parseDouble(oStandardTechOfferDto.getCurrent_A()), 2));
                				}
                			} else {
                				oTempTechnicalOfferDto.setCurrent_A(oNewRatingDto.getLtPerfRatedCurr());
                			}*/
                			double dTechOfferVoltage = StringUtils.isNotBlank(sTechOfferVoltage) ? CommonUtility.getDoubleValue(sTechOfferVoltage) : 0.0d;
                			double dRatingDataVoltage = StringUtils.isNotBlank(sRatingDataVoltage) ? CommonUtility.getDoubleValue(sRatingDataVoltage) : 0.0d;
                			double dTechOfferCurrent = StringUtils.isNotBlank(oStandardTechOfferDto.getCurrent_A()) ? CommonUtility.getDoubleValue(oStandardTechOfferDto.getCurrent_A()) : 0.0d;
                			
                			double actual_current = (dTechOfferVoltage/dRatingDataVoltage) * dTechOfferCurrent;
                			
                			oTempTechnicalOfferDto.setCurrent_A(CommonUtility.round(actual_current, 2));          			
                		}
                		// Change Request :: Only Current value changes for Voltage/Frequency change.
                		// Remaining Fields : Show as usual.
                		if(StringUtils.isBlank(oNewRatingDto.getLtPerfRatedSpeed()) || (StringUtils.isNotBlank(oNewRatingDto.getLtPerfRatedSpeed()) && CommonUtility.compareDecimalValues(oStandardTechOfferDto.getMaxSpeed(), oNewRatingDto.getLtPerfRatedSpeed()) == 0 ) ) {
            				if(StringUtils.isNotBlank(oStandardTechOfferDto.getMaxSpeed())) {
            					oTempTechnicalOfferDto.setMaxSpeed(CommonUtility.round(Double.parseDouble(oStandardTechOfferDto.getMaxSpeed()), 2));
            				}
            			} else {
            				oTempTechnicalOfferDto.setMaxSpeed(oNewRatingDto.getLtPerfRatedSpeed());
            			}
            			if(StringUtils.isBlank(oNewRatingDto.getLtPerfMaxTorque()) || (StringUtils.isNotBlank(oNewRatingDto.getLtPerfMaxTorque()) && CommonUtility.compareDecimalValues(oStandardTechOfferDto.getTorque(), oNewRatingDto.getLtPerfMaxTorque()) == 0 ) ) {
            				if(StringUtils.isNotBlank(oStandardTechOfferDto.getTorque())) {
            					long torque_longVal = Double.valueOf(oStandardTechOfferDto.getTorque()).longValue();
            					oTempTechnicalOfferDto.setTorque(String.valueOf(torque_longVal));
            				}
            			} else {;
            				long torque_longVal = Double.valueOf(oNewRatingDto.getLtPerfMaxTorque()).longValue();
            				oTempTechnicalOfferDto.setTorque(String.valueOf(torque_longVal));
            			}
            			if(StringUtils.isBlank(oNewRatingDto.getLtEffClass()) || (StringUtils.isNotBlank(oNewRatingDto.getLtEffClass()) && CommonUtility.compareStrings(oStandardTechOfferDto.getEfficiencyClass(), oNewRatingDto.getLtEffClass()) ) ) {
            				oTempTechnicalOfferDto.setEfficiencyClass(oStandardTechOfferDto.getEfficiencyClass());
            			} else {
            				oTempTechnicalOfferDto.setEfficiencyClass(oNewRatingDto.getLtEffClass());
            			}
            			
            			// Setting % Efficiency values at Load values.
            			if(StringUtils.isBlank(oNewRatingDto.getLtLoadEffPercent100Above()) || (StringUtils.isNotBlank(oNewRatingDto.getLtLoadEffPercent100Above()) && CommonUtility.compareDecimalValues(oStandardTechOfferDto.getEfficiency_100Above(), oNewRatingDto.getLtLoadEffPercent100Above()) == 0 ) ) {
            				if(StringUtils.isNotBlank(oStandardTechOfferDto.getEfficiency_100Above())) {
            					oTempTechnicalOfferDto.setEfficiency_100Above(CommonUtility.round(Double.parseDouble(oStandardTechOfferDto.getEfficiency_100Above()), 2));
            				}
            			} else {
            				oTempTechnicalOfferDto.setEfficiency_100Above(oNewRatingDto.getLtLoadEffPercent100Above());
            			}
            			if(StringUtils.isBlank(oNewRatingDto.getLtLoadEffPercentAt100()) || (StringUtils.isNotBlank(oNewRatingDto.getLtLoadEffPercentAt100()) && CommonUtility.compareDecimalValues(oStandardTechOfferDto.getEfficiency_100(), oNewRatingDto.getLtLoadEffPercentAt100()) == 0 ) ) {
            				if(StringUtils.isNotBlank(oStandardTechOfferDto.getEfficiency_100())) {
            					oTempTechnicalOfferDto.setEfficiency_100(CommonUtility.round(Double.parseDouble(oStandardTechOfferDto.getEfficiency_100()), 2));
            				}
            			} else {
            				oTempTechnicalOfferDto.setEfficiency_100(oNewRatingDto.getLtLoadEffPercentAt100());
            			}
            			if(StringUtils.isBlank(oNewRatingDto.getLtLoadEffPercentAt75()) || (StringUtils.isNotBlank(oNewRatingDto.getLtLoadEffPercentAt75()) && CommonUtility.compareDecimalValues(oStandardTechOfferDto.getEfficiency_75(), oNewRatingDto.getLtLoadEffPercentAt75()) == 0 ) ) {
            				if(StringUtils.isNotBlank(oStandardTechOfferDto.getEfficiency_75())) {
            					oTempTechnicalOfferDto.setEfficiency_75(CommonUtility.round(Double.parseDouble(oStandardTechOfferDto.getEfficiency_75()), 2));
            				}
            			} else {
            				oTempTechnicalOfferDto.setEfficiency_75(oNewRatingDto.getLtLoadEffPercentAt75());
            			}
            			if(StringUtils.isBlank(oNewRatingDto.getLtLoadEffPercentAt50()) || (StringUtils.isNotBlank(oNewRatingDto.getLtLoadEffPercentAt50()) && CommonUtility.compareDecimalValues(oStandardTechOfferDto.getEfficiency_50(), oNewRatingDto.getLtLoadEffPercentAt50()) == 0 ) ) {
            				if(StringUtils.isNotBlank(oStandardTechOfferDto.getEfficiency_50())) {
            					oTempTechnicalOfferDto.setEfficiency_50(CommonUtility.round(Double.parseDouble(oStandardTechOfferDto.getEfficiency_50()), 2));
            				}
            			} else {
            				oTempTechnicalOfferDto.setEfficiency_50(oNewRatingDto.getLtLoadEffPercentAt50());
            			}
            			
            			// Setting PF at Load values.
            			if(StringUtils.isBlank(oNewRatingDto.getLtLoadPFPercentAt100()) || (StringUtils.isNotBlank(oNewRatingDto.getLtLoadPFPercentAt100()) && CommonUtility.compareDecimalValues(oStandardTechOfferDto.getPowerFactor_100(), oNewRatingDto.getLtLoadPFPercentAt100()) == 0 ) ) {
            				if(StringUtils.isNotBlank(oStandardTechOfferDto.getPowerFactor_100())) {
            					oTempTechnicalOfferDto.setPowerFactor_100(CommonUtility.round(Double.parseDouble(oStandardTechOfferDto.getPowerFactor_100()), 2));
            				}
            			} else {
            				oTempTechnicalOfferDto.setPowerFactor_100(oNewRatingDto.getLtLoadPFPercentAt100());
            			}
            			if(StringUtils.isBlank(oNewRatingDto.getLtLoadPFPercentAt75()) || (StringUtils.isNotBlank(oNewRatingDto.getLtLoadPFPercentAt75()) && CommonUtility.compareDecimalValues(oStandardTechOfferDto.getPowerFactor_75(), oNewRatingDto.getLtLoadPFPercentAt75()) == 0 ) ) {
            				if(StringUtils.isNotBlank(oStandardTechOfferDto.getPowerFactor_75())) {
            					oTempTechnicalOfferDto.setPowerFactor_75(CommonUtility.round(Double.parseDouble(oStandardTechOfferDto.getPowerFactor_75()), 2));
            				}
            			} else {
            				oTempTechnicalOfferDto.setPowerFactor_75(oNewRatingDto.getLtLoadPFPercentAt75());
            			}
            			if(StringUtils.isBlank(oNewRatingDto.getLtLoadPFPercentAt50()) || (StringUtils.isNotBlank(oNewRatingDto.getLtLoadPFPercentAt50()) && CommonUtility.compareDecimalValues(oStandardTechOfferDto.getPowerFactor_50(), oNewRatingDto.getLtLoadPFPercentAt50()) == 0 ) ) {
            				if(StringUtils.isNotBlank(oStandardTechOfferDto.getPowerFactor_50())) {
            					oTempTechnicalOfferDto.setPowerFactor_50(CommonUtility.round(Double.parseDouble(oStandardTechOfferDto.getPowerFactor_50()), 2));
            				}
            			} else {
            				oTempTechnicalOfferDto.setPowerFactor_50(oNewRatingDto.getLtLoadPFPercentAt50());
            			}
            			
            			// Setting IA/IN,  TA/TN,  TK/TN.
            			// Check Logic Implementation ? - As of now, Loading Standard Technical Offer values.
            			if(StringUtils.isNotBlank(oStandardTechOfferDto.getStartingCurrent())) {
            				oTempTechnicalOfferDto.setStartingCurrent(CommonUtility.round(Double.parseDouble(oStandardTechOfferDto.getStartingCurrent()), 2));
            			} else {
            				oTempTechnicalOfferDto.setStartingCurrent("");
            			}
            			if(StringUtils.isNotBlank(oStandardTechOfferDto.getStarting_torque())) { 
            				oTempTechnicalOfferDto.setStarting_torque(oStandardTechOfferDto.getStarting_torque());
            			} else {
            				oTempTechnicalOfferDto.setStarting_torque("");
            			}
            			if(StringUtils.isNotBlank(oStandardTechOfferDto.getPullout_torque())) { 
            				oTempTechnicalOfferDto.setPullout_torque(oStandardTechOfferDto.getPullout_torque());
            			} else {
            				oTempTechnicalOfferDto.setPullout_torque("");
            			}
            			
                		
                		// RV / RA - Check for Only SlipRing Motors.
                		if(QuotationConstants.QUOTATIONLT_SLIPRING_KS.equalsIgnoreCase(oNewRatingDto.getLtProdLineId()) 
                				|| QuotationConstants.QUOTATIONLT_SLIPRING_KSCMR.equalsIgnoreCase(oNewRatingDto.getLtProdLineId())) {
                			if(StringUtils.isBlank(oNewRatingDto.getLtRVId()) || (StringUtils.isNotBlank(oNewRatingDto.getLtRVId()) && CommonUtility.compareDecimalValues(oStandardTechOfferDto.getRvValue(), oNewRatingDto.getLtRVId()) == 0 ) ) {
                				oTempTechnicalOfferDto.setRvValue(oStandardTechOfferDto.getRvValue());
                			} else {
                				oTempTechnicalOfferDto.setRvValue(oNewRatingDto.getLtRVId());
                			}
                			if(StringUtils.isBlank(oNewRatingDto.getLtRAId()) || (StringUtils.isNotBlank(oNewRatingDto.getLtRAId()) && CommonUtility.compareDecimalValues(oStandardTechOfferDto.getRaValue(), oNewRatingDto.getLtRAId()) == 0 ) ) {
                				oTempTechnicalOfferDto.setRaValue(oStandardTechOfferDto.getRaValue());
                			} else {
                				oTempTechnicalOfferDto.setRaValue(oNewRatingDto.getLtRAId());
                			}
                		}
                		
                		// ::::::::::::::::::::::::::::::::::::::::::: Comparing & Setting Common Fields ::::::::::::::::::::::::::::::::::::::::::::::
                		if(StringUtils.isBlank(oNewRatingDto.getLtEnclosure()) || (StringUtils.isNotBlank(oNewRatingDto.getLtEnclosure()) && CommonUtility.compareStrings(oStandardTechOfferDto.getEnclosureType(), oNewRatingDto.getLtEnclosure()) ) ) {
                			oTempTechnicalOfferDto.setEnclosureType(oStandardTechOfferDto.getEnclosureType());
                		} else {
                			oTempTechnicalOfferDto.setEnclosureType(oNewRatingDto.getLtEnclosure());
                		}
                		if(StringUtils.isBlank(oNewRatingDto.getLtDutyId()) || (StringUtils.isNotBlank(oNewRatingDto.getLtDutyId()) && CommonUtility.compareStrings(oStandardTechOfferDto.getDuty(), oNewRatingDto.getLtDutyId()) ) ) {
                			oTempTechnicalOfferDto.setDuty(oStandardTechOfferDto.getDuty());
                		} else {
                			oTempTechnicalOfferDto.setDuty(oNewRatingDto.getLtDutyId());
                		}
                		if(StringUtils.isBlank(oNewRatingDto.getLtVoltAddVariation()) || (StringUtils.isNotBlank(oNewRatingDto.getLtVoltAddVariation()) && CommonUtility.compareStrings(oStandardTechOfferDto.getVoltageAddVariation(), oNewRatingDto.getLtVoltAddVariation()) ) ) {
                			oTempTechnicalOfferDto.setVoltageAddVariation(oStandardTechOfferDto.getVoltageAddVariation());
                		} else {
                			oTempTechnicalOfferDto.setVoltageAddVariation(oNewRatingDto.getLtVoltAddVariation());
                		}
                		if(StringUtils.isBlank(oNewRatingDto.getLtVoltRemoveVariation()) || (StringUtils.isNotBlank(oNewRatingDto.getLtVoltRemoveVariation()) && CommonUtility.compareStrings(oStandardTechOfferDto.getVoltageRemoveVariation(), oNewRatingDto.getLtVoltRemoveVariation()) ) ) {
                			oTempTechnicalOfferDto.setVoltageRemoveVariation(oStandardTechOfferDto.getVoltageRemoveVariation());
                		} else {
                			oTempTechnicalOfferDto.setVoltageRemoveVariation(oNewRatingDto.getLtVoltRemoveVariation());
                		}
                		if(StringUtils.isBlank(oNewRatingDto.getLtFreqAddVariation()) || (StringUtils.isNotBlank(oNewRatingDto.getLtFreqAddVariation()) && CommonUtility.compareStrings(oStandardTechOfferDto.getFrequencyAddVariation(), oNewRatingDto.getLtFreqAddVariation()) ) ) {
                			oTempTechnicalOfferDto.setFrequencyAddVariation(oStandardTechOfferDto.getFrequencyAddVariation());
                		} else {
                			oTempTechnicalOfferDto.setFrequencyAddVariation(oNewRatingDto.getLtFreqAddVariation());
                		}
                		if(StringUtils.isBlank(oNewRatingDto.getLtFreqRemoveVariation()) || (StringUtils.isNotBlank(oNewRatingDto.getLtFreqRemoveVariation()) && CommonUtility.compareStrings(oStandardTechOfferDto.getFrequencyRemoveVariation(), oNewRatingDto.getLtFreqRemoveVariation()) ) ) {
                			oTempTechnicalOfferDto.setFrequencyRemoveVariation(oStandardTechOfferDto.getFrequencyRemoveVariation());
                		} else {
                			oTempTechnicalOfferDto.setFrequencyRemoveVariation(oNewRatingDto.getLtFreqRemoveVariation());
                		}
                		if(StringUtils.isBlank(oNewRatingDto.getLtCombVariation()) || (StringUtils.isNotBlank(oNewRatingDto.getLtCombVariation()) && CommonUtility.compareStrings(oStandardTechOfferDto.getCombinedVariation(), oNewRatingDto.getLtCombVariation()) ) ) {
                			oTempTechnicalOfferDto.setCombinedVariation(oStandardTechOfferDto.getCombinedVariation());
                		} else {
                			oTempTechnicalOfferDto.setCombinedVariation(oNewRatingDto.getLtCombVariation());
                		}
                		if(StringUtils.isBlank(oNewRatingDto.getLtServiceFactor()) || (StringUtils.isNotBlank(oNewRatingDto.getLtServiceFactor()) && CommonUtility.compareDecimalValues(oStandardTechOfferDto.getServiceFactor(), oNewRatingDto.getLtServiceFactor()) == 0 ) ) {
                			oTempTechnicalOfferDto.setServiceFactor(oStandardTechOfferDto.getServiceFactor());
                		} else {
                			oTempTechnicalOfferDto.setServiceFactor(oNewRatingDto.getLtServiceFactor());
                		}
                		if(StringUtils.isBlank(oNewRatingDto.getLtInsulationClassId()) || (StringUtils.isNotBlank(oNewRatingDto.getLtCombVariation()) && CommonUtility.compareStrings(oStandardTechOfferDto.getInsulationClass(), oNewRatingDto.getLtCombVariation()) ) ) {
                			oTempTechnicalOfferDto.setInsulationClass(oStandardTechOfferDto.getInsulationClass());
                		} else {
                			oTempTechnicalOfferDto.setInsulationClass(oNewRatingDto.getLtInsulationClassId());
                		}
                		if(StringUtils.isBlank(oNewRatingDto.getLtAmbTempId()) || (StringUtils.isNotBlank(oNewRatingDto.getLtAmbTempId()) && CommonUtility.compareStrings(oStandardTechOfferDto.getAmbientAddTemp(), oNewRatingDto.getLtAmbTempId()) ) ) {
                			oTempTechnicalOfferDto.setAmbientAddTemp(oStandardTechOfferDto.getAmbientAddTemp());
                		} else {
                			oTempTechnicalOfferDto.setAmbientAddTemp(oNewRatingDto.getLtAmbTempId());
                		}
                		if(StringUtils.isBlank(oNewRatingDto.getLtAmbTempRemoveId()) || (StringUtils.isNotBlank(oNewRatingDto.getLtAmbTempRemoveId()) && CommonUtility.compareStrings(oStandardTechOfferDto.getAmbientRemoveTemp(), oNewRatingDto.getLtAmbTempRemoveId()) ) ) {
                			oTempTechnicalOfferDto.setAmbientRemoveTemp(oStandardTechOfferDto.getAmbientRemoveTemp());
                		} else {
                			oTempTechnicalOfferDto.setAmbientRemoveTemp(oNewRatingDto.getLtAmbTempRemoveId());
                		}
                		if(StringUtils.isBlank(oNewRatingDto.getLtTempRise())) {
                			if(StringUtils.isNotBlank(oStandardTechOfferDto.getTempRise()) && oStandardTechOfferDto.getTempRise().length() > 2) {
                				oTempTechnicalOfferDto.setTempRise(oStandardTechOfferDto.getTempRise().substring(0, 2));
                			}
                		} else {
                			oTempTechnicalOfferDto.setTempRise(oNewRatingDto.getLtTempRise());
                			if(StringUtils.isNotBlank(oStandardTechOfferDto.getTempRise()) && oStandardTechOfferDto.getTempRise().length() > 2) {
                				String standardTempRise = oStandardTechOfferDto.getTempRise().substring(0, 2);
                    			if(standardTempRise.equalsIgnoreCase(oNewRatingDto.getLtTempRise())) {
                    				oTempTechnicalOfferDto.setTempRise(standardTempRise);
                    			}
                			}                 			
                		}
                		
                		oTempTechnicalOfferDto.setDeBearingType(oNewRatingDto.getLtBearingDEId());
                		oTempTechnicalOfferDto.setOdeBearingType(oNewRatingDto.getLtBearingNDEId());
                		// If Bearing Type = "Standard" - Then Set Bearing DE /NDE Size - Else Set Blanks.
                		if(QuotationConstants.QUOTATIONLT_STRING_STANDARD.equalsIgnoreCase(oNewRatingDto.getLtBearingSystemId())
                			&& QuotationConstants.QUOTATIONLT_STRING_STANDARD.equalsIgnoreCase(oNewRatingDto.getLtBearingDEId())
                			&& QuotationConstants.QUOTATIONLT_STRING_STANDARD.equalsIgnoreCase(oNewRatingDto.getLtBearingNDEId()) ) {
                			oTempTechnicalOfferDto.setDeBearingSize(oStandardTechOfferDto.getDeBearingSize());
                			oTempTechnicalOfferDto.setOdeBearingSize(oStandardTechOfferDto.getOdeBearingSize());
                		} else {
                			oTempTechnicalOfferDto.setDeBearingSize(QuotationConstants.QUOTATION_EMPTY);
                			oTempTechnicalOfferDto.setOdeBearingSize(QuotationConstants.QUOTATION_EMPTY);
                		}
                		
                		if(StringUtils.isBlank(oNewRatingDto.getLtLubrication()) || (StringUtils.isNotBlank(oNewRatingDto.getLtLubrication()) && CommonUtility.compareStrings(oStandardTechOfferDto.getLubMethod(), oNewRatingDto.getLtLubrication()) ) ) {
                			oTempTechnicalOfferDto.setLubMethod(oStandardTechOfferDto.getLubMethod());
                		} else {
                			oTempTechnicalOfferDto.setLubMethod(oNewRatingDto.getLtLubrication());
                		}
                		if(StringUtils.isBlank(oNewRatingDto.getLtGreaseType()) || (StringUtils.isNotBlank(oNewRatingDto.getLtGreaseType()) && CommonUtility.compareStrings(oStandardTechOfferDto.getTypeOfGrease(), oNewRatingDto.getLtGreaseType()) ) ) {
                			oTempTechnicalOfferDto.setTypeOfGrease(oStandardTechOfferDto.getTypeOfGrease());
                		} else {
                			oTempTechnicalOfferDto.setTypeOfGrease(oNewRatingDto.getLtGreaseType());
                		}
                		
                		if(QuotationConstants.QUOTATION_STRING_Y.equalsIgnoreCase(oNewRatingDto.getIsReferDesign())) {
                			oTempTechnicalOfferDto.setMotorWeight("");
                			oTempTechnicalOfferDto.setGrossWeight("");
                		} else {
                			oTempTechnicalOfferDto.setMotorWeight(oStandardTechOfferDto.getMotorWeight());
                			oTempTechnicalOfferDto.setGrossWeight(oStandardTechOfferDto.getGrossWeight());
                		}
                		
                		if(StringUtils.isBlank(oNewRatingDto.getLtCDFId()) || (StringUtils.isNotBlank(oNewRatingDto.getLtCDFId()) && CommonUtility.compareStrings(oStandardTechOfferDto.getCdf(), oNewRatingDto.getLtCDFId()) ) ) {
                			oTempTechnicalOfferDto.setCdf(oStandardTechOfferDto.getCdf());
                		} else {
                			oTempTechnicalOfferDto.setCdf(oNewRatingDto.getLtCDFId());
                		}
                		
                		if(StringUtils.isBlank(oNewRatingDto.getLtStartsId()) || (StringUtils.isNotBlank(oNewRatingDto.getLtStartsId()) && CommonUtility.compareStrings(oStandardTechOfferDto.getNoOfStarts_HR(), oNewRatingDto.getLtStartsId()) ) ) {
                			oTempTechnicalOfferDto.setNoOfStarts_HR(oStandardTechOfferDto.getNoOfStarts_HR());
                		} else {
                			oTempTechnicalOfferDto.setNoOfStarts_HR(oNewRatingDto.getLtStartsId());
                		}
                		
                		if(StringUtils.isBlank(oNewRatingDto.getLtIPId())) {
                			oTempTechnicalOfferDto.setIpCode(oStandardTechOfferDto.getIpCode());
                		} else {
                			String sIPCode = oNewRatingDto.getLtIPId().substring(2);
                			if(sIPCode.equalsIgnoreCase(oStandardTechOfferDto.getIpCode())) {
                				oTempTechnicalOfferDto.setIpCode(oStandardTechOfferDto.getIpCode());
                			} else {
                				oTempTechnicalOfferDto.setIpCode(oNewRatingDto.getLtIPId());
                			}
                		}
                		
                		if(StringUtils.isBlank(oNewRatingDto.getLtMotorGD2Value()) || (StringUtils.isNotBlank(oNewRatingDto.getLtMotorGD2Value()) && CommonUtility.compareDecimalValues(oStandardTechOfferDto.getMotorInertia(), oNewRatingDto.getLtMotorGD2Value()) == 0) ) {
                			oTempTechnicalOfferDto.setMotorInertia(oStandardTechOfferDto.getMotorInertia());
                		} else {
                			oTempTechnicalOfferDto.setMotorInertia(oNewRatingDto.getLtMotorGD2Value());
                		}
                		
                		if(StringUtils.isBlank(oStandardTechOfferDto.getLoadInertia())) {
                			oTempTechnicalOfferDto.setLoadInertia(oTempTechnicalOfferDto.getMotorInertia());
                		} else {
                			if(StringUtils.isBlank(oNewRatingDto.getLtLoadGD2Value()) || (StringUtils.isNotBlank(oNewRatingDto.getLtLoadGD2Value()) && CommonUtility.compareDecimalValues(oStandardTechOfferDto.getLoadInertia(), oNewRatingDto.getLtLoadGD2Value()) == 0) ) {
                				oTempTechnicalOfferDto.setLoadInertia(oStandardTechOfferDto.getLoadInertia());
                			} else {
                				oTempTechnicalOfferDto.setLoadInertia(oNewRatingDto.getLtLoadGD2Value());
                			}
                		}
                		
                		if(StringUtils.isBlank(oNewRatingDto.getLtVibrationId()) || (StringUtils.isNotBlank(oNewRatingDto.getLtVibrationId()) && CommonUtility.compareStrings(oStandardTechOfferDto.getVibration(), oNewRatingDto.getLtVibrationId()) ) ) {
                			oTempTechnicalOfferDto.setVibration(oStandardTechOfferDto.getVibration());
                		} else {
                			oTempTechnicalOfferDto.setVibration(oNewRatingDto.getLtVibrationId());
                		}
                		
                		if(StringUtils.isBlank(oNewRatingDto.getLtNoiseLevel()) || (StringUtils.isNotBlank(oNewRatingDto.getLtNoiseLevel()) && CommonUtility.compareStrings(oStandardTechOfferDto.getNoise(), oNewRatingDto.getLtNoiseLevel()) ) ) {
                			oTempTechnicalOfferDto.setNoise(oStandardTechOfferDto.getNoise());
                		} else {
                			oTempTechnicalOfferDto.setNoise(oNewRatingDto.getLtNoiseLevel());
                		}
                		
                		if(StringUtils.isBlank(oNewRatingDto.getLtMethodOfCoupling()) || (StringUtils.isNotBlank(oNewRatingDto.getLtMethodOfCoupling()) && CommonUtility.compareStrings(oStandardTechOfferDto.getMethodOfCoupling(), oNewRatingDto.getLtMethodOfCoupling()) ) ) {
                			oTempTechnicalOfferDto.setMethodOfCoupling(oStandardTechOfferDto.getMethodOfCoupling());
                		} else {
                			oTempTechnicalOfferDto.setMethodOfCoupling(oNewRatingDto.getLtMethodOfCoupling());
                		}
                		// Direction of Rotation.
                		if(StringUtils.isBlank(oNewRatingDto.getLtDirectionOfRotation()) || (StringUtils.isNotBlank(oNewRatingDto.getLtDirectionOfRotation()) && CommonUtility.compareStrings(oStandardTechOfferDto.getDirectionOfRotation(), oNewRatingDto.getLtDirectionOfRotation()) ) ) {
                			oTempTechnicalOfferDto.setDirectionOfRotation(oStandardTechOfferDto.getDirectionOfRotation());
                		} else {
                			oTempTechnicalOfferDto.setDirectionOfRotation(oNewRatingDto.getLtDirectionOfRotation());
                		}
                		// Standard Rotation.
                		if(StringUtils.isBlank(oNewRatingDto.getLtStandardRotation()) || (StringUtils.isNotBlank(oNewRatingDto.getLtStandardRotation()) && CommonUtility.compareStrings(oStandardTechOfferDto.getRotation(), oNewRatingDto.getLtStandardRotation()) ) ) {
                			oTempTechnicalOfferDto.setRotation(oStandardTechOfferDto.getRotation());
                		} else {
                			oTempTechnicalOfferDto.setRotation(oNewRatingDto.getLtStandardRotation());
                		}
                		
                		// IMP : Paint Shade, RTD, BTD, Thermister, Space Heater, Aux Terminal Box - These Fields are directly fetched from NewRatingDto. 
                		// Need not set abov Fields in 'oTempTechnicalOfferDto'.
                		
            		}
            		
            		// Rule 4 : Values to be Fetched Directly from Rating Table.
            		oTempTechnicalOfferDto.setIsReferDesign(oNewRatingDto.getIsReferDesign());
            		oTempTechnicalOfferDto.setAltitude(oNewRatingDto.getLtAltitude());
            		oTempTechnicalOfferDto.setHazArea(oNewRatingDto.getLtHazardAreaTypeId());
            		oTempTechnicalOfferDto.setZoneClassification(oNewRatingDto.getLtHazardZoneId());
            		oTempTechnicalOfferDto.setGasGroup(oNewRatingDto.getLtGasGroupId());
            		oTempTechnicalOfferDto.setTemperatureClass(oNewRatingDto.getLtTempClass());
            		oTempTechnicalOfferDto.setMotorOrientation(oNewRatingDto.getLtMountingId());
            		oTempTechnicalOfferDto.setCoolingMethod(oNewRatingDto.getLtForcedCoolingId());
            		oTempTechnicalOfferDto.setStartingType(oNewRatingDto.getLtMethodOfStarting());
            		oTempTechnicalOfferDto.setAuxTerminalBox(oNewRatingDto.getLtAuxTerminalBoxId());
            		
            		// Set the oTempTechnicalOfferDto to alPDFRatingsList.
            		if(oStandardTechOfferDto != null) {
            			alPDFRatingsList.add(oTempTechnicalOfferDto);
            		}
            	}
            }
        } catch(Exception e) {
        	e.printStackTrace();
        	
        }
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        return alPDFRatingsList;
	}
}
