/**
 * ******************************************************************************************
 * Project Name: Request For Quotation
 * Document Name: QuotationDao.java
 * Package: in.com.rbc.quotation.enquiry.dao
 * Desc:  Dao interface that defines methods related to Quaotation functionalities. <br>
 *        This interface is implemented by --> QuotationDaoImpl
 * ******************************************************************************************
 * @author 100006126
 * ******************************************************************************************
 */
package in.com.rbc.quotation.enquiry.dao;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import in.com.rbc.quotation.common.vo.KeyValueVo;
import in.com.rbc.quotation.enquiry.dto.EnquiryDto;
import in.com.rbc.quotation.enquiry.dto.NewEnquiryDto;
import in.com.rbc.quotation.enquiry.dto.NewMTORatingDto;
import in.com.rbc.quotation.enquiry.dto.NewRatingDto;
import in.com.rbc.quotation.enquiry.dto.NewTechnicalOfferDto;
import in.com.rbc.quotation.enquiry.dto.RatingDto;
import in.com.rbc.quotation.enquiry.dto.TeamMemberDto;
import in.com.rbc.quotation.enquiry.dto.TechnicalOfferDto;
import in.com.rbc.quotation.enquiry.dto.UserDetailsDto;
import in.com.rbc.quotation.user.dto.UserDto;
import in.com.rbc.quotation.workflow.dto.WorkflowDto;

public interface QuotationDao {
	
	/**
	 * Creates the enquiry with EnquiryDto details
	 * @param conn Connection object to connect to database
	 * @param oEnquiryDto EnquiryDto object having enquiry details
	 * @param sOperationType String object that defines type of operation
	 * @return Returns EnquiryDto object
	 * @throws Exception If any error occurs during the process
	 */
    public EnquiryDto createEnquiry(Connection conn, EnquiryDto oEnquiryDto, String sOperationType) throws Exception ;
    
    /**
	 * Creates a rating for an enquiry
	 * @param conn Connection object to connect to database
	 * @param oRatingDto RatingDto object having rating details
	 * @param sOperationType String object that defines type of operation
	 * @return Returns RatingDto object
	 * @throws Exception If any error occurs during the process
	 */
    public RatingDto createRating(Connection conn, RatingDto oRatingDto, String sOperationType) throws Exception ;
    
    /**
	 * Gets all the look up values
	 * @param conn Connection object to connect to database
	 * @param oEnquiryDto EnquiryDto object having enquiry details
	 * @return Returns the EnquiryDto object
	 * @throws Exception If any error occurs during the process
	 */
    public EnquiryDto loadLookUpValues(Connection conn, EnquiryDto oEnquiryDto) throws Exception ;
    
    /**
	 * Gets all the rating values that are associated with an enquiry
	 * @param conn Connection object to connect to database
	 * @param oEnquiryDto EnquiryDto object having enquiry details
	 * @return Returns an ArrayList object
	 * @throws Exception If any error occurs during the process
	 */
    public ArrayList getRatingListByEnquiryId(Connection conn, EnquiryDto oEnquiryDto) throws Exception ;
    
    /**
	 * Fetches the rating details
	 * @param conn Connection object to connect to database
	 * @param oRatingDto RatingDto object having rating details
	 * @return Returns RatingDto object
	 * @throws Exception If any error occurs during the process
	 */
    public RatingDto getRatingDetails(Connection conn, RatingDto oRatingDto) throws Exception ;    
    
    /**
	 * Fetches the enquiry and rating details based on an enquiryId
	 * @param conn Connection object to connect to database
	 * @param oEnquiryDto EnquiryDto object having enquiry details
	 * @param sOperationType String - to differentiate between edit and view request
	 * @return Returns EnquiryDto object
	 * @throws Exception If any error occurs during the process
	 */
    public EnquiryDto getEnquiryDetails(Connection conn, EnquiryDto oEnquiryDto, String sOperationType) throws Exception ;    
    
    /**
	 * Deletes a rating record
	 * @param conn Connection object to connect to database
	 * @param oRatingDto RatingDto object having rating details
	 * @return Returns boolean
	 * @throws Exception If any error occurs during the process
	 */
    public boolean deleteRating(Connection conn, RatingDto oRatingDto) throws Exception ;
    
    /**
	 * copy the Enquiry 
	 * @param conn Connection object to connect to database
	 * @param oEnquiryDto EnquiryDto object having enquiry details
	 * @return Returns the EnquiryDto object
	 * @throws Exception If any error occurs during the process
	 */
	public EnquiryDto copyEnquiry(Connection conn,EnquiryDto enquiryDto, UserDto userDto)throws Exception;
    
	/**
	 * delete the Enquiry 
	 * @param conn Connection object to connect to database
	 * @param oEnquiryDto EnquiryDto object having enquiry details
	 * @return Returns the boolean
	 * @throws Exception If any error occurs during the process
	 */
	public boolean deleteEnquiry(Connection conn, EnquiryDto enquiryDto)throws Exception;
	
	 /**
	 * Gets Sales Manager Id for the logged-in user
	 * @param conn Connection object to connect to database
	 * @param oEnquiryDto EnquiryDto object having enquiry details
	 * @return Returns the String object
	 * @throws Exception If any error occurs during the process
	 */
	public String getSalesManagerId(Connection conn, EnquiryDto oEnquiryDto)throws Exception;
	 
	/**
	 * Update Technical offer details of a rating
	 * @param conn Connection object to connect to database
	 * @param oTechnicalOfferDto TechnicalOfferDto object having enquiry details
	 * @return Returns the boolean
	 * @throws Exception If any error occurs during the process
	 */
	public boolean updatingTechincalOfferData(Connection conn, EnquiryDto oEnquiryDto, RatingDto oRatingDto, TechnicalOfferDto oTechnicalOfferDto)throws Exception;
	
	/**
	 * Delete all add on details for a rating
	 * @param conn Connection object to connect to database
	 * @param iRatingId int 
	 * @return Returns the boolean
	 * @throws Exception If any error occurs during the process
	 */
	public boolean deleteAllAddOnByRatingId(Connection conn, int iRatingId)throws Exception;
	
	/**
	 * Update Add on details of a rating
	 * @param conn Connection object to connect to database
	 * @param oRatingDto RatingDto object having rating details
	 * @return Returns the boolean
	 * @throws Exception If any error occurs during the process
	 */
	public boolean updateAddOn(Connection conn, RatingDto oRatingDto)throws Exception;
	
	/**
	 * Update Add on details of a rating
	 * @param conn Connection object to connect to database
	 * @param iRatingId int 
	 * @return Returns the ArrayList object
	 * @throws Exception If any error occurs during the process
	 */
	public ArrayList getAddOnList (Connection conn, int iRatingId) throws Exception;
	
	/**
	 * Gets the role details for an enquiry
	 * @param conn Connection object to connect to database
	 * @param oTeamMemberDto TeamMemberDto object having enquiry details
	 * @return Returns the ArrayList object
	 * @throws Exception If any error occurs during the process
	 */
	public TeamMemberDto getTeamMemberDetails (Connection conn,  TeamMemberDto oTeamMemberDto) throws Exception;
	
	/**
	 * Gets the role details for an enquiry
	 * @param  conn Connection object to connect to database
	 * @param  oEnquiryDto EnquiryDto object having enquiry details
	 * @param  sType String object
	 * @return Returns boolean
	 * @throws Exception If any error occurs during the process
	 */
	public boolean updateFactorDetails (Connection conn,  EnquiryDto oEnquiryDto, String sType) throws Exception;

	/**
	 * Updates an enquiry's terms
	 * @param conn Connection object to connect to database
	 * @param oTeamMemberDto TeamMemberDto object having enquiry details
	 * @return Returns the boolean
	 * @throws Exception If any error occurs during the process
	 */
	public boolean updateQuotationTerms(Connection conn, EnquiryDto enquiryDto) throws Exception;
	
	/**
	 * Used to get the direct managers list
	 * @param conn Connection object to connect to database
	 * @param sType String value of sType
	 * @param sCurrentUserId String value of current userid
	 * @return Returns the ArrayList object
	 * @throws Exception If any error occurs during the process
	 */
	public ArrayList getManagersList(Connection conn, String sType, String sCurrentUserId) throws Exception;
	
	/**
	 * User to check if the logged in user has access to view the enquiry page or not
	 * @param conn Connection object to connect to database
	 * @param oWorkflowDto WorkflowDto object
	 * @return Returns boolean ArrayList object
	 * @throws Exception If any error occurs during the process
	 */
	public boolean validateUser(Connection conn, WorkflowDto oWorkflowDto) throws Exception;
	
	/**
	 * User to create a new Revision for an Enquiry
	 * @param conn Connection object to connect to database
	 * @param  oEnquiryDto EnquiryDto object having enquiry details
	 * @param  oUserDto UserDto object having user details
	 * @return Returns boolean ArrayList object
	 * @throws Exception If any error occurs during the process
	 */
	public EnquiryDto createRevision(Connection conn, EnquiryDto oEnquiryDto, UserDto oUserDto)throws Exception;
	
	/**
	 * This method fetches the roles of the logged-in user for a particular enquiry
	 * @param conn Connection object to connect to database
	 * @param oEnquiryDto EnquiryDto object
	 * @param oUserDto UserDto object
	 * @param sUserType String contains the type
	 * @return Returns boolean
	 * @throws Exception If any error occurs during the process
	 */
	public boolean checkUserRole(Connection conn, EnquiryDto oEnquiryDto, UserDto userDto, String sUserType)throws Exception;
	
	/**
	 * This method will check if the workflow status of the enquiry for a user is pending or not
	 * @param conn Connection object to connect to database
	 * @param oWorkflowDto WorkflowDto object	
	 * @return Returns boolean
	 * @throws Exception If any error occurs during the process
	 */
	public boolean checkForWorkflowPendingStatus(Connection conn, WorkflowDto oWorkflowDto)throws Exception;
	
	/**
	 * This method will get the status of an enquiry
	 * @param conn Connection object to connect to database
	 * @param EnquiryDto oEnquiryDto object having enquiry details
	 * @return Returns EnquiryDto object having enquiry details
	 * @throws Exception If any error occurs during the process
	 */
	public EnquiryDto getEnquiryStatus(Connection conn, EnquiryDto oEnquiryDto)throws Exception;
	
	/**
	 * To fetch the list of values from ENQUIRY PICKLIST table for given lookup field
	 * @param conn Connection object to connect to database
	 * @param sLookupField string value of query parameter
	 * @return Returns ArrayList object
	 * @throws Exception If any error occurs during the process
	 */
	public ArrayList<KeyValueVo> getOptionsbyAttributeList(Connection conn,String sLookupField) throws Exception;
	
	/**
	 * This method will fetch the pick list values for offer page
	 * @param conn Connection object to connect to database
	 * @param oRatingDto RatingDto  object	
	 * @return Returns RatingDto Object
	 * @throws Exception If any error occurs during the process
	 */
	public RatingDto loadOfferPageLookUpValues(Connection conn, RatingDto oRatingDto) throws Exception;
	
	/**
	 * Update Intend Technical Data of a rating
	 * @param conn Connection object to connect to database
	 * @param EnquiryDto oEnquiryDto object having enquiry details
	 * @param oRatingDto RatingDto object having rating details
	 * @param oTechnicalOfferDto TechnicalOfferDto object having enquiry details
	 * @return Returns the boolean
	 * @throws Exception If any error occurs during the process
	 */
	public boolean updatingIntendTechnicalData(Connection conn, EnquiryDto oEnquiryDto, RatingDto oRatingDto, TechnicalOfferDto oTechnicalOfferDto)throws Exception;

	/**
	 * Fetch Delivery Details Based on Rating
	 * @param conn Connection object to connect to database
	 * @param ratingId int value of ratingId
	 * @return Returns ArrayList
	 * @throws Exception If any error occurs during the process
	 */
	public ArrayList getDeliveryDetailsList(Connection conn, int ratingId)throws Exception;
	/**
	 * Update Delivery details of a rating
	 * @param conn Connection object to connect to database
	 * @param oRatingDto RatingDto object having rating details
	 * @return Returns the boolean
	 * @throws Exception If any error occurs during the process
	 */
	public boolean updateDeliveryDetails(Connection conn, RatingDto oRatingDto)throws Exception;
	
	/**
	 * Delete all Delivery details for a rating
	 * @param conn Connection object to connect to database
	 * @param ratingId int value of ratingId
	 * @return Returns the boolean
	 * @throws Exception If any error occurs during the process
	 */
	public boolean deleteAllDelDetailsByRatingId(Connection conn, int iRatingId)throws Exception;
	
	/**
	 * Used to get the direct Commercial Managers list
	 * @param conn Connection object to connect to database
	 * @param sType String value of sType
	 * @param sCurrentUserId String value of current userid
	 * @return Returns the ArrayList object
	 * @throws Exception If any error occurs during the process
	 */
	public ArrayList getCommercialManagersList(Connection conn, String sType, String sCurrentUserId) throws Exception;

	
	/**
	 * Update Intend Technical Data of a rating
	 * @param conn Connection object to connect to database
	 * @param EnquiryDto oEnquiryDto object having enquiry details
	 * @return Returns the boolean
	 * @throws Exception If any error occurs during the process
	 */
	public boolean updatingLostIntendTechnicalData(Connection conn, EnquiryDto oEnquiryDto)throws Exception;
	
	/**
	 * This method is used to update the request information in released to Customer page
	 * @param conn Connection object to connect to database
	 * @param EnquiryDto oEnquiryDto object having enquiry details
	 * @return Returns the boolean true if update success
	 * @throws Exception If any error occurs during the process
	 */
	public boolean updateEnquiryBasicInfo(Connection conn, EnquiryDto oEnquiryDto) throws Exception;
	
	/**
	 * This method is used to save the calculated total motor, package price for Rating in indent page
	 * @param conn Connection object to connect to database
	 * @param RatingDto oRatingDto object having Rating details
	 * @return Returns the boolean true if update success
	 * @throws Exception If any error occurs during the process
	 */
	public boolean saveRatingMotorPkgPrice(Connection conn, RatingDto oRatingDto) throws Exception;
	
	/**
	 * This method is used to save the calculated total motor, package price and also include & extra for enquiry in indent page
	 * @param conn Connection object to connect to database
	 * @param EnquiryDto oEnquiryDto object having enquiry details
	 * @return Returns the boolean true if update success
	 * @throws Exception If any error occurs during the process
	 */
	public boolean saveEnquiryMotorPkgPrice(Connection conn, EnquiryDto oEnquiryDto) throws Exception;
	
	/**
	 * This method is used to save the calculated quoted price of Rating, for search results page
	 * @param conn Connection object to connect to database
	 * @param RatingDto oRatingDto object having Rating details
	 * @return Returns the boolean true if update success
	 * @throws Exception If any error occurs during the process
	 */
	public boolean saveRatingQuotedPrice(Connection conn, RatingDto oRatingDto) throws Exception;
	
	/**
	 * This method is used to save the calculated quoted price of Enquiry, for search results page
	 * @param conn Connection object to connect to database
	 * @param RatingDto oRatingDto object having Rating details
	 * @return Returns the boolean true if update success
	 * @throws Exception If any error occurs during the process
	 */
	public boolean saveEnquiryQuotedPrice(Connection conn, EnquiryDto oEnquiryDto) throws Exception ;

	
	
	/* ********************************************  QuotationLT : New Methods **************************************************** */
	
	/**
	 * Gets all the look up values for NewEnquiry
	 * @param conn Connection object to connect to database
	 * @param oNewEnquiryDto NewEnquiryDto object having enquiry details
	 * @return Returns the NewEnquiryDto object
	 * @throws Exception If any error occurs during the process
	 */
    public NewEnquiryDto loadNewEnquiryLookUpValues(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception;
	
	/**
	 * This Method is used to Fetch the Enquiry and Rating details based on an enquiryId
	 * @param conn
	 * @param oNewEnquiryDto
	 * @param sOperationType
	 * @return NewEnquiryDto
	 * @throws Exception
	 */
	public NewEnquiryDto getNewEnquiryDetailsForView(Connection conn, NewEnquiryDto oNewEnquiryDto, String sOperationType) throws Exception;
	
	/**
	 * Creates the enquiry with NewEnquiryDto details
	 * @param conn Connection object to connect to database
	 * @param oNewEnquiryDto NewEnquiryDto object having enquiry details
	 * @param sOperationType String object that defines type of operation
	 * @return Returns NewEnquiryDto object
	 * @throws Exception If any error occurs during the process
	 */
    public NewEnquiryDto createNewEnquiry(Connection conn, NewEnquiryDto oNewEnquiryDto, String sOperationType) throws Exception;
    
    /**
     * Updates the enquiry with NewEnquiryDto details
     * @param conn Connection object to connect to database
     * @param oNewEnquiryDto NewEnquiryDto object having enquiry details
     * @param sOperationType String object that defines type of operation
     * @return Returns NewEnquiryDto object
     * @throws Exception If any error occurs during the process
     */
    public NewEnquiryDto updateNewEnquiry(Connection conn, NewEnquiryDto oNewEnquiryDto, String sOperationType) throws Exception;
    
    /**
	 * Saves the Ratings details - New Rating then INSERT / Existing Rating then UPDATE 
	 * @param conn Connection object to connect to database
	 * @param oNewEnquiryDto NewEnquiryDto object having enquiry details
	 * @return Returns String Y/N TO indicate if all Ratings are Saved.
	 * @throws Exception If any error occurs during the process
	 */
    public String saveRatingDetails(Connection conn, NewRatingDto oNewRatingDto) throws Exception;
    
    /**
     * Saves the specific Rating Bean against the Enquiry ID - New Rating then INSERT / Existing Rating then UPDATE
     * @param conn Connection object to connect to database
     * @param oNewEnquiryDto NewEnquiryDto object having enquiry details
     * @return Returns String Y/N TO indicate if Rating is saved or not.
     * @throws Exception If any error occurs during the process
     */
    public String saveRatingByIdDetails(Connection conn, NewRatingDto oNewRatingDto) throws Exception;
    
    /**
	 * To fetch the list of values in Ascending Order from ENQUIRY PICKLIST table for given lookup field
	 * @param conn Connection object to connect to database
	 * @param sLookupField string value of query parameter
	 * @return Returns ArrayList object
	 * @throws Exception If any error occurs during the process
	 */
    public ArrayList<KeyValueVo> getAscendingOptionsbyAttributeList(Connection conn,String sLookupField) throws Exception;
    
    /**
	 * To fetch the Picklist Value based on ENQUIRY PICKLIST Attribute and Key
	 * @param conn Connection object to connect to database
	 * @param sLookupField string value of query parameter
	 * @return Returns ArrayList object
	 * @throws Exception If any error occurs during the process
	 */
	public String getOptionsbyAttributeKey(Connection conn, String sLookupField, String sLookupKey) throws Exception;
	
	/**
	 * Method To fetch the Enquiry Status
	 * @param conn
	 * @param oNewEnquiryDto
	 * @return
	 * @throws Exception
	 */
	public NewEnquiryDto getNewEnquiryStatus(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception;
	
	/**
	 * Method to remove Last Rating from New Enquiry - Based on RatingId
	 * @param conn
	 * @param oNewEnquiryDto
	 * @param lastRatingId
	 * @return
	 * @throws Exception
	 */
	public String removeLastRatingFromNewEnquiry(Connection conn, int iEnquiryId, int iRemoveRatingNo) throws Exception;
	
	/**
	 * Method to Fetch Rating Details Based on Enquiry ID
	 * @param conn
	 * @param oNewEnquiryDto
	 * @return
	 * @throws Exception
	 */
	public ArrayList<NewRatingDto> fetchNewRatingDetails(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception;
	
	/**
	 * Method to Fetch Sales Manager ID Based on SM SSO
	 * @param conn
	 * @param oNewEnquiryDto
	 * @return ArrayList<NewRatingDto> 
	 * @throws Exception
	 */
	public String getSalesManagerId(Connection conn, String smSSO) throws Exception;
	
	/**
	 * Method to Fetch RatingDto Based on Rating ID
	 * @param conn
	 * @param oNewRatingDto
	 * @return NewRatingDto
	 * @throws Exception
	 */
	public NewRatingDto fetchRatingByRatingId(Connection conn, NewRatingDto oNewRatingDto) throws Exception;
	
	public String savePrices_SubmitRSM(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception;
	
	public String savePrices_ApproveRSM(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception;
	
	public String savePrices_SubmitNSM(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception;
	
	public String savePrices_ApproveNSM(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception;
	
	public String savePrices_SubmitMH(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception;
	
	public String savePrices_ApproveMH(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception;
	
	public String savePrices_ReleaseSM(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception;
	
	/**
	 * Method to Create New Revision for Existing Enquiry
	 * @param conn
	 * @param oNewEnquiryDto
	 * @param oUserDto
	 * @return
	 * @throws Exception
	 */
	public NewEnquiryDto createNewEnquiryRevision(Connection conn, NewEnquiryDto oNewEnquiryDto, UserDto oUserDto) throws Exception;
	
	/**
	 * Method to Set Default values to NewRatingDto
	 * @param conn
	 * @param newRatingDto
	 * @return
	 * @throws Exception
	 */
	public NewRatingDto processNewRatingDefaults(Connection conn, NewRatingDto newRatingDto) throws Exception;
	
	/**
	 * This Method fetches the Logged-in User Approval Type
	 * @param conn Connection object to connect to database
	 * @param oEnquiryDto EnquiryDto object
	 * @param oUserDto UserDto object
	 * @param sUserType String contains the type
	 * @return Returns boolean
	 * @throws Exception If any error occurs during the process
	 */
	public String checkApprovalType(Connection conn, EnquiryDto oEnquiryDto, UserDto userDto) throws Exception;
	
	/**
	 * This Method is used to Save the Notes from Approval pages.
	 * @param conn
	 * @param oNewEnquiryDto
	 * @throws Exception
	 */
	public void saveNewEnquiryNotes(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception;
	
	/**
	 * This Method is used to Update Design Flag.
	 * @param conn
	 * @param sDesignFlag
	 * @throws Exception
	 */
	public void updateSubmitToDesignFlag(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception;
	
	/**
	 * This Method is used to Update specific Rating Details by Rating Id.
	 * @param conn
	 * @param oNewRatingDto
	 * @return
	 * @throws Exception
	 */
	public boolean updateMTORatingDetailsById(Connection conn, NewRatingDto oNewRatingDto) throws Exception;
	
	public boolean deleteAllNewRatingAddOns(Connection conn, int ratingId) throws Exception;
	
	public boolean updateNewRatingAddOns(Connection conn, NewRatingDto oNewRatingDto) throws Exception;
	
	public boolean updateMTORatingTechOfferDetails(Connection conn, NewTechnicalOfferDto oNewTechnicalOfferDto) throws Exception;
	
	public String deleteDraftEnquiryById(Connection conn, int iEnquiryId) throws Exception;
	
	/**
	 * This Method is used to fetch the Enquiry's Last Rating Number.
	 * @param conn Connection object to connect to database
	 * @param oNewEnquiryDto NewEnquiryDto object
	 * @return NewRatingDto which is added to the Enquiry
	 * @throws Exception If any error occurs during the process
	 */
	public Integer fetchLastRatingNumber(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception;
	
	/**
	 * This Method is used to fetch the Enquiry's Last Rating Details.
	 * @param conn Connection object to connect to database
	 * @param oNewEnquiryDto NewEnquiryDto object
	 * @return NewRatingDto which is added to the Enquiry
	 * @throws Exception If any error occurs during the process
	 */
	public NewRatingDto fetchLastRating(Connection conn, NewEnquiryDto oNewEnquiryDto, int iRatingNo) throws Exception;
	
	/**
	 * Method to Save Enquiry - ABONDENT Status Details.
	 * @param conn
	 * @param oNewEnquiryDto
	 * @return
	 * @throws Exception
	 */
	public String saveAbondentEnquiry(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception;

	/**
	 * Method to Save Enquiry - LOST Status Details.
	 * @param conn
	 * @param oNewEnquiryDto
	 * @param alRatingLostDetailsList
	 * @return
	 * @throws Exception
	 */
	public String saveLostEnquiryDetails(Connection conn, NewEnquiryDto oNewEnquiryDto, List<NewRatingDto> alRatingLostDetailsList) throws Exception;
	
	/**
	 * Method to Save Enquiry - WON Status Details.
	 * @param conn
	 * @param oNewEnquiryDto
	 * @param alRatingLostDetailsList
	 * @return
	 * @throws Exception
	 */
	public String saveWonEnquiryDetails(Connection conn, NewEnquiryDto oNewEnquiryDto, List<NewRatingDto> alRatingWonDetailsList) throws Exception;
	
	/**
	 * Method to Fetch Re-assign To User based on Re-assign To Id.
	 * @param conn
	 * @param sReassignToId
	 * @return
	 * @throws Exception
	 */
	public UserDetailsDto getUserDetailsById(Connection conn, String sReassignToId) throws Exception;
	
	/**
	 * Method to save the Price Values for MTO Rating.
	 * @param conn
	 * @param oNewRatingDto
	 * @throws Exception
	 */
	public void updateNewRatingMTOPriceDetails(Connection conn, NewRatingDto oNewRatingDto) throws Exception;
	
	/**
	 * Method to Save the MTO Parameters for MTO Rating.
	 * @param conn
	 * @param oNewMTORatingDto
	 * @throws Exception
	 */
	public void updateNewMTORatingDetails(Connection conn, NewMTORatingDto oNewMTORatingDto) throws Exception;
		
	/**
	 * Method to Fetch NewRating with values Based on EnquiryId and RatingNo.
	 * @param conn
	 * @param oNewRatingDto
	 * @throws Exception
	 */
	public NewRatingDto fetchNewRatingByIdandNo(Connection conn, NewRatingDto oNewRatingDto) throws Exception;
	
	/**
	 * Gets Design Manager Id - Manager Master Data.
	 * @param conn Connection object to connect to database
	 * @param oEnquiryDto EnquiryDto object having enquiry details
	 * @return Returns the String object
	 * @throws Exception If any error occurs during the process
	 */
	public String getDesignManagerId(Connection conn) throws Exception;
	
	/**
	 * Method to Fetch NewTechnicalOfferDto - Based on ProductLine params, Voltage and Frequency.
	 * @param conn
	 * @param oNewRatingDto
	 * @return
	 * @throws Exception
	 */
	public NewTechnicalOfferDto buildNewTechnicalOfferDto(Connection conn, NewRatingDto oNewRatingDto) throws Exception;
	
	/**
	 * This method is used to Update the request information in NewEnquiry Released to Customer page.
	 * @param conn Connection object to connect to database
	 * @param EnquiryDto oEnquiryDto object having enquiry details
	 * @return Returns the boolean true if update success
	 * @throws Exception If any error occurs during the process
	 */
	public boolean updateNewEnquiryBasicInfo(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception;

	/**
	 * Method to Process L.D
	 * @param conn
	 * @param oNewEnquiryDto
	 * @param sOperationType
	 * @return
	 * @throws Exception
	 */
	public String processLDApproval(Connection conn, NewEnquiryDto oNewEnquiryDto, String sOperationType) throws Exception;
	
	/**
	 * Method to Fetch Re-assign Users List for Sales Team.
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public List<KeyValueVo> fetchSalesReassignUsersList(Connection conn) throws Exception;
	
	/**
	 * Method to Update New Enquiry with Mfg. Plant Details.
	 * @param conn
	 * @param oNewEnquiryDto
	 * @return
	 * @throws Exception
	 */
	public String updateMfgPlantDetails(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception;
	
	/**
	 * Method to Save Rating Delivery Details - Mfg Plant.
	 * @param conn
	 * @param oNewRatingDto
	 * @return
	 * @throws Exception
	 */
	public String saveMfgPlantRatingDetails(Connection conn, NewRatingDto oNewRatingDto) throws Exception;
	
	/**
	 * Method to Make a Copy of NewEnquiry values.
	 * @param conn
	 * @param oNewEnquiryDto
	 * @param oUserDto
	 * @return
	 * @throws Exception
	 */
	public NewEnquiryDto copyNewEnquiry(Connection conn, NewEnquiryDto oNewEnquiryDto, UserDto oUserDto) throws Exception;
	
	public ArrayList<NewEnquiryDto> fetchNoMailSentEnquiries(Connection conn) throws Exception;
	
	public ArrayList<NewEnquiryDto> fetchMailSentCurrentAssigneeEnquiries(Connection conn) throws Exception;
	
	public ArrayList<NewEnquiryDto> fetchMailNotSentCurrentAssigneeEnquiries (Connection conn) throws Exception;
	
	public void processNotifierMailSentUpdates(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception;
	
	public void updateDmStatus(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception;

	public String updateMTOMaterialCost(Connection conn, NewRatingDto oNewRatingDto, NewMTORatingDto oNewMTORatingDto) throws Exception;

	public String checkEnquiryMCApplied(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception;

	public boolean checkIsNewRatingDesignComplete(Connection conn, NewRatingDto oNewRatingDto) throws Exception;
	
}
