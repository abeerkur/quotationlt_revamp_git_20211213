/**
 * ******************************************************************************************
 * Project Name: Request For Quotation
 * Document Name: TechnicalOfferDto.java
 * Package: in.com.rbc.quotation.enquiry.dto
 * Desc: DTO class that holds getters and setters of each property.
 * ******************************************************************************************
 * @author 100006126
 * ******************************************************************************************
 */

package in.com.rbc.quotation.enquiry.dto;

import in.com.rbc.quotation.common.utility.CommonUtility;

public class TechnicalOfferDto {
	
	private int ratingId;
	private String frameSize;
	private String efficiency;
	private String pf;	//QRD_TO_PF
	private String flcAmps; //QRD_TO_FLCAMPS
	private String potFLT; //QRD_TO_POTFLT
	private String flcSQCAGE; //QRD_TO_FLCSQCAGE
	private String noiseLevel; //QRD_TO_NOISELEVEL
	private String vibrationLevel;	//QRD_TO_VIBRATIONLEVEL
	private String fltSQCAGE;	//QRD_TO_FLTSQCAGE
	private String rpm;	//QRD_TO_RPM
	private String rtGD2; //QRD_TO_RTGD2
	private String sstSQCAGE; //QRD_TO_SSTSQCAGE
	private String rv; //QRD_TO_RV
	private String ra; //QRD_TO_RV
	private String remarks; //QRD_TO_REMARKS
	private String isDataValidated; //QRD_TO_ISVALIDATED

	
	private String commercialOfferType; //QRD_CO_OFFERTYPE
	private long commercialMCUnit; //QRD_CO_MCPERUNIT
	private long commercialCEUnitPrice; //QRD_CO_UNITPRICE_CE
	private long commercialRSMUnitPrice; //QRD_CO_UNITPRICE_RSM
	
	
	private long commercialMCUnitTotalPrice;
	private long commercialCETotalPrice;
	private long commercialRSMTotalPrice;
	
	private String formattedCommercialMCUnitTotalPrice;
	private String formattedCommercialCETotalPrice;
	private String formattedCommercialRSMTotalPrice;
	
	private String formattedCommercialMCUnit; 
	private String formattedCommercialCEUnitPrice; 
	private String formattedCommercialRSMUnitPrice;
	
	
	private String areaClassification;
	private String serviceFactor;
	private String bkw;
	private String nlCurrent;
	private String strtgCurrent;
	private String lrTorque;
	private String pllEff1;
	private String pllEff2;
	private String pllEff3;
	private String sstHot;
	private String sstCold;
	private String startTimeRV;
	private String startTimeRV80;
	private String coolingSystem;
	private String bearingDENDE;
	private String lubrication;
	private String migD2Load;
	private String migD2Motor;
	private String balancing;
	private String mainTermBoxPS;
	private String neutralTermBox;
	private String cableEntry;
	private String auxTermBox;
	private String statorConn;
	private String noBoTerminals;
	private String rotation;
	private String motorTotalWgt;
	private String spaceHeater;
	private String windingRTD;
	private String bearingRTD;
	private String minStartVolt;
	private String bearingThermoDT;
	private String airThermo;
	private String vibeProbMPads;
	private String encoder;
	private String speedSwitch;
	private String surgeSuppressors;
	private String currTransformers;
	private String vibProbes;
	private String spmNipple;
	private String keyPhasor;
	private String pllCurr1;
	private String pllCurr2;
	private String pllCurr3;
	private String pllCurr4;
	private String pllPf1;
	private String pllPf2;
	private String pllpf3;
	private String pllpf4;
	private String paintColor;
	private String createdBy;
	private String createdByName;
	private String pllCurr5;
	private String pllEff4;
	private String pllPf5;
	private String updateCreateBy;
	
	private int unitPriceRSM;
	
	private String transferCMultiplier;
	private String quantityMultiplier;
	
	private String mcornspratioMultiplier_rsm;
	private String transportationperunit_rsm;
	private String warrantyperunit_rsm;
	private String unitpriceMultiplier_rsm;
	private String totalpriceMultiplier_rsm;

	private int termBoxId;
	private String termBoxName;
	
	private String deviation_clarification;
	private String zone;
	private String gasGroup;
	//New Variable Added By Gangadhar on 14-Feb-2019
	private String priceincludeFrieght;
	private String priceincludeExtraWarn;
	private String priceincludeSupervisionCost;
	private String priceincludetypeTestCharges;
	private String priceextraFrieght;
	private String priceextraExtraWarn;
	private String priceextraSupervisionCost;
	private String priceextratypeTestCharges;
	private String priceincludeFrieghtVal;
	private String priceincludeExtraWarnVal;
	private String priceincludeSupervisionCostVal;
	private String priceincludetypeTestChargesVal;
	private String priceextraFrieghtVal;
	private String priceextraExtraWarnVal;
	private String priceextraSupervisionCostVal;
	private String priceextratypeTestChargesVal;



	
	
	public int getUnitPriceRSM() {
		return unitPriceRSM;
	}
	public void setUnitPriceRSM(int unitPriceRSM) {
		this.unitPriceRSM = unitPriceRSM;
	}
	/**
	 * @return Returns the updateCreateBy.
	 */
	public String getUpdateCreateBy() {
		return CommonUtility.replaceNull(updateCreateBy);
	}
	/**
	 * @param updateCreateBy The updateCreateBy to set.
	 */
	public void setUpdateCreateBy(String updateCreateBy) {
		this.updateCreateBy = updateCreateBy;
	}
	/**
	 * @return Returns the createdBy.
	 */
	public String getCreatedBy() {
		return  CommonUtility.replaceNull(createdBy);
	}
	/**
	 * @param createdBy The createdBy to set.
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return Returns the createdByName.
	 */
	public String getCreatedByName() {
		return  CommonUtility.replaceNull(createdByName);
	}
	/**
	 * @param createdByName The createdByName to set.
	 */
	public void setCreatedByName(String createdByName) {
		this.createdByName = createdByName;
	}
	/**
	 * @return Returns the efficiency.
	 */
	public String getEfficiency() {
		return CommonUtility.replaceNull(efficiency);
	}
	/**
	 * @param efficiency The efficiency to set.
	 */
	public void setEfficiency(String efficiency) {
		this.efficiency = efficiency;
	}
	/**
	 * @return Returns the flcAmps.
	 */
	public String getFlcAmps() {
		return CommonUtility.replaceNull(flcAmps);
	}
	/**
	 * @param flcAmps The flcAmps to set.
	 */
	public void setFlcAmps(String flcAmps) {
		this.flcAmps = flcAmps;
	}
	/**
	 * @return Returns the flcSQCAGE.
	 */
	public String getFlcSQCAGE() {
		return CommonUtility.replaceNull(flcSQCAGE);
	}
	/**
	 * @param flcSQCAGE The flcSQCAGE to set.
	 */
	public void setFlcSQCAGE(String flcSQCAGE) {
		this.flcSQCAGE = flcSQCAGE;
	}
	/**
	 * @return Returns the fltSQCAGE.
	 */
	public String getFltSQCAGE() {
		return CommonUtility.replaceNull(fltSQCAGE);
	}
	/**
	 * @param fltSQCAGE The fltSQCAGE to set.
	 */
	public void setFltSQCAGE(String fltSQCAGE) {
		this.fltSQCAGE = fltSQCAGE;
	}
	/**
	 * @return Returns the frameSize.
	 */
	public String getFrameSize() {
		return CommonUtility.replaceNull(frameSize);
	}
	/**
	 * @param frameSize The frameSize to set.
	 */
	public void setFrameSize(String frameSize) {
		this.frameSize = frameSize;
	}
	/**
	 * @return Returns the noiseLevel.
	 */
	public String getNoiseLevel() {
		return CommonUtility.replaceNull(noiseLevel);
	}
	/**
	 * @param noiseLevel The noiseLevel to set.
	 */
	public void setNoiseLevel(String noiseLevel) {
		this.noiseLevel = noiseLevel;
	}
	/**
	 * @return Returns the pf.
	 */
	public String getPf() {
		return CommonUtility.replaceNull(pf);
	}
	/**
	 * @param pf The pf to set.
	 */
	public void setPf(String pf) {
		this.pf = pf;
	}
	/**
	 * @return Returns the potFLT.
	 */
	public String getPotFLT() {
		return CommonUtility.replaceNull(potFLT);
	}
	/**
	 * @param potFLT The potFLT to set.
	 */
	public void setPotFLT(String potFLT) {
		this.potFLT = potFLT;
	}
	/**
	 * @return Returns the ra.
	 */
	public String getRa() {
		return CommonUtility.replaceNull(ra);
	}
	/**
	 * @param ra The ra to set.
	 */
	public void setRa(String ra) {
		this.ra = ra;
	}
	/**
	 * @return Returns the remarks.
	 */
	public String getRemarks() {
		return CommonUtility.replaceNull(remarks);
	}
	/**
	 * @param remarks The remarks to set.
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	/**
	 * @return Returns the rpm.
	 */
	public String getRpm() {
		return CommonUtility.replaceNull(rpm);
	}
	/**
	 * @param rpm The rpm to set.
	 */
	public void setRpm(String rpm) {
		this.rpm = rpm;
	}
	/**
	 * @return Returns the rtGD2.
	 */
	public String getRtGD2() {
		return CommonUtility.replaceNull(rtGD2);
	}
	/**
	 * @param rtGD2 The rtGD2 to set.
	 */
	public void setRtGD2(String rtGD2) {
		this.rtGD2 = rtGD2;
	}
	/**
	 * @return Returns the rv.
	 */
	public String getRv() {
		return CommonUtility.replaceNull(rv);
	}
	/**
	 * @param rv The rv to set.
	 */
	public void setRv(String rv) {
		this.rv = rv;
	}
	/**
	 * @return Returns the sstSQCAGE.
	 */
	public String getSstSQCAGE() {
		return CommonUtility.replaceNull(sstSQCAGE);
	}
	/**
	 * @param sstSQCAGE The sstSQCAGE to set.
	 */
	public void setSstSQCAGE(String sstSQCAGE) {
		this.sstSQCAGE = sstSQCAGE;
	}
	/**
	 * @return Returns the vibrationLevel.
	 */
	public String getVibrationLevel() {
		return CommonUtility.replaceNull(vibrationLevel);
	}
	/**
	 * @param vibrationLevel The vibrationLevel to set.
	 */
	public void setVibrationLevel(String vibrationLevel) {
		this.vibrationLevel = vibrationLevel;
	}
	/**
	 * @return Returns the ratingId.
	 */
	public int getRatingId() {
		return ratingId;
	}
	/**
	 * @param ratingId The ratingId to set.
	 */
	public void setRatingId(int ratingId) {
		this.ratingId = ratingId;
	}
	/**
	 * @return Returns the commercialMCUnit.
	 */
	public long getCommercialMCUnit() {
		return commercialMCUnit;
	}
	/**
	 * @param commercialMCUnit The commercialMCUnit to set.
	 */
	public void setCommercialMCUnit(long commercialMCUnit) {
		this.commercialMCUnit = commercialMCUnit;
	}
	/**
	 * @return Returns the commercialOfferType.
	 */
	public String getCommercialOfferType() {
		return CommonUtility.replaceNull(commercialOfferType);
	}
	/**
	 * @param commercialOfferType The commercialOfferType to set.
	 */
	public void setCommercialOfferType(String commercialOfferType) {
		this.commercialOfferType = commercialOfferType;
	}
	/**
	 * @return Returns the isDataValidated.
	 */
	public String getIsDataValidated() {
		return CommonUtility.replaceNull(isDataValidated);
	}
	/**
	 * @param isDataValidated The isDataValidated to set.
	 */
	public void setIsDataValidated(String isDataValidated) {
		this.isDataValidated = isDataValidated;
	}
	/**
	 * @return Returns the commercialCEUnitPrice.
	 */
	public long getCommercialCEUnitPrice() {
		return commercialCEUnitPrice;
	}
	/**
	 * @param commercialCEUnitPrice The commercialCEUnitPrice to set.
	 */
	public void setCommercialCEUnitPrice(long commercialCEUnitPrice) {
		this.commercialCEUnitPrice = commercialCEUnitPrice;
	}
	/**
	 * @return Returns the commercialRSMUnitPrice.
	 */
	public long getCommercialRSMUnitPrice() {
		return commercialRSMUnitPrice;
	}
	/**
	 * @param commercialRSMUnitPrice The commercialRSMUnitPrice to set.
	 */
	public void setCommercialRSMUnitPrice(long commercialRSMUnitPrice) {
		this.commercialRSMUnitPrice = commercialRSMUnitPrice;
	}
	/**
	 * @return Returns the commercialCETotalPrice.
	 */
	public long getCommercialCETotalPrice() {
		return commercialCETotalPrice;
	}
	/**
	 * @param commercialCETotalPrice The commercialCETotalPrice to set.
	 */
	public void setCommercialCETotalPrice(long commercialCETotalPrice) {
		this.commercialCETotalPrice = commercialCETotalPrice;
	}
	/**
	 * @return Returns the commercialMCUnitTotalPrice.
	 */
	public long getCommercialMCUnitTotalPrice() {
		return commercialMCUnitTotalPrice;
	}
	/**
	 * @param commercialMCUnitTotalPrice The commercialMCUnitTotalPrice to set.
	 */
	public void setCommercialMCUnitTotalPrice(long commercialMCUnitTotalPrice) {
		this.commercialMCUnitTotalPrice = commercialMCUnitTotalPrice;
	}
	/**
	 * @return Returns the commercialRSMTotalPrice.
	 */
	public long getCommercialRSMTotalPrice() {
		return commercialRSMTotalPrice;
	}
	/**
	 * @param commercialRSMTotalPrice The commercialRSMTotalPrice to set.
	 */
	public void setCommercialRSMTotalPrice(long commercialRSMTotalPrice) {
		this.commercialRSMTotalPrice = commercialRSMTotalPrice;
	}
	/**
	 * @return Returns the formattedCommercialCETotalPrice.
	 */
	public String getFormattedCommercialCETotalPrice() {
		return formattedCommercialCETotalPrice;
	}
	/**
	 * @param formattedCommercialCETotalPrice The formattedCommercialCETotalPrice to set.
	 */
	public void setFormattedCommercialCETotalPrice(
			String formattedCommercialCETotalPrice) {
		this.formattedCommercialCETotalPrice = formattedCommercialCETotalPrice;
	}
	/**
	 * @return Returns the formattedCommercialMCUnitTotalPrice.
	 */
	public String getFormattedCommercialMCUnitTotalPrice() {
		return formattedCommercialMCUnitTotalPrice;
	}
	/**
	 * @param formattedCommercialMCUnitTotalPrice The formattedCommercialMCUnitTotalPrice to set.
	 */
	public void setFormattedCommercialMCUnitTotalPrice(
			String formattedCommercialMCUnitTotalPrice) {
		this.formattedCommercialMCUnitTotalPrice = formattedCommercialMCUnitTotalPrice;
	}
	/**
	 * @return Returns the formattedCommercialRSMTotalPrice.
	 */
	public String getFormattedCommercialRSMTotalPrice() {
		return formattedCommercialRSMTotalPrice;
	}
	/**
	 * @param formattedCommercialRSMTotalPrice The formattedCommercialRSMTotalPrice to set.
	 */
	public void setFormattedCommercialRSMTotalPrice(
			String formattedCommercialRSMTotalPrice) {
		this.formattedCommercialRSMTotalPrice = formattedCommercialRSMTotalPrice;
	}
	/**
	 * @return Returns the formattedCommercialCEUnitPrice.
	 */
	public String getFormattedCommercialCEUnitPrice() {
		return formattedCommercialCEUnitPrice;
	}
	/**
	 * @param formattedCommercialCEUnitPrice The formattedCommercialCEUnitPrice to set.
	 */
	public void setFormattedCommercialCEUnitPrice(
			String formattedCommercialCEUnitPrice) {
		this.formattedCommercialCEUnitPrice = formattedCommercialCEUnitPrice;
	}
	/**
	 * @return Returns the formattedCommercialMCUnit.
	 */
	public String getFormattedCommercialMCUnit() {
		return formattedCommercialMCUnit;
	}
	/**
	 * @param formattedCommercialMCUnit The formattedCommercialMCUnit to set.
	 */
	public void setFormattedCommercialMCUnit(String formattedCommercialMCUnit) {
		this.formattedCommercialMCUnit = formattedCommercialMCUnit;
	}
	/**
	 * @return Returns the formattedCommercialRSMUnitPrice.
	 */
	public String getFormattedCommercialRSMUnitPrice() {
		return formattedCommercialRSMUnitPrice;
	}
	/**
	 * @param formattedCommercialRSMUnitPrice The formattedCommercialRSMUnitPrice to set.
	 */
	public void setFormattedCommercialRSMUnitPrice(
			String formattedCommercialRSMUnitPrice) {
		this.formattedCommercialRSMUnitPrice = formattedCommercialRSMUnitPrice;
	}
	
	public String getAreaClassification() {
		return CommonUtility.replaceNull( areaClassification);
	}
	public void setAreaClassification(String areaClassification) {
		this.areaClassification = areaClassification;
	}
	public String getServiceFactor() {
		return CommonUtility.replaceNull( serviceFactor);
	}
	public void setServiceFactor(String serviceFactor) {
		this.serviceFactor = serviceFactor;
	}
	public String getBkw() {
		return CommonUtility.replaceNull( bkw);
	}
	public void setBkw(String bkw) {
		this.bkw = bkw;
	}
	public String getNlCurrent() {
		return CommonUtility.replaceNull( nlCurrent);
	}
	public void setNlCurrent(String nlCurrent) {
		this.nlCurrent = nlCurrent;
	}
	public String getStrtgCurrent() {
		return CommonUtility.replaceNull( strtgCurrent);
	}
	public void setStrtgCurrent(String strtgCurrent) {
		this.strtgCurrent = strtgCurrent;
	}
	public String getLrTorque() {
		return CommonUtility.replaceNull( lrTorque);
	}
	public void setLrTorque(String lrTorque) {
		this.lrTorque = lrTorque;
	}
	public String getPllEff1() {
		return CommonUtility.replaceNull( pllEff1);
	}
	public void setPllEff1(String pllEff1) {
		this.pllEff1 = pllEff1;
	}
	public String getPllEff2() {
		return CommonUtility.replaceNull( pllEff2);
	}
	public void setPllEff2(String pllEff2) {
		this.pllEff2 = pllEff2;
	}
	public String getPllEff3() {
		return CommonUtility.replaceNull( pllEff3);
	}
	public void setPllEff3(String pllEff3) {
		this.pllEff3 = pllEff3;
	}
	public String getSstHot() {
		return CommonUtility.replaceNull( sstHot);
	}
	public void setSstHot(String sstHot) {
		this.sstHot = sstHot;
	}
	public String getSstCold() {
		return CommonUtility.replaceNull( sstCold);
	}
	public void setSstCold(String sstCold) {
		this.sstCold = sstCold;
	}
	public String getStartTimeRV() {
		return CommonUtility.replaceNull( startTimeRV);
	}
	public void setStartTimeRV(String startTimeRV) {
		this.startTimeRV = startTimeRV;
	}
	public String getStartTimeRV80() {
		return CommonUtility.replaceNull( startTimeRV80);
	}
	public void setStartTimeRV80(String startTimeRV80) {
		this.startTimeRV80 = startTimeRV80;
	}
	public String getCoolingSystem() {
		return CommonUtility.replaceNull( coolingSystem);
	}
	public void setCoolingSystem(String coolingSystem) {
		this.coolingSystem = coolingSystem;
	}
	public String getBearingDENDE() {
		return CommonUtility.replaceNull( bearingDENDE);
	}
	public void setBearingDENDE(String bearingDENDE) {
		this.bearingDENDE = bearingDENDE;
	}
	public String getLubrication() {
		return CommonUtility.replaceNull( lubrication);
	}
	public void setLubrication(String lubrication) {
		this.lubrication = lubrication;
	}
	public String getMigD2Load() {
		return CommonUtility.replaceNull( migD2Load);
	}
	public void setMigD2Load(String migD2Load) {
		this.migD2Load = migD2Load;
	}
	public String getMainTermBoxPS() {
		return CommonUtility.replaceNull( mainTermBoxPS);
	}
	public void setMainTermBoxPS(String mainTermBoxPS) {
		this.mainTermBoxPS = mainTermBoxPS;
	}
	public String getNeutralTermBox() {
		return CommonUtility.replaceNull( neutralTermBox);
	}
	public void setNeutralTermBox(String neutralTermBox) {
		this.neutralTermBox = neutralTermBox;
	}
	public String getCableEntry() {
		return CommonUtility.replaceNull( cableEntry);
	}
	public void setCableEntry(String cableEntry) {
		this.cableEntry = cableEntry;
	}
	public String getAuxTermBox() {
		return CommonUtility.replaceNull( auxTermBox);
	}
	public void setAuxTermBox(String auxTermBox) {
		this.auxTermBox = auxTermBox;
	}
	public String getStatorConn() {
		return CommonUtility.replaceNull( statorConn);
	}
	public void setStatorConn(String statorConn) {
		this.statorConn = statorConn;
	}
	public String getNoBoTerminals() {
		return CommonUtility.replaceNull( noBoTerminals);
	}
	public void setNoBoTerminals(String noBoTerminals) {
		this.noBoTerminals = noBoTerminals;
	}
	public String getRotation() {
		return CommonUtility.replaceNull( rotation);
	}
	public void setRotation(String rotation) {
		this.rotation = rotation;
	}
	public String getMotorTotalWgt() {
		return CommonUtility.replaceNull( motorTotalWgt);
	}
	public void setMotorTotalWgt(String motorTotalWgt) {
		this.motorTotalWgt = motorTotalWgt;
	}
	public String getSpaceHeater() {
		return CommonUtility.replaceNull( spaceHeater);
	}
	public void setSpaceHeater(String spaceHeater) {
		this.spaceHeater = spaceHeater;
	}
	public String getWindingRTD() {
		return CommonUtility.replaceNull( windingRTD);
	}
	public void setWindingRTD(String windingRTD) {
		this.windingRTD = windingRTD;
	}
	public String getBearingRTD() {
		return CommonUtility.replaceNull( bearingRTD);
	}
	public void setBearingRTD(String bearingRTD) {
		this.bearingRTD = bearingRTD;
	}
	public String getMinStartVolt() {
		return CommonUtility.replaceNull( minStartVolt);
	}
	public void setMinStartVolt(String minStartVolt) {
		this.minStartVolt = minStartVolt;
	}
	public String getBearingThermoDT() {
		return CommonUtility.replaceNull( bearingThermoDT);
	}
	public void setBearingThermoDT(String bearingThermoDT) {
		this.bearingThermoDT = bearingThermoDT;
	}
	public String getAirThermo() {
		return CommonUtility.replaceNull( airThermo);
	}
	public void setAirThermo(String airThermo) {
		this.airThermo = airThermo;
	}
	public String getVibeProbMPads() {
		return CommonUtility.replaceNull( vibeProbMPads);
	}
	public void setVibeProbMPads(String vibeProbMPads) {
		this.vibeProbMPads = vibeProbMPads;
	}
	public String getEncoder() {
		return CommonUtility.replaceNull( encoder);
	}
	public void setEncoder(String encoder) {
		this.encoder = encoder;
	}
	public String getSpeedSwitch() {
		return CommonUtility.replaceNull( speedSwitch);
	}
	public void setSpeedSwitch(String speedSwitch) {
		this.speedSwitch = speedSwitch;
	}
	public String getSurgeSuppressors() {
		return CommonUtility.replaceNull( surgeSuppressors);
	}
	public void setSurgeSuppressors(String surgeSuppressors) {
		this.surgeSuppressors = surgeSuppressors;
	}
	public String getCurrTransformers() {
		return CommonUtility.replaceNull( currTransformers);
	}
	public void setCurrTransformers(String currTransformers) {
		this.currTransformers = currTransformers;
	}
	public String getVibProbes() {
		return CommonUtility.replaceNull( vibProbes);
	}
	public void setVibProbes(String vibProbes) {
		this.vibProbes = vibProbes;
	}
	public String getSpmNipple() {
		return CommonUtility.replaceNull( spmNipple);
	}
	public void setSpmNipple(String spmNipple) {
		this.spmNipple = spmNipple;
	}
	public String getKeyPhasor() {
		return CommonUtility.replaceNull( keyPhasor);
	}
	public void setKeyPhasor(String keyPhasor) {
		this.keyPhasor = keyPhasor;
	}	
	public String getMigD2Motor() {
		return CommonUtility.replaceNull(migD2Motor);
	}
	public void setMigD2Motor(String migD2Motor) {
		this.migD2Motor = migD2Motor;
	}
	public String getBalancing() {
		return CommonUtility.replaceNull(balancing);
	}
	public void setBalancing(String balancing) {
		this.balancing = balancing;
	}	
	public String getPllCurr1() {
		return CommonUtility.replaceNull(pllCurr1);
	}
	public void setPllCurr1(String pllCurr1) {
		this.pllCurr1 = pllCurr1;
	}
	public String getPllCurr2() {
		return CommonUtility.replaceNull(pllCurr2);
	}
	public void setPllCurr2(String pllCurr2) {
		this.pllCurr2 = pllCurr2;
	}
	public String getPllCurr3() {
		return CommonUtility.replaceNull(pllCurr3);
	}
	public void setPllCurr3(String pllCurr3) {
		this.pllCurr3 = pllCurr3;
	}
	public String getPllCurr4() {
		return CommonUtility.replaceNull(pllCurr4);
	}
	public void setPllCurr4(String pllCurr4) {
		this.pllCurr4 = pllCurr4;
	}
	public String getPllPf1() {
		return CommonUtility.replaceNull(pllPf1);
	}
	public void setPllPf1(String pllPf1) {
		this.pllPf1 = pllPf1;
	}
	public String getPllPf2() {
		return CommonUtility.replaceNull(pllPf2);
	}
	public void setPllPf2(String pllPf2) {
		this.pllPf2 = pllPf2;
	}
	public String getPllpf3() {
		return CommonUtility.replaceNull(pllpf3);
	}
	public void setPllpf3(String pllpf3) {
		this.pllpf3 = pllpf3;
	}
	public String getPllpf4() {
		return CommonUtility.replaceNull(pllpf4);
	}
	public void setPllpf4(String pllpf4) {
		this.pllpf4 = pllpf4;
	}
	public String getPaintColor() {
		return CommonUtility.replaceNull(paintColor);
	}
	public void setPaintColor(String paintColor) {
		this.paintColor = paintColor;
	}
	public String getTransferCMultiplier() {
		return CommonUtility.replaceNull(transferCMultiplier);
	}
	public void setTransferCMultiplier(String transferCMultiplier) {
		this.transferCMultiplier = transferCMultiplier;
	}
	public String getQuantityMultiplier() {
		return CommonUtility.replaceNull(quantityMultiplier);
	}
	public void setQuantityMultiplier(String quantityMultiplier) {
		this.quantityMultiplier = quantityMultiplier;
	}
	public String getMcornspratioMultiplier_rsm() {
		return CommonUtility.replaceNull(mcornspratioMultiplier_rsm);
	}
	public void setMcornspratioMultiplier_rsm(String mcornspratioMultiplier_rsm) {
		this.mcornspratioMultiplier_rsm = mcornspratioMultiplier_rsm;
	}
	public String getTransportationperunit_rsm() {
		return CommonUtility.replaceNull(transportationperunit_rsm);
	}
	public void setTransportationperunit_rsm(String transportationperunit_rsm) {
		this.transportationperunit_rsm = transportationperunit_rsm;
	}
	public String getWarrantyperunit_rsm() {
		return CommonUtility.replaceNull(warrantyperunit_rsm);
	}
	public void setWarrantyperunit_rsm(String warrantyperunit_rsm) {
		this.warrantyperunit_rsm = warrantyperunit_rsm;
	}
	public String getUnitpriceMultiplier_rsm() {
		return CommonUtility.replaceNull(unitpriceMultiplier_rsm);
	}
	public void setUnitpriceMultiplier_rsm(String unitpriceMultiplier_rsm) {
		this.unitpriceMultiplier_rsm = unitpriceMultiplier_rsm;
	}
	public String getTotalpriceMultiplier_rsm() {
		return CommonUtility.replaceNull(totalpriceMultiplier_rsm);
	}
	public void setTotalpriceMultiplier_rsm(String totalpriceMultiplier_rsm) {
		this.totalpriceMultiplier_rsm = totalpriceMultiplier_rsm;
	}
	public int getTermBoxId() {
		return termBoxId;
	}
	public void setTermBoxId(int termBoxId) {
		this.termBoxId = termBoxId;
	}
	public String getTermBoxName() {
		return CommonUtility.replaceNull(termBoxName);
	}
	public void setTermBoxName(String termBoxName) {
		this.termBoxName = termBoxName;
	}
	public String getDeviation_clarification() {
		return CommonUtility.replaceNull(deviation_clarification);
	}
	public void setDeviation_clarification(String deviation_clarification) {
		this.deviation_clarification = deviation_clarification;
	}
	public String getZone() {
		return CommonUtility.replaceNull(zone);
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	public String getGasGroup() {
		return CommonUtility.replaceNull(gasGroup);
	}
	public void setGasGroup(String gasGroup) {
		this.gasGroup = gasGroup;
	}
	public String getPriceincludeFrieght() {
		return CommonUtility.replaceNull(priceincludeFrieght);
	}
	public void setPriceincludeFrieght(String priceincludeFrieght) {
		this.priceincludeFrieght = priceincludeFrieght;
	}
	public String getPriceincludeExtraWarn() {
		return CommonUtility.replaceNull(priceincludeExtraWarn);
	}
	public void setPriceincludeExtraWarn(String priceincludeExtraWarn) {
		this.priceincludeExtraWarn = priceincludeExtraWarn;
	}
	public String getPriceincludeSupervisionCost() {
		return CommonUtility.replaceNull(priceincludeSupervisionCost);
	}
	public void setPriceincludeSupervisionCost(String priceincludeSupervisionCost) {
		this.priceincludeSupervisionCost = priceincludeSupervisionCost;
	}
	public String getPriceincludetypeTestCharges() {
		return CommonUtility.replaceNull(priceincludetypeTestCharges);
	}
	public void setPriceincludetypeTestCharges(String priceincludetypeTestCharges) {
		this.priceincludetypeTestCharges = priceincludetypeTestCharges;
	}
	public String getPriceextraFrieght() {
		return CommonUtility.replaceNull(priceextraFrieght);
	}
	public void setPriceextraFrieght(String priceextraFrieght) {
		this.priceextraFrieght = priceextraFrieght;
	}
	public String getPriceextraExtraWarn() {
		return CommonUtility.replaceNull(priceextraExtraWarn);
	}
	public void setPriceextraExtraWarn(String priceextraExtraWarn) {
		this.priceextraExtraWarn = priceextraExtraWarn;
	}
	public String getPriceextraSupervisionCost() {
		return CommonUtility.replaceNull(priceextraSupervisionCost);
	}
	public void setPriceextraSupervisionCost(String priceextraSupervisionCost) {
		this.priceextraSupervisionCost = priceextraSupervisionCost;
	}
	public String getPriceextratypeTestCharges() {
		return CommonUtility.replaceNull(priceextratypeTestCharges);
	}
	public void setPriceextratypeTestCharges(String priceextratypeTestCharges) {
		this.priceextratypeTestCharges = priceextratypeTestCharges;
	}
	public String getPriceincludeFrieghtVal() {
		return CommonUtility.replaceNull(priceincludeFrieghtVal);
	}
	public void setPriceincludeFrieghtVal(String priceincludeFrieghtVal) {
		this.priceincludeFrieghtVal = priceincludeFrieghtVal;
	}
	public String getPriceincludeExtraWarnVal() {
		return CommonUtility.replaceNull(priceincludeExtraWarnVal);
	}
	public void setPriceincludeExtraWarnVal(String priceincludeExtraWarnVal) {
		this.priceincludeExtraWarnVal = priceincludeExtraWarnVal;
	}
	public String getPriceincludeSupervisionCostVal() {
		return CommonUtility.replaceNull(priceincludeSupervisionCostVal);
	}
	public void setPriceincludeSupervisionCostVal(
			String priceincludeSupervisionCostVal) {
		this.priceincludeSupervisionCostVal = priceincludeSupervisionCostVal;
	}
	public String getPriceincludetypeTestChargesVal() {
		return CommonUtility.replaceNull(priceincludetypeTestChargesVal);
	}
	public void setPriceincludetypeTestChargesVal(
			String priceincludetypeTestChargesVal) {
		this.priceincludetypeTestChargesVal = priceincludetypeTestChargesVal;
	}
	public String getPriceextraFrieghtVal() {
		return CommonUtility.replaceNull(priceextraFrieghtVal);
	}
	public void setPriceextraFrieghtVal(String priceextraFrieghtVal) {
		this.priceextraFrieghtVal = priceextraFrieghtVal;
	}
	public String getPriceextraExtraWarnVal() {
		return CommonUtility.replaceNull(priceextraExtraWarnVal);
	}
	public void setPriceextraExtraWarnVal(String priceextraExtraWarnVal) {
		this.priceextraExtraWarnVal = priceextraExtraWarnVal;
	}
	public String getPriceextraSupervisionCostVal() {
		return CommonUtility.replaceNull(priceextraSupervisionCostVal);
	}
	public void setPriceextraSupervisionCostVal(String priceextraSupervisionCostVal) {
		this.priceextraSupervisionCostVal = priceextraSupervisionCostVal;
	}
	public String getPriceextratypeTestChargesVal() {
		return CommonUtility.replaceNull(priceextratypeTestChargesVal);
	}
	public void setPriceextratypeTestChargesVal(String priceextratypeTestChargesVal) {
		this.priceextratypeTestChargesVal = priceextratypeTestChargesVal;
	}
	public String getPllCurr5() {
		return CommonUtility.replaceNull(pllCurr5);
	}
	public void setPllCurr5(String pllCurr5) {
		this.pllCurr5 = pllCurr5;
	}
	public String getPllEff4() {
		return CommonUtility.replaceNull(pllEff4);
	}
	public void setPllEff4(String pllEff4) {
		this.pllEff4 = pllEff4;
	}
	public String getPllPf5() {
		return CommonUtility.replaceNull(pllPf5);
	}
	public void setPllPf5(String pllPf5) {
		this.pllPf5 = pllPf5;
	}
	
	
	
}
