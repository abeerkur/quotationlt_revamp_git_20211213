/**
 * ******************************************************************************************
 * Project Name: Request For Quotation
 * Document Name: TeamMemberDto.java
 * Package: in.com.rbc.quotation.enquiry.dto
 * Desc: DTO class that holds getters and setters of each property.
 * ******************************************************************************************
 * @author 100006126
 * ******************************************************************************************
 */
package in.com.rbc.quotation.enquiry.dto;

import in.com.rbc.quotation.common.utility.CommonUtility;

import java.util.ArrayList;

public class TeamMemberDto {
	
	/*
	 * RFQ_TEAMMEMBERS_V
	 */
	
	private int enquiryId; //AST_ENQUIRYID
	private String designManager; //AST_DESIGNMANAGER
	private String designEngineer; //AST_DESIGNENGINEER
	private String primaryRSM; //AST_PRIMARYRSM
	private String additionalRSM; //AST_ADDITIONALRSM
	private String chiefExecutive; //AST_CHIEFEXECUTIVE
	private String salesManager; //AST_SALESMANAGER
	private String commercialManager; //AST_COMMERCIALMANAGER
	private String commercialEngineer; //AST_DESIGNENGINEER

	

	private String membertype; // this variable will be used as a filter
	private ArrayList teamMemberList = new ArrayList();
	private String userType;
	
	
	/**
	 * @return Returns the userType.
	 */
	public String getUserType() {
		return CommonUtility.replaceNull(userType);
	}
	/**
	 * @param userType The userType to set.
	 */
	public void setUserType(String userType) {
		this.userType = userType;
	}
	/**
	 * @return Returns the membertype.
	 */
	public String getMembertype() {
		return CommonUtility.replaceNull(membertype);
	}
	/**
	 * @param membertype The membertype to set.
	 */
	public void setMembertype(String membertype) {
		this.membertype = membertype;
	}
	/**
	 * @return Returns the teamMemberList.
	 */
	public ArrayList getTeamMemberList() {
		return teamMemberList;
	}
	/**
	 * @param teamMemberList The teamMemberList to set.
	 */
	public void setTeamMemberList(ArrayList teamMemberList) {
		this.teamMemberList = teamMemberList;
	}
	/**
	 * @return Returns the designEngineer.
	 */
	public String getDesignEngineer() {
		return CommonUtility.replaceNull(designEngineer);
	}
	/**
	 * @param designEngineer The designEngineer to set.
	 */
	public void setDesignEngineer(String designEngineer) {
		this.designEngineer = designEngineer;
	}
	/**
	 * @return Returns the designManager.
	 */
	public String getDesignManager() {
		return CommonUtility.replaceNull(designManager);
	}
	/**
	 * @param designManager The designManager to set.
	 */
	public void setDesignManager(String designManager) {
		this.designManager = designManager;
	}
	/**
	 * @return Returns the enquiryId.
	 */
	public int getEnquiryId() {
		return enquiryId;
	}
	/**
	 * @param enquiryId The enquiryId to set.
	 */
	public void setEnquiryId(int enquiryId) {
		this.enquiryId = enquiryId;
	}
	/**
	 * @return Returns the additionalRSM.
	 */
	public String getAdditionalRSM() {
		return additionalRSM;
	}
	/**
	 * @param additionalRSM The additionalRSM to set.
	 */
	public void setAdditionalRSM(String additionalRSM) {
		this.additionalRSM = additionalRSM;
	}
	/**
	 * @return Returns the chiefExecutive.
	 */
	public String getChiefExecutive() {
		return CommonUtility.replaceNull(chiefExecutive);
	}
	/**
	 * @param chiefExecutive The chiefExecutive to set.
	 */
	public void setChiefExecutive(String chiefExecutive) {
		this.chiefExecutive = chiefExecutive;
	}
	/**
	 * @return Returns the primaryRSM.
	 */
	public String getPrimaryRSM() {
		return CommonUtility.replaceNull(primaryRSM);
	}
	/**
	 * @param primaryRSM The primaryRSM to set.
	 */
	public void setPrimaryRSM(String primaryRSM) {
		this.primaryRSM = primaryRSM;
	}
	/**
	 * @return Returns the salesManager.
	 */
	public String getSalesManager() {
		return CommonUtility.replaceNull(salesManager);
	}
	/**
	 * @param salesManager The salesManager to set.
	 */
	public void setSalesManager(String salesManager) {
		this.salesManager = salesManager;
	}
	public String getCommercialManager() {
		return CommonUtility.replaceNull(commercialManager);
	}
	public void setCommercialManager(String commercialManager) {
		this.commercialManager = commercialManager;
	}
	public String getCommercialEngineer() {
		return CommonUtility.replaceNull(commercialEngineer);
	}
	public void setCommercialEngineer(String commercialEngineer) {
		this.commercialEngineer = commercialEngineer;
	}
}
