package in.com.rbc.quotation.enquiry.dto;

public class RemarksDto {

	private String tagNumber;
	private String quantity;
	private String application;
	private String earlierSuppliedMotor;
	private String standardDelivery;
	private String customerRequestedDelivery;
	private String nonStdVoltage;
	private String windingTreatment;
	private String windingWire;
	private String lead;
	private String startingCurrentDOL;
	private String windingConfig;
	private String loadGD2;
	private String vfdAppType;
	private String dualSpeed;
	private String overloadingDuty;
	private String constEffRange;
	private String shaftMaterial;
	private String paintingType;
	private String paintingThickness;
	private String terminalBoxSize;
	private String spreaderBox;
	private String flyingLeadWithoutTB;
	private String flyingLeadWithTB;
	private String metalFan;
	
	
	
	
	public String getTagNumber() {
		return tagNumber;
	}
	public void setTagNumber(String tagNumber) {
		this.tagNumber = tagNumber;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getApplication() {
		return application;
	}
	public void setApplication(String application) {
		this.application = application;
	}
	public String getEarlierSuppliedMotor() {
		return earlierSuppliedMotor;
	}
	public void setEarlierSuppliedMotor(String earlierSuppliedMotor) {
		this.earlierSuppliedMotor = earlierSuppliedMotor;
	}
	public String getStandardDelivery() {
		return standardDelivery;
	}
	public void setStandardDelivery(String standardDelivery) {
		this.standardDelivery = standardDelivery;
	}
	public String getCustomerRequestedDelivery() {
		return customerRequestedDelivery;
	}
	public void setCustomerRequestedDelivery(String customerRequestedDelivery) {
		this.customerRequestedDelivery = customerRequestedDelivery;
	}
	public String getNonStdVoltage() {
		return nonStdVoltage;
	}
	public void setNonStdVoltage(String nonStdVoltage) {
		this.nonStdVoltage = nonStdVoltage;
	}
	public String getWindingTreatment() {
		return windingTreatment;
	}
	public void setWindingTreatment(String windingTreatment) {
		this.windingTreatment = windingTreatment;
	}
	public String getWindingWire() {
		return windingWire;
	}
	public void setWindingWire(String windingWire) {
		this.windingWire = windingWire;
	}
	public String getLead() {
		return lead;
	}
	public void setLead(String lead) {
		this.lead = lead;
	}
	public String getStartingCurrentDOL() {
		return startingCurrentDOL;
	}
	public void setStartingCurrentDOL(String startingCurrentDOL) {
		this.startingCurrentDOL = startingCurrentDOL;
	}
	public String getWindingConfig() {
		return windingConfig;
	}
	public void setWindingConfig(String windingConfig) {
		this.windingConfig = windingConfig;
	}
	public String getLoadGD2() {
		return loadGD2;
	}
	public void setLoadGD2(String loadGD2) {
		this.loadGD2 = loadGD2;
	}
	public String getVfdAppType() {
		return vfdAppType;
	}
	public void setVfdAppType(String vfdAppType) {
		this.vfdAppType = vfdAppType;
	}
	public String getDualSpeed() {
		return dualSpeed;
	}
	public void setDualSpeed(String dualSpeed) {
		this.dualSpeed = dualSpeed;
	}
	public String getOverloadingDuty() {
		return overloadingDuty;
	}
	public void setOverloadingDuty(String overloadingDuty) {
		this.overloadingDuty = overloadingDuty;
	}
	public String getConstEffRange() {
		return constEffRange;
	}
	public void setConstEffRange(String constEffRange) {
		this.constEffRange = constEffRange;
	}
	public String getShaftMaterial() {
		return shaftMaterial;
	}
	public void setShaftMaterial(String shaftMaterial) {
		this.shaftMaterial = shaftMaterial;
	}
	public String getPaintingType() {
		return paintingType;
	}
	public void setPaintingType(String paintingType) {
		this.paintingType = paintingType;
	}
	public String getPaintingThickness() {
		return paintingThickness;
	}
	public void setPaintingThickness(String paintingThickness) {
		this.paintingThickness = paintingThickness;
	}
	public String getTerminalBoxSize() {
		return terminalBoxSize;
	}
	public void setTerminalBoxSize(String terminalBoxSize) {
		this.terminalBoxSize = terminalBoxSize;
	}
	public String getSpreaderBox() {
		return spreaderBox;
	}
	public void setSpreaderBox(String spreaderBox) {
		this.spreaderBox = spreaderBox;
	}
	public String getFlyingLeadWithoutTB() {
		return flyingLeadWithoutTB;
	}
	public void setFlyingLeadWithoutTB(String flyingLeadWithoutTB) {
		this.flyingLeadWithoutTB = flyingLeadWithoutTB;
	}
	public String getFlyingLeadWithTB() {
		return flyingLeadWithTB;
	}
	public void setFlyingLeadWithTB(String flyingLeadWithTB) {
		this.flyingLeadWithTB = flyingLeadWithTB;
	}
	public String getMetalFan() {
		return metalFan;
	}
	public void setMetalFan(String metalFan) {
		this.metalFan = metalFan;
	}
	
	
}
