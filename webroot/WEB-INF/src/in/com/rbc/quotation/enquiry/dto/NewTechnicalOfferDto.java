package in.com.rbc.quotation.enquiry.dto;

import in.com.rbc.quotation.common.utility.CommonUtility;

public class NewTechnicalOfferDto {

	private int ratingId;
	
	private int enquiryId;
	private int ratingNo;
	private int quantity;
	private String isReferDesign;
	
	private String productGroup;
	private String productLine;
	private String powerRating_KW;
	private String frameType;
	private String frameSuffix;
	private int noOfPoles;
	private String mountingType;
	private String tbPosition;
	private String mfgLocation;
	
	private String modelNumber;
	private String voltage;
	private String voltageAddVariation;
	private String voltageRemoveVariation;
	private String frequency;
	private String frequencyAddVariation;
	private String frequencyRemoveVariation;
	private String combinedVariation;
	private String coolingMethod;
	private String motorInertia;
	private String vibration;
	private String noise;
	private String noOfStarts;
	private String lrWithstandTime;
	private String directionOfRotation;
	private String powerRating_HP;
	private String cableSize;
	private String current_A;
	private String maxSpeed;
	private String powerFactor_100;
	private String powerFactor_75;
	private String powerFactor_50;
	private String efficiencyClass;
	private String efficiency_100Above;
	private String efficiency_100;
	private String efficiency_75;
	private String efficiency_50;
	private String startingCurrent;
	private String torque;
	private String starting_torque;
	private String pullout_torque;
	private String raValue;
	private String rvValue;
	private String hazArea;
	private String hazLocation;
	private String zoneClassification;
	private String gasGroup;
	private String temperatureClass;
	private String rotortype;
	private String electricalType;
	private String ipCode;
	private String insulationClass;
	private String serviceFactor;
	private String duty;
	private String cdf;
	private String noOfStarts_HR;
	private String noOfPhases;
	private String maxAmbientTemp;
	private String ambientAddTemp;
	private String ambientRemoveTemp;
	private String tempRise;
	private String altitude;
	private String deBearingSize;
	private String deBearingType;
	private String odeBearingSize;
	private String odeBearingType;
	private String enclosureType;
	private String lubMethod;
	private String typeOfGrease;
	private String motorOrientation;
	private String frameMaterial;
	private String frameLength;
	private String frameSize;
	private String rotation;
	private String noOfSpeeds;
	private String shaftType;
	private String shaftDiameter;
	private String shaftExtension;
	private String outlineDrawingNum;
	private String statorConnection;
	private String rotorConnection;
	private String connDrawingNum;
	private String overallLengthMM;
	private String motorWeight;
	private String grossWeight;
	private String startingType;
	private String thruBoltsExtension;
	private String overloadProtectionType;
	private String ceValue;
	private String ulValue;
	private String csaValue;
	private String design;
	private String loadInertia;
	private String methodOfCoupling;
	private String auxTerminalBox;
	
	private String createdBy;
	private String createdByName;
	
	// Additional Fields.
	private String noLoadCurrent;
	private String bkw;
	private String currentA_100;
	private String currentA_75;
	private String currentA_50;
	private String currentA_Start;
	private String currentA_DP;
	private String efficiency_DP;
	private String powerFactor_Start;
	private String powerFactor_DP;
	private String sstHot;
	private String sstCold;
	private String mainTBoxPS;
	private String startTimeRatedVolt;
	private String startTimeRatedVolt80;
	private String cableEntry;
	private String noBOTerminals;
	private String degOfProtection;
	private String minStartingVolt;
	private String mainTermBox;
	private String bearingThermo_DT;
	private String airThermo;
	private String vibProbeMPads;
	private String encoder;
	private String spmNipple;
	private String keyPhasor;
	private String speedSwitch;
	private String surgeSuppressors;
	private String currentTransformers;
	private String vibProbes;
	
	
	public int getRatingId() {
		return ratingId;
	}
	public void setRatingId(int ratingId) {
		this.ratingId = ratingId;
	}
	public int getEnquiryId() {
		return enquiryId;
	}
	public void setEnquiryId(int enquiryId) {
		this.enquiryId = enquiryId;
	}
	public int getRatingNo() {
		return ratingNo;
	}
	public void setRatingNo(int ratingNo) {
		this.ratingNo = ratingNo;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getIsReferDesign() {
		return CommonUtility.replaceNull(isReferDesign);
	}
	public void setIsReferDesign(String isReferDesign) {
		this.isReferDesign = isReferDesign;
	}
	public String getProductGroup() {
		return CommonUtility.replaceNull(productGroup);
	}
	public void setProductGroup(String productGroup) {
		this.productGroup = productGroup;
	}
	public String getProductLine() {
		return CommonUtility.replaceNull(productLine);
	}
	public void setProductLine(String productLine) {
		this.productLine = productLine;
	}
	public String getPowerRating_KW() {
		return CommonUtility.replaceNull(powerRating_KW);
	}
	public void setPowerRating_KW(String powerRating_KW) {
		this.powerRating_KW = powerRating_KW;
	}
	public String getFrameType() {
		return CommonUtility.replaceNull(frameType);
	}
	public void setFrameType(String frameType) {
		this.frameType = frameType;
	}
	public String getFrameSuffix() {
		return CommonUtility.replaceNull(frameSuffix);
	}
	public void setFrameSuffix(String frameSuffix) {
		this.frameSuffix = frameSuffix;
	}
	public String getFrameSize() {
		return CommonUtility.replaceNull(frameSize);
	}
	public void setFrameSize(String frameSize) {
		this.frameSize = frameSize;
	}
	public int getNoOfPoles() {
		return noOfPoles;
	}
	public void setNoOfPoles(int noOfPoles) {
		this.noOfPoles = noOfPoles;
	}
	public String getMountingType() {
		return CommonUtility.replaceNull(mountingType);
	}
	public void setMountingType(String mountingType) {
		this.mountingType = mountingType;
	}
	public String getTbPosition() {
		return CommonUtility.replaceNull(tbPosition);
	}
	public void setTbPosition(String tbPosition) {
		this.tbPosition = tbPosition;
	}
	public String getMfgLocation() {
		return CommonUtility.replaceNull(mfgLocation);
	}
	public void setMfgLocation(String mfgLocation) {
		this.mfgLocation = mfgLocation;
	}
	public String getModelNumber() {
		return CommonUtility.replaceNull(modelNumber);
	}
	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}
	public String getVoltage() {
		return CommonUtility.replaceNull(voltage);
	}
	public void setVoltage(String voltage) {
		this.voltage = voltage;
	}
	public String getVoltageAddVariation() {
		return CommonUtility.replaceNull(voltageAddVariation);
	}
	public void setVoltageAddVariation(String voltageAddVariation) {
		this.voltageAddVariation = voltageAddVariation;
	}
	public String getVoltageRemoveVariation() {
		return CommonUtility.replaceNull(voltageRemoveVariation);
	}
	public void setVoltageRemoveVariation(String voltageRemoveVariation) {
		this.voltageRemoveVariation = voltageRemoveVariation;
	}
	public String getFrequency() {
		return CommonUtility.replaceNull(frequency);
	}
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	public String getFrequencyAddVariation() {
		return CommonUtility.replaceNull(frequencyAddVariation);
	}
	public void setFrequencyAddVariation(String frequencyAddVariation) {
		this.frequencyAddVariation = frequencyAddVariation;
	}
	public String getFrequencyRemoveVariation() {
		return CommonUtility.replaceNull(frequencyRemoveVariation);
	}
	public void setFrequencyRemoveVariation(String frequencyRemoveVariation) {
		this.frequencyRemoveVariation = frequencyRemoveVariation;
	}
	public String getCombinedVariation() {
		return CommonUtility.replaceNull(combinedVariation);
	}
	public void setCombinedVariation(String combinedVariation) {
		this.combinedVariation = combinedVariation;
	}
	public String getCoolingMethod() {
		return CommonUtility.replaceNull(coolingMethod);
	}
	public void setCoolingMethod(String coolingMethod) {
		this.coolingMethod = coolingMethod;
	}
	public String getMotorInertia() {
		return CommonUtility.replaceNull(motorInertia);
	}
	public void setMotorInertia(String motorInertia) {
		this.motorInertia = motorInertia;
	}
	public String getVibration() {
		return CommonUtility.replaceNull(vibration);
	}
	public void setVibration(String vibration) {
		this.vibration = vibration;
	}
	public String getNoise() {
		return CommonUtility.replaceNull(noise);
	}
	public void setNoise(String noise) {
		this.noise = noise;
	}
	public String getNoOfStarts() {
		return CommonUtility.replaceNull(noOfStarts);
	}
	public void setNoOfStarts(String noOfStarts) {
		this.noOfStarts = noOfStarts;
	}
	public String getLrWithstandTime() {
		return CommonUtility.replaceNull(lrWithstandTime);
	}
	public void setLrWithstandTime(String lrWithstandTime) {
		this.lrWithstandTime = lrWithstandTime;
	}
	public String getDirectionOfRotation() {
		return CommonUtility.replaceNull(directionOfRotation);
	}
	public void setDirectionOfRotation(String directionOfRotation) {
		this.directionOfRotation = directionOfRotation;
	}
	public String getPowerRating_HP() {
		return CommonUtility.replaceNull(powerRating_HP);
	}
	public void setPowerRating_HP(String powerRating_HP) {
		this.powerRating_HP = powerRating_HP;
	}
	public String getCableSize() {
		return CommonUtility.replaceNull(cableSize);
	}
	public void setCableSize(String cableSize) {
		this.cableSize = cableSize;
	}
	public String getCurrent_A() {
		return CommonUtility.replaceNull(current_A);
	}
	public void setCurrent_A(String current_A) {
		this.current_A = current_A;
	}
	public String getMaxSpeed() {
		return CommonUtility.replaceNull(maxSpeed);
	}
	public void setMaxSpeed(String maxSpeed) {
		this.maxSpeed = maxSpeed;
	}
	public String getPowerFactor_100() {
		return CommonUtility.replaceNull(powerFactor_100);
	}
	public void setPowerFactor_100(String powerFactor_100) {
		this.powerFactor_100 = powerFactor_100;
	}
	public String getPowerFactor_75() {
		return CommonUtility.replaceNull(powerFactor_75);
	}
	public void setPowerFactor_75(String powerFactor_75) {
		this.powerFactor_75 = powerFactor_75;
	}
	public String getPowerFactor_50() {
		return CommonUtility.replaceNull(powerFactor_50);
	}
	public void setPowerFactor_50(String powerFactor_50) {
		this.powerFactor_50 = powerFactor_50;
	}
	public String getEfficiencyClass() {
		return CommonUtility.replaceNull(efficiencyClass);
	}
	public void setEfficiencyClass(String efficiencyClass) {
		this.efficiencyClass = efficiencyClass;
	}
	public String getEfficiency_100Above() {
		return CommonUtility.replaceNull(efficiency_100Above);
	}
	public void setEfficiency_100Above(String efficiency_100Above) {
		this.efficiency_100Above = efficiency_100Above;
	}
	public String getEfficiency_100() {
		return CommonUtility.replaceNull(efficiency_100);
	}
	public void setEfficiency_100(String efficiency_100) {
		this.efficiency_100 = efficiency_100;
	}
	public String getEfficiency_75() {
		return CommonUtility.replaceNull(efficiency_75);
	}
	public void setEfficiency_75(String efficiency_75) {
		this.efficiency_75 = efficiency_75;
	}
	public String getEfficiency_50() {
		return CommonUtility.replaceNull(efficiency_50);
	}
	public void setEfficiency_50(String efficiency_50) {
		this.efficiency_50 = efficiency_50;
	}
	public String getStartingCurrent() {
		return CommonUtility.replaceNull(startingCurrent);
	}
	public void setStartingCurrent(String startingCurrent) {
		this.startingCurrent = startingCurrent;
	}
	public String getTorque() {
		return CommonUtility.replaceNull(torque);
	}
	public void setTorque(String torque) {
		this.torque = torque;
	}
	public String getStarting_torque() {
		return CommonUtility.replaceNull(starting_torque);
	}
	public void setStarting_torque(String starting_torque) {
		this.starting_torque = starting_torque;
	}
	public String getPullout_torque() {
		return CommonUtility.replaceNull(pullout_torque);
	}
	public void setPullout_torque(String pullout_torque) {
		this.pullout_torque = pullout_torque;
	}
	public String getRaValue() {
		return CommonUtility.replaceNull(raValue);
	}
	public void setRaValue(String raValue) {
		this.raValue = raValue;
	}
	public String getRvValue() {
		return CommonUtility.replaceNull(rvValue);
	}
	public void setRvValue(String rvValue) {
		this.rvValue = rvValue;
	}
	public String getHazArea() {
		return CommonUtility.replaceNull(hazArea);
	}
	public void setHazArea(String hazArea) {
		this.hazArea = hazArea;
	}
	public String getHazLocation() {
		return CommonUtility.replaceNull(hazLocation);
	}
	public void setHazLocation(String hazLocation) {
		this.hazLocation = hazLocation;
	}
	public String getZoneClassification() {
		return CommonUtility.replaceNull(zoneClassification);
	}
	public void setZoneClassification(String zoneClassification) {
		this.zoneClassification = zoneClassification;
	}
	public String getGasGroup() {
		return CommonUtility.replaceNull(gasGroup);
	}
	public void setGasGroup(String gasGroup) {
		this.gasGroup = gasGroup;
	}
	public String getTemperatureClass() {
		return CommonUtility.replaceNull(temperatureClass);
	}
	public void setTemperatureClass(String temperatureClass) {
		this.temperatureClass = temperatureClass;
	}
	public String getRotortype() {
		return CommonUtility.replaceNull(rotortype);
	}
	public void setRotortype(String rotortype) {
		this.rotortype = rotortype;
	}
	public String getElectricalType() {
		return CommonUtility.replaceNull(electricalType);
	}
	public void setElectricalType(String electricalType) {
		this.electricalType = electricalType;
	}
	public String getIpCode() {
		return CommonUtility.replaceNull(ipCode);
	}
	public void setIpCode(String ipCode) {
		this.ipCode = ipCode;
	}
	public String getInsulationClass() {
		return CommonUtility.replaceNull(insulationClass);
	}
	public void setInsulationClass(String insulationClass) {
		this.insulationClass = insulationClass;
	}
	public String getServiceFactor() {
		return CommonUtility.replaceNull(serviceFactor);
	}
	public void setServiceFactor(String serviceFactor) {
		this.serviceFactor = serviceFactor;
	}
	public String getDuty() {
		return CommonUtility.replaceNull(duty);
	}
	public void setDuty(String duty) {
		this.duty = duty;
	}
	public String getCdf() {
		return CommonUtility.replaceNull(cdf);
	}
	public void setCdf(String cdf) {
		this.cdf = cdf;
	}
	public String getNoOfStarts_HR() {
		return CommonUtility.replaceNull(noOfStarts_HR);
	}
	public void setNoOfStarts_HR(String noOfStarts_HR) {
		this.noOfStarts_HR = noOfStarts_HR;
	}
	public String getNoOfPhases() {
		return CommonUtility.replaceNull(noOfPhases);
	}
	public void setNoOfPhases(String noOfPhases) {
		this.noOfPhases = noOfPhases;
	}
	public String getMaxAmbientTemp() {
		return CommonUtility.replaceNull(maxAmbientTemp);
	}
	public void setMaxAmbientTemp(String maxAmbientTemp) {
		this.maxAmbientTemp = maxAmbientTemp;
	}
	public String getAmbientAddTemp() {
		return CommonUtility.replaceNull(ambientAddTemp);
	}
	public void setAmbientAddTemp(String ambientAddTemp) {
		this.ambientAddTemp = ambientAddTemp;
	}
	public String getAmbientRemoveTemp() {
		return CommonUtility.replaceNull(ambientRemoveTemp);
	}
	public void setAmbientRemoveTemp(String ambientRemoveTemp) {
		this.ambientRemoveTemp = ambientRemoveTemp;
	}
	public String getTempRise() {
		return CommonUtility.replaceNull(tempRise);
	}
	public void setTempRise(String tempRise) {
		this.tempRise = tempRise;
	}
	public String getAltitude() {
		return CommonUtility.replaceNull(altitude);
	}
	public void setAltitude(String altitude) {
		this.altitude = altitude;
	}
	public String getDeBearingSize() {
		return CommonUtility.replaceNull(deBearingSize);
	}
	public void setDeBearingSize(String deBearingSize) {
		this.deBearingSize = deBearingSize;
	}
	public String getDeBearingType() {
		return CommonUtility.replaceNull(deBearingType);
	}
	public void setDeBearingType(String deBearingType) {
		this.deBearingType = deBearingType;
	}
	public String getOdeBearingSize() {
		return CommonUtility.replaceNull(odeBearingSize);
	}
	public void setOdeBearingSize(String odeBearingSize) {
		this.odeBearingSize = odeBearingSize;
	}
	public String getOdeBearingType() {
		return CommonUtility.replaceNull(odeBearingType);
	}
	public void setOdeBearingType(String odeBearingType) {
		this.odeBearingType = odeBearingType;
	}
	public String getEnclosureType() {
		return CommonUtility.replaceNull(enclosureType);
	}
	public void setEnclosureType(String enclosureType) {
		this.enclosureType = enclosureType;
	}
	public String getLubMethod() {
		return CommonUtility.replaceNull(lubMethod);
	}
	public void setLubMethod(String lubMethod) {
		this.lubMethod = lubMethod;
	}
	public String getTypeOfGrease() {
		return CommonUtility.replaceNull(typeOfGrease);
	}
	public void setTypeOfGrease(String typeOfGrease) {
		this.typeOfGrease = typeOfGrease;
	}
	public String getMotorOrientation() {
		return CommonUtility.replaceNull(motorOrientation);
	}
	public void setMotorOrientation(String motorOrientation) {
		this.motorOrientation = motorOrientation;
	}
	public String getFrameMaterial() {
		return CommonUtility.replaceNull(frameMaterial);
	}
	public void setFrameMaterial(String frameMaterial) {
		this.frameMaterial = frameMaterial;
	}
	public String getFrameLength() {
		return CommonUtility.replaceNull(frameLength);
	}
	public void setFrameLength(String frameLength) {
		this.frameLength = frameLength;
	}
	public String getRotation() {
		return CommonUtility.replaceNull(rotation);
	}
	public void setRotation(String rotation) {
		this.rotation = rotation;
	}
	public String getNoOfSpeeds() {
		return CommonUtility.replaceNull(noOfSpeeds);
	}
	public void setNoOfSpeeds(String noOfSpeeds) {
		this.noOfSpeeds = noOfSpeeds;
	}
	public String getShaftType() {
		return CommonUtility.replaceNull(shaftType);
	}
	public void setShaftType(String shaftType) {
		this.shaftType = shaftType;
	}
	public String getShaftDiameter() {
		return CommonUtility.replaceNull(shaftDiameter);
	}
	public void setShaftDiameter(String shaftDiameter) {
		this.shaftDiameter = shaftDiameter;
	}
	public String getShaftExtension() {
		return CommonUtility.replaceNull(shaftExtension);
	}
	public void setShaftExtension(String shaftExtension) {
		this.shaftExtension = shaftExtension;
	}
	public String getOutlineDrawingNum() {
		return CommonUtility.replaceNull(outlineDrawingNum);
	}
	public void setOutlineDrawingNum(String outlineDrawingNum) {
		this.outlineDrawingNum = outlineDrawingNum;
	}
	public String getStatorConnection() {
		return CommonUtility.replaceNull(statorConnection);
	}
	public void setStatorConnection(String statorConnection) {
		this.statorConnection = statorConnection;
	}
	public String getRotorConnection() {
		return CommonUtility.replaceNull(rotorConnection);
	}
	public void setRotorConnection(String rotorConnection) {
		this.rotorConnection = rotorConnection;
	}
	public String getConnDrawingNum() {
		return CommonUtility.replaceNull(connDrawingNum);
	}
	public void setConnDrawingNum(String connDrawingNum) {
		this.connDrawingNum = connDrawingNum;
	}
	public String getOverallLengthMM() {
		return CommonUtility.replaceNull(overallLengthMM);
	}
	public void setOverallLengthMM(String overallLengthMM) {
		this.overallLengthMM = overallLengthMM;
	}
	public String getMotorWeight() {
		return CommonUtility.replaceNull(motorWeight);
	}
	public void setMotorWeight(String motorWeight) {
		this.motorWeight = motorWeight;
	}
	public String getGrossWeight() {
		return CommonUtility.replaceNull(grossWeight);
	}
	public void setGrossWeight(String grossWeight) {
		this.grossWeight = grossWeight;
	}
	public String getStartingType() {
		return CommonUtility.replaceNull(startingType);
	}
	public void setStartingType(String startingType) {
		this.startingType = startingType;
	}
	public String getThruBoltsExtension() {
		return CommonUtility.replaceNull(thruBoltsExtension);
	}
	public void setThruBoltsExtension(String thruBoltsExtension) {
		this.thruBoltsExtension = thruBoltsExtension;
	}
	public String getOverloadProtectionType() {
		return CommonUtility.replaceNull(overloadProtectionType);
	}
	public void setOverloadProtectionType(String overloadProtectionType) {
		this.overloadProtectionType = overloadProtectionType;
	}
	public String getCeValue() {
		return CommonUtility.replaceNull(ceValue);
	}
	public void setCeValue(String ceValue) {
		this.ceValue = ceValue;
	}
	public String getUlValue() {
		return CommonUtility.replaceNull(ulValue);
	}
	public void setUlValue(String ulValue) {
		this.ulValue = ulValue;
	}
	public String getCsaValue() {
		return CommonUtility.replaceNull(csaValue);
	}
	public void setCsaValue(String csaValue) {
		this.csaValue = csaValue;
	}
	public String getCreatedBy() {
		return CommonUtility.replaceNull(createdBy);
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getCreatedByName() {
		return CommonUtility.replaceNull(createdByName);
	}
	public void setCreatedByName(String createdByName) {
		this.createdByName = createdByName;
	}
	public String getDesign() {
		return CommonUtility.replaceNull(design);
	}
	public void setDesign(String design) {
		this.design = design;
	}
	public String getLoadInertia() {
		return CommonUtility.replaceNull(loadInertia);
	}
	public void setLoadInertia(String loadInertia) {
		this.loadInertia = loadInertia;
	}
	
	
	// Additional Fields.
	
	public String getNoLoadCurrent() {
		return CommonUtility.replaceNull(noLoadCurrent);
	}
	public void setNoLoadCurrent(String noLoadCurrent) {
		this.noLoadCurrent = noLoadCurrent;
	}
	public String getBkw() {
		return CommonUtility.replaceNull(bkw);
	}
	public void setBkw(String bkw) {
		this.bkw = bkw;
	}
	public String getCurrentA_100() {
		return CommonUtility.replaceNull(currentA_100);
	}
	public void setCurrentA_100(String currentA_100) {
		this.currentA_100 = currentA_100;
	}
	public String getCurrentA_75() {
		return CommonUtility.replaceNull(currentA_75);
	}
	public void setCurrentA_75(String currentA_75) {
		this.currentA_75 = currentA_75;
	}
	public String getCurrentA_50() {
		return CommonUtility.replaceNull(currentA_50);
	}
	public void setCurrentA_50(String currentA_50) {
		this.currentA_50 = currentA_50;
	}
	public String getCurrentA_Start() {
		return CommonUtility.replaceNull(currentA_Start);
	}
	public void setCurrentA_Start(String currentA_Start) {
		this.currentA_Start = currentA_Start;
	}
	public String getCurrentA_DP() {
		return CommonUtility.replaceNull(currentA_DP);
	}
	public void setCurrentA_DP(String currentA_DP) {
		this.currentA_DP = currentA_DP;
	}
	public String getEfficiency_DP() {
		return CommonUtility.replaceNull(efficiency_DP);
	}
	public void setEfficiency_DP(String efficiency_DP) {
		this.efficiency_DP = efficiency_DP;
	}
	public String getPowerFactor_Start() {
		return CommonUtility.replaceNull(powerFactor_Start);
	}
	public void setPowerFactor_Start(String powerFactor_Start) {
		this.powerFactor_Start = powerFactor_Start;
	}
	public String getPowerFactor_DP() {
		return CommonUtility.replaceNull(powerFactor_DP);
	}
	public void setPowerFactor_DP(String powerFactor_DP) {
		this.powerFactor_DP = powerFactor_DP;
	}
	public String getSstHot() {
		return CommonUtility.replaceNull(sstHot);
	}
	public void setSstHot(String sstHot) {
		this.sstHot = sstHot;
	}
	public String getSstCold() {
		return CommonUtility.replaceNull(sstCold);
	}
	public void setSstCold(String sstCold) {
		this.sstCold = sstCold;
	}
	public String getMainTBoxPS() {
		return CommonUtility.replaceNull(mainTBoxPS);
	}
	public void setMainTBoxPS(String mainTBoxPS) {
		this.mainTBoxPS = mainTBoxPS;
	}
	public String getStartTimeRatedVolt() {
		return CommonUtility.replaceNull(startTimeRatedVolt);
	}
	public void setStartTimeRatedVolt(String startTimeRatedVolt) {
		this.startTimeRatedVolt = startTimeRatedVolt;
	}
	public String getStartTimeRatedVolt80() {
		return CommonUtility.replaceNull(startTimeRatedVolt80);
	}
	public void setStartTimeRatedVolt80(String startTimeRatedVolt80) {
		this.startTimeRatedVolt80 = startTimeRatedVolt80;
	}
	public String getCableEntry() {
		return CommonUtility.replaceNull(cableEntry);
	}
	public void setCableEntry(String cableEntry) {
		this.cableEntry = cableEntry;
	}
	public String getNoBOTerminals() {
		return CommonUtility.replaceNull(noBOTerminals);
	}
	public void setNoBOTerminals(String noBOTerminals) {
		this.noBOTerminals = noBOTerminals;
	}
	public String getDegOfProtection() {
		return CommonUtility.replaceNull(degOfProtection);
	}
	public void setDegOfProtection(String degOfProtection) {
		this.degOfProtection = degOfProtection;
	}
	public String getMinStartingVolt() {
		return CommonUtility.replaceNull(minStartingVolt);
	}
	public void setMinStartingVolt(String minStartingVolt) {
		this.minStartingVolt = minStartingVolt;
	}
	public String getMainTermBox() {
		return CommonUtility.replaceNull(mainTermBox);
	}
	public void setMainTermBox(String mainTermBox) {
		this.mainTermBox = mainTermBox;
	}
	public String getBearingThermo_DT() {
		return CommonUtility.replaceNull(bearingThermo_DT);
	}
	public void setBearingThermo_DT(String bearingThermo_DT) {
		this.bearingThermo_DT = bearingThermo_DT;
	}
	public String getAirThermo() {
		return CommonUtility.replaceNull(airThermo);
	}
	public void setAirThermo(String airThermo) {
		this.airThermo = airThermo;
	}
	public String getVibProbeMPads() {
		return CommonUtility.replaceNull(vibProbeMPads);
	}
	public void setVibProbeMPads(String vibProbeMPads) {
		this.vibProbeMPads = vibProbeMPads;
	}
	public String getEncoder() {
		return CommonUtility.replaceNull(encoder);
	}
	public void setEncoder(String encoder) {
		this.encoder = encoder;
	}
	public String getSpmNipple() {
		return CommonUtility.replaceNull(spmNipple);
	}
	public void setSpmNipple(String spmNipple) {
		this.spmNipple = spmNipple;
	}
	public String getKeyPhasor() {
		return CommonUtility.replaceNull(keyPhasor);
	}
	public void setKeyPhasor(String keyPhasor) {
		this.keyPhasor = keyPhasor;
	}
	public String getSpeedSwitch() {
		return CommonUtility.replaceNull(speedSwitch);
	}
	public void setSpeedSwitch(String speedSwitch) {
		this.speedSwitch = speedSwitch;
	}
	public String getSurgeSuppressors() {
		return CommonUtility.replaceNull(surgeSuppressors);
	}
	public void setSurgeSuppressors(String surgeSuppressors) {
		this.surgeSuppressors = surgeSuppressors;
	}
	public String getCurrentTransformers() {
		return CommonUtility.replaceNull(currentTransformers);
	}
	public void setCurrentTransformers(String currentTransformers) {
		this.currentTransformers = currentTransformers;
	}
	public String getVibProbes() {
		return CommonUtility.replaceNull(vibProbes);
	}
	public void setVibProbes(String vibProbes) {
		this.vibProbes = vibProbes;
	}
	public String getMethodOfCoupling() {
		return methodOfCoupling;
	}
	public void setMethodOfCoupling(String methodOfCoupling) {
		this.methodOfCoupling = methodOfCoupling;
	}
	public String getAuxTerminalBox() {
		return CommonUtility.replaceNull(auxTerminalBox);
	}
	public void setAuxTerminalBox(String auxTerminalBox) {
		this.auxTerminalBox = auxTerminalBox;
	}
	
	
}
