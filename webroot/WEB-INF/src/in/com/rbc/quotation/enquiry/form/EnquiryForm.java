/**
 * ******************************************************************************************
 * Project Name: Request For Quotation
 * Document Name: EnquiryForm.java
 * Package: in.com.rbc.quotation.enquiry.form
 * Desc:  Form class that holds getters and setters of each property.
 * ******************************************************************************************
 * @author 100006126
 * ******************************************************************************************
 */
package in.com.rbc.quotation.enquiry.form;

import in.com.rbc.quotation.common.utility.CommonUtility;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

public class EnquiryForm extends ActionForm {
	
	
	private String dispatch; // This variable is for diffrentiating between create and edit
	private String operationType; // This variable is to check whether INSERT OR UPDATE operation has to be done for enquiry 
	private String ratingOperation; // This variable is to check if the new rating is of COPY OR NEW type
	private String enquiryOperationType; // This variable is to diffrentiate between the button actions Manager or engineer
	private String ratingOperationType; // This variable is to check whether INSERT OR UPDATE operation has to be done for rating
	private String ratingOperationId; // This variable is used for storing the ratingId that has to fetched for editing
	private String ratingsValidationMessage; // This variable will have the validation message - when submit button in clicked we display an error message if all the ratings are not validated
	private String currentRating; // This variable will be used to display the current tab in bold
	private String isLast; // This variable is to check whether the current rating is the last rating or not in edit page
	private String nextRatingId; // This variable will store the next rating id
	private String isMaxRating; // Based on this variable value "Add and new rating button will be displayed" 
	
	private int enquiryId;
	private int revisionNumber; 
	private String enquiryNumber;

	
	private int customerId;	
	private int locationId;
	private int statusId;
	private String enquiryType;
	private String consultantName;
	private String createdBy;

	private String createdBySSO;
	private String customerName;
	private String location;
	private FormFile theFile1 = null;
	private String fileDescription1;	
	private FormFile theFile2 = null;
	private String fileDescription2;
	private FormFile theFile3 = null;
	private String fileDescription3;
	private FormFile theFile4 = null;
	private String fileDescription4;
	private FormFile theFile5 = null;
	private String fileDescription5;
	
	private String createdDate;
	private String enquiryReferenceNumber;
	private String description;
	private String lastModifiedBy;
	private String lastModifiedDate;
	private String statusName;
	private int discount;
	private String discountRemarks;
	private long ceFactor;
	private long rsmFactor;
	
	private String customerType;
	private String endUser;
	private String oem;
	private String epc;
	private String dealer;
	private String projectName;
	private String industry;
	private String endUserCountry;
	private String endUserIndustry;
	private String approval;
	private String targeted;
	private String winningChance;
	private String competitor;
	private String salesStage;
	private String totalOrderValue;
	private String orderClosingMonth;
	private String expectedDeliveryMonth;
	private String warrantyDispatchDate;
	private String warrantyCommissioningDate;
	private String deliveryType;
	private String destState;
	private String destCity;
	private String spa;
	private double customerCounterOffer;
	private String materialCost;
	private String mcnspRatio;
	private double motorPrice;
	private String warrantyCost;
	private String transportationCost;	
	private String ecSupervisionCost;
	private String hzAreaCertCost;
	private String sparesCost;
	private double totalCost;
	private String propSpaReason;
	private String requestRemarks;
	private String winlossReason;
	private String winlossComment;
	private String winningCompetitor;	
	private double unitMC;
	private double unitPrice;
	private String winlossPrice;
	private double totalMC;
	private String totalPrice;
	private String totalPkgPrice;	
	private String crossReference;
	private String deptAndAuth;

	//Rating properties
	private String ratingTitle;
	private int ratingId;
	private int quantity = 0;
	private int ratingNo;
	private int htltId = 0;
	private int poleId = 0;
	private int scsrId = 0;
	private String htltName;
	private int dutyId = 0;
	private String poleName;
	private int degreeOfProId = 0;
	private int termBoxId = 0;
	private int directionOfRotId = 0;
	private int shaftExtId = 0;
	private int applicationId = 0;
	private int insulationId = 0;
	private int enclosureId = 0;
	private int mountingId = 0;
	private String methodOfStg;
	private String coupling;
	private String kw;
	private String frequency;
	private String frequencyVar;
	private String volts;
	private String voltsVar;
	private String ambience;
	private String ambienceVar;
	private String altitude;
	private String altitudeVar;
	private String tempRaise;
	private String tempRaiseVar;
	private String paint;
	private String splFeatures;
	private String details;
	private String isValidated;
	private String ratingCreatedBy;
	private String ratingCreatedBySSO;
	private String scsrName;
	private String dutyName;
	private String degreeOfProName;
	private String termBoxName;
	private String directionOfRotName;
	private String shaftExtName;
	private String applicationName;
	private String insulationName;
	private String enclosureName;
	private String mountingName;
	private String ratingUnitPrice;	
	//technical offer properties
	private String savingIndent;
	private String revision;
	private String frameSize;
	private String efficiency;
	private String pf;
	private String flcAmps;
	private String potFLT;
	private String flcSQCAGE;
	private String noiseLevel;
	private String vibrationLevel;
	private String fltSQCAGE;
	private String rpm;
	private String rtGD2;
	private String sstSQCAGE;
	private String rv;
	private String ra;
	private String remarks;
	private String updateCreateBy;
	private String isDataValidated;
	private String areaClassification;
	private String serviceFactor;
	private String bkw;
	private String nlCurrent;
	private String strtgCurrent;
	private String lrTorque;
	private String pllEff1;
	private String pllEff2;
	private String pllEff3;
	private String sstHot;
	private String sstCold;
	private String startTimeRV;
	private String startTimeRV80;
	private String coolingSystem;
	private String bearingDENDE;
	private String lubrication;
	private String migD2Load;
	private String migD2Motor;
	private String balancing;
	private String mainTermBoxPS;
	private String neutralTermBox;
	private String cableEntry;
	private String auxTermBox;
	private String statorConn;
	private String noBoTerminals;
	private String rotation;
	private String motorTotalWgt;
	private String spaceHeater;
	private String windingRTD;
	private String bearingRTD;
	private String minStartVolt;
	private String bearingThermoDT;
	private String airThermo;
	private String vibeProbMPads;
	private String encoder;
	private String speedSwitch;
	private String surgeSuppressors;
	private String currTransformers;
	private String vibProbes;
	private String spmNipple;
	private String keyPhasor;
	private String commercialOfferType = "D";
	private long commercialMCUnit;
	private String ratedOutputUnit;
	
	private String assignToId;
	private String assignToName;
	private String assignRemarks;	
	
	private String returnTo;
	private String returnToRemarks;
	private String pllCurr1;
	private String pllCurr2;
	private String pllCurr3;
	private String pllCurr4;
	private String pllPf1;
	private String pllPf2;
	private String pllpf3;
	private String pllpf4;
	private String paintColor;
	
	

	private String advancePayment;
	private String paymentTerms;

	private String customerNameMultiplier;
	private String customerTypeMultiplier;
	private String endUserMultiplier;
	private String warrantyDispatchMultiplier;
	private String warrantycommissioningMultiplier;
	private String advancePaymMultiplier;
	private String payTermMultiplier;
	private String expDeliveryMultiplier;
	private String financeMultiplier;
	private String quantityMultiplier;
	private String ratedOutputPNMulitplier;
	private String typeMultiplier;
	private String areaCMultiplier;
	private String frameMultiplier;
	private String applicationMultiplier;
	private String factorMultiplier;
	private String netmcMultiplier;
	private String markupMultiplier;
	private String transferCMultiplier;
	private String smnote;
	private String dmnote;
	private String cenote;
	private String rsmnote;
	private String pagechecking;
	
	private String mcornspratioMultiplier_rsm;
	private String transportationperunit_rsm;
	private String warrantyperunit_rsm;
	private String unitpriceMultiplier_rsm;
	private String totalpriceMultiplier_rsm;


	private String sm_name;
	private String dm_ssoid;
	private String dm_name;
	private String ce_ssoid;
	private String ce_name;
	private String rsm_ssoid;
	private String rsm_name;
	private String deviation_clarification;
	
	
	private String termsVarFormula;
	private String termsDeliveryLT;
	private String termsDeliveryHT;
	private String termsPercentAdv;
	private String termsPercentBal;
	private String returnstatement;
	private String returnstatus;
	
	
	private String dmToSmReturn;
	private String ceToDmReturn;
	private String ceToSmReturn;
	private String rsmToCeReturn;
	private String rsmToDmReturn;
	private String rsmToSmReturn;
	private String commentOperation;
	private String zone;
	private String gasGroup;
	private String returnOperation;	
	private String designEngineerName;
	private String deToSmReturn;
	private String quotationtoRsmReturn;
	private String quotationtoCeReturn;
	private String quotationtoDmReturn;
	private String uploadedAttachmentId;
	private String fileName;
	private String fileExtension;
	private String sapCustomerCode;
	private String orderNumber;
	private String poiCopy;
	private String loiCopy;
	private String deliveryReqByCust;
	private String ld;
	private String drawingApproval;
	private String qapApproval;
	private String typeTest;
	private String routineTest;
	private String specialTest;
	private String specialTestDetails;
	private String thirdPartyInspection;
	private String stageInspection;
	private String enduserLocation;
	private String epcName;
	private String packing;
	private String insurance;
	private String replacement;
	private String oldSerialNo;
	private String customerPartNo;
	private String includeFrieght;
	private String includeExtraWarrnt;
	private String includeSupervisionCost;
	private String typeTestCharges;
	private String extraFrieght;
	private String extraExtraWarrnt;
	private String extraSupervisionCost;
	private String extratypeTestCharges;
	private String priceincludeFrieght;
	private String priceincludeExtraWarn;
	private String priceincludeSupervisionCost;
	private String priceincludetypeTestCharges;
	private String priceextraFrieght;
	private String priceextraExtraWarn;
	private String priceextraSupervisionCost;
	private String priceextratypeTestCharges;
	private String deliveryLocation;
	private String deliveryAddress;
	private String noOfMotors;
	private String pllCurr5;
	private String pllEff4;
	private String pllPf5;
	
	//Indent Data Added By Egr on 15-March-2019
	private String includetypeTestCharges;
	private String priceincludeFrieghtVal;
	private String priceincludeExtraWarnVal;
	private String priceincludeSupervisionCostVal;
	private String priceincludetypeTestChargesVal;
	private String priceextraFrieghtVal;
	private String priceextraExtraWarnVal;
	private String priceextraSupervisionCostVal;
	private String priceextratypeTestChargesVal;
	private String totalMotorPrice;
	private String indentOperation;
	private String indentOperationCheck;
	private String indentnote;
	private String cmToSmReturn;
	private String returnEnquiry;
    private String commengToSmReturn;
    private String dmToCmReturn;
    private String deToCmReturn;
    private String cmNote;
    private String cmName;
	private String indentDmtosmReturn;
	private String indentDetosmReturn;
	//Lost Page Details
	private String lossPrice;
	private String lossReason1;
	private String lossReason2;
	private String lossComment;
	private String lossCompetitor;
	private String abondentComment;


	//Array List objects
	private ArrayList customerList = new ArrayList();
	private ArrayList locationList = new ArrayList();
	
	private ArrayList htltList = new ArrayList();
	private ArrayList poleList = new ArrayList();
	private ArrayList scsrList = new ArrayList();
	private ArrayList dutyList = new ArrayList();
	private ArrayList degproList = new ArrayList();
	private ArrayList termBoxList = new ArrayList();
	private ArrayList rotDirectionList = new ArrayList();
	private ArrayList shaftList = new ArrayList();
	private ArrayList applicationList = new ArrayList();
	private ArrayList insulationList = new ArrayList();
	private ArrayList enclosureList = new ArrayList();
	private ArrayList mountingList = new ArrayList();
	private ArrayList ratingList = new ArrayList();
	private ArrayList ratingObjects = new ArrayList();
	private ArrayList currentRatingObject = new ArrayList();
	private ArrayList addOnList = new ArrayList();
	private ArrayList approverCommentsList = new ArrayList();
	private ArrayList teamMemberList = new ArrayList();
	private ArrayList directManagersList = new ArrayList();
	
	private ArrayList alCustomerTypeList = new ArrayList();
	private ArrayList alIndustryList = new ArrayList();
	private ArrayList alApprovalList = new ArrayList();
	private ArrayList alTargetedList = new ArrayList();
	private ArrayList alWinningChanceList = new ArrayList();
	private ArrayList alCompetitorList = new ArrayList();
	private ArrayList alEnquiryTypeList = new ArrayList();
	private ArrayList alSalesStageList = new ArrayList();
	private ArrayList alDeliveryTypeList = new ArrayList();
	private ArrayList alSPAList = new ArrayList();
	private ArrayList alWinlossReasonList = new ArrayList();
	private ArrayList alWinCompetitorList = new ArrayList();
	private ArrayList alVoltList = new ArrayList();
	private ArrayList alWinChanceList = new ArrayList();	
	private ArrayList alAreaClassificationList = new ArrayList();
	private ArrayList alServiceFactorList = new ArrayList();
	private ArrayList alFrequencyList = new ArrayList();
	private ArrayList alStrtgCurrentList = new ArrayList();	
	private ArrayList alAmbienceList = new ArrayList();
	private ArrayList alClgSystemList = new ArrayList();
	private ArrayList alBearingDENDEList = new ArrayList();
	private ArrayList alLubricationList = new ArrayList();
	private ArrayList alBalancingList = new ArrayList();
	private ArrayList alVibrationList = new ArrayList();
	private ArrayList alMainTermBoxPSList = new ArrayList();
	private ArrayList alNeutralTBList = new ArrayList();
	private ArrayList alCableEntryList = new ArrayList();
	private ArrayList alAuxTermBoxList = new ArrayList();
	private ArrayList alStatorConnList = new ArrayList();
	private ArrayList alBoTerminalsNoList = new ArrayList();
	private ArrayList alRotationList = new ArrayList();
	private ArrayList alSpaceHeaterList = new ArrayList();
	private ArrayList alWindingRTDList = new ArrayList();
	private ArrayList alBearingRTDList = new ArrayList();
	private ArrayList alMinStartVoltList = new ArrayList();
	private ArrayList alMethodOfStgList = new ArrayList();
	private ArrayList alBearingThermoDtList = new ArrayList();
	private ArrayList alYesNoList = new ArrayList();
	private ArrayList alTempRaiseList = new ArrayList();
	
	private ArrayList alZoneList = new ArrayList();
	private ArrayList alGasGroupList = new ArrayList();
	private ArrayList salesManagerList = new ArrayList();
	private ArrayList alnoiseLevelList=new ArrayList();
	private ArrayList previousratingList = new ArrayList();

	private ArrayList deliveryDetailsList = new ArrayList(); //To Store Delivery Details On Indent Page Status is Won

	private ArrayList commercialManagersList = new ArrayList();	
	
	private String isNewEnquiry;
	
	/**
	 * @return Returns the teamMemberList.
	 */
	public ArrayList getTeamMemberList() {
		return teamMemberList;
	}
	/**
	 * @param teamMemberList The teamMemberList to set.
	 */
	public void setTeamMemberList(ArrayList teamMemberList) {
		this.teamMemberList = teamMemberList;
	}
	/**
	 * @return Returns the approverCommentsList.
	 */
	public ArrayList getApproverCommentsList() {
		return approverCommentsList;
	}
	/**
	 * @param approverCommentsList The approverCommentsList to set.
	 */
	public void setApproverCommentsList(ArrayList approverCommentsList) {
		this.approverCommentsList = approverCommentsList;
	}
	/**
	 * @return Returns the addOnList.
	 */
	public ArrayList getAddOnList() {
		return addOnList;
	}
	/**
	 * @param addOnList The addOnList to set.
	 */
	public void setAddOnList(ArrayList addOnList) {
		this.addOnList = addOnList;
	}
	/**
	 * @return Returns the currentRatingObject.
	 */
	public ArrayList getCurrentRatingObject() {
		return currentRatingObject;
	}
	/**
	 * @param currentRatingObject The currentRatingObject to set.
	 */
	public void setCurrentRatingObject(ArrayList currentRatingObject) {
		this.currentRatingObject = currentRatingObject;
	}
	/**
	 * @return Returns the ratingObjects.
	 */
	public ArrayList getRatingObjects() {
		return ratingObjects;
	}
	/**
	 * @param ratingObjects The ratingObjects to set.
	 */
	public void setRatingObjects(ArrayList ratingObjects) {
		this.ratingObjects = ratingObjects;
	}
	/**
	 * @return Returns the ratingList.
	 */
	public ArrayList getRatingList() {
		return ratingList;
	}
	/**
	 * @param ratingList The ratingList to set.
	 */
	public void setRatingList(ArrayList ratingList) {
		this.ratingList = ratingList;
	}
	/**
	 * @return Returns the applicationList.
	 */
	public ArrayList getApplicationList() {
		return applicationList;
	}
	/**
	 * @param applicationList The applicationList to set.
	 */
	public void setApplicationList(ArrayList applicationList) {
		this.applicationList = applicationList;
	}
	/**
	 * @return Returns the degproList.
	 */
	public ArrayList getDegproList() {
		return degproList;
	}
	/**
	 * @param degproList The degproList to set.
	 */
	public void setDegproList(ArrayList degproList) {
		this.degproList = degproList;
	}
	/**
	 * @return Returns the dutyList.
	 */
	public ArrayList getDutyList() {
		return dutyList;
	}
	/**
	 * @param dutyList The dutyList to set.
	 */
	public void setDutyList(ArrayList dutyList) {
		this.dutyList = dutyList;
	}
	/**
	 * @return Returns the enclosureList.
	 */
	public ArrayList getEnclosureList() {
		return enclosureList;
	}
	/**
	 * @param enclosureList The enclosureList to set.
	 */
	public void setEnclosureList(ArrayList enclosureList) {
		this.enclosureList = enclosureList;
	}
	/**
	 * @return Returns the htltList.
	 */
	public ArrayList getHtltList() {
		return htltList;
	}
	/**
	 * @param htltList The htltList to set.
	 */
	public void setHtltList(ArrayList htltList) {
		this.htltList = htltList;
	}
	/**
	 * @return Returns the insulationList.
	 */
	public ArrayList getInsulationList() {
		return insulationList;
	}
	/**
	 * @param insulationList The insulationList to set.
	 */
	public void setInsulationList(ArrayList insulationList) {
		this.insulationList = insulationList;
	}
	/**
	 * @return Returns the mountingList.
	 */
	public ArrayList getMountingList() {
		return mountingList;
	}
	/**
	 * @param mountingList The mountingList to set.
	 */
	public void setMountingList(ArrayList mountingList) {
		this.mountingList = mountingList;
	}
	/**
	 * @return Returns the poleList.
	 */
	public ArrayList getPoleList() {
		return poleList;
	}
	/**
	 * @param poleList The poleList to set.
	 */
	public void setPoleList(ArrayList poleList) {
		this.poleList = poleList;
	}
	/**
	 * @return Returns the rotDirectionList.
	 */
	public ArrayList getRotDirectionList() {
		return rotDirectionList;
	}
	/**
	 * @param rotDirectionList The rotDirectionList to set.
	 */
	public void setRotDirectionList(ArrayList rotDirectionList) {
		this.rotDirectionList = rotDirectionList;
	}
	/**
	 * @return Returns the scsrList.
	 */
	public ArrayList getScsrList() {
		return scsrList;
	}
	/**
	 * @param scsrList The scsrList to set.
	 */
	public void setScsrList(ArrayList scsrList) {
		this.scsrList = scsrList;
	}
	/**
	 * @return Returns the shaftList.
	 */
	public ArrayList getShaftList() {
		return shaftList;
	}
	/**
	 * @param shaftList The shaftList to set.
	 */
	public void setShaftList(ArrayList shaftList) {
		this.shaftList = shaftList;
	}
	/**
	 * @return Returns the termBoxList.
	 */
	public ArrayList getTermBoxList() {
		return termBoxList;
	}
	/**
	 * @param termBoxList The termBoxList to set.
	 */
	public void setTermBoxList(ArrayList termBoxList) {
		this.termBoxList = termBoxList;
	}
	/**
	 * @return Returns the altitude.
	 */
	public String getAltitude() {
		return CommonUtility.replaceNull(altitude);
	}
	/**
	 * @param altitude The altitude to set.
	 */
	public void setAltitude(String altitude) {
		this.altitude = altitude;
	}
	/**
	 * @return Returns the altitudeVar.
	 */
	public String getAltitudeVar() {
		return CommonUtility.replaceNull(altitudeVar);
	}
	/**
	 * @param altitudeVar The altitudeVar to set.
	 */
	public void setAltitudeVar(String altitudeVar) {
		this.altitudeVar = altitudeVar;
	}
	/**
	 * @return Returns the ambience.
	 */
	public String getAmbience() {
		return CommonUtility.replaceNull(ambience);
	}
	/**
	 * @param ambience The ambience to set.
	 */
	public void setAmbience(String ambience) {
		this.ambience = ambience;
	}
	/**
	 * @return Returns the ambienceVar.
	 */
	public String getAmbienceVar() {
		return CommonUtility.replaceNull(ambienceVar);
	}
	/**
	 * @param ambienceVar The ambienceVar to set.
	 */
	public void setAmbienceVar(String ambienceVar) {
		this.ambienceVar = ambienceVar;
	}
	/**
	 * @return Returns the applicationId.
	 */
	public int getApplicationId() {
		return applicationId;
	}
	/**
	 * @param applicationId The applicationId to set.
	 */
	public void setApplicationId(int applicationId) {
		this.applicationId = applicationId;
	}
	/**
	 * @return Returns the consultantName.
	 */
	public String getConsultantName() {
		return CommonUtility.replaceNull(consultantName);
	}
	/**
	 * @param consultantName The consultantName to set.
	 */
	public void setConsultantName(String consultantName) {
		this.consultantName = consultantName;
	}
	/**
	 * @return Returns the coupling.
	 */
	public String getCoupling() {
		return CommonUtility.replaceNull(coupling);
	}
	/**
	 * @param coupling The coupling to set.
	 */
	public void setCoupling(String coupling) {
		this.coupling = coupling;
	}
	/**
	 * @return Returns the createdBy.
	 */
	public String getCreatedBy() {
		return CommonUtility.replaceNull(createdBy);
	}
	/**
	 * @param createdBy The createdBy to set.
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return Returns the createdDate.
	 */
	public String getCreatedDate() {
		return CommonUtility.replaceNull(createdDate);
	}
	/**
	 * @param createdDate The createdDate to set.
	 */
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	
	/**
	 * @return Returns the customerId.
	 */
	public int getCustomerId() {
		return customerId;
	}
	/**
	 * @param customerId The customerId to set.
	 */
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	/**
	 * @return Returns the degreeOfProId.
	 */
	public int getDegreeOfProId() {
		return degreeOfProId;
	}
	/**
	 * @param degreeOfProId The degreeOfProId to set.
	 */
	public void setDegreeOfProId(int degreeOfProId) {
		this.degreeOfProId = degreeOfProId;
	}
	/**
	 * @return Returns the description.
	 */
	public String getDescription() {
		return CommonUtility.replaceNull(description);
	}
	/**
	 * @param description The description to set.
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return Returns the details.
	 */
	public String getDetails() {
		return CommonUtility.replaceNull(details);
	}
	/**
	 * @param details The details to set.
	 */
	public void setDetails(String details) {
		this.details = details;
	}
	/**
	 * @return Returns the directionOfRotId.
	 */
	public int getDirectionOfRotId() {
		return directionOfRotId;
	}
	/**
	 * @param directionOfRotId The directionOfRotId to set.
	 */
	public void setDirectionOfRotId(int directionOfRotId) {
		this.directionOfRotId = directionOfRotId;
	}
	/**
	 * @return Returns the dutyId.
	 */
	public int getDutyId() {
		return dutyId;
	}
	/**
	 * @param dutyId The dutyId to set.
	 */
	public void setDutyId(int dutyId) {
		this.dutyId = dutyId;
	}
	/**
	 * @return Returns the enclosureId.
	 */
	public int getEnclosureId() {
		return enclosureId;
	}
	/**
	 * @param enclosureId The enclosureId to set.
	 */
	public void setEnclosureId(int enclosureId) {
		this.enclosureId = enclosureId;
	}
	/**
	 * @return Returns the enquiryId.
	 */
	public int getEnquiryId() {
		return enquiryId;
	}
	/**
	 * @param enquiryId The enquiryId to set.
	 */
	public void setEnquiryId(int enquiryId) {
		this.enquiryId = enquiryId;
	}
	/**
	 * @return Returns the enquiryNumber.
	 */
	public String getEnquiryNumber() {
		return CommonUtility.replaceNull(enquiryNumber);
	}
	/**
	 * @param enquiryNumber The enquiryNumber to set.
	 */
	public void setEnquiryNumber(String enquiryNumber) {
		this.enquiryNumber = enquiryNumber;
	}
	/**
	 * @return Returns the enquiryType.
	 */
	public String getEnquiryType() {
		return CommonUtility.replaceNull(enquiryType);
	}
	/**
	 * @param enquiryType The enquiryType to set.
	 */
	public void setEnquiryType(String enquiryType) {
		this.enquiryType = enquiryType;
	}
	/**
	 * @return Returns the frequency.
	 */
	public String getFrequency() {
		return CommonUtility.replaceNull(frequency);
	}
	/**
	 * @param frequency The frequency to set.
	 */
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	/**
	 * @return Returns the frequencyVar.
	 */
	public String getFrequencyVar() {
		return CommonUtility.replaceNull(frequencyVar);
	}
	/**
	 * @param frequencyVar The frequencyVar to set.
	 */
	public void setFrequencyVar(String frequencyVar) {
		this.frequencyVar = frequencyVar;
	}
	/**
	 * @return Returns the htltId.
	 */
	public int getHtltId() {
		return htltId;
	}
	/**
	 * @param htltId The htltId to set.
	 */
	public void setHtltId(int htltId) {
		this.htltId = htltId;
	}
	/**
	 * @return Returns the htltName.
	 */
	public String getHtltName() {
		return CommonUtility.replaceNull(htltName);
	}
	/**
	 * @param htltName The htltName to set.
	 */
	public void setHtltName(String htltName) {
		this.htltName = htltName;
	}
	/**
	 * @return Returns the insulationId.
	 */
	public int getInsulationId() {
		return insulationId;
	}
	/**
	 * @param insulationId The insulationId to set.
	 */
	public void setInsulationId(int insulationId) {
		this.insulationId = insulationId;
	}
	/**
	 * @return Returns the isValidated.
	 */
	public String getIsValidated() {
		return CommonUtility.replaceNull(isValidated);
	}
	/**
	 * @param isValidated The isValidated to set.
	 */
	public void setIsValidated(String isValidated) {
		this.isValidated = isValidated;
	}
	/**
	 * @return Returns the kw.
	 */
	public String getKw() {
		return CommonUtility.replaceNull(kw);
	}
	/**
	 * @param kw The kw to set.
	 */
	public void setKw(String kw) {
		this.kw = kw;
	}
	/**
	 * @return Returns the lastModifiedBy.
	 */
	public String getLastModifiedBy() {
		return CommonUtility.replaceNull(lastModifiedBy);
	}
	/**
	 * @param lastModifiedBy The lastModifiedBy to set.
	 */
	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
	/**
	 * @return Returns the locationId.
	 */
	public int getLocationId() {
		return locationId;
	}
	/**
	 * @param locationId The locationId to set.
	 */
	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}
	/**
	 * @return Returns the methodOfStg.
	 */
	public String getMethodOfStg() {
		return CommonUtility.replaceNull(methodOfStg);
	}
	/**
	 * @param methodOfStg The methodOfStg to set.
	 */
	public void setMethodOfStg(String methodOfStg) {
		this.methodOfStg = methodOfStg;
	}
	/**
	 * @return Returns the mountingId.
	 */
	public int getMountingId() {
		return mountingId;
	}
	/**
	 * @param mountingId The mountingId to set.
	 */
	public void setMountingId(int mountingId) {
		this.mountingId = mountingId;
	}
	/**
	 * @return Returns the paint.
	 */
	public String getPaint() {
		return CommonUtility.replaceNull(paint);
	}
	/**
	 * @param paint The paint to set.
	 */
	public void setPaint(String paint) {
		this.paint = paint;
	}
	/**
	 * @return Returns the poleId.
	 */
	public int getPoleId() {
		return poleId;
	}
	/**
	 * @param poleId The poleId to set.
	 */
	public void setPoleId(int poleId) {
		this.poleId = poleId;
	}
	/**
	 * @return Returns the poleName.
	 */
	public String getPoleName() {
		return CommonUtility.replaceNull(poleName);
	}
	/**
	 * @param poleName The poleName to set.
	 */
	public void setPoleName(String poleName) {
		this.poleName = poleName;
	}
	/**
	 * @return Returns the quantity.
	 */
	public int getQuantity() {
		return quantity;
	}
	/**
	 * @param quantity The quantity to set.
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	/**
	 * @return Returns the revisionNumber.
	 */
	public int getRevisionNumber() {
		return revisionNumber;
	}
	/**
	 * @param revisionNumber The revisionNumber to set.
	 */
	public void setRevisionNumber(int revisionNumber) {
		this.revisionNumber = revisionNumber;
	}
	/**
	 * @return Returns the scsrId.
	 */
	public int getScsrId() {
		return scsrId;
	}
	/**
	 * @param scsrId The scsrId to set.
	 */
	public void setScsrId(int scsrId) {
		this.scsrId = scsrId;
	}
	/**
	 * @return Returns the shaftExtId.
	 */
	public int getShaftExtId() {
		return shaftExtId;
	}
	/**
	 * @param shaftExtId The shaftExtId to set.
	 */
	public void setShaftExtId(int shaftExtId) {
		this.shaftExtId = shaftExtId;
	}
	/**
	 * @return Returns the splFeatures.
	 */
	public String getSplFeatures() {
		return CommonUtility.replaceNull(splFeatures);
	}
	/**
	 * @param splFeatures The splFeatures to set.
	 */
	public void setSplFeatures(String splFeatures) {
		this.splFeatures = splFeatures;
	}
	/**
	 * @return Returns the tempRaise.
	 */
	public String getTempRaise() {
		return CommonUtility.replaceNull(tempRaise);
	}
	/**
	 * @param tempRaise The tempRaise to set.
	 */
	public void setTempRaise(String tempRaise) {
		this.tempRaise = tempRaise;
	}
	/**
	 * @return Returns the tempRaiseVar.
	 */
	public String getTempRaiseVar() {
		return CommonUtility.replaceNull(tempRaiseVar);
	}
	/**
	 * @param tempRaiseVar The tempRaiseVar to set.
	 */
	public void setTempRaiseVar(String tempRaiseVar) {
		this.tempRaiseVar = tempRaiseVar;
	}
	/**
	 * @return Returns the termBoxId.
	 */
	public int getTermBoxId() {
		return termBoxId;
	}
	/**
	 * @param termBoxId The termBoxId to set.
	 */
	public void setTermBoxId(int termBoxId) {
		this.termBoxId = termBoxId;
	}
	/**
	 * @return Returns the volts.
	 */
	public String getVolts() {
		return CommonUtility.replaceNull(volts);
	}
	/**
	 * @param volts The volts to set.
	 */
	public void setVolts(String volts) {
		this.volts = volts;
	}
	/**
	 * @return Returns the voltsVar.
	 */
	public String getVoltsVar() {
		return CommonUtility.replaceNull(voltsVar);
	}
	/**
	 * @param voltsVar The voltsVar to set.
	 */
	public void setVoltsVar(String voltsVar) {
		this.voltsVar = voltsVar;
	}
	/**
	 * @return Returns the createdBySSO.
	 */
	public String getCreatedBySSO() {
		return CommonUtility.replaceNull(createdBySSO);
	}
	/**
	 * @param createdBySSO The createdBySSO to set.
	 */
	public void setCreatedBySSO(String createdBySSO) {
		this.createdBySSO = createdBySSO;
	}
	/**
	 * @return Returns the operationType.
	 */
	public String getOperationType() {
		return CommonUtility.replaceNull(operationType);
	}
	/**
	 * @param operationType The operationType to set.
	 */
	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}
	/**
	 * @return Returns the ratingOperation.
	 */
	public String getRatingOperation() {
		return CommonUtility.replaceNull(ratingOperation);
	}
	/**
	 * @param ratingOperation The ratingOperation to set.
	 */
	public void setRatingOperation(String ratingOperation) {
		this.ratingOperation = ratingOperation;
	}
	/**
	 * @return Returns the customerList.
	 */
	public ArrayList getCustomerList() {
		return customerList;
	}
	/**
	 * @param customerList The customerList to set.
	 */
	public void setCustomerList(ArrayList customerList) {
		this.customerList = customerList;
	}
	/**
	 * @return Returns the locationList.
	 */
	public ArrayList getLocationList() {
		return locationList;
	}
	/**
	 * @param locationList The locationList to set.
	 */
	public void setLocationList(ArrayList locationList) {
		this.locationList = locationList;
	}
	/**
	 * @return Returns the ratingCreatedBy.
	 */
	public String getRatingCreatedBy() {
		return CommonUtility.replaceNull(ratingCreatedBy);
	}
	/**
	 * @param ratingCreatedBy The ratingCreatedBy to set.
	 */
	public void setRatingCreatedBy(String ratingCreatedBy) {
		this.ratingCreatedBy = ratingCreatedBy;
	}
	/**
	 * @return Returns the ratingCreatedBySSO.
	 */
	public String getRatingCreatedBySSO() {
		return CommonUtility.replaceNull(ratingCreatedBySSO);
	}
	/**
	 * @param ratingCreatedBySSO The ratingCreatedBySSO to set.
	 */
	public void setRatingCreatedBySSO(String ratingCreatedBySSO) {
		this.ratingCreatedBySSO = ratingCreatedBySSO;
	}
	/**
	 * @return Returns the ratingTitle.
	 */
	public String getRatingTitle() {
		return CommonUtility.replaceNull(ratingTitle);
	}
	/**
	 * @param ratingTitle The ratingTitle to set.
	 */
	public void setRatingTitle(String ratingTitle) {
		this.ratingTitle = ratingTitle;
	}
	/**
	 * @return Returns the enquiryOperationType.
	 */
	public String getEnquiryOperationType() {
		return CommonUtility.replaceNull(enquiryOperationType);
	}
	/**
	 * @param enquiryOperationType The enquiryOperationType to set.
	 */
	public void setEnquiryOperationType(String enquiryOperationType) {
		this.enquiryOperationType = enquiryOperationType;
	}
	/**
	 * @return Returns the ratingOperationType.
	 */
	public String getRatingOperationType() {
		return CommonUtility.replaceNull(ratingOperationType);
	}
	/**
	 * @param ratingOperationType The ratingOperationType to set.
	 */
	public void setRatingOperationType(String ratingOperationType) {
		this.ratingOperationType = ratingOperationType;
	}
	/**
	 * @return Returns the ratingOperationId.
	 */
	public String getRatingOperationId() {
		return CommonUtility.replaceNull(ratingOperationId);
	}
	/**
	 * @param ratingOperationId The ratingOperationId to set.
	 */
	public void setRatingOperationId(String ratingOperationId) {
		this.ratingOperationId = ratingOperationId;
	}
	/**
	 * @return Returns the ratingId.
	 */
	public int getRatingId() {
		return ratingId;
	}
	/**
	 * @param ratingId The ratingId to set.
	 */
	public void setRatingId(int ratingId) {
		this.ratingId = ratingId;
	}
	/**
	 * @return Returns the ratingNo.
	 */
	public int getRatingNo() {
		return ratingNo;
	}
	/**
	 * @param ratingNo The ratingNo to set.
	 */
	public void setRatingNo(int ratingNo) {
		this.ratingNo = ratingNo;
	}
	/**
	 * @return Returns the ratingsValidationMessage.
	 */
	public String getRatingsValidationMessage() {
		return CommonUtility.replaceNull(ratingsValidationMessage);
	}
	/**
	 * @param ratingsValidationMessage The ratingsValidationMessage to set.
	 */
	public void setRatingsValidationMessage(String ratingsValidationMessage) {
		this.ratingsValidationMessage = ratingsValidationMessage;
	}
	/**
	 * @return Returns the customerName.
	 */
	public String getCustomerName() {
		return CommonUtility.replaceNull(customerName);
	}
	/**
	 * @param customerName The customerName to set.
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	/**
	 * @return Returns the locationName.
	 */
	public String getLocation() {
		return CommonUtility.replaceNull(location);
	}
	/**
	 * @param locationName The locationName to set.
	 */
	public void setLocation(String location) {
		this.location = location;
	}
	/**
	 * @return Returns the dispatch.
	 */
	public String getDispatch() {
		return CommonUtility.replaceNull(dispatch);
	}
	/**
	 * @param dispatch The dispatch to set.
	 */
	public void setDispatch(String dispatch) {
		this.dispatch = dispatch;
	}
	/**
	 * @return Returns the currentRating.
	 */
	public String getCurrentRating() {
		return CommonUtility.replaceNull(currentRating);
	}
	/**
	 * @param currentRating The currentRating to set.
	 */
	public void setCurrentRating(String currentRating) {
		this.currentRating = currentRating;
	}
	/**
	 * @return Returns the isLast.
	 */
	public String getIsLast() {
		return CommonUtility.replaceNull(isLast);
	}
	/**
	 * @param isLast The isLast to set.
	 */
	public void setIsLast(String isLast) {
		this.isLast = isLast;
	}
	/**
	 * @return Returns the nextRatingId.
	 */
	public String getNextRatingId() {
		return CommonUtility.replaceNull(nextRatingId);
	}
	/**
	 * @param nextRatingId The nextRatingId to set.
	 */
	public void setNextRatingId(String nextRatingId) {
		this.nextRatingId = nextRatingId;
	}
	/**
	 * @return Returns the isMaxRating.
	 */
	public String getIsMaxRating() {
		return CommonUtility.replaceNull(isMaxRating);
	}
	/**
	 * @param isMaxRating The isMaxRating to set.
	 */
	public void setIsMaxRating(String isMaxRating) {
		this.isMaxRating = isMaxRating;
	}
	/**
	 * @return Returns the efficiency.
	 */
	public String getEfficiency() {
		return CommonUtility.replaceNull(efficiency);
	}
	/**
	 * @param efficiency The efficiency to set.
	 */
	public void setEfficiency(String efficiency) {
		this.efficiency = efficiency;
	}
	/**
	 * @return Returns the flcAmps.
	 */
	public String getFlcAmps() {
		return CommonUtility.replaceNullorNAN(flcAmps);
	}
	/**
	 * @param flcAmps The flcAmps to set.
	 */
	public void setFlcAmps(String flcAmps) {
		this.flcAmps = flcAmps;
	}
	/**
	 * @return Returns the flcSQCAGE.
	 */
	public String getFlcSQCAGE() {
		return CommonUtility.replaceNull(flcSQCAGE);
	}
	/**
	 * @param flcSQCAGE The flcSQCAGE to set.
	 */
	public void setFlcSQCAGE(String flcSQCAGE) {
		this.flcSQCAGE = flcSQCAGE;
	}
	/**
	 * @return Returns the fltSQCAGE.
	 */
	public String getFltSQCAGE() {
		return CommonUtility.replaceNull(fltSQCAGE);
	}
	/**
	 * @param fltSQCAGE The fltSQCAGE to set.
	 */
	public void setFltSQCAGE(String fltSQCAGE) {
		this.fltSQCAGE = fltSQCAGE;
	}
	/**
	 * @return Returns the frameSize.
	 */
	public String getFrameSize() {
		return CommonUtility.replaceNull(frameSize);
	}
	/**
	 * @param frameSize The frameSize to set.
	 */
	public void setFrameSize(String frameSize) {
		this.frameSize = frameSize;
	}
	/**
	 * @return Returns the noiseLevel.
	 */
	public String getNoiseLevel() {
		return CommonUtility.replaceNull(noiseLevel);
	}
	/**
	 * @param noiseLevel The noiseLevel to set.
	 */
	public void setNoiseLevel(String noiseLevel) {
		this.noiseLevel = noiseLevel;
	}
	/**
	 * @return Returns the pf.
	 */
	public String getPf() {
		return CommonUtility.replaceNull(pf);
	}
	/**
	 * @param pf The pf to set.
	 */
	public void setPf(String pf) {
		this.pf = pf;
	}
	/**
	 * @return Returns the potFLT.
	 */
	public String getPotFLT() {
		return CommonUtility.replaceNull(potFLT);
	}
	/**
	 * @param potFLT The potFLT to set.
	 */
	public void setPotFLT(String potFLT) {
		this.potFLT = potFLT;
	}
	/**
	 * @return Returns the ra.
	 */
	public String getRa() {
		return CommonUtility.replaceNull(ra);
	}
	/**
	 * @param ra The ra to set.
	 */
	public void setRa(String ra) {
		this.ra = ra;
	}
	/**
	 * @return Returns the remarks.
	 */
	public String getRemarks() {
		return CommonUtility.replaceNull(remarks);
	}
	/**
	 * @param remarks The remarks to set.
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	/**
	 * @return Returns the rpm.
	 */
	public String getRpm() {
		return CommonUtility.replaceNull(rpm);
	}
	/**
	 * @param rpm The rpm to set.
	 */
	public void setRpm(String rpm) {
		this.rpm = rpm;
	}
	/**
	 * @return Returns the rtGD2.
	 */
	public String getRtGD2() {
		return CommonUtility.replaceNull(rtGD2);
	}
	/**
	 * @param rtGD2 The rtGD2 to set.
	 */
	public void setRtGD2(String rtGD2) {
		this.rtGD2 = rtGD2;
	}
	/**
	 * @return Returns the rv.
	 */
	public String getRv() {
		return CommonUtility.replaceNull(rv);
	}
	/**
	 * @param rv The rv to set.
	 */
	public void setRv(String rv) {
		this.rv = rv;
	}
	/**
	 * @return Returns the sstSQCAGE.
	 */
	public String getSstSQCAGE() {
		return CommonUtility.replaceNull(sstSQCAGE);
	}
	/**
	 * @param sstSQCAGE The sstSQCAGE to set.
	 */
	public void setSstSQCAGE(String sstSQCAGE) {
		this.sstSQCAGE = sstSQCAGE;
	}
	/**
	 * @return Returns the vibrationLevel.
	 */
	public String getVibrationLevel() {
		return CommonUtility.replaceNull(vibrationLevel);
	}
	/**
	 * @param vibrationLevel The vibrationLevel to set.
	 */
	public void setVibrationLevel(String vibrationLevel) {
		this.vibrationLevel = vibrationLevel;
	}
	/**
	 * @return Returns the applicationName.
	 */
	public String getApplicationName() {
		return CommonUtility.replaceNull(applicationName);
	}
	/**
	 * @param applicationName The applicationName to set.
	 */
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
	/**
	 * @return Returns the degreeOfProName.
	 */
	public String getDegreeOfProName() {
		return CommonUtility.replaceNull(degreeOfProName);
	}
	/**
	 * @param degreeOfProName The degreeOfProName to set.
	 */
	public void setDegreeOfProName(String degreeOfProName) {
		this.degreeOfProName = degreeOfProName;
	}
	/**
	 * @return Returns the directionOfRotName.
	 */
	public String getDirectionOfRotName() {
		return CommonUtility.replaceNull(directionOfRotName);
	}
	/**
	 * @param directionOfRotName The directionOfRotName to set.
	 */
	public void setDirectionOfRotName(String directionOfRotName) {
		this.directionOfRotName = directionOfRotName;
	}
	/**
	 * @return Returns the dutyName.
	 */
	public String getDutyName() {
		return CommonUtility.replaceNull(dutyName);
	}
	/**
	 * @param dutyName The dutyName to set.
	 */
	public void setDutyName(String dutyName) {
		this.dutyName = dutyName;
	}
	/**
	 * @return Returns the enclosureName.
	 */
	public String getEnclosureName() {
		return CommonUtility.replaceNull(enclosureName);
	}
	/**
	 * @param enclosureName The enclosureName to set.
	 */
	public void setEnclosureName(String enclosureName) {
		this.enclosureName = enclosureName;
	}
	/**
	 * @return Returns the insulationName.
	 */
	public String getInsulationName() {
		return CommonUtility.replaceNull(insulationName);
	}
	/**
	 * @param insulationName The insulationName to set.
	 */
	public void setInsulationName(String insulationName) {
		this.insulationName = insulationName;
	}
	/**
	 * @return Returns the mountingName.
	 */
	public String getMountingName() {
		return CommonUtility.replaceNull(mountingName);
	}
	/**
	 * @param mountingName The mountingName to set.
	 */
	public void setMountingName(String mountingName) {
		this.mountingName = mountingName;
	}
	/**
	 * @return Returns the scsrName.
	 */
	public String getScsrName() {
		return CommonUtility.replaceNull(scsrName);
	}
	/**
	 * @param scsrName The scsrName to set.
	 */
	public void setScsrName(String scsrName) {
		this.scsrName = scsrName;
	}
	/**
	 * @return Returns the shaftExtName.
	 */
	public String getShaftExtName() {
		return CommonUtility.replaceNull(shaftExtName);
	}
	/**
	 * @param shaftExtName The shaftExtName to set.
	 */
	public void setShaftExtName(String shaftExtName) {
		this.shaftExtName = shaftExtName;
	}
	/**
	 * @return Returns the termBoxName.
	 */
	public String getTermBoxName() {
		return CommonUtility.replaceNull(termBoxName);
	}
	/**
	 * @param termBoxName The termBoxName to set.
	 */
	public void setTermBoxName(String termBoxName) {
		this.termBoxName = termBoxName;
	}
	/**
	 * @return Returns the updateCreateBy.
	 */
	public String getUpdateCreateBy() {
		return CommonUtility.replaceNull(updateCreateBy);
	}
	/**
	 * @param updateCreateBy The updateCreateBy to set.
	 */
	public void setUpdateCreateBy(String updateCreateBy) {
		this.updateCreateBy = updateCreateBy;
	}
	/**
	 * @return Returns the assignToId.
	 */
	public String getAssignToId() {
		return CommonUtility.replaceNull(assignToId);
	}
	/**
	 * @param assignToId The assignToId to set.
	 */
	public void setAssignToId(String assignToId) {
		this.assignToId = assignToId;
	}
	/**
	 * @return Returns the assignToName.
	 */
	public String getAssignToName() {
		return CommonUtility.replaceNull(assignToName);
	}
	/**
	 * @param assignToName The assignToName to set.
	 */
	public void setAssignToName(String assignToName) {
		this.assignToName = assignToName;
	}
	/**
	 * @return Returns the assignRemarks.
	 */
	public String getAssignRemarks() {
		return CommonUtility.replaceNull(assignRemarks);
	}
	/**
	 * @param assignRemarks The assignRemarks to set.
	 */
	public void setAssignRemarks(String assignRemarks) {
		this.assignRemarks = assignRemarks;
	}
	/**
	 * @return Returns the commercialOfferType.
	 */
	public String getCommercialOfferType() {
		return CommonUtility.replaceNull(commercialOfferType);
	}
	/**
	 * @param commercialOfferType The commercialOfferType to set.
	 */
	public void setCommercialOfferType(String commercialOfferType) {
		this.commercialOfferType = commercialOfferType;
	}
	/**
	 * @return Returns the commercialMCUnit.
	 */
	public long getCommercialMCUnit() {
		return commercialMCUnit;
	}
	/**
	 * @param commercialMCUnit The commercialMCUnit to set.
	 */
	public void setCommercialMCUnit(long commercialMCUnit) {
		this.commercialMCUnit = commercialMCUnit;
	}
	/**
	 * @return Returns the isDataValidated.
	 */
	public String getIsDataValidated() {
		return CommonUtility.replaceNull(isDataValidated);
	}
	/**
	 * @param isDataValidated The isDataValidated to set.
	 */
	public void setIsDataValidated(String isDataValidated) {
		this.isDataValidated = isDataValidated;
	}
	/**
	 * @return Returns the statusName.
	 */
	public String getStatusName() {
		return CommonUtility.replaceNull(statusName);
	}
	/**
	 * @param statusName The statusName to set.
	 */
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}	
	/**
	 * @return Returns the discountRemarks.
	 */
	public String getDiscountRemarks() {
		return CommonUtility.replaceNull(discountRemarks);
	}
	/**
	 * @param discountRemarks The discountRemarks to set.
	 */
	public void setDiscountRemarks(String discountRemarks) {
		this.discountRemarks = discountRemarks;
	}
	/**
	 * @param discount The discount to set.
	 */
	public void setDiscount(int discount) {
		this.discount = discount;
	}
	/**
	 * @return Returns the discount.
	 */
	public int getDiscount() {
		return discount;
	}
	/**
	 * @return Returns the returnTo.
	 */
	public String getReturnTo() {
		return CommonUtility.replaceNull(returnTo);
	}
	/**
	 * @param returnTo The returnTo to set.
	 */
	public void setReturnTo(String returnTo) {
		this.returnTo = returnTo;
	}
	/**
	 * @return Returns the returnToRemarks.
	 */
	public String getReturnToRemarks() {
		return CommonUtility.replaceNull(returnToRemarks);
	}
	/**
	 * @param returnToRemarks The returnToRemarks to set.
	 */
	public void setReturnToRemarks(String returnToRemarks) {
		this.returnToRemarks = returnToRemarks;
	}
	public String getTermsDeliveryHT() {
		return termsDeliveryHT;
	}
	public void setTermsDeliveryHT(String termsDeliveryHT) {
		this.termsDeliveryHT = termsDeliveryHT;
	}
	public String getTermsDeliveryLT() {
		return termsDeliveryLT;
	}
	public void setTermsDeliveryLT(String termsDeliveryLT) {
		this.termsDeliveryLT = termsDeliveryLT;
	}
	public String getTermsPercentAdv() {
		return termsPercentAdv;
	}
	public void setTermsPercentAdv(String termsPercentAdv) {
		this.termsPercentAdv = termsPercentAdv;
	}
	public String getTermsPercentBal() {
		return termsPercentBal;
	}
	public void setTermsPercentBal(String termsPercentBal) {
		this.termsPercentBal = termsPercentBal;
	}
	public String getTermsVarFormula() {
		return termsVarFormula;
	}
	public void setTermsVarFormula(String termsVarFormula) {
		this.termsVarFormula = termsVarFormula;
	}
	/**
	 * @return Returns the ceFactor.
	 */
	public long getCeFactor() {
		return ceFactor;
	}
	/**
	 * @param ceFactor The ceFactor to set.
	 */
	public void setCeFactor(long ceFactor) {
		this.ceFactor = ceFactor;
	}
	/**
	 * @return Returns the rsmFactor.
	 */
	public long getRsmFactor() {
		return rsmFactor;
	}
	/**
	 * @param rsmFactor The rsmFactor to set.
	 */
	public void setRsmFactor(long rsmFactor) {
		this.rsmFactor = rsmFactor;
	}
	/**
	 * @return Returns the statusId.
	 */
	public int getStatusId() {
		return statusId;
	}
	/**
	 * @param statusId The statusId to set.
	 */
	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}
	/**
	 * @return Returns the directManagersList.
	 */
	public ArrayList getDirectManagersList() {
		return directManagersList;
	}
	/**
	 * @param directManagersList The directManagersList to set.
	 */
	public void setDirectManagersList(ArrayList directManagersList) {
		this.directManagersList = directManagersList;
	}
	/**
	 * @return Returns the theFile1.
	 */

	public FormFile getTheFile1() {
		return theFile1;
	}
	
	public void setTheFile1(FormFile theFile1) {
		this.theFile1 = theFile1;
	}
	public String getFileDescription1() {
		return CommonUtility.replaceNull(fileDescription1);
	}
	public void setFileDescription1(String fileDescription1) {
		this.fileDescription1 = fileDescription1;
	}
	public FormFile getTheFile2() {
		return theFile2;
	}
	public void setTheFile2(FormFile theFile2) {
		this.theFile2 = theFile2;
	}
	public String getFileDescription2() {
		return CommonUtility.replaceNull(fileDescription2);
	}
	public void setFileDescription2(String fileDescription2) {
		this.fileDescription2 = fileDescription2;
	}
	public FormFile getTheFile3() {
		return theFile3;
	}
	public void setTheFile3(FormFile theFile3) {
		this.theFile3 = theFile3;
	}
	public String getFileDescription3() {
		return CommonUtility.replaceNull(fileDescription3);
	}
	public void setFileDescription3(String fileDescription3) {
		this.fileDescription3 = fileDescription3;
	}
	public FormFile getTheFile4() {
		return theFile4;
	}
	public void setTheFile4(FormFile theFile4) {
		this.theFile4 = theFile4;
	}
	public String getFileDescription4() {
		return CommonUtility.replaceNull(fileDescription4);
	}
	public void setFileDescription4(String fileDescription4) {
		this.fileDescription4 = fileDescription4;
	}
	public FormFile getTheFile5() {
		return theFile5;
	}
	public void setTheFile5(FormFile theFile5) {
		this.theFile5 = theFile5;
	}
	public String getFileDescription5() {
		return CommonUtility.replaceNull(fileDescription5);
	}
	public void setFileDescription5(String fileDescription5) {
		this.fileDescription5 = fileDescription5;
	}
	public String getCustomerType() {
		return CommonUtility.replaceNull(customerType);
	}
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	public String getEndUser() {
		return CommonUtility.replaceNull(endUser);
	}
	public void setEndUser(String endUser) {
		this.endUser = endUser;
	}
	public String getOem() {
		return CommonUtility.replaceNull(oem);
	}
	public void setOem(String oem) {
		this.oem = oem;
	}
	public String getEpc() {
		return CommonUtility.replaceNull(epc);
	}
	public void setEpc(String epc) {
		this.epc = epc;
	}
	public String getDealer() {
		return CommonUtility.replaceNull(dealer);
	}
	public void setDealer(String dealer) {
		this.dealer = dealer;
	}
	public String getProjectName() {
		return CommonUtility.replaceNull(projectName);
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getIndustry() {
		return CommonUtility.replaceNull(industry);
	}
	public void setIndustry(String industry) {
		this.industry = industry;
	}
	public String getEndUserCountry() {
		return CommonUtility.replaceNull(endUserCountry);
	}
	public void setEndUserCountry(String endUserCountry) {
		this.endUserCountry = endUserCountry;
	}
	public String getEndUserIndustry() {
		return CommonUtility.replaceNull(endUserIndustry);
	}
	public void setEndUserIndustry(String endUserIndustry) {
		this.endUserIndustry = endUserIndustry;
	}
	public String getApproval() {
		return CommonUtility.replaceNull(approval);
	}
	public void setApproval(String approval) {
		this.approval = approval;
	}
	public String getTargeted() {
		return CommonUtility.replaceNull(targeted);
	}
	public void setTargeted(String targeted) {
		this.targeted = targeted;
	}
	public String getWinningChance() {
		return CommonUtility.replaceNull(winningChance);
	}
	public void setWinningChance(String winningChance) {
		this.winningChance = winningChance;
	}
	public String getCompetitor() {
		return CommonUtility.replaceNull(competitor);
	}
	public void setCompetitor(String competitor) {
		this.competitor = competitor;
	}
	public String getSalesStage() {
		return CommonUtility.replaceNull(salesStage);
	}
	public void setSalesStage(String salesStage) {
		this.salesStage = salesStage;
	}
	public String getTotalOrderValue() {
		return totalOrderValue;
	}
	public void setTotalOrderValue(String totalOrderValue) {
		this.totalOrderValue = totalOrderValue;
	}
	public String getOrderClosingMonth() {
		return CommonUtility.replaceNull(orderClosingMonth);
	}
	public void setOrderClosingMonth(String orderClosingMonth) {
		this.orderClosingMonth = orderClosingMonth;
	}
	public String getExpectedDeliveryMonth() {
		return CommonUtility.replaceNull(expectedDeliveryMonth);
	}
	public void setExpectedDeliveryMonth(String expectedDeliveryMonth) {
		this.expectedDeliveryMonth = expectedDeliveryMonth;
	}
	public String getWarrantyDispatchDate() {
		return CommonUtility.replaceNull(warrantyDispatchDate);
	}
	public void setWarrantyDispatchDate(String warrantyDispatchDate) {
		this.warrantyDispatchDate = warrantyDispatchDate;
	}
	public String getWarrantyCommissioningDate() {
		return CommonUtility.replaceNull(warrantyCommissioningDate);
	}
	public void setWarrantyCommissioningDate(String warrantyCommissioningDate) {
		this.warrantyCommissioningDate = warrantyCommissioningDate;
	}
	public String getDeliveryType() {
		return CommonUtility.replaceNull(deliveryType);
	}
	public void setDeliveryType(String deliveryType) {
		this.deliveryType = deliveryType;
	}
	public String getDestState() {
		return CommonUtility.replaceNull(destState);
	}
	public void setDestState(String destState) {
		this.destState = destState;
	}
	public String getDestCity() {
		return CommonUtility.replaceNull(destCity);
	}
	public void setDestCity(String destCity) {
		this.destCity = destCity;
	}
	public String getSpa() {
		return CommonUtility.replaceNull(spa);
	}
	public void setSpa(String spa) {
		this.spa = spa;
	}
	public double getCustomerCounterOffer() {
		return customerCounterOffer;
	}
	public void setCustomerCounterOffer(double customerCounterOffer) {
		this.customerCounterOffer = customerCounterOffer;
	}
	public String getMaterialCost() {
		return CommonUtility.replaceNull(materialCost);
	}
	public void setMaterialCost(String materialCost) {
		this.materialCost = materialCost;
	}
	public String getMcnspRatio() {
		return CommonUtility.replaceNull(mcnspRatio);
	}
	public void setMcnspRatio(String mcnspRatio) {
		this.mcnspRatio = mcnspRatio;
	}
	public double getMotorPrice() {
		return motorPrice;
	}
	public void setMotorPrice(double motorPrice) {
		this.motorPrice = motorPrice;
	}
	public String getWarrantyCost() {
		return CommonUtility.replaceNull(warrantyCost);
	}
	public void setWarrantyCost(String warrantyCost) {
		this.warrantyCost = warrantyCost;
	}
	public String getTransportationCost() {
		return CommonUtility.replaceNull(transportationCost);
	}
	public void setTransportationCost(String transportationCost) {
		this.transportationCost = transportationCost;
	}
	public String getEcSupervisionCost() {
		return CommonUtility.replaceNull(ecSupervisionCost);
	}
	public void setEcSupervisionCost(String ecSupervisionCost) {
		this.ecSupervisionCost = ecSupervisionCost;
	}
	public String getHzAreaCertCost() {
		return CommonUtility.replaceNull(hzAreaCertCost);
	}
	public void setHzAreaCertCost(String hzAreaCertCost) {
		this.hzAreaCertCost = hzAreaCertCost;
	}
	public String getSparesCost() {
		return CommonUtility.replaceNull(sparesCost);
	}
	public void setSparesCost(String sparesCost) {
		this.sparesCost = sparesCost;
	}
	public double getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}
	public String getPropSpaReason() {
		return CommonUtility.replaceNull(propSpaReason);
	}
	public void setPropSpaReason(String propSpaReason) {
		this.propSpaReason = propSpaReason;
	}
	public String getWinlossReason() {
		return CommonUtility.replaceNull(winlossReason);
	}
	public void setWinlossReason(String winlossReason) {
		this.winlossReason = winlossReason;
	}
	public String getWinlossComment() {
		return CommonUtility.replaceNull(winlossComment);
	}
	public void setWinlossComment(String winlossComment) {
		this.winlossComment = winlossComment;
	}
	public String getWinningCompetitor() {
		return CommonUtility.replaceNull(winningCompetitor);
	}
	public void setWinningCompetitor(String winningCompetitor) {
		this.winningCompetitor = winningCompetitor;
	}
	public double getUnitMC() {
		return unitMC;
	}
	public void setUnitMC(double unitMC) {
		this.unitMC = unitMC;
	}
	public double getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}
	public String getWinlossPrice() {
		return CommonUtility.replaceNull(winlossPrice);
	}
	public void setWinlossPrice(String winlossPrice) {
		this.winlossPrice = winlossPrice;
	}
	public double getTotalMC() {
		return totalMC;
	}
	public void setTotalMC(double totalMC) {
		this.totalMC = totalMC;
	}
	public String getTotalPrice() {
		return CommonUtility.replaceNull(totalPrice);
	}
	public void setTotalPrice(String totalPrice) {
		this.totalPrice = totalPrice;
	}
	public String getTotalPkgPrice() {
		return CommonUtility.replaceNull(totalPkgPrice);
	}
	public void setTotalPkgPrice(String totalPkgPrice) {
		this.totalPkgPrice = totalPkgPrice;
	}
	public ArrayList getAlCustomerTypeList() {
		return alCustomerTypeList;
	}
	public void setAlCustomerTypeList(ArrayList alCustomerTypeList) {
		this.alCustomerTypeList = alCustomerTypeList;
	}
	public ArrayList getAlIndustryList() {
		return alIndustryList;
	}
	public void setAlIndustryList(ArrayList alIndustryList) {
		this.alIndustryList = alIndustryList;
	}
	public ArrayList getAlApprovalList() {
		return alApprovalList;
	}
	public void setAlApprovalList(ArrayList alApprovalList) {
		this.alApprovalList = alApprovalList;
	}
	public ArrayList getAlTargetedList() {
		return alTargetedList;
	}
	public void setAlTargetedList(ArrayList alTargetedList) {
		this.alTargetedList = alTargetedList;
	}
	public ArrayList getAlWinningChanceList() {
		return alWinningChanceList;
	}
	public void setAlWinningChanceList(ArrayList alWinningChanceList) {
		this.alWinningChanceList = alWinningChanceList;
	}
	public ArrayList getAlCompetitorList() {
		return alCompetitorList;
	}
	public void setAlCompetitorList(ArrayList alCompetitorList) {
		this.alCompetitorList = alCompetitorList;
	}
	public ArrayList getAlEnquiryTypeList() {
		return alEnquiryTypeList;
	}
	public void setAlEnquiryTypeList(ArrayList alEnquiryTypeList) {
		this.alEnquiryTypeList = alEnquiryTypeList;
	}
	public ArrayList getAlSalesStageList() {
		return alSalesStageList;
	}
	public void setAlSalesStageList(ArrayList alSalesStageList) {
		this.alSalesStageList = alSalesStageList;
	}
	public ArrayList getAlDeliveryTypeList() {
		return alDeliveryTypeList;
	}
	public void setAlDeliveryTypeList(ArrayList alDeliveryTypeList) {
		this.alDeliveryTypeList = alDeliveryTypeList;
	}
	public ArrayList getAlSPAList() {
		return alSPAList;
	}
	public void setAlSPAList(ArrayList alSPAList) {
		this.alSPAList = alSPAList;
	}
	public ArrayList getAlWinlossReasonList() {
		return alWinlossReasonList;
	}
	public void setAlWinlossReasonList(ArrayList alWinlossReasonList) {
		this.alWinlossReasonList = alWinlossReasonList;
	}
	public ArrayList getAlWinCompetitorList() {
		return alWinCompetitorList;
	}
	public void setAlWinCompetitorList(ArrayList alWinCompetitorList) {
		this.alWinCompetitorList = alWinCompetitorList;
	}
	public String getRequestRemarks() {
		return CommonUtility.replaceNull(requestRemarks);
	}
	public void setRequestRemarks(String requestRemarks) {
		this.requestRemarks = requestRemarks;
	}
	
	public String getEnquiryReferenceNumber() {
		return CommonUtility.replaceNull(enquiryReferenceNumber);
	}
	public void setEnquiryReferenceNumber(String enquiryReferenceNumber) {
		this.enquiryReferenceNumber = enquiryReferenceNumber;
	}
	public String getCrossReference() {
		return CommonUtility.replaceNull(crossReference);
	}
	public void setCrossReference(String crossReference) {
		this.crossReference = crossReference;
	}
	public ArrayList getAlVoltList() {
		return alVoltList;
	}
	public void setAlVoltList(ArrayList alVoltList) {
		this.alVoltList = alVoltList;
	}
	public ArrayList getAlWinChanceList() {
		return alWinChanceList;
	}
	public void setAlWinChanceList(ArrayList alWinChanceList) {
		this.alWinChanceList = alWinChanceList;
	}
	
	public String getRatingUnitPrice() {
		return CommonUtility.replaceNull(ratingUnitPrice);
	}
	public void setRatingUnitPrice(String ratingUnitPrice) {
		this.ratingUnitPrice = ratingUnitPrice;
	}
	public String getAreaClassification() {
		return CommonUtility.replaceNull( areaClassification);
	}
	public void setAreaClassification(String areaClassification) {
		this.areaClassification = areaClassification;
	}
	public String getServiceFactor() {
		return CommonUtility.replaceNull( serviceFactor);
	}
	public void setServiceFactor(String serviceFactor) {
		this.serviceFactor = serviceFactor;
	}
	public String getBkw() {
		return CommonUtility.replaceNull( bkw);
	}
	public void setBkw(String bkw) {
		this.bkw = bkw;
	}
	public String getNlCurrent() {
		return CommonUtility.replaceNull( nlCurrent);
	}
	public void setNlCurrent(String nlCurrent) {
		this.nlCurrent = nlCurrent;
	}
	public String getStrtgCurrent() {
		return CommonUtility.replaceNull( strtgCurrent);
	}
	public void setStrtgCurrent(String strtgCurrent) {
		this.strtgCurrent = strtgCurrent;
	}
	public String getLrTorque() {
		return CommonUtility.replaceNull( lrTorque);
	}
	public void setLrTorque(String lrTorque) {
		this.lrTorque = lrTorque;
	}
	public String getPllEff1() {
		return CommonUtility.replaceNull( pllEff1);
	}
	public void setPllEff1(String pllEff1) {
		this.pllEff1 = pllEff1;
	}
	public String getPllEff2() {
		return CommonUtility.replaceNull( pllEff2);
	}
	public void setPllEff2(String pllEff2) {
		this.pllEff2 = pllEff2;
	}
	public String getPllEff3() {
		return CommonUtility.replaceNull( pllEff3);
	}
	public void setPllEff3(String pllEff3) {
		this.pllEff3 = pllEff3;
	}
	public String getSstHot() {
		return CommonUtility.replaceNull( sstHot);
	}
	public void setSstHot(String sstHot) {
		this.sstHot = sstHot;
	}
	public String getSstCold() {
		return CommonUtility.replaceNull( sstCold);
	}
	public void setSstCold(String sstCold) {
		this.sstCold = sstCold;
	}
	public String getStartTimeRV() {
		return CommonUtility.replaceNull( startTimeRV);
	}
	public void setStartTimeRV(String startTimeRV) {
		this.startTimeRV = startTimeRV;
	}
	public String getStartTimeRV80() {
		return CommonUtility.replaceNull( startTimeRV80);
	}
	public void setStartTimeRV80(String startTimeRV80) {
		this.startTimeRV80 = startTimeRV80;
	}
	public String getCoolingSystem() {
		return CommonUtility.replaceNull( coolingSystem);
	}
	public void setCoolingSystem(String coolingSystem) {
		this.coolingSystem = coolingSystem;
	}
	public String getBearingDENDE() {
		return CommonUtility.replaceNull( bearingDENDE);
	}
	public void setBearingDENDE(String bearingDENDE) {
		this.bearingDENDE = bearingDENDE;
	}
	public String getLubrication() {
		return CommonUtility.replaceNull( lubrication);
	}
	public void setLubrication(String lubrication) {
		this.lubrication = lubrication;
	}
	public String getMigD2Load() {
		return CommonUtility.replaceNull( migD2Load);
	}
	public void setMigD2Load(String migD2Load) {
		this.migD2Load = migD2Load;
	}
	public String getMigD2Motor() {
		return CommonUtility.replaceNull( migD2Motor);
	}
	public void setMigD2Motor(String migD2Motor) {
		this.migD2Motor = migD2Motor;
	}
	public String getMainTermBoxPS() {
		return CommonUtility.replaceNull( mainTermBoxPS);
	}
	public void setMainTermBoxPS(String mainTermBoxPS) {
		this.mainTermBoxPS = mainTermBoxPS;
	}
	public String getNeutralTermBox() {
		return CommonUtility.replaceNull( neutralTermBox);
	}
	public void setNeutralTermBox(String neutralTermBox) {
		this.neutralTermBox = neutralTermBox;
	}
	public String getCableEntry() {
		return CommonUtility.replaceNull( cableEntry);
	}
	public void setCableEntry(String cableEntry) {
		this.cableEntry = cableEntry;
	}
	public String getAuxTermBox() {
		return CommonUtility.replaceNull( auxTermBox);
	}
	public void setAuxTermBox(String auxTermBox) {
		this.auxTermBox = auxTermBox;
	}
	public String getStatorConn() {
		return CommonUtility.replaceNull( statorConn);
	}
	public void setStatorConn(String statorConn) {
		this.statorConn = statorConn;
	}
	public String getNoBoTerminals() {
		return CommonUtility.replaceNull( noBoTerminals);
	}
	public void setNoBoTerminals(String noBoTerminals) {
		this.noBoTerminals = noBoTerminals;
	}
	public String getRotation() {
		return CommonUtility.replaceNull( rotation);
	}
	public void setRotation(String rotation) {
		this.rotation = rotation;
	}
	public String getMotorTotalWgt() {
		return CommonUtility.replaceNull( motorTotalWgt);
	}
	public void setMotorTotalWgt(String motorTotalWgt) {
		this.motorTotalWgt = motorTotalWgt;
	}
	public String getSpaceHeater() {
		return CommonUtility.replaceNull( spaceHeater);
	}
	public void setSpaceHeater(String spaceHeater) {
		this.spaceHeater = spaceHeater;
	}
	public String getWindingRTD() {
		return CommonUtility.replaceNull( windingRTD);
	}
	public void setWindingRTD(String windingRTD) {
		this.windingRTD = windingRTD;
	}
	public String getBearingRTD() {
		return CommonUtility.replaceNull( bearingRTD);
	}
	public void setBearingRTD(String bearingRTD) {
		this.bearingRTD = bearingRTD;
	}
	public String getMinStartVolt() {
		return CommonUtility.replaceNull( minStartVolt);
	}
	public void setMinStartVolt(String minStartVolt) {
		this.minStartVolt = minStartVolt;
	}
	public String getBearingThermoDT() {
		return CommonUtility.replaceNull( bearingThermoDT);
	}
	public void setBearingThermoDT(String bearingThermoDT) {
		this.bearingThermoDT = bearingThermoDT;
	}
	public String getAirThermo() {
		return CommonUtility.replaceNull( airThermo);
	}
	public void setAirThermo(String airThermo) {
		this.airThermo = airThermo;
	}
	public String getVibeProbMPads() {
		return CommonUtility.replaceNull( vibeProbMPads);
	}
	public void setVibeProbMPads(String vibeProbMPads) {
		this.vibeProbMPads = vibeProbMPads;
	}
	public String getEncoder() {
		return CommonUtility.replaceNull( encoder);
	}
	public void setEncoder(String encoder) {
		this.encoder = encoder;
	}
	public String getSpeedSwitch() {
		return CommonUtility.replaceNull( speedSwitch);
	}
	public void setSpeedSwitch(String speedSwitch) {
		this.speedSwitch = speedSwitch;
	}
	public String getSurgeSuppressors() {
		return CommonUtility.replaceNull( surgeSuppressors);
	}
	public void setSurgeSuppressors(String surgeSuppressors) {
		this.surgeSuppressors = surgeSuppressors;
	}
	public String getCurrTransformers() {
		return CommonUtility.replaceNull( currTransformers);
	}
	public void setCurrTransformers(String currTransformers) {
		this.currTransformers = currTransformers;
	}
	public String getVibProbes() {
		return CommonUtility.replaceNull( vibProbes);
	}
	public void setVibProbes(String vibProbes) {
		this.vibProbes = vibProbes;
	}
	public String getSpmNipple() {
		return CommonUtility.replaceNull( spmNipple);
	}
	public void setSpmNipple(String spmNipple) {
		this.spmNipple = spmNipple;
	}
	public String getKeyPhasor() {
		return CommonUtility.replaceNull( keyPhasor);
	}
	public void setKeyPhasor(String keyPhasor) {
		this.keyPhasor = keyPhasor;
	}
	
	public String getBalancing() {
		return CommonUtility.replaceNull(balancing);
	}
	public void setBalancing(String balancing) {
		this.balancing = balancing;
	}
	public String getSavingIndent() {
		return CommonUtility.replaceNull(savingIndent);
	}
	public void setSavingIndent(String savingIndent) {
		this.savingIndent = savingIndent;
	}
	public String getPllCurr1() {
		return CommonUtility.replaceNull(pllCurr1);
	}
	public void setPllCurr1(String pllCurr1) {
		this.pllCurr1 = pllCurr1;
	}
	public String getPllCurr2() {
		return CommonUtility.replaceNull(pllCurr2);
	}
	public void setPllCurr2(String pllCurr2) {
		this.pllCurr2 = pllCurr2;
	}
	public String getPllCurr3() {
		return CommonUtility.replaceNull(pllCurr3);
	}
	public void setPllCurr3(String pllCurr3) {
		this.pllCurr3 = pllCurr3;
	}
	public String getPllCurr4() {
		return CommonUtility.replaceNull(pllCurr4);
	}
	public void setPllCurr4(String pllCurr4) {
		this.pllCurr4 = pllCurr4;
	}
	public String getPllPf1() {
		return CommonUtility.replaceNull(pllPf1);
	}
	public void setPllPf1(String pllPf1) {
		this.pllPf1 = pllPf1;
	}
	public String getPllPf2() {
		return CommonUtility.replaceNull(pllPf2);
	}
	public void setPllPf2(String pllPf2) {
		this.pllPf2 = pllPf2;
	}
	public String getPllpf3() {
		return CommonUtility.replaceNull(pllpf3);
	}
	public void setPllpf3(String pllpf3) {
		this.pllpf3 = pllpf3;
	}
	public String getPllpf4() {
		return CommonUtility.replaceNull(pllpf4);
	}
	public void setPllpf4(String pllpf4) {
		this.pllpf4 = pllpf4;
	}
	public String getPaintColor() {
		return CommonUtility.replaceNull(paintColor);
	}
	public void setPaintColor(String paintColor) {
		this.paintColor = paintColor;
	}
	public ArrayList getAlAreaClassificationList() {
		return alAreaClassificationList;
	}
	public void setAlAreaClassificationList(ArrayList alAreaClassificationList) {
		this.alAreaClassificationList = alAreaClassificationList;
	}
	public ArrayList getAlServiceFactorList() {
		return alServiceFactorList;
	}
	public void setAlServiceFactorList(ArrayList alServiceFactorList) {
		this.alServiceFactorList = alServiceFactorList;
	}
	public ArrayList getAlFrequencyList() {
		return alFrequencyList;
	}
	public void setAlFrequencyList(ArrayList alFrequencyList) {
		this.alFrequencyList = alFrequencyList;
	}
	public ArrayList getAlStrtgCurrentList() {
		return alStrtgCurrentList;
	}
	public void setAlStrtgCurrentList(ArrayList alStrtgCurrentList) {
		this.alStrtgCurrentList = alStrtgCurrentList;
	}
	public ArrayList getAlAmbienceList() {
		return alAmbienceList;
	}
	public void setAlAmbienceList(ArrayList alAmbienceList) {
		this.alAmbienceList = alAmbienceList;
	}
	public ArrayList getAlClgSystemList() {
		return alClgSystemList;
	}
	public void setAlClgSystemList(ArrayList alClgSystemList) {
		this.alClgSystemList = alClgSystemList;
	}
	public ArrayList getAlBearingDENDEList() {
		return alBearingDENDEList;
	}
	public void setAlBearingDENDEList(ArrayList alBearingDENDEList) {
		this.alBearingDENDEList = alBearingDENDEList;
	}
	public ArrayList getAlLubricationList() {
		return alLubricationList;
	}
	public void setAlLubricationList(ArrayList alLubricationList) {
		this.alLubricationList = alLubricationList;
	}
	public ArrayList getAlBalancingList() {
		return alBalancingList;
	}
	public void setAlBalancingList(ArrayList alBalancingList) {
		this.alBalancingList = alBalancingList;
	}
	public ArrayList getAlVibrationList() {
		return alVibrationList;
	}
	public void setAlVibrationList(ArrayList alVibrationList) {
		this.alVibrationList = alVibrationList;
	}
	public ArrayList getAlMainTermBoxPSList() {
		return alMainTermBoxPSList;
	}
	public void setAlMainTermBoxPSList(ArrayList alMainTermBoxPSList) {
		this.alMainTermBoxPSList = alMainTermBoxPSList;
	}
	public ArrayList getAlNeutralTBList() {
		return alNeutralTBList;
	}
	public void setAlNeutralTBList(ArrayList alNeutralTBList) {
		this.alNeutralTBList = alNeutralTBList;
	}
	public ArrayList getAlCableEntryList() {
		return alCableEntryList;
	}
	public void setAlCableEntryList(ArrayList alCableEntryList) {
		this.alCableEntryList = alCableEntryList;
	}
	public ArrayList getAlAuxTermBoxList() {
		return alAuxTermBoxList;
	}
	public void setAlAuxTermBoxList(ArrayList alAuxTermBoxList) {
		this.alAuxTermBoxList = alAuxTermBoxList;
	}
	public ArrayList getAlStatorConnList() {
		return alStatorConnList;
	}
	public void setAlStatorConnList(ArrayList alStatorConnList) {
		this.alStatorConnList = alStatorConnList;
	}
	public ArrayList getAlBoTerminalsNoList() {
		return alBoTerminalsNoList;
	}
	public void setAlBoTerminalsNoList(ArrayList alBoTerminalsNoList) {
		this.alBoTerminalsNoList = alBoTerminalsNoList;
	}
	public ArrayList getAlRotationList() {
		return alRotationList;
	}
	public void setAlRotationList(ArrayList alRotationList) {
		this.alRotationList = alRotationList;
	}
	public ArrayList getAlSpaceHeaterList() {
		return alSpaceHeaterList;
	}
	public void setAlSpaceHeaterList(ArrayList alSpaceHeaterList) {
		this.alSpaceHeaterList = alSpaceHeaterList;
	}
	public ArrayList getAlWindingRTDList() {
		return alWindingRTDList;
	}
	public void setAlWindingRTDList(ArrayList alWindingRTDList) {
		this.alWindingRTDList = alWindingRTDList;
	}
	public ArrayList getAlBearingRTDList() {
		return alBearingRTDList;
	}
	public void setAlBearingRTDList(ArrayList alBearingRTDList) {
		this.alBearingRTDList = alBearingRTDList;
	}
	public ArrayList getAlMinStartVoltList() {
		return alMinStartVoltList;
	}
	public void setAlMinStartVoltList(ArrayList alMinStartVoltList) {
		this.alMinStartVoltList = alMinStartVoltList;
	}
	public ArrayList getAlMethodOfStgList() {
		return alMethodOfStgList;
	}
	public void setAlMethodOfStgList(ArrayList alMethodOfStgList) {
		this.alMethodOfStgList = alMethodOfStgList;
	}
	public ArrayList getAlBearingThermoDtList() {
		return alBearingThermoDtList;
	}
	public void setAlBearingThermoDtList(ArrayList alBearingThermoDtList) {
		this.alBearingThermoDtList = alBearingThermoDtList;
	}
	public ArrayList getAlYesNoList() {
		return alYesNoList;
	}
	public void setAlYesNoList(ArrayList alYesNoList) {
		this.alYesNoList = alYesNoList;
	}
	
	public String getRevision() {
		return CommonUtility.replaceNull(revision);
	}
	public void setRevision(String revision) {
		this.revision = revision;
	}
	public String getLastModifiedDate() {
		return CommonUtility.replaceNull(lastModifiedDate);
	}
	public void setLastModifiedDate(String lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	public ArrayList getAlTempRaiseList() {
		return alTempRaiseList;
	}
	public void setAlTempRaiseList(ArrayList alTempRaiseList) {
		this.alTempRaiseList = alTempRaiseList;
	}
	public String getDeptAndAuth() {
		return CommonUtility.replaceNull(deptAndAuth);
	}
	public void setDeptAndAuth(String deptAndAuth) {
		this.deptAndAuth = deptAndAuth;
	}
	public String getRatedOutputUnit() {
		return CommonUtility.replaceNull(ratedOutputUnit);
	}
	public void setRatedOutputUnit(String ratedOutputUnit) {
		this.ratedOutputUnit = ratedOutputUnit;
	}
	public String getAdvancePayment() {
		return CommonUtility.replaceNull(advancePayment);
	}
	public void setAdvancePayment(String advancePayment) {
		this.advancePayment = advancePayment;
	}
	public String getPaymentTerms() {
		return CommonUtility.replaceNull(paymentTerms);
	}
	public void setPaymentTerms(String paymentTerms) {
		this.paymentTerms = paymentTerms;
	}
	public String getCustomerNameMultiplier() {
		return CommonUtility.replaceNull(customerNameMultiplier);
	}
	public void setCustomerNameMultiplier(String customerNameMultiplier) {
		this.customerNameMultiplier = customerNameMultiplier;
	}
	public String getCustomerTypeMultiplier() {
		return CommonUtility.replaceNull(customerTypeMultiplier);
	}
	public void setCustomerTypeMultiplier(String customerTypeMultiplier) {
		this.customerTypeMultiplier = customerTypeMultiplier;
	}
	public String getEndUserMultiplier() {
		return CommonUtility.replaceNull(endUserMultiplier);
	}
	public void setEndUserMultiplier(String endUserMultiplier) {
		this.endUserMultiplier = endUserMultiplier;
	}
	public String getWarrantyDispatchMultiplier() {
		return CommonUtility.replaceNull(warrantyDispatchMultiplier);
	}
	public void setWarrantyDispatchMultiplier(String warrantyDispatchMultiplier) {
		this.warrantyDispatchMultiplier = warrantyDispatchMultiplier;
	}
	public String getWarrantycommissioningMultiplier() {
		return CommonUtility.replaceNull(warrantycommissioningMultiplier);
	}
	public void setWarrantycommissioningMultiplier(
			String warrantycommissioningMultiplier) {
		this.warrantycommissioningMultiplier = warrantycommissioningMultiplier;
	}
	public String getAdvancePaymMultiplier() {
		return CommonUtility.replaceNull(advancePaymMultiplier);
	}
	public void setAdvancePaymMultiplier(String advancePaymMultiplier) {
		this.advancePaymMultiplier = advancePaymMultiplier;
	}
	public String getPayTermMultiplier() {
		return CommonUtility.replaceNull(payTermMultiplier);
	}
	public void setPayTermMultiplier(String payTermMultiplier) {
		this.payTermMultiplier = payTermMultiplier;
	}
	public String getExpDeliveryMultiplier() {
		return CommonUtility.replaceNull(expDeliveryMultiplier);
	}
	public void setExpDeliveryMultiplier(String expDeliveryMultiplier) {
		this.expDeliveryMultiplier = expDeliveryMultiplier;
	}
	public String getFinanceMultiplier() {
		return CommonUtility.replaceNull(financeMultiplier);
	}
	public void setFinanceMultiplier(String financeMultiplier) {
		this.financeMultiplier = financeMultiplier;
	}
	public String getQuantityMultiplier() {
		return CommonUtility.replaceNull(quantityMultiplier);
	}
	public void setQuantityMultiplier(String quantityMultiplier) {
		this.quantityMultiplier = quantityMultiplier;
	}
	public String getRatedOutputPNMulitplier() {
		return CommonUtility.replaceNull(ratedOutputPNMulitplier);
	}
	public void setRatedOutputPNMulitplier(String ratedOutputPNMulitplier) {
		this.ratedOutputPNMulitplier = ratedOutputPNMulitplier;
	}
	public String getTypeMultiplier() {
		return CommonUtility.replaceNull(typeMultiplier);
	}
	public void setTypeMultiplier(String typeMultiplier) {
		this.typeMultiplier = typeMultiplier;
	}
	public String getAreaCMultiplier() {
		return CommonUtility.replaceNull(areaCMultiplier);
	}
	public void setAreaCMultiplier(String areaCMultiplier) {
		this.areaCMultiplier = areaCMultiplier;
	}
	public String getFrameMultiplier() {
		return CommonUtility.replaceNull(frameMultiplier);
	}
	public void setFrameMultiplier(String frameMultiplier) {
		this.frameMultiplier = frameMultiplier;
	}
	public String getApplicationMultiplier() {
		return CommonUtility.replaceNull(applicationMultiplier);
	}
	public void setApplicationMultiplier(String applicationMultiplier) {
		this.applicationMultiplier = applicationMultiplier;
	}
	public String getFactorMultiplier() {
		return CommonUtility.replaceNull(factorMultiplier);
	}
	public void setFactorMultiplier(String factorMultiplier) {
		this.factorMultiplier = factorMultiplier;
	}
	public String getNetmcMultiplier() {
		return CommonUtility.replaceNull(netmcMultiplier);
	}
	public void setNetmcMultiplier(String netmcMultiplier) {
		this.netmcMultiplier = netmcMultiplier;
	}
	public String getMarkupMultiplier() {
		return CommonUtility.replaceNull(markupMultiplier);
	}
	public void setMarkupMultiplier(String markupMultiplier) {
		this.markupMultiplier = markupMultiplier;
	}
	public String getTransferCMultiplier() {
		return CommonUtility.replaceNull(transferCMultiplier);
	}
	public void setTransferCMultiplier(String transferCMultiplier) {
		this.transferCMultiplier = transferCMultiplier;
	}
	
	public String getMcornspratioMultiplier_rsm() {
		return CommonUtility.replaceNull(mcornspratioMultiplier_rsm);
	}
	public void setMcornspratioMultiplier_rsm(String mcornspratioMultiplier_rsm) {
		this.mcornspratioMultiplier_rsm = mcornspratioMultiplier_rsm;
	}
	public String getUnitpriceMultiplier_rsm() {
		return CommonUtility.replaceNull(unitpriceMultiplier_rsm);
	}
	public void setUnitpriceMultiplier_rsm(String unitpriceMultiplier_rsm) {
		this.unitpriceMultiplier_rsm = unitpriceMultiplier_rsm;
	}
	public String getTotalpriceMultiplier_rsm() {
		return CommonUtility.replaceNull(totalpriceMultiplier_rsm);
	}
	public void setTotalpriceMultiplier_rsm(String totalpriceMultiplier_rsm) {
		this.totalpriceMultiplier_rsm = totalpriceMultiplier_rsm;
	}
	public String getWarrantyperunit_rsm() {
		return CommonUtility.replaceNull(warrantyperunit_rsm);
	}
	public void setWarrantyperunit_rsm(String warrantyperunit_rsm) {
		this.warrantyperunit_rsm = warrantyperunit_rsm;
	}
	public String getTransportationperunit_rsm() {
		return CommonUtility.replaceNull(transportationperunit_rsm);
	}
	public void setTransportationperunit_rsm(String transportationperunit_rsm) {
		this.transportationperunit_rsm = transportationperunit_rsm;
	}
	public String getSmnote() {
		return CommonUtility.replaceNull(smnote);
	}
	public void setSmnote(String smnote) {
		this.smnote = smnote;
	}
	public String getDmnote() {
		return CommonUtility.replaceNull(dmnote);
	}
	public void setDmnote(String dmnote) {
		this.dmnote = dmnote;
	}
	public String getCenote() {
		return CommonUtility.replaceNull(cenote);
	}
	public void setCenote(String cenote) {
		this.cenote = cenote;
	}
	public String getRsmnote() {
		return CommonUtility.replaceNull(rsmnote);
	}
	public void setRsmnote(String rsmnote) {
		this.rsmnote = rsmnote;
	}
	public String getSm_name() {
		return CommonUtility.replaceNull(sm_name);
	}
	public void setSm_name(String sm_name) {
		this.sm_name = sm_name;
	}
	public String getDm_ssoid() {
		return CommonUtility.replaceNull(dm_ssoid);
	}
	public void setDm_ssoid(String dm_ssoid) {
		this.dm_ssoid = dm_ssoid;
	}
	public String getDm_name() {
		return CommonUtility.replaceNull(dm_name);
	}
	public void setDm_name(String dm_name) {
		this.dm_name = dm_name;
	}
	public String getCe_ssoid() {
		return CommonUtility.replaceNull(ce_ssoid);
	}
	public void setCe_ssoid(String ce_ssoid) {
		this.ce_ssoid = ce_ssoid;
	}
	public String getCe_name() {
		return CommonUtility.replaceNull(ce_name);
	}
	public void setCe_name(String ce_name) {
		this.ce_name = ce_name;
	}
	public String getRsm_ssoid() {
		return CommonUtility.replaceNull(rsm_ssoid);
	}
	public void setRsm_ssoid(String rsm_ssoid) {
		this.rsm_ssoid = rsm_ssoid;
	}
	public String getRsm_name() {
		return CommonUtility.replaceNull(rsm_name);
	}
	public void setRsm_name(String rsm_name) {
		this.rsm_name = rsm_name;
	}

	public String getPagechecking() {
		return CommonUtility.replaceNull(pagechecking);
	}
	public void setPagechecking(String pagechecking) {
		this.pagechecking = pagechecking;
	}
	public String getReturnstatement() {
		return CommonUtility.replaceNull(returnstatement);
	}
	public void setReturnstatement(String returnstatement) {
		this.returnstatement = returnstatement;
	}

	public String getReturnstatus() {
		return CommonUtility.replaceNull(returnstatus);
	}
	public void setReturnstatus(String returnstatus) {
		this.returnstatus = returnstatus;
	}
	public ArrayList getAlnoiseLevelList() {
		return alnoiseLevelList;
	}
	public void setAlnoiseLevelList(ArrayList alnoiseLevelList) {
		this.alnoiseLevelList = alnoiseLevelList;
	}
	public String getDeviation_clarification() {
		return CommonUtility.replaceNull(deviation_clarification);
	}
	public void setDeviation_clarification(String deviation_clarification) {
		this.deviation_clarification = deviation_clarification;
	}
	public ArrayList getAlZoneList() {
		return alZoneList;
	}
	public void setAlZoneList(ArrayList alZoneList) {
		this.alZoneList = alZoneList;
	}
	public ArrayList getAlGasGroupList() {
		return alGasGroupList;
	}
	public void setAlGasGroupList(ArrayList alGasGroupList) {
		this.alGasGroupList = alGasGroupList;
	}
	public String getZone() {
		return CommonUtility.replaceNull(zone);
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	
	public String getDesignEngineerName() {
		return CommonUtility.replaceNull(designEngineerName);
	}
	public void setDesignEngineerName(String designEngineerName) {
		this.designEngineerName = designEngineerName;
	}
	public ArrayList getPreviousratingList() {
		return previousratingList;
	}
	public void setPreviousratingList(ArrayList previousratingList) {
		this.previousratingList = previousratingList;
	}
	public String getDmToSmReturn() {
		return CommonUtility.replaceNull(dmToSmReturn);
	}
	public void setDmToSmReturn(String dmToSmReturn) {
		this.dmToSmReturn = dmToSmReturn;
	}
	public String getCeToDmReturn() {
		return CommonUtility.replaceNull(ceToDmReturn);
	}
	public void setCeToDmReturn(String ceToDmReturn) {
		this.ceToDmReturn = ceToDmReturn;
	}
	public String getCeToSmReturn() {
		return CommonUtility.replaceNull(ceToSmReturn);
	}
	public void setCeToSmReturn(String ceToSmReturn) {
		this.ceToSmReturn = ceToSmReturn;
	}
	public String getRsmToCeReturn() {
		return CommonUtility.replaceNull(rsmToCeReturn);
	}
	public void setRsmToCeReturn(String rsmToCeReturn) {
		this.rsmToCeReturn = rsmToCeReturn;
	}
	public String getRsmToDmReturn() {
		return CommonUtility.replaceNull(rsmToDmReturn);
	}
	public void setRsmToDmReturn(String rsmToDmReturn) {
		this.rsmToDmReturn = rsmToDmReturn;
	}
	public String getRsmToSmReturn() {
		return CommonUtility.replaceNull(rsmToSmReturn);
	}
	public void setRsmToSmReturn(String rsmToSmReturn) {
		this.rsmToSmReturn = rsmToSmReturn;
	}
	public String getCommentOperation() {
		return CommonUtility.replaceNull(commentOperation);
	}
	public void setCommentOperation(String commentOperation) {
		this.commentOperation = commentOperation;
	}
	public String getGasGroup() {
		return CommonUtility.replaceNull(gasGroup);
	}
	public void setGasGroup(String gasGroup) {
		this.gasGroup = gasGroup;
	}
	public String getReturnOperation() {
		return CommonUtility.replaceNull(returnOperation);
	}
	public void setReturnOperation(String returnOperation) {
		this.returnOperation = returnOperation;
	}
	public String getDeToSmReturn() {
		return CommonUtility.replaceNull(deToSmReturn);
	}
	public void setDeToSmReturn(String deToSmReturn) {
		this.deToSmReturn = deToSmReturn;
	}
	public String getQuotationtoRsmReturn() {
		return CommonUtility.replaceNull(quotationtoRsmReturn);
	}
	public void setQuotationtoRsmReturn(String quotationtoRsmReturn) {
		this.quotationtoRsmReturn = quotationtoRsmReturn;
	}
	public String getQuotationtoCeReturn() {
		return CommonUtility.replaceNull(quotationtoCeReturn);
	}
	public void setQuotationtoCeReturn(String quotationtoCeReturn) {
		this.quotationtoCeReturn = quotationtoCeReturn;
	}
	public String getQuotationtoDmReturn() {
		return CommonUtility.replaceNull(quotationtoDmReturn);
	}
	public void setQuotationtoDmReturn(String quotationtoDmReturn) {
		this.quotationtoDmReturn = quotationtoDmReturn;
	}
	public ArrayList getSalesManagerList() {
		return salesManagerList;
	}
	public void setSalesManagerList(ArrayList salesManagerList) {
		this.salesManagerList = salesManagerList;
	}
	public String getUploadedAttachmentId() {
		return CommonUtility.replaceNull(uploadedAttachmentId);
	}
	public void setUploadedAttachmentId(String uploadedAttachmentId) {
		this.uploadedAttachmentId = uploadedAttachmentId;
	}
	public String getFileName() {
		return CommonUtility.replaceNull(fileName);
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFileExtension() {
		return CommonUtility.replaceNull(fileExtension);
	}
	public void setFileExtension(String fileExtension) {
		this.fileExtension = fileExtension;
	}
	public String getOrderNumber() {
		return CommonUtility.replaceNull(orderNumber);
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getPoiCopy() {
		return CommonUtility.replaceNull(poiCopy);
	}
	public void setPoiCopy(String poiCopy) {
		this.poiCopy = poiCopy;
	}
	public String getLoiCopy() {
		return CommonUtility.replaceNull(loiCopy);
	}
	public void setLoiCopy(String loiCopy) {
		this.loiCopy = loiCopy;
	}
	public String getDeliveryReqByCust() {
		return CommonUtility.replaceNull(deliveryReqByCust);
	}
	public void setDeliveryReqByCust(String deliveryReqByCust) {
		this.deliveryReqByCust = deliveryReqByCust;
	}
	public String getLd() {
		return CommonUtility.replaceNull(ld);
	}
	public void setLd(String ld) {
		this.ld = ld;
	}
	public String getDrawingApproval() {
		return CommonUtility.replaceNull(drawingApproval);
	}
	public void setDrawingApproval(String drawingApproval) {
		this.drawingApproval = drawingApproval;
	}
	public String getQapApproval() {
		return CommonUtility.replaceNull(qapApproval);
	}
	public void setQapApproval(String qapApproval) {
		this.qapApproval = qapApproval;
	}
	public String getTypeTest() {
		return CommonUtility.replaceNull(typeTest);
	}
	public void setTypeTest(String typeTest) {
		this.typeTest = typeTest;
	}
	public String getRoutineTest() {
		return CommonUtility.replaceNull(routineTest);
	}
	public void setRoutineTest(String routineTest) {
		this.routineTest = routineTest;
	}
	public String getSpecialTest() {
		return CommonUtility.replaceNull(specialTest);
	}
	public void setSpecialTest(String specialTest) {
		this.specialTest = specialTest;
	}
	public String getThirdPartyInspection() {
		return CommonUtility.replaceNull(thirdPartyInspection);
	}
	public void setThirdPartyInspection(String thirdPartyInspection) {
		this.thirdPartyInspection = thirdPartyInspection;
	}
	public String getStageInspection() {
		return CommonUtility.replaceNull(stageInspection);
	}
	public void setStageInspection(String stageInspection) {
		this.stageInspection = stageInspection;
	}
	public String getSpecialTestDetails() {
		return CommonUtility.replaceNull(specialTestDetails);
	}
	public void setSpecialTestDetails(String specialTestDetails) {
		this.specialTestDetails = specialTestDetails;
	}
	public String getEnduserLocation() {
		return CommonUtility.replaceNull(enduserLocation);
	}
	public void setEnduserLocation(String enduserLocation) {
		this.enduserLocation = enduserLocation;
	}
	public String getEpcName() {
		return CommonUtility.replaceNull(epcName);
	}
	public void setEpcName(String epcName) {
		this.epcName = epcName;
	}
	public String getPacking() {
		return CommonUtility.replaceNull(packing);
	}
	public void setPacking(String packing) {
		this.packing = packing;
	}
	public String getInsurance() {
		return CommonUtility.replaceNull(insurance);
	}
	public void setInsurance(String insurance) {
		this.insurance = insurance;
	}
	public String getReplacement() {
		return CommonUtility.replaceNull(replacement);
	}
	public void setReplacement(String replacement) {
		this.replacement = replacement;
	}
	public String getOldSerialNo() {
		return CommonUtility.replaceNull(oldSerialNo);
	}
	public void setOldSerialNo(String oldSerialNo) {
		this.oldSerialNo = oldSerialNo;
	}
	public String getCustomerPartNo() {
		return CommonUtility.replaceNull(customerPartNo);
	}
	public void setCustomerPartNo(String customerPartNo) {
		this.customerPartNo = customerPartNo;
	}
	public String getDeliveryLocation() {
		return CommonUtility.replaceNull(deliveryLocation);
	}
	public void setDeliveryLocation(String deliveryLocation) {
		this.deliveryLocation = deliveryLocation;
	}
	public String getDeliveryAddress() {
		return CommonUtility.replaceNull(deliveryAddress);
	}
	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}
	public String getNoOfMotors() {
		return CommonUtility.replaceNull(noOfMotors);
	}
	public void setNoOfMotors(String noOfMotors) {
		this.noOfMotors = noOfMotors;
	}
	public String getIncludetypeTestCharges() {
		return CommonUtility.replaceNull(includetypeTestCharges);
	}
	public void setIncludetypeTestCharges(String includetypeTestCharges) {
		this.includetypeTestCharges = includetypeTestCharges;
	}
	public String getPriceincludeFrieghtVal() {
		return CommonUtility.replaceNull(priceincludeFrieghtVal);
	}
	public void setPriceincludeFrieghtVal(String priceincludeFrieghtVal) {
		this.priceincludeFrieghtVal = priceincludeFrieghtVal;
	}
	public String getPriceincludeExtraWarnVal() {
		return CommonUtility.replaceNull(priceincludeExtraWarnVal);
	}
	public void setPriceincludeExtraWarnVal(String priceincludeExtraWarnVal) {
		this.priceincludeExtraWarnVal = priceincludeExtraWarnVal;
	}
	public String getPriceincludeSupervisionCostVal() {
		return CommonUtility.replaceNull(priceincludeSupervisionCostVal);
	}
	public void setPriceincludeSupervisionCostVal(
			String priceincludeSupervisionCostVal) {
		this.priceincludeSupervisionCostVal = priceincludeSupervisionCostVal;
	}
	public String getPriceincludetypeTestChargesVal() {
		return CommonUtility.replaceNull(priceincludetypeTestChargesVal);
	}
	public void setPriceincludetypeTestChargesVal(
			String priceincludetypeTestChargesVal) {
		this.priceincludetypeTestChargesVal = priceincludetypeTestChargesVal;
	}
	public String getPriceextraFrieghtVal() {
		return CommonUtility.replaceNull(priceextraFrieghtVal);
	}
	public void setPriceextraFrieghtVal(String priceextraFrieghtVal) {
		this.priceextraFrieghtVal = priceextraFrieghtVal;
	}
	public String getPriceextraExtraWarnVal() {
		return CommonUtility.replaceNull(priceextraExtraWarnVal);
	}
	public void setPriceextraExtraWarnVal(String priceextraExtraWarnVal) {
		this.priceextraExtraWarnVal = priceextraExtraWarnVal;
	}
	public String getPriceextraSupervisionCostVal() {
		return CommonUtility.replaceNull(priceextraSupervisionCostVal);
	}
	public void setPriceextraSupervisionCostVal(String priceextraSupervisionCostVal) {
		this.priceextraSupervisionCostVal = priceextraSupervisionCostVal;
	}
	public String getPriceextratypeTestChargesVal() {
		return CommonUtility.replaceNull(priceextratypeTestChargesVal);
	}
	public void setPriceextratypeTestChargesVal(String priceextratypeTestChargesVal) {
		this.priceextratypeTestChargesVal = priceextratypeTestChargesVal;
	}
	public String getTotalMotorPrice() {
		return CommonUtility.replaceNull(totalMotorPrice);
	}
	public void setTotalMotorPrice(String totalMotorPrice) {
		this.totalMotorPrice = totalMotorPrice;
	}
	public String getIndentOperation() {
		return CommonUtility.replaceNull(indentOperation);
	}
	public void setIndentOperation(String indentOperation) {
		this.indentOperation = indentOperation;
	}
	public String getIndentOperationCheck() {
		return CommonUtility.replaceNull(indentOperationCheck);
	}
	public void setIndentOperationCheck(String indentOperationCheck) {
		this.indentOperationCheck = indentOperationCheck;
	}
	public String getIndentnote() {
		return CommonUtility.replaceNull(indentnote);
	}
	public void setIndentnote(String indentnote) {
		this.indentnote = indentnote;
	}
	public String getCmToSmReturn() {
		return CommonUtility.replaceNull(cmToSmReturn);
	}
	public void setCmToSmReturn(String cmToSmReturn) {
		this.cmToSmReturn = cmToSmReturn;
	}
	public String getReturnEnquiry() {
		return CommonUtility.replaceNull(returnEnquiry);
	}
	public void setReturnEnquiry(String returnEnquiry) {
		this.returnEnquiry = returnEnquiry;
	}
	public String getCommengToSmReturn() {
		return CommonUtility.replaceNull(commengToSmReturn);
	}
	public void setCommengToSmReturn(String commengToSmReturn) {
		this.commengToSmReturn = commengToSmReturn;
	}
	public String getDmToCmReturn() {
		return CommonUtility.replaceNull(dmToCmReturn);
	}
	public void setDmToCmReturn(String dmToCmReturn) {
		this.dmToCmReturn = dmToCmReturn;
	}
	public String getDeToCmReturn() {
		return CommonUtility.replaceNull(deToCmReturn);
	}
	public void setDeToCmReturn(String deToCmReturn) {
		this.deToCmReturn = deToCmReturn;
	}
	public String getCmNote() {
		return CommonUtility.replaceNull(cmNote);
	}
	public void setCmNote(String cmNote) {
		this.cmNote = cmNote;
	}
	public String getCmName() {
		return CommonUtility.replaceNull(cmName);
	}
	public void setCmName(String cmName) {
		this.cmName = cmName;
	}
	public String getIndentDmtosmReturn() {
		return CommonUtility.replaceNull(indentDmtosmReturn);
	}
	public void setIndentDmtosmReturn(String indentDmtosmReturn) {
		this.indentDmtosmReturn = indentDmtosmReturn;
	}
	public String getIndentDetosmReturn() {
		return CommonUtility.replaceNull(indentDetosmReturn);
	}
	public void setIndentDetosmReturn(String indentDetosmReturn) {
		this.indentDetosmReturn = indentDetosmReturn;
	}
	public ArrayList getDeliveryDetailsList() {
		return deliveryDetailsList;
	}
	public void setDeliveryDetailsList(ArrayList deliveryDetailsList) {
		this.deliveryDetailsList = deliveryDetailsList;
	}
	public String getSapCustomerCode() {
		return CommonUtility.replaceNull(sapCustomerCode);
	}
	public void setSapCustomerCode(String sapCustomerCode) {
		this.sapCustomerCode = sapCustomerCode;
	}
	public String getIncludeFrieght() {
		return CommonUtility.replaceNull(includeFrieght);
	}
	public void setIncludeFrieght(String includeFrieght) {
		this.includeFrieght = includeFrieght;
	}
	public String getIncludeExtraWarrnt() {
		return CommonUtility.replaceNull(includeExtraWarrnt);
	}
	public void setIncludeExtraWarrnt(String includeExtraWarrnt) {
		this.includeExtraWarrnt = includeExtraWarrnt;
	}
	public String getIncludeSupervisionCost() {
		return CommonUtility.replaceNull(includeSupervisionCost);
	}
	public void setIncludeSupervisionCost(String includeSupervisionCost) {
		this.includeSupervisionCost = includeSupervisionCost;
	}
	public String getTypeTestCharges() {
		return CommonUtility.replaceNull(typeTestCharges);
	}
	public void setTypeTestCharges(String typeTestCharges) {
		this.typeTestCharges = typeTestCharges;
	}
	public String getExtraFrieght() {
		return CommonUtility.replaceNull(extraFrieght);
	}
	public void setExtraFrieght(String extraFrieght) {
		this.extraFrieght = extraFrieght;
	}
	public String getExtraExtraWarrnt() {
		return CommonUtility.replaceNull(extraExtraWarrnt);
	}
	public void setExtraExtraWarrnt(String extraExtraWarrnt) {
		this.extraExtraWarrnt = extraExtraWarrnt;
	}
	public String getExtraSupervisionCost() {
		return CommonUtility.replaceNull(extraSupervisionCost);
	}
	public void setExtraSupervisionCost(String extraSupervisionCost) {
		this.extraSupervisionCost = extraSupervisionCost;
	}
	public String getPriceincludeFrieght() {
		return CommonUtility.replaceNull(priceincludeFrieght);
	}
	public void setPriceincludeFrieght(String priceincludeFrieght) {
		this.priceincludeFrieght = priceincludeFrieght;
	}
	public String getPriceincludeExtraWarn() {
		return CommonUtility.replaceNull(priceincludeExtraWarn);
	}
	public void setPriceincludeExtraWarn(String priceincludeExtraWarn) {
		this.priceincludeExtraWarn = priceincludeExtraWarn;
	}
	public String getPriceincludeSupervisionCost() {
		return CommonUtility.replaceNull(priceincludeSupervisionCost);
	}
	public void setPriceincludeSupervisionCost(String priceincludeSupervisionCost) {
		this.priceincludeSupervisionCost = priceincludeSupervisionCost;
	}
	public String getPriceincludetypeTestCharges() {
		return CommonUtility.replaceNull(priceincludetypeTestCharges);
	}
	public void setPriceincludetypeTestCharges(String priceincludetypeTestCharges) {
		this.priceincludetypeTestCharges = priceincludetypeTestCharges;
	}
	public String getPriceextraFrieght() {
		return CommonUtility.replaceNull(priceextraFrieght);
	}
	public void setPriceextraFrieght(String priceextraFrieght) {
		this.priceextraFrieght = priceextraFrieght;
	}
	public String getPriceextraExtraWarn() {
		return CommonUtility.replaceNull(priceextraExtraWarn);
	}
	public void setPriceextraExtraWarn(String priceextraExtraWarn) {
		this.priceextraExtraWarn = priceextraExtraWarn;
	}
	public String getPriceextraSupervisionCost() {
		return CommonUtility.replaceNull(priceextraSupervisionCost);
	}
	public void setPriceextraSupervisionCost(String priceextraSupervisionCost) {
		this.priceextraSupervisionCost = priceextraSupervisionCost;
	}
	public String getPriceextratypeTestCharges() {
		return CommonUtility.replaceNull(priceextratypeTestCharges);
	}
	public void setPriceextratypeTestCharges(String priceextratypeTestCharges) {
		this.priceextratypeTestCharges = priceextratypeTestCharges;
	}
	public ArrayList getCommercialManagersList() {
		return commercialManagersList;
	}
	public void setCommercialManagersList(ArrayList commercialManagersList) {
		this.commercialManagersList = commercialManagersList;
	}
	public String getLossPrice() {
		return CommonUtility.replaceNull(lossPrice);
	}
	public void setLossPrice(String lossPrice) {
		this.lossPrice = lossPrice;
	}
	public String getLossReason1() {
		return CommonUtility.replaceNull(lossReason1);
	}
	public void setLossReason1(String lossReason1) {
		this.lossReason1 = lossReason1;
	}
	public String getLossReason2() {
		return CommonUtility.replaceNull(lossReason2);
	}
	public void setLossReason2(String lossReason2) {
		this.lossReason2 = lossReason2;
	}
	public String getLossComment() {
		return CommonUtility.replaceNull(lossComment);
	}
	public void setLossComment(String lossComment) {
		this.lossComment = lossComment;
	}
	public String getLossCompetitor() {
		return CommonUtility.replaceNull(lossCompetitor);
	}
	public void setLossCompetitor(String lossCompetitor) {
		this.lossCompetitor = lossCompetitor;
	}
	public String getAbondentComment() {
		return CommonUtility.replaceNull(abondentComment);
	}
	public void setAbondentComment(String abondentComment) {
		this.abondentComment = abondentComment;
	}
	
	public String getPllCurr5() {
		return CommonUtility.replaceNull(pllCurr5);
	}
	public void setPllCurr5(String pllCurr5) {
		this.pllCurr5 = pllCurr5;
	}
	public String getPllEff4() {
		return CommonUtility.replaceNull(pllEff4);
	}
	public void setPllEff4(String pllEff4) {
		this.pllEff4 = pllEff4;
	}
	public String getPllPf5() {
		return CommonUtility.replaceNull(pllPf5);
	}
	public void setPllPf5(String pllPf5) {
		this.pllPf5 = pllPf5;
	}
	
	public String getExtratypeTestCharges() {
		return CommonUtility.replaceNull(extratypeTestCharges);
	}
	public void setExtratypeTestCharges(String extratypeTestCharges) {
		this.extratypeTestCharges = extratypeTestCharges;
	}
	
	public String getIsNewEnquiry() {
		return CommonUtility.replaceNull(isNewEnquiry);
	}
	public void setIsNewEnquiry(String isNewEnquiry) {
		this.isNewEnquiry = isNewEnquiry;
	}

}
