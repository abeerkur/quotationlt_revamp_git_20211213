/**
 * ******************************************************************************************
 * Project Name: Search For Quotation
 * Document Name: SearchQuotationDaoImpl.java
 * Package: in.com.rbc.quotation.enquiry.dao
 * Desc:  Dao Implementation class that implements all the methods defined in
 *        Quatation interface and those related to Quatation functionalities. <br>
 *        This class implements the interfaces --> SearchQuotationDao, EnquiryQueries
 * ******************************************************************************************
 * @author 100006126
 * ******************************************************************************************
 */
package in.com.rbc.quotation.enquiry.dao;



import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.utility.DBUtility;
import in.com.rbc.quotation.common.utility.ListDBColumnUtil;
import in.com.rbc.quotation.common.utility.ListModel;
import in.com.rbc.quotation.common.utility.LoggerUtility;
import in.com.rbc.quotation.common.utility.PropertyUtility;
import in.com.rbc.quotation.enquiry.dto.SearchEnquiryDto;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Hashtable;

import org.apache.commons.lang.StringUtils;

public class SearchEnquiryDaoImpl implements SearchEnquiryDao, EnquiryQueries 
{

	public Hashtable getEnquirySearchResults(Connection conn, SearchEnquiryDto searchEnquiryDto, ListModel oListModel,String sUserId) throws Exception {
		
		 String sMethodName = "getEnquirySearchResults";
		 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
			/* PreparedStatement object to handle database operations */
	        PreparedStatement pstmt = null ;
	        
	        /* ResultSet object to store the rows retrieved from database */
	        ResultSet rs = null ;      
	        
	        /* Object of DBUtility class to handle database operations */
	        DBUtility oDBUtility = new DBUtility();
	        
	        
	        /*
	         * Hashtable that stores the following values, and that is used to display results in search page
	         * --> ArrayList containing the rows retrieved from database
	         * --> ListModel object that hold the values used to retrieve values and display the same
	         * --> String array containing the list of column names to be displayed as column headings
	         */
	        Hashtable htSearchResults = null;
	        
	        ArrayList alSearchResultsList = new ArrayList(); // Stores the list of rows retrieved from the database
	        
	        int iCursorPosition = 0; // Stores the cursor position from where the records have to be retrieved
	        // from database; this is dependent on the page number selected for viewing
	        
	        
	        String sWhereString = null; // Stores the Where clause string
	        String sSqlQueryString = null; // Stores the complete SQL query to be executed
	        
	        try{
	        	
	        	/* Retrieving the where clause string that is built in the buildWhereClause method */
	            sWhereString = buildWhereClause(searchEnquiryDto); 
	            //System.out.println(" ::::::::::: SearchEnquiryDaoImpl.getEnquirySearchResults :::::::::: sWhereString = " + sWhereString);
	            /*
	             * Executing the SQL query by attaching the where clause string that is built
	             * to retrieve the count of rows obtained based on the filter criteria
	             * This count is stored in totalRecordCount property of ListModel object
	             * If no records were found, this value is set to zero
	             */
	            sSqlQueryString="";
	            if(searchEnquiryDto.getSearchType()!=null && searchEnquiryDto.getSearchType().equalsIgnoreCase("E"))
	            {
	            	sSqlQueryString ="SELECT COUNT(*) FROM (" +EnquiryQueries.FETCH_SEARCHENQUIRY_RESULTS+sWhereString + ")";
	            }	            	
	            else
	            {
	            	sSqlQueryString ="SELECT COUNT(*) FROM (" +EnquiryQueries.FETCH_SEARCHENQUIRYRATINGS_RESULTS+sWhereString + ")";
	            }
	            //System.out.println(" ::::::::::: SearchEnquiryDaoImpl.getEnquirySearchResults :::::::::: sSqlQueryString = " + sSqlQueryString);
	            
	            pstmt = conn.prepareStatement(sSqlQueryString);
	            pstmt.setString(1,sUserId);
	            rs = pstmt.executeQuery();
	            if (rs.next()) {
	                oListModel.setTotalRecordCount(rs.getInt(1));
	            } else {
	                oListModel.setTotalRecordCount(0);
	                
	            }
	            oDBUtility.releaseResources(rs, pstmt);
	   
	            if (oListModel.getTotalRecordCount() > 0) {
	            	htSearchResults = new Hashtable();
	            	
	            	if (oListModel.getCurrentPageInt()<0)
	                    throw new Exception("Current Page is negative in search method ");
	            	
	            	iCursorPosition = (oListModel.getCurrentPageInt()-1) * oListModel.getRecordsPerPageInt();
	            	
	            	if(iCursorPosition < oListModel.getTotalRecordCount()) {
	            	
	            		if(searchEnquiryDto.getSearchType()!=null && searchEnquiryDto.getSearchType().equalsIgnoreCase("E") )
                    	{
	            			sSqlQueryString = new StringBuffer("SELECT * FROM (SELECT Q.*, rownum AS rnum FROM ( ")
		                    						.append(EnquiryQueries.FETCH_SEARCHENQUIRY_RESULTS)
								                    .append(sWhereString)
								                    .append(" ORDER BY ")
								                    .append(ListDBColumnUtil.getSearchResultDBField(oListModel.getSortBy()))
								                    .append(" " + oListModel.getSortOrderDesc())
								                    .append(" ) Q WHERE rownum <= (")
								                    .append(iCursorPosition + " + " + oListModel.getRecordsPerPage() + ")) ")
								                    .append(" WHERE rnum > " + iCursorPosition + "")
								                    .toString();
                    	}else
                    	{
                    		sSqlQueryString = new StringBuffer("SELECT * FROM (SELECT Q.*, rownum AS rnum FROM ( ")
    						.append(EnquiryQueries.FETCH_SEARCHENQUIRYRATINGS_RESULTS)
		                    .append(sWhereString)
		                    .append(" ORDER BY ")
		                    .append(ListDBColumnUtil.getSearchResultDBField(oListModel.getSortBy()))
		                    .append(" " + oListModel.getSortOrderDesc())
		                    .append(" ) Q WHERE rownum <= (")
		                    .append(iCursorPosition + " + " + oListModel.getRecordsPerPage() + ")) ")
		                    .append(" WHERE rnum > " + iCursorPosition + "")
		                    .toString();
                    		
                    	}
	            		
	            		pstmt = conn.prepareStatement(sSqlQueryString, ResultSet.TYPE_SCROLL_INSENSITIVE,
	                             ResultSet.CONCUR_READ_ONLY);
	            		pstmt.setString(1,sUserId);
	            		String sOldEnquiryId="";
	            		rs = pstmt.executeQuery();
	            		SearchEnquiryDto oSearchEnquiryDto;
	            		if (rs.next()) {
	                        do {
                    				oSearchEnquiryDto = new SearchEnquiryDto();
                    				
                    				oSearchEnquiryDto.setYear(rs.getString("QEM_YEAR")==null?"":rs.getString("QEM_YEAR"));
                    				oSearchEnquiryDto.setMonth(rs.getString("QEM_MONTH")==null?"":rs.getString("QEM_MONTH"));
                    				oSearchEnquiryDto.setRegionName(rs.getString("QEM_REGIONNAME")==null?"":rs.getString("QEM_REGIONNAME"));
		                        	oSearchEnquiryDto.setSalesManagerName(rs.getString("QEM_CREATEDBYNAME")==null?"":rs.getString("QEM_CREATEDBYNAME"));
		                        	oSearchEnquiryDto.setEnquiryType(rs.getString("QEM_ENQUIRYTYPE")==null?"":rs.getString("QEM_ENQUIRYTYPE"));
		                        	oSearchEnquiryDto.setCustomerType(rs.getString("QEM_CUSTOMERTYPE")==null?"":rs.getString("QEM_CUSTOMERTYPE"));
                    				oSearchEnquiryDto.setEnquiryId(rs.getString("QEM_ENQUIRYID")==null?"":rs.getString("QEM_ENQUIRYID"));
		                        	oSearchEnquiryDto.setEnquiryReferenceNumber(rs.getString("QEM_ENQUIRYREFERENCENO")==null?"":rs.getString("QEM_ENQUIRYREFERENCENO"));
		                        	oSearchEnquiryDto.setProjectName(rs.getString("QEM_PROJECTNAME")==null?"":rs.getString("QEM_PROJECTNAME"));
		                        	oSearchEnquiryDto.setEndUserIndustry(rs.getString("QEM_EUINDUSTRY")==null?"":rs.getString("QEM_EUINDUSTRY"));
		                        	oSearchEnquiryDto.setEndUserName(rs.getString("QEM_ENDUSER")==null?"":rs.getString("QEM_ENDUSER"));
		                        	oSearchEnquiryDto.setOrderClosedDate(rs.getString("QEM_ORDCLOSEMNTH")==null?"":rs.getString("QEM_ORDCLOSEMNTH"));
		                        	oSearchEnquiryDto.setLd(rs.getString("QEM_LD"));
		                        	oSearchEnquiryDto.setApproval(rs.getString("QEM_APPROVAL"));		                        	
		                        	oSearchEnquiryDto.setTargeted(rs.getString("QEM_TARGETED")==null?"":rs.getString("QEM_TARGETED"));
		                        	oSearchEnquiryDto.setWinChance(rs.getString("QEM_WINCHANCE")==null?"":rs.getString("QEM_WINCHANCE"));
		                        	oSearchEnquiryDto.setDeliveryType(rs.getString("QEM_DELIVERYTYPE")==null?"":rs.getString("QEM_DELIVERYTYPE"));
		                        	oSearchEnquiryDto.setExDeliveryDate(rs.getString("QEM_EXPDELIVERYMNTH")==null?"":rs.getString("QEM_EXPDELIVERYMNTH"));
		                        	oSearchEnquiryDto.setSmnote(rs.getString("QEM_SMNOTE"));
		                        	oSearchEnquiryDto.setKw(rs.getString("QRD_KW")==null?"":rs.getString("QRD_KW"));
		                        	oSearchEnquiryDto.setSplFeatures(rs.getString("QRD_SPLFEATURES"));
		                        	oSearchEnquiryDto.setPoleName(rs.getString("QRD_POLEDESC")==null?"":rs.getString("QRD_POLEDESC"));
		                        	oSearchEnquiryDto.setVolts(rs.getString("QRD_VOLTS")==null?"":rs.getString("QRD_VOLTS"));
		                        	oSearchEnquiryDto.setFrameName(rs.getString("QRD_TO_FRAMESIZE")==null?"":rs.getString("QRD_TO_FRAMESIZE"));
		                        	oSearchEnquiryDto.setMounting(rs.getString("QRD_MOUNTINGDESC")==null?"":rs.getString("QRD_MOUNTINGDESC"));
		                        	oSearchEnquiryDto.setTypeDesc(rs.getString("QRD_SCSRDESC")==null?"":rs.getString("QRD_SCSRDESC"));
		                        	oSearchEnquiryDto.setEnclosureDesc(rs.getString("QRD_ENCLOSUREDESC")==null?"":rs.getString("QRD_ENCLOSUREDESC"));
		                        	oSearchEnquiryDto.setApplicationDesc(rs.getString("QRD_APPLICATIONDESC")==null?"":rs.getString("QRD_APPLICATIONDESC"));
		                        	if(searchEnquiryDto.getSearchType().equalsIgnoreCase("E")){
		                        		oSearchEnquiryDto.setTotalMotorPrice(rs.getString("QEM_QUOTEDPRICE"));
		                        		oSearchEnquiryDto.setTotalOrderPrice(rs.getString("QEM_TOTMOTORPRICE"));
		                        	}
		                        	else{
		                        		oSearchEnquiryDto.setTotalMotorPrice(rs.getString("QRD_QUOTEDPRICE"));
		                        		oSearchEnquiryDto.setTotalOrderPrice(rs.getString("QRD_TOTALMOTORPRICE"));
		                        	}
		                        	oSearchEnquiryDto.setWinReason(rs.getString("QEM_REASON_WINLOSS")==null?"":rs.getString("QEM_REASON_WINLOSS"));
		                        	oSearchEnquiryDto.setLossReason1(rs.getString("QEM_LOSS_REASON1")==null?"":rs.getString("QEM_LOSS_REASON1"));
		                        	oSearchEnquiryDto.setLossReason2(rs.getString("QEM_LOSS_REASON2")==null?"":rs.getString("QEM_LOSS_REASON2"));
		                        	oSearchEnquiryDto.setCompetitor(rs.getString("QEM_LOSS_COMPETITOR")==null?"":rs.getString("QEM_LOSS_COMPETITOR"));
		                        	oSearchEnquiryDto.setLossComment(rs.getString("QEM_LOSS_COMMENT")==null?"":rs.getString("QEM_LOSS_COMMENT"));
		                        	oSearchEnquiryDto.setWarrantyFromDispatch(rs.getString("QEM_DISPATCHDATEWRNTY")==null?"":rs.getString("QEM_DISPATCHDATEWRNTY"));
		                        	oSearchEnquiryDto.setWarrantyFromComm(rs.getString("QEM_COMMISSIONINGDATEWRNTY")==null?"":rs.getString("QEM_COMMISSIONINGDATEWRNTY"));
		                        	oSearchEnquiryDto.setSavingIndent(rs.getString("QEM_SAVINGINDENT"));		                        			                        	
		                        	oSearchEnquiryDto.setCreatedBy(rs.getString("QEM_CREATEDBYNAME")==null?"":rs.getString("QEM_CREATEDBYNAME"));
		                        	oSearchEnquiryDto.setCreatedDate(rs.getString("QEM_CREATEDDATE")==null?"":rs.getString("QEM_CREATEDDATE"));
		                        	oSearchEnquiryDto.setCustomerName(rs.getString("QEM_CUSTOMERNAME")==null?"":rs.getString("QEM_CUSTOMERNAME"));
		                        	oSearchEnquiryDto.setEnquiryNumber(rs.getString("QEM_ENQUIRYNO")==null?"":rs.getString("QEM_ENQUIRYNO"));
		                        	oSearchEnquiryDto.setLocation(rs.getString("QEM_LOCATIONNAME")==null?"":rs.getString("QEM_LOCATIONNAME"));
		                        	oSearchEnquiryDto.setStatusName(rs.getString("QEM_STATUSDESC")==null?"":rs.getString("QEM_STATUSDESC"));
		                        	oSearchEnquiryDto.setStatusId(rs.getString("QEM_STATUSID")==null?"":rs.getString("QEM_STATUSID"));
		                        	oSearchEnquiryDto.setCreatedBySSO(rs.getString("QEM_CREATEDBY")==null?"":rs.getString("QEM_CREATEDBY"));
		  		                    oSearchEnquiryDto.setAssignTo(rs.getString("QEM_ASSIGNTO")==null?"":rs.getString("QEM_ASSIGNTO"));
		                        	oSearchEnquiryDto.setDesignEngineerName(rs.getString("QEM_DESIGNENGINEER")==null?"":rs.getString("QEM_DESIGNENGINEER"));
		                        	oSearchEnquiryDto.setDesignedBy(rs.getString("QEM_DESIGNEDBY")==null?"":rs.getString("QEM_DESIGNEDBY"));
		                        	if(StringUtils.isBlank(rs.getString("QEM_IS_NEWENQUIRY"))) {
		                        		oSearchEnquiryDto.setIsNewEnquiry("N");
		                        	} else {
		                        		oSearchEnquiryDto.setIsNewEnquiry(rs.getString("QEM_IS_NEWENQUIRY").trim());
		                        	}
		                        	
		                        	alSearchResultsList.add(oSearchEnquiryDto);
				                        	
				                 } while (rs.next());
	                    }
	                    				
	            	}
	            	htSearchResults.put("resultsList", alSearchResultsList);
	                htSearchResults.put("ListModel", oListModel);  
	                sSqlQueryString="";
	                
            		if(searchEnquiryDto.getSearchType()!=null && searchEnquiryDto.getSearchType().equalsIgnoreCase("E") )
                	{
            			sSqlQueryString = new StringBuffer("SELECT * FROM (SELECT Q.*, rownum AS rnum FROM ( ")
	                    						.append(EnquiryQueries.FETCH_SEARCHENQUIRY_RESULTS)
							                    .append(sWhereString)
							                    .append(" ORDER BY ")
							                    .append(ListDBColumnUtil.getSearchResultDBField(oListModel.getSortBy()))
							                    .append(" " + oListModel.getSortOrderDesc())
							                    .append(" ) Q) ").toString();
                	}else
                	{
                		sSqlQueryString = new StringBuffer("SELECT * FROM (SELECT Q.*, rownum AS rnum FROM ( ")
						.append(EnquiryQueries.FETCH_SEARCHENQUIRYRATINGS_RESULTS)
	                    .append(sWhereString)
	                    .append(" ORDER BY ")
	                    .append(ListDBColumnUtil.getSearchResultDBField(oListModel.getSortBy()))
	                    .append(" " + oListModel.getSortOrderDesc())
	                    .append(" ) Q) ").toString();
                		
                	}
	                
	                htSearchResults.put("whereQuery", sSqlQueryString);
	            }
	        	
	        }finally {
	            /* Releasing ResultSet & PreparedStatement objects */
	            oDBUtility.releaseResources(rs, pstmt);
	        }
	       
			 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
	        return htSearchResults ;
	}

	private String buildWhereClause(SearchEnquiryDto oSearchEnquiryDto) {
		
		 String sMethodName = "buildWhereClause";
		 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		StringBuffer sbWhereString = new StringBuffer(" AND ");
		boolean isAdded = false;
			
       
        
        if((oSearchEnquiryDto.getEnquiryNumber()!= null) && !(oSearchEnquiryDto.getEnquiryNumber().trim().equalsIgnoreCase(""))){
			sbWhereString.append("UPPER(a.QEM_ENQUIRYNO) LIKE '%"+oSearchEnquiryDto.getEnquiryNumber().trim().toUpperCase()+"%'");
			sbWhereString.append(" AND "); 
		}

        
        
        if((oSearchEnquiryDto.getCustomerName()!= null) && oSearchEnquiryDto.getCustomerName().trim().length()>0){
			sbWhereString.append("UPPER(a.QEM_CUSTOMERNAME) LIKE '"+oSearchEnquiryDto.getCustomerName().trim().toUpperCase()+"%'");
			sbWhereString.append(" AND "); 
		}
        else if((oSearchEnquiryDto.getCustomerId()!= null) && !(oSearchEnquiryDto.getCustomerId().trim().equalsIgnoreCase("0") || oSearchEnquiryDto.getCustomerId().trim().equalsIgnoreCase("")) ){
			
			sbWhereString.append("a.QEM_CUSTOMERID = '"+oSearchEnquiryDto.getCustomerId().trim()+"' ");
			sbWhereString.append(" AND "); 
		}
			
        
			if((oSearchEnquiryDto.getCreatedBySSO()!= null && !oSearchEnquiryDto.getCreatedBySSO().equalsIgnoreCase("0")) && (oSearchEnquiryDto.getCreatedBySSO().trim().length()>0 )){
				sbWhereString.append("a.QEM_CREATEDBY = '"+oSearchEnquiryDto.getCreatedBySSO().trim()+"'");
				sbWhereString.append(" AND "); 
			}
			

			if((oSearchEnquiryDto.getAssignToSSO()!= null && !oSearchEnquiryDto.getAssignToSSO().equalsIgnoreCase("0")) && (oSearchEnquiryDto.getAssignToSSO().trim().length()>0 )){
				sbWhereString.append("a.QEM_ASSIGNTO_SSOID = '"+oSearchEnquiryDto.getAssignToSSO().trim()+"'");
				sbWhereString.append(" AND "); 
			}

			//Get DesignedBy Detailss
			
			if((oSearchEnquiryDto.getDesignedBySSO()!= null && !oSearchEnquiryDto.getDesignedBySSO().equalsIgnoreCase("0")) && (oSearchEnquiryDto.getDesignedBySSO().trim().length()>0 )){
				sbWhereString.append("a.QEM_DESIGNEDBY_SSOID = '"+oSearchEnquiryDto.getDesignedBySSO().trim()+"'");
				sbWhereString.append(" AND "); 
			}
           
            if(((oSearchEnquiryDto.getCreatedStartDate()!= null && oSearchEnquiryDto.getCreatedEndDate() != null)) &&
                    ((oSearchEnquiryDto.getCreatedStartDate().trim().length()>0 && oSearchEnquiryDto.getCreatedEndDate().trim().length() >0))){
            	sbWhereString.append(" TO_DATE(a.QEM_CREATEDDATE) BETWEEN  TO_DATE('" +oSearchEnquiryDto.getCreatedStartDate()+"','MM/DD/YYYY') AND TO_DATE('" +oSearchEnquiryDto.getCreatedEndDate()+"','MM/DD/YYYY')");
        		sbWhereString.append(" AND "); 
            }
            

			if((oSearchEnquiryDto.getStatusId()!= null)&&(!oSearchEnquiryDto.getStatusId().equals("")&&!oSearchEnquiryDto.getStatusId().equals("0"))){
				sbWhereString.append(" a.QEM_STATUSID = '"+oSearchEnquiryDto.getStatusId().trim()+"'");
				sbWhereString.append(" AND "); 
		
			}else if ((oSearchEnquiryDto.getStatusName()!= null)&&(oSearchEnquiryDto.getStatusName().trim().length()>0)){
				sbWhereString.append(" UPPER(a.QEM_STATUSDESC) = '"+oSearchEnquiryDto.getStatusName().trim().toUpperCase()+"'");
				sbWhereString.append(" AND ");
				
			}
				
			if((oSearchEnquiryDto.getRegionId()!= null)&&(!oSearchEnquiryDto.getRegionId().equals("")&&!oSearchEnquiryDto.getRegionId().equals("0"))){
				sbWhereString.append(" a.QEM_REGIONID = '"+oSearchEnquiryDto.getRegionId().trim()+"'");
				sbWhereString.append(" AND "); 
		
			}
			
			if((oSearchEnquiryDto.getYear()!= null)&&(!oSearchEnquiryDto.getYear().equals("")&&!oSearchEnquiryDto.getYear().equals("0"))){
				sbWhereString.append(" TO_CHAR(a.QEM_CREATEDDATE, 'YYYY') = '"+oSearchEnquiryDto.getYear().trim()+"'");
				sbWhereString.append(" AND "); 
		
			}
			if((oSearchEnquiryDto.getMonth()!= null)&&(!oSearchEnquiryDto.getMonth().equals("")&&!oSearchEnquiryDto.getMonth().equals("0"))){
				sbWhereString.append("  TO_CHAR(a.QEM_CREATEDDATE, 'MM') = '"+oSearchEnquiryDto.getMonth().trim()+"'");
				sbWhereString.append(" AND "); 
		
			}
			if((oSearchEnquiryDto.getPoleId()!= 0)){
				sbWhereString.append("b.QRD_POLEID = '"+oSearchEnquiryDto.getPoleId()+"'");
				sbWhereString.append(" AND "); 
			}
			
			if((oSearchEnquiryDto.getMounting() != null)&&(!oSearchEnquiryDto.getMounting().equals(""))){
				sbWhereString.append(" b.QRD_MOUNTINGID = '"+oSearchEnquiryDto.getMounting().trim()+"'");
				sbWhereString.append(" AND "); 
			}
            
            if((oSearchEnquiryDto.getFrameName() != null )&& (!oSearchEnquiryDto.getFrameName().equals(""))) {
                sbWhereString.append(" UPPER(b.QRD_TO_FRAMESIZE) LIKE '%"+oSearchEnquiryDto.getFrameName().trim().toUpperCase()+"%'");
                sbWhereString.append(" AND "); 
            }
            if((oSearchEnquiryDto.getVolts() != null )&& (!oSearchEnquiryDto.getVolts().equals(""))) {
                sbWhereString.append(" b.QRD_VOLTS = '"+oSearchEnquiryDto.getVolts().trim()+"'");
                sbWhereString.append(" AND "); 
            }
           
            
            if((oSearchEnquiryDto.getKw() != null) && (oSearchEnquiryDto.getKw().trim().length() > 0)) {
              sbWhereString.append(" b.QRD_KW = '"+oSearchEnquiryDto.getKw()+"'");
              sbWhereString.append(" AND "); 
              sbWhereString.append(" b.QRD_ROUNIT = '"+oSearchEnquiryDto.getRatedOutputUnit()+"'");
              sbWhereString.append(" AND "); 
            }
            
            if((oSearchEnquiryDto.getScsrId() != 0) ) {
                sbWhereString.append(" b.QRD_SCSRID = '"+oSearchEnquiryDto.getScsrId()+"'");
                sbWhereString.append(" AND "); 
            }
            if((oSearchEnquiryDto.getEnclosureId() !=0) ) {
                sbWhereString.append(" b.QRD_ENCLOSUREID = '"+oSearchEnquiryDto.getEnclosureId()+"'");
                sbWhereString.append(" AND "); 
            }
            if((oSearchEnquiryDto.getApplicationId() != null) && (oSearchEnquiryDto.getApplicationId().trim().length() > 0)) {
                sbWhereString.append(" b.QRD_APPLICATIONID = '"+oSearchEnquiryDto.getApplicationId()+"'");
                sbWhereString.append(" AND "); 
            }
            if((oSearchEnquiryDto.getEndUserIndustry() != null) && (oSearchEnquiryDto.getEndUserIndustry().trim().length() > 0)) {
                sbWhereString.append(" UPPER(a.QEM_EUINDUSTRY) = '"+oSearchEnquiryDto.getEndUserIndustry().trim().toUpperCase()+"'");
                sbWhereString.append(" AND "); 
            }
            if((oSearchEnquiryDto.getProjectName() != null) && (oSearchEnquiryDto.getProjectName().trim().length() > 0)) {
                sbWhereString.append(" UPPER(a.QEM_PROJECTNAME) = '"+oSearchEnquiryDto.getProjectName().trim().toUpperCase()+"'");
                sbWhereString.append(" AND "); 
            }
            if((oSearchEnquiryDto.getOrderClsStartDate() != null && oSearchEnquiryDto.getOrderClsEndDate() != null) && (oSearchEnquiryDto.getOrderClsEndDate().trim().length() > 0 && oSearchEnquiryDto.getOrderClsStartDate().trim().length() > 0  )) {               
                sbWhereString.append(" TO_DATE(a.QEM_ORDCLOSEMNTH) BETWEEN  TO_DATE('" +oSearchEnquiryDto.getOrderClsStartDate()+"','MM/DD/YYYY') AND TO_DATE('" +oSearchEnquiryDto.getOrderClsEndDate()+"','MM/DD/YYYY')");
                sbWhereString.append(" AND "); 
            }
            if((oSearchEnquiryDto.getSavingIndent() != null) && (oSearchEnquiryDto.getSavingIndent().trim().length() > 0)) {
                sbWhereString.append(" UPPER(a.QEM_SAVINGINDENT) LIKE '%"+oSearchEnquiryDto.getSavingIndent().trim().toUpperCase()+"%'");
                sbWhereString.append(" AND "); 
            }
            if((oSearchEnquiryDto.getEnquiryReferenceNumber() != null) && (oSearchEnquiryDto.getEnquiryReferenceNumber().trim().length() > 0)) {
                sbWhereString.append(" UPPER(a.QEM_ENQUIRYREFERENCENO)LIKE '%"+oSearchEnquiryDto.getEnquiryReferenceNumber().trim().toUpperCase()+"%'");
                sbWhereString.append(" AND "); 
            }
            
            // Add QuotationLT : Fetch Latest Enquiries from Activation Date - Created In QuotationLT Tool.
            sbWhereString.append(" TO_DATE(TO_CHAR(a.QEM_CREATEDDATE,'DD-MM-YYYY'), 'DD-MM-YYYY') > TO_DATE('" + PropertyUtility.getKeyValue("QUOTATIONLT_ACTIVATION_DATE") + "','DD-MM-YYYY') AND a.QEM_IS_NEWENQUIRY = 'Y' ");
            sbWhereString.append(" AND "); 
            
            LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
            
            if ( sbWhereString.toString().trim().equalsIgnoreCase(" AND") )
                return "";
            else if (sbWhereString.toString().trim().endsWith(" AND"))        	
                return sbWhereString.toString().substring(0, sbWhereString.toString().lastIndexOf("AND"));        
            else
            	return "";
	
	}

	public ArrayList getEnquirySearchList(Connection conn, SearchEnquiryDto searchEnquiryDto, ListModel oListModel, String sUserId,String sWhereString) throws Exception {
		 String sMethodName = "getEnquirySearchResults";
		 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
			/* PreparedStatement object to handle database operations */
	        PreparedStatement pstmt = null ;
	        
	        /* ResultSet object to store the rows retrieved from database */
	        ResultSet rs = null ;      
	        
	        /* Object of DBUtility class to handle database operations */
	        DBUtility oDBUtility = new DBUtility();
	        ArrayList alSearchResultsList = new ArrayList(); // Stores the list of rows retrieved from the database
	       
	        String sSqlQueryString = null; // Stores the complete SQL query to be executed
	        
	        try{
	        	
	        	/* Retrieving the where clause string that is built in the buildWhereClause method 
	          
	          	sSqlQueryString = new StringBuffer(FETCH_SEARCHENQUIRY_RESULTS)
							                    .append(sWhereString)
							                    .append(" ORDER BY ")
							                    .append(ListDBColumnUtil.getSearchResultDBField(oListModel.getSortBy()))
							                    .append(" " + oListModel.getSortOrderDesc())
							                    .toString();
	       		*/
	          	pstmt = conn.prepareStatement(sWhereString, ResultSet.TYPE_SCROLL_INSENSITIVE,
	                             ResultSet.CONCUR_READ_ONLY);
	            		
	            		pstmt.setString(1,sUserId);
	            		rs = pstmt.executeQuery();
	            		SearchEnquiryDto oSearchEnquiryDto;
	            		String sOldEnquiryId="";
	            		if (rs.next()) {
	                        do {
	                        				oSearchEnquiryDto = new SearchEnquiryDto();
	                        				
	                        				
	                        				oSearchEnquiryDto.setYear(rs.getString("QEM_YEAR")==null?"":rs.getString("QEM_YEAR"));
	                        				oSearchEnquiryDto.setMonth(rs.getString("QEM_MONTH")==null?"":rs.getString("QEM_MONTH"));
	                        				oSearchEnquiryDto.setRegionName(rs.getString("QEM_REGIONNAME")==null?"":rs.getString("QEM_REGIONNAME"));
	    		                        	oSearchEnquiryDto.setSalesManagerName(rs.getString("QEM_CREATEDBYNAME")==null?"":rs.getString("QEM_CREATEDBYNAME"));
	    		                        	oSearchEnquiryDto.setEnquiryType(rs.getString("QEM_ENQUIRYTYPE")==null?"":rs.getString("QEM_ENQUIRYTYPE"));
	    		                        	oSearchEnquiryDto.setCustomerType(rs.getString("QEM_CUSTOMERTYPE")==null?"":rs.getString("QEM_CUSTOMERTYPE"));
	                        				oSearchEnquiryDto.setEnquiryId(rs.getString("QEM_ENQUIRYID")==null?"":rs.getString("QEM_ENQUIRYID"));
	    		                        	oSearchEnquiryDto.setEnquiryReferenceNumber(rs.getString("QEM_ENQUIRYREFERENCENO")==null?"":rs.getString("QEM_ENQUIRYREFERENCENO"));
	    		                        	oSearchEnquiryDto.setProjectName(rs.getString("QEM_PROJECTNAME")==null?"":rs.getString("QEM_PROJECTNAME"));
	    		                        	oSearchEnquiryDto.setEndUserIndustry(rs.getString("QEM_EUINDUSTRY")==null?"":rs.getString("QEM_EUINDUSTRY"));
	    		                        	oSearchEnquiryDto.setEndUserName(rs.getString("QEM_ENDUSER")==null?"":rs.getString("QEM_ENDUSER"));
	    		                        	oSearchEnquiryDto.setOrderClosedDate(rs.getString("QEM_ORDCLOSEMNTH")==null?"":rs.getString("QEM_ORDCLOSEMNTH"));
	    		                        	//Winning Chance
	    		                        	oSearchEnquiryDto.setTargeted(rs.getString("QEM_TARGETED")==null?"":rs.getString("QEM_TARGETED"));
	    		                        	oSearchEnquiryDto.setWinChance(rs.getString("QEM_WINCHANCE")==null?"":rs.getString("QEM_WINCHANCE"));
	    		                        	oSearchEnquiryDto.setDeliveryType(rs.getString("QEM_DELIVERYTYPE")==null?"":rs.getString("QEM_DELIVERYTYPE"));
	    		                        	oSearchEnquiryDto.setExDeliveryDate(rs.getString("QEM_EXPDELIVERYMNTH")==null?"":rs.getString("QEM_EXPDELIVERYMNTH"));
	    		                        	oSearchEnquiryDto.setKw(rs.getString("QRD_KW")==null?"":rs.getString("QRD_KW"));
	    		                        	oSearchEnquiryDto.setPoleName(rs.getString("QRD_POLEDESC")==null?"":rs.getString("QRD_POLEDESC"));
	    		                        	oSearchEnquiryDto.setVolts(rs.getString("QRD_VOLTS")==null?"":rs.getString("QRD_VOLTS"));
	    		                        	oSearchEnquiryDto.setFrameName(rs.getString("QRD_TO_FRAMESIZE")==null?"":rs.getString("QRD_TO_FRAMESIZE"));
	    		                        	oSearchEnquiryDto.setMounting(rs.getString("QRD_MOUNTINGDESC")==null?"":rs.getString("QRD_MOUNTINGDESC"));
	    		                        	oSearchEnquiryDto.setTypeDesc(rs.getString("QRD_SCSRDESC")==null?"":rs.getString("QRD_SCSRDESC"));
	    		                        	oSearchEnquiryDto.setEnclosureDesc(rs.getString("QRD_ENCLOSUREDESC")==null?"":rs.getString("QRD_ENCLOSUREDESC"));
	    		                        	oSearchEnquiryDto.setApplicationDesc(rs.getString("QRD_APPLICATIONDESC")==null?"":rs.getString("QRD_APPLICATIONDESC"));
	    		                        	if(searchEnquiryDto.getSearchType().equalsIgnoreCase("E")){
	    		                        		oSearchEnquiryDto.setTotalMotorPrice(rs.getString("QEM_QUOTEDPRICE"));
	    		                        		oSearchEnquiryDto.setTotalOrderPrice(rs.getString("QEM_TOTMOTORPRICE"));
	    		                        	}
	    		                        	else{
	    		                        		oSearchEnquiryDto.setTotalMotorPrice(rs.getString("QRD_QUOTEDPRICE"));
	    		                        		oSearchEnquiryDto.setTotalOrderPrice(rs.getString("QRD_TOTALMOTORPRICE"));
	    		                        	}
	    		                        	oSearchEnquiryDto.setWinReason(rs.getString("QEM_REASON_WINLOSS")==null?"":rs.getString("QEM_REASON_WINLOSS"));
	    		                        	oSearchEnquiryDto.setLossReason1(rs.getString("QEM_LOSS_REASON1")==null?"":rs.getString("QEM_LOSS_REASON1"));
	    		                        	oSearchEnquiryDto.setLossReason2(rs.getString("QEM_LOSS_REASON2")==null?"":rs.getString("QEM_LOSS_REASON2"));
	    		                        	oSearchEnquiryDto.setCompetitor(rs.getString("QEM_LOSS_COMPETITOR")==null?"":rs.getString("QEM_LOSS_COMPETITOR"));
	    		                        	oSearchEnquiryDto.setLossComment(rs.getString("QEM_LOSS_COMMENT")==null?"":rs.getString("QEM_LOSS_COMMENT"));
	    		                        	oSearchEnquiryDto.setWarrantyFromDispatch(rs.getString("QEM_DISPATCHDATEWRNTY")==null?"":rs.getString("QEM_DISPATCHDATEWRNTY"));
	    		                        	oSearchEnquiryDto.setWarrantyFromComm(rs.getString("QEM_COMMISSIONINGDATEWRNTY")==null?"":rs.getString("QEM_COMMISSIONINGDATEWRNTY"));
	    		                        	oSearchEnquiryDto.setSavingIndent(rs.getString("QEM_SAVINGINDENT"));                   	
	    		                        	oSearchEnquiryDto.setCreatedBy(rs.getString("QEM_CREATEDBYNAME")==null?"":rs.getString("QEM_CREATEDBYNAME"));
	    		                        	oSearchEnquiryDto.setCreatedDate(rs.getString("QEM_CREATEDDATE")==null?"":rs.getString("QEM_CREATEDDATE"));
	    		                        	oSearchEnquiryDto.setCustomerName(rs.getString("QEM_CUSTOMERNAME")==null?"":rs.getString("QEM_CUSTOMERNAME"));
	    		                        	oSearchEnquiryDto.setEnquiryNumber(rs.getString("QEM_ENQUIRYNO")==null?"":rs.getString("QEM_ENQUIRYNO"));
	    		                        	oSearchEnquiryDto.setLocation(rs.getString("QEM_LOCATIONNAME")==null?"":rs.getString("QEM_LOCATIONNAME"));
	    		                        	oSearchEnquiryDto.setStatusName(rs.getString("QEM_STATUSDESC")==null?"":rs.getString("QEM_STATUSDESC"));
	    		                        	oSearchEnquiryDto.setStatusId(rs.getString("QEM_STATUSID")==null?"":rs.getString("QEM_STATUSID"));
	    		                        	oSearchEnquiryDto.setCreatedBySSO(rs.getString("QEM_CREATEDBY")==null?"":rs.getString("QEM_CREATEDBY"));
	    		  		                    oSearchEnquiryDto.setAssignTo(rs.getString("QEM_ASSIGNTO")==null?"":rs.getString("QEM_ASSIGNTO"));
	    		                        	oSearchEnquiryDto.setDesignEngineerName(rs.getString("QEM_DESIGNENGINEER")==null?"":rs.getString("QEM_DESIGNENGINEER"));
	    		                        	oSearchEnquiryDto.setDesignedBy(rs.getString("QEM_DESIGNEDBY")==null?"":rs.getString("QEM_DESIGNEDBY"));

	                        				
/*				
 * TODO
 *                         	oSearchEnquiryDto.setEnquiryId(rs.getString("QEM_ENQUIRYID")==null?"":rs.getString("QEM_ENQUIRYID"));
				                        	
				                        	oSearchEnquiryDto.setCreatedBy(rs.getString("QEM_CREATEDBYNAME")==null?"":rs.getString("QEM_CREATEDBYNAME"));
				                        	oSearchEnquiryDto.setCreatedDate(rs.getString("QEM_CREATEDDATE")==null?"":rs.getString("QEM_CREATEDDATE"));
				                        	oSearchEnquiryDto.setCustomerName(rs.getString("QEM_CUSTOMERNAME")==null?"":rs.getString("QEM_CUSTOMERNAME"));
				                        	oSearchEnquiryDto.setEnquiryNumber(rs.getString("QEM_ENQUIRYNO")==null?"":rs.getString("QEM_ENQUIRYNO"));
				                        	oSearchEnquiryDto.setLocation(rs.getString("QEM_LOCATIONNAME")==null?"":rs.getString("QEM_LOCATIONNAME"));
				                        	oSearchEnquiryDto.setKw(rs.getString("QRD_KW")==null?"":rs.getString("QRD_KW"));
				                        	oSearchEnquiryDto.setPoleName(rs.getString("QRD_POLEDESC")==null?"":rs.getString("QRD_POLEDESC"));
				                        	oSearchEnquiryDto.setStatusName(rs.getString("QEM_STATUSDESC")==null?"":rs.getString("QEM_STATUSDESC"));
				                        	oSearchEnquiryDto.setVolts(rs.getString("QRD_VOLTS")==null?"":rs.getString("QRD_VOLTS"));
				                        	oSearchEnquiryDto.setMounting(rs.getString("QRD_MOUNTINGDESC")==null?"":rs.getString("QRD_MOUNTINGDESC"));
				                        	oSearchEnquiryDto.setFrameName(rs.getString("QRD_TO_FRAMESIZE")==null?"":rs.getString("QRD_TO_FRAMESIZE"));
				                        	oSearchEnquiryDto.setStatusId(rs.getString("QEM_STATUSID")==null?"":rs.getString("QEM_STATUSID"));
				                        	oSearchEnquiryDto.setCreatedBySSO(rs.getString("QEM_CREATEDBY")==null?"":rs.getString("QEM_CREATEDBY"));

				                        	oSearchEnquiryDto.setAssignTo(rs.getString("QEM_ASSIGNTO")==null?"":rs.getString("QEM_ASSIGNTO"));
				                        	oSearchEnquiryDto.setDesignEngineerName(rs.getString("QEM_DESIGNENGINEER")==null?"":rs.getString("QEM_DESIGNENGINEER"));
				                        	oSearchEnquiryDto.setDesignedBy(rs.getString("QEM_DESIGNEDBY")==null?"":rs.getString("QEM_DESIGNEDBY"));
*/				                        	
				                        	alSearchResultsList.add(oSearchEnquiryDto);
				                  	
	                       } while (rs.next());
	                    }
	                    				
	            	
	            	}
	       	       finally
	       	       {
	            /* Releasing ResultSet & PreparedStatement objects */
	            oDBUtility.releaseResources(rs, pstmt);
	        }
	       
			 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
	        return alSearchResultsList ;
	}

	public boolean isSalesManager(Connection conn,String userId) throws Exception {
			String sMethodName = "isSalesManager";
			LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
			/* PreparedStatement object to handle database operations */
	        PreparedStatement pstmt = null ;
	        
	        /* ResultSet object to store the rows retrieved from database */
	        ResultSet rs = null ;      
	        
	        /* Object of DBUtility class to handle database operations */
	        DBUtility oDBUtility = new DBUtility();
	        Boolean isSalesManager = false;
	       
	        String sSqlQueryString = null; // Stores the complete SQL query to be executed
	        
	        try{
	        	
	          		sSqlQueryString = new StringBuffer(EnquiryQueries.IS_SALES_MANAGER)
							                    .toString();
	            		

	            		
	            	
	            		pstmt = conn.prepareStatement(sSqlQueryString, ResultSet.TYPE_SCROLL_INSENSITIVE,
	                             ResultSet.CONCUR_READ_ONLY);
	            		pstmt.setString(1,userId);
	            		rs = pstmt.executeQuery();
	            		
	            		if (rs.next()) {
	            			isSalesManager = true;
	                    }
	            
	        }finally {
	            /* Releasing ResultSet & PreparedStatement objects */
	            oDBUtility.releaseResources(rs, pstmt);
	        }
	       
			 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
	        return isSalesManager ;

	}

	public String getSaleManagerSSObyId(Connection conn, String salesManagerId) throws Exception {
	
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        try{
        	pstmt = conn.prepareStatement(FETCH_SALESMANAGER_SSOID);
        	pstmt.setString(1,salesManagerId);
            
            rs = pstmt.executeQuery();
            if(rs.next()){
            	return rs.getString(1);	
            }
        }finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
    
    	
    	return "";
	}
	
	/**
	* This method is used to set the flag whether to show quoted, order price in search results page 
	* @param conn Connection object to connect to database
	* @param String sCreatedSSO String value of enquiry created sso
	* @param String sLoginSSO String value of login user sso
	* @return Returns the boolean true if user is either SM, RSM or DM,CM,CE
	* @throws Exception If any error occurs during the process
	* @author 610092227
	* Created on: 16 Sept 2019
	*/
	
	public boolean setQuotedFlag(Connection conn,String sCreatedSSO, String sLoginSSO) throws Exception {
		String sMethodName = "setQuotedFlag";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;      
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();       
        String sSqlQueryString = null; // Stores the complete SQL query to be executed
        
        try{
        	
        	if(isSalesManager(conn,sLoginSSO))
        	{
        		return QuotationConstants.QUOTATION_TRUE;
        	}
        	sSqlQueryString = new StringBuffer(EnquiryQueries.IS_RSM)
						                    .toString();
    		pstmt = conn.prepareStatement(sSqlQueryString, ResultSet.TYPE_SCROLL_INSENSITIVE,
                     ResultSet.CONCUR_READ_ONLY);
    		pstmt.setString(1,sCreatedSSO);
    		pstmt.setString(2,sLoginSSO);
    		rs = pstmt.executeQuery();
    		
    		if (rs.next()) {
    			return QuotationConstants.QUOTATION_TRUE;
            }
    		 oDBUtility.releaseResources(rs, pstmt);
    		 sSqlQueryString = new StringBuffer(EnquiryQueries.IS_MANAGER).toString();
    		 pstmt = conn.prepareStatement(sSqlQueryString, ResultSet.TYPE_SCROLL_INSENSITIVE,
    				 ResultSet.CONCUR_READ_ONLY);    		 
    		 pstmt.setString(1,sLoginSSO);     		
     		rs = pstmt.executeQuery();     		
     		if (rs.next()) {
     			return QuotationConstants.QUOTATION_TRUE;
             }
            
        }finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
       
		 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
        return QuotationConstants.QUOTATION_FALSE ;

}


}
