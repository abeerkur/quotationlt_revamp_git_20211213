/**
 * ******************************************************************************************
 * Project Name: Search For Quotation
 * Document Name: SearchEnquiryDto.java
 * Package: in.com.rbc.quotation.enquiry.dto
 * Desc: DTO class that holds getters and setters of each property.
 * ******************************************************************************************
 * @author 100006126
 * ******************************************************************************************
 */

package in.com.rbc.quotation.enquiry.dto;

import in.com.rbc.quotation.common.utility.CommonUtility;

import java.util.ArrayList;

public class SearchEnquiryDto {
	private String enquiryId;
	private String enquiryNumber;
	private String createdStartDate;
	private String createdEndDate;
	private String customerName;
	private String createdBy;
	private String designedBy; //This Variable will stores Design Manager or Design Engineer.
	private String designedBySSO; //This Variable will stores Design Manager SSOID or Design Engineer SSOIDS.
	private String createdDate;
	private String location;
	private String statusId;
	private String statusName;
	private String volts;
	private String searchType;
	private String minVolts;
	private String maxVolts;
	private ArrayList searchResultsList;
	private String createdBySSO;
	private String mounting;
	private String poleName;
	private String locationName= null;
	private String kw;
	private String frameName;
	private int scsrId = 0;
	private int enclosureId = 0;
	private int poleId = 0;
	
	
	private String customerId;
	private String locationId;
	private String regionId;
	private String year;
	private String month;
	private String poleCheck = null;
	private String kwCheck = null;
	private String frameCheck = null;
	private String voltsCheck = null;
	private String mountingCheck = null;
	

	private String assignTo=null;
	private String designEngineerName=null;
	private String assignToSSO=null;
	
	private String regionName=null;
	private String salesManagerName=null;
	private String enquiryType=null;
	private String customerType=null;
	private String enquiryReferenceNumber=null;
	private String projectName=null;
	private String endUserIndustry=null;
	private String endUserName=null;
	private String orderClosedDate=null;
	//Newly Added Fields On 22-March-2019 By Gangadhar
	private String targeted=null;
	private String winChance=null;
	private String deliveryType=null;
	private String exDeliveryDate=null;
	private String typeDesc=null;
	private String applicationDesc=null;
	private String enclosureDesc=null;
	private String totalMotorPrice=null;
	private String totalOrderPrice=null;
	private String winReason=null;
	private String lossReason1=null;
	private String lossReason2=null;
	private String competitor=null;
	private String lossComment=null;
	private String warrantyFromDispatch=null;
	private String warrantyFromComm=null;
	private String savingIndent=null;
	private String ratedOutputUnit=null;
	private String applicationId=null;
	private String splFeatures=null;
	private String ld=null;
	private String approval=null;
	private String smnote = null;
	private String orderClsStartDate = null;
	private String orderClsEndDate = null;
	
	private String isNewEnquiry=null;
	
	
	/**
	 * @return Returns the createdBy.
	 */
	public String getCreatedBy() {
		return CommonUtility.replaceNull(createdBy);
	}
	/**
	 * @param createdBy The createdBy to set.
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return Returns the createdBySSO.
	 */
	public String getCreatedBySSO() {
		return CommonUtility.replaceNull(createdBySSO);
	}
	/**
	 * @param createdBySSO The createdBySSO to set.
	 */
	public void setCreatedBySSO(String createdBySSO) {
		this.createdBySSO = createdBySSO;
	}
	/**
	 * @return Returns the createdDate.
	 */
	public String getCreatedDate() {
		return CommonUtility.replaceNull(createdDate);
	}
	/**
	 * @param createdDate The createdDate to set.
	 */
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	/**
	 * @return Returns the createdEndDate.
	 */
	public String getCreatedEndDate() {
		return CommonUtility.replaceNull(createdEndDate);
	}
	/**
	 * @param createdEndDate The createdEndDate to set.
	 */
	public void setCreatedEndDate(String createdEndDate) {
		this.createdEndDate = createdEndDate;
	}
	/**
	 * @return Returns the createdStartDate.
	 */
	public String getCreatedStartDate() {
		return CommonUtility.replaceNull(createdStartDate);
	}
	/**
	 * @param createdStartDate The createdStartDate to set.
	 */
	public void setCreatedStartDate(String createdStartDate) {
		this.createdStartDate = createdStartDate;
	}
	/**
	 * @return Returns the customerId.
	 */
	public String getCustomerId() {
		return CommonUtility.replaceNull(customerId);
	}
	/**
	 * @param customerId The customerId to set.
	 */
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	/**
	 * @return Returns the customerName.
	 */
	public String getCustomerName() {
		return CommonUtility.replaceNull(customerName);
	}
	/**
	 * @param customerName The customerName to set.
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	/**
	 * @return Returns the enquiryId.
	 */
	public String getEnquiryId() {
		return CommonUtility.replaceNull(enquiryId);
	}
	/**
	 * @param enquiryId The enquiryId to set.
	 */
	public void setEnquiryId(String enquiryId) {
		this.enquiryId = enquiryId;
	}
	/**
	 * @return Returns the enquiryNumber.
	 */
	public String getEnquiryNumber() {
		return CommonUtility.replaceNull(enquiryNumber);
	}
	/**
	 * @param enquiryNumber The enquiryNumber to set.
	 */
	public void setEnquiryNumber(String enquiryNumber) {
		this.enquiryNumber = enquiryNumber;
	}
	/**
	 * @return Returns the frameCheck.
	 */
	public String getFrameCheck() {
		return CommonUtility.replaceNull(frameCheck);
	}
	/**
	 * @param frameCheck The frameCheck to set.
	 */
	public void setFrameCheck(String frameCheck) {
		this.frameCheck = frameCheck;
	}
	/**
	 * @return Returns the frameName.
	 */
	public String getFrameName() {
		return CommonUtility.replaceNull(frameName);
	}
	/**
	 * @param frameName The frameName to set.
	 */
	public void setFrameName(String frameName) {
		this.frameName = frameName;
	}
	/**
	 * @return Returns the kw.
	 */
	public String getKw() {
		return CommonUtility.replaceNull(kw);
	}
	/**
	 * @param kw The kw to set.
	 */
	public void setKw(String kw) {
		this.kw = kw;
	}
	/**
	 * @return Returns the kwCheck.
	 */
	public String getKwCheck() {
		return CommonUtility.replaceNull(kwCheck);
	}
	/**
	 * @param kwCheck The kwCheck to set.
	 */
	public void setKwCheck(String kwCheck) {
		this.kwCheck = kwCheck;
	}
	/**
	 * @return Returns the location.
	 */
	public String getLocation() {
		return CommonUtility.replaceNull(location);
	}
	/**
	 * @param location The location to set.
	 */
	public void setLocation(String location) {
		this.location = location;
	}
	/**
	 * @return Returns the locationId.
	 */
	public String getLocationId() {
		return CommonUtility.replaceNull(locationId);
	}
	/**
	 * @param locationId The locationId to set.
	 */
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
	/**
	 * @return Returns the locationName.
	 */
	public String getLocationName() {
		return CommonUtility.replaceNull(locationName);
	}
	/**
	 * @param locationName The locationName to set.
	 */
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	/**
	 * @return Returns the maxVolts.
	 */
	public String getMaxVolts() {
		return CommonUtility.replaceNull(maxVolts);
	}
	/**
	 * @param maxVolts The maxVolts to set.
	 */
	public void setMaxVolts(String maxVolts) {
		this.maxVolts = maxVolts;
	}
	/**
	 * @return Returns the minVolts.
	 */
	public String getMinVolts() {
		return CommonUtility.replaceNull(minVolts);
	}
	/**
	 * @param minVolts The minVolts to set.
	 */
	public void setMinVolts(String minVolts) {
		this.minVolts = minVolts;
	}
	/**
	 * @return Returns the month.
	 */
	public String getMonth() {
		return CommonUtility.replaceNull(month);
	}
	/**
	 * @param month The month to set.
	 */
	public void setMonth(String month) {
		this.month = month;
	}
	/**
	 * @return Returns the mounting.
	 */
	public String getMounting() {
		return CommonUtility.replaceNull(mounting);
	}
	/**
	 * @param mounting The mounting to set.
	 */
	public void setMounting(String mounting) {
		this.mounting = mounting;
	}
	/**
	 * @return Returns the mountingCheck.
	 */
	public String getMountingCheck() {
		return CommonUtility.replaceNull(mountingCheck);
	}
	/**
	 * @param mountingCheck The mountingCheck to set.
	 */
	public void setMountingCheck(String mountingCheck) {
		this.mountingCheck = mountingCheck;
	}
	/**
	 * @return Returns the poleCheck.
	 */
	public String getPoleCheck() {
		return CommonUtility.replaceNull(poleCheck);
	}
	/**
	 * @param poleCheck The poleCheck to set.
	 */
	public void setPoleCheck(String poleCheck) {
		this.poleCheck = poleCheck;
	}
	/**
	 * @return Returns the poleName.
	 */
	public String getPoleName() {
		return CommonUtility.replaceNull(poleName);
	}
	/**
	 * @param poleName The poleName to set.
	 */
	public void setPoleName(String poleName) {
		this.poleName = poleName;
	}
	/**
	 * @return Returns the regionId.
	 */
	public String getRegionId() {
		return CommonUtility.replaceNull(regionId);
	}
	/**
	 * @param regionId The regionId to set.
	 */
	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}
	/**
	 * @return Returns the searchResultsList.
	 */
	public ArrayList getSearchResultsList() {
		return searchResultsList;
	}
	/**
	 * @param searchResultsList The searchResultsList to set.
	 */
	public void setSearchResultsList(ArrayList searchResultsList) {
		this.searchResultsList = searchResultsList;
	}
	/**
	 * @return Returns the searchType.
	 */
	public String getSearchType() {
		return CommonUtility.replaceNull(searchType);
	}
	/**
	 * @param searchType The searchType to set.
	 */
	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}
	/**
	 * @return Returns the statusId.
	 */
	public String getStatusId() {
		return CommonUtility.replaceNull(statusId);
	}
	/**
	 * @param statusId The statusId to set.
	 */
	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}
	/**
	 * @return Returns the statusName.
	 */
	public String getStatusName() {
		return CommonUtility.replaceNull(statusName);
	}
	/**
	 * @param statusName The statusName to set.
	 */
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
	/**
	 * @return Returns the volts.
	 */
	public String getVolts() {
		return CommonUtility.replaceNull(volts);
	}
	/**
	 * @param volts The volts to set.
	 */
	public void setVolts(String volts) {
		this.volts = volts;
	}
	/**
	 * @return Returns the voltsCheck.
	 */
	public String getVoltsCheck() {
		return CommonUtility.replaceNull(voltsCheck);
	}
	/**
	 * @param voltsCheck The voltsCheck to set.
	 */
	public void setVoltsCheck(String voltsCheck) {
		this.voltsCheck = voltsCheck;
	}
	/**
	 * @return Returns the year.
	 */
	public String getYear() {
		return CommonUtility.replaceNull(year);
	}
	/**
	 * @param year The year to set.
	 */
	public void setYear(String year) {
		this.year = year;
	}
	public String getAssignTo() {
		return CommonUtility.replaceNull(assignTo);
	}
	public void setAssignTo(String assignTo) {
		this.assignTo = assignTo;
	}
	public String getDesignEngineerName() {
		return CommonUtility.replaceNull(designEngineerName);
	}
	public void setDesignEngineerName(String designEngineerName) {
		this.designEngineerName = designEngineerName;
	}
	public String getAssignToSSO() {
		return CommonUtility.replaceNull(assignToSSO);
	}
	public void setAssignToSSO(String assignToSSO) {
		this.assignToSSO = assignToSSO;
	}
	/**
	 * @return Returns the designedBy.
	 */

	public String getDesignedBy() {
		return CommonUtility.replaceNull(designedBy);
	}
	/**
	 * @param designedBy The designedBy to set.
	 */

	public void setDesignedBy(String designedBy) {
		this.designedBy = designedBy;
	}
	/**
	 * @return Returns the designedBySSO.
	 */

	public String getDesignedBySSO() {
		return CommonUtility.replaceNull(designedBySSO);
	}
	/**
	 * @param designedBySSO The designedBySSO to set.
	 */

	public void setDesignedBySSO(String designedBySSO) {
		this.designedBySSO = designedBySSO;
	}
	public String getRegionName() {
		return CommonUtility.replaceNull(regionName);
	}
	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}
	public String getSalesManagerName() {
		return CommonUtility.replaceNull(salesManagerName);
	}
	public void setSalesManagerName(String salesManagerName) {
		this.salesManagerName = salesManagerName;
	}
	public String getEnquiryType() {
		return CommonUtility.replaceNull(enquiryType);
	}
	public void setEnquiryType(String enquiryType) {
		this.enquiryType = enquiryType;
	}
	public String getCustomerType() {
		return CommonUtility.replaceNull(customerType);
	}
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	public String getEnquiryReferenceNumber() {
		return CommonUtility.replaceNull(enquiryReferenceNumber);
	}
	public void setEnquiryReferenceNumber(String enquiryReferenceNumber) {
		this.enquiryReferenceNumber = enquiryReferenceNumber;
	}
	public String getProjectName() {
		return CommonUtility.replaceNull(projectName);
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getEndUserIndustry() {
		return CommonUtility.replaceNull(endUserIndustry);
	}
	public void setEndUserIndustry(String endUserIndustry) {
		this.endUserIndustry = endUserIndustry;
	}
	public String getEndUserName() {
		return CommonUtility.replaceNull(endUserName);
	}
	public void setEndUserName(String endUserName) {
		this.endUserName = endUserName;
	}
	public String getOrderClosedDate() {
		return CommonUtility.replaceNull(orderClosedDate);
	}
	public void setOrderClosedDate(String orderClosedDate) {
		this.orderClosedDate = orderClosedDate;
	}
	public String getTargeted() {
		return CommonUtility.replaceNull(targeted);
	}
	public void setTargeted(String targeted) {
		this.targeted = targeted;
	}
	public String getWinChance() {
		return CommonUtility.replaceNull(winChance);
	}
	public void setWinChance(String winChance) {
		this.winChance = winChance;
	}
	public String getDeliveryType() {
		return CommonUtility.replaceNull(deliveryType);
	}
	public void setDeliveryType(String deliveryType) {
		this.deliveryType = deliveryType;
	}
	public String getExDeliveryDate() {
		return CommonUtility.replaceNull(exDeliveryDate);
	}
	public void setExDeliveryDate(String exDeliveryDate) {
		this.exDeliveryDate = exDeliveryDate;
	}
	public String getTypeDesc() {
		return CommonUtility.replaceNull(typeDesc);
	}
	public void setTypeDesc(String typeDesc) {
		this.typeDesc = typeDesc;
	}
	public String getApplicationDesc() {
		return CommonUtility.replaceNull(applicationDesc);
	}
	public void setApplicationDesc(String applicationDesc) {
		this.applicationDesc = applicationDesc;
	}
	public String getEnclosureDesc() {
		return CommonUtility.replaceNull(enclosureDesc);
	}
	public void setEnclosureDesc(String enclosureDesc) {
		this.enclosureDesc = enclosureDesc;
	}
	public String getTotalMotorPrice() {
		return CommonUtility.replaceNull(totalMotorPrice);
	}
	public void setTotalMotorPrice(String totalMotorPrice) {
		this.totalMotorPrice = totalMotorPrice;
	}
	public String getTotalOrderPrice() {
		return CommonUtility.replaceNull(totalOrderPrice);
	}
	public void setTotalOrderPrice(String totalOrderPrice) {
		this.totalOrderPrice = totalOrderPrice;
	}
	public String getWinReason() {
		return CommonUtility.replaceNull(winReason);
	}
	public void setWinReason(String winReason) {
		this.winReason = winReason;
	}
	public String getLossReason1() {
		return CommonUtility.replaceNull(lossReason1);
	}
	public void setLossReason1(String lossReason1) {
		this.lossReason1 = lossReason1;
	}
	public String getLossReason2() {
		return CommonUtility.replaceNull(lossReason2);
	}
	public void setLossReason2(String lossReason2) {
		this.lossReason2 = lossReason2;
	}
	public String getCompetitor() {
		return CommonUtility.replaceNull(competitor);
	}
	public void setCompetitor(String competitor) {
		this.competitor = competitor;
	}
	public String getLossComment() {
		return CommonUtility.replaceNull(lossComment);
	}
	public void setLossComment(String lossComment) {
		this.lossComment = lossComment;
	}
	public String getWarrantyFromDispatch() {
		return CommonUtility.replaceNull(warrantyFromDispatch);
	}
	public void setWarrantyFromDispatch(String warrantyFromDispatch) {
		this.warrantyFromDispatch = warrantyFromDispatch;
	}
	public String getWarrantyFromComm() {
		return CommonUtility.replaceNull(warrantyFromComm);
	}
	public void setWarrantyFromComm(String warrantyFromComm) {
		this.warrantyFromComm = warrantyFromComm;
	}
	public String getSavingIndent() {
		return CommonUtility.replaceNull(savingIndent);
	}
	public void setSavingIndent(String savingIndent) {
		this.savingIndent = savingIndent;
	}
	public String getRatedOutputUnit() {
		return CommonUtility.replaceNull(ratedOutputUnit);
	}
	public void setRatedOutputUnit(String ratedOutputUnit) {
		this.ratedOutputUnit = ratedOutputUnit;
	}
	public String getApplicationId() {
		return CommonUtility.replaceNull(applicationId);
	}
	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}
	public String getSplFeatures() {
		return CommonUtility.replaceNull(splFeatures);
	}
	public void setSplFeatures(String splFeatures) {
		this.splFeatures = splFeatures;
	}
	public String getLd() {
		return CommonUtility.replaceNull(ld);
	}
	public void setLd(String ld) {
		this.ld = ld;
	}
	public String getApproval() {
		return CommonUtility.replaceNull(approval);
	}
	public void setApproval(String approval) {
		this.approval = approval;
	}
	public String getSmnote() {
		return CommonUtility.replaceNull(smnote);
	}
	public void setSmnote(String smnote) {
		this.smnote = smnote;
	}
	public int getScsrId() {
		return scsrId;
	}
	public void setScsrId(int scsrId) {
		this.scsrId = scsrId;
	}
	
	
	public int getEnclosureId() {
		return enclosureId;
	}

	public void setEnclosureId(int enclosureId) {
		this.enclosureId = enclosureId;
	}
	public int getPoleId() {
		return poleId;
	}
	public void setPoleId(int poleId) {
		this.poleId = poleId;
	}
	public String getOrderClsStartDate() {
		return CommonUtility.replaceNull(orderClsStartDate);
	}

	public void setOrderClsStartDate(String orderClsStartDate) {
		this.orderClsStartDate = orderClsStartDate;
	}

	public String getOrderClsEndDate() {
		return CommonUtility.replaceNull(orderClsEndDate);
	}

	public void setOrderClsEndDate(String orderClsEndDate) {
		this.orderClsEndDate = orderClsEndDate;
	}
	
	public String getIsNewEnquiry() {
		return CommonUtility.replaceNull(isNewEnquiry);
	}
	public void setIsNewEnquiry(String isNewEnquiry) {
		this.isNewEnquiry = isNewEnquiry;
	}
	
	
}
