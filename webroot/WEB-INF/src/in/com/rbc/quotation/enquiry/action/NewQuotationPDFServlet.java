package in.com.rbc.quotation.enquiry.action;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.utility.DBUtility;
import in.com.rbc.quotation.common.utility.DateUtility;
import in.com.rbc.quotation.common.utility.ExceptionUtility;
import in.com.rbc.quotation.common.utility.LoggerUtility;
import in.com.rbc.quotation.common.utility.NewQuotationPDFUtility;
import in.com.rbc.quotation.enquiry.dao.QuotationDaoImpl;
import in.com.rbc.quotation.enquiry.dto.NewEnquiryDto;

public class NewQuotationPDFServlet extends HttpServlet {

	/**
	 * Method to display image in jsp -- invokes doPost
	 * @param request HttpServletRequest object to handle request operations
	 * @param response HttpServletResponse object to handle response operations 
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
	
	/**
	 * Method displays the image in jsp
	 * @param request HttpServletRequest object to handle request operations
	 * @param response HttpServletResponse object to handle response operations
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// Connection object to store the database connection
	    Connection conn = null;
	    // Object of DBUtility class to handle DB connection objects
	    DBUtility oDBUtility = null;
        
	    ServletOutputStream oServletOutputStream  = null;
	    ServletOutputStream oServletTechnicalOutputStream  = null;
	    
	    NewEnquiryDto oNewEnquiryDto = null; 
	    int iEnquiryId =0;
	    String sFinalEnquiryId=null;
	    String sZIPFilename = null, sCommFileName = null, sTechFileName = null;
	    String sCustomer = null;
		String[] oCustomerArray = null;
		
		try{
			/* Instantiating the DBUtility object */
			oDBUtility = new DBUtility();
			/*Intioalization of oEnquiryDto object*/
			oNewEnquiryDto = new NewEnquiryDto();
			/*Getting Enquiry Id from Request/*/
			
			iEnquiryId = Integer.parseInt(request.getParameter(QuotationConstants.QUOTATION_REQUEST_ENQUIRYID));
			oNewEnquiryDto.setEnquiryId(iEnquiryId);
			
			// Set "isPdfView" true - To Fetch Remarks List
			oNewEnquiryDto.setPdfView(true);
			
			/* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
			
            oNewEnquiryDto = new QuotationDaoImpl().getNewEnquiryDetailsForView(conn, oNewEnquiryDto, QuotationConstants.QUOTATION_FORWARD_VIEW.toUpperCase());
			
            ByteArrayOutputStream oCommercialOutputStream = new NewQuotationPDFUtility().generateCommercialPDF(oNewEnquiryDto, request);
            ByteArrayOutputStream oTechnicalOutputStream = new NewQuotationPDFUtility().generateTechnicalPDF(oNewEnquiryDto, request);
            
            sCustomer = oNewEnquiryDto.getCustomerName();
            if(sCustomer!=null&&!sCustomer.equals("")){
            	oCustomerArray = sCustomer.split(" ");
            	if(oCustomerArray.length >= QuotationConstants.QUOTATION_LITERAL_2)
            		sCustomer = oCustomerArray[0]+" "+oCustomerArray[1];
            	else
            		sCustomer = oCustomerArray[0];
            } 
            
            sFinalEnquiryId=oNewEnquiryDto.getEnquiryNumber().substring(15);
            
            // COMMERCIAL PDF.
            sCommFileName = QuotationConstants.QUOTATION_PDF + sFinalEnquiryId 
            			+ QuotationConstants.QUOTATION_STRING_UNDERSC + sCustomer+QuotationConstants.QUOTATION_STRING_UNDERSC
            			+ DateUtility.getCurrentYear() + QuotationConstants.QUOTATION_STRING_UNDERSC 
            			+ DateUtility.getCurrentMonth()+ QuotationConstants.QUOTATION_STRING_UNDERSC 
            			+ QuotationConstants.QUOTATIONLT_PDFTYPE_COMMERCIAL + QuotationConstants.QUOTATION_PDFEXT;

            // TECHNICAL PDF.
	        sTechFileName = QuotationConstants.QUOTATION_PDF + sFinalEnquiryId 
        				+ QuotationConstants.QUOTATION_STRING_UNDERSC + sCustomer+QuotationConstants.QUOTATION_STRING_UNDERSC
        				+ DateUtility.getCurrentYear() + QuotationConstants.QUOTATION_STRING_UNDERSC 
        				+ DateUtility.getCurrentMonth()+ QuotationConstants.QUOTATION_STRING_UNDERSC 
        				+ QuotationConstants.QUOTATIONLT_PDFTYPE_TECHNICAL + QuotationConstants.QUOTATION_PDFEXT;
	        
	        // ZIP File Name.
	        sZIPFilename = QuotationConstants.QUOTATION_PDF + sFinalEnquiryId 
    						+ QuotationConstants.QUOTATION_STRING_UNDERSC + sCustomer+QuotationConstants.QUOTATION_STRING_UNDERSC
    						+ DateUtility.getCurrentYear() + QuotationConstants.QUOTATION_STRING_UNDERSC 
    						+ DateUtility.getCurrentMonth()+ QuotationConstants.QUOTATION_STRING_UNDERSC
    						+ "PDF_FILES" + QuotationConstants.QUOTATION_ZIP_EXT;
	        
            /* ####################################################     Setting the content type to ZIP     #####################################  */
            response.setContentType("Content-type: text/zip");
            response.setHeader("Content-disposition","attachment; filename=" +sZIPFilename);
			
            ServletOutputStream out = response.getOutputStream();
    		ZipOutputStream zos = new ZipOutputStream(new BufferedOutputStream(out));
    		
    		zos.putNextEntry(new ZipEntry(sCommFileName));
    		zos.write(oCommercialOutputStream.toByteArray());
    		zos.closeEntry();
    		
    		zos.putNextEntry(new ZipEntry(sTechFileName));
    		zos.write(oTechnicalOutputStream.toByteArray());
    		zos.closeEntry();
    		
    		zos.close();
            
		} catch(Exception e) {
            /* Logging any exception that raised during this activity in log files using Log4j */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "doPost", ExceptionUtility.getStackTraceAsString(e));
            e.printStackTrace();
		}  finally {
            /* Releasing/closing the connection object */
            try {
				oDBUtility.releaseResources(conn);
				
			} catch (SQLException e) {
                /* Logging any exception that raised during this activity in log files using Log4j */
                LoggerUtility.log("DEBUG", this.getClass().getName(), "doPost", ExceptionUtility.getStackTraceAsString(e));
			}
        }
	}
	
	
}
