/**
 * ******************************************************************************************
 * Project Name: Search For Quotation
 * Document Name: SearchEnquiryForm.java
 * Package: in.com.rbc.quotation.enquiry.form
 * Desc:  Form class that holds getters and setters of each property.
 * ******************************************************************************************
 * @author 100006126
 * ******************************************************************************************
 */
package in.com.rbc.quotation.enquiry.form;

import in.com.rbc.quotation.common.utility.CommonUtility;

import java.util.ArrayList;

import org.apache.struts.action.ActionForm;

public class SearchEnquiryForm extends ActionForm {
	private String enquiryId= null;
	private String enquiryNumber= null;
	private String createdStartDate= null;
	private String createdEndDate= null;
	private String customerName= null;
	private String createdBy= null;
	private String createdBySSO= null;

	private String assignTo= null;
	private String assignToSSO= null;

	private String createdDate= null;
	private String location= null;
	private String locationName= null;
	private String statusId= null;
	private String searchType= null;
	private String minVolts= null;
	private String maxVolts= null;
	
	private String invoke= null;
	private String dispatch= null;
	private String statusName= null;
	private String volts= null;
	private String frameName= null;
	private String poleName= null;
	private int scsrId = 0;
	private int enclosureId = 0;
	private int poleId = 0;
	
	private String kw= null;
	
	private String mounting= null;
	private ArrayList mountingList= null;
	private ArrayList statusList= null;
	private ArrayList locationList =  null;
	private ArrayList customerList = null;
	private ArrayList alScsrList = null;
	private ArrayList alVoltsList = null;
	private ArrayList alEnclosureList = null;
	private ArrayList alApplicationList = null;
	private ArrayList alIndustryList = null;
	private ArrayList alPoleList = null;
	
	
	private String customerId= null;
	private String locationId = "0";
	private String regionId= null;
	private String year= null;
	private String poleCheck = null;
	private String kwCheck = null;
	private String frameCheck = null;
	private String voltsCheck = null;
	private String mountingCheck = null;
	
	private String typeCheck = null;
	private String enclosureCheck = null;
	private String applicationCheck = null;

	private String month= null;
	private String designedBy=null; //This Variable will stores Design Manager or Design Engineer.
	private String designedBySSO=null; //This Variable will stores Design Manager SSOID or Design Engineer SSOIDS.

	//Extra Data Added By Egr on 20-March-2019s
	private String regionName=null;
	private String salesManagerName=null;
	private String enquiryType=null;
	private String customerType=null;
	private String enquiryReferenceNumber=null;
	private String projectName=null;
	private String endUserIndustry=null;
	private String endUserName=null;
	private String orderClosedDate=null;
	private String totalMotorPrice=null;
	private String totalOrderPrice=null;
	private String winReason=null;
	private String lossReason1=null;
	private String lossReason2=null;
	private String competitor=null;
	private String lossComment=null;
	private String warrantyFromDispatch=null;
	private String warrantyFromComm=null;
	private String savingIndent=null;
	private String ratedOutputUnit=null;
	private String applicationId=null;
	private String splFeatures=null;
	private String ld=null;
	private String approval=null;
	private String smnote = null;
	private String orderClsStartDate = null;
	private String orderClsEndDate = null;
	
	private ArrayList employeesList= null;
	
	

	/**
	 * @return Returns the createdBy.
	 */
	public String getCreatedBy() {
		return CommonUtility.replaceNull(createdBy);
	}

	/**
	 * @param createdBy The createdBy to set.
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return Returns the createdBySSO.
	 */
	public String getCreatedBySSO() {
		return CommonUtility.replaceNull(createdBySSO);
	}

	/**
	 * @param createdBySSO The createdBySSO to set.
	 */
	public void setCreatedBySSO(String createdBySSO) {
		this.createdBySSO = createdBySSO;
	}

	/**
	 * @return Returns the createdDate.
	 */
	public String getCreatedDate() {
		return CommonUtility.replaceNull(createdDate);
	}

	/**
	 * @param createdDate The createdDate to set.
	 */
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return Returns the createdEndDate.
	 */
	public String getCreatedEndDate() {
		return CommonUtility.replaceNull(createdEndDate);
	}

	/**
	 * @param createdEndDate The createdEndDate to set.
	 */
	public void setCreatedEndDate(String createdEndDate) {
		this.createdEndDate = createdEndDate;
	}

	/**
	 * @return Returns the createdStartDate.
	 */
	public String getCreatedStartDate() {
		return CommonUtility.replaceNull(createdStartDate);
	}

	/**
	 * @param createdStartDate The createdStartDate to set.
	 */
	public void setCreatedStartDate(String createdStartDate) {
		this.createdStartDate = createdStartDate;
	}

	/**
	 * @return Returns the customerId.
	 */
	public String getCustomerId() {
		return CommonUtility.replaceNull(customerId);
	}

	/**
	 * @param customerId The customerId to set.
	 */
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	/**
	 * @return Returns the customerList.
	 */
	public ArrayList getCustomerList() {
		return customerList;
	}

	/**
	 * @param customerList The customerList to set.
	 */
	public void setCustomerList(ArrayList customerList) {
		this.customerList = customerList;
	}

	/**
	 * @return Returns the customerName.
	 */
	public String getCustomerName() {
		return CommonUtility.replaceNull(customerName);
	}

	/**
	 * @param customerName The customerName to set.
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	/**
	 * @return Returns the dispatch.
	 */
	public String getDispatch() {
		return CommonUtility.replaceNull(dispatch);
	}

	/**
	 * @param dispatch The dispatch to set.
	 */
	public void setDispatch(String dispatch) {
		this.dispatch = dispatch;
	}

	/**
	 * @return Returns the enquiryId.
	 */
	public String getEnquiryId() {
		return CommonUtility.replaceNull(enquiryId);
	}

	/**
	 * @param enquiryId The enquiryId to set.
	 */
	public void setEnquiryId(String enquiryId) {
		this.enquiryId = enquiryId;
	}

	/**
	 * @return Returns the enquiryNumber.
	 */
	public String getEnquiryNumber() {
		return CommonUtility.replaceNull(enquiryNumber);
	}

	/**
	 * @param enquiryNumber The enquiryNumber to set.
	 */
	public void setEnquiryNumber(String enquiryNumber) {
		this.enquiryNumber = enquiryNumber;
	}

	/**
	 * @return Returns the frameCheck.
	 */
	public String getFrameCheck() {
		return CommonUtility.replaceNull(frameCheck);
	}

	/**
	 * @param frameCheck The frameCheck to set.
	 */
	public void setFrameCheck(String frameCheck) {
		this.frameCheck = frameCheck;
	}

	/**
	 * @return Returns the frameName.
	 */
	public String getFrameName() {
		return CommonUtility.replaceNull(frameName);
	}

	/**
	 * @param frameName The frameName to set.
	 */
	public void setFrameName(String frameName) {
		this.frameName = frameName;
	}

	/**
	 * @return Returns the invoke.
	 */
	public String getInvoke() {
		return CommonUtility.replaceNull(invoke);
	}

	/**
	 * @param invoke The invoke to set.
	 */
	public void setInvoke(String invoke) {
		this.invoke = invoke;
	}

	/**
	 * @return Returns the kw.
	 */
	public String getKw() {
		return CommonUtility.replaceNull(kw);
	}

	/**
	 * @param kw The kw to set.
	 */
	public void setKw(String kw) {
		this.kw = kw;
	}

	/**
	 * @return Returns the kwCheck.
	 */
	public String getKwCheck() {
		return CommonUtility.replaceNull(kwCheck);
	}

	/**
	 * @param kwCheck The kwCheck to set.
	 */
	public void setKwCheck(String kwCheck) {
		this.kwCheck = kwCheck;
	}

	/**
	 * @return Returns the location.
	 */
	public String getLocation() {
		return CommonUtility.replaceNull(location);
	}

	/**
	 * @param location The location to set.
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * @return Returns the locationId.
	 */
	public String getLocationId() {
		return CommonUtility.replaceNull(locationId);
	}

	/**
	 * @param locationId The locationId to set.
	 */
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	/**
	 * @return Returns the locationList.
	 */
	public ArrayList getLocationList() {
		return locationList;
	}

	/**
	 * @param locationList The locationList to set.
	 */
	public void setLocationList(ArrayList locationList) {
		this.locationList = locationList;
	}

	/**
	 * @return Returns the locationName.
	 */
	public String getLocationName() {
		return CommonUtility.replaceNull(locationName);
	}

	/**
	 * @param locationName The locationName to set.
	 */
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	/**
	 * @return Returns the maxVolts.
	 */
	public String getMaxVolts() {
		return CommonUtility.replaceNull(maxVolts);
	}

	/**
	 * @param maxVolts The maxVolts to set.
	 */
	public void setMaxVolts(String maxVolts) {
		this.maxVolts = maxVolts;
	}

	/**
	 * @return Returns the minVolts.
	 */
	public String getMinVolts() {
		return CommonUtility.replaceNull(minVolts);
	}

	/**
	 * @param minVolts The minVolts to set.
	 */
	public void setMinVolts(String minVolts) {
		this.minVolts = minVolts;
	}

	/**
	 * @return Returns the month.
	 */
	public String getMonth() {
		return CommonUtility.replaceNull(month);
	}

	/**
	 * @param month The month to set.
	 */
	public void setMonth(String month) {
		this.month = month;
	}

	/**
	 * @return Returns the mounting.
	 */
	public String getMounting() {
		return CommonUtility.replaceNull(mounting);
	}

	/**
	 * @param mounting The mounting to set.
	 */
	public void setMounting(String mounting) {
		this.mounting = mounting;
	}

	/**
	 * @return Returns the mountingCheck.
	 */
	public String getMountingCheck() {
		return CommonUtility.replaceNull(mountingCheck);
	}

	/**
	 * @param mountingCheck The mountingCheck to set.
	 */
	public void setMountingCheck(String mountingCheck) {
		this.mountingCheck = mountingCheck;
	}

	/**
	 * @return Returns the mountingList.
	 */
	public ArrayList getMountingList() {
		return mountingList;
	}

	/**
	 * @param mountingList The mountingList to set.
	 */
	public void setMountingList(ArrayList mountingList) {
		this.mountingList = mountingList;
	}

	/**
	 * @return Returns the poleCheck.
	 */
	public String getPoleCheck() {
		return CommonUtility.replaceNull(poleCheck);
	}

	/**
	 * @param poleCheck The poleCheck to set.
	 */
	public void setPoleCheck(String poleCheck) {
		this.poleCheck = poleCheck;
	}

	/**
	 * @return Returns the poleName.
	 */
	public String getPoleName() {
		return CommonUtility.replaceNull(poleName);
	}

	/**
	 * @param poleName The poleName to set.
	 */
	public void setPoleName(String poleName) {
		this.poleName = poleName;
	}

	/**
	 * @return Returns the regionId.
	 */
	public String getRegionId() {
		return CommonUtility.replaceNull(regionId);
	}

	/**
	 * @param regionId The regionId to set.
	 */
	public void setRegionId(String regionId) {
		this.regionId = regionId;
	}

	/**
	 * @return Returns the searchType.
	 */
	public String getSearchType() {
		return CommonUtility.replaceNull(searchType);
	}

	/**
	 * @param searchType The searchType to set.
	 */
	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	/**
	 * @return Returns the statusId.
	 */
	public String getStatusId() {
		return CommonUtility.replaceNull(statusId);
	}

	/**
	 * @param statusId The statusId to set.
	 */
	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}

	/**
	 * @return Returns the statusList.
	 */
	public ArrayList getStatusList() {
		return statusList;
	}

	/**
	 * @param statusList The statusList to set.
	 */
	public void setStatusList(ArrayList statusList) {
		this.statusList = statusList;
	}

	/**
	 * @return Returns the statusName.
	 */
	public String getStatusName() {
		return CommonUtility.replaceNull(statusName);
	}

	/**
	 * @param statusName The statusName to set.
	 */
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	/**
	 * @return Returns the volts.
	 */
	public String getVolts() {
		return CommonUtility.replaceNull(volts);
	}

	/**
	 * @param volts The volts to set.
	 */
	public void setVolts(String volts) {
		this.volts = volts;
	}

	/**
	 * @return Returns the voltsCheck.
	 */
	public String getVoltsCheck() {
		return CommonUtility.replaceNull(voltsCheck);
	}

	/**
	 * @param voltsCheck The voltsCheck to set.
	 */
	public void setVoltsCheck(String voltsCheck) {
		this.voltsCheck = voltsCheck;
	}

	/**
	 * @return Returns the year.
	 */
	public String getYear() {
		return CommonUtility.replaceNull(year);
	}

	/**
	 * @param year The year to set.
	 */
	public void setYear(String year) {
		this.year = year;
	}

	public ArrayList getEmployeesList() {
		return employeesList;
	}

	public void setEmployeesList(ArrayList employeesList) {
		this.employeesList = employeesList;
	}

	public String getAssignTo() {
		return CommonUtility.replaceNull(assignTo);
	}

	public void setAssignTo(String assignTo) {
		this.assignTo = assignTo;
	}

	public String getAssignToSSO() {
		return CommonUtility.replaceNull(assignToSSO);
	}

	public void setAssignToSSO(String assignToSSO) {
		this.assignToSSO = assignToSSO;
	}
	
	/**
	 * @return Returns the designedBy.
	 */

	public String getDesignedBy() {
		return CommonUtility.replaceNull(designedBy);
	}
	/**
	 * @param designedBy The designedBy to set.
	 */

	public void setDesignedBy(String designedBy) {
		this.designedBy = designedBy;
	}
	/**
	 * @return Returns the designedBySSO.
	 */

	public String getDesignedBySSO() {
		return CommonUtility.replaceNull(designedBySSO);
	}
	/**
	 * @param designedBySSO The designedBySSO to set.
	 */

	public void setDesignedBySSO(String designedBySSO) {
		this.designedBySSO = designedBySSO;
	}

	public String getRegionName() {
		return CommonUtility.replaceNull(regionName);
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	public String getSalesManagerName() {
		return CommonUtility.replaceNull(salesManagerName);
	}

	public void setSalesManagerName(String salesManagerName) {
		this.salesManagerName = salesManagerName;
	}

	public String getEnquiryType() {
		return CommonUtility.replaceNull(enquiryType);
	}

	public void setEnquiryType(String enquiryType) {
		this.enquiryType = enquiryType;
	}

	public String getCustomerType() {
		return CommonUtility.replaceNull(customerType);
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	
	public String getEnquiryReferenceNumber() {
		return CommonUtility.replaceNull(enquiryReferenceNumber);
	}
	public void setEnquiryReferenceNumber(String enquiryReferenceNumber) {
		this.enquiryReferenceNumber = enquiryReferenceNumber;
	}
	public String getProjectName() {
		return CommonUtility.replaceNull(projectName);
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getEndUserIndustry() {
		return CommonUtility.replaceNull(endUserIndustry);
	}
	public void setEndUserIndustry(String endUserIndustry) {
		this.endUserIndustry = endUserIndustry;
	}

	public String getEndUserName() {
		return CommonUtility.replaceNull(endUserName);
	}
	public void setEndUserName(String endUserName) {
		this.endUserName = endUserName;
	}
	public String getOrderClosedDate() {
		return CommonUtility.replaceNull(orderClosedDate);
	}
	public void setOrderClosedDate(String orderClosedDate) {
		this.orderClosedDate = orderClosedDate;
	}

	public String getTotalMotorPrice() {
		return CommonUtility.replaceNull(totalMotorPrice);
	}

	public void setTotalMotorPrice(String totalMotorPrice) {
		this.totalMotorPrice = totalMotorPrice;
	}

	public String getTotalOrderPrice() {
		return CommonUtility.replaceNull(totalOrderPrice);
	}

	public void setTotalOrderPrice(String totalOrderPrice) {
		this.totalOrderPrice = totalOrderPrice;
	}

	public String getWinReason() {
		return CommonUtility.replaceNull(winReason);
	}

	public void setWinReason(String winReason) {
		this.winReason = winReason;
	}

	public String getLossReason1() {
		return CommonUtility.replaceNull(lossReason1);
	}

	public void setLossReason1(String lossReason1) {
		this.lossReason1 = lossReason1;
	}

	public String getLossReason2() {
		return CommonUtility.replaceNull(lossReason2);
	}

	public void setLossReason2(String lossReason2) {
		this.lossReason2 = lossReason2;
	}

	public String getCompetitor() {
		return CommonUtility.replaceNull(competitor);
	}

	public void setCompetitor(String competitor) {
		this.competitor = competitor;
	}

	public String getLossComment() {
		return CommonUtility.replaceNull(lossComment);
	}

	public void setLossComment(String lossComment) {
		this.lossComment = lossComment;
	}

	public String getWarrantyFromDispatch() {
		return CommonUtility.replaceNull(warrantyFromDispatch);
	}

	public void setWarrantyFromDispatch(String warrantyFromDispatch) {
		this.warrantyFromDispatch = warrantyFromDispatch;
	}

	public String getWarrantyFromComm() {
		return CommonUtility.replaceNull(warrantyFromComm);
	}

	public void setWarrantyFromComm(String warrantyFromComm) {
		this.warrantyFromComm = warrantyFromComm;
	}

	public String getSavingIndent() {
		return CommonUtility.replaceNull(savingIndent);
	}

	public void setSavingIndent(String savingIndent) {
		this.savingIndent = savingIndent;
	}

	public String getTypeCheck() {
		return CommonUtility.replaceNull(typeCheck);
	}

	public void setTypeCheck(String typeCheck) {
		this.typeCheck = typeCheck;
	}

	public String getEnclosureCheck() {
		return CommonUtility.replaceNull(enclosureCheck);
	}

	public void setEnclosureCheck(String enclosureCheck) {
		this.enclosureCheck = enclosureCheck;
	}

	public String getApplicationCheck() {
		return CommonUtility.replaceNull(applicationCheck);
	}

	public void setApplicationCheck(String applicationCheck) {
		this.applicationCheck = applicationCheck;
	}

	public ArrayList getAlScsrList() {
		return alScsrList;
	}

	public void setAlScsrList(ArrayList alScsrList) {
		this.alScsrList = alScsrList;
	}

	public ArrayList getAlVoltsList() {
		return alVoltsList;
	}

	public void setAlVoltsList(ArrayList alVoltsList) {
		this.alVoltsList = alVoltsList;
	}

	public ArrayList getAlEnclosureList() {
		return alEnclosureList;
	}

	public void setAlEnclosureList(ArrayList alEnclosureList) {
		this.alEnclosureList = alEnclosureList;
	}

	public ArrayList getAlApplicationList() {
		return alApplicationList;
	}

	public void setAlApplicationList(ArrayList alApplicationList) {
		this.alApplicationList = alApplicationList;
	}

	public ArrayList getAlIndustryList() {
		return alIndustryList;
	}

	public void setAlIndustryList(ArrayList alIndustryList) {
		this.alIndustryList = alIndustryList;
	}

	public String getRatedOutputUnit() {
		return CommonUtility.replaceNull(ratedOutputUnit);
	}

	public void setRatedOutputUnit(String ratedOutputUnit) {
		this.ratedOutputUnit = ratedOutputUnit;
	}

	public String getApplicationId() {
		return CommonUtility.replaceNull(applicationId);
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getSplFeatures() {
		return CommonUtility.replaceNull(splFeatures);
	}

	public void setSplFeatures(String splFeatures) {
		this.splFeatures = splFeatures;
	}

	public String getLd() {
		return CommonUtility.replaceNull(ld);
	}

	public void setLd(String ld) {
		this.ld = ld;
	}

	public String getApproval() {
		return CommonUtility.replaceNull(approval);
	}

	public void setApproval(String approval) {
		this.approval = approval;
	}

	public String getSmnote() {
		return CommonUtility.replaceNull(smnote);
	}

	public void setSmnote(String smnote) {
		this.smnote = smnote;
	}

	public int getScsrId() {
		return scsrId;
	}
	public void setScsrId(int scsrId) {
		this.scsrId = scsrId;
	}

	public int getEnclosureId() {
		return enclosureId;
	}

	public void setEnclosureId(int enclosureId) {
		this.enclosureId = enclosureId;
	}
	public int getPoleId() {
		return poleId;
	}
	public void setPoleId(int poleId) {
		this.poleId = poleId;
	}

	public ArrayList getAlPoleList() {
		return alPoleList;
	}

	public void setAlPoleList(ArrayList alPoleList) {
		this.alPoleList = alPoleList;
	}

	public String getOrderClsStartDate() {
		return CommonUtility.replaceNull(orderClsStartDate);
	}

	public void setOrderClsStartDate(String orderClsStartDate) {
		this.orderClsStartDate = orderClsStartDate;
	}

	public String getOrderClsEndDate() {
		return CommonUtility.replaceNull(orderClsEndDate);
	}

	public void setOrderClsEndDate(String orderClsEndDate) {
		this.orderClsEndDate = orderClsEndDate;
	}
	
	
}
