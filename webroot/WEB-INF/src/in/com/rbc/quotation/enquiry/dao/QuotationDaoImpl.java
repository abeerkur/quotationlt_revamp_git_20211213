/**
 * ******************************************************************************************

 * Project Name: Request For Quotation
 * Document Name: QuotationDaoImpl.java
 * Package: in.com.rbc.quotation.enquiry.dao
 * Desc:  Dao Implementation class that implements all the methods defined in
 *        Quatation interface and those related to Quatation functionalities. <br>
 *        This class implements the interfaces --> QuotationDao, EnquiryQueries
 * ******************************************************************************************
 * @author 100006126
 * ******************************************************************************************
 */
package in.com.rbc.quotation.enquiry.dao;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import in.com.rbc.quotation.admin.dao.AdminQueries;
import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.utility.CommonUtility;
import in.com.rbc.quotation.common.utility.CurrencyConvertUtility;
import in.com.rbc.quotation.common.utility.DBUtility;
import in.com.rbc.quotation.common.utility.DateUtility;
import in.com.rbc.quotation.common.utility.ExceptionUtility;
import in.com.rbc.quotation.common.utility.LoggerUtility;
import in.com.rbc.quotation.common.utility.PropertyUtility;
import in.com.rbc.quotation.common.vo.KeyValueVo;
import in.com.rbc.quotation.enquiry.dto.AddOnDto;
import in.com.rbc.quotation.enquiry.dto.DeliveryDetailsDto;
import in.com.rbc.quotation.enquiry.dto.EnquiryDto;
import in.com.rbc.quotation.enquiry.dto.NewEnquiryDto;
import in.com.rbc.quotation.enquiry.dto.NewMTORatingDto;
import in.com.rbc.quotation.enquiry.dto.NewRatingDto;
import in.com.rbc.quotation.enquiry.dto.NewTechnicalOfferDto;
import in.com.rbc.quotation.enquiry.dto.RatingDto;
import in.com.rbc.quotation.enquiry.dto.RemarksDto;
import in.com.rbc.quotation.enquiry.dto.TeamMemberDto;
import in.com.rbc.quotation.enquiry.dto.TechnicalOfferDto;
import in.com.rbc.quotation.enquiry.dto.UserDetailsDto;
import in.com.rbc.quotation.user.dto.UserDto;
import in.com.rbc.quotation.workflow.dto.WorkflowDto;

public class QuotationDaoImpl implements QuotationDao, EnquiryQueries {
	
	/* (non-Javadoc)
     * @see in.com.rbc.quotation.enquiry.dto.EnquiryDto#createEnquiry(java.sql.Connection, in.com.rbc.quotation.enquiry.dto.EnquiryDto, java.lang.String)  
     */
	 public EnquiryDto createEnquiry(Connection conn, EnquiryDto oEnquiryDto, String sOperationType) throws Exception{
		
		 
		 String sMethodName = "createEnquiry";
		 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		 /* PreparedStatement objects to handle database operations */
	     PreparedStatement pstmt1 = null;
	     PreparedStatement pstmt2 = null;
	     
	     
	   
	     ResultSet rs1 = null;   
	    
	     /* Object of DBUtility class to handle database operations */
	     DBUtility oDBUtility = new DBUtility();
	        
	     int iEnquiryId = 0;
	     String sEnquiryCode = ""; 
	     int isInserted = 0;
	     StringTokenizer stToken = null;
	     
	     try{
	    	 oEnquiryDto.setLocationId(Integer.parseInt(PropertyUtility.getKeyValue(
	    			 QuotationConstants.QUOTATION_PROPERTYFILENAME,
	    			 QuotationConstants.QUOTATION_DEFAULT_LOCATION)));
	    	 
	    	 
	    	 
	    	 if (sOperationType.equalsIgnoreCase("INSERT")){
	    		 pstmt1 = conn.prepareStatement(FETCH_ENQUIRYID_NEXTVAL);
	    		 pstmt1.setString(1,oEnquiryDto.getCreatedBySSO());
	    		 pstmt1.setInt(2, oEnquiryDto.getRevisionNumber());
	    		 rs1 = pstmt1.executeQuery();
	    		 
	    
	    		 if (rs1.next()) {
	    			 if (rs1.getString("ENQUIRYNO") != null){
	    				 stToken = new StringTokenizer(rs1.getString("ENQUIRYNO"), "^");
	    				 if (stToken != null && stToken.hasMoreTokens()){
	    					 iEnquiryId = Integer.parseInt(stToken.nextToken());
	    					 if (stToken.hasMoreTokens())
	    						 sEnquiryCode = stToken.nextToken();
	    				 }
	    			 }
	    		 }	 
	    	 
	    	 }else{
	    		 iEnquiryId = oEnquiryDto.getEnquiryId();
	    	 }


	    	 if (iEnquiryId > 0){
	    		 if (oEnquiryDto.getRevisionNumber() == 0)
	    			 oEnquiryDto.setRevisionNumber(1);
	    		 
	    		 if (sOperationType.equalsIgnoreCase("INSERT")){
	    			 pstmt2 = conn.prepareStatement(INSERT_ENQUIRY_DETAILS);
	    			 
	    			 oEnquiryDto.setEnquiryNumber(sEnquiryCode);
	    			 oEnquiryDto.setEnquiryId(iEnquiryId);     	
	    			 
	    			 pstmt2.setInt(1, iEnquiryId);
	    			 pstmt2.setString(2, sEnquiryCode);
	    			 
	    			 pstmt2.setInt(3, oEnquiryDto.getRevisionNumber());
	    			 pstmt2.setInt(4, oEnquiryDto.getCustomerId());
	    			 pstmt2.setString(5, oEnquiryDto.getCustomerType());
	    			 pstmt2.setString(6, oEnquiryDto.getEndUser());	    			 
	    			 pstmt2.setString(7, oEnquiryDto.getOem());
	    			 pstmt2.setString(8, oEnquiryDto.getEpc());
	    			 pstmt2.setString(9, oEnquiryDto.getDealer());
	    			 pstmt2.setString(10, oEnquiryDto.getProjectName());	    			 
	    			 pstmt2.setString(11, oEnquiryDto.getIndustry());
	    			 pstmt2.setString(12, oEnquiryDto.getEndUserCountry());
	    			 pstmt2.setString(13, oEnquiryDto.getEndUserIndustry());
	    			 pstmt2.setString(14, oEnquiryDto.getApproval());	    			 
	    			 pstmt2.setString(15, oEnquiryDto.getTargeted());
	    			 pstmt2.setString(16, oEnquiryDto.getWinningChance());
	    			 pstmt2.setString(17, oEnquiryDto.getCompetitor());
	    			 pstmt2.setString(18, oEnquiryDto.getSalesStage());
	    			 pstmt2.setString(19, oEnquiryDto.getTotalOrderValue());
	    			 pstmt2.setString(20, oEnquiryDto.getOrderClosingMonth());	    			 
	    			 pstmt2.setString(21, oEnquiryDto.getExpectedDeliveryMonth());
	    			 pstmt2.setString(22, oEnquiryDto.getWarrantyDispatchDate());
	    			 pstmt2.setString(23, oEnquiryDto.getWarrantyCommissioningDate());
	    			 pstmt2.setString(24, oEnquiryDto.getDeliveryType());
	    			 pstmt2.setString(25, oEnquiryDto.getDestState());
	    			 pstmt2.setString(26, oEnquiryDto.getDestCity());	    			 
	    			 pstmt2.setString(27, oEnquiryDto.getSpa());
	    			 pstmt2.setDouble(28, oEnquiryDto.getCustomerCounterOffer());	    			 
	    			 pstmt2.setString(29, oEnquiryDto.getMaterialCost());
	    			 pstmt2.setString(30, oEnquiryDto.getMcnspRatio());
	    			 pstmt2.setDouble(31, oEnquiryDto.getMotorPrice());
	    			 pstmt2.setString(32, oEnquiryDto.getWarrantyCost());	    			 
	    			 pstmt2.setString(33, oEnquiryDto.getTransportationCost());
	    			 pstmt2.setString(34, oEnquiryDto.getEcSupervisionCost());	    			 
	    			 pstmt2.setString(35, oEnquiryDto.getHzAreaCertCost());	    			 
	    			 pstmt2.setString(36, oEnquiryDto.getSparesCost());
	    			 pstmt2.setDouble(37, oEnquiryDto.getTotalCost());	    			 
	    			 pstmt2.setString(38, oEnquiryDto.getPropSpaReason());
	    			 pstmt2.setString(39, oEnquiryDto.getRequestRemarks());	    			 
	    			 pstmt2.setString(40, oEnquiryDto.getWinlossReason());
	    			 pstmt2.setString(41, oEnquiryDto.getWinlossComment());
	    			 pstmt2.setString(42, oEnquiryDto.getWinningCompetitor());	    			 
	    			 pstmt2.setString(43, oEnquiryDto.getWinlossPrice());
	    			 pstmt2.setDouble(44, oEnquiryDto.getUnitMC());
	    			 pstmt2.setDouble(45, oEnquiryDto.getUnitPrice());
	    			 pstmt2.setString(46, oEnquiryDto.getTotalPrice());
	    			 pstmt2.setDouble(47, oEnquiryDto.getTotalMC());	    			 
	    			 pstmt2.setString(48, oEnquiryDto.getTotalPkgPrice());
	    			 pstmt2.setString(49, oEnquiryDto.getEnquiryReferenceNumber());
	    			 pstmt2.setString(50, oEnquiryDto.getEnquiryType());
	    			 pstmt2.setString(51, oEnquiryDto.getConsultantName());	    			
	    			 pstmt2.setString(52, oEnquiryDto.getDescription().trim());
	    			 pstmt2.setInt(53,1);
	    			 pstmt2.setString(54, oEnquiryDto.getCreatedBySSO());
	    			 pstmt2.setString(55, oEnquiryDto.getCreatedBySSO());     
	    			 pstmt2.setInt(56, iEnquiryId);    
	    			 pstmt2.setInt(57, oEnquiryDto.getLocationId());
	    			 pstmt2.setString(58, oEnquiryDto.getAdvancePayment());
	    			 pstmt2.setString(59, oEnquiryDto.getPaymentTerms());
	    			 pstmt2.setString(60, oEnquiryDto.getSmnote());
	    			 pstmt2.setString(61, oEnquiryDto.getCreatedBy()); 		
	    			 
	    			 isInserted = pstmt2.executeUpdate();     		 
	    			 if (isInserted == 0)
	    				 iEnquiryId = 0;
	    		 }else{
	    			 
	    			 pstmt2 = conn.prepareStatement(UPDATE_ENQUIRY_DETAILS);
	    			 
	    			 pstmt2.setInt(1, oEnquiryDto.getRevisionNumber());
	    			 pstmt2.setInt(2, oEnquiryDto.getCustomerId());
	    			 pstmt2.setString(3, oEnquiryDto.getCustomerType());
	    			 pstmt2.setString(4, oEnquiryDto.getEndUser());	    			 
	    			 pstmt2.setString(5, oEnquiryDto.getOem());
	    			 pstmt2.setString(6, oEnquiryDto.getEpc());
	    			 pstmt2.setString(7, oEnquiryDto.getDealer());
	    			 pstmt2.setString(8, oEnquiryDto.getProjectName());	    			 
	    			 pstmt2.setString(9, oEnquiryDto.getIndustry());
	    			 pstmt2.setString(10, oEnquiryDto.getEndUserCountry());
	    			 pstmt2.setString(11, oEnquiryDto.getEndUserIndustry());
	    			 pstmt2.setString(12, oEnquiryDto.getApproval());	    			 
	    			 pstmt2.setString(13, oEnquiryDto.getTargeted());
	    			 pstmt2.setString(14, oEnquiryDto.getWinningChance());
	    			 pstmt2.setString(15, oEnquiryDto.getCompetitor());
	    			 pstmt2.setString(16, oEnquiryDto.getSalesStage());
	    			 pstmt2.setString(17, oEnquiryDto.getTotalOrderValue());
	    			 pstmt2.setString(18, oEnquiryDto.getOrderClosingMonth());	    			 
	    			 pstmt2.setString(19, oEnquiryDto.getExpectedDeliveryMonth());
	    			 pstmt2.setString(20, oEnquiryDto.getWarrantyDispatchDate());
	    			 pstmt2.setString(21, oEnquiryDto.getWarrantyCommissioningDate());
	    			 pstmt2.setString(22, oEnquiryDto.getDeliveryType());
	    			 pstmt2.setString(23, oEnquiryDto.getDestState());
	    			 pstmt2.setString(24, oEnquiryDto.getDestCity());	    			 
	    			 pstmt2.setString(25, oEnquiryDto.getSpa());
	    			 pstmt2.setDouble(26, oEnquiryDto.getCustomerCounterOffer());	    			 
	    			 pstmt2.setString(27, oEnquiryDto.getMaterialCost());
	    			 pstmt2.setString(28, oEnquiryDto.getMcnspRatio());
	    			 pstmt2.setDouble(29, oEnquiryDto.getMotorPrice());
	    			 pstmt2.setString(30, oEnquiryDto.getWarrantyCost());	    			 
	    			 pstmt2.setString(31, oEnquiryDto.getTransportationCost());
	    			 pstmt2.setString(32, oEnquiryDto.getEcSupervisionCost());	    			 
	    			 pstmt2.setString(33, oEnquiryDto.getHzAreaCertCost());
	    			 pstmt2.setString(34, oEnquiryDto.getSparesCost());
	    			 pstmt2.setDouble(35, oEnquiryDto.getTotalCost());	    			 
	    			 pstmt2.setString(36, oEnquiryDto.getPropSpaReason());
	    			 pstmt2.setString(37, oEnquiryDto.getRequestRemarks());	    			 
	    			 pstmt2.setString(38, oEnquiryDto.getWinlossReason());
	    			 pstmt2.setString(39, oEnquiryDto.getWinlossComment());
	    			 pstmt2.setString(40, oEnquiryDto.getWinningCompetitor());	    			 
	    			 pstmt2.setString(41, oEnquiryDto.getWinlossPrice());
	    			 pstmt2.setDouble(42, oEnquiryDto.getUnitMC());
	    			 pstmt2.setDouble(43, oEnquiryDto.getUnitPrice());
	    			 pstmt2.setString(44, oEnquiryDto.getTotalPrice());
	    			 pstmt2.setDouble(45, oEnquiryDto.getTotalMC());	    			 
	    			 pstmt2.setString(46, oEnquiryDto.getTotalPkgPrice());
	    			 pstmt2.setString(47, oEnquiryDto.getEnquiryReferenceNumber());
	    			 pstmt2.setString(48, oEnquiryDto.getEnquiryType());
	    			 pstmt2.setString(49, oEnquiryDto.getConsultantName());	    			
	    			 pstmt2.setString(50, oEnquiryDto.getDescription().trim());	    			 
	    			 pstmt2.setString(51, oEnquiryDto.getCreatedBySSO());
	    			 pstmt2.setInt(52, oEnquiryDto.getLocationId());
	    			 pstmt2.setString(53, oEnquiryDto.getAdvancePayment());
	    			 pstmt2.setString(54, oEnquiryDto.getPaymentTerms());
	    			 pstmt2.setString(55, oEnquiryDto.getSmnote());
	    			 pstmt2.setString(56, oEnquiryDto.getCreatedBy());	
	    			 pstmt2.setInt(57, oEnquiryDto.getEnquiryId());
	    			 
	    			 isInserted = pstmt2.executeUpdate();  
	    			 
	    			 if (isInserted == 0)
	    				 iEnquiryId = 0;	    			 
	    		 }
	    	 }
     	 }finally {
             /* Releasing PreparedStatment objects */
             oDBUtility.releaseResources(rs1, pstmt1);
             oDBUtility.releaseResources(pstmt2);           
         }
     	 
		 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
		 return oEnquiryDto;
	 }
	 
	 /* (non-Javadoc)
      * @see in.com.rbc.quotation.enquiry.dto.RatingDto#createRatingy(java.sql.Connection, in.com.rbc.quotation.enquiry.dto.RatingDto, java.lang.String) 
      */
	 public RatingDto createRating(Connection conn, RatingDto oRatingDto, String sOperationType) throws Exception{
		 String sMethodName = "createRating";
		 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		 
         /* PreparedStatement objects to handle database operations */
	     PreparedStatement pstmt1 = null;
	     PreparedStatement pstmt2 = null;
         ResultSet rs1 = null;
         
	     if (sOperationType.equalsIgnoreCase("") || sOperationType.trim().length() < 1)
	    	 sOperationType ="INSERT"; 
	   
	     /* Object of DBUtility class to handle database operations */
	     DBUtility oDBUtility = new DBUtility();
	     
	     int iRatingId = 0;
	     int isInserted = 0;
	     String sSQLInsertQuery = null;
	     
	     try{
	    	 if (sOperationType.equalsIgnoreCase("INSERT")){
	    		 pstmt1 = conn.prepareStatement(FETCH_RATINGID_NEXTVAL);	    
	    		 rs1 = pstmt1.executeQuery();
	    		 if (rs1.next()) {
	    			 iRatingId = Integer.parseInt(rs1.getString("RATING_ID"));
	    		 }
	    	 }else{
	    		 iRatingId = oRatingDto.getRatingId();
	    	 }
	    	 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "sRatingId -  "+iRatingId);	
	    	 
	    	 if (iRatingId > 0){
	    		 if (sOperationType.equalsIgnoreCase("INSERT"))
	    			 sSQLInsertQuery = buildRatingInsertQuery(oRatingDto, iRatingId);
	    		 else
	    			 sSQLInsertQuery = buildRatingUpdateQuery(oRatingDto, iRatingId);
	    		 
	    		 if (sSQLInsertQuery != null && sSQLInsertQuery.trim().length() > 0){
	    			 pstmt2 = conn.prepareStatement(sSQLInsertQuery);	    			
	    			 isInserted = pstmt2.executeUpdate();    
		    		 if (isInserted == 0)
		    			 iRatingId =0; 
	    		 }
	    	 }
	     }finally {
             /* Releasing PreparedStatment objects */
             oDBUtility.releaseResources(rs1, pstmt1);
             oDBUtility.releaseResources(pstmt2);           
         }   
	     
	     oRatingDto.setRatingId(iRatingId);
		 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
		 return oRatingDto;
	 }
	 
	 /* (non-Javadoc)
      * @see java.lang.String#buildRatingInsertQuery(in.com.rbc.quotation.enquiry.dto.RatingDto, int) 
      */
	 private String buildRatingInsertQuery(RatingDto oRatingDto, int iRatingId){
		 String sMethodName = "buildRatingInsertQuery";
		 String sInsertQuery = null;
		 
		 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		 
		 StringBuffer sbFieldQuery = new StringBuffer();
		 StringBuffer sbValuesQuery = new StringBuffer();
		 
		 if (oRatingDto != null){
			 sbFieldQuery.append(INSERT_RATING_DETAILS);
			 
			 sbFieldQuery.append("(");
			 sbValuesQuery.append(" VALUES (");
			 
			 /* Rating ID */
			 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "RatingId -  "+iRatingId);
			 sbFieldQuery.append("QRD_RATINGID, ");
			 sbValuesQuery.append(iRatingId+", ");
			 
			 /* Enquiry Id */
			 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "Enquiry Id -  "+oRatingDto.getEnquiryId());
			 sbFieldQuery.append("QRD_ENQUIRYID, ");
			 sbValuesQuery.append(oRatingDto.getEnquiryId()+", ");
			 
			 /*QRD_RATINGNO */
			 //TODO : If not required then the above field has to be removed from the database and later from the code
			 sbFieldQuery.append("QRD_RATINGNO, ");
			 sbValuesQuery.append(oRatingDto.getRatingNo()+", ");
			 
			 /* QRD_QUANTITY */			 
			 sbFieldQuery.append("QRD_QUANTITY, ");
			 sbValuesQuery.append(oRatingDto.getQuantity()+", ");
			 
			 // Fields from here on are optional fields, so include only when they are not empty
			 
			 /* QRD_HTLTID */
			 if (oRatingDto.getHtltId() > 0){
				sbFieldQuery.append("QRD_HTLTID, ");
				sbValuesQuery.append(oRatingDto.getHtltId()+", ");				 
			 }
			 
			 /* QRD_POLEID */
			 if (oRatingDto.getPoleId() > 0){
				sbFieldQuery.append("QRD_POLEID, ");
				sbValuesQuery.append(oRatingDto.getPoleId()+", ");				 
			 }
			 
			 /* QRD_SCSRID */
			 if (oRatingDto.getScsrId() >0){ 
				sbFieldQuery.append("QRD_SCSRID, ");
				sbValuesQuery.append(oRatingDto.getScsrId()+", ");				 
			 }
			 
			 /* QRD_DUTYID */
			 if (oRatingDto.getDutyId() >0){ 
				sbFieldQuery.append("QRD_DUTYID, ");
				sbValuesQuery.append(oRatingDto.getDutyId()+", ");				 
			 }
			 
			 /* QRD_DEGREEOFPROID */
			 if (oRatingDto.getDegreeOfProId() >0){ 
				sbFieldQuery.append("QRD_DEGREEOFPROID, ");
				sbValuesQuery.append(oRatingDto.getDegreeOfProId()+", ");				 
			 }
			 
			 /* QRD_METHODOFSTG */
			 if (oRatingDto.getMethodOfStg() != null && oRatingDto.getMethodOfStg().trim().length() >0){ 
				sbFieldQuery.append("QRD_METHODOFSTG, ");
				sbValuesQuery.append("'"+oRatingDto.getMethodOfStg()+"', ");				 
			 }
			 
			 /* QRD_COUPLING */
			 if (oRatingDto.getCoupling() != null && oRatingDto.getCoupling().trim().length() > 0){
				sbFieldQuery.append("QRD_COUPLING, ");
				sbValuesQuery.append("'"+oRatingDto.getCoupling()+"', ");				 
			 }
			 
			 /* QRD_TERMBOXID */
			 if (oRatingDto.getTermBoxId() >0){ 
				sbFieldQuery.append("QRD_TERMBOXID, ");
				sbValuesQuery.append(oRatingDto.getTermBoxId()+", ");				 
			 }
			 
			 /* QRD_DIRECTIONOFROTID */
			 if (oRatingDto.getDirectionOfRotId() > 0){
				sbFieldQuery.append("QRD_DIRECTIONOFROTID, ");
				sbValuesQuery.append(oRatingDto.getDirectionOfRotId()+", ");				 
			 }
			 
			 /* QRD_SHAFTEXTID */
			 if (oRatingDto.getShaftExtId() > 0){
				sbFieldQuery.append("QRD_SHAFTEXTID, ");
				sbValuesQuery.append(oRatingDto.getShaftExtId()+", ");				 
			 }
			 
			 /* QRD_APPLICATIONID */
			 if (oRatingDto.getApplicationId() > 0){
				sbFieldQuery.append("QRD_APPLICATIONID, ");
				sbValuesQuery.append(oRatingDto.getApplicationId()+", ");				 
			 }
			 
			 /* QRD_KW */
			 if (oRatingDto.getKw() != null && oRatingDto.getKw().trim().length() >0){ 
				sbFieldQuery.append("QRD_KW, ");
				sbValuesQuery.append("'"+oRatingDto.getKw()+"', ");				 
			 }
			 
			 if (oRatingDto.getRatedOutputUnit() != null && oRatingDto.getRatedOutputUnit().trim().length() >0){ 
					sbFieldQuery.append("QRD_ROUNIT, ");
					sbValuesQuery.append("'"+oRatingDto.getRatedOutputUnit()+"', ");				 
				 }
			 
			 /* QRD_FREQUENCY */
			 if (oRatingDto.getFrequency() != null && oRatingDto.getFrequency().trim().length() > 0){
				sbFieldQuery.append("QRD_FREQUENCY, ");
				sbValuesQuery.append("'"+oRatingDto.getFrequency()+"', ");				 
			 }
			 
			 /* QRD_FREQUENCYVAR */
			 if (oRatingDto.getFrequencyVar() != null && oRatingDto.getFrequencyVar().trim().length() > 0){
				sbFieldQuery.append("QRD_FREQUENCYVAR, ");
				sbValuesQuery.append("'"+oRatingDto.getFrequencyVar()+"', ");				 
			 }
			 
			 /* QRD_VOLTS */
			 if (oRatingDto.getVolts() != null && oRatingDto.getVolts().trim().length() > 0){
				sbFieldQuery.append("QRD_VOLTS, ");
				sbValuesQuery.append("'"+oRatingDto.getVolts()+"', ");				 
			 }
			 
			 /* QRD_VOLTSVAR */
			 if (oRatingDto.getVoltsVar() != null && oRatingDto.getVoltsVar().trim().length() > 0){
				sbFieldQuery.append("QRD_VOLTSVAR, ");
				sbValuesQuery.append("'"+oRatingDto.getVoltsVar()+"', ");				 
			 }
			 
			 /* QRD_AMBIENCE */
			 if (oRatingDto.getAmbience() != null && oRatingDto.getAmbience().trim().length() > 0){
				sbFieldQuery.append("QRD_AMBIENCE, ");
				sbValuesQuery.append("'"+oRatingDto.getAmbience()+"', ");				 
			 }
			 
			 /* QRD_AMBIENCEVAR */
			 if (oRatingDto.getAmbienceVar() != null && oRatingDto.getAmbienceVar().trim().length() > 0){
				sbFieldQuery.append("QRD_AMBIENCEVAR, ");
				sbValuesQuery.append("'"+oRatingDto.getAmbienceVar()+"', ");				 
			 }
			 
			 /* QRD_ALTITUDE */
			 if (oRatingDto.getAltitude() != null && oRatingDto.getAltitude().trim().length() > 0){
				sbFieldQuery.append("QRD_ALTITUDE, ");
				sbValuesQuery.append("'"+oRatingDto.getAltitude()+"', ");				 
			 }
			 
			 /* QRD_ALTITUDEVAR */
			 if (oRatingDto.getAltitudeVar() != null && oRatingDto.getAltitudeVar().trim().length() > 0){
				sbFieldQuery.append("QRD_ALTITUDEVAR, ");
				sbValuesQuery.append("'"+oRatingDto.getAmbienceVar()+"', ");				 
			 }
			 
			 /* QRD_TEMPRAISE */
			 if (oRatingDto.getTempRaise() != null && oRatingDto.getTempRaise().trim().length() > 0){
				sbFieldQuery.append("QRD_TEMPRAISE, ");
				sbValuesQuery.append("'"+oRatingDto.getTempRaise()+"', ");				 
			 }
			 
			 /* QRD_TEMPRAISEVAR */
			 if (oRatingDto.getTempRaiseVar() != null && oRatingDto.getTempRaiseVar().trim().length() > 0){
				sbFieldQuery.append("QRD_TEMPRAISEVAR, ");
				sbValuesQuery.append("'"+oRatingDto.getTempRaiseVar()+"', ");				 
			 }
			 
			 /* QRD_INSULATIONID */
			 if (oRatingDto.getInsulationId() > 0){
				sbFieldQuery.append("QRD_INSULATIONID, ");
				sbValuesQuery.append(oRatingDto.getInsulationId()+", ");				 
			 }
			 
			 /* QRD_ENCLOSUREID */
			 if (oRatingDto.getEnclosureId() > 0){
				sbFieldQuery.append("QRD_ENCLOSUREID, ");
				sbValuesQuery.append(oRatingDto.getEnclosureId()+", ");				 
			 }
			 
			 /* QRD_MOUNTINGID */
			 if (oRatingDto.getMountingId() > 0){
				sbFieldQuery.append("QRD_MOUNTINGID, ");
				sbValuesQuery.append(oRatingDto.getMountingId()+", ");				 
			 }
			 
			 /* QRD_PAINT */
			 if (oRatingDto.getPaint() != null && oRatingDto.getPaint().trim().length() > 0){
				sbFieldQuery.append("QRD_PAINT, ");
				sbValuesQuery.append("'"+oRatingDto.getPaint()+"', ");				 
			 }
			 
			 /* QRD_SPLFEATURES */
			 if (oRatingDto.getSplFeatures() != null && oRatingDto.getSplFeatures().trim().length() > 0){
				sbFieldQuery.append("QRD_SPLFEATURES, ");
				sbValuesQuery.append("'"+oRatingDto.getSplFeatures()+"', ");				 
			 }
			 
			 /* QRD_DETAILS */
			 if (oRatingDto.getDetails() != null && oRatingDto.getDetails().trim().length() > 0){
				sbFieldQuery.append("QRD_DETAILS, ");
				sbValuesQuery.append("'"+oRatingDto.getDetails()+"', ");				 
			 }
			
			sbFieldQuery.append("QRD_ISVALIDATED, ");
			sbValuesQuery.append("'"+oRatingDto.getIsValidated()+"', ");
			
			sbFieldQuery.append("QRD_RATINGUNITPRICE, ");
			sbValuesQuery.append("'"+oRatingDto.getRatingUnitPrice()+"', ");
			
			if (sbFieldQuery.toString().trim().endsWith(","))        	
		           sbFieldQuery = new StringBuffer(sbFieldQuery.toString().trim().substring(0, sbFieldQuery.toString().trim().lastIndexOf(",")));        
		    
			if (sbValuesQuery.toString().trim().endsWith(","))        	
				sbValuesQuery = new StringBuffer(sbValuesQuery.toString().trim().substring(0, sbValuesQuery.toString().trim().lastIndexOf(",")));        
			
			sbFieldQuery.append(")");
			sbValuesQuery.append(")");			
			
			sInsertQuery = sbFieldQuery.toString() + sbValuesQuery.toString();
		 }
		 
		 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "sbFieldQuery ...... "+sbFieldQuery);
		 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "sbValuesQuery ...... "+sbValuesQuery);
		 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "Final Query -  "+sInsertQuery);
		 
		 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
		 return sInsertQuery;
	 }
	 
	 /* (non-Javadoc)
      * @see java.lang.String#buildRatingUpdateQuery(in.com.rbc.quotation.enquiry.dto.RatingDto, int) 
      */
	 private String buildRatingUpdateQuery(RatingDto oRatingDto, int iRatingId){
		 String sMethodName = "buildRatingInsertQuery";
		 
		 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		 StringBuffer sbFieldQuery = new StringBuffer();
		 
		 if (oRatingDto != null){
			 sbFieldQuery.append(UPDATE_RATING_DETAILS);			 
			 
			 /* QRD_QUANTITY */			 
			 sbFieldQuery.append("QRD_QUANTITY = ");
			 sbFieldQuery.append(oRatingDto.getQuantity()+", ");
			 
			 // Fields from here on are optional fields, so include only when they are not empty
			 
			 /* QRD_HTLTID */
			 if (oRatingDto.getHtltId() > 0){
				sbFieldQuery.append("QRD_HTLTID = ");
				sbFieldQuery.append(oRatingDto.getHtltId()+", ");				 
			 }
			 
			 /* QRD_POLEID */
			 if (oRatingDto.getPoleId() > 0){
				sbFieldQuery.append("QRD_POLEID = ");
				sbFieldQuery.append(oRatingDto.getPoleId()+", ");				 
			 }
			 
			 /* QRD_SCSRID */
			 if (oRatingDto.getScsrId() > 0){
				sbFieldQuery.append("QRD_SCSRID = ");
				sbFieldQuery.append(oRatingDto.getScsrId()+", ");				 
			 }
			 
			 /* QRD_DUTYID */
			 if (oRatingDto.getDutyId() > 0){
				sbFieldQuery.append("QRD_DUTYID = ");
				sbFieldQuery.append(oRatingDto.getDutyId()+", ");				 
			 }
			 
			 /* QRD_DEGREEOFPROID */
			 if (oRatingDto.getDegreeOfProId() > 0){
				sbFieldQuery.append("QRD_DEGREEOFPROID = ");
				sbFieldQuery.append(oRatingDto.getDegreeOfProId()+", ");				 
			 }
			 
			 /* QRD_METHODOFSTG */
			 if (oRatingDto.getMethodOfStg() != null && oRatingDto.getMethodOfStg().trim().length() > 0){
				sbFieldQuery.append("QRD_METHODOFSTG = ");
				sbFieldQuery.append("'"+oRatingDto.getMethodOfStg()+"', ");				 
			 }
			 
			 /* QRD_COUPLING */
			 if (oRatingDto.getCoupling() != null && oRatingDto.getCoupling().trim().length() > 0){
				sbFieldQuery.append("QRD_COUPLING = ");
				sbFieldQuery.append("'"+oRatingDto.getCoupling()+"', ");				 
			 }
			 
			 /* QRD_TERMBOXID */
			 if (oRatingDto.getTermBoxId() > 0){
				sbFieldQuery.append("QRD_TERMBOXID = ");
				sbFieldQuery.append(oRatingDto.getTermBoxId()+", ");				 
			 }
			 
			 /* QRD_DIRECTIONOFROTID */
			 if (oRatingDto.getDirectionOfRotId() > 0){
				sbFieldQuery.append("QRD_DIRECTIONOFROTID = ");
				sbFieldQuery.append(oRatingDto.getDirectionOfRotId()+", ");				 
			 }
			 
			 /* QRD_SHAFTEXTID */
			 if (oRatingDto.getShaftExtId() > 0){
				sbFieldQuery.append("QRD_SHAFTEXTID = ");
				sbFieldQuery.append(oRatingDto.getShaftExtId()+", ");				 
			 }
			 
			 /* QRD_APPLICATIONID */
			 if (oRatingDto.getApplicationId() > 0){
				sbFieldQuery.append("QRD_APPLICATIONID = ");
				sbFieldQuery.append(oRatingDto.getApplicationId()+", ");				 
			 }
			 
			 /* QRD_KW */
			 if (oRatingDto.getKw() != null && oRatingDto.getKw().trim().length() > 0){
				sbFieldQuery.append("QRD_KW = ");
				sbFieldQuery.append("'"+oRatingDto.getKw()+"', ");				 
			 }
			 if (oRatingDto.getRatedOutputUnit() != null && oRatingDto.getRatedOutputUnit().trim().length() > 0){
				sbFieldQuery.append("QRD_ROUNIT= ");
				sbFieldQuery.append("'"+oRatingDto.getRatedOutputUnit()+"', ");				 
			 }
			 
			 /* QRD_FREQUENCY */
			 if (oRatingDto.getFrequency() != null && oRatingDto.getFrequency().trim().length() > 0){
				sbFieldQuery.append("QRD_FREQUENCY = ");
				sbFieldQuery.append("'"+oRatingDto.getFrequency()+"', ");				 
			 }
			 
			 /* QRD_FREQUENCYVAR */
			 if (oRatingDto.getFrequencyVar() != null && oRatingDto.getFrequencyVar().trim().length() > 0){
				sbFieldQuery.append("QRD_FREQUENCYVAR = ");
				sbFieldQuery.append("'"+oRatingDto.getFrequencyVar()+"', ");				 
			 }
			 
			 /* QRD_VOLTS */
			 if (oRatingDto.getVolts() != null && oRatingDto.getVolts().trim().length() > 0){
				sbFieldQuery.append("QRD_VOLTS = ");
				sbFieldQuery.append("'"+oRatingDto.getVolts()+"', ");				 
			 }
			 
			 /* QRD_VOLTSVAR */
			 if (oRatingDto.getVoltsVar() != null && oRatingDto.getVoltsVar().trim().length() > 0){
				sbFieldQuery.append("QRD_VOLTSVAR = ");
				sbFieldQuery.append("'"+oRatingDto.getVoltsVar()+"', ");				 
			 }
			 
			 /* QRD_AMBIENCE */
			 if (oRatingDto.getAmbience() != null && oRatingDto.getAmbience().trim().length() > 0){
				sbFieldQuery.append("QRD_AMBIENCE = ");
				sbFieldQuery.append("'"+oRatingDto.getAmbience()+"', ");				 
			 }
			 
			 /* QRD_AMBIENCEVAR */
			 if (oRatingDto.getAmbienceVar() != null && oRatingDto.getAmbienceVar().trim().length() > 0){
				sbFieldQuery.append("QRD_AMBIENCEVAR = ");
				sbFieldQuery.append("'"+oRatingDto.getAmbienceVar()+"', ");				 
			 }
			 
			 /* QRD_ALTITUDE */
			 if (oRatingDto.getAltitude() != null && oRatingDto.getAltitude().trim().length() > 0){
				sbFieldQuery.append("QRD_ALTITUDE = ");
				sbFieldQuery.append("'"+oRatingDto.getAltitude()+"', ");				 
			 }
			 
			 /* QRD_ALTITUDEVAR */
			 if (oRatingDto.getAltitudeVar() != null && oRatingDto.getAltitudeVar().trim().length() > 0){
				sbFieldQuery.append("QRD_ALTITUDEVAR = ");
				sbFieldQuery.append("'"+oRatingDto.getAmbienceVar()+"', ");				 
			 }
			 
			 /* QRD_TEMPRAISE */
			 if (oRatingDto.getTempRaise() != null && oRatingDto.getTempRaise().trim().length() > 0){
				sbFieldQuery.append("QRD_TEMPRAISE = ");
				sbFieldQuery.append("'"+oRatingDto.getTempRaise()+"', ");				 
			 }
			 
			 /* QRD_TEMPRAISEVAR */
			 if (oRatingDto.getTempRaiseVar() != null && oRatingDto.getTempRaiseVar().trim().length() > 0){
				sbFieldQuery.append("QRD_TEMPRAISEVAR = ");
				sbFieldQuery.append("'"+oRatingDto.getTempRaiseVar()+"', ");				 
			 }
			 
			 /* QRD_INSULATIONID */
			 if (oRatingDto.getInsulationId() > 0){
				sbFieldQuery.append("QRD_INSULATIONID = ");
				sbFieldQuery.append(oRatingDto.getInsulationId()+", ");				 
			 }
			 
			 /* QRD_ENCLOSUREID */
			 if (oRatingDto.getEnclosureId() > 0){
				sbFieldQuery.append("QRD_ENCLOSUREID = ");
				sbFieldQuery.append(oRatingDto.getEnclosureId()+", ");				 
			 }
			 
			 /* QRD_MOUNTINGID */
			 if (oRatingDto.getMountingId() > 0){
				sbFieldQuery.append("QRD_MOUNTINGID = ");
				sbFieldQuery.append(oRatingDto.getMountingId()+", ");				 
			 }
			 
			 /* QRD_PAINT */
			 if (oRatingDto.getPaint() != null && oRatingDto.getPaint().trim().length() > 0){
				sbFieldQuery.append("QRD_PAINT = ");
				sbFieldQuery.append("'"+oRatingDto.getPaint()+"', ");				 
			 }
			 
			 /* QRD_SPLFEATURES */
			 if (oRatingDto.getSplFeatures() != null && oRatingDto.getSplFeatures().trim().length() > 0){
				sbFieldQuery.append("QRD_SPLFEATURES = ");
				sbFieldQuery.append("'"+oRatingDto.getSplFeatures()+"', ");				 
			 }
			 
			 /* QRD_DETAILS */
			 if (oRatingDto.getDetails() != null && oRatingDto.getDetails().trim().length() > 0){
				sbFieldQuery.append("QRD_DETAILS = ");
				sbFieldQuery.append("'"+oRatingDto.getDetails()+"', ");				 
			 }
			 
			 if (oRatingDto.getRatingUnitPrice() != null && oRatingDto.getRatingUnitPrice().trim().length() > 0){
					sbFieldQuery.append("QRD_RATINGUNITPRICE = ");
					sbFieldQuery.append("'"+oRatingDto.getRatingUnitPrice()+"', ");				 
				 }
			 if (oRatingDto.getIsValidated() != null && oRatingDto.getIsValidated().trim().length() > 0){
				sbFieldQuery.append("QRD_ISVALIDATED = ");
				sbFieldQuery.append("'"+oRatingDto.getIsValidated()+"', ");
			 }			
			 
			
			
			if (sbFieldQuery.toString().trim().endsWith(","))        	
		           sbFieldQuery = new StringBuffer(sbFieldQuery.toString().trim().substring(0, sbFieldQuery.toString().trim().lastIndexOf(",")));       
			
			sbFieldQuery.append(" WHERE QRD_RATINGID = "+oRatingDto.getRatingId());
		 }
		
		
		 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "sbFieldQuery ...... "+sbFieldQuery);
		 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
		 return sbFieldQuery.toString();
	 }
	 
	 /* (non-Javadoc)
      * @see in.com.rbc.quotation.enquiry.dto.EnquiryDto#buildRatingUpdateQuery(java.sql.Connection, in.com.rbc.quotation.enquiry.dto.EnquiryDto) 
      */
	 public EnquiryDto loadLookUpValues(Connection conn, EnquiryDto oEnquiryDto) throws Exception {
		 
		 
		 oEnquiryDto.setCustomerList(getOptionsList(conn,QuotationConstants.QUOTATION_RFQ_CUSTOMER, QuotationConstants.QUOTATION_QCU_CUSID,QuotationConstants.QUOTATION_QCU_NAME));
		 oEnquiryDto.setAlCustomerTypeList(getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_CUSTTYPE));
		 oEnquiryDto.setAlIndustryList(getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_INDUSTRY));
		 oEnquiryDto.setAlApprovalList(getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_APPROVAL));
		 oEnquiryDto.setAlTargetedList(getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_TARGTD));
		 oEnquiryDto.setAlCompetitorList(getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_COMPR));
		 oEnquiryDto.setAlEnquiryTypeList(getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_ETYPE));
		 oEnquiryDto.setAlSalesStageList(getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_SSTAGE));
		 oEnquiryDto.setAlDeliveryTypeList(getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_DTYPE));
		 oEnquiryDto.setAlSPAList(getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_SPA));
		 oEnquiryDto.setAlWinCompetitorList(getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_WIN_COM));
		 oEnquiryDto.setAlWinlossReasonList(getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_RE_WL));	 
		 oEnquiryDto.setAlVoltList(getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_VOLTS));	
		 oEnquiryDto.setAlWinChanceList(getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_WINCH));

		 
		 return oEnquiryDto;
	 }
	 
	 /* (non-Javadoc)
	  * @see java.util.ArrayList#getOptionsList(java.sql.Connection, java.lang.String, java.lang.String, java.lang.String)
	  */
     private ArrayList getOptionsList(Connection conn,String sTableName, String sColumnName1, String sColumnName2) throws Exception {
    	  /* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        /* ArrayList to store the list of request status */
        ArrayList alrOptions = null;
        String sSqlQuery = null;
        KeyValueVo oKeyValueVo=null;
        
        try {
            /*
             * Querying the database to retrieve the employee title, storing the
             * details in KeyValueVo object, storing these object in an ArrayList 
             * and returning the same
             */
        	sSqlQuery = "SELECT * FROM "+EnquiryQueries.SCHEMA_QUOTATION+"."+sTableName;        	
        	if (sColumnName2 != null && sColumnName2.trim().length() >0 ){
        		sSqlQuery = sSqlQuery + " ORDER BY UPPER(" + sColumnName2 + ")";
        	}else{
        		sSqlQuery = sSqlQuery + " ORDER BY UPPER(" + sColumnName1 + ")";
        	}
        	pstmt = conn.prepareStatement(sSqlQuery);
        	rs = pstmt.executeQuery();
            if (rs.next()){            	
            	alrOptions = new ArrayList();
            	do {            
            		 oKeyValueVo = new KeyValueVo();
            		oKeyValueVo.setKey(rs.getString(sColumnName1));
            		if (sColumnName2 != null && sColumnName2.trim().length() > 0){
            			oKeyValueVo.setValue(rs.getString(sColumnName2));
            		}else{
            			oKeyValueVo.setValue(rs.getString(sColumnName1));
            		}
            		alrOptions.add(oKeyValueVo);
            	}while (rs.next());
            }
        } finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
        
        return alrOptions;
    	
       } 
     
     /* (non-Javadoc)
	  * @see java.util.ArrayList#getOptionsList(java.sql.Connection, java.lang.String, java.lang.String, java.lang.String)
	  */
     private ArrayList getLocationOptionsList(Connection conn, EnquiryDto oEnquiryDto) throws Exception {
    	  /* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        /* ArrayList to store the list of request status */
        ArrayList alrOptions = new ArrayList();
        String sSqlQuery = null;
        KeyValueVo oKeyValueVo =null;
        
        try {
            /*
             * Querying the database to retrieve the employee title, storing the
             * details in KeyValueVo object, storing these object in an ArrayList 
             * and returning the same
             */
            if ( oEnquiryDto.getCustomerName().length()>0 ) {
                sSqlQuery = "SELECT LOC.QLO_LOCATIONID, LOC.QLO_LOCATIONNAME FROM "+EnquiryQueries.SCHEMA_QUOTATION+".RFQ_LOCATION LOC, "+EnquiryQueries.SCHEMA_QUOTATION+".RFQ_CUSTOMER CUS";          
                sSqlQuery = sSqlQuery + "   WHERE CUS.QCU_NAME LIKE '"+oEnquiryDto.getCustomerName()+"' AND LOC.QLO_LOCATIONID = CUS.QCU_LOCATIONID AND LOC.QLO_ISACTIVE='A' AND CUS.QCU_ISDELETED = 'N' ";
                sSqlQuery = sSqlQuery + " ORDER BY UPPER(LOC.QLO_LOCATIONNAME)";
            } else if (oEnquiryDto.getCustomerId() > 0){
        		sSqlQuery = "SELECT LOC.QLO_LOCATIONID, LOC.QLO_LOCATIONNAME FROM "+EnquiryQueries.SCHEMA_QUOTATION+".RFQ_LOCATION LOC, "+EnquiryQueries.SCHEMA_QUOTATION+".RFQ_CUSTOMER CUS";        	
        		sSqlQuery = sSqlQuery + " 	WHERE CUS.QCU_CUSTOMERID = '"+oEnquiryDto.getCustomerId()+"' AND LOC.QLO_LOCATIONID = CUS.QCU_LOCATIONID AND LOC.QLO_ISACTIVE='A' AND CUS.QCU_ISDELETED = 'N'";
        		sSqlQuery = sSqlQuery + " ORDER BY UPPER(LOC.QLO_LOCATIONNAME)";
        	}else{
        		sSqlQuery = "SELECT LOC.QLO_LOCATIONID, LOC.QLO_LOCATIONNAME FROM "+EnquiryQueries.SCHEMA_QUOTATION+".RFQ_LOCATION LOC ";
        		sSqlQuery = sSqlQuery + " WHERE  LOC.QLO_ISACTIVE='A' ORDER BY UPPER(LOC.QLO_LOCATIONNAME)";        		
        	}       	
            
            pstmt = conn.prepareStatement(sSqlQuery);
            rs = pstmt.executeQuery();
            if (rs.next()){            	
            	do {            
            		oKeyValueVo = new KeyValueVo();
            		oKeyValueVo.setKey(rs.getString("QLO_LOCATIONID"));
            		oKeyValueVo.setValue(rs.getString("QLO_LOCATIONNAME"));
            		
            		alrOptions.add(oKeyValueVo);
            	}while (rs.next());
            }
        } finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
        
        return alrOptions;
    	
       } 
    
     /* (non-Javadoc)
	  * @see java.util.ArrayList#getRatingListByEnquiryId(java.sql.Connection, in.com.rbc.quotation.enquiry.dto.EnquiryDto)
	  */  
      public ArrayList getRatingListByEnquiryId(Connection conn, EnquiryDto oEnquiryDto) throws Exception {
    	
    	/* ArrayList to store the list of rating list */
    	ArrayList arlRatingList = new ArrayList();
    	
    	  /* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();	    
        KeyValueVo oKeyValueVo =null;
        
        int count = 1;
        
        try {
        
        	pstmt = conn.prepareStatement(FETCH_RATINGLIST);
        
        	pstmt.setInt(1, oEnquiryDto.getEnquiryId());
            rs = pstmt.executeQuery();
            if (rs.next()){
            	do {            
            		oKeyValueVo = new KeyValueVo();
            		oKeyValueVo.setKey(""+rs.getInt("QRD_RATINGID"));
            		oKeyValueVo.setValue("Rating "+count);
            		oKeyValueVo.setValue2(rs.getString("QRD_ISVALIDATED"));
            		oKeyValueVo.setRatingNumber(rs.getString("QRD_RATINGNO"));
            		
            		
            		count++;
            		
            		arlRatingList.add(oKeyValueVo);
            	}while (rs.next());
            }
        } finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }

    	return arlRatingList;
      }
      
      
      
      
      /**
       * This method is used to Fetch Previous Record Columns MCUNIT Value From RFQ_RATING_DETAILS
       * 
        * @param Connection 
        *        It Will Hold Connection Object To Get Connection From DataBase    
        *            
       * @param EnquiryDto
       *            EnquiryDto object to handle the DTO elements Copied From EnquiryForm
       * @return returns ArrayList Which Contains KeyValueVo Object
       * This Method is used by
       * QuotationDaoImpl.getEnquiryDetails()
       * @throws Exception
       *             if any error occurs while performing database operations, etc
       * @author 900010540 (Gangadhara Rao) 
        */


       public ArrayList getPreviousRatingListByEnquiryId(Connection conn, EnquiryDto oEnquiryDto) throws Exception {
     	
     	/* ArrayList to store the list of rating list */
     	ArrayList arlRatingList = new ArrayList();
     	
     	  /* PreparedStatement object to handle database operations */
         PreparedStatement pstmt = null ;
         
         /* ResultSet object to store the rows retrieved from database */
         ResultSet rs = null ;
         
         /* Object of DBUtility class to handle database operations */
         DBUtility oDBUtility = new DBUtility();	    
         
         int count = 1;
         KeyValueVo oKeyValueVo = null;
         
         try {
         
        	 pstmt = conn.prepareStatement(FETCH_PREVIOUS_RATINGLIST);
         
        	 pstmt.setInt(1, oEnquiryDto.getBaseEnquiryId());
        	 pstmt.setInt(2, oEnquiryDto.getEnquiryId());
        	 pstmt.setString(3, oEnquiryDto.getCreatedBySSO());
        	 rs = pstmt.executeQuery();
             if (rs.next()){
             	do {            
             		oKeyValueVo = new KeyValueVo();

             		oKeyValueVo.setRatingNumber(rs.getString("QRD_RATINGNO"));
             		oKeyValueVo.setMcValue(rs.getString("QRD_CO_MCPERUNIT"));
             		
             		oKeyValueVo.setEnquiryNumber(rs.getString("QEM_ENQUIRYNO"));
             		oKeyValueVo.setUnitPriceRsm(rs.getString("QRD_UNIT_PRICE_RSM"));
             		
             		arlRatingList.add(oKeyValueVo);
             	}while (rs.next());
             }
         } finally {
             /* Releasing ResultSet & PreparedStatement objects */
             oDBUtility.releaseResources(rs, pstmt);
         }

         
     	return arlRatingList;
       }

      
      /* (non-Javadoc)
 	   * @see in.com.rbc.quotation.enquiry.dto.RatingDto#getRatingDetails(java.sql.Connection, in.com.rbc.quotation.enquiry.dto.RatingDto)
 	   */  
	@SuppressWarnings("deprecation")
	public RatingDto getRatingDetails(Connection conn, RatingDto oRatingDto) throws Exception {
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        String sDeviation_clarification="";
        long lLength=0;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        try {
            LoggerUtility.log("INFO", this.getClass().getName(), "getRatingDetails", "Rating Details Query ::: " + GET_RATING_DETAILS);
            LoggerUtility.log("INFO", this.getClass().getName(), "getRatingDetails", "Rating Id ::: " + oRatingDto.getRatingId());
            pstmt = conn.prepareStatement(GET_RATING_DETAILS);
            pstmt.setInt(1, oRatingDto.getRatingId());
            rs = pstmt.executeQuery();
            
            if (rs != null && rs.next()){
            	oRatingDto.setRatingId(rs.getInt("QRD_RATINGID"));
            	oRatingDto.setEnquiryId(rs.getInt("QRD_ENQUIRYID"));
            	oRatingDto.setRatingNo(rs.getInt("QRD_RATINGNO"));
            	
            	oRatingDto.setQuantity(rs.getInt("QRD_QUANTITY"));
            	oRatingDto.setHtltId(rs.getInt("QRD_HTLTID"));
            	oRatingDto.setHtltName(rs.getString("QRD_HTLTDESC"));
            	oRatingDto.setPoleId(rs.getInt("QRD_POLEID"));
            	oRatingDto.setPoleName(rs.getString("QRD_POLEDESC"));
            	oRatingDto.setScsrId(rs.getInt("QRD_SCSRID"));
            	oRatingDto.setScsrName(rs.getString("QRD_SCSRDESC"));
            	oRatingDto.setDutyId(rs.getInt("QRD_DUTYID"));
            	oRatingDto.setDutyName(rs.getString("QRD_DUTYDESC"));
            	
            	oRatingDto.setMinStartVolt(rs.getString("QRD_TO_MINSTARTVOLT"));
            	
            	
            	oRatingDto.setDegreeOfProId(rs.getInt("QRD_DEGREEOFPROID")); 
            	oRatingDto.setDegreeOfProName(rs.getString("QRD_DEGREEOFPRODESC"));
            	
            	oRatingDto.setMethodOfStg(rs.getString("QRD_METHODOFSTG"));
            	oRatingDto.setCoupling(rs.getString("QRD_COUPLING"));
            	
            	oRatingDto.setTermBoxId(rs.getInt("QRD_TERMBOXID"));
            	oRatingDto.setTermBoxName(rs.getString("QRD_TERMBOXDESC"));
            	
            	oRatingDto.setDirectionOfRotId(rs.getInt("QRD_DIRECTIONOFROTID"));
            	
            	oRatingDto.setDirectionOfRotName(rs.getString("QRD_DIRECTIONOFROTDESC")); 
            	
            	oRatingDto.setShaftExtId(rs.getInt("QRD_SHAFTEXTID"));
            	oRatingDto.setShaftExtName(rs.getString("QRD_SHAFTEXNAME"));
            	oRatingDto.setApplicationId(rs.getInt("QRD_APPLICATIONID"));
            	oRatingDto.setApplicationName(rs.getString("QRD_APPLICATIONDESC"));
            	
            	oRatingDto.setKw(rs.getString("QRD_KW")); 
            	
            	
            	oRatingDto.setRatedOutputUnit(rs.getString("QRD_ROUNIT"));
            	oRatingDto.setFrequency(rs.getString("QRD_FREQUENCY"));
            	oRatingDto.setFrequencyVar(rs.getString("QRD_FREQUENCYVAR"));
            	oRatingDto.setVolts(rs.getString("QRD_VOLTS"));
            	oRatingDto.setVoltsVar(rs.getString("QRD_VOLTSVAR"));
            	
            	oRatingDto.setAmbience(rs.getString("QRD_AMBIENCE").equalsIgnoreCase("0")?"":rs.getString("QRD_AMBIENCE")); 
            	oRatingDto.setAmbienceVar(rs.getString("QRD_AMBIENCEVAR"));
            	oRatingDto.setAltitude(rs.getString("QRD_ALTITUDE"));
            	oRatingDto.setAltitudeVar(rs.getString("QRD_ALTITUDEVAR"));
            	
            	oRatingDto.setTempRaise(rs.getString("QRD_TEMPRAISE")); 
            	oRatingDto.setTempRaiseDesc(rs.getString("QRD_TEMPRAISEDESC"));
            	
            	oRatingDto.setTempRaiseVar(rs.getString("QRD_TEMPRAISEVAR"));
            	
            	oRatingDto.setInsulationId(rs.getInt("QRD_INSULATIONID")); 
            	
            	oRatingDto.setInsulationName(rs.getString("QRD_INSULATIONDESC"));
            	
            	oRatingDto.setEnclosureId(rs.getInt("QRD_ENCLOSUREID"));
            	oRatingDto.setEnclosureName(rs.getString("QRD_ENCLOSUREDESC"));
            	oRatingDto.setMountingId(rs.getInt("QRD_MOUNTINGID"));
            	oRatingDto.setMountingName(rs.getString("QRD_MOUNTINGDESC"));
            	
            	oRatingDto.setPaint(rs.getString("QRD_PAINT"));
            	oRatingDto.setSplFeatures(rs.getString("QRD_SPLFEATURES"));
            	oRatingDto.setDetails(rs.getString("QRD_DETAILS"));
            	oRatingDto.setIsValidated(rs.getString("QRD_ISVALIDATED")); 
            	oRatingDto.setRatingUnitPrice(rs.getString("QRD_RATINGUNITPRICE")); 
            	oRatingDto.setRatedOutputUnit(rs.getString("QRD_ROUNIT")); 
            	oRatingDto.setTotalMotorPrice(rs.getString("QRD_TOTALMOTORPRICE")); 
            	oRatingDto.setTotalPackagePrice(rs.getString("QRD_TOTALPKGPRICE")); 
            	
            	//now get the technical details
            	TechnicalOfferDto oTechnicalOfferDto = new TechnicalOfferDto();
            	
            	
            	oTechnicalOfferDto.setRatingId(rs.getInt("QRD_RATINGID"));
            	oTechnicalOfferDto.setFrameSize(rs.getString("QRD_TO_FRAMESIZE"));
            	oTechnicalOfferDto.setEfficiency(rs.getString("QRD_TO_EFFICIENCY"));
            	oTechnicalOfferDto.setPf(rs.getString("QRD_TO_PF"));
            	oTechnicalOfferDto.setFlcAmps(rs.getString("QRD_TO_FLCAMPS"));
            	oTechnicalOfferDto.setPotFLT(rs.getString("QRD_TO_POTFLT"));
            	oTechnicalOfferDto.setFlcSQCAGE(rs.getString("QRD_TO_FLCSQCAGE"));
            	oTechnicalOfferDto.setNoiseLevel(rs.getString("QRD_TO_NOISELEVEL"));
            	oTechnicalOfferDto.setFltSQCAGE(rs.getString("QRD_TO_FLTSQCAGE"));
            	oTechnicalOfferDto.setRpm(rs.getString("QRD_TO_RPM"));
            	oTechnicalOfferDto.setRtGD2(rs.getString("QRD_TO_RTGD2"));
            	oTechnicalOfferDto.setSstSQCAGE(rs.getString("QRD_TO_SSTSQCAGE"));
            	oTechnicalOfferDto.setRv(rs.getString("QRD_TO_RV"));
            	oTechnicalOfferDto.setRa(rs.getString("QRD_TO_RA"));
            	oTechnicalOfferDto.setVibrationLevel(rs.getString("QRD_TO_VIBRATIONLEVEL"));
            	oTechnicalOfferDto.setRemarks(rs.getString("QRD_TO_REMARKS"));
            	oTechnicalOfferDto.setCreatedBy(rs.getString("QRD_TO_CREATEDBY"));
            	oTechnicalOfferDto.setCommercialOfferType(rs.getString("QRD_CO_OFFERTYPE"));
            	oTechnicalOfferDto.setCommercialMCUnit(rs.getInt("QRD_CO_MCPERUNIT"));
            	oTechnicalOfferDto.setCommercialCEUnitPrice(rs.getInt("QRD_CO_UNITPRICE_CE"));
            	oTechnicalOfferDto.setCommercialRSMUnitPrice(rs.getInt("QRD_CO_UNITPRICE_RSM"));
            	oTechnicalOfferDto.setIsDataValidated(rs.getString("QRD_TO_ISVALIDATED"));
            	oTechnicalOfferDto.setUnitPriceRSM(rs.getInt("QRD_CO_UNITPRICE_RSM"));
            	
            	oTechnicalOfferDto.setAreaClassification(rs.getString("QRD_TO_AREACLASSIFICATION"));
            	oTechnicalOfferDto.setServiceFactor(rs.getString("QRD_TO_SERVICEFACTOR"));
            	oTechnicalOfferDto.setBkw(rs.getString("QRD_TO_BKW"));
            	oTechnicalOfferDto.setNlCurrent(rs.getString("QRD_TO_NLCURRENT"));
            	
            	oTechnicalOfferDto.setStrtgCurrent(rs.getString("QRD_TO_STRTGCURRENT"));
            	oTechnicalOfferDto.setLrTorque(rs.getString("QRD_TO_LRTORQUE"));
            	oTechnicalOfferDto.setPllEff1(rs.getString("QRD_TO_PLLEFF1"));
            	oTechnicalOfferDto.setPllEff2(rs.getString("QRD_TO_PLLEFF2"));
            	oTechnicalOfferDto.setPllEff3(rs.getString("QRD_TO_PLLEFF3"));
            	oTechnicalOfferDto.setPllEff4(rs.getString("QRD_TO_PLLEFF4"));
            	oTechnicalOfferDto.setPllCurr1(rs.getString("QRD_TO_PLLCUR1"));
            	oTechnicalOfferDto.setPllCurr2(rs.getString("QRD_TO_PLLCUR2"));
            	oTechnicalOfferDto.setPllCurr3(rs.getString("QRD_TO_PLLCUR3"));
            	oTechnicalOfferDto.setPllCurr4(rs.getString("QRD_TO_PLLCUR4"));
            	oTechnicalOfferDto.setPllCurr5(rs.getString("QRD_TO_PLLCUR5"));
            	oTechnicalOfferDto.setPllPf1(rs.getString("QRD_TO_PLLPF1"));
            	oTechnicalOfferDto.setPllPf2(rs.getString("QRD_TO_PLLPF2"));
            	oTechnicalOfferDto.setPllpf3(rs.getString("QRD_TO_PLLPF3"));
            	oTechnicalOfferDto.setPllpf4(rs.getString("QRD_TO_PLLPF4"));
            	oTechnicalOfferDto.setPllPf5(rs.getString("QRD_TO_PLLPF5"));
            	oTechnicalOfferDto.setSstHot(rs.getString("QRD_TO_SSTHOT"));
            	oTechnicalOfferDto.setSstCold(rs.getString("QRD_TO_SSTCOLD"));            	
            	oTechnicalOfferDto.setStartTimeRV(rs.getString("QRD_TO_STARTTIMERV"));
            	oTechnicalOfferDto.setStartTimeRV80(rs.getString("QRD_TO_STARTTIMERV80"));
            	oTechnicalOfferDto.setCoolingSystem(rs.getString("QRD_TO_COOLINGSYSTEM"));
            	oTechnicalOfferDto.setBearingDENDE(rs.getString("QRD_TO_BEARINGDENDE"));
            	oTechnicalOfferDto.setLubrication(rs.getString("QRD_TO_LUBRICATION"));
            	oTechnicalOfferDto.setMigD2Load(rs.getString("QRD_TO_MIGD2LOAD"));
            	oTechnicalOfferDto.setMigD2Motor(rs.getString("QRD_TO_MIGD2MOTOR"));
            	oTechnicalOfferDto.setBalancing(rs.getString("QRD_TO_BALANCING"));
            	oTechnicalOfferDto.setMainTermBoxPS(rs.getString("QRD_TO_MAINTERMBOXPS"));
            	oTechnicalOfferDto.setNeutralTermBox(rs.getString("QRD_TO_NEUTRALTERMBOX"));            	
            	oTechnicalOfferDto.setCableEntry(rs.getString("QRD_TO_CABLEENTRY"));
            	oTechnicalOfferDto.setAuxTermBox(rs.getString("QRD_TO_AUXTERMBOX"));
            	oTechnicalOfferDto.setStatorConn(rs.getString("QRD_TO_STATORCONN"));
            	oTechnicalOfferDto.setNoBoTerminals(rs.getString("QRD_TO_BOTERMINALSNO"));
            	oTechnicalOfferDto.setRotation(rs.getString("QRD_TO_ROTATION"));            	
            	oTechnicalOfferDto.setMotorTotalWgt(rs.getString("QRD_TO_MOTORTOTALWGT"));
            	oTechnicalOfferDto.setSpaceHeater(rs.getString("QRD_TO_SPACEHEATER"));
            	oTechnicalOfferDto.setWindingRTD(rs.getString("QRD_TO_WINDINGRTD"));
            	oTechnicalOfferDto.setBearingRTD(rs.getString("QRD_TO_BEARINGRTD"));
            	oTechnicalOfferDto.setMinStartVolt(rs.getString("QRD_TO_MINSTARTVOLT"));            	
            	oTechnicalOfferDto.setBearingThermoDT(rs.getString("QRD_TO_BEARINGTHERMODT"));
            	oTechnicalOfferDto.setAirThermo(rs.getString("QRD_TO_AIRTHERMO"));
            	oTechnicalOfferDto.setVibeProbMPads(rs.getString("QRD_TO_VIBPROBEMPADS"));
            	oTechnicalOfferDto.setEncoder(rs.getString("QRD_TO_ENCODER"));
            	oTechnicalOfferDto.setSpeedSwitch(rs.getString("QRD_TO_SPEEDSWITCH"));            	
            	oTechnicalOfferDto.setSurgeSuppressors(rs.getString("QRD_TO_SURGESUPPRESSORS"));
            	oTechnicalOfferDto.setCurrTransformers(rs.getString("QRD_TO_CURRTRANSFORMERS"));
            	oTechnicalOfferDto.setVibProbes(rs.getString("QRD_TO_VIBPROBES"));
            	oTechnicalOfferDto.setSpmNipple(rs.getString("QRD_TO_SPMNIPPLE"));
            	oTechnicalOfferDto.setKeyPhasor(rs.getString("QRD_TO_KEYPHASOR"));        
            	oTechnicalOfferDto.setPaintColor(rs.getString("QRD_TO_PAINTCOLOR"));   
            	oTechnicalOfferDto.setTransferCMultiplier(rs.getString("QRD_TRANSFERCOSTMUL_CE"));
            	oTechnicalOfferDto.setQuantityMultiplier(rs.getString("QRD_QNTYMULT_CE"));
            	
            	oTechnicalOfferDto.setMcornspratioMultiplier_rsm(rs.getString("QRD_MC_OR_NSP_RATIO_RSM"));
            	oTechnicalOfferDto.setTransportationperunit_rsm(rs.getString("QRD_TRANS_PER_UNIT_RSM"));
            	oTechnicalOfferDto.setWarrantyperunit_rsm(rs.getString("QRD_WARR_PER_UNIT_RSM"));
            	
            	
            	
            	oTechnicalOfferDto.setUnitpriceMultiplier_rsm(rs.getString("QRD_UNIT_PRICE_RSM"));
            	oTechnicalOfferDto.setTotalpriceMultiplier_rsm(rs.getString("QRD_TOTAL_PRICE_RSM"));
            	
            	oTechnicalOfferDto.setTermBoxId(rs.getInt("QRD_TERMBOXID"));
            	oTechnicalOfferDto.setTermBoxName(rs.getString("QRD_TERMBOXDESC"));
            	
            	oTechnicalOfferDto.setZone(rs.getString("QRD_ZONE"));
            	oTechnicalOfferDto.setGasGroup(rs.getString("QRD_GAS_GROUP"));
            	
            	   
            	//Conversion Of Clob To String
            	
            	
            	if(rs.getClob("QRD_DEVIATION_CLARIFICATION")!=null)
            	{
            		 lLength = rs.getClob("QRD_DEVIATION_CLARIFICATION").length();
                	 sDeviation_clarification = rs.getClob("QRD_DEVIATION_CLARIFICATION").getSubString(1, (int) lLength);

            	}
            	
            	oTechnicalOfferDto.setDeviation_clarification(sDeviation_clarification);
            	
            	/*
            	 * This Data are Added By Gangadhar on 15 Feb 2019 For IndentPage View
            	 */
            	
            	oTechnicalOfferDto.setPriceincludeFrieght(rs.getString("QRD_PRICEINFREIGHT"));
            	oTechnicalOfferDto.setPriceincludeFrieghtVal(rs.getString("QRD_PRICEINFREIGHTC"));
            	
            	oTechnicalOfferDto.setPriceincludeExtraWarn(rs.getString("QRD_PRICEINEXTRAWAR"));
            	oTechnicalOfferDto.setPriceincludeExtraWarnVal(rs.getString("QRD_PRICEINEXTRAWARC"));
            	
            	oTechnicalOfferDto.setPriceincludeSupervisionCost(rs.getString("QRD_PRICEINSUPVISION"));
            	oTechnicalOfferDto.setPriceincludeSupervisionCostVal(rs.getString("QRD_PRICEINSUPVISIONC"));

            	oTechnicalOfferDto.setPriceincludetypeTestCharges(rs.getString("QRD_PRICEINTYPE"));
            	oTechnicalOfferDto.setPriceincludetypeTestChargesVal(rs.getString("QRD_PRICEINTYPEC"));

            	

            	oTechnicalOfferDto.setPriceextraFrieght(rs.getString("QRD_PRICEEXFREIGHT"));
            	oTechnicalOfferDto.setPriceextraFrieghtVal(rs.getString("QRD_PRICEEXFREIGHTC"));

            	oTechnicalOfferDto.setPriceextraExtraWarn(rs.getString("QRD_PRICEEXTRAWAR"));
            	oTechnicalOfferDto.setPriceextraExtraWarnVal(rs.getString("QRD_PRICEEXTRAWARC"));

            	oTechnicalOfferDto.setPriceextraSupervisionCost(rs.getString("QRD_PRICEEXSUPVISION"));
            	oTechnicalOfferDto.setPriceextraSupervisionCostVal(rs.getString("QRD_PRICEEXSUPVISIONC"));

            	
            	oTechnicalOfferDto.setPriceextratypeTestCharges(rs.getString("QRD_PRICEEXTYPE"));
            	oTechnicalOfferDto.setPriceextratypeTestChargesVal(rs.getString("QRD_PRICEEXTYPEC"));
            	

            	
            	oTechnicalOfferDto.setFormattedCommercialMCUnit(CommonUtility.formatPrice(QuotationConstants.QUOTATION_PRICE_PATTERN, oTechnicalOfferDto.getCommercialMCUnit()));
            	oTechnicalOfferDto.setFormattedCommercialCEUnitPrice(CommonUtility.formatPrice(QuotationConstants.QUOTATION_PRICE_PATTERN, oTechnicalOfferDto.getCommercialCEUnitPrice()));
            	oTechnicalOfferDto.setFormattedCommercialRSMUnitPrice(CommonUtility.formatPrice(QuotationConstants.QUOTATION_PRICE_PATTERN, oTechnicalOfferDto.getCommercialRSMUnitPrice()));
            	
            	// now calculate the total price
            	oTechnicalOfferDto.setCommercialMCUnitTotalPrice(oRatingDto.getQuantity() * oTechnicalOfferDto.getCommercialMCUnit());
            	oTechnicalOfferDto.setCommercialCETotalPrice(oRatingDto.getQuantity() * oTechnicalOfferDto.getCommercialCEUnitPrice());
            	oTechnicalOfferDto.setCommercialRSMTotalPrice(oRatingDto.getQuantity() * oTechnicalOfferDto.getCommercialRSMUnitPrice());
            	
            	oTechnicalOfferDto.setFormattedCommercialMCUnitTotalPrice(CommonUtility.formatPrice(QuotationConstants.QUOTATION_PRICE_PATTERN, oRatingDto.getQuantity() * oTechnicalOfferDto.getCommercialMCUnit()));
            	oTechnicalOfferDto.setFormattedCommercialCETotalPrice(CommonUtility.formatPrice(QuotationConstants.QUOTATION_PRICE_PATTERN, oRatingDto.getQuantity() * oTechnicalOfferDto.getCommercialCEUnitPrice()));
            	oTechnicalOfferDto.setFormattedCommercialRSMTotalPrice(CommonUtility.formatPrice(QuotationConstants.QUOTATION_PRICE_PATTERN, oRatingDto.getQuantity() * oTechnicalOfferDto.getCommercialRSMUnitPrice()));
            	
            	oRatingDto.setTechnicalDto(oTechnicalOfferDto);
            	
            }
        }catch(Exception e)
        {
            LoggerUtility.log("DEBUG", "QuotationDaoImpl", "getRatingDetails", ExceptionUtility.getStackTraceAsString(e));

        }finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
        
		return oRatingDto;
	}
	
	/* (non-Javadoc)
 	 * @see in.com.rbc.quotation.enquiry.dto.EnquiryDto#getRatingDetails(java.sql.Connection, in.com.rbc.quotation.enquiry.dto.EnquiryDto)
 	 */  
	public EnquiryDto getEnquiryDetails(Connection conn, EnquiryDto oEnquiryDto, String sOperationType) throws Exception{
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        KeyValueVo oKeyValueVo =null;
        ArrayList alRatingObjects = null;
		RatingDto oRatingDto = null;
		String sSalesManagerId = "";
		String sTotalpkgPrice = "";
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
       
        ArrayList alTempList = new ArrayList();
        
        ArrayList alCurrentRating =null;
        DecimalFormat oDf = new DecimalFormat("###,###");
        
        try {
        	pstmt = conn.prepareStatement(FETCH_ENQUIRYDETAILS_FROMVIEW);
        	pstmt.setInt(1, oEnquiryDto.getEnquiryId());
        	rs = pstmt.executeQuery();
            
            if (rs != null && rs.next()){
                oEnquiryDto.setEnquiryNumber(rs.getString("QEM_ENQUIRYNO"));   
                oEnquiryDto.setRevisionNumber(rs.getInt("QEM_REVISIONNO"));
                oEnquiryDto.setBaseEnquiryId(rs.getInt("QEM_BASEENQUIRYID"));     
                
            	oEnquiryDto.setCustomerName(rs.getString("QEM_CUSTOMERNAME"));
            	oEnquiryDto.setCustomerId(rs.getInt("QEM_CUSTOMERID"));
            	oEnquiryDto.setEnquiryType(rs.getString("QEM_ENQUIRYTYPE"));
            	oEnquiryDto.setCustomerType(rs.getString("QEM_CUSTOMERTYPE")); 
            	oEnquiryDto.setEndUser(rs.getString("QEM_ENDUSER")); 
            	oEnquiryDto.setOem(rs.getString("QEM_OEM")); 
            	oEnquiryDto.setEpc(rs.getString("QEM_EPC")); 
            	oEnquiryDto.setDealer(rs.getString("QEM_DEALER")); 
            	oEnquiryDto.setProjectName(rs.getString("QEM_PROJECTNAME")); 
            	oEnquiryDto.setIndustry(rs.getString("QEM_INDUSTRY")); 
            	oEnquiryDto.setEndUserCountry(rs.getString("QEM_EUCOUNTRY")); 
            	oEnquiryDto.setEndUserIndustry(rs.getString("QEM_EUINDUSTRY")); 
            	oEnquiryDto.setApproval(rs.getString("QEM_APPROVAL")); 
            	oEnquiryDto.setTargeted(rs.getString("QEM_TARGETED")); 
            	oEnquiryDto.setWinningChance(rs.getString("QEM_WINCHANCE")); 
            	oEnquiryDto.setCompetitor(rs.getString("QEM_COMPETITOR")); 
            	oEnquiryDto.setSalesStage(rs.getString("QEM_SALESSTAGE")); 
            	oEnquiryDto.setTotalOrderValue(rs.getString("QEM_TOTALORDVAL")); 
            	oEnquiryDto.setOrderClosingMonth(rs.getString("QEM_ORDCLOSEMNTH")); 
            	oEnquiryDto.setExpectedDeliveryMonth(rs.getString("QEM_EXPDELIVERYMNTH")); 
            	oEnquiryDto.setWarrantyDispatchDate(rs.getString("QEM_DISPATCHDATEWRNTY")); 
            	oEnquiryDto.setWarrantyCommissioningDate(rs.getString("QEM_COMMISSIONINGDATEWRNTY")); 
            	oEnquiryDto.setDeliveryType(rs.getString("QEM_DELIVERYTYPE")); 
            	oEnquiryDto.setDestState(rs.getString("QEM_DESTSTATE")); 
            	oEnquiryDto.setDestCity(rs.getString("QEM_DESTCITY")); 
            	oEnquiryDto.setSpa(rs.getString("QEM_SPA")); 
            	oEnquiryDto.setCustomerCounterOffer(rs.getDouble("QEM_CUSTOMER_COUNTEROFFER")); 
            	oEnquiryDto.setMaterialCost(rs.getString("QEM_MATERIALCOST")); 
            	oEnquiryDto.setMcnspRatio(rs.getString("QEM_MCNSP_RATIO")); 
            	oEnquiryDto.setMotorPrice(rs.getDouble("QEM_MOTORPRICE")); 
            	oEnquiryDto.setWarrantyCost(rs.getString("QEM_WRNTYCOST"));            
            	oEnquiryDto.setTransportationCost(rs.getString("QEM_TRANSPORTCOST")); 
            	oEnquiryDto.setEcSupervisionCost(rs.getString("QEM_SUPERVISIONCOST")); 
            	oEnquiryDto.setHzAreaCertCost(rs.getString("QEM_HAZAREA_CERTCOST")); 
            	oEnquiryDto.setSparesCost(rs.getString("QEM_SPARESCOST")); 
            	oEnquiryDto.setTotalCost(rs.getDouble("QEM_TOTALCOST")); 
            	oEnquiryDto.setPropSpaReason(rs.getString("QEM_REASON_PROPSPA")); 
            	oEnquiryDto.setRequestRemarks(rs.getString("QEM_REMARKS")); 
            	oEnquiryDto.setWinlossReason(rs.getString("QEM_REASON_WINLOSS")); 
            	oEnquiryDto.setWinlossComment(rs.getString("QEM_COMMENT_WINLOSS")); 
            	oEnquiryDto.setWinningCompetitor(rs.getString("QEM_WINNING_COMPETITOR")); 
            	oEnquiryDto.setWinlossPrice(rs.getString("QEM_WINLOSS_PRICE")); 
            	oEnquiryDto.setUnitMC(rs.getDouble("QEM_UNITMC")); 
            	oEnquiryDto.setUnitPrice(rs.getDouble("QEM_UNITPRICE")); 
            	oEnquiryDto.setTotalPrice(rs.getString("QEM_TOTALPRICE")); 
            	oEnquiryDto.setTotalMC(rs.getDouble("QEM_TOTALMC"));             	
            	oEnquiryDto.setTotalPkgPrice(rs.getString("QEM_TOTAL_PKGPRICE")); 
            	oEnquiryDto.setEnquiryReferenceNumber(rs.getString("QEM_ENQUIRYREFERENCENO")); 
            	oEnquiryDto.setDescription(rs.getString("QEM_DESC"));
            	oEnquiryDto.setConsultantName(rs.getString("QEM_CONSULTANTNAME"));
            	oEnquiryDto.setCreatedDate(rs.getString("QEM_CREATEDDATE"));
            	oEnquiryDto.setCreatedBy(rs.getString("QEM_CREATEDBYNAME"));
            	oEnquiryDto.setCreatedBySSO(rs.getString("QEM_CREATEDBY"));
            	oEnquiryDto.setStatusId(rs.getInt("QEM_STATUSID"));
            	oEnquiryDto.setStatusName(rs.getString("QEM_STATUSDESC"));
            	oEnquiryDto.setCeFactor(rs.getLong("QEM_FACTOR_CE"));
            	oEnquiryDto.setRsmFactor(rs.getLong("QEM_FACTOR_RSM"));
            	oEnquiryDto.setSavingIndent(rs.getString("QEM_SAVINGINDENT"));
            	oEnquiryDto.setRevision(rs.getString("QEM_REVISION"));
            	oEnquiryDto.setLastModifiedDate(rs.getString("QEM_LASTMODIFIEDDATE"));            	
            	oEnquiryDto.setTermsDeliveryHT(rs.getString("QEM_TERMS_DELIVERYHT"));
            	oEnquiryDto.setTermsDeliveryLT(rs.getString("QEM_TERMS_DELIVERYLT"));
            	oEnquiryDto.setTermsPercentAdv(rs.getString("QEM_TERMS_PERCENTADV"));
            	oEnquiryDto.setTermsPercentBal(rs.getString("QEM_TERMS_PERCENTBAL"));
            	oEnquiryDto.setTermsVarFormula(rs.getString("QEM_TERMS_VARFORMULA"));
            	oEnquiryDto.setPaymentTerms(rs.getString("QEM_PAYMENTTERMS"));
            	oEnquiryDto.setAdvancePayment(rs.getString("QEM_ADVANCEPAYMENT"));
            	oEnquiryDto.setWarrantycommissioningMultiplier(rs.getString("QEM_WARCOMMMULT_CE"));
            	oEnquiryDto.setWarrantyDispatchMultiplier(rs.getString("QEM_WARDISMULT_CE"));
            	oEnquiryDto.setSmnote(rs.getString("QEM_SMNOTE"));
            	oEnquiryDto.setSm_name(rs.getString("QEM_SMNAME"));
            	oEnquiryDto.setDm_name(rs.getString("QEM_DMNAME"));
            	oEnquiryDto.setDm_ssoid(rs.getString("QEM_DMSSOID"));
            	
            	oEnquiryDto.setDmnote(rs.getString("QEM_DMNOTE"));
            	oEnquiryDto.setCe_name(rs.getString("QEM_CENAME"));
            	oEnquiryDto.setCe_ssoid(rs.getString("QEM_CESSOID"));
            	oEnquiryDto.setCenote(rs.getString("QEM_CENOTE"));
            	oEnquiryDto.setRsmnote(rs.getString("QEM_RSMNOTE"));
            	oEnquiryDto.setRsm_name(rs.getString("QEM_RSMNAME"));
            	oEnquiryDto.setRsm_ssoid(rs.getString("QEM_RSMSSOID"));
            	oEnquiryDto.setDmToSmReturn(rs.getString("QEM_DMTOSM_RETURN"));
            	oEnquiryDto.setCeToDmReturn(rs.getString("QEM_CETODM_RETURN"));
            	oEnquiryDto.setCeToSmReturn(rs.getString("QEM_CETOSM_RETURN"));
            	oEnquiryDto.setRsmToCeReturn(rs.getString("QEM_RSMTOCE_RETURN"));
            	oEnquiryDto.setRsmToDmReturn(rs.getString("QEM_RSMTODM_RETURN"));
            	oEnquiryDto.setRsmToSmReturn(rs.getString("QEM_RSMTOSM_RETURN"));	
            	oEnquiryDto.setDeToSmReturn(rs.getString("QEM_DETOSM_RETURN"));
            	oEnquiryDto.setCmNote(rs.getString("QEM_INDENT_CMNOTE"));
            	oEnquiryDto.setCmName(rs.getString("QEM_CMNAME"));
            	oEnquiryDto.setDmToCmReturn(rs.getString("QEM_DMTOCM_RETURN"));
            	oEnquiryDto.setDeToCmReturn(rs.getString("QEM_DETOCM_RETURN"));
            	oEnquiryDto.setIndentDmtosmReturn(rs.getString("QEM_INDMTOSM_RETURN"));
            	oEnquiryDto.setIndentDetosmReturn(rs.getString("QEM_INDETOSM_RETURN"));
            	if(StringUtils.isBlank(rs.getString("QEM_IS_NEWENQUIRY"))) {
            		oEnquiryDto.setIsNewEnquiry("N");
            	} else {
            		oEnquiryDto.setIsNewEnquiry(rs.getString("QEM_IS_NEWENQUIRY").trim());
            	}
            	
            	/*
            	 * TODO 
            	 * Adding Previous MC Values Along With Enquiry Number In Offerdata.jsp when the status is Superseded
            	 */
            	
            	if(oEnquiryDto.getRevisionNumber()>= 2)
            	{
            	oEnquiryDto.setPreviousratingList(getPreviousRatingListByEnquiryId(conn, oEnquiryDto));
            	}
            	oEnquiryDto.setQuotationtoCeReturn(rs.getString("QEM_QUOTATIONTOCE_RETURN"));
            	oEnquiryDto.setQuotationtoDmReturn(rs.getString("QEM_QUOTATIONTODM_RETURN"));
            	oEnquiryDto.setQuotationtoRsmReturn(rs.getString("QEM_QUOTATIONTORSM_RETURN"));
            	/*
            	 * TODO
            	 * Getting Columns From Database Which Required For Indent Page View
            	 * 
            	 */
            	
            	oEnquiryDto.setSapCustomerCode(rs.getString("QEM_SAPCUSTCODE"));
            	oEnquiryDto.setOrderNumber(rs.getString("QEM_ORDERNO"));
            	oEnquiryDto.setPoiCopy(rs.getString("QEM_POICOPY"));
            	oEnquiryDto.setLoiCopy(rs.getString("QEM_LOICOPY"));
            	oEnquiryDto.setDeliveryReqByCust(rs.getString("QEM_DELIVERREQBYCUST"));
            	oEnquiryDto.setLd(rs.getString("QEM_LD"));
            	oEnquiryDto.setDrawingApproval(rs.getString("QEM_DRAWINGAPPROVAL"));
            	oEnquiryDto.setQapApproval(rs.getString("QEM_QAPAPPROVAL"));
            	oEnquiryDto.setTypeTest(rs.getString("QEM_TYPETEST"));
            	oEnquiryDto.setRoutineTest(rs.getString("QEM_ROUTINETEST"));
            	oEnquiryDto.setSpecialTest(rs.getString("QEM_SPECIALTEST"));
            	oEnquiryDto.setSpecialTestDetails(rs.getString("QEM_SPTESTDETAILS"));
            	oEnquiryDto.setThirdPartyInspection(rs.getString("QEM_THIRDPARTY"));
            	oEnquiryDto.setStageInspection(rs.getString("QEM_STAGEINSPECTION"));
            	oEnquiryDto.setEnduserLocation(rs.getString("QEM_ENDUSERLOC"));
            	oEnquiryDto.setEpcName(rs.getString("QEM_EPCNAME"));
            	oEnquiryDto.setPacking(rs.getString("QEM_PACKING"));
            	oEnquiryDto.setInsurance(rs.getString("QEM_INSURANCE"));
            	oEnquiryDto.setOldSerialNo(rs.getString("QEM_OLDSERIALNO"));
            	oEnquiryDto.setCustomerPartNo(rs.getString("QEM_CUSTOMERPARTNO"));
            	oEnquiryDto.setIncludeFrieght(rs.getString("QEM_INFREIGHT"));
            	oEnquiryDto.setIncludeExtraWarrnt(rs.getString("QEM_INEXTRAWAR"));
            	oEnquiryDto.setIncludeSupervisionCost(rs.getString("QEM_INSUPVISION"));
            	oEnquiryDto.setIncludetypeTestCharges(rs.getString("QEM_INTYPETEST"));
            	oEnquiryDto.setExtraFrieght(rs.getString("QEM_EXFREIGHT"));
            	oEnquiryDto.setExtraExtraWarrnt(rs.getString("QEM_EXWARRANT"));
            	oEnquiryDto.setExtraSupervisionCost(rs.getString("QEM_EXSUPERVISION"));
            	oEnquiryDto.setExtratypeTestCharges(rs.getString("QEM_EXTYPETEST"));            	
            	oEnquiryDto.setTotalMotorPrice(rs.getString("QEM_TOTMOTORPRICE"));
            	oEnquiryDto.setIndentOperationCheck(rs.getString("QEM_INDENTSTATUS"));
            	oEnquiryDto.setIndentnote(rs.getString("QEM_INDENT_WONNOTE"));
            	oEnquiryDto.setCmToSmReturn(rs.getString("QEM_CMTOSM_RETURN"));
            	oEnquiryDto.setCommengToSmReturn(rs.getString("QEM_COMMENGTOSM_RETURN"));
            	
            	
            	// Rating List Details
            	
            	oEnquiryDto.setRatingList(getRatingListByEnquiryId(conn, oEnquiryDto));
                alTempList=new ArrayList();
             	if ( (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_VIEW) || sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_OFFERDATA) 
             			|| sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_INDENTDATA)
             			|| sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_INDENTDATA2)
             			|| sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_OFFERDATA2)) &&
            			oEnquiryDto.getRatingList().size() > QuotationConstants.QUOTATION_LITERAL_ZERO){	            		
            		alRatingObjects = new ArrayList();
            		oRatingDto = null;
            		for (int idx=0; idx<oEnquiryDto.getRatingList().size(); idx++){
            			oKeyValueVo = (KeyValueVo) oEnquiryDto.getRatingList().get(idx);
            			if (oKeyValueVo != null){
            				oRatingDto = new RatingDto();
            				oRatingDto.setRatingId(Integer.parseInt(oKeyValueVo.getKey()));
            				oRatingDto.setRatingTitle(oKeyValueVo.getValue());
            				oRatingDto = getRatingDetails(conn, oRatingDto);	            				
            				if (oEnquiryDto.getCurrentRating().equalsIgnoreCase(""+oRatingDto.getRatingId())){
            					alCurrentRating = new ArrayList();
            					alCurrentRating.add(oRatingDto);
            					oEnquiryDto.setCurrentRatingObject(alCurrentRating);

            				}
            				
            				if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_VIEW)){
            					oRatingDto.setAddOnList(getAddOnList(conn, oRatingDto.getRatingId()));
            					oRatingDto.setDeliveryDetailsList(getDeliveryDetailsList(conn,oRatingDto.getRatingId()));
            				}
            				alRatingObjects.add(oRatingDto);
            				
            				if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_OFFERDATA)){
            					TechnicalOfferDto oTempTechnicalOfferDto = oRatingDto.getTechnicalDto();
            					if (oTempTechnicalOfferDto != null){
            						oKeyValueVo.setValue2(oTempTechnicalOfferDto.getIsDataValidated());
            						
            						alTempList.add(oKeyValueVo);	            						
            					}
            				}
            				
            				if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_OFFERDATA2)){
            					TechnicalOfferDto oTempTechnicalOfferDto = oRatingDto.getTechnicalDto();
            					if (oTempTechnicalOfferDto != null){
            						
            						oKeyValueVo.setValue2("N");
            						alTempList.add(oKeyValueVo);	            						
            					}
            				}
            				if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_INDENTDATA)){
            					TechnicalOfferDto oTempTechnicalOfferDto = oRatingDto.getTechnicalDto();
            					if (oTempTechnicalOfferDto != null){
            						oKeyValueVo.setValue2(oRatingDto.getIsValidated());
            						
            						alTempList.add(oKeyValueVo);	            						
            					}
            				}
            				
            				if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_INDENTDATA2)){
            					TechnicalOfferDto oTempTechnicalOfferDto = oRatingDto.getTechnicalDto();
            					if (oTempTechnicalOfferDto != null){
            						
            						oKeyValueVo.setValue2("N");
            						alTempList.add(oKeyValueVo);	            						
            					}
            				}

            				

            			}
            		}
            		if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_OFFERDATA)){
            			oEnquiryDto.setRatingList(alTempList);
            		}
            		if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_OFFERDATA2)){
            			oEnquiryDto.setRatingList(alTempList);
            		}
            		if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_INDENTDATA)){
            			oEnquiryDto.setRatingList(alTempList);
            		}
            		if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_INDENTDATA2)){
            			oEnquiryDto.setRatingList(alTempList);
            		}


            		oEnquiryDto.setRatingObjects(alRatingObjects);
            	}else{
            		if (oEnquiryDto.getRatingList().size() > QuotationConstants.QUOTATION_LITERAL_ZERO){
            			 oKeyValueVo = (KeyValueVo) oEnquiryDto.getRatingList().get(0);
            			if (oKeyValueVo != null){
            				oEnquiryDto.setRatingOperationId(oKeyValueVo.getKey());	
            			}
            		}
            	} 
             	
             	 oDBUtility.releaseResources(rs, pstmt);
             	 
             	sSalesManagerId = getSalesManagerId(conn, oEnquiryDto);
             	
             	pstmt = conn.prepareStatement(AdminQueries.FETCH_SALESMANGER_INFO);
            	pstmt.setString(1, sSalesManagerId);
            	rs = pstmt.executeQuery();
            	 if (rs != null && rs.next()){ 
            		 oEnquiryDto.setSmEmail(rs.getString("SALESMANAGER_EMAIL"));
            		 oEnquiryDto.setSmContactNo(rs.getString("QSM_PHONENO"));
            	 }
            }
            
        }finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
		return oEnquiryDto;
	}
	
	/* (non-Javadoc)
 	 * @see java.util.ArrayList#getAddOnList(java.sql.Connection, int)
 	 */  
	public ArrayList getAddOnList (Connection conn, int iRatingId) throws Exception{
		ArrayList arAddOnlist = new ArrayList();
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        AddOnDto oAddOnDto = null;
        
        try {
        	pstmt = conn.prepareStatement(FETCH_ADDON_DETAILS);
        	pstmt.setInt(1, iRatingId);
        	rs = pstmt.executeQuery();
            
            if (rs != null && rs.next()){ 
            	do {
            		oAddOnDto = new AddOnDto();
            		oAddOnDto.setAddOnTypeText(rs.getString("QAO_ADDONTYPE"));
            		oAddOnDto.setAddOnQuantity(rs.getInt("QAO_QTY"));
            		oAddOnDto.setAddOnUnitPrice(rs.getDouble("QAO_UNITPRICE"));
            		oAddOnDto.setAddOnUnitPriceCE(rs.getInt("QAO_UNITPRICE_CE"));
            		oAddOnDto.setAddOnUnitPriceRSM(rs.getInt("QAO_UNITPRICE_RSM"));
            		
            		oAddOnDto.setFormattedAddOnUnitPrice(CommonUtility.formatPrice(QuotationConstants.QUOTATION_PRICE_PATTERN, oAddOnDto.getAddOnUnitPrice()));
            		oAddOnDto.setFormattedAddOnUnitPriceCE(CommonUtility.formatPrice(QuotationConstants.QUOTATION_PRICE_PATTERN, oAddOnDto.getAddOnUnitPriceCE()));
            		oAddOnDto.setFormattedAddOnUnitPriceRSM(CommonUtility.formatPrice(QuotationConstants.QUOTATION_PRICE_PATTERN, oAddOnDto.getAddOnUnitPriceRSM()));
            		
            	
            		arAddOnlist.add(oAddOnDto);	     
            	}while (rs.next());
            }
            
        }finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
		
		return arAddOnlist;
	}
	
	/* (non-Javadoc)
 	 * @see boolean#getRatingDetails(java.sql.Connection, in.com.rbc.quotation.enquiry.dto.RatingDto)
 	 */ 
	public boolean deleteRating(Connection conn, RatingDto oRatingDto) throws Exception{
		boolean isDeleted = false;
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        try {
        	pstmt = conn.prepareStatement(DELETE_RATING);
        	pstmt.setInt(1, oRatingDto.getRatingId());	            
        	pstmt.executeUpdate();
            isDeleted = true;
            
        }finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(pstmt);
        }
        
		return isDeleted;
	}
	
    public EnquiryDto copyEnquiry(Connection conn, EnquiryDto enquiryDto, UserDto userDto) throws Exception{
			 String sMethodName = "copyEnquiry";
			 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
			 
             /* PreparedStatement objects to handle database operations */
		     CallableStatement cstmt = null;
             
		     EnquiryDto oEnquiryDto = null;
		     String sEnquiryId;
             
		     /* Object of DBUtility class to handle database operations */
		     DBUtility oDBUtility = new DBUtility();
             
		     try {
		    	 cstmt = conn.prepareCall(CALL_COPYENQUIRY_PROC);
		    	 cstmt.setString(1,(""+enquiryDto.getEnquiryId()).trim());
		    	 cstmt.setString(2,userDto.getUserId());	
		    	 cstmt.setString(3,"COPY");	
		    	 cstmt.registerOutParameter(1,Types.VARCHAR);
		    	 cstmt.executeQuery();
		    	 sEnquiryId = cstmt.getString(1);
		    	 if(sEnquiryId!=null && !sEnquiryId.equalsIgnoreCase("")){
		    		 oEnquiryDto = new EnquiryDto();
		    		 oEnquiryDto.setEnquiryId(Integer.parseInt(sEnquiryId));
		    	 }
	     	 } finally {
		    	 /* Releasing PreparedStatment objects */
	             oDBUtility.releaseResources(cstmt);
	         }
			 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
			 return oEnquiryDto;
	}
    
	public EnquiryDto createRevision(Connection conn, EnquiryDto enquiryDto, UserDto userDto) throws Exception{
			 String sMethodName = "createRevision";
			 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
			 
             /* PreparedStatement objects to handle database operations */
		     CallableStatement cstmt = null;
		     
             EnquiryDto oEnquiryDto = null;
		     String sEnquiryId;
             
		     /* Object of DBUtility class to handle database operations */
		     DBUtility oDBUtility = new DBUtility();
		     try {
		    	 cstmt = conn.prepareCall(CALL_COPYENQUIRY_PROC);
		    	 cstmt.setString(1,(""+enquiryDto.getEnquiryId()).trim());
		    	 cstmt.setString(2,userDto.getUserId());
		    	 cstmt.setString(3,QuotationConstants.QUOTATION_REVISION);	
		    	 cstmt.registerOutParameter(1,Types.VARCHAR);
		    	 
		    	 cstmt.executeQuery();
		    	 sEnquiryId = cstmt.getString(1);
		    	 if(sEnquiryId!=null && !sEnquiryId.equalsIgnoreCase("")) {
		    		 oEnquiryDto = new EnquiryDto();
		    		 oEnquiryDto.setEnquiryId(Integer.parseInt(sEnquiryId));
		    	 }
	     	 } finally {
		    	 /* Releasing PreparedStatment objects */
	             oDBUtility.releaseResources(cstmt);
	         }   
			 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
			 return oEnquiryDto;
		 }
	public boolean deleteEnquiry(Connection conn, EnquiryDto enquiryDto) throws Exception {
		 String sMethodName = "deleteEnquiry";
		 boolean isDeleted = false;
		 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		 
         /* PreparedStatement objects to handle database operations */
	     PreparedStatement pstmt = null;
	     
         EnquiryDto oEnquiryDto = null;
	     String EnquiryId;
	     
         /* Object of DBUtility class to handle database operations */
	     DBUtility oDBUtility = new DBUtility();
	     
         try {
        	 pstmt = conn.prepareStatement(DELETE_ENQUIRY_RATINGS);
        	 pstmt.setString(1,enquiryDto.getEnquiryId()+"");
        	 pstmt.executeUpdate();
	    	 oDBUtility.releaseResources(pstmt);  
	    	 pstmt = conn.prepareStatement(DELETE_ENQUIRY);
	    	 pstmt.setString(1,enquiryDto.getEnquiryId()+"");
	    	 pstmt.executeUpdate();
	    	 isDeleted = true;
	     }
	     finally {
	    	 /* Releasing PreparedStatment objects */
      
             oDBUtility.releaseResources(pstmt);           
         }   
		 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
		 return isDeleted;
	}
	
	/* (non-Javadoc)
 	 * @see java.lang.String#getSalesManagerId(java.sql.Connection, in.com.rbc.quotation.enquiry.dto.EnquiryDto)
 	 */ 
	public String getSalesManagerId(Connection conn, EnquiryDto enquiryDto)throws Exception{
		
		String sSalesId = "";
		  /* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();	    
        
        try {
        	pstmt = conn.prepareStatement(IS_SALES_MANAGER);
        	pstmt.setString(1, enquiryDto.getCreatedBySSO());
        	rs = pstmt.executeQuery();
            if (rs.next()){
            	do {            
            		sSalesId = rs.getString("QSM_SALESMGRID");
            	}while (rs.next());
            }
        } finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
		return sSalesId;		
	}
	
	public String getSalesManagerId(Connection conn, String smSSO) throws Exception {
		String sSalesId = "";
		
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null ;
		/* ResultSet object to store the rows retrieved from database */
		ResultSet rs = null ;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();	 
		
		try {
        	pstmt = conn.prepareStatement(IS_SALES_MANAGER);
        	pstmt.setString(1, smSSO);
        	rs = pstmt.executeQuery();
            if (rs.next()){
            	do {            
            		sSalesId = rs.getString("QSM_SALESMGRID");
            	}while (rs.next());
            }
        } finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
		
		return sSalesId;
	}
	
	
	/* (non-Javadoc)
 	 * @see boolean#updatingTechincalOfferData(java.sql.Connection, in.com.rbc.quotation.enquiry.dto.TechnicalOfferDto)
 	 */ 
	public boolean updatingTechincalOfferData(Connection conn, EnquiryDto oEnquiryDto, RatingDto oRatingDto, TechnicalOfferDto oTechnicalOfferDto)throws Exception{
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        String sSQLUpdateQuery = null;
        int iInserted = 0;
        
        boolean isUpdated= false;
        Clob oMyClob=null;
        
        try {
        	if (oTechnicalOfferDto.getRatingId() > 0){
        		sSQLUpdateQuery = buildTechnicalOfferQuery(oTechnicalOfferDto);
	    		 
	    		 if (sSQLUpdateQuery != null && sSQLUpdateQuery.trim().length() > 0){
	    			 pstmt = conn.prepareStatement(sSQLUpdateQuery);
	    			 pstmt.setString(1, oTechnicalOfferDto.getFrameSize());
	    			 pstmt.setString(2, oTechnicalOfferDto.getEfficiency());
	    			 pstmt.setString(3, oTechnicalOfferDto.getPf());
	    			 pstmt.setString(4, oTechnicalOfferDto.getFlcAmps());
	    			 pstmt.setString(5, oTechnicalOfferDto.getPotFLT());
	    			 pstmt.setString(6, oTechnicalOfferDto.getFlcSQCAGE());
	    			 pstmt.setString(7, oTechnicalOfferDto.getNoiseLevel());
	    			 pstmt.setString(8, oTechnicalOfferDto.getFltSQCAGE());
	    			 pstmt.setString(9, oTechnicalOfferDto.getRpm());
	    			 pstmt.setString(10, oTechnicalOfferDto.getRtGD2());
	    			 pstmt.setString(11, oTechnicalOfferDto.getSstSQCAGE());
	    			 pstmt.setString(12, oTechnicalOfferDto.getRv());
	    			 pstmt.setString(13, oTechnicalOfferDto.getRa());
	    			 pstmt.setString(14, oTechnicalOfferDto.getVibrationLevel());
	    			 pstmt.setString(15, oTechnicalOfferDto.getRemarks());
	    			 pstmt.setString(16, oTechnicalOfferDto.getCommercialOfferType());
	    			 pstmt.setLong(17, oTechnicalOfferDto.getCommercialMCUnit());
	    			 pstmt.setString(18, oTechnicalOfferDto.getCreatedBy());
	    			 pstmt.setString(19, oTechnicalOfferDto.getAreaClassification());
	    			 pstmt.setString(20, oTechnicalOfferDto.getServiceFactor());	    			
	    			 pstmt.setString(21, oTechnicalOfferDto.getBkw());	    			
	    			 pstmt.setString(22, oTechnicalOfferDto.getNlCurrent());
	    			 pstmt.setString(23, oTechnicalOfferDto.getStrtgCurrent());
	    			 pstmt.setString(24, oTechnicalOfferDto.getLrTorque());
	    			 pstmt.setString(25, oTechnicalOfferDto.getPllCurr1());
	    			 pstmt.setString(26, oTechnicalOfferDto.getPllCurr2());
	    			 pstmt.setString(27, oTechnicalOfferDto.getPllCurr3());
	    			 pstmt.setString(28, oTechnicalOfferDto.getPllCurr4());
	    			 pstmt.setString(29, oTechnicalOfferDto.getPllEff1());
	    			 pstmt.setString(30, oTechnicalOfferDto.getPllEff2());	    			
	    			 pstmt.setString(31, oTechnicalOfferDto.getPllEff3());	    			
	    			 pstmt.setString(32, oTechnicalOfferDto.getPllPf1());
	    			 pstmt.setString(33, oTechnicalOfferDto.getPllPf2());
	    			 pstmt.setString(34, oTechnicalOfferDto.getPllpf3());
	    			 pstmt.setString(35, oTechnicalOfferDto.getPllpf4());
	    			 pstmt.setString(36, oTechnicalOfferDto.getSstHot());
	    			 pstmt.setString(37, oTechnicalOfferDto.getSstCold());
	    			 pstmt.setString(38, oTechnicalOfferDto.getStartTimeRV());
	    			 pstmt.setString(39, oTechnicalOfferDto.getStartTimeRV80());
	    			 pstmt.setString(40, oTechnicalOfferDto.getCoolingSystem());	    			
	    			 pstmt.setString(41, oTechnicalOfferDto.getBearingDENDE());	    			 
	    			 pstmt.setString(42, oTechnicalOfferDto.getLubrication());
	    			 pstmt.setString(43, oTechnicalOfferDto.getMigD2Load());
	    			 pstmt.setString(44, oTechnicalOfferDto.getBalancing());
	    			 pstmt.setString(45, oTechnicalOfferDto.getMigD2Motor());
	    			 pstmt.setString(46, oTechnicalOfferDto.getMainTermBoxPS());
	    			 pstmt.setString(47, oTechnicalOfferDto.getNeutralTermBox());
	    			 pstmt.setString(48, oTechnicalOfferDto.getCableEntry());
	    			 pstmt.setString(49, oTechnicalOfferDto.getAuxTermBox());
	    			 pstmt.setString(50, oTechnicalOfferDto.getStatorConn());	    			
	    			 pstmt.setString(51, oTechnicalOfferDto.getNoBoTerminals());	    			 
	    			 pstmt.setString(52, oTechnicalOfferDto.getRotation());
	    			 pstmt.setString(53, oTechnicalOfferDto.getMotorTotalWgt());
	    			 pstmt.setString(54, oTechnicalOfferDto.getSpaceHeater());
	    			 pstmt.setString(55, oTechnicalOfferDto.getWindingRTD());
	    			 pstmt.setString(56, oTechnicalOfferDto.getBearingRTD());
	    			 pstmt.setString(57, oTechnicalOfferDto.getMinStartVolt());
	    			 pstmt.setString(58, oTechnicalOfferDto.getBearingThermoDT());
	    			 pstmt.setString(59, oTechnicalOfferDto.getAirThermo());
	    			 pstmt.setString(60, oTechnicalOfferDto.getVibeProbMPads());	    			
	    			 pstmt.setString(61, oTechnicalOfferDto.getEncoder());	    			 
	    			 pstmt.setString(62, oTechnicalOfferDto.getSpeedSwitch());
	    			 pstmt.setString(63, oTechnicalOfferDto.getSurgeSuppressors());
	    			 pstmt.setString(64, oTechnicalOfferDto.getCurrTransformers());
	    			 pstmt.setString(65, oTechnicalOfferDto.getVibProbes());
	    			 pstmt.setString(66, oTechnicalOfferDto.getSpmNipple());
	    			 pstmt.setString(67, oTechnicalOfferDto.getKeyPhasor());
	    			 pstmt.setString(68, oTechnicalOfferDto.getPaintColor());
	    			 pstmt.setString(69, oTechnicalOfferDto.getZone());
	    			 pstmt.setString(70, oTechnicalOfferDto.getGasGroup());
	    			 oMyClob=conn.createClob();
	    			 oMyClob.setString(1, oTechnicalOfferDto.getDeviation_clarification());
	    			 pstmt.setClob(71, oMyClob);
	    			 pstmt.setString(72, oTechnicalOfferDto.getPllCurr5());
	    			 pstmt.setString(73, oTechnicalOfferDto.getPllEff4());
	    			 pstmt.setString(74, oTechnicalOfferDto.getPllPf5());
	    			 
	    			 
	    			 
	    			 iInserted = pstmt.executeUpdate();   
	    			 if (iInserted > 0)
	    				 isUpdated = true;
	    		 }
        	}
        	oDBUtility.releaseResources(pstmt);
        
        	if (oRatingDto.getRatingId() > 0){
        		sSQLUpdateQuery = buildRatingUpdateQuery(oRatingDto, oRatingDto.getRatingId());
        		if (sSQLUpdateQuery != null && sSQLUpdateQuery.trim().length() > 0){
        			pstmt = conn.prepareStatement(sSQLUpdateQuery);
        			 iInserted = pstmt.executeUpdate();
        			 if (iInserted > 0 && isUpdated != false)
	    				 isUpdated = true;
	    		 }
	    	 }
        	
        	if (oEnquiryDto.getEnquiryId() > 0){
        	
        			 pstmt = conn.prepareStatement(UPDATE_ENQUIRY_OFFER);
        			 pstmt.setInt(1,oEnquiryDto.getCustomerId());
        			 pstmt.setString(2,oEnquiryDto.getEndUser());
        			 pstmt.setString(3,oEnquiryDto.getEnquiryReferenceNumber());
        			 pstmt.setString(4,oEnquiryDto.getRevision());
        			 pstmt.setString(5,oEnquiryDto.getLastModifiedDate());
        			 pstmt.setString(6,oEnquiryDto.getProjectName());
        			 pstmt.setString(7,oEnquiryDto.getSavingIndent());
        			 pstmt.setString(8,oEnquiryDto.getDmnote());
        			 pstmt.setInt(9,oEnquiryDto.getEnquiryId());   			 
        			 
        			 iInserted = pstmt.executeUpdate();
        			 if (iInserted > 0 && isUpdated != false)
	    				 isUpdated = true;
	    		 
	    	 }
            
        }finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(pstmt);
        }
        
		return isUpdated;			
	}
	
	/* (non-Javadoc)
 	 * @see boolean#updatingTechincalOfferData(java.sql.Connection, in.com.rbc.quotation.enquiry.dto.TechnicalOfferDto)
 	 */ 
	private String buildTechnicalOfferQuery(TechnicalOfferDto oTechnicalOfferDto){
		 String sMethodName = "buildTechnicalOfferQuery";
		 
		 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		 StringBuffer sbFieldQuery = new StringBuffer();
		 
		 if (oTechnicalOfferDto != null){
			 sbFieldQuery.append(UPDATE_OFFER_DATA);			 
			 
			
			
			 if (! oTechnicalOfferDto.getUpdateCreateBy().equalsIgnoreCase("false")){
				 /* QRD_TO_CREATEDBY */			 
				 sbFieldQuery.append(", QRD_TO_CREATEDBY = ");
				 sbFieldQuery.append("'"+oTechnicalOfferDto.getCreatedBy()+"', ");				 
			 
			 
				 /* QRD_TO_CREATEDDATE */			 
				 sbFieldQuery.append("QRD_TO_CREATEDDATE = ");
				 sbFieldQuery.append("SYSDATE");
			 }
			 
			
			
			sbFieldQuery.append(" WHERE QRD_RATINGID = "+oTechnicalOfferDto.getRatingId());
		 }
		 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "sbFieldQuery ...... "+sbFieldQuery);
		 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
		 return sbFieldQuery.toString();
	 }
	
	/* (non-Javadoc)
 	 * @see boolean#deleteAllAddOnByRatingId(java.sql.Connection, int)
 	 */ 
	public boolean deleteAllAddOnByRatingId(Connection conn, int iRatingId)throws Exception{			
		boolean isDeleted = false;
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        try {
     
        	pstmt = conn.prepareStatement(DELETE_ADDONS);
        	pstmt.setInt(1, iRatingId);	            
        	pstmt.executeUpdate();
            isDeleted = true;
            
        }finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(pstmt);
        }
        
		return isDeleted;			
	}
	
	/* (non-Javadoc)
 	 * @see boolean#updateAddOn(java.sql.Connection, in.com.rbc.quotation.enquiry.dto.RatingDto)
 	 */ 
	public boolean updateAddOn(Connection conn, RatingDto oRatingDto)throws Exception {
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        AddOnDto oAddOnDto = null;
        int iInserted = 0;
        int iTotalAddOn = 0;
        
        boolean isInserted = false;
        
        try {
        	if (oRatingDto.getRatingId() > 0 && oRatingDto.getAddOnList().size() > 0){
        		
        		for (int i=0; i<oRatingDto.getAddOnList().size(); i++){
        			oAddOnDto = (AddOnDto) oRatingDto.getAddOnList().get(i);
        			if (oAddOnDto != null){
        			
        				pstmt = conn.prepareStatement(INSERT_ADDON_DETAILS);	
                    
        				pstmt.setInt(1, oAddOnDto.getAddOnRatingId());	        				
        				pstmt.setString(2, oAddOnDto.getAddOnTypeText().trim());
        				pstmt.setInt(3, oAddOnDto.getAddOnQuantity());
        				pstmt.setDouble(4, oAddOnDto.getAddOnUnitPrice());
        				pstmt.setInt(5, oAddOnDto.getAddOnUnitPriceCE());
        				pstmt.setInt(6, oAddOnDto.getAddOnUnitPriceRSM());
    	    			 
        				 iInserted = pstmt.executeUpdate();     		 
    	    			 if (iInserted > 0)
    	    				 iTotalAddOn++;
        			}
        		}
        		if (iTotalAddOn > 0)
        			isInserted = true;
	    	 }
        }finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(pstmt);
        }
		return isInserted;			
	}
	
	/* (non-Javadoc)
 	 * @see in.com.rbc.quotation.enquiry.dto.TeamMemberDto#getTeamMemberDetails(java.sql.Connection, in.com.rbc.quotation.enquiry.dto.TeamMemberDto)
 	 */ 
	public TeamMemberDto getTeamMemberDetails (Connection conn,  TeamMemberDto oTeamMemberDto) throws Exception{
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        ArrayList arlTeamMembers = new ArrayList();
        
        KeyValueVo oKeyValueVo = null;
        
        try {
        	pstmt = conn.prepareStatement(FETCH_TEAMMEMBER_DETAILS);
        	pstmt.setInt(1, oTeamMemberDto.getEnquiryId());
            rs = pstmt.executeQuery();
            
            if (rs != null && rs.next()){ 
            	oTeamMemberDto.setDesignEngineer(rs.getString("AST_DESIGNENGINEER"));
            	oTeamMemberDto.setDesignManager(rs.getString("AST_DESIGNMANAGER"));
            	oTeamMemberDto.setPrimaryRSM(rs.getString("AST_PRIMARYRSM"));
            	oTeamMemberDto.setAdditionalRSM(rs.getString("AST_ADDITIONALRSM"));
            	oTeamMemberDto.setChiefExecutive(rs.getString("AST_CHIEFEXECUTIVE"));
            	oTeamMemberDto.setSalesManager(rs.getString("AST_SALESMANAGER"));
            	oTeamMemberDto.setCommercialManager(rs.getString("AST_COMMERCIALMANAGER"));
            	oTeamMemberDto.setCommercialEngineer(rs.getString("AST_COMMERCIALENGINEER"));

            	
            	if (oTeamMemberDto.getMembertype().equalsIgnoreCase("CE")){
            		
            		if (rs.getString("AST_DESIGNMANAGER") != null && 
            				rs.getString("AST_DESIGNMANAGER").trim().length() > 0){
            			oKeyValueVo = new KeyValueVo();
            			oKeyValueVo.setKey(rs.getString("AST_DESIGNMANAGER")+"|"+rs.getString("AST_DESIGNMANAGEREMAIL")+"|DM");
            			oKeyValueVo.setValue(rs.getString("AST_DESIGNMANAGERNAME"));
            			oKeyValueVo.setValue2(rs.getString("AST_DESIGNMANAGEREMAIL"));
            			
            			arlTeamMembers.add(oKeyValueVo);
            		}
            		
            		if (rs.getString("AST_DESIGNENGINEER") != null && 
            				rs.getString("AST_DESIGNENGINEER").trim().length() > 0){
            			oKeyValueVo = new KeyValueVo();
            			oKeyValueVo.setKey(rs.getString("AST_DESIGNMANAGER")+"|"+rs.getString("AST_DESIGNMANAGEREMAIL")+"|DE");
            			oKeyValueVo.setValue(rs.getString("AST_DESIGNENGINEERNAME"));
            			oKeyValueVo.setValue2(rs.getString("AST_DESIGNENGINEEREMAIL"));
            			
            			arlTeamMembers.add(oKeyValueVo);	 
            		}	            		
            	}
            	else if (oTeamMemberDto.getMembertype().equalsIgnoreCase("DM")){
            		if (rs.getString("AST_SALESMANAGER") != null && 
            				rs.getString("AST_SALESMANAGER").trim().length() > 0){
            			oKeyValueVo = new KeyValueVo();
            			oKeyValueVo.setKey(rs.getString("AST_SALESMANAGER")+"|"+rs.getString("AST_SALESMANAGEREMAIL")+"|SM");
            			oKeyValueVo.setValue(rs.getString("AST_SALESMANAGERNAME"));
            			oKeyValueVo.setValue2(rs.getString("AST_SALESMANAGEREMAIL"));
            			
            			arlTeamMembers.add(oKeyValueVo);
            		}
	
            	}
            	else if (oTeamMemberDto.getMembertype().equalsIgnoreCase("CM")){
            		
            		if (rs.getString("AST_SALESMANAGER") != null && 
            				rs.getString("AST_SALESMANAGER").trim().length() > 0){
            			oKeyValueVo = new KeyValueVo();
            			oKeyValueVo.setKey(rs.getString("AST_SALESMANAGER")+"|"+rs.getString("AST_SALESMANAGEREMAIL")+"|SM");
            			oKeyValueVo.setValue("SM-"+rs.getString("AST_SALESMANAGERNAME"));
            			oKeyValueVo.setValue2(rs.getString("AST_SALESMANAGEREMAIL"));
            			
            			arlTeamMembers.add(oKeyValueVo);
            		}

            		
            		if (rs.getString("AST_COMMERCIALMANAGER") != null && 
            				rs.getString("AST_COMMERCIALMANAGER").trim().length() > 0){
            			oKeyValueVo = new KeyValueVo();
            			oKeyValueVo.setKey(rs.getString("AST_COMMERCIALMANAGER")+"|"+rs.getString("AST_COMMERCIALMANAGEREMAIL")+"|CM");
            			oKeyValueVo.setValue("CM-"+rs.getString("AST_COMMERCIALMANAGERNAME"));
            			oKeyValueVo.setValue2(rs.getString("AST_COMMERCIALMANAGEREMAIL"));
            			
            			arlTeamMembers.add(oKeyValueVo);
            		}
            		
	
            	}


            	else if (oTeamMemberDto.getMembertype().equalsIgnoreCase("CE|RSM|DM"))
            	{
            		if (rs.getString("AST_DESIGNMANAGER") != null && 
            				rs.getString("AST_DESIGNMANAGER").trim().length() > 0){
            			oKeyValueVo = new KeyValueVo();
            			oKeyValueVo.setKey(rs.getString("AST_DESIGNMANAGER")+"|"+rs.getString("AST_DESIGNMANAGEREMAIL")+"|DM");
            			oKeyValueVo.setValue(rs.getString("AST_DESIGNMANAGERNAME"));
            			oKeyValueVo.setValue2(rs.getString("AST_DESIGNMANAGEREMAIL"));
            			
            			arlTeamMembers.add(oKeyValueVo);
            		}
            		
            		if (rs.getString("AST_PRIMARYRSM") != null && 
            				rs.getString("AST_PRIMARYRSM").trim().length() > 0){
            			oKeyValueVo = new KeyValueVo();
            			oKeyValueVo.setKey(rs.getString("AST_PRIMARYRSM")+"|"+rs.getString("AST_PRIMARYRSMEMAIL")+"|RSM");
            			oKeyValueVo.setValue("RSM "+rs.getString("AST_PRIMARYRSMNAME"));
            			oKeyValueVo.setValue2(rs.getString("AST_PRIMARYRSMEMAIL"));
            			
            			arlTeamMembers.add(oKeyValueVo);
            		}
            		
            		if (rs.getString("AST_CHIEFEXECUTIVE") != null && 
            				rs.getString("AST_CHIEFEXECUTIVE").trim().length() > 0){
            			oKeyValueVo = new KeyValueVo();
            			oKeyValueVo.setKey(rs.getString("AST_CHIEFEXECUTIVE")+"|"+rs.getString("AST_CHIEFEXECUTIVEEMAIL")+"|CE");
            			oKeyValueVo.setValue("CE "+rs.getString("AST_CHIEFEXECUTIVENAME"));
            			oKeyValueVo.setValue2(rs.getString("AST_CHIEFEXECUTIVEEMAIL"));
            			
            			arlTeamMembers.add(oKeyValueVo);
            		}
            		
            	}
            	oTeamMemberDto.setTeamMemberList(arlTeamMembers);
            }
        } finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
        
		return oTeamMemberDto;
	}
	
	/* (non-Javadoc)
 	 * @see boolean#updateFactorDetails(java.sql.Connection, in.com.rbc.quotation.enquiry.dto.EnquiryDto, java.lang.String)
 	 */ 
	public boolean updateFactorDetails (Connection conn,  EnquiryDto oEnquiryDto, String sType) throws Exception{
		boolean isUpdated = false;
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
		EnquiryDto oEnquiryDtos=null;
		String sRatingId="";
		Map.Entry mpEntry=null;
		Map mpFinalMap=null;
		

        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        int iInserted = 0;
        try {

        	if (sType.equalsIgnoreCase("CE"))
        	{
        		pstmt = conn.prepareStatement(UPDATE_ENQUIRY_MULTIPLIER_FORCE);
        		pstmt.setString(1, oEnquiryDto.getCustomerNameMultiplier().equals("")?"0.00":oEnquiryDto.getCustomerNameMultiplier());
        		pstmt.setString(2, oEnquiryDto.getCustomerTypeMultiplier().equals("")?"0.00":oEnquiryDto.getCustomerTypeMultiplier());
        		pstmt.setString(3, oEnquiryDto.getEndUserMultiplier().equals("")?"0.00":oEnquiryDto.getEndUserMultiplier());
        		pstmt.setString(4, oEnquiryDto.getWarrantyDispatchMultiplier().equals("")?"0.00":oEnquiryDto.getWarrantyDispatchMultiplier());
        		pstmt.setString(5, oEnquiryDto.getWarrantycommissioningMultiplier().equals("")?"0.00":oEnquiryDto.getWarrantycommissioningMultiplier());
        		pstmt.setString(6, oEnquiryDto.getAdvancePaymMultiplier().equals("")?"0.00":oEnquiryDto.getAdvancePaymMultiplier());
        		pstmt.setString(7, oEnquiryDto.getPayTermMultiplier().equals("")?"0.00":oEnquiryDto.getPayTermMultiplier());
        		pstmt.setString(8, oEnquiryDto.getExpDeliveryMultiplier().equals("")?"0.00":oEnquiryDto.getExpDeliveryMultiplier());
        		pstmt.setString(9, oEnquiryDto.getFinanceMultiplier().equals("")?"0.00":oEnquiryDto.getFinanceMultiplier());
        		pstmt.setInt(10, oEnquiryDto.getEnquiryId());

            
    			iInserted = pstmt.executeUpdate();  


        	}
        	else
        	{
        	}
        		
			if (iInserted > 0)
   			 	isUpdated = true;
			//UPDATE_RATING_MULTIPLIER_FORCE
			mpFinalMap=oEnquiryDto.getMultiplierMap();
			Iterator iter = mpFinalMap.entrySet().iterator();

			while (iter.hasNext()) {
				mpEntry = (Map.Entry) iter.next();
				if(mpEntry!=null)
				{
		
				oEnquiryDtos=(EnquiryDto) mpEntry.getValue();
				
				sRatingId=(String)mpEntry.getKey();
		        if(oEnquiryDtos!=null && sRatingId!="")
		        {
	        	if (sType.equalsIgnoreCase("CE"))
	        	{
	        		pstmt = conn.prepareStatement(UPDATE_RATING_MULTIPLIER_FORCE);
	      
	        		pstmt.setString(1, oEnquiryDtos.getQuantityMultiplier());
	        		pstmt.setString(2, oEnquiryDtos.getRatedOutputPNMulitplier());
	        		pstmt.setString(3, oEnquiryDtos.getTypeMultiplier());
	        		pstmt.setString(4, oEnquiryDtos.getAreaCMultiplier());
	        		pstmt.setString(5, oEnquiryDtos.getFrameMultiplier());
	        		pstmt.setString(6, oEnquiryDtos.getApplicationMultiplier()); 
	        		pstmt.setString(7, oEnquiryDtos.getFactorMultiplier());
	        		pstmt.setString(8, oEnquiryDtos.getNetmcMultiplier());
	        		pstmt.setString(9, oEnquiryDtos.getMarkupMultiplier());
	        		pstmt.setString(10, oEnquiryDtos.getTransferCMultiplier());
	        		pstmt.setInt(11, Integer.parseInt(sRatingId));
	        		pstmt.setInt(12, oEnquiryDto.getEnquiryId());
		        	iInserted = pstmt.executeUpdate();  

	        	}
	        	else
	        	{
	        		pstmt = conn.prepareStatement(UPDATE_RATING_MULTIPLIER_FORRSM);
	        		pstmt.setString(1, oEnquiryDtos.getTransferCMultiplier());
	        		pstmt.setString(2, oEnquiryDtos.getMcornspratioMultiplier_rsm());
	        		pstmt.setString(3, oEnquiryDtos.getTransportationperunit_rsm());
	        		pstmt.setString(4, oEnquiryDtos.getWarrantyperunit_rsm());
	        		pstmt.setString(5, oEnquiryDtos.getUnitpriceMultiplier_rsm());
	        		pstmt.setString(6, oEnquiryDtos.getTotalpriceMultiplier_rsm());
	        		pstmt.setInt(7, Integer.parseInt(sRatingId));
	        		pstmt.setInt(8, oEnquiryDto.getEnquiryId());
		        	iInserted = pstmt.executeUpdate();     		 

	        	}
	        		
				if (iInserted > 0)
					isUpdated = true;
				
				}
				}

			}

        }finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(pstmt);
        }
        
		return isUpdated;
	}
	
	public boolean updateQuotationTerms(Connection conn, EnquiryDto enquiryDto) throws Exception {
		  /* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
         
        boolean isUpdated = false;
        
        try {
        	pstmt = conn.prepareStatement(UPDATE_QUOTATIONTERMS);
        	pstmt.setString(1,enquiryDto.getTermsVarFormula());
        	pstmt.setString(2,enquiryDto.getTermsDeliveryLT());
        	pstmt.setString(3,enquiryDto.getTermsDeliveryHT());
        	pstmt.setString(4,enquiryDto.getTermsPercentAdv());
        	pstmt.setString(5,enquiryDto.getTermsPercentBal());
        	pstmt.setString(6,enquiryDto.getLastModifiedBy());
        	pstmt.setString(7,enquiryDto.getEnquiryId()+"");
        	
           	if(pstmt.executeUpdate()>=1){
           		isUpdated = true;
           	}
        	
        } finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(pstmt);
        }
        
        return isUpdated;
	}
	
	/* (non-Javadoc)
 	 * @see java.util.ArrayList#getManagersList(java.sql.Connection, java.lang.String, java.lang.String)
 	 */ 
	public ArrayList getManagersList(Connection conn, String sType, String sCurrentUserId) throws Exception{
		ArrayList arlManagersList = new ArrayList();
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
	    KeyValueVo oKeyValueVo = null;
        
        try{
        	pstmt = conn.prepareStatement(FETCH_MANAGERS_LIST);
        	rs = pstmt.executeQuery();
            
            if (rs != null && rs.next()){
            	do {
            		if (! sCurrentUserId.equalsIgnoreCase(rs.getString("QDE_ENGINEERSSO"))){
            			oKeyValueVo = new KeyValueVo();
            		
            			oKeyValueVo.setKey(rs.getString("QDE_ENGINEERSSO"));
            			oKeyValueVo.setValue(rs.getString("EMP_NAMEFULL"));
            			arlManagersList.add(oKeyValueVo);
            		}
            	}while(rs.next());
            }
        } finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
        return arlManagersList;
	}
	
	/* (non-Javadoc)
 	 * @see boolean#validateUser(java.sql.Connection, in.com.rbc.quotation.enquiry.dto.WorkflowDto)
 	 */ 
	public boolean validateUser(Connection conn, WorkflowDto oWorkflowDto) throws Exception{
		boolean isValidUser = false;
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        StringBuffer sbQuery =null;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        try {
        	sbQuery = new StringBuffer ("SELECT COUNT(QWF_ENQUIRYID) AS COUNT FROM "+SCHEMA_QUOTATION+".RFQ_WORKFLOW WHERE " );
        	sbQuery.append(" QWF_ENQUIRYID = "+oWorkflowDto.getEnquiryId()+" AND QWF_ASSIGNTO = '"+oWorkflowDto.getAssignedTo()+"' ");
        	sbQuery.append(" AND QWF_WORKFLOWSTATUS = '"+oWorkflowDto.getWorkflowStatus()+"' AND QWF_OLDSTATUSID = "+oWorkflowDto.getOldStatusId());
        	
        	pstmt = conn.prepareStatement(sbQuery.toString());
        	rs = pstmt.executeQuery();
        	if (rs.next()) {	        	
        	 	if( rs.getInt("COUNT") > 0)
             		isValidUser = true;	             	
             }
        } finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
        
		return isValidUser;
	}
	
	/* (non-Javadoc)
 	 * @see boolean#checkUserRole(java.sql.Connection, in.com.rbc.quotation.enquiry.dto.EnquiryDto, in.com.rbc.quotation.user.dto.UserDto, java.lang.String)
 	 */ 
	public boolean checkUserRole(Connection conn, EnquiryDto oEnquiryDto, UserDto oUserDto, String sUserType)throws Exception{
		boolean isValidUser = false;
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        StringBuffer sbQuery = null;
        String sQuery = null;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        try {
        	sbQuery = new StringBuffer ("SELECT COUNT(AST_ENQUIRYID) AS COUNT FROM "+SCHEMA_QUOTATION+".RFQ_TEAMMEMBERS_V WHERE " );
        	sbQuery.append(" AST_ENQUIRYID = ?");
        	
        	
        	if (sUserType.equalsIgnoreCase("SM")){
        		sbQuery.append(" AND AST_SALESMANAGER = '"+oUserDto.getUserId()+"'");
        	} else if (sUserType.equalsIgnoreCase("DM")){
        		sbQuery.append(" AND (AST_DESIGNMANAGER = '"+oUserDto.getUserId()+"' OR ");
        		sbQuery.append(" AST_DESIGNENGINEER = '"+oUserDto.getUserId()+"' ) ");
        	} else if (sUserType.equalsIgnoreCase("CE")){
        		sbQuery.append(" AND AST_CHIEFEXECUTIVE = '"+oUserDto.getUserId()+"'");        		
        	} else if (sUserType.equalsIgnoreCase("RSM")){
        		sbQuery.append(" AND AST_PRIMARYRSM = '"+oUserDto.getUserId()+"'");        		
        	} else if (sUserType.equalsIgnoreCase("NSM")){
        		sbQuery.append(" AND AST_NSM = '"+oUserDto.getUserId()+"'");  
        	} else if (sUserType.equalsIgnoreCase("MH")){
        		sbQuery.append(" AND AST_MH = '"+oUserDto.getUserId()+"'");  
        	} else if (sUserType.equalsIgnoreCase("CM")){
        		sbQuery.append(" AND (AST_COMMERCIALMANAGER = '"+oUserDto.getUserId()+"' OR ");
        		sbQuery.append(" AST_COMMERCIALENGINEER = '"+oUserDto.getUserId()+"' ) ");
        	} else if (sUserType.equalsIgnoreCase("BH")){
        		sbQuery.append(" AND AST_BH = '"+oUserDto.getUserId()+"'"); 
        	}

        	pstmt = conn.prepareStatement(sbQuery.toString());
        	pstmt.setInt(1, oEnquiryDto.getEnquiryId());
        	rs = pstmt.executeQuery();
        	if (rs.next()) {	        	
        	 	if( rs.getInt("COUNT") > 0) 
        	 		isValidUser = true;	             	
            }
        	oDBUtility.releaseResources(rs, pstmt);
        	
        	// For Re-assign Flow : Default Role not set in TeamMembers View - Check the User in User Details Table.
        	sQuery = " SELECT * FROM " + SCHEMA_QUOTATION + ".RFQ_LT_USERS_DETAILS WHERE QUD_LT_USER_ISDELETED = 'N' AND QUD_LT_USER_SSO = ?";
        	pstmt = conn.prepareStatement(sQuery);
        	pstmt.setString(1, oUserDto.getUserId());
        	rs = pstmt.executeQuery();
        	if (rs.next()) {
        		isValidUser = true;	
        	}
        	
        } finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
	
		return isValidUser;
	}
	
	public String checkApprovalType(Connection conn, EnquiryDto oEnquiryDto, UserDto userDto) throws Exception {
		String sApprovalType = "";
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        StringBuffer sbQuery =null;
        
        try {
        	sbQuery = new StringBuffer ("SELECT * FROM "+SCHEMA_QUOTATION+".RFQ_TEAMMEMBERS_V WHERE AST_ENQUIRYID = ? " );
        	pstmt = conn.prepareStatement(sbQuery.toString());
        	pstmt.setInt(1, oEnquiryDto.getEnquiryId());
        	
        	rs = pstmt.executeQuery();
        	
        	if (rs.next()) {	        	
        	 	 if(userDto.getUserId().equals(rs.getString("AST_SALESMANAGER").trim())) {
        	 		sApprovalType = "SM";
        	 	 } else if(userDto.getUserId().equals(rs.getString("AST_PRIMARYRSM").trim())) {
        	 		sApprovalType = "RSM";
        	 	 } else if(userDto.getUserId().equals(rs.getString("AST_NSM").trim())) {
        	 		sApprovalType = "NSM";
        	 	 } else if(userDto.getUserId().equals(rs.getString("AST_MH").trim())) {
        	 		sApprovalType = "MH";
        	 	 } else {
         	 		sApprovalType = "NORMAL_USER";
         	 	 } 
            }
        }finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
        return sApprovalType;
	}
	
	/* (non-Javadoc)
 	 * @see boolean#checkForWorkflowPendingStatus(java.sql.Connection, in.com.rbc.quotation.workflow.dto.WorkflowDto)
 	 */ 
	public boolean checkForWorkflowPendingStatus(Connection conn, WorkflowDto oWorkflowDto)throws Exception{
		boolean isPending = false;
        
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        StringBuffer sbQuery =null;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        try {
        	sbQuery = new StringBuffer ("SELECT COUNT(QWF_ENQUIRYID) AS COUNT FROM "+SCHEMA_QUOTATION+".RFQ_WORKFLOW WHERE " );
        	sbQuery.append(" QWF_ENQUIRYID = "+oWorkflowDto.getEnquiryId()+" AND QWF_ASSIGNTO = '"+oWorkflowDto.getAssignedTo()+"' ");
        	sbQuery.append(" AND QWF_WORKFLOWSTATUS = '"+oWorkflowDto.getWorkflowStatus()+"'");
        	LoggerUtility.log("INFO", this.getClass().getName(), "checkForWorkflowPendingStatus", "Check Query - " + sbQuery);
            
        	
        	pstmt = conn.prepareStatement(sbQuery.toString());
        	rs = pstmt.executeQuery();
        	if (rs.next()) {	        	
        	 	if( rs.getInt("COUNT") > 0)
        	 		isPending = true;	             	
            }
        } finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }

		return isPending;
	}
	
	public EnquiryDto getEnquiryStatus(Connection conn, EnquiryDto oEnquiryDto)throws Exception{
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        try {        	
        	pstmt = conn.prepareStatement(FETCH_ENQUIRY_STATUS);
        	pstmt.setInt(1, oEnquiryDto.getEnquiryId());
        	rs = pstmt.executeQuery();
        	if (rs.next()) {	        	
        	 	 oEnquiryDto.setStatusId(rs.getInt("QEM_STATUSID"));  	
            }
        } finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
		return oEnquiryDto;
	}
	
	/**
	 * To fetch the list of values from ENQUIRY PICKLIST table for given lookup field
	 * @param conn Connection object to connect to database
	 * @param lookupfield string - query parameter
	 * @return Returns ArrayList object
	 * @throws Exception If any error occurs during the process
	 * @author 610092227 Kalyani U
	 * created on June 20, 2018
	 */
	public ArrayList<KeyValueVo> getOptionsbyAttributeList(Connection conn,String sLookupField) throws Exception {

		/**
		 * Setting the Method Name as generic for logger Utility purpose .
		 */
		String sMethodName = "getOptionsbyAttributeList";
		String sClassName = this.getClass().getName();

		LoggerUtility.log("INFO", sClassName, sMethodName,
				"************************START******************");

		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null;

		/* ResultSet object to store the rows retrieved from database */
		ResultSet rs = null;

		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();

		ArrayList<KeyValueVo> arlOptions = new ArrayList<KeyValueVo>();		
		KeyValueVo oKeyValueVo = null;

		try {			

			pstmt = conn.prepareStatement(FETCH_ENQUIRY_LIST);
			pstmt.setString(1, sLookupField);			
			rs = pstmt.executeQuery();

			if (rs.next()) {				
				do {
					oKeyValueVo = new KeyValueVo();
					oKeyValueVo.setKey(rs.getString("QEM_KEY"));
					oKeyValueVo.setValue(rs.getString("QEM_VALUE"));
					arlOptions.add(oKeyValueVo);
				} while (rs.next());
			}
		} finally {
			/* Releasing ResultSet & PreparedStatement objects */
			oDBUtility.releaseResources(rs, pstmt);
		}

		LoggerUtility.log("INFO", sClassName, sMethodName,
				"************************END******************");

		return arlOptions;
	}
	
	/**
	 * To fetch the list of values in Ascending Order from ENQUIRY PICKLIST table for given lookup field
	 * @param conn Connection object to connect to database
	 * @param lookupfield string - query parameter
	 * @return Returns ArrayList object
	 * @throws Exception If any error occurs during the process
	 */
	public ArrayList<KeyValueVo> getAscendingOptionsbyAttributeList(Connection conn,String sLookupField) throws Exception {
		String sMethodName = "getOptionsbyAttributeList";
		String sClassName = this.getClass().getName();
		LoggerUtility.log("INFO", sClassName, sMethodName, "************************START******************");

		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null;
		/* ResultSet object to store the rows retrieved from database */
		ResultSet rs = null;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();

		ArrayList<KeyValueVo> arlOptions = new ArrayList<KeyValueVo>();		
		KeyValueVo oKeyValueVo = null;

		try {			
			pstmt = conn.prepareStatement(FETCH_ENQUIRY_LIST_ASC);
			pstmt.setString(1, sLookupField);			
			rs = pstmt.executeQuery();
			if (rs.next()) {				
				do {
					oKeyValueVo = new KeyValueVo();
					oKeyValueVo.setKey(rs.getString("QEM_KEY"));
					oKeyValueVo.setValue(rs.getString("QEM_VALUE"));
					arlOptions.add(oKeyValueVo);
				} while (rs.next());
			}
		} finally {
			/* Releasing ResultSet & PreparedStatement objects */
			oDBUtility.releaseResources(rs, pstmt);
		}

		LoggerUtility.log("INFO", sClassName, sMethodName, "************************END******************");
		return arlOptions;
	}
	
	public String getOptionsbyAttributeKey(Connection conn, String sLookupField, String sLookupKey) throws Exception {
		String sMethodName = "getOptionsbyAttributeList";
		String sClassName = this.getClass().getName();

		LoggerUtility.log("INFO", sClassName, sMethodName, "************************START******************");
		
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null;
		/* ResultSet object to store the rows retrieved from database */
		ResultSet rs = null;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();
		ArrayList<KeyValueVo> arlOptions = new ArrayList<KeyValueVo>();		
		String sValue = "";
		
		try {
			pstmt = conn.prepareStatement(FETCH_ENQUIRY_LIST_BY_ATTRIBUTE_KEY);
			pstmt.setString(1, sLookupField);
			pstmt.setString(2, sLookupKey);
			rs = pstmt.executeQuery();
			
			if (rs.next()) {
				sValue = rs.getString("QEM_VALUE");
			}
		} finally {
			/* Releasing ResultSet & PreparedStatement objects */
			oDBUtility.releaseResources(rs, pstmt);
		}
		
		LoggerUtility.log("INFO", sClassName, sMethodName, "************************END******************");
		return sValue;
	}
	
	public RatingDto loadOfferPageLookUpValues(Connection conn, RatingDto oRatingDto) throws Exception {
	 	 oRatingDto.setAlAreaClassificationList(getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_AREACL));
	 	 oRatingDto.setAlServiceFactorList(getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_SERFA));
	 	 oRatingDto.setAlStrtgCurrentList(getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_STRTGCURRENT));
		 oRatingDto.setAlClgSystemList(getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_CLGSYSTEM));
		 oRatingDto.setAlBearingDENDEList(getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_BEARINGDENDE));
		 oRatingDto.setAlLubricationList(getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LUB));
		 oRatingDto.setAlBalancingList(getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_BAL));
		 oRatingDto.setAlVibrationList(getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_VIBR));	 
		 oRatingDto.setAlMainTermBoxPSList(getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_MTBPS));			 
		 oRatingDto.setAlCableEntryList(getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_CABLEENTRY));
		 oRatingDto.setAlAuxTermBoxList(getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_AUXTERMBOX));
		 oRatingDto.setAlStatorConnList(getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_STATORCONN));
		 oRatingDto.setAlBoTerminalsNoList(getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_BOTERMINALSNO));
		 oRatingDto.setAlRotationList(getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_ROTATION));
		 oRatingDto.setAlSpaceHeaterList(getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_SPACEHEATER));		 
		 oRatingDto.setAlWindingRTDList(getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_WINDINGRTD));
		 oRatingDto.setAlBearingRTDList(getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_BEARINGRTD));
		 oRatingDto.setAlMinStartVoltList(getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_MINSTVOLT));
		 oRatingDto.setAlMethodOfStgList(getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_METHODOFSTG));
		 oRatingDto.setAlBearingThermoDtList(getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_BEARME));
		 oRatingDto.setAlYesNoList(getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_YESNO));	
		 oRatingDto.setAlNeutralTBList(getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_NEUTRALTERMBOX));
		 oRatingDto.setAlZoneList(getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_ZONE));
		 oRatingDto.setAlGasGroupList(getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_GASGROUP));
		 
		 return oRatingDto;
	 }
	
	
 
	/* (non-Javadoc)
	 * @see boolean#updatingIntendTechnicalData(java.sql.Connection,in.com.rbc.quotation.enquiry.dto.EnquiryDto oEnquiryDto,in.com.rbc.quotation.enquiry.dto.RatingDto oRatingDto, in.com.rbc.quotation.enquiry.dto.TechnicalOfferDto)
	 */ 
	public boolean updatingIntendTechnicalData(Connection conn,
			EnquiryDto oEnquiryDto, RatingDto oRatingDto,
			TechnicalOfferDto oTechnicalOfferDto) throws Exception {
		// TODO Auto-generated method stub
		/* PreparedStatement object to handle database operations */
	 PreparedStatement pstmt = null ;
	 
	 /* Object of DBUtility class to handle database operations */
	 DBUtility oDBUtility = new DBUtility();
	 
	 String sSQLUpdateQuery = null;
	 int iInserted = QuotationConstants.QUOTATION_LITERAL_ZERO;
	 
	 boolean isUpdated= QuotationConstants.QUOTATION_FALSE;
	 Clob oMyClob=null;
	 
	 try {
	 	if (oTechnicalOfferDto.getRatingId() >QuotationConstants.QUOTATION_LITERAL_ZERO){
	 		sSQLUpdateQuery = buildTechnicalOfferQuery(oTechnicalOfferDto);
	 		 
	 		 if (sSQLUpdateQuery != null && sSQLUpdateQuery.trim().length() > QuotationConstants.QUOTATION_LITERAL_ZERO){
	 			 pstmt = conn.prepareStatement(sSQLUpdateQuery);
	 			 pstmt.setString(1, oTechnicalOfferDto.getFrameSize());
	 			 pstmt.setString(2, oTechnicalOfferDto.getEfficiency());
	 			 pstmt.setString(3, oTechnicalOfferDto.getPf());
	 			 pstmt.setString(4, oTechnicalOfferDto.getFlcAmps());
	 			 pstmt.setString(5, oTechnicalOfferDto.getPotFLT());
	 			 pstmt.setString(6, oTechnicalOfferDto.getFlcSQCAGE());
	 			 pstmt.setString(7, oTechnicalOfferDto.getNoiseLevel());
	 			 pstmt.setString(8, oTechnicalOfferDto.getFltSQCAGE());
	 			 pstmt.setString(9, oTechnicalOfferDto.getRpm());
	 			 pstmt.setString(10, oTechnicalOfferDto.getRtGD2());
	 			 pstmt.setString(11, oTechnicalOfferDto.getSstSQCAGE());
	 			 pstmt.setString(12, oTechnicalOfferDto.getRv());
	 			 pstmt.setString(13, oTechnicalOfferDto.getRa());
	 			 pstmt.setString(14, oTechnicalOfferDto.getVibrationLevel());
	 			 pstmt.setString(15, oTechnicalOfferDto.getRemarks());
	 			 pstmt.setString(16, oTechnicalOfferDto.getCommercialOfferType());
	 			 pstmt.setLong(17, oTechnicalOfferDto.getCommercialMCUnit());
	 			 pstmt.setString(18, oTechnicalOfferDto.getCreatedBy());
	 			 pstmt.setString(19, oTechnicalOfferDto.getAreaClassification());
	 			 pstmt.setString(20, oTechnicalOfferDto.getServiceFactor());	    			
	 			 pstmt.setString(21, oTechnicalOfferDto.getBkw());	    			
	 			 pstmt.setString(22, oTechnicalOfferDto.getNlCurrent());
	 			 pstmt.setString(23, oTechnicalOfferDto.getStrtgCurrent());
	 			 pstmt.setString(24, oTechnicalOfferDto.getLrTorque());
	 			 pstmt.setString(25, oTechnicalOfferDto.getPllCurr1());
	 			 pstmt.setString(26, oTechnicalOfferDto.getPllCurr2());
	 			 pstmt.setString(27, oTechnicalOfferDto.getPllCurr3());
	 			 pstmt.setString(28, oTechnicalOfferDto.getPllCurr4());
	 			 pstmt.setString(29, oTechnicalOfferDto.getPllEff1());
	 			 pstmt.setString(30, oTechnicalOfferDto.getPllEff2());	    			
	 			 pstmt.setString(31, oTechnicalOfferDto.getPllEff3());	    			
	 			 pstmt.setString(32, oTechnicalOfferDto.getPllPf1());
	 			 pstmt.setString(33, oTechnicalOfferDto.getPllPf2());
	 			 pstmt.setString(34, oTechnicalOfferDto.getPllpf3());
	 			 pstmt.setString(35, oTechnicalOfferDto.getPllpf4());
	 			 pstmt.setString(36, oTechnicalOfferDto.getSstHot());
	 			 pstmt.setString(37, oTechnicalOfferDto.getSstCold());
	 			 pstmt.setString(38, oTechnicalOfferDto.getStartTimeRV());
	 			 pstmt.setString(39, oTechnicalOfferDto.getStartTimeRV80());
	 			 pstmt.setString(40, oTechnicalOfferDto.getCoolingSystem());	    			
	 			 pstmt.setString(41, oTechnicalOfferDto.getBearingDENDE());	    			 
	 			 pstmt.setString(42, oTechnicalOfferDto.getLubrication());
	 			 pstmt.setString(43, oTechnicalOfferDto.getMigD2Load());
	 			 pstmt.setString(44, oTechnicalOfferDto.getBalancing());
	 			 pstmt.setString(45, oTechnicalOfferDto.getMigD2Motor());
	 			 pstmt.setString(46, oTechnicalOfferDto.getMainTermBoxPS());
	 			 pstmt.setString(47, oTechnicalOfferDto.getNeutralTermBox());
	 			 pstmt.setString(48, oTechnicalOfferDto.getCableEntry());
	 			 pstmt.setString(49, oTechnicalOfferDto.getAuxTermBox());
	 			 pstmt.setString(50, oTechnicalOfferDto.getStatorConn());	    			
	 			 pstmt.setString(51, oTechnicalOfferDto.getNoBoTerminals());	    			 
	 			 pstmt.setString(52, oTechnicalOfferDto.getRotation());
	 			 pstmt.setString(53, oTechnicalOfferDto.getMotorTotalWgt());
	 			 pstmt.setString(54, oTechnicalOfferDto.getSpaceHeater());
	 			 pstmt.setString(55, oTechnicalOfferDto.getWindingRTD());
	 			 pstmt.setString(56, oTechnicalOfferDto.getBearingRTD());
	 			 pstmt.setString(57, oTechnicalOfferDto.getMinStartVolt());
	 			 pstmt.setString(58, oTechnicalOfferDto.getBearingThermoDT());
	 			 pstmt.setString(59, oTechnicalOfferDto.getAirThermo());
	 			 pstmt.setString(60, oTechnicalOfferDto.getVibeProbMPads());	    			
	 			 pstmt.setString(61, oTechnicalOfferDto.getEncoder());	    			 
	 			 pstmt.setString(62, oTechnicalOfferDto.getSpeedSwitch());
	 			 pstmt.setString(63, oTechnicalOfferDto.getSurgeSuppressors());
	 			 pstmt.setString(64, oTechnicalOfferDto.getCurrTransformers());
	 			 pstmt.setString(65, oTechnicalOfferDto.getVibProbes());
	 			 pstmt.setString(66, oTechnicalOfferDto.getSpmNipple());
	 			 pstmt.setString(67, oTechnicalOfferDto.getKeyPhasor());
	 			 pstmt.setString(68, oTechnicalOfferDto.getPaintColor());
	 			 pstmt.setString(69, oTechnicalOfferDto.getZone());
	 			 pstmt.setString(70, oTechnicalOfferDto.getGasGroup());
	 			 oMyClob=conn.createClob();
	 			 oMyClob.setString(1, oTechnicalOfferDto.getDeviation_clarification());
	 			 pstmt.setClob(71, oMyClob);
	 			 
	 			 
	 			 
	 			 iInserted = pstmt.executeUpdate();   
	 			 if (iInserted > QuotationConstants.QUOTATION_LITERAL_ZERO)
	 				 isUpdated = QuotationConstants.QUOTATION_TRUE;
	 		 }
	 	}
	 	oDBUtility.releaseResources(pstmt);
	 
	 	if (oRatingDto.getRatingId() > QuotationConstants.QUOTATION_LITERAL_ZERO){
	 		sSQLUpdateQuery = buildIndentRatingUpdateQuery(oRatingDto, oRatingDto.getRatingId());
	 		if (sSQLUpdateQuery != null && sSQLUpdateQuery.trim().length() > QuotationConstants.QUOTATION_LITERAL_ZERO){
	 			pstmt = conn.prepareStatement(sSQLUpdateQuery);
	 			 iInserted = pstmt.executeUpdate();
	 			 if(oEnquiryDto.getIndentOperation()!=null && 
	 			   oEnquiryDto.getIndentOperation().equalsIgnoreCase(QuotationConstants.QUOTATION_SMINDENT))
	 			 {
	 				if(iInserted>QuotationConstants.QUOTATION_LITERAL_ZERO)
	 				{
	 					isUpdated=QuotationConstants.QUOTATION_TRUE;
	 				}
	 			 }
	 			 else
	 			 {
	 			 if (iInserted > QuotationConstants.QUOTATION_LITERAL_ZERO && isUpdated != QuotationConstants.QUOTATION_FALSE)
	 				 isUpdated = QuotationConstants.QUOTATION_TRUE;
	 			 }
	 		 }
	 	 }
	 	oDBUtility.releaseResources(pstmt);
	 	
	 	if (oEnquiryDto.getEnquiryId() >QuotationConstants.QUOTATION_LITERAL_ZERO){
	 			 pstmt = conn.prepareStatement(UPDATE_ENQUIRY_INDENTDATA);
	 			 pstmt.setString(1,oEnquiryDto.getSavingIndent());
	 			 pstmt.setString(2, oEnquiryDto.getSpecialTest());
	 			 pstmt.setString(3, oEnquiryDto.getSpecialTestDetails());
	 			 pstmt.setString(4,oEnquiryDto.getSapCustomerCode());
	 			 pstmt.setString(5, oEnquiryDto.getThirdPartyInspection());
	 			 pstmt.setString(6, oEnquiryDto.getOrderNumber());
	 			 pstmt.setString(7, oEnquiryDto.getStageInspection());
	 			 pstmt.setString(8, oEnquiryDto.getEnquiryReferenceNumber());
	 			 pstmt.setString(9, oEnquiryDto.getEndUser());
	 			 pstmt.setString(10, oEnquiryDto.getPoiCopy());
	 			 pstmt.setString(11, oEnquiryDto.getEnduserLocation());
	 			 pstmt.setString(12, oEnquiryDto.getLoiCopy());
	 			 pstmt.setString(13, oEnquiryDto.getEndUserIndustry());
	 			 pstmt.setString(14, oEnquiryDto.getDeliveryType());
	 			 pstmt.setString(15, oEnquiryDto.getConsultantName());
	 			 pstmt.setString(16, oEnquiryDto.getDeliveryReqByCust());
	 			 pstmt.setString(17, oEnquiryDto.getEpcName());
	 			 pstmt.setString(18, oEnquiryDto.getExpectedDeliveryMonth());
	 			 pstmt.setString(19, oEnquiryDto.getProjectName());
	 			 pstmt.setString(20, oEnquiryDto.getAdvancePayment());
	 			 pstmt.setString(21, oEnquiryDto.getPacking());
	 			 pstmt.setString(22, oEnquiryDto.getPaymentTerms());
	 			 pstmt.setString(23, oEnquiryDto.getInsurance());
	 			 pstmt.setString(24, oEnquiryDto.getLd());
	 			 pstmt.setString(25, oEnquiryDto.getReplacement());
	 			 pstmt.setString(26, oEnquiryDto.getWarrantyDispatchDate());
	 			 pstmt.setString(27, oEnquiryDto.getOldSerialNo());
	 			 pstmt.setString(28, oEnquiryDto.getWarrantyCommissioningDate());
	 			 pstmt.setString(29, oEnquiryDto.getCustomerPartNo());
	 			 pstmt.setString(30, oEnquiryDto.getDrawingApproval());
	 			 pstmt.setString(31, oEnquiryDto.getWinlossReason());
	 			 pstmt.setString(32, oEnquiryDto.getQapApproval());
	 			 pstmt.setString(33, oEnquiryDto.getWinlossComment());
	 			 pstmt.setString(34, oEnquiryDto.getTypeTest());
	 			 pstmt.setString(35, oEnquiryDto.getWinningCompetitor());
	 			 pstmt.setString(36, oEnquiryDto.getRoutineTest()); 			 
	 			 pstmt.setString(37, oEnquiryDto.getSparesCost());
	 			 pstmt.setString(38, (oEnquiryDto.getIndentOperationCheck()!=null && oEnquiryDto.getIndentOperationCheck().trim().length()>0)?oEnquiryDto.getIndentOperationCheck():QuotationConstants.QUOTATION_NA);
	 			 pstmt.setString(39, oEnquiryDto.getIndentnote()); 			
	 			 pstmt.setInt(40, oEnquiryDto.getEnquiryId());
	 			 
	 			 
	 			 iInserted = pstmt.executeUpdate();
	
	 			 if (iInserted > QuotationConstants.QUOTATION_LITERAL_ZERO && isUpdated != QuotationConstants.QUOTATION_FALSE)
	 				 isUpdated = QuotationConstants.QUOTATION_TRUE;
	 		 
	 	 }
	     
	 }finally {
	     /* Releasing ResultSet & PreparedStatement objects */
	     oDBUtility.releaseResources(pstmt);
	 }
	 
		return isUpdated;			
	
	}

	/**
	 * Update Delivery details of a rating
	 * @param conn Connection object to connect to database
	 * @param oRatingDto RatingDto object having rating details
	 * @return Returns the boolean
	 * @throws Exception If any error occurs during the process
	 * @author 900010540
	 * Created on: Feb 19, 2019
	 */
	@Override
	public boolean updateDeliveryDetails(Connection conn, RatingDto oRatingDto)
			throws Exception {
		// TODO Auto-generated method stub
		/* PreparedStatement object to handle database operations */
	    PreparedStatement pstmt = null ;
	    
	    /* Object of DBUtility class to handle database operations */
	    DBUtility oDBUtility = new DBUtility();
	    
	    DeliveryDetailsDto oDeliveryDetails = null;
	    int iInserted = QuotationConstants.QUOTATION_LITERAL_ZERO;
	    int iTotalDeliveryDet = QuotationConstants.QUOTATION_LITERAL_ZERO;
	    
	    boolean isInserted = QuotationConstants.QUOTATION_FALSE;
	    
	    try {
	    	if (oRatingDto.getRatingId() > QuotationConstants.QUOTATION_LITERAL_ZERO && oRatingDto.getDeliveryDetailsList().size() > QuotationConstants.QUOTATION_LITERAL_ZERO){
	    		
	    		for (int idx=0; idx<oRatingDto.getDeliveryDetailsList().size(); idx++){
	    			oDeliveryDetails = (DeliveryDetailsDto) oRatingDto.getDeliveryDetailsList().get(idx);
	    			if (oDeliveryDetails != null){
	    			
	    				pstmt = conn.prepareStatement(INSERT_DELIVERY_DETAILS);	
	                
	    				pstmt.setInt(1, oDeliveryDetails.getDeliveryRatingId());	        				
	    				pstmt.setString(2, oDeliveryDetails.getDeliveryLocation().trim());
	    				pstmt.setString(3, oDeliveryDetails.getDeliveryAddress());
	    				pstmt.setInt(4, oDeliveryDetails.getNoofMotors());
		    			 
	    				 iInserted = pstmt.executeUpdate();     		 
		    			 if (iInserted > QuotationConstants.QUOTATION_LITERAL_ZERO)
		    				 iTotalDeliveryDet++;
	    			}
	    		}
	    		if (iTotalDeliveryDet > QuotationConstants.QUOTATION_LITERAL_ZERO)
	    			isInserted = QuotationConstants.QUOTATION_TRUE;
	    	 }
	    }finally {
	        /* Releasing ResultSet & PreparedStatement objects */
	        oDBUtility.releaseResources(pstmt);
	    }
		return isInserted;			
	
	}

	/* (non-Javadoc)
	 * @see java.lang.String#buildIndentRatingUpdateQuery(in.com.rbc.quotation.enquiry.dto.RatingDto, int) 
	 */
	private String buildIndentRatingUpdateQuery(RatingDto oRatingDto, int iRatingId){
		 String sMethodName = "buildIndentRatingUpdateQuery";
		 
		 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		 StringBuffer sbFieldQuery = new StringBuffer();
		 
		 if (oRatingDto != null){
			 sbFieldQuery.append(UPDATE_RATING_DETAILS);			 
			 
			 if (oRatingDto.getQuantity()>QuotationConstants.QUOTATION_LITERAL_ZERO ){
					sbFieldQuery.append("QRD_QUANTITY = ");
					sbFieldQuery.append("'"+oRatingDto.getQuantity()+"', ");
				 }
			 
			 if (oRatingDto.getPriceincludeFrieght()!=null && oRatingDto.getPriceincludeFrieght().trim().length()>QuotationConstants.QUOTATION_LITERAL_ZERO ){
				sbFieldQuery.append("QRD_PRICEINFREIGHT = ");
				sbFieldQuery.append("'"+oRatingDto.getPriceincludeFrieght()+"', ");
			 }
			 if (oRatingDto.getPriceincludeFrieghtVal() != null && oRatingDto.getPriceincludeFrieghtVal().trim().length()>QuotationConstants.QUOTATION_LITERAL_ZERO){
				sbFieldQuery.append("QRD_PRICEINFREIGHTC = ");
				sbFieldQuery.append("'"+oRatingDto.getPriceincludeFrieghtVal()+"', ");
			 }
			 if (oRatingDto.getPriceincludeExtraWarn()!=null && oRatingDto.getPriceincludeExtraWarn().trim().length()>QuotationConstants.QUOTATION_LITERAL_ZERO ){
				sbFieldQuery.append("QRD_PRICEINEXTRAWAR = ");
				sbFieldQuery.append("'"+oRatingDto.getPriceincludeExtraWarn()+"', ");
			 }
			 if (oRatingDto.getPriceincludeExtraWarnVal() != null && oRatingDto.getPriceincludeExtraWarnVal().trim().length()>QuotationConstants.QUOTATION_LITERAL_ZERO){
				sbFieldQuery.append("QRD_PRICEINEXTRAWARC = ");
				sbFieldQuery.append("'"+oRatingDto.getPriceincludeExtraWarnVal()+"', ");
			 }
			 if (oRatingDto.getPriceincludeSupervisionCost()!=null && oRatingDto.getPriceincludeSupervisionCost().trim().length()>QuotationConstants.QUOTATION_LITERAL_ZERO){
				sbFieldQuery.append("QRD_PRICEINSUPVISION = ");
				sbFieldQuery.append("'"+oRatingDto.getPriceincludeSupervisionCost()+"', ");
			 }
			 if (oRatingDto.getPriceincludeSupervisionCostVal()!= null && oRatingDto.getPriceincludeSupervisionCostVal().trim().length() >QuotationConstants.QUOTATION_LITERAL_ZERO){
				sbFieldQuery.append("QRD_PRICEINSUPVISIONC = ");
				sbFieldQuery.append("'"+oRatingDto.getPriceincludeSupervisionCostVal()+"', ");
			 }
			 if (oRatingDto.getPriceincludetypeTestCharges()!=null && oRatingDto.getPriceincludetypeTestCharges().trim().length()> QuotationConstants.QUOTATION_LITERAL_ZERO ){
				sbFieldQuery.append("QRD_PRICEINTYPE = ");
				sbFieldQuery.append("'"+oRatingDto.getPriceincludetypeTestCharges()+"', ");
			 }
			 if (oRatingDto.getPriceincludetypeTestChargesVal()!= null && oRatingDto.getPriceincludetypeTestChargesVal().trim().length() > QuotationConstants.QUOTATION_LITERAL_ZERO){
				sbFieldQuery.append("QRD_PRICEINTYPEC = ");
				sbFieldQuery.append("'"+oRatingDto.getPriceincludetypeTestChargesVal()+"', ");
			 }
			 
			 if (oRatingDto.getPriceextraFrieght()!=null && oRatingDto.getPriceextraFrieght().trim().length()>QuotationConstants.QUOTATION_LITERAL_ZERO){
				sbFieldQuery.append("QRD_PRICEEXFREIGHT = ");
				sbFieldQuery.append("'"+oRatingDto.getPriceextraFrieght()+"', ");
			 }
			 if (oRatingDto.getPriceextraFrieghtVal()!= null && oRatingDto.getPriceextraFrieghtVal().trim().length() > QuotationConstants.QUOTATION_LITERAL_ZERO){
				sbFieldQuery.append("QRD_PRICEEXFREIGHTC = ");
				sbFieldQuery.append("'"+oRatingDto.getPriceextraFrieghtVal()+"', ");
			 }
			 if (oRatingDto.getPriceextraExtraWarn()!=null && oRatingDto.getPriceextraExtraWarn().trim().length()> QuotationConstants.QUOTATION_LITERAL_ZERO ){
				sbFieldQuery.append("QRD_PRICEEXTRAWAR = ");
				sbFieldQuery.append("'"+oRatingDto.getPriceextraExtraWarn()+"', ");
			 }
			 if (oRatingDto.getPriceextraExtraWarnVal()!= null && oRatingDto.getPriceextraExtraWarnVal().trim().length() > QuotationConstants.QUOTATION_LITERAL_ZERO){
				sbFieldQuery.append("QRD_PRICEEXTRAWARC = ");
				sbFieldQuery.append("'"+oRatingDto.getPriceextraExtraWarnVal()+"', ");
			 }
			 if (oRatingDto.getPriceextraSupervisionCost()!=null && oRatingDto.getPriceextraSupervisionCost().trim().length()> QuotationConstants.QUOTATION_LITERAL_ZERO ){
				sbFieldQuery.append("QRD_PRICEEXSUPVISION = ");
				sbFieldQuery.append("'"+oRatingDto.getPriceextraSupervisionCost()+"', ");
			 }
			 if (oRatingDto.getPriceextraSupervisionCostVal()!= null && oRatingDto.getPriceextraSupervisionCostVal().trim().length() > QuotationConstants.QUOTATION_LITERAL_ZERO){
				sbFieldQuery.append("QRD_PRICEEXSUPVISIONC = ");
				sbFieldQuery.append("'"+oRatingDto.getPriceextraSupervisionCostVal()+"', ");
			 }
			 if (oRatingDto.getPriceextratypeTestCharges()!=null && oRatingDto.getPriceextratypeTestCharges().trim().length()> QuotationConstants.QUOTATION_LITERAL_ZERO){
				sbFieldQuery.append("QRD_PRICEEXTYPE = ");
				sbFieldQuery.append("'"+oRatingDto.getPriceextratypeTestCharges()+"', ");
			 }
			 if (oRatingDto.getPriceextratypeTestChargesVal()!= null && oRatingDto.getPriceextratypeTestChargesVal().trim().length() >  QuotationConstants.QUOTATION_LITERAL_ZERO){
				sbFieldQuery.append("QRD_PRICEEXTYPEC = ");
				sbFieldQuery.append("'"+oRatingDto.getPriceextratypeTestChargesVal()+"', ");
			 }
			 
			 if (oRatingDto.getUnitPriceMultiplierRsm()!= null && oRatingDto.getUnitPriceMultiplierRsm().trim().length() > QuotationConstants.QUOTATION_LITERAL_ZERO){
				sbFieldQuery.append("QRD_UNIT_PRICE_RSM = ");
				sbFieldQuery.append("'"+oRatingDto.getUnitPriceMultiplierRsm()+"', ");
			 }
	
	
			
			if (sbFieldQuery.toString().trim().endsWith(","))        	
		           sbFieldQuery = new StringBuffer(sbFieldQuery.toString().trim().substring(0, sbFieldQuery.toString().trim().lastIndexOf(",")));       
			
			sbFieldQuery.append(" WHERE QRD_RATINGID = "+oRatingDto.getRatingId());
		 }
		
		
		 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "sbFieldQuery ...... "+sbFieldQuery);
		 LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
		 
		 return sbFieldQuery.toString();
	}

	/**
	 * Fetch Delivery Details Based on Rating
	 * @param conn Connection object to connect to database
	 * @param ratingId int value of ratingId
	 * @return Returns ArrayList
	 * @throws Exception If any error occurs during the process
	 * @author 900010540
	 * Created on: Feb 19, 2019
	 */
	public ArrayList getDeliveryDetailsList (Connection conn, int iRatingId) throws Exception{
		ArrayList alDeliveryDetailslist = new ArrayList();
		
		/* PreparedStatement object to handle database operations */
	    PreparedStatement pstmt = null ;
	    
	    /* ResultSet object to store the rows retrieved from database */
	    ResultSet rs = null ;
	    
	    /* Object of DBUtility class to handle database operations */
	    DBUtility oDBUtility = new DBUtility();
	    
	    DeliveryDetailsDto oDeliveryDetails=null;
	    
	    try {
	    	pstmt = conn.prepareStatement(FETCH_DELIVERY_DETAILS);
	    	pstmt.setInt(1, iRatingId);
	    	rs = pstmt.executeQuery();
	        
	        if (rs != null && rs.next()){ 
	        	do {
	        		oDeliveryDetails = new DeliveryDetailsDto();
	        		oDeliveryDetails.setDeliveryRatingId(rs.getInt("QDD_RATINGID"));
	        		oDeliveryDetails.setDeliveryLocation(rs.getString("QDD_DELIVERYLOCATION"));
	        		oDeliveryDetails.setDeliveryAddress(rs.getString("QDD_DELIVERYADDRESS"));
	        		oDeliveryDetails.setNoofMotors(rs.getInt("QDD_NOOFMOTORS"));
	        		
	        		
	        	
	        		alDeliveryDetailslist.add(oDeliveryDetails);	     
	        	}while (rs.next());
	        }
	        
	    }finally {
	        /* Releasing ResultSet & PreparedStatement objects */
	        oDBUtility.releaseResources(rs, pstmt);
	    }
		return alDeliveryDetailslist;
	}

	/**
	 * Delete all Delivery details for a rating
	 * @param conn Connection object to connect to database
	 * @param ratingId int value of ratingId
	 * @return Returns the boolean
	 * @throws Exception If any error occurs during the process
	 * @author 900010540
	 * Created on: Feb 19, 2019
	 */
	public boolean deleteAllDelDetailsByRatingId(Connection conn, int iRatingId)throws Exception{			
		boolean isDeleted = false;
		/* PreparedStatement object to handle database operations */
	    PreparedStatement pstmt = null ;
	    
	    /* Object of DBUtility class to handle database operations */
	    DBUtility oDBUtility = new DBUtility();
	    
	    try {
	 
	    	pstmt = conn.prepareStatement(DELETE_DELIVERYDETAILS);
	    	pstmt.setInt(1, iRatingId);	            
	    	pstmt.executeUpdate();
	        isDeleted = true;
	        
	    }finally {
	        /* Releasing ResultSet & PreparedStatement objects */
	        oDBUtility.releaseResources(pstmt);
	    }
	    
		return isDeleted;			
	}

	/**
	 * Used to get the direct Commercial Managers list
	 * @param conn Connection object to connect to database
	 * @param sType String value of sType
	 * @param sCurrentUserId String value of current userid
	 * @return Returns the ArrayList object
	 * @throws Exception If any error occurs during the process
	 * @author 900010540
	 * Created on: Feb 4, 2019
	 */
	public ArrayList getCommercialManagersList(Connection conn, String sType,
		String sCurrentUserId) throws Exception {
	// TODO Auto-generated method stub
	ArrayList arlCommercialManagersList = new ArrayList();
	
	/* PreparedStatement object to handle database operations */
	PreparedStatement pstmt = null ;
	
	/* ResultSet object to store the rows retrieved from database */
	ResultSet rs = null ;
	
	/* Object of DBUtility class to handle database operations */
	DBUtility oDBUtility = new DBUtility();
	
	KeyValueVo oKeyValueVo = null;
	
	try{
		pstmt = conn.prepareStatement(FETCH_COMMERCIALMANAGERS_LIST);
		rs = pstmt.executeQuery();
	    
	    if (rs != null && rs.next()){
	    	do {
	    		if (! sCurrentUserId.equalsIgnoreCase(rs.getString("QDE_ENGINEERSSO"))){
	    			oKeyValueVo = new KeyValueVo();
	    		
	    			oKeyValueVo.setKey(rs.getString("QDE_ENGINEERSSO"));
	    			oKeyValueVo.setValue(rs.getString("EMP_NAMEFULL"));
	    			arlCommercialManagersList.add(oKeyValueVo);
	    		}
	    	}while(rs.next());
	    }
	} finally {
	    /* Releasing ResultSet & PreparedStatement objects */
	    oDBUtility.releaseResources(rs, pstmt);
	}
	return arlCommercialManagersList;
	
	}

	/**
	 * Update Intend Technical Data of a rating
	 * @param conn Connection object to connect to database
	 * @param EnquiryDto oEnquiryDto object having enquiry details
	 * @return Returns the boolean
	 * @throws Exception If any error occurs during the process
	 * @author 900010540
	 * Created on: 19 March 2019
	 */
	public boolean updatingLostIntendTechnicalData(Connection conn,
		EnquiryDto oEnquiryDto) throws Exception {
	// TODO Auto-generated method stub
	/* PreparedStatement object to handle database operations */
	PreparedStatement pstmt = null ;
	
	/* Object of DBUtility class to handle database operations */
	DBUtility oDBUtility = new DBUtility();
	
	
	int iInserted =  QuotationConstants.QUOTATION_LITERAL_ZERO;
	boolean isUpdated=QuotationConstants.QUOTATION_FALSE;
	
	
	try {
		
		if (oEnquiryDto.getEnquiryId() > QuotationConstants.QUOTATION_LITERAL_ZERO){
			     if(oEnquiryDto.getLossChecking()!=null 
			    		 && 
			      oEnquiryDto.getLossChecking().equalsIgnoreCase(QuotationConstants.QUOTATION_LOST))
			     {
				 pstmt = conn.prepareStatement(UPDATE_ENQUIRY_LOSTINDENTDATA);
				 pstmt.setString(1,oEnquiryDto.getLossPrice());
				 pstmt.setString(2, oEnquiryDto.getLossReason1());
				 pstmt.setString(3, oEnquiryDto.getLossReason2());
				 pstmt.setString(4,oEnquiryDto.getLossCompetitor());
				 pstmt.setString(5, oEnquiryDto.getLossComment());
				 pstmt.setInt(6, oEnquiryDto.getEnquiryId());		 
				 iInserted = pstmt.executeUpdate();
			     }
			     else if(oEnquiryDto.getLossChecking()!=null 
			    		 && 
			      oEnquiryDto.getLossChecking().equalsIgnoreCase(QuotationConstants.QUOTATION_ABONDED))
			     {
					 pstmt = conn.prepareStatement(UPDATE_ENQUIRY_ABONDENTINDENTDATA);
					 pstmt.setString(1,oEnquiryDto.getAbondentComment());
					 pstmt.setInt(2, oEnquiryDto.getEnquiryId());		 
					 iInserted = pstmt.executeUpdate();
	 
			     }
	
	
				 if (iInserted > QuotationConstants.QUOTATION_LITERAL_ZERO )
					 isUpdated = QuotationConstants.QUOTATION_TRUE;
			 
		 }
	 
	}finally {
	 /* Releasing ResultSet & PreparedStatement objects */
	 oDBUtility.releaseResources(pstmt);
	}
	
	return isUpdated;			
	
	}

	/**
	 * This method is used to update the request information in released to Customer page
	 * @param conn Connection object to connect to database
	 * @param EnquiryDto oEnquiryDto object having enquiry details
	 * @return Returns the boolean true if update success
	 * @throws Exception If any error occurs during the process
	 * @author 610092227
	 * Created on: 03 July 2019
	 */
	public boolean updateEnquiryBasicInfo(Connection conn,
		EnquiryDto oEnquiryDto) throws Exception {
		// TODO Auto-generated method stub
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null ;
		
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();
		
		
		int iInserted =  QuotationConstants.QUOTATION_LITERAL_ZERO;
		boolean isUpdated=QuotationConstants.QUOTATION_FALSE;
	
	
	try {
		
			if (oEnquiryDto.getEnquiryId() > QuotationConstants.QUOTATION_LITERAL_ZERO){
				     
					 pstmt = conn.prepareStatement(UPDATE_ENQUIRY_BASICINFO);
					 pstmt.setString(1,oEnquiryDto.getOrderClosingMonth());
					 pstmt.setString(2, oEnquiryDto.getTargeted());
					 pstmt.setString(3, oEnquiryDto.getWinningChance());			
					 pstmt.setInt(4, oEnquiryDto.getEnquiryId());		 
					 iInserted = pstmt.executeUpdate();
		     }
		     
		   
		
			 if (iInserted > QuotationConstants.QUOTATION_LITERAL_ZERO )
				 isUpdated = QuotationConstants.QUOTATION_TRUE;
		 
			 
		 
		}finally {
		 /* Releasing ResultSet & PreparedStatement objects */
		 oDBUtility.releaseResources(pstmt);
		}
	
		return isUpdated;			
	
	}

	/**
	 * This method is used to save the calculated total motor, package price and also include & extra for enquiry, indent page
	 * @param conn Connection object to connect to database
	 * @param EnquiryDto oEnquiryDto object having enquiry details
	 * @return Returns the boolean true if update success
	 * @throws Exception If any error occurs during the process
	 * @author 610092227
	 * Created on: 05 Sept 2019
	 */
	public boolean saveEnquiryMotorPkgPrice(Connection conn, EnquiryDto oEnquiryDto) throws Exception {
		// TODO Auto-generated method stub
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null;

		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();

		int iInserted = QuotationConstants.QUOTATION_LITERAL_ZERO;
		boolean isUpdated = QuotationConstants.QUOTATION_FALSE;

		try {

			if (oEnquiryDto.getEnquiryId() > QuotationConstants.QUOTATION_LITERAL_ZERO) {

				pstmt = conn.prepareStatement(UPDATE_ENQUIRY_MOTORPKGPRICE);
				pstmt.setString(1, oEnquiryDto.getTotalMotorPrice());
				pstmt.setString(2, oEnquiryDto.getTotalPkgPrice());
				pstmt.setString(3, oEnquiryDto.getIncludeFrieght());
				pstmt.setString(4, oEnquiryDto.getIncludeExtraWarrnt());
				pstmt.setString(5, oEnquiryDto.getIncludeSupervisionCost());
				pstmt.setString(6, oEnquiryDto.getIncludetypeTestCharges());
				pstmt.setString(7, oEnquiryDto.getExtraFrieght());
				pstmt.setString(8, oEnquiryDto.getExtraExtraWarrnt());
				pstmt.setString(9, oEnquiryDto.getExtraSupervisionCost());
				pstmt.setString(10, oEnquiryDto.getExtratypeTestCharges());
				pstmt.setInt(11, oEnquiryDto.getEnquiryId());
				iInserted = pstmt.executeUpdate();
			}

			if (iInserted > QuotationConstants.QUOTATION_LITERAL_ZERO)
				isUpdated = QuotationConstants.QUOTATION_TRUE;

		} finally {
			/* Releasing ResultSet & PreparedStatement objects */
			oDBUtility.releaseResources(pstmt);
		}

		return isUpdated;

	}

	/**
	* This method is used to save the calculated total motor, package price for Rating, indent page
	* @param conn Connection object to connect to database
	* @param RatingDto oRatingDto object having Rating details
	* @return Returns the boolean true if update success
	* @throws Exception If any error occurs during the process
	* @author 610092227
	* Created on: 05 Sept 2019
	*/
	public boolean saveRatingMotorPkgPrice(Connection conn, RatingDto oRatingDto) throws Exception {
		// TODO Auto-generated method stub
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null;

		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();

		int iInserted = QuotationConstants.QUOTATION_LITERAL_ZERO;
		boolean isUpdated = QuotationConstants.QUOTATION_FALSE;

		try {

			if (oRatingDto.getRatingId() > QuotationConstants.QUOTATION_LITERAL_ZERO) {

				pstmt = conn.prepareStatement(UPDATE_RATING_MOTORPKGPRICE);
				pstmt.setString(1, oRatingDto.getTotalMotorPrice());
				pstmt.setString(2, oRatingDto.getTotalPackagePrice());
				pstmt.setInt(3, oRatingDto.getRatingId());
				pstmt.setInt(4, oRatingDto.getEnquiryId());

				iInserted = pstmt.executeUpdate();
			}

			if (iInserted > QuotationConstants.QUOTATION_LITERAL_ZERO)
				isUpdated = QuotationConstants.QUOTATION_TRUE;

		} finally {
			/* Releasing ResultSet & PreparedStatement objects */
			oDBUtility.releaseResources(pstmt);
		}

		return isUpdated;
	}
	
	/**
	* This method is used to save the calculated quoted price of Rating, for search results page
	* @param conn Connection object to connect to database
	* @param RatingDto oRatingDto object having Rating details
	* @return Returns the boolean true if update success
	* @throws Exception If any error occurs during the process
	* @author 610092227
	* Created on: 13 Sept 2019
	*/
	public boolean saveRatingQuotedPrice(Connection conn, RatingDto oRatingDto) throws Exception {
		// TODO Auto-generated method stub
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null;

		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();

		int iInserted = QuotationConstants.QUOTATION_LITERAL_ZERO;
		boolean isUpdated = QuotationConstants.QUOTATION_FALSE;

		try {

			if (oRatingDto.getRatingId() > QuotationConstants.QUOTATION_LITERAL_ZERO) {

				pstmt = conn.prepareStatement(UPDATE_RATING_QUOTEDPRICE);
				pstmt.setString(1, oRatingDto.getQuotedPrice());
				pstmt.setInt(2, oRatingDto.getRatingId());
				pstmt.setInt(3, oRatingDto.getEnquiryId());

				iInserted = pstmt.executeUpdate();
			}

			if (iInserted > QuotationConstants.QUOTATION_LITERAL_ZERO)
				isUpdated = QuotationConstants.QUOTATION_TRUE;

		} finally {
			/* Releasing ResultSet & PreparedStatement objects */
			oDBUtility.releaseResources(pstmt);
		}

		return isUpdated;

	}

	/**
	* This method is used to save the calculated quoted price of Enquiry, for search results page
	* @param conn Connection object to connect to database
	* @param RatingDto oRatingDto object having Rating details
	* @return Returns the boolean true if update success
	* @throws Exception If any error occurs during the process
	* @author 610092227
	* Created on: 13 Sept 2019
	*/
	public boolean saveEnquiryQuotedPrice(Connection conn, EnquiryDto oEnquiryDto) throws Exception {
		// TODO Auto-generated method stub
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null;

		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();

		int iInserted = QuotationConstants.QUOTATION_LITERAL_ZERO;
		boolean isUpdated = QuotationConstants.QUOTATION_FALSE;

		try {

			if (oEnquiryDto.getEnquiryId() > QuotationConstants.QUOTATION_LITERAL_ZERO) {

				pstmt = conn.prepareStatement(UPDATE_ENQUIRY_QUOTEDPRICE);
				pstmt.setString(1, oEnquiryDto.getEnqQuotedPrice());
				pstmt.setInt(2, oEnquiryDto.getEnquiryId());

				iInserted = pstmt.executeUpdate();
			}

			if (iInserted > QuotationConstants.QUOTATION_LITERAL_ZERO)
				isUpdated = QuotationConstants.QUOTATION_TRUE;

		} finally {
			/* Releasing ResultSet & PreparedStatement objects */
			oDBUtility.releaseResources(pstmt);
		}

		return isUpdated;

	}

	public NewEnquiryDto loadNewEnquiryLookUpValues(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception {
		//oNewEnquiryDto.setCustomerList(getOptionsList(conn,QuotationConstants.QUOTATION_RFQ_CUSTOMER, QuotationConstants.QUOTATION_QCU_CUSID,QuotationConstants.QUOTATION_QCU_NAME));
		//oNewEnquiryDto.setAlInstallLocationList(getOptionsList(conn,QuotationConstants.QUOTATION_RFQ_LOCATION, QuotationConstants.QUOTATION_QLO_LOCATIONID,QuotationConstants.QUOTATION_QLO_LOCATIONNAME));
		//oNewEnquiryDto.setAlApprovalList(getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_APPROVAL));
		
		oNewEnquiryDto.setAlIndustryList(getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_INDUSTRY));
		oNewEnquiryDto.setAlTargetedList(getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_TARGTD));
		oNewEnquiryDto.setAlEnquiryTypeList(getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_ETYPE));
		oNewEnquiryDto.setAlWinChanceList(getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_WINCH));
		
		return oNewEnquiryDto;
	}
 
	public NewEnquiryDto getNewEnquiryDetailsForView(Connection conn, NewEnquiryDto oNewEnquiryDto, String sOperationType) throws Exception {
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        
        KeyValueVo oKeyValueVo =null;
        NewRatingDto oNewRatingDto = null;
		String sSalesManagerId = "";
		String sTotalpkgPrice = "";
		String sQuotedPrice = "";
		ArrayList alRatingObjects = null;
        ArrayList alTempList = new ArrayList();
        ArrayList alCurrentRating =null;
        DecimalFormat oDf = new DecimalFormat("###,###");
        double quotedPrice = 0d;
        
        try {
        	pstmt = conn.prepareStatement(FETCH_LT_ENQUIRYDETAILS_FROMVIEW);
        	pstmt.setInt(1, oNewEnquiryDto.getEnquiryId());
        	rs = pstmt.executeQuery();
        	
        	if (rs != null && rs.next()) {
        		oNewEnquiryDto.setEnquiryNumber(rs.getString("QEM_ENQUIRYNO"));   
        		oNewEnquiryDto.setRevisionNumber(rs.getInt("QEM_REVISIONNO"));
        		oNewEnquiryDto.setCustomerName(rs.getString("QEM_CUSTOMERNAME"));
        		oNewEnquiryDto.setCustomerType(rs.getString("QEM_CUSTOMERTYPE"));
        		oNewEnquiryDto.setCustomerId(rs.getInt("QEM_CUSTOMERID"));
        		oNewEnquiryDto.setLocationId(rs.getInt("QEM_LOCATIONID"));
        		oNewEnquiryDto.setLocation(rs.getString("QEM_LOCATIONNAME"));
        		oNewEnquiryDto.setCustomerEnqReference(rs.getString("QEM_ENQUIRYREFERENCENO"));
        		oNewEnquiryDto.setCustomerIndustry(rs.getString("QEM_INDUSTRY"));
        		oNewEnquiryDto.setConcernedPerson(rs.getString("QEM_CONCERNED_PERSON"));
        		oNewEnquiryDto.setConcernedPersonHdn(rs.getString("QEM_CONCERNED_PERSON"));
        		oNewEnquiryDto.setProjectName(rs.getString("QEM_PROJECTNAME"));
        		oNewEnquiryDto.setConsultantName(rs.getString("QEM_CONSULTANTNAME"));
        		oNewEnquiryDto.setEndUser(rs.getString("QEM_ENDUSER"));
        		oNewEnquiryDto.setEnquiryType(rs.getString("QEM_ENQUIRYTYPE"));
        		oNewEnquiryDto.setWinningChance(rs.getString("QEM_WINCHANCE"));
        		oNewEnquiryDto.setTargeted(rs.getString("QEM_TARGETED"));
        		oNewEnquiryDto.setRemarks(rs.getString("QEM_REMARKS"));
        		
        		if(StringUtils.isNotBlank(rs.getString("QEM_ENQRECEIPTMNTH"))) {
        			oNewEnquiryDto.setReceiptDate(CommonUtility.buildDateString(rs.getString("QEM_ENQRECEIPTMNTH")));
        		}else {
        			oNewEnquiryDto.setReceiptDate("");
        		}
        		if(StringUtils.isNotBlank(rs.getString("QEM_REQOFFERMNTH"))) {
        			oNewEnquiryDto.setSubmissionDate(CommonUtility.buildDateString(rs.getString("QEM_REQOFFERMNTH")));
        		}else {
        			oNewEnquiryDto.setSubmissionDate("");
        		}
        		if(StringUtils.isNotBlank(rs.getString("QEM_ORDCLOSEMNTH"))) {
        			oNewEnquiryDto.setClosingDate(CommonUtility.buildDateString(rs.getString("QEM_ORDCLOSEMNTH")));
        		}else {
        			oNewEnquiryDto.setClosingDate("");
        		}
        		
        		if(StringUtils.isBlank(rs.getString("QEM_IS_NEWENQUIRY"))) {
        			oNewEnquiryDto.setIsNewEnquiry("N");
        		} else {
        			oNewEnquiryDto.setIsNewEnquiry(rs.getString("QEM_IS_NEWENQUIRY").trim());
        		}
        		oNewEnquiryDto.setStatusId(rs.getInt("QEM_STATUSID"));
        		oNewEnquiryDto.setStatusName(rs.getString("QEM_STATUSDESC"));
        		oNewEnquiryDto.setCreatedBy(rs.getString("QEM_CREATEDBYNAME"));
        		oNewEnquiryDto.setCreatedDate(rs.getString("QEM_CREATEDDATE"));
        		oNewEnquiryDto.setCreatedBySSO(rs.getString("QEM_CREATEDBY"));
        		oNewEnquiryDto.setSmNote(rs.getString("QEM_SMNOTE"));
        		oNewEnquiryDto.setSm_name(rs.getString("QEM_SMNAME"));
        		oNewEnquiryDto.setSupervisionDetails(rs.getString("QEM_SUPERVISION_DETAILS"));
        		oNewEnquiryDto.setSuperviseNoOfManDays(rs.getString("QEM_SUPERVISE_MANDAYS"));
        		oNewEnquiryDto.setSuperviseToFroTravel(rs.getString("QEM_SUPERVISE_TRAVELTOFRO"));
        		oNewEnquiryDto.setSuperviseLodging(rs.getString("QEM_SUPERVISE_LODGING"));
        		oNewEnquiryDto.setRsmNote(rs.getString("QEM_RSMNOTE"));
        		oNewEnquiryDto.setNsmNote(rs.getString("QEM_NSMNOTE"));
        		oNewEnquiryDto.setMhNote(rs.getString("QEM_MHNOTE"));
        		oNewEnquiryDto.setDesignNote(rs.getString("QEM_DMNOTE"));
        		oNewEnquiryDto.setDmUpdateStatus(rs.getString("QEM_DMUPDATESTATUS"));
        		oNewEnquiryDto.setIsReferDesign(rs.getString("QEM_IS_REFER_DESIGN"));
        		oNewEnquiryDto.setCommentsDeviations(rs.getString("QEM_DEVIATIONCOMMENTS"));
        		oNewEnquiryDto.setReassignRemarks(rs.getString("QEM_REASSIGN_COMMENTS"));
        		oNewEnquiryDto.setDesignReferenceNumber(rs.getString("QEM_ENQUIRYREFERENCENO"));
        		oNewEnquiryDto.setQaNote(rs.getString("QEM_QANOTE"));
        		oNewEnquiryDto.setScmNote(rs.getString("QEM_SCMNOTE"));
        		oNewEnquiryDto.setMfgNote(rs.getString("QEM_MFGNOTE"));
        		oNewEnquiryDto.setBhNote(rs.getString("QEM_BHNOTE"));
        		oNewEnquiryDto.setCustomerDealerLink(rs.getString("QEM_CUSTOMERDEALERLINK"));
        		
        		// Capture Abondent / Lost / Won Details.
        		if(QuotationConstants.QUOTATION_WORKFLOW_STATUS_LOST == oNewEnquiryDto.getStatusId()) {
        			oNewEnquiryDto.setWinlossComment(rs.getString("QEM_LOST_COMMENT"));
        		} else if(QuotationConstants.QUOTATION_WORKFLOW_STATUS_ABONDENT == oNewEnquiryDto.getStatusId()) {
        			oNewEnquiryDto.setWinlossComment(rs.getString("QEM_ABONDENT_COMMENT"));
        		} else if(QuotationConstants.QUOTATION_WORKFLOW_STATUS_WON == oNewEnquiryDto.getStatusId()) {
        			oNewEnquiryDto.setWinlossComment(rs.getString("QEM_WON_REMARKS"));
        		}
        		
        		oNewEnquiryDto.setAbondentCustomerApproval(rs.getString("QEM_ABONDENT_CUST_APPRV"));
        		oNewEnquiryDto.setLostManagerApproval(rs.getString("QEM_LOST_CUST_APPRV"));
        		oNewEnquiryDto.setWinningCompetitor("");
        		oNewEnquiryDto.setLostCompetitor(rs.getString("QEM_LOSS_COMPETITOR_DESC"));
        		oNewEnquiryDto.setTotalQuotedPrice(rs.getString("QEM_QUOTEDPRICE"));
        		
        		// ::::::::::::::::::::::::::::::::::::::::   Setting WON Order Details.  :::::::::::::::::::::::::::::::::::::::: 
        		
        		oNewEnquiryDto.setSavingIndent(rs.getString("QEM_SAVINGINDENT"));
        		oNewEnquiryDto.setOrderNumber(rs.getString("QEM_PONUMBER"));
        		// poiCopy
        		// loiCopy
        		oNewEnquiryDto.setWonPODate(rs.getString("QEM_PODATE"));
        		oNewEnquiryDto.setWonPOReceiptDate(rs.getString("QEM_PORECEIPTDATE"));
        		oNewEnquiryDto.setWonReason1(rs.getString("QEM_WON_REASON1"));
        		oNewEnquiryDto.setWonReason2(rs.getString("QEM_WON_REASON2"));
        		// Quoted Price
        		// Won Price
        		if(!QuotationConstants.QUOTATION_ZERO.equals(rs.getString("QEM_WON_REQDRAWING"))) {
        			oNewEnquiryDto.setWonRequiresDrawing(rs.getString("QEM_WON_REQDRAWING"));
        		}
        		if(!QuotationConstants.QUOTATION_ZERO.equals(rs.getString("QEM_WON_REQDATASHEET"))) {
        			oNewEnquiryDto.setWonRequiresDatasheet(rs.getString("QEM_WON_REQDATASHEET"));	
        		}
				if(!QuotationConstants.QUOTATION_ZERO.equals(rs.getString("QEM_WON_REQQAP"))) {
					oNewEnquiryDto.setWonRequiresQAP(rs.getString("QEM_WON_REQQAP"));
				}
				if(!QuotationConstants.QUOTATION_ZERO.equals(rs.getString("QEM_WON_REQPERFCURVES"))) {
					oNewEnquiryDto.setWonRequiresPerfCurves(rs.getString("QEM_WON_REQPERFCURVES"));
				}
		        
        		// ::::::::::::::::::::::::::::::::::::::::   Setting WON Order Details.  :::::::::::::::::::::::::::::::::::::::: 
        		
        		quotedPrice = rs.getDouble("QEM_LOSS_PRICE");
        		if(quotedPrice != 0) {
        			sQuotedPrice = CommonUtility.round(quotedPrice, 0);
        			oNewEnquiryDto.setTotalLostPrice(CurrencyConvertUtility.buildPriceFormat(sQuotedPrice));
        		}
        		
        		if(QuotationConstants.QUOTATION_FORWARD_VIEW.equalsIgnoreCase(sOperationType)) {
        			if(!QuotationConstants.QUOTATION_ZERO.equals(rs.getString("QEM_IS_MARATHON_APPRV"))) {
        				oNewEnquiryDto.setIs_marathon_approved(rs.getString("QEM_IS_MARATHON_APPRV"));
        			}
        			if(!QuotationConstants.QUOTATION_ZERO.equals(rs.getString("QEM_EUINDUSTRY"))) {
        				oNewEnquiryDto.setEndUserIndustry(rs.getString("QEM_EUINDUSTRY"));
        			}
        			oNewEnquiryDto.setLocationOfInstallation(rs.getString("QEM_LOC_OF_INSTALL_DESC"));
        			// Commercial Terms
        			oNewEnquiryDto.setDeliveryTerm(rs.getString("QEM_DELIVERYTYPE_DESC"));
        			oNewEnquiryDto.setPt1PercentAmtNetorderValue(rs.getString("QEM_PAYTERM1_PERCENT_DESC"));
            		oNewEnquiryDto.setPt1PaymentDays(rs.getString("QEM_PAYTERM1_DAYS_DESC"));
            		oNewEnquiryDto.setPt1PayableTerms(rs.getString("QEM_PAYTERM1_TERMS_DESC"));
            		oNewEnquiryDto.setPt1Instrument(rs.getString("QEM_PAYTERM1_INSTRUMENT_DESC"));
            		oNewEnquiryDto.setPt2PercentAmtNetorderValue(rs.getString("QEM_PAYTERM2_PERCENT_DESC"));
            		oNewEnquiryDto.setPt2PaymentDays(rs.getString("QEM_PAYTERM2_DAYS_DESC"));
            		oNewEnquiryDto.setPt2PayableTerms(rs.getString("QEM_PAYTERM2_TERMS_DESC"));
            		oNewEnquiryDto.setPt2Instrument(rs.getString("QEM_PAYTERM2_INSTRUMENT_DESC"));
            		oNewEnquiryDto.setPt3PercentAmtNetorderValue(rs.getString("QEM_PAYTERM3_PERCENT_DESC"));
            		oNewEnquiryDto.setPt3PaymentDays(rs.getString("QEM_PAYTERM3_DAYS_DESC"));
            		oNewEnquiryDto.setPt3PayableTerms(rs.getString("QEM_PAYTERM3_TERMS_DESC"));
            		oNewEnquiryDto.setPt3Instrument(rs.getString("QEM_PAYTERM3_INSTRUMENT_DESC"));
            		oNewEnquiryDto.setPt4PercentAmtNetorderValue(rs.getString("QEM_PAYTERM4_PERCENT_DESC"));
            		oNewEnquiryDto.setPt4PaymentDays(rs.getString("QEM_PAYTERM4_DAYS_DESC"));
            		oNewEnquiryDto.setPt4PayableTerms(rs.getString("QEM_PAYTERM4_TERMS_DESC"));
            		oNewEnquiryDto.setPt4Instrument(rs.getString("QEM_PAYTERM4_INSTRUMENT_DESC"));
            		oNewEnquiryDto.setPt5PercentAmtNetorderValue(rs.getString("QEM_PAYTERM5_PERCENT_DESC"));
            		oNewEnquiryDto.setPt5PaymentDays(rs.getString("QEM_PAYTERM5_DAYS_DESC"));
            		oNewEnquiryDto.setPt5PayableTerms(rs.getString("QEM_PAYTERM5_TERMS_DESC"));
            		oNewEnquiryDto.setPt5Instrument(rs.getString("QEM_PAYTERM5_INSTRUMENT_DESC"));
            		oNewEnquiryDto.setWarrantyDispatchDate(rs.getString("QEM_DISPATCHDATEWRNTY_DESC"));
            		oNewEnquiryDto.setWarrantyCommissioningDate(rs.getString("QEM_COMMISSIONDATEWRNTY_DESC"));
            		oNewEnquiryDto.setDeliveryInFull(rs.getString("QEM_DELIVERY_IN_FULL_DESC"));
            		oNewEnquiryDto.setGstValue(rs.getString("QEM_GST_DESC"));
            		oNewEnquiryDto.setPackaging(rs.getString("QEM_PACKAGING_DESC"));
            		oNewEnquiryDto.setLiquidatedDamagesDueToDeliveryDelay(rs.getString("QEM_DELIVERY_DAMAGES_DESC"));
            		oNewEnquiryDto.setOfferValidity(rs.getString("QEM_OFFERVALIDITY_DESC"));
            		oNewEnquiryDto.setPriceValidity(rs.getString("QEM_PRICEVALIDITY_DESC"));
            		oNewEnquiryDto.setCustRequestedDeliveryTime(rs.getString("QEM_CUST_REQ_DELIVERY_TIMEDESC"));
            		
            		// Check Condition for "Pending Mfg. Plant Details" Status
            		if(oNewEnquiryDto.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGMFG) {
            			oNewEnquiryDto.setOrderCompletionLeadTime(rs.getString("QEM_ORDER_LEAD_TIME"));
            		} else {
            			oNewEnquiryDto.setOrderCompletionLeadTime(rs.getString("QEM_ORDER_LEAD_TIME_DESC"));
            		}
            		
            		// ABONDENT Details
            		oNewEnquiryDto.setAbondentReason1(rs.getString("QEM_ABONDENT_REASON1_DESC"));
            		oNewEnquiryDto.setAbondentReason2(rs.getString("QEM_ABONDENT_REASON2_DESC"));
            		// LOST Details.
            		oNewEnquiryDto.setLostReason1(rs.getString("QEM_LOST_REASON1_DESC"));
            		oNewEnquiryDto.setLostReason2(rs.getString("QEM_LOST_REASON2_DESC"));
            		
            		// Fetch RatingsList for "Pending Mfg. Plant Details" Status - EDIT Mode.
            		if(oNewEnquiryDto.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGMFG) {
            			oNewEnquiryDto.setNewRatingsList(fetchNewRatingDetails(conn, oNewEnquiryDto));
            			
            		}  // Fetch RatingsList for VIEW
            		else {
            			oNewEnquiryDto.setNewRatingsList(fetchNewRatingDetailsForView(conn, oNewEnquiryDto));
            		}
            		
        		} else if(QuotationConstants.QUOTATION_EDIT.equalsIgnoreCase(sOperationType)) {
        			oNewEnquiryDto.setIs_marathon_approved(rs.getString("QEM_IS_MARATHON_APPRV"));
        			oNewEnquiryDto.setEndUserIndustry(rs.getString("QEM_EUINDUSTRY"));
        			oNewEnquiryDto.setLocationOfInstallation(rs.getString("QEM_LOC_OF_INSTALLATION"));
        			// Commercial Terms
        			oNewEnquiryDto.setDeliveryTerm(rs.getString("QEM_DELIVERYTYPE"));
        			oNewEnquiryDto.setPt1PercentAmtNetorderValue(rs.getString("QEM_PAYTERM1_PERCENTVALUE"));
            		oNewEnquiryDto.setPt1PaymentDays(rs.getString("QEM_PAYTERM1_DAYS"));
            		oNewEnquiryDto.setPt1PayableTerms(rs.getString("QEM_PAYTERM1_TERMS"));
            		oNewEnquiryDto.setPt1Instrument(rs.getString("QEM_PAYTERM1_INSTRUMENT"));
            		oNewEnquiryDto.setPt2PercentAmtNetorderValue(rs.getString("QEM_PAYTERM2_PERCENTVALUE"));
            		oNewEnquiryDto.setPt2PaymentDays(rs.getString("QEM_PAYTERM2_DAYS"));
            		oNewEnquiryDto.setPt2PayableTerms(rs.getString("QEM_PAYTERM2_TERMS"));
            		oNewEnquiryDto.setPt2Instrument(rs.getString("QEM_PAYTERM2_INSTRUMENT"));
            		oNewEnquiryDto.setPt3PercentAmtNetorderValue(rs.getString("QEM_PAYTERM3_PERCENTVALUE"));
            		oNewEnquiryDto.setPt3PaymentDays(rs.getString("QEM_PAYTERM3_DAYS"));
            		oNewEnquiryDto.setPt3PayableTerms(rs.getString("QEM_PAYTERM3_TERMS"));
            		oNewEnquiryDto.setPt3Instrument(rs.getString("QEM_PAYTERM3_INSTRUMENT"));
            		oNewEnquiryDto.setPt4PercentAmtNetorderValue(rs.getString("QEM_PAYTERM4_PERCENTVALUE"));
            		oNewEnquiryDto.setPt4PaymentDays(rs.getString("QEM_PAYTERM4_DAYS"));
            		oNewEnquiryDto.setPt4PayableTerms(rs.getString("QEM_PAYTERM4_TERMS"));
            		oNewEnquiryDto.setPt4Instrument(rs.getString("QEM_PAYTERM4_INSTRUMENT"));
            		oNewEnquiryDto.setPt5PercentAmtNetorderValue(rs.getString("QEM_PAYTERM5_PERCENTVALUE"));
            		oNewEnquiryDto.setPt5PaymentDays(rs.getString("QEM_PAYTERM5_DAYS"));
            		oNewEnquiryDto.setPt5PayableTerms(rs.getString("QEM_PAYTERM5_TERMS"));
            		oNewEnquiryDto.setPt5Instrument(rs.getString("QEM_PAYTERM5_INSTRUMENT"));
            		oNewEnquiryDto.setWarrantyDispatchDate(rs.getString("QEM_DISPATCHDATEWRNTY"));
            		oNewEnquiryDto.setWarrantyCommissioningDate(rs.getString("QEM_COMMISSIONDATEWRNTY"));
            		oNewEnquiryDto.setOrderCompletionLeadTime(rs.getString("QEM_ORDER_LEAD_TIME"));
            		oNewEnquiryDto.setDeliveryInFull(rs.getString("QEM_DELIVERY_IN_FULL"));
            		oNewEnquiryDto.setGstValue(rs.getString("QEM_GST"));
            		oNewEnquiryDto.setPackaging(rs.getString("QEM_PACKAGING"));
            		oNewEnquiryDto.setLiquidatedDamagesDueToDeliveryDelay(rs.getString("QEM_DELIVERY_DAMAGES"));
            		oNewEnquiryDto.setOfferValidity(rs.getString("QEM_OFFERVALIDITY"));
            		oNewEnquiryDto.setPriceValidity(rs.getString("QEM_PRICEVALIDITY"));
            		oNewEnquiryDto.setCustRequestedDeliveryTime(rs.getString("QEM_CUST_REQ_DELIVERY_TIME"));
            		
            		// ABONDENT Details
            		oNewEnquiryDto.setAbondentReason1(rs.getString("QEM_ABONDENT_REASON1"));
            		oNewEnquiryDto.setAbondentReason2(rs.getString("QEM_ABONDENT_REASON2"));
            		// LOST Details.
            		oNewEnquiryDto.setLostReason1(rs.getString("QEM_LOST_REASON1"));
            		oNewEnquiryDto.setLostReason2(rs.getString("QEM_LOST_REASON2"));
            		
            		// Fetch RatingsList for EDIT
        			oNewEnquiryDto.setNewRatingsList(fetchNewRatingDetails(conn, oNewEnquiryDto));
        		}
        		
        	}
        	oDBUtility.releaseResources(rs, pstmt);
        	 
         	sSalesManagerId = getSalesManagerId(conn, oNewEnquiryDto.getCreatedBySSO());
         	pstmt = conn.prepareStatement(AdminQueries.FETCH_SALESMANGER_INFO);
			pstmt.setString(1, sSalesManagerId);
			rs = pstmt.executeQuery();
			if (rs != null && rs.next()) {
				oNewEnquiryDto.setSmEmail(rs.getString("SALESMANAGER_EMAIL"));
				oNewEnquiryDto.setSmContactNo(rs.getString("QSM_PHONENO"));
			}
			
        } finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
        
		return oNewEnquiryDto;
	}
	
	public NewEnquiryDto createNewEnquiry(Connection conn, NewEnquiryDto oNewEnquiryDto, String sOperationType) throws Exception {
		String sMethodName = "createNewEnquiry";
		
		/* PreparedStatement objects to handle database operations */
		PreparedStatement pstmt1 = null;
		PreparedStatement pstmt2 = null;
		ResultSet rs1 = null;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();

		int iEnquiryId = 0;
		String sEnquiryCode = "", sChkQuery = "";
		int isInserted = 0, iIndex = 1;
		StringTokenizer stToken = null;

		try {
			LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
			
			if(oNewEnquiryDto.getLocationId() == 0)
				oNewEnquiryDto.setLocationId(Integer.parseInt(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_PROPERTYFILENAME, QuotationConstants.QUOTATION_DEFAULT_LOCATION)));

			if (sOperationType.equalsIgnoreCase("INSERT")) {
				pstmt1 = conn.prepareStatement(FETCH_ENQUIRYID_NEXTVAL);
				pstmt1.setString(1, oNewEnquiryDto.getCreatedBySSO());
				pstmt1.setInt(2, oNewEnquiryDto.getRevisionNumber());
				rs1 = pstmt1.executeQuery();

				if (rs1.next()) {
					if (rs1.getString("ENQUIRYNO") != null) {
						stToken = new StringTokenizer(rs1.getString("ENQUIRYNO"), "^");
						if (stToken != null && stToken.hasMoreTokens()) {
							iEnquiryId = Integer.parseInt(stToken.nextToken());
							if (stToken.hasMoreTokens())
								sEnquiryCode = stToken.nextToken();
						}
					}
				}
				oDBUtility.releaseResources(rs1, pstmt1);
			}

			if (iEnquiryId > 0) {
				if (oNewEnquiryDto.getRevisionNumber() == 0)
					oNewEnquiryDto.setRevisionNumber(1);

				if (sOperationType.equalsIgnoreCase("INSERT")) {
					pstmt2 = conn.prepareStatement(INSERT_NEWENQUIRY_DETAILS);

					oNewEnquiryDto.setEnquiryNumber(sEnquiryCode);
					
					/* Non Null Fields */
					pstmt2.setInt(iIndex++, iEnquiryId);
					pstmt2.setString(iIndex++, sEnquiryCode);
					pstmt2.setInt(iIndex++, oNewEnquiryDto.getRevisionNumber());
					pstmt2.setInt(iIndex++, iEnquiryId);
					pstmt2.setInt(iIndex++, oNewEnquiryDto.getCustomerId());
					pstmt2.setInt(iIndex++, oNewEnquiryDto.getLocationId());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getEnquiryType());
					pstmt2.setInt(iIndex++, 1);
					pstmt2.setString(iIndex++, oNewEnquiryDto.getCreatedBySSO());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getCreatedBySSO());
					/* Non Null Fields */
					pstmt2.setString(iIndex++, oNewEnquiryDto.getCustomerEnqReference());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getCustomerIndustry());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getConcernedPerson());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getProjectName());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getIs_marathon_approved());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getConsultantName());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getEndUser());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getEndUserIndustry());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getLocationOfInstallationId());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getWinningChance());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getTargeted());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getReceiptDate());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getSubmissionDate());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getClosingDate());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getDeliveryTerm());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getWarrantyDispatchDate());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getWarrantyCommissioningDate());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getOrderCompletionLeadTime());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getDeliveryInFull());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getGstValue());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getPackaging());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getLiquidatedDamagesDueToDeliveryDelay());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getSupervisionDetails());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getPt1PercentAmtNetorderValue());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getPt1PaymentDays());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getPt1PayableTerms());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getPt1Instrument());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getPt2PercentAmtNetorderValue());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getPt2PaymentDays());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getPt2PayableTerms());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getPt2Instrument());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getPt3PercentAmtNetorderValue());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getPt3PaymentDays());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getPt3PayableTerms());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getPt3Instrument());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getPt4PercentAmtNetorderValue());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getPt4PaymentDays());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getPt4PayableTerms());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getPt4Instrument());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getPt5PercentAmtNetorderValue());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getPt5PaymentDays());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getPt5PayableTerms());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getPt5Instrument());
					pstmt2.setString(iIndex++, "Y");
					pstmt2.setString(iIndex++, oNewEnquiryDto.getSmNote());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getCreatedBy());
					if("Yes".equalsIgnoreCase(oNewEnquiryDto.getSupervisionDetails())) {
						pstmt2.setString(iIndex++, oNewEnquiryDto.getSuperviseNoOfManDays());
						//pstmt2.setString(iIndex++, oNewEnquiryDto.getSuperviseToFroTravel());
						//pstmt2.setString(iIndex++, oNewEnquiryDto.getSuperviseLodging());
					} else {
						pstmt2.setString(iIndex++, "");
						//pstmt2.setString(iIndex++, "");
						//pstmt2.setString(iIndex++, "");
					}
					pstmt2.setString(iIndex++, oNewEnquiryDto.getIsReferDesign());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getCommentsDeviations());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getOfferValidity());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getPriceValidity());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getCustRequestedDeliveryTime());
					
					isInserted = pstmt2.executeUpdate();
					if (isInserted == 0)
						iEnquiryId = 0;
				}
				oNewEnquiryDto.setEnquiryId(iEnquiryId);
			}
		} finally {
			/* Releasing PreparedStatment objects */
			oDBUtility.releaseResources(rs1, pstmt1);
			oDBUtility.releaseResources(pstmt2);
		}

		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
		return oNewEnquiryDto;
	}
	
	public NewEnquiryDto updateNewEnquiry(Connection conn, NewEnquiryDto oNewEnquiryDto, String sOperationType) throws Exception {
		String sMethodName = "updateNewEnquiry";
		
		/* PreparedStatement objects to handle database operations */
		PreparedStatement pstmt1 = null;
		PreparedStatement pstmt2 = null;
		ResultSet rs1 = null;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();

		String sEnquiryCode = "", sChkQuery = "";
		int isInserted = 0, iIndex = 1;
		StringTokenizer stToken = null;
		
		try {
			LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
			
			if(oNewEnquiryDto.getLocationId() == 0)
				oNewEnquiryDto.setLocationId(Integer.parseInt(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_PROPERTYFILENAME, QuotationConstants.QUOTATION_DEFAULT_LOCATION)));
			
			if(oNewEnquiryDto.getEnquiryId() > 0) {
				if (oNewEnquiryDto.getRevisionNumber() == 0)
					oNewEnquiryDto.setRevisionNumber(1);
				
				sChkQuery = " SELECT * FROM " + SCHEMA_QUOTATION + ".RFQ_ENQUIRY_MASTER WHERE QEM_ENQUIRYID = ? ";
				pstmt1 = conn.prepareStatement(sChkQuery);
				pstmt1.setInt(1, oNewEnquiryDto.getEnquiryId());
				rs1 = pstmt1.executeQuery();
				
				if (rs1.next()) {
					pstmt2 = conn.prepareStatement(UPDATE_NEWENQUIRY_DETAILS);
					
					/* Non Null Fields */
					pstmt2.setInt(iIndex++, oNewEnquiryDto.getRevisionNumber());
					pstmt2.setInt(iIndex++, oNewEnquiryDto.getCustomerId());
					pstmt2.setInt(iIndex++, oNewEnquiryDto.getLocationId());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getEnquiryType());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getCreatedBySSO());
					/* Non Null Fields */
					pstmt2.setString(iIndex++, oNewEnquiryDto.getCustomerEnqReference());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getCustomerIndustry());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getConcernedPerson());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getProjectName());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getIs_marathon_approved());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getConsultantName());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getEndUser());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getEndUserIndustry());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getLocationOfInstallationId());
					//pstmt2.setString(iIndex++, oNewEnquiryDto.getEnquiryStage());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getWinningChance());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getTargeted());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getReceiptDate());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getSubmissionDate());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getClosingDate());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getDeliveryTerm());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getWarrantyDispatchDate());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getWarrantyCommissioningDate());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getOrderCompletionLeadTime());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getDeliveryInFull());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getGstValue());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getPackaging());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getLiquidatedDamagesDueToDeliveryDelay());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getSupervisionDetails());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getPt1PercentAmtNetorderValue());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getPt1PaymentDays());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getPt1PayableTerms());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getPt1Instrument());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getPt2PercentAmtNetorderValue());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getPt2PaymentDays());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getPt2PayableTerms());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getPt2Instrument());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getPt3PercentAmtNetorderValue());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getPt3PaymentDays());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getPt3PayableTerms());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getPt3Instrument());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getPt4PercentAmtNetorderValue());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getPt4PaymentDays());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getPt4PayableTerms());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getPt4Instrument());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getPt5PercentAmtNetorderValue());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getPt5PaymentDays());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getPt5PayableTerms());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getPt5Instrument());
					pstmt2.setString(iIndex++, "Y");
					pstmt2.setString(iIndex++, oNewEnquiryDto.getSmNote());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getCreatedBy());
					if("Yes".equalsIgnoreCase(oNewEnquiryDto.getSupervisionDetails())) {
						pstmt2.setString(iIndex++, oNewEnquiryDto.getSuperviseNoOfManDays());
						//pstmt2.setString(iIndex++, oNewEnquiryDto.getSuperviseToFroTravel());
						//pstmt2.setString(iIndex++, oNewEnquiryDto.getSuperviseLodging());
					} else {
						pstmt2.setString(iIndex++, "");
						//pstmt2.setString(iIndex++, "");
						//pstmt2.setString(iIndex++, "");
					}
					pstmt2.setString(iIndex++, oNewEnquiryDto.getIsReferDesign());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getCommentsDeviations());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getOfferValidity());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getPriceValidity());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getCustRequestedDeliveryTime());
					pstmt2.setString(iIndex++, oNewEnquiryDto.getCustomerDealerLink());
					
					// For Where Condition
					pstmt2.setInt(iIndex++, oNewEnquiryDto.getEnquiryId());
					
					isInserted = pstmt2.executeUpdate();

					if (isInserted == 0)
						oNewEnquiryDto.setEnquiryId(new Integer(0));
				}
			}
			
		} finally {
			/* Releasing PreparedStatment objects */
			oDBUtility.releaseResources(rs1, pstmt1);
			oDBUtility.releaseResources(pstmt2);
		}

		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
		return oNewEnquiryDto;
	}
	
	public String saveRatingDetails(Connection conn, NewRatingDto oNewRatingDto) throws Exception {
		String sMethodName = "saveRatingDetails";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null;
		PreparedStatement pstmt1 = null;
		PreparedStatement pstmt2 = null;
		/* ResultSet object to store the rows retrieved from database */
		ResultSet rs = null, rs1 = null;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();
		
		int iRatingId = 0, iResult = 0;
		String sResult = QuotationConstants.QUOTATION_STRING_Y;
		
		String sSQLRatingQuery = null;
		String sChkQuery = "SELECT * FROM " + SCHEMA_QUOTATION + ".RFQ_LT_RATING_DETAILS WHERE QRD_LT_RATINGID = ? AND QRD_LT_ENQUIRYID = ?";
		pstmt = conn.prepareStatement(sChkQuery);
		
		try {
			pstmt.setInt(1, oNewRatingDto.getRatingId());
			pstmt.setInt(2, oNewRatingDto.getEnquiryId());
			rs = pstmt.executeQuery();
			
			if(rs.next()) {		// UPDATE
				iRatingId = oNewRatingDto.getRatingId();
				sSQLRatingQuery = buildNewRatingUpdateQuery(oNewRatingDto, iRatingId, false);
				pstmt1 = conn.prepareStatement(sSQLRatingQuery);
				iResult = pstmt1.executeUpdate();
			} else {			// INSERT
				pstmt2 = conn.prepareStatement(FETCH_LT_RATINGID_NEXTVAL);	    
	    		rs1 = pstmt2.executeQuery();
	    		if (rs1.next()) {
	    			iRatingId = Integer.parseInt(rs1.getString("RATING_ID"));
	    		}
	    		oDBUtility.releaseResources(rs1, pstmt2);
	    		
	    		sSQLRatingQuery = buildNewRatingInsertQuery(oNewRatingDto, iRatingId);
	    		pstmt2 = conn.prepareStatement(sSQLRatingQuery);
	    		iResult = pstmt2.executeUpdate();
			}
			if(iResult == 0) {
				sResult = QuotationConstants.QUOTATION_STRING_N;
			}
		} finally {
			oDBUtility.releaseResources(rs, pstmt);
			oDBUtility.releaseResources(pstmt1);
            oDBUtility.releaseResources(pstmt2);
		}
		
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
		return sResult;
	}
	
	public String saveRatingByIdDetails(Connection conn, NewRatingDto oNewRatingDto) throws Exception {
		String sMethodName = "saveRatingByIdDetails";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null;
		PreparedStatement pstmt1 = null;
		PreparedStatement pstmt2 = null;
		/* ResultSet object to store the rows retrieved from database */
		ResultSet rs = null, rs1 = null;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();
		
		int iIndex = 1, iRatingId = 0, iCountVal = 0, iResult = 0;
		String sResult = QuotationConstants.QUOTATION_STRING_Y;
		
		String sSQLRatingQuery = null;
		String sChkQuery = "SELECT * FROM " + SCHEMA_QUOTATION + ".RFQ_LT_RATING_DETAILS WHERE QRD_LT_RATINGID = ? AND QRD_LT_ENQUIRYID = ?";
		pstmt = conn.prepareStatement(sChkQuery);
		
		try {
			pstmt.setInt(1, oNewRatingDto.getRatingId());
			pstmt.setInt(2, oNewRatingDto.getEnquiryId());
			rs = pstmt.executeQuery();
			
			if(rs.next()) {		// UPDATE
				iRatingId = oNewRatingDto.getRatingId();
				sSQLRatingQuery = buildNewRatingUpdateQuery(oNewRatingDto, iRatingId, false);
				pstmt1 = conn.prepareStatement(sSQLRatingQuery);
				iResult = pstmt1.executeUpdate();
				
			} else {			// INSERT
				pstmt2 = conn.prepareStatement(FETCH_LT_RATINGID_NEXTVAL);	    
	    		rs1 = pstmt2.executeQuery();
	    		if (rs1.next()) {
	    			iRatingId = Integer.parseInt(rs1.getString("RATING_ID"));
	    		}
	    		oDBUtility.releaseResources(rs1, pstmt2);
	    		
	    		sSQLRatingQuery = buildNewRatingInsertQuery(oNewRatingDto, iRatingId);
	    		pstmt2 = conn.prepareStatement(sSQLRatingQuery);
	    		iResult = pstmt2.executeUpdate();
			}
			
			
			if(iResult == 0) {
				sResult = QuotationConstants.QUOTATION_STRING_N;
			}
		} finally {
			oDBUtility.releaseResources(pstmt1);
            oDBUtility.releaseResources(pstmt2);
            oDBUtility.releaseResources(rs, pstmt);
		}
		
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
		return sResult;
	}
	
	private String buildNewRatingUpdateQuery(NewRatingDto oNewRatingDto, int iRatingId, boolean updateMTOFields) {
		String sMethodName = "buildNewRatingUpdateQuery";

		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		StringBuffer sbFieldQuery = new StringBuffer();

		if (oNewRatingDto != null) {
			sbFieldQuery.append(UPDATE_NEWRATING_DETAILS);

			/* QRD_QUANTITY */
			sbFieldQuery.append("QRD_LT_QUANTITY = ");
			sbFieldQuery.append(oNewRatingDto.getQuantity() + ", ");

			// Fields from here on are optional fields, so include only when they are not empty
			if (Integer.parseInt(oNewRatingDto.getLtProdLineId()) > 0) {
				sbFieldQuery.append("QRD_LT_PRODLINEID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtProdLineId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtKWId()) ) {
				sbFieldQuery.append("QRD_LT_KW = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtKWId() + "', ");
			}
			if (Integer.parseInt(oNewRatingDto.getLtPoleId()) > 0) {
				sbFieldQuery.append("QRD_LT_POLE = ");
				sbFieldQuery.append(Integer.parseInt(oNewRatingDto.getLtPoleId()) + ", ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtFrameId()) ) {
				sbFieldQuery.append("QRD_LT_FRAME = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtFrameId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtFrameSuffixId()) ) {
				sbFieldQuery.append("QRD_LT_FRAME_SUFFIX = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtFrameSuffixId() + "', ");
			}
			if (Integer.parseInt(oNewRatingDto.getLtMountingId()) > 0) {
				sbFieldQuery.append("QRD_LT_MOUNTINGID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtMountingId() + "', ");
			}
			if (Integer.parseInt(oNewRatingDto.getLtTBPositionId()) > 0) {
				sbFieldQuery.append("QRD_LT_TBPOSITIONID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtTBPositionId() + "', ");
			}
			/* **********************  Above Fields are for List price Search ************************ */
			
			
			if (StringUtils.isNotBlank(oNewRatingDto.getTagNumber()) ) {
				sbFieldQuery.append("QRD_LT_TAG_NUMBER = ");
				sbFieldQuery.append("'"+oNewRatingDto.getTagNumber() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtManufacturingLocation()) ) {
				sbFieldQuery.append("QRD_LT_MFGLOCATION = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtManufacturingLocation() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtEffClass()) ) {
				sbFieldQuery.append("QRD_LT_EFFICIENCYCLASS = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtEffClass() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtVoltId()) ) {
				sbFieldQuery.append("QRD_LT_VOLTAGEID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtVoltId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtVoltAddVariation()) ) {
				sbFieldQuery.append("QRD_LT_VOLTADDVARIATION = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtVoltAddVariation() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtVoltRemoveVariation()) ) {
				sbFieldQuery.append("QRD_LT_VOLTREMOVEVARIATION = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtVoltRemoveVariation() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtFreqId()) ) {
				sbFieldQuery.append("QRD_LT_FREQUENCYID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtFreqId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtFreqAddVariation()) ) {
				sbFieldQuery.append("QRD_LT_FREQADDVARIATION = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtFreqAddVariation() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtFreqRemoveVariation()) ) {
				sbFieldQuery.append("QRD_LT_FREQREMOVEVARIATION = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtFreqRemoveVariation() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtCombVariation()) ) {
				sbFieldQuery.append("QRD_LT_COMBINEDVARIATION = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtCombVariation() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtServiceFactor()) ) {
				sbFieldQuery.append("QRD_LT_SERVICEFACTOR = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtServiceFactor() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtMethodOfStarting()) ) {
				sbFieldQuery.append("QRD_LT_METHODOFSTARTINGID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtMethodOfStarting() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtStartingCurrentOnDOLStart()) ) {
				sbFieldQuery.append("QRD_LT_STARTINGCURRENTDOLID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtStartingCurrentOnDOLStart() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtAmbTempId())) {
				sbFieldQuery.append("QRD_LT_AMBTEMPADDID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtAmbTempId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtAmbTempRemoveId())) {
				sbFieldQuery.append("QRD_LT_AMBTEMPREMOVEID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtAmbTempRemoveId() + "', ");
			}	
			if (StringUtils.isNotBlank(oNewRatingDto.getLtTempRise())) {
				sbFieldQuery.append("QRD_LT_TEMPRISEID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtTempRise() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtInsulationClassId()) ) {
				sbFieldQuery.append("QRD_LT_INSULATIONCLASSID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtInsulationClassId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtForcedCoolingId()) ) {
				sbFieldQuery.append("QRD_LT_METHODOFCOOLING = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtForcedCoolingId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtAltitude()) ) {
				sbFieldQuery.append("QRD_LT_ALTITUDE = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtAltitude() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtHazardAreaId()) ) {
				sbFieldQuery.append("QRD_LT_HAZARDAREAID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtHazardAreaId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtHazardAreaTypeId()) ) {
				sbFieldQuery.append("QRD_LT_HAZARDAREATYPEID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtHazardAreaTypeId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtGasGroupId()) ) {
				sbFieldQuery.append("QRD_LT_GASGROUPID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtGasGroupId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtHazardZoneId()) ) {
				sbFieldQuery.append("QRD_LT_HAZARDZONEID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtHazardZoneId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtDustGroupId()) ) {
				sbFieldQuery.append("QRD_LT_DUSTGROUPID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtDustGroupId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtTempClass()) ) {
				sbFieldQuery.append("QRD_LT_TEMPCLASSID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtTempClass() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtApplicationId()) ) {
				sbFieldQuery.append("QRD_LT_APPLICATIONID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtApplicationId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtDutyId())) {
				sbFieldQuery.append("QRD_LT_DUTYID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtDutyId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtCDFId())) {
				sbFieldQuery.append("QRD_LT_CDFID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtCDFId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtStartsId())) {
				sbFieldQuery.append("QRD_LT_STARTSID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtStartsId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtNoiseLevel())) {
				sbFieldQuery.append("QRD_LT_NOISELEVEL = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtNoiseLevel() + "', ");
			}
			
			/* **********************************************	Price Fields	********************************************  */
			if (StringUtils.isNotBlank(oNewRatingDto.getLtTotalAddonPercent()) ) {
				sbFieldQuery.append("QRD_LT_TOTALADDONPERCENT = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtTotalAddonPercent() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtTotalCashExtra()) ) {
				sbFieldQuery.append("QRD_LT_TOTALCASHEXTRA = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtFrameId() + "', ");
			}
			sbFieldQuery.append("QRD_LT_LISTPRICE = ");
			sbFieldQuery.append("'"+oNewRatingDto.getLtLPPerUnit() + "', ");
			
			sbFieldQuery.append("QRD_LT_LISTPRICEWITHADDON = ");
			sbFieldQuery.append("'"+oNewRatingDto.getLtLPWithAddonPerUnit() + "', ");
			
			if (StringUtils.isNotBlank(oNewRatingDto.getLtStandardDiscount()) ) {
				sbFieldQuery.append("QRD_LT_STANDARDDISCOUNT = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtStandardDiscount() + "', ");
			}
			
			sbFieldQuery.append("QRD_LT_PRICEPERUNIT = ");
			sbFieldQuery.append("'"+oNewRatingDto.getLtPricePerUnit() + "', ");
			
			sbFieldQuery.append("QRD_LT_TOTALORDERPRICE = ");
			sbFieldQuery.append("'"+oNewRatingDto.getLtTotalOrderPrice() + "', ");
			
			sbFieldQuery.append("QRD_LT_MATERIALCOST = ");
			sbFieldQuery.append("'"+oNewRatingDto.getRatingMaterialCost() + "', ");
			
			sbFieldQuery.append("QRD_LT_LOH = ");
			sbFieldQuery.append("'"+oNewRatingDto.getRatingLOH() + "', ");
			
			sbFieldQuery.append("QRD_LT_TOTALCOST = ");
			sbFieldQuery.append("'"+oNewRatingDto.getRatingTotalCost() + "', ");
			
			sbFieldQuery.append("QRD_LT_PROFITMARGIN = ");
			sbFieldQuery.append("'"+oNewRatingDto.getRatingProfitMargin() + "', ");
			
			/* **********************************************	Price Fields	********************************************  */
			
			/*
			if (StringUtils.isNotBlank(oNewRatingDto.getLtRequestedDiscount_SM()) ) {
				sbFieldQuery.append("QRD_LT_REQUESTEDDISCOUNT_SM = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtRequestedDiscount_SM() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtRequestedPricePerUnit_SM()) ) {
				sbFieldQuery.append("QRD_LT_REQ_PRICEPERUNIT_SM = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtRequestedPricePerUnit_SM() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtRequestedTotalPrice_SM()) ) {
				sbFieldQuery.append("QRD_LT_REQ_TOTALPRICE_SM = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtRequestedTotalPrice_SM() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtApprovedDiscount_RSM()) ) {
				sbFieldQuery.append("QRD_LT_APPROVEDDISCOUNT_RSM = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtApprovedDiscount_RSM() + "', ");
			}
			sbFieldQuery.append("QRD_LT_APPRV_PRICEPERUNIT_RSM = ");
			sbFieldQuery.append("'"+oNewRatingDto.getLtApprovedPricePerunit_RSM() + "', ");
			sbFieldQuery.append("QRD_LT_APPRV_TOTALPRICE_RSM = ");
			sbFieldQuery.append("'"+oNewRatingDto.getLtApprovedTotalPrice_RSM() + "', ");
			if (StringUtils.isNotBlank(oNewRatingDto.getLtQuotedDiscount()) ) {
				sbFieldQuery.append("QRD_LT_QUOTEDDISCOUNT = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtQuotedDiscount() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtQuotedPricePerUnit()) ) {
				sbFieldQuery.append("QRD_LT_QUOTED_PRICEPERUNIT = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtQuotedPricePerUnit() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtQuotedTotalPrice()) ) {
				sbFieldQuery.append("QRD_LT_QUOTED_TOTALPRICE = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtQuotedTotalPrice() + "', ");
			}
			*/
			
			/* **********************************************	Standard & Customer Req. Delivery Fields	*********************  */
			if (StringUtils.isNotBlank(oNewRatingDto.getLtReplacementMotor()) ) {
				sbFieldQuery.append("QRD_LT_REPLACEMENTMOTOR = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtReplacementMotor() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtEarlierMotorSerialNo()) ) {
				sbFieldQuery.append("QRD_LT_EARLIER_SUPPLIED_MOTOR = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtEarlierMotorSerialNo() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtStandardDeliveryWks()) ) {
				sbFieldQuery.append("QRD_LT_STANDARD_DELIVERY = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtStandardDeliveryWks() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtCustReqDeliveryWks()) ) {
				sbFieldQuery.append("QRD_LT_CUSTOMER_REQ_DELIVERY = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtCustReqDeliveryWks() + "', ");
			}
			
			
			/* **********************************************	Electrical Fields	********************************************  */
			if (StringUtils.isNotBlank(oNewRatingDto.getLtRVId()) ) {
				sbFieldQuery.append("QRD_LT_RV = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtRVId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtRAId()) ) {
				sbFieldQuery.append("QRD_LT_RA = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtRAId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtWindingConfig()) ) {
				sbFieldQuery.append("QRD_LT_WINDINGCONFIGID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtWindingConfig() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtWindingTreatmentId()) ) {
				sbFieldQuery.append("QRD_LT_WINDINGTREATMENTID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtWindingTreatmentId() + "', ");
			}
			/*if (StringUtils.isNotBlank(oNewRatingDto.getLtWindingWire()) ) {
				sbFieldQuery.append("QRD_LT_WINDINGWIREID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtWindingWire() + "', ");
			}*/
			if (StringUtils.isNotBlank(oNewRatingDto.getLtLeadId()) ) {
				sbFieldQuery.append("QRD_LT_LEADID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtLeadId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtLoadGD2Value()) ) {
				sbFieldQuery.append("QRD_LT_LOADGD2 = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtLoadGD2Value() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtVFDApplTypeId()) ) {
				sbFieldQuery.append("QRD_LT_VFDTYPEID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtVFDApplTypeId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtVFDMotorSpeedRangeMin()) ) {
				sbFieldQuery.append("QRD_LT_VFDSPEEDMINID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtVFDMotorSpeedRangeMin() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtVFDMotorSpeedRangeMax()) ) {
				sbFieldQuery.append("QRD_LT_VFDSPEEDMAXID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtVFDMotorSpeedRangeMax() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtOverloadingDuty()) ) {
				sbFieldQuery.append("QRD_LT_OVERLOADDUTYID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtOverloadingDuty() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtConstantEfficiencyRange()) ) {
				sbFieldQuery.append("QRD_LT_CONSTEFFRANGEID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtConstantEfficiencyRange() + "', ");
			}
			
			
			/* **********************************************	Mechanical Fields	********************************************  */
			if (StringUtils.isNotBlank(oNewRatingDto.getLtShaftTypeId()) ) {
				sbFieldQuery.append("QRD_LT_SHAFTTYPEID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtShaftTypeId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtShaftMaterialId()) ) {
				sbFieldQuery.append("QRD_LT_SHAFTMATERIALID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtShaftMaterialId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtDirectionOfRotation()) ) {
				sbFieldQuery.append("QRD_LT_ROTATIONDIRECTIONID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtDirectionOfRotation() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtStandardRotation()) ) {
				sbFieldQuery.append("QRD_LT_STANDARDROTATION = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtStandardRotation() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtMethodOfCoupling()) ) {
				sbFieldQuery.append("QRD_LT_METHODOFCOUPLINGID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtMethodOfCoupling() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtPaintingTypeId()) ) {
				sbFieldQuery.append("QRD_LT_PAINTTYPEID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtPaintingTypeId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtPaintShadeId()) ) {
				sbFieldQuery.append("QRD_LT_PAINTSHADEID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtPaintShadeId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtPaintShadeValue()) ) {
				sbFieldQuery.append("QRD_LT_PAINTSHADE_VALUE = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtPaintShadeValue() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtPaintThicknessId()) ) {
				sbFieldQuery.append("QRD_LT_PAINTTHICKESSID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtPaintThicknessId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtIPId()) ) {
				sbFieldQuery.append("QRD_LT_IPID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtIPId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtCableSizeId()) ) {
				sbFieldQuery.append("QRD_LT_CABLESIZEID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtCableSizeId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtNoOfRuns()) ) {
				sbFieldQuery.append("QRD_LT_NOOFRUNSID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtNoOfRuns() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtNoOfCores()) ) {
				sbFieldQuery.append("QRD_LT_NOOFCORESID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtNoOfCores() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtCrossSectionAreaId()) ) {
				sbFieldQuery.append("QRD_LT_CROSSSECTIONID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtCrossSectionAreaId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtConductorMaterialId()) ) {
				sbFieldQuery.append("QRD_LT_CONDMATERIALID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtConductorMaterialId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtCableDiameter()) ) {
				sbFieldQuery.append("QRD_LT_CABLEDIAMETER = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtCableDiameter() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtTerminalBoxSizeId()) ) {
				sbFieldQuery.append("QRD_LT_TERMINALBOXSIZEID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtTerminalBoxSizeId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtSpreaderBoxId()) ) {
				sbFieldQuery.append("QRD_LT_SPREADERBOX = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtSpreaderBoxId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtSpaceHeaterId()) ) {
				sbFieldQuery.append("QRD_LT_SPACEHEATER = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtSpaceHeaterId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtVibrationId()) ) {
				sbFieldQuery.append("QRD_LT_VIBRATION = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtVibrationId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtFlyingLeadWithoutTDId()) ) {
				sbFieldQuery.append("QRD_LT_FLYINGLEADWITHOUTTB = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtFlyingLeadWithoutTDId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtFlyingLeadWithTDId()) ) {
				sbFieldQuery.append("QRD_LT_FLYINGLEADWITHTB = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtFlyingLeadWithTDId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtMetalFanId()) ) {
				sbFieldQuery.append("QRD_LT_METALFAN = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtMetalFanId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtTechoMounting()) ) {
				sbFieldQuery.append("QRD_LT_TECHOMOUNTING = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtTechoMounting() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtShaftGroundingId()) ) {
				sbFieldQuery.append("QRD_LT_SHAFTGROUNDING = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtShaftGroundingId() + "', ");
			}
			
			
			/* **********************************************	Accessories Fields	********************************************  */
			if (StringUtils.isNotBlank(oNewRatingDto.getLtHardware()) ) {
				sbFieldQuery.append("QRD_LT_HARDWARE = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtHardware() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtGlandPlateId()) ) {
				sbFieldQuery.append("QRD_LT_GLANDPLATE = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtGlandPlateId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtDoubleCompressionGlandId()) ) {
				sbFieldQuery.append("QRD_LT_DOUBLECOMPRESSION = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtDoubleCompressionGlandId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtSPMMountingProvisionId()) ) {
				sbFieldQuery.append("QRD_LT_SPMMOUNTING = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtSPMMountingProvisionId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtAddNamePlateId()) ) {
				sbFieldQuery.append("QRD_LT_ADDLNAMEPLATE = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtAddNamePlateId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtDirectionArrowPlateId()) ) {
				sbFieldQuery.append("QRD_LT_DIRECTIONARROWPLATE = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtDirectionArrowPlateId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtRTDId()) ) {
				sbFieldQuery.append("QRD_LT_RTDID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtRTDId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtBTDId()) ) {
				sbFieldQuery.append("QRD_LT_BTDID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtBTDId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtThermisterId()) ) {
				sbFieldQuery.append("QRD_LT_THERMISTERID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtThermisterId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtAuxTerminalBoxId()) ) {
				sbFieldQuery.append("QRD_LT_AUXTERMINALBOX = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtAuxTerminalBoxId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtCableSealingBoxId()) ) {
				sbFieldQuery.append("QRD_LT_CABLESEALINGBOX = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtCableSealingBoxId() + "', ");
			}
			
			
			/* **********************************************	Bearings Fields	********************************************  */
			if (StringUtils.isNotBlank(oNewRatingDto.getLtBearingSystemId()) ) {
				sbFieldQuery.append("QRD_LT_BEARINGSYSTEMID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtBearingSystemId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtBearingNDEId()) ) {
				sbFieldQuery.append("QRD_LT_BEARINGNDEID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtBearingNDEId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtBearingDEId()) ) {
				sbFieldQuery.append("QRD_LT_BEARINGDEID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtBearingDEId() + "', ");
			}
			
			
			/* **********************************************	Certificate, Spares, Tests Fields	*************************  */
			if (StringUtils.isNotBlank(oNewRatingDto.getLtWitnessRoutineId()) ) {
				sbFieldQuery.append("QRD_LT_WITNESSROUTINE = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtWitnessRoutineId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtAdditionalTest1Id()) ) {
				sbFieldQuery.append("QRD_LT_ADDL_TEST1 = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtAdditionalTest1Id() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtAdditionalTest2Id()) ) {
				sbFieldQuery.append("QRD_LT_ADDL_TEST2 = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtAdditionalTest2Id() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtAdditionalTest3Id()) ) {
				sbFieldQuery.append("QRD_LT_ADDL_TEST3 = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtAdditionalTest3Id() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtTypeTestId()) ) {
				sbFieldQuery.append("QRD_LT_TYPETEST = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtTypeTestId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtULCEId()) ) {
				sbFieldQuery.append("QRD_LT_CERTIFICATION = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtULCEId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtQAPId()) ) {
				sbFieldQuery.append("QRD_LT_QAPID = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtQAPId() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtSpares1()) ) {
				sbFieldQuery.append("QRD_LT_SPARES1 = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtSpares1() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtSpares2()) ) {
				sbFieldQuery.append("QRD_LT_SPARES2 = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtSpares2() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtSpares3()) ) {
				sbFieldQuery.append("QRD_LT_SPARES3 = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtSpares3() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtSpares4()) ) {
				sbFieldQuery.append("QRD_LT_SPARES4 = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtSpares4() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtSpares5()) ) {
				sbFieldQuery.append("QRD_LT_SPARES5 = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtSpares5() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtDataSheet()) ) {
				sbFieldQuery.append("QRD_LT_DATASHEET = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtDataSheet() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtMotorGA()) ) {
				sbFieldQuery.append("QRD_LT_MOTORGA = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtMotorGA() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtPerfCurves()) ) {
				sbFieldQuery.append("QRD_LT_PERFCURVE = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtPerfCurves() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtTBoxGA()) ) {
				sbFieldQuery.append("QRD_LT_TBOXGA = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtTBoxGA() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getLtAdditionalComments()) ) {
				sbFieldQuery.append("QRD_LT_ADDL_COMMENTS = ");
				sbFieldQuery.append("'"+oNewRatingDto.getLtAdditionalComments() + "', ");
			}
			
			/* ************************************   Flags for Design page ************************************** */
			if (StringUtils.isNotBlank(oNewRatingDto.getFlg_PriceFields_MTO()) ) {
				sbFieldQuery.append("FLG_PRICEFIELDS_MTO = ");
				sbFieldQuery.append("'"+oNewRatingDto.getFlg_PriceFields_MTO() + "', ");
			}
			
			if (StringUtils.isNotBlank(oNewRatingDto.getIsReferDesign()) ) {
				sbFieldQuery.append("QRD_LT_ISREFERDESIGN = ");
				sbFieldQuery.append("'"+oNewRatingDto.getIsReferDesign() + "', ");
			}
			if (StringUtils.isNotBlank(oNewRatingDto.getIsDesignComplete()) ) {
				sbFieldQuery.append("QRD_LT_ISDESIGNCOMPLETE = ");
				sbFieldQuery.append("'"+oNewRatingDto.getIsDesignComplete() + "', ");
			}
			
			if (sbFieldQuery.toString().trim().endsWith(","))
				sbFieldQuery = new StringBuffer(sbFieldQuery.toString().trim().substring(0, sbFieldQuery.toString().trim().lastIndexOf(",")));

			sbFieldQuery.append(" WHERE QRD_LT_RATINGID = " + oNewRatingDto.getRatingId());
		}

		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "sbFieldQuery ...... " + sbFieldQuery);
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
		
		return sbFieldQuery.toString();
	}
	
	private String buildNewRatingInsertQuery(NewRatingDto oNewRatingDto, int iRatingId) {
		String sMethodName = "buildNewRatingInsertQuery";

		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		String sInsertQuery = null;
		StringBuffer sbFieldQuery = new StringBuffer();
		StringBuffer sbValuesQuery = new StringBuffer();

		if (oNewRatingDto != null) {
			sbFieldQuery.append(INSERT_NEWRATING_DETAILS);

			sbFieldQuery.append("(");
			sbValuesQuery.append(" VALUES (");

			/* Rating ID */
			LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "RatingId -  " + iRatingId);
			sbFieldQuery.append("QRD_LT_RATINGID, ");
			sbValuesQuery.append(iRatingId + ", ");

			/* EnquiryId, RatingNo Setting */
			LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "Enquiry Id -  " + oNewRatingDto.getEnquiryId());
			sbFieldQuery.append("QRD_LT_ENQUIRYID, ");
			sbValuesQuery.append(oNewRatingDto.getEnquiryId() + ", ");
			
			sbFieldQuery.append("QRD_LT_RATINGNO, ");
			sbValuesQuery.append(oNewRatingDto.getRatingNo() + ", ");
			
			sbFieldQuery.append("QRD_LT_QUANTITY, ");
			sbValuesQuery.append(oNewRatingDto.getQuantity() + ", ");
			
			/* **********************  Fields are for List price Search ****************************** */
			sbFieldQuery.append("QRD_LT_PRODLINEID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtProdLineId() + "', ");
			sbFieldQuery.append("QRD_LT_KW, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtKWId() + "', ");
			sbFieldQuery.append("QRD_LT_POLE, ");
			sbValuesQuery.append(Integer.parseInt(oNewRatingDto.getLtPoleId()) + ", ");
			sbFieldQuery.append("QRD_LT_FRAME, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtFrameId() + "', ");
			sbFieldQuery.append("QRD_LT_FRAME_SUFFIX, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtFrameSuffixId() + "', ");
			sbFieldQuery.append("QRD_LT_MOUNTINGID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtMountingId() + "', ");
			sbFieldQuery.append("QRD_LT_TBPOSITIONID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtTBPositionId() + "', ");
			/* **********************  Fields are for List price Search ****************************** */
			
			
			sbFieldQuery.append("QRD_LT_TAG_NUMBER, ");
			sbValuesQuery.append("'"+oNewRatingDto.getTagNumber() + "', ");
			sbFieldQuery.append("QRD_LT_MFGLOCATION, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtManufacturingLocation() + "', ");
			sbFieldQuery.append("QRD_LT_EFFICIENCYCLASS, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtEffClass() + "', ");
			sbFieldQuery.append("QRD_LT_VOLTAGEID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtVoltId() + "', ");
			sbFieldQuery.append("QRD_LT_VOLTADDVARIATION, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtVoltAddVariation() + "', ");
			sbFieldQuery.append("QRD_LT_VOLTREMOVEVARIATION, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtVoltRemoveVariation() + "', ");
			sbFieldQuery.append("QRD_LT_FREQUENCYID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtFreqId() + "', ");
			sbFieldQuery.append("QRD_LT_FREQADDVARIATION, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtFreqAddVariation() + "', ");
			sbFieldQuery.append("QRD_LT_FREQREMOVEVARIATION, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtFreqRemoveVariation() + "', ");
			sbFieldQuery.append("QRD_LT_COMBINEDVARIATION, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtCombVariation() + "', ");
			sbFieldQuery.append("QRD_LT_SERVICEFACTOR, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtServiceFactor() + "', ");
			sbFieldQuery.append("QRD_LT_METHODOFSTARTINGID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtMethodOfStarting() + "', ");
			sbFieldQuery.append("QRD_LT_STARTINGCURRENTDOLID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtStartingCurrentOnDOLStart() + "', ");
			sbFieldQuery.append("QRD_LT_AMBTEMPADDID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtAmbTempId() + "', ");
			sbFieldQuery.append("QRD_LT_AMBTEMPREMOVEID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtAmbTempRemoveId() + "', ");
			sbFieldQuery.append("QRD_LT_TEMPRISEID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtTempRise() + "', ");
			sbFieldQuery.append("QRD_LT_INSULATIONCLASSID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtInsulationClassId() + "', ");
			sbFieldQuery.append("QRD_LT_METHODOFCOOLING, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtForcedCoolingId() + "', ");
			sbFieldQuery.append("QRD_LT_ALTITUDE, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtAltitude() + "', ");
			sbFieldQuery.append("QRD_LT_HAZARDAREAID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtHazardAreaId() + "', ");
			sbFieldQuery.append("QRD_LT_HAZARDAREATYPEID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtHazardAreaTypeId() + "', ");
			sbFieldQuery.append("QRD_LT_GASGROUPID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtGasGroupId() + "', ");
			sbFieldQuery.append("QRD_LT_HAZARDZONEID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtHazardZoneId() + "', ");
			sbFieldQuery.append("QRD_LT_DUSTGROUPID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtDustGroupId() + "', ");
			sbFieldQuery.append("QRD_LT_TEMPCLASSID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtTempClass() + "', ");
			sbFieldQuery.append("QRD_LT_APPLICATIONID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtApplicationId() + "', ");
			sbFieldQuery.append("QRD_LT_DUTYID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtDutyId() + "', ");
			sbFieldQuery.append("QRD_LT_CDFID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtCDFId() + "', ");
			sbFieldQuery.append("QRD_LT_STARTSID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtStartsId() + "', ");
			sbFieldQuery.append("QRD_LT_NOISELEVEL, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtNoiseLevel() + "', ");
			
			
			/* **********************************************	Price Fields	************************************************  */
			sbFieldQuery.append("QRD_LT_TOTALADDONPERCENT, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtTotalAddonPercent() + "', ");
			sbFieldQuery.append("QRD_LT_TOTALCASHEXTRA, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtTotalCashExtra() + "', ");
			sbFieldQuery.append("QRD_LT_LISTPRICE, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtLPPerUnit() + "', ");
			sbFieldQuery.append("QRD_LT_LISTPRICEWITHADDON, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtLPWithAddonPerUnit() + "', ");
			sbFieldQuery.append("QRD_LT_STANDARDDISCOUNT, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtStandardDiscount() + "', ");
			sbFieldQuery.append("QRD_LT_PRICEPERUNIT, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtPricePerUnit() + "', ");
			sbFieldQuery.append("QRD_LT_TOTALORDERPRICE, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtTotalOrderPrice() + "', ");
			sbFieldQuery.append("QRD_LT_MATERIALCOST, ");
			sbValuesQuery.append("'"+oNewRatingDto.getRatingMaterialCost() + "', ");
			sbFieldQuery.append("QRD_LT_LOH, ");
			sbValuesQuery.append("'"+oNewRatingDto.getRatingLOH() + "', ");
			sbFieldQuery.append("QRD_LT_TOTALCOST, ");
			sbValuesQuery.append("'"+oNewRatingDto.getRatingTotalCost() + "', ");
			sbFieldQuery.append("QRD_LT_PROFITMARGIN, ");
			sbValuesQuery.append("'"+oNewRatingDto.getRatingProfitMargin() + "', ");
			/* **********************************************	Price Fields	************************************************  */
			
			/*
			sbFieldQuery.append("QRD_LT_REQUESTEDDISCOUNT_SM, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtRequestedDiscount_SM() + "', ");
			sbFieldQuery.append("QRD_LT_REQ_PRICEPERUNIT_SM, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtRequestedPricePerUnit_SM() + "', ");
			sbFieldQuery.append("QRD_LT_REQ_TOTALPRICE_SM, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtRequestedTotalPrice_SM() + "', ");
			sbFieldQuery.append("QRD_LT_APPROVEDDISCOUNT_RSM, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtApprovedDiscount_RSM() + "', ");
			sbFieldQuery.append("QRD_LT_APPRV_PRICEPERUNIT_RSM, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtApprovedPricePerunit_RSM() + "', ");
			sbFieldQuery.append("QRD_LT_APPRV_TOTALPRICE_RSM, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtApprovedTotalPrice_RSM() + "', ");
			sbFieldQuery.append("QRD_LT_QUOTEDDISCOUNT, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtQuotedDiscount() + "', ");
			sbFieldQuery.append("QRD_LT_QUOTED_PRICEPERUNIT, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtQuotedPricePerUnit() + "', ");
			sbFieldQuery.append("QRD_LT_QUOTED_TOTALPRICE, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtQuotedTotalPrice() + "', ");
			*/
			
			/* **********************************************	Standard & Customer Req. Delivery Fields	********************  */
			sbFieldQuery.append("QRD_LT_EARLIER_SUPPLIED_MOTOR, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtEarlierMotorSerialNo() + "', ");
			sbFieldQuery.append("QRD_LT_REPLACEMENTMOTOR, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtReplacementMotor() + "', ");
			sbFieldQuery.append("QRD_LT_STANDARD_DELIVERY, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtStandardDeliveryWks() + "', ");
			sbFieldQuery.append("QRD_LT_CUSTOMER_REQ_DELIVERY, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtCustReqDeliveryWks() + "', ");
			/* **********************************************	Standard & Customer Req. Delivery Fields	********************  */
			
			/* **********************************************	Electrical Fields	********************************************  */
			sbFieldQuery.append("QRD_LT_RV, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtRVId() + "', ");
			sbFieldQuery.append("QRD_LT_RA, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtRAId() + "', ");
			sbFieldQuery.append("QRD_LT_WINDINGCONFIGID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtWindingConfig() + "', ");
			sbFieldQuery.append("QRD_LT_WINDINGTREATMENTID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtWindingTreatmentId() + "', ");
			/*sbFieldQuery.append("QRD_LT_WINDINGWIREID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtWindingWire() + "', ");*/
			sbFieldQuery.append("QRD_LT_LEADID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtLeadId() + "', ");
			sbFieldQuery.append("QRD_LT_LOADGD2, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtLoadGD2Value() + "', ");
			sbFieldQuery.append("QRD_LT_VFDTYPEID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtVFDApplTypeId() + "', ");
			sbFieldQuery.append("QRD_LT_VFDSPEEDMINID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtVFDMotorSpeedRangeMin() + "', ");
			sbFieldQuery.append("QRD_LT_VFDSPEEDMAXID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtVFDMotorSpeedRangeMax() + "', ");
			sbFieldQuery.append("QRD_LT_OVERLOADDUTYID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtOverloadingDuty() + "', ");
			sbFieldQuery.append("QRD_LT_CONSTEFFRANGEID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtConstantEfficiencyRange() + "', ");
			/* **********************************************	Electrical Fields	********************************************  */
			
			/* **********************************************	Mechanical Fields	********************************************  */
			sbFieldQuery.append("QRD_LT_SHAFTTYPEID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtShaftTypeId() + "', ");
			sbFieldQuery.append("QRD_LT_SHAFTMATERIALID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtShaftMaterialId() + "', ");
			sbFieldQuery.append("QRD_LT_ROTATIONDIRECTIONID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtDirectionOfRotation() + "', ");
			sbFieldQuery.append("QRD_LT_STANDARDROTATION, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtStandardRotation() + "', ");
			sbFieldQuery.append("QRD_LT_METHODOFCOUPLINGID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtMethodOfCoupling() + "', ");
			sbFieldQuery.append("QRD_LT_PAINTTYPEID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtPaintingTypeId() + "', ");
			sbFieldQuery.append("QRD_LT_PAINTSHADEID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtPaintShadeId() + "', ");
			sbFieldQuery.append("QRD_LT_PAINTSHADE_VALUE, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtPaintShadeValue() + "', ");
			sbFieldQuery.append("QRD_LT_PAINTTHICKESSID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtPaintThicknessId() + "', ");
			sbFieldQuery.append("QRD_LT_IPID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtIPId() + "', ");
			sbFieldQuery.append("QRD_LT_CABLESIZEID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtCableSizeId() + "', ");
			sbFieldQuery.append("QRD_LT_NOOFRUNSID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtNoOfRuns() + "', ");
			sbFieldQuery.append("QRD_LT_NOOFCORESID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtNoOfCores() + "', ");
			sbFieldQuery.append("QRD_LT_CROSSSECTIONID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtCrossSectionAreaId() + "', ");
			sbFieldQuery.append("QRD_LT_CONDMATERIALID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtConductorMaterialId() + "', ");
			sbFieldQuery.append("QRD_LT_CABLEDIAMETER, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtCableDiameter() + "', ");
			sbFieldQuery.append("QRD_LT_TERMINALBOXSIZEID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtTerminalBoxSizeId() + "', ");
			sbFieldQuery.append("QRD_LT_SPREADERBOX, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtSpreaderBoxId() + "', ");
			sbFieldQuery.append("QRD_LT_SPACEHEATER, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtSpaceHeaterId() + "', ");
			sbFieldQuery.append("QRD_LT_VIBRATION, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtVibrationId() + "', ");
			sbFieldQuery.append("QRD_LT_FLYINGLEADWITHOUTTB, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtFlyingLeadWithoutTDId() + "', ");
			sbFieldQuery.append("QRD_LT_FLYINGLEADWITHTB, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtFlyingLeadWithTDId() + "', ");
			sbFieldQuery.append("QRD_LT_METALFAN, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtMetalFanId() + "', ");
			sbFieldQuery.append("QRD_LT_TECHOMOUNTING, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtTechoMounting() + "', ");
			sbFieldQuery.append("QRD_LT_SHAFTGROUNDING, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtShaftGroundingId() + "', ");
			/* **********************************************	Mechanical Fields	********************************************  */
			
			/* **********************************************	Accessories Fields	********************************************  */
			sbFieldQuery.append("QRD_LT_HARDWARE, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtHardware() + "', ");
			sbFieldQuery.append("QRD_LT_GLANDPLATE, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtGlandPlateId() + "', ");
			sbFieldQuery.append("QRD_LT_DOUBLECOMPRESSION, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtDoubleCompressionGlandId() + "', ");
			sbFieldQuery.append("QRD_LT_SPMMOUNTING, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtSPMMountingProvisionId() + "', ");
			sbFieldQuery.append("QRD_LT_ADDLNAMEPLATE, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtAddNamePlateId() + "', ");
			sbFieldQuery.append("QRD_LT_DIRECTIONARROWPLATE, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtDirectionArrowPlateId() + "', ");
			sbFieldQuery.append("QRD_LT_RTDID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtRTDId() + "', ");
			sbFieldQuery.append("QRD_LT_BTDID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtBTDId() + "', ");
			sbFieldQuery.append("QRD_LT_THERMISTERID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtThermisterId() + "', ");
			sbFieldQuery.append("QRD_LT_AUXTERMINALBOX, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtAuxTerminalBoxId() + "', ");
			sbFieldQuery.append("QRD_LT_CABLESEALINGBOX, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtCableSealingBoxId() + "', ");
			/* **********************************************	Accessories Fields	********************************************  */
			
			/* **********************************************	Bearings Fields	************************************************  */
			sbFieldQuery.append("QRD_LT_BEARINGSYSTEMID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtBearingSystemId() + "', ");
			sbFieldQuery.append("QRD_LT_BEARINGNDEID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtBearingNDEId() + "', ");
			sbFieldQuery.append("QRD_LT_BEARINGDEID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtBearingDEId() + "', ");
			/* **********************************************	Bearings Fields	************************************************  */
			
			/* **********************************************	Certificate, Spares, Tests Fields	****************************  */
			sbFieldQuery.append("QRD_LT_WITNESSROUTINE, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtWitnessRoutineId() + "', ");
			sbFieldQuery.append("QRD_LT_ADDL_TEST1, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtAdditionalTest1Id() + "', ");
			sbFieldQuery.append("QRD_LT_ADDL_TEST2, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtAdditionalTest2Id() + "', ");
			sbFieldQuery.append("QRD_LT_ADDL_TEST3, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtAdditionalTest3Id() + "', ");
			sbFieldQuery.append("QRD_LT_TYPETEST, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtTypeTestId() + "', ");
			sbFieldQuery.append("QRD_LT_CERTIFICATION, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtULCEId() + "', ");
			sbFieldQuery.append("QRD_LT_QAPID, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtQAPId() + "', ");
			sbFieldQuery.append("QRD_LT_SPARES1, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtSpares1() + "', ");
			sbFieldQuery.append("QRD_LT_SPARES2, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtSpares2() + "', ");
			sbFieldQuery.append("QRD_LT_SPARES3, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtSpares3() + "', ");
			sbFieldQuery.append("QRD_LT_SPARES4, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtSpares4() + "', ");
			sbFieldQuery.append("QRD_LT_SPARES5, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtSpares5() + "', ");
			sbFieldQuery.append("QRD_LT_DATASHEET, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtDataSheet() + "', ");
			sbFieldQuery.append("QRD_LT_MOTORGA, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtMotorGA() + "', ");
			sbFieldQuery.append("QRD_LT_PERFCURVE, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtPerfCurves() + "', ");
			sbFieldQuery.append("QRD_LT_TBOXGA, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtTBoxGA() + "', ");
			sbFieldQuery.append("QRD_LT_ADDL_COMMENTS, ");
			sbValuesQuery.append("'"+oNewRatingDto.getLtAdditionalComments() + "', ");
			/* **********************************************	Certificate, Spares, Tests Fields	****************************  */
			
			/* **********************************************   Flags for Design page ******************************************  */
			sbFieldQuery.append("FLG_PRICEFIELDS_MTO, ");
			sbValuesQuery.append("'"+oNewRatingDto.getFlg_PriceFields_MTO() + "', ");
			sbFieldQuery.append("QRD_LT_ISREFERDESIGN, ");
			sbValuesQuery.append("'"+oNewRatingDto.getIsReferDesign() + "', ");
			sbFieldQuery.append("QRD_LT_ISDESIGNCOMPLETE, ");
			sbValuesQuery.append("'"+oNewRatingDto.getIsDesignComplete() + "', ");
			/* **********************************************   Flags for Design page ******************************************  */
			
			if (sbFieldQuery.toString().trim().endsWith(","))
				sbFieldQuery = new StringBuffer(sbFieldQuery.toString().trim().substring(0, sbFieldQuery.toString().trim().lastIndexOf(",")));

			if (sbValuesQuery.toString().trim().endsWith(","))
				sbValuesQuery = new StringBuffer(sbValuesQuery.toString().trim().substring(0, sbValuesQuery.toString().trim().lastIndexOf(",")));

			sbFieldQuery.append(")");
			sbValuesQuery.append(")");

			sInsertQuery = sbFieldQuery.toString() + sbValuesQuery.toString();
		}
		//System.out.println("sbFieldQuery ...... " + sbFieldQuery);
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "sbFieldQuery ...... " + sbFieldQuery);
		//System.out.println("sbValuesQuery ...... " + sbValuesQuery);
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "sbValuesQuery ...... " + sbValuesQuery);
		//System.out.println("Final Query -  " + sInsertQuery);
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "Final Query -  " + sInsertQuery);
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");

		return sInsertQuery;
	}
	
	public NewEnquiryDto getNewEnquiryStatus(Connection conn, NewEnquiryDto oNewEnquiryDto)throws Exception{
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        try {        	
        	pstmt = conn.prepareStatement(FETCH_ENQUIRY_STATUS);
        	pstmt.setInt(1, oNewEnquiryDto.getEnquiryId());
        	rs = pstmt.executeQuery();
        	if (rs.next()) {
        		oNewEnquiryDto.setEnquiryId(rs.getInt("QEM_ENQUIRYID"));
        		oNewEnquiryDto.setEnquiryNumber(rs.getString("QEM_ENQUIRYNO"));
        		oNewEnquiryDto.setStatusId(rs.getInt("QEM_STATUSID"));  	
            }
        } finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
		return oNewEnquiryDto;
	}
	
	@Override
	public String removeLastRatingFromNewEnquiry(Connection conn, int iEnquiryId, int iRemoveRatingNo) throws Exception {
		String sMethodName = "removeLastRatingFromNewEnquiry";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null, pstmt1 = null;
		/* ResultSet object to store the rows retrieved from database */
		ResultSet rs = null;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();
		
		String sQuery = "";
		String sResult = QuotationConstants.QUOTATION_STRING_N;
		
		try {
			// 1.
			pstmt = conn.prepareStatement(DELETE_NEWRATING_DETAILS);
        	pstmt.setInt(1, iRemoveRatingNo);
        	pstmt.setInt(2, iEnquiryId);
        	
        	int iValue = pstmt.executeUpdate();
        	
        	if(iValue == 1) {
        		// 2.
        		oDBUtility.releaseResources(pstmt);
            	sQuery = "SELECT * FROM " + SCHEMA_QUOTATION + ".RFQ_LT_MTOADDON_DETAILS WHERE QRD_LT_MTOENQUIRYID = ? AND QRD_LT_MTORATINGNO = ? ";
            	pstmt = conn.prepareStatement(sQuery);
            	pstmt.setInt(1, iEnquiryId);
            	pstmt.setInt(2, iRemoveRatingNo);
            	rs = pstmt.executeQuery();
        		if (rs.next()) {
        			pstmt1 = conn.prepareStatement(DELETE_NEWMTOADDON_DETAILS);
        			pstmt1.setInt(1, iEnquiryId);
                	pstmt1.setInt(2, iRemoveRatingNo);
                	pstmt1.executeUpdate();
        		}
        		
        		// 3.
        		oDBUtility.releaseResources(rs, pstmt);
        		oDBUtility.releaseResources(pstmt1);
        		sQuery = "SELECT * FROM " + SCHEMA_QUOTATION + ".RFQ_LT_MTORATING_DETAILS WHERE QRD_LT_MTOENQUIRYID = ? AND QRD_LT_MTORATINGNO = ? ";
        		pstmt = conn.prepareStatement(sQuery);
            	pstmt.setInt(1, iEnquiryId);
            	pstmt.setInt(2, iRemoveRatingNo);
            	rs = pstmt.executeQuery();
        		if (rs.next()) {
        			pstmt1 = conn.prepareStatement(DELETE_NEWMTORATING_DETAILS);
        			pstmt1.setInt(1, iEnquiryId);
                	pstmt1.setInt(2, iRemoveRatingNo);
                	pstmt1.executeUpdate();
        		}
        		
        		// Set DELETE Result = 'Y'
        		sResult = QuotationConstants.QUOTATION_STRING_Y;
        	}
        	
        	
		} finally {
			oDBUtility.releaseResources(rs, pstmt);
			oDBUtility.releaseResources(pstmt1);
		}
		
		return sResult;
	}
	
	
	public ArrayList<NewRatingDto> fetchNewRatingDetails(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception {
		String sMethodName = "fetchNewRatingDetails";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        /* ArrayList to Fetch NewRatingDto */ 
    	ArrayList<NewRatingDto> alRatingsList = new ArrayList<NewRatingDto>();
    	NewRatingDto oTempNewRatingDto = null;
    	
    	try {
    		int iEnquiryId = oNewEnquiryDto.getEnquiryId();
    		
    		pstmt = conn.prepareStatement(GET_NEWRATING_DETAILS);
    		pstmt.setInt(1, iEnquiryId);
    		rs = pstmt.executeQuery();
    		
    		while(rs.next()) {
    			oTempNewRatingDto = new NewRatingDto();
    			
    			oTempNewRatingDto.setRatingId(rs.getInt("QRD_LT_RATINGID"));
    			oTempNewRatingDto.setEnquiryId(rs.getInt("QRD_LT_ENQUIRYID"));
    			oTempNewRatingDto.setRatingNo(rs.getInt("QRD_LT_RATINGNO"));
    			oTempNewRatingDto.setQuantity(rs.getInt("QRD_LT_QUANTITY"));
    			
    			//oTempNewRatingDto.setLtProdGroupId(rs.getString("QRD_LT_PRODGROUPID"));
    			oTempNewRatingDto.setLtProdLineId(rs.getString("QRD_LT_PRODLINEID"));
    			//oTempNewRatingDto.setLtMotorTypeId(rs.getString("QRD_LT_MOTORTYPEID"));
    			oTempNewRatingDto.setLtKWId(rs.getString("QRD_LT_KW"));
    			oTempNewRatingDto.setLtPoleId(String.valueOf(rs.getInt("QRD_LT_POLE")));
    			oTempNewRatingDto.setLtFrameId(rs.getString("QRD_LT_FRAME"));
    			oTempNewRatingDto.setLtFrameSuffixId(rs.getString("QRD_LT_FRAME_SUFFIX"));
    			oTempNewRatingDto.setLtMountingId(rs.getString("QRD_LT_MOUNTINGID"));
    			oTempNewRatingDto.setLtTBPositionId(rs.getString("QRD_LT_TBPOSITIONID"));
    			
    			oTempNewRatingDto.setTagNumber(rs.getString("QRD_LT_TAG_NUMBER"));
    			oTempNewRatingDto.setLtManufacturingLocation(rs.getString("QRD_LT_MFGLOCATION"));
    			oTempNewRatingDto.setLtEffClass(rs.getString("QRD_LT_EFFICIENCYCLASS"));
    			oTempNewRatingDto.setLtVoltId(rs.getString("QRD_LT_VOLTAGEID"));
    			oTempNewRatingDto.setLtVoltAddVariation(rs.getString("QRD_LT_VOLTADDVARIATION"));
    			oTempNewRatingDto.setLtVoltRemoveVariation(rs.getString("QRD_LT_VOLTREMOVEVARIATION"));
    			oTempNewRatingDto.setLtFreqId(rs.getString("QRD_LT_FREQUENCYID"));
    			oTempNewRatingDto.setLtFreqAddVariation(rs.getString("QRD_LT_FREQADDVARIATION"));
    			oTempNewRatingDto.setLtFreqRemoveVariation(rs.getString("QRD_LT_FREQREMOVEVARIATION"));
    			oTempNewRatingDto.setLtCombVariation(rs.getString("QRD_LT_COMBINEDVARIATION"));
    			oTempNewRatingDto.setLtServiceFactor(rs.getString("QRD_LT_SERVICEFACTOR"));
    			oTempNewRatingDto.setLtMethodOfStarting(rs.getString("QRD_LT_METHODOFSTARTINGID"));
    			oTempNewRatingDto.setLtStartingCurrentOnDOLStart(rs.getString("QRD_LT_STARTINGCURRENTDOLID"));
    			oTempNewRatingDto.setLtAmbTempId(rs.getString("QRD_LT_AMBTEMPADDID"));
    			oTempNewRatingDto.setLtAmbTempRemoveId(rs.getString("QRD_LT_AMBTEMPREMOVEID"));
    			oTempNewRatingDto.setLtTempRise(rs.getString("QRD_LT_TEMPRISEID"));
    			oTempNewRatingDto.setLtInsulationClassId(rs.getString("QRD_LT_INSULATIONCLASSID"));
    			oTempNewRatingDto.setLtForcedCoolingId(rs.getString("QRD_LT_METHODOFCOOLING"));
    			oTempNewRatingDto.setLtAltitude(rs.getString("QRD_LT_ALTITUDE"));
    			oTempNewRatingDto.setLtHazardAreaId(rs.getString("QRD_LT_HAZARDAREAID"));
    			oTempNewRatingDto.setLtHazardAreaTypeId(rs.getString("QRD_LT_HAZARDAREATYPEID"));
    			oTempNewRatingDto.setLtGasGroupId(rs.getString("QRD_LT_GASGROUPID"));
    			oTempNewRatingDto.setLtHazardZoneId(rs.getString("QRD_LT_HAZARDZONEID"));
    			oTempNewRatingDto.setLtDustGroupId(rs.getString("QRD_LT_DUSTGROUPID"));
    			oTempNewRatingDto.setLtTempClass(rs.getString("QRD_LT_TEMPCLASSID"));
    			oTempNewRatingDto.setLtApplicationId(rs.getString("QRD_LT_APPLICATIONID"));
    			oTempNewRatingDto.setLtDutyId(rs.getString("QRD_LT_DUTYID"));
    			oTempNewRatingDto.setLtCDFId(rs.getString("QRD_LT_CDFID"));
    			oTempNewRatingDto.setLtStartsId(rs.getString("QRD_LT_STARTSID"));
    			oTempNewRatingDto.setLtNoiseLevel(rs.getString("QRD_LT_NOISELEVEL"));
    			
    			/* **********************************************	Price Fields	************************************************  */
    			oTempNewRatingDto.setLtTotalAddonPercent(rs.getString("QRD_LT_TOTALADDONPERCENT"));
    			oTempNewRatingDto.setLtTotalCashExtra(rs.getString("QRD_LT_TOTALCASHEXTRA"));
    			oTempNewRatingDto.setLtLPPerUnit(rs.getString("QRD_LT_LISTPRICE"));
    			oTempNewRatingDto.setLtLPWithAddonPerUnit(rs.getString("QRD_LT_LISTPRICEWITHADDON"));
    			oTempNewRatingDto.setLtStandardDiscount(rs.getString("QRD_LT_STANDARDDISCOUNT"));
    			oTempNewRatingDto.setLtPricePerUnit(rs.getString("QRD_LT_PRICEPERUNIT"));
    			oTempNewRatingDto.setLtTotalOrderPrice(rs.getString("QRD_LT_TOTALORDERPRICE"));
    			oTempNewRatingDto.setLtRequestedDiscount_SM(rs.getString("QRD_LT_REQUESTEDDISCOUNT_SM"));
    			oTempNewRatingDto.setLtRequestedPricePerUnit_SM(rs.getString("QRD_LT_REQ_PRICEPERUNIT_SM"));
    			oTempNewRatingDto.setLtRequestedTotalPrice_SM(rs.getString("QRD_LT_REQ_TOTALPRICE_SM"));
    			oTempNewRatingDto.setLtRequestedDiscount_RSM(rs.getString("QRD_REQUESTEDDISCOUNT_RSM"));
    			oTempNewRatingDto.setLtRequestedPricePerUnit_RSM(rs.getString("QRD_REQ_PRICEPERUNIT_RSM"));
    			oTempNewRatingDto.setLtRequestedTotalPrice_RSM(rs.getString("QRD_RED_TOTALPRICE_RSM"));
    			oTempNewRatingDto.setLtRequestedDiscount_NSM(rs.getString("QRD_REQUESTEDDISCOUNT_NSM"));
    			oTempNewRatingDto.setLtRequestedPricePerUnit_NSM(rs.getString("QRD_REQ_PRICEPERUNIT_NSM"));
    			oTempNewRatingDto.setLtRequestedTotalPrice_NSM(rs.getString("QRD_REQ_TOTALPRICE_NSM"));
    			oTempNewRatingDto.setLtApprovedDiscount_RSM(rs.getString("QRD_LT_APPROVEDDISCOUNT_RSM"));
    			oTempNewRatingDto.setLtApprovedPricePerunit_RSM(rs.getString("QRD_LT_APPRV_PRICEPERUNIT_RSM"));
    			oTempNewRatingDto.setLtApprovedTotalPrice_RSM(rs.getString("QRD_LT_APPRV_TOTALPRICE_RSM"));
    			oTempNewRatingDto.setLtApprovedDiscount_NSM(rs.getString("QRD_APPROVEDDISCOUNT_NSM"));
    			oTempNewRatingDto.setLtApprovedPricePerunit_NSM(rs.getString("QRD_APPRV_PRICEPERUNIT_NSM"));
    			oTempNewRatingDto.setLtApprovedTotalPrice_NSM(rs.getString("QRD_APPRV_TOTALPRICE_NSM"));
    			oTempNewRatingDto.setLtApprovedDiscount_MH(rs.getString("QRD_APPROVEDDISCOUNT_MH"));
    			oTempNewRatingDto.setLtApprovedPricePerunit_MH(rs.getString("QRD_APPRV_PRICEPERUNIT_MH"));
    			oTempNewRatingDto.setLtApprovedTotalPrice_MH(rs.getString("QRD_APPRV_TOTALPRICE_MH"));
    			oTempNewRatingDto.setLtQuotedDiscount(rs.getString("QRD_LT_QUOTEDDISCOUNT"));
    			oTempNewRatingDto.setLtQuotedPricePerUnit(rs.getString("QRD_LT_QUOTED_PRICEPERUNIT"));
    			oTempNewRatingDto.setLtQuotedTotalPrice(rs.getString("QRD_LT_QUOTED_TOTALPRICE"));
    			oTempNewRatingDto.setRatingMaterialCost(rs.getString("QRD_LT_MATERIALCOST"));
    			oTempNewRatingDto.setRatingLOH(rs.getString("QRD_LT_LOH"));
    			oTempNewRatingDto.setRatingTotalCost(rs.getString("QRD_LT_TOTALCOST"));
    			oTempNewRatingDto.setRatingProfitMargin(rs.getString("QRD_LT_PROFITMARGIN"));
    			oTempNewRatingDto.setRatingMC_ByDesign(rs.getString("QRD_LT_DM_MATCOST"));
    			oTempNewRatingDto.setRatingNSPPercent(rs.getString("QRD_LT_NSP"));
    			oTempNewRatingDto.setRatingUnitPrice_MCNSP(rs.getString("QRD_LT_UNITPRICE_MCNSP"));
    			
    			/* **********************************************	Standard & Customer Req. Delivery Fields	********************  */
    			oTempNewRatingDto.setLtEarlierMotorSerialNo(rs.getString("QRD_LT_EARLIER_SUPPLIED_MOTOR"));
    			oTempNewRatingDto.setLtReplacementMotor(rs.getString("QRD_LT_REPLACEMENTMOTOR"));
    			oTempNewRatingDto.setLtStandardDeliveryWks(rs.getString("QRD_LT_STANDARD_DELIVERY"));
    			oTempNewRatingDto.setLtCustReqDeliveryWks(rs.getString("QRD_LT_CUSTOMER_REQ_DELIVERY"));
    			
    			/* **********************************************	Electrical Fields	********************************************  */
    			oTempNewRatingDto.setLtRVId(rs.getString("QRD_LT_RV"));
    			oTempNewRatingDto.setLtRAId(rs.getString("QRD_LT_RA"));
    			oTempNewRatingDto.setLtWindingTreatmentId(rs.getString("QRD_LT_WINDINGTREATMENTID"));
    			oTempNewRatingDto.setLtWindingWire(rs.getString("QRD_LT_WINDINGWIREID"));
    			oTempNewRatingDto.setLtLeadId(rs.getString("QRD_LT_LEADID"));
    			oTempNewRatingDto.setLtWindingConfig(rs.getString("QRD_LT_WINDINGCONFIGID"));
    			oTempNewRatingDto.setLtLoadGD2Value(rs.getString("QRD_LT_LOADGD2"));
    			oTempNewRatingDto.setLtVFDApplTypeId(rs.getString("QRD_LT_VFDTYPEID"));
    			oTempNewRatingDto.setLtVFDMotorSpeedRangeMin(rs.getString("QRD_LT_VFDSPEEDMINID"));
    			oTempNewRatingDto.setLtVFDMotorSpeedRangeMax(rs.getString("QRD_LT_VFDSPEEDMAXID"));
    			oTempNewRatingDto.setLtOverloadingDuty(rs.getString("QRD_LT_OVERLOADDUTYID"));
    			oTempNewRatingDto.setLtConstantEfficiencyRange(rs.getString("QRD_LT_CONSTEFFRANGEID"));
    			
    			/* **********************************************	Mechanical Fields	********************************************  */
    			oTempNewRatingDto.setLtShaftTypeId(rs.getString("QRD_LT_SHAFTTYPEID"));
    			oTempNewRatingDto.setLtShaftMaterialId(rs.getString("QRD_LT_SHAFTMATERIALID"));
    			oTempNewRatingDto.setLtDirectionOfRotation(rs.getString("QRD_LT_ROTATIONDIRECTIONID"));
    			oTempNewRatingDto.setLtStandardRotation(rs.getString("QRD_LT_STANDARDROTATION"));
    			oTempNewRatingDto.setLtMethodOfCoupling(rs.getString("QRD_LT_METHODOFCOUPLINGID"));
    			oTempNewRatingDto.setLtPaintingTypeId(rs.getString("QRD_LT_PAINTTYPEID"));
    			oTempNewRatingDto.setLtPaintShadeId(rs.getString("QRD_LT_PAINTSHADEID"));
    			oTempNewRatingDto.setLtPaintShadeValue(rs.getString("QRD_LT_PAINTSHADE_VALUE"));
    			oTempNewRatingDto.setLtPaintThicknessId(rs.getString("QRD_LT_PAINTTHICKESSID"));
    			oTempNewRatingDto.setLtIPId(rs.getString("QRD_LT_IPID"));
    			oTempNewRatingDto.setLtCableSizeId(rs.getString("QRD_LT_CABLESIZEID"));
    			oTempNewRatingDto.setLtNoOfRuns(rs.getString("QRD_LT_NOOFRUNSID"));
    			oTempNewRatingDto.setLtNoOfCores(rs.getString("QRD_LT_NOOFCORESID"));
    			oTempNewRatingDto.setLtCrossSectionAreaId(rs.getString("QRD_LT_CROSSSECTIONID"));
    			oTempNewRatingDto.setLtConductorMaterialId(rs.getString("QRD_LT_CONDMATERIALID"));
    			oTempNewRatingDto.setLtCableDiameter(rs.getString("QRD_LT_CABLEDIAMETER"));
    			oTempNewRatingDto.setLtTerminalBoxSizeId(rs.getString("QRD_LT_TERMINALBOXSIZEID"));
    			oTempNewRatingDto.setLtSpreaderBoxId(rs.getString("QRD_LT_SPREADERBOX"));
    			oTempNewRatingDto.setLtSpaceHeaterId(rs.getString("QRD_LT_SPACEHEATER"));
    			oTempNewRatingDto.setLtVibrationId(rs.getString("QRD_LT_VIBRATION"));
    			oTempNewRatingDto.setLtFlyingLeadWithoutTDId(rs.getString("QRD_LT_FLYINGLEADWITHOUTTB"));
    			oTempNewRatingDto.setLtFlyingLeadWithTDId(rs.getString("QRD_LT_FLYINGLEADWITHTB"));
    			oTempNewRatingDto.setLtMetalFanId(rs.getString("QRD_LT_METALFAN"));
    			oTempNewRatingDto.setLtTechoMounting(rs.getString("QRD_LT_TECHOMOUNTING"));
    			oTempNewRatingDto.setLtShaftGroundingId(rs.getString("QRD_LT_SHAFTGROUNDING"));
    			
    			/* **********************************************	Accessories Fields	********************************************  */
    			oTempNewRatingDto.setLtHardware(rs.getString("QRD_LT_HARDWARE"));
    			oTempNewRatingDto.setLtGlandPlateId(rs.getString("QRD_LT_GLANDPLATE"));
    			oTempNewRatingDto.setLtDoubleCompressionGlandId(rs.getString("QRD_LT_DOUBLECOMPRESSION"));
    			oTempNewRatingDto.setLtSPMMountingProvisionId(rs.getString("QRD_LT_SPMMOUNTING"));
    			oTempNewRatingDto.setLtAddNamePlateId(rs.getString("QRD_LT_ADDLNAMEPLATE"));
    			oTempNewRatingDto.setLtDirectionArrowPlateId(rs.getString("QRD_LT_DIRECTIONARROWPLATE"));
    			oTempNewRatingDto.setLtRTDId(rs.getString("QRD_LT_RTDID"));
    			oTempNewRatingDto.setLtBTDId(rs.getString("QRD_LT_BTDID"));
    			oTempNewRatingDto.setLtThermisterId(rs.getString("QRD_LT_THERMISTERID"));
    			oTempNewRatingDto.setLtAuxTerminalBoxId(rs.getString("QRD_LT_AUXTERMINALBOX"));
    			oTempNewRatingDto.setLtCableSealingBoxId(rs.getString("QRD_LT_CABLESEALINGBOX"));
    			
    			/* **********************************************	Bearings Fields	************************************************  */
    			oTempNewRatingDto.setLtBearingSystemId(rs.getString("QRD_LT_BEARINGSYSTEMID"));
    			oTempNewRatingDto.setLtBearingNDEId(rs.getString("QRD_LT_BEARINGNDEID"));
    			oTempNewRatingDto.setLtBearingDEId(rs.getString("QRD_LT_BEARINGDEID"));
    			
    			/* **********************************************	Certificate, Spares, Tests Fields	****************************  */
    			oTempNewRatingDto.setLtWitnessRoutineId(rs.getString("QRD_LT_WITNESSROUTINE"));
    			oTempNewRatingDto.setLtAdditionalTest1Id(rs.getString("QRD_LT_ADDL_TEST1"));
    			oTempNewRatingDto.setLtAdditionalTest2Id(rs.getString("QRD_LT_ADDL_TEST2"));
    			oTempNewRatingDto.setLtAdditionalTest3Id(rs.getString("QRD_LT_ADDL_TEST3"));
    			oTempNewRatingDto.setLtTypeTestId(rs.getString("QRD_LT_TYPETEST"));
    			oTempNewRatingDto.setLtULCEId(rs.getString("QRD_LT_CERTIFICATION"));
    			oTempNewRatingDto.setLtQAPId(rs.getString("QRD_LT_QAPID"));
    			oTempNewRatingDto.setLtSpares1(rs.getString("QRD_LT_SPARES1"));
    			oTempNewRatingDto.setLtSpares2(rs.getString("QRD_LT_SPARES2"));
    			oTempNewRatingDto.setLtSpares3(rs.getString("QRD_LT_SPARES3"));
    			oTempNewRatingDto.setLtSpares4(rs.getString("QRD_LT_SPARES4"));
    			oTempNewRatingDto.setLtSpares5(rs.getString("QRD_LT_SPARES5"));
    			oTempNewRatingDto.setLtDataSheet(rs.getString("QRD_LT_DATASHEET"));
    			oTempNewRatingDto.setLtMotorGA(rs.getString("QRD_LT_MOTORGA"));
    			oTempNewRatingDto.setLtPerfCurves(rs.getString("QRD_LT_PERFCURVE"));
    			oTempNewRatingDto.setLtTBoxGA(rs.getString("QRD_LT_TBOXGA"));
    			oTempNewRatingDto.setLtAdditionalComments(rs.getString("QRD_LT_ADDL_COMMENTS"));
    			
    			
    			oTempNewRatingDto.setFlg_PriceFields_MTO(rs.getString("FLG_PRICEFIELDS_MTO"));
    			/*oTempNewRatingDto.setFlg_Addon_Leads_MTO(rs.getString("FLG_ADDON_LEADS_MTO"));
    			oTempNewRatingDto.setFlg_Addon_TBoxSize_MTO(rs.getString("FLG_ADDON_TBOXSIZE_MTO"));
    			oTempNewRatingDto.setFlg_Addon_BearingDE_MTO(rs.getString("FLG_ADDON_BEARINGDE_MTO"));
    			oTempNewRatingDto.setFlg_Addon_CableEntry_MTO(rs.getString("FLG_ADDON_CABLEENTRY_MTO"));*/
    			oTempNewRatingDto.setFlg_Txt_MotorNo_MTO(rs.getString("FLG_TXT_MOTORNO_MTO"));
    			oTempNewRatingDto.setFlg_Txt_RV_MTO(rs.getString("FLG_TXT_RV_MTO"));
    			oTempNewRatingDto.setFlg_Txt_RA_MTO(rs.getString("FLG_TXT_RA_MTO"));
    			oTempNewRatingDto.setFlg_Txt_LoadGD2_MTO(rs.getString("FLG_TXT_LOADGD2_MTO"));
    			oTempNewRatingDto.setFlg_Txt_NonSTDPaint_MTO(rs.getString("FLG_TXT_NONSTDPAINT_MTO"));
    			
    			oTempNewRatingDto.setMtoHighlightFlag(rs.getString("QRD_LT_MTOHIGHLIGHT"));
    			
    			oTempNewRatingDto.setIsReferDesign(rs.getString("QRD_LT_ISREFERDESIGN"));
    			
    			alRatingsList.add(oTempNewRatingDto);
    		}
    		
    	} finally {
    		/* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
    	}
    	return alRatingsList;
	}
	
	public ArrayList<NewRatingDto> fetchNewRatingDetailsForView(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception {
		String sMethodName = "fetchNewRatingDetailsForView";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        /* ArrayList to Fetch NewRatingDto */ 
    	ArrayList<NewRatingDto> alRatingsList = new ArrayList<NewRatingDto>();
    	NewRatingDto oTempNewRatingDto = null;
    	NewTechnicalOfferDto oNewTechnicalOfferDto = null;
    	RemarksDto oRemarksDto = null;
    	double quotedPrice = 0d, quotedPriceTotal = 0d;
    	String sQuotedPrice = "", sTotalQuotedPrice = "";
    	
    	try {
    		int iEnquiryId = oNewEnquiryDto.getEnquiryId();
    		
    		pstmt = conn.prepareStatement(GET_NEWRATING_DETAILS);
    		pstmt.setInt(1, iEnquiryId);
    		rs = pstmt.executeQuery();
    		
    		while(rs.next()) {
    			oTempNewRatingDto = new NewRatingDto();
    			
    			oTempNewRatingDto.setRatingId(rs.getInt("QRD_LT_RATINGID"));
    			oTempNewRatingDto.setEnquiryId(rs.getInt("QRD_LT_ENQUIRYID"));
    			oTempNewRatingDto.setRatingNo(rs.getInt("QRD_LT_RATINGNO"));
    			oTempNewRatingDto.setQuantity(rs.getInt("QRD_LT_QUANTITY"));
    			
    			//oTempNewRatingDto.setLtProdGroupId(rs.getString("QRD_LT_PRODGROUPDESC"));
    			oTempNewRatingDto.setLtProdLineId(rs.getString("QRD_LT_PRODLINEDESC"));
    			//oTempNewRatingDto.setLtMotorTypeId(rs.getString("QRD_LT_MOTORTYPEDESC"));
    			oTempNewRatingDto.setLtKWId(rs.getString("QRD_LT_KWDESC"));
    			oTempNewRatingDto.setLtPoleId(rs.getString("QRD_LT_POLEDESC"));
    			oTempNewRatingDto.setLtFrameId(rs.getString("QRD_LT_FRAMEDESC"));
    			oTempNewRatingDto.setLtFrameSuffixId(rs.getString("QRD_LT_FRAME_SUFFIX_DESC"));
    			oTempNewRatingDto.setLtMountingId(rs.getString("QRD_LT_MOUNTINGDESC"));
    			oTempNewRatingDto.setLtTBPositionId(rs.getString("QRD_LT_TBPOSITIONDESC"));
    			
    			//oTempNewRatingDto.setLtProdGroupIdVal(rs.getString("QRD_LT_PRODGROUPID"));
    			oTempNewRatingDto.setLtProdLineIdVal(rs.getString("QRD_LT_PRODLINEID"));
    			oTempNewRatingDto.setLtKWIdVal(rs.getString("QRD_LT_KW"));
    			oTempNewRatingDto.setLtPoleIdVal(rs.getString("QRD_LT_POLE"));
    			oTempNewRatingDto.setLtFrameIdVal(rs.getString("QRD_LT_FRAME"));
    			oTempNewRatingDto.setLtFrameSuffixIdVal(rs.getString("QRD_LT_FRAME_SUFFIX"));
    			oTempNewRatingDto.setLtMountingIdVal(rs.getString("QRD_LT_MOUNTINGID"));
    			oTempNewRatingDto.setLtTBPositionIdVal(rs.getString("QRD_LT_TBPOSITIONID"));
    			
    			oTempNewRatingDto.setTagNumber(rs.getString("QRD_LT_TAG_NUMBER"));
    			oTempNewRatingDto.setLtManufacturingLocation(rs.getString("QRD_LT_MFGLOCATIONDESC"));
    			oTempNewRatingDto.setLtEffClass(rs.getString("QRD_LT_EFFICIENCYCLASSDESC"));
    			oTempNewRatingDto.setLtVoltId(rs.getString("QRD_LT_VOLTAGEDESC"));
    			oTempNewRatingDto.setLtVoltAddVariation(rs.getString("QRD_LT_VOLTADDVARIATIONDESC"));
    			oTempNewRatingDto.setLtVoltRemoveVariation(rs.getString("QRD_LT_VOLTREMOVEVARIATIONDESC"));
    			oTempNewRatingDto.setLtFreqId(rs.getString("QRD_LT_FREQUENCYDESC"));
    			oTempNewRatingDto.setLtFreqAddVariation(rs.getString("QRD_LT_FREQADDVARIATIONDESC"));
    			oTempNewRatingDto.setLtFreqRemoveVariation(rs.getString("QRD_LT_FREQREMOVEVARIATIONDESC"));
    			oTempNewRatingDto.setLtCombVariation(rs.getString("QRD_LT_COMBINEDVARIATIONDESC"));
    			oTempNewRatingDto.setLtServiceFactor(rs.getString("QRD_LT_SERVICEFACTORDESC"));
    			oTempNewRatingDto.setLtMethodOfStarting(rs.getString("QRD_LT_METHODOFSTARTINGDESC"));
    			oTempNewRatingDto.setLtStartingCurrentOnDOLStart(rs.getString("QRD_LT_STARTINGCURRENTDOLDESC"));
    			oTempNewRatingDto.setLtAmbTempId(rs.getString("QRD_LT_AMBTEMPADDDESC"));
    			oTempNewRatingDto.setLtAmbTempRemoveId(rs.getString("QRD_LT_AMBTEMPREMOVEDESC"));
    			oTempNewRatingDto.setLtTempRise(rs.getString("QRD_LT_TEMPRISEDESC"));
    			oTempNewRatingDto.setLtInsulationClassId(rs.getString("QRD_LT_INSULATIONCLASSDESC"));
    			oTempNewRatingDto.setLtForcedCoolingId(rs.getString("QRD_LT_METHODOFCOOLINGDESC"));
    			oTempNewRatingDto.setLtAltitude(rs.getString("QRD_LT_ALTITUDE"));
    			oTempNewRatingDto.setLtHazardAreaId(rs.getString("QRD_LT_HAZARDAREADESC"));
    			oTempNewRatingDto.setLtHazardAreaTypeId(rs.getString("QRD_LT_HAZARDAREATYPEDESC"));
    			oTempNewRatingDto.setLtGasGroupId(rs.getString("QRD_LT_GASGROUPDESC"));
    			oTempNewRatingDto.setLtHazardZoneId(rs.getString("QRD_LT_HAZARDZONEDESC"));
    			oTempNewRatingDto.setLtDustGroupId(rs.getString("QRD_LT_DUSTGROUPDESC"));
    			oTempNewRatingDto.setLtTempClass(rs.getString("QRD_LT_TEMPCLASSDESC"));
    			oTempNewRatingDto.setLtApplicationId(rs.getString("QRD_LT_APPLICATIONDESC"));
    			oTempNewRatingDto.setLtDutyId(rs.getString("QRD_LT_DUTYDESC"));
    			oTempNewRatingDto.setLtCDFId(rs.getString("QRD_LT_CDFDESC"));
    			oTempNewRatingDto.setLtStartsId(rs.getString("QRD_LT_STARTSDESC"));
    			oTempNewRatingDto.setLtNoiseLevel(rs.getString("QRD_LT_NOISELEVELDESC"));
    			
    			/* **********************************************	Price Fields	************************************************  */
    			oTempNewRatingDto.setLtTotalAddonPercent(rs.getString("QRD_LT_TOTALADDONPERCENT"));
    			oTempNewRatingDto.setLtTotalCashExtra(rs.getString("QRD_LT_TOTALCASHEXTRA"));
    			oTempNewRatingDto.setLtLPPerUnit(rs.getString("QRD_LT_LISTPRICE"));
    			oTempNewRatingDto.setLtLPWithAddonPerUnit(rs.getString("QRD_LT_LISTPRICEWITHADDON"));
    			oTempNewRatingDto.setLtStandardDiscount(rs.getString("QRD_LT_STANDARDDISCOUNT"));
    			oTempNewRatingDto.setLtPricePerUnit(rs.getString("QRD_LT_PRICEPERUNIT"));
    			oTempNewRatingDto.setLtTotalOrderPrice(rs.getString("QRD_LT_TOTALORDERPRICE"));
    			oTempNewRatingDto.setLtRequestedDiscount_SM(rs.getString("QRD_LT_REQUESTEDDISCOUNT_SM"));
    			oTempNewRatingDto.setLtRequestedPricePerUnit_SM(rs.getString("QRD_LT_REQ_PRICEPERUNIT_SM"));
    			oTempNewRatingDto.setLtRequestedTotalPrice_SM(rs.getString("QRD_LT_REQ_TOTALPRICE_SM"));
    			oTempNewRatingDto.setLtRequestedDiscount_RSM(rs.getString("QRD_REQUESTEDDISCOUNT_RSM"));
    			oTempNewRatingDto.setLtRequestedPricePerUnit_RSM(rs.getString("QRD_REQ_PRICEPERUNIT_RSM"));
    			oTempNewRatingDto.setLtRequestedTotalPrice_RSM(rs.getString("QRD_RED_TOTALPRICE_RSM"));
    			oTempNewRatingDto.setLtRequestedDiscount_NSM(rs.getString("QRD_REQUESTEDDISCOUNT_NSM"));
    			oTempNewRatingDto.setLtRequestedPricePerUnit_NSM(rs.getString("QRD_REQ_PRICEPERUNIT_NSM"));
    			oTempNewRatingDto.setLtRequestedTotalPrice_NSM(rs.getString("QRD_REQ_TOTALPRICE_NSM"));
    			oTempNewRatingDto.setLtApprovedDiscount_RSM(rs.getString("QRD_LT_APPROVEDDISCOUNT_RSM"));
    			oTempNewRatingDto.setLtApprovedPricePerunit_RSM(rs.getString("QRD_LT_APPRV_PRICEPERUNIT_RSM"));
    			oTempNewRatingDto.setLtApprovedTotalPrice_RSM(rs.getString("QRD_LT_APPRV_TOTALPRICE_RSM"));
    			oTempNewRatingDto.setLtApprovedDiscount_NSM(rs.getString("QRD_APPROVEDDISCOUNT_NSM"));
    			oTempNewRatingDto.setLtApprovedPricePerunit_NSM(rs.getString("QRD_APPRV_PRICEPERUNIT_NSM"));
    			oTempNewRatingDto.setLtApprovedTotalPrice_NSM(rs.getString("QRD_APPRV_TOTALPRICE_NSM"));
    			oTempNewRatingDto.setLtApprovedDiscount_MH(rs.getString("QRD_APPROVEDDISCOUNT_MH"));
    			oTempNewRatingDto.setLtApprovedPricePerunit_MH(rs.getString("QRD_APPRV_PRICEPERUNIT_MH"));
    			oTempNewRatingDto.setLtApprovedTotalPrice_MH(rs.getString("QRD_APPRV_TOTALPRICE_MH"));
    			oTempNewRatingDto.setLtQuotedDiscount(rs.getString("QRD_LT_QUOTEDDISCOUNT"));
    			oTempNewRatingDto.setLtQuotedPricePerUnit(rs.getString("QRD_LT_QUOTED_PRICEPERUNIT"));
    			oTempNewRatingDto.setLtQuotedTotalPrice(rs.getString("QRD_LT_QUOTED_TOTALPRICE"));
    			oTempNewRatingDto.setLtLostPricePerUnit(rs.getString("QRD_LT_LOST_PRICEPERUNIT"));
    			oTempNewRatingDto.setLtLostTotalPrice(rs.getString("QRD_LT_LOST_TOTALPRICE"));
    			oTempNewRatingDto.setRatingMaterialCost(rs.getString("QRD_LT_MATERIALCOST"));
    			oTempNewRatingDto.setRatingLOH(rs.getString("QRD_LT_LOH"));
    			oTempNewRatingDto.setRatingTotalCost(rs.getString("QRD_LT_TOTALCOST"));
    			oTempNewRatingDto.setRatingProfitMargin(rs.getString("QRD_LT_PROFITMARGIN"));
    			oTempNewRatingDto.setRatingMC_ByDesign(rs.getString("QRD_LT_DM_MATCOST"));
    			oTempNewRatingDto.setRatingNSPPercent(rs.getString("QRD_LT_NSP"));
    			oTempNewRatingDto.setRatingUnitPrice_MCNSP(rs.getString("QRD_LT_UNITPRICE_MCNSP"));
    			
    			// If QuotedPricePerUnit is empty : Set LPPerUnit as QuoterPricePerUnit.
    			if(StringUtils.isBlank(oTempNewRatingDto.getLtQuotedPricePerUnit()) || QuotationConstants.QUOTATION_ZERO.equals(oTempNewRatingDto.getLtQuotedPricePerUnit().trim())) {
    				oTempNewRatingDto.setLtQuotedPricePerUnit(rs.getString("QRD_LT_PRICEPERUNIT"));
    				oTempNewRatingDto.setLtQuotedTotalPrice(rs.getString("QRD_LT_TOTALORDERPRICE"));
    			}
    			
    			/* **********************************************	Standard & Customer Req. Delivery Fields	********************  */
    			oTempNewRatingDto.setLtEarlierMotorSerialNo(rs.getString("QRD_LT_EARLIER_SUPPLIED_MOTOR"));
    			oTempNewRatingDto.setLtReplacementMotor(rs.getString("QRD_LT_REPLACEMENTMOTOR"));
    			oTempNewRatingDto.setLtStandardDeliveryWks(rs.getString("QRD_LT_STANDARD_DELIVERY"));
    			oTempNewRatingDto.setLtCustReqDeliveryWks(rs.getString("QRD_LT_CUSTOMER_REQ_DELIVERY"));
    			
    			/* **********************************************	Electrical Fields	********************************************  */
    			oTempNewRatingDto.setLtRVId(rs.getString("QRD_LT_RV"));
    			oTempNewRatingDto.setLtRAId(rs.getString("QRD_LT_RA"));
    			oTempNewRatingDto.setLtWindingTreatmentId(rs.getString("QRD_LT_WINDINGTREATMENTDESC"));
    			oTempNewRatingDto.setLtWindingWire(rs.getString("QRD_LT_WINDINGWIREDESC"));
    			oTempNewRatingDto.setLtLeadId(rs.getString("QRD_LT_LEADDESC"));
    			oTempNewRatingDto.setLtWindingConfig(rs.getString("QRD_LT_WINDINGCONFIGDESC"));
    			oTempNewRatingDto.setLtLoadGD2Value(rs.getString("QRD_LT_LOADGD2"));
    			oTempNewRatingDto.setLtVFDApplTypeId(rs.getString("QRD_LT_VFDTYPEDESC"));
    			oTempNewRatingDto.setLtVFDMotorSpeedRangeMin(rs.getString("QRD_LT_VFDSPEEDMINDESC"));
    			oTempNewRatingDto.setLtVFDMotorSpeedRangeMax(rs.getString("QRD_LT_VFDSPEEDMAXDESC"));
    			oTempNewRatingDto.setLtOverloadingDuty(rs.getString("QRD_LT_OVERLOADDUTYDESC"));
    			oTempNewRatingDto.setLtConstantEfficiencyRange(rs.getString("QRD_LT_CONSTEFFRANGEDESC"));
    			
    			/* **********************************************	Mechanical Fields	********************************************  */
    			oTempNewRatingDto.setLtShaftTypeId(rs.getString("QRD_LT_SHAFTTYPEDESC"));
    			oTempNewRatingDto.setLtShaftMaterialId(rs.getString("QRD_LT_SHAFTMATERIALDESC"));
    			oTempNewRatingDto.setLtDirectionOfRotation(rs.getString("QRD_LT_ROTATIONDIRECTIONDESC"));
    			oTempNewRatingDto.setLtStandardRotation(rs.getString("QRD_LT_STANDARDROT_DESC"));
    			oTempNewRatingDto.setLtMethodOfCoupling(rs.getString("QRD_LT_METHODOFCOUPLINGDESC"));
    			oTempNewRatingDto.setLtPaintingTypeId(rs.getString("QRD_LT_PAINTTYPEDESC"));
    			oTempNewRatingDto.setLtPaintShadeId(rs.getString("QRD_LT_PAINTSHADEDESC"));
    			oTempNewRatingDto.setLtPaintShadeValue(rs.getString("QRD_LT_PAINTSHADE_VALUE"));
    			oTempNewRatingDto.setLtPaintThicknessId(rs.getString("QRD_LT_PAINTTHICKESSDESC"));
    			oTempNewRatingDto.setLtIPId(rs.getString("QRD_LT_IPDESC"));
    			oTempNewRatingDto.setLtCableSizeId(rs.getString("QRD_LT_CABLESIZEDESC"));
    			oTempNewRatingDto.setLtNoOfRuns(rs.getString("QRD_LT_NOOFRUNSDESC"));
    			oTempNewRatingDto.setLtNoOfCores(rs.getString("QRD_LT_NOOFCORESDESC"));
    			oTempNewRatingDto.setLtCrossSectionAreaId(rs.getString("QRD_LT_CROSSSECTIONDESC"));
    			oTempNewRatingDto.setLtConductorMaterialId(rs.getString("QRD_LT_CONDMATERIALDESC"));
    			oTempNewRatingDto.setLtCableDiameter(rs.getString("QRD_LT_CABLEDIAMETER"));
    			oTempNewRatingDto.setLtTerminalBoxSizeId(rs.getString("QRD_LT_TERMINALBOXSIZEDESC"));
    			oTempNewRatingDto.setLtSpreaderBoxId(rs.getString("QRD_LT_SPREADERBOXDESC"));
    			oTempNewRatingDto.setLtSpaceHeaterId(rs.getString("QRD_LT_SPACEHEATERDESC"));
    			oTempNewRatingDto.setLtVibrationId(rs.getString("QRD_LT_VIBRATIONDESC"));
    			oTempNewRatingDto.setLtFlyingLeadWithoutTDId(rs.getString("QRD_LT_FLYINGLEADWITHOUTTBDESC"));
    			oTempNewRatingDto.setLtFlyingLeadWithTDId(rs.getString("QRD_LT_FLYINGLEADWITHTBDESC"));
    			oTempNewRatingDto.setLtMetalFanId(rs.getString("QRD_LT_METALFANDESC"));
    			oTempNewRatingDto.setLtTechoMounting(rs.getString("QRD_LT_TECHOMOUNTINGDESC"));
    			oTempNewRatingDto.setLtShaftGroundingId(rs.getString("QRD_LT_SHAFTGROUNDINGDESC"));
    			
    			/* **********************************************	Accessories Fields	********************************************  */
    			oTempNewRatingDto.setLtHardware(rs.getString("QRD_LT_HARDWAREDESC"));
    			oTempNewRatingDto.setLtGlandPlateId(rs.getString("QRD_LT_GLANDPLATEDESC"));
    			oTempNewRatingDto.setLtDoubleCompressionGlandId(rs.getString("QRD_LT_DOUBLECOMPRESSIONDESC"));
    			oTempNewRatingDto.setLtSPMMountingProvisionId(rs.getString("QRD_LT_SPMMOUNTINGDESC"));
    			oTempNewRatingDto.setLtAddNamePlateId(rs.getString("QRD_LT_ADDLNAMEPLATEDESC"));
    			oTempNewRatingDto.setLtDirectionArrowPlateId(rs.getString("QRD_LT_DIRECTIONARROWPLATEDESC"));
    			oTempNewRatingDto.setLtRTDId(rs.getString("QRD_LT_RTDDESC"));
    			oTempNewRatingDto.setLtBTDId(rs.getString("QRD_LT_BTDDESC"));
    			oTempNewRatingDto.setLtThermisterId(rs.getString("QRD_LT_THERMISTERDESC"));
    			oTempNewRatingDto.setLtAuxTerminalBoxId(rs.getString("QRD_LT_AUXTERMINALBOXDESC"));
    			oTempNewRatingDto.setLtCableSealingBoxId(rs.getString("QRD_LT_CABLESEALINGBOXDESC"));
    			
    			/* **********************************************	Bearings Fields	************************************************  */
    			oTempNewRatingDto.setLtBearingSystemId(rs.getString("QRD_LT_BEARINGSYSTEMDESC"));
    			oTempNewRatingDto.setLtBearingNDEId(rs.getString("QRD_LT_BEARINGNDEDESC"));
    			oTempNewRatingDto.setLtBearingDEId(rs.getString("QRD_LT_BEARINGDEDESC"));
    			
    			/* **********************************************	Certificate, Spares, Tests Fields	****************************  */
    			oTempNewRatingDto.setLtWitnessRoutineId(rs.getString("QRD_LT_WITNESSROUTINEDESC"));
    			oTempNewRatingDto.setLtAdditionalTest1Id(rs.getString("QRD_LT_ADDL_TEST1DESC"));
    			oTempNewRatingDto.setLtAdditionalTest2Id(rs.getString("QRD_LT_ADDL_TEST2DESC"));
    			oTempNewRatingDto.setLtAdditionalTest3Id(rs.getString("QRD_LT_ADDL_TEST3DESC"));
    			oTempNewRatingDto.setLtTypeTestId(rs.getString("QRD_LT_TYPETESTDESC"));
    			oTempNewRatingDto.setLtULCEId(rs.getString("QRD_LT_CERTIFICATIONDESC"));
    			oTempNewRatingDto.setLtQAPId(rs.getString("QRD_LT_QAPDESC"));
    			oTempNewRatingDto.setLtSpares1(rs.getString("QRD_LT_SPARES1_DESC"));
    			oTempNewRatingDto.setLtSpares2(rs.getString("QRD_LT_SPARES2_DESC"));
    			oTempNewRatingDto.setLtSpares3(rs.getString("QRD_LT_SPARES3_DESC"));
    			oTempNewRatingDto.setLtSpares4(rs.getString("QRD_LT_SPARES4_DESC"));
    			oTempNewRatingDto.setLtSpares5(rs.getString("QRD_LT_SPARES5_DESC"));
    			oTempNewRatingDto.setLtDataSheet(rs.getString("QRD_LT_DATASHEETDESC"));
    			oTempNewRatingDto.setLtMotorGA(rs.getString("QRD_LT_MOTORGA"));
    			oTempNewRatingDto.setLtPerfCurves(rs.getString("QRD_LT_PERFCURVEDESC"));
    			oTempNewRatingDto.setLtTBoxGA(rs.getString("QRD_LT_TBOXGA"));
    			oTempNewRatingDto.setLtAdditionalComments(rs.getString("QRD_LT_ADDL_COMMENTS"));
    			
    			oTempNewRatingDto.setFlg_PriceFields_MTO(rs.getString("FLG_PRICEFIELDS_MTO"));
    			/*oTempNewRatingDto.setFlg_Addon_Leads_MTO(rs.getString("FLG_ADDON_LEADS_MTO"));
    			oTempNewRatingDto.setFlg_Addon_TBoxSize_MTO(rs.getString("FLG_ADDON_TBOXSIZE_MTO"));
    			oTempNewRatingDto.setFlg_Addon_BearingDE_MTO(rs.getString("FLG_ADDON_BEARINGDE_MTO"));
    			oTempNewRatingDto.setFlg_Addon_CableEntry_MTO(rs.getString("FLG_ADDON_CABLEENTRY_MTO"));*/
    			oTempNewRatingDto.setFlg_Txt_MotorNo_MTO(rs.getString("FLG_TXT_MOTORNO_MTO"));
    			oTempNewRatingDto.setFlg_Txt_RV_MTO(rs.getString("FLG_TXT_RV_MTO"));
    			oTempNewRatingDto.setFlg_Txt_RA_MTO(rs.getString("FLG_TXT_RA_MTO"));
    			oTempNewRatingDto.setFlg_Txt_LoadGD2_MTO(rs.getString("FLG_TXT_LOADGD2_MTO"));
    			oTempNewRatingDto.setFlg_Txt_NonSTDPaint_MTO(rs.getString("FLG_TXT_NONSTDPAINT_MTO"));
    			
    			oTempNewRatingDto.setMtoHighlightFlag(rs.getString("QRD_LT_MTOHIGHLIGHT"));
    			
    			oTempNewRatingDto.setIsReferDesign(rs.getString("QRD_LT_ISREFERDESIGN"));
    			
    			// When the fetchNewRatings is invoked for EmailQuote or ViewPDF action.
    			if(oNewEnquiryDto.isPdfView()) {
    				oNewTechnicalOfferDto = buildNewPDFTechOfferDto(conn, oTempNewRatingDto);
        			
    				if(oNewTechnicalOfferDto != null) {
    					oTempNewRatingDto.setNewTechnicalOfferDto(oNewTechnicalOfferDto);
    					
    					ArrayList<KeyValueVo> alRemarksList = buildRemarksList(conn, oTempNewRatingDto);
        				oTempNewRatingDto.setAlRemarksList(alRemarksList);
    				}
    			}
    			
    			alRatingsList.add(oTempNewRatingDto);
    		}
    	} finally {
    		/* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
    	}
    	return alRatingsList;
	}
	
	@Override
	public NewTechnicalOfferDto buildNewTechnicalOfferDto(Connection conn, NewRatingDto oNewRatingDto) throws Exception {
		String sMethodName = "buildNewTechnicalOfferDto";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
		
		NewTechnicalOfferDto oNewTechnicalOfferDto = null;
		
		try {
			String sqlQuery = " SELECT * FROM " + SCHEMA_QUOTATION + ".RFQ_TECHNICAL_DATA WHERE PRODUCT_LINE = '" + oNewRatingDto.getLtProdLineId().trim() 
					+ "' AND POWER_RATING_KW = '" + oNewRatingDto.getLtKWId().trim() + "' AND FRAME_TYPE = '" + oNewRatingDto.getLtFrameId().trim()
					+ "' AND FRAME_SUFFIX = '" + oNewRatingDto.getLtFrameSuffixId().trim() + "' AND NUMBER_OF_POLES = " + Integer.parseInt(oNewRatingDto.getLtPoleId())
					+ " AND MOUNTING_TYPE = '" + oNewRatingDto.getLtMountingId().trim() + "' ";
			
			if(! QuotationConstants.QUOTATIONLT_STRING_STANDARD.equalsIgnoreCase(oNewRatingDto.getLtTBPositionId().trim())) {
				sqlQuery = sqlQuery + " AND UPPER(TB_POSITION) = UPPER('" + oNewRatingDto.getLtTBPositionId().trim() + "') ";
			}
					
					//+ "') AND VOLTAGE_V = '" + CommonUtility.fetchBaseFrameType(oNewRatingDto.getLtVoltId()) 
					//+ "' AND FREQUENCY_HZ = '" + CommonUtility.fetchBaseFrameType(oNewRatingDto.getLtFreqId()) + "' ";
			
			pstmt = conn.prepareStatement(sqlQuery);
    		
    		rs = pstmt.executeQuery();
    		
    		if(rs.next()) {
    			oNewTechnicalOfferDto = new NewTechnicalOfferDto();
    			
    			oNewTechnicalOfferDto.setProductGroup(rs.getString("PRODUCT_GROUP"));
    			oNewTechnicalOfferDto.setProductLine(rs.getString("PRODUCT_LINE"));
    			oNewTechnicalOfferDto.setPowerRating_KW(rs.getString("POWER_RATING_KW"));
    			oNewTechnicalOfferDto.setFrameType(rs.getString("FRAME_TYPE"));
    			oNewTechnicalOfferDto.setFrameSuffix(rs.getString("FRAME_SUFFIX"));
    			oNewTechnicalOfferDto.setNoOfPoles(rs.getInt("NUMBER_OF_POLES"));
    			oNewTechnicalOfferDto.setMountingType(rs.getString("MOUNTING_TYPE"));
    			oNewTechnicalOfferDto.setTbPosition(rs.getString("TB_POSITION"));
    			
    			oNewTechnicalOfferDto.setModelNumber(rs.getString("MODEL_NO"));
    			oNewTechnicalOfferDto.setVoltage(rs.getString("VOLTAGE_V"));
    			oNewTechnicalOfferDto.setVoltageAddVariation(rs.getString("VOLTAGE_ADD_VAR"));
    			oNewTechnicalOfferDto.setVoltageRemoveVariation(rs.getString("VOLTAGE_REMOVE_VAR"));
    			oNewTechnicalOfferDto.setFrequency(rs.getString("FREQUENCY_HZ"));
    			oNewTechnicalOfferDto.setFrequencyAddVariation(rs.getString("FREQUENCY_ADD_VAR"));
    			oNewTechnicalOfferDto.setFrequencyRemoveVariation(rs.getString("FREQUENCY_REMOVE_VAR"));
    			oNewTechnicalOfferDto.setCombinedVariation(rs.getString("COMBINED_VAR"));
    			oNewTechnicalOfferDto.setCoolingMethod(rs.getString("COOLING_METHOD"));
    			oNewTechnicalOfferDto.setMotorInertia(rs.getString("MOTOR_INERTIA"));
    			oNewTechnicalOfferDto.setVibration(rs.getString("VIBRATION"));
    			oNewTechnicalOfferDto.setNoise(rs.getString("NOISE"));
    			oNewTechnicalOfferDto.setNoOfStarts(rs.getString("NUMBER_OF_STARTS"));
    			oNewTechnicalOfferDto.setLrWithstandTime(rs.getString("LR_WITHSTAND_TIME"));
    			oNewTechnicalOfferDto.setDirectionOfRotation(rs.getString("DIRECTION_OD_ROTATION"));
    			oNewTechnicalOfferDto.setPowerRating_HP(rs.getString("POWER_RATING_HP"));
    			oNewTechnicalOfferDto.setCableSize(rs.getString("CABLE_SIZE"));
    			oNewTechnicalOfferDto.setCurrent_A(rs.getString("CURRENT_A"));
    			oNewTechnicalOfferDto.setMaxSpeed(rs.getString("SPEED_MAX"));
    			oNewTechnicalOfferDto.setPowerFactor_100(rs.getString("POWER_FACTOR_100"));
    			oNewTechnicalOfferDto.setPowerFactor_75(rs.getString("POWER_FACTOR_75"));
    			oNewTechnicalOfferDto.setPowerFactor_50(rs.getString("POWER_FACTOR_50"));
    			oNewTechnicalOfferDto.setEfficiencyClass(rs.getString("EFFICIENCY_CLASS"));
    			oNewTechnicalOfferDto.setEfficiency_100Above(rs.getString("EFFICIENCY_100ABOVE"));
    			oNewTechnicalOfferDto.setEfficiency_100(rs.getString("EFFICIENCY_100"));
    			oNewTechnicalOfferDto.setEfficiency_75(rs.getString("EFFICIENCY_75"));
    			oNewTechnicalOfferDto.setEfficiency_50(rs.getString("EFFICIENCY_50"));
    			oNewTechnicalOfferDto.setStartingCurrent(rs.getString("STARTING_CURRENT"));
    			oNewTechnicalOfferDto.setTorque(rs.getString("TORQUE"));
    			oNewTechnicalOfferDto.setStarting_torque(rs.getString("STARTING_TORQUE"));
    			oNewTechnicalOfferDto.setPullout_torque(rs.getString("PULLOUT_TORQUE"));
    			oNewTechnicalOfferDto.setRaValue(rs.getString("RA"));
    			oNewTechnicalOfferDto.setRvValue(rs.getString("RV"));
    			oNewTechnicalOfferDto.setHazArea(rs.getString("HAZ_AREA"));
    			oNewTechnicalOfferDto.setHazLocation(rs.getString("HAZ_LOCATION"));
    			oNewTechnicalOfferDto.setZoneClassification(rs.getString("ZONE_CLASSIFICATION"));
    			oNewTechnicalOfferDto.setGasGroup(rs.getString("GAS_GROUP"));
    			oNewTechnicalOfferDto.setTemperatureClass(rs.getString("TEMP_CLASS"));
    			oNewTechnicalOfferDto.setRotortype(rs.getString("ROTOR_TYPE"));
    			oNewTechnicalOfferDto.setElectricalType(rs.getString("ELECTRICAL_TYPE"));
    			oNewTechnicalOfferDto.setIpCode(rs.getString("IP_CODE"));
    			oNewTechnicalOfferDto.setInsulationClass(rs.getString("INSULATION_CLASS"));
    			oNewTechnicalOfferDto.setServiceFactor(rs.getString("SERVICE_FACTOR"));
    			oNewTechnicalOfferDto.setDuty(rs.getString("DUTY"));
    			oNewTechnicalOfferDto.setCdf(rs.getString("CDF"));
    			oNewTechnicalOfferDto.setNoOfStarts_HR(rs.getString("NUMBER_OF_STARTS_HR"));
    			oNewTechnicalOfferDto.setNoOfPhases(rs.getString("NUMBER_OF_PHASES"));
    			oNewTechnicalOfferDto.setMaxAmbientTemp(rs.getString("MAX_AMBIENT_C"));
    			oNewTechnicalOfferDto.setTempRise(rs.getString("TEMP_RISE"));
    			oNewTechnicalOfferDto.setAltitude(rs.getString("ALTITUDE"));
    			oNewTechnicalOfferDto.setDeBearingSize(rs.getString("DE_BEARING_SIZE"));
    			oNewTechnicalOfferDto.setDeBearingType(rs.getString("DE_BEARING_TYPE"));
    			oNewTechnicalOfferDto.setOdeBearingSize(rs.getString("ODE_BEARING_SIZE"));
    			oNewTechnicalOfferDto.setOdeBearingType(rs.getString("ODE_BEARING_TYPE"));
    			oNewTechnicalOfferDto.setEnclosureType(rs.getString("ENCLOSURE_TYPE"));
    			oNewTechnicalOfferDto.setLubMethod(rs.getString("LUB_METHOD"));
    			oNewTechnicalOfferDto.setTypeOfGrease(rs.getString("TYPE_OF_GREASE"));
    			oNewTechnicalOfferDto.setMotorOrientation(rs.getString("MOTOR_ORIENTATION"));
    			oNewTechnicalOfferDto.setFrameMaterial(rs.getString("FRAME_MATERIAL"));
    			oNewTechnicalOfferDto.setFrameLength(rs.getString("FRAME_LENGTH"));
    			oNewTechnicalOfferDto.setRotation(rs.getString("ROTATION"));
    			oNewTechnicalOfferDto.setNoOfSpeeds(rs.getString("NUMBER_OF_SPEEDS"));
    			oNewTechnicalOfferDto.setShaftType(rs.getString("SHAFT_TYPE"));
    			oNewTechnicalOfferDto.setShaftDiameter(rs.getString("SHAFT_DIAMETER_MM"));
    			oNewTechnicalOfferDto.setShaftExtension(rs.getString("SHAFT_EXTENSION_MM"));
    			oNewTechnicalOfferDto.setOutlineDrawingNum(rs.getString("OUTLINE_DWG_NO"));
    			oNewTechnicalOfferDto.setStatorConnection(rs.getString("STATOR_CONNECTION"));
    			oNewTechnicalOfferDto.setRotorConnection(rs.getString("ROTOR_CONNECTION"));
    			oNewTechnicalOfferDto.setConnDrawingNum(rs.getString("CONNECTION_DRAWING_NO"));
    			oNewTechnicalOfferDto.setOverallLengthMM(rs.getString("OVERALL_LENGTH_MM"));
    			oNewTechnicalOfferDto.setMotorWeight(rs.getString("MOTOR_WEIGHT"));
    			oNewTechnicalOfferDto.setGrossWeight(rs.getString("GROSS_WEIGHT"));
    			oNewTechnicalOfferDto.setStartingType(rs.getString("STARTING_TYPE"));
    			oNewTechnicalOfferDto.setThruBoltsExtension(rs.getString("THRU_BOLTS_EXTENSION"));
    			oNewTechnicalOfferDto.setOverloadProtectionType(rs.getString("TYPE_OF_OVERLOAD_PROTECTION"));
    			oNewTechnicalOfferDto.setCeValue(rs.getString("CE"));
    			oNewTechnicalOfferDto.setUlValue(rs.getString("UL"));
    			oNewTechnicalOfferDto.setCsaValue(rs.getString("CSA"));
    		}
		} finally {
    		/* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
    	}
		
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
		return oNewTechnicalOfferDto;
	}
	
	public NewTechnicalOfferDto buildNewPDFTechOfferDto(Connection conn, NewRatingDto oNewRatingDto) throws Exception {
		String sMethodName = "buildNewPDFTechOfferDto";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
		
		NewTechnicalOfferDto oNewTechnicalOfferDto = null;
		
		try {
			pstmt = conn.prepareStatement(FETCH_PDF_TECHOFFER_DETAILS);
			pstmt.setInt(1, oNewRatingDto.getEnquiryId());
			pstmt.setInt(2, oNewRatingDto.getRatingNo());
    		rs = pstmt.executeQuery();
    		
    		if(rs.next()) {
    			oNewTechnicalOfferDto = new NewTechnicalOfferDto();
    			
    			oNewTechnicalOfferDto.setEnquiryId(rs.getInt("QPD_LT_ENQUIRYID"));
    			oNewTechnicalOfferDto.setRatingNo(rs.getInt("QPD_LT_RATINGNO"));
    			oNewTechnicalOfferDto.setQuantity(rs.getInt("QPD_LT_QUANTITY"));
    			
    			oNewTechnicalOfferDto.setProductGroup(rs.getString("QPD_LT_PRODGROUPID"));
    			oNewTechnicalOfferDto.setProductLine(rs.getString("QPD_LT_PRODLINEID"));
    			oNewTechnicalOfferDto.setPowerRating_KW(rs.getString("QPD_LT_KW"));
    			oNewTechnicalOfferDto.setNoOfPoles(rs.getInt("QPD_LT_POLE"));
    			oNewTechnicalOfferDto.setFrameType(rs.getString("QPD_LT_FRAME"));
    			oNewTechnicalOfferDto.setFrameSuffix(rs.getString("QPD_LT_FRAME_SUFFIX"));
    			oNewTechnicalOfferDto.setMountingType(rs.getString("QPD_LT_MOUNTINGID"));
    			oNewTechnicalOfferDto.setTbPosition(rs.getString("QPD_LT_TBPOSITIONID"));
    			oNewTechnicalOfferDto.setIsReferDesign(rs.getString("QPD_LT_ISREFERDESIGN"));
    			oNewTechnicalOfferDto.setModelNumber(rs.getString("QPD_LT_MODELNUMBER"));
    			
    			// Top Table Fields.
    			oNewTechnicalOfferDto.setVoltage(rs.getString("QPD_LT_VOLTAGE"));
    			oNewTechnicalOfferDto.setFrequency(rs.getString("QPD_LT_FREQUENCY"));
    			oNewTechnicalOfferDto.setPowerRating_HP(rs.getString("QPD_LT_POWER_HP"));
    			oNewTechnicalOfferDto.setCurrent_A(rs.getString("QPD_LT_CURRENT"));
    			oNewTechnicalOfferDto.setMaxSpeed(rs.getString("QPD_LT_RPM"));
    			oNewTechnicalOfferDto.setTorque(rs.getString("QPD_LT_TORQUE"));
    			oNewTechnicalOfferDto.setEfficiencyClass(rs.getString("QPD_LT_IECLASS"));
    			oNewTechnicalOfferDto.setEfficiency_100Above(rs.getString("QPD_LT_LOADEFF100ABOVE"));
    			oNewTechnicalOfferDto.setEfficiency_100(rs.getString("QPD_LT_LOADEFFAT100"));
    			oNewTechnicalOfferDto.setEfficiency_75(rs.getString("QPD_LT_LOADEFFAT75"));
    			oNewTechnicalOfferDto.setEfficiency_50(rs.getString("QPD_LT_LOADEFFAT50"));
    			oNewTechnicalOfferDto.setPowerFactor_100(rs.getString("QPD_LT_LOADPFAT100"));
    			oNewTechnicalOfferDto.setPowerFactor_75(rs.getString("QPD_LT_LOADPFAT75"));
    			oNewTechnicalOfferDto.setPowerFactor_50(rs.getString("QPD_LT_LOADPFAT50"));
    			oNewTechnicalOfferDto.setStartingCurrent(rs.getString("QPD_LT_IAIN"));
    			oNewTechnicalOfferDto.setStarting_torque(rs.getString("QPD_LT_TATN"));
    			oNewTechnicalOfferDto.setPullout_torque(rs.getString("QPD_LT_TKTN"));
    			oNewTechnicalOfferDto.setRaValue(rs.getString("QPD_LT_RA"));
    			oNewTechnicalOfferDto.setRvValue(rs.getString("QPD_LT_RV"));
    			
    			// Left Side Fields.
    			oNewTechnicalOfferDto.setEnclosureType(rs.getString("QPD_LT_ENCLOSURE"));
    			oNewTechnicalOfferDto.setFrameMaterial(rs.getString("QPD_LT_FRAME_MATERIAL"));
    			oNewTechnicalOfferDto.setDuty(rs.getString("QPD_LT_DUTY"));
    			oNewTechnicalOfferDto.setVoltageAddVariation(rs.getString("QPD_LT_VOLT_ADDVAR"));
    			oNewTechnicalOfferDto.setVoltageRemoveVariation(rs.getString("QPD_LT_VOLT_REMVAR"));
    			oNewTechnicalOfferDto.setFrequencyAddVariation(rs.getString("QPD_LT_FREQ_ADDVAR"));
    			oNewTechnicalOfferDto.setFrequencyRemoveVariation(rs.getString("QPD_LT_FREQ_REMVAR"));
    			oNewTechnicalOfferDto.setCombinedVariation(rs.getString("QPD_LT_COMB_VAR"));
    			oNewTechnicalOfferDto.setDesign(rs.getString("QPD_LT_DESIGN_REF"));
    			oNewTechnicalOfferDto.setServiceFactor(rs.getString("QPD_LT_SERVICE_FACTOR"));
    			oNewTechnicalOfferDto.setInsulationClass(rs.getString("QPD_LT_INSULATIONCLASS"));
    			oNewTechnicalOfferDto.setAmbientAddTemp(rs.getString("QPD_LT_AMB_ADDTEMP"));
    			oNewTechnicalOfferDto.setAmbientRemoveTemp(rs.getString("QPD_LT_AMB_REMTEMP"));
    			oNewTechnicalOfferDto.setTempRise(rs.getString("QPD_LT_TEMPRISE"));
    			oNewTechnicalOfferDto.setAltitude(rs.getString("QPD_LT_ALTITUDE"));
    			oNewTechnicalOfferDto.setHazArea(rs.getString("QPD_LT_HAZARDAREATYPE"));
    			oNewTechnicalOfferDto.setZoneClassification(rs.getString("QPD_LT_HAZARDZONE"));
    			oNewTechnicalOfferDto.setGasGroup(rs.getString("QPD_LT_GASGROUP"));
    			oNewTechnicalOfferDto.setTemperatureClass(rs.getString("QPD_LT_TEMPCLASS"));
    			oNewTechnicalOfferDto.setRotortype(rs.getString("QPD_LT_ROTORTYPE"));
    			oNewTechnicalOfferDto.setDeBearingType(rs.getString("QPD_LT_BEARINGDE"));
    			oNewTechnicalOfferDto.setOdeBearingType(rs.getString("QPD_LT_BEARINGNDE"));
    			oNewTechnicalOfferDto.setDeBearingSize(rs.getString("QPD_LT_BEARINGDESIZE"));
    			oNewTechnicalOfferDto.setOdeBearingSize(rs.getString("QPD_LT_BEARINGNDESIZE"));
    			oNewTechnicalOfferDto.setLubMethod(rs.getString("QPD_LT_LUBRICATION"));
    			oNewTechnicalOfferDto.setTypeOfGrease(rs.getString("QPD_LT_GREASETYPE"));
    			oNewTechnicalOfferDto.setMotorWeight(rs.getString("QPD_LT_MOTORWEIGHT"));
    			oNewTechnicalOfferDto.setGrossWeight(rs.getString("QPD_LT_GROSSWEIGHT"));
    			
    			// Right Side Fields.
    			oNewTechnicalOfferDto.setStatorConnection(rs.getString("QPD_LT_STATORCONN"));
    			oNewTechnicalOfferDto.setRotorConnection(rs.getString("QPD_LT_ROTORCONN"));
    			oNewTechnicalOfferDto.setCdf(rs.getString("QPD_LT_CDF"));
    			oNewTechnicalOfferDto.setNoOfStarts_HR(rs.getString("QPD_LT_NOOFSTARTS"));
    			oNewTechnicalOfferDto.setIpCode(rs.getString("QPD_LT_IP"));
    			oNewTechnicalOfferDto.setMountingType(rs.getString("QPD_LT_MOUNTING"));
    			oNewTechnicalOfferDto.setCoolingMethod(rs.getString("QPD_LT_METHODOFCOOLING"));
    			oNewTechnicalOfferDto.setMotorInertia(rs.getString("QPD_LT_MOTORGD2"));
    			oNewTechnicalOfferDto.setLoadInertia(rs.getString("QPD_LT_LOADGD2"));
    			oNewTechnicalOfferDto.setVibration(rs.getString("QPD_LT_VIBRATIONLEVEL"));
    			oNewTechnicalOfferDto.setNoise(rs.getString("QPD_LT_NOISELEVEL"));
    			oNewTechnicalOfferDto.setNoOfStarts(rs.getString("QPD_LT_NOOFSTARTS_HOTCOLD"));
    			oNewTechnicalOfferDto.setStartingType(rs.getString("QPD_LT_STARTINGMETHOD"));
    			oNewTechnicalOfferDto.setLrWithstandTime(rs.getString("QPD_LT_LRWITHSTANDTIME"));
    			oNewTechnicalOfferDto.setDirectionOfRotation(rs.getString("QPD_LT_DIROFROTATION"));
    			oNewTechnicalOfferDto.setRotation(rs.getString("QPD_LT_STANDARDROTATION"));
    			oNewTechnicalOfferDto.setCableSize(rs.getString("QPD_LT_MAXCABLESIZE"));
    		}
		} finally {
    		/* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
    	}
		
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
		return oNewTechnicalOfferDto;
	}
	
	private ArrayList<KeyValueVo> buildRemarksList(Connection conn, NewRatingDto oNewRatingDto) throws Exception {
		String sMethodName = "buildRemarksList";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
		
        HashMap<String,String> hmAddonNameValueMap = new HashMap<>();
        ArrayList<String> alEnquiryAddOnList = new ArrayList<>();
        ArrayList<KeyValueVo> alRemarksList = new ArrayList<>();
        String sAddOnTypeString = "", sAddOnTypeName = "", sAddOnTypeValue = "", sRemarksValue = "";
        int iRemarksIndex = 1;
        try {
        	pstmt = conn.prepareStatement(GET_COMPLETE_MTO_ADDON_DETAILS_BY_RATINGNO);
        	pstmt.setInt(1, oNewRatingDto.getEnquiryId());
        	pstmt.setInt(2, oNewRatingDto.getRatingNo());
        	rs = pstmt.executeQuery();
        	while(rs.next()) {
        		sAddOnTypeString = rs.getString("QRD_LT_MTOADDONTYPE");
        		if(sAddOnTypeString.contains("-")) {
        			sAddOnTypeName = sAddOnTypeString.substring(0, sAddOnTypeString.indexOf(" -"));
        			sAddOnTypeValue = sAddOnTypeString.substring(sAddOnTypeString.indexOf("- ")+1);
        			hmAddonNameValueMap.put(sAddOnTypeName, sAddOnTypeValue);
        		}
        	}
        	oDBUtility.releaseResources(rs, pstmt);
        	
        	ArrayList<String> alPDFProdLinesList = new ArrayList<>();
        	ArrayList<KeyValueVo> arlOptions = getAscendingOptionsbyAttributeList(conn, QuotationConstants.QPDF_LT_REMARKSFIELD);
			for(KeyValueVo oKV : arlOptions) {
				alPDFProdLinesList.add(oKV.getValue());
			}
			
			// Set Add-On's and MTO's From RFQ_LT_MTOADDON_DETAILS Table.
        	Set<String> hmKeySet = hmAddonNameValueMap.keySet();
        	Iterator addOnNameItr = hmKeySet.iterator();
        	while (addOnNameItr.hasNext()) {
        		String sTempAddONVal = (String)addOnNameItr.next();
        		//System.out.println("sTempAddONVal: "+sTempAddONVal);
        		if(alPDFProdLinesList.contains(sTempAddONVal)) {
        			KeyValueVo oKV = new KeyValueVo();
        			oKV.setKey(String.valueOf(iRemarksIndex)); 
        			sRemarksValue = " " + sTempAddONVal + " : " + hmAddonNameValueMap.get(sTempAddONVal);
        			oKV.setValue(sRemarksValue);
        			alRemarksList.add(oKV);
        			iRemarksIndex++;
        		}
        	}
        	
        	// Set Additional Fields.
        	if(StringUtils.isNotBlank(oNewRatingDto.getLtTypeTestId()) && !"NA".equalsIgnoreCase(oNewRatingDto.getLtTypeTestId().trim())) {
        		KeyValueVo oKV = new KeyValueVo();
        		oKV.setKey(String.valueOf(iRemarksIndex)); 
        		sRemarksValue = " Type Test" + " : " + oNewRatingDto.getLtTypeTestId();
    			oKV.setValue(sRemarksValue);
    			iRemarksIndex++;
        	}
        	if(StringUtils.isNotBlank(oNewRatingDto.getLtQAPId()) && !"NA".equalsIgnoreCase(oNewRatingDto.getLtQAPId().trim())) {
        		KeyValueVo oKV = new KeyValueVo();
        		oKV.setKey(String.valueOf(iRemarksIndex)); 
        		sRemarksValue = " QAP" + " : " + oNewRatingDto.getLtQAPId();
    			oKV.setValue(sRemarksValue);
    			iRemarksIndex++;
        	}
        	if(StringUtils.isNotBlank(oNewRatingDto.getLtSpares1()) && !"NA".equalsIgnoreCase(oNewRatingDto.getLtSpares1().trim())) {
        		KeyValueVo oKV = new KeyValueVo();
        		oKV.setKey(String.valueOf(iRemarksIndex)); 
        		sRemarksValue = " Spares 1" + " : " + oNewRatingDto.getLtSpares1();
    			oKV.setValue(sRemarksValue);
    			iRemarksIndex++;
        	}
        	if(StringUtils.isNotBlank(oNewRatingDto.getLtSpares2()) && !"NA".equalsIgnoreCase(oNewRatingDto.getLtSpares2().trim())) {
        		KeyValueVo oKV = new KeyValueVo();
        		oKV.setKey(String.valueOf(iRemarksIndex)); 
        		sRemarksValue = " Spares 2" + " : " + oNewRatingDto.getLtSpares2();
    			oKV.setValue(sRemarksValue);
    			iRemarksIndex++;
        	}
        	if(StringUtils.isNotBlank(oNewRatingDto.getLtSpares3()) && !"NA".equalsIgnoreCase(oNewRatingDto.getLtSpares3().trim())) {
        		KeyValueVo oKV = new KeyValueVo();
        		oKV.setKey(String.valueOf(iRemarksIndex)); 
        		sRemarksValue = " Spares 3" + " : " + oNewRatingDto.getLtSpares3();
    			oKV.setValue(sRemarksValue);
    			iRemarksIndex++;
        	}
        	if(StringUtils.isNotBlank(oNewRatingDto.getLtSpares4()) && !"NA".equalsIgnoreCase(oNewRatingDto.getLtSpares4().trim())) {
        		KeyValueVo oKV = new KeyValueVo();
        		oKV.setKey(String.valueOf(iRemarksIndex)); 
        		sRemarksValue = " Spares 4" + " : " + oNewRatingDto.getLtSpares4();
    			oKV.setValue(sRemarksValue);
    			iRemarksIndex++;
        	}
        	if(StringUtils.isNotBlank(oNewRatingDto.getLtSpares5()) && !"NA".equalsIgnoreCase(oNewRatingDto.getLtSpares5().trim())) {
        		KeyValueVo oKV = new KeyValueVo();
        		oKV.setKey(String.valueOf(iRemarksIndex)); 
        		sRemarksValue = " Spares 5" + " : " + oNewRatingDto.getLtSpares5();
    			oKV.setValue(sRemarksValue);
    			iRemarksIndex++;
        	}
        	
        } catch(Exception e) {
        	e.printStackTrace();
        }
        
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
        return alRemarksList;
	}
	
	public NewRatingDto fetchRatingByRatingId(Connection conn, NewRatingDto oNewRatingDto) throws Exception {
		String sMethodName = "fetchRatingByRatingId";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        NewRatingDto oTempNewRatingDto = null;
        
        try {
        	pstmt = conn.prepareStatement(GET_NEWRATING_BY_RATINGID);
    		pstmt.setInt(1, oNewRatingDto.getRatingId());
    		rs = pstmt.executeQuery();
    		
    		if(rs.next()) {
    			oTempNewRatingDto = new NewRatingDto();
    			
    			oTempNewRatingDto.setRatingId(rs.getInt("QRD_LT_RATINGID"));
    			oTempNewRatingDto.setEnquiryId(rs.getInt("QRD_LT_ENQUIRYID"));
    			oTempNewRatingDto.setRatingNo(rs.getInt("QRD_LT_RATINGNO"));
    			oTempNewRatingDto.setQuantity(rs.getInt("QRD_LT_QUANTITY"));
    			
    			//oTempNewRatingDto.setLtProdGroupId(rs.getString("QRD_LT_PRODGROUPID"));
    			oTempNewRatingDto.setLtProdLineId(rs.getString("QRD_LT_PRODLINEID"));
    			//oTempNewRatingDto.setLtMotorTypeId(rs.getString("QRD_LT_MOTORTYPEID"));
    			oTempNewRatingDto.setLtKWId(rs.getString("QRD_LT_KW"));
    			oTempNewRatingDto.setLtPoleId(String.valueOf(rs.getInt("QRD_LT_POLE")));
    			oTempNewRatingDto.setLtFrameId(rs.getString("QRD_LT_FRAME"));
    			oTempNewRatingDto.setLtFrameSuffixId(rs.getString("QRD_LT_FRAME_SUFFIX"));
    			oTempNewRatingDto.setLtMountingId(rs.getString("QRD_LT_MOUNTINGID"));
    			oTempNewRatingDto.setLtTBPositionId(rs.getString("QRD_LT_TBPOSITIONID"));
    			
    			oTempNewRatingDto.setTagNumber(rs.getString("QRD_LT_TAG_NUMBER"));
    			oTempNewRatingDto.setLtManufacturingLocation(rs.getString("QRD_LT_MFGLOCATION"));
    			oTempNewRatingDto.setLtEffClass(rs.getString("QRD_LT_EFFICIENCYCLASS"));
    			oTempNewRatingDto.setLtVoltId(rs.getString("QRD_LT_VOLTAGEID"));
    			oTempNewRatingDto.setLtVoltAddVariation(rs.getString("QRD_LT_VOLTADDVARIATION"));
    			oTempNewRatingDto.setLtVoltRemoveVariation(rs.getString("QRD_LT_VOLTREMOVEVARIATION"));
    			oTempNewRatingDto.setLtFreqId(rs.getString("QRD_LT_FREQUENCYID"));
    			oTempNewRatingDto.setLtFreqAddVariation(rs.getString("QRD_LT_FREQADDVARIATION"));
    			oTempNewRatingDto.setLtFreqRemoveVariation(rs.getString("QRD_LT_FREQREMOVEVARIATION"));
    			oTempNewRatingDto.setLtCombVariation(rs.getString("QRD_LT_COMBINEDVARIATION"));
    			oTempNewRatingDto.setLtServiceFactor(rs.getString("QRD_LT_SERVICEFACTOR"));
    			oTempNewRatingDto.setLtMethodOfStarting(rs.getString("QRD_LT_METHODOFSTARTINGID"));
    			oTempNewRatingDto.setLtStartingCurrentOnDOLStart(rs.getString("QRD_LT_STARTINGCURRENTDOLID"));
    			oTempNewRatingDto.setLtAmbTempId(rs.getString("QRD_LT_AMBTEMPADDID"));
    			oTempNewRatingDto.setLtAmbTempRemoveId(rs.getString("QRD_LT_AMBTEMPREMOVEID"));
    			oTempNewRatingDto.setLtTempRise(rs.getString("QRD_LT_TEMPRISEID"));
    			oTempNewRatingDto.setLtInsulationClassId(rs.getString("QRD_LT_INSULATIONCLASSID"));
    			oTempNewRatingDto.setLtForcedCoolingId(rs.getString("QRD_LT_METHODOFCOOLING"));
    			oTempNewRatingDto.setLtAltitude(rs.getString("QRD_LT_ALTITUDE"));
    			oTempNewRatingDto.setLtHazardAreaId(rs.getString("QRD_LT_HAZARDAREAID"));
    			oTempNewRatingDto.setLtHazardAreaTypeId(rs.getString("QRD_LT_HAZARDAREATYPEID"));
    			oTempNewRatingDto.setLtGasGroupId(rs.getString("QRD_LT_GASGROUPID"));
    			oTempNewRatingDto.setLtHazardZoneId(rs.getString("QRD_LT_HAZARDZONEID"));
    			oTempNewRatingDto.setLtDustGroupId(rs.getString("QRD_LT_DUSTGROUPID"));
    			oTempNewRatingDto.setLtTempClass(rs.getString("QRD_LT_TEMPCLASSID"));
    			oTempNewRatingDto.setLtApplicationId(rs.getString("QRD_LT_APPLICATIONID"));
    			oTempNewRatingDto.setLtDutyId(rs.getString("QRD_LT_DUTYID"));
    			oTempNewRatingDto.setLtCDFId(rs.getString("QRD_LT_CDFID"));
    			oTempNewRatingDto.setLtStartsId(rs.getString("QRD_LT_STARTSID"));
    			oTempNewRatingDto.setLtNoiseLevel(rs.getString("QRD_LT_NOISELEVEL"));
    			
    			/* **********************************************	Price Fields	************************************************  */
    			oTempNewRatingDto.setLtTotalAddonPercent(rs.getString("QRD_LT_TOTALADDONPERCENT"));
    			oTempNewRatingDto.setLtTotalCashExtra(rs.getString("QRD_LT_TOTALCASHEXTRA"));
    			oTempNewRatingDto.setLtLPPerUnit(rs.getString("QRD_LT_LISTPRICE"));
    			oTempNewRatingDto.setLtLPWithAddonPerUnit(rs.getString("QRD_LT_LISTPRICEWITHADDON"));
    			oTempNewRatingDto.setLtStandardDiscount(rs.getString("QRD_LT_STANDARDDISCOUNT"));
    			oTempNewRatingDto.setLtPricePerUnit(rs.getString("QRD_LT_PRICEPERUNIT"));
    			oTempNewRatingDto.setLtTotalOrderPrice(rs.getString("QRD_LT_TOTALORDERPRICE"));
    			oTempNewRatingDto.setLtRequestedDiscount_SM(rs.getString("QRD_LT_REQUESTEDDISCOUNT_SM"));
    			oTempNewRatingDto.setLtRequestedPricePerUnit_SM(rs.getString("QRD_LT_REQ_PRICEPERUNIT_SM"));
    			oTempNewRatingDto.setLtRequestedTotalPrice_SM(rs.getString("QRD_LT_REQ_TOTALPRICE_SM"));
    			oTempNewRatingDto.setLtRequestedDiscount_RSM(rs.getString("QRD_REQUESTEDDISCOUNT_RSM"));
    			oTempNewRatingDto.setLtRequestedPricePerUnit_RSM(rs.getString("QRD_REQ_PRICEPERUNIT_RSM"));
    			oTempNewRatingDto.setLtRequestedTotalPrice_RSM(rs.getString("QRD_RED_TOTALPRICE_RSM"));
    			oTempNewRatingDto.setLtRequestedDiscount_NSM(rs.getString("QRD_REQUESTEDDISCOUNT_NSM"));
    			oTempNewRatingDto.setLtRequestedPricePerUnit_NSM(rs.getString("QRD_REQ_PRICEPERUNIT_NSM"));
    			oTempNewRatingDto.setLtRequestedTotalPrice_NSM(rs.getString("QRD_REQ_TOTALPRICE_NSM"));
    			oTempNewRatingDto.setLtApprovedDiscount_RSM(rs.getString("QRD_LT_APPROVEDDISCOUNT_RSM"));
    			oTempNewRatingDto.setLtApprovedPricePerunit_RSM(rs.getString("QRD_LT_APPRV_PRICEPERUNIT_RSM"));
    			oTempNewRatingDto.setLtApprovedTotalPrice_RSM(rs.getString("QRD_LT_APPRV_TOTALPRICE_RSM"));
    			oTempNewRatingDto.setLtApprovedDiscount_NSM(rs.getString("QRD_APPROVEDDISCOUNT_NSM"));
    			oTempNewRatingDto.setLtApprovedPricePerunit_NSM(rs.getString("QRD_APPRV_PRICEPERUNIT_NSM"));
    			oTempNewRatingDto.setLtApprovedTotalPrice_NSM(rs.getString("QRD_APPRV_TOTALPRICE_NSM"));
    			oTempNewRatingDto.setLtApprovedDiscount_MH(rs.getString("QRD_APPROVEDDISCOUNT_MH"));
    			oTempNewRatingDto.setLtApprovedPricePerunit_MH(rs.getString("QRD_APPRV_PRICEPERUNIT_MH"));
    			oTempNewRatingDto.setLtApprovedTotalPrice_MH(rs.getString("QRD_APPRV_TOTALPRICE_MH"));
    			oTempNewRatingDto.setLtQuotedDiscount(rs.getString("QRD_LT_QUOTEDDISCOUNT"));
    			oTempNewRatingDto.setLtQuotedPricePerUnit(rs.getString("QRD_LT_QUOTED_PRICEPERUNIT"));
    			oTempNewRatingDto.setLtQuotedTotalPrice(rs.getString("QRD_LT_QUOTED_TOTALPRICE"));
    			
    			/* **********************************************	Standard & Customer Req. Delivery Fields	********************  */
    			oTempNewRatingDto.setLtEarlierMotorSerialNo(rs.getString("QRD_LT_EARLIER_SUPPLIED_MOTOR"));
    			oTempNewRatingDto.setLtReplacementMotor(rs.getString("QRD_LT_REPLACEMENTMOTOR"));
    			oTempNewRatingDto.setLtStandardDeliveryWks(rs.getString("QRD_LT_STANDARD_DELIVERY"));
    			oTempNewRatingDto.setLtCustReqDeliveryWks(rs.getString("QRD_LT_CUSTOMER_REQ_DELIVERY"));
    			
    			/* **********************************************	Electrical Fields	********************************************  */
    			oTempNewRatingDto.setLtRVId(rs.getString("QRD_LT_RV"));
    			oTempNewRatingDto.setLtRAId(rs.getString("QRD_LT_RA"));
    			oTempNewRatingDto.setLtWindingTreatmentId(rs.getString("QRD_LT_WINDINGTREATMENTID"));
    			oTempNewRatingDto.setLtWindingWire(rs.getString("QRD_LT_WINDINGWIREID"));
    			oTempNewRatingDto.setLtLeadId(rs.getString("QRD_LT_LEADID"));
    			oTempNewRatingDto.setLtWindingConfig(rs.getString("QRD_LT_WINDINGCONFIGID"));
    			oTempNewRatingDto.setLtLoadGD2Value(rs.getString("QRD_LT_LOADGD2"));
    			oTempNewRatingDto.setLtVFDApplTypeId(rs.getString("QRD_LT_VFDTYPEID"));
    			oTempNewRatingDto.setLtVFDMotorSpeedRangeMin(rs.getString("QRD_LT_VFDSPEEDMINID"));
    			oTempNewRatingDto.setLtVFDMotorSpeedRangeMax(rs.getString("QRD_LT_VFDSPEEDMAXID"));
    			oTempNewRatingDto.setLtOverloadingDuty(rs.getString("QRD_LT_OVERLOADDUTYID"));
    			oTempNewRatingDto.setLtConstantEfficiencyRange(rs.getString("QRD_LT_CONSTEFFRANGEID"));
    			
    			/* **********************************************	Mechanical Fields	********************************************  */
    			oTempNewRatingDto.setLtShaftTypeId(rs.getString("QRD_LT_SHAFTTYPEID"));
    			oTempNewRatingDto.setLtShaftMaterialId(rs.getString("QRD_LT_SHAFTMATERIALID"));
    			oTempNewRatingDto.setLtDirectionOfRotation(rs.getString("QRD_LT_ROTATIONDIRECTIONID"));
    			oTempNewRatingDto.setLtStandardRotation(rs.getString("QRD_LT_STANDARDROTATION"));
    			oTempNewRatingDto.setLtMethodOfCoupling(rs.getString("QRD_LT_METHODOFCOUPLINGID"));
    			oTempNewRatingDto.setLtPaintingTypeId(rs.getString("QRD_LT_PAINTTYPEID"));
    			oTempNewRatingDto.setLtPaintShadeId(rs.getString("QRD_LT_PAINTSHADEID"));
    			oTempNewRatingDto.setLtPaintShadeValue(rs.getString("QRD_LT_PAINTSHADE_VALUE"));
    			oTempNewRatingDto.setLtPaintThicknessId(rs.getString("QRD_LT_PAINTTHICKESSID"));
    			oTempNewRatingDto.setLtIPId(rs.getString("QRD_LT_IPID"));
    			oTempNewRatingDto.setLtCableSizeId(rs.getString("QRD_LT_CABLESIZEID"));
    			oTempNewRatingDto.setLtNoOfRuns(rs.getString("QRD_LT_NOOFRUNSID"));
    			oTempNewRatingDto.setLtNoOfCores(rs.getString("QRD_LT_NOOFCORESID"));
    			oTempNewRatingDto.setLtCrossSectionAreaId(rs.getString("QRD_LT_CROSSSECTIONID"));
    			oTempNewRatingDto.setLtConductorMaterialId(rs.getString("QRD_LT_CONDMATERIALID"));
    			oTempNewRatingDto.setLtCableDiameter(rs.getString("QRD_LT_CABLEDIAMETER"));
    			oTempNewRatingDto.setLtTerminalBoxSizeId(rs.getString("QRD_LT_TERMINALBOXSIZEID"));
    			oTempNewRatingDto.setLtSpreaderBoxId(rs.getString("QRD_LT_SPREADERBOX"));
    			oTempNewRatingDto.setLtSpaceHeaterId(rs.getString("QRD_LT_SPACEHEATER"));
    			oTempNewRatingDto.setLtVibrationId(rs.getString("QRD_LT_VIBRATION"));
    			oTempNewRatingDto.setLtFlyingLeadWithoutTDId(rs.getString("QRD_LT_FLYINGLEADWITHOUTTB"));
    			oTempNewRatingDto.setLtFlyingLeadWithTDId(rs.getString("QRD_LT_FLYINGLEADWITHTB"));
    			oTempNewRatingDto.setLtMetalFanId(rs.getString("QRD_LT_METALFAN"));
    			oTempNewRatingDto.setLtTechoMounting(rs.getString("QRD_LT_TECHOMOUNTING"));
    			oTempNewRatingDto.setLtShaftGroundingId(rs.getString("QRD_LT_SHAFTGROUNDING"));
    			
    			/* **********************************************	Accessories Fields	********************************************  */
    			oTempNewRatingDto.setLtHardware(rs.getString("QRD_LT_HARDWARE"));
    			oTempNewRatingDto.setLtGlandPlateId(rs.getString("QRD_LT_GLANDPLATE"));
    			oTempNewRatingDto.setLtDoubleCompressionGlandId(rs.getString("QRD_LT_DOUBLECOMPRESSION"));
    			oTempNewRatingDto.setLtSPMMountingProvisionId(rs.getString("QRD_LT_SPMMOUNTING"));
    			oTempNewRatingDto.setLtAddNamePlateId(rs.getString("QRD_LT_ADDLNAMEPLATE"));
    			oTempNewRatingDto.setLtDirectionArrowPlateId(rs.getString("QRD_LT_DIRECTIONARROWPLATE"));
    			oTempNewRatingDto.setLtRTDId(rs.getString("QRD_LT_RTDID"));
    			oTempNewRatingDto.setLtBTDId(rs.getString("QRD_LT_BTDID"));
    			oTempNewRatingDto.setLtThermisterId(rs.getString("QRD_LT_THERMISTERID"));
    			oTempNewRatingDto.setLtAuxTerminalBoxId(rs.getString("QRD_LT_AUXTERMINALBOX"));
    			oTempNewRatingDto.setLtCableSealingBoxId(rs.getString("QRD_LT_CABLESEALINGBOX"));
    			
    			/* **********************************************	Bearings Fields	************************************************  */
    			oTempNewRatingDto.setLtBearingSystemId(rs.getString("QRD_LT_BEARINGSYSTEMID"));
    			oTempNewRatingDto.setLtBearingNDEId(rs.getString("QRD_LT_BEARINGNDEID"));
    			oTempNewRatingDto.setLtBearingDEId(rs.getString("QRD_LT_BEARINGDEID"));
    			
    			/* **********************************************	Certificate, Spares, Tests Fields	****************************  */
    			oTempNewRatingDto.setLtWitnessRoutineId(rs.getString("QRD_LT_WITNESSROUTINE"));
    			oTempNewRatingDto.setLtAdditionalTest1Id(rs.getString("QRD_LT_ADDL_TEST1"));
    			oTempNewRatingDto.setLtAdditionalTest2Id(rs.getString("QRD_LT_ADDL_TEST2"));
    			oTempNewRatingDto.setLtAdditionalTest3Id(rs.getString("QRD_LT_ADDL_TEST3"));
    			oTempNewRatingDto.setLtTypeTestId(rs.getString("QRD_LT_TYPETEST"));
    			oTempNewRatingDto.setLtULCEId(rs.getString("QRD_LT_CERTIFICATION"));
    			oTempNewRatingDto.setLtQAPId(rs.getString("QRD_LT_QAPID"));
    			oTempNewRatingDto.setLtSpares1(rs.getString("QRD_LT_SPARES1"));
    			oTempNewRatingDto.setLtSpares2(rs.getString("QRD_LT_SPARES2"));
    			oTempNewRatingDto.setLtSpares3(rs.getString("QRD_LT_SPARES3"));
    			oTempNewRatingDto.setLtSpares4(rs.getString("QRD_LT_SPARES4"));
    			oTempNewRatingDto.setLtSpares5(rs.getString("QRD_LT_SPARES5"));
    			oTempNewRatingDto.setLtDataSheet(rs.getString("QRD_LT_DATASHEET"));
    			oTempNewRatingDto.setLtMotorGA(rs.getString("QRD_LT_MOTORGA"));
    			oTempNewRatingDto.setLtPerfCurves(rs.getString("QRD_LT_PERFCURVE"));
    			oTempNewRatingDto.setLtTBoxGA(rs.getString("QRD_LT_TBOXGA"));
    			oTempNewRatingDto.setLtAdditionalComments(rs.getString("QRD_LT_ADDL_COMMENTS"));
    			
    			oTempNewRatingDto.setFlg_PriceFields_MTO(rs.getString("FLG_PRICEFIELDS_MTO"));
    			/*oTempNewRatingDto.setFlg_Addon_Leads_MTO(rs.getString("FLG_ADDON_LEADS_MTO"));
    			oTempNewRatingDto.setFlg_Addon_TBoxSize_MTO(rs.getString("FLG_ADDON_TBOXSIZE_MTO"));
    			oTempNewRatingDto.setFlg_Addon_BearingDE_MTO(rs.getString("FLG_ADDON_BEARINGDE_MTO"));
    			oTempNewRatingDto.setFlg_Addon_CableEntry_MTO(rs.getString("FLG_ADDON_CABLEENTRY_MTO"));*/
    			oTempNewRatingDto.setFlg_Txt_MotorNo_MTO(rs.getString("FLG_TXT_MOTORNO_MTO"));
    			oTempNewRatingDto.setFlg_Txt_RV_MTO(rs.getString("FLG_TXT_RV_MTO"));
    			oTempNewRatingDto.setFlg_Txt_RA_MTO(rs.getString("FLG_TXT_RA_MTO"));
    			oTempNewRatingDto.setFlg_Txt_LoadGD2_MTO(rs.getString("FLG_TXT_LOADGD2_MTO"));
    			oTempNewRatingDto.setFlg_Txt_NonSTDPaint_MTO(rs.getString("FLG_TXT_NONSTDPAINT_MTO"));
    			
    			oTempNewRatingDto.setIsReferDesign(rs.getString("QRD_LT_ISREFERDESIGN"));
    		}
        } finally {
    		/* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
    	}
        
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
        return oTempNewRatingDto;
	}
	
	public String savePrices_SubmitRSM(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception {
		String sMethodName = "savePrices_SubmitRSM";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null;
		/* ResultSet object to store the rows retrieved from database */
		ResultSet rs = null, rs1 = null;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();
		
		int iIndex = 0;
		String sResult = QuotationConstants.QUOTATION_STRING_Y;
		ArrayList<NewRatingDto> alRatingsList = new ArrayList<>(); 
		NewRatingDto oTempNewRatingDto = null;
		
		String sSQLQuery = "UPDATE "+ SCHEMA_QUOTATION + ".RFQ_LT_RATING_DETAILS SET QRD_LT_REQUESTEDDISCOUNT_SM=?, QRD_LT_REQ_PRICEPERUNIT_SM=?, QRD_LT_REQ_TOTALPRICE_SM=? WHERE QRD_LT_RATINGID=? ";
		pstmt = conn.prepareStatement(sSQLQuery);
		
		try {
			if(oNewEnquiryDto.getNewRatingsList() != null && oNewEnquiryDto.getNewRatingsList().size() > 0) {
				alRatingsList = oNewEnquiryDto.getNewRatingsList();
				for(int iCount = 0; iCount < oNewEnquiryDto.getNewRatingsList().size(); iCount++) {
					oTempNewRatingDto = (NewRatingDto) alRatingsList.get(iCount);
					iIndex = 1;
					
					pstmt.setString(iIndex++, oTempNewRatingDto.getLtRequestedDiscount_SM());
					pstmt.setString(iIndex++, oTempNewRatingDto.getLtRequestedPricePerUnit_SM());
					pstmt.setString(iIndex++, oTempNewRatingDto.getLtRequestedTotalPrice_SM());
					// WHERE CONDITION
					pstmt.setInt(iIndex++, oTempNewRatingDto.getRatingId());
					pstmt.addBatch();
				}
				if(pstmt != null) {
					int iVal1[] = pstmt.executeBatch();
					for(int i=0; i<iVal1.length; i++) {
						if(iVal1[i]==1){
						} else {
							sResult=QuotationConstants.QUOTATION_STRING_N;
							break;
						}
					}
				}
			}
		} finally {
			oDBUtility.releaseResources(pstmt);
		}
		
		return sResult;
	}
	
	public String savePrices_ApproveRSM(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception {
		String sMethodName = "savePrices_ApproveRSM";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null;
		/* ResultSet object to store the rows retrieved from database */
		ResultSet rs = null, rs1 = null;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();
		
		int iIndex = 0;
		String sResult = QuotationConstants.QUOTATION_STRING_Y;
		ArrayList<NewRatingDto> alRatingsList = new ArrayList<>(); 
		NewRatingDto oTempNewRatingDto = null;
		
		String sSQLQuery = "UPDATE "+ SCHEMA_QUOTATION + ".RFQ_LT_RATING_DETAILS SET QRD_LT_APPROVEDDISCOUNT_RSM=?, QRD_LT_APPRV_PRICEPERUNIT_RSM=?, QRD_LT_APPRV_TOTALPRICE_RSM=? WHERE QRD_LT_RATINGID=? ";
		pstmt = conn.prepareStatement(sSQLQuery);
		
		try {
			if(oNewEnquiryDto.getNewRatingsList() != null && oNewEnquiryDto.getNewRatingsList().size() > 0) {
				alRatingsList = oNewEnquiryDto.getNewRatingsList();
				for(int iCount = 0; iCount < oNewEnquiryDto.getNewRatingsList().size(); iCount++) {
					oTempNewRatingDto = (NewRatingDto) alRatingsList.get(iCount);
					iIndex = 1;
					
					pstmt.setString(iIndex++, oTempNewRatingDto.getLtApprovedDiscount_RSM());
					pstmt.setString(iIndex++, oTempNewRatingDto.getLtApprovedPricePerunit_RSM());
					pstmt.setString(iIndex++, oTempNewRatingDto.getLtApprovedTotalPrice_RSM());
					// WHERE CONDITION
					pstmt.setInt(iIndex++, oTempNewRatingDto.getRatingId());
					pstmt.addBatch();
				}
				if(pstmt != null) {
					int iVal1[] = pstmt.executeBatch();
					for(int i=0; i<iVal1.length; i++) {
						if(iVal1[i]==1){
						} else {
							sResult=QuotationConstants.QUOTATION_STRING_N;
							break;
						}
					}
				}
			}
		} finally {
			oDBUtility.releaseResources(pstmt);
		}
		
		return sResult;
	}
	
	public String savePrices_SubmitNSM(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception {
		String sMethodName = "savePrices_SubmitNSM";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null;
		/* ResultSet object to store the rows retrieved from database */
		ResultSet rs = null, rs1 = null;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();
		
		int iIndex = 0;
		String sResult = QuotationConstants.QUOTATION_STRING_Y;
		ArrayList<NewRatingDto> alRatingsList = new ArrayList<>(); 
		NewRatingDto oTempNewRatingDto = null;
		
		String sSQLQuery = "UPDATE "+ SCHEMA_QUOTATION + ".RFQ_LT_RATING_DETAILS SET QRD_REQUESTEDDISCOUNT_RSM=?, QRD_REQ_PRICEPERUNIT_RSM=?, QRD_RED_TOTALPRICE_RSM=? WHERE QRD_LT_RATINGID=? ";
		pstmt = conn.prepareStatement(sSQLQuery);
		
		try {
			if(oNewEnquiryDto.getNewRatingsList() != null && oNewEnquiryDto.getNewRatingsList().size() > 0) {
				alRatingsList = oNewEnquiryDto.getNewRatingsList();
				for(int iCount = 0; iCount < oNewEnquiryDto.getNewRatingsList().size(); iCount++) {
					oTempNewRatingDto = (NewRatingDto) alRatingsList.get(iCount);
					iIndex = 1;
					
					pstmt.setString(iIndex++, oTempNewRatingDto.getLtRequestedDiscount_RSM());
					pstmt.setString(iIndex++, oTempNewRatingDto.getLtRequestedPricePerUnit_RSM());
					pstmt.setString(iIndex++, oTempNewRatingDto.getLtRequestedTotalPrice_RSM());
					// WHERE CONDITION
					pstmt.setInt(iIndex++, oTempNewRatingDto.getRatingId());
					pstmt.addBatch();
				}
				if(pstmt != null) {
					int iVal1[] = pstmt.executeBatch();
					for(int i=0; i<iVal1.length; i++) {
						if(iVal1[i]==1){
						} else {
							sResult=QuotationConstants.QUOTATION_STRING_N;
							break;
						}
					}
				}
			}
		} finally {
			oDBUtility.releaseResources(pstmt);
		}
		
		return sResult;
	}
	
	public String savePrices_ApproveNSM(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception {
		String sMethodName = "savePrices_ApproveNSM";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null;
		/* ResultSet object to store the rows retrieved from database */
		ResultSet rs = null, rs1 = null;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();
		
		int iIndex = 0;
		String sResult = QuotationConstants.QUOTATION_STRING_Y;
		ArrayList<NewRatingDto> alRatingsList = new ArrayList<>(); 
		NewRatingDto oTempNewRatingDto = null;
		
		String sSQLQuery = "UPDATE "+ SCHEMA_QUOTATION + ".RFQ_LT_RATING_DETAILS SET QRD_APPROVEDDISCOUNT_NSM=?, QRD_APPRV_PRICEPERUNIT_NSM=?, QRD_APPRV_TOTALPRICE_NSM=? WHERE QRD_LT_RATINGID=? ";
		pstmt = conn.prepareStatement(sSQLQuery);
		
		try {
			if(oNewEnquiryDto.getNewRatingsList() != null && oNewEnquiryDto.getNewRatingsList().size() > 0) {
				alRatingsList = oNewEnquiryDto.getNewRatingsList();
				for(int iCount = 0; iCount < oNewEnquiryDto.getNewRatingsList().size(); iCount++) {
					oTempNewRatingDto = (NewRatingDto) alRatingsList.get(iCount);
					iIndex = 1;
					
					pstmt.setString(iIndex++, oTempNewRatingDto.getLtApprovedDiscount_NSM());
					pstmt.setString(iIndex++, oTempNewRatingDto.getLtApprovedPricePerunit_NSM());
					pstmt.setString(iIndex++, oTempNewRatingDto.getLtApprovedTotalPrice_NSM());
					// WHERE CONDITION
					pstmt.setInt(iIndex++, oTempNewRatingDto.getRatingId());
					pstmt.addBatch();
				}
				if(pstmt != null) {
					int iVal1[] = pstmt.executeBatch();
					for(int i=0; i<iVal1.length; i++) {
						if(iVal1[i]==1){
						} else {
							sResult=QuotationConstants.QUOTATION_STRING_N;
							break;
						}
					}
				}
			}
			
		} finally {
			oDBUtility.releaseResources(pstmt);
		}
		
		return sResult;
	}
	
	public String savePrices_SubmitMH(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception {
		String sMethodName = "savePrices_SubmitMH";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null;
		/* ResultSet object to store the rows retrieved from database */
		ResultSet rs = null, rs1 = null;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();
		
		int iIndex = 0;
		String sResult = QuotationConstants.QUOTATION_STRING_Y;
		ArrayList<NewRatingDto> alRatingsList = new ArrayList<>(); 
		NewRatingDto oTempNewRatingDto = null;
		
		String sSQLQuery = "UPDATE "+ SCHEMA_QUOTATION + ".RFQ_LT_RATING_DETAILS SET QRD_REQUESTEDDISCOUNT_NSM=?, QRD_REQ_PRICEPERUNIT_NSM=?, QRD_REQ_TOTALPRICE_NSM=? WHERE QRD_LT_RATINGID=? ";
		pstmt = conn.prepareStatement(sSQLQuery);
		
		try {
			if(oNewEnquiryDto.getNewRatingsList() != null && oNewEnquiryDto.getNewRatingsList().size() > 0) {
				alRatingsList = oNewEnquiryDto.getNewRatingsList();
				for(int iCount = 0; iCount < oNewEnquiryDto.getNewRatingsList().size(); iCount++) {
					oTempNewRatingDto = (NewRatingDto) alRatingsList.get(iCount);
					iIndex = 1;
					
					pstmt.setString(iIndex++, oTempNewRatingDto.getLtRequestedDiscount_NSM());
					pstmt.setString(iIndex++, oTempNewRatingDto.getLtRequestedPricePerUnit_NSM());
					pstmt.setString(iIndex++, oTempNewRatingDto.getLtRequestedTotalPrice_NSM());
					// WHERE CONDITION
					pstmt.setInt(iIndex++, oTempNewRatingDto.getRatingId());
					pstmt.addBatch();
				}
				if(pstmt != null) {
					int iVal1[] = pstmt.executeBatch();
					for(int i=0; i<iVal1.length; i++) {
						if(iVal1[i]==1){
						} else {
							sResult=QuotationConstants.QUOTATION_STRING_N;
							break;
						}
					}
				}
			}
		} finally {
			oDBUtility.releaseResources(pstmt);
		}
		
		return sResult;
	}
	
	public String savePrices_ApproveMH(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception {
		String sMethodName = "savePrices_ApproveMH";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null;
		/* ResultSet object to store the rows retrieved from database */
		ResultSet rs = null, rs1 = null;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();
		
		int iIndex = 0;
		String sResult = QuotationConstants.QUOTATION_STRING_Y;
		ArrayList<NewRatingDto> alRatingsList = new ArrayList<>(); 
		NewRatingDto oTempNewRatingDto = null;
		
		String sSQLQuery = "UPDATE "+ SCHEMA_QUOTATION + ".RFQ_LT_RATING_DETAILS SET QRD_APPROVEDDISCOUNT_MH=?, QRD_APPRV_PRICEPERUNIT_MH=?, QRD_APPRV_TOTALPRICE_MH=? WHERE QRD_LT_RATINGID=? ";
		pstmt = conn.prepareStatement(sSQLQuery);
		
		try {
			if(oNewEnquiryDto.getNewRatingsList() != null && oNewEnquiryDto.getNewRatingsList().size() > 0) {
				alRatingsList = oNewEnquiryDto.getNewRatingsList();
				for(int iCount = 0; iCount < oNewEnquiryDto.getNewRatingsList().size(); iCount++) {
					oTempNewRatingDto = (NewRatingDto) alRatingsList.get(iCount);
					iIndex = 1;
					
					pstmt.setString(iIndex++, oTempNewRatingDto.getLtApprovedDiscount_MH());
					pstmt.setString(iIndex++, oTempNewRatingDto.getLtApprovedPricePerunit_MH());
					pstmt.setString(iIndex++, oTempNewRatingDto.getLtApprovedTotalPrice_MH());
					// WHERE CONDITION
					pstmt.setInt(iIndex++, oTempNewRatingDto.getRatingId());
					pstmt.addBatch();
				}
				if(pstmt != null) {
					int iVal1[] = pstmt.executeBatch();
					for(int i=0; i<iVal1.length; i++) {
						if(iVal1[i]==1){
						} else {
							sResult=QuotationConstants.QUOTATION_STRING_N;
							break;
						}
					}
				}
			}
		} finally {
			oDBUtility.releaseResources(pstmt);
		}
		
		return sResult;
	}
	
	public String savePrices_ReleaseSM(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception {
		String sMethodName = "savePrices_ReleaseSM";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null;
		/* ResultSet object to store the rows retrieved from database */
		ResultSet rs = null, rs1 = null;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();
		
		int iIndex = 0;
		String sResult = QuotationConstants.QUOTATION_STRING_Y;
		ArrayList<NewRatingDto> alRatingsList = new ArrayList<>(); 
		NewRatingDto oTempNewRatingDto = null;
		
		String sSQLQuery = "UPDATE "+ SCHEMA_QUOTATION + ".RFQ_LT_RATING_DETAILS SET QRD_LT_STANDARDDISCOUNT=?, QRD_LT_PRICEPERUNIT=?, QRD_LT_TOTALORDERPRICE=?, QRD_LT_QUOTEDDISCOUNT=?, QRD_LT_QUOTED_PRICEPERUNIT=?, QRD_LT_QUOTED_TOTALPRICE=? WHERE QRD_LT_RATINGID=? ";
		pstmt = conn.prepareStatement(sSQLQuery);
		
		try {
			if(oNewEnquiryDto.getNewRatingsList() != null && oNewEnquiryDto.getNewRatingsList().size() > 0) {
				alRatingsList = oNewEnquiryDto.getNewRatingsList();
				for(int iCount = 0; iCount < oNewEnquiryDto.getNewRatingsList().size(); iCount++) {
					oTempNewRatingDto = (NewRatingDto) alRatingsList.get(iCount);
					iIndex = 1;
					
					pstmt.setString(iIndex++, oTempNewRatingDto.getLtQuotedDiscount());
					pstmt.setString(iIndex++, oTempNewRatingDto.getLtQuotedPricePerUnit());
					pstmt.setString(iIndex++, oTempNewRatingDto.getLtQuotedTotalPrice());
					pstmt.setString(iIndex++, oTempNewRatingDto.getLtQuotedDiscount());
					pstmt.setString(iIndex++, oTempNewRatingDto.getLtQuotedPricePerUnit());
					pstmt.setString(iIndex++, oTempNewRatingDto.getLtQuotedTotalPrice());
					// WHERE CONDITION
					pstmt.setInt(iIndex++, oTempNewRatingDto.getRatingId());
					pstmt.addBatch();
				}
				if(pstmt != null) {
					int iVal1[] = pstmt.executeBatch();
					for(int i=0; i<iVal1.length; i++) {
						if(iVal1[i]==1){
						} else {
							sResult=QuotationConstants.QUOTATION_STRING_N;
							break;
						}
					}
				}
			}
		} finally {
			oDBUtility.releaseResources(pstmt);
		}
		
		return sResult;
	}
	
	public NewEnquiryDto createNewEnquiryRevision(Connection conn, NewEnquiryDto oNewEnquiryDto, UserDto oUserDto) throws Exception {
		
		String sMethodName = "createNewEnquiryRevision";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");

		/* PreparedStatement objects to handle database operations */
		CallableStatement cstmt = null;

		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();

		NewEnquiryDto oTempNewEnquiryDto = null;
		String sEnquiryId;
		
		try {
			cstmt = conn.prepareCall(CALL_COPYNEWENQUIRY_PROC);
			cstmt.setString(1, ("" + oNewEnquiryDto.getEnquiryId()).trim());
			cstmt.setString(2, oUserDto.getUserId());
			cstmt.setString(3, QuotationConstants.QUOTATION_REVISION);
			cstmt.registerOutParameter(1, Types.VARCHAR);

			cstmt.executeQuery();
			
			sEnquiryId = cstmt.getString(1);
			
			if (sEnquiryId != null && !sEnquiryId.equalsIgnoreCase("")) {
				oTempNewEnquiryDto = new NewEnquiryDto();
				oTempNewEnquiryDto.setEnquiryId(Integer.parseInt(sEnquiryId));
			}
		} finally {
			/* Releasing PreparedStatment objects */
			oDBUtility.releaseResources(cstmt);
		} 
		
		oTempNewEnquiryDto = getNewEnquiryDetailsForView(conn, oTempNewEnquiryDto, QuotationConstants.QUOTATION_FORWARD_VIEW);
		
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
		return oTempNewEnquiryDto;
	}
	
	public NewRatingDto processNewRatingDefaults(Connection conn, NewRatingDto newRatingDto) throws Exception {
		String sMethodName = "processNewRatingDefaults";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");

		Map<String,String> ratingDefaultsMap = new HashMap<>();
		
		try {
			ArrayList<KeyValueVo> arlOptions = getAscendingOptionsbyAttributeList(conn, QuotationConstants.QEM_LT_DEFAULTS);
			
			for(KeyValueVo oKV : arlOptions) {
				ratingDefaultsMap.put(oKV.getKey(), oKV.getValue());
			}
			
			newRatingDto.setQuantity(Integer.parseInt(QuotationConstants.QUOTATION_ONE));
			
			// For Mandatory Params - Set value "0".
			newRatingDto.setLtProdGroupId(QuotationConstants.QUOTATION_ZERO);
			newRatingDto.setLtProdLineId(QuotationConstants.QUOTATION_ZERO);
			newRatingDto.setLtMotorTypeId(QuotationConstants.QUOTATION_ZERO);
			newRatingDto.setLtKWId(QuotationConstants.QUOTATION_ZERO);
			newRatingDto.setLtPoleId(QuotationConstants.QUOTATION_ZERO);
			newRatingDto.setLtFrameId(QuotationConstants.QUOTATION_ZERO);
			newRatingDto.setLtFrameSuffixId(QuotationConstants.QUOTATION_ZERO);
			newRatingDto.setLtMountingId(QuotationConstants.QUOTATION_ZERO);
			newRatingDto.setLtTBPositionId(QuotationConstants.QUOTATION_ZERO);
			
			newRatingDto.setLtVoltId(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_VOLT));
			newRatingDto.setLtVoltAddVariation(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_VOLTADDVAR));
			newRatingDto.setLtVoltRemoveVariation(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_VOLTREMVAR));
			newRatingDto.setLtFreqId(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_FREQ));
			newRatingDto.setLtFreqAddVariation(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_FREQADDVAR));
			newRatingDto.setLtFreqRemoveVariation(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_FREQREMVAR));
			newRatingDto.setLtCombVariation(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_COMBVAR));
			newRatingDto.setLtServiceFactor(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_SERVICEFACTOR));
			newRatingDto.setLtMethodOfStarting(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_MTHDOFSTART));
			newRatingDto.setLtStartingCurrentOnDOLStart(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_DOLSTART));
			newRatingDto.setLtTBPositionId(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_TBPOS));
			newRatingDto.setLtAmbTempId(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_AMBTEMP));
			newRatingDto.setLtTempRise(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_TEMPRISE));
			newRatingDto.setLtInsulationClassId(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_INSCLASS));
			newRatingDto.setLtForcedCoolingId(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_MTHDOFCOOL));
			newRatingDto.setLtHazardAreaId(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_HAZLOC));
			newRatingDto.setLtHazardAreaTypeId(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_HAZAREA));
			newRatingDto.setLtGasGroupId(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_GASGROUP));
			newRatingDto.setLtDutyId(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_DUTY));
			newRatingDto.setLtReplacementMotor(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_REPLACEMOTOR));
			newRatingDto.setLtWindingTreatmentId(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_WINDTREATMENT));
			newRatingDto.setLtWindingWire(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_WINDWIRE));
			newRatingDto.setLtLeadId(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_LEAD));
			newRatingDto.setLtWindingConfig(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_WINDCONFIG));
			newRatingDto.setLtOverloadingDuty(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_OVERLOADDUTY));
			newRatingDto.setLtConstantEfficiencyRange(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_CONSTEFFRANGE));
			newRatingDto.setLtShaftTypeId(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_SHAFTTYPE));
			newRatingDto.setLtShaftMaterialId(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_SHAFTMATERIAL));
			newRatingDto.setLtDirectionOfRotation(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_ROTATIONDIR));
			newRatingDto.setLtMethodOfCoupling(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_MTHDOFCOUPLING));
			newRatingDto.setLtPaintingTypeId(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_PAINTTYPE));
			newRatingDto.setLtPaintShadeId(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_PAINTSHADE));
			newRatingDto.setLtPaintThicknessId(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_PAINTTHICKNESS));
			newRatingDto.setLtCableSizeId(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_CABLESIZE));
			newRatingDto.setLtTerminalBoxSizeId(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_TBOXSIZE));
			newRatingDto.setLtSpreaderBoxId(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_SPREADERBOX));
			newRatingDto.setLtSpaceHeaterId(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_SPACEHEATER));
			newRatingDto.setLtVibrationId(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_VIBRATION));
			newRatingDto.setLtFlyingLeadWithoutTDId(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_FLYLEADWITHOUTTB));
			newRatingDto.setLtFlyingLeadWithTDId(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_FLYLEADWITHTB));
			newRatingDto.setLtMetalFanId(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_METALFAN));
			newRatingDto.setLtTechoMounting(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_TECHOMOUNTING));
			newRatingDto.setLtShaftGroundingId(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_SHAFTGROUNDING));
			newRatingDto.setLtHardware(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_HARDWARE));
			newRatingDto.setLtGlandPlateId(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_GLANDPLATE));
			newRatingDto.setLtDoubleCompressionGlandId(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_DOUBLECOMPRESSGLAND));
			newRatingDto.setLtSPMMountingProvisionId(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_SPMMOUNTPROVISION));
			newRatingDto.setLtAddNamePlateId(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_ADDLNAMEPLATE));
			newRatingDto.setLtDirectionArrowPlateId(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_ARROWPLATEDIR));
			newRatingDto.setLtRTDId(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_RTD));
			newRatingDto.setLtBTDId(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_BTD));
			newRatingDto.setLtThermisterId(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_THERMISTER));
			newRatingDto.setLtAuxTerminalBoxId(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_AUXTBOX));
			newRatingDto.setLtCableSealingBoxId(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_CABLESEALBOX));
			newRatingDto.setLtBearingSystemId(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_BEARINGSYSTEM));
			newRatingDto.setLtBearingNDEId(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_BEARINGNDE));
			newRatingDto.setLtBearingDEId(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_BEARINGDE));
			newRatingDto.setLtWitnessRoutineId(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_WITNESS));
			newRatingDto.setLtAdditionalTest1Id(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_ADDLTEST1));
			newRatingDto.setLtAdditionalTest2Id(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_ADDLTEST2));
			newRatingDto.setLtAdditionalTest3Id(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_ADDLTEST3));
			newRatingDto.setLtTypeTestId(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_TYPETEST));
			newRatingDto.setLtULCEId(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_CERTIFICATION));
			newRatingDto.setLtSpares1(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_SPARES));
			newRatingDto.setLtSpares2(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_SPARES));
			newRatingDto.setLtSpares3(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_SPARES));
			newRatingDto.setLtSpares4(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_SPARES));
			newRatingDto.setLtSpares5(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_SPARES));
			newRatingDto.setLtNoiseLevel(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_NOISELEVEL));
			newRatingDto.setLtQAPId(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_QAP));
			newRatingDto.setLtDataSheet(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_DATASHEET));
			newRatingDto.setLtMotorGA(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_MOTORGA));
			newRatingDto.setLtPerfCurves(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_PERFCURVE));
			newRatingDto.setLtTBoxGA(ratingDefaultsMap.get(QuotationConstants.RATING_DEFAULT_TBOXGA));
			
			newRatingDto.setLtAltitude(QuotationConstants.RATING_DEFAULT_ALTITUDE);
		} finally {
			
		}
		
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
		return newRatingDto;
	}
	
	public void saveNewEnquiryNotes(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception {
		String sMethodName = "saveNewEnquiryNotes";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null;
		/* ResultSet object to store the rows retrieved from database */
		ResultSet rs = null;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();
		
		ArrayList<NewRatingDto> alRatingsList = new ArrayList<>(); 
		NewRatingDto oTempNewRatingDto = null;
		
		String sSQLQuery = "UPDATE "+ SCHEMA_QUOTATION + ".RFQ_ENQUIRY_MASTER SET QEM_SMNOTE=?, QEM_RSMNOTE=?, QEM_NSMNOTE=?, QEM_MHNOTE=? WHERE QEM_ENQUIRYID=? ";
		
		try {
			pstmt = conn.prepareStatement(sSQLQuery);
			
			pstmt.setString(1, oNewEnquiryDto.getSmNote());
			pstmt.setString(2, oNewEnquiryDto.getRsmNote());
			pstmt.setString(3, oNewEnquiryDto.getNsmNote());
			pstmt.setString(4, oNewEnquiryDto.getMhNote());
			pstmt.setInt(5, oNewEnquiryDto.getEnquiryId());
			
			pstmt.executeUpdate();
		} finally {
			oDBUtility.releaseResources(rs, pstmt);
		}
		
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
	}
	
	public void updateSubmitToDesignFlag(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception {
		String sMethodName = "saveNewEnquiryNotes";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null;
		/* ResultSet object to store the rows retrieved from database */
		ResultSet rs = null;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();
		
		String sSQLQuery = "UPDATE "+ SCHEMA_QUOTATION + ".RFQ_ENQUIRY_MASTER SET QEM_IS_REFER_DESIGN=? WHERE QEM_ENQUIRYID=? ";
		
		try {
			pstmt = conn.prepareStatement(sSQLQuery);
			
			pstmt.setString(1, oNewEnquiryDto.getIsReferDesign());
			pstmt.setInt(2, oNewEnquiryDto.getEnquiryId());
			
			pstmt.executeUpdate();
		} finally {
			oDBUtility.releaseResources(rs, pstmt);
		}
		
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
	}
	
	public boolean updateMTORatingDetailsById(Connection conn, NewRatingDto oNewRatingDto) throws Exception {
		String sMethodName = "updateMTORatingDetailsById";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		int iUpdateRes = 0;
		boolean isUpdated = false;
		String sSQLRatingQuery = null;
		
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();
		
		try {
			sSQLRatingQuery = buildNewRatingUpdateQuery(oNewRatingDto, oNewRatingDto.getEnquiryId(), true);
			pstmt = conn.prepareStatement(sSQLRatingQuery);
			iUpdateRes = pstmt.executeUpdate();
			if(iUpdateRes == 1) {
				isUpdated = true;
			}
		} finally {
			oDBUtility.releaseResources(pstmt);
		}
		
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
		return isUpdated;
	}
	
	
	public boolean deleteAllNewRatingAddOns(Connection conn, int ratingId) throws Exception {
		boolean isDeleted = false;
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        try {
        	pstmt = conn.prepareStatement(DELETE_ADDONS);
        	pstmt.setInt(1, ratingId);	            
        	pstmt.executeUpdate();
            isDeleted = true;
            
        } finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(pstmt);
        }
        
		return isDeleted;
	}
	
	public boolean updateNewRatingAddOns(Connection conn, NewRatingDto oNewRatingDto) throws Exception {
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        AddOnDto oAddOnDto = null;
        int iInserted = 0;
        int iTotalAddOn = 0;
        boolean isInserted = false;
        
        try {
        	if (oNewRatingDto.getRatingId() > 0 && oNewRatingDto.getAddOnList().size() > 0){
        		for (int i=0; i < oNewRatingDto.getAddOnList().size(); i++){
        			oAddOnDto = (AddOnDto) oNewRatingDto.getAddOnList().get(i);
        			if (oAddOnDto != null){
        				pstmt = conn.prepareStatement(INSERT_ADDON_DETAILS);	
        				pstmt.setInt(1, oAddOnDto.getAddOnRatingId());	        				
        				pstmt.setString(2, oAddOnDto.getAddOnTypeText().trim());
        				pstmt.setInt(3, oAddOnDto.getAddOnQuantity());
        				pstmt.setDouble(4, oAddOnDto.getAddOnUnitPrice());
        				pstmt.setInt(5, oAddOnDto.getAddOnUnitPriceCE());
        				pstmt.setInt(6, oAddOnDto.getAddOnUnitPriceRSM());
        				pstmt.setInt(7, oAddOnDto.getAddOnPercent());
    	    			 
        				iInserted = pstmt.executeUpdate();     		 
    	    			
        				if (iInserted > 0)
    	    				 iTotalAddOn++;
        			}
        		}
        		if (iTotalAddOn > 0)
        			isInserted = true;
	    	 }
        } finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(pstmt);
        }
        
		return isInserted;
	}
	
	
	public boolean updateMTORatingTechOfferDetails(Connection conn, NewTechnicalOfferDto oNewTechnicalOfferDto) throws Exception {
		String sMethodName = "updateMTORatingTechOfferDetails";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        PreparedStatement pstmt1 = null;
		PreparedStatement pstmt2 = null;
		/* ResultSet object to store the rows retrieved from database */
		ResultSet rs = null, rs1 = null;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();
        
		String sSQLRatingQuery = null;
		String sChkQuery = "SELECT * FROM " + SCHEMA_QUOTATION + ".RFQ_TECHNICAL_DATA WHERE RATING_ID = ? ";
		int iResult = 0;
        boolean isUpdated = false;
        
        try {
        	pstmt = conn.prepareStatement(sChkQuery);
        	pstmt.setInt(1, oNewTechnicalOfferDto.getRatingId());
        	rs = pstmt.executeQuery();
        	
        	if(rs.next()) {		// UPDATE
        		sSQLRatingQuery = buildNewTechOfferUpdateQuery(oNewTechnicalOfferDto);
        		pstmt1 = conn.prepareStatement(sSQLRatingQuery);
        		iResult = pstmt1.executeUpdate();
        		
        	} else {			// INSERT
        		sSQLRatingQuery = buildNewTechOfferInsertQuery(oNewTechnicalOfferDto);
        		pstmt1 = conn.prepareStatement(sSQLRatingQuery);
        		iResult = pstmt1.executeUpdate();
        		
        	}
        	
        	if(iResult == 1) {
        		isUpdated = true;
        	}
        } finally {
        	oDBUtility.releaseResources(rs, pstmt);
        	oDBUtility.releaseResources(pstmt1);
        }
        
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
        return isUpdated;
	}
	
	private String buildNewTechOfferUpdateQuery(NewTechnicalOfferDto oNewTechnicalOfferDto) {
		String sMethodName = "buildNewTechOfferUpdateQuery";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		StringBuffer sbFieldQuery = new StringBuffer();
		
		if (oNewTechnicalOfferDto != null) {
			sbFieldQuery.append(UPDATE_NEW_TECHDATA_DETAILS);
			
			sbFieldQuery.append("PRODUCT_GROUP = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getProductGroup() + "', ");
			
			sbFieldQuery.append("PRODUCT_LINE = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getProductLine() + "', ");
			
			sbFieldQuery.append("STANDARD = ");
			sbFieldQuery.append("'" + "N" + "', ");
			
			sbFieldQuery.append("POWER_RATING_KW = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getPowerRating_KW() + "', ");
			
			sbFieldQuery.append("FRAME_TYPE = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getFrameType() + "', ");
			
			sbFieldQuery.append("NUMBER_OF_POLES = ");
			sbFieldQuery.append(oNewTechnicalOfferDto.getNoOfPoles() + ", ");
			
			sbFieldQuery.append("MOUNTING_TYPE = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getMountingType() + "', ");
			
			sbFieldQuery.append("TB_POSITION = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getTbPosition() + "', ");
			
			sbFieldQuery.append("MODEL_NO = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getModelNumber() + "', ");
			
			sbFieldQuery.append("VOLTAGE_V = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getVoltage() + "', ");
			
			sbFieldQuery.append("VOLTAGE_ADD_VAR = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getVoltageAddVariation() + "', ");
			
			sbFieldQuery.append("VOLTAGE_REMOVE_VAR = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getVoltageRemoveVariation() + "', ");
			
			sbFieldQuery.append("FREQUENCY_HZ = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getFrequency() + "', ");
			
			sbFieldQuery.append("FREQUENCY_ADD_VAR = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getFrequencyAddVariation() + "', ");
			
			sbFieldQuery.append("FREQUENCY_REMOVE_VAR = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getFrequencyRemoveVariation() + "', ");
			
			sbFieldQuery.append("COMBINED_VAR = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getCombinedVariation() + "', ");
			
			sbFieldQuery.append("COOLING_METHOD = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getCoolingMethod() + "', ");
			
			sbFieldQuery.append("MOTOR_INERTIA = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getMotorInertia() + "', ");
			
			sbFieldQuery.append("VIBRATION = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getVibration() + "', ");
			
			sbFieldQuery.append("NOISE = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getNoise() + "', ");
			
			sbFieldQuery.append("NUMBER_OF_STARTS = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getNoOfStarts() + "', ");
			
			sbFieldQuery.append("LR_WITHSTAND_TIME = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getLrWithstandTime() + "', ");
			
			sbFieldQuery.append("DIRECTION_OD_ROTATION = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getDirectionOfRotation() + "', ");
			
			sbFieldQuery.append("POWER_RATING_HP = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getPowerRating_HP() + "', ");
			
			sbFieldQuery.append("CABLE_SIZE = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getCableSize() + "', ");
			
			sbFieldQuery.append("CURRENT_A = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getCurrent_A() + "', ");
			
			sbFieldQuery.append("SPEED_MAX = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getMaxSpeed() + "', ");
			
			sbFieldQuery.append("POWER_FACTOR_100 = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getPowerFactor_100() + "', ");
			
			sbFieldQuery.append("POWER_FACTOR_75 = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getPowerFactor_75() + "', ");
			
			sbFieldQuery.append("POWER_FACTOR_50 = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getPowerFactor_50() + "', ");
			
			sbFieldQuery.append("EFFICIENCY_CLASS = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getEfficiencyClass() + "', ");
			
			sbFieldQuery.append("EFFICIENCY_100 = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getEfficiency_100() + "', ");
			
			sbFieldQuery.append("EFFICIENCY_75 = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getEfficiency_75() + "', ");
			
			sbFieldQuery.append("EFFICIENCY_50 = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getEfficiency_50() + "', ");
			
			sbFieldQuery.append("STARTING_CURRENT = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getStartingCurrent() + "', ");
			
			sbFieldQuery.append("TORQUE = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getTorque() + "', ");
			
			sbFieldQuery.append("STARTING_TORQUE = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getStarting_torque() + "', ");
			
			sbFieldQuery.append("PULLOUT_TORQUE = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getPullout_torque() + "', ");
			
			sbFieldQuery.append("RA = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getRaValue() + "', ");
			
			sbFieldQuery.append("RV = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getRvValue() + "', ");
			
			sbFieldQuery.append("HAZ_AREA = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getHazArea() + "', ");
			
			sbFieldQuery.append("HAZ_LOCATION = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getHazLocation() + "', ");
			
			sbFieldQuery.append("ZONE_CLASSIFICATION = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getZoneClassification() + "', ");
			
			sbFieldQuery.append("GAS_GROUP = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getGasGroup() + "', ");
			
			sbFieldQuery.append("TEMP_CLASS = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getTemperatureClass() + "', ");
			
			sbFieldQuery.append("ROTOR_TYPE = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getRotortype() + "', ");
			
			sbFieldQuery.append("ELECTRICAL_TYPE = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getElectricalType() + "', ");
			
			sbFieldQuery.append("IP_CODE = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getIpCode() + "', ");
			
			sbFieldQuery.append("INSULATION_CLASS = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getInsulationClass() + "', ");
			
			sbFieldQuery.append("SERVICE_FACTOR = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getServiceFactor() + "', ");
			
			sbFieldQuery.append("DUTY = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getDuty() + "', ");
			
			sbFieldQuery.append("CDF = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getCdf() + "', ");
			
			sbFieldQuery.append("NUMBER_OF_STARTS_HR = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getNoOfStarts_HR() + "', ");
			
			sbFieldQuery.append("NUMBER_OF_PHASES = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getNoOfPhases() + "', ");
			
			sbFieldQuery.append("MAX_AMBIENT_C = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getMaxAmbientTemp() + "', ");
			
			sbFieldQuery.append("TEMP_RISE = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getTempRise() + "', ");
			
			sbFieldQuery.append("ALTITUDE = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getAltitude() + "', ");
			
			sbFieldQuery.append("DE_BEARING_SIZE = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getDeBearingSize() + "', ");
			
			sbFieldQuery.append("DE_BEARING_TYPE = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getDeBearingType() + "', ");
			
			sbFieldQuery.append("ODE_BEARING_SIZE = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getOdeBearingSize() + "', ");
			
			sbFieldQuery.append("ODE_BEARING_TYPE = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getOdeBearingType() + "', ");
			
			sbFieldQuery.append("ENCLOSURE_TYPE = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getEnclosureType() + "', ");
			
			sbFieldQuery.append("LUB_METHOD = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getLubMethod() + "', ");
			
			sbFieldQuery.append("TYPE_OF_GREASE = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getTypeOfGrease() + "', ");
			
			sbFieldQuery.append("MOTOR_ORIENTATION = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getMotorOrientation() + "', ");
			
			sbFieldQuery.append("FRAME_MATERIAL = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getFrameMaterial() + "', ");
			
			sbFieldQuery.append("FRAME_LENGTH = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getFrameLength() + "', ");
			
			sbFieldQuery.append("ROTATION = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getRotation() + "', ");
			
			sbFieldQuery.append("NUMBER_OF_SPEEDS = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getNoOfSpeeds() + "', ");
			
			sbFieldQuery.append("SHAFT_TYPE = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getShaftType() + "', ");
			
			sbFieldQuery.append("SHAFT_DIAMETER_MM = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getShaftDiameter() + "', ");
			
			sbFieldQuery.append("SHAFT_EXTENSION_MM = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getShaftExtension() + "', ");
			
			sbFieldQuery.append("OUTLINE_DWG_NO = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getOutlineDrawingNum() + "', ");
			
			sbFieldQuery.append("STATOR_CONNECTION = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getStatorConnection() + "', ");
			
			sbFieldQuery.append("ROTOR_CONNECTION = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getRotorConnection() + "', ");
			
			sbFieldQuery.append("CONNECTION_DRAWING_NO = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getConnDrawingNum() + "', ");
			
			sbFieldQuery.append("OVERALL_LENGTH_MM = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getOverallLengthMM() + "', ");
			
			sbFieldQuery.append("STARTING_TYPE = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getStartingType() + "', ");
			
			sbFieldQuery.append("THRU_BOLTS_EXTENSION = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getThruBoltsExtension() + "', ");
			
			sbFieldQuery.append("TYPE_OF_OVERLOAD_PROTECTION = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getOverloadProtectionType() + "', ");
			
			sbFieldQuery.append("CE = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getCeValue() + "', ");
			
			sbFieldQuery.append("UL = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getUlValue() + "', ");
			
			sbFieldQuery.append("CSA = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getCsaValue() + "', ");
			
			sbFieldQuery.append("MOTOR_WEIGHT = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getMotorWeight() + "', ");
			
			sbFieldQuery.append("GROSS_WEIGHT = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getGrossWeight() + "', ");
			
			sbFieldQuery.append("DESIGN = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getDesign() + "', ");
			
			sbFieldQuery.append("LOAD_INERTIA = ");
			sbFieldQuery.append("'" + oNewTechnicalOfferDto.getLoadInertia() + "', ");
			
			if (sbFieldQuery.toString().trim().endsWith(","))
				sbFieldQuery = new StringBuffer(sbFieldQuery.toString().trim().substring(0, sbFieldQuery.toString().trim().lastIndexOf(",")));
			
			sbFieldQuery.append(" WHERE RATING_ID = " + oNewTechnicalOfferDto.getRatingId());
		}
		
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "sbFieldQuery ...... " + sbFieldQuery);
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
		
		return sbFieldQuery.toString();
	}
	
	private String buildNewTechOfferInsertQuery(NewTechnicalOfferDto oNewTechnicalOfferDto) {
		String sMethodName = "buildNewTechOfferInsertQuery";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		String sInsertQuery = null;
		StringBuffer sbFieldQuery = new StringBuffer();
		StringBuffer sbValuesQuery = new StringBuffer();
		
		sbFieldQuery.append(INSERT_NEW_TECHDATA_DETAILS);

		sbFieldQuery.append("(");
		sbValuesQuery.append(" VALUES (");

		/* Serial ID */
		sbFieldQuery.append("SERIAL_ID, ");
		sbValuesQuery.append("(SELECT MAX(SERIAL_ID) from QUOTATIONLT.RFQ_TECHNICAL_DATA )+1" + ", ");
		
		/* Rating ID */
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "RatingId -  " + oNewTechnicalOfferDto.getRatingId());
		sbFieldQuery.append("RATING_ID, ");
		sbValuesQuery.append(oNewTechnicalOfferDto.getRatingId() + ", ");
		
		sbFieldQuery.append("PRODUCT_GROUP, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getProductGroup() + "', ");
		
		sbFieldQuery.append("PRODUCT_LINE, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getProductLine() + "', ");
		
		sbFieldQuery.append("STANDARD, ");
		sbValuesQuery.append("'" + "N" + "', ");
		
		sbFieldQuery.append("POWER_RATING_KW, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getPowerRating_KW() + "', ");
		
		sbFieldQuery.append("FRAME_TYPE, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getFrameType() + "', ");
		
		sbFieldQuery.append("NUMBER_OF_POLES, ");
		sbValuesQuery.append(oNewTechnicalOfferDto.getNoOfPoles() + ", ");
		
		sbFieldQuery.append("MOUNTING_TYPE, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getMountingType() + "', ");
		
		sbFieldQuery.append("TB_POSITION, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getTbPosition() + "', ");
		
		sbFieldQuery.append("MODEL_NO, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getModelNumber() + "', ");
		
		sbFieldQuery.append("VOLTAGE_V, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getVoltage() + "', ");
		
		sbFieldQuery.append("VOLTAGE_ADD_VAR, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getVoltageAddVariation() + "', ");
		
		sbFieldQuery.append("VOLTAGE_REMOVE_VAR, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getVoltageRemoveVariation() + "', ");
		
		sbFieldQuery.append("FREQUENCY_HZ, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getFrequency() + "', ");
		
		sbFieldQuery.append("FREQUENCY_ADD_VAR, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getFrequencyAddVariation() + "', ");
		
		sbFieldQuery.append("FREQUENCY_REMOVE_VAR, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getFrequencyRemoveVariation() + "', ");
		
		sbFieldQuery.append("COMBINED_VAR, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getCombinedVariation() + "', ");
		
		sbFieldQuery.append("COOLING_METHOD, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getCoolingMethod() + "', ");
		
		sbFieldQuery.append("MOTOR_INERTIA, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getMotorInertia() + "', ");
		
		sbFieldQuery.append("VIBRATION, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getVibration() + "', ");
		
		sbFieldQuery.append("NOISE, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getNoise() + "', ");
		
		sbFieldQuery.append("NUMBER_OF_STARTS, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getNoOfStarts() + "', ");
		
		sbFieldQuery.append("LR_WITHSTAND_TIME, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getLrWithstandTime() + "', ");
		
		sbFieldQuery.append("DIRECTION_OD_ROTATION, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getDirectionOfRotation() + "', ");
		
		sbFieldQuery.append("POWER_RATING_HP, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getPowerRating_HP() + "', ");
		
		sbFieldQuery.append("CABLE_SIZE, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getCableSize() + "', ");
		
		sbFieldQuery.append("CURRENT_A, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getCurrent_A() + "', ");
		
		sbFieldQuery.append("SPEED_MAX, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getMaxSpeed() + "', ");
		
		sbFieldQuery.append("POWER_FACTOR_100, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getPowerFactor_100() + "', ");
		
		sbFieldQuery.append("POWER_FACTOR_75, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getPowerFactor_75() + "', ");
		
		sbFieldQuery.append("POWER_FACTOR_50, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getPowerFactor_50() + "', ");
		
		sbFieldQuery.append("EFFICIENCY_CLASS, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getEfficiencyClass() + "', ");
		
		sbFieldQuery.append("EFFICIENCY_100, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getEfficiency_100() + "', ");
		
		sbFieldQuery.append("EFFICIENCY_75, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getEfficiency_75() + "', ");
		
		sbFieldQuery.append("EFFICIENCY_50, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getEfficiency_50() + "', ");
		
		sbFieldQuery.append("STARTING_CURRENT, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getStartingCurrent() + "', ");
		
		sbFieldQuery.append("TORQUE, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getTorque() + "', ");
		
		sbFieldQuery.append("STARTING_TORQUE, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getStarting_torque() + "', ");
		
		sbFieldQuery.append("PULLOUT_TORQUE, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getPullout_torque() + "', ");
		
		sbFieldQuery.append("RA, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getRaValue() + "', ");
		
		sbFieldQuery.append("RV, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getRvValue() + "', ");
		
		sbFieldQuery.append("HAZ_AREA, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getHazArea() + "', ");
		
		sbFieldQuery.append("HAZ_LOCATION, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getHazLocation() + "', ");
		
		sbFieldQuery.append("ZONE_CLASSIFICATION, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getZoneClassification() + "', ");
		
		sbFieldQuery.append("GAS_GROUP, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getGasGroup() + "', ");
		
		sbFieldQuery.append("TEMP_CLASS, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getTemperatureClass() + "', ");
		
		sbFieldQuery.append("ROTOR_TYPE, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getRotortype() + "', ");
		
		sbFieldQuery.append("ELECTRICAL_TYPE, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getElectricalType() + "', ");
		
		sbFieldQuery.append("IP_CODE, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getIpCode() + "', ");
		
		sbFieldQuery.append("INSULATION_CLASS, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getInsulationClass() + "', ");
		
		sbFieldQuery.append("SERVICE_FACTOR, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getServiceFactor() + "', ");
		
		sbFieldQuery.append("DUTY, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getDuty() + "', ");
		
		sbFieldQuery.append("CDF, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getCdf() + "', ");
		
		sbFieldQuery.append("NUMBER_OF_STARTS_HR, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getNoOfStarts_HR() + "', ");
		
		sbFieldQuery.append("NUMBER_OF_PHASES, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getNoOfPhases() + "', ");
		
		sbFieldQuery.append("MAX_AMBIENT_C, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getMaxAmbientTemp() + "', ");
		
		sbFieldQuery.append("TEMP_RISE, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getTempRise() + "', ");
		
		sbFieldQuery.append("ALTITUDE, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getAltitude() + "', ");
		
		sbFieldQuery.append("DE_BEARING_SIZE, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getDeBearingSize() + "', ");
		
		sbFieldQuery.append("DE_BEARING_TYPE, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getDeBearingType() + "', ");
		
		sbFieldQuery.append("ODE_BEARING_SIZE, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getOdeBearingSize() + "', ");
		
		sbFieldQuery.append("ODE_BEARING_TYPE, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getOdeBearingType() + "', ");
		
		sbFieldQuery.append("ENCLOSURE_TYPE, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getEnclosureType() + "', ");
		
		sbFieldQuery.append("LUB_METHOD, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getLubMethod() + "', ");
		
		sbFieldQuery.append("TYPE_OF_GREASE, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getTypeOfGrease() + "', ");
		
		sbFieldQuery.append("MOTOR_ORIENTATION, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getMotorOrientation() + "', ");
		
		sbFieldQuery.append("FRAME_MATERIAL, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getFrameMaterial() + "', ");
		
		sbFieldQuery.append("FRAME_LENGTH, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getFrameLength() + "', ");
		
		sbFieldQuery.append("ROTATION, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getRotation() + "', ");
		
		sbFieldQuery.append("NUMBER_OF_SPEEDS, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getNoOfSpeeds() + "', ");
		
		sbFieldQuery.append("SHAFT_TYPE, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getShaftType() + "', ");
		
		sbFieldQuery.append("SHAFT_DIAMETER_MM, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getShaftDiameter() + "', ");
		
		sbFieldQuery.append("SHAFT_EXTENSION_MM, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getShaftExtension() + "', ");
		
		sbFieldQuery.append("OUTLINE_DWG_NO, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getOutlineDrawingNum() + "', ");
		
		sbFieldQuery.append("STATOR_CONNECTION, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getStatorConnection() + "', ");
		
		sbFieldQuery.append("ROTOR_CONNECTION, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getRotorConnection() + "', ");
		
		sbFieldQuery.append("CONNECTION_DRAWING_NO, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getConnDrawingNum() + "', ");
		
		sbFieldQuery.append("OVERALL_LENGTH_MM, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getOverallLengthMM() + "', ");
		
		sbFieldQuery.append("STARTING_TYPE, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getStartingType() + "', ");
		
		sbFieldQuery.append("THRU_BOLTS_EXTENSION, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getThruBoltsExtension() + "', ");
		
		sbFieldQuery.append("TYPE_OF_OVERLOAD_PROTECTION, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getOverloadProtectionType() + "', ");
		
		sbFieldQuery.append("CE, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getCeValue() + "', ");
		
		sbFieldQuery.append("UL, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getUlValue() + "', ");
		
		sbFieldQuery.append("CSA, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getCsaValue() + "', ");
		
		sbFieldQuery.append("MOTOR_WEIGHT, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getMotorWeight() + "', ");
		
		sbFieldQuery.append("GROSS_WEIGHT, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getGrossWeight() + "', ");
		
		sbFieldQuery.append("DESIGN, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getDesign() + "', ");
		
		sbFieldQuery.append("LOAD_INERTIA, ");
		sbValuesQuery.append("'" + oNewTechnicalOfferDto.getLoadInertia() + "', ");
		
		if (sbFieldQuery.toString().trim().endsWith(","))
			sbFieldQuery = new StringBuffer(sbFieldQuery.toString().trim().substring(0, sbFieldQuery.toString().trim().lastIndexOf(",")));

		if (sbValuesQuery.toString().trim().endsWith(","))
			sbValuesQuery = new StringBuffer(sbValuesQuery.toString().trim().substring(0, sbValuesQuery.toString().trim().lastIndexOf(",")));

		sbFieldQuery.append(")");
		sbValuesQuery.append(")");

		sInsertQuery = sbFieldQuery.toString() + sbValuesQuery.toString();
		
		
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "sbFieldQuery ...... " + sbFieldQuery);
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "sbValuesQuery ...... " + sbValuesQuery);
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "Final Query -  " + sInsertQuery);
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");

		return sInsertQuery;
	}
	
	public String deleteDraftEnquiryById(Connection conn, int iEnquiryId) throws Exception {

		String sMethodName = "deleteDraftEnquiryById";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        /* ResultSet object to store the rows retrieved from database */
		ResultSet rs = null;
		/* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();

        String sDeletedResult = "N";
        String sCheckQuery = " SELECT * FROM " + SCHEMA_QUOTATION + ".RFQ_LT_RATING_DETAILS WHERE QRD_LT_ENQUIRYID = ? ";
        String sCheckMTOAddOnQuery = " SELECT * FROM " + SCHEMA_QUOTATION + ".RFQ_LT_MTOADDON_DETAILS WHERE QRD_LT_MTOENQUIRYID = ? ";
        String sCheckMTORatingQuery = " SELECT * FROM " + SCHEMA_QUOTATION + ".RFQ_LT_MTORATING_DETAILS WHERE QRD_LT_MTOENQUIRYID = ? ";
        int iDelResult = 0;
        
        try {
        	// 1. DELETE all Records from : RFQ_LT_RATING_DETAILS
        	pstmt = conn.prepareStatement(sCheckQuery);
        	pstmt.setInt(1, iEnquiryId);
        	rs = pstmt.executeQuery();
        	if(rs.next()) {
        		pstmt = conn.prepareStatement(DELETE_NEWENQUIRY_RATINGS);
           	 	pstmt.setInt(1, iEnquiryId);
           	 	pstmt.executeUpdate();
        	}
        	
        	// 2. DELETE all Records from : RFQ_LT_MTOADDON_DETAILS
        	oDBUtility.releaseResources(rs, pstmt);
        	pstmt = conn.prepareStatement(sCheckMTOAddOnQuery);
        	pstmt.setInt(1, iEnquiryId);
        	rs = pstmt.executeQuery();
        	if(rs.next()) {
        		pstmt = conn.prepareStatement(DELETE_NEWENQUIRY_MTOADDONS);
           	 	pstmt.setInt(1, iEnquiryId);
           	 	pstmt.executeUpdate();
        	}
        	
        	// 3. DELETE all Records from : RFQ_LT_MTORATING_DETAILS
        	oDBUtility.releaseResources(rs, pstmt);
        	pstmt = conn.prepareStatement(sCheckMTORatingQuery);
        	pstmt.setInt(1, iEnquiryId);
        	rs = pstmt.executeQuery();
        	if(rs.next()) {
        		pstmt = conn.prepareStatement(DELETE_NEWENQUIRY_MTORATINGS);
           	 	pstmt.setInt(1, iEnquiryId);
           	 	pstmt.executeUpdate();
        	}
        	
        	oDBUtility.releaseResources(rs, pstmt);
        	pstmt = conn.prepareStatement(DELETE_ENQUIRY);
	    	pstmt.setInt(1, iEnquiryId);
	    	iDelResult = pstmt.executeUpdate();
	    	
	    	if(iDelResult == 1) {
	    		sDeletedResult = "Y";
        	}
	    	
        } finally {
        	oDBUtility.releaseResources(rs, pstmt);
        }
        
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
        return sDeletedResult;
	}
	
	public Integer fetchLastRatingNumber(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception {
		String sMethodName = "fetchLastRatingNumber";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        /* ResultSet object to store the rows retrieved from database */
		ResultSet rs = null;
		/* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        Integer iRatingNo = 0;
        String sCheckQuery = "SELECT MAX(QRD_LT_RATINGNO) AS QRD_LT_RATINGNO  FROM " + SCHEMA_QUOTATION + ".RFQ_LT_RATINGS_V WHERE QRD_LT_ENQUIRYID = ? ";
        
        try {
        	pstmt = conn.prepareStatement(sCheckQuery);
        	pstmt.setInt(1, oNewEnquiryDto.getEnquiryId());
        	rs = pstmt.executeQuery();
        	
        	if(rs.next()) {
        		iRatingNo = rs.getInt("QRD_LT_RATINGNO");
        	}
        } finally {
        	/* Releasing ResultSet & PreparedStatement objects */
        	oDBUtility.releaseResources(rs, pstmt);
        }
        
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
		return iRatingNo;
	}
	
	public NewRatingDto fetchLastRating(Connection conn, NewEnquiryDto oNewEnquiryDto, int iRatingNo) throws Exception {
		
		String sMethodName = "fetchLastRating";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null ;
        /* ResultSet object to store the rows retrieved from database */
		ResultSet rs = null;
		/* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        NewRatingDto oNewRatingDto = null;
        String sCheckQuery = "SELECT * FROM " + SCHEMA_QUOTATION + ".RFQ_LT_RATINGS_V WHERE QRD_LT_ENQUIRYID = ? AND QRD_LT_RATINGNO = ? "; 
        try {
        	pstmt = conn.prepareStatement(sCheckQuery);
    		pstmt.setInt(1, oNewEnquiryDto.getEnquiryId());
    		pstmt.setInt(2, iRatingNo);
    		rs = pstmt.executeQuery();
    		
    		if(rs.next()) {
    			oNewRatingDto = new NewRatingDto();
    			
    			oNewRatingDto.setRatingId(rs.getInt("QRD_LT_RATINGID"));
    			oNewRatingDto.setEnquiryId(rs.getInt("QRD_LT_ENQUIRYID"));
    			oNewRatingDto.setRatingNo(rs.getInt("QRD_LT_RATINGNO"));
    			oNewRatingDto.setQuantity(rs.getInt("QRD_LT_QUANTITY"));
    			
    			//oNewRatingDto.setLtProdGroupId(rs.getString("QRD_LT_PRODGROUPID"));
    			oNewRatingDto.setLtProdLineId(rs.getString("QRD_LT_PRODLINEID"));
    			//oNewRatingDto.setLtMotorTypeId(rs.getString("QRD_LT_MOTORTYPEID"));
    			oNewRatingDto.setLtKWId(rs.getString("QRD_LT_KW"));
    			oNewRatingDto.setLtPoleId(String.valueOf(rs.getInt("QRD_LT_POLE")));
    			oNewRatingDto.setLtFrameId(rs.getString("QRD_LT_FRAME"));
    			oNewRatingDto.setLtFrameSuffixId(rs.getString("QRD_LT_FRAME_SUFFIX"));
    			oNewRatingDto.setLtMountingId(rs.getString("QRD_LT_MOUNTINGID"));
    			oNewRatingDto.setLtTBPositionId(rs.getString("QRD_LT_TBPOSITIONID"));
    			
    			oNewRatingDto.setTagNumber(rs.getString("QRD_LT_TAG_NUMBER"));
    			oNewRatingDto.setLtManufacturingLocation(rs.getString("QRD_LT_MFGLOCATION"));
    			oNewRatingDto.setLtEffClass(rs.getString("QRD_LT_EFFICIENCYCLASS"));
    			oNewRatingDto.setLtVoltId(rs.getString("QRD_LT_VOLTAGEID"));
    			oNewRatingDto.setLtVoltAddVariation(rs.getString("QRD_LT_VOLTADDVARIATION"));
    			oNewRatingDto.setLtVoltRemoveVariation(rs.getString("QRD_LT_VOLTREMOVEVARIATION"));
    			oNewRatingDto.setLtFreqId(rs.getString("QRD_LT_FREQUENCYID"));
    			oNewRatingDto.setLtFreqAddVariation(rs.getString("QRD_LT_FREQADDVARIATION"));
    			oNewRatingDto.setLtFreqRemoveVariation(rs.getString("QRD_LT_FREQREMOVEVARIATION"));
    			oNewRatingDto.setLtCombVariation(rs.getString("QRD_LT_COMBINEDVARIATION"));
    			oNewRatingDto.setLtServiceFactor(rs.getString("QRD_LT_SERVICEFACTOR"));
    			oNewRatingDto.setLtMethodOfStarting(rs.getString("QRD_LT_METHODOFSTARTINGID"));
    			oNewRatingDto.setLtStartingCurrentOnDOLStart(rs.getString("QRD_LT_STARTINGCURRENTDOLID"));
    			oNewRatingDto.setLtAmbTempId(rs.getString("QRD_LT_AMBTEMPADDID"));
    			oNewRatingDto.setLtAmbTempRemoveId(rs.getString("QRD_LT_AMBTEMPREMOVEID"));
    			oNewRatingDto.setLtTempRise(rs.getString("QRD_LT_TEMPRISEID"));
    			oNewRatingDto.setLtInsulationClassId(rs.getString("QRD_LT_INSULATIONCLASSID"));
    			oNewRatingDto.setLtForcedCoolingId(rs.getString("QRD_LT_METHODOFCOOLING"));
    			oNewRatingDto.setLtAltitude(rs.getString("QRD_LT_ALTITUDE"));
    			oNewRatingDto.setLtHazardAreaId(rs.getString("QRD_LT_HAZARDAREAID"));
    			oNewRatingDto.setLtHazardAreaTypeId(rs.getString("QRD_LT_HAZARDAREATYPEID"));
    			oNewRatingDto.setLtGasGroupId(rs.getString("QRD_LT_GASGROUPID"));
    			oNewRatingDto.setLtHazardZoneId(rs.getString("QRD_LT_HAZARDZONEID"));
    			oNewRatingDto.setLtDustGroupId(rs.getString("QRD_LT_DUSTGROUPID"));
    			oNewRatingDto.setLtTempClass(rs.getString("QRD_LT_TEMPCLASSID"));
    			oNewRatingDto.setLtApplicationId(rs.getString("QRD_LT_APPLICATIONID"));
    			oNewRatingDto.setLtDutyId(rs.getString("QRD_LT_DUTYID"));
    			oNewRatingDto.setLtCDFId(rs.getString("QRD_LT_CDFID"));
    			oNewRatingDto.setLtStartsId(rs.getString("QRD_LT_STARTSID"));
    			oNewRatingDto.setLtNoiseLevel(rs.getString("QRD_LT_NOISELEVEL"));
    			
    			/* **********************************************	Price Fields	************************************************  */
    			oNewRatingDto.setLtTotalAddonPercent(rs.getString("QRD_LT_TOTALADDONPERCENT"));
    			oNewRatingDto.setLtTotalCashExtra(rs.getString("QRD_LT_TOTALCASHEXTRA"));
    			oNewRatingDto.setLtLPPerUnit(rs.getString("QRD_LT_LISTPRICE"));
    			oNewRatingDto.setLtLPWithAddonPerUnit(rs.getString("QRD_LT_LISTPRICEWITHADDON"));
    			oNewRatingDto.setLtStandardDiscount(rs.getString("QRD_LT_STANDARDDISCOUNT"));
    			oNewRatingDto.setLtPricePerUnit(rs.getString("QRD_LT_PRICEPERUNIT"));
    			oNewRatingDto.setLtTotalOrderPrice(rs.getString("QRD_LT_TOTALORDERPRICE"));
    			oNewRatingDto.setLtRequestedDiscount_SM(rs.getString("QRD_LT_REQUESTEDDISCOUNT_SM"));
    			oNewRatingDto.setLtRequestedPricePerUnit_SM(rs.getString("QRD_LT_REQ_PRICEPERUNIT_SM"));
    			oNewRatingDto.setLtRequestedTotalPrice_SM(rs.getString("QRD_LT_REQ_TOTALPRICE_SM"));
    			oNewRatingDto.setLtRequestedDiscount_RSM(rs.getString("QRD_REQUESTEDDISCOUNT_RSM"));
    			oNewRatingDto.setLtRequestedPricePerUnit_RSM(rs.getString("QRD_REQ_PRICEPERUNIT_RSM"));
    			oNewRatingDto.setLtRequestedTotalPrice_RSM(rs.getString("QRD_RED_TOTALPRICE_RSM"));
    			oNewRatingDto.setLtRequestedDiscount_NSM(rs.getString("QRD_REQUESTEDDISCOUNT_NSM"));
    			oNewRatingDto.setLtRequestedPricePerUnit_NSM(rs.getString("QRD_REQ_PRICEPERUNIT_NSM"));
    			oNewRatingDto.setLtRequestedTotalPrice_NSM(rs.getString("QRD_REQ_TOTALPRICE_NSM"));
    			oNewRatingDto.setLtApprovedDiscount_RSM(rs.getString("QRD_LT_APPROVEDDISCOUNT_RSM"));
    			oNewRatingDto.setLtApprovedPricePerunit_RSM(rs.getString("QRD_LT_APPRV_PRICEPERUNIT_RSM"));
    			oNewRatingDto.setLtApprovedTotalPrice_RSM(rs.getString("QRD_LT_APPRV_TOTALPRICE_RSM"));
    			oNewRatingDto.setLtApprovedDiscount_NSM(rs.getString("QRD_APPROVEDDISCOUNT_NSM"));
    			oNewRatingDto.setLtApprovedPricePerunit_NSM(rs.getString("QRD_APPRV_PRICEPERUNIT_NSM"));
    			oNewRatingDto.setLtApprovedTotalPrice_NSM(rs.getString("QRD_APPRV_TOTALPRICE_NSM"));
    			oNewRatingDto.setLtApprovedDiscount_MH(rs.getString("QRD_APPROVEDDISCOUNT_MH"));
    			oNewRatingDto.setLtApprovedPricePerunit_MH(rs.getString("QRD_APPRV_PRICEPERUNIT_MH"));
    			oNewRatingDto.setLtApprovedTotalPrice_MH(rs.getString("QRD_APPRV_TOTALPRICE_MH"));
    			oNewRatingDto.setLtQuotedDiscount(rs.getString("QRD_LT_QUOTEDDISCOUNT"));
    			oNewRatingDto.setLtQuotedPricePerUnit(rs.getString("QRD_LT_QUOTED_PRICEPERUNIT"));
    			oNewRatingDto.setLtQuotedTotalPrice(rs.getString("QRD_LT_QUOTED_TOTALPRICE"));
    			
    			/* **********************************************	Standard & Customer Req. Delivery Fields	********************  */
    			oNewRatingDto.setLtEarlierMotorSerialNo(rs.getString("QRD_LT_EARLIER_SUPPLIED_MOTOR"));
    			oNewRatingDto.setLtReplacementMotor(rs.getString("QRD_LT_REPLACEMENTMOTOR"));
    			oNewRatingDto.setLtStandardDeliveryWks(rs.getString("QRD_LT_STANDARD_DELIVERY"));
    			oNewRatingDto.setLtCustReqDeliveryWks(rs.getString("QRD_LT_CUSTOMER_REQ_DELIVERY"));
    			
    			/* **********************************************	Electrical Fields	********************************************  */
    			oNewRatingDto.setLtRVId(rs.getString("QRD_LT_RV"));
    			oNewRatingDto.setLtRAId(rs.getString("QRD_LT_RA"));
    			oNewRatingDto.setLtWindingTreatmentId(rs.getString("QRD_LT_WINDINGTREATMENTID"));
    			oNewRatingDto.setLtWindingWire(rs.getString("QRD_LT_WINDINGWIREID"));
    			oNewRatingDto.setLtLeadId(rs.getString("QRD_LT_LEADID"));
    			oNewRatingDto.setLtWindingConfig(rs.getString("QRD_LT_WINDINGCONFIGID"));
    			oNewRatingDto.setLtLoadGD2Value(rs.getString("QRD_LT_LOADGD2"));
    			oNewRatingDto.setLtVFDApplTypeId(rs.getString("QRD_LT_VFDTYPEID"));
    			oNewRatingDto.setLtVFDMotorSpeedRangeMin(rs.getString("QRD_LT_VFDSPEEDMINID"));
    			oNewRatingDto.setLtVFDMotorSpeedRangeMax(rs.getString("QRD_LT_VFDSPEEDMAXID"));
    			oNewRatingDto.setLtOverloadingDuty(rs.getString("QRD_LT_OVERLOADDUTYID"));
    			oNewRatingDto.setLtConstantEfficiencyRange(rs.getString("QRD_LT_CONSTEFFRANGEID"));
    			
    			/* **********************************************	Mechanical Fields	********************************************  */
    			oNewRatingDto.setLtShaftTypeId(rs.getString("QRD_LT_SHAFTTYPEID"));
    			oNewRatingDto.setLtShaftMaterialId(rs.getString("QRD_LT_SHAFTMATERIALID"));
    			oNewRatingDto.setLtDirectionOfRotation(rs.getString("QRD_LT_ROTATIONDIRECTIONID"));
    			oNewRatingDto.setLtStandardRotation(rs.getString("QRD_LT_STANDARDROTATION"));
    			oNewRatingDto.setLtMethodOfCoupling(rs.getString("QRD_LT_METHODOFCOUPLINGID"));
    			oNewRatingDto.setLtPaintingTypeId(rs.getString("QRD_LT_PAINTTYPEID"));
    			oNewRatingDto.setLtPaintShadeId(rs.getString("QRD_LT_PAINTSHADEID"));
    			oNewRatingDto.setLtPaintShadeValue(rs.getString("QRD_LT_PAINTSHADE_VALUE"));
    			oNewRatingDto.setLtPaintThicknessId(rs.getString("QRD_LT_PAINTTHICKESSID"));
    			oNewRatingDto.setLtIPId(rs.getString("QRD_LT_IPID"));
    			oNewRatingDto.setLtCableSizeId(rs.getString("QRD_LT_CABLESIZEID"));
    			oNewRatingDto.setLtNoOfRuns(rs.getString("QRD_LT_NOOFRUNSID"));
    			oNewRatingDto.setLtNoOfCores(rs.getString("QRD_LT_NOOFCORESID"));
    			oNewRatingDto.setLtCrossSectionAreaId(rs.getString("QRD_LT_CROSSSECTIONID"));
    			oNewRatingDto.setLtConductorMaterialId(rs.getString("QRD_LT_CONDMATERIALID"));
    			oNewRatingDto.setLtCableDiameter(rs.getString("QRD_LT_CABLEDIAMETER"));
    			oNewRatingDto.setLtTerminalBoxSizeId(rs.getString("QRD_LT_TERMINALBOXSIZEID"));
    			oNewRatingDto.setLtSpreaderBoxId(rs.getString("QRD_LT_SPREADERBOX"));
    			oNewRatingDto.setLtSpaceHeaterId(rs.getString("QRD_LT_SPACEHEATER"));
    			oNewRatingDto.setLtVibrationId(rs.getString("QRD_LT_VIBRATION"));
    			oNewRatingDto.setLtFlyingLeadWithoutTDId(rs.getString("QRD_LT_FLYINGLEADWITHOUTTB"));
    			oNewRatingDto.setLtFlyingLeadWithTDId(rs.getString("QRD_LT_FLYINGLEADWITHTB"));
    			oNewRatingDto.setLtMetalFanId(rs.getString("QRD_LT_METALFAN"));
    			oNewRatingDto.setLtTechoMounting(rs.getString("QRD_LT_TECHOMOUNTING"));
    			oNewRatingDto.setLtShaftGroundingId(rs.getString("QRD_LT_SHAFTGROUNDING"));
    			
    			/* **********************************************	Accessories Fields	********************************************  */
    			oNewRatingDto.setLtHardware(rs.getString("QRD_LT_HARDWARE"));
    			oNewRatingDto.setLtGlandPlateId(rs.getString("QRD_LT_GLANDPLATE"));
    			oNewRatingDto.setLtDoubleCompressionGlandId(rs.getString("QRD_LT_DOUBLECOMPRESSION"));
    			oNewRatingDto.setLtSPMMountingProvisionId(rs.getString("QRD_LT_SPMMOUNTING"));
    			oNewRatingDto.setLtAddNamePlateId(rs.getString("QRD_LT_ADDLNAMEPLATE"));
    			oNewRatingDto.setLtDirectionArrowPlateId(rs.getString("QRD_LT_DIRECTIONARROWPLATE"));
    			oNewRatingDto.setLtRTDId(rs.getString("QRD_LT_RTDID"));
    			oNewRatingDto.setLtBTDId(rs.getString("QRD_LT_BTDID"));
    			oNewRatingDto.setLtThermisterId(rs.getString("QRD_LT_THERMISTERID"));
    			oNewRatingDto.setLtAuxTerminalBoxId(rs.getString("QRD_LT_AUXTERMINALBOX"));
    			oNewRatingDto.setLtCableSealingBoxId(rs.getString("QRD_LT_CABLESEALINGBOX"));
    			
    			/* **********************************************	Bearings Fields	************************************************  */
    			oNewRatingDto.setLtBearingSystemId(rs.getString("QRD_LT_BEARINGSYSTEMID"));
    			oNewRatingDto.setLtBearingNDEId(rs.getString("QRD_LT_BEARINGNDEID"));
    			oNewRatingDto.setLtBearingDEId(rs.getString("QRD_LT_BEARINGDEID"));
    			
    			/* **********************************************	Certificate, Spares, Tests Fields	****************************  */
    			oNewRatingDto.setLtWitnessRoutineId(rs.getString("QRD_LT_WITNESSROUTINE"));
    			oNewRatingDto.setLtAdditionalTest1Id(rs.getString("QRD_LT_ADDL_TEST1"));
    			oNewRatingDto.setLtAdditionalTest2Id(rs.getString("QRD_LT_ADDL_TEST2"));
    			oNewRatingDto.setLtAdditionalTest3Id(rs.getString("QRD_LT_ADDL_TEST3"));
    			oNewRatingDto.setLtTypeTestId(rs.getString("QRD_LT_TYPETEST"));
    			oNewRatingDto.setLtULCEId(rs.getString("QRD_LT_CERTIFICATION"));
    			oNewRatingDto.setLtQAPId(rs.getString("QRD_LT_QAPID"));
    			oNewRatingDto.setLtSpares1(rs.getString("QRD_LT_SPARES1"));
    			oNewRatingDto.setLtSpares2(rs.getString("QRD_LT_SPARES2"));
    			oNewRatingDto.setLtSpares3(rs.getString("QRD_LT_SPARES3"));
    			oNewRatingDto.setLtSpares4(rs.getString("QRD_LT_SPARES4"));
    			oNewRatingDto.setLtSpares5(rs.getString("QRD_LT_SPARES5"));
    			oNewRatingDto.setLtDataSheet(rs.getString("QRD_LT_DATASHEET"));
    			oNewRatingDto.setLtMotorGA(rs.getString("QRD_LT_MOTORGA"));
    			oNewRatingDto.setLtPerfCurves(rs.getString("QRD_LT_PERFCURVE"));
    			oNewRatingDto.setLtTBoxGA(rs.getString("QRD_LT_TBOXGA"));
    			oNewRatingDto.setLtAdditionalComments(rs.getString("QRD_LT_ADDL_COMMENTS"));
    			
    			
    			oNewRatingDto.setFlg_PriceFields_MTO(rs.getString("FLG_PRICEFIELDS_MTO"));
    			oNewRatingDto.setFlg_Addon_Leads_MTO(rs.getString("FLG_ADDON_LEADS_MTO"));
    			oNewRatingDto.setFlg_Addon_TBoxSize_MTO(rs.getString("FLG_ADDON_TBOXSIZE_MTO"));
    			oNewRatingDto.setFlg_Addon_BearingDE_MTO(rs.getString("FLG_ADDON_BEARINGDE_MTO"));
    			oNewRatingDto.setFlg_Addon_CableEntry_MTO(rs.getString("FLG_ADDON_CABLEENTRY_MTO"));
    			oNewRatingDto.setFlg_Txt_MotorNo_MTO(rs.getString("FLG_TXT_MOTORNO_MTO"));
    			oNewRatingDto.setFlg_Txt_RV_MTO(rs.getString("FLG_TXT_RV_MTO"));
    			oNewRatingDto.setFlg_Txt_RA_MTO(rs.getString("FLG_TXT_RA_MTO"));
    			oNewRatingDto.setFlg_Txt_LoadGD2_MTO(rs.getString("FLG_TXT_LOADGD2_MTO"));
    			oNewRatingDto.setFlg_Txt_NonSTDPaint_MTO(rs.getString("FLG_TXT_NONSTDPAINT_MTO"));
    			
    			oNewRatingDto.setIsReferDesign(rs.getString("QRD_LT_ISREFERDESIGN"));
    		}
        } finally {
        	/* Releasing ResultSet & PreparedStatement objects */
        	oDBUtility.releaseResources(rs, pstmt);
        }
		
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
		return oNewRatingDto;
	}
	
	public String saveAbondentEnquiry(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception {
		String sMethodName = "saveAbondentEnquiry";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null, pstmt1 = null;
		/* ResultSet object to store the rows retrieved from database */
		ResultSet rs = null, rs1 = null;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();
		
		int iUpdateRes = 0;
		String sResult = QuotationConstants.QUOTATION_STRING_Y;
		
		try {
			String sChkQuery = " SELECT * FROM " + SCHEMA_QUOTATION + ".RFQ_ENQUIRY_MASTER WHERE QEM_ENQUIRYID = ? ";
			pstmt = conn.prepareStatement(sChkQuery);
			pstmt.setInt(1, oNewEnquiryDto.getEnquiryId());
			rs = pstmt.executeQuery();
			
			if (rs.next()) {
				pstmt1 = conn.prepareStatement(UPDATE_NEWENQUIRY_ABONDENT_DETAILS);
				
				pstmt1.setString(1, oNewEnquiryDto.getAbondentReason1());
				pstmt1.setString(2, oNewEnquiryDto.getAbondentReason2());
				pstmt1.setString(3, oNewEnquiryDto.getAbondentRemarks());
				pstmt1.setString(4, oNewEnquiryDto.getAbondentCustomerApproval());
				pstmt1.setInt(5, QuotationConstants.QUOTATION_WORKFLOW_STATUS_ABONDENT);
				pstmt1.setInt(6, oNewEnquiryDto.getEnquiryId());
				
				iUpdateRes = pstmt1.executeUpdate();
				
				if(iUpdateRes == 0) {
					sResult = QuotationConstants.QUOTATION_STRING_N;
				}
			}
		} finally {
			oDBUtility.releaseResources(rs, pstmt);
			oDBUtility.releaseResources(pstmt1);
		}
		
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
		return sResult;
	}
	
	public String saveLostEnquiryDetails(Connection conn, NewEnquiryDto oNewEnquiryDto, List<NewRatingDto> alRatingLostDetailsList) throws Exception {
		String sMethodName = "saveLostEnquiryDetails";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null, pstmt1 = null, pstmt2 = null;
		/* ResultSet object to store the rows retrieved from database */
		ResultSet rs = null, rs1 = null;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();
		
		int iUpdateRes = 0;
		ArrayList<Integer> alUpdateResult = new ArrayList<>();
		NewRatingDto oTempNewRatingDto = null;
		String sResult = QuotationConstants.QUOTATION_STRING_Y;
		String sUpdateRatingQuery = "UPDATE " + SCHEMA_QUOTATION + ".RFQ_LT_RATING_DETAILS SET QRD_LT_LOST_PRICEPERUNIT = ?, QRD_LT_LOST_TOTALPRICE = ? WHERE QRD_LT_ENQUIRYID = ? AND QRD_LT_RATINGNO = ? ";
		String sChkQuery = "SELECT * FROM " + SCHEMA_QUOTATION + ".RFQ_LT_RATING_DETAILS WHERE QRD_LT_ENQUIRYID = ? AND QRD_LT_RATINGNO = ? ";
		pstmt = conn.prepareStatement(sChkQuery);
		
		try {
			if(CollectionUtils.isNotEmpty(alRatingLostDetailsList)) {
				for(int iCount = 0; iCount < alRatingLostDetailsList.size(); iCount++) {
					oTempNewRatingDto = (NewRatingDto) alRatingLostDetailsList.get(iCount);
					
					pstmt.setInt(1, oTempNewRatingDto.getEnquiryId());
					pstmt.setInt(2, oTempNewRatingDto.getRatingNo());
					rs = pstmt.executeQuery();
					
					if(rs.next()) { 
						pstmt1 = conn.prepareStatement(sUpdateRatingQuery);
						pstmt1.setString(1, oTempNewRatingDto.getLtLostPricePerUnit());
						pstmt1.setString(2, oTempNewRatingDto.getLtLostTotalPrice());
						pstmt1.setInt(3, oTempNewRatingDto.getEnquiryId());
						pstmt1.setInt(4, oTempNewRatingDto.getRatingNo());
						
						iUpdateRes = pstmt1.executeUpdate();
						alUpdateResult.add(iUpdateRes);
					}
					oDBUtility.releaseResources(rs);
				}
				for(Integer iUpdateVal : alUpdateResult) {
					if(iUpdateVal == 0) {
						sResult=QuotationConstants.QUOTATION_STRING_N;
						break;
					}
				}
				
				if(QuotationConstants.QUOTATION_STRING_Y.equalsIgnoreCase(sResult)) {
					pstmt2 = conn.prepareStatement(UPDATE_NEWENQUIRY_LOST_DETAILS);
					
					pstmt2.setString(1, oNewEnquiryDto.getLostCompetitor());
					pstmt2.setString(2, oNewEnquiryDto.getLostReason1());
					pstmt2.setString(3, oNewEnquiryDto.getLostReason2());
					pstmt2.setString(4, oNewEnquiryDto.getLostRemarks());
					pstmt2.setString(5, oNewEnquiryDto.getLostManagerApproval());
					pstmt2.setString(6, oNewEnquiryDto.getTotalQuotedPrice());
					pstmt2.setString(7, oNewEnquiryDto.getTotalLostPrice());
					pstmt2.setInt(8, QuotationConstants.QUOTATION_WORKFLOW_STATUS_LOST);
					pstmt2.setInt(9, oNewEnquiryDto.getEnquiryId());
					
					iUpdateRes = pstmt2.executeUpdate();
					
					if(iUpdateRes == 0) {
						sResult = QuotationConstants.QUOTATION_STRING_N;
					}
				}
			}
		} finally {
			oDBUtility.releaseResources(pstmt1);
			oDBUtility.releaseResources(rs, pstmt);
			oDBUtility.releaseResources(pstmt2);
		}
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
		return sResult;
	}
	
	public String saveWonEnquiryDetails(Connection conn, NewEnquiryDto oNewEnquiryDto, List<NewRatingDto> alRatingWonDetailsList) throws Exception {
		String sMethodName = "saveLostEnquiryDetails";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null, pstmt1 = null, pstmt2 = null;
		/* ResultSet object to store the rows retrieved from database */
		ResultSet rs = null, rs1 = null;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();
		
		int iUpdateRes = 0;
		ArrayList<Integer> alUpdateResult = new ArrayList<>();
		NewRatingDto oTempNewRatingDto = null;
		String sResult = QuotationConstants.QUOTATION_STRING_Y;
		String sUpdateRatingQuery = "UPDATE " + SCHEMA_QUOTATION + ".RFQ_LT_RATING_DETAILS SET QRD_LT_WON_PRICEPERUNIT = ?, QRD_LT_WON_TOTALPRICE = ? WHERE QRD_LT_ENQUIRYID = ? AND QRD_LT_RATINGNO = ? ";
		String sChkQuery = "SELECT * FROM " + SCHEMA_QUOTATION + ".RFQ_LT_RATING_DETAILS WHERE QRD_LT_ENQUIRYID = ? AND QRD_LT_RATINGNO = ? ";
		pstmt = conn.prepareStatement(sChkQuery);
		
		try {
			if(CollectionUtils.isNotEmpty(alRatingWonDetailsList)) {
				for(int iCount = 0; iCount < alRatingWonDetailsList.size(); iCount++) {
					oTempNewRatingDto = (NewRatingDto) alRatingWonDetailsList.get(iCount);
					
					pstmt.setInt(1, oTempNewRatingDto.getEnquiryId());
					pstmt.setInt(2, oTempNewRatingDto.getRatingNo());
					rs = pstmt.executeQuery();
					
					if(rs.next()) {
						pstmt1 = conn.prepareStatement(sUpdateRatingQuery);
						pstmt1.setString(1, oTempNewRatingDto.getLtWonPricePerUnit());
						pstmt1.setString(2, oTempNewRatingDto.getLtWonTotalPrice());
						pstmt1.setInt(3, oTempNewRatingDto.getEnquiryId());
						pstmt1.setInt(4, oTempNewRatingDto.getRatingNo());
						
						iUpdateRes = pstmt1.executeUpdate();
						alUpdateResult.add(iUpdateRes);
					}
					oDBUtility.releaseResources(rs);
				}
				for(Integer iUpdateVal : alUpdateResult) {
					if(iUpdateVal == 0) {
						sResult=QuotationConstants.QUOTATION_STRING_N;
						break;
					}
				}
				
				// Save WON Details in Enquiry Table.
				if(QuotationConstants.QUOTATION_STRING_Y.equalsIgnoreCase(sResult)) {
					pstmt2 = conn.prepareStatement(UPDATE_NEWENQUIRY_WON_DETAILS);
					
					pstmt2.setString(1, oNewEnquiryDto.getWonIndentNo());
					pstmt2.setString(2, oNewEnquiryDto.getWonIsPOorLOI());
					pstmt2.setString(3, oNewEnquiryDto.getWonPONo());
					pstmt2.setString(4, oNewEnquiryDto.getWonPODate());
					pstmt2.setString(5, oNewEnquiryDto.getWonPOReceiptDate());
					pstmt2.setString(6, oNewEnquiryDto.getWonReason1());
					pstmt2.setString(7, oNewEnquiryDto.getWonReason2());
					pstmt2.setString(8, oNewEnquiryDto.getWonRemarks());
					pstmt2.setString(9, oNewEnquiryDto.getTotalQuotedPrice());
					pstmt2.setString(10, oNewEnquiryDto.getTotalWonPrice());
					pstmt2.setString(11, oNewEnquiryDto.getWonRequiresDrawing());
					pstmt2.setString(12, oNewEnquiryDto.getWonRequiresDatasheet());
					pstmt2.setString(13, oNewEnquiryDto.getWonRequiresQAP());
					pstmt2.setString(14, oNewEnquiryDto.getWonRequiresPerfCurves());
					pstmt2.setInt(15, QuotationConstants.QUOTATION_WORKFLOW_STATUS_WON);
					pstmt2.setInt(16, oNewEnquiryDto.getEnquiryId());
					
					iUpdateRes = pstmt2.executeUpdate();
					
					if(iUpdateRes == 0) {
						sResult = QuotationConstants.QUOTATION_STRING_N;
					}
				}
			}
		} finally {
			oDBUtility.releaseResources(pstmt1);
			oDBUtility.releaseResources(rs, pstmt);
			oDBUtility.releaseResources(pstmt2);
		}
		
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
		return sResult;
	}
	
	public UserDetailsDto getUserDetailsById(Connection conn, String sReassignToId) throws Exception {
		String sMethodName = "getUserDetailsById";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt1 = null;
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs1 = null;
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        UserDetailsDto oUserDetailsDto = null;
        
        try {
        	pstmt1 = conn.prepareStatement(FETCH_USER_DETAILS_BY_ID);
        	pstmt1.setString(1, sReassignToId);
    		rs1 = pstmt1.executeQuery();
    		
    		if(rs1.next()) {
    			oUserDetailsDto = new UserDetailsDto();
    			
    			oUserDetailsDto.setUserDetailsSSO(rs1.getString("QUD_LT_USER_SSO"));
    			oUserDetailsDto.setUserDetailsType(rs1.getString("QUD_LT_USER_TYPE"));
    		}
        } finally {
    		/* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs1, pstmt1);
    	}
    	
    	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
    	return oUserDetailsDto;
	}
	
	public void updateNewRatingMTOPriceDetails(Connection conn, NewRatingDto oNewRatingDto) throws Exception {
		String sMethodName = "updateNewRatingMTOPriceDetails";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt1 = null;
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        try {
        	pstmt1 = conn.prepareStatement(UPDATE_NEWRATING_MTO_PRICEDETAILS);
        	pstmt1.setString(1, oNewRatingDto.getLtTotalAddonPercent());
        	pstmt1.setString(2, oNewRatingDto.getLtTotalCashExtra());
        	pstmt1.setString(3, oNewRatingDto.getLtLPPerUnit());
        	pstmt1.setString(4, oNewRatingDto.getLtLPWithAddonPerUnit());
        	pstmt1.setString(5, oNewRatingDto.getLtStandardDiscount());
        	pstmt1.setString(6, oNewRatingDto.getLtPricePerUnit());
        	pstmt1.setString(7, oNewRatingDto.getLtTotalOrderPrice());
        	pstmt1.setString(8, oNewRatingDto.getIsDesignComplete());
        	pstmt1.setString(9, oNewRatingDto.getIsReferDesign());
        	pstmt1.setString(10, oNewRatingDto.getFlg_PriceFields_MTO());
        	pstmt1.setString(11, oNewRatingDto.getRatingMaterialCost());
        	pstmt1.setString(12, oNewRatingDto.getRatingLOH());
        	pstmt1.setString(13, oNewRatingDto.getRatingTotalCost());
        	pstmt1.setString(14, oNewRatingDto.getRatingProfitMargin());
        	pstmt1.setInt(15, oNewRatingDto.getEnquiryId());
        	pstmt1.setInt(16, oNewRatingDto.getRatingNo());
        	
        	pstmt1.executeUpdate();
        	
        } finally {
    		/* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(pstmt1);
    	}
    	
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
	}
	
	public void updateNewMTORatingDetails(Connection conn, NewMTORatingDto oNewMTORatingDto) throws Exception {
		String sMethodName = "updateNewRatingMTOPriceDetails";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt1 = null;
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        try {
        	pstmt1 = conn.prepareStatement(UPDATE_NEWMTORATING_DETAILS);
        	pstmt1.setString(1, oNewMTORatingDto.getMtoProdLineId());
        	pstmt1.setString(2, oNewMTORatingDto.getMtoKWId());
        	pstmt1.setString(3, oNewMTORatingDto.getMtoPoleId());
        	pstmt1.setString(4, oNewMTORatingDto.getMtoFrameId());
        	pstmt1.setString(5, oNewMTORatingDto.getMtoFrameSuffixId());
        	pstmt1.setString(6, oNewMTORatingDto.getMtoMountingId());
        	pstmt1.setString(7, oNewMTORatingDto.getMtoTBPositionId());
        	pstmt1.setString(8, oNewMTORatingDto.getMtoMfgLocation());
        	pstmt1.setString(9, oNewMTORatingDto.getMtoSpecialAddOnPercent());
        	pstmt1.setString(10, oNewMTORatingDto.getMtoSpecialCashExtra());
        	pstmt1.setString(11, oNewMTORatingDto.getMtoPercentCopper());
        	pstmt1.setString(12, oNewMTORatingDto.getMtoPercentStamping());
        	pstmt1.setString(13, oNewMTORatingDto.getMtoPercentAluminium());
        	pstmt1.setString(14, oNewMTORatingDto.getMtoTotalAddOnPercent());
        	pstmt1.setString(15, oNewMTORatingDto.getMtoTotalCashExtra());
        	pstmt1.setString(16, "Yes");
        	pstmt1.setInt(17, oNewMTORatingDto.getMtoEnquiryId());
        	pstmt1.setInt(18, oNewMTORatingDto.getMtoRatingNo());
        	
        	pstmt1.executeUpdate();
        	
        } finally {
    		/* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(pstmt1);
    	}
    	
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
	}
	
	public NewRatingDto fetchNewRatingByIdandNo(Connection conn, NewRatingDto oNewRatingDto) throws Exception {
		String sMethodName = "fetchNewRatingByIdandNo";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
        PreparedStatement pstmt = null;
        /* ResultSet object to store the rows retrieved from database */
        ResultSet rs = null ;
        /* Object of DBUtility class to handle database operations */
        DBUtility oDBUtility = new DBUtility();
        
        NewRatingDto oFetchNewRatingDto = null;
        
        try {
        	pstmt = conn.prepareStatement(GET_NEWRATING_BY_IDANDNO);
    		pstmt.setInt(1, oNewRatingDto.getEnquiryId());
    		pstmt.setInt(2, oNewRatingDto.getRatingNo());
    		rs = pstmt.executeQuery();
    		
    		if(rs.next()) {
    			oFetchNewRatingDto = new NewRatingDto();
    			
    			oFetchNewRatingDto.setRatingId(rs.getInt("QRD_LT_RATINGID"));
    			oFetchNewRatingDto.setEnquiryId(rs.getInt("QRD_LT_ENQUIRYID"));
    			oFetchNewRatingDto.setRatingNo(rs.getInt("QRD_LT_RATINGNO"));
    			oFetchNewRatingDto.setQuantity(rs.getInt("QRD_LT_QUANTITY"));
    			
    			//oFetchNewRatingDto.setLtProdGroupId(rs.getString("QRD_LT_PRODGROUPID"));
    			oFetchNewRatingDto.setLtProdLineId(rs.getString("QRD_LT_PRODLINEID"));
    			//oFetchNewRatingDto.setLtMotorTypeId(rs.getString("QRD_LT_MOTORTYPEID"));
    			oFetchNewRatingDto.setLtKWId(rs.getString("QRD_LT_KW"));
    			oFetchNewRatingDto.setLtPoleId(String.valueOf(rs.getInt("QRD_LT_POLE")));
    			oFetchNewRatingDto.setLtFrameId(rs.getString("QRD_LT_FRAME"));
    			oFetchNewRatingDto.setLtFrameSuffixId(rs.getString("QRD_LT_FRAME_SUFFIX"));
    			oFetchNewRatingDto.setLtMountingId(rs.getString("QRD_LT_MOUNTINGID"));
    			oFetchNewRatingDto.setLtTBPositionId(rs.getString("QRD_LT_TBPOSITIONID"));
    			
    			oFetchNewRatingDto.setTagNumber(rs.getString("QRD_LT_TAG_NUMBER"));
    			oFetchNewRatingDto.setLtManufacturingLocation(rs.getString("QRD_LT_MFGLOCATION"));
    			oFetchNewRatingDto.setLtEffClass(rs.getString("QRD_LT_EFFICIENCYCLASS"));
    			oFetchNewRatingDto.setLtVoltId(rs.getString("QRD_LT_VOLTAGEID"));
    			oFetchNewRatingDto.setLtVoltAddVariation(rs.getString("QRD_LT_VOLTADDVARIATION"));
    			oFetchNewRatingDto.setLtVoltRemoveVariation(rs.getString("QRD_LT_VOLTREMOVEVARIATION"));
    			oFetchNewRatingDto.setLtFreqId(rs.getString("QRD_LT_FREQUENCYID"));
    			oFetchNewRatingDto.setLtFreqAddVariation(rs.getString("QRD_LT_FREQADDVARIATION"));
    			oFetchNewRatingDto.setLtFreqRemoveVariation(rs.getString("QRD_LT_FREQREMOVEVARIATION"));
    			oFetchNewRatingDto.setLtCombVariation(rs.getString("QRD_LT_COMBINEDVARIATION"));
    			oFetchNewRatingDto.setLtServiceFactor(rs.getString("QRD_LT_SERVICEFACTOR"));
    			oFetchNewRatingDto.setLtMethodOfStarting(rs.getString("QRD_LT_METHODOFSTARTINGID"));
    			oFetchNewRatingDto.setLtStartingCurrentOnDOLStart(rs.getString("QRD_LT_STARTINGCURRENTDOLID"));
    			oFetchNewRatingDto.setLtAmbTempId(rs.getString("QRD_LT_AMBTEMPADDID"));
    			oFetchNewRatingDto.setLtAmbTempRemoveId(rs.getString("QRD_LT_AMBTEMPREMOVEID"));
    			oFetchNewRatingDto.setLtTempRise(rs.getString("QRD_LT_TEMPRISEID"));
    			oFetchNewRatingDto.setLtInsulationClassId(rs.getString("QRD_LT_INSULATIONCLASSID"));
    			oFetchNewRatingDto.setLtForcedCoolingId(rs.getString("QRD_LT_METHODOFCOOLING"));
    			oFetchNewRatingDto.setLtAltitude(rs.getString("QRD_LT_ALTITUDE"));
    			oFetchNewRatingDto.setLtHazardAreaId(rs.getString("QRD_LT_HAZARDAREAID"));
    			oFetchNewRatingDto.setLtHazardAreaTypeId(rs.getString("QRD_LT_HAZARDAREATYPEID"));
    			oFetchNewRatingDto.setLtGasGroupId(rs.getString("QRD_LT_GASGROUPID"));
    			oFetchNewRatingDto.setLtHazardZoneId(rs.getString("QRD_LT_HAZARDZONEID"));
    			oFetchNewRatingDto.setLtDustGroupId(rs.getString("QRD_LT_DUSTGROUPID"));
    			oFetchNewRatingDto.setLtTempClass(rs.getString("QRD_LT_TEMPCLASSID"));
    			oFetchNewRatingDto.setLtApplicationId(rs.getString("QRD_LT_APPLICATIONID"));
    			oFetchNewRatingDto.setLtDutyId(rs.getString("QRD_LT_DUTYID"));
    			oFetchNewRatingDto.setLtCDFId(rs.getString("QRD_LT_CDFID"));
    			oFetchNewRatingDto.setLtStartsId(rs.getString("QRD_LT_STARTSID"));
    			oFetchNewRatingDto.setLtNoiseLevel(rs.getString("QRD_LT_NOISELEVEL"));
    			
    			/* **********************************************	Price Fields	************************************************  */
    			oFetchNewRatingDto.setLtTotalAddonPercent(rs.getString("QRD_LT_TOTALADDONPERCENT"));
    			oFetchNewRatingDto.setLtTotalCashExtra(rs.getString("QRD_LT_TOTALCASHEXTRA"));
    			oFetchNewRatingDto.setLtLPPerUnit(rs.getString("QRD_LT_LISTPRICE"));
    			oFetchNewRatingDto.setLtLPWithAddonPerUnit(rs.getString("QRD_LT_LISTPRICEWITHADDON"));
    			oFetchNewRatingDto.setLtStandardDiscount(rs.getString("QRD_LT_STANDARDDISCOUNT"));
    			oFetchNewRatingDto.setLtPricePerUnit(rs.getString("QRD_LT_PRICEPERUNIT"));
    			oFetchNewRatingDto.setLtTotalOrderPrice(rs.getString("QRD_LT_TOTALORDERPRICE"));
    			oFetchNewRatingDto.setLtRequestedDiscount_SM(rs.getString("QRD_LT_REQUESTEDDISCOUNT_SM"));
    			oFetchNewRatingDto.setLtRequestedPricePerUnit_SM(rs.getString("QRD_LT_REQ_PRICEPERUNIT_SM"));
    			oFetchNewRatingDto.setLtRequestedTotalPrice_SM(rs.getString("QRD_LT_REQ_TOTALPRICE_SM"));
    			oFetchNewRatingDto.setLtRequestedDiscount_RSM(rs.getString("QRD_REQUESTEDDISCOUNT_RSM"));
    			oFetchNewRatingDto.setLtRequestedPricePerUnit_RSM(rs.getString("QRD_REQ_PRICEPERUNIT_RSM"));
    			oFetchNewRatingDto.setLtRequestedTotalPrice_RSM(rs.getString("QRD_RED_TOTALPRICE_RSM"));
    			oFetchNewRatingDto.setLtRequestedDiscount_NSM(rs.getString("QRD_REQUESTEDDISCOUNT_NSM"));
    			oFetchNewRatingDto.setLtRequestedPricePerUnit_NSM(rs.getString("QRD_REQ_PRICEPERUNIT_NSM"));
    			oFetchNewRatingDto.setLtRequestedTotalPrice_NSM(rs.getString("QRD_REQ_TOTALPRICE_NSM"));
    			oFetchNewRatingDto.setLtApprovedDiscount_RSM(rs.getString("QRD_LT_APPROVEDDISCOUNT_RSM"));
    			oFetchNewRatingDto.setLtApprovedPricePerunit_RSM(rs.getString("QRD_LT_APPRV_PRICEPERUNIT_RSM"));
    			oFetchNewRatingDto.setLtApprovedTotalPrice_RSM(rs.getString("QRD_LT_APPRV_TOTALPRICE_RSM"));
    			oFetchNewRatingDto.setLtApprovedDiscount_NSM(rs.getString("QRD_APPROVEDDISCOUNT_NSM"));
    			oFetchNewRatingDto.setLtApprovedPricePerunit_NSM(rs.getString("QRD_APPRV_PRICEPERUNIT_NSM"));
    			oFetchNewRatingDto.setLtApprovedTotalPrice_NSM(rs.getString("QRD_APPRV_TOTALPRICE_NSM"));
    			oFetchNewRatingDto.setLtApprovedDiscount_MH(rs.getString("QRD_APPROVEDDISCOUNT_MH"));
    			oFetchNewRatingDto.setLtApprovedPricePerunit_MH(rs.getString("QRD_APPRV_PRICEPERUNIT_MH"));
    			oFetchNewRatingDto.setLtApprovedTotalPrice_MH(rs.getString("QRD_APPRV_TOTALPRICE_MH"));
    			oFetchNewRatingDto.setLtQuotedDiscount(rs.getString("QRD_LT_QUOTEDDISCOUNT"));
    			oFetchNewRatingDto.setLtQuotedPricePerUnit(rs.getString("QRD_LT_QUOTED_PRICEPERUNIT"));
    			oFetchNewRatingDto.setLtQuotedTotalPrice(rs.getString("QRD_LT_QUOTED_TOTALPRICE"));
    			
    			/* **********************************************	Standard & Customer Req. Delivery Fields	********************  */
    			oFetchNewRatingDto.setLtEarlierMotorSerialNo(rs.getString("QRD_LT_EARLIER_SUPPLIED_MOTOR"));
    			oFetchNewRatingDto.setLtReplacementMotor(rs.getString("QRD_LT_REPLACEMENTMOTOR"));
    			oFetchNewRatingDto.setLtStandardDeliveryWks(rs.getString("QRD_LT_STANDARD_DELIVERY"));
    			oFetchNewRatingDto.setLtCustReqDeliveryWks(rs.getString("QRD_LT_CUSTOMER_REQ_DELIVERY"));
    			
    			/* **********************************************	Electrical Fields	********************************************  */
    			oFetchNewRatingDto.setLtRVId(rs.getString("QRD_LT_RV"));
    			oFetchNewRatingDto.setLtRAId(rs.getString("QRD_LT_RA"));
    			oFetchNewRatingDto.setLtWindingTreatmentId(rs.getString("QRD_LT_WINDINGTREATMENTID"));
    			oFetchNewRatingDto.setLtWindingWire(rs.getString("QRD_LT_WINDINGWIREID"));
    			oFetchNewRatingDto.setLtLeadId(rs.getString("QRD_LT_LEADID"));
    			oFetchNewRatingDto.setLtWindingConfig(rs.getString("QRD_LT_WINDINGCONFIGID"));
    			oFetchNewRatingDto.setLtLoadGD2Value(rs.getString("QRD_LT_LOADGD2"));
    			oFetchNewRatingDto.setLtVFDApplTypeId(rs.getString("QRD_LT_VFDTYPEID"));
    			oFetchNewRatingDto.setLtVFDMotorSpeedRangeMin(rs.getString("QRD_LT_VFDSPEEDMINID"));
    			oFetchNewRatingDto.setLtVFDMotorSpeedRangeMax(rs.getString("QRD_LT_VFDSPEEDMAXID"));
    			oFetchNewRatingDto.setLtOverloadingDuty(rs.getString("QRD_LT_OVERLOADDUTYID"));
    			oFetchNewRatingDto.setLtConstantEfficiencyRange(rs.getString("QRD_LT_CONSTEFFRANGEID"));
    			
    			/* **********************************************	Mechanical Fields	********************************************  */
    			oFetchNewRatingDto.setLtShaftTypeId(rs.getString("QRD_LT_SHAFTTYPEID"));
    			oFetchNewRatingDto.setLtShaftMaterialId(rs.getString("QRD_LT_SHAFTMATERIALID"));
    			oFetchNewRatingDto.setLtDirectionOfRotation(rs.getString("QRD_LT_ROTATIONDIRECTIONID"));
    			oFetchNewRatingDto.setLtStandardRotation(rs.getString("QRD_LT_STANDARDROTATION"));
    			oFetchNewRatingDto.setLtMethodOfCoupling(rs.getString("QRD_LT_METHODOFCOUPLINGID"));
    			oFetchNewRatingDto.setLtPaintingTypeId(rs.getString("QRD_LT_PAINTTYPEID"));
    			oFetchNewRatingDto.setLtPaintShadeId(rs.getString("QRD_LT_PAINTSHADEID"));
    			oFetchNewRatingDto.setLtPaintShadeValue(rs.getString("QRD_LT_PAINTSHADE_VALUE"));
    			oFetchNewRatingDto.setLtPaintThicknessId(rs.getString("QRD_LT_PAINTTHICKESSID"));
    			oFetchNewRatingDto.setLtIPId(rs.getString("QRD_LT_IPID"));
    			oFetchNewRatingDto.setLtCableSizeId(rs.getString("QRD_LT_CABLESIZEID"));
    			oFetchNewRatingDto.setLtNoOfRuns(rs.getString("QRD_LT_NOOFRUNSID"));
    			oFetchNewRatingDto.setLtNoOfCores(rs.getString("QRD_LT_NOOFCORESID"));
    			oFetchNewRatingDto.setLtCrossSectionAreaId(rs.getString("QRD_LT_CROSSSECTIONID"));
    			oFetchNewRatingDto.setLtConductorMaterialId(rs.getString("QRD_LT_CONDMATERIALID"));
    			oFetchNewRatingDto.setLtCableDiameter(rs.getString("QRD_LT_CABLEDIAMETER"));
    			oFetchNewRatingDto.setLtTerminalBoxSizeId(rs.getString("QRD_LT_TERMINALBOXSIZEID"));
    			oFetchNewRatingDto.setLtSpreaderBoxId(rs.getString("QRD_LT_SPREADERBOX"));
    			oFetchNewRatingDto.setLtSpaceHeaterId(rs.getString("QRD_LT_SPACEHEATER"));
    			oFetchNewRatingDto.setLtVibrationId(rs.getString("QRD_LT_VIBRATION"));
    			oFetchNewRatingDto.setLtFlyingLeadWithoutTDId(rs.getString("QRD_LT_FLYINGLEADWITHOUTTB"));
    			oFetchNewRatingDto.setLtFlyingLeadWithTDId(rs.getString("QRD_LT_FLYINGLEADWITHTB"));
    			oFetchNewRatingDto.setLtMetalFanId(rs.getString("QRD_LT_METALFAN"));
    			oFetchNewRatingDto.setLtTechoMounting(rs.getString("QRD_LT_TECHOMOUNTING"));
    			oFetchNewRatingDto.setLtShaftGroundingId(rs.getString("QRD_LT_SHAFTGROUNDING"));
    			
    			/* **********************************************	Accessories Fields	********************************************  */
    			oFetchNewRatingDto.setLtHardware(rs.getString("QRD_LT_HARDWARE"));
    			oFetchNewRatingDto.setLtGlandPlateId(rs.getString("QRD_LT_GLANDPLATE"));
    			oFetchNewRatingDto.setLtDoubleCompressionGlandId(rs.getString("QRD_LT_DOUBLECOMPRESSION"));
    			oFetchNewRatingDto.setLtSPMMountingProvisionId(rs.getString("QRD_LT_SPMMOUNTING"));
    			oFetchNewRatingDto.setLtAddNamePlateId(rs.getString("QRD_LT_ADDLNAMEPLATE"));
    			oFetchNewRatingDto.setLtDirectionArrowPlateId(rs.getString("QRD_LT_DIRECTIONARROWPLATE"));
    			oFetchNewRatingDto.setLtRTDId(rs.getString("QRD_LT_RTDID"));
    			oFetchNewRatingDto.setLtBTDId(rs.getString("QRD_LT_BTDID"));
    			oFetchNewRatingDto.setLtThermisterId(rs.getString("QRD_LT_THERMISTERID"));
    			oFetchNewRatingDto.setLtAuxTerminalBoxId(rs.getString("QRD_LT_AUXTERMINALBOX"));
    			oFetchNewRatingDto.setLtCableSealingBoxId(rs.getString("QRD_LT_CABLESEALINGBOX"));
    			
    			/* **********************************************	Bearings Fields	************************************************  */
    			oFetchNewRatingDto.setLtBearingSystemId(rs.getString("QRD_LT_BEARINGSYSTEMID"));
    			oFetchNewRatingDto.setLtBearingNDEId(rs.getString("QRD_LT_BEARINGNDEID"));
    			oFetchNewRatingDto.setLtBearingDEId(rs.getString("QRD_LT_BEARINGDEID"));
    			
    			/* **********************************************	Certificate, Spares, Tests Fields	****************************  */
    			oFetchNewRatingDto.setLtWitnessRoutineId(rs.getString("QRD_LT_WITNESSROUTINE"));
    			oFetchNewRatingDto.setLtAdditionalTest1Id(rs.getString("QRD_LT_ADDL_TEST1"));
    			oFetchNewRatingDto.setLtAdditionalTest2Id(rs.getString("QRD_LT_ADDL_TEST2"));
    			oFetchNewRatingDto.setLtAdditionalTest3Id(rs.getString("QRD_LT_ADDL_TEST3"));
    			oFetchNewRatingDto.setLtTypeTestId(rs.getString("QRD_LT_TYPETEST"));
    			oFetchNewRatingDto.setLtULCEId(rs.getString("QRD_LT_CERTIFICATION"));
    			oFetchNewRatingDto.setLtQAPId(rs.getString("QRD_LT_QAPID"));
    			oFetchNewRatingDto.setLtSpares1(rs.getString("QRD_LT_SPARES1"));
    			oFetchNewRatingDto.setLtSpares2(rs.getString("QRD_LT_SPARES2"));
    			oFetchNewRatingDto.setLtSpares3(rs.getString("QRD_LT_SPARES3"));
    			oFetchNewRatingDto.setLtSpares4(rs.getString("QRD_LT_SPARES4"));
    			oFetchNewRatingDto.setLtSpares5(rs.getString("QRD_LT_SPARES5"));
    			oFetchNewRatingDto.setLtDataSheet(rs.getString("QRD_LT_DATASHEET"));
    			oFetchNewRatingDto.setLtMotorGA(rs.getString("QRD_LT_MOTORGA"));
    			oFetchNewRatingDto.setLtPerfCurves(rs.getString("QRD_LT_PERFCURVE"));
    			oFetchNewRatingDto.setLtTBoxGA(rs.getString("QRD_LT_TBOXGA"));
    			oFetchNewRatingDto.setLtAdditionalComments(rs.getString("QRD_LT_ADDL_COMMENTS"));
    			
    			
    			oFetchNewRatingDto.setFlg_PriceFields_MTO(rs.getString("FLG_PRICEFIELDS_MTO"));
    			/*oTempNewRatingDto.setFlg_Addon_Leads_MTO(rs.getString("FLG_ADDON_LEADS_MTO"));
    			oTempNewRatingDto.setFlg_Addon_TBoxSize_MTO(rs.getString("FLG_ADDON_TBOXSIZE_MTO"));
    			oTempNewRatingDto.setFlg_Addon_BearingDE_MTO(rs.getString("FLG_ADDON_BEARINGDE_MTO"));
    			oTempNewRatingDto.setFlg_Addon_CableEntry_MTO(rs.getString("FLG_ADDON_CABLEENTRY_MTO"));*/
    			oFetchNewRatingDto.setFlg_Txt_MotorNo_MTO(rs.getString("FLG_TXT_MOTORNO_MTO"));
    			oFetchNewRatingDto.setFlg_Txt_RV_MTO(rs.getString("FLG_TXT_RV_MTO"));
    			oFetchNewRatingDto.setFlg_Txt_RA_MTO(rs.getString("FLG_TXT_RA_MTO"));
    			oFetchNewRatingDto.setFlg_Txt_LoadGD2_MTO(rs.getString("FLG_TXT_LOADGD2_MTO"));
    			oFetchNewRatingDto.setFlg_Txt_NonSTDPaint_MTO(rs.getString("FLG_TXT_NONSTDPAINT_MTO"));
    			
    			oFetchNewRatingDto.setMtoHighlightFlag(rs.getString("QRD_LT_MTOHIGHLIGHT"));
    			
    			oFetchNewRatingDto.setIsReferDesign(rs.getString("QRD_LT_ISREFERDESIGN"));
    		}
    		
        } finally {
    		/* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
    	}
        
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
        return oFetchNewRatingDto;
	}
	
	public String getDesignManagerId(Connection conn) throws Exception {
		String sMethodName = "getDesignManagerId";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null ;
		/* ResultSet object to store the rows retrieved from database */
		ResultSet rs = null ;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();	 
		
		String sDesignManagerId = "";
		
		try {
        	pstmt = conn.prepareStatement(FETCH_DESIGNMANAGER_ID);
        	rs = pstmt.executeQuery();
            if (rs.next()) {
            	sDesignManagerId = rs.getString("RLE_MGRSSO");
            }
        } finally {
            /* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs, pstmt);
        }
		
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
		return sDesignManagerId;
	}

	public boolean updateNewEnquiryBasicInfo(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception {
		String sMethodName = "updateNewEnquiryBasicInfo";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null ;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();
		
		int iInserted =  QuotationConstants.QUOTATION_LITERAL_ZERO;
		boolean isUpdated = QuotationConstants.QUOTATION_FALSE;
		
		try {
			pstmt = conn.prepareStatement(UPDATE_NEWENQUIRY_BASICINFO);
			pstmt.setString(1, oNewEnquiryDto.getClosingDate());
			pstmt.setString(2, oNewEnquiryDto.getTargeted());
			pstmt.setString(3, oNewEnquiryDto.getWinningChance());
			pstmt.setString(4, oNewEnquiryDto.getSmNote());
			pstmt.setInt(5, oNewEnquiryDto.getEnquiryId());		 
			
			iInserted = pstmt.executeUpdate();
			
			if (iInserted > QuotationConstants.QUOTATION_LITERAL_ZERO )
				isUpdated = QuotationConstants.QUOTATION_TRUE;
			
		} finally {
			/* Releasing ResultSet & PreparedStatement objects */
			oDBUtility.releaseResources(pstmt);
		}
		
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
		return isUpdated;
	}
	
	public String processLDApproval(Connection conn, NewEnquiryDto oNewEnquiryDto, String sOperationType) throws Exception {
		String sMethodName = "saveLostEnquiryDetails";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null;
		/* ResultSet object to store the rows retrieved from database */
		//ResultSet rs = null, rs1 = null;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();
		
		int iUpdateRes = 0;
		String sResult = QuotationConstants.QUOTATION_STRING_N;
		
		try {
			pstmt = conn.prepareStatement(UPDATE_NEWENQUIRY_LD_VALUE);
			
			if(QuotationConstants.QUOTATIONLT_LIQUIDATEDDAMAGE_APPROVE.equalsIgnoreCase(sOperationType)) {
				pstmt.setString(1, oNewEnquiryDto.getLiquidatedDamagesDueToDeliveryDelay());
			} else if(QuotationConstants.QUOTATIONLT_LIQUIDATEDDAMAGE_REJECT.equalsIgnoreCase(sOperationType)) {
				pstmt.setString(1, QuotationConstants.QUOTATION_STRING_ONE);
			}
			pstmt.setString(2, oNewEnquiryDto.getBhNote());
			
			// For WHERE Clause.
			pstmt.setInt(3, oNewEnquiryDto.getEnquiryId());
			
			iUpdateRes = pstmt.executeUpdate();
			
			if(iUpdateRes > 0) {
				sResult = QuotationConstants.QUOTATION_STRING_Y;
			}
			
		} finally {
			/* Releasing ResultSet & PreparedStatement objects */
			oDBUtility.releaseResources(pstmt);
		}
		
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
		return sResult;
	}
	
	public List<KeyValueVo> fetchSalesReassignUsersList(Connection conn) throws Exception {
		String sMethodName = "fetchSalesReassignUsersList";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt1 = null;
		/* ResultSet object to store the rows retrieved from database */
		ResultSet rs1 = null;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();
		
		List<KeyValueVo> alReassignUsersList = new ArrayList<>();
        KeyValueVo oKeyValueVo = null;
        
		try {
			pstmt1 = conn.prepareStatement(FETCH_SALES_REASSIGN_USER_DETAILS);
    		rs1 = pstmt1.executeQuery();
    		
    		while(rs1.next()) {
    			oKeyValueVo = new KeyValueVo();
    			oKeyValueVo.setKey(rs1.getString("QUD_LT_USER_SSO"));
    			oKeyValueVo.setValue(rs1.getString("USER_FULLNAME"));
    			
    			alReassignUsersList.add(oKeyValueVo);
    		}
		} finally {
			/* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs1, pstmt1);
		}
		
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		return alReassignUsersList;
	}
	
	public String updateMfgPlantDetails(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception {
		String sMethodName = "fetchSalesReassignUsersList";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();
		
		int iUpdateRes = 0;
		String sResult = QuotationConstants.QUOTATION_STRING_N;
		
		try {
			pstmt = conn.prepareStatement(UPDATE_NEWENQUIRY_MFGPLANT_DETAILS);
			
			pstmt.setString(1, oNewEnquiryDto.getOrderCompletionLeadTime());
			pstmt.setString(2, oNewEnquiryDto.getMfgNote());
			
			pstmt.setInt(3, oNewEnquiryDto.getEnquiryId());
			
			iUpdateRes = pstmt.executeUpdate();
			
			if(iUpdateRes > 0)
				sResult = QuotationConstants.QUOTATION_STRING_Y;
			
		} finally {
			/* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(pstmt);
		}
		
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		return sResult;
	}
	
	/* (non-Javadoc)
	 * @see in.com.rbc.quotation.enquiry.dao.QuotationDao#saveMfgPlantRatingDetails(java.sql.Connection, in.com.rbc.quotation.enquiry.dto.NewRatingDto)
	 */
	public String saveMfgPlantRatingDetails(Connection conn, NewRatingDto oNewRatingDto) throws Exception {
		String sMethodName = "saveMfgPlantRatingDetails";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();
		
		int iUpdateRes = 0;
		String sResult = QuotationConstants.QUOTATION_STRING_N;
		
		try {
			pstmt = conn.prepareStatement(UPDATE_NEWRATING_MFGPLANT_DETAILS);
			
			pstmt.setString(1, oNewRatingDto.getLtStandardDeliveryWks());
			pstmt.setString(2, oNewRatingDto.getLtCustReqDeliveryWks());
			
			// Set WHERE Clause.
			pstmt.setInt(3, oNewRatingDto.getRatingId());
			
			iUpdateRes = pstmt.executeUpdate();
			
			if(iUpdateRes > 0)
				sResult = QuotationConstants.QUOTATION_STRING_Y;
			
		} finally {
			/* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(pstmt);
		}
		
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
		return sResult;
	}

	public ArrayList<NewEnquiryDto> fetchNoMailSentEnquiries(Connection conn) throws Exception {
		String sMethodName = "fetchSalesReassignUsersList";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt1 = null;
		/* ResultSet object to store the rows retrieved from database */
		ResultSet rs1 = null;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();
		
		ArrayList<NewEnquiryDto> alNoMailSentEnquiriesList = new ArrayList<>();
		NewEnquiryDto oNewEnquiryDto = null;
		
		try {
			pstmt1 = conn.prepareStatement(FETCH_NOTIFIERMAIL_NOTSENTMAIL_ENQUIRIES);
			pstmt1.setString(1, PropertyUtility.getKeyValue("QUOTATIONLT_ACTIVATION_DATE"));
    		rs1 = pstmt1.executeQuery();
    		
    		while(rs1.next()) {
    			oNewEnquiryDto = new NewEnquiryDto();
    			
    			oNewEnquiryDto.setEnquiryId(rs1.getInt("QEM_ENQUIRYID"));
    			oNewEnquiryDto.setEnquiryNumber(rs1.getString("QEM_ENQUIRYNO"));
    			oNewEnquiryDto.setWorkflowAssignedTo(rs1.getString("QWF_ASSIGNTO"));
    			oNewEnquiryDto.setCreatedDate(DateUtility.getRBCStandardDate(rs1.getString("QEM_CREATEDDATE")));
    			oNewEnquiryDto.setCreatedBy(rs1.getString("QEM_CREATEDBY"));
    			// Set CASE '1'
    			oNewEnquiryDto.setNotifierType(QuotationConstants.QUOTATION_STRING_ONE);
    			
    			alNoMailSentEnquiriesList.add(oNewEnquiryDto);
    		}
    		
		} finally {
			/* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs1, pstmt1);
		}
		
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
		return alNoMailSentEnquiriesList;
	}
	
	public ArrayList<NewEnquiryDto> fetchMailSentCurrentAssigneeEnquiries(Connection conn) throws Exception {
		String sMethodName = "fetchSalesReassignUsersList";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt1 = null;
		/* ResultSet object to store the rows retrieved from database */
		ResultSet rs1 = null;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();
		
		ArrayList<NewEnquiryDto> alMailSent_ToAssigned_EnquiriesList = new ArrayList<>();
		NewEnquiryDto oNewEnquiryDto = null;
		
		try {
			pstmt1 = conn.prepareStatement(FETCH_NOTIFIERMAIL_SENTMAIL_TOASSIGNED_ENQUIRIES);
			pstmt1.setString(1, PropertyUtility.getKeyValue("QUOTATIONLT_ACTIVATION_DATE"));
    		rs1 = pstmt1.executeQuery();
    		
    		while(rs1.next()) {
    			oNewEnquiryDto = new NewEnquiryDto();
    			
    			oNewEnquiryDto.setEnquiryId(rs1.getInt("QEM_ENQUIRYID"));
    			oNewEnquiryDto.setEnquiryNumber(rs1.getString("QEM_ENQUIRYNO"));
    			oNewEnquiryDto.setWorkflowAssignedTo(rs1.getString("QWF_ASSIGNTO"));
    			oNewEnquiryDto.setIsNotifierEmailSent(rs1.getString("QEM_NOTIFIER_ISEMAILSENT"));
    			oNewEnquiryDto.setNotifierEmailSentToSSO(rs1.getString("QEM_NOTIFIER_EMAILSENTTO"));
    			oNewEnquiryDto.setNotifierEmailSentDate(DateUtility.getRBCStandardDate(rs1.getString("QEM_NOTIFIER_EMAILSENTDATE")));
    			oNewEnquiryDto.setCreatedBy(rs1.getString("QEM_CREATEDBY"));
    			// Set CASE '2'
    			oNewEnquiryDto.setNotifierType(QuotationConstants.QUOTATION_STRING_TWO);
    			
    			alMailSent_ToAssigned_EnquiriesList.add(oNewEnquiryDto);
    		}
		}
		finally {
			/* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs1, pstmt1);
		}
		
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
		return alMailSent_ToAssigned_EnquiriesList;
	}
	
	public ArrayList<NewEnquiryDto> fetchMailNotSentCurrentAssigneeEnquiries (Connection conn) throws Exception {
		String sMethodName = "fetchSalesReassignUsersList";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt1 = null;
		/* ResultSet object to store the rows retrieved from database */
		ResultSet rs1 = null;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();
		
		ArrayList<NewEnquiryDto> alMailSent_NotToAssigned_EnquiriesList = new ArrayList<>();
		NewEnquiryDto oNewEnquiryDto = null;
		
		try {
			pstmt1 = conn.prepareStatement(FETCH_NOTIFIERMAIL_SENTMAIL_NOT_TOASSIGNED_ENQUIRIES);
			pstmt1.setString(1, PropertyUtility.getKeyValue("QUOTATIONLT_ACTIVATION_DATE"));
    		rs1 = pstmt1.executeQuery();
    		
    		while(rs1.next()) {
    			oNewEnquiryDto = new NewEnquiryDto();
    			
    			oNewEnquiryDto.setEnquiryId(rs1.getInt("QEM_ENQUIRYID"));
    			oNewEnquiryDto.setEnquiryNumber(rs1.getString("QEM_ENQUIRYNO"));
    			oNewEnquiryDto.setWorkflowAssignedTo(rs1.getString("QWF_ASSIGNTO"));
    			oNewEnquiryDto.setIsNotifierEmailSent(rs1.getString("QEM_NOTIFIER_ISEMAILSENT"));
    			oNewEnquiryDto.setNotifierEmailSentToSSO(rs1.getString("QEM_NOTIFIER_EMAILSENTTO"));
    			oNewEnquiryDto.setNotifierEmailSentDate(DateUtility.getRBCStandardDate(rs1.getString("QEM_NOTIFIER_EMAILSENTDATE")));
    			oNewEnquiryDto.setWorkflowAssignedDate(DateUtility.getRBCStandardDate(rs1.getString("QWF_CREATEDDATE")));
    			oNewEnquiryDto.setCreatedBy(rs1.getString("QEM_CREATEDBY"));
    			// Set CASE '1'
    			oNewEnquiryDto.setNotifierType(QuotationConstants.QUOTATION_STRING_THREE);
    			
    			alMailSent_NotToAssigned_EnquiriesList.add(oNewEnquiryDto);
    		}
		} finally {
			/* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs1, pstmt1);
		}
		
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
		return alMailSent_NotToAssigned_EnquiriesList;
	}
	
	public void processNotifierMailSentUpdates(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception {
		String sMethodName = "processNotifierMailSentUpdates";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();
		
		int iUpdateRes = 0;
		String sResult = QuotationConstants.QUOTATION_STRING_N;
		
		try {
			pstmt = conn.prepareStatement(UPDATE_NEWENQUIRY_NOTIFIER_DETAILS);
			
			pstmt.setString(1, QuotationConstants.QUOTATION_STRING_Y);
			pstmt.setString(2, oNewEnquiryDto.getWorkflowAssignedTo());
			
			// Set WHERE clause.
			pstmt.setInt(3, oNewEnquiryDto.getEnquiryId());
			
			iUpdateRes = pstmt.executeUpdate();
			
			if(iUpdateRes > 0)
				sResult = QuotationConstants.QUOTATION_STRING_Y;
			
		} finally {
			/* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(pstmt);
		}
		
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
	}

	@Override
	public NewEnquiryDto copyNewEnquiry(Connection conn, NewEnquiryDto oNewEnquiryDto, UserDto oUserDto) throws Exception {
		String sMethodName = "copyNewEnquiry";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		 
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility(); 
		/* PreparedStatement objects to handle database operations */
		CallableStatement cstmt = null;
        
		NewEnquiryDto oTempNewEnquiryDto = null;
		String sEnquiryId;
        
		try {
			cstmt = conn.prepareCall(CALL_COPYNEWENQUIRY_PROC);
			cstmt.setString(1, (""+oNewEnquiryDto.getEnquiryId()).trim());
			cstmt.setString(2, oUserDto.getUserId());	
			cstmt.setString(3,"COPY");	
			cstmt.registerOutParameter(1,Types.VARCHAR);
	    	
			cstmt.executeQuery();
			
			sEnquiryId = cstmt.getString(1);
			
			if (sEnquiryId != null && !sEnquiryId.equalsIgnoreCase("")) {
				oTempNewEnquiryDto = new NewEnquiryDto();
				oTempNewEnquiryDto.setEnquiryId(Integer.parseInt(sEnquiryId));
			}
		} finally {
			/* Releasing PreparedStatment objects */
			oDBUtility.releaseResources(cstmt);
		}
		
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
		return oTempNewEnquiryDto;
	}
	
	public void updateDmStatus(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception {
		String sMethodName = "updateDmStatus";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt = null;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();
		
		int iUpdateRes = 0;
		String sResult = QuotationConstants.QUOTATION_STRING_N;
		
		try {
			pstmt = conn.prepareStatement(UPDATE_NEWENQUIRY_DMUPDATESTATUS);
			
			pstmt.setString(1, oNewEnquiryDto.getDmUpdateStatus());
			
			// Set WHERE clause.
			pstmt.setInt(2, oNewEnquiryDto.getEnquiryId());
			
			iUpdateRes = pstmt.executeUpdate();
			
			if(iUpdateRes > 0)
				sResult = QuotationConstants.QUOTATION_STRING_Y;
			
		} finally {
			/* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(pstmt);
		}
		
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
	}
	
	public String updateMTOMaterialCost(Connection conn, NewRatingDto oNewRatingDto, NewMTORatingDto oNewMTORatingDto) throws Exception {
		String sMethodName = "updateMTOMaterialCost";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt1 = null;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();
		
		int iUpdateRes = 0;
		String sResult = QuotationConstants.QUOTATION_STRING_N;
		
		try {
			// 1. Update RFQ_LT_RATING_DETAILS.
			pstmt1 = conn.prepareStatement(UPDATE_NEWRATING_MTO_MATERIALCOST);
			pstmt1.setString(1, oNewMTORatingDto.getMtoMaterialCost());
			pstmt1.setString(2, oNewRatingDto.getIsDesignComplete());
        	pstmt1.setString(3, oNewRatingDto.getIsReferDesign());
			pstmt1.setInt(4, oNewRatingDto.getEnquiryId());
        	pstmt1.setInt(5, oNewRatingDto.getRatingNo());
        	
        	iUpdateRes = pstmt1.executeUpdate();
        	
        	if(iUpdateRes > 0)
        		sResult = QuotationConstants.QUOTATION_STRING_Y;
        	
			// 2. RFQ_LT_MTORATING_DETAILS.
        	if(QuotationConstants.QUOTATION_STRING_Y.equalsIgnoreCase(sResult)) {
        		oDBUtility.releaseResources(pstmt1);
        		
        		pstmt1 = conn.prepareStatement(UPDATE_NEWMTORATING_MC_DETAILS);
        		pstmt1.setString(1, oNewMTORatingDto.getMtoProdLineId());
            	pstmt1.setString(2, oNewMTORatingDto.getMtoKWId());
            	pstmt1.setString(3, oNewMTORatingDto.getMtoPoleId());
            	pstmt1.setString(4, oNewMTORatingDto.getMtoFrameId());
            	pstmt1.setString(5, oNewMTORatingDto.getMtoFrameSuffixId());
            	pstmt1.setString(6, oNewMTORatingDto.getMtoMountingId());
            	pstmt1.setString(7, oNewMTORatingDto.getMtoTBPositionId());
            	pstmt1.setString(8, oNewMTORatingDto.getMtoMfgLocation());
            	pstmt1.setString(9, oNewMTORatingDto.getMtoMaterialCost());
            	pstmt1.setString(10, "Yes");
            	pstmt1.setInt(17, oNewMTORatingDto.getMtoEnquiryId());
            	pstmt1.setInt(18, oNewMTORatingDto.getMtoRatingNo());
            	
            	iUpdateRes = pstmt1.executeUpdate();
            	
            	if(iUpdateRes > 0)
            		sResult = QuotationConstants.QUOTATION_STRING_Y;
        	}
			
		} finally {
			/* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(pstmt1);
		}
		
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
		return sResult; 
	}

	
	public String checkEnquiryMCApplied(Connection conn, NewEnquiryDto oNewEnquiryDto) throws Exception {
		
		String sMethodName = "checkEnquiryMCApplied";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt1 = null;
		/* ResultSet object to store the rows retrieved from database */
		ResultSet rs1 = null;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();
		
		String sMatCostValue = null;
		String sResult = QuotationConstants.QUOTATION_STRING_N;
		
		try {
			pstmt1 = conn.prepareStatement(FETCH_NEWENQUIRY_MCAPPLIED);
			pstmt1.setInt(1, oNewEnquiryDto.getEnquiryId());
    		rs1 = pstmt1.executeQuery();
    		
    		while(rs1.next()) {
    			sMatCostValue = rs1.getString("QRD_LT_DM_MATCOST");
    			
    			if(StringUtils.isNotBlank(sMatCostValue)) {
    				sResult = QuotationConstants.QUOTATION_STRING_Y;
    				break;
    			}
    		}
		} finally {
			/* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs1, pstmt1);
		}
		
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
		return sResult; 
	}

	public boolean checkIsNewRatingDesignComplete(Connection conn, NewRatingDto oNewRatingDto) throws Exception {
		String sMethodName = "checkIsNewRatingDesignComplete";
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
		/* PreparedStatement object to handle database operations */
		PreparedStatement pstmt1 = null;
		/* ResultSet object to store the rows retrieved from database */
		ResultSet rs1 = null;
		/* Object of DBUtility class to handle database operations */
		DBUtility oDBUtility = new DBUtility();
		
		boolean isDesignComplete = false;
		String sDesignCompleteVal = null;
		
		try {
			pstmt1 = conn.prepareStatement(FETCH_NEWRATING_ISDESIGNCOMPLETE);
			pstmt1.setInt(1, oNewRatingDto.getEnquiryId());
			pstmt1.setInt(2, oNewRatingDto.getRatingNo());
    		rs1 = pstmt1.executeQuery();
    		
    		if(rs1.next()) {
    			sDesignCompleteVal = rs1.getString("QRD_LT_MTO_ISDESIGNCOMPLETE");
    			
    			if(StringUtils.isNotBlank(sDesignCompleteVal) && QuotationConstants.QUOTATION_YES.equalsIgnoreCase(sDesignCompleteVal)) {
    				isDesignComplete = true;
    			}
    		}
		} finally {
			/* Releasing ResultSet & PreparedStatement objects */
            oDBUtility.releaseResources(rs1, pstmt1);
		}
		
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
		return isDesignComplete; 
	}
	
}

