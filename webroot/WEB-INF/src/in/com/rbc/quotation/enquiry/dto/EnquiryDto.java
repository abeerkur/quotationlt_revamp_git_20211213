/**
 * ******************************************************************************************
 * Project Name: Request For Quotation
 * Document Name: EnquiryDto.java
 * Package: in.com.rbc.quotation.enquiry.dto
 * Desc: DTO class that holds getters and setters of each property.
 * ******************************************************************************************
 * @author 100006126
 * ******************************************************************************************
 */

package in.com.rbc.quotation.enquiry.dto;

import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.utility.CommonUtility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class EnquiryDto {
	private int enquiryId;
	private String enquiryNumber;
	private int revisionNumber;
    private int baseEnquiryId;
	private int customerId;
	private int locationId;
	private int statusId;
	private String enquiryType;
	private String customerName;
	private String location;
	private String consultantName;
	private String createdBy;
	private String createdBySSO;
	private String createdDate;
	private String crossReference;
	private String description;
	private String lastModifiedBy;
	private String lastModifiedDate;
	private String statusName;
	private String currentRating;
	private int discount;
	private String discountRemarks;
	private long ceFactor;
	private long rsmFactor;	
	private String returnTo;
	private String returnToRemarks;	
	private String ratingOperationId;	
	private String customerType;
	private String endUser;
	private String oem;
	private String epc;
	private String dealer;
	private String projectName;
	private String industry;
	private String endUserCountry;
	private String endUserIndustry;
	private String approval;
	private String targeted;
	private String winningChance;
	private String competitor;
	private String salesStage;
	private String totalOrderValue;
	private String orderClosingMonth;
	private String expectedDeliveryMonth;
	private String warrantyDispatchDate;
	private String warrantyCommissioningDate;
	private String deliveryType;
	private String destState;
	private String destCity;
	private String spa;
	private double customerCounterOffer;
	private String materialCost;
	private String mcnspRatio;
	private double motorPrice;
	private String warrantyCost;
	private String transportationCost;	
	private String ecSupervisionCost;
	private String hzAreaCertCost;
	private String sparesCost;
	private double totalCost;
	private String propSpaReason;
	private String winlossReason;
	private String winlossComment;
	private String winningCompetitor;	
	private double unitMC;
	private double unitPrice;
	private String winlossPrice;
	private double totalMC;
	private String totalPrice;
	private String totalPkgPrice;
	private String requestRemarks;
	private String enquiryReferenceNumber;
	private String savingIndent;
	private String revision;
	private String deptAndAuth;
	private String attachmentFileName; //Stores Dm Attachment File Names
	private String totalMotorPrice;
	private String enqQuotedPrice;
	
	
	private ArrayList customerList = new ArrayList();
	private ArrayList locationList = new ArrayList();
	private ArrayList ratingList = new ArrayList();
	private ArrayList ratingObjects = new ArrayList();
	private ArrayList currentRatingObject = new ArrayList();
	private ArrayList addOnList = new ArrayList();
	private ArrayList approverCommentsList = new ArrayList();
	private ArrayList ratingFactorList = new ArrayList();
	private ArrayList teamMemberList = new ArrayList();
	
	private ArrayList alCustomerTypeList = new ArrayList();
	private ArrayList alIndustryList = new ArrayList();
	private ArrayList alApprovalList = new ArrayList();
	private ArrayList alTargetedList = new ArrayList();
	private ArrayList alWinningChanceList = new ArrayList();
	private ArrayList alCompetitorList = new ArrayList();
	private ArrayList alEnquiryTypeList = new ArrayList();
	private ArrayList alSalesStageList = new ArrayList();
	private ArrayList alDeliveryTypeList = new ArrayList();
	private ArrayList alSPAList = new ArrayList();
	private ArrayList alWinlossReasonList = new ArrayList();
	private ArrayList alWinCompetitorList = new ArrayList();
	private ArrayList alVoltList = new ArrayList();
	private ArrayList alWinChanceList = new ArrayList();
	
	
	private ArrayList salesManagerList = new ArrayList();
	
	
	
	private ArrayList previousratingList = new ArrayList();
	private String termsVarFormula;
	private String termsDeliveryLT;
	private String termsDeliveryHT;
	private String termsPercentAdv;
	private String termsPercentBal;
	
	private TeamMemberDto teamMemberDto = null;
	

	private String advancePayment;
	private String paymentTerms;
	


	private String customerNameMultiplier;
	private String customerTypeMultiplier;
	private String endUserMultiplier;
	private String warrantyDispatchMultiplier;
	private String warrantycommissioningMultiplier;
	private String advancePaymMultiplier;
	private String payTermMultiplier;
	private String expDeliveryMultiplier;
	private String financeMultiplier;
	private String quantityMultiplier;
	private String ratedOutputPNMulitplier;
	private String typeMultiplier;
	private String areaCMultiplier;
	private String frameMultiplier;
	private String applicationMultiplier;
	private String factorMultiplier;
	private String netmcMultiplier;
	private String markupMultiplier;
	private String transferCMultiplier;
	
	
	private Map<String,EnquiryDto> MultiplierMap=new HashMap<String,EnquiryDto>();
	
	
	private String returnoperation;
	
	
	private String mcornspratioMultiplier_rsm;
	private String transportationperunit_rsm;
	private String warrantyperunit_rsm;
	private String unitpriceMultiplier_rsm;
	private String totalpriceMultiplier_rsm;


	private String smnote;
	private String dmnote;
	private String cenote;
	private String rsmnote;
	private String sm_name;
	private String dm_ssoid;
	private String dm_name;
	private String ce_ssoid;
	private String ce_name;
	private String rsm_ssoid;
	private String rsm_name;
	private String pagechecking;
	private String zone;
	private String gasGroup;
	private String dmToSmReturn;
	private String ceToDmReturn;
	private String ceToSmReturn;
	private String rsmToCeReturn;
	private String rsmToDmReturn;
	private String rsmToSmReturn;
	private String commentOperation;
	private String deviation_clarification;
	private String deToSmReturn;
	private String quotationtoRsmReturn;
	private String quotationtoCeReturn;
	private String quotationtoDmReturn;






    private String enquiryOperationType; // This variable is to diffrentiate between the button actions Manager or engineer
    private int attachmentId; //This Variable stores attachmentId.
    
	//Indent Variable Added on 15-March-2019
	private String sapCustomerCode;
	private String orderNumber;
	private String poiCopy;
	private String loiCopy;
	private String deliveryReqByCust;
	private String ld;
	private String drawingApproval;
	private String qapApproval;
	private String typeTest;
	private String routineTest;
	private String specialTest;
	private String specialTestDetails;
	private String thirdPartyInspection;
	private String stageInspection;
	private String enduserLocation;
	private String epcName;
	private String packing;
	private String insurance;
	private String replacement;
	private String oldSerialNo;
	private String customerPartNo;
	private String includeFrieght;
	private String includeExtraWarrnt;
	private String includeSupervisionCost;
	private String includetypeTestCharges;
	private String extraFrieght;
	private String extraExtraWarrnt;
	private String extraSupervisionCost;
	private String extratypeTestCharges;
	
	private String cmToSmReturn;
    private String dmToCmReturn;
    private String deToCmReturn;

	private String indentOperation;
	private String indentOperationCheck;
	private String indentnote;
	private String commengToSmReturn;
	private String indentDmtosmReturn;
	private String indentDetosmReturn;
    private String cmNote;
    private String cmName;
	//Lost Page Details
	private String lossPrice;
	private String lossReason1;
	private String lossReason2;
	private String lossComment;
	private String lossCompetitor;
	private String lossChecking;
	private String abondentComment;
	private String dmFlag = QuotationConstants.QUOTATION_STRING_FALSE;
	private String smEmail;
	private String smContactNo;

	private String isNewEnquiry;
	
	/**
	 * @return Returns the teamMemberDto.
	 */
	public TeamMemberDto getTeamMemberDto() {
		return teamMemberDto;
	}
	/**
	 * @param teamMemberDto The teamMemberDto to set.
	 */
	public void setTeamMemberDto(TeamMemberDto teamMemberDto) {
		this.teamMemberDto = teamMemberDto;
	}
	/**
	 * @return Returns the currentRatingObject.
	 */
	public ArrayList getCurrentRatingObject() {
		return currentRatingObject;
	}
	/**
	 * @param currentRatingObject The currentRatingObject to set.
	 */
	public void setCurrentRatingObject(ArrayList currentRatingObject) {
		this.currentRatingObject = currentRatingObject;
	}
	/**
	 * @return Returns the ratingObjects.
	 */
	public ArrayList getRatingObjects() {
		return ratingObjects;
	}
	/**
	 * @param ratingObjects The ratingObjects to set.
	 */
	public void setRatingObjects(ArrayList ratingObjects) {
		this.ratingObjects = ratingObjects;
	}
	/**
	 * @return Returns the customerList.
	 */
	public ArrayList getCustomerList() {
		return customerList;
	}
	/**
	 * @param customerList The customerList to set.
	 */
	public void setCustomerList(ArrayList customerList) {
		this.customerList = customerList;
	}
	/**
	 * @return Returns the locationList.
	 */
	public ArrayList getLocationList() {
		return locationList;
	}
	/**
	 * @param locationList The locationList to set.
	 */
	public void setLocationList(ArrayList locationList) {
		this.locationList = locationList;
	}
	/**
	 * @return Returns the statusId.
	 */
	public int getStatusId() {
		return statusId;
	}
	/**
	 * @param statusId The statusId to set.
	 */
	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}
	/**
	 * @return Returns the statusName.
	 */
	public String getStatusName() {
		return CommonUtility.replaceNull(statusName);
	}
	/**
	 * @param statusName The statusName to set.
	 */
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
	/**
	 * @return Returns the customerId.
	 */
	public int getCustomerId() {
		return customerId;
	}
	/**
	 * @param customerId The customerId to set.
	 */
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	/**
	 * @return Returns the enquiryId.
	 */
	public int getEnquiryId() {
		return enquiryId;
	}
	/**
	 * @param enquiryId The enquiryId to set.
	 */
	public void setEnquiryId(int enquiryId) {
		this.enquiryId = enquiryId;
	}
	/**
	 * @return Returns the lastModifiedBy.
	 */
	public String getLastModifiedBy() {
		return CommonUtility.replaceNull(lastModifiedBy);
	}
	/**
	 * @param lastModifiedBy The lastModifiedBy to set.
	 */
	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}
	/**
	 * @return Returns the locationId.
	 */
	public int getLocationId() {
		return locationId;
	}
	/**
	 * @param locationId The locationId to set.
	 */
	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}
	/**
	 * @return Returns the revisionNumber.
	 */
	public int getRevisionNumber() {
		return revisionNumber;
	}
	/**
	 * @param revisionNumber The revisionNumber to set.
	 */
	public void setRevisionNumber(int revisionNumber) {
		this.revisionNumber = revisionNumber;
	}
	/**
	 * @return Returns the consultantName.
	 */
	public String getConsultantName() {
		return CommonUtility.replaceNull(consultantName);
	}
	/**
	 * @param consultantName The consultantName to set.
	 */
	public void setConsultantName(String consultantName) {
		this.consultantName = consultantName;
	}
	/**
	 * @return Returns the createdBy.
	 */
	public String getCreatedBy() {
		return CommonUtility.replaceNull(createdBy);
	}
	/**
	 * @param createdBy The createdBy to set.
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return Returns the createdDate.
	 */
	public String getCreatedDate() {
		return CommonUtility.replaceNull(createdDate);
	}
	/**
	 * @param createdDate The createdDate to set.
	 */
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	/**
	 * @return Returns the crossReference.
	 */
	public String getCrossReference() {
		return CommonUtility.replaceNull(crossReference);
	}
	/**
	 * @param crossReference The crossReference to set.
	 */
	public void setCrossReference(String crossReference) {
		this.crossReference = crossReference;
	}
	/**
	 * @return Returns the customerName.
	 */
	public String getCustomerName() {
		return CommonUtility.replaceNull(customerName);
	}
	/**
	 * @param customerName The customerName to set.
	 */
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	/**
	 * @return Returns the description.
	 */
	public String getDescription() {
		return CommonUtility.replaceNull(description);
	}
	/**
	 * @param description The description to set.
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return Returns the enquiryType.
	 */
	public String getEnquiryType() {
		return CommonUtility.replaceNull(enquiryType);
	}
	/**
	 * @param enquiryType The enquiryType to set.
	 */
	public void setEnquiryType(String enquiryType) {
		this.enquiryType = enquiryType;
	}
	/**
	 * @return Returns the location.
	 */
	public String getLocation() {
		return CommonUtility.replaceNull(location);
	}
	/**
	 * @param location The location to set.
	 */
	public void setLocation(String location) {
		this.location = location;
	}
	/**
	 * @return Returns the enquiryNumber.
	 */
	public String getEnquiryNumber() {
		return CommonUtility.replaceNull(enquiryNumber);
	}
	/**
	 * @param enquiryNumber The enquiryNumber to set.
	 */
	public void setEnquiryNumber(String enquiryNumber) {
		this.enquiryNumber = enquiryNumber;
	}
	/**
	 * @return Returns the createdBySSO.
	 */
	public String getCreatedBySSO() {
		return CommonUtility.replaceNull(createdBySSO);
	}
	/**
	 * @param createdBySSO The createdBySSO to set.
	 */
	public void setCreatedBySSO(String createdBySSO) {
		this.createdBySSO = createdBySSO;
	}
	/**
	 * @return Returns the ratingList.
	 */
	public ArrayList getRatingList() {
		return ratingList;
	}
	/**
	 * @param ratingList The ratingList to set.
	 */
	public void setRatingList(ArrayList ratingList) {
		this.ratingList = ratingList;
	}
	/**
	 * @return Returns the ratingOperationId.
	 */
	public String getRatingOperationId() {
		return CommonUtility.replaceNull(ratingOperationId);
	}
	/**
	 * @param ratingOperationId The ratingOperationId to set.
	 */
	public void setRatingOperationId(String ratingOperationId) {
		this.ratingOperationId = ratingOperationId;
	}
	/**
	 * @return Returns the currentRating.
	 */
	public String getCurrentRating() {
		return CommonUtility.replaceNull(currentRating);
	}
	/**
	 * @param currentRating The currentRating to set.
	 */
	public void setCurrentRating(String currentRating) {
		this.currentRating = currentRating;
	}
	/**
	 * @return Returns the addOnList.
	 */
	public ArrayList getAddOnList() {
		return addOnList;
	}
	/**
	 * @param addOnList The addOnList to set.
	 */
	public void setAddOnList(ArrayList addOnList) {
		this.addOnList = addOnList;
	}
	/**
	 * @return Returns the approverCommentsList.
	 */
	public ArrayList getApproverCommentsList() {
		return approverCommentsList;
	}
	/**
	 * @param approverCommentsList The approverCommentsList to set.
	 */
	public void setApproverCommentsList(ArrayList approverCommentsList) {
		this.approverCommentsList = approverCommentsList;
	}
	/**
	 * @return Returns the discount.
	 */
	public int getDiscount() {
		return discount;
	}
	/**
	 * @param discount The discount to set.
	 */
	public void setDiscount(int discount) {
		this.discount = discount;
	}
	/**
	 * @return Returns the discountRemarks.
	 */
	public String getDiscountRemarks() {
		return CommonUtility.replaceNull(discountRemarks);
	}
	/**
	 * @param discountRemarks The discountRemarks to set.
	 */
	public void setDiscountRemarks(String discountRemarks) {
		this.discountRemarks = discountRemarks;
	}
	/**
	 * @return Returns the ratingFactorList.
	 */
	public ArrayList getRatingFactorList() {
		return ratingFactorList;
	}
	/**
	 * @param ratingFactorList The ratingFactorList to set.
	 */
	public void setRatingFactorList(ArrayList ratingFactorList) {
		this.ratingFactorList = ratingFactorList;
	}
	/**
	 * @return Returns the teamMemberList.
	 */
	public ArrayList getTeamMemberList() {
		return teamMemberList;
	}
	/**
	 * @param teamMemberList The teamMemberList to set.
	 */
	public void setTeamMemberList(ArrayList teamMemberList) {
		this.teamMemberList = teamMemberList;
	}
	/**
	 * @return Returns the returnTo.
	 */
	public String getReturnTo() {
		return CommonUtility.replaceNull(returnTo);
	}
	/**
	 * @param returnTo The returnTo to set.
	 */
	public void setReturnTo(String returnTo) {
		this.returnTo = returnTo;
	}
	/**
	 * @return Returns the returnToRemarks.
	 */
	public String getReturnToRemarks() {
		return CommonUtility.replaceNull(returnToRemarks);
	}
	/**
	 * @param returnToRemarks The returnToRemarks to set.
	 */
	public void setReturnToRemarks(String returnToRemarks) {
		this.returnToRemarks = returnToRemarks;
	}
	
	public String getTermsDeliveryHT() {
		return termsDeliveryHT;
	}
	public void setTermsDeliveryHT(String termsDeliveryHT) {
		this.termsDeliveryHT = termsDeliveryHT;
	}
	public String getTermsDeliveryLT() {
		return termsDeliveryLT;
	}
	public void setTermsDeliveryLT(String termsDeliveryLT) {
		this.termsDeliveryLT = termsDeliveryLT;
	}
	public String getTermsPercentAdv() {
		return termsPercentAdv;
	}
	public void setTermsPercentAdv(String termsPercentAdv) {
		this.termsPercentAdv = termsPercentAdv;
	}
	public String getTermsPercentBal() {
		return termsPercentBal;
	}
	public void setTermsPercentBal(String termsPercentBal) {
		this.termsPercentBal = termsPercentBal;
	}
	public String getTermsVarFormula() {
		return termsVarFormula;
	}
	public void setTermsVarFormula(String termsVarFormula) {
		this.termsVarFormula = termsVarFormula;
	}
	/**
	 * @return Returns the ceFactor.
	 */
	public long getCeFactor() {
		return ceFactor;
	}
	/**
	 * @param ceFactor The ceFactor to set.
	 */
	public void setCeFactor(long ceFactor) {
		this.ceFactor = ceFactor;
	}
	/**
	 * @return Returns the rsmFactor.
	 */
	public long getRsmFactor() {
		return rsmFactor;
	}
	/**
	 * @param rsmFactor The rsmFactor to set.
	 */
	public void setRsmFactor(long rsmFactor) {
		this.rsmFactor = rsmFactor;
	}
    /**
     * @return Returns the baseEnquiryId.
     */
    public int getBaseEnquiryId() {
        return this.baseEnquiryId;
    }
    /**
     * @param baseEnquiryId The baseEnquiryId to set.
     */
    public void setBaseEnquiryId(int baseEnquiryId) {
        this.baseEnquiryId = baseEnquiryId;
    }
    
    public String getCustomerType() {
		return CommonUtility.replaceNull(customerType);
	}
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	public String getEndUser() {
		return CommonUtility.replaceNull(endUser);
	}
	public void setEndUser(String endUser) {
		this.endUser = endUser;
	}
	public String getOem() {
		return CommonUtility.replaceNull(oem);
	}
	public void setOem(String oem) {
		this.oem = oem;
	}
	public String getEpc() {
		return CommonUtility.replaceNull(epc);
	}
	public void setEpc(String epc) {
		this.epc = epc;
	}
	public String getDealer() {
		return CommonUtility.replaceNull(dealer);
	}
	public void setDealer(String dealer) {
		this.dealer = dealer;
	}
	public String getProjectName() {
		return CommonUtility.replaceNull(projectName);
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getIndustry() {
		return CommonUtility.replaceNull(industry);
	}
	public void setIndustry(String industry) {
		this.industry = industry;
	}
	public String getEndUserCountry() {
		return CommonUtility.replaceNull(endUserCountry);
	}
	public void setEndUserCountry(String endUserCountry) {
		this.endUserCountry = endUserCountry;
	}
	public String getEndUserIndustry() {
		return CommonUtility.replaceNull(endUserIndustry);
	}
	public void setEndUserIndustry(String endUserIndustry) {
		this.endUserIndustry = endUserIndustry;
	}
	public String getApproval() {
		return CommonUtility.replaceNull(approval);
	}
	public void setApproval(String approval) {
		this.approval = approval;
	}
	public String getTargeted() {
		return CommonUtility.replaceNull(targeted);
	}
	public void setTargeted(String targeted) {
		this.targeted = targeted;
	}
	public String getWinningChance() {
		return CommonUtility.replaceNull(winningChance);
	}
	public void setWinningChance(String winningChance) {
		this.winningChance = winningChance;
	}
	public String getCompetitor() {
		return CommonUtility.replaceNull(competitor);
	}
	public void setCompetitor(String competitor) {
		this.competitor = competitor;
	}
	public String getSalesStage() {
		return CommonUtility.replaceNull(salesStage);
	}
	public void setSalesStage(String salesStage) {
		this.salesStage = salesStage;
	}
	public String getTotalOrderValue() {
		return CommonUtility.replaceNull(totalOrderValue);
	}
	public void setTotalOrderValue(String totalOrderValue) {
		this.totalOrderValue = totalOrderValue;
	}
	public String getOrderClosingMonth() {
		return CommonUtility.replaceNull(orderClosingMonth);
	}
	public void setOrderClosingMonth(String orderClosingMonth) {
		this.orderClosingMonth = orderClosingMonth;
	}
	public String getExpectedDeliveryMonth() {
		return CommonUtility.replaceNull(expectedDeliveryMonth);
	}
	public void setExpectedDeliveryMonth(String expectedDeliveryMonth) {
		this.expectedDeliveryMonth = expectedDeliveryMonth;
	}
	public String getWarrantyDispatchDate() {
		return CommonUtility.replaceNull(warrantyDispatchDate);
	}
	public void setWarrantyDispatchDate(String warrantyDispatchDate) {
		this.warrantyDispatchDate = warrantyDispatchDate;
	}
	public String getWarrantyCommissioningDate() {
		return CommonUtility.replaceNull(warrantyCommissioningDate);
	}
	public void setWarrantyCommissioningDate(String warrantyCommissioningDate) {
		this.warrantyCommissioningDate = warrantyCommissioningDate;
	}
	public String getDeliveryType() {
		return CommonUtility.replaceNull(deliveryType);
	}
	public void setDeliveryType(String deliveryType) {
		this.deliveryType = deliveryType;
	}
	public String getDestState() {
		return CommonUtility.replaceNull(destState);
	}
	public void setDestState(String destState) {
		this.destState = destState;
	}
	public String getDestCity() {
		return CommonUtility.replaceNull(destCity);
	}
	public void setDestCity(String destCity) {
		this.destCity = destCity;
	}
	public String getSpa() {
		return CommonUtility.replaceNull(spa);
	}
	public void setSpa(String spa) {
		this.spa = spa;
	}
	public double getCustomerCounterOffer() {
		return customerCounterOffer;
	}
	public void setCustomerCounterOffer(double customerCounterOffer) {
		this.customerCounterOffer = customerCounterOffer;
	}
	public String getMaterialCost() {
		return CommonUtility.replaceNull(materialCost);
	}
	public void setMaterialCost(String materialCost) {
		this.materialCost = materialCost;
	}
	public String getMcnspRatio() {
		return CommonUtility.replaceNull(mcnspRatio);
	}
	public void setMcnspRatio(String mcnspRatio) {
		this.mcnspRatio = mcnspRatio;
	}
	public double getMotorPrice() {
		return motorPrice;
	}
	public void setMotorPrice(double motorPrice) {
		this.motorPrice = motorPrice;
	}
	public String getWarrantyCost() {
		return CommonUtility.replaceNull(warrantyCost);
	}
	public void setWarrantyCost(String warrantyCost) {
		this.warrantyCost = warrantyCost;
	}
	public String getTransportationCost() {
		return CommonUtility.replaceNull(transportationCost);
	}
	public void setTransportationCost(String transportationCost) {
		this.transportationCost = transportationCost;
	}
	public String getEcSupervisionCost() {
		return CommonUtility.replaceNull(ecSupervisionCost);
	}
	public void setEcSupervisionCost(String ecSupervisionCost) {
		this.ecSupervisionCost = ecSupervisionCost;
	}
	public String getHzAreaCertCost() {
		return CommonUtility.replaceNull(hzAreaCertCost);
	}
	public void setHzAreaCertCost(String hzAreaCertCost) {
		this.hzAreaCertCost = hzAreaCertCost;
	}
	public String getSparesCost() {
		return CommonUtility.replaceNull(sparesCost);
	}
	public void setSparesCost(String sparesCost) {
		this.sparesCost = sparesCost;
	}
	public double getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}
	public String getPropSpaReason() {
		return CommonUtility.replaceNull(propSpaReason);
	}
	public void setPropSpaReason(String propSpaReason) {
		this.propSpaReason = propSpaReason;
	}
	public String getWinlossReason() {
		return CommonUtility.replaceNull(winlossReason);
	}
	public void setWinlossReason(String winlossReason) {
		this.winlossReason = winlossReason;
	}
	public String getWinlossComment() {
		return CommonUtility.replaceNull(winlossComment);
	}
	public void setWinlossComment(String winlossComment) {
		this.winlossComment = winlossComment;
	}
	public String getWinningCompetitor() {
		return CommonUtility.replaceNull(winningCompetitor);
	}
	public void setWinningCompetitor(String winningCompetitor) {
		this.winningCompetitor = winningCompetitor;
	}
	public double getUnitMC() {
		return unitMC;
	}
	public void setUnitMC(double unitMC) {
		this.unitMC = unitMC;
	}
	public double getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}
	public String getWinlossPrice() {
		return winlossPrice;
	}
	public void setWinlossPrice(String winlossPrice) {
		this.winlossPrice = winlossPrice;
	}
	public double getTotalMC() {
		return totalMC;
	}
	public void setTotalMC(double totalMC) {
		this.totalMC = totalMC;
	}
	public String getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(String totalPrice) {
		this.totalPrice = totalPrice;
	}
	public String getTotalPkgPrice() {
		return totalPkgPrice;
	}
	public void setTotalPkgPrice(String totalPkgPrice) {
		this.totalPkgPrice = totalPkgPrice;
	}
	public ArrayList getAlCustomerTypeList() {
		return alCustomerTypeList;
	}
	public void setAlCustomerTypeList(ArrayList alCustomerTypeList) {
		this.alCustomerTypeList = alCustomerTypeList;
	}
	public ArrayList getAlIndustryList() {
		return alIndustryList;
	}
	public void setAlIndustryList(ArrayList alIndustryList) {
		this.alIndustryList = alIndustryList;
	}
	public ArrayList getAlApprovalList() {
		return alApprovalList;
	}
	public void setAlApprovalList(ArrayList alApprovalList) {
		this.alApprovalList = alApprovalList;
	}
	public ArrayList getAlTargetedList() {
		return alTargetedList;
	}
	public void setAlTargetedList(ArrayList alTargetedList) {
		this.alTargetedList = alTargetedList;
	}
	public ArrayList getAlWinningChanceList() {
		return alWinningChanceList;
	}
	public void setAlWinningChanceList(ArrayList alWinningChanceList) {
		this.alWinningChanceList = alWinningChanceList;
	}
	public ArrayList getAlCompetitorList() {
		return alCompetitorList;
	}
	public void setAlCompetitorList(ArrayList alCompetitorList) {
		this.alCompetitorList = alCompetitorList;
	}
	public ArrayList getAlEnquiryTypeList() {
		return alEnquiryTypeList;
	}
	public void setAlEnquiryTypeList(ArrayList alEnquiryTypeList) {
		this.alEnquiryTypeList = alEnquiryTypeList;
	}
	public ArrayList getAlSalesStageList() {
		return alSalesStageList;
	}
	public void setAlSalesStageList(ArrayList alSalesStageList) {
		this.alSalesStageList = alSalesStageList;
	}
	public ArrayList getAlDeliveryTypeList() {
		return alDeliveryTypeList;
	}
	public void setAlDeliveryTypeList(ArrayList alDeliveryTypeList) {
		this.alDeliveryTypeList = alDeliveryTypeList;
	}
	public ArrayList getAlSPAList() {
		return alSPAList;
	}
	public void setAlSPAList(ArrayList alSPAList) {
		this.alSPAList = alSPAList;
	}
	public ArrayList getAlWinlossReasonList() {
		return alWinlossReasonList;
	}
	public void setAlWinlossReasonList(ArrayList alWinlossReasonList) {
		this.alWinlossReasonList = alWinlossReasonList;
	}
	public ArrayList getAlWinCompetitorList() {
		return alWinCompetitorList;
	}
	public void setAlWinCompetitorList(ArrayList alWinCompetitorList) {
		this.alWinCompetitorList = alWinCompetitorList;
	}
	public String getRequestRemarks() {
		return CommonUtility.replaceNull(requestRemarks);
	}
	public void setRequestRemarks(String requestRemarks) {
		this.requestRemarks = requestRemarks;
	}
	public String getEnquiryReferenceNumber() {
		return CommonUtility.replaceNull(enquiryReferenceNumber);
	}
	public void setEnquiryReferenceNumber(String enquiryReferenceNumber) {
		this.enquiryReferenceNumber = enquiryReferenceNumber;
	}
	
	public ArrayList getAlVoltList() {
		return alVoltList;
	}
	public void setAlVoltList(ArrayList alVoltList) {
		this.alVoltList = alVoltList;
	}
	public ArrayList getAlWinChanceList() {
		return alWinChanceList;
	}
	public void setAlWinChanceList(ArrayList alWinChanceList) {
		this.alWinChanceList = alWinChanceList;
	}
	
	public String getSavingIndent() {
		return CommonUtility.replaceNull(savingIndent);
	}
	public void setSavingIndent(String savingIndent) {
		this.savingIndent = savingIndent;
	}
	public String getRevision() {
		return CommonUtility.replaceNull(revision);
	}
	public void setRevision(String revision) {
		this.revision = revision;
	}
	public String getLastModifiedDate() {
		return CommonUtility.replaceNull(lastModifiedDate);
	}
	public void setLastModifiedDate(String lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	public String getDeptAndAuth() {
		return CommonUtility.replaceNull(deptAndAuth);
	}
	public void setDeptAndAuth(String deptAndAuth) {
		this.deptAndAuth = deptAndAuth;
	}
	public String getAdvancePayment() {
		return CommonUtility.replaceNull(advancePayment);
	}
	public void setAdvancePayment(String advancePayment) {
		this.advancePayment = advancePayment;
	}
	public String getPaymentTerms() {
		return CommonUtility.replaceNull(paymentTerms);
	}
	public void setPaymentTerms(String paymentTerms) {
		this.paymentTerms = paymentTerms;
	}
	public String getCustomerNameMultiplier() {
		return CommonUtility.replaceNull(customerNameMultiplier);
	}
	public void setCustomerNameMultiplier(String customerNameMultiplier) {
		this.customerNameMultiplier = customerNameMultiplier;
	}
	public String getCustomerTypeMultiplier() {
		return CommonUtility.replaceNull(customerTypeMultiplier);
	}
	public void setCustomerTypeMultiplier(String customerTypeMultiplier) {
		this.customerTypeMultiplier = customerTypeMultiplier;
	}
	public String getEndUserMultiplier() {
		return CommonUtility.replaceNull(endUserMultiplier);
	}
	public void setEndUserMultiplier(String endUserMultiplier) {
		this.endUserMultiplier = endUserMultiplier;
	}
	public String getWarrantyDispatchMultiplier() {
		return CommonUtility.replaceNull(warrantyDispatchMultiplier);
	}
	public void setWarrantyDispatchMultiplier(String warrantyDispatchMultiplier) {
		this.warrantyDispatchMultiplier = warrantyDispatchMultiplier;
	}
	public String getWarrantycommissioningMultiplier() {
		return CommonUtility.replaceNull(warrantycommissioningMultiplier);
	}
	public void setWarrantycommissioningMultiplier(
			String warrantycommissioningMultiplier) {
		this.warrantycommissioningMultiplier = warrantycommissioningMultiplier;
	}
	public String getAdvancePaymMultiplier() {
		return CommonUtility.replaceNull(advancePaymMultiplier);
	}
	public void setAdvancePaymMultiplier(String advancePaymMultiplier) {
		this.advancePaymMultiplier = advancePaymMultiplier;
	}
	public String getPayTermMultiplier() {
		return CommonUtility.replaceNull(payTermMultiplier);
	}
	public void setPayTermMultiplier(String payTermMultiplier) {
		this.payTermMultiplier = payTermMultiplier;
	}
	public String getExpDeliveryMultiplier() {
		return CommonUtility.replaceNull(expDeliveryMultiplier);
	}
	public void setExpDeliveryMultiplier(String expDeliveryMultiplier) {
		this.expDeliveryMultiplier = expDeliveryMultiplier;
	}
	public String getFinanceMultiplier() {
		return CommonUtility.replaceNull(financeMultiplier);
	}
	public void setFinanceMultiplier(String financeMultiplier) {
		this.financeMultiplier = financeMultiplier;
	}
	public String getQuantityMultiplier() {
		return CommonUtility.replaceNull(quantityMultiplier);
	}
	public void setQuantityMultiplier(String quantityMultiplier) {
		this.quantityMultiplier = quantityMultiplier;
	}
	public String getRatedOutputPNMulitplier() {
		return CommonUtility.replaceNull(ratedOutputPNMulitplier);
	}
	public void setRatedOutputPNMulitplier(String ratedOutputPNMulitplier) {
		this.ratedOutputPNMulitplier = ratedOutputPNMulitplier;
	}
	public String getTypeMultiplier() {
		return CommonUtility.replaceNull(typeMultiplier);
	}
	public void setTypeMultiplier(String typeMultiplier) {
		this.typeMultiplier = typeMultiplier;
	}
	public String getAreaCMultiplier() {
		return CommonUtility.replaceNull(areaCMultiplier);
	}
	public void setAreaCMultiplier(String areaCMultiplier) {
		this.areaCMultiplier = areaCMultiplier;
	}
	public String getFrameMultiplier() {
		return CommonUtility.replaceNull(frameMultiplier);
	}
	public void setFrameMultiplier(String frameMultiplier) {
		this.frameMultiplier = frameMultiplier;
	}
	public String getApplicationMultiplier() {
		return CommonUtility.replaceNull(applicationMultiplier);
	}
	public void setApplicationMultiplier(String applicationMultiplier) {
		this.applicationMultiplier = applicationMultiplier;
	}
	public String getFactorMultiplier() {
		return CommonUtility.replaceNull(factorMultiplier);
	}
	public void setFactorMultiplier(String factorMultiplier) {
		this.factorMultiplier = factorMultiplier;
	}
	public String getNetmcMultiplier() {
		return CommonUtility.replaceNull(netmcMultiplier);
	}
	public void setNetmcMultiplier(String netmcMultiplier) {
		this.netmcMultiplier = netmcMultiplier;
	}
	public String getMarkupMultiplier() {
		return CommonUtility.replaceNull(markupMultiplier);
	}
	public void setMarkupMultiplier(String markupMultiplier) {
		this.markupMultiplier = markupMultiplier;
	}
	public String getTransferCMultiplier() {
		return CommonUtility.replaceNull(transferCMultiplier);
	}
	public void setTransferCMultiplier(String transferCMultiplier) {
		this.transferCMultiplier = transferCMultiplier;
	}
	public Map<String, EnquiryDto> getMultiplierMap() {
		return MultiplierMap;
	}
	public void setMultiplierMap(Map<String, EnquiryDto> multiplierMap) {
		MultiplierMap = multiplierMap;
	}
	public String getMcornspratioMultiplier_rsm() {
		return CommonUtility.replaceNull(mcornspratioMultiplier_rsm);
	}
	public void setMcornspratioMultiplier_rsm(String mcornspratioMultiplier_rsm) {
		this.mcornspratioMultiplier_rsm = mcornspratioMultiplier_rsm;
	}
	public String getUnitpriceMultiplier_rsm() {
		return CommonUtility.replaceNull(unitpriceMultiplier_rsm);
	}
	public void setUnitpriceMultiplier_rsm(String unitpriceMultiplier_rsm) {
		this.unitpriceMultiplier_rsm = unitpriceMultiplier_rsm;
	}
	public String getTotalpriceMultiplier_rsm() {
		return CommonUtility.replaceNull(totalpriceMultiplier_rsm);
	}
	public void setTotalpriceMultiplier_rsm(String totalpriceMultiplier_rsm) {
		this.totalpriceMultiplier_rsm = totalpriceMultiplier_rsm;
	}
	public String getWarrantyperunit_rsm() {
		return CommonUtility.replaceNull(warrantyperunit_rsm);
	}
	public void setWarrantyperunit_rsm(String warrantyperunit_rsm) {
		this.warrantyperunit_rsm = warrantyperunit_rsm;
	}
	public String getTransportationperunit_rsm() {
		return CommonUtility.replaceNull(transportationperunit_rsm);
	}
	public void setTransportationperunit_rsm(String transportationperunit_rsm) {
		this.transportationperunit_rsm = transportationperunit_rsm;
	}
	public String getSmnote() {
		return CommonUtility.replaceNull(smnote);
	}
	public void setSmnote(String smnote) {
		this.smnote = smnote;
	}
	public String getDmnote() {
		return CommonUtility.replaceNull(dmnote);
	}
	public void setDmnote(String dmnote) {
		this.dmnote = dmnote;
	}
	public String getCenote() {
		return CommonUtility.replaceNull(cenote);
	}
	public void setCenote(String cenote) {
		this.cenote = cenote;
	}
	public String getRsmnote() {
		return CommonUtility.replaceNull(rsmnote);
	}
	public void setRsmnote(String rsmnote) {
		this.rsmnote = rsmnote;
	}
	public String getSm_name() {
		return CommonUtility.replaceNull(sm_name);
	}
	public void setSm_name(String sm_name) {
		this.sm_name = sm_name;
	}
	public String getDm_ssoid() {
		return CommonUtility.replaceNull(dm_ssoid);
	}
	public void setDm_ssoid(String dm_ssoid) {
		this.dm_ssoid = dm_ssoid;
	}
	public String getDm_name() {
		return CommonUtility.replaceNull(dm_name);
	}
	public void setDm_name(String dm_name) {
		this.dm_name = dm_name;
	}
	public String getCe_ssoid() {
		return CommonUtility.replaceNull(ce_ssoid);
	}
	public void setCe_ssoid(String ce_ssoid) {
		this.ce_ssoid = ce_ssoid;
	}
	public String getCe_name() {
		return CommonUtility.replaceNull(ce_name);
	}
	public void setCe_name(String ce_name) {
		this.ce_name = ce_name;
	}
	public String getRsm_ssoid() {
		return CommonUtility.replaceNull(rsm_ssoid);
	}
	public void setRsm_ssoid(String rsm_ssoid) {
		this.rsm_ssoid = rsm_ssoid;
	}
	public String getRsm_name() {
		return CommonUtility.replaceNull(rsm_name);
	}
	public void setRsm_name(String rsm_name) {
		this.rsm_name = rsm_name;
	}
	public String getPagechecking() {
		return CommonUtility.replaceNull(pagechecking);
	}
	public void setPagechecking(String pagechecking) {
		this.pagechecking = pagechecking;
	}
	public ArrayList getSalesManagerList() {
		return salesManagerList;
	}
	public void setSalesManagerList(ArrayList salesManagerList) {
		this.salesManagerList = salesManagerList;
	}
	public String getReturnoperation() {
		return CommonUtility.replaceNull(returnoperation);
	}
	public void setReturnoperation(String returnoperation) {
		this.returnoperation = returnoperation;
	}
	public String getDeviation_clarification() {
		return CommonUtility.replaceNull(deviation_clarification);
	}
	public void setDeviation_clarification(String deviation_clarification) {
		this.deviation_clarification = deviation_clarification;
	}
	public String getZone() {
		return CommonUtility.replaceNull(zone);
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	public String getEnquiryOperationType() {
		return CommonUtility.replaceNull(enquiryOperationType);
	}
	public void setEnquiryOperationType(String enquiryOperationType) {
		this.enquiryOperationType = enquiryOperationType;
	}
	public ArrayList getPreviousratingList() {
		return previousratingList;
	}
	public void setPreviousratingList(ArrayList previousratingList) {
		this.previousratingList = previousratingList;
	}
	public String getGasGroup() {
		return CommonUtility.replaceNull(gasGroup);
	}
	public void setGasGroup(String gasGroup) {
		this.gasGroup = gasGroup;
	}
	public String getDmToSmReturn() {
		return CommonUtility.replaceNull(dmToSmReturn);
	}
	public void setDmToSmReturn(String dmToSmReturn) {
		this.dmToSmReturn = dmToSmReturn;
	}
	public String getCeToDmReturn() {
		return CommonUtility.replaceNull(ceToDmReturn);
	}
	public void setCeToDmReturn(String ceToDmReturn) {
		this.ceToDmReturn = ceToDmReturn;
	}
	public String getCeToSmReturn() {
		return CommonUtility.replaceNull(ceToSmReturn);
	}
	public void setCeToSmReturn(String ceToSmReturn) {
		this.ceToSmReturn = ceToSmReturn;
	}
	public String getRsmToCeReturn() {
		return CommonUtility.replaceNull(rsmToCeReturn);
	}
	public void setRsmToCeReturn(String rsmToCeReturn) {
		this.rsmToCeReturn = rsmToCeReturn;
	}
	public String getRsmToDmReturn() {
		return CommonUtility.replaceNull(rsmToDmReturn);
	}
	public void setRsmToDmReturn(String rsmToDmReturn) {
		this.rsmToDmReturn = rsmToDmReturn;
	}
	public String getRsmToSmReturn() {
		return CommonUtility.replaceNull(rsmToSmReturn);
	}
	public void setRsmToSmReturn(String rsmToSmReturn) {
		this.rsmToSmReturn = rsmToSmReturn;
	}
	public String getCommentOperation() {
		return CommonUtility.replaceNull(commentOperation);
	}
	public void setCommentOperation(String commentOperation) {
		this.commentOperation = commentOperation;
	}
	public String getDeToSmReturn() {
		return CommonUtility.replaceNull(deToSmReturn);
	}
	public void setDeToSmReturn(String deToSmReturn) {
		this.deToSmReturn = deToSmReturn;
	}
	public String getQuotationtoRsmReturn() {
		return CommonUtility.replaceNull(quotationtoRsmReturn);
	}
	public void setQuotationtoRsmReturn(String quotationtoRsmReturn) {
		this.quotationtoRsmReturn = quotationtoRsmReturn;
	}
	public String getQuotationtoCeReturn() {
		return CommonUtility.replaceNull(quotationtoCeReturn);
	}
	public void setQuotationtoCeReturn(String quotationtoCeReturn) {
		this.quotationtoCeReturn = quotationtoCeReturn;
	}
	public String getQuotationtoDmReturn() {
		return CommonUtility.replaceNull(quotationtoDmReturn);
	}
	public void setQuotationtoDmReturn(String quotationtoDmReturn) {
		this.quotationtoDmReturn = quotationtoDmReturn;
	}
	public String getAttachmentFileName() {
		return CommonUtility.replaceNull(attachmentFileName);
	}
	public void setAttachmentFileName(String attachmentFileName) {
		this.attachmentFileName = attachmentFileName;
	}
	public int getAttachmentId() {
		return attachmentId;
	}
	public void setAttachmentId(int attachmentId) {
		this.attachmentId = attachmentId;
	}
	public String getIndentOperation() {
		return CommonUtility.replaceNull(indentOperation);
	}
	public void setIndentOperation(String indentOperation) {
		this.indentOperation = indentOperation;
	}
	public String getIndentOperationCheck() {
		return CommonUtility.replaceNull(indentOperationCheck);
	}
	public void setIndentOperationCheck(String indentOperationCheck) {
		this.indentOperationCheck = indentOperationCheck;
	}
	public String getIndentnote() {
		return CommonUtility.replaceNull(indentnote);
	}
	public void setIndentnote(String indentnote) {
		this.indentnote = indentnote;
	}
	public String getCommengToSmReturn() {
		return CommonUtility.replaceNull(commengToSmReturn);
	}
	public void setCommengToSmReturn(String commengToSmReturn) {
		this.commengToSmReturn = commengToSmReturn;
	}
	public String getIndentDmtosmReturn() {
		return CommonUtility.replaceNull(indentDmtosmReturn);
	}
	public void setIndentDmtosmReturn(String indentDmtosmReturn) {
		this.indentDmtosmReturn = indentDmtosmReturn;
	}
	public String getIndentDetosmReturn() {
		return CommonUtility.replaceNull(indentDetosmReturn);
	}
	public void setIndentDetosmReturn(String indentDetosmReturn) {
		this.indentDetosmReturn = indentDetosmReturn;
	}
	public String getCmNote() {
		return CommonUtility.replaceNull(cmNote);
	}
	public void setCmNote(String cmNote) {
		this.cmNote = cmNote;
	}
	public String getCmName() {
		return CommonUtility.replaceNull(cmName);
	}
	public void setCmName(String cmName) {
		this.cmName = cmName;
	}
	public String getOrderNumber() {
		return CommonUtility.replaceNull(orderNumber);
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public String getPoiCopy() {
		return CommonUtility.replaceNull(poiCopy);
	}
	public void setPoiCopy(String poiCopy) {
		this.poiCopy = poiCopy;
	}
	public String getLoiCopy() {
		return CommonUtility.replaceNull(loiCopy);
	}
	public void setLoiCopy(String loiCopy) {
		this.loiCopy = loiCopy;
	}
	public String getDeliveryReqByCust() {
		return CommonUtility.replaceNull(deliveryReqByCust);
	}
	public void setDeliveryReqByCust(String deliveryReqByCust) {
		this.deliveryReqByCust = deliveryReqByCust;
	}
	public String getLd() {
		return CommonUtility.replaceNull(ld);
	}
	public void setLd(String ld) {
		this.ld = ld;
	}
	public String getDrawingApproval() {
		return CommonUtility.replaceNull(drawingApproval);
	}
	public void setDrawingApproval(String drawingApproval) {
		this.drawingApproval = drawingApproval;
	}
	public String getQapApproval() {
		return CommonUtility.replaceNull(qapApproval);
	}
	public void setQapApproval(String qapApproval) {
		this.qapApproval = qapApproval;
	}
	public String getTypeTest() {
		return CommonUtility.replaceNull(typeTest);
	}
	public void setTypeTest(String typeTest) {
		this.typeTest = typeTest;
	}
	public String getRoutineTest() {
		return CommonUtility.replaceNull(routineTest);
	}
	public void setRoutineTest(String routineTest) {
		this.routineTest = routineTest;
	}
	public String getSpecialTest() {
		return CommonUtility.replaceNull(specialTest);
	}
	public void setSpecialTest(String specialTest) {
		this.specialTest = specialTest;
	}
	public String getSpecialTestDetails() {
		return CommonUtility.replaceNull(specialTestDetails);
	}
	public void setSpecialTestDetails(String specialTestDetails) {
		this.specialTestDetails = specialTestDetails;
	}
	public String getThirdPartyInspection() {
		return CommonUtility.replaceNull(thirdPartyInspection);
	}
	public void setThirdPartyInspection(String thirdPartyInspection) {
		this.thirdPartyInspection = thirdPartyInspection;
	}
	public String getStageInspection() {
		return CommonUtility.replaceNull(stageInspection);
	}
	public void setStageInspection(String stageInspection) {
		this.stageInspection = stageInspection;
	}
	public String getEnduserLocation() {
		return CommonUtility.replaceNull(enduserLocation);
	}
	public void setEnduserLocation(String enduserLocation) {
		this.enduserLocation = enduserLocation;
	}
	public String getEpcName() {
		return CommonUtility.replaceNull(epcName);
	}
	public void setEpcName(String epcName) {
		this.epcName = epcName;
	}
	public String getPacking() {
		return CommonUtility.replaceNull(packing);
	}
	public void setPacking(String packing) {
		this.packing = packing;
	}
	public String getInsurance() {
		return CommonUtility.replaceNull(insurance);
	}
	public void setInsurance(String insurance) {
		this.insurance = insurance;
	}
	public String getReplacement() {
		return CommonUtility.replaceNull(replacement);
	}
	public void setReplacement(String replacement) {
		this.replacement = replacement;
	}
	public String getOldSerialNo() {
		return CommonUtility.replaceNull(oldSerialNo);
	}
	public void setOldSerialNo(String oldSerialNo) {
		this.oldSerialNo = oldSerialNo;
	}
	public String getCustomerPartNo() {
		return CommonUtility.replaceNull(customerPartNo);
	}
	public void setCustomerPartNo(String customerPartNo) {
		this.customerPartNo = customerPartNo;
	}
	public String getSapCustomerCode() {
		return CommonUtility.replaceNull(sapCustomerCode);
	}
	public void setSapCustomerCode(String sapCustomerCode) {
		this.sapCustomerCode = sapCustomerCode;
	}
	public String getIncludeFrieght() {
		return CommonUtility.replaceNull(includeFrieght);
	}
	public void setIncludeFrieght(String includeFrieght) {
		this.includeFrieght = includeFrieght;
	}
	public String getIncludeExtraWarrnt() {
		return CommonUtility.replaceNull(includeExtraWarrnt);
	}
	public void setIncludeExtraWarrnt(String includeExtraWarrnt) {
		this.includeExtraWarrnt = includeExtraWarrnt;
	}
	public String getIncludeSupervisionCost() {
		return CommonUtility.replaceNull(includeSupervisionCost);
	}
	public void setIncludeSupervisionCost(String includeSupervisionCost) {
		this.includeSupervisionCost = includeSupervisionCost;
	}
	public String getExtraFrieght() {
		return CommonUtility.replaceNull(extraFrieght);
	}
	public void setExtraFrieght(String extraFrieght) {
		this.extraFrieght = extraFrieght;
	}
	public String getExtraExtraWarrnt() {
		return CommonUtility.replaceNull(extraExtraWarrnt);
	}
	public void setExtraExtraWarrnt(String extraExtraWarrnt) {
		this.extraExtraWarrnt = extraExtraWarrnt;
	}
	public String getExtraSupervisionCost() {
		return CommonUtility.replaceNull(extraSupervisionCost);
	}
	public void setExtraSupervisionCost(String extraSupervisionCost) {
		this.extraSupervisionCost = extraSupervisionCost;
	}
	
	public String getIncludetypeTestCharges() {
		return CommonUtility.replaceNull(includetypeTestCharges);
	}
	public void setIncludetypeTestCharges(String includetypeTestCharges) {
		this.includetypeTestCharges = includetypeTestCharges;
	}
	public String getCmToSmReturn() {
		return CommonUtility.replaceNull(cmToSmReturn);
	}
	public void setCmToSmReturn(String cmToSmReturn) {
		this.cmToSmReturn = cmToSmReturn;
	}
	public String getDmToCmReturn() {
		return CommonUtility.replaceNull(dmToCmReturn);
	}
	public void setDmToCmReturn(String dmToCmReturn) {
		this.dmToCmReturn = dmToCmReturn;
	}
	public String getDeToCmReturn() {
		return CommonUtility.replaceNull(deToCmReturn);
	}
	public void setDeToCmReturn(String deToCmReturn) {
		this.deToCmReturn = deToCmReturn;
	}
	public String getLossPrice() {
		return CommonUtility.replaceNull(lossPrice);
	}
	public void setLossPrice(String lossPrice) {
		this.lossPrice = lossPrice;
	}
	public String getLossReason1() {
		return CommonUtility.replaceNull(lossReason1);
	}
	public void setLossReason1(String lossReason1) {
		this.lossReason1 = lossReason1;
	}
	public String getLossReason2() {
		return CommonUtility.replaceNull(lossReason2);
	}
	public void setLossReason2(String lossReason2) {
		this.lossReason2 = lossReason2;
	}
	public String getLossComment() {
		return CommonUtility.replaceNull(lossComment);
	}
	public void setLossComment(String lossComment) {
		this.lossComment = lossComment;
	}
	public String getLossCompetitor() {
		return CommonUtility.replaceNull(lossCompetitor);
	}
	public void setLossCompetitor(String lossCompetitor) {
		this.lossCompetitor = lossCompetitor;
	}
	public String getLossChecking() {
		return CommonUtility.replaceNull(lossChecking);
	}
	public void setLossChecking(String lossChecking) {
		this.lossChecking = lossChecking;
	}
	public String getAbondentComment() {
		return CommonUtility.replaceNull(abondentComment);
	}
	public void setAbondentComment(String abondentComment) {
		this.abondentComment = abondentComment;
	}
	public String getDmFlag() {
		return CommonUtility.replaceNull(dmFlag);
	}
	public void setDmFlag(String dmFlag) {
		this.dmFlag = dmFlag;
	}
	public String getSmEmail() {
		return CommonUtility.replaceNull(smEmail);
	}
	public void setSmEmail(String smEmail) {
		this.smEmail = smEmail;
	}
	public String getSmContactNo() {
		return CommonUtility.replaceNull(smContactNo);
	}
	public void setSmContactNo(String smContactNo) {
		this.smContactNo = smContactNo;
	}
	public String getTotalMotorPrice() {
		return CommonUtility.replaceNull(totalMotorPrice);
	}
	public void setTotalMotorPrice(String totalMotorPrice) {
		this.totalMotorPrice = totalMotorPrice;
	}
	public String getExtratypeTestCharges() {
		return CommonUtility.replaceNull(extratypeTestCharges);
	}
	public void setExtratypeTestCharges(String extratypeTestCharges) {
		this.extratypeTestCharges = extratypeTestCharges;
	}
	public String getEnqQuotedPrice() {
		return CommonUtility.replaceNull(enqQuotedPrice);
	}
	public void setEnqQuotedPrice(String enqQuotedPrice) {
		this.enqQuotedPrice = enqQuotedPrice;
	}
	
	public String getIsNewEnquiry() {
		return CommonUtility.replaceNull(isNewEnquiry);
	}
	public void setIsNewEnquiry(String isNewEnquiry) {
		this.isNewEnquiry = isNewEnquiry;
	}
	
	
	
	


	
}
