/**
 * ******************************************************************************************
 * Project Name: Request For Quotation
 * Document Name: DeliveryDetailsDto.java
 * Package: in.com.rbc.quotation.enquiry.dto
 * Desc: DTO class that holds getters and setters of each property.
 * ******************************************************************************************
 * @author 900010540
 * ******************************************************************************************
 */


package in.com.rbc.quotation.enquiry.dto;
import in.com.rbc.quotation.common.utility.CommonUtility;

public class DeliveryDetailsDto {
	/*
	 * TABLE - RFQ_DELIVERY_DETAILS
	 */
	
	private int deliveryRatingId; //QDD_RATINGID
	private String deliveryLocation ; //QDD_DELIVERYLOCATION
	private String deliveryAddress; //QDD_DELIVERYADDRESS
	private int noofMotors; //QAO_NOOFMOTORS
	
	public int getDeliveryRatingId() {
		return deliveryRatingId;
	}
	public void setDeliveryRatingId(int deliveryRatingId) {
		this.deliveryRatingId = deliveryRatingId;
	}
	public String getDeliveryLocation() {
		return CommonUtility.replaceNull(deliveryLocation);
	}
	public void setDeliveryLocation(String deliveryLocation) {
		this.deliveryLocation = deliveryLocation;
	}
	public String getDeliveryAddress() {
		return CommonUtility.replaceNull(deliveryAddress);
	}
	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}
	public int getNoofMotors() {
		return noofMotors;
	}
	public void setNoofMotors(int noofMotors) {
		this.noofMotors = noofMotors;
	}

}
