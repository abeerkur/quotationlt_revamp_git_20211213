/**
 * ******************************************************************************************
 * Project Name: Search For Quotation
 * Document Name: SearchQuotationDao.java
 * Package: in.com.rbc.quotation.enquiry.dao
 * Desc:  Dao interface that defines methods related to Quaotation Search functionalities. <br>
 *        This interface is implemented by --> SearchQuotationDaoImpl
 * ******************************************************************************************
 * @author 100006126
 * ******************************************************************************************
 */

package in.com.rbc.quotation.enquiry.dao;

import in.com.rbc.quotation.common.utility.ListModel;
import in.com.rbc.quotation.enquiry.dto.SearchEnquiryDto;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Hashtable;

public interface SearchEnquiryDao {

	Hashtable getEnquirySearchResults(Connection conn, SearchEnquiryDto searchEnquiryDto, ListModel listModel, String userId) throws Exception;

	ArrayList getEnquirySearchList(Connection conn, SearchEnquiryDto searchEnquiryDto, ListModel listModel, String userId, String string)throws Exception;

	boolean isSalesManager(Connection conn,String userId)throws Exception;

	String getSaleManagerSSObyId(Connection conn, String salesManagerId) throws Exception;
	
	/**
	* This method is used to set the flag whether to show quoted, order price in search results page 
	* @param conn Connection object to connect to database
	* @param String sCreatedSSO String value of enquiry created sso
	* @param String sLoginSSO String value of login user sso
	* @return Returns the boolean true if user is either SM, RSM or DM,CM,CE
	* @throws Exception If any error occurs during the process
	* @author 610092227
	* Created on: 16 Sept 2019
	*/
	
	boolean setQuotedFlag(Connection conn,String sCreatedSSO, String sLoginSSO) throws Exception;

}
