/**
 * ******************************************************************************************
 * Project Name: Request For Quotation
 * Document Name: RatingDto.java
 * Package: in.com.rbc.quotation.enquiry.dto
 * Desc: DTO class that holds getters and setters of each property.
 * ******************************************************************************************
 * @author 100006126
 * ******************************************************************************************
 */

package in.com.rbc.quotation.enquiry.dto;

import java.util.ArrayList;

import in.com.rbc.quotation.common.utility.CommonUtility;

public class RatingDto {
	
	private int ratingId;
	private int enquiryId;
	private int ratingNo;
	private int quantity;
	private String ratingTitle;
	
	private String ratingCreatedBy;
	private String ratingCreatedBySSO;
	
	private int htltId = 0;
	private int poleId = 0;
	private int scsrId = 0;
	private String htltName;
	private int dutyId = 0;
	private String poleName;
	private int degreeOfProId = 0;
	private int termBoxId = 0;
	private int directionOfRotId = 0;
	private int shaftExtId = 0;
	private int applicationId = 0;
	private int insulationId = 0;
	private int enclosureId = 0;
	private int mountingId = 0;
	
	private String scsrName;
	private String dutyName;
	private String degreeOfProName;
	private String termBoxName;
	private String directionOfRotName;
	private String shaftExtName;
	private String applicationName;
	private String insulationName;
	private String enclosureName;
	private String mountingName;
	
	private String methodOfStg;
	private String coupling;
	private String kw;
	private String frequency;
	private String frequencyVar;
	private String volts;
	private String voltsVar;
	private String ambience;
	private String ambienceVar;
	private String altitude;
	private String altitudeVar;
	private String tempRaise;
	private String tempRaiseDesc;
	private String tempRaiseVar;
	private String paint;
	private String splFeatures;
	private String details;
	private String isValidated;
	private String ratingUnitPrice;
	private String ratedOutputUnit;
	
	
	private ArrayList addOnList = new ArrayList();
	private ArrayList alAreaClassificationList = new ArrayList();
	private ArrayList alServiceFactorList = new ArrayList();
	private ArrayList alStrtgCurrentList = new ArrayList();	
	private ArrayList alClgSystemList = new ArrayList();
	private ArrayList alBearingDENDEList = new ArrayList();
	private ArrayList alLubricationList = new ArrayList();
	private ArrayList alBalancingList = new ArrayList();
	private ArrayList alVibrationList = new ArrayList();
	private ArrayList alMainTermBoxPSList = new ArrayList();
	private ArrayList alNeutralTBList = new ArrayList();
	private ArrayList alCableEntryList = new ArrayList();
	private ArrayList alAuxTermBoxList = new ArrayList();
	private ArrayList alStatorConnList = new ArrayList();
	private ArrayList alBoTerminalsNoList = new ArrayList();
	private ArrayList alRotationList = new ArrayList();
	private ArrayList alSpaceHeaterList = new ArrayList();
	private ArrayList alWindingRTDList = new ArrayList();
	private ArrayList alBearingRTDList = new ArrayList();
	private ArrayList alMinStartVoltList = new ArrayList();
	private ArrayList alMethodOfStgList = new ArrayList();
	private ArrayList alBearingThermoDtList = new ArrayList();
	private ArrayList alYesNoList = new ArrayList();
	private ArrayList alZoneList = new ArrayList();
	private ArrayList alGasGroupList = new ArrayList();
	

	private TechnicalOfferDto technicalDto = null;
	

	private String minStartVolt;
	private String unitPriceMultiplierRsm;
	
	// Variable Added By Gangadhar on 14-Feb-2019
	private String priceincludeFrieght;
	private String priceincludeExtraWarn;
	private String priceincludeSupervisionCost;
	private String priceincludetypeTestCharges;
	private String priceextraFrieght;
	private String priceextraExtraWarn;
	private String priceextraSupervisionCost;
	private String priceextratypeTestCharges;
	private String priceincludeFrieghtVal;
	private String priceincludeExtraWarnVal;
	private String priceincludeSupervisionCostVal;
	private String priceincludetypeTestChargesVal;
	private String priceextraFrieghtVal;
	private String priceextraExtraWarnVal;
	private String priceextraSupervisionCostVal;
	private String priceextratypeTestChargesVal;
	private ArrayList deliveryDetailsList = new ArrayList();//To Store Delivery Details On Indent Page Status is Won
	
	private String totalMotorPrice;
	private String totalPackagePrice;
	private String quotedPrice;
	
	


	
	
	
	/**
	 * @return Returns the altitude.
	 */
	public String getAltitude() {
		return CommonUtility.replaceNull(altitude);
	}
	/**
	 * @param altitude The altitude to set.
	 */
	public void setAltitude(String altitude) {
		this.altitude = altitude;
	}
	/**
	 * @return Returns the altitudeVar.
	 */
	public String getAltitudeVar() {
		return CommonUtility.replaceNull(altitudeVar);
	}
	/**
	 * @param altitudeVar The altitudeVar to set.
	 */
	public void setAltitudeVar(String altitudeVar) {
		this.altitudeVar = altitudeVar;
	}
	/**
	 * @return Returns the ambience.
	 */
	public String getAmbience() {
		return CommonUtility.replaceNull(ambience);
	}
	/**
	 * @param ambience The ambience to set.
	 */
	public void setAmbience(String ambience) {
		this.ambience = ambience;
	}
	/**
	 * @return Returns the ambienceVar.
	 */
	public String getAmbienceVar() {
		return CommonUtility.replaceNull(ambienceVar);
	}
	/**
	 * @param ambienceVar The ambienceVar to set.
	 */
	public void setAmbienceVar(String ambienceVar) {
		this.ambienceVar = ambienceVar;
	}
	/**
	 * @return Returns the applicationId.
	 */
	public int getApplicationId() {
		return applicationId;
	}
	/**
	 * @param applicationId The applicationId to set.
	 */
	public void setApplicationId(int applicationId) {
		this.applicationId = applicationId;
	}
	/**
	 * @return Returns the applicationName.
	 */
	public String getApplicationName() {
		return CommonUtility.replaceNull(applicationName);
	}
	/**
	 * @param applicationName The applicationName to set.
	 */
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
	/**
	 * @return Returns the coupling.
	 */
	public String getCoupling() {
		return CommonUtility.replaceNull(coupling);
	}
	/**
	 * @param coupling The coupling to set.
	 */
	public void setCoupling(String coupling) {
		this.coupling = coupling;
	}
	/**
	 * @return Returns the degreeOfProId.
	 */
	public int getDegreeOfProId() {
		return degreeOfProId;
	}
	/**
	 * @param degreeOfProId The degreeOfProId to set.
	 */
	public void setDegreeOfProId(int degreeOfProId) {
		this.degreeOfProId = degreeOfProId;
	}
	/**
	 * @return Returns the degreeOfProName.
	 */
	public String getDegreeOfProName() {
		return CommonUtility.replaceNull(degreeOfProName);
	}
	/**
	 * @param degreeOfProName The degreeOfProName to set.
	 */
	public void setDegreeOfProName(String degreeOfProName) {
		this.degreeOfProName = degreeOfProName;
	}
	/**
	 * @return Returns the details.
	 */
	public String getDetails() {
		return CommonUtility.replaceNull(details);
	}
	/**
	 * @param details The details to set.
	 */
	public void setDetails(String details) {
		this.details = details;
	}
	/**
	 * @return Returns the directionOfRotId.
	 */
	public int getDirectionOfRotId() {
		return directionOfRotId;
	}
	/**
	 * @param directionOfRotId The directionOfRotId to set.
	 */
	public void setDirectionOfRotId(int directionOfRotId) {
		this.directionOfRotId = directionOfRotId;
	}
	/**
	 * @return Returns the directionOfRotName.
	 */
	public String getDirectionOfRotName() {
		return CommonUtility.replaceNull(directionOfRotName);
	}
	/**
	 * @param directionOfRotName The directionOfRotName to set.
	 */
	public void setDirectionOfRotName(String directionOfRotName) {
		this.directionOfRotName = directionOfRotName;
	}
	/**
	 * @return Returns the dutyId.
	 */
	public int getDutyId() {
		return dutyId;
	}
	/**
	 * @param dutyId The dutyId to set.
	 */
	public void setDutyId(int dutyId) {
		this.dutyId = dutyId;
	}
	/**
	 * @return Returns the dutyName.
	 */
	public String getDutyName() {
		return CommonUtility.replaceNull(dutyName);
	}
	/**
	 * @param dutyName The dutyName to set.
	 */
	public void setDutyName(String dutyName) {
		this.dutyName = dutyName;
	}
	/**
	 * @return Returns the enclosureId.
	 */
	public int getEnclosureId() {
		return enclosureId;
	}
	/**
	 * @param enclosureId The enclosureId to set.
	 */
	public void setEnclosureId(int enclosureId) {
		this.enclosureId = enclosureId;
	}
	/**
	 * @return Returns the enclosureName.
	 */
	public String getEnclosureName() {
		return CommonUtility.replaceNull(enclosureName);
	}
	/**
	 * @param enclosureName The enclosureName to set.
	 */
	public void setEnclosureName(String enclosureName) {
		this.enclosureName = enclosureName;
	}
	/**
	 * @return Returns the enquiryId.
	 */
	public int getEnquiryId() {
		return enquiryId;
	}
	/**
	 * @param enquiryId The enquiryId to set.
	 */
	public void setEnquiryId(int enquiryId) {
		this.enquiryId = enquiryId;
	}
	/**
	 * @return Returns the frequency.
	 */
	public String getFrequency() {
		return CommonUtility.replaceNull(frequency);
	}
	/**
	 * @param frequency The frequency to set.
	 */
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	/**
	 * @return Returns the frequencyVar.
	 */
	public String getFrequencyVar() {
		return CommonUtility.replaceNull(frequencyVar);
	}
	/**
	 * @param frequencyVar The frequencyVar to set.
	 */
	public void setFrequencyVar(String frequencyVar) {
		this.frequencyVar = frequencyVar;
	}
	/**
	 * @return Returns the htltId.
	 */
	public int getHtltId() {
		return htltId;
	}
	/**
	 * @param htltId The htltId to set.
	 */
	public void setHtltId(int htltId) {
		this.htltId = htltId;
	}
	/**
	 * @return Returns the htltName.
	 */
	public String getHtltName() {
		return CommonUtility.replaceNull(htltName);
	}
	/**
	 * @param htltName The htltName to set.
	 */
	public void setHtltName(String htltName) {
		this.htltName = htltName;
	}
	/**
	 * @return Returns the insulationId.
	 */
	public int getInsulationId() {
		return insulationId;
	}
	/**
	 * @param insulationId The insulationId to set.
	 */
	public void setInsulationId(int insulationId) {
		this.insulationId = insulationId;
	}
	/**
	 * @return Returns the insulationName.
	 */
	public String getInsulationName() {
		return CommonUtility.replaceNull(insulationName);
	}
	/**
	 * @param insulationName The insulationName to set.
	 */
	public void setInsulationName(String insulationName) {
		this.insulationName = insulationName;
	}
	/**
	 * @return Returns the isValidated.
	 */
	public String getIsValidated() {
		return isValidated;
	}
	/**
	 * @param isValidated The isValidated to set.
	 */
	public void setIsValidated(String isValidated) {
		this.isValidated = isValidated;
	}
	/**
	 * @return Returns the kw.
	 */
	public String getKw() {
		return CommonUtility.replaceNull(kw);
	}
	/**
	 * @param kw The kw to set.
	 */
	public void setKw(String kw) {
		this.kw = kw;
	}
	/**
	 * @return Returns the methodOfStg.
	 */
	public String getMethodOfStg() {
		return CommonUtility.replaceNull(methodOfStg);
	}
	/**
	 * @param methodOfStg The methodOfStg to set.
	 */
	public void setMethodOfStg(String methodOfStg) {
		this.methodOfStg = methodOfStg;
	}
	/**
	 * @return Returns the mountingId.
	 */
	public int getMountingId() {
		return mountingId;
	}
	/**
	 * @param mountingId The mountingId to set.
	 */
	public void setMountingId(int mountingId) {
		this.mountingId = mountingId;
	}
	/**
	 * @return Returns the mountingName.
	 */
	public String getMountingName() {
		return CommonUtility.replaceNull(mountingName);
	}
	/**
	 * @param mountingName The mountingName to set.
	 */
	public void setMountingName(String mountingName) {
		this.mountingName = mountingName;
	}
	/**
	 * @return Returns the paint.
	 */
	public String getPaint() {
		return CommonUtility.replaceNull(paint);
	}
	/**
	 * @param paint The paint to set.
	 */
	public void setPaint(String paint) {
		this.paint = paint;
	}
	/**
	 * @return Returns the poleId.
	 */
	public int getPoleId() {
		return poleId;
	}
	/**
	 * @param poleId The poleId to set.
	 */
	public void setPoleId(int poleId) {
		this.poleId = poleId;
	}
	/**
	 * @return Returns the poleName.
	 */
	public String getPoleName() {
		return CommonUtility.replaceNull(poleName);
	}
	/**
	 * @param poleName The poleName to set.
	 */
	public void setPoleName(String poleName) {
		this.poleName = poleName;
	}
	/**
	 * @return Returns the quantity.
	 */
	public int getQuantity() {
		return quantity;
	}
	/**
	 * @param quantity The quantity to set.
	 */
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	/**
	 * @return Returns the ratingId.
	 */
	public int getRatingId() {
		return ratingId;
	}
	/**
	 * @param ratingId The ratingId to set.
	 */
	public void setRatingId(int ratingId) {
		this.ratingId = ratingId;
	}
	/**
	 * @return Returns the ratingNo.
	 */
	public int getRatingNo() {
		return ratingNo;
	}
	/**
	 * @param ratingNo The ratingNo to set.
	 */
	public void setRatingNo(int ratingNo) {
		this.ratingNo = ratingNo;
	}
	/**
	 * @return Returns the scsrId.
	 */
	public int getScsrId() {
		return scsrId;
	}
	/**
	 * @param scsrId The scsrId to set.
	 */
	public void setScsrId(int scsrId) {
		this.scsrId = scsrId;
	}
	/**
	 * @return Returns the scsrName.
	 */
	public String getScsrName() {
		return CommonUtility.replaceNull(scsrName);
	}
	/**
	 * @param scsrName The scsrName to set.
	 */
	public void setScsrName(String scsrName) {
		this.scsrName = scsrName;
	}
	/**
	 * @return Returns the shaftExtId.
	 */
	public int getShaftExtId() {
		return shaftExtId;
	}
	/**
	 * @param shaftExtId The shaftExtId to set.
	 */
	public void setShaftExtId(int shaftExtId) {
		this.shaftExtId = shaftExtId;
	}
	/**
	 * @return Returns the shaftExtName.
	 */
	public String getShaftExtName() {
		return CommonUtility.replaceNull(shaftExtName);
	}
	/**
	 * @param shaftExtName The shaftExtName to set.
	 */
	public void setShaftExtName(String shaftExtName) {
		this.shaftExtName = shaftExtName;
	}
	/**
	 * @return Returns the splFeatures.
	 */
	public String getSplFeatures() {
		return CommonUtility.replaceNull(splFeatures);
	}
	/**
	 * @param splFeatures The splFeatures to set.
	 */
	public void setSplFeatures(String splFeatures) {
		this.splFeatures = splFeatures;
	}
	/**
	 * @return Returns the tempRaise.
	 */
	public String getTempRaise() {
		return tempRaise;
	}
	/**
	 * @param tempRaise The tempRaise to set.
	 */
	public void setTempRaise(String tempRaise) {
		this.tempRaise = tempRaise;
	}
	/**
	 * @return Returns the tempRaiseVar.
	 */
	public String getTempRaiseVar() {
		return CommonUtility.replaceNull(tempRaiseVar);
	}
	/**
	 * @param tempRaiseVar The tempRaiseVar to set.
	 */
	public void setTempRaiseVar(String tempRaiseVar) {
		this.tempRaiseVar = tempRaiseVar;
	}
	/**
	 * @return Returns the termBoxId.
	 */
	public int getTermBoxId() {
		return termBoxId;
	}
	/**
	 * @param termBoxId The termBoxId to set.
	 */
	public void setTermBoxId(int termBoxId) {
		this.termBoxId = termBoxId;
	}
	/**
	 * @return Returns the termBoxName.
	 */
	public String getTermBoxName() {
		return CommonUtility.replaceNull(termBoxName);
	}
	/**
	 * @param termBoxName The termBoxName to set.
	 */
	public void setTermBoxName(String termBoxName) {
		this.termBoxName = termBoxName;
	}
	/**
	 * @return Returns the volts.
	 */
	public String getVolts() {
		return CommonUtility.replaceNull(volts);
	}
	/**
	 * @param volts The volts to set.
	 */
	public void setVolts(String volts) {
		this.volts = volts;
	}
	/**
	 * @return Returns the voltsVar.
	 */
	public String getVoltsVar() {
		return CommonUtility.replaceNull(voltsVar);
	}
	/**
	 * @param voltsVar The voltsVar to set.
	 */
	public void setVoltsVar(String voltsVar) {
		this.voltsVar = voltsVar;
	}
	/**
	 * @return Returns the ratingCreatedBy.
	 */
	public String getRatingCreatedBy() {
		return CommonUtility.replaceNull(ratingCreatedBy);
	}
	/**
	 * @param ratingCreatedBy The ratingCreatedBy to set.
	 */
	public void setRatingCreatedBy(String ratingCreatedBy) {
		this.ratingCreatedBy = ratingCreatedBy;
	}
	/**
	 * @return Returns the ratingCreatedBySSO.
	 */
	public String getRatingCreatedBySSO() {
		return CommonUtility.replaceNull(ratingCreatedBySSO);
	}
	/**
	 * @param ratingCreatedBySSO The ratingCreatedBySSO to set.
	 */
	public void setRatingCreatedBySSO(String ratingCreatedBySSO) {
		this.ratingCreatedBySSO = ratingCreatedBySSO;
	}
	/**
	 * @return Returns the ratingTitle.
	 */
	public String getRatingTitle() {
		return CommonUtility.replaceNull(ratingTitle);
	}
	/**
	 * @param ratingTitle The ratingTitle to set.
	 */
	public void setRatingTitle(String ratingTitle) {
		this.ratingTitle = ratingTitle;
	}
	/**
	 * @return Returns the technicalDto.
	 */
	public TechnicalOfferDto getTechnicalDto() {
		return technicalDto;
	}
	/**
	 * @param technicalDto The technicalDto to set.
	 */
	public void setTechnicalDto(TechnicalOfferDto technicalDto) {
		this.technicalDto = technicalDto;
	}
	/**
	 * @return Returns the addOnList.
	 */
	public ArrayList getAddOnList() {
		return addOnList;
	}
	/**
	 * @param addOnList The addOnList to set.
	 */
	public void setAddOnList(ArrayList addOnList) {
		this.addOnList = addOnList;
	}
	public String getRatingUnitPrice() {
		return CommonUtility.replaceNull(ratingUnitPrice);
	}
	public void setRatingUnitPrice(String ratingUnitPrice) {
		this.ratingUnitPrice = ratingUnitPrice;
	}
	public ArrayList getAlAreaClassificationList() {
		return alAreaClassificationList;
	}
	public void setAlAreaClassificationList(ArrayList alAreaClassificationList) {
		this.alAreaClassificationList = alAreaClassificationList;
	}
	public ArrayList getAlServiceFactorList() {
		return alServiceFactorList;
	}
	public void setAlServiceFactorList(ArrayList alServiceFactorList) {
		this.alServiceFactorList = alServiceFactorList;
	}
	/*public ArrayList getAlFrequencyList() {
		return alFrequencyList;
	}
	public void setAlFrequencyList(ArrayList alFrequencyList) {
		this.alFrequencyList = alFrequencyList;
	}*/
	public ArrayList getAlStrtgCurrentList() {
		return alStrtgCurrentList;
	}
	public void setAlStrtgCurrentList(ArrayList alStrtgCurrentList) {
		this.alStrtgCurrentList = alStrtgCurrentList;
	}
	/*public ArrayList getAlAmbienceList() {
		return alAmbienceList;
	}
	public void setAlAmbienceList(ArrayList alAmbienceList) {
		this.alAmbienceList = alAmbienceList;
	}*/
	public ArrayList getAlClgSystemList() {
		return alClgSystemList;
	}
	public void setAlClgSystemList(ArrayList alClgSystemList) {
		this.alClgSystemList = alClgSystemList;
	}
	public ArrayList getAlBearingDENDEList() {
		return alBearingDENDEList;
	}
	public void setAlBearingDENDEList(ArrayList alBearingDENDEList) {
		this.alBearingDENDEList = alBearingDENDEList;
	}
	public ArrayList getAlLubricationList() {
		return alLubricationList;
	}
	public void setAlLubricationList(ArrayList alLubricationList) {
		this.alLubricationList = alLubricationList;
	}
	public ArrayList getAlBalancingList() {
		return alBalancingList;
	}
	public void setAlBalancingList(ArrayList alBalancingList) {
		this.alBalancingList = alBalancingList;
	}
	public ArrayList getAlVibrationList() {
		return alVibrationList;
	}
	public void setAlVibrationList(ArrayList alVibrationList) {
		this.alVibrationList = alVibrationList;
	}
	public ArrayList getAlMainTermBoxPSList() {
		return alMainTermBoxPSList;
	}
	public void setAlMainTermBoxPSList(ArrayList alMainTermBoxPSList) {
		this.alMainTermBoxPSList = alMainTermBoxPSList;
	}
	public ArrayList getAlNeutralTBList() {
		return alNeutralTBList;
	}
	public void setAlNeutralTBList(ArrayList alNeutralTBList) {
		this.alNeutralTBList = alNeutralTBList;
	}
	public ArrayList getAlCableEntryList() {
		return alCableEntryList;
	}
	public void setAlCableEntryList(ArrayList alCableEntryList) {
		this.alCableEntryList = alCableEntryList;
	}
	public ArrayList getAlAuxTermBoxList() {
		return alAuxTermBoxList;
	}
	public void setAlAuxTermBoxList(ArrayList alAuxTermBoxList) {
		this.alAuxTermBoxList = alAuxTermBoxList;
	}
	public ArrayList getAlStatorConnList() {
		return alStatorConnList;
	}
	public void setAlStatorConnList(ArrayList alStatorConnList) {
		this.alStatorConnList = alStatorConnList;
	}
	public ArrayList getAlBoTerminalsNoList() {
		return alBoTerminalsNoList;
	}
	public void setAlBoTerminalsNoList(ArrayList alBoTerminalsNoList) {
		this.alBoTerminalsNoList = alBoTerminalsNoList;
	}
	public ArrayList getAlRotationList() {
		return alRotationList;
	}
	public void setAlRotationList(ArrayList alRotationList) {
		this.alRotationList = alRotationList;
	}
	public ArrayList getAlSpaceHeaterList() {
		return alSpaceHeaterList;
	}
	public void setAlSpaceHeaterList(ArrayList alSpaceHeaterList) {
		this.alSpaceHeaterList = alSpaceHeaterList;
	}
	public ArrayList getAlWindingRTDList() {
		return alWindingRTDList;
	}
	public void setAlWindingRTDList(ArrayList alWindingRTDList) {
		this.alWindingRTDList = alWindingRTDList;
	}
	public ArrayList getAlBearingRTDList() {
		return alBearingRTDList;
	}
	public void setAlBearingRTDList(ArrayList alBearingRTDList) {
		this.alBearingRTDList = alBearingRTDList;
	}
	public ArrayList getAlMinStartVoltList() {
		return alMinStartVoltList;
	}
	public void setAlMinStartVoltList(ArrayList alMinStartVoltList) {
		this.alMinStartVoltList = alMinStartVoltList;
	}
	public ArrayList getAlMethodOfStgList() {
		return alMethodOfStgList;
	}
	public void setAlMethodOfStgList(ArrayList alMethodOfStgList) {
		this.alMethodOfStgList = alMethodOfStgList;
	}
	public ArrayList getAlBearingThermoDtList() {
		return alBearingThermoDtList;
	}
	public void setAlBearingThermoDtList(ArrayList alBearingThermoDtList) {
		this.alBearingThermoDtList = alBearingThermoDtList;
	}
	public ArrayList getAlYesNoList() {
		return alYesNoList;
	}
	public void setAlYesNoList(ArrayList alYesNoList) {
		this.alYesNoList = alYesNoList;
	}
	/*public ArrayList getAlTempRaiseList() {
		return alTempRaiseList;
	}
	public void setAlTempRaiseList(ArrayList alTempRaiseList) {
		this.alTempRaiseList = alTempRaiseList;
	}*/
	public String getRatedOutputUnit() {
		return CommonUtility.replaceNull(ratedOutputUnit);
	}
	public void setRatedOutputUnit(String ratedOutputUnit) {
		this.ratedOutputUnit = ratedOutputUnit;
	}
	public String getTempRaiseDesc() {
		return CommonUtility.replaceNull(tempRaiseDesc);
	}
	public void setTempRaiseDesc(String tempRaiseDesc) {
		this.tempRaiseDesc = tempRaiseDesc;
	}
	public ArrayList getAlZoneList() {
		return alZoneList;
	}
	public void setAlZoneList(ArrayList alZoneList) {
		this.alZoneList = alZoneList;
	}
	public ArrayList getAlGasGroupList() {
		return alGasGroupList;
	}
	public void setAlGasGroupList(ArrayList alGasGroupList) {
		this.alGasGroupList = alGasGroupList;
	}
	public String getMinStartVolt() {
		return CommonUtility.replaceNull(minStartVolt);
	}
	public void setMinStartVolt(String minStartVolt) {
		this.minStartVolt = minStartVolt;
	}
	
	public String getPriceincludeFrieghtVal() {
		return CommonUtility.replaceNull(priceincludeFrieghtVal);
	}
	public void setPriceincludeFrieghtVal(String priceincludeFrieghtVal) {
		this.priceincludeFrieghtVal = priceincludeFrieghtVal;
	}
	public String getPriceincludeExtraWarnVal() {
		return CommonUtility.replaceNull(priceincludeExtraWarnVal);
	}
	public void setPriceincludeExtraWarnVal(String priceincludeExtraWarnVal) {
		this.priceincludeExtraWarnVal = priceincludeExtraWarnVal;
	}
	public String getPriceincludeSupervisionCostVal() {
		return CommonUtility.replaceNull(priceincludeSupervisionCostVal);
	}
	public void setPriceincludeSupervisionCostVal(
			String priceincludeSupervisionCostVal) {
		this.priceincludeSupervisionCostVal = priceincludeSupervisionCostVal;
	}
	public String getPriceincludetypeTestChargesVal() {
		return CommonUtility.replaceNull(priceincludetypeTestChargesVal);
	}
	public void setPriceincludetypeTestChargesVal(
			String priceincludetypeTestChargesVal) {
		this.priceincludetypeTestChargesVal = priceincludetypeTestChargesVal;
	}
	public String getPriceextraFrieghtVal() {
		return CommonUtility.replaceNull(priceextraFrieghtVal);
	}
	public void setPriceextraFrieghtVal(String priceextraFrieghtVal) {
		this.priceextraFrieghtVal = priceextraFrieghtVal;
	}
	public String getPriceextraExtraWarnVal() {
		return CommonUtility.replaceNull(priceextraExtraWarnVal);
	}
	public void setPriceextraExtraWarnVal(String priceextraExtraWarnVal) {
		this.priceextraExtraWarnVal = priceextraExtraWarnVal;
	}
	public String getPriceextraSupervisionCostVal() {
		return CommonUtility.replaceNull(priceextraSupervisionCostVal);
	}
	public void setPriceextraSupervisionCostVal(String priceextraSupervisionCostVal) {
		this.priceextraSupervisionCostVal = priceextraSupervisionCostVal;
	}
	public String getPriceextratypeTestChargesVal() {
		return CommonUtility.replaceNull(priceextratypeTestChargesVal);
	}
	public void setPriceextratypeTestChargesVal(String priceextratypeTestChargesVal) {
		this.priceextratypeTestChargesVal = priceextratypeTestChargesVal;
	}
	public String getUnitPriceMultiplierRsm() {
		return CommonUtility.replaceNull(unitPriceMultiplierRsm);
	}
	public void setUnitPriceMultiplierRsm(String unitPriceMultiplierRsm) {
		this.unitPriceMultiplierRsm = unitPriceMultiplierRsm;
	}
	public ArrayList getDeliveryDetailsList() {
		return deliveryDetailsList;
	}
	public void setDeliveryDetailsList(ArrayList deliveryDetailsList) {
		this.deliveryDetailsList = deliveryDetailsList;
	}
	public String getPriceincludeFrieght() {
		return priceincludeFrieght;
	}
	public void setPriceincludeFrieght(String priceincludeFrieght) {
		this.priceincludeFrieght = priceincludeFrieght;
	}
	public String getPriceincludeExtraWarn() {
		return priceincludeExtraWarn;
	}
	public void setPriceincludeExtraWarn(String priceincludeExtraWarn) {
		this.priceincludeExtraWarn = priceincludeExtraWarn;
	}
	public String getPriceincludeSupervisionCost() {
		return priceincludeSupervisionCost;
	}
	public void setPriceincludeSupervisionCost(String priceincludeSupervisionCost) {
		this.priceincludeSupervisionCost = priceincludeSupervisionCost;
	}
	public String getPriceincludetypeTestCharges() {
		return priceincludetypeTestCharges;
	}
	public void setPriceincludetypeTestCharges(String priceincludetypeTestCharges) {
		this.priceincludetypeTestCharges = priceincludetypeTestCharges;
	}
	public String getPriceextraFrieght() {
		return priceextraFrieght;
	}
	public void setPriceextraFrieght(String priceextraFrieght) {
		this.priceextraFrieght = priceextraFrieght;
	}
	public String getPriceextraExtraWarn() {
		return priceextraExtraWarn;
	}
	public void setPriceextraExtraWarn(String priceextraExtraWarn) {
		this.priceextraExtraWarn = priceextraExtraWarn;
	}
	public String getPriceextraSupervisionCost() {
		return priceextraSupervisionCost;
	}
	public void setPriceextraSupervisionCost(String priceextraSupervisionCost) {
		this.priceextraSupervisionCost = priceextraSupervisionCost;
	}
	public String getPriceextratypeTestCharges() {
		return priceextratypeTestCharges;
	}
	public void setPriceextratypeTestCharges(String priceextratypeTestCharges) {
		this.priceextratypeTestCharges = priceextratypeTestCharges;
	}
	
	public String getTotalMotorPrice() {
		return CommonUtility.replaceNull(totalMotorPrice);
	}
	public void setTotalMotorPrice(String totalMotorPrice) {
		this.totalMotorPrice = totalMotorPrice;
	}
	public String getTotalPackagePrice() {
		return CommonUtility.replaceNull(totalPackagePrice);
	}
	public void setTotalPackagePrice(String totalPackagePrice) {
		this.totalPackagePrice = totalPackagePrice;
	}
	public String getQuotedPrice() {
		return CommonUtility.replaceNull(quotedPrice);
	}
	public void setQuotedPrice(String quotedPrice) {
		this.quotedPrice = quotedPrice;
	}
	

	
	
	
	
	
}
	
	