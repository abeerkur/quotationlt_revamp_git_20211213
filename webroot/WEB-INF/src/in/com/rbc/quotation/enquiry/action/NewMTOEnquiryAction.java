package in.com.rbc.quotation.enquiry.action;

import java.io.PrintWriter;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import in.com.rbc.quotation.admin.dao.ListPriseDao;
import in.com.rbc.quotation.admin.dto.ListPriceDto;
import in.com.rbc.quotation.base.BaseAction;
import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.utility.AttachmentUtility;
import in.com.rbc.quotation.common.utility.CommonUtility;
import in.com.rbc.quotation.common.utility.DBUtility;
import in.com.rbc.quotation.common.utility.ExceptionUtility;
import in.com.rbc.quotation.common.utility.LoggerUtility;
import in.com.rbc.quotation.enquiry.dao.MTOQuotationDao;
import in.com.rbc.quotation.enquiry.dao.QuotationDao;
import in.com.rbc.quotation.enquiry.dto.NewEnquiryDto;
import in.com.rbc.quotation.enquiry.dto.NewMTORatingDto;
import in.com.rbc.quotation.enquiry.dto.NewRatingDto;
import in.com.rbc.quotation.enquiry.dto.UserDetailsDto;
import in.com.rbc.quotation.enquiry.form.NewMTOEnquiryForm;
import in.com.rbc.quotation.enquiry.utility.NewEnquiryUtilityManager;
import in.com.rbc.quotation.factory.DaoFactory;
import in.com.rbc.quotation.user.action.UserAction;
import in.com.rbc.quotation.user.dao.UserDao;
import in.com.rbc.quotation.user.dto.UserDto;
import in.com.rbc.quotation.workflow.dto.WorkflowDto;
import in.com.rbc.quotation.workflow.utility.WorkflowUtility;

public class NewMTOEnquiryAction extends BaseAction {

	/**
     * Method for Fetching LT Quotation MTO Enquiry Details.
     * @param actionMapping	ActionMapping object to redirect the user to next page depending on the activity
     * @param actionForm	ActionForm object to handle the form elements
     * @param request		HttpServletRequest object to handle request operations
     * @param response		HttpServletResponse object to handle response operations
     * @return returns 		ActionForward depending on which user is redirected to next page
     * @throws Exception	if any error occurs while performing database operations, etc
     */
	public ActionForward viewMTOEnquiryDetails(ActionMapping actionMapping,
            ActionForm actionForm, 
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
		
		String sMethodName = "viewMTOEnquiryDetails"; 
    	String sClassName = this.getClass().getName();
    	LoggerUtility.log("INFO", sClassName, sMethodName, "START");
    	
    	Connection conn = null;  		// Connection object to store the database connection
        DBUtility oDBUtility = null;  	// Object of DBUtility class to handle DB connection objects
        
        NewMTOEnquiryForm oNewMTOEnquiryForm = null;
        NewEnquiryDto oNewEnquiryDto = null;
        UserDto oUserDto = null;
        UserAction oUserAction = null; //Instantiating the UserAction object
        DaoFactory oDaoFactory = null; //Creating an instance of of DaoFactory & retrieving UserDao object
        QuotationDao oQuotationDao = null;
        MTOQuotationDao oMTOQuotationDao = null;
        
        String sOperationType = null, sOperationType2 = null, sDeptAndAuth = null;
        String[] saSections = null;
        
    	try {
    		/* Setting user's information in request, using the header information received through request */
            setRolesToRequest(request);
            LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "Roles set to request.");
            
            oNewMTOEnquiryForm = (NewMTOEnquiryForm) actionForm;
    		if (oNewMTOEnquiryForm == null)
    			oNewMTOEnquiryForm = new NewMTOEnquiryForm();
    		
    		sOperationType = oNewMTOEnquiryForm.getOperationType();
    		
    		/* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Instantiating the UserAction object */
            oUserAction = new UserAction();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            
            /* Retrieving the logged-in user information */
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            
            if (StringUtils.isBlank(sOperationType) || sOperationType.trim().length() == QuotationConstants.QUOTATION_LITERAL_ZERO) {
    			sOperationType = QuotationConstants.QUOTATION_VIEWOFFDATA;    			
    			saveToken(request);    			
    		} else {
				// for other cases validate token
				if (!isTokenValid(request)) {
					/* Setting error details to request to display the same in Exception page */
					request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_INVALIDTOKENMESSAGE);
					/* Forwarding request to Error page */
					return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
				}
    		}
            
            oNewEnquiryDto = new NewEnquiryDto();
            NewEnquiryUtilityManager oNewEnquiryManager = new NewEnquiryUtilityManager();
            oQuotationDao = oDaoFactory.getQuotationDao();
            oMTOQuotationDao = oDaoFactory.getMTOQuotationDao();
            
            oNewMTOEnquiryForm.setEnquiryId(CommonUtility.getIntParameter(request, QuotationConstants.QUOTATION_REQUEST_ENQUIRYID));	
            
			if (oNewMTOEnquiryForm.getEnquiryId() < QuotationConstants.QUOTATION_LITERAL_ONE) {
            	 /* Setting error details to request to display the same in Exception page */
                request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_CRET_ROWS); 
                /* Forwarding request to Error page */
                return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);            	
            }
			
            sOperationType = QuotationConstants.QUOTATION_VIEWOFFDATA;                
            sOperationType2 = QuotationConstants.QUOTATION_VIEWOFFDATA2;
            
            if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_VIEWOFFDATA)) {
            	
            	PropertyUtils.copyProperties(oNewEnquiryDto, oNewMTOEnquiryForm);
            	sDeptAndAuth = QuotationConstants.QUOTATION_TECHNICAL_DEPT;
            	// Get the Attachment Files
    			request.setAttribute(QuotationConstants.QUOTATION_ATTACHLIST, AttachmentUtility.getNewEnquiryAttachmentDetails(oNewEnquiryDto));
    			
    			if(sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_VIEWOFFDATA)) {
					oNewEnquiryDto = oMTOQuotationDao.getMTOEnquiryDetailsForView(conn, oNewEnquiryDto, QuotationConstants.QUOTATION_EDIT.toUpperCase()); 
				}
    			
    			// Fetch the MTO Ratings List. Set "mtoTotalRatingIndex".
    			ArrayList<NewMTORatingDto> alMTORatingsList = oMTOQuotationDao.fetchNewMTORatingDetails(conn, oNewEnquiryDto);
    			oNewMTOEnquiryForm.setMtoTotalRatingIndex(alMTORatingsList.size());
    			
    			// Check isDesignComplete - For QA, Sourcing, Mfg.Plant
    			if(oNewEnquiryDto.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGQA 
    					|| oNewEnquiryDto.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGSCM ) {
    				oNewMTOEnquiryForm.setIsDesignCompleteCheck("Y");
    				for(NewMTORatingDto oTempNewMTORatingDto : alMTORatingsList) {
    					if(!QuotationConstants.QUOTATION_YES.equalsIgnoreCase(oTempNewMTORatingDto.getMtoDesignComplete())) {
    						oNewMTOEnquiryForm.setIsDesignCompleteCheck("N");
    						break;
    					}
    				}
    			}
    			
    			ArrayList<NewMTORatingDto> alMTOCompleteList = new ArrayList<>();
    			// Fetch MTO AddOn and Technical Details.
    			for(NewMTORatingDto oMTORatingDto : alMTORatingsList) {
    				oMTORatingDto = oMTOQuotationDao.fetchNewMTOTechAddOnDetails(conn, oMTORatingDto);
    				alMTOCompleteList.add(oMTORatingDto);
    			}
    			oNewEnquiryDto.setNewMTORatingsList(alMTOCompleteList);
    			Map<String,String> hmCompleteAddOnsMap = oMTOQuotationDao.fetchMTOAddOnMap(conn, oNewEnquiryDto);
    			String jsonMTOAddOns = oNewEnquiryManager.convertListToJSON(hmCompleteAddOnsMap);
    			//System.out.println("In Action class :: jsonMTOAddOns ..... " + jsonMTOAddOns);
    			request.setAttribute("jsonMTOAddOns", jsonMTOAddOns);
    			
    			oNewEnquiryDto = oQuotationDao.loadNewEnquiryLookUpValues(conn, oNewEnquiryDto);
    			PropertyUtils.copyProperties(oNewMTOEnquiryForm, oNewEnquiryDto);
				oNewMTOEnquiryForm = oNewEnquiryManager.loadOfferPageLookUpValues(conn, oNewMTOEnquiryForm);
            	
				// Set NewRatings List size to totalRatingIndex.
				oNewMTOEnquiryForm.setTotalRatingIndex(oNewEnquiryDto.getNewRatingsList().size());
				// Populate Re-assign Users List.
				oNewMTOEnquiryForm.setMtoReassignUsersList(oMTOQuotationDao.fetchMTOReassignUsersList(conn));
				
            }
    	} catch(Exception e) {
        	//e.printStackTrace();
            /* Logging any exception that raised during this activity in log files using Log4j */
            LoggerUtility.log("DEBUG", this.getClass().getName(), sMethodName, ExceptionUtility.getStackTraceAsString(e));
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        } finally {
            /* Releasing/closing the connection object */
            if ( conn!=null )
                oDBUtility.releaseResources(conn);
        }
    	
    	LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        
    	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_MTOENQUIRY_VIEW);
	}
	
	/**
	 * Method to Re-assign the MTO Enquiry to Appropriate User as per the User Type(DE/DM, QA, Sourcing, Mfg. Plant).
	 * @param actionMapping	ActionMapping object to redirect the user to next page depending on the activity
     * @param actionForm	ActionForm object to handle the form elements
     * @param request		HttpServletRequest object to handle request operations
     * @param response		HttpServletResponse object to handle response operations
     * @return returns 		ActionForward depending on which user is redirected to next page
     * @throws Exception	if any error occurs while performing database operations, etc
	 */
	public ActionForward reassignMTOEnquiry(ActionMapping actionMapping,
            ActionForm actionForm, 
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
		
		String sMethodName = "reassignMTOEnquiry"; 
    	String sClassName = this.getClass().getName();
    	LoggerUtility.log("INFO", sClassName, sMethodName, "START");
    	
    	Connection conn = null;  			// Connection object to store the database connection
        DBUtility oDBUtility = null;  		// Object of DBUtility class to handle DB connection objects
        
        DaoFactory oDaoFactory = null; 		// Creating an instance of of DaoFactory & retrieving UserDao object
        QuotationDao oQuotationDao = null;	// Creating an Instance for QuotationDao
        MTOQuotationDao oMTOQuotationDao = null;	// Creating an Instance for MTOQuotationDao
        UserDao oUserDao = null;			// Creating an Instance for UserDao
        UserAction oUserAction = null; 		//Instantiating the UserAction object
        NewMTOEnquiryForm oNewMTOEnquiryForm = null;
        NewEnquiryDto oNewEnquiryDto = null;
        UserDto oUserDto = null;
        UserDto oReassignUserDto = null;
        UserDetailsDto oUserDetailsDto = null;
        WorkflowUtility oWorkflowUtility = null;
		WorkflowDto oWorkflowDto = null;
		int iWorkflowExecuteStatus=0;
		String sReassignToId = "", sResult=null;;
		String sMsg=null,sLinkMsg1=null,sLinkMsg2=null,sLinkMsg=null;
		
        try {
        	/* Setting user's information in request, using the header information received through request */
            setRolesToRequest(request);
            LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "Roles set to request.");
            
            oNewMTOEnquiryForm = (NewMTOEnquiryForm) actionForm;
    		if (oNewMTOEnquiryForm == null)
    			oNewMTOEnquiryForm = new NewMTOEnquiryForm();
    		
    		/* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
            /* Create QuotationDao */
            oQuotationDao = oDaoFactory.getQuotationDao();
            /* Create MTOQuotationDao */
            oMTOQuotationDao = oDaoFactory.getMTOQuotationDao();
            /* Instantiating the UserAction object */
            oUserAction = new UserAction();
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            
            NewEnquiryUtilityManager oNewEnquiryManager = new NewEnquiryUtilityManager();
            oNewEnquiryDto = new NewEnquiryDto();
        	PropertyUtils.copyProperties(oNewEnquiryDto, oNewMTOEnquiryForm);
        	
        	sReassignToId = CommonUtility.getStringParameter(request, QuotationConstants.QUOTATIONLT_REASSIGNTO_ID);
        	
        	/* Retrieving the logged-in user information */
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            if (oUserDto == null){
            	oUserDto = new UserDto();
            }
            
            /* Retrieving User information - Based on Re-assign To User Id. */
        	oUserDao = oDaoFactory.getUserDao();
        	oReassignUserDto = oUserDao.getUserInfo(conn, sReassignToId);
            if (oReassignUserDto == null){
            	oReassignUserDto = new UserDto();
            }
            oUserDetailsDto = oQuotationDao.getUserDetailsById(conn, sReassignToId);
            
        	// #########################################################################################################################################
        	
        	 oDBUtility.setAutoCommitFalse(conn);
             
             // Save the Design Page Attributes.
             sResult = oMTOQuotationDao.updateDesignFieldsOnReturn(conn, oNewEnquiryDto);
             if (QuotationConstants.QUOTATION_STRING_Y.equals(sResult)) {
 				oDBUtility.commit(conn);
 			 } else {
 				oDBUtility.rollback(conn);
 			 }
             
             // Save Design(MTO) Section Details.
             int mtoTotalRatingIdx = oNewMTOEnquiryForm.getMtoTotalRatingIndex();
             // Save MTO Tech Ratings.
             // Save AddOn Details for MTO Tech Ratings.
             ArrayList<NewMTORatingDto> alMTOTechRatingsfromPage = oNewEnquiryManager.getMTOTechRatingsList(conn, oNewEnquiryDto, mtoTotalRatingIdx, request);
             ArrayList<NewMTORatingDto> alMTOTechCompleteDetailsfromPage = new ArrayList<>();
             for(NewMTORatingDto oMTOTechRatingDto : alMTOTechRatingsfromPage) {
             	oMTOTechRatingDto = oNewEnquiryManager.getMTOTechRatingsWithDesignAddOns(conn, oMTOTechRatingDto, mtoTotalRatingIdx, request);
             	alMTOTechCompleteDetailsfromPage.add(oMTOTechRatingDto);
             }
             sResult = oMTOQuotationDao.saveMTOTechCompleteDetails(conn, alMTOTechCompleteDetailsfromPage, oUserDto.getUserId());
             if (QuotationConstants.QUOTATION_STRING_Y.equals(sResult)) {
 				oDBUtility.commit(conn);
 			 } else {
 				oDBUtility.rollback(conn);
 			 }
             
             // Save the MTO Ratings values.
             ArrayList<NewRatingDto> alMTORatingsList = oNewEnquiryManager.getMTORatingsList(conn, oQuotationDao, oNewEnquiryDto, request);
             sResult = oMTOQuotationDao.saveMTORatingDetails(conn, alMTORatingsList);
             if (QuotationConstants.QUOTATION_STRING_Y.equals(sResult)) {
 				oDBUtility.commit(conn);
 			 } else {
 				oDBUtility.rollback(conn);
 			 }
             
             oDBUtility.setAutoCommitTrue(conn);
             
            // #########################################################################################################################################
        	
            
            // Save the Re-assign Comments for the Enquiry. (Add to Existing Comments.)
            oNewEnquiryDto = oMTOQuotationDao.getMTOEnquiryDetailsForView(conn, oNewEnquiryDto, QuotationConstants.QUOTATION_EDIT.toUpperCase());
            oNewEnquiryDto.setReassignRemarks(CommonUtility.getStringParameter(request, "reassignRemarks"));
            oMTOQuotationDao.saveReassignComments(conn, oNewEnquiryDto);
            
        	if(StringUtils.isNotBlank(sReassignToId)) {
        		
        		// Workflow :  Re-assign To Flow : Below Rules to be Executed.
        		// If Re-assign To User = DE/DM : Change the "QWF_ASSIGNTO" User-Id where QWF_WORKFLOWSTATUS = "P" for this Enquiry.
        		// If Re-assign To User = QA : Change Status to "Pending - Quality Approval".
        		// If Re-assign To User = SCM : Change Status to "Pending - Sourcing".
        		// If Re-assign To User = MFG : Change Status to "Pending - Mfg. Plant Details".
        		resetToken(request);
    		    LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Initiating Workflow process - - -");
    		    oWorkflowUtility = new WorkflowUtility();
                oWorkflowDto = new WorkflowDto();
                oWorkflowDto.setEnquiryId(oNewEnquiryDto.getEnquiryId());
                oWorkflowDto.setEnquiryNo(oNewEnquiryDto.getEnquiryNumber());
                oWorkflowDto.setLoginUserId(oUserDto.getUserId());
                oWorkflowDto.setLoginUserName(oUserDto.getFullName());
                oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
                
                // For Re-assign Workflow : Set "sReassignToId" as the WorkflowDto Assigned To value.
                oWorkflowDto.setAssignedTo(sReassignToId);
                
                if(QuotationConstants.QUOTATIONLT_REASSIGNTO_DE.equalsIgnoreCase(oUserDetailsDto.getUserDetailsType()) 
                		|| QuotationConstants.QUOTATIONLT_REASSIGNTO_DM.equalsIgnoreCase(oUserDetailsDto.getUserDetailsType()) ) {
                	oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTODESIGNOFFICE);
                } else if(QuotationConstants.QUOTATIONLT_REASSIGNTO_QA.equalsIgnoreCase(oUserDetailsDto.getUserDetailsType()) ) {
                	oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTOQA);
                } else if(QuotationConstants.QUOTATIONLT_REASSIGNTO_SCM.equalsIgnoreCase(oUserDetailsDto.getUserDetailsType()) ) {
                	oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTOSCM);
                } else if(QuotationConstants.QUOTATIONLT_REASSIGNTO_MFG.equalsIgnoreCase(oUserDetailsDto.getUserDetailsType()) ) {
                	oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTOMFG);
                }
                oWorkflowDto.setOldStatusId(oNewEnquiryDto.getStatusId()); 
                
                LoggerUtility.log("INFO", sClassName, sMethodName, "2 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
        		iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
        		
        		if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE ) {
                    LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFS);
                } else  if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ZERO){ 
                    LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFSEMAILF);
                } else {
                    LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFF);
                    
                    request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE,QuotationConstants.QUOTATION_ER_INSERT);
                    // Forwarding request to Error page 
            		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
                }
                
                // Build the Success Message
        		sMsg = QuotationConstants.QUOTATION_REASSIGN_TRACKNO1 + oReassignUserDto.getFullName() + QuotationConstants.QUOTATION_REASSIGN_TRACKNO2 + oNewEnquiryDto.getEnquiryNumber();
                
                sLinkMsg1 = QuotationConstants.QUOTATION_RETURNHOME_URL + QuotationConstants.QUOTATION_GRSYMBOL + QuotationConstants.QUOTATION_VIEWLINK_RETURNHOME; 
                sLinkMsg2 = QuotationConstants.QUOTATION_LT_VIEWCR_ENQ_URL + QuotationConstants.QUOTATION_PLACELINKS; 
                sLinkMsg = sLinkMsg1 + QuotationConstants.QUOTATION_BREAK; 
                
                request.setAttribute(QuotationConstants.QUOTATION_HEADINGMESSAGE,QuotationConstants.QUOTATION_SUB_SUCES);
                request.setAttribute(QuotationConstants.QUOTATION_SUCCESSMESSAGE,sMsg);
                request.setAttribute(QuotationConstants.QUOTATION_LINKSMESSAGE,sLinkMsg);
                
        	}
        } catch(Exception e) {
        	//e.printStackTrace();
            /* Logging any exception that raised during this activity in log files using Log4j */
            LoggerUtility.log("DEBUG", this.getClass().getName(), sMethodName, ExceptionUtility.getStackTraceAsString(e));
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        } finally {
            /* Releasing/closing the connection object */
            if ( conn!=null )
                oDBUtility.releaseResources(conn);
        }
    	
    	LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        
    	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);
	}
	
	/**
	 * Method for Search MTO Ratings based on Search Criteria.
	 * @param actionMapping	ActionMapping object to redirect the user to next page depending on the activity
     * @param actionForm	ActionForm object to handle the form elements
     * @param request		HttpServletRequest object to handle request operations
     * @param response		HttpServletResponse object to handle response operations
     * @return returns 		ActionForward depending on which user is redirected to next page
     * @throws Exception	if any error occurs while performing database operations, etc
	 */
	public ActionForward searchMTORatings(ActionMapping actionMapping,
            ActionForm actionForm, 
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
		
		String sMethodName = "searchMTORatings"; 
    	String sClassName = this.getClass().getName();
    	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
    	
    	Connection conn = null;  		// Connection object to store the database connection
        DBUtility oDBUtility = null;  	// Object of DBUtility class to handle DB connection objects
        
        NewMTOEnquiryForm oNewMTOEnquiryForm = null;
        NewEnquiryDto oNewEnquiryDto = null;
        UserDto oUserDto = null;
        UserAction oUserAction = null; //Instantiating the UserAction object
        DaoFactory oDaoFactory = null; //Creating an instance of of DaoFactory & retrieving UserDao object
        QuotationDao oQuotationDao = null;
        MTOQuotationDao oMTOQuotationDao = null;
        
        String sOperationType = null, sOperationType2 = null, sDeptAndAuth = null;
        String[] saSections = null;
        
    	try {
    		/* Setting user's information in request, using the header information received through request */
            setRolesToRequest(request);
            LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "Roles set to request.");
            
            oNewMTOEnquiryForm = (NewMTOEnquiryForm) actionForm;
    		if (oNewMTOEnquiryForm == null)
    			oNewMTOEnquiryForm = new NewMTOEnquiryForm();
    		
    		/* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Instantiating the UserAction object */
            oUserAction = new UserAction();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            oNewEnquiryDto = new NewEnquiryDto();
            oQuotationDao = oDaoFactory.getQuotationDao();
            oMTOQuotationDao = oDaoFactory.getMTOQuotationDao();
            NewEnquiryUtilityManager oNewEnquiryManager = new NewEnquiryUtilityManager();
            
            oNewMTOEnquiryForm.setEnquiryId(CommonUtility.getIntParameter(request, QuotationConstants.QUOTATION_REQUEST_ENQUIRYID));	
            
			if (oNewMTOEnquiryForm.getEnquiryId() < QuotationConstants.QUOTATION_LITERAL_ONE) {
            	 /* Setting error details to request to display the same in Exception page */
                request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_CRET_ROWS); 
                /* Forwarding request to Error page */
                return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);            	
            }
			
			PropertyUtils.copyProperties(oNewEnquiryDto, oNewMTOEnquiryForm);
			oNewEnquiryDto = oMTOQuotationDao.getMTOEnquiryDetailsForView(conn, oNewEnquiryDto, QuotationConstants.QUOTATION_EDIT.toUpperCase()); 
			
			// Invoking Search MTO Ratings logic.
			ArrayList<NewMTORatingDto> alMTORatingsList = oMTOQuotationDao.searchMTORatings(conn, oNewEnquiryDto);
			oNewMTOEnquiryForm.setMtoTotalRatingIndex(alMTORatingsList.size());
			
			ArrayList<NewMTORatingDto> alMTOCompleteList = new ArrayList<>();
			// Fetch MTO AddOn and Technical Details.
			for(NewMTORatingDto oMTORatingDto : alMTORatingsList) {
				oMTORatingDto = oMTOQuotationDao.fetchNewMTOTechAddOnDetails(conn, oMTORatingDto);
				alMTOCompleteList.add(oMTORatingDto);
			}
			oNewEnquiryDto.setNewMTORatingsList(alMTOCompleteList);
			Map<String,String> hmCompleteAddOnsMap = oMTOQuotationDao.fetchMTOAddOnMap(conn, oNewEnquiryDto);
			String jsonMTOAddOns = oNewEnquiryManager.convertListToJSON(hmCompleteAddOnsMap);
			request.setAttribute("jsonMTOAddOns", jsonMTOAddOns);
			
			oNewEnquiryDto = oQuotationDao.loadNewEnquiryLookUpValues(conn, oNewEnquiryDto);
			PropertyUtils.copyProperties(oNewMTOEnquiryForm, oNewEnquiryDto);
			oNewMTOEnquiryForm = oNewEnquiryManager.loadOfferPageLookUpValues(conn, oNewMTOEnquiryForm);
			
			// Set NewRatings List size to totalRatingIndex.
			oNewMTOEnquiryForm.setTotalRatingIndex(oNewEnquiryDto.getNewRatingsList().size());
			// Populate Re-assign Users List.
			oNewMTOEnquiryForm.setMtoReassignUsersList(oMTOQuotationDao.fetchMTOReassignUsersList(conn));
			
    	} catch(Exception e) {
        	//e.printStackTrace();
            /* Logging any exception that raised during this activity in log files using Log4j */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "home", ExceptionUtility.getStackTraceAsString(e));
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        } finally {
            /* Releasing/closing the connection object */
            if ( conn!=null )
                oDBUtility.releaseResources(conn);
        }
    	
    	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
        
    	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_MTOENQUIRY_VIEW);
	}
	
	public ActionForward validateMTOMaterialCostCheckAjax(ActionMapping actionMapping, 
			ActionForm form,
			HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
	
		String sMethodName = "validateMTOMaterialCostCheckAjax"; 
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        Connection conn = null;
        DaoFactory oDaoFactory = null; 
        QuotationDao oQuotationDao = null;
        ListPriseDao oListPriceDao = null;
        NewRatingDto oNewRatingDto = null;
        NewMTORatingDto oNewMTORatingDto = null;
        DBUtility oDBUtility = new DBUtility();
        NewEnquiryUtilityManager oNewEnquiryManager = new NewEnquiryUtilityManager();
        Map<String,String> keyvalueMap = new HashMap<>();
        String sResult = "";
        
        setRolesToRequest(request);
        response.setContentType("application/json");
        
        try {
        	conn = oDBUtility.getDBConnection();
        	oDaoFactory = new DaoFactory();
        	oQuotationDao = oDaoFactory.getQuotationDao();
        	oListPriceDao = oDaoFactory.getListPriseDao();
        	oNewRatingDto = new NewRatingDto();
        	oNewMTORatingDto = new NewMTORatingDto();
        	
        	oNewRatingDto.setLtCustomerId(CommonUtility.getStringParameter(request, "customerId"));
        	oNewRatingDto.setEnquiryId(Integer.parseInt(CommonUtility.getStringParameter(request, "enquiryId")));
        	oNewRatingDto.setRatingNo(Integer.parseInt(CommonUtility.getStringParameter(request, "ratingNo")));
        	// Fetch NewRating values Based on EnquiryId and RatingNo.
        	oNewRatingDto = oQuotationDao.fetchNewRatingByIdandNo(conn, oNewRatingDto);
        	
        	oNewRatingDto.setQuantity(Integer.parseInt(CommonUtility.getStringParameter(request, "quantity")));
			oNewRatingDto.setLtManufacturingLocation(CommonUtility.getStringParameter(request, "mtoMfgLocation"));
			oNewRatingDto.setLtProdLineId(CommonUtility.getStringParameter(request, "mtoProdLineVal"));
			oNewRatingDto.setLtKWId(CommonUtility.getStringParameter(request, "mtoKWVal"));
			oNewRatingDto.setLtFrameId(CommonUtility.getStringParameter(request, "mtoFrameVal"));
			oNewRatingDto.setLtFrameSuffixId(CommonUtility.getStringParameter(request, "mtoFrameSuffixVal"));
			oNewRatingDto.setLtPoleId(CommonUtility.getStringParameter(request, "mtoPoleVal"));
			oNewRatingDto.setLtMountingId(CommonUtility.getStringParameter(request, "mtoMountingVal"));
			oNewRatingDto.setLtTBPositionId(CommonUtility.getStringParameter(request, "mtoTBPosVal"));
			// Set MTO Design Complete value.
			oNewRatingDto.setIsDesignComplete("Y");
			// Set IsReferDesign.
			oNewRatingDto.setIsReferDesign("Y");
			
			// Fetching MTO Material Cost : Set to NewMTORatingDto.
			oNewMTORatingDto.setMtoEnquiryId(Integer.parseInt(CommonUtility.getStringParameter(request, "enquiryId")));
			oNewMTORatingDto.setMtoRatingNo(Integer.parseInt(CommonUtility.getStringParameter(request, "ratingNo")));
			oNewMTORatingDto.setMtoQuantity(Integer.parseInt(CommonUtility.getStringParameter(request, "quantity")));
			oNewMTORatingDto.setMtoMfgLocation(CommonUtility.getStringParameter(request, "mtoMfgLocation"));
			oNewMTORatingDto.setMtoProdLineId(CommonUtility.getStringParameter(request, "mtoProdLineVal"));
			oNewMTORatingDto.setMtoKWId(CommonUtility.getStringParameter(request, "mtoKWVal"));
			oNewMTORatingDto.setMtoFrameId(CommonUtility.getStringParameter(request, "mtoFrameVal"));
			oNewMTORatingDto.setMtoFrameSuffixId(CommonUtility.getStringParameter(request, "mtoFrameSuffixVal"));
			oNewMTORatingDto.setMtoPoleId(CommonUtility.getStringParameter(request, "mtoPoleVal"));
			oNewMTORatingDto.setMtoMountingId(CommonUtility.getStringParameter(request, "mtoMountingVal"));
			oNewMTORatingDto.setMtoTBPositionId(CommonUtility.getStringParameter(request, "mtoTBPosVal"));
			oNewMTORatingDto.setMtoMaterialCost(CommonUtility.getStringParameter(request, "mtoMaterialCostVal"));
			
			sResult = oQuotationDao.updateMTOMaterialCost(conn, oNewRatingDto, oNewMTORatingDto);
			
			String[] sValuesArray = new String[3];
			if (QuotationConstants.QUOTATION_STRING_Y.equalsIgnoreCase(sResult)) {
				sValuesArray[0] = String.valueOf(oNewMTORatingDto.getMtoMaterialCost());
				getAjax(response, conn, QuotationConstants.QUOTATION_FORWARD_SUCCESS, sValuesArray);
			} else {
				sValuesArray[0] = QuotationConstants.QUOTATION_EMPTY;
				getAjax(response, conn, QuotationConstants.QUOTATION_FORWARD_FAILURE, sValuesArray);
			}
			
        } catch(Exception e) {
        	//e.printStackTrace();
        } finally {
            /* Releasing/closing the connection object */
            if ( conn!=null ) 
                oDBUtility.releaseResources(conn);
        }
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
    	return null;
	}
	
	public ActionForward validateMTOPriceCheckAjax(ActionMapping actionMapping, 
			ActionForm form,
			HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		String sMethodName = "validateMTOPriceCheckAjax"; 
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        Connection conn = null;
        DaoFactory oDaoFactory = null; 
        QuotationDao oQuotationDao = null;
        ListPriseDao oListPriceDao = null;
        NewRatingDto oNewRatingDto = null;
        NewMTORatingDto oNewMTORatingDto = null;
        DBUtility oDBUtility = new DBUtility();
        NewEnquiryUtilityManager oNewEnquiryManager = new NewEnquiryUtilityManager();
        Map<String,String> keyvalueMap = new HashMap<>();
        
        setRolesToRequest(request);
        response.setContentType("application/json");
        
        try {
        	conn = oDBUtility.getDBConnection();
        	oDaoFactory = new DaoFactory();
        	oQuotationDao = oDaoFactory.getQuotationDao();
        	oListPriceDao = oDaoFactory.getListPriseDao();
        	oNewRatingDto = new NewRatingDto();
        	oNewMTORatingDto = new NewMTORatingDto();
        	
        	oNewRatingDto.setEnquiryId(Integer.parseInt(CommonUtility.getStringParameter(request, "enquiryId")));
        	oNewRatingDto.setRatingNo(Integer.parseInt(CommonUtility.getStringParameter(request, "ratingNo")));
        	// Fetch NewRating values Based on EnquiryId and RatingNo.
        	oNewRatingDto = oQuotationDao.fetchNewRatingByIdandNo(conn, oNewRatingDto);
        	
        	// Set Customer ID from Request
        	oNewRatingDto.setLtCustomerId(CommonUtility.getStringParameter(request, "customerId"));
        	
        	oNewRatingDto.setQuantity(Integer.parseInt(CommonUtility.getStringParameter(request, "quantity")));
			oNewRatingDto.setLtManufacturingLocation(CommonUtility.getStringParameter(request, "mtoMfgLocation"));
			oNewRatingDto.setLtProdLineId(CommonUtility.getStringParameter(request, "mtoProdLineVal"));
			oNewRatingDto.setLtKWId(CommonUtility.getStringParameter(request, "mtoKWVal"));
			oNewRatingDto.setLtFrameId(CommonUtility.getStringParameter(request, "mtoFrameVal"));
			oNewRatingDto.setLtFrameSuffixId(CommonUtility.getStringParameter(request, "mtoFrameSuffixVal"));
			oNewRatingDto.setLtPoleId(CommonUtility.getStringParameter(request, "mtoPoleVal"));
			oNewRatingDto.setLtMountingId(CommonUtility.getStringParameter(request, "mtoMountingVal"));
			oNewRatingDto.setLtTBPositionId(CommonUtility.getStringParameter(request, "mtoTBPosVal"));
			
			// Fetching MTO Parameters and Add-On values from AJAX : Set to NewMTORatingDto.
			oNewMTORatingDto.setMtoEnquiryId(Integer.parseInt(CommonUtility.getStringParameter(request, "enquiryId")));
			oNewMTORatingDto.setMtoRatingNo(Integer.parseInt(CommonUtility.getStringParameter(request, "ratingNo")));
			oNewMTORatingDto.setMtoQuantity(Integer.parseInt(CommonUtility.getStringParameter(request, "quantity")));
			oNewMTORatingDto.setMtoMfgLocation(CommonUtility.getStringParameter(request, "mtoMfgLocation"));
			oNewMTORatingDto.setMtoProdLineId(CommonUtility.getStringParameter(request, "mtoProdLineVal"));
			oNewMTORatingDto.setMtoKWId(CommonUtility.getStringParameter(request, "mtoKWVal"));
			oNewMTORatingDto.setMtoFrameId(CommonUtility.getStringParameter(request, "mtoFrameVal"));
			oNewMTORatingDto.setMtoFrameSuffixId(CommonUtility.getStringParameter(request, "mtoFrameSuffixVal"));
			oNewMTORatingDto.setMtoPoleId(CommonUtility.getStringParameter(request, "mtoPoleVal"));
			oNewMTORatingDto.setMtoMountingId(CommonUtility.getStringParameter(request, "mtoMountingVal"));
			oNewMTORatingDto.setMtoTBPositionId(CommonUtility.getStringParameter(request, "mtoTBPosVal"));
			oNewMTORatingDto.setMtoSpecialAddOnPercent(CommonUtility.getStringParameter(request, "specialAddOnPercentVal"));
			oNewMTORatingDto.setMtoSpecialCashExtra(CommonUtility.getStringParameter(request, "specialAddOnCashVal"));
			oNewMTORatingDto.setMtoPercentStamping(CommonUtility.getStringParameter(request, "percentStampingVal"));
			oNewMTORatingDto.setMtoPercentCopper(CommonUtility.getStringParameter(request, "percentCopperVal"));
			oNewMTORatingDto.setMtoPercentAluminium(CommonUtility.getStringParameter(request, "percentAluminiumVal"));
			oNewMTORatingDto.setMtoTotalAddOnPercent(CommonUtility.getStringParameter(request, "totalAddOnPercentVal"));
			oNewMTORatingDto.setMtoTotalCashExtra(CommonUtility.getStringParameter(request, "totalAddOnCashVal"));
						
			ListPriceDto oListPriceDto = oNewEnquiryManager.buildListPriceSearchFilter(conn, oQuotationDao, oNewRatingDto);
			
			//CustomerDiscountDto  oCustomerDiscountDto = oNewEnquiryManager.buildCustDiscountSearchFilter(conn, oQuotationDao, oNewRatingDto);
			
			// Method to Fetch the LP Per Unit value from LIST_PRISE Table
			oListPriceDto = oListPriceDao.fetchListPrice(conn, oListPriceDto);
			Double lpPerUnitValue = oListPriceDto.getListPriceValue();
						
			// Method to Fetch the Customer Discount value from CUSTOMER_DISCOUNT Table
			//iCustomerDiscount = oCustomerDiscountDao.fetchCustomerDiscount(conn, oCustomerDiscountDto);
			
			String[] sValuesArray = new String[3];
			if (lpPerUnitValue != null && lpPerUnitValue > 0 ) {
				// Fetch 
				// Calculate and Save MTO Parameters and Price Values.
				oNewEnquiryManager.processMTORatingPrice(oNewRatingDto, oNewMTORatingDto, oListPriceDto);
				
				sValuesArray[0] = String.valueOf(lpPerUnitValue);
			} else {
				sValuesArray[0] = QuotationConstants.QUOTATION_EMPTY;
			}
			
			if (lpPerUnitValue != null && lpPerUnitValue > 0) {
				getAjax(response, conn, QuotationConstants.QUOTATION_FORWARD_SUCCESS, sValuesArray);
			} else {
				getAjax(response, conn, QuotationConstants.QUOTATION_FORWARD_FAILURE, sValuesArray);
			}
			
        } catch(Exception e) {
        	//e.printStackTrace();
        } finally {
            /* Releasing/closing the connection object */
            if ( conn!=null ) 
                oDBUtility.releaseResources(conn);
        }
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
    	return null;
	}
	
	/**
	 * Method to Check if All MTO Ratings are in Design Complete Status or Not.
	 * @param actionMapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward verifyMTODesignCompleteAjax(ActionMapping actionMapping, 
			ActionForm form,
			HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		String sMethodName = "verifyMTODesignCompleteAjax"; 
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        Connection conn = null;
        DaoFactory oDaoFactory = null; 
        QuotationDao oQuotationDao = null;
        MTOQuotationDao oMTOQuotationDao = null;
        DBUtility oDBUtility = new DBUtility();
        String sEnquiryId = "";
        boolean isEnquiryDesignComplete = false; 
        
        setRolesToRequest(request);
        response.setContentType("application/json");
        
        try {
        	conn = oDBUtility.getDBConnection();
        	oDaoFactory = new DaoFactory();
        	oQuotationDao = oDaoFactory.getQuotationDao();
        	oMTOQuotationDao = oDaoFactory.getMTOQuotationDao();
        	
        	sEnquiryId = CommonUtility.getStringParameter(request, QuotationConstants.QUOTATION_REQUEST_ENQUIRYID);
        	
        	isEnquiryDesignComplete = oMTOQuotationDao.verifyMTOEnquiryDesignComplete(conn, sEnquiryId);
        	
        	String[] sValuesArray = new String[3];
        	if (isEnquiryDesignComplete) {
				getAjax(response, conn, QuotationConstants.QUOTATION_FORWARD_SUCCESS, sValuesArray);
			} else {
				getAjax(response, conn, QuotationConstants.QUOTATION_FORWARD_FAILURE, sValuesArray);
			}
        	
        } catch(Exception e) {
        	//e.printStackTrace();
        } finally {
            /* Releasing/closing the connection object */
            if ( conn!=null ) 
                oDBUtility.releaseResources(conn);
        }
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
    	return null;
	}
	
	/**
	 * Method to Submit the MTO Enquiry to SM : Verify if MTO and Complete Ratings are Saved, before Submit.
	 * @param actionMapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward submitMTOEnquiryToSM(ActionMapping actionMapping, 
			ActionForm actionForm,
			HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		String sMethodName = "verifyMTODesignCompleteAjax"; 
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        Connection conn = null;  		// Connection object to store the database connection
        DBUtility oDBUtility = null;  	// Object of DBUtility class to handle DB connection objects
        
        NewMTOEnquiryForm oNewMTOEnquiryForm = null;
        NewEnquiryDto oNewEnquiryDto = null;
        UserAction oUserAction = null; //Instantiating the UserAction object
        DaoFactory oDaoFactory = null; //Creating an instance of of DaoFactory & retrieving UserDao object
        QuotationDao oQuotationDao = null;
        MTOQuotationDao oMTOQuotationDao = null;
        UserDto oUserDto = null;
        NewEnquiryUtilityManager oNewEnquiryManager = new NewEnquiryUtilityManager();
        WorkflowUtility oWorkflowUtility = null;
		WorkflowDto oWorkflowDto = null;
		int iWorkflowExecuteStatus=0;
		String sMsg=null,sLinkMsg1=null,sLinkMsg2=null,sLinkMsg=null,sResult=null;
		String sOperationType = null;
		
        try {
        	/* Setting user's information in request, using the header information received through request */
            setRolesToRequest(request);
            LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "Roles set to request.");
            
            oNewMTOEnquiryForm = (NewMTOEnquiryForm) actionForm;
    		if (oNewMTOEnquiryForm == null)
    			oNewMTOEnquiryForm = new NewMTOEnquiryForm();
    		
    		/* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
            /* Create QuotationDao */
            oQuotationDao = oDaoFactory.getQuotationDao();
            /* Create MTOQuotationDao */
            oMTOQuotationDao = oDaoFactory.getMTOQuotationDao();
            /* Instantiating the UserAction object */
            oUserAction = new UserAction();
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            
            oNewEnquiryDto = new NewEnquiryDto();
        	PropertyUtils.copyProperties(oNewEnquiryDto, oNewMTOEnquiryForm);
        	
        	/* Retrieving the logged-in user information */
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            if (oUserDto == null){
            	oUserDto = new UserDto();
            }
            
            oDBUtility.setAutoCommitFalse(conn);
            
            sOperationType = CommonUtility.getStringParameter(request, QuotationConstants.QUOTATION_OPERATION_TYPE);
            // Save the Note By DM and Design Page Attributes.
            oNewEnquiryDto.setDmUpdateStatus(QuotationConstants.QUOTATION_DMUPDATE_SUBMIT);
            sResult = oMTOQuotationDao.updateDesignFieldsOnReturn(conn, oNewEnquiryDto);
            if (QuotationConstants.QUOTATION_STRING_Y.equals(sResult)) {
				oDBUtility.commit(conn);
			} else {
				oDBUtility.rollback(conn);
			}
            
            // Save the MTO Ratings values.
            ArrayList<NewRatingDto> alMTORatingsList = oNewEnquiryManager.getMTORatingsList(conn, oQuotationDao, oNewEnquiryDto, request);
            sResult = oMTOQuotationDao.saveMTORatingDetails(conn, alMTORatingsList);
            if (QuotationConstants.QUOTATION_STRING_Y.equals(sResult)) {
				oDBUtility.commit(conn);
			} else {
				oDBUtility.rollback(conn);
			}
            
    		oDBUtility.setAutoCommitTrue(conn);
    		
            
        	// Workflow : Submit MTO Enquiry to SM.
        	resetToken(request);
    		LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Initiating Workflow process - - -");
    		//oWorkFlowDao = oDaoFactory.getWorkflowDao();
    		oWorkflowUtility = new WorkflowUtility();
    		oWorkflowDto = new WorkflowDto();
    		oWorkflowDto.setEnquiryId(oNewEnquiryDto.getEnquiryId());
    		oWorkflowDto.setEnquiryNo(oNewEnquiryDto.getEnquiryNumber());
    		oWorkflowDto.setLoginUserId(oUserDto.getUserId());
    		oWorkflowDto.setLoginUserName(oUserDto.getFullName());
    		oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
    		
    		oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTOSM);
    		oWorkflowDto.setOldStatusId(oNewEnquiryDto.getStatusId());
    		
    		LoggerUtility.log("INFO", sClassName, sMethodName, "2 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
    		iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
    		
    		if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE ) {
                LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFS);
            } else  if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ZERO){ 
                LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFSEMAILF);
            } else {
                LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFF);
                
                request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE,QuotationConstants.QUOTATION_ER_INSERT);
                // Forwarding request to Error page 
        		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
            }
            
            // Build the Success Message
    		sMsg = QuotationConstants.QUOTATION_REVISEBYRSM_TRACKNO + oNewEnquiryDto.getEnquiryNumber();
            
            sLinkMsg1 = QuotationConstants.QUOTATION_RETURNHOME_URL + QuotationConstants.QUOTATION_GRSYMBOL + QuotationConstants.QUOTATION_VIEWLINK_RETURNHOME; 
            sLinkMsg2 = QuotationConstants.QUOTATION_LT_VIEWCR_ENQ_URL + QuotationConstants.QUOTATION_PLACELINKS; 
            sLinkMsg = sLinkMsg1 + QuotationConstants.QUOTATION_BREAK; 
            
            request.setAttribute(QuotationConstants.QUOTATION_HEADINGMESSAGE,QuotationConstants.QUOTATION_SUB_SUCES);
            request.setAttribute(QuotationConstants.QUOTATION_SUCCESSMESSAGE,sMsg);
            request.setAttribute(QuotationConstants.QUOTATION_LINKSMESSAGE,sLinkMsg);
            
        } catch(Exception e) {
        	//e.printStackTrace();
            /* Logging any exception that raised during this activity in log files using Log4j */
            LoggerUtility.log("DEBUG", this.getClass().getName(), sMethodName, ExceptionUtility.getStackTraceAsString(e));
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        } finally {
            /* Releasing/closing the connection object */
            if ( conn!=null )
                oDBUtility.releaseResources(conn);
        }
    	
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        
        return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);
	}
	
	/**
	 * Method to Return the MTO Enquiry to Sales Manager.
	 * @param actionMapping
	 * @param actionForm
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward returnMTOEnquiryToSM(ActionMapping actionMapping, 
			ActionForm actionForm,
			HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		String sMethodName = "returnMTOEnquiryToSM"; 
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        Connection conn = null;  		// Connection object to store the database connection
        DBUtility oDBUtility = null;  	// Object of DBUtility class to handle DB connection objects
        
        NewMTOEnquiryForm oNewMTOEnquiryForm = null;
        NewEnquiryDto oNewEnquiryDto = null;
        UserAction oUserAction = null; //Instantiating the UserAction object
        DaoFactory oDaoFactory = null; //Creating an instance of of DaoFactory & retrieving UserDao object
        QuotationDao oQuotationDao = null;
        MTOQuotationDao oMTOQuotationDao = null; 
        UserDto oUserDto = null;
        WorkflowUtility oWorkflowUtility = null;
		WorkflowDto oWorkflowDto = null;
		int iWorkflowExecuteStatus=0;
		String sMsg=null,sLinkMsg1=null,sLinkMsg2=null,sLinkMsg=null,sResult=null;
		
		try {
			/* Setting user's information in request, using the header information received through request */
            setRolesToRequest(request);
            LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "Roles set to request.");
            
            oNewMTOEnquiryForm = (NewMTOEnquiryForm) actionForm;
    		if (oNewMTOEnquiryForm == null)
    			oNewMTOEnquiryForm = new NewMTOEnquiryForm();
    		
    		/* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
            /* Create QuotationDao */
            oQuotationDao = oDaoFactory.getQuotationDao();
            oMTOQuotationDao = oDaoFactory.getMTOQuotationDao();
            /* Instantiating the UserAction object */
            oUserAction = new UserAction();
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            NewEnquiryUtilityManager oNewEnquiryManager = new NewEnquiryUtilityManager();
            
            oNewEnquiryDto = new NewEnquiryDto();
        	PropertyUtils.copyProperties(oNewEnquiryDto, oNewMTOEnquiryForm);
        	
        	/* Retrieving the logged-in user information */
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            if (oUserDto == null){
            	oUserDto = new UserDto();
            }
            
            oDBUtility.setAutoCommitFalse(conn);
            
            // Save the Note By DM and Design Page Attributes.
            oNewEnquiryDto.setDmUpdateStatus(QuotationConstants.QUOTATION_DMUPDATE_RETURN);
            sResult = oMTOQuotationDao.updateDesignFieldsOnReturn(conn, oNewEnquiryDto);
            if (QuotationConstants.QUOTATION_STRING_Y.equals(sResult)) {
				oDBUtility.commit(conn);
			} else {
				oDBUtility.rollback(conn);
			}
            
            // Save the MTO Ratings values.
            ArrayList<NewRatingDto> alMTORatingsList = oNewEnquiryManager.getMTORatingsList(conn, oQuotationDao, oNewEnquiryDto, request);
            sResult = oMTOQuotationDao.saveMTORatingDetails(conn, alMTORatingsList);
            if (QuotationConstants.QUOTATION_STRING_Y.equals(sResult)) {
				oDBUtility.commit(conn);
			} else {
				oDBUtility.rollback(conn);
			}
            
            oDBUtility.setAutoCommitTrue(conn);
            
            // Workflow : Return MTO Enquiry to SM.
        	resetToken(request);
    		LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Initiating Workflow process - - -");
    		//oWorkFlowDao = oDaoFactory.getWorkflowDao();
    		oWorkflowUtility = new WorkflowUtility();
    		oWorkflowDto = new WorkflowDto();
    		oWorkflowDto.setEnquiryId(oNewEnquiryDto.getEnquiryId());
    		oWorkflowDto.setEnquiryNo(oNewEnquiryDto.getEnquiryNumber());
    		oWorkflowDto.setLoginUserId(oUserDto.getUserId());
    		oWorkflowDto.setLoginUserName(oUserDto.getFullName());
    		oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
    		
    		oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_RETURNTOSM);
    		oWorkflowDto.setOldStatusId(oNewEnquiryDto.getStatusId());
    		
    		LoggerUtility.log("INFO", sClassName, sMethodName, "2 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
    		iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
    		
    		if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE ) {
                LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFS);
            } else  if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ZERO){ 
                LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFSEMAILF);
            } else {
                LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFF);
                
                request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE,QuotationConstants.QUOTATION_ER_INSERT);
                // Forwarding request to Error page 
        		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
            }
    		
    		// Build the Success Message
    		sMsg = QuotationConstants.QUOTATION_REVISEBYRSM_TRACKNO + oNewEnquiryDto.getEnquiryNumber();
            
            sLinkMsg1 = QuotationConstants.QUOTATION_RETURNHOME_URL + QuotationConstants.QUOTATION_GRSYMBOL + QuotationConstants.QUOTATION_VIEWLINK_RETURNHOME; 
            sLinkMsg2 = QuotationConstants.QUOTATION_LT_VIEWCR_ENQ_URL + QuotationConstants.QUOTATION_PLACELINKS; 
            sLinkMsg = sLinkMsg1 + QuotationConstants.QUOTATION_BREAK; 
            
            request.setAttribute(QuotationConstants.QUOTATION_HEADINGMESSAGE,QuotationConstants.QUOTATION_SUB_SUCES);
            request.setAttribute(QuotationConstants.QUOTATION_SUCCESSMESSAGE,sMsg);
            request.setAttribute(QuotationConstants.QUOTATION_LINKSMESSAGE,sLinkMsg);
            
		} catch(Exception e) {
        	//e.printStackTrace();
            /* Logging any exception that raised during this activity in log files using Log4j */
            LoggerUtility.log("DEBUG", this.getClass().getName(), sMethodName, ExceptionUtility.getStackTraceAsString(e));
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        } finally {
            /* Releasing/closing the connection object */
            if ( conn!=null )
                oDBUtility.releaseResources(conn);
        }
		
		LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        
        return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);
	}
	
	/**
	 * Method to move the MTO Enquiry to Regret status.
	 * @param actionMapping
	 * @param actionForm
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward regretMTOEnquiry(ActionMapping actionMapping, 
			ActionForm actionForm,
			HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		String sMethodName = "regretMTOEnquiry"; 
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        Connection conn = null;  		// Connection object to store the database connection
        DBUtility oDBUtility = null;  	// Object of DBUtility class to handle DB connection objects
        
        NewMTOEnquiryForm oNewMTOEnquiryForm = null;
        NewEnquiryDto oNewEnquiryDto = null;
        UserAction oUserAction = null; //Instantiating the UserAction object
        DaoFactory oDaoFactory = null; //Creating an instance of of DaoFactory & retrieving UserDao object
        QuotationDao oQuotationDao = null;
        UserDto oUserDto = null;
        WorkflowUtility oWorkflowUtility = null;
		WorkflowDto oWorkflowDto = null;
		int iWorkflowExecuteStatus=0;
		String sMsg=null,sLinkMsg1=null,sLinkMsg2=null,sLinkMsg=null;
		
		try {
			/* Setting user's information in request, using the header information received through request */
            setRolesToRequest(request);
            LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "Roles set to request.");
            
            oNewMTOEnquiryForm = (NewMTOEnquiryForm) actionForm;
    		if (oNewMTOEnquiryForm == null)
    			oNewMTOEnquiryForm = new NewMTOEnquiryForm();
    		
    		/* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
            /* Create QuotationDao */
            oQuotationDao = oDaoFactory.getQuotationDao();
            /* Instantiating the UserAction object */
            oUserAction = new UserAction();
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            
            oNewEnquiryDto = new NewEnquiryDto();
        	PropertyUtils.copyProperties(oNewEnquiryDto, oNewMTOEnquiryForm);
        	
        	/* Retrieving the logged-in user information */
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            if (oUserDto == null){
            	oUserDto = new UserDto();
            }
            
            // Workflow : Regret MTO Enquiry.
        	resetToken(request);
    		LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Initiating Workflow process - - -");
    		//oWorkFlowDao = oDaoFactory.getWorkflowDao();
    		oWorkflowUtility = new WorkflowUtility();
    		oWorkflowDto = new WorkflowDto();
    		oWorkflowDto.setEnquiryId(oNewEnquiryDto.getEnquiryId());
    		oWorkflowDto.setEnquiryNo(oNewEnquiryDto.getEnquiryNumber());
    		oWorkflowDto.setLoginUserId(oUserDto.getUserId());
    		oWorkflowDto.setLoginUserName(oUserDto.getFullName());
    		oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
    		
    		oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_REGRETMTOENQUIRY);
    		oWorkflowDto.setOldStatusId(oNewEnquiryDto.getStatusId());
    		
    		LoggerUtility.log("INFO", sClassName, sMethodName, "2 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
    		iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
    		
    		if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE ) {
                LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFS);
            } else  if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ZERO){ 
                LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFSEMAILF);
            } else {
                LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFF);
                
                request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE,QuotationConstants.QUOTATION_ER_INSERT);
                // Forwarding request to Error page 
        		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
            }
    		
    		// Build the Success Message
    		sMsg = QuotationConstants.QUOTATION_REVISEBYRSM_TRACKNO + oNewEnquiryDto.getEnquiryNumber();
            
            sLinkMsg1 = QuotationConstants.QUOTATION_RETURNHOME_URL + QuotationConstants.QUOTATION_GRSYMBOL + QuotationConstants.QUOTATION_VIEWLINK_RETURNHOME; 
            sLinkMsg2 = QuotationConstants.QUOTATION_LT_VIEWCR_ENQ_URL + QuotationConstants.QUOTATION_PLACELINKS; 
            sLinkMsg = sLinkMsg1 + QuotationConstants.QUOTATION_BREAK; 
            
            request.setAttribute(QuotationConstants.QUOTATION_HEADINGMESSAGE,QuotationConstants.QUOTATION_SUB_SUCES);
            request.setAttribute(QuotationConstants.QUOTATION_SUCCESSMESSAGE,sMsg);
            request.setAttribute(QuotationConstants.QUOTATION_LINKSMESSAGE,sLinkMsg);
            
		} catch(Exception e) {
        	//e.printStackTrace();
            /* Logging any exception that raised during this activity in log files using Log4j */
            LoggerUtility.log("DEBUG", this.getClass().getName(), sMethodName, ExceptionUtility.getStackTraceAsString(e));
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        } finally {
            /* Releasing/closing the connection object */
            if ( conn!=null )
                oDBUtility.releaseResources(conn);
        }
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        
        return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);
	}
	
	/**
	 * Method to Save all Ratings Values for MTO Enquiry.
	 * @param actionMapping
	 * @param actionForm
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward saveMTOEnquiry(ActionMapping actionMapping, 
			ActionForm actionForm,
			HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		String sMethodName = "saveMTOEnquiry"; 
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        Connection conn = null;  		// Connection object to store the database connection
        DBUtility oDBUtility = null;  	// Object of DBUtility class to handle DB connection objects
        
        NewMTOEnquiryForm oNewMTOEnquiryForm = null;
        NewEnquiryDto oNewEnquiryDto = null;
        UserAction oUserAction = null; //Instantiating the UserAction object
        DaoFactory oDaoFactory = null; //Creating an instance of of DaoFactory & retrieving UserDao object
        QuotationDao oQuotationDao = null;
        MTOQuotationDao oMTOQuotationDao = null;
        UserDto oUserDto = null;
        String sResult=null;
		
		try {
			/* Setting user's information in request, using the header information received through request */
            setRolesToRequest(request);
            LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "Roles set to request.");
            
            oNewMTOEnquiryForm = (NewMTOEnquiryForm) actionForm;
    		if (oNewMTOEnquiryForm == null)
    			oNewMTOEnquiryForm = new NewMTOEnquiryForm();
    		
    		/* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
            /* Create QuotationDao */
            oQuotationDao = oDaoFactory.getQuotationDao();
            /* Create MTOQuotationDao */
            oMTOQuotationDao = oDaoFactory.getMTOQuotationDao();
            /* Instantiating the UserAction object */
            oUserAction = new UserAction();
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            NewEnquiryUtilityManager oNewEnquiryManager = new NewEnquiryUtilityManager();
            
            oNewEnquiryDto = new NewEnquiryDto();
        	PropertyUtils.copyProperties(oNewEnquiryDto, oNewMTOEnquiryForm);
        	
        	/* Retrieving the logged-in user information */
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            if (oUserDto == null){
            	oUserDto = new UserDto();
            }
            
            oDBUtility.setAutoCommitFalse(conn);
            
            // Save the Design Page Attributes.
            sResult = oMTOQuotationDao.updateDesignFieldsOnReturn(conn, oNewEnquiryDto);
            if (QuotationConstants.QUOTATION_STRING_Y.equals(sResult)) {
				oDBUtility.commit(conn);
			} else {
				oDBUtility.rollback(conn);
			}
            
            // Save Design(MTO) Section Details.
            int mtoTotalRatingIdx = oNewMTOEnquiryForm.getMtoTotalRatingIndex();
            // Save MTO Tech Ratings.
            // Save AddOn Details for MTO Tech Ratings.
            ArrayList<NewMTORatingDto> alMTOTechRatingsfromPage = oNewEnquiryManager.getMTOTechRatingsList(conn, oNewEnquiryDto, mtoTotalRatingIdx, request);
            ArrayList<NewMTORatingDto> alMTOTechCompleteDetailsfromPage = new ArrayList<>();
            for(NewMTORatingDto oMTOTechRatingDto : alMTOTechRatingsfromPage) {
            	oMTOTechRatingDto = oNewEnquiryManager.getMTOTechRatingsWithDesignAddOns(conn, oMTOTechRatingDto, mtoTotalRatingIdx, request);
            	alMTOTechCompleteDetailsfromPage.add(oMTOTechRatingDto);
            }
            sResult = oMTOQuotationDao.saveMTOTechCompleteDetails(conn, alMTOTechCompleteDetailsfromPage, oUserDto.getUserId());
            if (QuotationConstants.QUOTATION_STRING_Y.equals(sResult)) {
				oDBUtility.commit(conn);
			} else {
				oDBUtility.rollback(conn);
			}
            
            // Save the MTO Ratings values.
            ArrayList<NewRatingDto> alMTORatingsList = oNewEnquiryManager.getMTORatingsList(conn, oQuotationDao, oNewEnquiryDto, request);
            sResult = oMTOQuotationDao.saveMTORatingDetails(conn, alMTORatingsList);
            if (QuotationConstants.QUOTATION_STRING_Y.equals(sResult)) {
				oDBUtility.commit(conn);
			} else {
				oDBUtility.rollback(conn);
			}
            
            oDBUtility.setAutoCommitTrue(conn);
            
            // ------------------------------------------------------------------------------
            // Fetch Complete Enquiry Details and Set to NewMTOEnquiryForm.
            oNewEnquiryDto = oMTOQuotationDao.getMTOEnquiryDetailsForView(conn, oNewEnquiryDto, QuotationConstants.QUOTATION_EDIT.toUpperCase()); 
            
            ArrayList<NewMTORatingDto> alMTORatingsFetchList = oMTOQuotationDao.fetchNewMTORatingDetails(conn, oNewEnquiryDto);
			oNewMTOEnquiryForm.setMtoTotalRatingIndex(alMTORatingsFetchList.size());
            
			ArrayList<NewMTORatingDto> alMTOCompleteList = new ArrayList<>();
			// Fetch MTO AddOn and Technical Details.
			for(NewMTORatingDto oMTORatingDto : alMTORatingsFetchList) {
				oMTORatingDto = oMTOQuotationDao.fetchNewMTOTechAddOnDetails(conn, oMTORatingDto);
				alMTOCompleteList.add(oMTORatingDto);
			}
			oNewEnquiryDto.setNewMTORatingsList(alMTOCompleteList);
			Map<String,String> hmCompleteAddOnsMap = oMTOQuotationDao.fetchMTOAddOnMap(conn, oNewEnquiryDto);
			String jsonMTOAddOns = oNewEnquiryManager.convertListToJSON(hmCompleteAddOnsMap);
			//System.out.println("In Action class :: jsonMTOAddOns ..... " + jsonMTOAddOns);
			request.setAttribute("jsonMTOAddOns", jsonMTOAddOns);
			
			oNewEnquiryDto = oQuotationDao.loadNewEnquiryLookUpValues(conn, oNewEnquiryDto);
			PropertyUtils.copyProperties(oNewMTOEnquiryForm, oNewEnquiryDto);
			oNewMTOEnquiryForm = oNewEnquiryManager.loadOfferPageLookUpValues(conn, oNewMTOEnquiryForm);
        	
			// Set NewRatings List size to totalRatingIndex.
			oNewMTOEnquiryForm.setTotalRatingIndex(oNewEnquiryDto.getNewRatingsList().size());
			// Populate Re-assign Users List.
			oNewMTOEnquiryForm.setMtoReassignUsersList(oMTOQuotationDao.fetchMTOReassignUsersList(conn));
			
		} catch(Exception e) {
        	//e.printStackTrace();
            /* Logging any exception that raised during this activity in log files using Log4j */
            LoggerUtility.log("DEBUG", this.getClass().getName(), sMethodName, ExceptionUtility.getStackTraceAsString(e));
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        } finally {
            /* Releasing/closing the connection object */
            if ( conn!=null )
                oDBUtility.releaseResources(conn);
        }
		
		LoggerUtility.log("INFO", sClassName, sMethodName, "END");
		
		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_MTOENQUIRY_VIEW);
	}
	
	private void getAjax(HttpServletResponse response, Connection conn, String sConfirmationMessage, String[] sValuesArray) throws Exception {
		response.setContentType("text/xml");
		PrintWriter oOut = null;

		StringBuffer sb = new StringBuffer();

		sb.append("<?xml version=\"1.0\" ?>");
		sb.append("<pricecalculation>");
		try {
			oOut = response.getWriter();
			sb.append("<msg>").append(sConfirmationMessage).append("</msg>");
			sb.append("<returnValue1>").append(sValuesArray[0]).append("</returnValue1>");
			sb.append("<returnValue2>").append(sValuesArray[1]).append("</returnValue2>");
		} finally {
			sb.append("</pricecalculation>");
			oOut.print(sb);
			oOut.flush();
			oOut.close();
		}
	}
	
}
