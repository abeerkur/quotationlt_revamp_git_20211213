/**
 * ******************************************************************************************
 * Project Name: Request For Quotation
 * Document Name: AddOnDto.java
 * Package: in.com.rbc.quotation.enquiry.dto
 * Desc: DTO class that holds getters and setters of each property.
 * ******************************************************************************************
 * @author 100006126
 * ******************************************************************************************
 */

package in.com.rbc.quotation.enquiry.dto;

import in.com.rbc.quotation.common.utility.CommonUtility;

public class AddOnDto {
	
	/*
	 * TABLE - RFQ_ADDON_DETAILS
	 */
	
	private int addOnRatingId; //QAO_RATINGID
	private String addOnTypeText ; //QAO_ADDONTYPE
	private int addOnQuantity; //QAO_QTY
	private double addOnUnitPrice; //QAO_UNITPRICE
	private int addOnUnitPriceCE; //QAO_UNITPRICE_CE
	private int addOnUnitPriceRSM; //QAO_UNITPRICE_RSM
	private int addOnPercent;
	
	private String formattedAddOnUnitPrice;
	private String formattedAddOnUnitPriceCE;
	private String formattedAddOnUnitPriceRSM;
	
	
	/**
	 * @return Returns the addOnQuantity.
	 */
	public int getAddOnQuantity() {
		return addOnQuantity;
	}
	/**
	 * @param addOnQuantity The addOnQuantity to set.
	 */
	public void setAddOnQuantity(int addOnQuantity) {
		this.addOnQuantity = addOnQuantity;
	}
	/**
	 * @return Returns the addOnRatingId.
	 */
	public int getAddOnRatingId() {
		return addOnRatingId;
	}
	/**
	 * @param addOnRatingId The addOnRatingId to set.
	 */
	public void setAddOnRatingId(int addOnRatingId) {
		this.addOnRatingId = addOnRatingId;
	}
	/**
	 * @return Returns the addOnType.
	 */
/*	public String getAddOnType() {
		return CommonUtility.replaceNull(addOnType);
	}
	*//**
	 * @param addOnType The addOnType to set.
	 *//*
	public void setAddOnType(String addOnType) {
		this.addOnType = addOnType;
	}
*/	/**
	 * @return Returns the addOnUnitPrice.
	 */
	public double getAddOnUnitPrice() {
		return addOnUnitPrice;
	}
	public String getAddOnTypeText() {
	return CommonUtility.replaceNull(addOnTypeText);
}
public void setAddOnTypeText(String addOnTypeText) {
	this.addOnTypeText = addOnTypeText;
}
	/**
	 * @param addOnUnitPrice The addOnUnitPrice to set.
	 */
	public void setAddOnUnitPrice(double addOnUnitPrice) {
		this.addOnUnitPrice = addOnUnitPrice;
	}
	/**
	 * @return Returns the addOnUnitPriceCE.
	 */
	public int getAddOnUnitPriceCE() {
		return addOnUnitPriceCE;
	}
	/**
	 * @param addOnUnitPriceCE The addOnUnitPriceCE to set.
	 */
	public void setAddOnUnitPriceCE(int addOnUnitPriceCE) {
		this.addOnUnitPriceCE = addOnUnitPriceCE;
	}
	/**
	 * @return Returns the addOnUnitPriceRSM.
	 */
	public int getAddOnUnitPriceRSM() {
		return addOnUnitPriceRSM;
	}
	/**
	 * @param addOnUnitPriceRSM The addOnUnitPriceRSM to set.
	 */
	public void setAddOnUnitPriceRSM(int addOnUnitPriceRSM) {
		this.addOnUnitPriceRSM = addOnUnitPriceRSM;
	}
	/**
	 * @return Returns the formattedAddOnUnitPrice.
	 */
	public String getFormattedAddOnUnitPrice() {
		return formattedAddOnUnitPrice;
	}
	/**
	 * @param formattedAddOnUnitPrice The formattedAddOnUnitPrice to set.
	 */
	public void setFormattedAddOnUnitPrice(String formattedAddOnUnitPrice) {
		this.formattedAddOnUnitPrice = formattedAddOnUnitPrice;
	}
	/**
	 * @return Returns the formattedAddOnUnitPriceCE.
	 */
	public String getFormattedAddOnUnitPriceCE() {
		return formattedAddOnUnitPriceCE;
	}
	/**
	 * @param formattedAddOnUnitPriceCE The formattedAddOnUnitPriceCE to set.
	 */
	public void setFormattedAddOnUnitPriceCE(String formattedAddOnUnitPriceCE) {
		this.formattedAddOnUnitPriceCE = formattedAddOnUnitPriceCE;
	}
	/**
	 * @return Returns the formattedAddOnUnitPriceRSM.
	 */
	public String getFormattedAddOnUnitPriceRSM() {
		return formattedAddOnUnitPriceRSM;
	}
	/**
	 * @param formattedAddOnUnitPriceRSM The formattedAddOnUnitPriceRSM to set.
	 */
	public void setFormattedAddOnUnitPriceRSM(String formattedAddOnUnitPriceRSM) {
		this.formattedAddOnUnitPriceRSM = formattedAddOnUnitPriceRSM;
	}
	
	public int getAddOnPercent() {
		return addOnPercent;
	}
	public void setAddOnPercent(int addOnPercent) {
		this.addOnPercent = addOnPercent;
	}
	
	

}
