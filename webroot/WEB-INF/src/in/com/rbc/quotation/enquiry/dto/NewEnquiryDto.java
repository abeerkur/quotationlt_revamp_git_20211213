/**
 * ******************************************************************************************
 * Project Name: Request For Quotation
 * Document Name: NewEnquiryDto.java
 * Package: in.com.rbc.quotation.enquiry.dto
 * Desc: DTO class that holds getters and setters of each property.
 * ******************************************************************************************
 */

package in.com.rbc.quotation.enquiry.dto;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts.upload.FormFile;

import in.com.rbc.quotation.common.utility.CommonUtility;
import in.com.rbc.quotation.common.vo.KeyValueVo;

public class NewEnquiryDto {
	
	private String dispatch; // This variable is for diffrentiating between create and edit
	private String operationType; // This variable is to check whether INSERT OR UPDATE operation has to be done for enquiry 
	private String enquiryOperationType; // This variable is to diffrentiate between the button actions Manager or engineer
	private String ratingOperationType; // This variable is to check whether INSERT OR UPDATE operation has to be done for rating
	private String ratingsValidationMessage; // This variable will have the validation message - when submit button in clicked we display an error message if all the ratings are not validated
	private String currentRating; // This variable will be used to display the current tab in bold
	private String commentOperation;
	
	private String isLast; // This variable is to check whether the current rating is the last rating or not in edit page
	private String nextRatingId; // This variable will store the next rating id
	private String isMaxRating; // Based on this variable value "Add New rating button will be displayed" 
	
	private String enquiryReferenceNumber; 
	private int enquiryId;
	private int revisionNumber; 
	private String enquiryNumber;
	private int statusId;
	private String statusName;
	private String createdBy;
	private String createdBySSO;
	private String createdDate;
	private String lastModifiedBy;
	private String lastModifiedDate;
	private String designReferenceNumber;
	
	// New Fields - Indent Pages
	private String savingIndent;
	private String sapCustomerCode;
	private String orderNumber;
	private String poiCopy;
	private String loiCopy;
	private String deliveryType;
	private String deliveryReqByCust;
	private String expectedDeliveryMonth;
	private String advancePayment;
	private String paymentTerms;
	private String ld;
	private String drawingApproval;
	private String qapApproval;
	private String typeTest;
	private String routineTest;
	private String specialTest;
	private String specialTestDetails;
	private String thirdPartyInspection;
	private String stageInspection;
	private String enduserLocation;
	private String epcName;
	private String insurance;
	private String replacement;
	private String oldSerialNo;
	private String customerPartNo;
	private String winlossReason;
	private String winlossComment;
	private String winningCompetitor;
	private String remarks;
	
	private String totalMotorPrice;
	private String totalPkgPrice;
	private String includeFrieght;
	private String includeExtraWarrnt;
	private String includeSupervisionCost;
	private String includetypeTestCharges;
	
	private String extraFrieght;
	private String extraExtraWarrnt;
	private String extraSupervisionCost;
	private String extratypeTestCharges;
	private String sparesCost;
	
	// --------------------    Enquiry Information	-------------------- 
	private String customerEnqReference;
	private int customerId;
	private String customerName;
	private String customerType;
	private int locationId = 0;
	private String location;
	private String customerIndustry;
	private String concernedPerson;
	private String concernedPersonHdn;
	private String projectName;
	private String is_marathon_approved;
	private String consultantName;
	private String endUser;
	private String endUserIndustry;
	private String locationOfInstallation;
	private String locationOfInstallationId;
	private String customerDealerLink;
	// --------------------    Enquiry Waitage	-------------------- 
	private String enquiryType;
	private String enquiryStage;
	private String winningChance;
	private String targeted;
	// --------------------    Important Dates	--------------------
	private String receiptDate;
	private String entryDate;
	private String submissionDate;
	private String actualSubmissionDate;
	private String closingDate;
	
	// --------------------    Commercial Terms and Conditions ----------
	private String deliveryTerm;
	
	private String pt1PercentAmtNetorderValue;
	private String pt1PaymentDays;
	private String pt1PayableTerms;
	private String pt1Instrument;
	private String pt2PercentAmtNetorderValue;
	private String pt2PaymentDays;
	private String pt2PayableTerms;
	private String pt2Instrument;
	private String pt3PercentAmtNetorderValue;
	private String pt3PaymentDays;
	private String pt3PayableTerms;
	private String pt3Instrument;
	private String pt4PercentAmtNetorderValue;
	private String pt4PaymentDays;
	private String pt4PayableTerms;
	private String pt4Instrument;
	private String pt5PercentAmtNetorderValue;
	private String pt5PaymentDays;
	private String pt5PayableTerms;
	private String pt5Instrument;
	
	private String warrantyDispatchDate;
	private String warrantyCommissioningDate;
	private String custRequestedDeliveryTime;
	private String orderCompletionLeadTime;
	private String deliveryInFull;
	private String gstValue;
	private String packaging;
	private String liquidatedDamagesDueToDeliveryDelay;
	private String supervisionDetails;
	private String superviseNoOfManDays;
	private String superviseToFroTravel;
	private String superviseLodging;
	private String offerValidity;
	private String priceValidity;
	
	private FormFile commPurchaseSpecFile1 = null;
	private String commPurchaseSpecFileDesc1;
	
	
	// --------------------    Rating properties	----------
	private int ratingId;
	private String ratingTitle;
	private int ratingNo;
	private int quantity = 0;
	
	private ArrayList<NewRatingDto> newRatingsList = new ArrayList<NewRatingDto>();
	private ArrayList<NewMTORatingDto> newMTORatingsList = new ArrayList<NewMTORatingDto>();
	
	private List<KeyValueVo> newCompleteAddOnsList = new ArrayList<>();
	private ArrayList<String> alPDFProdLinesList = new ArrayList<>();
	
	//Array List objects
	private ArrayList customerList = new ArrayList();
	private ArrayList locationList = new ArrayList();
	private ArrayList alApprovalList = new ArrayList();
	private ArrayList alTargetedList = new ArrayList();
	private ArrayList alIndustryList = new ArrayList();
	private ArrayList alEnquiryTypeList = new ArrayList();
	private ArrayList alWinChanceList = new ArrayList();
	private ArrayList alInstallLocationList = new ArrayList();
	
	private int totalRatingIndex = 0;

	private String isNewEnquiry;
	private boolean isPdfView = false;
	private String dmFlag;
	
	private TeamMemberDto teamMemberDto = null;
	private ArrayList teamMemberList = new ArrayList();
	private ArrayList salesManagerList = new ArrayList();
	
	private String revision;
	
	private int attachmentId;
	private String attachmentFileName; //Stores Dm Attachment File Names
	
	private String smNote;
	private String sm_name;
	private String smEmail;
	private String smContactNo;
	
	private String rsmNote;
	private String nsmNote;
	private String mhNote;
	private String designNote;
	private String dmUpdateStatus;
	private String bhNote;
	private String mfgNote;
	private String qaNote;
	private String scmNote;
	
	private String approvalUserType;
	private String isReferDesign;
	private String commentsDeviations;
	private String reassignRemarks;

	// Won / Lost / Abondent - Required Fields.
	private String abondentReason1;
	private String abondentReason2;
	private String abondentRemarks;
	private String abondentCustomerApproval;
	
	private String lostCompetitor;
	private String lostReason1;
	private String lostReason2;
	private String lostRemarks;
	private String lostManagerApproval;
	private String totalQuotedPrice;
	private String totalLostPrice;
	
	private String wonIndentNo;
	private String wonIsPOorLOI;
	private String wonPONo;
	private String wonPODate;
	private String wonPOReceiptDate;
	private String wonReason1;
	private String wonReason2;
	private String wonRemarks;
	private String totalWonPrice;
	private String wonRequiresDrawing;
	private String wonRequiresDatasheet;
	private String wonRequiresQAP;
	private String wonRequiresPerfCurves;
	
	// MC - NSP
	private String cummulativeTotalCost;
	private String cummulativeProfitMargin;
	
	// --------------------    Design MTO Search Related -----------------
	private String searchMfgLocation;
	private String searchProdLine;
	private String searchKW;
	private String searchPole;
	private String searchFrame;
	private String searchFrameSuffix;
	private String searchMounting;
	private String searchTBPosition;
	
	// --------------------    Approval Email Notifier Related -----------------
	private String isNotifierEmailSent;
	private String notifierEmailSentToSSO;
	private String notifierEmailSentDate;
	private String notifierType;
	
	private String workflowAssignedTo;
	private String workflowAssignedDate;
	
	public String getDispatch() {
		return CommonUtility.replaceNull(dispatch);
	}

	public void setDispatch(String dispatch) {
		this.dispatch = dispatch;
	}

	public String getOperationType() {
		return CommonUtility.replaceNull(operationType);
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	public String getEnquiryOperationType() {
		return CommonUtility.replaceNull(enquiryOperationType);
	}

	public void setEnquiryOperationType(String enquiryOperationType) {
		this.enquiryOperationType = enquiryOperationType;
	}

	public String getRatingOperationType() {
		return CommonUtility.replaceNull(ratingOperationType);
	}

	public void setRatingOperationType(String ratingOperationType) {
		this.ratingOperationType = ratingOperationType;
	}
	
	public String getRatingsValidationMessage() {
		return CommonUtility.replaceNull(ratingsValidationMessage);
	}

	public void setRatingsValidationMessage(String ratingsValidationMessage) {
		this.ratingsValidationMessage = ratingsValidationMessage;
	}

	public String getCurrentRating() {
		return CommonUtility.replaceNull(currentRating);
	}

	public void setCurrentRating(String currentRating) {
		this.currentRating = currentRating;
	}

	public String getCommentOperation() {
		return CommonUtility.replaceNull(commentOperation);
	}

	public void setCommentOperation(String commentOperation) {
		this.commentOperation = commentOperation;
	}
	
	public String getIsLast() {
		return CommonUtility.replaceNull(isLast);
	}

	public void setIsLast(String isLast) {
		this.isLast = isLast;
	}

	public String getNextRatingId() {
		return CommonUtility.replaceNull(nextRatingId);
	}

	public void setNextRatingId(String nextRatingId) {
		this.nextRatingId = nextRatingId;
	}

	public String getIsMaxRating() {
		return CommonUtility.replaceNull(isMaxRating);
	}

	public void setIsMaxRating(String isMaxRating) {
		this.isMaxRating = isMaxRating;
	}
	
	public String getEnquiryReferenceNumber() {
		return enquiryReferenceNumber;
	}

	public void setEnquiryReferenceNumber(String enquiryReferenceNumber) {
		this.enquiryReferenceNumber = enquiryReferenceNumber;
	}

	public int getEnquiryId() {
		return enquiryId;
	}

	public void setEnquiryId(int enquiryId) {
		this.enquiryId = enquiryId;
	}

	public int getRevisionNumber() {
		return revisionNumber;
	}

	public void setRevisionNumber(int revisionNumber) {
		this.revisionNumber = revisionNumber;
	}

	public String getEnquiryNumber() {
		return CommonUtility.replaceNull(enquiryNumber);
	}

	public void setEnquiryNumber(String enquiryNumber) {
		this.enquiryNumber = enquiryNumber;
	}

	public int getStatusId() {
		return statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	public String getStatusName() {
		return CommonUtility.replaceNull(statusName);
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public String getCreatedBy() {
		return CommonUtility.replaceNull(createdBy);
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedBySSO() {
		return CommonUtility.replaceNull(createdBySSO);
	}

	public void setCreatedBySSO(String createdBySSO) {
		this.createdBySSO = createdBySSO;
	}

	public String getCreatedDate() {
		return CommonUtility.replaceNull(createdDate);
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getLastModifiedBy() {
		return CommonUtility.replaceNull(lastModifiedBy);
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public String getLastModifiedDate() {
		return CommonUtility.replaceNull(lastModifiedDate);
	}

	public void setLastModifiedDate(String lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	
	public String getCustomerEnqReference() {
		return CommonUtility.replaceNull(customerEnqReference);
	}

	public void setCustomerEnqReference(String customerEnqReference) {
		this.customerEnqReference = customerEnqReference;
	}
	
	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return CommonUtility.replaceNull(customerName);
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerType() {
		return CommonUtility.replaceNull(customerType);
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public int getLocationId() {
		return locationId;
	}

	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}

	public String getLocation() {
		return CommonUtility.replaceNull(location);
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getCustomerIndustry() {
		return CommonUtility.replaceNull(customerIndustry);
	}

	public void setCustomerIndustry(String customerIndustry) {
		this.customerIndustry = customerIndustry;
	}

	public String getConcernedPerson() {
		return CommonUtility.replaceNull(concernedPerson);
	}

	public void setConcernedPerson(String concernedPerson) {
		this.concernedPerson = concernedPerson;
	}

	public String getConcernedPersonHdn() {
		return CommonUtility.replaceNull(concernedPersonHdn);
	}

	public void setConcernedPersonHdn(String concernedPersonHdn) {
		this.concernedPersonHdn = concernedPersonHdn;
	}

	public String getProjectName() {
		return CommonUtility.replaceNull(projectName);
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getIs_marathon_approved() {
		return CommonUtility.replaceNull(is_marathon_approved);
	}

	public void setIs_marathon_approved(String is_marathon_approved) {
		this.is_marathon_approved = is_marathon_approved;
	}

	public String getConsultantName() {
		return CommonUtility.replaceNull(consultantName);
	}

	public void setConsultantName(String consultantName) {
		this.consultantName = consultantName;
	}

	public String getEndUser() {
		return CommonUtility.replaceNull(endUser);
	}

	public void setEndUser(String endUser) {
		this.endUser = endUser;
	}

	public String getEndUserIndustry() {
		return CommonUtility.replaceNull(endUserIndustry);
	}

	public void setEndUserIndustry(String endUserIndustry) {
		this.endUserIndustry = endUserIndustry;
	}

	public String getLocationOfInstallation() {
		return CommonUtility.replaceNull(locationOfInstallation);
	}

	public void setLocationOfInstallation(String locationOfInstallation) {
		this.locationOfInstallation = locationOfInstallation;
	}

	public String getLocationOfInstallationId() {
		return CommonUtility.replaceNull(locationOfInstallationId);
	}

	public void setLocationOfInstallationId(String locationOfInstallationId) {
		this.locationOfInstallationId = locationOfInstallationId;
	}
	
	public String getEnquiryType() {
		return CommonUtility.replaceNull(enquiryType);
	}

	public void setEnquiryType(String enquiryType) {
		this.enquiryType = enquiryType;
	}

	public String getEnquiryStage() {
		return CommonUtility.replaceNull(enquiryStage);
	}

	public void setEnquiryStage(String enquiryStage) {
		this.enquiryStage = enquiryStage;
	}

	public String getWinningChance() {
		return CommonUtility.replaceNull(winningChance);
	}

	public void setWinningChance(String winningChance) {
		this.winningChance = winningChance;
	}

	public String getTargeted() {
		return CommonUtility.replaceNull(targeted);
	}

	public void setTargeted(String targeted) {
		this.targeted = targeted;
	}

	public String getReceiptDate() {
		return CommonUtility.replaceNull(receiptDate);
	}

	public void setReceiptDate(String receiptDate) {
		this.receiptDate = receiptDate;
	}

	public String getEntryDate() {
		return CommonUtility.replaceNull(entryDate);
	}

	public void setEntryDate(String entryDate) {
		this.entryDate = entryDate;
	}

	public String getSubmissionDate() {
		return CommonUtility.replaceNull(submissionDate);
	}

	public void setSubmissionDate(String submissionDate) {
		this.submissionDate = submissionDate;
	}

	public String getActualSubmissionDate() {
		return CommonUtility.replaceNull(actualSubmissionDate);
	}

	public void setActualSubmissionDate(String actualSubmissionDate) {
		this.actualSubmissionDate = actualSubmissionDate;
	}

	public String getClosingDate() {
		return CommonUtility.replaceNull(closingDate);
	}

	public void setClosingDate(String closingDate) {
		this.closingDate = closingDate;
	}

	public String getDeliveryTerm() {
		return CommonUtility.replaceNull(deliveryTerm);
	}

	public void setDeliveryTerm(String deliveryTerm) {
		this.deliveryTerm = deliveryTerm;
	}

	public String getPt1PercentAmtNetorderValue() {
		return CommonUtility.replaceNull(pt1PercentAmtNetorderValue);
	}

	public void setPt1PercentAmtNetorderValue(String pt1PercentAmtNetorderValue) {
		this.pt1PercentAmtNetorderValue = pt1PercentAmtNetorderValue;
	}

	public String getPt1PaymentDays() {
		return CommonUtility.replaceNull(pt1PaymentDays);
	}

	public void setPt1PaymentDays(String pt1PaymentDays) {
		this.pt1PaymentDays = pt1PaymentDays;
	}

	public String getPt1PayableTerms() {
		return CommonUtility.replaceNull(pt1PayableTerms);
	}

	public void setPt1PayableTerms(String pt1PayableTerms) {
		this.pt1PayableTerms = pt1PayableTerms;
	}

	public String getPt1Instrument() {
		return CommonUtility.replaceNull(pt1Instrument);
	}

	public void setPt1Instrument(String pt1Instrument) {
		this.pt1Instrument = pt1Instrument;
	}

	public String getPt2PercentAmtNetorderValue() {
		return CommonUtility.replaceNull(pt2PercentAmtNetorderValue);
	}

	public void setPt2PercentAmtNetorderValue(String pt2PercentAmtNetorderValue) {
		this.pt2PercentAmtNetorderValue = pt2PercentAmtNetorderValue;
	}

	public String getPt2PaymentDays() {
		return CommonUtility.replaceNull(pt2PaymentDays);
	}

	public void setPt2PaymentDays(String pt2PaymentDays) {
		this.pt2PaymentDays = pt2PaymentDays;
	}

	public String getPt2PayableTerms() {
		return CommonUtility.replaceNull(pt2PayableTerms);
	}

	public void setPt2PayableTerms(String pt2PayableTerms) {
		this.pt2PayableTerms = pt2PayableTerms;
	}

	public String getPt2Instrument() {
		return CommonUtility.replaceNull(pt2Instrument);
	}

	public void setPt2Instrument(String pt2Instrument) {
		this.pt2Instrument = pt2Instrument;
	}

	public String getPt3PercentAmtNetorderValue() {
		return CommonUtility.replaceNull(pt3PercentAmtNetorderValue);
	}

	public void setPt3PercentAmtNetorderValue(String pt3PercentAmtNetorderValue) {
		this.pt3PercentAmtNetorderValue = pt3PercentAmtNetorderValue;
	}

	public String getPt3PaymentDays() {
		return CommonUtility.replaceNull(pt3PaymentDays);
	}

	public void setPt3PaymentDays(String pt3PaymentDays) {
		this.pt3PaymentDays = pt3PaymentDays;
	}

	public String getPt3PayableTerms() {
		return CommonUtility.replaceNull(pt3PayableTerms);
	}

	public void setPt3PayableTerms(String pt3PayableTerms) {
		this.pt3PayableTerms = pt3PayableTerms;
	}

	public String getPt3Instrument() {
		return CommonUtility.replaceNull(pt3Instrument);
	}

	public void setPt3Instrument(String pt3Instrument) {
		this.pt3Instrument = pt3Instrument;
	}

	public String getPt4PercentAmtNetorderValue() {
		return CommonUtility.replaceNull(pt4PercentAmtNetorderValue);
	}

	public void setPt4PercentAmtNetorderValue(String pt4PercentAmtNetorderValue) {
		this.pt4PercentAmtNetorderValue = pt4PercentAmtNetorderValue;
	}

	public String getPt4PaymentDays() {
		return CommonUtility.replaceNull(pt4PaymentDays);
	}

	public void setPt4PaymentDays(String pt4PaymentDays) {
		this.pt4PaymentDays = pt4PaymentDays;
	}

	public String getPt4PayableTerms() {
		return CommonUtility.replaceNull(pt4PayableTerms);
	}

	public void setPt4PayableTerms(String pt4PayableTerms) {
		this.pt4PayableTerms = pt4PayableTerms;
	}

	public String getPt4Instrument() {
		return CommonUtility.replaceNull(pt4Instrument);
	}

	public void setPt4Instrument(String pt4Instrument) {
		this.pt4Instrument = pt4Instrument;
	}

	public String getPt5PercentAmtNetorderValue() {
		return CommonUtility.replaceNull(pt5PercentAmtNetorderValue);
	}

	public void setPt5PercentAmtNetorderValue(String pt5PercentAmtNetorderValue) {
		this.pt5PercentAmtNetorderValue = pt5PercentAmtNetorderValue;
	}

	public String getPt5PaymentDays() {
		return CommonUtility.replaceNull(pt5PaymentDays);
	}

	public void setPt5PaymentDays(String pt5PaymentDays) {
		this.pt5PaymentDays = pt5PaymentDays;
	}

	public String getPt5PayableTerms() {
		return CommonUtility.replaceNull(pt5PayableTerms);
	}

	public void setPt5PayableTerms(String pt5PayableTerms) {
		this.pt5PayableTerms = pt5PayableTerms;
	}

	public String getPt5Instrument() {
		return CommonUtility.replaceNull(pt5Instrument);
	}

	public void setPt5Instrument(String pt5Instrument) {
		this.pt5Instrument = pt5Instrument;
	}

	public String getWarrantyDispatchDate() {
		return CommonUtility.replaceNull(warrantyDispatchDate);
	}

	public void setWarrantyDispatchDate(String warrantyDispatchDate) {
		this.warrantyDispatchDate = warrantyDispatchDate;
	}

	public String getWarrantyCommissioningDate() {
		return CommonUtility.replaceNull(warrantyCommissioningDate);
	}

	public void setWarrantyCommissioningDate(String warrantyCommissioningDate) {
		this.warrantyCommissioningDate = warrantyCommissioningDate;
	}

	public String getCustRequestedDeliveryTime() {
		return CommonUtility.replaceNull(custRequestedDeliveryTime);
	}

	public void setCustRequestedDeliveryTime(String custRequestedDeliveryTime) {
		this.custRequestedDeliveryTime = custRequestedDeliveryTime;
	}

	public String getOrderCompletionLeadTime() {
		return CommonUtility.replaceNull(orderCompletionLeadTime);
	}

	public void setOrderCompletionLeadTime(String orderCompletionLeadTime) {
		this.orderCompletionLeadTime = orderCompletionLeadTime;
	}

	public String getDeliveryInFull() {
		return CommonUtility.replaceNull(deliveryInFull);
	}

	public void setDeliveryInFull(String deliveryInFull) {
		this.deliveryInFull = deliveryInFull;
	}

	public String getGstValue() {
		return CommonUtility.replaceNull(gstValue);
	}

	public void setGstValue(String gstValue) {
		this.gstValue = gstValue;
	}

	public String getPackaging() {
		return CommonUtility.replaceNull(packaging);
	}

	public void setPackaging(String packaging) {
		this.packaging = packaging;
	}

	public String getLiquidatedDamagesDueToDeliveryDelay() {
		return CommonUtility.replaceNull(liquidatedDamagesDueToDeliveryDelay);
	}

	public void setLiquidatedDamagesDueToDeliveryDelay(String liquidatedDamagesDueToDeliveryDelay) {
		this.liquidatedDamagesDueToDeliveryDelay = liquidatedDamagesDueToDeliveryDelay;
	}

	public String getSupervisionDetails() {
		return CommonUtility.replaceNull(supervisionDetails);
	}

	public void setSupervisionDetails(String supervisionDetails) {
		this.supervisionDetails = supervisionDetails;
	}

	public String getSuperviseNoOfManDays() {
		return CommonUtility.replaceNull(superviseNoOfManDays);
	}

	public void setSuperviseNoOfManDays(String superviseNoOfManDays) {
		this.superviseNoOfManDays = superviseNoOfManDays;
	}

	public String getSuperviseToFroTravel() {
		return CommonUtility.replaceNull(superviseToFroTravel);
	}

	public void setSuperviseToFroTravel(String superviseToFroTravel) {
		this.superviseToFroTravel = superviseToFroTravel;
	}

	public String getSuperviseLodging() {
		return CommonUtility.replaceNull(superviseLodging);
	}

	public void setSuperviseLodging(String superviseLodging) {
		this.superviseLodging = superviseLodging;
	}
	
	public String getOfferValidity() {
		return CommonUtility.replaceNull(offerValidity);
	}

	public void setOfferValidity(String offerValidity) {
		this.offerValidity = offerValidity;
	}

	public String getPriceValidity() {
		return CommonUtility.replaceNull(priceValidity);
	}

	public void setPriceValidity(String priceValidity) {
		this.priceValidity = priceValidity;
	}
	
	// Commercial Purchase Spec : File Upload
	public FormFile getCommPurchaseSpecFile1() {
		return commPurchaseSpecFile1;
	}

	public void setCommPurchaseSpecFile1(FormFile commPurchaseSpecFile1) {
		this.commPurchaseSpecFile1 = commPurchaseSpecFile1;
	}

	public String getCommPurchaseSpecFileDesc1() {
		return CommonUtility.replaceNull(commPurchaseSpecFileDesc1);
	}

	public void setCommPurchaseSpecFileDesc1(String commPurchaseSpecFileDesc1) {
		this.commPurchaseSpecFileDesc1 = commPurchaseSpecFileDesc1;
	}

	public int getRatingId() {
		return ratingId;
	}

	public void setRatingId(int ratingId) {
		this.ratingId = ratingId;
	}

	public String getRatingTitle() {
		return CommonUtility.replaceNull(ratingTitle);
	}

	public void setRatingTitle(String ratingTitle) {
		this.ratingTitle = ratingTitle;
	}

	public int getRatingNo() {
		return ratingNo;
	}

	public void setRatingNo(int ratingNo) {
		this.ratingNo = ratingNo;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	// Quotationlt - newRatingsList
	public ArrayList<NewRatingDto> getNewRatingsList() {
		return newRatingsList;
	}

	public void setNewRatingsList(ArrayList<NewRatingDto> newRatingsList) {
		this.newRatingsList = newRatingsList;
	}

	public ArrayList<NewMTORatingDto> getNewMTORatingsList() {
		return newMTORatingsList;
	}

	public void setNewMTORatingsList(ArrayList<NewMTORatingDto> newMTORatingsList) {
		this.newMTORatingsList = newMTORatingsList;
	}
	
	public List<KeyValueVo> getNewCompleteAddOnsList() {
		return newCompleteAddOnsList;
	}

	public void setNewCompleteAddOnsList(List<KeyValueVo> newCompleteAddOnsList) {
		this.newCompleteAddOnsList = newCompleteAddOnsList;
	}

	public ArrayList getCustomerList() {
		return customerList;
	}

	public void setCustomerList(ArrayList customerList) {
		this.customerList = customerList;
	}

	public ArrayList getLocationList() {
		return locationList;
	}

	public void setLocationList(ArrayList locationList) {
		this.locationList = locationList;
	}

	
	public ArrayList getAlApprovalList() {
		return alApprovalList;
	}

	public void setAlApprovalList(ArrayList alApprovalList) {
		this.alApprovalList = alApprovalList;
	}

	public ArrayList getAlTargetedList() {
		return alTargetedList;
	}

	public void setAlTargetedList(ArrayList alTargetedList) {
		this.alTargetedList = alTargetedList;
	}

	public ArrayList getAlIndustryList() {
		return alIndustryList;
	}

	public void setAlIndustryList(ArrayList alIndustryList) {
		this.alIndustryList = alIndustryList;
	}

	public ArrayList getAlEnquiryTypeList() {
		return alEnquiryTypeList;
	}

	public void setAlEnquiryTypeList(ArrayList alEnquiryTypeList) {
		this.alEnquiryTypeList = alEnquiryTypeList;
	}
	
	public ArrayList getAlWinChanceList() {
		return alWinChanceList;
	}

	public void setAlWinChanceList(ArrayList alWinChanceList) {
		this.alWinChanceList = alWinChanceList;
	}
	
	public ArrayList getAlInstallLocationList() {
		return alInstallLocationList;
	}

	public void setAlInstallLocationList(ArrayList alInstallLocationList) {
		this.alInstallLocationList = alInstallLocationList;
	}

	// Quotationlt - totalRatingIndex - To indicate number of Ratings added in Enquiry
	public int getTotalRatingIndex() {
		return totalRatingIndex;
	}

	public void setTotalRatingIndex(int totalRatingIndex) {
		this.totalRatingIndex = totalRatingIndex;
	}
	
	public String getIsNewEnquiry() {
		return CommonUtility.replaceNull(isNewEnquiry);
	}

	public void setIsNewEnquiry(String isNewEnquiry) {
		this.isNewEnquiry = isNewEnquiry;
	}
	
	public boolean isPdfView() {
		return isPdfView;
	}

	public void setPdfView(boolean isPdfView) {
		this.isPdfView = isPdfView;
	}

	public String getDmFlag() {
		return dmFlag;
	}

	public void setDmFlag(String dmFlag) {
		this.dmFlag = dmFlag;
	}

	public TeamMemberDto getTeamMemberDto() {
		return teamMemberDto;
	}

	public void setTeamMemberDto(TeamMemberDto teamMemberDto) {
		this.teamMemberDto = teamMemberDto;
	}

	public ArrayList getTeamMemberList() {
		return teamMemberList;
	}

	public void setTeamMemberList(ArrayList teamMemberList) {
		this.teamMemberList = teamMemberList;
	}

	public ArrayList getSalesManagerList() {
		return salesManagerList;
	}

	public void setSalesManagerList(ArrayList salesManagerList) {
		this.salesManagerList = salesManagerList;
	}
	
	public String getRevision() {
		return CommonUtility.replaceNull(revision);
	}
	public void setRevision(String revision) {
		this.revision = revision;
	}

	public int getAttachmentId() {
		return attachmentId;
	}

	public void setAttachmentId(int attachmentId) {
		this.attachmentId = attachmentId;
	}
	
	public String getAttachmentFileName() {
		return CommonUtility.replaceNull(attachmentFileName);
	}
	public void setAttachmentFileName(String attachmentFileName) {
		this.attachmentFileName = attachmentFileName;
	}

	public String getSmNote() {
		return CommonUtility.replaceNull(smNote);
	}
	public void setSmNote(String smNote) {
		this.smNote = smNote;
	}
	public String getSm_name() {
		return CommonUtility.replaceNull(sm_name);
	}
	public void setSm_name(String sm_name) {
		this.sm_name = sm_name;
	}
	public String getSmEmail() {
		return CommonUtility.replaceNull(smEmail);
	}
	public void setSmEmail(String smEmail) {
		this.smEmail = smEmail;
	}
	public String getSmContactNo() {
		return CommonUtility.replaceNull(smContactNo);
	}
	public void setSmContactNo(String smContactNo) {
		this.smContactNo = smContactNo;
	}

	public String getApprovalUserType() {
		return CommonUtility.replaceNull(approvalUserType);
	}
	public void setApprovalUserType(String approvalUserType) {
		this.approvalUserType = approvalUserType;
	}
	
	public String getRsmNote() {
		return CommonUtility.replaceNull(rsmNote);
	}
	public void setRsmNote(String rsmNote) {
		this.rsmNote = rsmNote;
	}
	public String getNsmNote() {
		return CommonUtility.replaceNull(nsmNote);
	}
	public void setNsmNote(String nsmNote) {
		this.nsmNote = nsmNote;
	}
	public String getMhNote() {
		return CommonUtility.replaceNull(mhNote);
	}
	public void setMhNote(String mhNote) {
		this.mhNote = mhNote;
	}
	public String getBhNote() {
		return CommonUtility.replaceNull(bhNote);
	}
	public void setBhNote(String bhNote) {
		this.bhNote = bhNote;
	}
	public String getMfgNote() {
		return CommonUtility.replaceNull(mfgNote);
	}
	public void setMfgNote(String mfgNote) {
		this.mfgNote = mfgNote;
	}
	public String getQaNote() {
		return CommonUtility.replaceNull(qaNote);
	}
	public void setQaNote(String qaNote) {
		this.qaNote = qaNote;
	}
	public String getScmNote() {
		return CommonUtility.replaceNull(scmNote);
	}
	public void setScmNote(String scmNote) {
		this.scmNote = scmNote;
	}
	public String getDesignNote() {
		return CommonUtility.replaceNull(designNote);
	}
	public void setDesignNote(String designNote) {
		this.designNote = designNote;
	}
	public String getDmUpdateStatus() {
		return CommonUtility.replaceNull(dmUpdateStatus);
	}
	public void setDmUpdateStatus(String dmUpdateStatus) {
		this.dmUpdateStatus = dmUpdateStatus;
	}
	public String getIsReferDesign() {
		return CommonUtility.replaceNull(isReferDesign);
	}
	public void setIsReferDesign(String isReferDesign) {
		this.isReferDesign = isReferDesign;
	}
	public String getCommentsDeviations() {
		return CommonUtility.replaceNull(commentsDeviations);
	}
	public void setCommentsDeviations(String commentsDeviations) {
		this.commentsDeviations = commentsDeviations;
	}
	
	public String getReassignRemarks() {
		return CommonUtility.replaceNull(reassignRemarks);
	}

	public void setReassignRemarks(String reassignRemarks) {
		this.reassignRemarks = reassignRemarks;
	}

	public String getAbondentReason1() {
		return CommonUtility.replaceNull(abondentReason1);
	}
	public void setAbondentReason1(String abondentReason1) {
		this.abondentReason1 = abondentReason1;
	}

	public String getAbondentReason2() {
		return CommonUtility.replaceNull(abondentReason2);
	}
	public void setAbondentReason2(String abondentReason2) {
		this.abondentReason2 = abondentReason2;
	}

	public String getAbondentRemarks() {
		return CommonUtility.replaceNull(abondentRemarks);
	}
	public void setAbondentRemarks(String abondentRemarks) {
		this.abondentRemarks = abondentRemarks;
	}

	public String getAbondentCustomerApproval() {
		return CommonUtility.replaceNull(abondentCustomerApproval);
	}
	public void setAbondentCustomerApproval(String abondentCustomerApproval) {
		this.abondentCustomerApproval = abondentCustomerApproval;
	}

	public String getSavingIndent() {
		return CommonUtility.replaceNull(savingIndent);
	}

	public void setSavingIndent(String savingIndent) {
		this.savingIndent = savingIndent;
	}

	public String getSapCustomerCode() {
		return CommonUtility.replaceNull(sapCustomerCode);
	}

	public void setSapCustomerCode(String sapCustomerCode) {
		this.sapCustomerCode = sapCustomerCode;
	}

	public String getOrderNumber() {
		return CommonUtility.replaceNull(orderNumber);
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getPoiCopy() {
		return CommonUtility.replaceNull(poiCopy);
	}

	public void setPoiCopy(String poiCopy) {
		this.poiCopy = poiCopy;
	}

	public String getLoiCopy() {
		return CommonUtility.replaceNull(loiCopy);
	}

	public void setLoiCopy(String loiCopy) {
		this.loiCopy = loiCopy;
	}

	public String getDeliveryType() {
		return CommonUtility.replaceNull(deliveryType);
	}

	public void setDeliveryType(String deliveryType) {
		this.deliveryType = deliveryType;
	}

	public String getDeliveryReqByCust() {
		return CommonUtility.replaceNull(deliveryReqByCust);
	}

	public void setDeliveryReqByCust(String deliveryReqByCust) {
		this.deliveryReqByCust = deliveryReqByCust;
	}

	public String getExpectedDeliveryMonth() {
		return CommonUtility.replaceNull(expectedDeliveryMonth);
	}

	public void setExpectedDeliveryMonth(String expectedDeliveryMonth) {
		this.expectedDeliveryMonth = expectedDeliveryMonth;
	}

	public String getAdvancePayment() {
		return CommonUtility.replaceNull(advancePayment);
	}

	public void setAdvancePayment(String advancePayment) {
		this.advancePayment = advancePayment;
	}

	public String getPaymentTerms() {
		return CommonUtility.replaceNull(paymentTerms);
	}

	public void setPaymentTerms(String paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public String getLd() {
		return CommonUtility.replaceNull(ld);
	}

	public void setLd(String ld) {
		this.ld = ld;
	}

	public String getDrawingApproval() {
		return CommonUtility.replaceNull(drawingApproval);
	}

	public void setDrawingApproval(String drawingApproval) {
		this.drawingApproval = drawingApproval;
	}

	public String getQapApproval() {
		return CommonUtility.replaceNull(qapApproval);
	}

	public void setQapApproval(String qapApproval) {
		this.qapApproval = qapApproval;
	}

	public String getTypeTest() {
		return CommonUtility.replaceNull(typeTest);
	}

	public void setTypeTest(String typeTest) {
		this.typeTest = typeTest;
	}

	public String getRoutineTest() {
		return CommonUtility.replaceNull(routineTest);
	}

	public void setRoutineTest(String routineTest) {
		this.routineTest = routineTest;
	}

	public String getSpecialTest() {
		return CommonUtility.replaceNull(specialTest);
	}

	public void setSpecialTest(String specialTest) {
		this.specialTest = specialTest;
	}

	public String getSpecialTestDetails() {
		return CommonUtility.replaceNull(specialTestDetails);
	}

	public void setSpecialTestDetails(String specialTestDetails) {
		this.specialTestDetails = specialTestDetails;
	}

	public String getThirdPartyInspection() {
		return CommonUtility.replaceNull(thirdPartyInspection);
	}

	public void setThirdPartyInspection(String thirdPartyInspection) {
		this.thirdPartyInspection = thirdPartyInspection;
	}

	public String getStageInspection() {
		return CommonUtility.replaceNull(stageInspection);
	}

	public void setStageInspection(String stageInspection) {
		this.stageInspection = stageInspection;
	}

	public String getEnduserLocation() {
		return CommonUtility.replaceNull(enduserLocation);
	}

	public void setEnduserLocation(String enduserLocation) {
		this.enduserLocation = enduserLocation;
	}

	public String getEpcName() {
		return CommonUtility.replaceNull(epcName);
	}

	public void setEpcName(String epcName) {
		this.epcName = epcName;
	}

	public String getInsurance() {
		return CommonUtility.replaceNull(insurance);
	}

	public void setInsurance(String insurance) {
		this.insurance = insurance;
	}

	public String getReplacement() {
		return CommonUtility.replaceNull(replacement);
	}

	public void setReplacement(String replacement) {
		this.replacement = replacement;
	}

	public String getOldSerialNo() {
		return CommonUtility.replaceNull(oldSerialNo);
	}

	public void setOldSerialNo(String oldSerialNo) {
		this.oldSerialNo = oldSerialNo;
	}

	public String getCustomerPartNo() {
		return CommonUtility.replaceNull(customerPartNo);
	}

	public void setCustomerPartNo(String customerPartNo) {
		this.customerPartNo = customerPartNo;
	}

	public String getWinlossReason() {
		return CommonUtility.replaceNull(winlossReason);
	}

	public void setWinlossReason(String winlossReason) {
		this.winlossReason = winlossReason;
	}

	public String getWinlossComment() {
		return CommonUtility.replaceNull(winlossComment);
	}

	public void setWinlossComment(String winlossComment) {
		this.winlossComment = winlossComment;
	}

	public String getWinningCompetitor() {
		return CommonUtility.replaceNull(winningCompetitor);
	}

	public void setWinningCompetitor(String winningCompetitor) {
		this.winningCompetitor = winningCompetitor;
	}

	public String getRemarks() {
		return CommonUtility.replaceNull(remarks);
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getTotalMotorPrice() {
		return CommonUtility.replaceNull(totalMotorPrice);
	}

	public void setTotalMotorPrice(String totalMotorPrice) {
		this.totalMotorPrice = totalMotorPrice;
	}

	public String getTotalPkgPrice() {
		return CommonUtility.replaceNull(totalPkgPrice);
	}

	public void setTotalPkgPrice(String totalPkgPrice) {
		this.totalPkgPrice = totalPkgPrice;
	}

	public String getIncludeFrieght() {
		return CommonUtility.replaceNull(includeFrieght);
	}

	public void setIncludeFrieght(String includeFrieght) {
		this.includeFrieght = includeFrieght;
	}

	public String getIncludeExtraWarrnt() {
		return CommonUtility.replaceNull(includeExtraWarrnt);
	}

	public void setIncludeExtraWarrnt(String includeExtraWarrnt) {
		this.includeExtraWarrnt = includeExtraWarrnt;
	}

	public String getIncludeSupervisionCost() {
		return CommonUtility.replaceNull(includeSupervisionCost);
	}

	public void setIncludeSupervisionCost(String includeSupervisionCost) {
		this.includeSupervisionCost = includeSupervisionCost;
	}

	public String getIncludetypeTestCharges() {
		return CommonUtility.replaceNull(includetypeTestCharges);
	}

	public void setIncludetypeTestCharges(String includetypeTestCharges) {
		this.includetypeTestCharges = includetypeTestCharges;
	}

	public String getExtraFrieght() {
		return CommonUtility.replaceNull(extraFrieght);
	}

	public void setExtraFrieght(String extraFrieght) {
		this.extraFrieght = extraFrieght;
	}

	public String getExtraExtraWarrnt() {
		return CommonUtility.replaceNull(extraExtraWarrnt);
	}

	public void setExtraExtraWarrnt(String extraExtraWarrnt) {
		this.extraExtraWarrnt = extraExtraWarrnt;
	}

	public String getExtraSupervisionCost() {
		return CommonUtility.replaceNull(extraSupervisionCost);
	}

	public void setExtraSupervisionCost(String extraSupervisionCost) {
		this.extraSupervisionCost = extraSupervisionCost;
	}

	public String getExtratypeTestCharges() {
		return CommonUtility.replaceNull(extratypeTestCharges);
	}

	public void setExtratypeTestCharges(String extratypeTestCharges) {
		this.extratypeTestCharges = extratypeTestCharges;
	}

	public String getSparesCost() {
		return CommonUtility.replaceNull(sparesCost);
	}

	public void setSparesCost(String sparesCost) {
		this.sparesCost = sparesCost;
	}
	
	public String getLostCompetitor() {
		return CommonUtility.replaceNull(lostCompetitor);
	}

	public void setLostCompetitor(String lostCompetitor) {
		this.lostCompetitor = lostCompetitor;
	}

	public String getLostReason1() {
		return CommonUtility.replaceNull(lostReason1);
	}

	public void setLostReason1(String lostReason1) {
		this.lostReason1 = lostReason1;
	}

	public String getLostReason2() {
		return CommonUtility.replaceNull(lostReason2);
	}

	public void setLostReason2(String lostReason2) {
		this.lostReason2 = lostReason2;
	}

	public String getLostRemarks() {
		return CommonUtility.replaceNull(lostRemarks);
	}

	public void setLostRemarks(String lostRemarks) {
		this.lostRemarks = lostRemarks;
	}

	public String getLostManagerApproval() {
		return CommonUtility.replaceNull(lostManagerApproval);
	}

	public void setLostManagerApproval(String lostManagerApproval) {
		this.lostManagerApproval = lostManagerApproval;
	}

	public String getTotalQuotedPrice() {
		return CommonUtility.replaceNull(totalQuotedPrice);
	}

	public void setTotalQuotedPrice(String totalQuotedPrice) {
		this.totalQuotedPrice = totalQuotedPrice;
	}

	public String getTotalLostPrice() {
		return CommonUtility.replaceNull(totalLostPrice);
	}

	public void setTotalLostPrice(String totalLostPrice) {
		this.totalLostPrice = totalLostPrice;
	}

	public String getTotalWonPrice() {
		return CommonUtility.replaceNull(totalWonPrice);
	}

	public void setTotalWonPrice(String totalWonPrice) {
		this.totalWonPrice = totalWonPrice;
	}

	public String getSearchMfgLocation() {
		return CommonUtility.replaceNull(searchMfgLocation);
	}

	public void setSearchMfgLocation(String searchMfgLocation) {
		this.searchMfgLocation = searchMfgLocation;
	}

	public String getSearchProdLine() {
		return CommonUtility.replaceNull(searchProdLine);
	}

	public void setSearchProdLine(String searchProdLine) {
		this.searchProdLine = searchProdLine;
	}

	public String getSearchKW() {
		return CommonUtility.replaceNull(searchKW);
	}

	public void setSearchKW(String searchKW) {
		this.searchKW = searchKW;
	}

	public String getSearchPole() {
		return CommonUtility.replaceNull(searchPole);
	}

	public void setSearchPole(String searchPole) {
		this.searchPole = searchPole;
	}

	public String getSearchFrame() {
		return CommonUtility.replaceNull(searchFrame);
	}

	public void setSearchFrame(String searchFrame) {
		this.searchFrame = searchFrame;
	}

	public String getSearchFrameSuffix() {
		return CommonUtility.replaceNull(searchFrameSuffix);
	}

	public void setSearchFrameSuffix(String searchFrameSuffix) {
		this.searchFrameSuffix = searchFrameSuffix;
	}

	public String getSearchMounting() {
		return CommonUtility.replaceNull(searchMounting);
	}

	public void setSearchMounting(String searchMounting) {
		this.searchMounting = searchMounting;
	}

	public String getSearchTBPosition() {
		return CommonUtility.replaceNull(searchTBPosition);
	}

	public void setSearchTBPosition(String searchTBPosition) {
		this.searchTBPosition = searchTBPosition;
	}

	public String getDesignReferenceNumber() {
		return CommonUtility.replaceNull(designReferenceNumber);
	}

	public void setDesignReferenceNumber(String designReferenceNumber) {
		this.designReferenceNumber = designReferenceNumber;
	}

	public String getWonIndentNo() {
		return CommonUtility.replaceNull(wonIndentNo);
	}

	public void setWonIndentNo(String wonIndentNo) {
		this.wonIndentNo = wonIndentNo;
	}

	public String getWonIsPOorLOI() {
		return CommonUtility.replaceNull(wonIsPOorLOI);
	}

	public void setWonIsPOorLOI(String wonIsPOorLOI) {
		this.wonIsPOorLOI = wonIsPOorLOI;
	}

	public String getWonPONo() {
		return CommonUtility.replaceNull(wonPONo);
	}

	public void setWonPONo(String wonPONo) {
		this.wonPONo = wonPONo;
	}

	public String getWonPODate() {
		return CommonUtility.replaceNull(wonPODate);
	}

	public void setWonPODate(String wonPODate) {
		this.wonPODate = wonPODate;
	}

	public String getWonPOReceiptDate() {
		return CommonUtility.replaceNull(wonPOReceiptDate);
	}

	public void setWonPOReceiptDate(String wonPOReceiptDate) {
		this.wonPOReceiptDate = wonPOReceiptDate;
	}

	public String getWonReason1() {
		return CommonUtility.replaceNull(wonReason1);
	}

	public void setWonReason1(String wonReason1) {
		this.wonReason1 = wonReason1;
	}

	public String getWonReason2() {
		return CommonUtility.replaceNull(wonReason2);
	}

	public void setWonReason2(String wonReason2) {
		this.wonReason2 = wonReason2;
	}

	public String getWonRemarks() {
		return CommonUtility.replaceNull(wonRemarks);
	}

	public void setWonRemarks(String wonRemarks) {
		this.wonRemarks = wonRemarks;
	}

	public String getWonRequiresDrawing() {
		return CommonUtility.replaceNull(wonRequiresDrawing);
	}

	public void setWonRequiresDrawing(String wonRequiresDrawing) {
		this.wonRequiresDrawing = wonRequiresDrawing;
	}

	public String getWonRequiresDatasheet() {
		return CommonUtility.replaceNull(wonRequiresDatasheet);
	}

	public void setWonRequiresDatasheet(String wonRequiresDatasheet) {
		this.wonRequiresDatasheet = wonRequiresDatasheet;
	}

	public String getWonRequiresQAP() {
		return CommonUtility.replaceNull(wonRequiresQAP);
	}

	public void setWonRequiresQAP(String wonRequiresQAP) {
		this.wonRequiresQAP = wonRequiresQAP;
	}

	public String getWonRequiresPerfCurves() {
		return CommonUtility.replaceNull(wonRequiresPerfCurves);
	}

	public void setWonRequiresPerfCurves(String wonRequiresPerfCurves) {
		this.wonRequiresPerfCurves = wonRequiresPerfCurves;
	}

	public ArrayList<String> getAlPDFProdLinesList() {
		return alPDFProdLinesList;
	}

	public void setAlPDFProdLinesList(ArrayList<String> alPDFProdLinesList) {
		this.alPDFProdLinesList = alPDFProdLinesList;
	}

	public String getIsNotifierEmailSent() {
		return CommonUtility.replaceNull(isNotifierEmailSent);
	}

	public void setIsNotifierEmailSent(String isNotifierEmailSent) {
		this.isNotifierEmailSent = isNotifierEmailSent;
	}

	public String getNotifierEmailSentToSSO() {
		return CommonUtility.replaceNull(notifierEmailSentToSSO);
	}

	public void setNotifierEmailSentToSSO(String notifierEmailSentToSSO) {
		this.notifierEmailSentToSSO = notifierEmailSentToSSO;
	}

	public String getNotifierEmailSentDate() {
		return CommonUtility.replaceNull(notifierEmailSentDate);
	}

	public void setNotifierEmailSentDate(String notifierEmailSentDate) {
		this.notifierEmailSentDate = notifierEmailSentDate;
	}

	public String getWorkflowAssignedTo() {
		return CommonUtility.replaceNull(workflowAssignedTo);
	}

	public void setWorkflowAssignedTo(String workflowAssignedTo) {
		this.workflowAssignedTo = workflowAssignedTo;
	}

	public String getWorkflowAssignedDate() {
		return CommonUtility.replaceNull(workflowAssignedDate);
	}

	public void setWorkflowAssignedDate(String workflowAssignedDate) {
		this.workflowAssignedDate = workflowAssignedDate;
	}

	public String getNotifierType() {
		return CommonUtility.replaceNull(notifierType);
	}

	public void setNotifierType(String notifierType) {
		this.notifierType = notifierType;
	}

	public String getCustomerDealerLink() {
		return CommonUtility.replaceNull(customerDealerLink);
	}

	public void setCustomerDealerLink(String customerDealerLink) {
		this.customerDealerLink = customerDealerLink;
	}

	public String getCummulativeTotalCost() {
		return CommonUtility.replaceNull(cummulativeTotalCost);
	}

	public void setCummulativeTotalCost(String cummulativeTotalCost) {
		this.cummulativeTotalCost = cummulativeTotalCost;
	}

	public String getCummulativeProfitMargin() {
		return CommonUtility.replaceNull(cummulativeProfitMargin);
	}

	public void setCummulativeProfitMargin(String cummulativeProfitMargin) {
		this.cummulativeProfitMargin = cummulativeProfitMargin;
	}
	
	
	
}
