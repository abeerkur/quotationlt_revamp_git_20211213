/**
 * ******************************************************************************************
 * Project Name: Request For Quotation
 * Document Name: NewEnquiryForm.java
 * Package: in.com.rbc.quotation.enquiry.form
 * Desc:  Form class that holds getters and setters of each property.
 * ******************************************************************************************
 */
package in.com.rbc.quotation.enquiry.form;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

import in.com.rbc.quotation.common.utility.CommonUtility;
import in.com.rbc.quotation.common.vo.KeyValueVo;
import in.com.rbc.quotation.enquiry.dto.NewRatingDto;

public class NewEnquiryForm extends ActionForm {
	
	private String dispatch; // This variable is for diffrentiating between create and edit
	private String operationType; // This variable is to check whether INSERT OR UPDATE operation has to be done for enquiry 
	private String enquiryOperationType; // This variable is to diffrentiate between the button actions Manager or engineer
	private String ratingOperationType; // This variable is to check whether INSERT OR UPDATE operation has to be done for rating
	private String ratingsValidationMessage; // This variable will have the validation message - when submit button in clicked we display an error message if all the ratings are not validated
	private String currentRating; // This variable will be used to display the current tab in bold
	private String commentOperation;
	private String reviseUserType;
	private String ratingOperationId; // This variable is used for storing the ratingId that has to fetched for editing - Design Office
	
	private String isLast; // This variable is to check whether the current rating is the last rating or not in edit page
	private String nextRatingId; // This variable will store the next rating id
	private String isMaxRating; // Based on this variable value "Add New rating button will be displayed" 
	
	private int enquiryId;
	private int revisionNumber; 
	private String enquiryNumber;
	private int statusId;
	private String statusName;
	private String createdBy;
	private String createdBySSO;
	private String createdDate;
	private String lastModifiedBy;
	private String lastModifiedDate;
	
	private FormFile theFile1 = null;
	private String fileDescription1;	
	private FormFile theFile2 = null;
	private String fileDescription2;
	private FormFile theFile3 = null;
	private String fileDescription3;
	private FormFile theFile4 = null;
	private String fileDescription4;
	private FormFile theFile5 = null;
	private String fileDescription5;
	private FormFile theFile6 = null;
	private String fileDescription6;
	private FormFile theFile7 = null;
	private String fileDescription7;
	private FormFile theFile8 = null;
	private String fileDescription8;
	private FormFile theFile9 = null;
	private String fileDescription9;
	private FormFile theFile10 = null;
	private String fileDescription10;
	private String fileTypesStr;
	
	// --------------------    Enquiry Information	-------------------- 
	private String customerEnqReference;
	private int customerId;
	private String customerName;
	private String customerType;
	private int locationId;
	private String location;
	private String customerIndustry;
	private String customerIndustryId;
	private String concernedPerson;
	private String concernedPersonHdn;
	private String projectName;
	private String is_marathon_approved;
	private String consultantName;
	private String endUser;
	private String endUserIndustry;
	private String locationOfInstallation;
	private String locationOfInstallationId;
	private String customerDealerLink;
	// --------------------    Enquiry Waitage	-------------------- 
	private String enquiryType;
	private String enquiryStage;
	private String winningChance;
	private String targeted;
	// --------------------    Important Dates	--------------------
	private String receiptDate;
	private String entryDate;
	private String submissionDate;
	private String actualSubmissionDate;
	private String closingDate;
	
	// --------------------    Commercial Terms and Conditions ----------
	private String deliveryTerm;
	
	private String pt1PercentAmtNetorderValue;
	private String pt1PaymentDays;
	private String pt1PayableTerms;
	private String pt1Instrument;
	private String pt2PercentAmtNetorderValue;
	private String pt2PaymentDays;
	private String pt2PayableTerms;
	private String pt2Instrument;
	private String pt3PercentAmtNetorderValue;
	private String pt3PaymentDays;
	private String pt3PayableTerms;
	private String pt3Instrument;
	private String pt4PercentAmtNetorderValue;
	private String pt4PaymentDays;
	private String pt4PayableTerms;
	private String pt4Instrument;
	private String pt5PercentAmtNetorderValue;
	private String pt5PaymentDays;
	private String pt5PayableTerms;
	private String pt5Instrument;
	
	private String warrantyDispatchDate;
	private String warrantyCommissioningDate;
	private String custRequestedDeliveryTime;
	private String orderCompletionLeadTime;
	private String deliveryInFull;
	private String gstValue;
	private String packaging;
	private String liquidatedDamagesDueToDeliveryDelay;
	private String supervisionDetails;
	private String superviseNoOfManDays;
	private String superviseToFroTravel;
	private String superviseLodging;
	private String offerValidity;
	private String priceValidity;
	private String ldApproveStatus;
	
	private FormFile commPurchaseSpecFile1 = null;
	private String commPurchaseSpecFileDesc1;
	
	
	// --------------------    Rating properties	----------
	private int ratingId;
	private String ratingTitle;
	private int ratingNo;
	private int quantity = 0;
	
	private ArrayList<NewRatingDto> newRatingsList = new ArrayList<NewRatingDto>();
	
	private List<KeyValueVo> salesReassignUsersList = new ArrayList<>();
	
	// --------------------    Array List objects --------------------    
	private ArrayList customerList = new ArrayList();
	private ArrayList locationList = new ArrayList();
	private ArrayList htltList = new ArrayList();
	
	// --------------------    Quotation LT : Rating PickLists --------------------    
	private List<KeyValueVo> ltProductGroupList = new ArrayList<>();
	private List<KeyValueVo> ltProductLineList = new ArrayList<>();
	private List<KeyValueVo> ltMotorTypeList = new ArrayList<>();
	private List<KeyValueVo> ltKWList = new ArrayList<>();
	private List<KeyValueVo> ltPoleList = new ArrayList<>();
	private List<KeyValueVo> ltFrameList = new ArrayList<>();
	private List<KeyValueVo> ltFrameSuffixList = new ArrayList<>();
	private List<KeyValueVo> ltMountingList = new ArrayList<>();
	private List<KeyValueVo> ltTBPositionList = new ArrayList<>();
	private List<KeyValueVo> ltAmbientTemperatureList = new ArrayList<>();
	private List<KeyValueVo> ltAmbientRemTempList = new ArrayList<>();
	private List<KeyValueVo> ltGasGroupList = new ArrayList<>();
	private List<KeyValueVo> ltHazardZoneList = new ArrayList<>();
	private List<KeyValueVo> ltDustGroupList = new ArrayList<>();
	private List<KeyValueVo> ltApplicationList = new ArrayList<>();
	private List<KeyValueVo> ltVoltageList = new ArrayList<>();
	private List<KeyValueVo> ltFrequencyList = new ArrayList<>();
	private List<KeyValueVo> ltCombinedVariationList = new ArrayList<>();
	private List<KeyValueVo> ltDutyList = new ArrayList<>();
	private List<KeyValueVo> ltCDFList = new ArrayList<>();
	private List<KeyValueVo> ltStartsList = new ArrayList<>();
	private List<KeyValueVo> ltRVRAList = new ArrayList<>();
	private List<KeyValueVo> ltWindingTreatmentList = new ArrayList<>();
	private List<KeyValueVo> ltLeadList = new ArrayList<>();
	private List<KeyValueVo> ltVFDTypeList = new ArrayList<>();
	private List<KeyValueVo> ltDualSpeedTypeList = new ArrayList<>();
	private List<KeyValueVo> ltShaftMaterialTypeList = new ArrayList<>();
	private List<KeyValueVo> ltPaintingTypeList = new ArrayList<>();
	private List<KeyValueVo> ltPaintShadeList = new ArrayList<>();
	private List<KeyValueVo> ltPaintThicknessList = new ArrayList<>();
	private List<KeyValueVo> ltIPList = new ArrayList<>();
	private List<KeyValueVo> ltInsulationClassList = new ArrayList<>();
	private List<KeyValueVo> ltTerminalBoxList = new ArrayList<>();
	private List<KeyValueVo> ltSpreaderBoxList = new ArrayList<>();
	private List<KeyValueVo> ltAuxTerminalBoxList = new ArrayList<>();
	private List<KeyValueVo> ltSpaceHeaterList = new ArrayList<>();
	private List<KeyValueVo> ltVibrationList = new ArrayList<>();
	private List<KeyValueVo> ltFlyingLeadList = new ArrayList<>();
	private List<KeyValueVo> ltMetalFanList = new ArrayList<>();
	private List<KeyValueVo> ltShaftGroundingList = new ArrayList<>();
	private List<KeyValueVo> ltForcedCoolingList = new ArrayList<>();
	private List<KeyValueVo> ltHardwareList = new ArrayList<>();
	private List<KeyValueVo> ltGlandPlateList = new ArrayList<>();
	private List<KeyValueVo> ltDoubleCompressionGlandList = new ArrayList<>();
	private List<KeyValueVo> ltSPMMountingProvisionList = new ArrayList<>();
	private List<KeyValueVo> ltAddNamePlateList = new ArrayList<>();
	private List<KeyValueVo> ltArrowPlateDirectionList = new ArrayList<>();
	private List<KeyValueVo> ltRTDList = new ArrayList<>();
	private List<KeyValueVo> ltBTDList = new ArrayList<>();
	private List<KeyValueVo> ltThermisterList = new ArrayList<>();
	private List<KeyValueVo> ltCableSealingBoxList = new ArrayList<>();
	private List<KeyValueVo> ltBearingSystemList = new ArrayList<>();
	private List<KeyValueVo> ltBearingNDEList = new ArrayList<>();
	private List<KeyValueVo> ltBearingDEList = new ArrayList<>();
	private List<KeyValueVo> ltWitnessRoutineList = new ArrayList<>();
	private List<KeyValueVo> ltAdditionalTestList = new ArrayList<>();
	private List<KeyValueVo> ltTypeTestList = new ArrayList<>();
	private List<KeyValueVo> ltULCEList = new ArrayList<>();
	private List<KeyValueVo> ltQAPList = new ArrayList<>();
	private List<KeyValueVo> ltSeaworthyPackingList = new ArrayList<>();
	private List<KeyValueVo> ltWarrantyList = new ArrayList<>();
	private List<KeyValueVo> ltVoltVariationList = new ArrayList<>();
	private List<KeyValueVo> ltFreqVariationList = new ArrayList<>();
	private List<KeyValueVo> ltStartingDOLCurrentList = new ArrayList<>();
	private List<KeyValueVo> ltWindingConfigurationList = new ArrayList<>();
	private List<KeyValueVo> ltMethodOfStartingList = new ArrayList<>();
	private List<KeyValueVo> ltWindingWireList = new ArrayList<>();
	private List<KeyValueVo> ltVFDSpeedRangeMinList = new ArrayList<>();
	private List<KeyValueVo> ltVFDSpeedRangeMaxList = new ArrayList<>();
	private List<KeyValueVo> ltOverloadingDutyList = new ArrayList<>();
	private List<KeyValueVo> ltConstantEffRangeList = new ArrayList<>();
	private List<KeyValueVo> ltShaftTypeList = new ArrayList<>();
	private List<KeyValueVo> ltDirectionOfRotationList = new ArrayList<>();
	private List<KeyValueVo> ltMethodOfCouplingList = new ArrayList<>();
	private List<KeyValueVo> ltTechoMountingList = new ArrayList<>();
	private List<KeyValueVo> ltDataSheetList = new ArrayList<>();
	private List<KeyValueVo> ltNonStandardVoltageList = new ArrayList<>();
	private List<KeyValueVo> ltHazAreaList = new ArrayList<>();
	private List<KeyValueVo> ltHazAreaTypeList = new ArrayList<>();
	private List<KeyValueVo> ltMfgLocationList = new ArrayList<>();
	private List<KeyValueVo> ltServiceFactorList = new ArrayList<>();
	private List<KeyValueVo> ltStandardRotationList = new ArrayList<>();
	private List<KeyValueVo> ltTemperatureRiseList = new ArrayList<>();
	private List<KeyValueVo> ltNoiseLevelList = new ArrayList<>();
	private List<KeyValueVo> ltPerfCurvesList = new ArrayList<>();
	private List<KeyValueVo> ltCableSizeList = new ArrayList<>();
	private List<KeyValueVo> ltNoOfRunsList = new ArrayList<>();
	private List<KeyValueVo> ltNoOfCoresList = new ArrayList<>();
	private List<KeyValueVo> ltCrossSectionAreaList = new ArrayList<>();
	private List<KeyValueVo> ltConductorMaterialList = new ArrayList<>();
	private List<KeyValueVo> ltTemperatureClassList = new ArrayList<>();
	private List<KeyValueVo> ltEfficiencyClassList = new ArrayList<>();
	private List<KeyValueVo> ltTypeOfGreaseList = new ArrayList<>();
	private List<KeyValueVo> ltSparesList = new ArrayList<>();
	private List<KeyValueVo> ltMotorGAList = new ArrayList<>();
	private List<KeyValueVo> ltTBoxGAList = new ArrayList<>();
	
	private ArrayList alApprovalList = new ArrayList();
	private ArrayList alTargetedList = new ArrayList();
	private ArrayList alIndustryList = new ArrayList();
	private ArrayList alEnquiryTypeList = new ArrayList();
	private ArrayList alWinChanceList = new ArrayList();
	private ArrayList alInstallLocationList = new ArrayList();
	private ArrayList alVoltList = new ArrayList();
	private ArrayList alMainTermBoxPSList = new ArrayList();
	private ArrayList alNeutralTBList = new ArrayList();
	private ArrayList alCableEntryList = new ArrayList();
	private ArrayList alBoTerminalsNoList = new ArrayList();
	private ArrayList alLubricationList = new ArrayList();
	private ArrayList alnoiseLevelList = new ArrayList();
	private ArrayList alMinStartVoltList = new ArrayList();
	private ArrayList alBalancingList = new ArrayList();
	private ArrayList alBearingThermoDtList = new ArrayList();
	private ArrayList degproList = new ArrayList();
	private ArrayList termBoxList = new ArrayList();
	private ArrayList enclosureList = new ArrayList();
	
	private ArrayList addOnList = new ArrayList();
	private ArrayList teamMemberList = new ArrayList();
	private ArrayList directManagersList = new ArrayList();
	
	private int totalRatingIndex = 0;

	private String returnstatement;
	private String returnstatus;
	private String dmToSmReturn;
	private String deToSmReturn;
	
	// --------------------    Quotation LT : Commercial Terms and Conditions PickLists --------------------    
	private List<KeyValueVo> tcDeliveryTypeList = new ArrayList<>();
	private List<KeyValueVo> tcPaymentTermsList = new ArrayList<>();
	private List<KeyValueVo> tcPercentNetOrderValueList = new ArrayList<>();
	private List<KeyValueVo> tcPaymentDaysList = new ArrayList<>();
	private List<KeyValueVo> tcPayableTermsList = new ArrayList<>();
	private List<KeyValueVo> tcInstrumentList = new ArrayList<>();
	private List<KeyValueVo> tcWarrantyDaysList = new ArrayList<>();
	private List<KeyValueVo> tcOrderCompleteLeadTimeList = new ArrayList<>();
	private List<KeyValueVo> tcGstValuesList = new ArrayList<>();
	private List<KeyValueVo> tcPackagingList = new ArrayList<>();
	private List<KeyValueVo> tcLiquidatedDamagesList = new ArrayList<>();
	private List<KeyValueVo> tcDeliveryLotList = new ArrayList<>();
	private List<KeyValueVo> tcDeliveryDamagesList = new ArrayList<>();
	private List<KeyValueVo> tcOfferValidityList = new ArrayList<>();
	private List<KeyValueVo> tcPriceValidityList = new ArrayList<>();
	
	private String isNewEnquiry;
	private String selectedRemoveRatingId;
	private String enquiryReferenceNumber;
	private String description;
	
	// Workflow Related
	private String assignToId;
	private String assignToName;
	private String assignRemarks;	
	private String returnTo;
	private String returnToRemarks;
	private String showReAssignSectionFlag;
	
	private String pagechecking;
	
	private String smNote;
	private String sm_name;
	private String smEmail;
	private String smContactNo;
	
	private String rsmNote;
	private String nsmNote;
	private String mhNote;
	private String designNote;
	private String dmUpdateStatus;
	private String bhNote;
	private String mfgNote;
	private String qaNote;
	private String scmNote;
	
	private String deptAndAuth;
	private String scsrName;
	private String ratedOutputUnit;
	
	private String flcAmps;
	private String isReferDesign;
	private String commentsDeviations;
	private String reassignRemarks;
	
	// Won / Lost / Abondent - Required Fields.
	private String abondentReason1;
	private String abondentReason2;
	private String abondentRemarks;
	private String abondentCustomerApproval;
	
	private String lostCompetitor;
	private String lostReason1;
	private String lostReason2;
	private String lostRemarks;
	private String lostManagerApproval;
	private String totalQuotedPrice;
	private String totalLostPrice;
	
	private String wonIndentNo;
	private String wonIsPOorLOI;
	private String wonPONo;
	private String wonPODate;
	private String wonPOReceiptDate;
	private FormFile wonPOCopy;
	private String wonReason1;
	private String wonReason2;
	private String wonRemarks;
	private String totalWonPrice;
	private String wonRequiresDrawing;
	private String wonRequiresDatasheet;
	private String wonRequiresQAP;
	private String wonRequiresPerfCurves;
	
	// MC - NSP
	private String totalCost;
	private String totalProfitMargin;
	
	private List<KeyValueVo> ltAbondentReasonList = new ArrayList<>();
	private List<KeyValueVo> ltLostCompetitorList = new ArrayList<>();
	private List<KeyValueVo> ltLostReasonList = new ArrayList<>();
	private List<KeyValueVo> ltWonPOOptionsList = new ArrayList<>();
	private List<KeyValueVo> ltWonReasonList = new ArrayList<>();
	
	public String getDispatch() {
		return CommonUtility.replaceNull(dispatch);
	}

	public void setDispatch(String dispatch) {
		this.dispatch = dispatch;
	}

	public String getOperationType() {
		return CommonUtility.replaceNull(operationType);
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	public String getEnquiryOperationType() {
		return CommonUtility.replaceNull(enquiryOperationType);
	}

	public void setEnquiryOperationType(String enquiryOperationType) {
		this.enquiryOperationType = enquiryOperationType;
	}
	
	public String getRatingOperationType() {
		return CommonUtility.replaceNull(ratingOperationType);
	}

	public void setRatingOperationType(String ratingOperationType) {
		this.ratingOperationType = ratingOperationType;
	}

	public String getRatingsValidationMessage() {
		return CommonUtility.replaceNull(ratingsValidationMessage);
	}

	public void setRatingsValidationMessage(String ratingsValidationMessage) {
		this.ratingsValidationMessage = ratingsValidationMessage;
	}

	public String getCurrentRating() {
		return CommonUtility.replaceNull(currentRating);
	}

	public void setCurrentRating(String currentRating) {
		this.currentRating = currentRating;
	}

	public String getCommentOperation() {
		return CommonUtility.replaceNull(commentOperation);
	}

	public void setCommentOperation(String commentOperation) {
		this.commentOperation = commentOperation;
	}

	public String getReviseUserType() {
		return CommonUtility.replaceNull(reviseUserType);
	}

	public void setReviseUserType(String reviseUserType) {
		this.reviseUserType = reviseUserType;
	}

	public String getIsLast() {
		return CommonUtility.replaceNull(isLast);
	}

	public void setIsLast(String isLast) {
		this.isLast = isLast;
	}

	public String getNextRatingId() {
		return CommonUtility.replaceNull(nextRatingId);
	}

	public void setNextRatingId(String nextRatingId) {
		this.nextRatingId = nextRatingId;
	}

	public String getIsMaxRating() {
		return CommonUtility.replaceNull(isMaxRating);
	}

	public void setIsMaxRating(String isMaxRating) {
		this.isMaxRating = isMaxRating;
	}

	public int getEnquiryId() {
		return enquiryId;
	}

	public void setEnquiryId(int enquiryId) {
		this.enquiryId = enquiryId;
	}

	public int getRevisionNumber() {
		return revisionNumber;
	}

	public void setRevisionNumber(int revisionNumber) {
		this.revisionNumber = revisionNumber;
	}

	public String getEnquiryNumber() {
		return CommonUtility.replaceNull(enquiryNumber);
	}

	public void setEnquiryNumber(String enquiryNumber) {
		this.enquiryNumber = enquiryNumber;
	}

	public int getStatusId() {
		return statusId;
	}

	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}

	public String getStatusName() {
		return CommonUtility.replaceNull(statusName);
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public String getCreatedBy() {
		return CommonUtility.replaceNull(createdBy);
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedBySSO() {
		return CommonUtility.replaceNull(createdBySSO);
	}

	public void setCreatedBySSO(String createdBySSO) {
		this.createdBySSO = createdBySSO;
	}

	public String getCreatedDate() {
		return CommonUtility.replaceNull(createdDate);
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getLastModifiedBy() {
		return CommonUtility.replaceNull(lastModifiedBy);
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public String getLastModifiedDate() {
		return CommonUtility.replaceNull(lastModifiedDate);
	}

	public void setLastModifiedDate(String lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public FormFile getTheFile1() {
		return theFile1;
	}

	public void setTheFile1(FormFile theFile1) {
		this.theFile1 = theFile1;
	}

	public String getFileDescription1() {
		return CommonUtility.replaceNull(fileDescription1);
	}

	public void setFileDescription1(String fileDescription1) {
		this.fileDescription1 = fileDescription1;
	}

	public FormFile getTheFile2() {
		return theFile2;
	}

	public void setTheFile2(FormFile theFile2) {
		this.theFile2 = theFile2;
	}

	public String getFileDescription2() {
		return CommonUtility.replaceNull(fileDescription2);
	}

	public void setFileDescription2(String fileDescription2) {
		this.fileDescription2 = fileDescription2;
	}

	public FormFile getTheFile3() {
		return theFile3;
	}

	public void setTheFile3(FormFile theFile3) {
		this.theFile3 = theFile3;
	}

	public String getFileDescription3() {
		return CommonUtility.replaceNull(fileDescription3);
	}

	public void setFileDescription3(String fileDescription3) {
		this.fileDescription3 = fileDescription3;
	}

	public FormFile getTheFile4() {
		return theFile4;
	}

	public void setTheFile4(FormFile theFile4) {
		this.theFile4 = theFile4;
	}

	public String getFileDescription4() {
		return CommonUtility.replaceNull(fileDescription4);
	}

	public void setFileDescription4(String fileDescription4) {
		this.fileDescription4 = fileDescription4;
	}

	public FormFile getTheFile5() {
		return theFile5;
	}

	public void setTheFile5(FormFile theFile5) {
		this.theFile5 = theFile5;
	}

	public String getFileDescription5() {
		return CommonUtility.replaceNull(fileDescription5);
	}

	public void setFileDescription5(String fileDescription5) {
		this.fileDescription5 = fileDescription5;
	}

	public FormFile getTheFile6() {
		return theFile6;
	}

	public void setTheFile6(FormFile theFile6) {
		this.theFile6 = theFile6;
	}

	public String getFileDescription6() {
		return CommonUtility.replaceNull(fileDescription6);
	}

	public void setFileDescription6(String fileDescription6) {
		this.fileDescription6 = fileDescription6;
	}

	public FormFile getTheFile7() {
		return theFile7;
	}

	public void setTheFile7(FormFile theFile7) {
		this.theFile7 = theFile7;
	}

	public String getFileDescription7() {
		return CommonUtility.replaceNull(fileDescription7);
	}

	public void setFileDescription7(String fileDescription7) {
		this.fileDescription7 = fileDescription7;
	}

	public FormFile getTheFile8() {
		return theFile8;
	}

	public void setTheFile8(FormFile theFile8) {
		this.theFile8 = theFile8;
	}

	public String getFileDescription8() {
		return CommonUtility.replaceNull(fileDescription8);
	}

	public void setFileDescription8(String fileDescription8) {
		this.fileDescription8 = fileDescription8;
	}

	public FormFile getTheFile9() {
		return theFile9;
	}

	public void setTheFile9(FormFile theFile9) {
		this.theFile9 = theFile9;
	}

	public String getFileDescription9() {
		return CommonUtility.replaceNull(fileDescription9);
	}

	public void setFileDescription9(String fileDescription9) {
		this.fileDescription9 = fileDescription9;
	}

	public FormFile getTheFile10() {
		return theFile10;
	}

	public void setTheFile10(FormFile theFile10) {
		this.theFile10 = theFile10;
	}

	public String getFileDescription10() {
		return CommonUtility.replaceNull(fileDescription10);
	}

	public void setFileDescription10(String fileDescription10) {
		this.fileDescription10 = fileDescription10;
	}

	public String getFileTypesStr() {
		return CommonUtility.replaceNull(fileTypesStr);
	}
	
	public void setFileTypesStr(String fileTypesStr) {
		this.fileTypesStr = fileTypesStr;
	}

	public String getCustomerEnqReference() {
		return CommonUtility.replaceNull(customerEnqReference);
	}

	public void setCustomerEnqReference(String customerEnqReference) {
		this.customerEnqReference = customerEnqReference;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return CommonUtility.replaceNull(customerName);
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	
	public String getCustomerType() {
		return CommonUtility.replaceNull(customerType);
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public int getLocationId() {
		return locationId;
	}

	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}

	public String getLocation() {
		return CommonUtility.replaceNull(location);
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getCustomerIndustry() {
		return CommonUtility.replaceNull(customerIndustry);
	}

	public void setCustomerIndustry(String customerIndustry) {
		this.customerIndustry = customerIndustry;
	}
	
	public String getCustomerIndustryId() {
		return CommonUtility.replaceNull(customerIndustryId);
	}

	public void setCustomerIndustryId(String customerIndustryId) {
		this.customerIndustryId = customerIndustryId;
	}

	public String getConcernedPerson() {
		return CommonUtility.replaceNull(concernedPerson);
	}

	public void setConcernedPerson(String concernedPerson) {
		this.concernedPerson = concernedPerson;
	}

	public String getConcernedPersonHdn() {
		return CommonUtility.replaceNull(concernedPersonHdn);
	}

	public void setConcernedPersonHdn(String concernedPersonHdn) {
		this.concernedPersonHdn = concernedPersonHdn;
	}

	public String getProjectName() {
		return CommonUtility.replaceNull(projectName);
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getIs_marathon_approved() {
		return CommonUtility.replaceNull(is_marathon_approved);
	}

	public void setIs_marathon_approved(String is_marathon_approved) {
		this.is_marathon_approved = is_marathon_approved;
	}

	public String getConsultantName() {
		return CommonUtility.replaceNull(consultantName);
	}

	public void setConsultantName(String consultantName) {
		this.consultantName = consultantName;
	}

	public String getEndUser() {
		return CommonUtility.replaceNull(endUser);
	}

	public void setEndUser(String endUser) {
		this.endUser = endUser;
	}

	public String getEndUserIndustry() {
		return CommonUtility.replaceNull(endUserIndustry);
	}

	public void setEndUserIndustry(String endUserIndustry) {
		this.endUserIndustry = endUserIndustry;
	}

	public String getLocationOfInstallation() {
		return CommonUtility.replaceNull(locationOfInstallation);
	}

	public void setLocationOfInstallation(String locationOfInstallation) {
		this.locationOfInstallation = locationOfInstallation;
	}
	
	public String getLocationOfInstallationId() {
		return CommonUtility.replaceNull(locationOfInstallationId);
	}

	public void setLocationOfInstallationId(String locationOfInstallationId) {
		this.locationOfInstallationId = locationOfInstallationId;
	}

	public String getEnquiryType() {
		return CommonUtility.replaceNull(enquiryType);
	}

	public void setEnquiryType(String enquiryType) {
		this.enquiryType = enquiryType;
	}

	public String getEnquiryStage() {
		return CommonUtility.replaceNull(enquiryStage);
	}

	public void setEnquiryStage(String enquiryStage) {
		this.enquiryStage = enquiryStage;
	}

	public String getWinningChance() {
		return CommonUtility.replaceNull(winningChance);
	}

	public void setWinningChance(String winningChance) {
		this.winningChance = winningChance;
	}

	public String getTargeted() {
		return CommonUtility.replaceNull(targeted);
	}

	public void setTargeted(String targeted) {
		this.targeted = targeted;
	}

	public String getReceiptDate() {
		return CommonUtility.replaceNull(receiptDate);
	}

	public void setReceiptDate(String receiptDate) {
		this.receiptDate = receiptDate;
	}

	public String getEntryDate() {
		return CommonUtility.replaceNull(entryDate);
	}

	public void setEntryDate(String entryDate) {
		this.entryDate = entryDate;
	}

	public String getSubmissionDate() {
		return CommonUtility.replaceNull(submissionDate);
	}

	public void setSubmissionDate(String submissionDate) {
		this.submissionDate = submissionDate;
	}

	public String getActualSubmissionDate() {
		return CommonUtility.replaceNull(actualSubmissionDate);
	}

	public void setActualSubmissionDate(String actualSubmissionDate) {
		this.actualSubmissionDate = actualSubmissionDate;
	}

	public String getClosingDate() {
		return CommonUtility.replaceNull(closingDate);
	}

	public void setClosingDate(String closingDate) {
		this.closingDate = closingDate;
	}

	public String getDeliveryTerm() {
		return CommonUtility.replaceNull(deliveryTerm);
	}

	public void setDeliveryTerm(String deliveryTerm) {
		this.deliveryTerm = deliveryTerm;
	}
	
	public String getPt1PercentAmtNetorderValue() {
		return CommonUtility.replaceNull(pt1PercentAmtNetorderValue);
	}

	public void setPt1PercentAmtNetorderValue(String pt1PercentAmtNetorderValue) {
		this.pt1PercentAmtNetorderValue = pt1PercentAmtNetorderValue;
	}

	public String getPt1PaymentDays() {
		return CommonUtility.replaceNull(pt1PaymentDays);
	}

	public void setPt1PaymentDays(String pt1PaymentDays) {
		this.pt1PaymentDays = pt1PaymentDays;
	}

	public String getPt1PayableTerms() {
		return CommonUtility.replaceNull(pt1PayableTerms);
	}

	public void setPt1PayableTerms(String pt1PayableTerms) {
		this.pt1PayableTerms = pt1PayableTerms;
	}

	public String getPt1Instrument() {
		return CommonUtility.replaceNull(pt1Instrument);
	}

	public void setPt1Instrument(String pt1Instrument) {
		this.pt1Instrument = pt1Instrument;
	}

	public String getPt2PercentAmtNetorderValue() {
		return CommonUtility.replaceNull(pt2PercentAmtNetorderValue);
	}

	public void setPt2PercentAmtNetorderValue(String pt2PercentAmtNetorderValue) {
		this.pt2PercentAmtNetorderValue = pt2PercentAmtNetorderValue;
	}

	public String getPt2PaymentDays() {
		return CommonUtility.replaceNull(pt2PaymentDays);
	}

	public void setPt2PaymentDays(String pt2PaymentDays) {
		this.pt2PaymentDays = pt2PaymentDays;
	}

	public String getPt2PayableTerms() {
		return CommonUtility.replaceNull(pt2PayableTerms);
	}

	public void setPt2PayableTerms(String pt2PayableTerms) {
		this.pt2PayableTerms = pt2PayableTerms;
	}

	public String getPt2Instrument() {
		return CommonUtility.replaceNull(pt2Instrument);
	}

	public void setPt2Instrument(String pt2Instrument) {
		this.pt2Instrument = pt2Instrument;
	}

	public String getPt3PercentAmtNetorderValue() {
		return CommonUtility.replaceNull(pt3PercentAmtNetorderValue);
	}

	public void setPt3PercentAmtNetorderValue(String pt3PercentAmtNetorderValue) {
		this.pt3PercentAmtNetorderValue = pt3PercentAmtNetorderValue;
	}

	public String getPt3PaymentDays() {
		return CommonUtility.replaceNull(pt3PaymentDays);
	}

	public void setPt3PaymentDays(String pt3PaymentDays) {
		this.pt3PaymentDays = pt3PaymentDays;
	}

	public String getPt3PayableTerms() {
		return CommonUtility.replaceNull(pt3PayableTerms);
	}

	public void setPt3PayableTerms(String pt3PayableTerms) {
		this.pt3PayableTerms = pt3PayableTerms;
	}

	public String getPt3Instrument() {
		return CommonUtility.replaceNull(pt3Instrument);
	}

	public void setPt3Instrument(String pt3Instrument) {
		this.pt3Instrument = pt3Instrument;
	}

	public String getPt4PercentAmtNetorderValue() {
		return CommonUtility.replaceNull(pt4PercentAmtNetorderValue);
	}

	public void setPt4PercentAmtNetorderValue(String pt4PercentAmtNetorderValue) {
		this.pt4PercentAmtNetorderValue = pt4PercentAmtNetorderValue;
	}

	public String getPt4PaymentDays() {
		return CommonUtility.replaceNull(pt4PaymentDays);
	}

	public void setPt4PaymentDays(String pt4PaymentDays) {
		this.pt4PaymentDays = pt4PaymentDays;
	}

	public String getPt4PayableTerms() {
		return CommonUtility.replaceNull(pt4PayableTerms);
	}

	public void setPt4PayableTerms(String pt4PayableTerms) {
		this.pt4PayableTerms = pt4PayableTerms;
	}

	public String getPt4Instrument() {
		return CommonUtility.replaceNull(pt4Instrument);
	}

	public void setPt4Instrument(String pt4Instrument) {
		this.pt4Instrument = pt4Instrument;
	}

	public String getPt5PercentAmtNetorderValue() {
		return CommonUtility.replaceNull(pt5PercentAmtNetorderValue);
	}

	public void setPt5PercentAmtNetorderValue(String pt5PercentAmtNetorderValue) {
		this.pt5PercentAmtNetorderValue = pt5PercentAmtNetorderValue;
	}

	public String getPt5PaymentDays() {
		return CommonUtility.replaceNull(pt5PaymentDays);
	}

	public void setPt5PaymentDays(String pt5PaymentDays) {
		this.pt5PaymentDays = pt5PaymentDays;
	}

	public String getPt5PayableTerms() {
		return CommonUtility.replaceNull(pt5PayableTerms);
	}

	public void setPt5PayableTerms(String pt5PayableTerms) {
		this.pt5PayableTerms = pt5PayableTerms;
	}

	public String getPt5Instrument() {
		return CommonUtility.replaceNull(pt5Instrument);
	}

	public void setPt5Instrument(String pt5Instrument) {
		this.pt5Instrument = pt5Instrument;
	}

	public String getWarrantyDispatchDate() {
		return CommonUtility.replaceNull(warrantyDispatchDate);
	}

	public void setWarrantyDispatchDate(String warrantyDispatchDate) {
		this.warrantyDispatchDate = warrantyDispatchDate;
	}

	public String getWarrantyCommissioningDate() {
		return CommonUtility.replaceNull(warrantyCommissioningDate);
	}

	public void setWarrantyCommissioningDate(String warrantyCommissioningDate) {
		this.warrantyCommissioningDate = warrantyCommissioningDate;
	}

	public String getCustRequestedDeliveryTime() {
		return CommonUtility.replaceNull(custRequestedDeliveryTime);
	}

	public void setCustRequestedDeliveryTime(String custRequestedDeliveryTime) {
		this.custRequestedDeliveryTime = custRequestedDeliveryTime;
	}

	public String getOrderCompletionLeadTime() {
		return CommonUtility.replaceNull(orderCompletionLeadTime);
	}

	public void setOrderCompletionLeadTime(String orderCompletionLeadTime) {
		this.orderCompletionLeadTime = orderCompletionLeadTime;
	}

	public String getDeliveryInFull() {
		return CommonUtility.replaceNull(deliveryInFull);
	}

	public void setDeliveryInFull(String deliveryInFull) {
		this.deliveryInFull = deliveryInFull;
	}

	public String getGstValue() {
		return CommonUtility.replaceNull(gstValue);
	}

	public void setGstValue(String gstValue) {
		this.gstValue = gstValue;
	}

	public String getPackaging() {
		return CommonUtility.replaceNull(packaging);
	}

	public void setPackaging(String packaging) {
		this.packaging = packaging;
	}

	public String getLiquidatedDamagesDueToDeliveryDelay() {
		return CommonUtility.replaceNull(liquidatedDamagesDueToDeliveryDelay);
	}

	public void setLiquidatedDamagesDueToDeliveryDelay(String liquidatedDamagesDueToDeliveryDelay) {
		this.liquidatedDamagesDueToDeliveryDelay = liquidatedDamagesDueToDeliveryDelay;
	}

	public String getSupervisionDetails() {
		return CommonUtility.replaceNull(supervisionDetails);
	}

	public void setSupervisionDetails(String supervisionDetails) {
		this.supervisionDetails = supervisionDetails;
	}
	
	public String getSuperviseNoOfManDays() {
		return CommonUtility.replaceNull(superviseNoOfManDays);
	}

	public void setSuperviseNoOfManDays(String superviseNoOfManDays) {
		this.superviseNoOfManDays = superviseNoOfManDays;
	}

	public String getSuperviseToFroTravel() {
		return CommonUtility.replaceNull(superviseToFroTravel);
	}

	public void setSuperviseToFroTravel(String superviseToFroTravel) {
		this.superviseToFroTravel = superviseToFroTravel;
	}

	public String getSuperviseLodging() {
		return CommonUtility.replaceNull(superviseLodging);
	}

	public void setSuperviseLodging(String superviseLodging) {
		this.superviseLodging = superviseLodging;
	}

	public String getOfferValidity() {
		return CommonUtility.replaceNull(offerValidity);
	}

	public void setOfferValidity(String offerValidity) {
		this.offerValidity = offerValidity;
	}

	public String getPriceValidity() {
		return CommonUtility.replaceNull(priceValidity);
	}

	public void setPriceValidity(String priceValidity) {
		this.priceValidity = priceValidity;
	}

	public String getLdApproveStatus() {
		return CommonUtility.replaceNull(ldApproveStatus);
	}

	public void setLdApproveStatus(String ldApproveStatus) {
		this.ldApproveStatus = ldApproveStatus;
	}

	// Commercial Purchase Spec : File Upload
	public FormFile getCommPurchaseSpecFile1() {
		return commPurchaseSpecFile1;
	}

	public void setCommPurchaseSpecFile1(FormFile commPurchaseSpecFile1) {
		this.commPurchaseSpecFile1 = commPurchaseSpecFile1;
	}

	public String getCommPurchaseSpecFileDesc1() {
		return CommonUtility.replaceNull(commPurchaseSpecFileDesc1);
	}

	public void setCommPurchaseSpecFileDesc1(String commPurchaseSpecFileDesc1) {
		this.commPurchaseSpecFileDesc1 = commPurchaseSpecFileDesc1;
	}

	public int getRatingId() {
		return ratingId;
	}

	public void setRatingId(int ratingId) {
		this.ratingId = ratingId;
	}

	public String getRatingTitle() {
		return CommonUtility.replaceNull(ratingTitle);
	}

	public void setRatingTitle(String ratingTitle) {
		this.ratingTitle = ratingTitle;
	}

	public int getRatingNo() {
		return ratingNo;
	}

	public void setRatingNo(int ratingNo) {
		this.ratingNo = ratingNo;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	// Quotationlt - newRatingsList
	public ArrayList<NewRatingDto> getNewRatingsList() {
		return newRatingsList;
	}

	public void setNewRatingsList(ArrayList<NewRatingDto> newRatingsList) {
		this.newRatingsList = newRatingsList;
	}

	public ArrayList getCustomerList() {
		return customerList;
	}

	public void setCustomerList(ArrayList customerList) {
		this.customerList = customerList;
	}

	public ArrayList getLocationList() {
		return locationList;
	}

	public void setLocationList(ArrayList locationList) {
		this.locationList = locationList;
	}

	public ArrayList getHtltList() {
		return htltList;
	}

	public void setHtltList(ArrayList htltList) {
		this.htltList = htltList;
	}

	public List<KeyValueVo> getLtProductGroupList() {
		return ltProductGroupList;
	}

	public void setLtProductGroupList(List<KeyValueVo> ltProductGroupList) {
		this.ltProductGroupList = ltProductGroupList;
	}

	public List<KeyValueVo> getLtProductLineList() {
		return ltProductLineList;
	}

	public void setLtProductLineList(List<KeyValueVo> ltProductLineList) {
		this.ltProductLineList = ltProductLineList;
	}

	public List<KeyValueVo> getLtMotorTypeList() {
		return ltMotorTypeList;
	}

	public void setLtMotorTypeList(List<KeyValueVo> ltMotorTypeList) {
		this.ltMotorTypeList = ltMotorTypeList;
	}

	public List<KeyValueVo> getLtKWList() {
		return ltKWList;
	}

	public void setLtKWList(List<KeyValueVo> ltKWList) {
		this.ltKWList = ltKWList;
	}

	public List<KeyValueVo> getLtPoleList() {
		return ltPoleList;
	}

	public void setLtPoleList(List<KeyValueVo> ltPoleList) {
		this.ltPoleList = ltPoleList;
	}

	public List<KeyValueVo> getLtFrameList() {
		return ltFrameList;
	}

	public void setLtFrameList(List<KeyValueVo> ltFrameList) {
		this.ltFrameList = ltFrameList;
	}

	public List<KeyValueVo> getLtFrameSuffixList() {
		return ltFrameSuffixList;
	}

	public void setLtFrameSuffixList(List<KeyValueVo> ltFrameSuffixList) {
		this.ltFrameSuffixList = ltFrameSuffixList;
	}

	public List<KeyValueVo> getLtMountingList() {
		return ltMountingList;
	}

	public void setLtMountingList(List<KeyValueVo> ltMountingList) {
		this.ltMountingList = ltMountingList;
	}

	public List<KeyValueVo> getLtTBPositionList() {
		return ltTBPositionList;
	}

	public void setLtTBPositionList(List<KeyValueVo> ltTBPositionList) {
		this.ltTBPositionList = ltTBPositionList;
	}

	public List<KeyValueVo> getLtAmbientTemperatureList() {
		return ltAmbientTemperatureList;
	}

	public void setLtAmbientTemperatureList(List<KeyValueVo> ltAmbientTemperatureList) {
		this.ltAmbientTemperatureList = ltAmbientTemperatureList;
	}
	
	public List<KeyValueVo> getLtAmbientRemTempList() {
		return ltAmbientRemTempList;
	}

	public void setLtAmbientRemTempList(List<KeyValueVo> ltAmbientRemTempList) {
		this.ltAmbientRemTempList = ltAmbientRemTempList;
	}

	public List<KeyValueVo> getLtGasGroupList() {
		return ltGasGroupList;
	}

	public void setLtGasGroupList(List<KeyValueVo> ltGasGroupList) {
		this.ltGasGroupList = ltGasGroupList;
	}

	public List<KeyValueVo> getLtHazardZoneList() {
		return ltHazardZoneList;
	}

	public void setLtHazardZoneList(List<KeyValueVo> ltHazardZoneList) {
		this.ltHazardZoneList = ltHazardZoneList;
	}

	public List<KeyValueVo> getLtDustGroupList() {
		return ltDustGroupList;
	}

	public void setLtDustGroupList(List<KeyValueVo> ltDustGroupList) {
		this.ltDustGroupList = ltDustGroupList;
	}

	public List<KeyValueVo> getLtApplicationList() {
		return ltApplicationList;
	}

	public void setLtApplicationList(List<KeyValueVo> ltApplicationList) {
		this.ltApplicationList = ltApplicationList;
	}

	public List<KeyValueVo> getLtVoltageList() {
		return ltVoltageList;
	}

	public void setLtVoltageList(List<KeyValueVo> ltVoltageList) {
		this.ltVoltageList = ltVoltageList;
	}

	public List<KeyValueVo> getLtFrequencyList() {
		return ltFrequencyList;
	}

	public void setLtFrequencyList(List<KeyValueVo> ltFrequencyList) {
		this.ltFrequencyList = ltFrequencyList;
	}

	public List<KeyValueVo> getLtCombinedVariationList() {
		return ltCombinedVariationList;
	}

	public void setLtCombinedVariationList(List<KeyValueVo> ltCombinedVariationList) {
		this.ltCombinedVariationList = ltCombinedVariationList;
	}

	public List<KeyValueVo> getLtDutyList() {
		return ltDutyList;
	}

	public void setLtDutyList(List<KeyValueVo> ltDutyList) {
		this.ltDutyList = ltDutyList;
	}

	public List<KeyValueVo> getLtCDFList() {
		return ltCDFList;
	}

	public void setLtCDFList(List<KeyValueVo> ltCDFList) {
		this.ltCDFList = ltCDFList;
	}

	public List<KeyValueVo> getLtStartsList() {
		return ltStartsList;
	}

	public void setLtStartsList(List<KeyValueVo> ltStartsList) {
		this.ltStartsList = ltStartsList;
	}

	public List<KeyValueVo> getLtRVRAList() {
		return ltRVRAList;
	}

	public void setLtRVRAList(List<KeyValueVo> ltRVRAList) {
		this.ltRVRAList = ltRVRAList;
	}

	public List<KeyValueVo> getLtWindingTreatmentList() {
		return ltWindingTreatmentList;
	}

	public void setLtWindingTreatmentList(List<KeyValueVo> ltWindingTreatmentList) {
		this.ltWindingTreatmentList = ltWindingTreatmentList;
	}

	public List<KeyValueVo> getLtLeadList() {
		return ltLeadList;
	}

	public void setLtLeadList(List<KeyValueVo> ltLeadList) {
		this.ltLeadList = ltLeadList;
	}

	public List<KeyValueVo> getLtVFDTypeList() {
		return ltVFDTypeList;
	}

	public void setLtVFDTypeList(List<KeyValueVo> ltVFDTypeList) {
		this.ltVFDTypeList = ltVFDTypeList;
	}

	public List<KeyValueVo> getLtDualSpeedTypeList() {
		return ltDualSpeedTypeList;
	}

	public void setLtDualSpeedTypeList(List<KeyValueVo> ltDualSpeedTypeList) {
		this.ltDualSpeedTypeList = ltDualSpeedTypeList;
	}

	public List<KeyValueVo> getLtShaftMaterialTypeList() {
		return ltShaftMaterialTypeList;
	}

	public void setLtShaftMaterialTypeList(List<KeyValueVo> ltShaftMaterialTypeList) {
		this.ltShaftMaterialTypeList = ltShaftMaterialTypeList;
	}

	public List<KeyValueVo> getLtPaintingTypeList() {
		return ltPaintingTypeList;
	}

	public void setLtPaintingTypeList(List<KeyValueVo> ltPaintingTypeList) {
		this.ltPaintingTypeList = ltPaintingTypeList;
	}

	public List<KeyValueVo> getLtPaintShadeList() {
		return ltPaintShadeList;
	}

	public void setLtPaintShadeList(List<KeyValueVo> ltPaintShadeList) {
		this.ltPaintShadeList = ltPaintShadeList;
	}

	public List<KeyValueVo> getLtPaintThicknessList() {
		return ltPaintThicknessList;
	}

	public void setLtPaintThicknessList(List<KeyValueVo> ltPaintThicknessList) {
		this.ltPaintThicknessList = ltPaintThicknessList;
	}

	public List<KeyValueVo> getLtIPList() {
		return ltIPList;
	}

	public void setLtIPList(List<KeyValueVo> ltIPList) {
		this.ltIPList = ltIPList;
	}

	public List<KeyValueVo> getLtInsulationClassList() {
		return ltInsulationClassList;
	}

	public void setLtInsulationClassList(List<KeyValueVo> ltInsulationClassList) {
		this.ltInsulationClassList = ltInsulationClassList;
	}

	public List<KeyValueVo> getLtTerminalBoxList() {
		return ltTerminalBoxList;
	}

	public void setLtTerminalBoxList(List<KeyValueVo> ltTerminalBoxList) {
		this.ltTerminalBoxList = ltTerminalBoxList;
	}

	public List<KeyValueVo> getLtSpreaderBoxList() {
		return ltSpreaderBoxList;
	}

	public void setLtSpreaderBoxList(List<KeyValueVo> ltSpreaderBoxList) {
		this.ltSpreaderBoxList = ltSpreaderBoxList;
	}

	public List<KeyValueVo> getLtAuxTerminalBoxList() {
		return ltAuxTerminalBoxList;
	}

	public void setLtAuxTerminalBoxList(List<KeyValueVo> ltAuxTerminalBoxList) {
		this.ltAuxTerminalBoxList = ltAuxTerminalBoxList;
	}

	public List<KeyValueVo> getLtSpaceHeaterList() {
		return ltSpaceHeaterList;
	}

	public void setLtSpaceHeaterList(List<KeyValueVo> ltSpaceHeaterList) {
		this.ltSpaceHeaterList = ltSpaceHeaterList;
	}

	public List<KeyValueVo> getLtVibrationList() {
		return ltVibrationList;
	}

	public void setLtVibrationList(List<KeyValueVo> ltVibrationList) {
		this.ltVibrationList = ltVibrationList;
	}

	public List<KeyValueVo> getLtFlyingLeadList() {
		return ltFlyingLeadList;
	}

	public void setLtFlyingLeadList(List<KeyValueVo> ltFlyingLeadList) {
		this.ltFlyingLeadList = ltFlyingLeadList;
	}

	public List<KeyValueVo> getLtMetalFanList() {
		return ltMetalFanList;
	}

	public void setLtMetalFanList(List<KeyValueVo> ltMetalFanList) {
		this.ltMetalFanList = ltMetalFanList;
	}

	public List<KeyValueVo> getLtShaftGroundingList() {
		return ltShaftGroundingList;
	}

	public void setLtShaftGroundingList(List<KeyValueVo> ltShaftGroundingList) {
		this.ltShaftGroundingList = ltShaftGroundingList;
	}

	public List<KeyValueVo> getLtForcedCoolingList() {
		return ltForcedCoolingList;
	}

	public void setLtForcedCoolingList(List<KeyValueVo> ltForcedCoolingList) {
		this.ltForcedCoolingList = ltForcedCoolingList;
	}

	public List<KeyValueVo> getLtHardwareList() {
		return ltHardwareList;
	}

	public void setLtHardwareList(List<KeyValueVo> ltHardwareList) {
		this.ltHardwareList = ltHardwareList;
	}

	public List<KeyValueVo> getLtGlandPlateList() {
		return ltGlandPlateList;
	}

	public void setLtGlandPlateList(List<KeyValueVo> ltGlandPlateList) {
		this.ltGlandPlateList = ltGlandPlateList;
	}

	public List<KeyValueVo> getLtDoubleCompressionGlandList() {
		return ltDoubleCompressionGlandList;
	}

	public void setLtDoubleCompressionGlandList(List<KeyValueVo> ltDoubleCompressionGlandList) {
		this.ltDoubleCompressionGlandList = ltDoubleCompressionGlandList;
	}

	public List<KeyValueVo> getLtSPMMountingProvisionList() {
		return ltSPMMountingProvisionList;
	}

	public void setLtSPMMountingProvisionList(List<KeyValueVo> ltSPMMountingProvisionList) {
		this.ltSPMMountingProvisionList = ltSPMMountingProvisionList;
	}

	public List<KeyValueVo> getLtAddNamePlateList() {
		return ltAddNamePlateList;
	}

	public void setLtAddNamePlateList(List<KeyValueVo> ltAddNamePlateList) {
		this.ltAddNamePlateList = ltAddNamePlateList;
	}

	public List<KeyValueVo> getLtArrowPlateDirectionList() {
		return ltArrowPlateDirectionList;
	}

	public void setLtArrowPlateDirectionList(List<KeyValueVo> ltArrowPlateDirectionList) {
		this.ltArrowPlateDirectionList = ltArrowPlateDirectionList;
	}

	public List<KeyValueVo> getLtRTDList() {
		return ltRTDList;
	}

	public void setLtRTDList(List<KeyValueVo> ltRTDList) {
		this.ltRTDList = ltRTDList;
	}

	public List<KeyValueVo> getLtBTDList() {
		return ltBTDList;
	}

	public void setLtBTDList(List<KeyValueVo> ltBTDList) {
		this.ltBTDList = ltBTDList;
	}

	public List<KeyValueVo> getLtThermisterList() {
		return ltThermisterList;
	}

	public void setLtThermisterList(List<KeyValueVo> ltThermisterList) {
		this.ltThermisterList = ltThermisterList;
	}

	public List<KeyValueVo> getLtCableSealingBoxList() {
		return ltCableSealingBoxList;
	}

	public void setLtCableSealingBoxList(List<KeyValueVo> ltCableSealingBoxList) {
		this.ltCableSealingBoxList = ltCableSealingBoxList;
	}

	public List<KeyValueVo> getLtBearingSystemList() {
		return ltBearingSystemList;
	}

	public void setLtBearingSystemList(List<KeyValueVo> ltBearingSystemList) {
		this.ltBearingSystemList = ltBearingSystemList;
	}

	public List<KeyValueVo> getLtBearingNDEList() {
		return ltBearingNDEList;
	}

	public void setLtBearingNDEList(List<KeyValueVo> ltBearingNDEList) {
		this.ltBearingNDEList = ltBearingNDEList;
	}

	public List<KeyValueVo> getLtBearingDEList() {
		return ltBearingDEList;
	}

	public void setLtBearingDEList(List<KeyValueVo> ltBearingDEList) {
		this.ltBearingDEList = ltBearingDEList;
	}

	public List<KeyValueVo> getLtWitnessRoutineList() {
		return ltWitnessRoutineList;
	}

	public void setLtWitnessRoutineList(List<KeyValueVo> ltWitnessRoutineList) {
		this.ltWitnessRoutineList = ltWitnessRoutineList;
	}

	public List<KeyValueVo> getLtAdditionalTestList() {
		return ltAdditionalTestList;
	}

	public void setLtAdditionalTestList(List<KeyValueVo> ltAdditionalTestList) {
		this.ltAdditionalTestList = ltAdditionalTestList;
	}

	public List<KeyValueVo> getLtTypeTestList() {
		return ltTypeTestList;
	}

	public void setLtTypeTestList(List<KeyValueVo> ltTypeTestList) {
		this.ltTypeTestList = ltTypeTestList;
	}

	public List<KeyValueVo> getLtULCEList() {
		return ltULCEList;
	}

	public void setLtULCEList(List<KeyValueVo> ltULCEList) {
		this.ltULCEList = ltULCEList;
	}

	public List<KeyValueVo> getLtQAPList() {
		return ltQAPList;
	}

	public void setLtQAPList(List<KeyValueVo> ltQAPList) {
		this.ltQAPList = ltQAPList;
	}

	public List<KeyValueVo> getLtSeaworthyPackingList() {
		return ltSeaworthyPackingList;
	}

	public void setLtSeaworthyPackingList(List<KeyValueVo> ltSeaworthyPackingList) {
		this.ltSeaworthyPackingList = ltSeaworthyPackingList;
	}

	public List<KeyValueVo> getLtWarrantyList() {
		return ltWarrantyList;
	}

	public void setLtWarrantyList(List<KeyValueVo> ltWarrantyList) {
		this.ltWarrantyList = ltWarrantyList;
	}
	
	public List<KeyValueVo> getLtVoltVariationList() {
		return ltVoltVariationList;
	}

	public void setLtVoltVariationList(List<KeyValueVo> ltVoltVariationList) {
		this.ltVoltVariationList = ltVoltVariationList;
	}

	public List<KeyValueVo> getLtFreqVariationList() {
		return ltFreqVariationList;
	}

	public void setLtFreqVariationList(List<KeyValueVo> ltFreqVariationList) {
		this.ltFreqVariationList = ltFreqVariationList;
	}
	
	public List<KeyValueVo> getLtStartingDOLCurrentList() {
		return ltStartingDOLCurrentList;
	}

	public void setLtStartingDOLCurrentList(List<KeyValueVo> ltStartingDOLCurrentList) {
		this.ltStartingDOLCurrentList = ltStartingDOLCurrentList;
	}
	
	public List<KeyValueVo> getLtWindingConfigurationList() {
		return ltWindingConfigurationList;
	}

	public void setLtWindingConfigurationList(List<KeyValueVo> ltWindingConfigurationList) {
		this.ltWindingConfigurationList = ltWindingConfigurationList;
	}

	public List<KeyValueVo> getLtMethodOfStartingList() {
		return ltMethodOfStartingList;
	}

	public void setLtMethodOfStartingList(List<KeyValueVo> ltMethodOfStartingList) {
		this.ltMethodOfStartingList = ltMethodOfStartingList;
	}

	public List<KeyValueVo> getLtWindingWireList() {
		return ltWindingWireList;
	}

	public void setLtWindingWireList(List<KeyValueVo> ltWindingWireList) {
		this.ltWindingWireList = ltWindingWireList;
	}

	public List<KeyValueVo> getLtVFDSpeedRangeMinList() {
		return ltVFDSpeedRangeMinList;
	}

	public void setLtVFDSpeedRangeMinList(List<KeyValueVo> ltVFDSpeedRangeMinList) {
		this.ltVFDSpeedRangeMinList = ltVFDSpeedRangeMinList;
	}

	public List<KeyValueVo> getLtVFDSpeedRangeMaxList() {
		return ltVFDSpeedRangeMaxList;
	}

	public void setLtVFDSpeedRangeMaxList(List<KeyValueVo> ltVFDSpeedRangeMaxList) {
		this.ltVFDSpeedRangeMaxList = ltVFDSpeedRangeMaxList;
	}

	public List<KeyValueVo> getLtOverloadingDutyList() {
		return ltOverloadingDutyList;
	}

	public void setLtOverloadingDutyList(List<KeyValueVo> ltOverloadingDutyList) {
		this.ltOverloadingDutyList = ltOverloadingDutyList;
	}

	public List<KeyValueVo> getLtConstantEffRangeList() {
		return ltConstantEffRangeList;
	}

	public void setLtConstantEffRangeList(List<KeyValueVo> ltConstantEffRangeList) {
		this.ltConstantEffRangeList = ltConstantEffRangeList;
	}

	public List<KeyValueVo> getLtShaftTypeList() {
		return ltShaftTypeList;
	}

	public void setLtShaftTypeList(List<KeyValueVo> ltShaftTypeList) {
		this.ltShaftTypeList = ltShaftTypeList;
	}

	public List<KeyValueVo> getLtDirectionOfRotationList() {
		return ltDirectionOfRotationList;
	}

	public void setLtDirectionOfRotationList(List<KeyValueVo> ltDirectionOfRotationList) {
		this.ltDirectionOfRotationList = ltDirectionOfRotationList;
	}

	public List<KeyValueVo> getLtMethodOfCouplingList() {
		return ltMethodOfCouplingList;
	}

	public void setLtMethodOfCouplingList(List<KeyValueVo> ltMethodOfCouplingList) {
		this.ltMethodOfCouplingList = ltMethodOfCouplingList;
	}

	public List<KeyValueVo> getLtTechoMountingList() {
		return ltTechoMountingList;
	}

	public void setLtTechoMountingList(List<KeyValueVo> ltTechoMountingList) {
		this.ltTechoMountingList = ltTechoMountingList;
	}
	
	public List<KeyValueVo> getLtDataSheetList() {
		return ltDataSheetList;
	}

	public void setLtDataSheetList(List<KeyValueVo> ltDataSheetList) {
		this.ltDataSheetList = ltDataSheetList;
	}

	public List<KeyValueVo> getLtNonStandardVoltageList() {
		return ltNonStandardVoltageList;
	}

	public void setLtNonStandardVoltageList(List<KeyValueVo> ltNonStandardVoltageList) {
		this.ltNonStandardVoltageList = ltNonStandardVoltageList;
	}
	
	public List<KeyValueVo> getLtHazAreaTypeList() {
		return ltHazAreaTypeList;
	}

	public void setLtHazAreaTypeList(List<KeyValueVo> ltHazAreaTypeList) {
		this.ltHazAreaTypeList = ltHazAreaTypeList;
	}

	public List<KeyValueVo> getLtHazAreaList() {
		return ltHazAreaList;
	}

	public void setLtHazAreaList(List<KeyValueVo> ltHazAreaList) {
		this.ltHazAreaList = ltHazAreaList;
	}

	public ArrayList getAlApprovalList() {
		return alApprovalList;
	}

	public void setAlApprovalList(ArrayList alApprovalList) {
		this.alApprovalList = alApprovalList;
	}

	public ArrayList getAlTargetedList() {
		return alTargetedList;
	}

	public void setAlTargetedList(ArrayList alTargetedList) {
		this.alTargetedList = alTargetedList;
	}

	public ArrayList getAlIndustryList() {
		return alIndustryList;
	}

	public void setAlIndustryList(ArrayList alIndustryList) {
		this.alIndustryList = alIndustryList;
	}
	
	public ArrayList getAlEnquiryTypeList() {
		return alEnquiryTypeList;
	}

	public void setAlEnquiryTypeList(ArrayList alEnquiryTypeList) {
		this.alEnquiryTypeList = alEnquiryTypeList;
	}
	
	public ArrayList getAlWinChanceList() {
		return alWinChanceList;
	}

	public void setAlWinChanceList(ArrayList alWinChanceList) {
		this.alWinChanceList = alWinChanceList;
	}
	
	public ArrayList getAlInstallLocationList() {
		return alInstallLocationList;
	}

	public void setAlInstallLocationList(ArrayList alInstallLocationList) {
		this.alInstallLocationList = alInstallLocationList;
	}

	public ArrayList getAlVoltList() {
		return alVoltList;
	}

	public void setAlVoltList(ArrayList alVoltList) {
		this.alVoltList = alVoltList;
	}

	// Quotationlt - totalRatingIndex - To indicate number of Ratings added in Enquiry
	public int getTotalRatingIndex() {
		return totalRatingIndex;
	}

	public void setTotalRatingIndex(int totalRatingIndex) {
		this.totalRatingIndex = totalRatingIndex;
	}
	
	public String getReturnstatement() {
		return CommonUtility.replaceNull(returnstatement);
	}

	public void setReturnstatement(String returnstatement) {
		this.returnstatement = returnstatement;
	}

	public String getReturnstatus() {
		return CommonUtility.replaceNull(returnstatus);
	}

	public void setReturnstatus(String returnstatus) {
		this.returnstatus = returnstatus;
	}

	public String getDmToSmReturn() {
		return CommonUtility.replaceNull(dmToSmReturn);
	}

	public void setDmToSmReturn(String dmToSmReturn) {
		this.dmToSmReturn = dmToSmReturn;
	}

	public String getDeToSmReturn() {
		return CommonUtility.replaceNull(deToSmReturn);
	}

	public void setDeToSmReturn(String deToSmReturn) {
		this.deToSmReturn = deToSmReturn;
	}

	public List<KeyValueVo> getTcDeliveryTypeList() {
		return tcDeliveryTypeList;
	}

	public void setTcDeliveryTypeList(List<KeyValueVo> tcDeliveryTypeList) {
		this.tcDeliveryTypeList = tcDeliveryTypeList;
	}

	public List<KeyValueVo> getTcPaymentTermsList() {
		return tcPaymentTermsList;
	}

	public void setTcPaymentTermsList(List<KeyValueVo> tcPaymentTermsList) {
		this.tcPaymentTermsList = tcPaymentTermsList;
	}

	public List<KeyValueVo> getTcPercentNetOrderValueList() {
		return tcPercentNetOrderValueList;
	}

	public void setTcPercentNetOrderValueList(List<KeyValueVo> tcPercentNetOrderValueList) {
		this.tcPercentNetOrderValueList = tcPercentNetOrderValueList;
	}

	public List<KeyValueVo> getTcPaymentDaysList() {
		return tcPaymentDaysList;
	}

	public void setTcPaymentDaysList(List<KeyValueVo> tcPaymentDaysList) {
		this.tcPaymentDaysList = tcPaymentDaysList;
	}

	public List<KeyValueVo> getTcPayableTermsList() {
		return tcPayableTermsList;
	}

	public void setTcPayableTermsList(List<KeyValueVo> tcPayableTermsList) {
		this.tcPayableTermsList = tcPayableTermsList;
	}

	public List<KeyValueVo> getTcInstrumentList() {
		return tcInstrumentList;
	}

	public void setTcInstrumentList(List<KeyValueVo> tcInstrumentList) {
		this.tcInstrumentList = tcInstrumentList;
	}

	public List<KeyValueVo> getTcWarrantyDaysList() {
		return tcWarrantyDaysList;
	}

	public void setTcWarrantyDaysList(List<KeyValueVo> tcWarrantyDaysList) {
		this.tcWarrantyDaysList = tcWarrantyDaysList;
	}

	public List<KeyValueVo> getTcOrderCompleteLeadTimeList() {
		return tcOrderCompleteLeadTimeList;
	}

	public void setTcOrderCompleteLeadTimeList(List<KeyValueVo> tcOrderCompleteLeadTimeList) {
		this.tcOrderCompleteLeadTimeList = tcOrderCompleteLeadTimeList;
	}

	public List<KeyValueVo> getTcGstValuesList() {
		return tcGstValuesList;
	}

	public void setTcGstValuesList(List<KeyValueVo> tcGstValuesList) {
		this.tcGstValuesList = tcGstValuesList;
	}

	public List<KeyValueVo> getTcPackagingList() {
		return tcPackagingList;
	}

	public void setTcPackagingList(List<KeyValueVo> tcPackagingList) {
		this.tcPackagingList = tcPackagingList;
	}

	public List<KeyValueVo> getTcLiquidatedDamagesList() {
		return tcLiquidatedDamagesList;
	}

	public void setTcLiquidatedDamagesList(List<KeyValueVo> tcLiquidatedDamagesList) {
		this.tcLiquidatedDamagesList = tcLiquidatedDamagesList;
	}

	public String getIsNewEnquiry() {
		return CommonUtility.replaceNull(isNewEnquiry);
	}

	public void setIsNewEnquiry(String isNewEnquiry) {
		this.isNewEnquiry = isNewEnquiry;
	}

	public List<KeyValueVo> getTcDeliveryLotList() {
		return tcDeliveryLotList;
	}

	public void setTcDeliveryLotList(List<KeyValueVo> tcDeliveryLotList) {
		this.tcDeliveryLotList = tcDeliveryLotList;
	}

	public List<KeyValueVo> getTcDeliveryDamagesList() {
		return tcDeliveryDamagesList;
	}

	public void setTcDeliveryDamagesList(List<KeyValueVo> tcDeliveryDamagesList) {
		this.tcDeliveryDamagesList = tcDeliveryDamagesList;
	}
	
	public List<KeyValueVo> getTcOfferValidityList() {
		return tcOfferValidityList;
	}

	public void setTcOfferValidityList(List<KeyValueVo> tcOfferValidityList) {
		this.tcOfferValidityList = tcOfferValidityList;
	}

	public List<KeyValueVo> getTcPriceValidityList() {
		return tcPriceValidityList;
	}

	public void setTcPriceValidityList(List<KeyValueVo> tcPriceValidityList) {
		this.tcPriceValidityList = tcPriceValidityList;
	}

	public List<KeyValueVo> getLtMfgLocationList() {
		return ltMfgLocationList;
	}

	public void setLtMfgLocationList(List<KeyValueVo> ltMfgLocationList) {
		this.ltMfgLocationList = ltMfgLocationList;
	}

	public String getSelectedRemoveRatingId() {
		return CommonUtility.replaceNull(selectedRemoveRatingId);
	}

	public void setSelectedRemoveRatingId(String selectedRemoveRatingId) {
		this.selectedRemoveRatingId = selectedRemoveRatingId;
	}

	public String getEnquiryReferenceNumber() {
		return CommonUtility.replaceNull(enquiryReferenceNumber);
	}

	public void setEnquiryReferenceNumber(String enquiryReferenceNumber) {
		this.enquiryReferenceNumber = enquiryReferenceNumber;
	}

	public String getDescription() {
		return CommonUtility.replaceNull(description);
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAssignToId() {
		return assignToId;
	}

	public void setAssignToId(String assignToId) {
		this.assignToId = assignToId;
	}

	public String getAssignToName() {
		return assignToName;
	}

	public void setAssignToName(String assignToName) {
		this.assignToName = assignToName;
	}

	public String getAssignRemarks() {
		return assignRemarks;
	}

	public void setAssignRemarks(String assignRemarks) {
		this.assignRemarks = assignRemarks;
	}

	public String getReturnTo() {
		return returnTo;
	}

	public void setReturnTo(String returnTo) {
		this.returnTo = returnTo;
	}

	public String getReturnToRemarks() {
		return returnToRemarks;
	}

	public void setReturnToRemarks(String returnToRemarks) {
		this.returnToRemarks = returnToRemarks;
	}
	
	public String getPagechecking() {
		return CommonUtility.replaceNull(pagechecking);
	}
	public void setPagechecking(String pagechecking) {
		this.pagechecking = pagechecking;
	}
	
	public String getSmNote() {
		return CommonUtility.replaceNull(smNote);
	}
	public void setSmNote(String smNote) {
		this.smNote = smNote;
	}
	public String getSm_name() {
		return CommonUtility.replaceNull(sm_name);
	}
	public void setSm_name(String sm_name) {
		this.sm_name = sm_name;
	}
	public String getSmEmail() {
		return CommonUtility.replaceNull(smEmail);
	}
	public void setSmEmail(String smEmail) {
		this.smEmail = smEmail;
	}
	public String getSmContactNo() {
		return CommonUtility.replaceNull(smContactNo);
	}
	public void setSmContactNo(String smContactNo) {
		this.smContactNo = smContactNo;
	}
	public List<KeyValueVo> getLtServiceFactorList() {
		return ltServiceFactorList;
	}
	public void setLtServiceFactorList(List<KeyValueVo> ltServiceFactorList) {
		this.ltServiceFactorList = ltServiceFactorList;
	}
	public List<KeyValueVo> getLtStandardRotationList() {
		return ltStandardRotationList;
	}
	public void setLtStandardRotationList(List<KeyValueVo> ltStandardRotationList) {
		this.ltStandardRotationList = ltStandardRotationList;
	}
	public List<KeyValueVo> getLtTemperatureRiseList() {
		return ltTemperatureRiseList;
	}
	public void setLtTemperatureRiseList(List<KeyValueVo> ltTemperatureRiseList) {
		this.ltTemperatureRiseList = ltTemperatureRiseList;
	}
	public List<KeyValueVo> getLtNoiseLevelList() {
		return ltNoiseLevelList;
	}
	public void setLtNoiseLevelList(List<KeyValueVo> ltNoiseLevelList) {
		this.ltNoiseLevelList = ltNoiseLevelList;
	}
	public List<KeyValueVo> getLtPerfCurvesList() {
		return ltPerfCurvesList;
	}
	public void setLtPerfCurvesList(List<KeyValueVo> ltPerfCurvesList) {
		this.ltPerfCurvesList = ltPerfCurvesList;
	}

	public String getDeptAndAuth() {
		return CommonUtility.replaceNull(deptAndAuth);
	}
	public void setDeptAndAuth(String deptAndAuth) {
		this.deptAndAuth = deptAndAuth;
	}
	public String getScsrName() {
		return CommonUtility.replaceNull(scsrName);
	}
	public void setScsrName(String scsrName) {
		this.scsrName = scsrName;
	}

	public String getRatingOperationId() {
		return CommonUtility.replaceNull(ratingOperationId);
	}
	public void setRatingOperationId(String ratingOperationId) {
		this.ratingOperationId = ratingOperationId;
	}

	public ArrayList getTeamMemberList() {
		return teamMemberList;
	}

	public void setTeamMemberList(ArrayList teamMemberList) {
		this.teamMemberList = teamMemberList;
	}

	public ArrayList getDirectManagersList() {
		return directManagersList;
	}

	public void setDirectManagersList(ArrayList directManagersList) {
		this.directManagersList = directManagersList;
	}

	public String getRatedOutputUnit() {
		return CommonUtility.replaceNull(ratedOutputUnit);
	}
	public void setRatedOutputUnit(String ratedOutputUnit) {
		this.ratedOutputUnit = ratedOutputUnit;
	}

	public String getRsmNote() {
		return CommonUtility.replaceNull(rsmNote);
	}
	public void setRsmNote(String rsmNote) {
		this.rsmNote = rsmNote;
	}
	public String getNsmNote() {
		return CommonUtility.replaceNull(nsmNote);
	}
	public void setNsmNote(String nsmNote) {
		this.nsmNote = nsmNote;
	}
	public String getMhNote() {
		return CommonUtility.replaceNull(mhNote);
	}
	public void setMhNote(String mhNote) {
		this.mhNote = mhNote;
	}
	public String getBhNote() {
		return CommonUtility.replaceNull(bhNote);
	}
	public void setBhNote(String bhNote) {
		this.bhNote = bhNote;
	}
	public String getMfgNote() {
		return CommonUtility.replaceNull(mfgNote);
	}
	public void setMfgNote(String mfgNote) {
		this.mfgNote = mfgNote;
	}
	public String getQaNote() {
		return CommonUtility.replaceNull(qaNote);
	}
	public void setQaNote(String qaNote) {
		this.qaNote = qaNote;
	}
	public String getScmNote() {
		return CommonUtility.replaceNull(scmNote);
	}
	public void setScmNote(String scmNote) {
		this.scmNote = scmNote;
	}
	public ArrayList getAddOnList() {
		return addOnList;
	}
	public void setAddOnList(ArrayList addOnList) {
		this.addOnList = addOnList;
	}
	public String getFlcAmps() {
		return CommonUtility.replaceNull(flcAmps);
	}
	public void setFlcAmps(String flcAmps) {
		this.flcAmps = flcAmps;
	}

	public ArrayList getAlMainTermBoxPSList() {
		return alMainTermBoxPSList;
	}

	public void setAlMainTermBoxPSList(ArrayList alMainTermBoxPSList) {
		this.alMainTermBoxPSList = alMainTermBoxPSList;
	}

	public ArrayList getAlNeutralTBList() {
		return alNeutralTBList;
	}

	public void setAlNeutralTBList(ArrayList alNeutralTBList) {
		this.alNeutralTBList = alNeutralTBList;
	}

	public ArrayList getAlCableEntryList() {
		return alCableEntryList;
	}

	public void setAlCableEntryList(ArrayList alCableEntryList) {
		this.alCableEntryList = alCableEntryList;
	}

	public ArrayList getAlBoTerminalsNoList() {
		return alBoTerminalsNoList;
	}

	public void setAlBoTerminalsNoList(ArrayList alBoTerminalsNoList) {
		this.alBoTerminalsNoList = alBoTerminalsNoList;
	}

	public ArrayList getAlLubricationList() {
		return alLubricationList;
	}

	public void setAlLubricationList(ArrayList alLubricationList) {
		this.alLubricationList = alLubricationList;
	}

	public ArrayList getAlnoiseLevelList() {
		return alnoiseLevelList;
	}

	public void setAlnoiseLevelList(ArrayList alnoiseLevelList) {
		this.alnoiseLevelList = alnoiseLevelList;
	}

	public ArrayList getAlMinStartVoltList() {
		return alMinStartVoltList;
	}

	public void setAlMinStartVoltList(ArrayList alMinStartVoltList) {
		this.alMinStartVoltList = alMinStartVoltList;
	}

	public ArrayList getAlBalancingList() {
		return alBalancingList;
	}

	public void setAlBalancingList(ArrayList alBalancingList) {
		this.alBalancingList = alBalancingList;
	}

	public ArrayList getAlBearingThermoDtList() {
		return alBearingThermoDtList;
	}

	public void setAlBearingThermoDtList(ArrayList alBearingThermoDtList) {
		this.alBearingThermoDtList = alBearingThermoDtList;
	}

	public ArrayList getDegproList() {
		return degproList;
	}

	public void setDegproList(ArrayList degproList) {
		this.degproList = degproList;
	}

	public ArrayList getTermBoxList() {
		return termBoxList;
	}

	public void setTermBoxList(ArrayList termBoxList) {
		this.termBoxList = termBoxList;
	}

	public String getIsReferDesign() {
		return CommonUtility.replaceNull(isReferDesign);
	}

	public void setIsReferDesign(String isReferDesign) {
		this.isReferDesign = isReferDesign;
	}

	public List<KeyValueVo> getLtCableSizeList() {
		return ltCableSizeList;
	}

	public void setLtCableSizeList(List<KeyValueVo> ltCableSizeList) {
		this.ltCableSizeList = ltCableSizeList;
	}

	public List<KeyValueVo> getLtNoOfRunsList() {
		return ltNoOfRunsList;
	}

	public void setLtNoOfRunsList(List<KeyValueVo> ltNoOfRunsList) {
		this.ltNoOfRunsList = ltNoOfRunsList;
	}

	public List<KeyValueVo> getLtNoOfCoresList() {
		return ltNoOfCoresList;
	}

	public void setLtNoOfCoresList(List<KeyValueVo> ltNoOfCoresList) {
		this.ltNoOfCoresList = ltNoOfCoresList;
	}

	public List<KeyValueVo> getLtCrossSectionAreaList() {
		return ltCrossSectionAreaList;
	}

	public void setLtCrossSectionAreaList(List<KeyValueVo> ltCrossSectionAreaList) {
		this.ltCrossSectionAreaList = ltCrossSectionAreaList;
	}

	public List<KeyValueVo> getLtConductorMaterialList() {
		return ltConductorMaterialList;
	}

	public void setLtConductorMaterialList(List<KeyValueVo> ltConductorMaterialList) {
		this.ltConductorMaterialList = ltConductorMaterialList;
	}

	public ArrayList getEnclosureList() {
		return enclosureList;
	}

	public void setEnclosureList(ArrayList enclosureList) {
		this.enclosureList = enclosureList;
	}

	public List<KeyValueVo> getLtTemperatureClassList() {
		return ltTemperatureClassList;
	}

	public void setLtTemperatureClassList(List<KeyValueVo> ltTemperatureClassList) {
		this.ltTemperatureClassList = ltTemperatureClassList;
	}

	public List<KeyValueVo> getLtEfficiencyClassList() {
		return ltEfficiencyClassList;
	}

	public void setLtEfficiencyClassList(List<KeyValueVo> ltEfficiencyClassList) {
		this.ltEfficiencyClassList = ltEfficiencyClassList;
	}

	public List<KeyValueVo> getLtTypeOfGreaseList() {
		return ltTypeOfGreaseList;
	}

	public void setLtTypeOfGreaseList(List<KeyValueVo> ltTypeOfGreaseList) {
		this.ltTypeOfGreaseList = ltTypeOfGreaseList;
	}
	
	public List<KeyValueVo> getLtSparesList() {
		return ltSparesList;
	}

	public void setLtSparesList(List<KeyValueVo> ltSparesList) {
		this.ltSparesList = ltSparesList;
	}
	
	public List<KeyValueVo> getLtMotorGAList() {
		return ltMotorGAList;
	}

	public void setLtMotorGAList(List<KeyValueVo> ltMotorGAList) {
		this.ltMotorGAList = ltMotorGAList;
	}

	public List<KeyValueVo> getLtTBoxGAList() {
		return ltTBoxGAList;
	}

	public void setLtTBoxGAList(List<KeyValueVo> ltTBoxGAList) {
		this.ltTBoxGAList = ltTBoxGAList;
	}

	public String getCommentsDeviations() {
		return CommonUtility.replaceNull(commentsDeviations);
	}
	public void setCommentsDeviations(String commentsDeviations) {
		this.commentsDeviations = commentsDeviations;
	}
	public String getReassignRemarks() {
		return CommonUtility.replaceNull(reassignRemarks);
	}
	public void setReassignRemarks(String reassignRemarks) {
		this.reassignRemarks = reassignRemarks;
	}

	public String getAbondentReason1() {
		return CommonUtility.replaceNull(abondentReason1);
	}

	public void setAbondentReason1(String abondentReason1) {
		this.abondentReason1 = abondentReason1;
	}

	public String getAbondentReason2() {
		return CommonUtility.replaceNull(abondentReason2);
	}

	public void setAbondentReason2(String abondentReason2) {
		this.abondentReason2 = abondentReason2;
	}

	public String getAbondentRemarks() {
		return CommonUtility.replaceNull(abondentRemarks);
	}

	public void setAbondentRemarks(String abondentRemarks) {
		this.abondentRemarks = abondentRemarks;
	}

	public String getAbondentCustomerApproval() {
		return CommonUtility.replaceNull(abondentCustomerApproval);
	}

	public void setAbondentCustomerApproval(String abondentCustomerApproval) {
		this.abondentCustomerApproval = abondentCustomerApproval;
	}

	public List<KeyValueVo> getLtAbondentReasonList() {
		return ltAbondentReasonList;
	}

	public void setLtAbondentReasonList(List<KeyValueVo> ltAbondentReasonList) {
		this.ltAbondentReasonList = ltAbondentReasonList;
	}

	public String getLostCompetitor() {
		return CommonUtility.replaceNull(lostCompetitor);
	}

	public void setLostCompetitor(String lostCompetitor) {
		this.lostCompetitor = lostCompetitor;
	}

	public String getLostReason1() {
		return CommonUtility.replaceNull(lostReason1);
	}

	public void setLostReason1(String lostReason1) {
		this.lostReason1 = lostReason1;
	}

	public String getLostReason2() {
		return CommonUtility.replaceNull(lostReason2);
	}

	public void setLostReason2(String lostReason2) {
		this.lostReason2 = lostReason2;
	}

	public String getLostRemarks() {
		return CommonUtility.replaceNull(lostRemarks);
	}

	public void setLostRemarks(String lostRemarks) {
		this.lostRemarks = lostRemarks;
	}

	public String getLostManagerApproval() {
		return CommonUtility.replaceNull(lostManagerApproval);
	}

	public void setLostManagerApproval(String lostManagerApproval) {
		this.lostManagerApproval = lostManagerApproval;
	}

	public String getTotalQuotedPrice() {
		return CommonUtility.replaceNull(totalQuotedPrice);
	}

	public void setTotalQuotedPrice(String totalQuotedPrice) {
		this.totalQuotedPrice = totalQuotedPrice;
	}

	public String getTotalLostPrice() {
		return CommonUtility.replaceNull(totalLostPrice);
	}

	public void setTotalLostPrice(String totalLostPrice) {
		this.totalLostPrice = totalLostPrice;
	}

	public List<KeyValueVo> getLtLostCompetitorList() {
		return ltLostCompetitorList;
	}

	public void setLtLostCompetitorList(List<KeyValueVo> ltLostCompetitorList) {
		this.ltLostCompetitorList = ltLostCompetitorList;
	}

	public List<KeyValueVo> getLtLostReasonList() {
		return ltLostReasonList;
	}

	public void setLtLostReasonList(List<KeyValueVo> ltLostReasonList) {
		this.ltLostReasonList = ltLostReasonList;
	}

	public List<KeyValueVo> getLtWonPOOptionsList() {
		return ltWonPOOptionsList;
	}

	public void setLtWonPOOptionsList(List<KeyValueVo> ltWonPOOptionsList) {
		this.ltWonPOOptionsList = ltWonPOOptionsList;
	}

	public List<KeyValueVo> getLtWonReasonList() {
		return ltWonReasonList;
	}

	public void setLtWonReasonList(List<KeyValueVo> ltWonReasonList) {
		this.ltWonReasonList = ltWonReasonList;
	}

	public FormFile getWonPOCopy() {
		return wonPOCopy;
	}

	public void setWonPOCopy(FormFile wonPOCopy) {
		this.wonPOCopy = wonPOCopy;
	}

	public String getWonIndentNo() {
		return CommonUtility.replaceNull(wonIndentNo);
	}

	public void setWonIndentNo(String wonIndentNo) {
		this.wonIndentNo = wonIndentNo;
	}

	public String getWonIsPOorLOI() {
		return CommonUtility.replaceNull(wonIsPOorLOI);
	}

	public void setWonIsPOorLOI(String wonIsPOorLOI) {
		this.wonIsPOorLOI = wonIsPOorLOI;
	}

	public String getWonPONo() {
		return CommonUtility.replaceNull(wonPONo);
	}

	public void setWonPONo(String wonPONo) {
		this.wonPONo = wonPONo;
	}

	public String getWonPODate() {
		return CommonUtility.replaceNull(wonPODate);
	}

	public void setWonPODate(String wonPODate) {
		this.wonPODate = wonPODate;
	}

	public String getWonPOReceiptDate() {
		return CommonUtility.replaceNull(wonPOReceiptDate);
	}

	public void setWonPOReceiptDate(String wonPOReceiptDate) {
		this.wonPOReceiptDate = wonPOReceiptDate;
	}

	public String getWonReason1() {
		return CommonUtility.replaceNull(wonReason1);
	}

	public void setWonReason1(String wonReason1) {
		this.wonReason1 = wonReason1;
	}

	public String getWonReason2() {
		return CommonUtility.replaceNull(wonReason2);
	}

	public void setWonReason2(String wonReason2) {
		this.wonReason2 = wonReason2;
	}

	public String getWonRemarks() {
		return CommonUtility.replaceNull(wonRemarks);
	}

	public void setWonRemarks(String wonRemarks) {
		this.wonRemarks = wonRemarks;
	}

	public String getTotalWonPrice() {
		return CommonUtility.replaceNull(totalWonPrice);
	}

	public void setTotalWonPrice(String totalWonPrice) {
		this.totalWonPrice = totalWonPrice;
	}

	public String getWonRequiresDrawing() {
		return CommonUtility.replaceNull(wonRequiresDrawing);
	}

	public void setWonRequiresDrawing(String wonRequiresDrawing) {
		this.wonRequiresDrawing = wonRequiresDrawing;
	}

	public String getWonRequiresDatasheet() {
		return CommonUtility.replaceNull(wonRequiresDatasheet);
	}

	public void setWonRequiresDatasheet(String wonRequiresDatasheet) {
		this.wonRequiresDatasheet = wonRequiresDatasheet;
	}

	public String getWonRequiresQAP() {
		return CommonUtility.replaceNull(wonRequiresQAP);
	}

	public void setWonRequiresQAP(String wonRequiresQAP) {
		this.wonRequiresQAP = wonRequiresQAP;
	}

	public String getWonRequiresPerfCurves() {
		return CommonUtility.replaceNull(wonRequiresPerfCurves);
	}

	public void setWonRequiresPerfCurves(String wonRequiresPerfCurves) {
		this.wonRequiresPerfCurves = wonRequiresPerfCurves;
	}

	public String getDesignNote() {
		return CommonUtility.replaceNull(designNote);
	}

	public void setDesignNote(String designNote) {
		this.designNote = designNote;
	}

	public String getDmUpdateStatus() {
		return CommonUtility.replaceNull(dmUpdateStatus);
	}

	public void setDmUpdateStatus(String dmUpdateStatus) {
		this.dmUpdateStatus = dmUpdateStatus;
	}

	public List<KeyValueVo> getSalesReassignUsersList() {
		return salesReassignUsersList;
	}

	public void setSalesReassignUsersList(List<KeyValueVo> salesReassignUsersList) {
		this.salesReassignUsersList = salesReassignUsersList;
	}

	public String getShowReAssignSectionFlag() {
		return CommonUtility.replaceNull(showReAssignSectionFlag);
	}

	public void setShowReAssignSectionFlag(String showReAssignSectionFlag) {
		this.showReAssignSectionFlag = showReAssignSectionFlag;
	}

	public String getCustomerDealerLink() {
		return CommonUtility.replaceNull(customerDealerLink);
	}

	public void setCustomerDealerLink(String customerDealerLink) {
		this.customerDealerLink = customerDealerLink;
	}
	
	public String getTotalCost() {
		return CommonUtility.replaceNull(totalCost);
	}

	public void setTotalCost(String totalCost) {
		this.totalCost = totalCost;
	}

	public String getTotalProfitMargin() {
		return CommonUtility.replaceNull(totalProfitMargin);
	}

	public void setTotalProfitMargin(String totalProfitMargin) {
		this.totalProfitMargin = totalProfitMargin;
	}
	
	
}
