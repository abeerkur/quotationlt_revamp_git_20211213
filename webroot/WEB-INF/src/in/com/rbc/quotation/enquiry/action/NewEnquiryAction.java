/**
 * ******************************************************************************************
 * Project Name: Request For Quotation
 * Document Name: NewEnquiryAction.java
 * Package: in.com.rbc.quotation.enquiry.action
 * Desc:  Action class that contains all the action methods for NewEnquiry
 * ******************************************************************************************
 */
package in.com.rbc.quotation.enquiry.action;


import java.io.PrintWriter;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.json.simple.JSONValue;

import com.fasterxml.jackson.databind.ObjectMapper;

import in.com.rbc.quotation.admin.dao.AddOnDetailsDao;
import in.com.rbc.quotation.admin.dao.CustomerDao;
import in.com.rbc.quotation.admin.dao.CustomerDiscountDao;
import in.com.rbc.quotation.admin.dao.ListPriseDao;
import in.com.rbc.quotation.admin.dto.AddOnDetailsDto;
import in.com.rbc.quotation.admin.dto.CustomerDiscountDto;
import in.com.rbc.quotation.admin.dto.CustomerDto;
import in.com.rbc.quotation.admin.dto.ListPriceDto;
import in.com.rbc.quotation.base.BaseAction;
import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.dto.AttachmentDto;
import in.com.rbc.quotation.common.utility.AttachmentUtility;
import in.com.rbc.quotation.common.utility.CommonUtility;
import in.com.rbc.quotation.common.utility.DBUtility;
import in.com.rbc.quotation.common.utility.DateUtility;
import in.com.rbc.quotation.common.utility.ExceptionUtility;
import in.com.rbc.quotation.common.utility.LoggerUtility;
import in.com.rbc.quotation.common.utility.NewQuotationPDFUtility;
import in.com.rbc.quotation.common.vo.KeyValueVo;
import in.com.rbc.quotation.enquiry.dao.MTOQuotationDao;
import in.com.rbc.quotation.enquiry.dao.QuotationDao;
import in.com.rbc.quotation.enquiry.dao.SearchEnquiryDao;
import in.com.rbc.quotation.enquiry.dao.SearchEnquiryDaoImpl;
import in.com.rbc.quotation.enquiry.dto.AddOnDto;
import in.com.rbc.quotation.enquiry.dto.EnquiryDto;
import in.com.rbc.quotation.enquiry.dto.NewEnquiryDto;
import in.com.rbc.quotation.enquiry.dto.NewRatingDto;
import in.com.rbc.quotation.enquiry.dto.NewTechnicalOfferDto;
import in.com.rbc.quotation.enquiry.dto.RatingDto;
import in.com.rbc.quotation.enquiry.dto.SearchEnquiryDto;
import in.com.rbc.quotation.enquiry.dto.TeamMemberDto;
import in.com.rbc.quotation.enquiry.dto.UserDetailsDto;
import in.com.rbc.quotation.enquiry.form.EnquiryForm;
import in.com.rbc.quotation.enquiry.form.NewEnquiryForm;
import in.com.rbc.quotation.enquiry.utility.NewEnquiryUtilityManager;
import in.com.rbc.quotation.enquiry.utility.NewPDFUtilityManager;
import in.com.rbc.quotation.factory.DaoFactory;
import in.com.rbc.quotation.user.action.UserAction;
import in.com.rbc.quotation.user.dao.UserDao;
import in.com.rbc.quotation.user.dto.UserDto;
import in.com.rbc.quotation.workflow.dao.WorkflowDao;
import in.com.rbc.quotation.workflow.dto.WorkflowDto;
import in.com.rbc.quotation.workflow.utility.WorkflowUtility;

public class NewEnquiryAction extends BaseAction {
	
	
    /**
     * This method is used to get look up fields data required in the create page.
     * @param actionMapping ActionMapping object to redirect the user to next page depending on the activity
     * @param actionForm ActionForm object to handle the form elements
     * @param request HttpServletRequest object to handle request operations
     * @param response HttpServletResponse object to handle response operations
     * @return returns ActionForward depending on which user is redirected to next page
     * @throws Exception if any error occurs while performing database operations, etc
     */
    public ActionForward viewCreateEnquiry(ActionMapping actionMapping,
                              ActionForm actionForm, 
                              HttpServletRequest request,
                              HttpServletResponse response) throws Exception {
    	
    	String sMethodName = QuotationConstants.QUOTATION_VIEWCINQ;
    	
    	Connection conn = null;  			// Connection object to store the database connection
        DBUtility oDBUtility = null;  		// Object of DBUtility class to handle DB connection objects
        UserDto oUserDto = null;
        UserAction oUserAction = null; 		// Instantiating the UserAction object
        DaoFactory oDaoFactory = null; 		// Creating an instance of of DaoFactory & retrieving UserDao object
        SearchEnquiryDao oSearchEnquiryDao = null; 	// Check if the logged-in user is a sales manager or not, if not re-direct the user to the exeption page
        QuotationDao oQuotationDao = null;	// Creating an instance of QuotationDao
        NewEnquiryForm oNewEnquiryForm = null;
        NewEnquiryDto oNewEnquiryDto = null;
        AttachmentDto oAttachmentDto = null;
        
        try{
    		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
    		setRolesToRequest(request);
            LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "Roles set to request.");
            
            oNewEnquiryForm = (NewEnquiryForm) actionForm;
    		if (oNewEnquiryForm == null)
    			oNewEnquiryForm = new NewEnquiryForm();
    		
    		/* To avoid duplicate form submission (Use Synchronizer Token Pattern) - This token will be validated while saving the request. */
    		saveToken(request);
            /* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Instantiating the UserAction object */
            oUserAction = new UserAction();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            
            /* Retrieving the logged-in user information */
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            
            /* Check if the logged-in user is a sales manager or not, if not re-direct the user to the exeption page*/
            oSearchEnquiryDao = oDaoFactory.getSearchEnquiryDao();
            
            if(!oSearchEnquiryDao.isSalesManager(conn,oUserDto.getUserId())){      	
            	/* Setting error details to request to display the same in Exception page */
            	request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_MSG_UNAUTH);
              
            	/* Forwarding request to Error page */
               return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);  
            }
            
            oNewEnquiryDto = new NewEnquiryDto();
            oQuotationDao = oDaoFactory.getQuotationDao();
            NewEnquiryUtilityManager oNewEnquiryManager = new NewEnquiryUtilityManager();
            PropertyUtils.copyProperties(oNewEnquiryDto,oNewEnquiryForm);
            oNewEnquiryDto.setEnquiryId(new Integer(0));
            oNewEnquiryDto = oQuotationDao.loadNewEnquiryLookUpValues(conn, oNewEnquiryDto);
            PropertyUtils.copyProperties(oNewEnquiryForm, oNewEnquiryDto);
            oNewEnquiryForm = oNewEnquiryManager.loadLookUpValues(conn, oNewEnquiryForm);
			
            oNewEnquiryDto.setCreatedBySSO(oUserDto.getUserId());
            oNewEnquiryForm.setCreatedBy(oUserDto.getFullName());           
            oNewEnquiryForm.setCreatedBySSO(oUserDto.getUserId());
            oNewEnquiryForm.setCreatedDate(DateUtility.getRBCStandardDate());
            oNewEnquiryForm.setRevisionNumber(QuotationConstants.QUOTATION_LITERAL_ONE); 
            oNewEnquiryForm.setRatingNo(QuotationConstants.QUOTATION_LITERAL_ONE); 
            oNewEnquiryForm.setRatingTitle(QuotationConstants.QUOTATION_RATING+oNewEnquiryForm.getRevisionNumber());
            oNewEnquiryForm.setEnquiryOperationType(QuotationConstants.QUOTATION_INSERT);
            oNewEnquiryForm.setRatingOperationType(QuotationConstants.QUOTATION_INSERT);
            
            // Setting Default Values.
            oNewEnquiryForm.setDeliveryTerm(QuotationConstants.QUOTATION_STRING_TWO);
            oNewEnquiryForm.setDeliveryInFull(QuotationConstants.QUOTATION_STRING_ONE);
            oNewEnquiryForm.setCustRequestedDeliveryTime(QuotationConstants.QUOTATION_STRING_ELEVEN);
            if(StringUtils.isBlank(oNewEnquiryForm.getWarrantyDispatchDate())) {
            	oNewEnquiryForm.setWarrantyDispatchDate(QuotationConstants.QUOTATION_STRING_TWO); 
            }
            if(StringUtils.isBlank(oNewEnquiryForm.getWarrantyCommissioningDate())) {
            	oNewEnquiryForm.setWarrantyCommissioningDate(QuotationConstants.QUOTATION_STRING_ONE);
            }
            if(StringUtils.isBlank(oNewEnquiryForm.getOrderCompletionLeadTime())) {
            	oNewEnquiryForm.setOrderCompletionLeadTime(QuotationConstants.QUOTATION_STRING_ELEVEN);
            }
            if(StringUtils.isBlank(oNewEnquiryForm.getGstValue())) {
            	oNewEnquiryForm.setGstValue(QuotationConstants.QUOTATION_STRING_THREE);
            }
            if(StringUtils.isBlank(oNewEnquiryForm.getPackaging())) {
            	oNewEnquiryForm.setPackaging(QuotationConstants.QUOTATION_STRING_ONE);
            }
            if(StringUtils.isBlank(oNewEnquiryForm.getOfferValidity())) {
            	oNewEnquiryForm.setOfferValidity(QuotationConstants.QUOTATION_STRING_TWO);
            }
            if(StringUtils.isBlank(oNewEnquiryForm.getPriceValidity())) {
            	oNewEnquiryForm.setPriceValidity(QuotationConstants.QUOTATION_STRING_FOUR);
            }
            if(StringUtils.isBlank(oNewEnquiryForm.getLiquidatedDamagesDueToDeliveryDelay())) {
            	oNewEnquiryForm.setLiquidatedDamagesDueToDeliveryDelay(QuotationConstants.QUOTATION_STRING_ONE);
            }
            if(StringUtils.isBlank(oNewEnquiryForm.getSupervisionDetails())) {
            	oNewEnquiryForm.setSupervisionDetails("No");
            }
            
            // Setting the Initial Rating Object and Rating Index to NewEnquiryForm
			ArrayList<NewRatingDto> newRatingsList = new ArrayList<NewRatingDto>();
			
			// Setting First Rating values.
			NewRatingDto oNewRatingDto = new NewRatingDto();
			oNewRatingDto.setRatingId(new Integer(1));
			oNewRatingDto.setRatingNo(new Integer(1));
			// Set Default values for First Rating.
			oNewRatingDto = oQuotationDao.processNewRatingDefaults(conn, oNewRatingDto);
			
			newRatingsList.add(oNewRatingDto);
			
			oNewEnquiryForm.setNewRatingsList(newRatingsList);
			oNewEnquiryForm.setTotalRatingIndex(new Integer(1));
			
            if (request.getSession().getAttribute(QuotationConstants.QUOTATION_ATTDTO) == null) {
            	// get the attachment details and put them in session
                oAttachmentDto = AttachmentUtility.getApplicationData();
                if (oAttachmentDto != null) {
                	request.getSession().setAttribute(QuotationConstants.QUOTATION_ATTDTO, oAttachmentDto);
                	// Fetch FileTypes Token String - Set to NewEnquiryForm.
                	StringBuffer sbFileType = oNewEnquiryManager.fetchFileTypesString(oAttachmentDto);
                	oNewEnquiryForm.setFileTypesStr(sbFileType.toString());
                }
            } else {
            	oAttachmentDto = (AttachmentDto)request.getSession().getAttribute(QuotationConstants.QUOTATION_ATTDTO);
            	// Fetch FileTypes Token String - Set to NewEnquiryForm.
            	StringBuffer sbFileType = oNewEnquiryManager.fetchFileTypesString(oAttachmentDto);
				oNewEnquiryForm.setFileTypesStr(sbFileType.toString());
            }
            
            // "lastRatingNum", "prevIdVal" - Session Attributes.
            request.getSession().setAttribute(QuotationConstants.QUOTATION_LT_LASTRATINGNUM, new Integer(1));
            request.getSession().setAttribute(QuotationConstants.QUOTATION_LT_PREVIDVAL, new Integer(1));
            request.getSession().setAttribute(QuotationConstants.QUOTATION_LT_SHOWREASSIGN_FLAG, QuotationConstants.QUOTATION_STRING_N);
            
    	} catch(Exception e) {
    		/* Logging any exception that raised during this activity in log files using Log4j */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "home", ExceptionUtility.getStackTraceAsString(e));
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        } finally {
            /* Releasing/closing the connection object */
        	if (conn != null)
        		oDBUtility.releaseResources(conn);	
        }
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
    	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_VIEW);
    }
    
    /**
     * This method is used to create an enquiry.
     * @param actionMapping ActionMapping object to redirect the user to next page depending on the activity
     * @param actionForm ActionForm object to handle the form elements
     * @param request HttpServletRequest object to handle request operations
     * @param response HttpServletResponse object to handle response operations
     * @return returns ActionForward depending on which user is redirected to next page
     * @throws Exception if any error occurs while performing database operations, etc
     */
    public ActionForward createEnquiry(ActionMapping actionMapping,
                              ActionForm actionForm, 
                              HttpServletRequest request,
                              HttpServletResponse response) throws Exception {
    	
    	String sMethodName = QuotationConstants.QUOTATION_CINQ; 
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
    	Connection conn = null;  				// Connection object to store the database connection
        DBUtility oDBUtility = null;  			// Object of DBUtility class to handle DB connection objects
        UserDto oUserDto = null;
        String sDispatch = null;
        UserAction oUserAction=null; 			//Instantiating the UserAction object
        DaoFactory oDaoFactory=null; 			//Creating an instance of of DaoFactory & retrieving UserDao object
        QuotationDao oQuotationDao=null;		// Creating an Instance for QuotationDao
        AddOnDetailsDao oAddOnDetailsDao = null;
        NewEnquiryForm oNewEnquiryForm = null;
		NewEnquiryDto oNewEnquiryDto = null;
		WorkflowUtility oWorkflowUtility = null;
		WorkflowDto oWorkflowDto = null;
		WorkflowDao oWorkFlowDao = null;
        int iWorkflowExecuteStatus=0;
        boolean isUpdatedRating=QuotationConstants.QUOTATION_FALSE;
        String sMsg=null,sLinkMsg1=null,sLinkMsg2=null,sLinkMsg=null,sRatingResult=null,sResult=null;
        String sOperationType = QuotationConstants.QUOTATION_NONE; 
        
    	try {
    		setRolesToRequest(request);
            LoggerUtility.log("INFO", sClassName, sMethodName, "Roles set to request.");
            
    		oNewEnquiryForm = (NewEnquiryForm) actionForm;
    		if (oNewEnquiryForm == null)
    			oNewEnquiryForm = new NewEnquiryForm();
    		
    		sOperationType = oNewEnquiryForm.getOperationType();
    		sDispatch = oNewEnquiryForm.getDispatch(); 
    		
            /* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Instantiating the UserAction object */
            oUserAction = new UserAction();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
            
            oQuotationDao = oDaoFactory.getQuotationDao();
            oAddOnDetailsDao = oDaoFactory.getAddOnDetailsDao();
            
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
           
            if (! isTokenValid(request)) {
           	 /* Setting error details to request to display the same in Exception page */ 
               request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_INVALIDTOKENMESSAGE);
               /* Forwarding request to Error page */
               return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
            }          
            
            oDBUtility.setAutoCommitFalse(conn);
            
            oNewEnquiryDto = new NewEnquiryDto();
            NewEnquiryUtilityManager oNewEnquiryManager = new NewEnquiryUtilityManager();
            
            /* Retrieving the logged-in user information */
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            if (oUserDto == null){
            	oUserDto = new UserDto();
            }
            
            oNewEnquiryForm.setCreatedBy(oUserDto.getFullName());           
            oNewEnquiryForm.setCreatedBySSO(oUserDto.getUserId());
            PropertyUtils.copyProperties(oNewEnquiryDto, oNewEnquiryForm);  
            
            // Set the SM Note to DTO
            oNewEnquiryDto.setSmNote(CommonUtility.getStringParameter(request, "smNote"));
            
            LoggerUtility.log("INFO", sClassName, sMethodName, "iEnquiryId -  " + oNewEnquiryDto.getEnquiryId());
            
            if (sDispatch.equalsIgnoreCase(QuotationConstants.QUOTATION_VIEWSUBMIT)) {
            	
            	/* This block will be executed when the Enquiry is submitted from view-draft page. */
            	oNewEnquiryDto.setNewRatingsList(oQuotationDao.fetchNewRatingDetails(conn, oNewEnquiryDto));
            	
            	if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_LT_ABONDENT))  {
        			
        			PropertyUtils.copyProperties(oNewEnquiryDto, oNewEnquiryForm);
        			oNewEnquiryDto = oQuotationDao.getNewEnquiryDetailsForView(conn, oNewEnquiryDto, QuotationConstants.QUOTATION_FORWARD_VIEW.toUpperCase());
        			
        			PropertyUtils.copyProperties(oNewEnquiryForm, oNewEnquiryDto);
        			// Load ABONDENT Related Lists.
        			oNewEnquiryForm.setLtAbondentReasonList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_ABONDENT_REASON));
        			oNewEnquiryForm.setAlTargetedList(oQuotationDao.getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_TARGTD));
        			
                    return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_ABONDENT_VIEW);
                    
        		} // Ending Of ABONDENT
        		else if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_LT_LOST))  {
        			
        			PropertyUtils.copyProperties(oNewEnquiryDto, oNewEnquiryForm);
        			oNewEnquiryDto = oQuotationDao.getNewEnquiryDetailsForView(conn, oNewEnquiryDto, QuotationConstants.QUOTATION_FORWARD_VIEW.toUpperCase());
        			// Calculate TotalQuotedPrice
        			String sQuotedTotal = oNewEnquiryManager.calculateQuotedTotalPrice(oNewEnquiryDto);
        			oNewEnquiryDto.setTotalQuotedPrice(sQuotedTotal);
        			
        			PropertyUtils.copyProperties(oNewEnquiryForm, oNewEnquiryDto);
        			oNewEnquiryForm.setTotalRatingIndex(oNewEnquiryDto.getNewRatingsList().size());
        			// Load LOST Related Lists.
        			oNewEnquiryForm.setLtLostCompetitorList(oQuotationDao.getOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_LOSS_COMPETITOR));
        			oNewEnquiryForm.setLtLostReasonList(oQuotationDao.getOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_LOSS_REASON));
        			oNewEnquiryForm.setAlTargetedList(oQuotationDao.getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_TARGTD));
        			// Setting NewEnquiryDto to Request : For fetching Ratings List.
        			request.setAttribute(QuotationConstants.QUOTATION_LOWER_NEW_INQUIRYDTO, oNewEnquiryDto);
        			
        			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_LOST_VIEW);
        			
        		} // Ending of LOST
        		else if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_SUBMIT))  {
        			boolean isInsertUpdateComplete = false; 
        			request.setAttribute(QuotationConstants.QUOTATION_REQUEST_ENQUIRYID, ""+oNewEnquiryDto.getEnquiryId());
                	request.setAttribute(QuotationConstants.QUOTATION_REQUEST_ENQUIRYCODE, oNewEnquiryDto.getEnquiryNumber());
        			
                	// Fetch Rating Details from DB.
            		ArrayList<NewRatingDto> alRatingsList = oQuotationDao.fetchNewRatingDetails(conn, oNewEnquiryDto);
        			
        			// If DmUpdateStatus is Empty or "Return" : Verify Prices and Save Add-On's
        			if(StringUtils.isBlank(oNewEnquiryDto.getDmUpdateStatus()) || QuotationConstants.QUOTATION_DMUPDATE_RETURN.equalsIgnoreCase(oNewEnquiryDto.getDmUpdateStatus())) {
        				processCompleteRatings(conn, oNewEnquiryDto, oUserDto, alRatingsList, QuotationConstants.QUOTATION_SUBMIT);
        			}
        			
        			// Starting Of SUBMIT Workflow.
            		resetToken(request);
            		LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Initiating Workflow process - - -");
            		oWorkFlowDao = oDaoFactory.getWorkflowDao();
            		oWorkflowUtility = new WorkflowUtility();
            		oWorkflowDto = new WorkflowDto();
            		oWorkflowDto.setEnquiryId(oNewEnquiryDto.getEnquiryId());
            		oWorkflowDto.setEnquiryNo(oNewEnquiryDto.getEnquiryNumber());
            		oWorkflowDto.setLoginUserId(oUserDto.getUserId());
            		oWorkflowDto.setLoginUserName(oUserDto.getFullName());
            		oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
                    
            		// Check Enquiry "DM Update Status" 
            		// If "dmUpdateStatus" - Empty - New Enquiry.
            		// If "dmUpdateStatus" - "DMRETURN" - Return from DM - Check and Submit to DM / SM.
            		// If "dmUpdateStatus" - "DMSUBMIT" - Submit from DM - On Submit - Send to SM
            		if(StringUtils.isBlank(oNewEnquiryDto.getDmUpdateStatus())) {
            			// Check if NewEnquiry."IsReferDesign" is set to "Y" - Send to DM (else) Send to SM.
                		if(QuotationConstants.QUOTATION_STRING_Y.equalsIgnoreCase(oNewEnquiryDto.getIsReferDesign())) {
                			oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTODESIGNOFFICE);
                		} else if(StringUtils.isNotBlank(oNewEnquiryDto.getLiquidatedDamagesDueToDeliveryDelay()) && Integer.parseInt(oNewEnquiryDto.getLiquidatedDamagesDueToDeliveryDelay()) > 1 ) {
                			oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTOBH);
                		} else {
                			oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTOSM);
                		}
                		oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_DRAFT);
            		}
            		else if( QuotationConstants.QUOTATION_DMUPDATE_RETURN.equalsIgnoreCase(oNewEnquiryDto.getDmUpdateStatus())) {
            			// Returned Enquiry from DM - Set Different Action Code to avoid initiateWorkflow.
            			// Check if NewEnquiry."IsReferDesign" is set to "Y" - Send to DM (else) Send to SM.
            			if(QuotationConstants.QUOTATION_STRING_Y.equalsIgnoreCase(oNewEnquiryDto.getIsReferDesign())) {
                			oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_DMRETURNED_SUBMITTODM);
                		} else if(StringUtils.isNotBlank(oNewEnquiryDto.getLiquidatedDamagesDueToDeliveryDelay()) && Integer.parseInt(oNewEnquiryDto.getLiquidatedDamagesDueToDeliveryDelay()) > 1 ) {
                			oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_DMRETURNED_SUBMITTOBH);
                		} else {
                			oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_DMRETURNED_SUBMITTOSM);
                		}
                		oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_DRAFT);
            		}
            		else if( QuotationConstants.QUOTATION_DMUPDATE_SUBMIT.equalsIgnoreCase(oNewEnquiryDto.getDmUpdateStatus())) {
            			if(StringUtils.isNotBlank(oNewEnquiryDto.getLiquidatedDamagesDueToDeliveryDelay()) && Integer.parseInt(oNewEnquiryDto.getLiquidatedDamagesDueToDeliveryDelay()) > 1 ) {
            				oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTOBH);
            			} else {
            				oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTOSM);
            			}
            			oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_ReturnSM);
            		}
            		
            		LoggerUtility.log("INFO", sClassName, sMethodName, "2 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
            		iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
            		isUpdatedRating=oWorkflowUtility.updateRatingIsValidated(conn,oWorkFlowDao,oWorkflowDto);
                       
            		if ( iWorkflowExecuteStatus == QuotationConstants.QUOTATION_LITERAL_ONE ) {
            			LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFS);
            		} else  if ( iWorkflowExecuteStatus == QuotationConstants.QUOTATION_LITERAL_ZERO){ 
            			LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFSEMAILF);
            		} else {
            			LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFF);
            		}
            		
                    sMsg = QuotationConstants.QUOTATION_TRACKNO + oNewEnquiryDto.getEnquiryNumber();
                    
                    sLinkMsg1 = QuotationConstants.QUOTATION_LT_VALIDATEREQ_URL+oNewEnquiryDto.getEnquiryId()+QuotationConstants.QUOTATION_GRSYMBOL+QuotationConstants.QUOTATION_VIEWLINK; 
                    sLinkMsg2 = QuotationConstants.QUOTATION_LT_VIEWCR_ENQ_URL+QuotationConstants.QUOTATION_PLACELINKS; 
                    sLinkMsg = sLinkMsg1+QuotationConstants.QUOTATION_BREAK+sLinkMsg2;
                    
                    request.setAttribute(QuotationConstants.QUOTATION_HEADINGMESSAGE,QuotationConstants.QUOTATION_SUB_SUCES);
                    request.setAttribute(QuotationConstants.QUOTATION_SUCCESSMESSAGE,sMsg);
                    request.setAttribute(QuotationConstants.QUOTATION_LINKSMESSAGE,sLinkMsg);
                    
            		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);	
            		
        		} // Ending of 'SUBMIT' - View Mode.
        		else {
        			LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Initiating Workflow process - - -");
                    oWorkflowUtility = new WorkflowUtility();
                    oWorkflowDto = new WorkflowDto();               
                    oWorkflowDto.setEnquiryId(oNewEnquiryDto.getEnquiryId());
                    oWorkflowDto.setEnquiryNo(oNewEnquiryDto.getEnquiryNumber());
                    oWorkflowDto.setLoginUserId(oUserDto.getUserId());
                    oWorkflowDto.setLoginUserName(oUserDto.getFullName());
                    oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
                    
                    // Check "Requested Discount" Field for Each Rating.
                    boolean isDiscountRequested = oNewEnquiryManager.verifyRequestedDiscountForRatings(oNewEnquiryDto);
                    if(isDiscountRequested) {
                    	oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTORSM); 
                    } else {
                    	oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTOSM); 
                    }
                    
                    oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_DRAFT);
                    
                    LoggerUtility.log("INFO", sClassName, sMethodName, "1 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
                    iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
                    
                    oDBUtility.commit(conn);
            		oDBUtility.setAutoCommitTrue(conn);
            		
                    if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE ) {
                        LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFS);
                    } else  if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ZERO ){
                        LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFSEMAILF);
                    } else {
                        LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFF);
                    }
                    LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFPC);
                    
                    sMsg = QuotationConstants.QUOTATION_TRACKNO + oNewEnquiryDto.getEnquiryNumber();
                    
                    sLinkMsg1 = QuotationConstants.QUOTATION_LT_VALIDATEREQ_URL + oNewEnquiryDto.getEnquiryId()+QuotationConstants.QUOTATION_GRSYMBOL+QuotationConstants.QUOTATION_VIEWLINK; 
                    sLinkMsg2 = QuotationConstants.QUOTATION_LT_VIEWCR_ENQ_URL + QuotationConstants.QUOTATION_PLACELINKS; 
                    sLinkMsg = sLinkMsg1 + QuotationConstants.QUOTATION_BREAK + sLinkMsg2; 
                    
                    request.setAttribute(QuotationConstants.QUOTATION_HEADINGMESSAGE,QuotationConstants.QUOTATION_SUB_SUCES);
                    request.setAttribute(QuotationConstants.QUOTATION_SUCCESSMESSAGE,sMsg);
                    request.setAttribute(QuotationConstants.QUOTATION_LINKSMESSAGE,sLinkMsg);
                    
        			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);	
        		}
    			
            } else {            	
            	
            	boolean isInsertUpdateComplete = false; 
            	
            	// Fetching "lastRatingNum" - Session Attribute.
            	Integer iLastRatingNum = Integer.parseInt(CommonUtility.getStringParameter(request, "lastRatingNum"));
            	
            	// Fetch Location Id value.
            	if(CommonUtility.getStringParameter(request, "location") != null) {
            		oNewEnquiryDto.setLocationId(Integer.parseInt(CommonUtility.getStringParameter(request, "location")));
            	}
            	
            	if(iLastRatingNum == 1 && oNewEnquiryDto.getEnquiryId() == 0) {
            		oDBUtility.setAutoCommitFalse(conn);
            		oNewEnquiryDto = oQuotationDao.createNewEnquiry(conn, oNewEnquiryDto, oNewEnquiryForm.getEnquiryOperationType());
            		if (oNewEnquiryDto.getEnquiryId() > QuotationConstants.QUOTATION_LITERAL_ZERO) {
            			oNewEnquiryForm.setEnquiryId(oNewEnquiryDto.getEnquiryId());
            		}
            		// Save/Upload Attachments During Initial Enquiry Create operation.
            		oNewEnquiryManager.manageUploadAttachments(oNewEnquiryForm, oUserDto, request);
            		
            		oDBUtility.commit(conn);
                	oDBUtility.setAutoCommitTrue(conn);
            	}
            	
            	
            	/* Condition Check : Identify where to forward the request to
            	 *  if  sOperationType = 'SAVEASDRAFT' - then forward the request to view page
            	 *  if  sOperationType = 'ADDNEWRATING' - then forward the request to create page 
            	 *  if  sOperationType = 'SUBMIT' OR 'REFERDESIGN' - then forward the request to success page
            	 */
            	request.setAttribute(QuotationConstants.QUOTATION_REQUEST_ENQUIRYID, ""+oNewEnquiryDto.getEnquiryId());
            	request.setAttribute(QuotationConstants.QUOTATION_REQUEST_ENQUIRYCODE, oNewEnquiryDto.getEnquiryNumber());
            	
            	LoggerUtility.log("INFO", sClassName, sMethodName, "sOperationType -  "+sOperationType);
            	
            	if (sOperationType.equalsIgnoreCase("REFERDESIGN")) {
            		oDBUtility.setAutoCommitFalse(conn);
            		
            		if(oNewEnquiryDto.getEnquiryId() > 0) {
            			// Update Enquiry Details.
                		oNewEnquiryDto = oQuotationDao.updateNewEnquiry(conn, oNewEnquiryDto, oNewEnquiryForm.getEnquiryOperationType());
                		
            			if (oNewEnquiryDto.getEnquiryId() > 0) {
                			oDBUtility.commit(conn);
                			isInsertUpdateComplete = true;
                			// Save/Upload Attachments During "Save as Draft" operation.
                    		oNewEnquiryManager.manageUploadAttachments(oNewEnquiryForm, oUserDto, request);
                		} else {
                			oDBUtility.rollback(conn);
                			isInsertUpdateComplete = false;
                		}
            		}
            		oDBUtility.setAutoCommitTrue(conn);
            		
            		// Fetch Rating Details from NewEnquiryForm Request : Build Ratings List.
            		ArrayList<NewRatingDto> alRatingsList = oNewEnquiryManager.getRatingsList(conn, oQuotationDao, oNewEnquiryDto, request);
            		
            		// If DmUpdateStatus is Empty or "Return" : Verify Prices and Save Add-On's
        			if(StringUtils.isBlank(oNewEnquiryDto.getDmUpdateStatus()) || QuotationConstants.QUOTATION_DMUPDATE_RETURN.equalsIgnoreCase(oNewEnquiryDto.getDmUpdateStatus())) {
        				processCompleteRatings(conn, oNewEnquiryDto, oUserDto, alRatingsList, QuotationConstants.QUOTATION_REFERDESIGN);
        			}
        			
        			// For "Refer to Design" - By Default Set the "IsReferDesign" flag as "Y".
        			oNewEnquiryDto.setIsReferDesign(QuotationConstants.QUOTATION_STRING_Y);
        			oQuotationDao.updateSubmitToDesignFlag(conn, oNewEnquiryDto);
        			
        			// Starting of "Refer to Design" Workflow.
            		resetToken(request);
            		LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Initiating Workflow process - - -");
            		oWorkFlowDao = oDaoFactory.getWorkflowDao();
            		oWorkflowUtility = new WorkflowUtility();
            		oWorkflowDto = new WorkflowDto();
            		oWorkflowDto.setEnquiryId(oNewEnquiryDto.getEnquiryId());
            		oWorkflowDto.setEnquiryNo(oNewEnquiryDto.getEnquiryNumber());
            		oWorkflowDto.setLoginUserId(oUserDto.getUserId());
            		oWorkflowDto.setLoginUserName(oUserDto.getFullName());
            		oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
            		
            		// Check Enquiry "DM Update Status" 
            		// If "dmUpdateStatus" - Empty - New Enquiry.
            		// If "dmUpdateStatus" - "DMRETURN" - Return from DM - Check and Submit to DM / SM.
            		if(StringUtils.isBlank(oNewEnquiryDto.getDmUpdateStatus())) {
            			oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTODESIGNOFFICE);
            		}
            		else if( QuotationConstants.QUOTATION_DMUPDATE_RETURN.equalsIgnoreCase(oNewEnquiryDto.getDmUpdateStatus()) ) {
            			oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_DMRETURNED_SUBMITTODM);
            		}
            		oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_DRAFT);
            		
            		LoggerUtility.log("INFO", sClassName, sMethodName, "2 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
            		iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
            		isUpdatedRating=oWorkflowUtility.updateRatingIsValidated(conn,oWorkFlowDao,oWorkflowDto);
                       
            		if ( iWorkflowExecuteStatus == QuotationConstants.QUOTATION_LITERAL_ONE ) {
            			LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFS);
            		} else  if ( iWorkflowExecuteStatus == QuotationConstants.QUOTATION_LITERAL_ZERO){ 
            			LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFSEMAILF);
            		} else {
            			LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFF);
            		}
                    
                    LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFPC);
                    if(isUpdatedRating) {
                        LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_RATINGUS);
                    } else {
                        LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_RATINGUF);
                    }
                    LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_RATINGUC);

                    sMsg = QuotationConstants.QUOTATION_TRACKNO + oNewEnquiryDto.getEnquiryNumber();
                    
                    sLinkMsg1 = QuotationConstants.QUOTATION_LT_VALIDATEREQ_URL + oNewEnquiryDto.getEnquiryId() + QuotationConstants.QUOTATION_GRSYMBOL + QuotationConstants.QUOTATION_VIEWLINK; 
                    sLinkMsg2 = QuotationConstants.QUOTATION_LT_VIEWCR_ENQ_URL + QuotationConstants.QUOTATION_PLACELINKS; 
                    sLinkMsg = sLinkMsg1 + QuotationConstants.QUOTATION_BREAK + sLinkMsg2;
                    
                    request.setAttribute(QuotationConstants.QUOTATION_HEADINGMESSAGE,QuotationConstants.QUOTATION_SUB_SUCES);
                    request.setAttribute(QuotationConstants.QUOTATION_SUCCESSMESSAGE,sMsg);
                    request.setAttribute(QuotationConstants.QUOTATION_LINKSMESSAGE,sLinkMsg);
                    
            		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);	
            		
            	}// Ending Of REFERDESIGN
            	else if (sOperationType.equalsIgnoreCase("SAVEASDRAFT")) {
            		oDBUtility.setAutoCommitFalse(conn);
            		
            		if(oNewEnquiryDto.getEnquiryId() > 0) {
            			// Update Enquiry Details.
                		oNewEnquiryDto = oQuotationDao.updateNewEnquiry(conn, oNewEnquiryDto, oNewEnquiryForm.getEnquiryOperationType());
                		
            			if (oNewEnquiryDto.getEnquiryId() > 0) {
                			oDBUtility.commit(conn);
                			isInsertUpdateComplete = true;
                			// Save/Upload Attachments During "Save as Draft" operation.
                    		oNewEnquiryManager.manageUploadAttachments(oNewEnquiryForm, oUserDto, request);
                		} else {
                			oDBUtility.rollback(conn);
                			isInsertUpdateComplete = false;
                		}
            		}
            		
            		// Fetch Rating Details from NewEnquiryForm Request : Build Ratings List.
            		ArrayList<NewRatingDto> alRatingsList = oNewEnquiryManager.getRatingsList(conn, oQuotationDao, oNewEnquiryDto, request);
            		for(NewRatingDto oNewRatingDto : alRatingsList) {
            			// Fetch Prices / AddOns / MTOs.
            			oNewRatingDto.setFinalPriceCheck(true);
            			oNewEnquiryManager.calculateNewEnquiryPrices(oNewRatingDto);
            			// Save the NewRatingDto.
            			oNewRatingDto.setIsDesignComplete("N");
            			sResult = oQuotationDao.saveRatingDetails(conn, oNewRatingDto);
            			if (QuotationConstants.QUOTATION_STRING_Y.equals(sResult)) {
            				oDBUtility.commit(conn);
            			} else {
            				oDBUtility.rollback(conn);
            			}
            			// If "Refer Engineering" is "Y" for Current Rating - Set NewEnquiry."IsReferDesign" = true.
            			if( QuotationConstants.QUOTATION_STRING_Y.equalsIgnoreCase(oNewRatingDto.getIsReferDesign()) ) {
            				oNewEnquiryDto.setIsReferDesign(oNewRatingDto.getIsReferDesign());
            			}
            			// Save the Add-On's and MTO's Lists to DB.
            			oAddOnDetailsDao.updateAddOnMTODetails(conn, oNewRatingDto, oUserDto.getUserId());
            		}
            		// Save "Refer Engineering" Flag - After processing Ratings List.
            		oQuotationDao.updateSubmitToDesignFlag(conn, oNewEnquiryDto);
            		
            		oDBUtility.setAutoCommitTrue(conn);
            		
            		oNewEnquiryForm = oNewEnquiryManager.loadLookUpValues(conn, oNewEnquiryForm);
            		oNewEnquiryDto = oQuotationDao.loadNewEnquiryLookUpValues(conn, oNewEnquiryDto);
            		PropertyUtils.copyProperties(oNewEnquiryForm, oNewEnquiryDto);
            		
            		// Get the Attachment Files
            		request.setAttribute(QuotationConstants.QUOTATION_ATTACHLIST, AttachmentUtility.getNewEnquiryAttachmentDetails(oNewEnquiryDto));
            		request.setAttribute(QuotationConstants.QUOTATION_APPID, "" + QuotationConstants.QUOTATION_ATTACHMENT_APPLICATIONID);
            		
            		return new ActionForward (QuotationConstants.QUOTATION_LT_GET_ENQDET_URL + oNewEnquiryDto.getEnquiryId(),true);
            		
            	} // Ending Of SAVEASDRAFT
            	else if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_SUBMIT)) { 
            		oDBUtility.setAutoCommitFalse(conn);
            		
            		if(oNewEnquiryDto.getEnquiryId() > 0) {
            			// Update Enquiry Details.
                		oNewEnquiryDto = oQuotationDao.updateNewEnquiry(conn, oNewEnquiryDto, oNewEnquiryForm.getEnquiryOperationType());
                		
                		if (oNewEnquiryDto.getEnquiryId() > 0) {
                			isInsertUpdateComplete = true;
                			oDBUtility.commit(conn);
                			// Save/Upload Attachments During "Save as Draft" operation.
                    		oNewEnquiryManager.manageUploadAttachments(oNewEnquiryForm, oUserDto, request);
                		} else {
                			isInsertUpdateComplete = false;
            				oDBUtility.rollback(conn);
                		}
            		}
            		oDBUtility.setAutoCommitTrue(conn);
            		
            		// Fetch Rating Details from NewEnquiryForm Request : Build Ratings List.
            		ArrayList<NewRatingDto> alRatingsList = oNewEnquiryManager.getRatingsList(conn, oQuotationDao, oNewEnquiryDto, request);
        			
        			// If DmUpdateStatus is Empty or "Return" : Verify Prices and Save Add-On's
        			if(StringUtils.isBlank(oNewEnquiryDto.getDmUpdateStatus()) || QuotationConstants.QUOTATION_DMUPDATE_RETURN.equalsIgnoreCase(oNewEnquiryDto.getDmUpdateStatus())) {
        				processCompleteRatings(conn, oNewEnquiryDto, oUserDto, alRatingsList, QuotationConstants.QUOTATION_SUBMIT);
        			}
        			
        			// Starting Of SUBMIT Workflow.
            		resetToken(request);
            		LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Initiating Workflow process - - -");
            		oWorkFlowDao = oDaoFactory.getWorkflowDao();
            		oWorkflowUtility = new WorkflowUtility();
            		oWorkflowDto = new WorkflowDto();
            		oWorkflowDto.setEnquiryId(oNewEnquiryDto.getEnquiryId());
            		oWorkflowDto.setEnquiryNo(oNewEnquiryDto.getEnquiryNumber());
            		oWorkflowDto.setLoginUserId(oUserDto.getUserId());
            		oWorkflowDto.setLoginUserName(oUserDto.getFullName());
            		oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
                    
            		// Check Enquiry "DM Update Status" 
            		// If "dmUpdateStatus" - Empty - New Enquiry.
            		// If "dmUpdateStatus" - "DMRETURN" - Return from DM - Check and Submit to DM / SM.
            		// If "dmUpdateStatus" - "DMSUBMIT" - Submit from DM - On Submit - Send to SM
            		if(StringUtils.isBlank(oNewEnquiryDto.getDmUpdateStatus())) {
            			// Check if NewEnquiry."IsReferDesign" is set to "Y" - Send to DM (else) Send to SM.
                		if(QuotationConstants.QUOTATION_STRING_Y.equalsIgnoreCase(oNewEnquiryDto.getIsReferDesign())) {
                			oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTODESIGNOFFICE);
                		} else if(StringUtils.isNotBlank(oNewEnquiryDto.getLiquidatedDamagesDueToDeliveryDelay()) && Integer.parseInt(oNewEnquiryDto.getLiquidatedDamagesDueToDeliveryDelay()) > 1 ) {
                			oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTOBH);
                		} else {
                			oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTOSM);
                		}
                		oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_DRAFT);
            		}
            		else if( QuotationConstants.QUOTATION_DMUPDATE_RETURN.equalsIgnoreCase(oNewEnquiryDto.getDmUpdateStatus())) {
            			// Returned Enquiry from DM - Set Different Action Code to avoid initiateWorkflow.
            			// Check if NewEnquiry."IsReferDesign" is set to "Y" - Send to DM (else) Send to SM.
            			if(QuotationConstants.QUOTATION_STRING_Y.equalsIgnoreCase(oNewEnquiryDto.getIsReferDesign())) {
                			oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_DMRETURNED_SUBMITTODM);
                		} else if(StringUtils.isNotBlank(oNewEnquiryDto.getLiquidatedDamagesDueToDeliveryDelay()) && Integer.parseInt(oNewEnquiryDto.getLiquidatedDamagesDueToDeliveryDelay()) > 1 ) {
                			oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_DMRETURNED_SUBMITTOBH);
                		} else {
                			oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_DMRETURNED_SUBMITTOSM);
                		}
                		oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_DRAFT);
            		}
            		else if( QuotationConstants.QUOTATION_DMUPDATE_SUBMIT.equalsIgnoreCase(oNewEnquiryDto.getDmUpdateStatus())) {
            			if(StringUtils.isNotBlank(oNewEnquiryDto.getLiquidatedDamagesDueToDeliveryDelay()) && Integer.parseInt(oNewEnquiryDto.getLiquidatedDamagesDueToDeliveryDelay()) > 1 ) {
            				oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTOBH);
            			} else {
            				oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTOSM);
            			}
            			oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_ReturnSM);
            		}
            		
            		LoggerUtility.log("INFO", sClassName, sMethodName, "2 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
            		iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
            		isUpdatedRating=oWorkflowUtility.updateRatingIsValidated(conn,oWorkFlowDao,oWorkflowDto);
                       
            		if ( iWorkflowExecuteStatus == QuotationConstants.QUOTATION_LITERAL_ONE ) {
            			LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFS);
            		} else  if ( iWorkflowExecuteStatus == QuotationConstants.QUOTATION_LITERAL_ZERO){ 
            			LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFSEMAILF);
            		} else {
            			LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFF);
            		}
                    
                    LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFPC);
                    if(isUpdatedRating) {
                        LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_RATINGUS);
                    } else {
                        LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_RATINGUF);
                    }
                    LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_RATINGUC);

                    sMsg = QuotationConstants.QUOTATION_TRACKNO + oNewEnquiryDto.getEnquiryNumber();
                    
                    sLinkMsg1 = QuotationConstants.QUOTATION_LT_VALIDATEREQ_URL+oNewEnquiryDto.getEnquiryId()+QuotationConstants.QUOTATION_GRSYMBOL+QuotationConstants.QUOTATION_VIEWLINK; 
                    sLinkMsg2 = QuotationConstants.QUOTATION_LT_VIEWCR_ENQ_URL+QuotationConstants.QUOTATION_PLACELINKS; 
                    sLinkMsg = sLinkMsg1+QuotationConstants.QUOTATION_BREAK+sLinkMsg2;
                    
                    request.setAttribute(QuotationConstants.QUOTATION_HEADINGMESSAGE,QuotationConstants.QUOTATION_SUB_SUCES);
                    request.setAttribute(QuotationConstants.QUOTATION_SUCCESSMESSAGE,sMsg);
                    request.setAttribute(QuotationConstants.QUOTATION_LINKSMESSAGE,sLinkMsg);
                    
            		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);	
            			
            	} // Ending Of SUBMIT
            	else if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_ADDNEWRATING) || sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_COPYPREVIOUSRATING)) {
            		oDBUtility.setAutoCommitFalse(conn);
            		// Fetch Rating Details from NewEnquiryForm Request : Build Ratings List.
            		ArrayList<NewRatingDto> alRequestRatingsList = oNewEnquiryManager.getRatingsList(conn, oQuotationDao, oNewEnquiryDto, request);
            		for(NewRatingDto oNewRatingDto : alRequestRatingsList) {
            			// Fetch Prices / AddOns / MTOs.
            			oNewRatingDto.setFinalPriceCheck(false);
            			oNewEnquiryManager.calculateNewEnquiryPrices(oNewRatingDto);
            			// Save the NewRatingDto.
            			oNewRatingDto.setIsDesignComplete("N");
            			sResult = oQuotationDao.saveRatingDetails(conn, oNewRatingDto);
            			if (QuotationConstants.QUOTATION_STRING_Y.equals(sResult)) {
            				oDBUtility.commit(conn);
            			} else {
            				oDBUtility.rollback(conn);
            			}
            		}
            		
            		// -----------------  INSERT New Rating to Existing Enquiry's Ratings List.  --------------------------------
            		// Below Logic is for 'Add New Rating' and 'Copy & Add Previous Rating' Buttons.
            		
            		Integer iAddNewRatingNo = 0;
					NewRatingDto tempNewRatingDto = new NewRatingDto();
					// Set the EnquiryId from NewEnquiryDto.
            		tempNewRatingDto.setEnquiryId(oNewEnquiryDto.getEnquiryId());
					// Fetch Last Rating Number.
					Integer iLastRatingNo = oQuotationDao.fetchLastRatingNumber(conn, oNewEnquiryDto);
					// If sOperationType = "Copy & Add Rating" - then Fetch Last Rating - Copy its Details to the New Rating.
            		if(sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_COPYPREVIOUSRATING)) {
            			// Fetch Last Rating from Enquiry.
            			if(iLastRatingNo > 0) {
            				NewRatingDto lastRatingDto = oQuotationDao.fetchLastRating(conn, oNewEnquiryDto, iLastRatingNo);
            				PropertyUtils.copyProperties(tempNewRatingDto, lastRatingDto);
            			} else {
            				tempNewRatingDto = oQuotationDao.processNewRatingDefaults(conn, tempNewRatingDto);
            			}
            		}
            		// If sOperationType = "Add Rating" - Create New Rating with Default values.
            		if(sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_ADDNEWRATING)) {
            			// Set Default values for New Rating.
            			tempNewRatingDto = oQuotationDao.processNewRatingDefaults(conn, tempNewRatingDto);
            		}
            		
            		// Setting RatingId and RatingNo.
            		tempNewRatingDto.setRatingId(new Integer(0));
            		if(iLastRatingNo > 0) {
            			// Set RatingNo = iLastRatingNo+1.
            			iAddNewRatingNo = iLastRatingNo+1;
            			tempNewRatingDto.setRatingNo(iAddNewRatingNo);
            		} else {
            			// Set RatingNo = 1 (As it would be first Rating).
        				iAddNewRatingNo = 1;
            			tempNewRatingDto.setRatingNo(iAddNewRatingNo);
            		}
            		// Add tempNewRatingDto to the Enquiry - as New Rating.
        			sRatingResult = oQuotationDao.saveRatingByIdDetails(conn, tempNewRatingDto);
        			if (QuotationConstants.QUOTATION_STRING_Y.equals(sRatingResult)) {
        				oDBUtility.commit(conn);
        			} else {
        				oDBUtility.rollback(conn);
        			}
                    oDBUtility.setAutoCommitTrue(conn);
                    
        			// -----------------  INSERT New Rating to Existing Enquiry's Ratings List.  --------------------------------
            		
        			// Fetch the Ratings List : Assign to NewEnquiryDto.
            		ArrayList<NewRatingDto> alRatingsList = oQuotationDao.fetchNewRatingDetails(conn, oNewEnquiryDto);
            		oNewEnquiryDto.setNewRatingsList(alRatingsList);
                	
            		// "lastRatingNum", "prevIdVal" - Session Attributes.
                    request.getSession().setAttribute(QuotationConstants.QUOTATION_LT_LASTRATINGNUM, alRatingsList.size());
                    request.getSession().setAttribute(QuotationConstants.QUOTATION_LT_PREVIDVAL, iAddNewRatingNo);
                    
            		oNewEnquiryForm = oNewEnquiryManager.loadLookUpValues(conn, oNewEnquiryForm);
            		oNewEnquiryDto = oQuotationDao.loadNewEnquiryLookUpValues(conn, oNewEnquiryDto);
            		PropertyUtils.copyProperties(oNewEnquiryForm, oNewEnquiryDto);
            			
            		oNewEnquiryForm.setTotalRatingIndex(oNewEnquiryDto.getNewRatingsList().size());
            		oNewEnquiryForm.setOperationType(sOperationType);
            		oNewEnquiryForm.setEnquiryOperationType(QuotationConstants.QUOTATION_FORWARD_UPDATE.toUpperCase()); 
			            
            		// Get the Attachment Files
            		request.setAttribute(QuotationConstants.QUOTATION_ATTACHLIST, AttachmentUtility.getNewEnquiryAttachmentDetails(oNewEnquiryDto));
            		request.setAttribute(QuotationConstants.QUOTATION_APPID, "" + QuotationConstants.QUOTATION_ATTACHMENT_APPLICATIONID); 
            			
            		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_VIEW);
			            
            	} // Ending Of ADDNEWRATING and COPYPREVIOUSRATING
            	else if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_REMOVERATING)) {
            		oDBUtility.setAutoCommitFalse(conn);
            		// Fetch Rating Details from NewEnquiryForm Request : Build Ratings List.
            		ArrayList<NewRatingDto> alRequestRatingsList = oNewEnquiryManager.getRatingsList(conn, oQuotationDao, oNewEnquiryDto, request);
            		for(NewRatingDto oNewRatingDto : alRequestRatingsList) {
            			// Fetch Prices / AddOns / MTOs.
            			oNewRatingDto.setFinalPriceCheck(false);
            			oNewEnquiryManager.calculateNewEnquiryPrices(oNewRatingDto);
            			// Save the NewRatingDto.
            			oNewRatingDto.setIsDesignComplete("N");
            			sResult = oQuotationDao.saveRatingDetails(conn, oNewRatingDto);
            			if (QuotationConstants.QUOTATION_STRING_Y.equals(sResult)) {
            				oDBUtility.commit(conn);
            			} else {
            				oDBUtility.rollback(conn);
            			}
            		}
            		
            		// Remove/Delete the Rating 
            		Integer iRemoveRatingNo = Integer.parseInt(CommonUtility.getStringParameter(request, "prevIdVal"));
            		Integer iEnquiryId = Integer.parseInt(CommonUtility.getStringParameter(request, "enquiryId"));
            		sRatingResult = oQuotationDao.removeLastRatingFromNewEnquiry(conn, iEnquiryId, iRemoveRatingNo);
            		if (QuotationConstants.QUOTATION_STRING_Y.equals(sRatingResult)) {
        				oDBUtility.commit(conn);
        			} else {
        				oDBUtility.rollback(conn);
        			}
            		oDBUtility.setAutoCommitTrue(conn);
            		
            		// Fetch the Ratings List : Assign to NewEnquiryDto.
            		ArrayList<NewRatingDto> alRatingsList = oQuotationDao.fetchNewRatingDetails(conn, oNewEnquiryDto);
            		oNewEnquiryDto.setNewRatingsList(alRatingsList);
            		// Fetch Last Rating Number.
					Integer iLastRatingNo = oQuotationDao.fetchLastRatingNumber(conn, oNewEnquiryDto);
					
            		// "lastRatingNum", "prevIdVal" - Session Attributes.
                    request.getSession().setAttribute(QuotationConstants.QUOTATION_LT_LASTRATINGNUM, alRatingsList.size());
                    request.getSession().setAttribute(QuotationConstants.QUOTATION_LT_PREVIDVAL, iLastRatingNo);
                    
            		oNewEnquiryForm = oNewEnquiryManager.loadLookUpValues(conn, oNewEnquiryForm);
            		oNewEnquiryDto = oQuotationDao.loadNewEnquiryLookUpValues(conn, oNewEnquiryDto);
            		PropertyUtils.copyProperties(oNewEnquiryForm, oNewEnquiryDto);
            			
            		oNewEnquiryForm.setTotalRatingIndex(oNewEnquiryDto.getNewRatingsList().size());
            		oNewEnquiryForm.setOperationType(sOperationType);
            		oNewEnquiryForm.setEnquiryOperationType(QuotationConstants.QUOTATION_FORWARD_UPDATE.toUpperCase()); 
			            
            		// Get the Attachment Files
            		request.setAttribute(QuotationConstants.QUOTATION_ATTACHLIST, AttachmentUtility.getNewEnquiryAttachmentDetails(oNewEnquiryDto));
            		request.setAttribute(QuotationConstants.QUOTATION_APPID, "" + QuotationConstants.QUOTATION_ATTACHMENT_APPLICATIONID); 
            			
            		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_VIEW);
            			
            	} // Ending Of REMOVERATING
            	
            	
            	if(!isInsertUpdateComplete) {
            		request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE,QuotationConstants.QUOTATION_ER_INSERT);
            		
            		// Forwarding request to Error page 
            		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
            	}
            } // Else Part Ending
           
    	} catch(Exception e) {
            /* Logging any exception that raised during this activity in log files using Log4j */
            LoggerUtility.log("DEBUG", sClassName, "home", ExceptionUtility.getStackTraceAsString(e));
            //e.printStackTrace();
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        } finally {
            /* Releasing/closing the connection object */
            if ( conn!=null )
                oDBUtility.releaseResources(conn);
        }
    	
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
    	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_HOME);
    }
    
    /**
     * This method is used to get the enquiry and rating details based on enquiryId.
     * @param actionMapping 	ActionMapping object to redirect the user to next page depending on the activity
     * @param actionForm 		ActionForm object to handle the form elements
     * @param request 			HttpServletRequest object to handle request operations
     * @param response 			HttpServletResponse object to handle response operations
     * @return 					returns ActionForward depending on which user is redirected to next page
     * @throws 					Exception if any error occurs while performing database operations, etc
     */
    public ActionForward getEnquiryDetails(ActionMapping actionMapping,
    		ActionForm actionForm, 
    		HttpServletRequest request,
    		HttpServletResponse response) throws Exception {
    	
    	String sMethodName = QuotationConstants.QUOTATION_GETINQDET;
    	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
    	
    	Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        String sOperationType = null;
        String sEnquiryCopied = null;
        String sForwardView = null;
        DaoFactory oDaoFactory = null;
        QuotationDao oQuotationDao = null;
        NewEnquiryDto oNewEnquiryDto = null;
        NewEnquiryForm oNewEnquiryForm = null;
        AttachmentDto oAttachmentDto = null;
        
    	try {
    		setRolesToRequest(request);
            LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "Roles set to request.");
            
    		oNewEnquiryForm = (NewEnquiryForm) actionForm;
    		if (oNewEnquiryForm == null)
    			oNewEnquiryForm = new NewEnquiryForm();
    		
            sEnquiryCopied = CommonUtility.getStringParameter(request, QuotationConstants.QUOTATION_COPY); 
            if (sEnquiryCopied.trim().length() > QuotationConstants.QUOTATION_LITERAL_ZERO) 
            	request.setAttribute(QuotationConstants.QUOTATION_COPY, QuotationConstants.QUOTATION_STRING_Y); 
            
    		sOperationType = CommonUtility.getStringParameter(request, QuotationConstants.QUOTATION_TYPE); 
    		if (sOperationType.equalsIgnoreCase(""))
    			sOperationType = oNewEnquiryForm.getOperationType();
    		
    		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "sOperationType == "+sOperationType);
    		            
    		oNewEnquiryForm.setEnquiryId(CommonUtility.getIntParameter(request, QuotationConstants.QUOTATION_REQUEST_ENQUIRYID));
            
            if (oNewEnquiryForm.getEnquiryId() < QuotationConstants.QUOTATION_LITERAL_ONE){
            	 /* Setting error details to request to display the same in Exception page */
                request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_CRET_ROWS);
                /* Forwarding request to Error page */
                return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);            	
            }
            
            /* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
    		
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            
            oNewEnquiryDto = new NewEnquiryDto();
            NewEnquiryUtilityManager oNewEnquiryManager = new NewEnquiryUtilityManager();
            
            PropertyUtils.copyProperties(oNewEnquiryDto, oNewEnquiryForm);          
            oQuotationDao = oDaoFactory.getQuotationDao();
            
            oNewEnquiryDto = oQuotationDao.getNewEnquiryDetailsForView(conn, oNewEnquiryDto, QuotationConstants.QUOTATION_FORWARD_VIEW.toUpperCase());           
            PropertyUtils.copyProperties(oNewEnquiryForm, oNewEnquiryDto);
            oNewEnquiryForm.setTotalRatingIndex(oNewEnquiryDto.getNewRatingsList().size());
            
            request.setAttribute(QuotationConstants.QUOTATION_LOWER_NEW_INQUIRYDTO, oNewEnquiryDto);
            
            /*
             * Build the rating error message - submit button has to be submitted only when this message is empty
             * as this validation has to be done for both edit and view - the below method call has been placed outside the edit condition
             */ 
            oNewEnquiryForm = buildRatingErrorMessage(oNewEnquiryForm); 
            
            if (request.getSession().getAttribute(QuotationConstants.QUOTATION_ATTDTO) == null) {
            	// get the attachment details and put them in session
                oAttachmentDto = AttachmentUtility.getApplicationData();
                if (oAttachmentDto != null) {
                	request.getSession().setAttribute(QuotationConstants.QUOTATION_ATTDTO, oAttachmentDto);
                	// Fetch FileTypes Token String - Set to NewEnquiryForm.
                	StringBuffer sbFileType = oNewEnquiryManager.fetchFileTypesString(oAttachmentDto);
                	oNewEnquiryForm.setFileTypesStr(sbFileType.toString());
                }
            } else {
            	oAttachmentDto = (AttachmentDto)request.getSession().getAttribute(QuotationConstants.QUOTATION_ATTDTO);
            	// Fetch FileTypes Token String - Set to NewEnquiryForm.
            	StringBuffer sbFileType = oNewEnquiryManager.fetchFileTypesString(oAttachmentDto);
				oNewEnquiryForm.setFileTypesStr(sbFileType.toString());
            }
            // Get the Attachment Files
			request.setAttribute(QuotationConstants.QUOTATION_ATTACHLIST, AttachmentUtility.getNewEnquiryAttachmentDetails(oNewEnquiryDto));
			request.setAttribute(QuotationConstants.QUOTATION_APPID, "" + QuotationConstants.QUOTATION_ATTACHMENT_APPLICATIONID);
           
            saveToken(request);
            // Display Re-assign Section : Set Default to 'N'.
            request.getSession().setAttribute(QuotationConstants.QUOTATION_LT_SHOWREASSIGN_FLAG, QuotationConstants.QUOTATION_STRING_N);
            
            // Write Method to Load ArrayList<NewRatingDto> to NewEnquiryForm
            
            if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_EDIT.toUpperCase()))  {
            	
            	if(oNewEnquiryDto.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGMFG) {
            		oNewEnquiryDto = oQuotationDao.getNewEnquiryDetailsForView(conn, oNewEnquiryDto, QuotationConstants.QUOTATION_FORWARD_VIEW.toUpperCase());
            		
            	} // EDIT OPERATION
            	else {
            		oNewEnquiryDto = oQuotationDao.getNewEnquiryDetailsForView(conn, oNewEnquiryDto, sOperationType);
            	}
            	
            	oNewEnquiryForm.setEnquiryOperationType(QuotationConstants.QUOTATION_FORWARD_UPDATE.toUpperCase());
            	oNewEnquiryForm.setDispatch(QuotationConstants.QUOTATION_EDIT);  
            	oNewEnquiryForm.setReturnstatement(QuotationConstants.QUOTATION_EDIT);
            	
            	oNewEnquiryDto = oQuotationDao.loadNewEnquiryLookUpValues(conn, oNewEnquiryDto);
	            PropertyUtils.copyProperties(oNewEnquiryForm, oNewEnquiryDto);
	            oNewEnquiryForm = oNewEnquiryManager.loadLookUpValues(conn, oNewEnquiryForm);
	            
	            oNewEnquiryForm.setTotalRatingIndex(oNewEnquiryDto.getNewRatingsList().size());
	            oNewEnquiryForm.setCreatedBy(oNewEnquiryForm.getCreatedBy());
				oNewEnquiryForm.setCreatedBySSO(oNewEnquiryForm.getCreatedBySSO());
				oNewEnquiryForm.setCreatedDate(oNewEnquiryForm.getCreatedDate());	
				// For Edit Enquiry Page.
				oNewEnquiryForm.setOperationType("EDITENQUIRY");
				// Set Sales Team Re-assign Users List
				oNewEnquiryForm.setSalesReassignUsersList(oQuotationDao.fetchSalesReassignUsersList(conn));
				
				// "lastRatingNum", "prevIdVal" - Session Attributes.
                request.getSession().setAttribute(QuotationConstants.QUOTATION_LT_LASTRATINGNUM, oNewEnquiryDto.getNewRatingsList().size());
                request.getSession().setAttribute(QuotationConstants.QUOTATION_LT_PREVIDVAL, oNewEnquiryDto.getNewRatingsList().size());
                if(oNewEnquiryDto.getStatusId() != QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGBH 
                		&& oNewEnquiryDto.getStatusId() != QuotationConstants.QUOTATION_WORKFLOW_STATUS_REGRET_PENDINGSM) {
                	request.getSession().setAttribute(QuotationConstants.QUOTATION_LT_SHOWREASSIGN_FLAG, QuotationConstants.QUOTATION_STRING_Y);
                }
                
                // Build Forward View Details.
                if(oNewEnquiryDto.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGMFG) {
                	sForwardView = QuotationConstants.QUOTATION_FORWARD_UPDATE_PLANTDETAILS_VIEW;
                } else {
                	sForwardView = QuotationConstants.QUOTATION_FORWARD_VIEW;
                }
                
	            return actionMapping.findForward(sForwardView);
	            
            } else  if(sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_RETURNFROMDM)) {
            	
            	oNewEnquiryDto = oQuotationDao.getNewEnquiryDetailsForView(conn, oNewEnquiryDto, sOperationType);
            	
            	oNewEnquiryForm.setEnquiryOperationType(QuotationConstants.QUOTATION_FORWARD_UPDATE.toUpperCase());
            	oNewEnquiryForm.setDispatch(QuotationConstants.QUOTATION_EDIT); 
            	oNewEnquiryForm.setReturnstatement(QuotationConstants.QUOTATION_RETURNDM);
            	oNewEnquiryForm.setReturnstatus(QuotationConstants.QUOTATION_RET_REPSM);
            	
            	oNewEnquiryDto = oQuotationDao.loadNewEnquiryLookUpValues(conn, oNewEnquiryDto);
	            PropertyUtils.copyProperties(oNewEnquiryForm, oNewEnquiryDto);
	            oNewEnquiryForm = oNewEnquiryManager.loadLookUpValues(conn, oNewEnquiryForm);
	            
	            oNewEnquiryForm.setTotalRatingIndex(oNewEnquiryDto.getNewRatingsList().size());
	            oNewEnquiryForm.setCreatedBy(oNewEnquiryForm.getCreatedBy());
				oNewEnquiryForm.setCreatedBySSO(oNewEnquiryForm.getCreatedBySSO());
				oNewEnquiryForm.setCreatedDate(oNewEnquiryForm.getCreatedDate());	
				
	            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_VIEW);
            }

    	} catch(Exception e) {
            /* Logging any exception that raised during this activity in log files using Log4j */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "home", ExceptionUtility.getStackTraceAsString(e));
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        } finally {
            /* Releasing/closing the connection object */
            if ( conn!=null )
                oDBUtility.releaseResources(conn);
        }
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
        
    	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_VIEWENQUIRY);
    }
    
    /**
     * This method is used to get the Revision Enquiry and Rating details based on enquiryId.
     * @param actionMapping 	ActionMapping object to redirect the user to next page depending on the activity
     * @param actionForm 		ActionForm object to handle the form elements
     * @param request 			HttpServletRequest object to handle request operations
     * @param response 			HttpServletResponse object to handle response operations
     * @return 					returns ActionForward depending on which user is redirected to next page
     * @throws 					Exception if any error occurs while performing database operations, etc
     */
    public ActionForward getRevisionEnquiryDetails(ActionMapping actionMapping,
            ActionForm actionForm, 
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {

    	String sMethodName = "getRevisionEnquiryDetails";
    	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
    	
    	Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        String sOperationType = null;
        String sEnquiryCopied = null;
        DaoFactory oDaoFactory = null;
        QuotationDao oQuotationDao = null;
        UserAction oUserAction = null;
        UserDto oUserDto = null;
        WorkflowDto oWorkflowDto = null;
        NewEnquiryDto oNewEnquiryDto = null;
        NewEnquiryForm oNewEnquiryForm = null;
        boolean isValidUser = QuotationConstants.QUOTATION_FALSE;
        
    	try {
    		setRolesToRequest(request);
            LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "Roles set to request.");
            
            oNewEnquiryForm = (NewEnquiryForm)actionForm;
    	    if(oNewEnquiryForm == null)
    	    	oNewEnquiryForm = new NewEnquiryForm();
    	    // Get the enquiryId from the request and set to the form object
            oNewEnquiryForm.setEnquiryId(CommonUtility.getIntParameter(request, QuotationConstants.QUOTATION_REQUEST_ENQUIRYID));
            
            /* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Instantiating the UserAction object */
            oUserAction = new UserAction();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
            /* Retrieving the QuotationDao object */
            oQuotationDao = oDaoFactory.getQuotationDao();
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            /* Retrieving the logged-in user information */
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            
            PropertyUtils.copyProperties(oNewEnquiryDto, oNewEnquiryForm);
            oNewEnquiryDto = oQuotationDao.getNewEnquiryDetailsForView(conn, oNewEnquiryDto, QuotationConstants.QUOTATION_FORWARD_VIEW.toUpperCase());
            
            oWorkflowDto = new WorkflowDto();
 		   	oWorkflowDto.setEnquiryId(oNewEnquiryForm.getEnquiryId());
 		   	oWorkflowDto.setAssignedTo(oUserDto.getUserId());
 		   	oWorkflowDto.setWorkflowStatus(QuotationConstants.QUOTATION_WORKFLOW_ACTION_PENDING);
 		   	
 		   	EnquiryDto oEnquiryDto = new EnquiryDto();
			PropertyUtils.copyProperties(oEnquiryDto, oNewEnquiryDto);
			
 		   	// If Status Id = (7) && If Enquiry is for Technical Revision - Verify User - Forward to Design.
 		   	// If Status Id = (7) && If Enquiry is for Commercial Revision - Verify User - Forward to SM.
            if ( oNewEnquiryDto.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_SUPERSEDED 
            		&& QuotationConstants.QUOTATION_WORKFLOW_REVISION_TECHNICAL.equalsIgnoreCase(oNewEnquiryDto.getRemarks())) {
            	
            	isValidUser = oQuotationDao.checkUserRole(conn, oEnquiryDto, oUserDto, QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_DM);
            	
            	if(isValidUser) {
            		// Check For Pending Workflow Record for Commercial Revision - For SM.
            		// Insert / Update Pending Workflow Record - Against SM and Forward View to SM.
            		
            		return new ActionForward(QuotationConstants.QUOTATION_LT_VIEWOFFERD_URL + oNewEnquiryForm.getEnquiryId());
            	} else {
            		return new ActionForward(QuotationConstants.QUOTATION_LT_VIEWOFFCONSOL_URL + oNewEnquiryForm.getEnquiryId());
            	}
            }
            else if ( oNewEnquiryDto.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_SUPERSEDED 
            		&& QuotationConstants.QUOTATION_WORKFLOW_REVISION_COMMERCIAL.equalsIgnoreCase(oNewEnquiryDto.getRemarks())) {
            	
            	isValidUser = oQuotationDao.checkUserRole(conn, oEnquiryDto, oUserDto, QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_SM);
            	
            	if(isValidUser) {
            		// Check For Pending Workflow Record for Commercial Revision - For SM.
            		// Insert / Update Pending Workflow Record - Against SM and Forward View to SM.
            		
            		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_SMAPPROVE);
            	} else {
            		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_VIEWQUOTATION);
            	}
            }
            
    	} catch(Exception e) {
            /* Logging any exception that raised during this activity in log files using Log4j */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "home", ExceptionUtility.getStackTraceAsString(e));
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        } finally {
            /* Releasing/closing the connection object */
            if ( conn!=null )
                oDBUtility.releaseResources(conn);
        }
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
    	
        /* Forwarding request to Error page */
        return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
    }
    
    public ActionForward deleteDraftEnquiry(ActionMapping actionMapping,
            ActionForm actionForm, 
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
    	
    	String sMethodName = QuotationConstants.QUOTATION_LT_DELETEDRAFT_ENQ;
    	Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        
        String sResult = QuotationConstants.QUOTATION_STRING_N;
        int iEnquiryId = QuotationConstants.QUOTATION_LITERAL_ZERO;
        UserAction oUserAction = null; //Instantiating the UserAction object
        DaoFactory oDaoFactory = null; //Creating an instance of of DaoFactory & retrieving UserDao object
		QuotationDao oQuotationDao = null;
		String sMsg=null,sLinkMsg1=null,sLinkMsg2=null,sLinkMsg=null,sCommentOperation,sDmComment=null,sDeComment=null,sRatingResult=null;
		
        try{
        	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
    		setRolesToRequest(request);
            LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "Roles set to request."); 
            
            /* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Instantiating the UserAction object */
            oUserAction = new UserAction();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
    		
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();  
            
            oQuotationDao = oDaoFactory.getQuotationDao();
            
            iEnquiryId = CommonUtility.getIntParameter(request, QuotationConstants.QUOTATION_REQUEST_ENQUIRYID);
            
            NewEnquiryDto oNewEnquiryDto = new NewEnquiryDto();
            oNewEnquiryDto.setEnquiryId(iEnquiryId);
            oNewEnquiryDto = oQuotationDao.getNewEnquiryDetailsForView(conn, oNewEnquiryDto, QuotationConstants.QUOTATION_FORWARD_VIEW.toUpperCase());
            
            /* Delete New Ratings and Enquiry Details. */
            sResult = oQuotationDao.deleteDraftEnquiryById(conn, iEnquiryId);
            
            if(QuotationConstants.QUOTATION_STRING_N.equalsIgnoreCase(sResult)) {
                request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE,QuotationConstants.QUOTATION_ER_DELETE);
        		
        		// Forwarding request to Error page 
        		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
            } else {
            	// Build the Success Message
                sMsg = QuotationConstants.QUOTATION_DELETED_TRACKNO + oNewEnquiryDto.getEnquiryNumber();
                
            	sLinkMsg1 = QuotationConstants.QUOTATION_RETURNHOME_URL + QuotationConstants.QUOTATION_GRSYMBOL + QuotationConstants.QUOTATION_VIEWLINK_RETURNHOME; 
                sLinkMsg2 = QuotationConstants.QUOTATION_LT_VIEWCR_ENQ_URL + QuotationConstants.QUOTATION_PLACELINKS; 
                sLinkMsg = sLinkMsg1 + QuotationConstants.QUOTATION_BREAK + sLinkMsg2; 
                
                request.setAttribute(QuotationConstants.QUOTATION_HEADINGMESSAGE,QuotationConstants.QUOTATION_SUB_SUCES);
                request.setAttribute(QuotationConstants.QUOTATION_SUCCESSMESSAGE,sMsg);
                request.setAttribute(QuotationConstants.QUOTATION_LINKSMESSAGE,sLinkMsg);
            }
            
        } catch(Exception e) {
            /* Logging any exception that raised during this activity in log files using Log4j */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "deleteDraftEnquiry", ExceptionUtility.getStackTraceAsString(e));
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        } finally {
            /* Releasing/closing the connection object */
            if ( conn!=null )
                oDBUtility.releaseResources(conn);
        }
        
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
        
        return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);
    }
    
    /**
     * This method is used to delete the rating object 
     * @param actionMapping ActionMapping object to redirect the user to next page depending on the activity
     * @param actionForm ActionForm object to handle the form elements 
     * @param request HttpServletRequest object to handle request operations
     * @param response HttpServletResponse object to handle response operations
     * @return returns ActionForward depending on which user is redirected to next page
     * @throws Exception if any error occurs while performing database operations, etc
     */
    public ActionForward deleteRating(ActionMapping actionMapping,
                              ActionForm actionForm, 
                              HttpServletRequest request,
                              HttpServletResponse response) throws Exception {
    	
    	String sMethodName = QuotationConstants.QUOTATION_DELRATING;
    	Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        
        boolean isRatingDeleted = QuotationConstants.QUOTATION_FALSE;
    	int iEnquiryId = QuotationConstants.QUOTATION_LITERAL_ZERO;
		int iRatingId = QuotationConstants.QUOTATION_LITERAL_ZERO;
		UserAction oUserAction = null; //Instantiating the UserAction object
		DaoFactory oDaoFactory =null; //Creating an instance of of DaoFactory & retrieving UserDao object
		QuotationDao oQuotationDao =null;
		RatingDto oRatingDto =null;
		EnquiryForm oNewEnquiryForm =null;
       
    	try{
    		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
    		setRolesToRequest(request);
            LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "Roles set to request."); 
            
    		oNewEnquiryForm = (EnquiryForm) actionForm;
    		if (oNewEnquiryForm == null)
    			oNewEnquiryForm = new EnquiryForm();
    		
    		iEnquiryId = CommonUtility.getIntParameter(request, QuotationConstants.QUOTATION_REQUEST_ENQUIRYID);
    		iRatingId = CommonUtility.getIntParameter(request, QuotationConstants.QUOTATION_REQUEST_RATINGID);
    		
    		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "iEnquiryId == "+iEnquiryId);
    		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "iRatingId == "+iRatingId);
    		
    		/* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Instantiating the UserAction object */
            oUserAction = new UserAction();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
    		
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();  
            
            oQuotationDao = oDaoFactory.getQuotationDao();
            
            /*if (iRatingId > QuotationConstants.QUOTATION_LITERAL_ZERO && iEnquiryId > QuotationConstants.QUOTATION_LITERAL_ZERO) {
            	oRatingDto = new RatingDto();
            	oRatingDto.setRatingId(iRatingId);
            	
            	isRatingDeleted = oQuotationDao.deleteRating(conn, oRatingDto);
            	
            	if (!isRatingDeleted) {
            		// Setting error details to request to display the same in Exception page 
                    request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_RAT_NOTDEL);
                    
                    // Forwarding request to Error page 
                    return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);  
            	}
            }else{
            	// Setting error details to request to display the same in Exception page 
                request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_RAT_NOTDEL);
                
                // Forwarding request to Error page 
                return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);            	
            }*/
    	} catch(Exception e) {
            /*
             * Logging any exception that raised during this activity in log files using Log4j
             */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "home", ExceptionUtility.getStackTraceAsString(e));
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        } finally {
            /* Releasing/closing the connection object */
            if ( conn!=null )
                oDBUtility.releaseResources(conn);
        }
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
        
        return new ActionForward(QuotationConstants.QUOTATION_GET_ENQDET_URL+iEnquiryId); 
    }
    
    public ActionForward validateRequestAction(ActionMapping actionMapping,
            ActionForm actionForm, 
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
    	
    	String sMethodName = QuotationConstants.QUOTATION_VALREQACT;
	    String sClassName = this.getClass().getName();
	    LoggerUtility.log("INFO", sClassName, sMethodName, "START");
	    
	    Connection conn = null;  // Connection object to store the database connection
	    DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
	    
	    UserDto oUserDto = null;
        NewEnquiryDto oNewEnquiryDto = null;
        WorkflowDto oWorkflowDto = null;
        UserAction oUserAction =null;
        DaoFactory oDaoFactory =null;
        QuotationDao oQuotationDao =null;
        boolean isValidUser = QuotationConstants.QUOTATION_FALSE;
        int iIndex =QuotationConstants.QUOTATION_LITERAL_ZERO;
        String sPreviousId=null;
        String sPreviousStatus=null;
        StringBuffer sBuffer = null;
        String sNextId=null;
        String sNextStatus=null;
        ArrayList arlSessionList =new ArrayList();
        
        try {
        	HttpSession session = request.getSession();
        	setRolesToRequest(request);
            LoggerUtility.log("INFO", sClassName, sMethodName, "Roles set to request.");
            
            NewEnquiryForm oNewEnquiryForm = (NewEnquiryForm)actionForm;
    	    if(oNewEnquiryForm == null)
    	    	oNewEnquiryForm = new NewEnquiryForm();
    	    
            /* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Instantiating the UserAction object */
            oUserAction = new UserAction();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
            /* Retrieving the QuotationDao object */
            oQuotationDao = oDaoFactory.getQuotationDao();
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            
            /* Retrieving the logged-in user information */
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            
            // get the enquiryId, statusId from the request and set to the form object
            oNewEnquiryForm.setEnquiryId(CommonUtility.getIntParameter(request, QuotationConstants.QUOTATION_REQUEST_ENQUIRYID));
            oNewEnquiryForm.setStatusId(CommonUtility.getIntParameter(request, QuotationConstants.QUOTATION_REQUEST_STATUSID)); 
            
            oNewEnquiryDto = new NewEnquiryDto();
            PropertyUtils.copyProperties(oNewEnquiryDto, oNewEnquiryForm);
            
            oWorkflowDto = new WorkflowDto();
 		   	oWorkflowDto.setEnquiryId(oNewEnquiryForm.getEnquiryId());
 		   	oWorkflowDto.setAssignedTo(oUserDto.getUserId());
 		   	oWorkflowDto.setWorkflowStatus(QuotationConstants.QUOTATION_WORKFLOW_ACTION_PENDING);
 		   
 		   	if (oNewEnquiryDto.getStatusId() == QuotationConstants.QUOTATION_LITERAL_ZERO ){
 		   		oNewEnquiryDto = oQuotationDao.getNewEnquiryStatus(conn, oNewEnquiryDto);		
 		   		oNewEnquiryForm.setStatusId(oNewEnquiryDto.getStatusId());
 		   	}
 		   	
 		   	arlSessionList = (ArrayList) session.getAttribute(QuotationConstants.QUOTATION_LIST_RESULTSLIST);
 		   	
			if (arlSessionList != null && CommonUtility.getStringParameter(request, QuotationConstants.QUOTATION_INDEX) != null) {
				// display the paging part only if the below attribute exists in the request
				request.setAttribute(QuotationConstants.QUOTATION_PAGING_DISPLAYPAGING, QuotationConstants.QUOTATION_SESSION_ADMIN_FLAG_TRUE);

				iIndex = CommonUtility.getIntParameter(request, QuotationConstants.QUOTATION_INDEX);
				request.setAttribute(QuotationConstants.QUOTATION_PAGING_CURRENTINDEX, "" + iIndex);
				if (iIndex > QuotationConstants.QUOTATION_LITERAL_ZERO && arlSessionList.get(iIndex - 1) != null) {
					sPreviousId = "" + ((SearchEnquiryDto) arlSessionList.get(iIndex - 1)).getEnquiryId();
					sPreviousStatus = "" + ((SearchEnquiryDto) arlSessionList.get(iIndex - 1)).getStatusId();
					request.setAttribute(QuotationConstants.QUOTATION_PAGING_PREVIOUSID, sPreviousId);
					request.setAttribute(QuotationConstants.QUOTATION_PRE_STA, sPreviousStatus);
					request.setAttribute(QuotationConstants.QUOTATION_PAGING_PREVIOUSINDEX, "" + (iIndex - 1));
				}

				sBuffer = new StringBuffer();
				sBuffer.append("Request ");
				sBuffer.append(iIndex + 1);
				sBuffer.append(" / ");
				sBuffer.append(arlSessionList.size());
				request.setAttribute(QuotationConstants.QUOTATION_PAGING_DISPLAY_MESSAGE, sBuffer.toString());
				if (((iIndex + 1) != arlSessionList.size()) && (arlSessionList.get(iIndex + 1) != null)) {
					sNextId = "" + ((SearchEnquiryDto) arlSessionList.get(iIndex + 1)).getEnquiryId();
					sNextStatus = "" + ((SearchEnquiryDto) arlSessionList.get(iIndex + 1)).getStatusId();
					request.setAttribute(QuotationConstants.QUOTATION_PAGING_NEXTID, sNextId);
					request.setAttribute(QuotationConstants.QUOTATION_NEX_STA, sNextStatus);
					request.setAttribute(QuotationConstants.QUOTATION_PAGING_NEXTINDEX, "" + (iIndex + 1));
				}
			}
			
			EnquiryDto oEnquiryDto = new EnquiryDto();
			PropertyUtils.copyProperties(oEnquiryDto, oNewEnquiryDto);
			
			// Status: "Draft" (1)
			if (oNewEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_DRAFT) {
				
				isValidUser = oQuotationDao.checkUserRole(conn, oEnquiryDto, oUserDto, QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_SM);

				if (isValidUser) {
					return new ActionForward(QuotationConstants.QUOTATION_LT_GET_ENQDET_URL + oNewEnquiryForm.getEnquiryId());
				} else {
					/* Setting error details to request to display the same in Exception page */
					request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_MSG_UNAUTH);
					/* Forwarding request to Error page */
					return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
				}
			} // Status : "Return to Sales Manager" (8)
			else if (oNewEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_ReturnSM) {
				
				isValidUser = oQuotationDao.checkUserRole(conn, oEnquiryDto, oUserDto, QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_SM);
				
				if (isValidUser) {
					return new ActionForward(QuotationConstants.QUOTATION_LT_RETRUN_BY_DM_URL + oNewEnquiryForm.getEnquiryId());
				} else {
					/* Setting error details to request to display the same in Exception page */
					request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_MSG_UNAUTH);

					/* Forwarding request to Error page */
					return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
				}
			} // Status : "Pending - Design Office" (2) 
			else if (oNewEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGDM) {
				
				isValidUser = oQuotationDao.checkUserRole(conn, oEnquiryDto, oUserDto, QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_DM); 
				
				if (isValidUser) {
					isValidUser = oQuotationDao.checkForWorkflowPendingStatus(conn, oWorkflowDto);
					
					if (isValidUser) {
						return new ActionForward(QuotationConstants.QUOTATION_LT_VIEWOFFERD_URL + oNewEnquiryForm.getEnquiryId());
					} else {
						return new ActionForward(QuotationConstants.QUOTATION_LT_VIEWOFFCONSOL_URL + oNewEnquiryForm.getEnquiryId());
					}
				} else {
					return new ActionForward(QuotationConstants.QUOTATION_LT_VIEWOFFCONSOL_URL + oNewEnquiryForm.getEnquiryId()); 
				}
			} // Status : "Pending - Estimate and Advice" (3) 
        	else if (oNewEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGCE) {
        		
        		isValidUser = oQuotationDao.checkUserRole(conn, oEnquiryDto, oUserDto, QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_CE);
         	   	
        		if (isValidUser){       		   
        			isValidUser = oQuotationDao.checkForWorkflowPendingStatus(conn, oWorkflowDto);
         		   
        			if (isValidUser){
         			   return new ActionForward(QuotationConstants.QUOTATION_LT_VIEWENQ_EST_URL1 + oNewEnquiryForm.getEnquiryId()); 
         		   	} else {
         			   return new ActionForward(QuotationConstants.QUOTATION_LT_VIEWOFFCONSOL_URL + oNewEnquiryForm.getEnquiryId()); 
         		   	}        		   
         	   } else {
         		  return new ActionForward(QuotationConstants.QUOTATION_LT_VIEWOFFCONSOL_URL + oNewEnquiryForm.getEnquiryId()); 
         	   } 
        	} // Status : "Pending - RSM" (4)
        	else if (oNewEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGRSM) {
        		
        		isValidUser = oQuotationDao.checkUserRole(conn, oEnquiryDto, oUserDto, QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_RSM);
        		
        		if (isValidUser){       		   
         		   isValidUser = oQuotationDao.checkForWorkflowPendingStatus(conn, oWorkflowDto);
         		   
         		   if (isValidUser){
         			   return new ActionForward(QuotationConstants.QUOTATION_LT_VIEWENQ_URL + oNewEnquiryForm.getEnquiryId()); 
         		   } else {
         			   return new ActionForward(QuotationConstants.QUOTATION_LT_VIEWENQ_URL + oNewEnquiryForm.getEnquiryId()); 
         		   }        		   
         	   } else {
         		  return new ActionForward(QuotationConstants.QUOTATION_LT_VIEWENQ_URL + oNewEnquiryForm.getEnquiryId()); 
         	   }  
        	} // Status : "Pending - Sales Manager" (5)
        	else if (oNewEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGSM) {
        		
        		isValidUser = oQuotationDao.checkUserRole(conn, oEnquiryDto, oUserDto, QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_SM);
        		
         	   	if (isValidUser) {
         		   isValidUser = oQuotationDao.checkForWorkflowPendingStatus(conn, oWorkflowDto);
         		   
         		   if (isValidUser) {
         			   return new ActionForward(QuotationConstants.QUOTATION_LT_VIEWENQ_URL + oNewEnquiryForm.getEnquiryId()); 
         		   } else {
         			   return new ActionForward(QuotationConstants.QUOTATION_LT_VIEWENQ_URL + oNewEnquiryForm.getEnquiryId()); 
         		   }        		   
         	   } else {
         		  return new ActionForward(QuotationConstants.QUOTATION_LT_VIEWENQ_URL + oNewEnquiryForm.getEnquiryId()); 
         	   }
         	   	
        	} // Status : "Released To Customer" (6)  OR  Status : "Superseded" (7)
        	else if ( ( oNewEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_RELEASED )
        				|| ( oNewEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_SUPERSEDED ) ) {
        		PropertyUtils.copyProperties(oNewEnquiryForm, oNewEnquiryDto);
         	   	
        		return new ActionForward(QuotationConstants.QUOTATION_LT_VIEWOFFCONSOL_URL + oNewEnquiryForm.getEnquiryId());
         	   
        	} // Status : "Won-Pending Sales" (9)
        	else if (oNewEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_WONPENDINGSM) {
        		
        		isValidUser = oQuotationDao.checkUserRole(conn, oEnquiryDto, oUserDto, QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_CM);        	  
         	   	
        		if (isValidUser){
        			isValidUser = oQuotationDao.checkForWorkflowPendingStatus(conn, oWorkflowDto);
         		   
        			if (isValidUser){
         			   return new ActionForward(QuotationConstants.QUOTATION_LT_VIEWINDENTOFFERD_URL + oNewEnquiryForm.getEnquiryId()); 
         		   	}else{
         			   return new ActionForward(QuotationConstants.QUOTATION_LT_VIEWOFFINDENTPAGE_URL + oNewEnquiryForm.getEnquiryId()); 
         		   	}        		   
        		} else {
         		  return new ActionForward(QuotationConstants.QUOTATION_LT_VIEWOFFINDENTPAGE_URL + oNewEnquiryForm.getEnquiryId());  
        		}  
        	} // Status : "Won-Pending Commercial" (10)
        	else if (oNewEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_WONPENDINGCM) {
        		
        		isValidUser = oQuotationDao.checkUserRole(conn, oEnquiryDto, oUserDto, QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_CM);
        		
         	   	if (isValidUser){
         	   		isValidUser = oQuotationDao.checkForWorkflowPendingStatus(conn, oWorkflowDto);
         	   		
         	   		if (isValidUser){
         			   return new ActionForward(QuotationConstants.QUOTATION_LT_VIEWINDENTOFFERD_URL + oNewEnquiryForm.getEnquiryId()); 
         	   		}else{
         			   return new ActionForward(QuotationConstants.QUOTATION_LT_VIEWOFFINDENTPAGE_URL + oNewEnquiryForm.getEnquiryId()); 
         	   		}        		   
         	   } else {
         		  return new ActionForward(QuotationConstants.QUOTATION_LT_VIEWOFFINDENTPAGE_URL + oNewEnquiryForm.getEnquiryId());  
         	   }
        	} // Status : "Won-Pending Design" (11)
        	else if (oNewEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_WONPENDINGDM) {
        		
        		isValidUser = oQuotationDao.checkUserRole(conn, oEnquiryDto, oUserDto, QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_DM);        	  
         	   	if (isValidUser){
         	   		isValidUser = oQuotationDao.checkForWorkflowPendingStatus(conn, oWorkflowDto);
         		   
         	   		if (isValidUser){
         			   return new ActionForward(QuotationConstants.QUOTATION_VIEWINDENTDESIGNOFFERD_URL + oNewEnquiryForm.getEnquiryId()); 
         	   		}else{
         			   return new ActionForward(QuotationConstants.QUOTATION_LT_VIEWOFFINDENTPAGE_URL + oNewEnquiryForm.getEnquiryId()); 
         	   		}        		   
         	   } else {
         		  return new ActionForward(QuotationConstants.QUOTATION_LT_VIEWOFFINDENTPAGE_URL + oNewEnquiryForm.getEnquiryId());  
         	   }
        	} // Status : "WON" (12)	||	Status : "LOST" (13)	||		Status : "ABONDENT" (14)
        	else if ( oNewEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_WON
        				|| oNewEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_LOST
        				|| oNewEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_ABONDENT ) {
        		
        		return new ActionForward(QuotationConstants.QUOTATION_LT_VIEWOFFINDENTPAGE_URL + oNewEnquiryForm.getEnquiryId());
        		
        	} // Status : "Pending - National Sales Manager" (15)
        	else if (oNewEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGNSM) {
        		
        		isValidUser = oQuotationDao.checkUserRole(conn, oEnquiryDto, oUserDto, QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_NSM);
        		
         	   	if (isValidUser) {
         		   isValidUser = oQuotationDao.checkForWorkflowPendingStatus(conn, oWorkflowDto);
         		   
         		   if (isValidUser) {
         			   return new ActionForward(QuotationConstants.QUOTATION_LT_VIEWENQ_URL + oNewEnquiryForm.getEnquiryId()); 
         		   } else {
         			   return new ActionForward(QuotationConstants.QUOTATION_LT_VIEWENQ_URL + oNewEnquiryForm.getEnquiryId()); 
         		   }        		   
         	   } else {
         		  return new ActionForward(QuotationConstants.QUOTATION_LT_VIEWENQ_URL + oNewEnquiryForm.getEnquiryId()); 
         	   }
        	} // Status : "Pending - Marketing Head" (16)
        	else if (oNewEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGMH) {
        		
        		isValidUser = oQuotationDao.checkUserRole(conn, oEnquiryDto, oUserDto, QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_MH);
        		
         	   	if (isValidUser) {
         		   isValidUser = oQuotationDao.checkForWorkflowPendingStatus(conn, oWorkflowDto);
         		   
         		   if (isValidUser) {
         			   return new ActionForward(QuotationConstants.QUOTATION_LT_VIEWENQ_URL + oNewEnquiryForm.getEnquiryId()); 
         		   } else {
         			   return new ActionForward(QuotationConstants.QUOTATION_LT_VIEWENQ_URL + oNewEnquiryForm.getEnquiryId()); 
         		   }        		   
         	   } else {
         		  return new ActionForward(QuotationConstants.QUOTATION_LT_VIEWENQ_URL + oNewEnquiryForm.getEnquiryId()); 
         	   }
        	} // Status : "Pending - Quality Approval" (17)
        	else if (oNewEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGQA) {
        		
        		isValidUser = oQuotationDao.checkUserRole(conn, oEnquiryDto, oUserDto, QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_QA);
        		
        		if (isValidUser) {
        			isValidUser = oQuotationDao.checkForWorkflowPendingStatus(conn, oWorkflowDto);
        			
        			if (isValidUser) {
        				return new ActionForward(QuotationConstants.QUOTATION_LT_VIEWOFFERQA_URL + oNewEnquiryForm.getEnquiryId());
        			} else {
        				return new ActionForward(QuotationConstants.QUOTATION_LT_VIEWOFFCONSOL_URL + oNewEnquiryForm.getEnquiryId()); 
        			}
        		} else {
        			return new ActionForward(QuotationConstants.QUOTATION_LT_VIEWOFFCONSOL_URL + oNewEnquiryForm.getEnquiryId()); 
        		}
        	} // Status : "Pending - Sourcing" (18)
        	else if (oNewEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGSCM) {
        		
        		isValidUser = oQuotationDao.checkUserRole(conn, oEnquiryDto, oUserDto, QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_SCM);
        		
        		if (isValidUser) {
        			isValidUser = oQuotationDao.checkForWorkflowPendingStatus(conn, oWorkflowDto);
        			
        			if (isValidUser) {
        				return new ActionForward(QuotationConstants.QUOTATION_LT_VIEWOFFERQA_URL + oNewEnquiryForm.getEnquiryId());
        			} else {
        				return new ActionForward(QuotationConstants.QUOTATION_LT_VIEWOFFCONSOL_URL + oNewEnquiryForm.getEnquiryId()); 
        			}
        		} else {
        			return new ActionForward(QuotationConstants.QUOTATION_LT_VIEWOFFCONSOL_URL + oNewEnquiryForm.getEnquiryId()); 
        		}
        	} // Status : "Regret - Pending SM" (19)
        	else if (oNewEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_REGRET_PENDINGSM) {
        		
        		isValidUser = oQuotationDao.checkUserRole(conn, oEnquiryDto, oUserDto, QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_SM);
        		
        		if (isValidUser) {
        			isValidUser = oQuotationDao.checkForWorkflowPendingStatus(conn, oWorkflowDto);
        				
        			if (isValidUser) {
        				return new ActionForward(QuotationConstants.QUOTATION_LT_GET_ENQDET_URL + oNewEnquiryForm.getEnquiryId());
        			} else {
        				return new ActionForward(QuotationConstants.QUOTATION_LT_VIEWOFFCONSOL_URL + oNewEnquiryForm.getEnquiryId());
        			}
        		} else {
        			return new ActionForward(QuotationConstants.QUOTATION_LT_VIEWOFFCONSOL_URL + oNewEnquiryForm.getEnquiryId()); 
        		}
        	} // Status : "Pending - BH Approval" (20)
        	else if (oNewEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGBH) {
        		
        		isValidUser = oQuotationDao.checkUserRole(conn, oEnquiryDto, oUserDto, QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_BH);
        		
        		if (isValidUser) {
        			isValidUser = oQuotationDao.checkForWorkflowPendingStatus(conn, oWorkflowDto);
        			
        			if (isValidUser) {
        				return new ActionForward(QuotationConstants.QUOTATION_LT_BHAPPROVE_URL + oNewEnquiryForm.getEnquiryId());
        			} else {
        				return new ActionForward(QuotationConstants.QUOTATION_LT_VIEWENQ_URL + oNewEnquiryForm.getEnquiryId()); 
        			}
        		} else {
        			return new ActionForward(QuotationConstants.QUOTATION_LT_VIEWENQ_URL + oNewEnquiryForm.getEnquiryId()); 
        		}
        	} // Status : "Pending - Mfg. Plant Details" (21)
        	else if (oNewEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGMFG) {
        		
        		isValidUser = oQuotationDao.checkUserRole(conn, oEnquiryDto, oUserDto, QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_MFG);
        		
        		if (isValidUser) {
        			isValidUser = oQuotationDao.checkForWorkflowPendingStatus(conn, oWorkflowDto);
        			
        			if (isValidUser) {
        				return new ActionForward(QuotationConstants.QUOTATION_LT_MFGAPPROVAL_URL + oNewEnquiryForm.getEnquiryId());
        			} else {
        				return new ActionForward(QuotationConstants.QUOTATION_LT_VIEWENQ_URL + oNewEnquiryForm.getEnquiryId()); 
        			}
        		} else {
        			return new ActionForward(QuotationConstants.QUOTATION_LT_VIEWENQ_URL + oNewEnquiryForm.getEnquiryId()); 
        		}
        	} 
			
        } catch(Exception e) {
            /* Logging any exception that raised during this activity in log files using Log4j */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "validateRequestAction", ExceptionUtility.getStackTraceAsString(e));
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        } finally {
            /* Releasing/closing the connection object */
            if ( conn!=null )
                oDBUtility.releaseResources(conn);
        }
    	
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_MSG_UNAUTH);
        
        /* Forwarding request to Error page */
        return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
    }
    
    /* (non-Javadoc)
     * @see in.com.rbc.quotation.enquiry.form.EnquiryForm#buildRatingErrorMessage(in.com.rbc.quotation.enquiry.form.EnquiryForm)
     * This method is used to generate the error message - an enquiry will be submitted only when this message is empty ( Save as draft and Save & add new rating cases only)
     */
    private NewEnquiryForm buildRatingErrorMessage (NewEnquiryForm oNewEnquiryForm) {
    	//  check if all the ratings are validated or not and build the appropriate error message
    	StringBuffer sbValidationMessage =null;
    	int iCount = QuotationConstants.QUOTATION_LITERAL_ONE;	
    	KeyValueVo oKeyValueVo =null;
    	
		if (oNewEnquiryForm.getNewRatingsList().size() > QuotationConstants.QUOTATION_LITERAL_ZERO) 
		{
			sbValidationMessage = new StringBuffer();
			/*
			 * This varible is for tracking the rating count mentionded in the title - Rating "1", Rating "2" 
			 */
			/*for (int i=0; i<oNewEnquiryForm.getNewRatingsList().size(); i++){
				oKeyValueVo = (KeyValueVo) oNewEnquiryForm.getRatingList().get(i);
				if (oKeyValueVo != null){
					//Do not add the error message for the rating that is currently in edit mode
					if ((! oNewEnquiryForm.getRatingOperationId().trim().equalsIgnoreCase(oKeyValueVo.getKey()))){

						if (! oKeyValueVo.getValue2().equalsIgnoreCase(QuotationConstants.QUOTATION_STRING_Y)) 
						{
							sbValidationMessage.append(QuotationConstants.QUOTATION_RATING+iCount);  
							sbValidationMessage.append(QuotationConstants.QUOTATION_STRING_COMMA); 
							iCount++;
						}else{
							iCount++;
						}
					}else{
						iCount++;
					}
				}
			}*/
			if (sbValidationMessage.toString().trim().endsWith(QuotationConstants.QUOTATION_STRING_COMMA))       	
				oNewEnquiryForm.setRatingsValidationMessage(sbValidationMessage.toString().substring(0, sbValidationMessage.toString().lastIndexOf(QuotationConstants.QUOTATION_STRING_COMMA)));
			else
				oNewEnquiryForm.setRatingsValidationMessage(sbValidationMessage.toString());	
			
		}
		return oNewEnquiryForm;
    }
    
    
	/**
	 * This Method is used to Calculate the List Price Per Unit.
	 * @param actionMapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward calculateLPPerUnitValueAndDiscountByAjax(ActionMapping actionMapping, 
			ActionForm form,
			HttpServletRequest request, 
			HttpServletResponse response) throws Exception {

		String sMethodName = "calculateLPPerUnitValueAndDiscountByAjax"; 
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        int iQuantity = 1;
        double iCustomerDiscount = 0.0d;
        ListPriseDao oListPriceDao = null;
        CustomerDiscountDao oCustomerDiscountDao = null;
        DaoFactory oDaoFactory = null; //Creating an instance of of DaoFactory & retrieving UserDao object
        QuotationDao oQuotationDao = null;// Creating an instance of QuotationDao
        DBUtility oDBUtility = new DBUtility();
        NewEnquiryUtilityManager oNewEnquiryUtilityManager = new NewEnquiryUtilityManager();
        setRolesToRequest(request);
        
        try (Connection conn = oDBUtility.getDBConnection()) {
        	oDaoFactory = new DaoFactory();
        	oListPriceDao = oDaoFactory.getListPriseDao();
        	oCustomerDiscountDao = oDaoFactory.getCustomerDiscountDao();
        	oQuotationDao = oDaoFactory.getQuotationDao();
        	
        	NewEnquiryForm oNewEnquiryForm = (NewEnquiryForm) form;
			if (oNewEnquiryForm == null) {
				oNewEnquiryForm = new NewEnquiryForm();
			}
			
			NewEnquiryDto oNewEnquiryDto = new NewEnquiryDto();
			PropertyUtils.copyProperties(oNewEnquiryDto, oNewEnquiryForm);
			NewRatingDto oNewRatingDto = new NewRatingDto();
			
			// Ajax Information
			if(StringUtils.isNotBlank(CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_QUANTITY_VALUE)) ) {
				iQuantity = Integer.parseInt(CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_QUANTITY_VALUE));
			}
			oNewRatingDto.setLtManufacturingLocation(CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_MFGLOCATION_VALUE));
			oNewRatingDto.setLtProdGroupId(CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_PRODGROUP_VALUE));
			oNewRatingDto.setLtProdLineId(CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_PRODLINE_VALUE));
			oNewRatingDto.setLtKWId(CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_KW_VALUE));
			oNewRatingDto.setLtFrameId(CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_FRAME_VALUE));
			oNewRatingDto.setLtPoleId(CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_POLE_VALUE));
			oNewRatingDto.setLtMountingId(CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_MOUNTING_VALUE));
			oNewRatingDto.setLtTBPositionId(CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_TBPOSITION_VALUE));
			oNewRatingDto.setLtCustomerId(CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_CUSTOMERID_VALUE));
			
			ListPriceDto oListPriceDto = oNewEnquiryUtilityManager.buildListPriceSearchFilter(conn, oQuotationDao, oNewRatingDto);
			
			CustomerDiscountDto  oCustomerDiscountDto = oNewEnquiryUtilityManager.buildCustDiscountSearchFilter(conn, oQuotationDao, oNewRatingDto);
			
			// Method to Fetch the LP Per Unit value from LIST_PRISE Table
			oListPriceDto = oListPriceDao.fetchListPrice(conn, oListPriceDto);
			Double lpPerUnitValue = oListPriceDto.getListPriceValue();
			
			// Method to Fetch the Customer Discount value from CUSTOMER_DISCOUNT Table
			iCustomerDiscount = oCustomerDiscountDao.fetchCustomerDiscount(conn, oCustomerDiscountDto);
			
			String[] sValuesArray = new String[3];
			if (lpPerUnitValue != null && lpPerUnitValue > 0 && iCustomerDiscount > 0) {
				sValuesArray[0] = String.valueOf(lpPerUnitValue);
				sValuesArray[1] = String.valueOf(iCustomerDiscount);
			} else {
				sValuesArray[0] = QuotationConstants.QUOTATION_EMPTY;
				sValuesArray[1] = QuotationConstants.QUOTATION_EMPTY;
			}
			
			if (lpPerUnitValue != null && lpPerUnitValue > 0) {
				getAjax(response, conn, QuotationConstants.QUOTATION_FORWARD_SUCCESS, sValuesArray);
			} else {
				getAjax(response, conn, QuotationConstants.QUOTATION_FORWARD_FAILURE, sValuesArray);
			}
        }
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
    	return null;
	}
	
	
	public ActionForward calculateTotalAddOnByAjax(ActionMapping actionMapping, 
			ActionForm form,
			HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		String sMethodName = "calculateLPPerUnitValueByAjax"; 
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        DaoFactory oDaoFactory = null; //Creating an instance of of DaoFactory & retrieving UserDao object
        QuotationDao oQuotationDao = null;// Creating an instance of QuotationDao
        AddOnDetailsDao oAddOnDetailsDao = null;
        DBUtility oDBUtility = new DBUtility();
        NewEnquiryUtilityManager oNewEnquiryUtilityManager = new NewEnquiryUtilityManager();
        setRolesToRequest(request);
        
        try (Connection conn = oDBUtility.getDBConnection()) {
        	oDaoFactory = new DaoFactory();
        	oAddOnDetailsDao = oDaoFactory.getAddOnDetailsDao();
        	oQuotationDao = oDaoFactory.getQuotationDao();
        	
        	NewEnquiryForm oNewEnquiryForm = (NewEnquiryForm) form;
			if (oNewEnquiryForm == null) {
				oNewEnquiryForm = new NewEnquiryForm();
			}
			
			NewEnquiryDto oNewEnquiryDto = new NewEnquiryDto();
			PropertyUtils.copyProperties(oNewEnquiryDto, oNewEnquiryForm);
			NewRatingDto oNewRatingDto = new NewRatingDto();
			
			// Ajax Information
			oNewRatingDto.setLtProdGroupId(CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_PRODGROUP_VALUE));
			oNewRatingDto.setLtProdLineId(CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_PRODLINE_VALUE));
			oNewRatingDto.setLtFrameId(CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_FRAME_VALUE));
			oNewRatingDto.setLtNonStdVoltageId(CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_NONSTDVOLTAGE_VALUE));
			oNewRatingDto.setLtWindingTreatmentId(CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_WINDINGTREATMENT_VALUE));
			oNewRatingDto.setLtShaftTypeId(CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_SHAFTTYPE_VALUE));
			oNewRatingDto.setLtShaftMaterialId(CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_SHAFTMATERIAL_VALUE));
			oNewRatingDto.setLtSpaceHeaterId(CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_SPACEHEATER_VALUE));
			oNewRatingDto.setLtHardware(CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_HARDWARE_VALUE));
			oNewRatingDto.setLtDoubleCompressionGlandId(CommonUtility.getStringParameter(request,QuotationConstants.QUOTATION_DOUBLECOMPRESSGLAND_VALUE));
			oNewRatingDto.setLtMountingId(CommonUtility.getStringParameter(request,"mountingVal"));
			oNewRatingDto.setLtPaintingTypeId(CommonUtility.getStringParameter(request,"paintTypeVal"));
			oNewRatingDto.setLtPaintShadeId(CommonUtility.getStringParameter(request,"paintShadeVal"));
			oNewRatingDto.setLtPaintThicknessId(CommonUtility.getStringParameter(request,"paintThicknessVal"));
			oNewRatingDto.setLtInsulationClassId(CommonUtility.getStringParameter(request,"insulationClassVal"));
			oNewRatingDto.setLtTerminalBoxSizeId(CommonUtility.getStringParameter(request,"terminalBoxVal"));
			oNewRatingDto.setLtSpreaderBoxId(CommonUtility.getStringParameter(request,"spreaderBoxVal"));
			oNewRatingDto.setLtAuxTerminalBoxId(CommonUtility.getStringParameter(request,"auxTBVal"));
			oNewRatingDto.setLtVibrationId(CommonUtility.getStringParameter(request,"vibrationLevelVal"));
			oNewRatingDto.setLtFlyingLeadWithoutTDId(CommonUtility.getStringParameter(request,"flyingLeadWithoutTBVal"));
			oNewRatingDto.setLtFlyingLeadWithTDId(CommonUtility.getStringParameter(request,"flyingLeadWithTBVal"));
			oNewRatingDto.setLtSeaworthyPackingId(CommonUtility.getStringParameter(request,"seaworthyVal"));
			oNewRatingDto.setLtAddNamePlateId(CommonUtility.getStringParameter(request,"addlNamePlateVal"));
			oNewRatingDto.setLtDirectionArrowPlateId(CommonUtility.getStringParameter(request,"directionArrowPlateVal"));
			oNewRatingDto.setLtWitnessRoutineId(CommonUtility.getStringParameter(request,"witnessRoutineVal"));
			oNewRatingDto.setLtWarrantyId(CommonUtility.getStringParameter(request,"warrantyVal"));
			oNewRatingDto.setLtAdditionalTest1Id(CommonUtility.getStringParameter(request,"additionalTest1Val"));
			oNewRatingDto.setLtAdditionalTest2Id(CommonUtility.getStringParameter(request,"additionalTest2Val"));
			oNewRatingDto.setLtAdditionalTest3Id(CommonUtility.getStringParameter(request,"additionalTest3Val"));
			oNewRatingDto.setLtTypeTestId(CommonUtility.getStringParameter(request,"typeTestVal"));
			oNewRatingDto.setLtRTDId(CommonUtility.getStringParameter(request,"rtdVal"));
			oNewRatingDto.setLtThermisterId(CommonUtility.getStringParameter(request,"thermisterVal"));
			oNewRatingDto.setLtBTDId(CommonUtility.getStringParameter(request,"btdVal"));
			oNewRatingDto.setLtTechoMounting(CommonUtility.getStringParameter(request,"techoMountingVal"));
			oNewRatingDto.setLtMetalFanId(CommonUtility.getStringParameter(request,"metalFanVal"));
			oNewRatingDto.setLtForcedCoolingId(CommonUtility.getStringParameter(request,"methodOfCoolVal"));
			oNewRatingDto.setLtHazardAreaId(CommonUtility.getStringParameter(request,"hazardAreaVal"));
			oNewRatingDto.setLtCableSealingBoxId(CommonUtility.getStringParameter(request,"cableSealingBoxVal"));
			oNewRatingDto.setLtBearingNDEId(CommonUtility.getStringParameter(request,"bearingNDEVal"));
			oNewRatingDto.setLtBearingDEId(CommonUtility.getStringParameter(request,"bearingDEVal"));
			//oNewRatingDto.setLtDualSpeedId(CommonUtility.getStringParameter(request,"dualSpeedVal"));
			oNewRatingDto.setLtGasGroupId(CommonUtility.getStringParameter(request,"gasGroupVal"));
			oNewRatingDto.setLtShaftGroundingId(CommonUtility.getStringParameter(request,"shaftGroundingVal"));
			oNewRatingDto.setLtSPMMountingProvisionId(CommonUtility.getStringParameter(request,"spmMountingVal"));
			oNewRatingDto.setLtVFDApplTypeId(CommonUtility.getStringParameter(request,"vfdTypeVal"));
			oNewRatingDto.setLtULCEId(CommonUtility.getStringParameter(request,"certificationVal"));
			oNewRatingDto.setLtIPId(CommonUtility.getStringParameter(request,"ipVal"));
			oNewRatingDto.setLtLeadId(CommonUtility.getStringParameter(request,"leadVal"));
			
			AddOnDetailsDto oAddOnDetailsDto = oNewEnquiryUtilityManager.buildAddOnDetailsSearchFilter(conn, oQuotationDao, oNewRatingDto);
			
			oAddOnDetailsDto = oAddOnDetailsDao.fetchAddOnDetails(conn, oAddOnDetailsDto);
			
			String[] sValuesArray = new String[2];
			if (oAddOnDetailsDto.getTotalAddOnPercentValue() == 0d) { sValuesArray[0] = QuotationConstants.QUOTATION_EMPTY; } else { sValuesArray[0] = String.valueOf(oAddOnDetailsDto.getTotalAddOnPercentValue()); }
			if (oAddOnDetailsDto.getTotalAddOnCashExtraValue() == 0d) { sValuesArray[1] = QuotationConstants.QUOTATION_EMPTY; } else { sValuesArray[1] = String.valueOf(oAddOnDetailsDto.getTotalAddOnCashExtraValue()); }
			
			if (oAddOnDetailsDto.getTotalAddOnPercentValue() > 0 || oAddOnDetailsDto.getTotalAddOnCashExtraValue() > 0) {
				getAjax(response, conn, QuotationConstants.QUOTATION_FORWARD_SUCCESS, sValuesArray);
			} else {
				getAjax(response, conn, QuotationConstants.QUOTATION_FORWARD_FAILURE, sValuesArray);
			}
        }
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
    	return null;
	}
	
	
	private void getAjax(HttpServletResponse response, Connection conn, String sConfirmationMessage, String[] sValuesArray) throws Exception {
		response.setContentType("text/xml");
		PrintWriter oOut = null;

		StringBuffer sb = new StringBuffer();

		sb.append("<?xml version=\"1.0\" ?>");
		sb.append("<pricecalculation>");
		try {
			oOut = response.getWriter();
			sb.append("<msg>").append(sConfirmationMessage).append("</msg>");
			sb.append("<returnValue1>").append(sValuesArray[0]).append("</returnValue1>");
			sb.append("<returnValue2>").append(sValuesArray[1]).append("</returnValue2>");
		} finally {
			sb.append("</pricecalculation>");
			oOut.print(sb);
			oOut.flush();
			oOut.close();
		}
	}
	
	/**
     * Method for setting up Quotation LT : Offer Data page.
     * @param actionMapping	ActionMapping object to redirect the user to next page depending on the activity
     * @param actionForm	ActionForm object to handle the form elements
     * @param request		HttpServletRequest object to handle request operations
     * @param response		HttpServletResponse object to handle response operations
     * @return returns 		ActionForward depending on which user is redirected to next page
     * @throws Exception	if any error occurs while performing database operations, etc
     */
	public ActionForward viewOfferData(ActionMapping actionMapping,
            ActionForm actionForm, 
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
		
		String sMethodName =QuotationConstants.QUOTATION_VIEWOFF; 
    	String sClassName = this.getClass().getName();
    	LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
    	
    	Connection conn = null;  		// Connection object to store the database connection
        DBUtility oDBUtility = null;  	// Object of DBUtility class to handle DB connection objects
        
        UserDto oUserDto = null;
        NewRatingDto oNewRatingDto = null;
        
        String sOperationType = null, sOperationType2 = null;
        
        WorkflowUtility oWorkflowUtility = new WorkflowUtility();
        WorkflowDto oWorkflowDto = null;
        UserAction oUserAction =null; //Instantiating the UserAction object
        DaoFactory oDaoFactory =null; //Creating an instance of of DaoFactory & retrieving UserDao object
        QuotationDao oQuotationDao = null;
        
        String sMsg = null, sLinkMsg1=null, sLinkMsg2=null, sLinkMsg=null, sCurrentRateId=null, sFullName = null;
        String[] saSections=null;
        StringBuffer oAddOnTypeNames = null;
        NewTechnicalOfferDto oNewTechnicalOfferDto = null;
        boolean isRatingUpdated = QuotationConstants.QUOTATION_FALSE;
        boolean isTechOfferUpdated = QuotationConstants.QUOTATION_FALSE;
        boolean isAddOnUpdated = QuotationConstants.QUOTATION_FALSE;
        int iHidRowIndex = QuotationConstants.QUOTATION_LITERAL_ZERO;
        int iRatingId = QuotationConstants.QUOTATION_LITERAL_ZERO;
        String sDeptAndAuth = null;
        ArrayList<NewRatingDto> alRatingList =null;
    	int iWorkflowExecuteStatus = QuotationConstants.QUOTATION_LITERAL_ZERO;
    	
    	NewEnquiryForm oNewEnquiryForm = null;
        NewEnquiryDto oNewEnquiryDto = null;
        
        try {
        	/* Setting user's information in request, using the header information received through request */
            setRolesToRequest(request);
            LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "Roles set to request.");
            
            oNewEnquiryForm = (NewEnquiryForm) actionForm;
    		if (oNewEnquiryForm == null)
    			oNewEnquiryForm = new NewEnquiryForm();
    		
    		sOperationType = oNewEnquiryForm.getOperationType();
    		
    		/* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Instantiating the UserAction object */
            oUserAction = new UserAction();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            
            if (sOperationType.trim().length() == QuotationConstants.QUOTATION_LITERAL_ZERO) {
    			sOperationType = QuotationConstants.QUOTATION_VIEWOFFDATA;    			
    			saveToken(request);    			
    		} else {
				// for other cases validate token
				if (!isTokenValid(request)) {
					/* Setting error details to request to display the same in Exception page */
					request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_INVALIDTOKENMESSAGE);
					/* Forwarding request to Error page */
					return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
				}
    		}
            
            /* Retrieving the logged-in user information */
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            
            oNewEnquiryDto = new NewEnquiryDto();
            NewEnquiryUtilityManager oNewEnquiryManager = new NewEnquiryUtilityManager();
            oQuotationDao = oDaoFactory.getQuotationDao();
            
            // if its reassign the initiate the work flow and the forward the request to respective page
			if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_REASSIGN)) {
				// reset the token
				resetToken(request);

				oWorkflowDto = new WorkflowDto();
				oWorkflowDto.setEnquiryId(oNewEnquiryForm.getEnquiryId());
				oWorkflowDto.setEnquiryNo(oNewEnquiryForm.getEnquiryNumber());
				oWorkflowDto.setLoginUserId(oUserDto.getUserId());
				oWorkflowDto.setLoginUserName(oUserDto.getFullName());
				oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
				oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_REASSIGNTODE);
				oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGDM);
				oWorkflowDto.setAssignedTo(oNewEnquiryForm.getAssignToId());
				oWorkflowDto.setRemarks(oNewEnquiryForm.getAssignRemarks());
				oWorkflowDto.setDmToSmReturn(oNewEnquiryForm.getReturnToRemarks());

				LoggerUtility.log("INFO", sClassName, sMethodName, "3 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
				iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);

				if (iWorkflowExecuteStatus == QuotationConstants.QUOTATION_LITERAL_ONE) {
					LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Successfully");
				} else if (iWorkflowExecuteStatus == QuotationConstants.QUOTATION_LITERAL_ZERO) {
					LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed - But email could not be sent. Please send email manually.");
				} else {
					LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Failed");
				}

				LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Workflow process completed - - -");

				sMsg = QuotationConstants.QUOTATION_ASSIGNED_SUCESS + oNewEnquiryForm.getAssignToName();
				sLinkMsg = QuotationConstants.QUOTATION_VALIDATEREQ_URL + oNewEnquiryForm.getEnquiryId() + QuotationConstants.QUOTATION_GRSYMBOL + QuotationConstants.QUOTATION_VIEWLINK;

				request.setAttribute(QuotationConstants.QUOTATION_HEADINGMESSAGE, QuotationConstants.QUOTATION_REASSIG_SUCES);
				request.setAttribute(QuotationConstants.QUOTATION_SUCCESSMESSAGE, sMsg);
				request.setAttribute(QuotationConstants.QUOTATION_LINKSMESSAGE, sLinkMsg);
				return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);
			}
			
			if (oNewEnquiryForm.getEnquiryId() < QuotationConstants.QUOTATION_LITERAL_ONE) 
				oNewEnquiryForm.setEnquiryId(CommonUtility.getIntParameter(request, QuotationConstants.QUOTATION_REQUEST_ENQUIRYID));
            
			if (oNewEnquiryForm.getEnquiryId() < QuotationConstants.QUOTATION_LITERAL_ONE) {
            	 /* Setting error details to request to display the same in Exception page */
                request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_CRET_ROWS); 
                /* Forwarding request to Error page */
                return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);            	
            }
        	
			// ::::::::::::::::::::::::::::::::::::::  Operation Type = Update Rating  ::::::::::::::::::::::::::::::::::::::  
			if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_UPDRATING)) {
				
				oDBUtility.setAutoCommitFalse(conn);
				
				oNewRatingDto = new NewRatingDto();
				oNewRatingDto.setRatingId(oNewEnquiryForm.getRatingId());
				iHidRowIndex = CommonUtility.getIntParameter(request, QuotationConstants.QUOTATION_HIDEROWIND);
				
				// **********************************  Update Add-on Details  ********************************************************
				// 1. Fetching all Add-ons from the Existing form page.
				oNewRatingDto.setAddOnList(getNewRatingAddOns(iHidRowIndex, oNewEnquiryForm.getRatingId(), request));
				// 2. Delete all existing Add-ons. 
				isAddOnUpdated = oQuotationDao.deleteAllNewRatingAddOns(conn, oNewEnquiryForm.getRatingId());
				// 3. Insert all Add-ons from the Existing form page.
				if (isAddOnUpdated){
                	oDBUtility.commit(conn);
                } else {
                	oDBUtility.rollback(conn);
               	 	/* Setting error details to request to display the same in Exception page */
                	request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_ERROR_DELETING);
                	/* Forwarding request to Error page */
                	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
                }
				if (isAddOnUpdated) {
					isAddOnUpdated = QuotationConstants.QUOTATION_FALSE;
					isAddOnUpdated = oQuotationDao.updateNewRatingAddOns(conn, oNewRatingDto);
				}
				if (isAddOnUpdated) {
					oDBUtility.commit(conn);
				}
				// **********************************  Update Add-on Details  ********************************************************
				
				// **********************************  Update the Rating Data  ********************************************************
				if (oNewEnquiryForm.getCurrentRating().trim().length() > QuotationConstants.QUOTATION_LITERAL_ZERO) {
					oNewRatingDto.setRatingId(Integer.parseInt(oNewEnquiryForm.getCurrentRating().trim()));
					oNewRatingDto.setLtCustomerId(CommonUtility.getStringParameter(request, "customerId"));
					oNewRatingDto = oNewEnquiryManager.getSelectedRatingFieldValues(conn, oQuotationDao, oNewRatingDto, request);
				}
				isRatingUpdated = oQuotationDao.updateMTORatingDetailsById(conn, oNewRatingDto);
				
				if (isRatingUpdated) {
                	oDBUtility.commit(conn);
                } else {
                	oDBUtility.rollback(conn);
                	 /* Setting error details to request to display the same in Exception page */
                    request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_ERROR_UPDATING);
                    /* Forwarding request to Error page */
                    return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
                }
				// **********************************  Update the Rating Data  ********************************************************
				
				// **********************************  Update the Technical info Data  ************************************************
				oNewTechnicalOfferDto = new NewTechnicalOfferDto();
				oNewTechnicalOfferDto.setCreatedBy(oUserDto.getUserId());
				oNewTechnicalOfferDto.setCreatedByName(oUserDto.getFullName());
				
				oNewTechnicalOfferDto = getSelectedRatingTechOfferFieldValues(conn, oQuotationDao, oNewRatingDto, oNewTechnicalOfferDto, request);
				
				isTechOfferUpdated = oQuotationDao.updateMTORatingTechOfferDetails(conn, oNewTechnicalOfferDto);
				
				if(isTechOfferUpdated) {
					oDBUtility.commit(conn);
				} else {
					oDBUtility.rollback(conn);
               	 	/* Setting error details to request to display the same in Exception page */
					request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_ERROR_UPDATING);
					/* Forwarding request to Error page */
					return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
				}
				// **********************************  Update the Technical info Data  ************************************************
				
				oDBUtility.setAutoCommitTrue(conn);
				
			}
			// ::::::::::::::::::::::::::::::::::::::  Operation Type = Update Rating  ::::::::::::::::::::::::::::::::::::::  
			
			sOperationType = QuotationConstants.QUOTATION_VIEWOFFDATA;                
            sOperationType2 = QuotationConstants.QUOTATION_VIEWOFFDATA2;
            
			// Operation Type = "viewofferdata"
			if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_VIEWOFFDATA)) {
				
				iRatingId = QuotationConstants.QUOTATION_LITERAL_ZERO;            	
            	sDeptAndAuth = QuotationConstants.QUOTATION_TECHNICAL_DEPT;
            	
				// Get the Attachment Files
    			request.setAttribute(QuotationConstants.QUOTATION_ATTACHLIST, AttachmentUtility.getNewEnquiryAttachmentDetails(oNewEnquiryDto));
    			
				PropertyUtils.copyProperties(oNewEnquiryDto, oNewEnquiryForm);
				
				// load the attachment list
                // request.setAttribute(QuotationConstants.QUOTATION_ATTACHLIST, AttachmentUtility.getAttachmentDetails(oNewEnquiryDto)); 
                // load the dm uploaded attachmentlist 
                // request.setAttribute(QuotationConstants.QUOTATION_DMATTACHLIST, AttachmentUtility.getDMUploadedAttachmentDetails(oNewEnquiryDto));
                
				if(sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_VIEWOFFDATA)) {
					oNewEnquiryDto = oQuotationDao.getNewEnquiryDetailsForView(conn, oNewEnquiryDto, QuotationConstants.QUOTATION_EDIT.toUpperCase()); 
				}
				
				oNewEnquiryDto = oQuotationDao.loadNewEnquiryLookUpValues(conn, oNewEnquiryDto);
				PropertyUtils.copyProperties(oNewEnquiryForm, oNewEnquiryDto);
				//oNewEnquiryForm = oNewEnquiryManager.loadLookUpValues(conn, oNewEnquiryForm);
				//oNewEnquiryForm = oNewEnquiryManager.loadOfferPageLookUpValues(conn, oNewEnquiryForm);
				
				sFullName = oNewEnquiryForm.getCreatedBy();
	            saSections = sFullName.split(" ");
	            for(String sPart:saSections) {
	            	sDeptAndAuth= sDeptAndAuth+sPart.substring(0,1);
	            }
	            oNewEnquiryForm.setDeptAndAuth(sDeptAndAuth);
	            
	            // Get the New Ratings List.
	            alRatingList = oNewEnquiryForm.getNewRatingsList();
	            
	            // **********************************  Fetching the Rating Data  ********************************************************
	            if (oNewEnquiryForm.getCurrentRating().length() > QuotationConstants.QUOTATION_LITERAL_ZERO) {
	            	oNewRatingDto = new NewRatingDto();
	            	oNewRatingDto.setRatingId(Integer.parseInt(oNewEnquiryForm.getCurrentRating()));
	            	oNewRatingDto = oQuotationDao.fetchRatingByRatingId(conn, oNewRatingDto);
	            	
	            	oNewEnquiryForm.setCurrentRating("" + oNewRatingDto.getRatingId());
	            	oNewEnquiryForm.setRatingOperationId("" + oNewRatingDto.getRatingId());
	            	
	            } else if (oNewEnquiryForm.getNewRatingsList().size() > QuotationConstants.QUOTATION_LITERAL_ZERO) {
        			oNewRatingDto = (NewRatingDto) oNewEnquiryForm.getNewRatingsList().get(0);
        			
        			oNewEnquiryForm.setCurrentRating("" + oNewRatingDto.getRatingId());
        			iRatingId = oNewRatingDto.getRatingId();
        			oNewEnquiryForm.setRatingOperationId("" + oNewRatingDto.getRatingId());
        		}
	            // **********************************  Fetching the Rating Data  ********************************************************
	            
	            // Set the MTO Total Add On Field Values for the Rating object.
	            oNewRatingDto.setLtTotalAddOn_MTO(oNewRatingDto.getLtTotalAddonPercent());
	            oNewRatingDto.setLtTotalCashExtra_MTO(oNewRatingDto.getLtTotalCashExtra());
	            
	            if (oNewRatingDto ==  null)
	            	oNewRatingDto = new NewRatingDto();
	            
	            // Set Rating DTO to Request.
	            request.setAttribute(QuotationConstants.QUOTATION_LOWER_NEW_RATINGDTO, oNewRatingDto);
	            
	            // TO DO:  COPY all Required Code from Enquiry Action
	            
	            // oQuotationDao.loadOfferPageLookUpValuesNewEnq(conn, oNewRatingDto);
	            
	            // TO DO:  COPY all Required Code from Enquiry Action
			}
			
        } catch(Exception e) {
        	//e.printStackTrace();
            /* Logging any exception that raised during this activity in log files using Log4j */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "home", ExceptionUtility.getStackTraceAsString(e));
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        } finally {
            /* Releasing/closing the connection object */
            if ( conn!=null )
                oDBUtility.releaseResources(conn);
        }
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
        
    	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_OFFERDATA_VIEW);
	}
	
	/**
     * Method for setting up Quotation LT : View Enquiry page.
     * @param actionMapping	ActionMapping object to redirect the user to next page depending on the activity
     * @param actionForm	ActionForm object to handle the form elements
     * @param request		HttpServletRequest object to handle request operations
     * @param response		HttpServletResponse object to handle response operations
     * @return returns 		ActionForward depending on which user is redirected to next page
     * @throws Exception	if any error occurs while performing database operations, etc
     */
	public ActionForward viewEnquiry(ActionMapping actionMapping,
            ActionForm actionForm, 
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
		
		String sMethodName = QuotationConstants.QUOTATION_VIEWLINK_CAP;
		LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "START");
		
    	Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        
        UserDto oUserDto = null;
        NewRatingDto oNewRatingDto = null;
        TeamMemberDto oTeamMemberDto = null;
        WorkflowDto oWorkFlowDto = null;
        
        NewEnquiryForm oNewEnquiryForm =null;
        NewEnquiryDto oNewEnquiryDto =null;
        NewEnquiryUtilityManager oNewEnquiryManager = null;
        boolean isValidUser = QuotationConstants.QUOTATION_FALSE;
        String returnenquiry=QuotationConstants.QUOTATION_STRING_NO;
        String sOperationType = null, sOperationType2 = null;
        UserAction oUserAction =null;
        DaoFactory oDaoFactory=null;
        QuotationDao oQuotationDao =null;
        
        try {
        	/* Setting user's information in request, using the header information received through request */
            setRolesToRequest(request);
            LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "Roles set to request.");
            
        	 /* Saving Request into a Token */
            saveToken(request);
           
            oNewEnquiryForm = (NewEnquiryForm) actionForm;
    		if (oNewEnquiryForm == null)
    			oNewEnquiryForm = new NewEnquiryForm();
    		
    		sOperationType = CommonUtility.getStringParameter(request, QuotationConstants.QUOTATION_OPERATION_TYPE); 
    		
    		if (sOperationType.trim().length() == QuotationConstants.QUOTATION_LITERAL_ZERO)
    			sOperationType = oNewEnquiryForm.getOperationType();    		
    		
    		if (sOperationType.trim().length() == QuotationConstants.QUOTATION_LITERAL_ZERO)
    			sOperationType = QuotationConstants.QUOTATION_VIEWLINK_CAP;
    		
    		oNewEnquiryForm.setOperationType(sOperationType);
    		
    		/* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Instantiating the UserAction object */
            oUserAction = new UserAction();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            /* Retrieving the logged-in user information */
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            /* Retrieving the QuotationDao object */
            oQuotationDao = oDaoFactory.getQuotationDao();
            
            oNewEnquiryManager = new NewEnquiryUtilityManager();
            oNewEnquiryDto = new NewEnquiryDto();
            if (oNewEnquiryForm.getEnquiryId() < QuotationConstants.QUOTATION_LITERAL_ONE) 
            	oNewEnquiryForm.setEnquiryId(CommonUtility.getIntParameter(request, QuotationConstants.QUOTATION_REQUEST_ENQUIRYID));  
            PropertyUtils.copyProperties(oNewEnquiryDto, oNewEnquiryForm); 
            
            oNewEnquiryDto = oQuotationDao.getNewEnquiryDetailsForView(conn, oNewEnquiryDto, QuotationConstants.QUOTATION_FORWARD_VIEW.toUpperCase());
            // Load Lookup Values in NewEnquiryDto.
            oNewEnquiryDto = oQuotationDao.loadNewEnquiryLookUpValues(conn, oNewEnquiryDto);
            
            EnquiryDto oEnquiryDto = new EnquiryDto();
			PropertyUtils.copyProperties(oEnquiryDto, oNewEnquiryDto);
			
			if (oNewEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_DRAFT) {
				isValidUser = oQuotationDao.checkUserRole(conn, oEnquiryDto, oUserDto, QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_SM);
			} else if (oNewEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGDM) {
				isValidUser = oQuotationDao.checkUserRole(conn, oEnquiryDto, oUserDto, QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_DM); 
			} else if (oNewEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGCE) {
				isValidUser = oQuotationDao.checkUserRole(conn, oEnquiryDto, oUserDto, QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_CE);
			} else if (oNewEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGRSM) {
				isValidUser = oQuotationDao.checkUserRole(conn, oEnquiryDto, oUserDto, QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_RSM);
			} else if (oNewEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGSM) {
				isValidUser = oQuotationDao.checkUserRole(conn, oEnquiryDto, oUserDto, QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_SM);
			} else if (oNewEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGNSM) {
				isValidUser = oQuotationDao.checkUserRole(conn, oEnquiryDto, oUserDto, QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_NSM);
			} else if (oNewEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGMH) {
				isValidUser = oQuotationDao.checkUserRole(conn, oEnquiryDto, oUserDto, QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_MH);
			}
            
            String sApprovalType = oQuotationDao.checkApprovalType(conn, oEnquiryDto, oUserDto);
            request.setAttribute(QuotationConstants.QUOTATIONLT_APPROVAL_TYPE, sApprovalType);
            
            if (oNewEnquiryForm.getEnquiryId() < QuotationConstants.QUOTATION_LITERAL_ONE) {
            	 /* Setting error details to request to display the same in Exception page */
                request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_CRET_ROWS);
                /* Forwarding request to Error page */
                return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);            	
            }
            
            // Now check if the logged-in user has access to view this page
            oWorkFlowDto = new WorkflowDto(); 
            oWorkFlowDto.setAssignedTo(oUserDto.getUserId());
            oWorkFlowDto.setWorkflowStatus(QuotationConstants.QUOTATION_WORKFLOW_ACTION_PENDING);
            oWorkFlowDto.setEnquiryId(oNewEnquiryForm.getEnquiryId());
        	
			if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_REFACOFF_DATA)) {
				oWorkFlowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGRSM);
			} else if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_ESTOFFDATA)) {
				oWorkFlowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGCE);
				returnenquiry = CommonUtility.getStringParameter(request, QuotationConstants.QUOTATION_RETURN_ENQUIRY);
			}
            
			if ((new SearchEnquiryDaoImpl().isSalesManager(conn, oUserDto.getUserId()))) {
				request.setAttribute(QuotationConstants.QUOTATION_SMANAGER, QuotationConstants.QUOTATION_SESSION_ADMIN_FLAG_TRUE);
			}
			//isValidUser = oQuotationDao.validateUser(conn, oWorkFlowDto);
			request.setAttribute(QuotationConstants.QUOTATION_USERSSO,oUserDto.getUserId());
			
            
            //load the attachments for this enquiry
            request.setAttribute(QuotationConstants.QUOTATION_ATTACHLIST, AttachmentUtility.getNewEnquiryAttachmentDetails(oNewEnquiryDto));          	
            //load the dm uploaded attachmentlist 
            //alDmAttachmentList =AttachmentUtility.getDMUploadedAttachmentDetails(oNewEnquiryDto);
            //request.setAttribute(QuotationConstants.QUOTATION_DMATTACHLIST, alDmAttachmentList);
            //load the Intend uploaded AttachmentList
            //alIndentAttachmentList=AttachmentUtility.getIntendUploadedAttachmentDetails(oNewEnquiryDto);
            //request.setAttribute(QuotationConstants.QUOTATION_INTENDATTACHLIST, alIndentAttachmentList);
            
        	// Consolidated View QUOTATION_CONSO_VIEW
        	if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_CONSO_VIEW)) 
        	{
        		// get approvers comments
        		oWorkFlowDto = new WorkflowDto();
        		WorkflowDao oWorkFlowDao = oDaoFactory.getWorkflowDao();
        		oWorkFlowDto.setEnquiryId(oNewEnquiryForm.getEnquiryId());
        		//oNewEnquiryDto.setApproverCommentsList(oWorkFlowDao.getApproversCommentsList(conn, oWorkFlowDto));
        		
        		oTeamMemberDto = new TeamMemberDto();
        		oTeamMemberDto.setEnquiryId(oNewEnquiryForm.getEnquiryId());        		
        		oTeamMemberDto = oQuotationDao.getTeamMemberDetails(conn, oTeamMemberDto);
        		
        		//  now check the type of the logged-in user and set the type appropriately
        		if (oUserDto.getUserId().equalsIgnoreCase(oTeamMemberDto.getDesignEngineer()) 
        				|| oUserDto.getUserId().equalsIgnoreCase(oTeamMemberDto.getDesignManager())){
        			//	Design level user
        			oTeamMemberDto.setUserType(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_DM);
        			oNewEnquiryDto.setDmFlag(QuotationConstants.QUOTATION_STRING_TRUE);//added to know if user is DM/DE/RSM
        		} else if (oUserDto.getUserId().equalsIgnoreCase(oTeamMemberDto.getChiefExecutive())){
        			// CE level user
        			oTeamMemberDto.setUserType(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_CE);        			
        		} else if (oUserDto.getUserId().equalsIgnoreCase(oTeamMemberDto.getPrimaryRSM()) 
        				|| oUserDto.getUserId().equalsIgnoreCase(oTeamMemberDto.getAdditionalRSM()) ){
        			// RSM level user
        			oNewEnquiryDto.setDmFlag(QuotationConstants.QUOTATION_STRING_TRUE);//added to know if user is DM/DE/RSM
        			oTeamMemberDto.setUserType(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_RSM);        			
        		} else if (oUserDto.getUserId().equalsIgnoreCase(oTeamMemberDto.getSalesManager())){
        			// SM level user
        			oTeamMemberDto.setUserType(QuotationConstants.QUOTATION_NRM_USER);      			
        		} else{
        			// normal user
        			oTeamMemberDto.setUserType(QuotationConstants.QUOTATION_NRM_USER);         			
        		}  
        		oNewEnquiryDto.setTeamMemberDto(oTeamMemberDto);
        	}
        	//This block fetches the user details to be displayed in return users pulldown
        	if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_ESTOFFDATA)) 
        	{
        		oTeamMemberDto = new TeamMemberDto();
        		oTeamMemberDto.setEnquiryId(oNewEnquiryForm.getEnquiryId());
        		oTeamMemberDto.setMembertype(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_CE);
        		oTeamMemberDto = oQuotationDao.getTeamMemberDetails(conn, oTeamMemberDto);
        		oNewEnquiryDto.setTeamMemberList(oTeamMemberDto.getTeamMemberList());  

        		oTeamMemberDto = new TeamMemberDto();
        		oTeamMemberDto.setEnquiryId(oNewEnquiryForm.getEnquiryId());
        		oTeamMemberDto.setMembertype(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_DM);
        		oTeamMemberDto = oQuotationDao.getTeamMemberDetails(conn, oTeamMemberDto);
        		oNewEnquiryDto.setSalesManagerList(oTeamMemberDto.getTeamMemberList());
        	}
        	
        	PropertyUtils.copyProperties(oNewEnquiryForm, oNewEnquiryDto);
        	oNewEnquiryForm.setTotalRatingIndex(oNewEnquiryDto.getNewRatingsList().size());
        	oNewEnquiryForm.setCreatedBy(oUserDto.getFullName());
        	oNewEnquiryForm.setCreatedBySSO(oUserDto.getUserId());
        	
        	request.setAttribute(QuotationConstants.QUOTATION_LOWER_NEW_INQUIRYDTO, oNewEnquiryDto); 
        	
        	oNewEnquiryForm = oNewEnquiryManager.loadLookUpValues(conn, oNewEnquiryForm);
        	
        	if(sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_ESTOFFDATA))
            {
        		oNewEnquiryForm.setPagechecking(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_CE.toLowerCase()); 
            }
            if(sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_REFACOFF_DATA))
            {
            	oNewEnquiryForm.setPagechecking(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_RSM.toLowerCase());   
            }
        	
        } catch(Exception e) {
            /* Logging any exception that raised during this activity in log files using Log4j */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "home", ExceptionUtility.getStackTraceAsString(e));
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        } finally {
            /* Releasing/closing the connection object */
            if ( conn!=null )
                oDBUtility.releaseResources(conn);
        }
        LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "END");
        
        // ----------------------------------------------   Action Forward ----------------------------------------------------------------
        	request.setAttribute(QuotationConstants.QUOTATION_RETURN_ENQUIRY, returnenquiry); 
	        
        	if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_ESTOFFDATA)) {
	        	request.setAttribute(QuotationConstants.QUOTATION_PAGEHEADTYPE, QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_CE); 
	        	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_VIEWENQUIRY);        	
	        } else if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_REFACOFF_DATA)) {
	        	request.setAttribute(QuotationConstants.QUOTATION_PAGEHEADTYPE, QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_RSM);
	        	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_OFFERDATA_VIEW);    
	        } else if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_QUOTDATA)) {
	        	if(isValidUser && oNewEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGSM){
	        		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_SMAPPROVE);  
	        	} else if(isValidUser && oNewEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGRSM) {
	        		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_RSMAPPROVE);
	        	} else if(isValidUser && oNewEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGNSM) {
	        		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_NSMAPPROVE);
	        	} else if(isValidUser && oNewEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGMH) {
	        		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_MHAPPROVE);
	        	} else {
	        		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_VIEWQUOTATION);  
	        	}
	        } else if(sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_INDENTPAGEVIEW)) {        	
	        	request.setAttribute(QuotationConstants.QUOTATION_PAGEHEADTYPE, QuotationConstants.QUOTATION_FORWARD_VIEW); 
	        	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_INDENTPAGEVIEW);
	        } else {
	        	request.setAttribute(QuotationConstants.QUOTATION_PAGEHEADTYPE, QuotationConstants.QUOTATION_FORWARD_VIEW); 
	        	if(oNewEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGSM){
	        		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_SMAPPROVE);  
	        	} else if(oNewEnquiryForm.getStatusId() == QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGRSM) {
	        		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_RSMAPPROVE);
	        	} else {
	        		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_VIEW);
	        	}
	        	
	        }
	    // ----------------------------------------------   Action Forward ----------------------------------------------------------------
	}
	
	/**
	 * Method to Submit Enquiry from Pending Sales - "Released to Customer" status.
	 * Once Submitted Quotation PDF would be Generated.
	 * @param actionMapping
	 * @param actionForm
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward emailQuoteToCustomerNewEnquiry(ActionMapping actionMapping,
            ActionForm actionForm, 
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
		
		String sMethodName = QuotationConstants.QUOTAION_EMAILTO_CUSTOMER;
    	String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        NewEnquiryDto oNewEnquiryDto = null;
    	NewEnquiryForm oNewEnquiryForm = (NewEnquiryForm) actionForm;
		if (oNewEnquiryForm == null)
			oNewEnquiryForm = new NewEnquiryForm();
		
    	Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        
        UserDto oUserDto = null;
        boolean isUpdated = QuotationConstants.QUOTATION_FALSE;
        UserAction oUserAction =null;
        DaoFactory oDaoFactory =null;
        QuotationDao oQuotationDao =null;
        MTOQuotationDao oMTOQuotationDao =null;
        NewQuotationPDFUtility oPDFUtility =null;
        NewPDFUtilityManager oNewPDFUtilityManager =null;
        String sFilePath =null;
        WorkflowUtility oWorkflowUtility =null;
        WorkflowDto oWorkflowDto =null;
        String sMsg=null, sRatingResult=null, sResult=null;
        ArrayList alDmAttachmentList=null;
        KeyValueVo oKeyValueVo=null;
        String sAttachmentId=null;
        String sFileName=null;
        String sFileExten=null;
        String sAttachmentFileName=null;
        String sFinalEnquiryId=null;
        boolean isInsertUpdateComplete = false;
        
        int iWorkflowExecuteStatus = QuotationConstants.QUOTATION_LITERAL_ZERO;
        
        try {
        	if(isTokenValid(request)) {
        		setRolesToRequest(request);
                LoggerUtility.log("INFO", sClassName, sMethodName, "Roles set to request.");
                
        		resetToken(request);
        		
        		/* Creating an instance of of oEnquiryDto */
                oNewEnquiryDto = new NewEnquiryDto();
        		/* Instantiating the DBUtility object */
                oDBUtility = new DBUtility();
                /* Instantiating the UserAction object */
                oUserAction = new UserAction();
                /* Creating an instance of of DaoFactory & retrieving UserDao object */
                oDaoFactory = new DaoFactory();
                /* Retrieving the WorkflowDao object */
                oQuotationDao = oDaoFactory.getQuotationDao();
                /* Retrieving the database connection using DBUtility.getDBConnection method */
                conn = oDBUtility.getDBConnection();
                /* Retrieving the logged-in user information */
                oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
                /* Initializing NewPDFUtilityManager */
                oNewPDFUtilityManager = new NewPDFUtilityManager();
                // 
                oMTOQuotationDao = oDaoFactory.getMTOQuotationDao();
                
                /*Copying Properties from ActionForm To Dto Object*/
                PropertyUtils.copyProperties(oNewEnquiryDto, oNewEnquiryForm);    
                
                // Fetch the Notes
                oNewEnquiryDto.setSmNote(CommonUtility.getStringParameter(request, "smNote"));
                oNewEnquiryDto.setRsmNote(CommonUtility.getStringParameter(request, "rsmNote"));
                oNewEnquiryDto.setNsmNote(CommonUtility.getStringParameter(request, "nsmNote"));
                oNewEnquiryDto.setMhNote(CommonUtility.getStringParameter(request, "mhNote"));
                
                oDBUtility.setAutoCommitFalse(conn);
                
                // Save the Notes
                oQuotationDao.saveNewEnquiryNotes(conn, oNewEnquiryDto);
                
                // Fetch Quoted Prices By SM and Save as Standard prices
                oNewEnquiryDto.setApprovalUserType("RELEASE_SM");
                ArrayList<NewRatingDto> alRatingsList = getRatingsCommercialDetails_ForApproval(conn, oQuotationDao, oNewEnquiryDto, request);
                oNewEnquiryDto.setNewRatingsList(alRatingsList);
                // Save the Quoted Discount and Quoted Price.
                sRatingResult = oQuotationDao.savePrices_ReleaseSM(conn, oNewEnquiryDto);
                if (QuotationConstants.QUOTATION_STRING_Y.equals(sRatingResult)) {
    				oDBUtility.commit(conn);
    				isInsertUpdateComplete = true;
    			} else {
    				isInsertUpdateComplete = false;
    				oDBUtility.rollback(conn);
    			}
                
                // Fetch RatingPDF List Details.
                ArrayList<NewTechnicalOfferDto> alRatingPDFList = oNewPDFUtilityManager.fetchRatingPDFList(conn, oNewEnquiryDto);
                // Save Each RatingPDF Content - In PDFRatingDetails Table.
                sResult = oMTOQuotationDao.savePDFRatingsList(conn, oNewEnquiryDto, alRatingPDFList);
                if (QuotationConstants.QUOTATION_STRING_Y.equals(sResult)) {
    				oDBUtility.commit(conn);
    				isInsertUpdateComplete = true;
    			} else {
    				isInsertUpdateComplete = false;
    				oDBUtility.rollback(conn);
    			}
                
                oDBUtility.setAutoCommitTrue(conn);
                
                oNewEnquiryDto.setLastModifiedBy(oUserDto.getUserId());
                // Set "isPdfView" true - To Fetch Remarks List
    			oNewEnquiryDto.setPdfView(true);
    			
                oNewEnquiryDto = oQuotationDao.getNewEnquiryDetailsForView(conn, oNewEnquiryDto, QuotationConstants.QUOTATION_FORWARD_VIEW.toUpperCase());
                
                //  PDF Generation
                oPDFUtility = new NewQuotationPDFUtility();
                StringBuffer sbFilePathValue = new StringBuffer();
                
                sbFilePathValue.append(oPDFUtility.generateQuotationPDF(oNewEnquiryDto, request, QuotationConstants.QUOTATIONLT_PDFTYPE_COMMERCIAL));
                sbFilePathValue.append(QuotationConstants.QUOTATION_SPLITQSYMBOL);
                sbFilePathValue.append(oPDFUtility.generateQuotationPDF(oNewEnquiryDto, request, QuotationConstants.QUOTATIONLT_PDFTYPE_TECHNICAL));
                
                sFilePath = sbFilePathValue.toString();
                
                sFinalEnquiryId = QuotationConstants.QUOTATION_STRING_DOUBLEDIGZERO + oNewEnquiryDto.getEnquiryId()+QuotationConstants.QUOTATION_R1;
                
                String[] oCustomerArray = null;
                String sCustomer = oNewEnquiryDto.getCustomerName();
                if (sCustomer != null && !sCustomer.equals("")) {
        			oCustomerArray = sCustomer.split(" ");
        			if (oCustomerArray.length >= QuotationConstants.QUOTATION_LITERAL_2)
        				sCustomer = oCustomerArray[0] + " " + oCustomerArray[1];
        			else
        				sCustomer = oCustomerArray[0];
        		}
                
                StringBuffer sbAttachmentFileNameValue = new StringBuffer();
                
                sbAttachmentFileNameValue.append(QuotationConstants.QUOTATION_STRING_QUOTATION+QuotationConstants.QUOTATION_STRING_UNDERSC
                						+ sFinalEnquiryId+QuotationConstants.QUOTATION_STRING_UNDERSC
                						+ sCustomer+QuotationConstants.QUOTATION_STRING_UNDERSC
                						+ DateUtility.getCurrentYear() + QuotationConstants.QUOTATION_STRING_UNDERSC 
                						+ DateUtility.getCurrentMonth()+ QuotationConstants.QUOTATION_STRING_UNDERSC 
                						+ QuotationConstants.QUOTATIONLT_PDFTYPE_COMMERCIAL + QuotationConstants.QUOTATION_PDFEXT);
                sbAttachmentFileNameValue.append(QuotationConstants.QUOTATION_SPLITQSYMBOL);
                sbAttachmentFileNameValue.append(QuotationConstants.QUOTATION_STRING_QUOTATION+QuotationConstants.QUOTATION_STRING_UNDERSC
						+ sFinalEnquiryId+QuotationConstants.QUOTATION_STRING_UNDERSC
						+ sCustomer+QuotationConstants.QUOTATION_STRING_UNDERSC
						+ DateUtility.getCurrentYear() + QuotationConstants.QUOTATION_STRING_UNDERSC 
						+ DateUtility.getCurrentMonth()+ QuotationConstants.QUOTATION_STRING_UNDERSC 
						+ QuotationConstants.QUOTATIONLT_PDFTYPE_TECHNICAL + QuotationConstants.QUOTATION_PDFEXT);
                
                sAttachmentFileName = sbAttachmentFileNameValue.toString();
                
                
                /* Get DM Uploaded Attachment List 	 */ 
                alDmAttachmentList = AttachmentUtility.getNewEnqDMUploadedAttachmentDetails(oNewEnquiryDto);
				if (alDmAttachmentList.size() > 0) {
					for (int i = 0; i < alDmAttachmentList.size(); i++) {
						oKeyValueVo = (KeyValueVo) alDmAttachmentList.get(i);
						sAttachmentId = oKeyValueVo.getKey();	// Attachment Id
						sFileName = oKeyValueVo.getValue(); 	// File Name
						sFileExten = oKeyValueVo.getValue2(); 	// Extension
						oNewEnquiryDto.setAttachmentFileName(sFileName + QuotationConstants.QUOTATION_PERIOD + sFileExten);
						oNewEnquiryDto.setAttachmentId(Integer.parseInt(sAttachmentId.equals(null) ? QuotationConstants.QUOTATION_STRING_ZERO : sAttachmentId));
						
						sFilePath = sFilePath + QuotationConstants.QUOTATION_SPLITQSYMBOL
								+ oPDFUtility.generateNewEnqDmUploadedAttachments(oNewEnquiryDto, request);

						sAttachmentFileName = sAttachmentFileName + QuotationConstants.QUOTATION_SPLITQSYMBOL
								+ sFileName + QuotationConstants.QUOTATION_PERIOD + sFileExten;
					}
				}
				
                
                /*
                 * #############################################################
                 * Workflow process initiated
                 * #############################################################
                 */
                
				LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Initiating Workflow process - - -");

                oWorkflowUtility = new WorkflowUtility();
                oWorkflowDto = new WorkflowDto();
                oWorkflowDto.setEnquiryId(oNewEnquiryDto.getEnquiryId());
                oWorkflowDto.setEnquiryNo(oNewEnquiryDto.getEnquiryNumber());
                oWorkflowDto.setLoginUserId(oUserDto.getUserId());
                oWorkflowDto.setLoginUserName(oUserDto.getFullName());
                oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
                oWorkflowDto.setCcEmailTo(oUserDto.getEmailId());
                oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_RELEASETOCUSTOMER);
                oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGSM);
                oWorkflowDto.setAttachmentFile(sFilePath);
                oWorkflowDto.setAttachmentFileName(sAttachmentFileName);
            	
                LoggerUtility.log("INFO", sClassName, sMethodName, "11 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
                iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
                
                if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE ){
                    LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Successfully");
                } else  if ( iWorkflowExecuteStatus== QuotationConstants.QUOTATION_LITERAL_ZERO){
                    LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed - But email could not be sent. Please send email manually.");
                } else {
                    LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Failed");
                }
                LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Workflow process completed - - -");
                
                /*
                 * #############################################################
                 * Workflow process completed
                 * #############################################################
                 */
                
                oPDFUtility.deletePDF(sFilePath);

                sMsg = QuotationConstants.QUOTATION_QUOTESUC;
                request.setAttribute(QuotationConstants.QUOTATION_HEADINGMESSAGE,QuotationConstants.QUOTATION_EMAILCUSTCONFIRM);
                request.setAttribute(QuotationConstants.QUOTATION_SUCCESSMESSAGE,sMsg);
                request.setAttribute(QuotationConstants.QUOTATION_LINKSMESSAGE,"");
                
        	} else {
        		request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE,QuotationConstants.QUOTATION_INVALIDTOKENMESSAGE);
        		/* Forwarding request to Error page */
                return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        	}
        	if(!isInsertUpdateComplete) {
        		request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE,QuotationConstants.QUOTATION_ER_INSERT);
        		// Forwarding request to Error page 
        		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        	}
        } catch(Exception e) {
        	e.printStackTrace();
            /* Logging any exception that raised during this activity in log files using Log4j */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "home", ExceptionUtility.getStackTraceAsString(e));
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        } finally {
            /* Releasing/closing the connection object */
            if ( conn!=null ) 
                oDBUtility.releaseResources(conn);
        }
    	
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        
    	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);
    	
	}
	
	/**
	 * Method to Auto Save all Ratings.
	 * @param actionMapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward processRatingsAutoSaveAjax(ActionMapping actionMapping, 
			ActionForm form,
			HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		String sMethodName = "processRatingsAutoSaveAjax"; 
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        Connection conn = null;
		PrintWriter oOut = null;
		UserDto oUserDto = null;
		UserAction oUserAction = null;
        DaoFactory oDaoFactory = null; 
        QuotationDao oQuotationDao = null;
        NewEnquiryDto oNewEnquiryDto = null;
        DBUtility oDBUtility = new DBUtility();
        NewEnquiryUtilityManager oNewEnquiryManager = new NewEnquiryUtilityManager();
        Map<String,String> keyvalueMap = new HashMap<>();
        String sRatingResult = null;
        boolean isInsertUpdateComplete = false; 
        
        setRolesToRequest(request);
        response.setContentType("application/json");
        
        try {
        	conn = oDBUtility.getDBConnection();
        	oDaoFactory = new DaoFactory();
        	oQuotationDao = oDaoFactory.getQuotationDao();
        	oNewEnquiryDto = new NewEnquiryDto();
        	oUserAction = new UserAction();
        	
        	/* Retrieving the logged-in user information */
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            if (oUserDto == null){
            	oUserDto = new UserDto();
            }
            oNewEnquiryDto.setCreatedBy(oUserDto.getFullName());
        	oNewEnquiryDto.setCreatedBySSO(oUserDto.getUserId());
        	
            /*
        	String data = (String) request.getParameter("data");
        	ObjectMapper objectMapper = new ObjectMapper();
        	HashMap<String, Object> map = new HashMap<String, Object>();
			map = objectMapper.readValue(data, HashMap.class);
			*/
        	
        	oDBUtility.setAutoCommitFalse(conn);
        	
        	if(StringUtils.isNotBlank(CommonUtility.getStringParameter(request, "enquiryId"))) {
        		oNewEnquiryDto.setEnquiryId(Integer.parseInt(CommonUtility.getStringParameter(request, "enquiryId")));
        	}
        	if(StringUtils.isBlank(CommonUtility.getStringParameter(request, "revisonNumberVal"))) {
        		oNewEnquiryDto.setRevisionNumber(new Integer(1));
        	} else {
        		oNewEnquiryDto.setRevisionNumber(Integer.parseInt(CommonUtility.getStringParameter(request, "revisonNumberVal")));
        	}
        	if(StringUtils.isBlank(CommonUtility.getStringParameter(request, "locationVal"))) {
        		oNewEnquiryDto.setLocationId(new Integer(0));
        	} else {
        		oNewEnquiryDto.setLocationId(Integer.parseInt(CommonUtility.getStringParameter(request, "locationVal")));
        	}
        	oNewEnquiryDto.setCustomerId(Integer.parseInt(CommonUtility.getStringParameter(request, "customerIdVal")));
        	oNewEnquiryDto.setEnquiryType(CommonUtility.getStringParameter(request, "currEnquiryTypeVal"));
        	
        	
        	if(StringUtils.isBlank(CommonUtility.getStringParameter(request, "enquiryId")) || oNewEnquiryDto.getEnquiryId() == 0) {
        		oNewEnquiryDto = oQuotationDao.createNewEnquiry(conn, oNewEnquiryDto, "INSERT");
        		
        		if(oNewEnquiryDto.getEnquiryId() > 0) {
            		oDBUtility.commit(conn);
            	} else {
            		oDBUtility.rollback(conn);
            	}
        	}
        	
        	NewRatingDto oCurrentRatingDto = oNewEnquiryManager.getRatingObjectFromRequest(conn, oQuotationDao, oNewEnquiryDto, request);
        	sRatingResult = oQuotationDao.saveRatingByIdDetails(conn, oCurrentRatingDto);
        	
        	if (QuotationConstants.QUOTATION_STRING_Y.equals(sRatingResult)) {
				oDBUtility.commit(conn);
				isInsertUpdateComplete = true;
			} else {
				oDBUtility.rollback(conn);
				isInsertUpdateComplete = false;
			}
        	oDBUtility.setAutoCommitTrue(conn);
        	
        	String[] sValuesArray = new String[2];
			if (QuotationConstants.QUOTATION_STRING_Y.equals(sRatingResult)) {
				sValuesArray[0] = String.valueOf(oNewEnquiryDto.getEnquiryId());
				getAjax(response, conn, QuotationConstants.QUOTATION_FORWARD_SUCCESS, sValuesArray);
			} else {
				sValuesArray[0] = QuotationConstants.QUOTATION_EMPTY;
				getAjax(response, conn, QuotationConstants.QUOTATION_FORWARD_FAILURE, sValuesArray);
			}
			
        } catch(Exception e) {
        	//e.printStackTrace();
        } finally {
            /* Releasing/closing the connection object */
            if ( conn!=null ) 
                oDBUtility.releaseResources(conn);
        }
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
    	return null;
	}
	
	/**
	 * 
	 * @param actionMapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward fetchCustomerTypeByAjax(ActionMapping actionMapping, 
			ActionForm form,
			HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		String sMethodName = "fetchCustomerTypeByAjax"; 
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        Connection conn = null;
		PrintWriter oOut = null;
        DBUtility oDBUtility = new DBUtility();
        DaoFactory oDaoFactory = null; //Creating an instance of of DaoFactory & retrieving UserDao object
        CustomerDao oCustomerDao = null;
        Map<String,String> keyvalueMap = new HashMap<>();
        String sCustomerType = "";
        
        setRolesToRequest(request);
        response.setContentType("application/json");
        
        try {
        	conn = oDBUtility.getDBConnection();
        	oDaoFactory = new DaoFactory();
        	oCustomerDao = oDaoFactory.getCustomerDao();
        	
        	String data = (String) request.getParameter("reqparams");
        	ObjectMapper objectMapper = new ObjectMapper();
        	HashMap<String, Object> map = new HashMap<String, Object>();
			map = objectMapper.readValue(data, HashMap.class);
			//System.out.println("Map values: " + map.toString());
			CustomerDto  oCustomerDto = new CustomerDto();
			oCustomerDto.setCustomerId(Integer.parseInt((String)map.get("customerIdVal")));
			
			sCustomerType = oCustomerDao.fetchCustomerTypeById(conn, oCustomerDto);
			
			keyvalueMap.put("returnValue1", sCustomerType);
			response.setHeader("X-JSON", JSONValue.toJSONString(keyvalueMap));	
			oOut = response.getWriter();
			oOut.write(JSONValue.toJSONString(keyvalueMap));
			oOut.flush();
			oOut.close();
        } catch(Exception e) {
        	//e.printStackTrace();
        } finally {
            /* Releasing/closing the connection object */
            if ( conn!=null ) 
                oDBUtility.releaseResources(conn);
        }
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
    	return null;
	}
	
	/**
	 * 
	 * @param actionMapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward loadCustomerLocationByAjax(ActionMapping actionMapping, 
			ActionForm form,
			HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		String sMethodName = "loadCustomerLocationByAjax"; 
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        Connection conn = null;
		PrintWriter oOut = null;
        DBUtility oDBUtility = new DBUtility();
        DaoFactory oDaoFactory = null; //Creating an instance of of DaoFactory & retrieving UserDao object
        CustomerDao oCustomerDao = null;
        setRolesToRequest(request);
        ArrayList<KeyValueVo> alCustomerLocations = new ArrayList<KeyValueVo>();
        Map<String,String> keyvalueMap = new HashMap<>();
        response.setContentType("application/json");
        
        try {
        	conn = oDBUtility.getDBConnection();
        	oDaoFactory = new DaoFactory();
        	oCustomerDao = oDaoFactory.getCustomerDao();
        	
        	String data = (String) request.getParameter("reqparams");
        	ObjectMapper objectMapper = new ObjectMapper();
        	HashMap<String, Object> map = new HashMap<String, Object>();
			map = objectMapper.readValue(data, HashMap.class);
			//System.out.println("Map values: " + map.toString());
			CustomerDto  oCustomerDto = new CustomerDto();
			oCustomerDto.setCustomerId(Integer.parseInt((String)map.get("customerIdVal")));
			
			alCustomerLocations = oCustomerDao.fetchLocationsByCustomerId(conn, oCustomerDto);
			
			if(CollectionUtils.isNotEmpty(alCustomerLocations)) {
				for(KeyValueVo vo : alCustomerLocations) {
					keyvalueMap.put(vo.getKey(), vo.getValue());
				}
			}
			//System.out.println("jsonValue: " + JSONValue.toJSONString(keyvalueMap));
			response.setHeader("X-JSON", JSONValue.toJSONString(keyvalueMap));	
			oOut = response.getWriter();
			oOut.write(JSONValue.toJSONString(keyvalueMap));
			oOut.flush();
			oOut.close();
        } catch(Exception e) {
        	//e.printStackTrace();
        } finally {
            /* Releasing/closing the connection object */
            if ( conn!=null ) 
                oDBUtility.releaseResources(conn);
        }
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
    	return null;
	}
	
	/**
	 * 
	 * @param actionMapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward loadCustomerContactByAjax(ActionMapping actionMapping, 
			ActionForm form,
			HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		String sMethodName = "loadCustomerLocationByAjax"; 
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
		PrintWriter oOut = null;
        DBUtility oDBUtility = new DBUtility();
        DaoFactory oDaoFactory = null; //Creating an instance of of DaoFactory & retrieving UserDao object
        CustomerDao oCustomerDao = null;
        setRolesToRequest(request);
        ArrayList<KeyValueVo> alCustomerContacts = new ArrayList<KeyValueVo>();
        Map<String,String> keyvalueMap = new HashMap<>();
        response.setContentType("application/json");
        
        try (Connection conn = oDBUtility.getDBConnection()) {
        	oDaoFactory = new DaoFactory();
        	oCustomerDao = oDaoFactory.getCustomerDao();
        	
        	String data = (String) request.getParameter("reqparams");
        	ObjectMapper objectMapper = new ObjectMapper();
        	HashMap<String, Object> map = new HashMap<String, Object>();
			map = objectMapper.readValue(data, HashMap.class);
			CustomerDto  oCustomerDto = new CustomerDto();
			oCustomerDto.setCustomerId(Integer.parseInt((String)map.get("customerIdVal")));
			
			alCustomerContacts = oCustomerDao.fetchContactsByCustomerId(conn, oCustomerDto);
			
			if(CollectionUtils.isNotEmpty(alCustomerContacts)) {
				for(KeyValueVo vo : alCustomerContacts) {
					keyvalueMap.put(vo.getKey(), vo.getValue());
				}
			}
			response.setHeader("X-JSON", JSONValue.toJSONString(keyvalueMap));	
			oOut = response.getWriter();
			oOut.write(JSONValue.toJSONString(keyvalueMap));
			oOut.flush();
			oOut.close();
        }
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
    	return null;
	}
	
	/**
	 * 
	 * @param actionMapping
	 * @param form
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward loadCustomerIndustryByAjax(ActionMapping actionMapping, 
			ActionForm form,
			HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		String sMethodName = "loadCustomerLocationByAjax"; 
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        PrintWriter oOut = null;
        Map<String,String> keyvalueMap = new HashMap<>();
        DBUtility oDBUtility = new DBUtility();
        DaoFactory oDaoFactory = null; //Creating an instance of of DaoFactory & retrieving UserDao object
        CustomerDao oCustomerDao = null;
        setRolesToRequest(request);
        response.setContentType("application/json");
        
        try (Connection conn = oDBUtility.getDBConnection()) {
        	oDaoFactory = new DaoFactory();
        	oCustomerDao = oDaoFactory.getCustomerDao();
        	
        	String data = (String) request.getParameter("reqparams");
        	ObjectMapper objectMapper = new ObjectMapper();
        	HashMap<String, Object> map = new HashMap<String, Object>();
			map = objectMapper.readValue(data, HashMap.class);
			CustomerDto  oCustomerDto = new CustomerDto();
			oCustomerDto.setCustomerId(Integer.parseInt((String)map.get("customerIdVal")));
			
			oCustomerDto = oCustomerDao.getCustomerInfo(conn, oCustomerDto);
			
			keyvalueMap.put("customerIndustry", oCustomerDto.getIndustry());
			response.setHeader("X-JSON", JSONValue.toJSONString(keyvalueMap));	
			oOut = response.getWriter();
			oOut.write(JSONValue.toJSONString(keyvalueMap));
			oOut.flush();
			oOut.close();
        }
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
    	return null;
	}
	
	private ArrayList<NewRatingDto> getRatingsCommercialDetails_ForApproval(Connection conn, QuotationDao oQuotationDao, NewEnquiryDto oNewEnquiryDto, HttpServletRequest request) {
		String sMethodName = "getRatingsCommercialDetails_ForApproval"; 
		String sClassName = this.getClass().getName();
		LoggerUtility.log("INFO", sClassName, sMethodName, "START");
		
		NewEnquiryUtilityManager oNewEnquiryManager = new NewEnquiryUtilityManager();
		ArrayList<NewRatingDto> alRatingsList = new ArrayList<NewRatingDto>();
		int totalRatingIndex = oNewEnquiryDto.getTotalRatingIndex();
		
		try {
			NewRatingDto oTempNewRatingDto = null;
			for (int iRatingIndex = 1; iRatingIndex <= totalRatingIndex; iRatingIndex++) {
				oTempNewRatingDto = new NewRatingDto();
				oTempNewRatingDto.setRatingId(Integer.parseInt(CommonUtility.getStringParameter(request, "ratingObjId" + iRatingIndex)));
				
				oTempNewRatingDto = oQuotationDao.fetchRatingByRatingId(conn, oTempNewRatingDto);
				
				oTempNewRatingDto.setLtCustomerId(String.valueOf(oNewEnquiryDto.getCustomerId()));
				
				// For Approval : Set the Requested Discount from RSM / NSM / MH Approval Page.
				if("APPRV_RSM".equals(oNewEnquiryDto.getApprovalUserType())) {
					oTempNewRatingDto.setLtApprovedDiscount_RSM(CommonUtility.getStringParameter(request, "aprvDiscount_RSM" + iRatingIndex));
				} else if("SUBMIT_NSM".equals(oNewEnquiryDto.getApprovalUserType())) {
					oTempNewRatingDto.setLtRequestedDiscount_RSM(CommonUtility.getStringParameter(request, "reqDiscount_RSM" + iRatingIndex));
				} else if("APPRV_NSM".equals(oNewEnquiryDto.getApprovalUserType())) {
					oTempNewRatingDto.setLtApprovedDiscount_NSM(CommonUtility.getStringParameter(request, "aprvDiscount_NSM" + iRatingIndex));
				} else if("SUBMIT_MH".equals(oNewEnquiryDto.getApprovalUserType())) {
					oTempNewRatingDto.setLtRequestedDiscount_NSM(CommonUtility.getStringParameter(request, "reqDiscount_NSM" + iRatingIndex));
				} else if("APPRV_MH".equals(oNewEnquiryDto.getApprovalUserType())) {
					oTempNewRatingDto.setLtApprovedDiscount_MH(CommonUtility.getStringParameter(request, "aprvDiscount_MH" + iRatingIndex));
				} else if("RELEASE_SM".equals(oNewEnquiryDto.getApprovalUserType())) {
					oTempNewRatingDto.setLtQuotedDiscount(CommonUtility.getStringParameter(request, "quotedDiscount" + iRatingIndex));
				}
				
				oNewEnquiryManager.calculateNewEnquiryPrices(oTempNewRatingDto);
				
				alRatingsList.add(oTempNewRatingDto);
			}
		} catch (Exception e) {
			//e.printStackTrace();
			LoggerUtility.log("ERROR: ", this.getClass().getName(), sMethodName, ExceptionUtility.getStackTraceAsString(e));
		}
		
		LoggerUtility.log("INFO", sClassName, sMethodName, "END");
		return alRatingsList;
	}
	
	private ArrayList<NewRatingDto> getRatingsCommercialDetails_ForSM(Connection conn, QuotationDao oQuotationDao, NewEnquiryDto oNewEnquiryDto, HttpServletRequest request) {
		String sMethodName = "getRatingsCommercialDetails_ForSM"; 
		String sClassName = this.getClass().getName();
		LoggerUtility.log("INFO", sClassName, sMethodName, "START");
		
		NewEnquiryUtilityManager oNewEnquiryManager = new NewEnquiryUtilityManager();
		ArrayList<NewRatingDto> alRatingsList = new ArrayList<NewRatingDto>();
		int totalRatingIndex = oNewEnquiryDto.getTotalRatingIndex();
		
		try {
			NewRatingDto oTempNewRatingDto = null;
			for (int iRatingIndex = 1; iRatingIndex <= totalRatingIndex; iRatingIndex++) {
				oTempNewRatingDto = new NewRatingDto();
				oTempNewRatingDto.setRatingId(Integer.parseInt(CommonUtility.getStringParameter(request, "ratingObjId" + iRatingIndex)));
				
				oTempNewRatingDto = oQuotationDao.fetchRatingByRatingId(conn, oTempNewRatingDto);
				
				oTempNewRatingDto.setLtCustomerId(String.valueOf(oNewEnquiryDto.getCustomerId()));
				
				// Sales Manager : Set the Requested / Quoted Discount from SM Approve Page.
				oTempNewRatingDto.setLtRequestedDiscount_SM(CommonUtility.getStringParameter(request, "requestedDiscount" + iRatingIndex));
				oTempNewRatingDto.setLtQuotedDiscount(CommonUtility.getStringParameter(request, "quotedDiscount" + iRatingIndex));
				
				oNewEnquiryManager.calculateNewEnquiryPrices(oTempNewRatingDto);
				
				alRatingsList.add(oTempNewRatingDto);
			}
		} catch (Exception e) {
			//e.printStackTrace();
			LoggerUtility.log("ERROR: ", this.getClass().getName(), sMethodName, ExceptionUtility.getStackTraceAsString(e));
		}
		
		LoggerUtility.log("INFO", sClassName, sMethodName, "END");
		return alRatingsList;
	}
	
	/**
     * This method is used to Submit SM "Requested Discount" to RSM
     * @param actionMapping ActionMapping object to redirect the user to next page depending on the activity
     * @param actionForm ActionForm object to handle the form elements
     * @param request HttpServletRequest object to handle request operations
     * @param response HttpServletResponse object to handle response operations
     * @return returns ActionForward depending on which user is redirected to next page
     * @throws Exception if any error occurs while performing database operations, etc
     */
	public ActionForward submitToRSM(ActionMapping actionMapping,
            ActionForm actionForm, 
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {

		String sMethodName = QuotationConstants.QUOTATION_MTHD_SUBMITTORSM; 
		String sClassName = this.getClass().getName();
		LoggerUtility.log("INFO", sClassName, sMethodName, "START");
		
		Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        UserDto oUserDto = null;
        UserAction oUserAction=null; //Instantiating the UserAction object
        DaoFactory oDaoFactory=null; //Creating an instance of of DaoFactory & retrieving UserDao object
        QuotationDao oQuotationDao=null;// Creating an Instance for QuotationDao
        String sMsg=null,sLinkMsg1=null,sLinkMsg=null,sRatingResult=null;
        int iWorkflowExecuteStatus=0;
        //boolean isUpdatedRating=QuotationConstants.QUOTATION_FALSE;
		NewEnquiryForm oNewEnquiryForm = null;
		NewEnquiryDto oNewEnquiryDto = null;
		WorkflowUtility oWorkflowUtility = null;
		WorkflowDto oWorkflowDto = null;
		boolean isInsertUpdateComplete = false;
		
		try {
			setRolesToRequest(request);
            LoggerUtility.log("INFO", sClassName, sMethodName, "Roles set to request.");
            
    		oNewEnquiryForm = (NewEnquiryForm) actionForm;
    		if (oNewEnquiryForm == null)
    			oNewEnquiryForm = new NewEnquiryForm();
    		
            /* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Instantiating the UserAction object */
            oUserAction = new UserAction();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
            /* Create QuotationDao */
            oQuotationDao = oDaoFactory.getQuotationDao();
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            
            /* Retrieving the logged-in user information */
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            if (oUserDto == null){
            	oUserDto = new UserDto();
            }
            
            oNewEnquiryDto = new NewEnquiryDto();
            oNewEnquiryForm.setCreatedBy(oUserDto.getFullName());           
            oNewEnquiryForm.setCreatedBySSO(oUserDto.getUserId());
            PropertyUtils.copyProperties(oNewEnquiryDto, oNewEnquiryForm);
            // Fetch the Notes
            oNewEnquiryDto.setSmNote(CommonUtility.getStringParameter(request, "smNote"));
            oNewEnquiryDto.setRsmNote(CommonUtility.getStringParameter(request, "rsmNote"));
            oNewEnquiryDto.setNsmNote(CommonUtility.getStringParameter(request, "nsmNote"));
            oNewEnquiryDto.setMhNote(CommonUtility.getStringParameter(request, "mhNote"));
            
            LoggerUtility.log("INFO", sClassName, sMethodName, "iEnquiryId -  " + oNewEnquiryDto.getEnquiryId());
            
            oDBUtility.setAutoCommitFalse(conn);
            
            // Save the Notes
            oQuotationDao.saveNewEnquiryNotes(conn, oNewEnquiryDto);
            
            ArrayList<NewRatingDto> alRatingsList = getRatingsCommercialDetails_ForSM(conn, oQuotationDao, oNewEnquiryDto, request);
            oNewEnquiryDto.setNewRatingsList(alRatingsList);
            
            sRatingResult = oQuotationDao.savePrices_SubmitRSM(conn, oNewEnquiryDto);
            
            if (QuotationConstants.QUOTATION_STRING_Y.equals(sRatingResult)) {
				oDBUtility.commit(conn);
				isInsertUpdateComplete = true;
			} else {
				isInsertUpdateComplete = false;
				oDBUtility.rollback(conn);
			}
            oDBUtility.setAutoCommitTrue(conn);
            
         	// Workflow :  SUBMIT TO RSM
            resetToken(request);
		    LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Initiating Workflow process - - -");
		    oWorkflowUtility = new WorkflowUtility();
            oWorkflowDto = new WorkflowDto();
            oWorkflowDto.setEnquiryId(oNewEnquiryDto.getEnquiryId());
            oWorkflowDto.setEnquiryNo(oNewEnquiryDto.getEnquiryNumber());
            oWorkflowDto.setLoginUserId(oUserDto.getUserId());
            oWorkflowDto.setLoginUserName(oUserDto.getFullName());
            oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
            oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTORSM);
            oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGSM);
            
            LoggerUtility.log("INFO", sClassName, sMethodName, "2 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
            iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
            
            if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE ) {
                LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFS);
            } else  if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ZERO){ 
                LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFSEMAILF);
            } else {
                LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFF);
            }
            
            if(!isInsertUpdateComplete) {
        		request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE,QuotationConstants.QUOTATION_ER_INSERT);
        		
        		// Forwarding request to Error page 
        		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        	} else {
        		sMsg = QuotationConstants.QUOTATION_SUBMITTORSM_TRACKNO + oNewEnquiryDto.getEnquiryNumber();
        		
        		sLinkMsg1 = QuotationConstants.QUOTATION_RETURNHOME_URL + QuotationConstants.QUOTATION_GRSYMBOL + QuotationConstants.QUOTATION_VIEWLINK_RETURNHOME; 
                sLinkMsg = sLinkMsg1; 
                
                request.setAttribute(QuotationConstants.QUOTATION_HEADINGMESSAGE,QuotationConstants.QUOTATION_SUB_SUCES);
                request.setAttribute(QuotationConstants.QUOTATION_SUCCESSMESSAGE,sMsg);
                request.setAttribute(QuotationConstants.QUOTATION_LINKSMESSAGE,sLinkMsg);
        	}
		} catch(Exception e) {
            /* Logging any exception that raised during this activity in log files using Log4j */
            LoggerUtility.log("DEBUG", sClassName, sMethodName, ExceptionUtility.getStackTraceAsString(e));
            //e.printStackTrace();
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        } finally {
            /* Releasing/closing the connection object */
            if ( conn!=null )
                oDBUtility.releaseResources(conn);
        }
		
		LoggerUtility.log("INFO", sClassName, sMethodName, "END");
    	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);
	}
	
	/**
     * This method is used for RSM Approval - Return "Approved Discount" to SM
     * @param actionMapping ActionMapping object to redirect the user to next page depending on the activity
     * @param actionForm ActionForm object to handle the form elements
     * @param request HttpServletRequest object to handle request operations
     * @param response HttpServletResponse object to handle response operations
     * @return returns ActionForward depending on which user is redirected to next page
     * @throws Exception if any error occurs while performing database operations, etc
     */
	public ActionForward approveByRSM(ActionMapping actionMapping,
            ActionForm actionForm, 
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
		
		String sMethodName = QuotationConstants.QUOTATION_MTHD_APPROVEBYRSM; 
		String sClassName = this.getClass().getName();
		LoggerUtility.log("INFO", sClassName, sMethodName, "START");
		
		Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        UserDto oUserDto = null;
        UserAction oUserAction=null; //Instantiating the UserAction object
        DaoFactory oDaoFactory=null; //Creating an instance of of DaoFactory & retrieving UserDao object
        QuotationDao oQuotationDao=null;// Creating an Instance for QuotationDao
        String sMsg=null,sLinkMsg1=null,sLinkMsg=null,sRatingResult=null;
        int iWorkflowExecuteStatus=0;
        //boolean isUpdatedRating=QuotationConstants.QUOTATION_FALSE;
		NewEnquiryForm oNewEnquiryForm = null;
		NewEnquiryDto oNewEnquiryDto = null;
		WorkflowUtility oWorkflowUtility = null;
		WorkflowDto oWorkflowDto = null;
		boolean isInsertUpdateComplete = false;
		
		try {
			setRolesToRequest(request);
            LoggerUtility.log("INFO", sClassName, sMethodName, "Roles set to request.");
            
    		oNewEnquiryForm = (NewEnquiryForm) actionForm;
    		if (oNewEnquiryForm == null)
    			oNewEnquiryForm = new NewEnquiryForm();
    		
            /* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Instantiating the UserAction object */
            oUserAction = new UserAction();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
            /* Create QuotationDao */
            oQuotationDao = oDaoFactory.getQuotationDao();
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            
            /* Retrieving the logged-in user information */
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            if (oUserDto == null){
            	oUserDto = new UserDto();
            }
            
            oNewEnquiryDto = new NewEnquiryDto();
            oNewEnquiryForm.setCreatedBy(oUserDto.getFullName());           
            oNewEnquiryForm.setCreatedBySSO(oUserDto.getUserId());
            PropertyUtils.copyProperties(oNewEnquiryDto, oNewEnquiryForm);  
            // Fetch the Notes
            oNewEnquiryDto.setSmNote(CommonUtility.getStringParameter(request, "smNote"));
            oNewEnquiryDto.setRsmNote(CommonUtility.getStringParameter(request, "rsmNote"));
            oNewEnquiryDto.setNsmNote(CommonUtility.getStringParameter(request, "nsmNote"));
            oNewEnquiryDto.setMhNote(CommonUtility.getStringParameter(request, "mhNote"));
            
            LoggerUtility.log("INFO", sClassName, sMethodName, "iEnquiryId -  " + oNewEnquiryDto.getEnquiryId());
            
            oDBUtility.setAutoCommitFalse(conn);
            
            // Save the Notes
            oQuotationDao.saveNewEnquiryNotes(conn, oNewEnquiryDto);
            
            oNewEnquiryDto.setApprovalUserType("APPRV_RSM");
            ArrayList<NewRatingDto> alRatingsList = getRatingsCommercialDetails_ForApproval(conn, oQuotationDao, oNewEnquiryDto, request);
            oNewEnquiryDto.setNewRatingsList(alRatingsList);
            
            sRatingResult = oQuotationDao.savePrices_ApproveRSM(conn, oNewEnquiryDto);
            
            if (QuotationConstants.QUOTATION_STRING_Y.equals(sRatingResult)) {
				oDBUtility.commit(conn);
				isInsertUpdateComplete = true;
			} else {
				isInsertUpdateComplete = false;
				oDBUtility.rollback(conn);
			}
            oDBUtility.setAutoCommitTrue(conn);
            
            // Workflow :  APPROVE BY RSM
            resetToken(request);
		    LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Initiating Workflow process - - -");
		    oWorkflowUtility = new WorkflowUtility();
            oWorkflowDto = new WorkflowDto();
            oWorkflowDto.setEnquiryId(oNewEnquiryDto.getEnquiryId());
            oWorkflowDto.setEnquiryNo(oNewEnquiryDto.getEnquiryNumber());
            oWorkflowDto.setLoginUserId(oUserDto.getUserId());
            oWorkflowDto.setLoginUserName(oUserDto.getFullName());
            oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
            oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTOSM);
            oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGRSM);
            
            LoggerUtility.log("INFO", sClassName, sMethodName, "2 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
            iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
            
            if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE ) {
                LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFS);
            } else  if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ZERO){ 
                LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFSEMAILF);
            } else {
                LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFF);
            }
            
            if(!isInsertUpdateComplete) {
        		request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE,QuotationConstants.QUOTATION_ER_INSERT);
        		
        		// Forwarding request to Error page 
        		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        	} else {
        		sMsg = QuotationConstants.QUOTATION_APPROVEBYRSM_TRACKNO + oNewEnquiryDto.getEnquiryNumber();
        		
        		sLinkMsg1 = QuotationConstants.QUOTATION_RETURNHOME_URL + QuotationConstants.QUOTATION_GRSYMBOL + QuotationConstants.QUOTATION_VIEWLINK_RETURNHOME; 
                sLinkMsg = sLinkMsg1; 
                
                request.setAttribute(QuotationConstants.QUOTATION_HEADINGMESSAGE,QuotationConstants.QUOTATION_SUB_SUCES);
                request.setAttribute(QuotationConstants.QUOTATION_SUCCESSMESSAGE,sMsg);
                request.setAttribute(QuotationConstants.QUOTATION_LINKSMESSAGE,sLinkMsg);
        	}
		} catch(Exception e) {
            /* Logging any exception that raised during this activity in log files using Log4j */
            LoggerUtility.log("DEBUG", sClassName, sMethodName, ExceptionUtility.getStackTraceAsString(e));
            //e.printStackTrace();
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        } finally {
            /* Releasing/closing the connection object */
            if ( conn!=null )
                oDBUtility.releaseResources(conn);
        }
		
		LoggerUtility.log("INFO", sClassName, sMethodName, "END");
    	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);
	}
	
	/**
     * This method is used to Submit RSM "Approval Discount" to NSM
     * @param actionMapping ActionMapping object to redirect the user to next page depending on the activity
     * @param actionForm ActionForm object to handle the form elements
     * @param request HttpServletRequest object to handle request operations
     * @param response HttpServletResponse object to handle response operations
     * @return returns ActionForward depending on which user is redirected to next page
     * @throws Exception if any error occurs while performing database operations, etc
     */
	public ActionForward submitToNSM(ActionMapping actionMapping,
            ActionForm actionForm, 
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
		
		String sMethodName = QuotationConstants.QUOTATION_MTHD_SUBMITTONSM; 
		String sClassName = this.getClass().getName();
		LoggerUtility.log("INFO", sClassName, sMethodName, "START");
		
		Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        UserDto oUserDto = null;
        UserAction oUserAction=null; //Instantiating the UserAction object
        DaoFactory oDaoFactory=null; //Creating an instance of of DaoFactory & retrieving UserDao object
        QuotationDao oQuotationDao=null;// Creating an Instance for QuotationDao
        String sMsg=null,sLinkMsg1=null,sLinkMsg=null,sRatingResult=null;
        int iWorkflowExecuteStatus=0;
        //boolean isUpdatedRating=QuotationConstants.QUOTATION_FALSE;
		NewEnquiryForm oNewEnquiryForm = null;
		NewEnquiryDto oNewEnquiryDto = null;
		WorkflowUtility oWorkflowUtility = null;
		WorkflowDto oWorkflowDto = null;
		boolean isInsertUpdateComplete = false;
		
		try {
			setRolesToRequest(request);
            LoggerUtility.log("INFO", sClassName, sMethodName, "Roles set to request.");
            
    		oNewEnquiryForm = (NewEnquiryForm) actionForm;
    		if (oNewEnquiryForm == null)
    			oNewEnquiryForm = new NewEnquiryForm();
    		
            /* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Instantiating the UserAction object */
            oUserAction = new UserAction();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
            /* Create QuotationDao */
            oQuotationDao = oDaoFactory.getQuotationDao();
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            
            /* Retrieving the logged-in user information */
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            if (oUserDto == null){
            	oUserDto = new UserDto();
            }
            
            oNewEnquiryDto = new NewEnquiryDto();
            oNewEnquiryForm.setCreatedBy(oUserDto.getFullName());           
            oNewEnquiryForm.setCreatedBySSO(oUserDto.getUserId());
            PropertyUtils.copyProperties(oNewEnquiryDto, oNewEnquiryForm);  
            // Fetch the Notes
            oNewEnquiryDto.setSmNote(CommonUtility.getStringParameter(request, "smNote"));
            oNewEnquiryDto.setRsmNote(CommonUtility.getStringParameter(request, "rsmNote"));
            oNewEnquiryDto.setNsmNote(CommonUtility.getStringParameter(request, "nsmNote"));
            oNewEnquiryDto.setMhNote(CommonUtility.getStringParameter(request, "mhNote"));
            
            LoggerUtility.log("INFO", sClassName, sMethodName, "iEnquiryId -  " + oNewEnquiryDto.getEnquiryId());
            
            oDBUtility.setAutoCommitFalse(conn);
            
            // Save the Notes
            oQuotationDao.saveNewEnquiryNotes(conn, oNewEnquiryDto);
            
            oNewEnquiryDto.setApprovalUserType("SUBMIT_NSM");
            ArrayList<NewRatingDto> alRatingsList = getRatingsCommercialDetails_ForApproval(conn, oQuotationDao, oNewEnquiryDto, request);
            oNewEnquiryDto.setNewRatingsList(alRatingsList);
            
            sRatingResult = oQuotationDao.savePrices_SubmitNSM(conn, oNewEnquiryDto);
            
            if (QuotationConstants.QUOTATION_STRING_Y.equals(sRatingResult)) {
				oDBUtility.commit(conn);
				isInsertUpdateComplete = true;
			} else {
				isInsertUpdateComplete = false;
				oDBUtility.rollback(conn);
			}
            oDBUtility.setAutoCommitTrue(conn);
            
            // Workflow :  SUBMIT TO NSM
            resetToken(request);
		    LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Initiating Workflow process - - -");
		    oWorkflowUtility = new WorkflowUtility();
            oWorkflowDto = new WorkflowDto();
            oWorkflowDto.setEnquiryId(oNewEnquiryDto.getEnquiryId());
            oWorkflowDto.setEnquiryNo(oNewEnquiryDto.getEnquiryNumber());
            oWorkflowDto.setLoginUserId(oUserDto.getUserId());
            oWorkflowDto.setLoginUserName(oUserDto.getFullName());
            oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
            oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTONSM);
            oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGRSM);
            
            LoggerUtility.log("INFO", sClassName, sMethodName, "2 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
            iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
            
            if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE ) {
                LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFS);
            } else  if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ZERO){ 
                LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFSEMAILF);
            } else {
                LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFF);
            }
            
            if(!isInsertUpdateComplete) {
        		request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE,QuotationConstants.QUOTATION_ER_INSERT);
        		
        		// Forwarding request to Error page 
        		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        	} else {
        		sMsg = QuotationConstants.QUOTATION_SUBMITTONSM_TRACKNO + oNewEnquiryDto.getEnquiryNumber();
        		
        		sLinkMsg1 = QuotationConstants.QUOTATION_RETURNHOME_URL + QuotationConstants.QUOTATION_GRSYMBOL + QuotationConstants.QUOTATION_VIEWLINK_RETURNHOME; 
                sLinkMsg = sLinkMsg1; 
                
                request.setAttribute(QuotationConstants.QUOTATION_HEADINGMESSAGE,QuotationConstants.QUOTATION_SUB_SUCES);
                request.setAttribute(QuotationConstants.QUOTATION_SUCCESSMESSAGE,sMsg);
                request.setAttribute(QuotationConstants.QUOTATION_LINKSMESSAGE,sLinkMsg);
        	}
		} catch(Exception e) {
            /* Logging any exception that raised during this activity in log files using Log4j */
            LoggerUtility.log("DEBUG", sClassName, sMethodName, ExceptionUtility.getStackTraceAsString(e));
            //e.printStackTrace();
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        } finally {
            /* Releasing/closing the connection object */
            if ( conn!=null )
                oDBUtility.releaseResources(conn);
        }
		
		LoggerUtility.log("INFO", sClassName, sMethodName, "END");
    	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);
	}
	
	/**
     * This method is used for NSM Approval - Return "Approved Discount" to RSM
     * @param actionMapping ActionMapping object to redirect the user to next page depending on the activity
     * @param actionForm ActionForm object to handle the form elements
     * @param request HttpServletRequest object to handle request operations
     * @param response HttpServletResponse object to handle response operations
     * @return returns ActionForward depending on which user is redirected to next page
     * @throws Exception if any error occurs while performing database operations, etc
     */
	public ActionForward approveByNSM(ActionMapping actionMapping,
            ActionForm actionForm, 
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
		
		String sMethodName = QuotationConstants.QUOTATION_MTHD_APPROVEBYNSM; 
		String sClassName = this.getClass().getName();
		LoggerUtility.log("INFO", sClassName, sMethodName, "START");
		
		Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        UserDto oUserDto = null;
        UserAction oUserAction=null; //Instantiating the UserAction object
        DaoFactory oDaoFactory=null; //Creating an instance of of DaoFactory & retrieving UserDao object
        QuotationDao oQuotationDao=null;// Creating an Instance for QuotationDao
        String sMsg=null,sLinkMsg1=null,sLinkMsg=null,sRatingResult=null;
        int iWorkflowExecuteStatus=0;
        //boolean isUpdatedRating=QuotationConstants.QUOTATION_FALSE;
		NewEnquiryForm oNewEnquiryForm = null;
		NewEnquiryDto oNewEnquiryDto = null;
		WorkflowUtility oWorkflowUtility = null;
		WorkflowDto oWorkflowDto = null;
		boolean isInsertUpdateComplete = false;
		
		try {
			setRolesToRequest(request);
            LoggerUtility.log("INFO", sClassName, sMethodName, "Roles set to request.");
            
    		oNewEnquiryForm = (NewEnquiryForm) actionForm;
    		if (oNewEnquiryForm == null)
    			oNewEnquiryForm = new NewEnquiryForm();
    		
            /* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Instantiating the UserAction object */
            oUserAction = new UserAction();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
            /* Create QuotationDao */
            oQuotationDao = oDaoFactory.getQuotationDao();
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            
            /* Retrieving the logged-in user information */
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            if (oUserDto == null){
            	oUserDto = new UserDto();
            }
            
            oNewEnquiryDto = new NewEnquiryDto();
            oNewEnquiryForm.setCreatedBy(oUserDto.getFullName());           
            oNewEnquiryForm.setCreatedBySSO(oUserDto.getUserId());
            PropertyUtils.copyProperties(oNewEnquiryDto, oNewEnquiryForm);  
            // Fetch the Notes
            oNewEnquiryDto.setSmNote(CommonUtility.getStringParameter(request, "smNote"));
            oNewEnquiryDto.setRsmNote(CommonUtility.getStringParameter(request, "rsmNote"));
            oNewEnquiryDto.setNsmNote(CommonUtility.getStringParameter(request, "nsmNote"));
            oNewEnquiryDto.setMhNote(CommonUtility.getStringParameter(request, "mhNote"));
            
            LoggerUtility.log("INFO", sClassName, sMethodName, "iEnquiryId -  " + oNewEnquiryDto.getEnquiryId());
            
            oDBUtility.setAutoCommitFalse(conn);
            
            // Save the Notes
            oQuotationDao.saveNewEnquiryNotes(conn, oNewEnquiryDto);
            
            oNewEnquiryDto.setApprovalUserType("APPRV_NSM");
            ArrayList<NewRatingDto> alRatingsList = getRatingsCommercialDetails_ForApproval(conn, oQuotationDao, oNewEnquiryDto, request);
            oNewEnquiryDto.setNewRatingsList(alRatingsList);
            
            sRatingResult = oQuotationDao.savePrices_ApproveNSM(conn, oNewEnquiryDto);
            
            if (QuotationConstants.QUOTATION_STRING_Y.equals(sRatingResult)) {
				oDBUtility.commit(conn);
				isInsertUpdateComplete = true;
			} else {
				isInsertUpdateComplete = false;
				oDBUtility.rollback(conn);
			}
            oDBUtility.setAutoCommitTrue(conn);
            
            // Workflow :  APPROVE BY NSM
            resetToken(request);
		    LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Initiating Workflow process - - -");
		    oWorkflowUtility = new WorkflowUtility();
            oWorkflowDto = new WorkflowDto();
            oWorkflowDto.setEnquiryId(oNewEnquiryDto.getEnquiryId());
            oWorkflowDto.setEnquiryNo(oNewEnquiryDto.getEnquiryNumber());
            oWorkflowDto.setLoginUserId(oUserDto.getUserId());
            oWorkflowDto.setLoginUserName(oUserDto.getFullName());
            oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
            oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTORSM);
            oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGNSM);
            
            LoggerUtility.log("INFO", sClassName, sMethodName, "2 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
            iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
            
            if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE ) {
                LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFS);
            } else  if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ZERO){ 
                LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFSEMAILF);
            } else {
                LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFF);
            }
            
            if(!isInsertUpdateComplete) {
        		request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE,QuotationConstants.QUOTATION_ER_INSERT);
        		
        		// Forwarding request to Error page 
        		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        	} else {
        		sMsg = QuotationConstants.QUOTATION_APPROVEBYNSM_TRACKNO + oNewEnquiryDto.getEnquiryNumber();
        		
        		sLinkMsg1 = QuotationConstants.QUOTATION_RETURNHOME_URL + QuotationConstants.QUOTATION_GRSYMBOL + QuotationConstants.QUOTATION_VIEWLINK_RETURNHOME; 
                sLinkMsg = sLinkMsg1; 
                
                request.setAttribute(QuotationConstants.QUOTATION_HEADINGMESSAGE,QuotationConstants.QUOTATION_SUB_SUCES);
                request.setAttribute(QuotationConstants.QUOTATION_SUCCESSMESSAGE,sMsg);
                request.setAttribute(QuotationConstants.QUOTATION_LINKSMESSAGE,sLinkMsg);
        	}
		} catch(Exception e) {
            /* Logging any exception that raised during this activity in log files using Log4j */
            LoggerUtility.log("DEBUG", sClassName, sMethodName, ExceptionUtility.getStackTraceAsString(e));
            //e.printStackTrace();
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        } finally {
            /* Releasing/closing the connection object */
            if ( conn!=null )
                oDBUtility.releaseResources(conn);
        }
		
		LoggerUtility.log("INFO", sClassName, sMethodName, "END");
    	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);
	}
	
	/**
     * This method is used to Submit NSM "Approval Discount" to MH
     * @param actionMapping ActionMapping object to redirect the user to next page depending on the activity
     * @param actionForm ActionForm object to handle the form elements
     * @param request HttpServletRequest object to handle request operations
     * @param response HttpServletResponse object to handle response operations
     * @return returns ActionForward depending on which user is redirected to next page
     * @throws Exception if any error occurs while performing database operations, etc
     */
	public ActionForward submitToMH(ActionMapping actionMapping,
            ActionForm actionForm, 
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
		
		String sMethodName = QuotationConstants.QUOTATION_MTHD_SUBMITTOMH; 
		String sClassName = this.getClass().getName();
		LoggerUtility.log("INFO", sClassName, sMethodName, "START");
		
		Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        UserDto oUserDto = null;
        UserAction oUserAction=null; //Instantiating the UserAction object
        DaoFactory oDaoFactory=null; //Creating an instance of of DaoFactory & retrieving UserDao object
        QuotationDao oQuotationDao=null;// Creating an Instance for QuotationDao
        String sMsg=null,sLinkMsg1=null,sLinkMsg=null,sRatingResult=null;
        int iWorkflowExecuteStatus=0;
        //boolean isUpdatedRating=QuotationConstants.QUOTATION_FALSE;
		NewEnquiryForm oNewEnquiryForm = null;
		NewEnquiryDto oNewEnquiryDto = null;
		WorkflowUtility oWorkflowUtility = null;
		WorkflowDto oWorkflowDto = null;
		boolean isInsertUpdateComplete = false;
		
		try {
			setRolesToRequest(request);
            LoggerUtility.log("INFO", sClassName, sMethodName, "Roles set to request.");
            
    		oNewEnquiryForm = (NewEnquiryForm) actionForm;
    		if (oNewEnquiryForm == null)
    			oNewEnquiryForm = new NewEnquiryForm();
    		
            /* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Instantiating the UserAction object */
            oUserAction = new UserAction();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
            /* Create QuotationDao */
            oQuotationDao = oDaoFactory.getQuotationDao();
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            
            /* Retrieving the logged-in user information */
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            if (oUserDto == null){
            	oUserDto = new UserDto();
            }
            
            oNewEnquiryDto = new NewEnquiryDto();
            oNewEnquiryForm.setCreatedBy(oUserDto.getFullName());           
            oNewEnquiryForm.setCreatedBySSO(oUserDto.getUserId());
            PropertyUtils.copyProperties(oNewEnquiryDto, oNewEnquiryForm);  
            // Fetch the Notes
            oNewEnquiryDto.setSmNote(CommonUtility.getStringParameter(request, "smNote"));
            oNewEnquiryDto.setRsmNote(CommonUtility.getStringParameter(request, "rsmNote"));
            oNewEnquiryDto.setNsmNote(CommonUtility.getStringParameter(request, "nsmNote"));
            oNewEnquiryDto.setMhNote(CommonUtility.getStringParameter(request, "mhNote"));
            
            LoggerUtility.log("INFO", sClassName, sMethodName, "iEnquiryId -  " + oNewEnquiryDto.getEnquiryId());
            
            oDBUtility.setAutoCommitFalse(conn);
            
            // Save the Notes
            oQuotationDao.saveNewEnquiryNotes(conn, oNewEnquiryDto);
            
            oNewEnquiryDto.setApprovalUserType("SUBMIT_MH");
            ArrayList<NewRatingDto> alRatingsList = getRatingsCommercialDetails_ForApproval(conn, oQuotationDao, oNewEnquiryDto, request);
            oNewEnquiryDto.setNewRatingsList(alRatingsList);
            
            sRatingResult = oQuotationDao.savePrices_SubmitMH(conn, oNewEnquiryDto);
            
            if (QuotationConstants.QUOTATION_STRING_Y.equals(sRatingResult)) {
				oDBUtility.commit(conn);
				isInsertUpdateComplete = true;
			} else {
				isInsertUpdateComplete = false;
				oDBUtility.rollback(conn);
			}
            oDBUtility.setAutoCommitTrue(conn);
            
            // Workflow :  SUBMIT TO MH
            resetToken(request);
		    LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Initiating Workflow process - - -");
		    oWorkflowUtility = new WorkflowUtility();
            oWorkflowDto = new WorkflowDto();
            oWorkflowDto.setEnquiryId(oNewEnquiryDto.getEnquiryId());
            oWorkflowDto.setEnquiryNo(oNewEnquiryDto.getEnquiryNumber());
            oWorkflowDto.setLoginUserId(oUserDto.getUserId());
            oWorkflowDto.setLoginUserName(oUserDto.getFullName());
            oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
            oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTOMH);
            oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGNSM);
            
            LoggerUtility.log("INFO", sClassName, sMethodName, "2 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
            iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
            
            if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE ) {
                LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFS);
            } else  if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ZERO){ 
                LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFSEMAILF);
            } else {
                LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFF);
            }
            
            if(!isInsertUpdateComplete) {
        		request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE,QuotationConstants.QUOTATION_ER_INSERT);
        		
        		// Forwarding request to Error page 
        		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        	} else {
        		sMsg = QuotationConstants.QUOTATION_SUBMITTOMH_TRACKNO + oNewEnquiryDto.getEnquiryNumber();
        		
        		sLinkMsg1 = QuotationConstants.QUOTATION_RETURNHOME_URL + QuotationConstants.QUOTATION_GRSYMBOL + QuotationConstants.QUOTATION_VIEWLINK_RETURNHOME; 
                sLinkMsg = sLinkMsg1; 
                
                request.setAttribute(QuotationConstants.QUOTATION_HEADINGMESSAGE,QuotationConstants.QUOTATION_SUB_SUCES);
                request.setAttribute(QuotationConstants.QUOTATION_SUCCESSMESSAGE,sMsg);
                request.setAttribute(QuotationConstants.QUOTATION_LINKSMESSAGE,sLinkMsg);
        	}
		} catch(Exception e) {
            /* Logging any exception that raised during this activity in log files using Log4j */
            LoggerUtility.log("DEBUG", sClassName, sMethodName, ExceptionUtility.getStackTraceAsString(e));
            //e.printStackTrace();
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        } finally {
            /* Releasing/closing the connection object */
            if ( conn!=null )
                oDBUtility.releaseResources(conn);
        }
		
		LoggerUtility.log("INFO", sClassName, sMethodName, "END");
    	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);
	}
	
	/**
     * This method is used for MH Approval - Return "Approved Discount" to NSM
     * @param actionMapping ActionMapping object to redirect the user to next page depending on the activity
     * @param actionForm ActionForm object to handle the form elements
     * @param request HttpServletRequest object to handle request operations
     * @param response HttpServletResponse object to handle response operations
     * @return returns ActionForward depending on which user is redirected to next page
     * @throws Exception if any error occurs while performing database operations, etc
     */
	public ActionForward approveByMH(ActionMapping actionMapping,
            ActionForm actionForm, 
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
		
		String sMethodName = QuotationConstants.QUOTATION_MTHD_APPROVEBYNSM; 
		String sClassName = this.getClass().getName();
		LoggerUtility.log("INFO", sClassName, sMethodName, "START");
		
		Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        UserDto oUserDto = null;
        UserAction oUserAction=null; //Instantiating the UserAction object
        DaoFactory oDaoFactory=null; //Creating an instance of of DaoFactory & retrieving UserDao object
        QuotationDao oQuotationDao=null;// Creating an Instance for QuotationDao
        String sMsg=null,sLinkMsg1=null,sLinkMsg=null,sRatingResult=null;
        int iWorkflowExecuteStatus=0;
        //boolean isUpdatedRating=QuotationConstants.QUOTATION_FALSE;
		NewEnquiryForm oNewEnquiryForm = null;
		NewEnquiryDto oNewEnquiryDto = null;
		WorkflowUtility oWorkflowUtility = null;
		WorkflowDto oWorkflowDto = null;
		boolean isInsertUpdateComplete = false;
		
		try {
			setRolesToRequest(request);
            LoggerUtility.log("INFO", sClassName, sMethodName, "Roles set to request.");
            
    		oNewEnquiryForm = (NewEnquiryForm) actionForm;
    		if (oNewEnquiryForm == null)
    			oNewEnquiryForm = new NewEnquiryForm();
    		
            /* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Instantiating the UserAction object */
            oUserAction = new UserAction();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
            /* Create QuotationDao */
            oQuotationDao = oDaoFactory.getQuotationDao();
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            
            /* Retrieving the logged-in user information */
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            if (oUserDto == null){
            	oUserDto = new UserDto();
            }
            
            oNewEnquiryDto = new NewEnquiryDto();
            oNewEnquiryForm.setCreatedBy(oUserDto.getFullName());           
            oNewEnquiryForm.setCreatedBySSO(oUserDto.getUserId());
            PropertyUtils.copyProperties(oNewEnquiryDto, oNewEnquiryForm);  
            // Fetch the Notes
            oNewEnquiryDto.setSmNote(CommonUtility.getStringParameter(request, "smNote"));
            oNewEnquiryDto.setRsmNote(CommonUtility.getStringParameter(request, "rsmNote"));
            oNewEnquiryDto.setNsmNote(CommonUtility.getStringParameter(request, "nsmNote"));
            oNewEnquiryDto.setMhNote(CommonUtility.getStringParameter(request, "mhNote"));
            
            LoggerUtility.log("INFO", sClassName, sMethodName, "iEnquiryId -  " + oNewEnquiryDto.getEnquiryId());
            
            oDBUtility.setAutoCommitFalse(conn);
            
            // Save the Notes
            oQuotationDao.saveNewEnquiryNotes(conn, oNewEnquiryDto);
            
            oNewEnquiryDto.setApprovalUserType("APPRV_MH");
            ArrayList<NewRatingDto> alRatingsList = getRatingsCommercialDetails_ForApproval(conn, oQuotationDao, oNewEnquiryDto, request);
            oNewEnquiryDto.setNewRatingsList(alRatingsList);
            
            sRatingResult = oQuotationDao.savePrices_ApproveMH(conn, oNewEnquiryDto);
            
            if (QuotationConstants.QUOTATION_STRING_Y.equals(sRatingResult)) {
				oDBUtility.commit(conn);
				isInsertUpdateComplete = true;
			} else {
				isInsertUpdateComplete = false;
				oDBUtility.rollback(conn);
			}
            oDBUtility.setAutoCommitTrue(conn);
            
            // Workflow :  APPROVE BY MH
            resetToken(request);
		    LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Initiating Workflow process - - -");
		    oWorkflowUtility = new WorkflowUtility();
            oWorkflowDto = new WorkflowDto();
            oWorkflowDto.setEnquiryId(oNewEnquiryDto.getEnquiryId());
            oWorkflowDto.setEnquiryNo(oNewEnquiryDto.getEnquiryNumber());
            oWorkflowDto.setLoginUserId(oUserDto.getUserId());
            oWorkflowDto.setLoginUserName(oUserDto.getFullName());
            oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
            oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTONSM);
            oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGMH);
            
            LoggerUtility.log("INFO", sClassName, sMethodName, "2 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
            iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
            
            if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE ) {
                LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFS);
            } else  if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ZERO){ 
                LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFSEMAILF);
            } else {
                LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFF);
            }
            
            if(!isInsertUpdateComplete) {
        		request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE,QuotationConstants.QUOTATION_ER_INSERT);
        		
        		// Forwarding request to Error page 
        		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        	} else {
        		sMsg = QuotationConstants.QUOTATION_APPROVEBYMH_TRACKNO + oNewEnquiryDto.getEnquiryNumber();
        		
        		sLinkMsg1 = QuotationConstants.QUOTATION_RETURNHOME_URL + QuotationConstants.QUOTATION_GRSYMBOL + QuotationConstants.QUOTATION_VIEWLINK_RETURNHOME; 
                sLinkMsg = sLinkMsg1; 
                
                request.setAttribute(QuotationConstants.QUOTATION_HEADINGMESSAGE,QuotationConstants.QUOTATION_SUB_SUCES);
                request.setAttribute(QuotationConstants.QUOTATION_SUCCESSMESSAGE,sMsg);
                request.setAttribute(QuotationConstants.QUOTATION_LINKSMESSAGE,sLinkMsg);
        	}
		} catch(Exception e) {
            /* Logging any exception that raised during this activity in log files using Log4j */
            LoggerUtility.log("DEBUG", sClassName, sMethodName, ExceptionUtility.getStackTraceAsString(e));
            //e.printStackTrace();
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        } finally {
            /* Releasing/closing the connection object */
            if ( conn!=null )
                oDBUtility.releaseResources(conn);
        }
		
		LoggerUtility.log("INFO", sClassName, sMethodName, "END");
    	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);
	}
	
	/**
	 * Method to handle Review Enquiry action from Pending SM page.
	 * @param actionMapping		ActionMapping object to redirect the user to next page depending on the activity
	 * @param actionForm		ActionForm object to handle the form elements
	 * @param request			HttpServletRequest object to handle request operations
	 * @param response			HttpServletResponse object to handle response operations
	 * @return ActionForward 	depending on which user is redirected to next page
	 * @throws Exception		if any error occurs while performing database operations, etc
	 */
	public ActionForward processReviseEnquiry(ActionMapping actionMapping,
            ActionForm actionForm, 
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
		
		String sMethodName = "processReviseEnquiry";
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        UserDto oUserDto = null;
        UserAction oUserAction=null; //Instantiating the UserAction object
        DaoFactory oDaoFactory=null; //Creating an instance of of DaoFactory & retrieving UserDao object
        QuotationDao oQuotationDao=null;// Creating an Instance for QuotationDao
        String sMsg=null,sLinkMsg1=null,sLinkMsg=null;
        int iWorkflowExecuteStatus=0;
        NewEnquiryForm oNewEnquiryForm = null;
		NewEnquiryDto oNewEnquiryDto = null;
		WorkflowUtility oWorkflowUtility = null;
		WorkflowDto oWorkflowDto = null;
		
		try {
			setRolesToRequest(request);
            LoggerUtility.log("INFO", sClassName, sMethodName, "Roles set to request.");
            
    		oNewEnquiryForm = (NewEnquiryForm) actionForm;
    		if (oNewEnquiryForm == null)
    			oNewEnquiryForm = new NewEnquiryForm();
    		
            /* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Instantiating the UserAction object */
            oUserAction = new UserAction();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
            /* Create QuotationDao */
            oQuotationDao = oDaoFactory.getQuotationDao();
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            
            /* Retrieving the logged-in user information */
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            if (oUserDto == null){
            	oUserDto = new UserDto();
            }
            
            oNewEnquiryDto = new NewEnquiryDto();
            oNewEnquiryForm.setCreatedBy(oUserDto.getFullName());           
            oNewEnquiryForm.setCreatedBySSO(oUserDto.getUserId());
            PropertyUtils.copyProperties(oNewEnquiryDto, oNewEnquiryForm);
            
            oNewEnquiryDto = oQuotationDao.getNewEnquiryDetailsForView(conn, oNewEnquiryDto, QuotationConstants.QUOTATION_FORWARD_VIEW.toUpperCase());
            
            // Fetch the Notes
            oNewEnquiryDto.setSmNote(CommonUtility.getStringParameter(request, "smNote"));
            oNewEnquiryDto.setRsmNote(CommonUtility.getStringParameter(request, "rsmNote"));
            oNewEnquiryDto.setNsmNote(CommonUtility.getStringParameter(request, "nsmNote"));
            oNewEnquiryDto.setMhNote(CommonUtility.getStringParameter(request, "mhNote"));
            
            // Save the Notes
            oQuotationDao.saveNewEnquiryNotes(conn, oNewEnquiryDto);
            
            String sReviseUserType = CommonUtility.getStringParameter(request, "reviseUserType");
            
            if(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_SM.equalsIgnoreCase(sReviseUserType)) {
            	oNewEnquiryDto.setDmUpdateStatus(QuotationConstants.QUOTATION_EMPTY);
            	oQuotationDao.updateDmStatus(conn, oNewEnquiryDto);
            }
            
            // Workflow :  REVISE ENQUIRY 		- RESET TO DRAFT STATUS
            resetToken(request);
		    LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Initiating Workflow process - - -");
		    oWorkflowUtility = new WorkflowUtility();
            oWorkflowDto = new WorkflowDto();
            oWorkflowDto.setEnquiryId(oNewEnquiryDto.getEnquiryId());
            oWorkflowDto.setEnquiryNo(oNewEnquiryDto.getEnquiryNumber());
            oWorkflowDto.setLoginUserId(oUserDto.getUserId());
            oWorkflowDto.setLoginUserName(oUserDto.getFullName());
            oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
            
            if(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_SM.equalsIgnoreCase(sReviseUserType)) {
            	oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SM_REVISE_ENQ);
                oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGSM);
            } else if(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_RSM.equalsIgnoreCase(sReviseUserType)) {
            	oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_RSM_REVISE_ENQ);
                oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGRSM);
            } else if(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_NSM.equalsIgnoreCase(sReviseUserType)) {
            	oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_NSM_REVISE_ENQ);
                oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGNSM);
            } else if(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_MH.equalsIgnoreCase(sReviseUserType)) {
            	oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_MH_REVISE_ENQ);
                oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGMH);
            }
            
            LoggerUtility.log("INFO", sClassName, sMethodName, "2 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
            iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
            
            if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE ) {
                LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFS);
            } else  if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ZERO){ 
                LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFSEMAILF);
            } else {
                LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFF);
            }
            
            if(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_SM.equalsIgnoreCase(sReviseUserType)) {
            	sMsg = QuotationConstants.QUOTATION_REVISEBYSM_TRACKNO + oNewEnquiryDto.getEnquiryNumber();
            } else if(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_RSM.equalsIgnoreCase(sReviseUserType)) {
            	sMsg = QuotationConstants.QUOTATION_REVISEBYRSM_TRACKNO + oNewEnquiryDto.getEnquiryNumber();
            } else if(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_NSM.equalsIgnoreCase(sReviseUserType)) {
            	sMsg = QuotationConstants.QUOTATION_REVISEBYNSM_TRACKNO + oNewEnquiryDto.getEnquiryNumber();
            } else if(QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_MH.equalsIgnoreCase(sReviseUserType)) {
            	sMsg = QuotationConstants.QUOTATION_REVISEBYMH_TRACKNO + oNewEnquiryDto.getEnquiryNumber();
            }
            
    		sLinkMsg1 = QuotationConstants.QUOTATION_RETURNHOME_URL + QuotationConstants.QUOTATION_GRSYMBOL + QuotationConstants.QUOTATION_VIEWLINK_RETURNHOME; 
            sLinkMsg = sLinkMsg1; 
            
            request.setAttribute(QuotationConstants.QUOTATION_HEADINGMESSAGE,QuotationConstants.QUOTATION_SUB_SUCES);
            request.setAttribute(QuotationConstants.QUOTATION_SUCCESSMESSAGE,sMsg);
            request.setAttribute(QuotationConstants.QUOTATION_LINKSMESSAGE,sLinkMsg);
            
		} catch (Exception e) {
			/* Logging any exception that raised during this activity in log files using Log4j */
			LoggerUtility.log("DEBUG", this.getClass().getName(), sMethodName, ExceptionUtility.getStackTraceAsString(e));

			/* Setting error details to request to display the same in Exception page */
			request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));

			/* Forwarding request to Error page */
			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
		} finally {
			/* Releasing/closing the connection object */
			if (conn != null)
				oDBUtility.releaseResources(conn);
		}

		LoggerUtility.log("INFO", sClassName, sMethodName, "END");
		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);
	}
	
	/**
	 * This method is used to copy the Existing Enquiry - Create New Enquiry - Assign to Design.
	 * @param actionMapping		ActionMapping object to redirect the user to next page depending on the activity
	 * @param actionForm		ActionForm object to handle the form elements
	 * @param request			HttpServletRequest object to handle request operations
	 * @param response			HttpServletResponse object to handle response operations
	 * @return ActionForward 	depending on which user is redirected to next page
	 * @throws Exception		if any error occurs while performing database operations, etc
	 */
	public ActionForward createNewEnqTechRevision(ActionMapping actionMapping,
            ActionForm actionForm, 
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
		
		String sMethodName = "createNewEnqTechRevision";
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        NewEnquiryDto oNewEnquiryDto = null;
   	    Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        
        UserDto oUserDto = null;
        UserAction oUserAction =null;
        DaoFactory oDaoFactory =null;
        QuotationDao oQuotationDao =null;
        WorkflowDto oWorkflowDto = null;
        WorkflowUtility oWorkflowUtility =null;
        int sOldEnquiryId = QuotationConstants.QUOTATION_LITERAL_ZERO;
        int iWorkflowExecuteStatus = QuotationConstants.QUOTATION_LITERAL_ZERO;
        String sMsg=null,sLinkMsg1=null,sLinkMsg2=null,sLinkMsg=null;
        
        NewEnquiryForm oNewEnquiryForm = (NewEnquiryForm)actionForm;
		if (oNewEnquiryForm == null){
			oNewEnquiryForm = new NewEnquiryForm();
		}
		
		 try {
			 if(isTokenValid(request)) {
				resetToken(request);

				/* Setting user's information in request, using the header information received through request */
				setRolesToRequest(request);
				LoggerUtility.log("INFO", sClassName, sMethodName, "Roles set to request.");
	             
				/* Instantiating the DBUtility object */
				oDBUtility = new DBUtility();
				/* Instantiating the UserAction object */
				oUserAction = new UserAction();
				/* Creating an instance of of DaoFactory & retrieving UserDao object */
				oDaoFactory = new DaoFactory();
				/* Retrieving the database connection using DBUtility.getDBConnection method */
				conn = oDBUtility.getDBConnection();
				oQuotationDao = oDaoFactory.getQuotationDao();
				/* Retrieving the logged-in user information */
				oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
				
				oNewEnquiryDto = new NewEnquiryDto();
				PropertyUtils.copyProperties(oNewEnquiryDto, oNewEnquiryForm);
				sOldEnquiryId = oNewEnquiryDto.getEnquiryId(); 	// Old Enquiry Id
				
				oNewEnquiryDto = oQuotationDao.createNewEnquiryRevision(conn, oNewEnquiryDto, oUserDto);
	               
	            request.setAttribute(QuotationConstants.QUOTATION_ENQUIRYDTO, oNewEnquiryDto); 
	            request.setAttribute(QuotationConstants.QUOTATION_USERDTO, oUserDto); 
	            
				if (oNewEnquiryDto == null) {
					/* Setting error details to request to display the same in Exception page */
					request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_PROB_COPYREC);
					/* Forwarding request to Error page */
					return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
				} else {
					// Set DmUpdateStatus to "". - Since Enquiry is submitted for Tech Review - Remove "DMSUBMIT" from DmUpdateStatus.
					oNewEnquiryDto.setDmUpdateStatus(QuotationConstants.QUOTATION_EMPTY);
					oQuotationDao.updateDmStatus(conn, oNewEnquiryDto);
					
					oNewEnquiryDto = oQuotationDao.getNewEnquiryStatus(conn, oNewEnquiryDto);
					
					/*
					 * ############################################################# 
					 *  Technical Revision - Workflow process initiated
					 * #############################################################
					 */
					LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Initiating Workflow process - - -");
					oWorkflowUtility = new WorkflowUtility();
					oWorkflowDto = new WorkflowDto();

					oWorkflowDto.setEnquiryId(oNewEnquiryDto.getEnquiryId());
					oWorkflowDto.setEnquiryNo(oNewEnquiryDto.getEnquiryNumber());
					oWorkflowDto.setBaseEnquiryId(sOldEnquiryId);
					oWorkflowDto.setLoginUserId(oUserDto.getUserId());
					oWorkflowDto.setLoginUserName(oUserDto.getFullName());
					oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
					oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_CREATEREVISION);
					oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_RELEASED);
					oWorkflowDto.setRevisionRemarks(QuotationConstants.QUOTATION_WORKFLOW_REVISION_TECHNICAL);
					// Fetch DM Details from Manager Table - Set to WorkflowDto.
					oWorkflowDto.setDmSsoid(oQuotationDao.getDesignManagerId(conn));
					
					LoggerUtility.log("INFO", sClassName, sMethodName, "12 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
					iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);

					if (iWorkflowExecuteStatus == QuotationConstants.QUOTATION_LITERAL_ONE) {
						LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Successfully");
					} else if (iWorkflowExecuteStatus == QuotationConstants.QUOTATION_LITERAL_ZERO) {
						LoggerUtility.log("INFO", sClassName, sMethodName,
								"Workflow Executed - But email could not be sent. Please send email manually.");
					} else {
						LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Failed");
					}

					LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Workflow process completed - - -");
					/*
					 * ############################################################# 
					 * Technical Revision - Workflow process completed
					 * #############################################################
					 */
					
					// Build the Success Message
		            sMsg = QuotationConstants.QUOTATION_NEWREVISION_TRACKNO + oNewEnquiryDto.getEnquiryNumber();
		            
		            sLinkMsg1 = QuotationConstants.QUOTATION_RETURNHOME_URL + QuotationConstants.QUOTATION_GRSYMBOL + QuotationConstants.QUOTATION_VIEWLINK_RETURNHOME; 
		            sLinkMsg2 = QuotationConstants.QUOTATION_LT_VIEWCR_ENQ_URL + QuotationConstants.QUOTATION_PLACELINKS; 
		            sLinkMsg = sLinkMsg1 + QuotationConstants.QUOTATION_BREAK + sLinkMsg2;
		            
		            request.setAttribute(QuotationConstants.QUOTATION_HEADINGMESSAGE,QuotationConstants.QUOTATION_SUB_SUCES);
		            request.setAttribute(QuotationConstants.QUOTATION_SUCCESSMESSAGE,sMsg);
		            request.setAttribute(QuotationConstants.QUOTATION_LINKSMESSAGE,sLinkMsg);
		            
				}
			 } else {
                 request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE,QuotationConstants.QUOTATION_INVALIDTOKENMESSAGE);
                 /* Forwarding request to Error page */
                 return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
			 }
			 
		 } catch(Exception e) {
			   //e.printStackTrace();
	           /* Logging any exception that raised during this activity in log files using Log4j */
	           LoggerUtility.log("DEBUG", this.getClass().getName(), sMethodName, ExceptionUtility.getStackTraceAsString(e));
	           /* Setting error details to request to display the same in Exception page */
	           request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
	           /* Forwarding request to Error page */
	           return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
		 } finally {
	           /* Releasing/closing the connection object */
	           if ( conn!=null )
	               oDBUtility.releaseResources(conn);
		 }
	   	   
		 LoggerUtility.log("INFO", sClassName, sMethodName, "END");
		 return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);
	}
	
	/**
	 * This method is used to copy the Existing Enquiry - Create New Enquiry - Assign to SM.
	 * @param actionMapping		ActionMapping object to redirect the user to next page depending on the activity
	 * @param actionForm		ActionForm object to handle the form elements
	 * @param request			HttpServletRequest object to handle request operations
	 * @param response			HttpServletResponse object to handle response operations
	 * @return ActionForward 	depending on which user is redirected to next page
	 * @throws Exception		if any error occurs while performing database operations, etc
	 */
	public ActionForward createNewEnqCommRevision(ActionMapping actionMapping,
            ActionForm actionForm, 
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
		
		String sMethodName = "createNewEnqCommRevision";
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        NewEnquiryDto oNewEnquiryDto = null;
   	    Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        
        UserDto oUserDto = null;
        UserAction oUserAction =null;
        DaoFactory oDaoFactory =null;
        QuotationDao oQuotationDao =null;
        WorkflowDto oWorkflowDto = null;
        WorkflowUtility oWorkflowUtility =null;
        int sOldEnquiryId = QuotationConstants.QUOTATION_LITERAL_ZERO;
        int iWorkflowExecuteStatus = QuotationConstants.QUOTATION_LITERAL_ZERO;
        String sMsg=null,sLinkMsg1=null,sLinkMsg2=null,sLinkMsg=null;
        
        NewEnquiryForm oNewEnquiryForm = (NewEnquiryForm)actionForm;
		if (oNewEnquiryForm == null){
			oNewEnquiryForm = new NewEnquiryForm();
		}
		
		 try {
			 if(isTokenValid(request)) {
				resetToken(request);

				/* Setting user's information in request, using the header information received through request */
				setRolesToRequest(request);
				LoggerUtility.log("INFO", sClassName, sMethodName, "Roles set to request.");
	             
				/* Instantiating the DBUtility object */
				oDBUtility = new DBUtility();
				/* Instantiating the UserAction object */
				oUserAction = new UserAction();
				/* Creating an instance of of DaoFactory & retrieving UserDao object */
				oDaoFactory = new DaoFactory();
				/* Retrieving the database connection using DBUtility.getDBConnection method */
				conn = oDBUtility.getDBConnection();
				oQuotationDao = oDaoFactory.getQuotationDao();
				/* Retrieving the logged-in user information */
				oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
				
				oNewEnquiryDto = new NewEnquiryDto();
				PropertyUtils.copyProperties(oNewEnquiryDto, oNewEnquiryForm);
				sOldEnquiryId = oNewEnquiryDto.getEnquiryId(); 	// Old Enquiry Id
				
				oNewEnquiryDto = oQuotationDao.createNewEnquiryRevision(conn, oNewEnquiryDto, oUserDto);
	               
	            request.setAttribute(QuotationConstants.QUOTATION_ENQUIRYDTO, oNewEnquiryDto); 
	            request.setAttribute(QuotationConstants.QUOTATION_USERDTO, oUserDto); 
	            
				if (oNewEnquiryDto == null) {
					/* Setting error details to request to display the same in Exception page */
					request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_PROB_COPYREC);
					/* Forwarding request to Error page */
					return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
				} else {
					oNewEnquiryDto = oQuotationDao.getNewEnquiryStatus(conn, oNewEnquiryDto);
					
					/*
					 * ############################################################# 
					 *  Commercial Revision - Workflow process initiated
					 * #############################################################
					 */
					LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Initiating Workflow process - - -");
					oWorkflowUtility = new WorkflowUtility();
					oWorkflowDto = new WorkflowDto();

					oWorkflowDto.setEnquiryId(oNewEnquiryDto.getEnquiryId());
					oWorkflowDto.setEnquiryNo(oNewEnquiryDto.getEnquiryNumber());
					oWorkflowDto.setBaseEnquiryId(sOldEnquiryId);
					oWorkflowDto.setLoginUserId(oUserDto.getUserId());
					oWorkflowDto.setLoginUserName(oUserDto.getFullName());
					oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
					oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_CREATEREVISION);
					oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_RELEASED);
					oWorkflowDto.setRevisionRemarks(QuotationConstants.QUOTATION_WORKFLOW_REVISION_COMMERCIAL);

					LoggerUtility.log("INFO", sClassName, sMethodName, "12 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
					iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);

					if (iWorkflowExecuteStatus == QuotationConstants.QUOTATION_LITERAL_ONE) {
						LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Successfully");
					} else if (iWorkflowExecuteStatus == QuotationConstants.QUOTATION_LITERAL_ZERO) {
						LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed - But email could not be sent. Please send email manually.");
					} else {
						LoggerUtility.log("INFO", sClassName, sMethodName, "Workflow Executed Failed");
					}

					LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Workflow process completed - - -");
					/*
					 * ############################################################# 
					 * Commercial Revision - Workflow process completed
					 * #############################################################
					 */
					
					// Build the Success Message
		            sMsg = QuotationConstants.QUOTATION_NEWREVISION_TRACKNO + oNewEnquiryDto.getEnquiryNumber();
		            
		            sLinkMsg1 = QuotationConstants.QUOTATION_RETURNHOME_URL + QuotationConstants.QUOTATION_GRSYMBOL + QuotationConstants.QUOTATION_VIEWLINK_RETURNHOME; 
		            sLinkMsg2 = QuotationConstants.QUOTATION_LT_VIEWCR_ENQ_URL + QuotationConstants.QUOTATION_PLACELINKS; 
		            sLinkMsg = sLinkMsg1 + QuotationConstants.QUOTATION_BREAK + sLinkMsg2;
		            
		            request.setAttribute(QuotationConstants.QUOTATION_HEADINGMESSAGE,QuotationConstants.QUOTATION_SUB_SUCES);
		            request.setAttribute(QuotationConstants.QUOTATION_SUCCESSMESSAGE,sMsg);
		            request.setAttribute(QuotationConstants.QUOTATION_LINKSMESSAGE,sLinkMsg);
		            
				}
			 } else {
                 request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE,QuotationConstants.QUOTATION_INVALIDTOKENMESSAGE);
                 /* Forwarding request to Error page */
                 return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
			 }
			 
		 } catch(Exception e) {
	           /* Logging any exception that raised during this activity in log files using Log4j */
	           LoggerUtility.log("DEBUG", this.getClass().getName(), sMethodName, ExceptionUtility.getStackTraceAsString(e));
	           /* Setting error details to request to display the same in Exception page */
	           request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
	           /* Forwarding request to Error page */
	           return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
		 } finally {
	           /* Releasing/closing the connection object */
	           if ( conn!=null )
	               oDBUtility.releaseResources(conn);
		 }
	   	   
		 LoggerUtility.log("INFO", sClassName, sMethodName, "END");
		 return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);
	}
	
	private ArrayList getNewRatingAddOns(int iRowSize, int iRatingId, HttpServletRequest request) {
		
		String sAddOnType = "";
		int iQtyRejected = QuotationConstants.QUOTATION_LITERAL_ZERO;
		int iAddOnPercent = 0;
		double dUnitPrice = 0.0;
		AddOnDto oAddOnDto = null;
		ArrayList arlAddOnList = new ArrayList();
		
		for (int i = 1; i <= iRowSize; i++) {
			sAddOnType = CommonUtility.getStringParameter(request, QuotationConstants.QUOTATION_UNITPRICE + i); 
			iQtyRejected = CommonUtility.getIntParameter(request, QuotationConstants.QUOTATION_ADDONQUANTITY + i);
			iAddOnPercent = CommonUtility.getIntParameter(request, "addOnPercent" + i);
			dUnitPrice = CommonUtility.getDoubleParameter(request, QuotationConstants.QUOTATION_ADDONUNITPRICE + i);
			
			if (sAddOnType.trim().length() > QuotationConstants.QUOTATION_LITERAL_ZERO || 
				iQtyRejected > QuotationConstants.QUOTATION_LITERAL_ZERO || 
				iAddOnPercent > QuotationConstants.QUOTATION_LITERAL_ZERO || 
				dUnitPrice > QuotationConstants.QUOTATION_LITERAL_ZERO) {
				
					oAddOnDto = new AddOnDto();
					oAddOnDto.setAddOnRatingId(iRatingId);
					oAddOnDto.setAddOnTypeText(sAddOnType);
					oAddOnDto.setAddOnQuantity(iQtyRejected);
					oAddOnDto.setAddOnPercent(iAddOnPercent);
					oAddOnDto.setAddOnUnitPrice(dUnitPrice);
					
					arlAddOnList.add(oAddOnDto);
			}
		}
		
		return arlAddOnList;
	}
	
	private NewTechnicalOfferDto getSelectedRatingTechOfferFieldValues(Connection conn, QuotationDao oQuotationDao, NewRatingDto oNewRatingDto, NewTechnicalOfferDto oNewTechnicalOfferDto, HttpServletRequest request) throws Exception {
		String sMethodName = "getSelectedRatingTechOfferFieldValues";
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        oNewTechnicalOfferDto.setRatingId(oNewRatingDto.getRatingId());
        
        oNewTechnicalOfferDto.setProductGroup(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_PRODGROUP, CommonUtility.getStringParameter(request, "productGroup" )) );
        oNewTechnicalOfferDto.setProductLine(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_PRODLINE, CommonUtility.getStringParameter(request, "productLine" )) );
        oNewTechnicalOfferDto.setPowerRating_KW(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_KW, CommonUtility.getStringParameter(request, "kw" )) );
        oNewTechnicalOfferDto.setFrameType(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_FRAME, CommonUtility.getStringParameter(request, "frame" )) );
        oNewTechnicalOfferDto.setNoOfPoles(Integer.parseInt(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_FRAME, CommonUtility.getStringParameter(request, "pole" )) ) );
        oNewTechnicalOfferDto.setMountingType(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_FRAME, CommonUtility.getStringParameter(request, "mounting" )) );
        oNewTechnicalOfferDto.setTbPosition(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_FRAME, CommonUtility.getStringParameter(request, "tbPosition" )) );
        
        oNewTechnicalOfferDto.setModelNumber(CommonUtility.getStringParameter(request, "modelNumber" ) );
        oNewTechnicalOfferDto.setVoltage(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_VOLT, CommonUtility.getStringParameter(request, "volt" )) );
        oNewTechnicalOfferDto.setVoltageAddVariation(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_VOLT_VARIATION, CommonUtility.getStringParameter(request, "voltAddVariation" )) );
        oNewTechnicalOfferDto.setVoltageRemoveVariation(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_VOLT_VARIATION, CommonUtility.getStringParameter(request, "voltRemoveVariation" )) );
        oNewTechnicalOfferDto.setFrequency(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_FREQ, CommonUtility.getStringParameter(request, "freq" )) );
        oNewTechnicalOfferDto.setFrequencyAddVariation(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_FREQ_VARIATION, CommonUtility.getStringParameter(request, "freqAddVariation" )) );
        oNewTechnicalOfferDto.setFrequencyRemoveVariation(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_FREQ_VARIATION, CommonUtility.getStringParameter(request, "freqRemoveVariation" )) );
        oNewTechnicalOfferDto.setCombinedVariation(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_COMBVAR, CommonUtility.getStringParameter(request, "combinedVariation" )) );
        oNewTechnicalOfferDto.setCoolingMethod(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_FORCED_COOL, CommonUtility.getStringParameter(request, "methodOfCooling" )) );
        oNewTechnicalOfferDto.setMotorInertia(CommonUtility.getStringParameter(request, "rtGD2" ) );
        oNewTechnicalOfferDto.setLoadInertia(CommonUtility.getStringParameter(request, "miGD2Load" ) );
        oNewTechnicalOfferDto.setVibration(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_VIBRATION, CommonUtility.getStringParameter(request, "vibration" )) );
        oNewTechnicalOfferDto.setNoise(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_NOISE_LEVEL, CommonUtility.getStringParameter(request, "noiseLevel" )) );
        oNewTechnicalOfferDto.setNoOfStarts(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_STARTS, CommonUtility.getStringParameter(request, "starts" )) );
        oNewTechnicalOfferDto.setDirectionOfRotation(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_DIR_OF_ROTATION, CommonUtility.getStringParameter(request, "directionOfRotation" )) );
        
        // Below Fields - Only For Standard FLow 
        // In Non - Standard Flow 	- Keep these fields Blank.
        oNewTechnicalOfferDto.setPowerRating_HP("");
        oNewTechnicalOfferDto.setMotorOrientation("");
        oNewTechnicalOfferDto.setFrameLength("");
        oNewTechnicalOfferDto.setNoOfSpeeds("");
        oNewTechnicalOfferDto.setShaftDiameter("");
        oNewTechnicalOfferDto.setShaftExtension("");
        
        String sNoOfRunsVal = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_NO_OF_RUNS, CommonUtility.getStringParameter(request, "noOfRuns" )); 
        String sNoOfCoresVal = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_NO_OF_CORES, CommonUtility.getStringParameter(request, "noOfCores" )); 
        String sCrossSectionAreaVal = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_CROSSSECTION_AREA, CommonUtility.getStringParameter(request, "crossSectionArea" )); 
        String sConductorMaterialVal = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_CONDUCTOR_MATERIAL, CommonUtility.getStringParameter(request, "conductorMaterial" )); 
        if("Aluminium".equalsIgnoreCase(sConductorMaterialVal)) {
        	sConductorMaterialVal = "AL";
        } else if("Copper".equalsIgnoreCase(sConductorMaterialVal)) {
        	sConductorMaterialVal = "CU";
        } else {
        	sConductorMaterialVal = "";
        }
        oNewTechnicalOfferDto.setCableSize(sNoOfRunsVal + "R" + " X " + sNoOfCoresVal + "C" + " X " + sCrossSectionAreaVal + " mm2" + " " + sConductorMaterialVal);
        
        oNewTechnicalOfferDto.setCurrent_A(CommonUtility.getStringParameter(request, "flcAmps" ) );
        oNewTechnicalOfferDto.setNoLoadCurrent(CommonUtility.getStringParameter(request, "nlCurrent" ) );
        oNewTechnicalOfferDto.setMaxSpeed(CommonUtility.getStringParameter(request, "rpm" ) );
        oNewTechnicalOfferDto.setPowerFactor_100(CommonUtility.getStringParameter(request, "pllPf1" ) );
        oNewTechnicalOfferDto.setPowerFactor_75(CommonUtility.getStringParameter(request, "pllPf2" ) );
        oNewTechnicalOfferDto.setPowerFactor_50(CommonUtility.getStringParameter(request, "pllpf3" ) );
        oNewTechnicalOfferDto.setPowerFactor_Start(CommonUtility.getStringParameter(request, "pllpf4" ) );
        oNewTechnicalOfferDto.setPowerFactor_DP(CommonUtility.getStringParameter(request, "pllpf5" ) );
        oNewTechnicalOfferDto.setEfficiencyClass(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_EFFICIENCY_CLASS, CommonUtility.getStringParameter(request, "efficiencyClass" )) );
        oNewTechnicalOfferDto.setEfficiency_100(CommonUtility.getStringParameter(request, "pllEff1" ) );
        oNewTechnicalOfferDto.setEfficiency_75(CommonUtility.getStringParameter(request, "pllEff2" ) );
        oNewTechnicalOfferDto.setEfficiency_50(CommonUtility.getStringParameter(request, "pllEff3" ) );
        oNewTechnicalOfferDto.setEfficiency_DP(CommonUtility.getStringParameter(request, "pllEff4" ) );
        oNewTechnicalOfferDto.setCurrentA_100(CommonUtility.getStringParameter(request, "pllCurr1" ) );
        oNewTechnicalOfferDto.setCurrentA_75(CommonUtility.getStringParameter(request, "pllCurr2" ) );
        oNewTechnicalOfferDto.setCurrentA_50(CommonUtility.getStringParameter(request, "pllCurr3" ) );
        oNewTechnicalOfferDto.setCurrentA_Start(CommonUtility.getStringParameter(request, "pllCurr4" ) );
        oNewTechnicalOfferDto.setCurrentA_DP(CommonUtility.getStringParameter(request, "pllCurr5" ) );
        oNewTechnicalOfferDto.setStartingCurrent(CommonUtility.getStringParameter(request, "strtgCurrentText" ) );
        oNewTechnicalOfferDto.setTorque(CommonUtility.getStringParameter(request, "fltSQCAGE" ) );
        oNewTechnicalOfferDto.setStarting_torque(CommonUtility.getStringParameter(request, "lrTorque" ) );
        oNewTechnicalOfferDto.setPullout_torque(CommonUtility.getStringParameter(request, "potFLT" ) );
        oNewTechnicalOfferDto.setBkw(CommonUtility.getStringParameter(request, "bkw" ) );
        oNewTechnicalOfferDto.setRaValue(CommonUtility.getStringParameter(request, "ra" ) );
        oNewTechnicalOfferDto.setRvValue(CommonUtility.getStringParameter(request, "rv" ) );
        
        String sHazAreaType = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_HAZARD_AREA_TYPE, CommonUtility.getStringParameter(request, "hazardAreaType" ));
        String sHazZone = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_HAZARD_AREA, CommonUtility.getStringParameter(request, "hazardArea" ));
        oNewTechnicalOfferDto.setZoneClassification(sHazAreaType + " " + sHazZone);
        oNewTechnicalOfferDto.setGasGroup(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_GASGROUP, CommonUtility.getStringParameter(request, "gasGroup" )) );
        oNewTechnicalOfferDto.setTemperatureClass(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_TEMPERATURE_CLASS, CommonUtility.getStringParameter(request, "tempClass" )) );
        
        oNewTechnicalOfferDto.setSstHot(CommonUtility.getStringParameter(request, "sstHot" ) );
        oNewTechnicalOfferDto.setSstCold(CommonUtility.getStringParameter(request, "sstCold" ) );
        oNewTechnicalOfferDto.setLrWithstandTime(oNewTechnicalOfferDto.getSstHot() + " / " + oNewTechnicalOfferDto.getSstCold() + " s");
        oNewTechnicalOfferDto.setRotortype("");
        oNewTechnicalOfferDto.setElectricalType(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_MOTORTYPE, CommonUtility.getStringParameter(request, "typeOfMotor" )) );
        oNewTechnicalOfferDto.setIpCode(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_IP, CommonUtility.getStringParameter(request, "ip" )) );
        oNewTechnicalOfferDto.setInsulationClass(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_INSULATION_CLASS, CommonUtility.getStringParameter(request, "insulationClass" )) );
        oNewTechnicalOfferDto.setServiceFactor(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_SERVICE_FACTOR, CommonUtility.getStringParameter(request, "serviceFactor" )) );
        oNewTechnicalOfferDto.setDuty(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_SERVICE_FACTOR, CommonUtility.getStringParameter(request, "serviceFactor" )) );
        oNewTechnicalOfferDto.setCdf(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_CDF, CommonUtility.getStringParameter(request, "cdf" )) );
        oNewTechnicalOfferDto.setNoOfStarts_HR(CommonUtility.getStringParameter(request, "startsPerHour" ) );
        oNewTechnicalOfferDto.setNoOfPhases("");
        oNewTechnicalOfferDto.setMaxAmbientTemp(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_AMBTEMP, CommonUtility.getStringParameter(request, "ambientTemp" )) );
        oNewTechnicalOfferDto.setTempRise(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_TEMP_RISE, CommonUtility.getStringParameter(request, "tempRise" )) );
        oNewTechnicalOfferDto.setAltitude(CommonUtility.getStringParameter(request, "altitude" ) );
        oNewTechnicalOfferDto.setDeBearingSize("");
        oNewTechnicalOfferDto.setDeBearingType("");
        oNewTechnicalOfferDto.setOdeBearingSize("");
        oNewTechnicalOfferDto.setOdeBearingType("");
        oNewTechnicalOfferDto.setEnclosureType(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_ENCLOSURE, CommonUtility.getStringParameter(request, "enclosureId" )) );
        oNewTechnicalOfferDto.setLubMethod(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LUB, CommonUtility.getStringParameter(request, "lubrication" )) );
        oNewTechnicalOfferDto.setTypeOfGrease("");
        
        oNewTechnicalOfferDto.setFrameMaterial("");
        oNewTechnicalOfferDto.setFrameSize("");
        oNewTechnicalOfferDto.setRotation(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_STANDARD_ROTATION, CommonUtility.getStringParameter(request, "standardRotation" )) );
        oNewTechnicalOfferDto.setShaftType(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_SHAFT_TYPE, CommonUtility.getStringParameter(request, "shaftType" )) );
        oNewTechnicalOfferDto.setOutlineDrawingNum("");
        oNewTechnicalOfferDto.setStatorConnection("");
        oNewTechnicalOfferDto.setRotorConnection("");
        oNewTechnicalOfferDto.setConnDrawingNum("");
        oNewTechnicalOfferDto.setOverallLengthMM("");
        oNewTechnicalOfferDto.setMotorWeight("");
        oNewTechnicalOfferDto.setGrossWeight(CommonUtility.getStringParameter(request, "motorTotalWgt" ));
        oNewTechnicalOfferDto.setStartingType(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_METHOD_OF_STARTING, CommonUtility.getStringParameter(request, "methodOfStarting" )) );
        oNewTechnicalOfferDto.setThruBoltsExtension("");
        oNewTechnicalOfferDto.setOverloadProtectionType("");
        
        String sCertificationVal = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_ULCE, CommonUtility.getStringParameter(request, "ulce" ));
        if("CE".equalsIgnoreCase(sCertificationVal)) {
        	oNewTechnicalOfferDto.setCeValue("Yes");
        } else {
        	oNewTechnicalOfferDto.setCeValue("No");
        }
        if("UL".equalsIgnoreCase(sCertificationVal)) {
        	oNewTechnicalOfferDto.setUlValue("Yes");
        } else {
        	oNewTechnicalOfferDto.setUlValue("No");
        }
        
        oNewTechnicalOfferDto.setCsaValue("");
        oNewTechnicalOfferDto.setStartTimeRatedVolt(CommonUtility.getStringParameter(request, "sstHot" ) );
        oNewTechnicalOfferDto.setStartTimeRatedVolt80(CommonUtility.getStringParameter(request, "sstHot" ) );
        oNewTechnicalOfferDto.setCableEntry(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_CABLEENTRY, CommonUtility.getStringParameter(request, "cableEntry" )) );
        oNewTechnicalOfferDto.setNoBOTerminals(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_BOTERMINALSNO, CommonUtility.getStringParameter(request, "noBoTerminals" )) );
        oNewTechnicalOfferDto.setMainTBoxPS(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_MTBPS, CommonUtility.getStringParameter(request, "mainTermBoxPS" )) );
        
        oNewTechnicalOfferDto.setMinStartingVolt("");
        oNewTechnicalOfferDto.setMainTermBox("");
        oNewTechnicalOfferDto.setBearingThermo_DT("");
        oNewTechnicalOfferDto.setAirThermo("");
        oNewTechnicalOfferDto.setVibProbeMPads("");
        oNewTechnicalOfferDto.setEncoder("");
        oNewTechnicalOfferDto.setSpmNipple("");
        oNewTechnicalOfferDto.setKeyPhasor("");
        oNewTechnicalOfferDto.setSpeedSwitch("");
        oNewTechnicalOfferDto.setSurgeSuppressors("");
        oNewTechnicalOfferDto.setCurrentTransformers("");
        oNewTechnicalOfferDto.setVibProbes("");
        
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        return oNewTechnicalOfferDto;
	}
	
	public ActionForward fetchRSMApprovalLimitAjax(ActionMapping actionMapping, 
			ActionForm form,
			HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		String sMethodName = "fetchRSMApprovalLimitAjax"; 
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        PrintWriter oOut = null;
        DBUtility oDBUtility = new DBUtility();
        DaoFactory oDaoFactory = null; //Creating an instance of of DaoFactory & retrieving UserDao object
        CustomerDiscountDao oCustomerDiscountDao = null;
        setRolesToRequest(request);
        KeyValueVo oRSMDiscountVo = new KeyValueVo();
        Map<String,String> keyvalueMap = new HashMap<>();
        response.setContentType("application/json");
        
        try (Connection conn = oDBUtility.getDBConnection()) {
        	oDaoFactory = new DaoFactory();
        	oCustomerDiscountDao = oDaoFactory.getCustomerDiscountDao();
        	
        	String data = (String) request.getParameter("reqparams");
        	ObjectMapper objectMapper = new ObjectMapper();
        	HashMap<String, Object> map = new HashMap<String, Object>();
			map = objectMapper.readValue(data, HashMap.class);
			CustomerDiscountDto oCustomerDiscountDto = new CustomerDiscountDto();
			oCustomerDiscountDto.setCustomerId((String)map.get("customerIdVal"));
			oCustomerDiscountDto.setProduct_line((String)map.get("prodLineVal"));
			oCustomerDiscountDto.setFrame_type((String)map.get("frameTypeVal"));
			
			oRSMDiscountVo = oCustomerDiscountDao.fetchRSMDiscountApprovalLimit(conn, oCustomerDiscountDto);
			
			if(oRSMDiscountVo != null) {
				keyvalueMap.put(oRSMDiscountVo.getKey(), oRSMDiscountVo.getValue());
			}
			
			response.setHeader("X-JSON", JSONValue.toJSONString(keyvalueMap));	
			oOut = response.getWriter();
			oOut.write(JSONValue.toJSONString(keyvalueMap));
			oOut.flush();
			oOut.close();
        }
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
    	return null;
	}
	
	public ActionForward fetchNSMApprovalLimitAjax(ActionMapping actionMapping, 
			ActionForm form,
			HttpServletRequest request, 
			HttpServletResponse response) throws Exception {
		
		String sMethodName = "fetchNSMApprovalLimitAjax"; 
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        PrintWriter oOut = null;
        DBUtility oDBUtility = new DBUtility();
        DaoFactory oDaoFactory = null; //Creating an instance of of DaoFactory & retrieving UserDao object
        CustomerDiscountDao oCustomerDiscountDao = null;
        setRolesToRequest(request);
        KeyValueVo oNSMDiscountVo = new KeyValueVo();
        Map<String,String> keyvalueMap = new HashMap<>();
        response.setContentType("application/json");
        
        try (Connection conn = oDBUtility.getDBConnection()) {
        	oDaoFactory = new DaoFactory();
        	oCustomerDiscountDao = oDaoFactory.getCustomerDiscountDao();
        	
        	String data = (String) request.getParameter("reqparams");
        	ObjectMapper objectMapper = new ObjectMapper();
        	HashMap<String, Object> map = new HashMap<String, Object>();
			map = objectMapper.readValue(data, HashMap.class);
			CustomerDiscountDto oCustomerDiscountDto = new CustomerDiscountDto();
			oCustomerDiscountDto.setCustomerId((String)map.get("customerIdVal"));
			oCustomerDiscountDto.setProduct_line((String)map.get("prodLineVal"));
			oCustomerDiscountDto.setFrame_type((String)map.get("frameTypeVal"));
			
			oNSMDiscountVo = oCustomerDiscountDao.fetchNSMDiscountApprovalLimit(conn, oCustomerDiscountDto);
			
			if(oNSMDiscountVo != null) {
				keyvalueMap.put(oNSMDiscountVo.getKey(), oNSMDiscountVo.getValue());
			}
			
			response.setHeader("X-JSON", JSONValue.toJSONString(keyvalueMap));	
			oOut = response.getWriter();
			oOut.write(JSONValue.toJSONString(keyvalueMap));
			oOut.flush();
			oOut.close();
        }
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
    	return null;
	}
	
	private void processCompleteRatings(Connection conn, NewEnquiryDto oNewEnquiryDto, UserDto oUserDto, ArrayList<NewRatingDto> alRatingsList, String sActionType) throws Exception {
		
		String sMethodName = "processCompleteRatings"; 
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        DBUtility oDBUtility = null;
		DaoFactory oDaoFactory = null; 
        QuotationDao oQuotationDao = null;
        AddOnDetailsDao oAddOnDetailsDao = null;
        String sResult = "";
        
		try {
			/* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
			/* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
            
            oQuotationDao = oDaoFactory.getQuotationDao();
            oAddOnDetailsDao = oDaoFactory.getAddOnDetailsDao();
            
            NewEnquiryUtilityManager oNewEnquiryManager = new NewEnquiryUtilityManager();
    		
    		for(NewRatingDto oNewRatingDto : alRatingsList) {
    			// Fetch Prices / AddOns / MTOs.
    			oNewRatingDto.setLtCustomerId(String.valueOf(oNewEnquiryDto.getCustomerId()));
    			oNewRatingDto.setFinalPriceCheck(true);
    			oNewEnquiryManager.calculateNewEnquiryPrices(oNewRatingDto);
    			
    			// Save the NewRatingDto.
    			oDBUtility.setAutoCommitFalse(conn);
    			if(QuotationConstants.QUOTATION_REFERDESIGN.equalsIgnoreCase(sActionType)) {
    				oNewRatingDto.setIsReferDesign(QuotationConstants.QUOTATION_STRING_Y);
    			}
    			oNewRatingDto.setIsDesignComplete("N");
    			sResult = oQuotationDao.saveRatingDetails(conn, oNewRatingDto);
    			if (QuotationConstants.QUOTATION_STRING_Y.equals(sResult)) {
    				oDBUtility.commit(conn);
    			} else {
    				oDBUtility.rollback(conn);
    			}
    			oDBUtility.setAutoCommitTrue(conn);
    			
    			// If "Refer Engineering" is "Y" for Current Rating - Set NewEnquiry."IsReferDesign" = true.
    			if( QuotationConstants.QUOTATION_STRING_Y.equalsIgnoreCase(oNewRatingDto.getIsReferDesign()) ) {
    				oNewEnquiryDto.setIsReferDesign(oNewRatingDto.getIsReferDesign());
    			}
    			
    			// Save the Add-On's and MTO's Lists to DB.
    			oAddOnDetailsDao.updateAddOnMTODetails(conn, oNewRatingDto, oUserDto.getUserId());
    		}
    		
    		// Save "Refer Engineering" Flag - After processing Ratings List.
    		oQuotationDao.updateSubmitToDesignFlag(conn, oNewEnquiryDto);
    		
		} catch(Exception e) {
            /* Logging any exception that raised during this activity in log files using Log4j */
            LoggerUtility.log("DEBUG", sClassName, sMethodName, ExceptionUtility.getStackTraceAsString(e));
            //System.out.println(" ::::  Exception while Saving Add-Ons and MTOs :: " + sClassName + " :: " + sMethodName + ExceptionUtility.getStackTraceAsString(e));
            //e.printStackTrace();
            
        }
		
		LoggerUtility.log("INFO", sClassName, sMethodName, "END");
	}
	
	
	/**
	 * Method used for Processing the Action from Released to Customer Page.
	 * @param actionMapping
	 * @param actionForm
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward processReleasedQuote(ActionMapping actionMapping,
            ActionForm actionForm, 
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
		
		String sMethodName = "processReleasedQuote"; 
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        UserDto oUserDto = null;
        String sOperationType = QuotationConstants.QUOTATION_NONE; 
        String sDispatch = null;
        UserAction oUserAction=null; //Instantiating the UserAction object
        DaoFactory oDaoFactory=null; //Creating an instance of of DaoFactory & retrieving UserDao object
        QuotationDao oQuotationDao=null;// Creating an Instance for QuotationDao
        NewEnquiryForm oNewEnquiryForm = null;
		NewEnquiryDto oNewEnquiryDto = null;
		NewRatingDto oNewRatingDto = null;
		NewRatingDto oTempNewRatingDto = null;
		WorkflowUtility oWorkflowUtility = null;
		WorkflowDto oWorkflowDto = null;
		WorkflowDao oWorkFlowDao = null;
		String sMsg=null,sLinkMsg1=null,sLinkMsg2=null,sLinkMsg=null,sCommentOperation,sDmComment=null,sDeComment=null,sRatingResult=null;
		
		
        try {
        	setRolesToRequest(request);
            LoggerUtility.log("INFO", sClassName, sMethodName, "Roles set to request.");
            
    		oNewEnquiryForm = (NewEnquiryForm) actionForm;
    		if (oNewEnquiryForm == null)
    			oNewEnquiryForm = new NewEnquiryForm();
    		
    		sOperationType = oNewEnquiryForm.getOperationType();
    		sDispatch = oNewEnquiryForm.getDispatch(); 
    		sCommentOperation=oNewEnquiryForm.getCommentOperation();
    		
            /* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Instantiating the UserAction object */
            oUserAction = new UserAction();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
            
            oQuotationDao = oDaoFactory.getQuotationDao();
            
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
           
            if (! isTokenValid(request)) {
           	 /* Setting error details to request to display the same in Exception page */ 
               request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, QuotationConstants.QUOTATION_INVALIDTOKENMESSAGE);
               /* Forwarding request to Error page */
               return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
            }          
            
            oDBUtility.setAutoCommitFalse(conn);
            
            oNewEnquiryDto = new NewEnquiryDto();
            oNewRatingDto = new NewRatingDto();
            NewEnquiryUtilityManager oNewEnquiryManager = new NewEnquiryUtilityManager();
            
            /* Retrieving the logged-in user information */
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            if (oUserDto == null){
            	oUserDto = new UserDto();
            }
            
            oNewEnquiryForm.setCreatedBy(oUserDto.getFullName());           
            oNewEnquiryForm.setCreatedBySSO(oUserDto.getUserId());
            PropertyUtils.copyProperties(oNewEnquiryDto, oNewEnquiryForm);
            
            LoggerUtility.log("INFO", sClassName, sMethodName, "iEnquiryId -  " + oNewEnquiryDto.getEnquiryId());
            
            if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_LT_ABONDENT))  {
            	PropertyUtils.copyProperties(oNewEnquiryDto, oNewEnquiryForm);
    			oNewEnquiryDto = oQuotationDao.getNewEnquiryDetailsForView(conn, oNewEnquiryDto, QuotationConstants.QUOTATION_FORWARD_VIEW.toUpperCase());
    			
    			PropertyUtils.copyProperties(oNewEnquiryForm, oNewEnquiryDto);
    			// Load ABONDENT Related Lists.
    			oNewEnquiryForm.setLtAbondentReasonList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_ABONDENT_REASON));
    			oNewEnquiryForm.setAlTargetedList(oQuotationDao.getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_TARGTD));
    			
                return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_ABONDENT_VIEW);
            }
            
            if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_LT_LOST))  {
            	PropertyUtils.copyProperties(oNewEnquiryDto, oNewEnquiryForm);
    			oNewEnquiryDto = oQuotationDao.getNewEnquiryDetailsForView(conn, oNewEnquiryDto, QuotationConstants.QUOTATION_FORWARD_VIEW.toUpperCase());
    			// Calculate TotalQuotedPrice
    			String sQuotedTotal = oNewEnquiryManager.calculateQuotedTotalPrice(oNewEnquiryDto);
    			oNewEnquiryDto.setTotalQuotedPrice(sQuotedTotal);
    			
    			PropertyUtils.copyProperties(oNewEnquiryForm, oNewEnquiryDto);
    			oNewEnquiryForm.setTotalRatingIndex(oNewEnquiryDto.getNewRatingsList().size());
    			
    			// Load LOST Related Lists.
    			oNewEnquiryForm.setLtLostCompetitorList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_LOSS_COMPETITOR));
    			oNewEnquiryForm.setLtLostReasonList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_LOSS_REASON));
    			oNewEnquiryForm.setAlTargetedList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_TARGTD));
    			
    			// Setting NewEnquiryDto to Request : For fetching Ratings List.
    			request.setAttribute(QuotationConstants.QUOTATION_LOWER_NEW_INQUIRYDTO, oNewEnquiryDto);
    			
    			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_LOST_VIEW);
            }
            
            if (sOperationType.equalsIgnoreCase(QuotationConstants.QUOTATION_LT_WON))  {
            	PropertyUtils.copyProperties(oNewEnquiryDto, oNewEnquiryForm);
            	oNewEnquiryDto = oQuotationDao.getNewEnquiryDetailsForView(conn, oNewEnquiryDto, QuotationConstants.QUOTATION_FORWARD_VIEW.toUpperCase());
            	
            	String sQuotedTotal = oNewEnquiryManager.calculateQuotedTotalPrice(oNewEnquiryDto);
    			oNewEnquiryDto.setTotalQuotedPrice(sQuotedTotal);
    			
    			PropertyUtils.copyProperties(oNewEnquiryForm, oNewEnquiryDto);
    			oNewEnquiryForm.setTotalRatingIndex(oNewEnquiryDto.getNewRatingsList().size());
    			
    			// Load WON Related Lists.
    			oNewEnquiryForm.setLtWonPOOptionsList(oQuotationDao.getAscendingOptionsbyAttributeList(conn, QuotationConstants.QEM_LT_WON_POOPTIONS));
    			oNewEnquiryForm.setLtWonReasonList(oQuotationDao.getAscendingOptionsbyAttributeList(conn, QuotationConstants.QEM_LT_WON_REASON));
    			oNewEnquiryForm.setAlTargetedList(oQuotationDao.getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_TARGTD));
    			
    			// Setting NewEnquiryDto to Request : For fetching Ratings List.
    			request.setAttribute(QuotationConstants.QUOTATION_LOWER_NEW_INQUIRYDTO, oNewEnquiryDto);
    			
    			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_WON_VIEW);
            }
            
        } catch (Exception e) {
			/* Logging any exception that raised during this activity in log files using Log4j */
			LoggerUtility.log("DEBUG", this.getClass().getName(), sMethodName, ExceptionUtility.getStackTraceAsString(e));

			/* Setting error details to request to display the same in Exception page */
			request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));

			/* Forwarding request to Error page */
			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
		} finally {
			/* Releasing/closing the connection object */
			if (conn != null)
				oDBUtility.releaseResources(conn);
		}
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);
	}
	
	
	/**
	 * Method used for Processing the ABONDENT Request for New Enquiry.
	 * @param actionMapping
	 * @param actionForm
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward processAbandonEnquiry(ActionMapping actionMapping,
            ActionForm actionForm, 
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
		
		String sMethodName = "processAbandonEnquiry"; 
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        UserDto oUserDto = null;
        UserAction oUserAction=null; //Instantiating the UserAction object
        DaoFactory oDaoFactory=null; //Creating an instance of of DaoFactory & retrieving UserDao object
        QuotationDao oQuotationDao=null;// Creating an Instance for QuotationDao
        String sMsg=null,sLinkMsg1=null,sLinkMsg2=null,sLinkMsg=null,sRatingResult=null;
        int iWorkflowExecuteStatus=0;
        //boolean isUpdatedRating=QuotationConstants.QUOTATION_FALSE;
		NewEnquiryForm oNewEnquiryForm = null;
		NewEnquiryDto oNewEnquiryDto = null;
		WorkflowUtility oWorkflowUtility = null;
		WorkflowDto oWorkflowDto = null;
		boolean isInsertUpdateComplete = false;
		
        try {
        	setRolesToRequest(request);
            LoggerUtility.log("INFO", sClassName, sMethodName, "Roles set to request.");
            
    		oNewEnquiryForm = (NewEnquiryForm) actionForm;
    		if (oNewEnquiryForm == null)
    			oNewEnquiryForm = new NewEnquiryForm();
    		
            /* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Instantiating the UserAction object */
            oUserAction = new UserAction();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
            /* Create QuotationDao */
            oQuotationDao = oDaoFactory.getQuotationDao();
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            
            /* Retrieving the logged-in user information */
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            if (oUserDto == null){
            	oUserDto = new UserDto();
            }
            
            oNewEnquiryDto = new NewEnquiryDto();
            oNewEnquiryForm.setCreatedBy(oUserDto.getFullName());           
            oNewEnquiryForm.setCreatedBySSO(oUserDto.getUserId());
            PropertyUtils.copyProperties(oNewEnquiryDto, oNewEnquiryForm);
            
            LoggerUtility.log("INFO", sClassName, sMethodName, "iEnquiryId -  " + oNewEnquiryDto.getEnquiryId());
            
            oDBUtility.setAutoCommitFalse(conn);
            
            // Save ABONDENT Details.
            sRatingResult = oQuotationDao.saveAbondentEnquiry(conn, oNewEnquiryDto);
            
            if (QuotationConstants.QUOTATION_STRING_Y.equals(sRatingResult)) {
				oDBUtility.commit(conn);
				isInsertUpdateComplete = true;
			} else {
				isInsertUpdateComplete = false;
				oDBUtility.rollback(conn);
			}
            oDBUtility.setAutoCommitTrue(conn);
            
            // Workflow :  ABONDENT
            resetToken(request);
		    LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Initiating Workflow process - - -");
		    oWorkflowUtility = new WorkflowUtility();
            oWorkflowDto = new WorkflowDto();
            oWorkflowDto.setEnquiryId(oNewEnquiryDto.getEnquiryId());
            oWorkflowDto.setEnquiryNo(oNewEnquiryDto.getEnquiryNumber());
            oWorkflowDto.setLoginUserId(oUserDto.getUserId());
            oWorkflowDto.setLoginUserName(oUserDto.getFullName());
            oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
            oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_ABONDENT);
            oWorkflowDto.setOldStatusId(oNewEnquiryDto.getStatusId());
            oWorkflowDto.setReturnEnquiry(QuotationConstants.QUOTATION_ABONDED);
            
            LoggerUtility.log("INFO", sClassName, sMethodName, "2 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
            iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
            
            if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE ) {
                LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFS);
            } else  if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ZERO){ 
                LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFSEMAILF);
            } else {
                LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFF);
                
                request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE,QuotationConstants.QUOTATION_ER_INSERT);
                // Forwarding request to Error page 
        		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
            }
            
            // Build the Success Message
            sMsg = QuotationConstants.QUOTATION_ABONDENT_TRACKNO + oNewEnquiryDto.getEnquiryNumber();
            
            sLinkMsg1 = QuotationConstants.QUOTATION_RETURNHOME_URL + QuotationConstants.QUOTATION_GRSYMBOL + QuotationConstants.QUOTATION_VIEWLINK_RETURNHOME; 
            sLinkMsg2 = QuotationConstants.QUOTATION_LT_VIEWCR_ENQ_URL + QuotationConstants.QUOTATION_PLACELINKS; 
            sLinkMsg = sLinkMsg1 + QuotationConstants.QUOTATION_BREAK + sLinkMsg2; 
            
            request.setAttribute(QuotationConstants.QUOTATION_HEADINGMESSAGE,QuotationConstants.QUOTATION_SUB_SUCES);
            request.setAttribute(QuotationConstants.QUOTATION_SUCCESSMESSAGE,sMsg);
            request.setAttribute(QuotationConstants.QUOTATION_LINKSMESSAGE,sLinkMsg);
            
        } catch (Exception e) {
			/* Logging any exception that raised during this activity in log files using Log4j */
			LoggerUtility.log("DEBUG", this.getClass().getName(), sMethodName, ExceptionUtility.getStackTraceAsString(e));

			/* Setting error details to request to display the same in Exception page */
			request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));

			/* Forwarding request to Error page */
			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
		} finally {
			/* Releasing/closing the connection object */
			if (conn != null)
				oDBUtility.releaseResources(conn);
		}
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);
	}
	
	/**
	 * Method used for Processing the LOST Request for New Enquiry.
	 * @param actionMapping
	 * @param actionForm
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward processLostEnquiry(ActionMapping actionMapping,
            ActionForm actionForm, 
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
		
		String sMethodName = "processLostEnquiry"; 
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        UserDto oUserDto = null;
        UserAction oUserAction=null; //Instantiating the UserAction object
        DaoFactory oDaoFactory=null; //Creating an instance of of DaoFactory & retrieving UserDao object
        QuotationDao oQuotationDao=null;// Creating an Instance for QuotationDao
        String sMsg=null,sLinkMsg1=null,sLinkMsg2=null,sLinkMsg=null,sRatingResult=null;
        int iWorkflowExecuteStatus=0;
        //boolean isUpdatedRating=QuotationConstants.QUOTATION_FALSE;
        List<NewRatingDto> alRatingLostDetailsList = new ArrayList<>();
		NewEnquiryForm oNewEnquiryForm = null;
		NewEnquiryDto oNewEnquiryDto = null;
		NewRatingDto oTempNewRatingDto = null;
		WorkflowUtility oWorkflowUtility = null;
		WorkflowDto oWorkflowDto = null;
		boolean isInsertUpdateComplete = false;
		
        try {
        	setRolesToRequest(request);
            LoggerUtility.log("INFO", sClassName, sMethodName, "Roles set to request.");
            
    		oNewEnquiryForm = (NewEnquiryForm) actionForm;
    		if (oNewEnquiryForm == null)
    			oNewEnquiryForm = new NewEnquiryForm();
    		
            /* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Instantiating the UserAction object */
            oUserAction = new UserAction();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
            /* Create QuotationDao */
            oQuotationDao = oDaoFactory.getQuotationDao();
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            
            /* Retrieving the logged-in user information */
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            if (oUserDto == null){
            	oUserDto = new UserDto();
            }
            
            oNewEnquiryDto = new NewEnquiryDto();
            oNewEnquiryForm.setCreatedBy(oUserDto.getFullName());           
            oNewEnquiryForm.setCreatedBySSO(oUserDto.getUserId());
            PropertyUtils.copyProperties(oNewEnquiryDto, oNewEnquiryForm);
            
            LoggerUtility.log("INFO", sClassName, sMethodName, "iEnquiryId -  " + oNewEnquiryDto.getEnquiryId());
            
            // Calculate TotalLostPrice.
            double quotedPricePerUnit = 0d, lostPrice = 0d, lostPricePerUnit = 0d;
            double totalLostPrice = 0d;
            int totalRatingIndex = oNewEnquiryDto.getTotalRatingIndex();
            for (int iRatingIndex = 1; iRatingIndex <= totalRatingIndex; iRatingIndex++) {
            	oTempNewRatingDto = new NewRatingDto();
            	oTempNewRatingDto.setEnquiryId(oNewEnquiryDto.getEnquiryId());
            	oTempNewRatingDto.setRatingNo(CommonUtility.getIntParameter(request, "ratingNo"+iRatingIndex));
            	oTempNewRatingDto.setQuantity(CommonUtility.getIntParameter(request, "quantity"+iRatingIndex));
            	oTempNewRatingDto.setLtQuotedTotalPrice(CommonUtility.getStringParameter(request, "quotedPrice"+iRatingIndex));
            	oTempNewRatingDto.setLtLostTotalPrice(CommonUtility.getStringParameter(request, "lostPrice"+iRatingIndex));
            	lostPrice = Double.parseDouble(CommonUtility.replaceString(oTempNewRatingDto.getLtLostTotalPrice(), ",", ""));
            	lostPricePerUnit = lostPrice / oTempNewRatingDto.getQuantity();
            	totalLostPrice = totalLostPrice + lostPrice;
            	
            	alRatingLostDetailsList.add(oTempNewRatingDto);
            }
            oNewEnquiryDto.setTotalLostPrice(String.valueOf(totalLostPrice));
            
            oDBUtility.setAutoCommitFalse(conn);
            
            // Save the Ratings Lost Prices and Enquiry Total Lost Price and Lost Details.
            sRatingResult = oQuotationDao.saveLostEnquiryDetails(conn, oNewEnquiryDto, alRatingLostDetailsList);
            
            if (QuotationConstants.QUOTATION_STRING_Y.equals(sRatingResult)) {
				oDBUtility.commit(conn);
				isInsertUpdateComplete = true;
			} else {
				isInsertUpdateComplete = false;
				oDBUtility.rollback(conn);
			}
            oDBUtility.setAutoCommitTrue(conn);
            
            // Workflow :  LOST
            resetToken(request);
		    LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Initiating Workflow process - - -");
		    oWorkflowUtility = new WorkflowUtility();
            oWorkflowDto = new WorkflowDto();
            oWorkflowDto.setEnquiryId(oNewEnquiryDto.getEnquiryId());
            oWorkflowDto.setEnquiryNo(oNewEnquiryDto.getEnquiryNumber());
            oWorkflowDto.setLoginUserId(oUserDto.getUserId());
            oWorkflowDto.setLoginUserName(oUserDto.getFullName());
            oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
            oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_LOST);
            oWorkflowDto.setOldStatusId(oNewEnquiryDto.getStatusId());
            oWorkflowDto.setReturnEnquiry(QuotationConstants.QUOTATION_LOST);
            
            LoggerUtility.log("INFO", sClassName, sMethodName, "2 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
            iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
            
            if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE ) {
                LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFS);
            } else  if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ZERO){ 
                LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFSEMAILF);
            } else {
                LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFF);
                
                request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE,QuotationConstants.QUOTATION_ER_INSERT);
        		
        		// Forwarding request to Error page 
        		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
            }
            
            // Build the Success Message
            sMsg = QuotationConstants.QUOTATION_LOST_TRACKNO + oNewEnquiryDto.getEnquiryNumber();
            
            sLinkMsg1 = QuotationConstants.QUOTATION_RETURNHOME_URL + QuotationConstants.QUOTATION_GRSYMBOL + QuotationConstants.QUOTATION_VIEWLINK_RETURNHOME; 
            sLinkMsg2 = QuotationConstants.QUOTATION_LT_VIEWCR_ENQ_URL + QuotationConstants.QUOTATION_PLACELINKS; 
            sLinkMsg = sLinkMsg1 + QuotationConstants.QUOTATION_BREAK + sLinkMsg2;
            
            request.setAttribute(QuotationConstants.QUOTATION_HEADINGMESSAGE,QuotationConstants.QUOTATION_SUB_SUCES);
            request.setAttribute(QuotationConstants.QUOTATION_SUCCESSMESSAGE,sMsg);
            request.setAttribute(QuotationConstants.QUOTATION_LINKSMESSAGE,sLinkMsg);
            
        } catch (Exception e) {
			/* Logging any exception that raised during this activity in log files using Log4j */
			LoggerUtility.log("DEBUG", this.getClass().getName(), sMethodName, ExceptionUtility.getStackTraceAsString(e));

			/* Setting error details to request to display the same in Exception page */
			request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));

			/* Forwarding request to Error page */
			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
		} finally {
			/* Releasing/closing the connection object */
			if (conn != null)
				oDBUtility.releaseResources(conn);
		}
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);
	}
	
	/**
	 * Method used for Processing the WON Request for New Enquiry.
	 * @param actionMapping
	 * @param actionForm
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward processWonEnquiry(ActionMapping actionMapping,
            ActionForm actionForm, 
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
		
		String sMethodName = "processWonEnquiry"; 
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        UserDto oUserDto = null;
        UserAction oUserAction=null; //Instantiating the UserAction object
        DaoFactory oDaoFactory=null; //Creating an instance of of DaoFactory & retrieving UserDao object
        QuotationDao oQuotationDao=null;// Creating an Instance for QuotationDao
        String sMsg=null,sLinkMsg1=null,sLinkMsg2=null,sLinkMsg=null,sRatingResult=null;
        int iWorkflowExecuteStatus=0;
        //boolean isUpdatedRating=QuotationConstants.QUOTATION_FALSE;
        List<NewRatingDto> alRatingWonDetailsList = new ArrayList<>();
		NewEnquiryForm oNewEnquiryForm = null;
		NewEnquiryDto oNewEnquiryDto = null;
		NewRatingDto oTempNewRatingDto = null;
		WorkflowUtility oWorkflowUtility = null;
		WorkflowDto oWorkflowDto = null;
		boolean isInsertUpdateComplete = false;
		
		try {
			setRolesToRequest(request);
            LoggerUtility.log("INFO", sClassName, sMethodName, "Roles set to request.");
            
    		oNewEnquiryForm = (NewEnquiryForm) actionForm;
    		if (oNewEnquiryForm == null)
    			oNewEnquiryForm = new NewEnquiryForm();
    		
            /* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Instantiating the UserAction object */
            oUserAction = new UserAction();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
            /* Create QuotationDao */
            oQuotationDao = oDaoFactory.getQuotationDao();
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            
            /* Retrieving the logged-in user information */
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            if (oUserDto == null){
            	oUserDto = new UserDto();
            }
            
            oNewEnquiryDto = new NewEnquiryDto();
            oNewEnquiryForm.setCreatedBy(oUserDto.getFullName());           
            oNewEnquiryForm.setCreatedBySSO(oUserDto.getUserId());
            PropertyUtils.copyProperties(oNewEnquiryDto, oNewEnquiryForm);
            
            LoggerUtility.log("INFO", sClassName, sMethodName, "iEnquiryId -  " + oNewEnquiryDto.getEnquiryId());
            
            // Calculate Total WON Price.
            double quotedPricePerUnit = 0d, wonPrice = 0d, wonPricePerUnit = 0d;
            double totalWonPrice = 0d;
            int totalRatingIndex = oNewEnquiryDto.getTotalRatingIndex();
            for (int iRatingIndex = 1; iRatingIndex <= totalRatingIndex; iRatingIndex++) {
            	oTempNewRatingDto = new NewRatingDto();
            	oTempNewRatingDto.setEnquiryId(oNewEnquiryDto.getEnquiryId());
            	oTempNewRatingDto.setRatingNo(CommonUtility.getIntParameter(request, "ratingNo"+iRatingIndex));
            	oTempNewRatingDto.setQuantity(CommonUtility.getIntParameter(request, "quantity"+iRatingIndex));
            	oTempNewRatingDto.setLtQuotedTotalPrice(CommonUtility.getStringParameter(request, "quotedPrice"+iRatingIndex));
            	oTempNewRatingDto.setLtWonTotalPrice(CommonUtility.getStringParameter(request, "wonPrice"+iRatingIndex));
            	wonPrice = Double.parseDouble(CommonUtility.replaceString(oTempNewRatingDto.getLtWonTotalPrice(), ",", ""));
            	wonPricePerUnit = wonPrice / oTempNewRatingDto.getQuantity();
            	totalWonPrice = totalWonPrice + wonPrice;
            	
            	alRatingWonDetailsList.add(oTempNewRatingDto);
            }
            oNewEnquiryDto.setTotalWonPrice(String.valueOf(totalWonPrice));
            
            oDBUtility.setAutoCommitFalse(conn);
            
            // Save the Ratings WON Prices and Enquiry Total Won Price and Won Details.
            sRatingResult = oQuotationDao.saveWonEnquiryDetails(conn, oNewEnquiryDto, alRatingWonDetailsList);
            
            if (QuotationConstants.QUOTATION_STRING_Y.equals(sRatingResult)) {
				oDBUtility.commit(conn);
				isInsertUpdateComplete = true;
			} else {
				isInsertUpdateComplete = false;
				oDBUtility.rollback(conn);
			}
            oDBUtility.setAutoCommitTrue(conn);
            
            // Workflow :  WON
            resetToken(request);
		    LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Initiating Workflow process - - -");
		    oWorkflowUtility = new WorkflowUtility();
            oWorkflowDto = new WorkflowDto();
            oWorkflowDto.setEnquiryId(oNewEnquiryDto.getEnquiryId());
            oWorkflowDto.setEnquiryNo(oNewEnquiryDto.getEnquiryNumber());
            oWorkflowDto.setLoginUserId(oUserDto.getUserId());
            oWorkflowDto.setLoginUserName(oUserDto.getFullName());
            oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
            oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_WON);
            oWorkflowDto.setOldStatusId(oNewEnquiryDto.getStatusId());
            oWorkflowDto.setReturnEnquiry(QuotationConstants.QUOTATION_WON);
            
            LoggerUtility.log("INFO", sClassName, sMethodName, "2 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
            iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
            
            if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE ) {
                LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFS);
            } else  if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ZERO){ 
                LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFSEMAILF);
            } else {
                LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFF);
                
                request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE,QuotationConstants.QUOTATION_ER_INSERT);
        		
        		// Forwarding request to Error page 
        		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
            }
            
            // Build the Success Message
            sMsg = QuotationConstants.QUOTATION_WON_TRACKNO + oNewEnquiryDto.getEnquiryNumber();
            
            sLinkMsg1 = QuotationConstants.QUOTATION_RETURNHOME_URL + QuotationConstants.QUOTATION_GRSYMBOL + QuotationConstants.QUOTATION_VIEWLINK_RETURNHOME; 
            sLinkMsg2 = QuotationConstants.QUOTATION_LT_VIEWCR_ENQ_URL + QuotationConstants.QUOTATION_PLACELINKS; 
            sLinkMsg = sLinkMsg1 + QuotationConstants.QUOTATION_BREAK + sLinkMsg2;
            
            request.setAttribute(QuotationConstants.QUOTATION_HEADINGMESSAGE,QuotationConstants.QUOTATION_SUB_SUCES);
            request.setAttribute(QuotationConstants.QUOTATION_SUCCESSMESSAGE,sMsg);
            request.setAttribute(QuotationConstants.QUOTATION_LINKSMESSAGE,sLinkMsg);
            
		} catch (Exception e) {
			/* Logging any exception that raised during this activity in log files using Log4j */
			LoggerUtility.log("DEBUG", this.getClass().getName(), sMethodName, ExceptionUtility.getStackTraceAsString(e));

			/* Setting error details to request to display the same in Exception page */
			request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));

			/* Forwarding request to Error page */
			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
		} finally {
			/* Releasing/closing the connection object */
			if (conn != null)
				oDBUtility.releaseResources(conn);
		}
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);
	}
	
	/**
	 * This method is used update the requestion information in released to Customer page
	 * @param sEnquiryId		String Object which contains sEnquiryId value
	 * @param sOrderClosingDt	String Object which contains OrderClosingDt value
	 * @param sTargeted			String Object which contains Targeted value 
	 * @param sWinningChance	String Object which contains WinningChance value 
	 * @return 					returns String Object 'YES' if update success
	 * @throws Exception		if any error occurs while performing database operations, etc
	 */
	public String updateNewEnquiryBasicInfo(String sEnquiryId, String sOrderClosingDt, String sTargeted, String sWinningChance, String sNoteBySM ) throws Exception {

   		String sMethodName = "updateNewEnquiryBasicInfo";
   		String sClassName = this.getClass().getName();
   		
   		Connection conn = null;  // Connection object to store the database connection
   		DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
       
   		String sOperationType = null;       
   		DaoFactory oDaoFactory = null;
   		NewEnquiryDto oNewEnquiryDto = null;      
   		QuotationDao oQuotationDao = null;
   		
   		try {
   			/* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            /* Initialize DaoFactory */
            oDaoFactory = new DaoFactory();
            /* Initialize QuotationDao */
            oQuotationDao = oDaoFactory.getQuotationDao();
            
            oNewEnquiryDto =  new NewEnquiryDto();
            
            // Getting request parameters and setting to the oEnquiryDto object.
            if(StringUtils.isNotBlank(sEnquiryId)) {
            	oNewEnquiryDto.setEnquiryId(Integer.parseInt(sEnquiryId));          
            	oNewEnquiryDto.setClosingDate(sOrderClosingDt);
            	oNewEnquiryDto.setWinningChance(sWinningChance);           
            	oNewEnquiryDto.setTargeted(sTargeted);
                oNewEnquiryDto.setSmNote(sNoteBySM);
                
                boolean isNewEnquiryUpdated = oQuotationDao.updateNewEnquiryBasicInfo(conn, oNewEnquiryDto);
                
                if(isNewEnquiryUpdated)
                	return QuotationConstants.QUOTATION_YES;
                else
                	return QuotationConstants.QUOTATION_NO;
                
            } else {
            	return QuotationConstants.QUOTATION_NO;
            }
            
   		} catch(Exception e) {
   			/* Logging any exception that raised during this activity in log files using Log4j */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "home", ExceptionUtility.getStackTraceAsString(e));
            
            return QuotationConstants.QUOTATION_NO;
   		} finally {
   			/* Releasing/closing the connection object */
            if ( conn!=null )
                oDBUtility.releaseResources(conn);
   		}
	}
	
	/**
	 * Method used for Processing Liquidated Damages - Approval Status - for New Enquiry.
	 * @param actionMapping
	 * @param actionForm
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public ActionForward processLDApproval(ActionMapping actionMapping,
            ActionForm actionForm, 
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
		
		String sMethodName = "processLDApproval"; 
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        UserDto oUserDto = null;
        UserAction oUserAction=null; //Instantiating the UserAction object
        DaoFactory oDaoFactory=null; //Creating an instance of of DaoFactory & retrieving UserDao object
        QuotationDao oQuotationDao=null;// Creating an Instance for QuotationDao
        String sMsg=null,sLinkMsg1=null,sLinkMsg2=null,sLinkMsg=null,sResult=null;
        String sOperationType = null;
        int iWorkflowExecuteStatus=0;
        //boolean isUpdatedRating=QuotationConstants.QUOTATION_FALSE;
        List<NewRatingDto> alRatingLostDetailsList = new ArrayList<>();
		NewEnquiryForm oNewEnquiryForm = null;
		NewEnquiryDto oNewEnquiryDto = null;
		NewRatingDto oTempNewRatingDto = null;
		WorkflowUtility oWorkflowUtility = null;
		WorkflowDto oWorkflowDto = null;
		boolean isInsertUpdateComplete = false;
		
		try {
			setRolesToRequest(request);
            LoggerUtility.log("INFO", sClassName, sMethodName, "Roles set to request.");
            
    		oNewEnquiryForm = (NewEnquiryForm) actionForm;
    		if (oNewEnquiryForm == null)
    			oNewEnquiryForm = new NewEnquiryForm();
    		
    		/* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Instantiating the UserAction object */
            oUserAction = new UserAction();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
            /* Create QuotationDao */
            oQuotationDao = oDaoFactory.getQuotationDao();
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            
            /* Retrieving the logged-in user information */
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            if (oUserDto == null){
            	oUserDto = new UserDto();
            }
            
            oNewEnquiryDto = new NewEnquiryDto();
            oNewEnquiryForm.setCreatedBy(oUserDto.getFullName());           
            oNewEnquiryForm.setCreatedBySSO(oUserDto.getUserId());
            PropertyUtils.copyProperties(oNewEnquiryDto, oNewEnquiryForm);
            
            // Update Liquidated Damages value - Based on Approval Status and Enquiry Id.
            sOperationType = CommonUtility.getStringParameter(request, QuotationConstants.QUOTATION_OPERATION_TYPE); 
            //System.out.println("OperationType value = " + sOperationType);
            
            oDBUtility.setAutoCommitFalse(conn);
            
            sResult = oQuotationDao.processLDApproval(conn, oNewEnquiryDto, sOperationType);
            
            if (QuotationConstants.QUOTATION_STRING_Y.equals(sResult)) {
				oDBUtility.commit(conn);
				isInsertUpdateComplete = true;
			} else {
				isInsertUpdateComplete = false;
				oDBUtility.rollback(conn);
			}
            
            oDBUtility.setAutoCommitTrue(conn);
            
            // Workflow :  Liquidated Damages Approve/Reject - SUBMIT.
            resetToken(request);
		    LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Initiating Workflow process - - -");
		    oWorkflowUtility = new WorkflowUtility();
            oWorkflowDto = new WorkflowDto();
            oWorkflowDto.setEnquiryId(oNewEnquiryDto.getEnquiryId());
            oWorkflowDto.setEnquiryNo(oNewEnquiryDto.getEnquiryNumber());
            oWorkflowDto.setLoginUserId(oUserDto.getUserId());
            oWorkflowDto.setLoginUserName(oUserDto.getFullName());
            oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
            oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTOSM);
            oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGBH);
            
            LoggerUtility.log("INFO", sClassName, sMethodName, "2 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
            iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
            
            if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE ) {
                LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFS);
            } else  if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ZERO){ 
                LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFSEMAILF);
            } else {
                LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFF);
                
                request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE,QuotationConstants.QUOTATION_ER_INSERT);
        		
        		// Forwarding request to Error page 
        		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
            }
            
            // Build the Success Message
            sMsg = QuotationConstants.QUOTATION_LD_TRACKNO + oNewEnquiryDto.getEnquiryNumber();
            
            sLinkMsg1 = QuotationConstants.QUOTATION_RETURNHOME_URL + QuotationConstants.QUOTATION_GRSYMBOL + QuotationConstants.QUOTATION_VIEWLINK_RETURNHOME; 
            //sLinkMsg2 = QuotationConstants.QUOTATION_LT_VIEWCR_ENQ_URL + QuotationConstants.QUOTATION_PLACELINKS; 
            sLinkMsg = sLinkMsg1 + QuotationConstants.QUOTATION_BREAK;
            
            request.setAttribute(QuotationConstants.QUOTATION_HEADINGMESSAGE,QuotationConstants.QUOTATION_SUB_SUCES);
            request.setAttribute(QuotationConstants.QUOTATION_SUCCESSMESSAGE,sMsg);
            request.setAttribute(QuotationConstants.QUOTATION_LINKSMESSAGE,sLinkMsg);
            
		} catch (Exception e) {
			/* Logging any exception that raised during this activity in log files using Log4j */
			LoggerUtility.log("DEBUG", this.getClass().getName(), sMethodName, ExceptionUtility.getStackTraceAsString(e));

			/* Setting error details to request to display the same in Exception page */
			request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));

			/* Forwarding request to Error page */
			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
		} finally {
			/* Releasing/closing the connection object */
			if (conn != null)
				oDBUtility.releaseResources(conn);
		}
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);
	}
	
	public ActionForward processMfgPlantDetails(ActionMapping actionMapping,
            ActionForm actionForm, 
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
	
		String sMethodName = "processLDApproval"; 
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        UserDto oUserDto = null;
        UserAction oUserAction=null; //Instantiating the UserAction object
        DaoFactory oDaoFactory=null; //Creating an instance of of DaoFactory & retrieving UserDao object
        QuotationDao oQuotationDao=null;// Creating an Instance for QuotationDao
        String sMsg=null,sLinkMsg1=null,sLinkMsg2=null,sLinkMsg=null,sResult=null;
        String sOperationType = null;
        int iWorkflowExecuteStatus=0;
        //boolean isUpdatedRating=QuotationConstants.QUOTATION_FALSE;
        List<NewRatingDto> alRatingLostDetailsList = new ArrayList<>();
		NewEnquiryForm oNewEnquiryForm = null;
		NewEnquiryDto oNewEnquiryDto = null;
		NewRatingDto oTempNewRatingDto = null;
		NewEnquiryUtilityManager oNewEnquiryManager = null;
		WorkflowUtility oWorkflowUtility = null;
		WorkflowDto oWorkflowDto = null;
		boolean isInsertUpdateComplete = false;
		
		try {
			setRolesToRequest(request);
            LoggerUtility.log("INFO", sClassName, sMethodName, "Roles set to request.");
            
    		oNewEnquiryForm = (NewEnquiryForm) actionForm;
    		if (oNewEnquiryForm == null)
    			oNewEnquiryForm = new NewEnquiryForm();
    		
    		/* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Instantiating the UserAction object */
            oUserAction = new UserAction();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
            /* Create QuotationDao */
            oQuotationDao = oDaoFactory.getQuotationDao();
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            
            /* Retrieving the logged-in user information */
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            if (oUserDto == null){
            	oUserDto = new UserDto();
            }
            
            oNewEnquiryManager = new NewEnquiryUtilityManager();
            oNewEnquiryDto = new NewEnquiryDto();
            oNewEnquiryForm.setCreatedBy(oUserDto.getFullName());           
            oNewEnquiryForm.setCreatedBySSO(oUserDto.getUserId());
            PropertyUtils.copyProperties(oNewEnquiryDto, oNewEnquiryForm);
            
            oDBUtility.setAutoCommitFalse(conn);
            
            sResult = oQuotationDao.updateMfgPlantDetails(conn, oNewEnquiryDto);
            
            if (QuotationConstants.QUOTATION_STRING_Y.equals(sResult)) {
				oDBUtility.commit(conn);
				isInsertUpdateComplete = true;
			} else {
				isInsertUpdateComplete = false;
				oDBUtility.rollback(conn);
			}
            
            // Fetch Rating Details from NewEnquiryForm Request : Build Ratings List.
    		ArrayList<NewRatingDto> alRatingsList = oNewEnquiryManager.getRatingsList(conn, oQuotationDao, oNewEnquiryDto, request);
    		for(NewRatingDto oNewRatingDto : alRatingsList) {
    			// Save the NewRatingDto - Mfg Details.
    			sResult = oQuotationDao.saveMfgPlantRatingDetails(conn, oNewRatingDto);
    			if (QuotationConstants.QUOTATION_STRING_Y.equals(sResult)) {
    				oDBUtility.commit(conn);
    			} else {
    				oDBUtility.rollback(conn);
    			}
    		}
    		
    		oDBUtility.setAutoCommitTrue(conn);
    		
            // Workflow :  Update Mfg. Plant Details - SUBMIT.
            resetToken(request);
		    LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Initiating Workflow process - - -");
		    oWorkflowUtility = new WorkflowUtility();
            oWorkflowDto = new WorkflowDto();
            oWorkflowDto.setEnquiryId(oNewEnquiryDto.getEnquiryId());
            oWorkflowDto.setEnquiryNo(oNewEnquiryDto.getEnquiryNumber());
            oWorkflowDto.setLoginUserId(oUserDto.getUserId());
            oWorkflowDto.setLoginUserName(oUserDto.getFullName());
            oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
            oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTOSM);
            oWorkflowDto.setOldStatusId(QuotationConstants.QUOTATION_WORKFLOW_STATUS_PENDINGMFG);
            
            LoggerUtility.log("INFO", sClassName, sMethodName, "2 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
            iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
            
            if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE ) {
                LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFS);
            } else  if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ZERO){ 
                LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFSEMAILF);
            } else {
                LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFF);
                
                request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE,QuotationConstants.QUOTATION_ER_INSERT);
        		
        		// Forwarding request to Error page 
        		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
            }
            
            // Build the Success Message
            sMsg = QuotationConstants.QUOTATION_TRACKNO + oNewEnquiryDto.getEnquiryNumber();
            
            sLinkMsg1 = QuotationConstants.QUOTATION_RETURNHOME_URL + QuotationConstants.QUOTATION_GRSYMBOL + QuotationConstants.QUOTATION_VIEWLINK_RETURNHOME; 
            //sLinkMsg2 = QuotationConstants.QUOTATION_LT_VIEWCR_ENQ_URL + QuotationConstants.QUOTATION_PLACELINKS; 
            sLinkMsg = sLinkMsg1 + QuotationConstants.QUOTATION_BREAK;
            
            request.setAttribute(QuotationConstants.QUOTATION_HEADINGMESSAGE,QuotationConstants.QUOTATION_SUB_SUCES);
            request.setAttribute(QuotationConstants.QUOTATION_SUCCESSMESSAGE,sMsg);
            request.setAttribute(QuotationConstants.QUOTATION_LINKSMESSAGE,sLinkMsg);
            
		} catch (Exception e) {
			/* Logging any exception that raised during this activity in log files using Log4j */
			LoggerUtility.log("DEBUG", this.getClass().getName(), sMethodName, ExceptionUtility.getStackTraceAsString(e));

			/* Setting error details to request to display the same in Exception page */
			request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));

			/* Forwarding request to Error page */
			return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
		} finally {
			/* Releasing/closing the connection object */
			if (conn != null)
				oDBUtility.releaseResources(conn);
		}
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);
	}
	
	/**
	 * Method to Re-assign the Sales Enquiry to Appropriate User as per the User Type(QA, Sourcing, Mfg. Plant).
	 * @param actionMapping	ActionMapping object to redirect the user to next page depending on the activity
     * @param actionForm	ActionForm object to handle the form elements
     * @param request		HttpServletRequest object to handle request operations
     * @param response		HttpServletResponse object to handle response operations
     * @return returns 		ActionForward depending on which user is redirected to next page
     * @throws Exception	if any error occurs while performing database operations, etc
	 */
	public ActionForward reassignSalesEnquiry(ActionMapping actionMapping,
            ActionForm actionForm, 
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
		
		String sMethodName = "reassignSalesEnquiry"; 
    	String sClassName = this.getClass().getName();
    	LoggerUtility.log("INFO", sClassName, sMethodName, "START");
    	
    	Connection conn = null;  			// Connection object to store the database connection
        DBUtility oDBUtility = null;  		// Object of DBUtility class to handle DB connection objects
        
        DaoFactory oDaoFactory = null; 		// Creating an instance of of DaoFactory & retrieving UserDao object
        QuotationDao oQuotationDao = null;	// Creating an Instance for QuotationDao
        MTOQuotationDao oMTOQuotationDao = null;	// Creating an Instance for MTOQuotationDao
        UserDao oUserDao = null;			// Creating an Instance for UserDao
        UserAction oUserAction = null; 		//Instantiating the UserAction object
        NewEnquiryForm oNewEnquiryForm = null;
        NewEnquiryDto oNewEnquiryDto = null;
        UserDto oUserDto = null;
        UserDto oReassignUserDto = null;
        UserDetailsDto oUserDetailsDto = null;
        WorkflowUtility oWorkflowUtility = null;
		WorkflowDto oWorkflowDto = null;
		int iWorkflowExecuteStatus=0;
		String sReassignToId = "";
		String sMsg=null,sLinkMsg1=null,sLinkMsg2=null,sLinkMsg=null;
		
		try {
			/* Setting user's information in request, using the header information received through request */
            setRolesToRequest(request);
            LoggerUtility.log("INFO", this.getClass().getName(), sMethodName, "Roles set to request.");
            
            oNewEnquiryForm = (NewEnquiryForm) actionForm;
    		if (oNewEnquiryForm == null)
    			oNewEnquiryForm = new NewEnquiryForm();
    		
    		/* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
            /* Create QuotationDao */
            oQuotationDao = oDaoFactory.getQuotationDao();
            /* Create MTOQuotationDao */
            oMTOQuotationDao = oDaoFactory.getMTOQuotationDao();
            /* Instantiating the UserAction object */
            oUserAction = new UserAction();
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            
            oNewEnquiryDto = new NewEnquiryDto();
        	PropertyUtils.copyProperties(oNewEnquiryDto, oNewEnquiryForm);
        	
        	sReassignToId = CommonUtility.getStringParameter(request, QuotationConstants.QUOTATIONLT_REASSIGNTO_ID);
        	
        	/* Retrieving the logged-in user information */
            oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            if (oUserDto == null){
            	oUserDto = new UserDto();
            }
            
            /* Retrieving User information - Based on Re-assign To User Id. */
        	oUserDao = oDaoFactory.getUserDao();
        	oReassignUserDto = oUserDao.getUserInfo(conn, sReassignToId);
            if (oReassignUserDto == null){
            	oReassignUserDto = new UserDto();
            }
            oUserDetailsDto = oQuotationDao.getUserDetailsById(conn, sReassignToId);
            
            // Save the Re-assign Comments for the Enquiry. (Add to Existing Comments.)
            oNewEnquiryDto = oQuotationDao.getNewEnquiryDetailsForView(conn, oNewEnquiryDto, QuotationConstants.QUOTATION_EDIT.toUpperCase());
            oNewEnquiryDto.setReassignRemarks(CommonUtility.getStringParameter(request, "reassignRemarks"));
            oMTOQuotationDao.saveReassignComments(conn, oNewEnquiryDto);
            
            if(StringUtils.isNotBlank(sReassignToId)) {
            	
            	// Workflow :  Re-assign To Flow : Below Rules to be Executed.
        		// If Re-assign To User = QA : Change Status to "Pending - Quality Approval".
        		// If Re-assign To User = SCM : Change Status to "Pending - Sourcing".
            	// If Re-assign To User = MFG : Change Status to "Pending - Mfg. Plant Details".
        		resetToken(request);
    		    LoggerUtility.log("INFO", sClassName, sMethodName, "- - - Initiating Workflow process - - -");
    		    oWorkflowUtility = new WorkflowUtility();
                oWorkflowDto = new WorkflowDto();
                oWorkflowDto.setEnquiryId(oNewEnquiryDto.getEnquiryId());
                oWorkflowDto.setEnquiryNo(oNewEnquiryDto.getEnquiryNumber());
                oWorkflowDto.setLoginUserId(oUserDto.getUserId());
                oWorkflowDto.setLoginUserName(oUserDto.getFullName());
                oWorkflowDto.setLoginUserEmail(oUserDto.getEmailId());
                
                // For Re-assign Workflow : Set "sReassignToId" as the WorkflowDto Assigned To value.
                oWorkflowDto.setAssignedTo(sReassignToId);
                
                if(QuotationConstants.QUOTATIONLT_REASSIGNTO_QA.equalsIgnoreCase(oUserDetailsDto.getUserDetailsType()) ) {
                	oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTOQA);
                } else if(QuotationConstants.QUOTATIONLT_REASSIGNTO_SCM.equalsIgnoreCase(oUserDetailsDto.getUserDetailsType()) ) {
                	oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTOSCM);
                } else if(QuotationConstants.QUOTATIONLT_REASSIGNTO_MFG.equalsIgnoreCase(oUserDetailsDto.getUserDetailsType()) ) {
                	oWorkflowDto.setActionCode(QuotationConstants.QUOTATION_WORKFLOW_ACTION_SUBMITTOMFG);
                }
                oWorkflowDto.setOldStatusId(oNewEnquiryDto.getStatusId()); 
                
                LoggerUtility.log("INFO", sClassName, sMethodName, "2 - Enquiry No. = " + oWorkflowDto.getEnquiryNo());
        		iWorkflowExecuteStatus = oWorkflowUtility.executeWorkflow(conn, request, oWorkflowDto);
        		
        		if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ONE ) {
                    LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFS);
                } else  if ( iWorkflowExecuteStatus==QuotationConstants.QUOTATION_LITERAL_ZERO){ 
                    LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFSEMAILF);
                } else {
                    LoggerUtility.log("INFO", sClassName, sMethodName, QuotationConstants.QUOTATION_WFF);
                    
                    request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE,QuotationConstants.QUOTATION_ER_INSERT);
                    // Forwarding request to Error page 
            		return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
                }
        		
        		// Build the Success Message
        		sMsg = QuotationConstants.QUOTATION_REASSIGN_TRACKNO1 + oReassignUserDto.getFullName() + QuotationConstants.QUOTATION_REASSIGN_TRACKNO2 + oNewEnquiryDto.getEnquiryNumber();
                
                sLinkMsg1 = QuotationConstants.QUOTATION_RETURNHOME_URL + QuotationConstants.QUOTATION_GRSYMBOL + QuotationConstants.QUOTATION_VIEWLINK_RETURNHOME; 
                sLinkMsg2 = QuotationConstants.QUOTATION_LT_VIEWCR_ENQ_URL + QuotationConstants.QUOTATION_PLACELINKS; 
                sLinkMsg = sLinkMsg1 + QuotationConstants.QUOTATION_BREAK; 
                
                request.setAttribute(QuotationConstants.QUOTATION_HEADINGMESSAGE,QuotationConstants.QUOTATION_SUB_SUCES);
                request.setAttribute(QuotationConstants.QUOTATION_SUCCESSMESSAGE,sMsg);
                request.setAttribute(QuotationConstants.QUOTATION_LINKSMESSAGE,sLinkMsg);
                
            }
		} catch(Exception e) {
        	//e.printStackTrace();
            /* Logging any exception that raised during this activity in log files using Log4j */
            LoggerUtility.log("DEBUG", this.getClass().getName(), sMethodName, ExceptionUtility.getStackTraceAsString(e));
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        } finally {
            /* Releasing/closing the connection object */
            if ( conn!=null )
                oDBUtility.releaseResources(conn);
        }
    	
		LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        
    	return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_CONFIRM);
	}
	
	public ActionForward copyEnquiry(ActionMapping actionMapping,
            ActionForm actionForm, 
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
		
		String sMethodName = QuotationConstants.QUOTATION_COPYINQ;
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
    	Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        
        EnquiryDto oEnquiryDto = null;
        NewEnquiryDto oNewEnquiryDto = null;
        UserDto oUserDto = null;
        UserAction oUserAction =null; //Instantiating the UserAction object
        DaoFactory oDaoFactory = null; //Creating an instance of of DaoFactory & retrieving UserDao object
        QuotationDao oQuotationDao =null;
        
        NewEnquiryForm oNewEnquiryForm = (NewEnquiryForm)actionForm;
		if (oNewEnquiryForm == null){
			oNewEnquiryForm = new NewEnquiryForm();
		}
		
		try {
			if(isTokenValid(request)) {
				resetToken(request);
        		
        		/* Setting user's information in request, using the header information received through request */
                setRolesToRequest(request);
                LoggerUtility.log("INFO", sClassName, sMethodName, "Roles set to request.");
                
                /* Instantiating the DBUtility object */
                oDBUtility = new DBUtility();
                /* Instantiating the UserAction object */
                oUserAction = new UserAction();
                /* Creating an instance of of DaoFactory & retrieving UserDao object */
                oDaoFactory = new DaoFactory();
                /* Retrieving the database connection using DBUtility.getDBConnection method */
                conn = oDBUtility.getDBConnection();
                /* Retrieving the logged-in user information */
                oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
                
                oQuotationDao = oDaoFactory.getQuotationDao();
                oNewEnquiryDto = new NewEnquiryDto();
                PropertyUtils.copyProperties(oNewEnquiryDto, oNewEnquiryForm);
                if(StringUtils.isNotBlank(CommonUtility.getStringParameter(request, "enquiryId"))) {
                	oNewEnquiryDto.setEnquiryId(Integer.parseInt(CommonUtility.getStringParameter(request, "enquiryId")));
                }
                
                oNewEnquiryDto = oQuotationDao.copyNewEnquiry(conn, oNewEnquiryDto, oUserDto);
                
                request.setAttribute(QuotationConstants.QUOTATION_ENQUIRYDTO, oNewEnquiryDto); 
                request.setAttribute(QuotationConstants.QUOTATION_USERDTO, oUserDto); 
			}
			
		} catch(Exception e) {
            /*
             * Logging any exception that raised during this activity in log files using Log4j
             */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "home", ExceptionUtility.getStackTraceAsString(e));
            
            /* Setting error details to request to display the same in Exception page */
            request.setAttribute(QuotationConstants.QUOTATION_ERRORMESSAGE, ExceptionUtility.getStackTraceAsString(e));
            
            /* Forwarding request to Error page */
            return actionMapping.findForward(QuotationConstants.QUOTATION_FORWARD_FAILURE);
        } finally {
            /* Releasing/closing the connection object */
            if ( conn!=null )
                oDBUtility.releaseResources(conn);
        }
		
		LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        return new ActionForward(QuotationConstants.QUOTATION_GET_NEWENQDETCOPY_URL + oNewEnquiryDto.getEnquiryId()); 
	}
	
	private boolean checkEnquiryMCApplied(NewEnquiryDto oNewEnquiryDto) throws Exception {
		String sMethodName = QuotationConstants.QUOTATION_COPYINQ;
        String sClassName = this.getClass().getName();
		LoggerUtility.log("INFO", sClassName, sMethodName, "END");
		
		Connection conn = null;  // Connection object to store the database connection
        DBUtility oDBUtility = null;  // Object of DBUtility class to handle DB connection objects
        
        EnquiryDto oEnquiryDto = null;
        //UserDto oUserDto = null;
        //UserAction oUserAction =null; //Instantiating the UserAction object
        DaoFactory oDaoFactory = null; //Creating an instance of of DaoFactory & retrieving UserDao object
        QuotationDao oQuotationDao =null;
        
        String sResult = QuotationConstants.QUOTATION_STRING_N;
		boolean isMCAppliedForEnquiry = false;
		
		try {
			/* Instantiating the DBUtility object */
            oDBUtility = new DBUtility();
            /* Instantiating the UserAction object */
            //oUserAction = new UserAction();
            /* Creating an instance of of DaoFactory & retrieving UserDao object */
            oDaoFactory = new DaoFactory();
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
            /* Retrieving the logged-in user information */
            //oUserDto = oUserAction.getUserInfoForLoginUser(request, conn);
            
            oQuotationDao = oDaoFactory.getQuotationDao();
            
            sResult = oQuotationDao.checkEnquiryMCApplied(conn, oNewEnquiryDto);
            
            if(QuotationConstants.QUOTATION_STRING_Y.equalsIgnoreCase(sResult)) {
            	isMCAppliedForEnquiry = true;
            }
            
		} catch(Exception e) {
			/*
             * Logging any exception that raised during this activity in log files using Log4j
             */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "home", ExceptionUtility.getStackTraceAsString(e));
            
            return isMCAppliedForEnquiry;
        } finally {
            /* Releasing/closing the connection object */
            if ( conn!=null )
                oDBUtility.releaseResources(conn);
        }
		
		LoggerUtility.log("INFO", sClassName, sMethodName, "END");
		return isMCAppliedForEnquiry;
	}
	
}