package in.com.rbc.quotation.enquiry.utility;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.struts.upload.FormFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import in.com.rbc.quotation.admin.dao.AddOnDetailsDao;
import in.com.rbc.quotation.admin.dao.CustomerDiscountDao;
import in.com.rbc.quotation.admin.dao.ListPriseDao;
import in.com.rbc.quotation.admin.dto.AddOnDetailsDto;
import in.com.rbc.quotation.admin.dto.CustomerDiscountDto;
import in.com.rbc.quotation.admin.dto.ListPriceDto;
import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.dto.AttachmentDto;
import in.com.rbc.quotation.common.utility.AttachmentUtility;
import in.com.rbc.quotation.common.utility.CommonUtility;
import in.com.rbc.quotation.common.utility.CurrencyConvertUtility;
import in.com.rbc.quotation.common.utility.DBUtility;
import in.com.rbc.quotation.common.utility.ExceptionUtility;
import in.com.rbc.quotation.common.utility.LoggerUtility;
import in.com.rbc.quotation.common.utility.PropertyUtility;
import in.com.rbc.quotation.enquiry.dao.QuotationDao;
import in.com.rbc.quotation.enquiry.dto.NewEnquiryDto;
import in.com.rbc.quotation.enquiry.dto.NewMTOAddOnDto;
import in.com.rbc.quotation.enquiry.dto.NewMTORatingDto;
import in.com.rbc.quotation.enquiry.dto.NewRatingDto;
import in.com.rbc.quotation.enquiry.form.NewEnquiryForm;
import in.com.rbc.quotation.enquiry.form.NewMTOEnquiryForm;
import in.com.rbc.quotation.factory.DaoFactory;
import in.com.rbc.quotation.user.dto.UserDto;

public class NewEnquiryUtilityManager {

	/**
	 * This method is used to Load Lookup Lists for NewEnquiryForm.
	 * @param conn Connection object to fetch the Required Enquiry Details.
	 * @param oNewEnquiryForm NewEnquiryForm Bean contains required information.
	 * @return NewEnquiryForm Bean with the Loaded Picklists.
	 * @throws Exception if any error occurs while performing database operations, etc
	 */
	public NewEnquiryForm loadLookUpValues(Connection conn, NewEnquiryForm oNewEnquiryForm) throws Exception {
    	
    	/* Creating an instance of of DaoFactory & retrieving UserDao object */
        DaoFactory oDaoFactory = new DaoFactory();
        QuotationDao oQuotationDao = oDaoFactory.getQuotationDao();
		
		oNewEnquiryForm.setHtltList(oQuotationDao.getOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_HTLT));
		
		// Loading Commercial Terms and Conditions related Lists
		oNewEnquiryForm.setTcDeliveryTypeList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_DELIVERYTYPE));
		oNewEnquiryForm.setTcPaymentTermsList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_PAYMENTTERMS));
		oNewEnquiryForm.setTcPercentNetOrderValueList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_PERCENT_NETORDERVALUE));
		oNewEnquiryForm.setTcPaymentDaysList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_PAYMENTDAYS));
		oNewEnquiryForm.setTcPayableTermsList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_PAYABLETERMS));
		oNewEnquiryForm.setTcInstrumentList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_INSTRUMENT));
		oNewEnquiryForm.setTcWarrantyDaysList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_WARRANTYMONTHS));
		oNewEnquiryForm.setTcOrderCompleteLeadTimeList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_ORDERCOMPLETELEADTIME));
		oNewEnquiryForm.setTcGstValuesList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_GST));
		oNewEnquiryForm.setTcPackagingList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_PACKAGING));
		oNewEnquiryForm.setTcDeliveryLotList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_DELIVERY_LOT));
		oNewEnquiryForm.setTcDeliveryDamagesList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_DELIVERY_DAMAGES));
		oNewEnquiryForm.setTcOfferValidityList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_OFFERVALIDITY));
		oNewEnquiryForm.setTcPriceValidityList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_PRICEVALIDITY));
		
		// Loading Ratings related Lists
		// Top Fields Lists - Mandatory
		oNewEnquiryForm.setLtProductGroupList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LT_PRODGROUP));
		oNewEnquiryForm.setLtProductLineList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LT_PRODLINE));
		oNewEnquiryForm.setLtMotorTypeList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_TYPE));
		oNewEnquiryForm.setLtKWList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LT_KW));
		oNewEnquiryForm.setLtPoleList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LT_POLE));
		oNewEnquiryForm.setLtFrameList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LT_FRAME));
		oNewEnquiryForm.setLtFrameSuffixList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_FRAME_SUFFIX));
		oNewEnquiryForm.setLtMountingList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LT_MOUNTING));
		oNewEnquiryForm.setLtTBPositionList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LT_TBPOS));
		
		// Basic Fields Lists.
		oNewEnquiryForm.setLtMfgLocationList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_MFG_LOCATION));
		oNewEnquiryForm.setLtVoltageList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LT_VOLT));
		oNewEnquiryForm.setLtVoltVariationList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_VOLT_VARIATION));
		oNewEnquiryForm.setLtFrequencyList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LT_FREQ));
		oNewEnquiryForm.setLtFreqVariationList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_FREQ_VARIATION));
		oNewEnquiryForm.setLtCombinedVariationList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LT_COMBVAR));
		oNewEnquiryForm.setLtServiceFactorList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_SERVICE_FACTOR));
		oNewEnquiryForm.setLtMethodOfStartingList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_METHOD_OF_STARTING));
		oNewEnquiryForm.setLtStartingDOLCurrentList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_START_DOL_CURRENT));
		oNewEnquiryForm.setLtAmbientTemperatureList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LT_AMBTEMP));
		oNewEnquiryForm.setLtAmbientRemTempList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_AMBREMTEMP));
		oNewEnquiryForm.setLtTemperatureRiseList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_TEMP_RISE));
		oNewEnquiryForm.setLtInsulationClassList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_INSULATION_CLASS));
		oNewEnquiryForm.setLtForcedCoolingList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_FORCED_COOL));
		oNewEnquiryForm.setLtHazAreaTypeList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_HAZARD_AREA_TYPE));
		oNewEnquiryForm.setLtHazAreaList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_HAZARD_AREA));
		oNewEnquiryForm.setLtGasGroupList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LT_GASGROUP));
		oNewEnquiryForm.setLtHazardZoneList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_HAZARD_ZONE));
		oNewEnquiryForm.setLtDustGroupList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_DUST_GROUP));
		oNewEnquiryForm.setLtApplicationList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LT_APPL));
		oNewEnquiryForm.setLtDutyList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LT_DUTY));
		oNewEnquiryForm.setLtCDFList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LT_CDF));
		oNewEnquiryForm.setLtStartsList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LT_STARTS));
		oNewEnquiryForm.setLtNoiseLevelList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_NOISE_LEVEL));
		
		// Electrical Fields Lists.
		oNewEnquiryForm.setLtRVRAList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_RVRA));
		oNewEnquiryForm.setLtWindingTreatmentList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_WINDING_TREATMENT));
		oNewEnquiryForm.setLtWindingWireList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_WINDING_WIRE));
		oNewEnquiryForm.setLtWindingConfigurationList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_WINDING_CONFIG));
		oNewEnquiryForm.setLtLeadList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_LEAD));
		oNewEnquiryForm.setLtVFDTypeList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_VFD_TYPE));
		oNewEnquiryForm.setLtVFDSpeedRangeMinList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_VFD_SPEEDRANGE_MIN));
		oNewEnquiryForm.setLtVFDSpeedRangeMaxList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_VFD_SPEEDRANGE_MAX));
		oNewEnquiryForm.setLtOverloadingDutyList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_OVERLOADING_DUTY));
		oNewEnquiryForm.setLtConstantEffRangeList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_CONSTANT_EFF_RANGE));
		
		// Mechanical Fields Lists
		oNewEnquiryForm.setLtShaftTypeList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_SHAFT_TYPE));
		oNewEnquiryForm.setLtShaftMaterialTypeList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_SHAFT_MATERIAL));
		oNewEnquiryForm.setLtDirectionOfRotationList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_DIR_OF_ROTATION));
		oNewEnquiryForm.setLtStandardRotationList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_STANDARD_ROTATION));
		oNewEnquiryForm.setLtMethodOfCouplingList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_METHOD_OF_COUPLING));
		oNewEnquiryForm.setLtPaintingTypeList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_PAINT_TYPE));
		oNewEnquiryForm.setLtPaintShadeList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_PAINT_SHADE));
		oNewEnquiryForm.setLtPaintThicknessList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_PAINT_THICKNESS));
		oNewEnquiryForm.setLtIPList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_IP));
		oNewEnquiryForm.setLtCableSizeList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_CABLE_SIZE));
		oNewEnquiryForm.setLtNoOfRunsList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_NO_OF_RUNS));
		oNewEnquiryForm.setLtNoOfCoresList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_NO_OF_CORES));
		oNewEnquiryForm.setLtCrossSectionAreaList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_CROSSSECTION_AREA));
		oNewEnquiryForm.setLtConductorMaterialList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_CONDUCTOR_MATERIAL));
		oNewEnquiryForm.setLtTerminalBoxList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_TERMINAL_BOX));
		oNewEnquiryForm.setLtSpreaderBoxList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_SPREADER_BOX));
		oNewEnquiryForm.setLtSpaceHeaterList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_SPACE_HEATER));
		oNewEnquiryForm.setLtVibrationList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_VIBRATION));
		oNewEnquiryForm.setLtFlyingLeadList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_FLYING_LEAD));
		oNewEnquiryForm.setLtMetalFanList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_METAL_FAN));
		oNewEnquiryForm.setLtTechoMountingList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_TECHO_MOUNTING));
		
		// Accessories Fields Lists
		oNewEnquiryForm.setLtHardwareList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_HARDWARE));
		oNewEnquiryForm.setLtGlandPlateList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_GLAND_PLATE));
		oNewEnquiryForm.setLtDoubleCompressionGlandList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_DOUBLE_COMPRESS_GLAND));
		oNewEnquiryForm.setLtSPMMountingProvisionList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_SPM_MOUNT_PROVISION));
		oNewEnquiryForm.setLtAddNamePlateList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_ADD_NAME_PLATE));
		oNewEnquiryForm.setLtArrowPlateDirectionList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_ARROW_PLATE_DIRECTION));
		oNewEnquiryForm.setLtRTDList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_RTD));
		oNewEnquiryForm.setLtBTDList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_BTD));
		oNewEnquiryForm.setLtThermisterList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_THERMISTER));
		oNewEnquiryForm.setLtAuxTerminalBoxList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_AUX_TERMINAL_BOX));
		
		// Bearings Fields Lists
		oNewEnquiryForm.setLtBearingSystemList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_BEARING_SYSTEM));
		oNewEnquiryForm.setLtBearingNDEList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_BEARING_NDE));
		oNewEnquiryForm.setLtBearingDEList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_BEARING_DE));
		
		// Certificates Spares Tests Lists
		oNewEnquiryForm.setLtWitnessRoutineList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_WITNESS_ROUTINE));
		oNewEnquiryForm.setLtAdditionalTestList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_ADDITIONAL_TEST));
		oNewEnquiryForm.setLtTypeTestList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_TYPE_TEST));
		oNewEnquiryForm.setLtULCEList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_ULCE));
		oNewEnquiryForm.setLtQAPList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_QAP));
		oNewEnquiryForm.setLtSeaworthyPackingList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_SEAWORTHY_PACKING));
		oNewEnquiryForm.setLtWarrantyList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_WARRANTY));
		oNewEnquiryForm.setLtSparesList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_SPARES));
		oNewEnquiryForm.setLtDataSheetList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_DATA_SHEET));
		oNewEnquiryForm.setLtPerfCurvesList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_PERF_CURVES));
		oNewEnquiryForm.setLtMotorGAList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_MOTOR_GA));
		oNewEnquiryForm.setLtTBoxGAList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_TBOX_GA));
		
		oNewEnquiryForm.setLtNonStandardVoltageList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_NON_STD_VOLTAGE));
		oNewEnquiryForm.setLtTemperatureClassList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_TEMPERATURE_CLASS));
		oNewEnquiryForm.setLtEfficiencyClassList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_EFFICIENCY_CLASS));
		oNewEnquiryForm.setLtTypeOfGreaseList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_TYPE_OF_GREASE));
		oNewEnquiryForm.setLtDualSpeedTypeList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_DUAL_SPEED_TYPE));
		
		return oNewEnquiryForm;
    }

	/**
	 * This method is used to Load Lookup Lists for Offer Page.
	 * @param conn Connection object to fetch the Required Enquiry Details.
	 * @param oNewEnquiryForm NewEnquiryForm Bean contains required information.
	 * @return NewEnquiryForm Bean with the Loaded Picklists.
	 * @throws Exception if any error occurs while performing database operations, etc
	 */
	public NewMTOEnquiryForm loadOfferPageLookUpValues(Connection conn, NewMTOEnquiryForm oNewMTOEnquiryForm) throws Exception {
    	
    	/* Creating an instance of of DaoFactory & retrieving UserDao object */
        DaoFactory oDaoFactory = new DaoFactory();
        QuotationDao oQuotationDao = oDaoFactory.getQuotationDao();
		
        oNewMTOEnquiryForm.setAlVoltList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_VOLTS));
        oNewMTOEnquiryForm.setAlMainTermBoxPSList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_MTBPS));
        oNewMTOEnquiryForm.setAlNeutralTBList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_NEUTRALTERMBOX));
        oNewMTOEnquiryForm.setAlCableEntryList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_CABLEENTRY));
        oNewMTOEnquiryForm.setAlBoTerminalsNoList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_BOTERMINALSNO));
        oNewMTOEnquiryForm.setAlLubricationList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LUB));
        oNewMTOEnquiryForm.setAlnoiseLevelList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_NOISEL));
        oNewMTOEnquiryForm.setAlMinStartVoltList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_MINSTVOLT));
        oNewMTOEnquiryForm.setAlBalancingList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_BAL));
        oNewMTOEnquiryForm.setAlBearingThermoDtList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_BEARME));
        oNewMTOEnquiryForm.setDegproList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_DEGPRO));
        oNewMTOEnquiryForm.setTermBoxList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_TERMBOX));
        oNewMTOEnquiryForm.setEnclosureList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_ENCLOSURE));
		
        // Top Fields Lists - Mandatory.
        oNewMTOEnquiryForm.setLtProductLineList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LT_PRODLINE));
        oNewMTOEnquiryForm.setLtKWList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LT_KW));
        oNewMTOEnquiryForm.setLtPoleList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LT_POLE));
        oNewMTOEnquiryForm.setLtFrameList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LT_FRAME));
        oNewMTOEnquiryForm.setLtFrameSuffixList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_FRAME_SUFFIX));
        oNewMTOEnquiryForm.setLtMountingList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LT_MOUNTING));
        oNewMTOEnquiryForm.setLtTBPositionList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LT_TBPOS));
     	oNewMTOEnquiryForm.setLtMfgLocationList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_MFG_LOCATION));
     	
     	// Basic Fields Lists.
     	oNewMTOEnquiryForm.setLtVoltageList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LT_VOLT));
     	oNewMTOEnquiryForm.setLtVoltVariationList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_VOLT_VARIATION));
     	oNewMTOEnquiryForm.setLtFrequencyList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LT_FREQ));
     	oNewMTOEnquiryForm.setLtFreqVariationList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_FREQ_VARIATION));
     	oNewMTOEnquiryForm.setLtCombinedVariationList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LT_COMBVAR));
     	oNewMTOEnquiryForm.setLtServiceFactorList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_SERVICE_FACTOR));
     	oNewMTOEnquiryForm.setLtMethodOfStartingList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_METHOD_OF_STARTING));
     	oNewMTOEnquiryForm.setLtStartingDOLCurrentList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_START_DOL_CURRENT));
     	oNewMTOEnquiryForm.setLtAmbientTemperatureList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LT_AMBTEMP));
     	oNewMTOEnquiryForm.setLtAmbientRemTempList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_AMBREMTEMP));
     	oNewMTOEnquiryForm.setLtTemperatureRiseList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_TEMP_RISE));
     	oNewMTOEnquiryForm.setLtInsulationClassList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_INSULATION_CLASS));
     	oNewMTOEnquiryForm.setLtForcedCoolingList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_FORCED_COOL));
     	oNewMTOEnquiryForm.setLtHazAreaTypeList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_HAZARD_AREA_TYPE));
     	oNewMTOEnquiryForm.setLtHazAreaList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_HAZARD_AREA));
     	oNewMTOEnquiryForm.setLtGasGroupList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LT_GASGROUP));
     	oNewMTOEnquiryForm.setLtHazardZoneList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_HAZARD_ZONE));
     	oNewMTOEnquiryForm.setLtDustGroupList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_DUST_GROUP));
     	oNewMTOEnquiryForm.setLtApplicationList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LT_APPL));
     	oNewMTOEnquiryForm.setLtDutyList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LT_DUTY));
     	oNewMTOEnquiryForm.setLtCDFList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LT_CDF));
     	oNewMTOEnquiryForm.setLtStartsList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QUOTATION_QEM_LT_STARTS));
     	oNewMTOEnquiryForm.setLtNoiseLevelList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_NOISE_LEVEL));
     	
     	// Electrical Fields Lists.
     	oNewMTOEnquiryForm.setLtRVRAList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_RVRA));
     	oNewMTOEnquiryForm.setLtWindingTreatmentList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_WINDING_TREATMENT));
     	oNewMTOEnquiryForm.setLtWindingWireList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_WINDING_WIRE));
     	oNewMTOEnquiryForm.setLtWindingConfigurationList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_WINDING_CONFIG));
     	oNewMTOEnquiryForm.setLtBroughtOutTerminalsList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_BROUGHTOUTTERMINALS));
     	oNewMTOEnquiryForm.setLtLeadList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_LEAD));
     	oNewMTOEnquiryForm.setLtVFDTypeList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_VFD_TYPE));
     	oNewMTOEnquiryForm.setLtVFDSpeedRangeMinList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_VFD_SPEEDRANGE_MIN));
     	oNewMTOEnquiryForm.setLtVFDSpeedRangeMaxList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_VFD_SPEEDRANGE_MAX));
     	oNewMTOEnquiryForm.setLtOverloadingDutyList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_OVERLOADING_DUTY));
     	oNewMTOEnquiryForm.setLtConstantEffRangeList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_CONSTANT_EFF_RANGE));
     	
     	// Mechanical Fields Lists
     	oNewMTOEnquiryForm.setLtShaftTypeList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_SHAFT_TYPE));
     	oNewMTOEnquiryForm.setLtShaftMaterialTypeList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_SHAFT_MATERIAL));
     	oNewMTOEnquiryForm.setLtDirectionOfRotationList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_DIR_OF_ROTATION));
     	oNewMTOEnquiryForm.setLtStandardRotationList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_STANDARD_ROTATION));
     	oNewMTOEnquiryForm.setLtMethodOfCouplingList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_METHOD_OF_COUPLING));
     	oNewMTOEnquiryForm.setLtPaintingTypeList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_PAINT_TYPE));
     	oNewMTOEnquiryForm.setLtPaintShadeList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_PAINT_SHADE));
     	oNewMTOEnquiryForm.setLtPaintThicknessList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_PAINT_THICKNESS));
     	oNewMTOEnquiryForm.setLtIPList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_IP));
     	oNewMTOEnquiryForm.setLtCableSizeList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_CABLE_SIZE));
     	oNewMTOEnquiryForm.setLtNoOfRunsList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_NO_OF_RUNS));
     	oNewMTOEnquiryForm.setLtNoOfCoresList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_NO_OF_CORES));
     	oNewMTOEnquiryForm.setLtCrossSectionAreaList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_CROSSSECTION_AREA));
     	oNewMTOEnquiryForm.setLtConductorMaterialList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_CONDUCTOR_MATERIAL));
     	oNewMTOEnquiryForm.setLtTerminalBoxList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_TERMINAL_BOX));
     	oNewMTOEnquiryForm.setLtSpreaderBoxList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_SPREADER_BOX));
     	oNewMTOEnquiryForm.setLtSpaceHeaterList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_SPACE_HEATER));
     	oNewMTOEnquiryForm.setLtVibrationList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_VIBRATION));
     	oNewMTOEnquiryForm.setLtFlyingLeadList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_FLYING_LEAD));
     	oNewMTOEnquiryForm.setLtMetalFanList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_METAL_FAN));
     	oNewMTOEnquiryForm.setLtTechoMountingList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_TECHO_MOUNTING));
     	oNewMTOEnquiryForm.setLtMainTBList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_MAIN_TB));
     	oNewMTOEnquiryForm.setLtCableEntryList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_CABLE_ENTRY));
     	oNewMTOEnquiryForm.setLtLubricationList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_LUBRICATION));
     	
     	// Accessories Fields Lists
     	oNewMTOEnquiryForm.setLtHardwareList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_HARDWARE));
     	oNewMTOEnquiryForm.setLtGlandPlateList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_GLAND_PLATE));
     	oNewMTOEnquiryForm.setLtDoubleCompressionGlandList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_DOUBLE_COMPRESS_GLAND));
     	oNewMTOEnquiryForm.setLtSPMMountingProvisionList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_SPM_MOUNT_PROVISION));
     	oNewMTOEnquiryForm.setLtAddNamePlateList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_ADD_NAME_PLATE));
     	oNewMTOEnquiryForm.setLtArrowPlateDirectionList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_ARROW_PLATE_DIRECTION));
     	oNewMTOEnquiryForm.setLtRTDList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_RTD));
     	oNewMTOEnquiryForm.setLtBTDList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_BTD));
     	oNewMTOEnquiryForm.setLtThermisterList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_THERMISTER));
     	oNewMTOEnquiryForm.setLtAuxTerminalBoxList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_AUX_TERMINAL_BOX));
     	
     	// Bearings Fields Lists
     	oNewMTOEnquiryForm.setLtBearingSystemList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_BEARING_SYSTEM));
     	oNewMTOEnquiryForm.setLtBearingNDEList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_BEARING_NDE));
     	oNewMTOEnquiryForm.setLtBearingDEList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_BEARING_DE));
     	
     	// Certificates Spares Tests Lists
     	oNewMTOEnquiryForm.setLtWitnessRoutineList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_WITNESS_ROUTINE));
     	oNewMTOEnquiryForm.setLtAdditionalTestList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_ADDITIONAL_TEST));
     	oNewMTOEnquiryForm.setLtTypeTestList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_TYPE_TEST));
     	oNewMTOEnquiryForm.setLtULCEList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_ULCE));
     	oNewMTOEnquiryForm.setLtQAPList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_QAP));
     	oNewMTOEnquiryForm.setLtSeaworthyPackingList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_SEAWORTHY_PACKING));
     	oNewMTOEnquiryForm.setLtWarrantyList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_WARRANTY));
     	oNewMTOEnquiryForm.setLtSparesList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_SPARES));
     	oNewMTOEnquiryForm.setLtDataSheetList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_DATA_SHEET));
     	oNewMTOEnquiryForm.setLtPerfCurvesList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_PERF_CURVES));
     	oNewMTOEnquiryForm.setLtMotorGAList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_MOTOR_GA));
     	oNewMTOEnquiryForm.setLtTBoxGAList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_TBOX_GA));
     	
     	oNewMTOEnquiryForm.setLtNonStandardVoltageList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_NON_STD_VOLTAGE));
     	oNewMTOEnquiryForm.setLtTemperatureClassList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_TEMPERATURE_CLASS));
     	oNewMTOEnquiryForm.setLtEfficiencyClassList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_EFFICIENCY_CLASS));
     	oNewMTOEnquiryForm.setLtTypeOfGreaseList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_TYPE_OF_GREASE));
     	oNewMTOEnquiryForm.setLtDualSpeedTypeList(oQuotationDao.getAscendingOptionsbyAttributeList(conn,QuotationConstants.QEM_LT_DUAL_SPEED_TYPE));
     	
		return oNewMTOEnquiryForm;
    }

	/**
	 * This Method is used to Save / Upload Attachments from Enquiry page.
	 * @param oNewEnquiryForm NewEnquiryForm Bean which contains the Attachment File Details.
	 * @param oUserDto UserDto Bean which contains the User Details.
	 * @param request HttpServletRequest object to handle request operations.
	 * @throws Exception if any error occurs while performing database operations, etc
	 */
	public void manageUploadAttachments(NewEnquiryForm oNewEnquiryForm, UserDto oUserDto, HttpServletRequest request) throws Exception {
		
		String sAttach1 = addAttachment( oNewEnquiryForm.getTheFile1(),oNewEnquiryForm.getFileDescription1(),oNewEnquiryForm.getEnquiryId(), oUserDto);
		request.setAttribute(QuotationConstants.QUOTATION_ATTACHMENT1, sAttach1);
		oNewEnquiryForm.setFileDescription1("");
		String sAttach2 = addAttachment( oNewEnquiryForm.getTheFile2(),oNewEnquiryForm.getFileDescription2(),oNewEnquiryForm.getEnquiryId(), oUserDto);
		request.setAttribute(QuotationConstants.QUOTATION_ATTACHMENT2, sAttach2);
		oNewEnquiryForm.setFileDescription2("");
		String sAttach3 = addAttachment( oNewEnquiryForm.getTheFile3(),oNewEnquiryForm.getFileDescription3(),oNewEnquiryForm.getEnquiryId(), oUserDto);
		request.setAttribute(QuotationConstants.QUOTATION_ATTACHMENT3, sAttach3);
		oNewEnquiryForm.setFileDescription3("");
		String sAttach4 = addAttachment( oNewEnquiryForm.getTheFile4(),oNewEnquiryForm.getFileDescription4(),oNewEnquiryForm.getEnquiryId(), oUserDto);
		request.setAttribute(QuotationConstants.QUOTATION_ATTACHMENT4, sAttach4);
		oNewEnquiryForm.setFileDescription4("");
		String sAttach5 = addAttachment( oNewEnquiryForm.getTheFile5(),oNewEnquiryForm.getFileDescription5(),oNewEnquiryForm.getEnquiryId(), oUserDto);
		request.setAttribute(QuotationConstants.QUOTATION_ATTACHMENT5, sAttach5);
		oNewEnquiryForm.setFileDescription5("");
		String sAttach6 = addAttachment( oNewEnquiryForm.getTheFile6(),oNewEnquiryForm.getFileDescription6(),oNewEnquiryForm.getEnquiryId(), oUserDto);
		request.setAttribute(QuotationConstants.QUOTATION_ATTACHMENT6, sAttach6);
		oNewEnquiryForm.setFileDescription6("");
		String sAttach7 = addAttachment( oNewEnquiryForm.getTheFile7(),oNewEnquiryForm.getFileDescription7(),oNewEnquiryForm.getEnquiryId(), oUserDto);
		request.setAttribute(QuotationConstants.QUOTATION_ATTACHMENT7, sAttach7);
		oNewEnquiryForm.setFileDescription7("");
		String sAttach8 = addAttachment( oNewEnquiryForm.getTheFile8(),oNewEnquiryForm.getFileDescription8(),oNewEnquiryForm.getEnquiryId(), oUserDto);
		request.setAttribute(QuotationConstants.QUOTATION_ATTACHMENT8, sAttach8);
		oNewEnquiryForm.setFileDescription8("");
		String sAttach9 = addAttachment( oNewEnquiryForm.getTheFile9(),oNewEnquiryForm.getFileDescription9(),oNewEnquiryForm.getEnquiryId(), oUserDto);
		request.setAttribute(QuotationConstants.QUOTATION_ATTACHMENT9, sAttach9);
		oNewEnquiryForm.setFileDescription9("");
		String sAttach10 = addAttachment( oNewEnquiryForm.getTheFile10(),oNewEnquiryForm.getFileDescription10(),oNewEnquiryForm.getEnquiryId(), oUserDto);
		request.setAttribute(QuotationConstants.QUOTATION_ATTACHMENT10, sAttach10);
		oNewEnquiryForm.setFileDescription10("");
		
		String sAttachCommercialPurchaseSpec = addAttachment( oNewEnquiryForm.getCommPurchaseSpecFile1(),oNewEnquiryForm.getCommPurchaseSpecFileDesc1(),oNewEnquiryForm.getEnquiryId(),oUserDto);
		request.setAttribute(QuotationConstants.QUOTATION_ATTACHMENT_COMMPURCHASESPEC, sAttachCommercialPurchaseSpec);
		oNewEnquiryForm.setCommPurchaseSpecFileDesc1("");
		
	}
	
	
	private String addAttachment(FormFile oImageFile, String sFileDesc, int iEnquiryId, UserDto oUserDto) throws Exception {
		
		String sValidateAttachment = null;
		String sFileName = null;
		String sFileType = null;
		String[] sFileTypes = null;

		if (oImageFile != null && oImageFile.getFileSize() > QuotationConstants.QUOTATION_LITERAL_ZERO) {

			// step1. validate this attachment
			sValidateAttachment = AttachmentUtility.validateAttachment(iEnquiryId, oImageFile);

			if (sValidateAttachment == null)
				sValidateAttachment = "";

			// validation failed
			if ((sValidateAttachment.trim().length() == QuotationConstants.QUOTATION_LITERAL_ZERO)
					|| (!sValidateAttachment.equalsIgnoreCase(QuotationConstants.QUOTATION_VALID))) {
				return sValidateAttachment;

			} else {
				// go for upload
				sFileName = "";
				sFileType = "";
				sFileTypes = oImageFile.getFileName().split("\\.");
				if (sFileTypes.length == QuotationConstants.QUOTATION_LITERAL_TWO) {
					sFileType = sFileTypes[1];
				}
				if (sFileType.equals(QuotationConstants.QUOTAION_MSG)) {
					sFileName = QuotationConstants.QUOTAION_MSOUTLOOKMSG;
				} else {
					sFileName = oImageFile.getFileName();
				}

				// set all the required properties to dto object
				AttachmentDto oAttachmentDto = new AttachmentDto();

				oAttachmentDto.setApplicationId(CommonUtility.getIntValue(
						PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_ATTACHMENT_APPLICATIONID)));

				oAttachmentDto.setFileName(sFileName);
				oAttachmentDto.setFileBytes(oImageFile.getFileData());
				oAttachmentDto.setFileSize(oImageFile.getFileSize());

				oAttachmentDto.setApplicationUniqueId("" + iEnquiryId);
				oAttachmentDto.setAttachedBy(Integer.parseInt(oUserDto.getUserId()));
				oAttachmentDto.setAttachedByName(oUserDto.getFullName());
				oAttachmentDto.setFileDescription(sFileDesc);

				boolean isUploaded = AttachmentUtility.uploadAttachment(oAttachmentDto);
				if (!isUploaded) {
					return QuotationConstants.QUOTATION_FILEUPLOADF;
				}
			}
		}
		return "";
	}

	/**
	 * This Method is used to verify the Send to MTO criteria for each of the Ratings in the Enquiry.
	 * @param oNewEnquiryDto - NewEnquiryDto contains the Ratings List.
	 * @return boolean - Checks Send to MTO criteria - returns true if any condition matches else returns false.
	 * @throws Exception if any error occurs while performing database operations, etc.
	 */
	public boolean verifyDesignReviewForRatings(NewEnquiryDto oNewEnquiryDto) throws Exception {
		boolean isDesignReviewRequired = false;
		
		for(NewRatingDto oNewRatingDto : oNewEnquiryDto.getNewRatingsList()) {
			
			if(oNewRatingDto.getLtStandardDiscount().equals("0") ||  oNewRatingDto.getLtStandardDiscount().equals("") || oNewRatingDto.getLtLPPerUnit().equals("0") || oNewRatingDto.getLtLPPerUnit().equals("") ) {
				isDesignReviewRequired = true;
				break;
			}
			// Check AddOn - RE Values.
			
			// Check Design Specific Text Field Values.
		}
		
		return isDesignReviewRequired;
	}
	/**
	 * This Method captures the NewRatingDto's List which have been created for the Enquiry.
	 * @param conn Connection object to fetch the Required Enquiry Details.
	 * @param oQuotationDao QuotationDao reference to invoke updateSubmitToDesignFlag.
	 * @param oNewEnquiryDto NewEnquiryDto Bean to which the List of Ratings need to be assigned.
	 * @param request HttpServletRequest object to handle request operations.
	 * @return ArrayList - Returns the NewRatingDto's List which have been created for the Enquiry.
	 * @throws Exception if any error occurs while performing database operations, etc.
	 */
	public ArrayList<NewRatingDto> getRatingsList(Connection conn, QuotationDao oQuotationDao, NewEnquiryDto oNewEnquiryDto, HttpServletRequest request) throws Exception {
		String sMethodName = "getRatingsList"; 
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
		ArrayList<NewRatingDto> alRatingsList = new ArrayList<>();
		int totalRatingIndex = oNewEnquiryDto.getTotalRatingIndex();
		boolean isEnquirySetToDesign = false;
		
		try {
			NewRatingDto oTempNewRatingDto = null;
			for (int iRatingIndex = 1; iRatingIndex <= totalRatingIndex; iRatingIndex++) {
				oTempNewRatingDto = new NewRatingDto();
				oTempNewRatingDto.setEnquiryId(oNewEnquiryDto.getEnquiryId());
				oTempNewRatingDto.setRatingId(Integer.parseInt(CommonUtility.getStringParameter(request, "ratingObjId" + iRatingIndex)));
				oTempNewRatingDto.setRatingNo(Integer.parseInt(CommonUtility.getStringParameter(request, "ratingNo" + iRatingIndex)));
				
				oTempNewRatingDto.setQuantity(Integer.parseInt(CommonUtility.getStringParameter(request, "quantity" + iRatingIndex)));
				oTempNewRatingDto.setLtProdLineId(CommonUtility.getStringParameter(request, "productLine" + iRatingIndex));
				oTempNewRatingDto.setLtKWId(CommonUtility.getStringParameter(request, "kw" + iRatingIndex));
				oTempNewRatingDto.setLtPoleId(CommonUtility.getStringParameter(request, "pole" + iRatingIndex));
				oTempNewRatingDto.setLtFrameId(CommonUtility.getStringParameter(request, "frame" + iRatingIndex));
				oTempNewRatingDto.setLtFrameSuffixId(CommonUtility.getStringParameter(request, "framesuffix" + iRatingIndex));
				oTempNewRatingDto.setLtMountingId(CommonUtility.getStringParameter(request, "mounting" + iRatingIndex));
				oTempNewRatingDto.setLtTBPositionId(CommonUtility.getStringParameter(request, "tbPosition" + iRatingIndex));
				
				oTempNewRatingDto.setTagNumber(CommonUtility.getStringParameter(request, "tagNumber" + iRatingIndex));
				oTempNewRatingDto.setLtManufacturingLocation(CommonUtility.getStringParameter(request, "mfgLocation" + iRatingIndex));
				oTempNewRatingDto.setLtEffClass(CommonUtility.getStringParameter(request, "effClass" + iRatingIndex));
				oTempNewRatingDto.setLtVoltId(CommonUtility.getStringParameter(request, "volt" + iRatingIndex));
				oTempNewRatingDto.setLtVoltAddVariation(CommonUtility.getStringParameter(request, "voltAddVariation" + iRatingIndex));
				oTempNewRatingDto.setLtVoltRemoveVariation(CommonUtility.getStringParameter(request, "voltRemoveVariation" + iRatingIndex));
				oTempNewRatingDto.setLtFreqId(CommonUtility.getStringParameter(request, "freq" + iRatingIndex));
				oTempNewRatingDto.setLtFreqAddVariation(CommonUtility.getStringParameter(request, "freqAddVariation" + iRatingIndex));
				oTempNewRatingDto.setLtFreqRemoveVariation(CommonUtility.getStringParameter(request, "freqRemoveVariation" + iRatingIndex));
				oTempNewRatingDto.setLtCombVariation(CommonUtility.getStringParameter(request, "combinedVariation" + iRatingIndex));
				oTempNewRatingDto.setLtServiceFactor(CommonUtility.getStringParameter(request, "serviceFactor" + iRatingIndex));
				oTempNewRatingDto.setLtMethodOfStarting(CommonUtility.getStringParameter(request, "methodOfStarting" + iRatingIndex));
				oTempNewRatingDto.setLtStartingCurrentOnDOLStart(CommonUtility.getStringParameter(request, "startingCurrOnDOLStarting" + iRatingIndex));
				oTempNewRatingDto.setLtAmbTempId(CommonUtility.getStringParameter(request, "ambientTemp" + iRatingIndex));
				oTempNewRatingDto.setLtAmbTempRemoveId(CommonUtility.getStringParameter(request, "ambientTempSub" + iRatingIndex));
				oTempNewRatingDto.setLtTempRise(CommonUtility.getStringParameter(request, "tempRise" + iRatingIndex));
				oTempNewRatingDto.setLtInsulationClassId(CommonUtility.getStringParameter(request, "insulationClass" + iRatingIndex));
				oTempNewRatingDto.setLtForcedCoolingId(CommonUtility.getStringParameter(request, "methodOfCooling" + iRatingIndex));
				oTempNewRatingDto.setLtAltitude(CommonUtility.getStringParameter(request, "altitude" + iRatingIndex));
				oTempNewRatingDto.setLtHazardAreaId(CommonUtility.getStringParameter(request, "hazardArea" + iRatingIndex));
				oTempNewRatingDto.setLtHazardAreaTypeId(CommonUtility.getStringParameter(request, "hazardAreaType" + iRatingIndex));
				oTempNewRatingDto.setLtGasGroupId(CommonUtility.getStringParameter(request, "gasGroup" + iRatingIndex));
				oTempNewRatingDto.setLtHazardZoneId(CommonUtility.getStringParameter(request, "hazardZone" + iRatingIndex));
				oTempNewRatingDto.setLtDustGroupId(CommonUtility.getStringParameter(request, "dustGroup" + iRatingIndex));
				oTempNewRatingDto.setLtTempClass(CommonUtility.getStringParameter(request, "tempClass" + iRatingIndex));
				oTempNewRatingDto.setLtApplicationId(CommonUtility.getStringParameter(request, "application" + iRatingIndex));
				oTempNewRatingDto.setLtDutyId(CommonUtility.getStringParameter(request, "duty" + iRatingIndex));
				oTempNewRatingDto.setLtCDFId(CommonUtility.getStringParameter(request, "cdf" + iRatingIndex));
				oTempNewRatingDto.setLtStartsId(CommonUtility.getStringParameter(request, "starts" + iRatingIndex));
				oTempNewRatingDto.setLtNoiseLevel(CommonUtility.getStringParameter(request, "noiseLevel" + iRatingIndex));
				oTempNewRatingDto.setLtReplacementMotor(CommonUtility.getStringParameter(request, "isReplacementMotor" + iRatingIndex));
				oTempNewRatingDto.setLtEarlierMotorSerialNo(CommonUtility.getStringParameter(request, "earlierSuppliedMotorSerialNo" + iRatingIndex));
				oTempNewRatingDto.setLtStandardDeliveryWks(CommonUtility.getStringParameter(request, "standardDelivery" + iRatingIndex));
				oTempNewRatingDto.setLtCustReqDeliveryWks(CommonUtility.getStringParameter(request, "customerRequestedDelivery" + iRatingIndex));
				
				oTempNewRatingDto.setLtRVId(CommonUtility.getStringParameter(request, "rv" + iRatingIndex));
				oTempNewRatingDto.setLtRAId(CommonUtility.getStringParameter(request, "ra" + iRatingIndex));
				oTempNewRatingDto.setLtWindingConfig(CommonUtility.getStringParameter(request, "windingConfiguration" + iRatingIndex));
				oTempNewRatingDto.setLtWindingTreatmentId(CommonUtility.getStringParameter(request, "windingTreatment" + iRatingIndex));
				//oTempNewRatingDto.setLtWindingWire(CommonUtility.getStringParameter(request, "windingWire" + iRatingIndex));
				oTempNewRatingDto.setLtLeadId(CommonUtility.getStringParameter(request, "lead" + iRatingIndex));
				oTempNewRatingDto.setLtLoadGD2Value(CommonUtility.getStringParameter(request, "loadGD2Value" + iRatingIndex));
				oTempNewRatingDto.setLtVFDApplTypeId(CommonUtility.getStringParameter(request, "vfdType" + iRatingIndex));
				oTempNewRatingDto.setLtVFDMotorSpeedRangeMin(CommonUtility.getStringParameter(request, "vfdSpeedRangeMin" + iRatingIndex));
				oTempNewRatingDto.setLtVFDMotorSpeedRangeMax(CommonUtility.getStringParameter(request, "vfdSpeedRangeMax" + iRatingIndex));
				oTempNewRatingDto.setLtOverloadingDuty(CommonUtility.getStringParameter(request, "overloadingDuty" + iRatingIndex));
				oTempNewRatingDto.setLtConstantEfficiencyRange(CommonUtility.getStringParameter(request, "constantEfficiencyRange" + iRatingIndex));
				
				oTempNewRatingDto.setLtShaftTypeId(CommonUtility.getStringParameter(request, "shaftType" + iRatingIndex));
				oTempNewRatingDto.setLtShaftMaterialId(CommonUtility.getStringParameter(request, "shaftMaterialType" + iRatingIndex));
				oTempNewRatingDto.setLtDirectionOfRotation(CommonUtility.getStringParameter(request, "directionOfRotation" + iRatingIndex));
				oTempNewRatingDto.setLtStandardRotation(CommonUtility.getStringParameter(request, "standardRotation" + iRatingIndex));
				oTempNewRatingDto.setLtMethodOfCoupling(CommonUtility.getStringParameter(request, "methodOfCoupling" + iRatingIndex));
				oTempNewRatingDto.setLtPaintingTypeId(CommonUtility.getStringParameter(request, "paintingType" + iRatingIndex));
				oTempNewRatingDto.setLtPaintShadeId(CommonUtility.getStringParameter(request, "paintShade" + iRatingIndex));
				oTempNewRatingDto.setLtPaintShadeValue(CommonUtility.getStringParameter(request, "paintShadeVal" + iRatingIndex));
				oTempNewRatingDto.setLtPaintThicknessId(CommonUtility.getStringParameter(request, "paintThickness" + iRatingIndex));
				oTempNewRatingDto.setLtIPId(CommonUtility.getStringParameter(request, "ip" + iRatingIndex));
				oTempNewRatingDto.setLtCableSizeId(CommonUtility.getStringParameter(request, "cableSize" + iRatingIndex));
				oTempNewRatingDto.setLtNoOfRuns(CommonUtility.getStringParameter(request, "noOfRuns" + iRatingIndex));
				oTempNewRatingDto.setLtNoOfCores(CommonUtility.getStringParameter(request, "noOfCores" + iRatingIndex));
				oTempNewRatingDto.setLtCrossSectionAreaId(CommonUtility.getStringParameter(request, "crossSectionArea" + iRatingIndex));
				oTempNewRatingDto.setLtConductorMaterialId(CommonUtility.getStringParameter(request, "conductorMaterial" + iRatingIndex));
				oTempNewRatingDto.setLtCableDiameter(CommonUtility.getStringParameter(request, "cableDiameter" + iRatingIndex));
				oTempNewRatingDto.setLtTerminalBoxSizeId(CommonUtility.getStringParameter(request, "terminalBoxSize" + iRatingIndex));
				oTempNewRatingDto.setLtSpreaderBoxId(CommonUtility.getStringParameter(request, "spreaderBox" + iRatingIndex));
				oTempNewRatingDto.setLtSpaceHeaterId(CommonUtility.getStringParameter(request, "spaceHeater" + iRatingIndex));
				oTempNewRatingDto.setLtVibrationId(CommonUtility.getStringParameter(request, "vibration" + iRatingIndex));
				oTempNewRatingDto.setLtFlyingLeadWithoutTDId(CommonUtility.getStringParameter(request, "flyingLeadWithoutTB" + iRatingIndex));
				oTempNewRatingDto.setLtFlyingLeadWithTDId(CommonUtility.getStringParameter(request, "flyingLeadWithTB" + iRatingIndex));
				oTempNewRatingDto.setLtMetalFanId(CommonUtility.getStringParameter(request, "metalFan" + iRatingIndex));
				oTempNewRatingDto.setLtTechoMounting(CommonUtility.getStringParameter(request, "techoMounting" + iRatingIndex));
				oTempNewRatingDto.setLtShaftGroundingId(CommonUtility.getStringParameter(request, "shaftGrounding" + iRatingIndex));
				
				oTempNewRatingDto.setLtHardware(CommonUtility.getStringParameter(request, "hardware" + iRatingIndex));
				oTempNewRatingDto.setLtGlandPlateId(CommonUtility.getStringParameter(request, "glandPlate" + iRatingIndex));
				oTempNewRatingDto.setLtDoubleCompressionGlandId(CommonUtility.getStringParameter(request, "doubleCompressGland" + iRatingIndex));
				oTempNewRatingDto.setLtSPMMountingProvisionId(CommonUtility.getStringParameter(request, "spmMountingProvision" + iRatingIndex));
				oTempNewRatingDto.setLtAddNamePlateId(CommonUtility.getStringParameter(request, "addlNamePlate" + iRatingIndex));
				oTempNewRatingDto.setLtDirectionArrowPlateId(CommonUtility.getStringParameter(request, "directionArrowPlate" + iRatingIndex));
				oTempNewRatingDto.setLtRTDId(CommonUtility.getStringParameter(request, "rtd" + iRatingIndex));
				oTempNewRatingDto.setLtBTDId(CommonUtility.getStringParameter(request, "btd" + iRatingIndex));
				oTempNewRatingDto.setLtThermisterId(CommonUtility.getStringParameter(request, "thermister" + iRatingIndex));
				oTempNewRatingDto.setLtAuxTerminalBoxId(CommonUtility.getStringParameter(request, "auxTerminalBox" + iRatingIndex));
				oTempNewRatingDto.setLtCableSealingBoxId(CommonUtility.getStringParameter(request, "cableSealingBox" + iRatingIndex));
				
				oTempNewRatingDto.setLtBearingSystemId(CommonUtility.getStringParameter(request, "bearingSystem" + iRatingIndex));
				oTempNewRatingDto.setLtBearingNDEId(CommonUtility.getStringParameter(request, "bearingNDE" + iRatingIndex));
				oTempNewRatingDto.setLtBearingDEId(CommonUtility.getStringParameter(request, "bearingDE" + iRatingIndex));
				
				oTempNewRatingDto.setLtWitnessRoutineId(CommonUtility.getStringParameter(request, "witnessRoutine" + iRatingIndex));
				oTempNewRatingDto.setLtAdditionalTest1Id(CommonUtility.getStringParameter(request, "additional1Test" + iRatingIndex));
				oTempNewRatingDto.setLtAdditionalTest2Id(CommonUtility.getStringParameter(request, "additional2Test" + iRatingIndex));
				oTempNewRatingDto.setLtAdditionalTest3Id(CommonUtility.getStringParameter(request, "additional3Test" + iRatingIndex));
				oTempNewRatingDto.setLtTypeTestId(CommonUtility.getStringParameter(request, "typeTest" + iRatingIndex));
				oTempNewRatingDto.setLtULCEId(CommonUtility.getStringParameter(request, "ulce" + iRatingIndex));
				oTempNewRatingDto.setLtQAPId(CommonUtility.getStringParameter(request, "qap" + iRatingIndex));
				oTempNewRatingDto.setLtSpares1(CommonUtility.getStringParameter(request, "spare1Rating" + iRatingIndex));
				oTempNewRatingDto.setLtSpares2(CommonUtility.getStringParameter(request, "spare2Rating" + iRatingIndex));
				oTempNewRatingDto.setLtSpares3(CommonUtility.getStringParameter(request, "spare3Rating" + iRatingIndex));
				oTempNewRatingDto.setLtSpares4(CommonUtility.getStringParameter(request, "spare4Rating" + iRatingIndex));
				oTempNewRatingDto.setLtSpares5(CommonUtility.getStringParameter(request, "spare5Rating" + iRatingIndex));
				oTempNewRatingDto.setLtDataSheet(CommonUtility.getStringParameter(request, "dataSheet" + iRatingIndex));
				oTempNewRatingDto.setLtMotorGA(CommonUtility.getStringParameter(request, "motorGA" + iRatingIndex));
				oTempNewRatingDto.setLtPerfCurves(CommonUtility.getStringParameter(request, "perfCurves" + iRatingIndex));
				oTempNewRatingDto.setLtTBoxGA(CommonUtility.getStringParameter(request, "tBoxGA" + iRatingIndex));
				oTempNewRatingDto.setLtAdditionalComments(CommonUtility.getStringParameter(request, "additionalComments" + iRatingIndex));
				
				oTempNewRatingDto.setLtCustomerId(String.valueOf(oNewEnquiryDto.getCustomerId()));
				
				alRatingsList.add(oTempNewRatingDto);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			LoggerUtility.log("ERROR: ", this.getClass().getName(), "getRatingsList", ExceptionUtility.getStackTraceAsString(e));
		}
		
		LoggerUtility.log("INFO", sClassName, sMethodName, "END");
		return alRatingsList;
	}
	
	/**
	 * This Method captures the MTO NewRatingDto's List from Design Page.
	 * @param conn Connection object to fetch the Required Enquiry Details.
	 * @param oQuotationDao QuotationDao reference to invoke updateSubmitToDesignFlag.
	 * @param oNewEnquiryDto NewEnquiryDto Bean to which the List of Ratings need to be assigned.
	 * @param request HttpServletRequest object to handle request operations.
	 * @return ArrayList - Returns the NewRatingDto's List which have been created for the Enquiry.
	 * @throws Exception if any error occurs while performing database operations, etc.
	 */
	public ArrayList<NewRatingDto> getMTORatingsList(Connection conn, QuotationDao oQuotationDao, NewEnquiryDto oNewEnquiryDto, HttpServletRequest request) throws Exception {
		String sMethodName = "getMTORatingsList"; 
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
		ArrayList<NewRatingDto> alRatingsList = new ArrayList<>();
		int totalRatingIndex = oNewEnquiryDto.getTotalRatingIndex();
		boolean isEnquirySetToDesign = false;
		
		try {
			NewRatingDto oTempNewRatingDto = null;
			for (int iRatingIndex = 1; iRatingIndex <= totalRatingIndex; iRatingIndex++) {
				oTempNewRatingDto = new NewRatingDto();
				oTempNewRatingDto.setEnquiryId(oNewEnquiryDto.getEnquiryId());
				oTempNewRatingDto.setRatingId(Integer.parseInt(CommonUtility.getStringParameter(request, "ratingObjId" + iRatingIndex)));
				oTempNewRatingDto.setRatingNo(Integer.parseInt(CommonUtility.getStringParameter(request, "ratingNo" + iRatingIndex)));
				
				oTempNewRatingDto.setModelNumber(CommonUtility.getStringParameter(request, "modelNumber" + iRatingIndex));
				oTempNewRatingDto.setLtApplicationId(CommonUtility.getStringParameter(request, "application" + iRatingIndex));
				oTempNewRatingDto.setQuantity(Integer.parseInt(CommonUtility.getStringParameter(request, "quantity" + iRatingIndex)));
				oTempNewRatingDto.setLtProdLineId(CommonUtility.getStringParameter(request, "productLine" + iRatingIndex));
				oTempNewRatingDto.setLtKWId(CommonUtility.getStringParameter(request, "kw" + iRatingIndex));
				oTempNewRatingDto.setLtPoleId(CommonUtility.getStringParameter(request, "pole" + iRatingIndex));
				oTempNewRatingDto.setLtFrameId(CommonUtility.getStringParameter(request, "frame" + iRatingIndex));
				oTempNewRatingDto.setLtFrameSuffixId(CommonUtility.getStringParameter(request, "framesuffix" + iRatingIndex));
				oTempNewRatingDto.setLtMountingId(CommonUtility.getStringParameter(request, "mounting" + iRatingIndex));
				oTempNewRatingDto.setLtTBPositionId(CommonUtility.getStringParameter(request, "tbPosition" + iRatingIndex));
				oTempNewRatingDto.setLtEffClass(CommonUtility.getStringParameter(request, "effClass" + iRatingIndex));
				
				oTempNewRatingDto.setLtReplacementMotor(CommonUtility.getStringParameter(request, "isReplacementMotor" + iRatingIndex));
				oTempNewRatingDto.setLtEarlierMotorSerialNo(CommonUtility.getStringParameter(request, "earlierSuppliedMotorSerialNo" + iRatingIndex));
				oTempNewRatingDto.setLtEnclosure(CommonUtility.getStringParameter(request, "enclosure" + iRatingIndex));
				oTempNewRatingDto.setLtForcedCoolingId(CommonUtility.getStringParameter(request, "methodOfCooling" + iRatingIndex));
				oTempNewRatingDto.setLtVoltId(CommonUtility.getStringParameter(request, "volt" + iRatingIndex));
				oTempNewRatingDto.setLtVoltAddVariation(CommonUtility.getStringParameter(request, "voltAddVariation" + iRatingIndex));
				oTempNewRatingDto.setLtVoltRemoveVariation(CommonUtility.getStringParameter(request, "voltRemoveVariation" + iRatingIndex));
				oTempNewRatingDto.setLtFreqId(CommonUtility.getStringParameter(request, "freq" + iRatingIndex));
				oTempNewRatingDto.setLtFreqAddVariation(CommonUtility.getStringParameter(request, "freqAddVariation" + iRatingIndex));
				oTempNewRatingDto.setLtFreqRemoveVariation(CommonUtility.getStringParameter(request, "freqRemoveVariation" + iRatingIndex));
				oTempNewRatingDto.setLtCombVariation(CommonUtility.getStringParameter(request, "combinedVariation" + iRatingIndex));
				oTempNewRatingDto.setLtHazardAreaTypeId(CommonUtility.getStringParameter(request, "hazardAreaType" + iRatingIndex));
				oTempNewRatingDto.setLtHazardAreaId(CommonUtility.getStringParameter(request, "hazardArea" + iRatingIndex));
				oTempNewRatingDto.setLtGasGroupId(CommonUtility.getStringParameter(request, "gasGroup" + iRatingIndex));
				oTempNewRatingDto.setLtHazardZoneId(CommonUtility.getStringParameter(request, "hazardZone" + iRatingIndex));
				oTempNewRatingDto.setLtDustGroupId(CommonUtility.getStringParameter(request, "dustGroup" + iRatingIndex));
				oTempNewRatingDto.setLtTempClass(CommonUtility.getStringParameter(request, "tempClass" + iRatingIndex));
				oTempNewRatingDto.setLtAmbTempId(CommonUtility.getStringParameter(request, "ambientTemp" + iRatingIndex));
				oTempNewRatingDto.setLtAmbTempRemoveId(CommonUtility.getStringParameter(request, "ambientTempSub" + iRatingIndex));
				oTempNewRatingDto.setLtInsulationClassId(CommonUtility.getStringParameter(request, "insulationClass" + iRatingIndex));
				oTempNewRatingDto.setLtTempRise(CommonUtility.getStringParameter(request, "tempRise" + iRatingIndex));
				oTempNewRatingDto.setLtAltitude(CommonUtility.getStringParameter(request, "altitude" + iRatingIndex));
				oTempNewRatingDto.setLtIPId(CommonUtility.getStringParameter(request, "ip" + iRatingIndex));
				oTempNewRatingDto.setLtMethodOfStarting(CommonUtility.getStringParameter(request, "methodOfStarting" + iRatingIndex));
				oTempNewRatingDto.setLtDirectionOfRotation(CommonUtility.getStringParameter(request, "directionOfRotation" + iRatingIndex));
				oTempNewRatingDto.setLtStandardRotation(CommonUtility.getStringParameter(request, "standardRotation" + iRatingIndex));
				oTempNewRatingDto.setLtGreaseType(CommonUtility.getStringParameter(request, "typeOfGrease" + iRatingIndex));
				oTempNewRatingDto.setLtPaintingTypeId(CommonUtility.getStringParameter(request, "paintingType" + iRatingIndex));
				oTempNewRatingDto.setLtPaintShadeId(CommonUtility.getStringParameter(request, "paintShade" + iRatingIndex));
				oTempNewRatingDto.setLtPaintShadeValue(CommonUtility.getStringParameter(request, "paintShadeVal" + iRatingIndex));
				oTempNewRatingDto.setLtPaintThicknessId(CommonUtility.getStringParameter(request, "paintThickness" + iRatingIndex));
				oTempNewRatingDto.setLtCableSizeId(CommonUtility.getStringParameter(request, "cableSize" + iRatingIndex));
				oTempNewRatingDto.setLtNoOfRuns(CommonUtility.getStringParameter(request, "noOfRuns" + iRatingIndex));
				oTempNewRatingDto.setLtNoOfCores(CommonUtility.getStringParameter(request, "noOfCores" + iRatingIndex));
				oTempNewRatingDto.setLtCrossSectionAreaId(CommonUtility.getStringParameter(request, "crossSectionArea" + iRatingIndex));
				oTempNewRatingDto.setLtConductorMaterialId(CommonUtility.getStringParameter(request, "conductorMaterial" + iRatingIndex));
				oTempNewRatingDto.setLtCableDiameter(CommonUtility.getStringParameter(request, "cableDiameter" + iRatingIndex));
				oTempNewRatingDto.setLtNoiseLevel(CommonUtility.getStringParameter(request, "noiseLevel" + iRatingIndex));
				
				oTempNewRatingDto.setLtWindingConfig(CommonUtility.getStringParameter(request, "windingConfiguration" + iRatingIndex));
				oTempNewRatingDto.setLtWindingWire(CommonUtility.getStringParameter(request, "windingWire" + iRatingIndex));
				oTempNewRatingDto.setLtWindingTreatmentId(CommonUtility.getStringParameter(request, "windingTreatment" + iRatingIndex));
				oTempNewRatingDto.setLtDutyId(CommonUtility.getStringParameter(request, "duty" + iRatingIndex));
				oTempNewRatingDto.setLtCDFId(CommonUtility.getStringParameter(request, "cdf" + iRatingIndex));
				oTempNewRatingDto.setLtStartsId(CommonUtility.getStringParameter(request, "starts" + iRatingIndex));
				oTempNewRatingDto.setLtOverloadingDuty(CommonUtility.getStringParameter(request, "overloadingDuty" + iRatingIndex));
				oTempNewRatingDto.setLtServiceFactor(CommonUtility.getStringParameter(request, "serviceFactor" + iRatingIndex));
				oTempNewRatingDto.setLtBroughtOutTerminals(CommonUtility.getStringParameter(request, "noBroughtOutTerm" + iRatingIndex));
				oTempNewRatingDto.setLtLeadId(CommonUtility.getStringParameter(request, "lead" + iRatingIndex));
				oTempNewRatingDto.setLtStartingCurrentOnDOLStart(CommonUtility.getStringParameter(request, "startingCurrOnDOLStarting" + iRatingIndex));
				oTempNewRatingDto.setLtVFDApplTypeId(CommonUtility.getStringParameter(request, "vfdType" + iRatingIndex));
				oTempNewRatingDto.setLtVFDMotorSpeedRangeMin(CommonUtility.getStringParameter(request, "vfdSpeedRangeMin" + iRatingIndex));
				oTempNewRatingDto.setLtVFDMotorSpeedRangeMax(CommonUtility.getStringParameter(request, "vfdSpeedRangeMax" + iRatingIndex));
				oTempNewRatingDto.setLtConstantEfficiencyRange(CommonUtility.getStringParameter(request, "constantEfficiencyRange" + iRatingIndex));
				oTempNewRatingDto.setLtRVId(CommonUtility.getStringParameter(request, "rv" + iRatingIndex));
				oTempNewRatingDto.setLtRAId(CommonUtility.getStringParameter(request, "ra" + iRatingIndex));
				
				oTempNewRatingDto.setLtMethodOfCoupling(CommonUtility.getStringParameter(request, "methodOfCoupling" + iRatingIndex));
				oTempNewRatingDto.setLtMainTB(CommonUtility.getStringParameter(request, "mainTermBox" + iRatingIndex));
				oTempNewRatingDto.setLtTerminalBoxSizeId(CommonUtility.getStringParameter(request, "terminalBoxSize" + iRatingIndex));
				oTempNewRatingDto.setLtAuxTerminalBoxId(CommonUtility.getStringParameter(request, "auxTerminalBox" + iRatingIndex));
				oTempNewRatingDto.setLtShaftTypeId(CommonUtility.getStringParameter(request, "shaftType" + iRatingIndex));
				oTempNewRatingDto.setLtShaftMaterialId(CommonUtility.getStringParameter(request, "shaftMaterialType" + iRatingIndex));
				oTempNewRatingDto.setLtShaftGroundingId(CommonUtility.getStringParameter(request, "shaftGrounding" + iRatingIndex));
				oTempNewRatingDto.setLtBearingSystemId(CommonUtility.getStringParameter(request, "bearingSystem" + iRatingIndex));
				oTempNewRatingDto.setLtBearingNDEId(CommonUtility.getStringParameter(request, "bearingNDE" + iRatingIndex));
				oTempNewRatingDto.setLtBearingNDESize(CommonUtility.getStringParameter(request, "bearingNDESize" + iRatingIndex));
				oTempNewRatingDto.setLtBearingDEId(CommonUtility.getStringParameter(request, "bearingDE" + iRatingIndex));
				oTempNewRatingDto.setLtBearingDESize(CommonUtility.getStringParameter(request, "bearingDESize" + iRatingIndex));
				oTempNewRatingDto.setLtCableEntry(CommonUtility.getStringParameter(request, "cableEntry" + iRatingIndex));
				oTempNewRatingDto.setLtLubrication(CommonUtility.getStringParameter(request, "lubrication" + iRatingIndex));
				
				oTempNewRatingDto.setLtPerfRatedSpeed(CommonUtility.getStringParameter(request, "ratedSpeed" + iRatingIndex));
				oTempNewRatingDto.setLtPerfRatedCurr(CommonUtility.getStringParameter(request, "ratedCurr" + iRatingIndex));
				oTempNewRatingDto.setLtPerfNoLoadCurr(CommonUtility.getStringParameter(request, "noLoadCurr" + iRatingIndex));
				oTempNewRatingDto.setLtPerfLockRotorCurr(CommonUtility.getStringParameter(request, "lockRotorCurr" + iRatingIndex));
				oTempNewRatingDto.setLtPerfNominalTorque(CommonUtility.getStringParameter(request, "nominalTorque" + iRatingIndex));
				oTempNewRatingDto.setLtPerfMaxTorque(CommonUtility.getStringParameter(request, "maxTorque" + iRatingIndex));
				oTempNewRatingDto.setLtPerfLockRotorTorque(CommonUtility.getStringParameter(request, "lockRotorTorque" + iRatingIndex));
				oTempNewRatingDto.setLtPerfSSTHot(CommonUtility.getStringParameter(request, "sstHot" + iRatingIndex));
				oTempNewRatingDto.setLtPerfSSTCold(CommonUtility.getStringParameter(request, "sstCold" + iRatingIndex));
				oTempNewRatingDto.setLtPerfStartTimeRatedVolt(CommonUtility.getStringParameter(request, "startTimeRatedVolt" + iRatingIndex));
				oTempNewRatingDto.setLtPerfStartTime80RatedVolt(CommonUtility.getStringParameter(request, "startTimeAt80RatedVolt" + iRatingIndex));
				oTempNewRatingDto.setLtMotorGD2Value(CommonUtility.getStringParameter(request, "motorGD2" + iRatingIndex));
				oTempNewRatingDto.setLtLoadGD2Value(CommonUtility.getStringParameter(request, "loadGD2" + iRatingIndex));
				oTempNewRatingDto.setLtVibrationId(CommonUtility.getStringParameter(request, "vibration" + iRatingIndex));
				oTempNewRatingDto.setLtMotorWeight(CommonUtility.getStringParameter(request, "totalMotorWeight" + iRatingIndex));
				oTempNewRatingDto.setLtLoadEffPercent100Above(CommonUtility.getStringParameter(request, "effPercentAbove100Load" + iRatingIndex));
				oTempNewRatingDto.setLtLoadEffPercentAt100(CommonUtility.getStringParameter(request, "effPercentAt100Load" + iRatingIndex));
				oTempNewRatingDto.setLtLoadEffPercentAt75(CommonUtility.getStringParameter(request, "effPercentAt75Load" + iRatingIndex));
				oTempNewRatingDto.setLtLoadEffPercentAt50(CommonUtility.getStringParameter(request, "effPercentAt50Load" + iRatingIndex));
				oTempNewRatingDto.setLtLoadPFPercentAt100(CommonUtility.getStringParameter(request, "powerFactorAt100Load" + iRatingIndex));
				oTempNewRatingDto.setLtLoadPFPercentAt75(CommonUtility.getStringParameter(request, "powerFactorAt75Load" + iRatingIndex));
				oTempNewRatingDto.setLtLoadPFPercentAt50(CommonUtility.getStringParameter(request, "powerFactorAt50Load" + iRatingIndex));
				
				oTempNewRatingDto.setLtFlyingLeadWithoutTDId(CommonUtility.getStringParameter(request, "flyingLeadWithoutTB" + iRatingIndex));
				oTempNewRatingDto.setLtFlyingLeadWithTDId(CommonUtility.getStringParameter(request, "flyingLeadWithTB" + iRatingIndex));
				oTempNewRatingDto.setLtHardware(CommonUtility.getStringParameter(request, "hardware" + iRatingIndex));
				oTempNewRatingDto.setLtSpaceHeaterId(CommonUtility.getStringParameter(request, "spaceHeater" + iRatingIndex));
				oTempNewRatingDto.setLtThermisterId(CommonUtility.getStringParameter(request, "thermister" + iRatingIndex));
				oTempNewRatingDto.setLtRTDId(CommonUtility.getStringParameter(request, "rtd" + iRatingIndex));
				oTempNewRatingDto.setLtBTDId(CommonUtility.getStringParameter(request, "btd" + iRatingIndex));
				oTempNewRatingDto.setLtSpreaderBoxId(CommonUtility.getStringParameter(request, "spreaderBox" + iRatingIndex));
				oTempNewRatingDto.setLtCableSealingBoxId(CommonUtility.getStringParameter(request, "cableSealingBox" + iRatingIndex));
				oTempNewRatingDto.setLtGlandPlateId(CommonUtility.getStringParameter(request, "glandPlate" + iRatingIndex));
				oTempNewRatingDto.setLtDoubleCompressionGlandId(CommonUtility.getStringParameter(request, "doubleCompressGland" + iRatingIndex));
				oTempNewRatingDto.setLtDirectionArrowPlateId(CommonUtility.getStringParameter(request, "directionArrowPlate" + iRatingIndex));
				oTempNewRatingDto.setLtAddNamePlateId(CommonUtility.getStringParameter(request, "addlNamePlate" + iRatingIndex));
				oTempNewRatingDto.setLtVibrationProbe(CommonUtility.getStringParameter(request, "vibrationProbeMP" + iRatingIndex));
				oTempNewRatingDto.setLtSPMMountingProvisionId(CommonUtility.getStringParameter(request, "spmMountingProvision" + iRatingIndex));
				oTempNewRatingDto.setLtTechoMounting(CommonUtility.getStringParameter(request, "techoMounting" + iRatingIndex));
				oTempNewRatingDto.setLtMetalFanId(CommonUtility.getStringParameter(request, "metalFan" + iRatingIndex));
				
				oTempNewRatingDto.setLtWitnessRoutineId(CommonUtility.getStringParameter(request, "witnessRoutine" + iRatingIndex));
				oTempNewRatingDto.setLtAdditionalTest1Id(CommonUtility.getStringParameter(request, "additional1Test" + iRatingIndex));
				oTempNewRatingDto.setLtAdditionalTest2Id(CommonUtility.getStringParameter(request, "additional2Test" + iRatingIndex));
				oTempNewRatingDto.setLtAdditionalTest3Id(CommonUtility.getStringParameter(request, "additional3Test" + iRatingIndex));
				oTempNewRatingDto.setLtTypeTestId(CommonUtility.getStringParameter(request, "typeTest" + iRatingIndex));
				oTempNewRatingDto.setLtULCEId(CommonUtility.getStringParameter(request, "ulce" + iRatingIndex));
				oTempNewRatingDto.setLtQAPId(CommonUtility.getStringParameter(request, "qap" + iRatingIndex));
				oTempNewRatingDto.setLtSpares1(CommonUtility.getStringParameter(request, "spare1Rating" + iRatingIndex));
				oTempNewRatingDto.setLtSpares2(CommonUtility.getStringParameter(request, "spare2Rating" + iRatingIndex));
				oTempNewRatingDto.setLtSpares3(CommonUtility.getStringParameter(request, "spare3Rating" + iRatingIndex));
				oTempNewRatingDto.setLtSpares4(CommonUtility.getStringParameter(request, "spare4Rating" + iRatingIndex));
				oTempNewRatingDto.setLtSpares5(CommonUtility.getStringParameter(request, "spare5Rating" + iRatingIndex));
				oTempNewRatingDto.setLtDataSheet(CommonUtility.getStringParameter(request, "dataSheet" + iRatingIndex));
				oTempNewRatingDto.setLtMotorGA(CommonUtility.getStringParameter(request, "motorGA" + iRatingIndex));
				oTempNewRatingDto.setLtPerfCurves(CommonUtility.getStringParameter(request, "perfCurves" + iRatingIndex));
				oTempNewRatingDto.setLtTBoxGA(CommonUtility.getStringParameter(request, "tBoxGA" + iRatingIndex));
				oTempNewRatingDto.setLtAdditionalComments(CommonUtility.getStringParameter(request, "additionalComments" + iRatingIndex));
				
				oTempNewRatingDto.setLtCustomerId(String.valueOf(oNewEnquiryDto.getCustomerId()));
				
				alRatingsList.add(oTempNewRatingDto);
			}
		} catch (Exception e) {
			e.printStackTrace();
			LoggerUtility.log("ERROR: ", this.getClass().getName(), "getMTORatingsList", ExceptionUtility.getStackTraceAsString(e));
		}
		
		LoggerUtility.log("INFO", sClassName, sMethodName, "END");
		return alRatingsList;
	}
	
	public NewRatingDto getRatingObjectFromRequest(Connection conn, QuotationDao oQuotationDao, NewEnquiryDto oNewEnquiryDto, HttpServletRequest request) throws Exception {
		String sMethodName = "getRatingObjectFromRequest"; 
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        NewRatingDto oTempNewRatingDto = new NewRatingDto();
        boolean isEnquirySetToDesign = false;
        
        try {
        	oTempNewRatingDto.setRatingId(Integer.parseInt(CommonUtility.getStringParameter(request, "currentRatingId")));
			oTempNewRatingDto.setEnquiryId(oNewEnquiryDto.getEnquiryId());
			oTempNewRatingDto.setRatingNo(Integer.parseInt(CommonUtility.getStringParameter(request, "currentRatingNum")));
			
			oTempNewRatingDto.setQuantity(Integer.parseInt(CommonUtility.getStringParameter(request, "currQuantity")));
			oTempNewRatingDto.setLtProdLineId(CommonUtility.getStringParameter(request, "currProdLine"));
			oTempNewRatingDto.setLtKWId(CommonUtility.getStringParameter(request, "currKW"));
			oTempNewRatingDto.setLtPoleId(CommonUtility.getStringParameter(request, "currPoles"));
			oTempNewRatingDto.setLtFrameId(CommonUtility.getStringParameter(request, "currFrame"));
			oTempNewRatingDto.setLtFrameSuffixId(CommonUtility.getStringParameter(request, "currFrameSuffix"));
			oTempNewRatingDto.setLtMountingId(CommonUtility.getStringParameter(request, "currMounting"));
			oTempNewRatingDto.setLtTBPositionId(CommonUtility.getStringParameter(request, "currTBPosition"));
			
			oTempNewRatingDto.setTagNumber(CommonUtility.getStringParameter(request, "currTagNo"));
			oTempNewRatingDto.setLtManufacturingLocation(CommonUtility.getStringParameter(request, "currMfgLoc"));
			oTempNewRatingDto.setLtEffClass(CommonUtility.getStringParameter(request, "currEffClass"));
			oTempNewRatingDto.setLtVoltId(CommonUtility.getStringParameter(request, "currVoltage"));
			oTempNewRatingDto.setLtVoltAddVariation(CommonUtility.getStringParameter(request, "currVoltAddVar"));
			oTempNewRatingDto.setLtVoltRemoveVariation(CommonUtility.getStringParameter(request, "currVoltRemVar"));
			oTempNewRatingDto.setLtFreqId(CommonUtility.getStringParameter(request, "currFrequency"));
			oTempNewRatingDto.setLtFreqAddVariation(CommonUtility.getStringParameter(request, "currFreqAddVar"));
			oTempNewRatingDto.setLtFreqRemoveVariation(CommonUtility.getStringParameter(request, "currFreqRemVar"));
			oTempNewRatingDto.setLtCombVariation(CommonUtility.getStringParameter(request, "currCombinedVar"));
			oTempNewRatingDto.setLtServiceFactor(CommonUtility.getStringParameter(request, "currServiceFactor"));
			oTempNewRatingDto.setLtMethodOfStarting(CommonUtility.getStringParameter(request, "currMethodOfStarting"));
			oTempNewRatingDto.setLtStartingCurrentOnDOLStart(CommonUtility.getStringParameter(request, "currStartingCurr"));
			oTempNewRatingDto.setLtAmbTempId(CommonUtility.getStringParameter(request, "currAmbTempAdd"));
			oTempNewRatingDto.setLtAmbTempRemoveId(CommonUtility.getStringParameter(request, "currAmbTempRem"));
			oTempNewRatingDto.setLtTempRise(CommonUtility.getStringParameter(request, "currTempRise"));
			oTempNewRatingDto.setLtInsulationClassId(CommonUtility.getStringParameter(request, "currInsClass"));
			oTempNewRatingDto.setLtForcedCoolingId(CommonUtility.getStringParameter(request, "currMethodOfCool"));
			oTempNewRatingDto.setLtAltitude(CommonUtility.getStringParameter(request, "currAltitude"));
			oTempNewRatingDto.setLtHazardAreaId(CommonUtility.getStringParameter(request, "currHazProtection"));
			oTempNewRatingDto.setLtHazardAreaTypeId(CommonUtility.getStringParameter(request, "currHazAreaType"));
			oTempNewRatingDto.setLtGasGroupId(CommonUtility.getStringParameter(request, "currGasGroup"));
			oTempNewRatingDto.setLtHazardZoneId(CommonUtility.getStringParameter(request, "currHazZone"));
			oTempNewRatingDto.setLtDustGroupId(CommonUtility.getStringParameter(request, "currDustGroup"));
			oTempNewRatingDto.setLtApplicationId(CommonUtility.getStringParameter(request, "currApplication"));
			oTempNewRatingDto.setLtDutyId(CommonUtility.getStringParameter(request, "currDuty"));
			oTempNewRatingDto.setLtCDFId(CommonUtility.getStringParameter(request, "currCDF"));
			oTempNewRatingDto.setLtStartsId(CommonUtility.getStringParameter(request, "currNoOfStarts"));
			oTempNewRatingDto.setLtNoiseLevel(CommonUtility.getStringParameter(request, "currNoiseLevel"));
			oTempNewRatingDto.setLtReplacementMotor(CommonUtility.getStringParameter(request, "currReplaceMotor"));
			oTempNewRatingDto.setLtEarlierMotorSerialNo(CommonUtility.getStringParameter(request, "currEarlierMotorNo"));
			oTempNewRatingDto.setLtStandardDeliveryWks(CommonUtility.getStringParameter(request, "currStdDelivery"));
			oTempNewRatingDto.setLtCustReqDeliveryWks(CommonUtility.getStringParameter(request, "currCustomerDelivery"));
			
			oTempNewRatingDto.setLtRVId(CommonUtility.getStringParameter(request, "currRV"));
			oTempNewRatingDto.setLtRAId(CommonUtility.getStringParameter(request, "currRA"));
			oTempNewRatingDto.setLtWindingConfig(CommonUtility.getStringParameter(request, "currWindingConn"));
			oTempNewRatingDto.setLtWindingTreatmentId(CommonUtility.getStringParameter(request, "windingTreatment"));
			oTempNewRatingDto.setLtLeadId(CommonUtility.getStringParameter(request, "currLeads"));
			oTempNewRatingDto.setLtLoadGD2Value(CommonUtility.getStringParameter(request, "currLoadGD2"));
			oTempNewRatingDto.setLtVFDApplTypeId(CommonUtility.getStringParameter(request, "currVFDAppType"));
			oTempNewRatingDto.setLtVFDMotorSpeedRangeMin(CommonUtility.getStringParameter(request, "currVFDSpeedMin"));
			oTempNewRatingDto.setLtVFDMotorSpeedRangeMax(CommonUtility.getStringParameter(request, "currVFDSpeedMax"));
			oTempNewRatingDto.setLtOverloadingDuty(CommonUtility.getStringParameter(request, "currOverloadDuty"));
			oTempNewRatingDto.setLtConstantEfficiencyRange(CommonUtility.getStringParameter(request, "currConstEffRange"));
			
			oTempNewRatingDto.setLtShaftTypeId(CommonUtility.getStringParameter(request, "currShaftType"));
			oTempNewRatingDto.setLtShaftMaterialId(CommonUtility.getStringParameter(request, "currShaftMaterial"));
			oTempNewRatingDto.setLtDirectionOfRotation(CommonUtility.getStringParameter(request, "currDirOfRotation"));
			//oTempNewRatingDto.setLtStandardRotation(CommonUtility.getStringParameter(request, "standardRotation"));
			oTempNewRatingDto.setLtMethodOfCoupling(CommonUtility.getStringParameter(request, "currCouplingMethod"));
			oTempNewRatingDto.setLtPaintingTypeId(CommonUtility.getStringParameter(request, "currPaintType"));
			oTempNewRatingDto.setLtPaintShadeId(CommonUtility.getStringParameter(request, "currPaintShade"));
			oTempNewRatingDto.setLtPaintShadeValue(CommonUtility.getStringParameter(request, "currPaintShadeValue"));
			oTempNewRatingDto.setLtPaintThicknessId(CommonUtility.getStringParameter(request, "currPaintThickness"));
			oTempNewRatingDto.setLtIPId(CommonUtility.getStringParameter(request, "currIPVal"));
			oTempNewRatingDto.setLtCableSizeId(CommonUtility.getStringParameter(request, "currCableSize"));
			oTempNewRatingDto.setLtNoOfRuns(CommonUtility.getStringParameter(request, "currNoOfRuns"));
			oTempNewRatingDto.setLtNoOfCores(CommonUtility.getStringParameter(request, "currNoOfCores"));
			oTempNewRatingDto.setLtCrossSectionAreaId(CommonUtility.getStringParameter(request, "currCrossSectionArea"));
			oTempNewRatingDto.setLtConductorMaterialId(CommonUtility.getStringParameter(request, "currConductorMat"));
			oTempNewRatingDto.setLtCableDiameter(CommonUtility.getStringParameter(request, "currCableDiameter"));
			oTempNewRatingDto.setLtTerminalBoxSizeId(CommonUtility.getStringParameter(request, "currTBoxSize"));
			oTempNewRatingDto.setLtSpreaderBoxId(CommonUtility.getStringParameter(request, "currSpreaderBox"));
			oTempNewRatingDto.setLtSpaceHeaterId(CommonUtility.getStringParameter(request, "currSpaceHeater"));
			oTempNewRatingDto.setLtVibrationId(CommonUtility.getStringParameter(request, "currVibration"));
			oTempNewRatingDto.setLtFlyingLeadWithoutTDId(CommonUtility.getStringParameter(request, "currFlyLeadWithoutTB"));
			oTempNewRatingDto.setLtFlyingLeadWithTDId(CommonUtility.getStringParameter(request, "currFlyLeadWithTB"));
			oTempNewRatingDto.setLtMetalFanId(CommonUtility.getStringParameter(request, "currFan"));
			oTempNewRatingDto.setLtTechoMounting(CommonUtility.getStringParameter(request, "currEncoderMounting"));
			oTempNewRatingDto.setLtShaftGroundingId(CommonUtility.getStringParameter(request, "currShaftGrounding"));
			
			oTempNewRatingDto.setLtHardware(CommonUtility.getStringParameter(request, "currHardware"));
			oTempNewRatingDto.setLtGlandPlateId(CommonUtility.getStringParameter(request, "currGlandPlate"));
			oTempNewRatingDto.setLtDoubleCompressionGlandId(CommonUtility.getStringParameter(request, "currDoubleCompressGland"));
			oTempNewRatingDto.setLtSPMMountingProvisionId(CommonUtility.getStringParameter(request, "currSPMMountProvision"));
			oTempNewRatingDto.setLtAddNamePlateId(CommonUtility.getStringParameter(request, "currAddlNamePlate"));
			oTempNewRatingDto.setLtDirectionArrowPlateId(CommonUtility.getStringParameter(request, "currDirArrowPlate"));
			oTempNewRatingDto.setLtRTDId(CommonUtility.getStringParameter(request, "currRTD"));
			oTempNewRatingDto.setLtBTDId(CommonUtility.getStringParameter(request, "currBTD"));
			oTempNewRatingDto.setLtThermisterId(CommonUtility.getStringParameter(request, "currThermister"));
			oTempNewRatingDto.setLtAuxTerminalBoxId(CommonUtility.getStringParameter(request, "currAuxTBox"));
			oTempNewRatingDto.setLtCableSealingBoxId(CommonUtility.getStringParameter(request, "currCableSealingBox"));
			
			oTempNewRatingDto.setLtBearingSystemId(CommonUtility.getStringParameter(request, "currBearingSystem"));
			oTempNewRatingDto.setLtBearingNDEId(CommonUtility.getStringParameter(request, "currBearingNDE"));
			oTempNewRatingDto.setLtBearingDEId(CommonUtility.getStringParameter(request, "currBearingDE"));
			
			oTempNewRatingDto.setLtWitnessRoutineId(CommonUtility.getStringParameter(request, "currWitnessTest"));
			oTempNewRatingDto.setLtAdditionalTest1Id(CommonUtility.getStringParameter(request, "currAddlTest1"));
			oTempNewRatingDto.setLtAdditionalTest2Id(CommonUtility.getStringParameter(request, "currAddlTest2"));
			oTempNewRatingDto.setLtAdditionalTest3Id(CommonUtility.getStringParameter(request, "currAddlTest3"));
			oTempNewRatingDto.setLtTypeTestId(CommonUtility.getStringParameter(request, "currTypeTest"));
			oTempNewRatingDto.setLtULCEId(CommonUtility.getStringParameter(request, "currCertification"));
			oTempNewRatingDto.setLtSpares1(CommonUtility.getStringParameter(request, "currSpare1Val"));
			oTempNewRatingDto.setLtSpares2(CommonUtility.getStringParameter(request, "currSpare2Val"));
			oTempNewRatingDto.setLtSpares3(CommonUtility.getStringParameter(request, "currSpare3Val"));
			oTempNewRatingDto.setLtSpares4(CommonUtility.getStringParameter(request, "currSpare4Val"));
			oTempNewRatingDto.setLtSpares5(CommonUtility.getStringParameter(request, "currSpare5Val"));
			oTempNewRatingDto.setLtDataSheet(CommonUtility.getStringParameter(request, "currDatasheet"));
			oTempNewRatingDto.setLtMotorGA(CommonUtility.getStringParameter(request, "currMotorGA"));
			oTempNewRatingDto.setLtPerfCurves(CommonUtility.getStringParameter(request, "currPerfCurve"));
			oTempNewRatingDto.setLtTBoxGA(CommonUtility.getStringParameter(request, "currTBoxGA"));
			oTempNewRatingDto.setLtAdditionalComments(CommonUtility.getStringParameter(request, "currAddlComments"));
			
			oTempNewRatingDto.setLtCustomerId(CommonUtility.getStringParameter(request, "customerIdVal"));
			oTempNewRatingDto.setFinalPriceCheck(false);
			calculateNewEnquiryPrices(oTempNewRatingDto);
			
			// If "Refer Engineering" is "Y" for Current Rating  && If Enquiry is Not Already set to "Refer Engineering".
			if( QuotationConstants.QUOTATION_STRING_Y.equalsIgnoreCase(oTempNewRatingDto.getIsReferDesign()) ) {
				isEnquirySetToDesign = true;
			}
			
			// Check "Refer Engineering" Text fields - If anyone of them has value then - Set Refer isReferDesign = "Y"
			if(StringUtils.isNotBlank(oTempNewRatingDto.getLtEarlierMotorSerialNo())) {
				oTempNewRatingDto.setFlg_Txt_MotorNo_MTO(QuotationConstants.QUOTATION_STRING_Y);
				oTempNewRatingDto.setIsReferDesign(QuotationConstants.QUOTATION_STRING_Y);
				isEnquirySetToDesign = true;
			}
			if(StringUtils.isNotBlank(oTempNewRatingDto.getLtRVId())) {
				oTempNewRatingDto.setFlg_Txt_RV_MTO(QuotationConstants.QUOTATION_STRING_Y);
				oTempNewRatingDto.setIsReferDesign(QuotationConstants.QUOTATION_STRING_Y);
				isEnquirySetToDesign = true;
			}
			if(StringUtils.isNotBlank(oTempNewRatingDto.getLtRAId())) {
				oTempNewRatingDto.setFlg_Txt_RA_MTO(QuotationConstants.QUOTATION_STRING_Y);
				oTempNewRatingDto.setIsReferDesign(QuotationConstants.QUOTATION_STRING_Y);
				isEnquirySetToDesign = true;
			}
			if(StringUtils.isNotBlank(oTempNewRatingDto.getLtLoadGD2Value())) {
				oTempNewRatingDto.setFlg_Txt_LoadGD2_MTO(QuotationConstants.QUOTATION_STRING_Y);
				oTempNewRatingDto.setIsReferDesign(QuotationConstants.QUOTATION_STRING_Y);
				isEnquirySetToDesign = true;
			}
			if(StringUtils.isNotBlank(oTempNewRatingDto.getLtPaintShadeValue())) {
				oTempNewRatingDto.setFlg_Txt_NonSTDPaint_MTO(QuotationConstants.QUOTATION_STRING_Y);
				oTempNewRatingDto.setIsReferDesign(QuotationConstants.QUOTATION_STRING_Y);
				isEnquirySetToDesign = true;
			}
			if(StringUtils.isNotBlank(oTempNewRatingDto.getLtCableSizeId()) && "2".equals(oTempNewRatingDto.getLtCableSizeId().trim())) {
				oTempNewRatingDto.setFlg_Addon_CableEntry_MTO(QuotationConstants.QUOTATION_STRING_Y);
				oTempNewRatingDto.setIsReferDesign(QuotationConstants.QUOTATION_STRING_Y);
				isEnquirySetToDesign = true;
			}
			
			if(isEnquirySetToDesign) {
				oNewEnquiryDto.setIsReferDesign(QuotationConstants.QUOTATION_STRING_Y);
				oQuotationDao.updateSubmitToDesignFlag(conn, oNewEnquiryDto);
			}
			
        } catch (Exception e) {
			e.printStackTrace();
			LoggerUtility.log("ERROR: ", this.getClass().getName(), "getRatingObjectById", ExceptionUtility.getStackTraceAsString(e));
		}
		
		LoggerUtility.log("INFO", sClassName, sMethodName, "END");
		return oTempNewRatingDto;
	}
	
	public NewRatingDto getRatingObjectById(Connection conn, QuotationDao oQuotationDao, NewEnquiryDto oNewEnquiryDto, Integer iRatingId, HttpServletRequest request) throws Exception {
		String sMethodName = "getRatingObjectById"; 
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        NewRatingDto oTempNewRatingDto = new NewRatingDto();
        boolean isEnquirySetToDesign = false;
        
		try {
			if(StringUtils.isBlank(CommonUtility.getStringParameter(request, "ratingObjId" + iRatingId))) {
				oTempNewRatingDto = oQuotationDao.processNewRatingDefaults(conn, oTempNewRatingDto);
				oTempNewRatingDto.setEnquiryId(oNewEnquiryDto.getEnquiryId());
				oTempNewRatingDto.setRatingId(new Integer(0));
				oTempNewRatingDto.setRatingNo(new Integer(1));
			}
			else {
				oTempNewRatingDto.setEnquiryId(oNewEnquiryDto.getEnquiryId());
				oTempNewRatingDto.setRatingId(Integer.parseInt(CommonUtility.getStringParameter(request, "ratingObjId" + iRatingId)));
				oTempNewRatingDto.setRatingNo(Integer.parseInt(CommonUtility.getStringParameter(request, "ratingNo" + iRatingId)));
				
				oTempNewRatingDto.setQuantity(Integer.parseInt(CommonUtility.getStringParameter(request, "quantity" + iRatingId)));
				oTempNewRatingDto.setLtProdLineId(CommonUtility.getStringParameter(request, "productLine" + iRatingId));
				oTempNewRatingDto.setLtKWId(CommonUtility.getStringParameter(request, "kw" + iRatingId));
				oTempNewRatingDto.setLtPoleId(CommonUtility.getStringParameter(request, "pole" + iRatingId));
				oTempNewRatingDto.setLtFrameId(CommonUtility.getStringParameter(request, "frame" + iRatingId));
				oTempNewRatingDto.setLtFrameSuffixId(CommonUtility.getStringParameter(request, "framesuffix" + iRatingId));
				oTempNewRatingDto.setLtMountingId(CommonUtility.getStringParameter(request, "mounting" + iRatingId));
				oTempNewRatingDto.setLtTBPositionId(CommonUtility.getStringParameter(request, "tbPosition" + iRatingId));
				
				oTempNewRatingDto.setTagNumber(CommonUtility.getStringParameter(request, "tagNumber" + iRatingId));
				oTempNewRatingDto.setLtManufacturingLocation(CommonUtility.getStringParameter(request, "mfgLocation" + iRatingId));
				oTempNewRatingDto.setLtEffClass(CommonUtility.getStringParameter(request, "effClass" + iRatingId));
				oTempNewRatingDto.setLtVoltId(CommonUtility.getStringParameter(request, "volt" + iRatingId));
				oTempNewRatingDto.setLtVoltAddVariation(CommonUtility.getStringParameter(request, "voltAddVariation" + iRatingId));
				oTempNewRatingDto.setLtVoltRemoveVariation(CommonUtility.getStringParameter(request, "voltRemoveVariation" + iRatingId));
				oTempNewRatingDto.setLtFreqId(CommonUtility.getStringParameter(request, "freq" + iRatingId));
				oTempNewRatingDto.setLtFreqAddVariation(CommonUtility.getStringParameter(request, "freqAddVariation" + iRatingId));
				oTempNewRatingDto.setLtFreqRemoveVariation(CommonUtility.getStringParameter(request, "freqRemoveVariation" + iRatingId));
				oTempNewRatingDto.setLtCombVariation(CommonUtility.getStringParameter(request, "combinedVariation" + iRatingId));
				oTempNewRatingDto.setLtServiceFactor(CommonUtility.getStringParameter(request, "serviceFactor" + iRatingId));
				oTempNewRatingDto.setLtMethodOfStarting(CommonUtility.getStringParameter(request, "methodOfStarting" + iRatingId));
				oTempNewRatingDto.setLtStartingCurrentOnDOLStart(CommonUtility.getStringParameter(request, "startingCurrOnDOLStarting" + iRatingId));
				oTempNewRatingDto.setLtAmbTempId(CommonUtility.getStringParameter(request, "ambientTemp" + iRatingId));
				oTempNewRatingDto.setLtAmbTempRemoveId(CommonUtility.getStringParameter(request, "ambientTempSub" + iRatingId));
				oTempNewRatingDto.setLtTempRise(CommonUtility.getStringParameter(request, "tempRise" + iRatingId));
				oTempNewRatingDto.setLtInsulationClassId(CommonUtility.getStringParameter(request, "insulationClass" + iRatingId));
				oTempNewRatingDto.setLtForcedCoolingId(CommonUtility.getStringParameter(request, "methodOfCooling" + iRatingId));
				oTempNewRatingDto.setLtAltitude(CommonUtility.getStringParameter(request, "altitude" + iRatingId));
				oTempNewRatingDto.setLtHazardAreaId(CommonUtility.getStringParameter(request, "hazardArea" + iRatingId));
				oTempNewRatingDto.setLtHazardAreaTypeId(CommonUtility.getStringParameter(request, "hazardAreaType" + iRatingId));
				oTempNewRatingDto.setLtGasGroupId(CommonUtility.getStringParameter(request, "gasGroup" + iRatingId));
				oTempNewRatingDto.setLtHazardZoneId(CommonUtility.getStringParameter(request, "hazardZone" + iRatingId));
				oTempNewRatingDto.setLtDustGroupId(CommonUtility.getStringParameter(request, "dustGroup" + iRatingId));
				oTempNewRatingDto.setLtTempClass(CommonUtility.getStringParameter(request, "tempClass" + iRatingId));
				oTempNewRatingDto.setLtApplicationId(CommonUtility.getStringParameter(request, "application" + iRatingId));
				oTempNewRatingDto.setLtDutyId(CommonUtility.getStringParameter(request, "duty" + iRatingId));
				oTempNewRatingDto.setLtCDFId(CommonUtility.getStringParameter(request, "cdf" + iRatingId));
				oTempNewRatingDto.setLtStartsId(CommonUtility.getStringParameter(request, "starts" + iRatingId));
				oTempNewRatingDto.setLtNoiseLevel(CommonUtility.getStringParameter(request, "noiseLevel" + iRatingId));
				oTempNewRatingDto.setLtReplacementMotor(CommonUtility.getStringParameter(request, "isReplacementMotor" + iRatingId));
				oTempNewRatingDto.setLtEarlierMotorSerialNo(CommonUtility.getStringParameter(request, "earlierSuppliedMotorSerialNo" + iRatingId));
				oTempNewRatingDto.setLtStandardDeliveryWks(CommonUtility.getStringParameter(request, "standardDelivery" + iRatingId));
				oTempNewRatingDto.setLtCustReqDeliveryWks(CommonUtility.getStringParameter(request, "customerRequestedDelivery" + iRatingId));
				
				oTempNewRatingDto.setLtRVId(CommonUtility.getStringParameter(request, "rv" + iRatingId));
				oTempNewRatingDto.setLtRAId(CommonUtility.getStringParameter(request, "ra" + iRatingId));
				oTempNewRatingDto.setLtWindingConfig(CommonUtility.getStringParameter(request, "windingConfiguration" + iRatingId));
				oTempNewRatingDto.setLtWindingTreatmentId(CommonUtility.getStringParameter(request, "windingTreatment" + iRatingId));
				//oTempNewRatingDto.setLtWindingWire(CommonUtility.getStringParameter(request, "windingWire" + iRatingId));
				oTempNewRatingDto.setLtLeadId(CommonUtility.getStringParameter(request, "lead" + iRatingId));
				oTempNewRatingDto.setLtLoadGD2Value(CommonUtility.getStringParameter(request, "loadGD2Value" + iRatingId));
				oTempNewRatingDto.setLtVFDApplTypeId(CommonUtility.getStringParameter(request, "vfdType" + iRatingId));
				oTempNewRatingDto.setLtVFDMotorSpeedRangeMin(CommonUtility.getStringParameter(request, "vfdSpeedRangeMin" + iRatingId));
				oTempNewRatingDto.setLtVFDMotorSpeedRangeMax(CommonUtility.getStringParameter(request, "vfdSpeedRangeMax" + iRatingId));
				oTempNewRatingDto.setLtOverloadingDuty(CommonUtility.getStringParameter(request, "overloadingDuty" + iRatingId));
				oTempNewRatingDto.setLtConstantEfficiencyRange(CommonUtility.getStringParameter(request, "constantEfficiencyRange" + iRatingId));
				
				oTempNewRatingDto.setLtShaftTypeId(CommonUtility.getStringParameter(request, "shaftType" + iRatingId));
				oTempNewRatingDto.setLtShaftMaterialId(CommonUtility.getStringParameter(request, "shaftMaterialType" + iRatingId));
				oTempNewRatingDto.setLtDirectionOfRotation(CommonUtility.getStringParameter(request, "directionOfRotation" + iRatingId));
				oTempNewRatingDto.setLtStandardRotation(CommonUtility.getStringParameter(request, "standardRotation" + iRatingId));
				oTempNewRatingDto.setLtMethodOfCoupling(CommonUtility.getStringParameter(request, "methodOfCoupling" + iRatingId));
				oTempNewRatingDto.setLtPaintingTypeId(CommonUtility.getStringParameter(request, "paintingType" + iRatingId));
				oTempNewRatingDto.setLtPaintShadeId(CommonUtility.getStringParameter(request, "paintShade" + iRatingId));
				oTempNewRatingDto.setLtPaintShadeValue(CommonUtility.getStringParameter(request, "paintShadeVal" + iRatingId));
				oTempNewRatingDto.setLtPaintThicknessId(CommonUtility.getStringParameter(request, "paintThickness" + iRatingId));
				oTempNewRatingDto.setLtIPId(CommonUtility.getStringParameter(request, "ip" + iRatingId));
				oTempNewRatingDto.setLtCableSizeId(CommonUtility.getStringParameter(request, "cableSize" + iRatingId));
				oTempNewRatingDto.setLtNoOfRuns(CommonUtility.getStringParameter(request, "noOfRuns" + iRatingId));
				oTempNewRatingDto.setLtNoOfCores(CommonUtility.getStringParameter(request, "noOfCores" + iRatingId));
				oTempNewRatingDto.setLtCrossSectionAreaId(CommonUtility.getStringParameter(request, "crossSectionArea" + iRatingId));
				oTempNewRatingDto.setLtConductorMaterialId(CommonUtility.getStringParameter(request, "conductorMaterial" + iRatingId));
				oTempNewRatingDto.setLtCableDiameter(CommonUtility.getStringParameter(request, "cableDiameter" + iRatingId));
				oTempNewRatingDto.setLtTerminalBoxSizeId(CommonUtility.getStringParameter(request, "terminalBoxSize" + iRatingId));
				oTempNewRatingDto.setLtSpreaderBoxId(CommonUtility.getStringParameter(request, "spreaderBox" + iRatingId));
				oTempNewRatingDto.setLtSpaceHeaterId(CommonUtility.getStringParameter(request, "spaceHeater" + iRatingId));
				oTempNewRatingDto.setLtVibrationId(CommonUtility.getStringParameter(request, "vibration" + iRatingId));
				oTempNewRatingDto.setLtFlyingLeadWithoutTDId(CommonUtility.getStringParameter(request, "flyingLeadWithoutTB" + iRatingId));
				oTempNewRatingDto.setLtFlyingLeadWithTDId(CommonUtility.getStringParameter(request, "flyingLeadWithTB" + iRatingId));
				oTempNewRatingDto.setLtMetalFanId(CommonUtility.getStringParameter(request, "metalFan" + iRatingId));
				oTempNewRatingDto.setLtTechoMounting(CommonUtility.getStringParameter(request, "techoMounting" + iRatingId));
				oTempNewRatingDto.setLtShaftGroundingId(CommonUtility.getStringParameter(request, "shaftGrounding" + iRatingId));
				
				oTempNewRatingDto.setLtHardware(CommonUtility.getStringParameter(request, "hardware" + iRatingId));
				oTempNewRatingDto.setLtGlandPlateId(CommonUtility.getStringParameter(request, "glandPlate" + iRatingId));
				oTempNewRatingDto.setLtDoubleCompressionGlandId(CommonUtility.getStringParameter(request, "doubleCompressGland" + iRatingId));
				oTempNewRatingDto.setLtSPMMountingProvisionId(CommonUtility.getStringParameter(request, "spmMountingProvision" + iRatingId));
				oTempNewRatingDto.setLtAddNamePlateId(CommonUtility.getStringParameter(request, "addlNamePlate" + iRatingId));
				oTempNewRatingDto.setLtDirectionArrowPlateId(CommonUtility.getStringParameter(request, "directionArrowPlate" + iRatingId));
				oTempNewRatingDto.setLtRTDId(CommonUtility.getStringParameter(request, "rtd" + iRatingId));
				oTempNewRatingDto.setLtBTDId(CommonUtility.getStringParameter(request, "btd" + iRatingId));
				oTempNewRatingDto.setLtThermisterId(CommonUtility.getStringParameter(request, "thermister" + iRatingId));
				oTempNewRatingDto.setLtAuxTerminalBoxId(CommonUtility.getStringParameter(request, "auxTerminalBox" + iRatingId));
				oTempNewRatingDto.setLtCableSealingBoxId(CommonUtility.getStringParameter(request, "cableSealingBox" + iRatingId));
				
				oTempNewRatingDto.setLtBearingSystemId(CommonUtility.getStringParameter(request, "bearingSystem" + iRatingId));
				oTempNewRatingDto.setLtBearingNDEId(CommonUtility.getStringParameter(request, "bearingNDE" + iRatingId));
				oTempNewRatingDto.setLtBearingDEId(CommonUtility.getStringParameter(request, "bearingDE" + iRatingId));
				
				oTempNewRatingDto.setLtWitnessRoutineId(CommonUtility.getStringParameter(request, "witnessRoutine" + iRatingId));
				oTempNewRatingDto.setLtAdditionalTest1Id(CommonUtility.getStringParameter(request, "additional1Test" + iRatingId));
				oTempNewRatingDto.setLtAdditionalTest2Id(CommonUtility.getStringParameter(request, "additional2Test" + iRatingId));
				oTempNewRatingDto.setLtAdditionalTest3Id(CommonUtility.getStringParameter(request, "additional3Test" + iRatingId));
				oTempNewRatingDto.setLtTypeTestId(CommonUtility.getStringParameter(request, "typeTest" + iRatingId));
				oTempNewRatingDto.setLtULCEId(CommonUtility.getStringParameter(request, "ulce" + iRatingId));
				oTempNewRatingDto.setLtQAPId(CommonUtility.getStringParameter(request, "qap" + iRatingId));
				oTempNewRatingDto.setLtSpares1(CommonUtility.getStringParameter(request, "spare1Rating" + iRatingId));
				oTempNewRatingDto.setLtSpares2(CommonUtility.getStringParameter(request, "spare2Rating" + iRatingId));
				oTempNewRatingDto.setLtSpares3(CommonUtility.getStringParameter(request, "spare3Rating" + iRatingId));
				oTempNewRatingDto.setLtSpares4(CommonUtility.getStringParameter(request, "spare4Rating" + iRatingId));
				oTempNewRatingDto.setLtSpares5(CommonUtility.getStringParameter(request, "spare5Rating" + iRatingId));
				oTempNewRatingDto.setLtDataSheet(CommonUtility.getStringParameter(request, "dataSheet" + iRatingId));
				oTempNewRatingDto.setLtMotorGA(CommonUtility.getStringParameter(request, "motorGA" + iRatingId));
				oTempNewRatingDto.setLtPerfCurves(CommonUtility.getStringParameter(request, "perfCurves" + iRatingId));
				oTempNewRatingDto.setLtTBoxGA(CommonUtility.getStringParameter(request, "tBoxGA" + iRatingId));
				oTempNewRatingDto.setLtAdditionalComments(CommonUtility.getStringParameter(request, "additionalComments" + iRatingId));
				
				oTempNewRatingDto.setLtCustomerId(String.valueOf(oNewEnquiryDto.getCustomerId()));
				oTempNewRatingDto.setFinalPriceCheck(false);
				calculateNewEnquiryPrices(oTempNewRatingDto);
				
				// If "Refer Engineering" is "Y" for Current Rating  && If Enquiry is Not Already set to "Refer Engineering".
				if( QuotationConstants.QUOTATION_STRING_Y.equalsIgnoreCase(oTempNewRatingDto.getIsReferDesign()) ) {
					isEnquirySetToDesign = true;
				}
				
				// Check "Refer Engineering" Text fields - If anyone of them has value then - Set Refer isReferDesign = "Y"
				if(StringUtils.isNotBlank(oTempNewRatingDto.getLtEarlierMotorSerialNo())) {
					oTempNewRatingDto.setFlg_Txt_MotorNo_MTO(QuotationConstants.QUOTATION_STRING_Y);
					oTempNewRatingDto.setIsReferDesign(QuotationConstants.QUOTATION_STRING_Y);
					isEnquirySetToDesign = true;
				}
				if(StringUtils.isNotBlank(oTempNewRatingDto.getLtRVId())) {
					oTempNewRatingDto.setFlg_Txt_RV_MTO(QuotationConstants.QUOTATION_STRING_Y);
					oTempNewRatingDto.setIsReferDesign(QuotationConstants.QUOTATION_STRING_Y);
					isEnquirySetToDesign = true;
				}
				if(StringUtils.isNotBlank(oTempNewRatingDto.getLtRAId())) {
					oTempNewRatingDto.setFlg_Txt_RA_MTO(QuotationConstants.QUOTATION_STRING_Y);
					oTempNewRatingDto.setIsReferDesign(QuotationConstants.QUOTATION_STRING_Y);
					isEnquirySetToDesign = true;
				}
				if(StringUtils.isNotBlank(oTempNewRatingDto.getLtLoadGD2Value())) {
					oTempNewRatingDto.setFlg_Txt_LoadGD2_MTO(QuotationConstants.QUOTATION_STRING_Y);
					oTempNewRatingDto.setIsReferDesign(QuotationConstants.QUOTATION_STRING_Y);
					isEnquirySetToDesign = true;
				}
				if(StringUtils.isNotBlank(oTempNewRatingDto.getLtPaintShadeValue())) {
					oTempNewRatingDto.setFlg_Txt_NonSTDPaint_MTO(QuotationConstants.QUOTATION_STRING_Y);
					oTempNewRatingDto.setIsReferDesign(QuotationConstants.QUOTATION_STRING_Y);
					isEnquirySetToDesign = true;
				}
				if(StringUtils.isNotBlank(oTempNewRatingDto.getLtCableSizeId()) && "2".equals(oTempNewRatingDto.getLtCableSizeId().trim())) {
					oTempNewRatingDto.setFlg_Addon_CableEntry_MTO(QuotationConstants.QUOTATION_STRING_Y);
					oTempNewRatingDto.setIsReferDesign(QuotationConstants.QUOTATION_STRING_Y);
					isEnquirySetToDesign = true;
				}
				
				if(isEnquirySetToDesign) {
					oNewEnquiryDto.setIsReferDesign(QuotationConstants.QUOTATION_STRING_Y);
					oQuotationDao.updateSubmitToDesignFlag(conn, oNewEnquiryDto);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			LoggerUtility.log("ERROR: ", this.getClass().getName(), "getRatingObjectById", ExceptionUtility.getStackTraceAsString(e));
		}
		
		LoggerUtility.log("INFO", sClassName, sMethodName, "END");
		return oTempNewRatingDto;
	}
	
	/**
	 * Fetching MTO Rating Object by RatingId.
	 * @param conn
	 * @param oQuotationDao
	 * @param oNewEnquiryDto
	 * @param iRatingId
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public NewRatingDto getMTORatingObjectById(Connection conn, QuotationDao oQuotationDao, NewEnquiryDto oNewEnquiryDto, Integer iRatingId, HttpServletRequest request) throws Exception {
		String sMethodName = "getMTORatingObjectById"; 
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        NewRatingDto oTempNewRatingDto = new NewRatingDto();
        boolean isEnquirySetToDesign = false;
        
		try {
			if(StringUtils.isBlank(CommonUtility.getStringParameter(request, "ratingObjId" + iRatingId))) {
				oTempNewRatingDto = oQuotationDao.processNewRatingDefaults(conn, oTempNewRatingDto);
				oTempNewRatingDto.setEnquiryId(oNewEnquiryDto.getEnquiryId());
				oTempNewRatingDto.setRatingId(new Integer(0));
				oTempNewRatingDto.setRatingNo(new Integer(1));
			}
			else {
				oTempNewRatingDto.setEnquiryId(oNewEnquiryDto.getEnquiryId());
				oTempNewRatingDto.setRatingId(Integer.parseInt(CommonUtility.getStringParameter(request, "ratingObjId" + iRatingId)));
				oTempNewRatingDto.setRatingNo(Integer.parseInt(CommonUtility.getStringParameter(request, "ratingNo" + iRatingId)));
				
				oTempNewRatingDto.setModelNumber(CommonUtility.getStringParameter(request, "modelNumber" + iRatingId));
				oTempNewRatingDto.setLtApplicationId(CommonUtility.getStringParameter(request, "application" + iRatingId));
				oTempNewRatingDto.setQuantity(Integer.parseInt(CommonUtility.getStringParameter(request, "quantity" + iRatingId)));
				oTempNewRatingDto.setLtProdLineId(CommonUtility.getStringParameter(request, "productLine" + iRatingId));
				oTempNewRatingDto.setLtKWId(CommonUtility.getStringParameter(request, "kw" + iRatingId));
				oTempNewRatingDto.setLtPoleId(CommonUtility.getStringParameter(request, "pole" + iRatingId));
				oTempNewRatingDto.setLtFrameId(CommonUtility.getStringParameter(request, "frame" + iRatingId));
				oTempNewRatingDto.setLtFrameSuffixId(CommonUtility.getStringParameter(request, "framesuffix" + iRatingId));
				oTempNewRatingDto.setLtMountingId(CommonUtility.getStringParameter(request, "mounting" + iRatingId));
				oTempNewRatingDto.setLtTBPositionId(CommonUtility.getStringParameter(request, "tbPosition" + iRatingId));
				oTempNewRatingDto.setLtEffClass(CommonUtility.getStringParameter(request, "effClass" + iRatingId));
				
				oTempNewRatingDto.setLtReplacementMotor(CommonUtility.getStringParameter(request, "isReplacementMotor" + iRatingId));
				oTempNewRatingDto.setLtEarlierMotorSerialNo(CommonUtility.getStringParameter(request, "earlierSuppliedMotorSerialNo" + iRatingId));
				oTempNewRatingDto.setLtEnclosure(CommonUtility.getStringParameter(request, "enclosure" + iRatingId));
				oTempNewRatingDto.setLtForcedCoolingId(CommonUtility.getStringParameter(request, "methodOfCooling" + iRatingId));
				oTempNewRatingDto.setLtVoltId(CommonUtility.getStringParameter(request, "volt" + iRatingId));
				oTempNewRatingDto.setLtVoltAddVariation(CommonUtility.getStringParameter(request, "voltAddVariation" + iRatingId));
				oTempNewRatingDto.setLtVoltRemoveVariation(CommonUtility.getStringParameter(request, "voltRemoveVariation" + iRatingId));
				oTempNewRatingDto.setLtFreqId(CommonUtility.getStringParameter(request, "freq" + iRatingId));
				oTempNewRatingDto.setLtFreqAddVariation(CommonUtility.getStringParameter(request, "freqAddVariation" + iRatingId));
				oTempNewRatingDto.setLtFreqRemoveVariation(CommonUtility.getStringParameter(request, "freqRemoveVariation" + iRatingId));
				oTempNewRatingDto.setLtCombVariation(CommonUtility.getStringParameter(request, "combinedVariation" + iRatingId));
				oTempNewRatingDto.setLtHazardAreaTypeId(CommonUtility.getStringParameter(request, "hazardAreaType" + iRatingId));
				oTempNewRatingDto.setLtHazardAreaId(CommonUtility.getStringParameter(request, "hazardArea" + iRatingId));
				oTempNewRatingDto.setLtGasGroupId(CommonUtility.getStringParameter(request, "gasGroup" + iRatingId));
				oTempNewRatingDto.setLtHazardZoneId(CommonUtility.getStringParameter(request, "hazardZone" + iRatingId));
				oTempNewRatingDto.setLtDustGroupId(CommonUtility.getStringParameter(request, "dustGroup" + iRatingId));
				oTempNewRatingDto.setLtTempClass(CommonUtility.getStringParameter(request, "tempClass" + iRatingId));
				oTempNewRatingDto.setLtAmbTempId(CommonUtility.getStringParameter(request, "ambientTemp" + iRatingId));
				oTempNewRatingDto.setLtAmbTempRemoveId(CommonUtility.getStringParameter(request, "ambientTempSub" + iRatingId));
				oTempNewRatingDto.setLtInsulationClassId(CommonUtility.getStringParameter(request, "insulationClass" + iRatingId));
				oTempNewRatingDto.setLtTempRise(CommonUtility.getStringParameter(request, "tempRise" + iRatingId));
				oTempNewRatingDto.setLtAltitude(CommonUtility.getStringParameter(request, "altitude" + iRatingId));
				oTempNewRatingDto.setLtIPId(CommonUtility.getStringParameter(request, "ip" + iRatingId));
				oTempNewRatingDto.setLtMethodOfStarting(CommonUtility.getStringParameter(request, "methodOfStarting" + iRatingId));
				oTempNewRatingDto.setLtDirectionOfRotation(CommonUtility.getStringParameter(request, "directionOfRotation" + iRatingId));
				oTempNewRatingDto.setLtStandardRotation(CommonUtility.getStringParameter(request, "standardRotation" + iRatingId));
				oTempNewRatingDto.setLtGreaseType(CommonUtility.getStringParameter(request, "typeOfGrease" + iRatingId));
				oTempNewRatingDto.setLtPaintingTypeId(CommonUtility.getStringParameter(request, "paintingType" + iRatingId));
				oTempNewRatingDto.setLtPaintShadeId(CommonUtility.getStringParameter(request, "paintShade" + iRatingId));
				oTempNewRatingDto.setLtPaintShadeValue(CommonUtility.getStringParameter(request, "paintShadeVal" + iRatingId));
				oTempNewRatingDto.setLtPaintThicknessId(CommonUtility.getStringParameter(request, "paintThickness" + iRatingId));
				oTempNewRatingDto.setLtCableSizeId(CommonUtility.getStringParameter(request, "cableSize" + iRatingId));
				oTempNewRatingDto.setLtNoOfRuns(CommonUtility.getStringParameter(request, "noOfRuns" + iRatingId));
				oTempNewRatingDto.setLtNoOfCores(CommonUtility.getStringParameter(request, "noOfCores" + iRatingId));
				oTempNewRatingDto.setLtCrossSectionAreaId(CommonUtility.getStringParameter(request, "crossSectionArea" + iRatingId));
				oTempNewRatingDto.setLtConductorMaterialId(CommonUtility.getStringParameter(request, "conductorMaterial" + iRatingId));
				oTempNewRatingDto.setLtCableDiameter(CommonUtility.getStringParameter(request, "cableDiameter" + iRatingId));
				oTempNewRatingDto.setLtNoiseLevel(CommonUtility.getStringParameter(request, "noiseLevel" + iRatingId));
				
				oTempNewRatingDto.setLtWindingConfig(CommonUtility.getStringParameter(request, "windingConfiguration" + iRatingId));
				oTempNewRatingDto.setLtWindingWire(CommonUtility.getStringParameter(request, "windingWire" + iRatingId));
				oTempNewRatingDto.setLtWindingTreatmentId(CommonUtility.getStringParameter(request, "windingTreatment" + iRatingId));
				oTempNewRatingDto.setLtDutyId(CommonUtility.getStringParameter(request, "duty" + iRatingId));
				oTempNewRatingDto.setLtCDFId(CommonUtility.getStringParameter(request, "cdf" + iRatingId));
				oTempNewRatingDto.setLtStartsId(CommonUtility.getStringParameter(request, "starts" + iRatingId));
				oTempNewRatingDto.setLtOverloadingDuty(CommonUtility.getStringParameter(request, "overloadingDuty" + iRatingId));
				oTempNewRatingDto.setLtServiceFactor(CommonUtility.getStringParameter(request, "serviceFactor" + iRatingId));
				oTempNewRatingDto.setLtBroughtOutTerminals(CommonUtility.getStringParameter(request, "noBroughtOutTerm" + iRatingId));
				oTempNewRatingDto.setLtLeadId(CommonUtility.getStringParameter(request, "lead" + iRatingId));
				oTempNewRatingDto.setLtStartingCurrentOnDOLStart(CommonUtility.getStringParameter(request, "startingCurrOnDOLStarting" + iRatingId));
				oTempNewRatingDto.setLtVFDApplTypeId(CommonUtility.getStringParameter(request, "vfdType" + iRatingId));
				oTempNewRatingDto.setLtVFDMotorSpeedRangeMin(CommonUtility.getStringParameter(request, "vfdSpeedRangeMin" + iRatingId));
				oTempNewRatingDto.setLtVFDMotorSpeedRangeMax(CommonUtility.getStringParameter(request, "vfdSpeedRangeMax" + iRatingId));
				oTempNewRatingDto.setLtConstantEfficiencyRange(CommonUtility.getStringParameter(request, "constantEfficiencyRange" + iRatingId));
				oTempNewRatingDto.setLtRVId(CommonUtility.getStringParameter(request, "rv" + iRatingId));
				oTempNewRatingDto.setLtRAId(CommonUtility.getStringParameter(request, "ra" + iRatingId));
				
				oTempNewRatingDto.setLtMethodOfCoupling(CommonUtility.getStringParameter(request, "methodOfCoupling" + iRatingId));
				oTempNewRatingDto.setLtMainTB(CommonUtility.getStringParameter(request, "mainTermBox" + iRatingId));
				oTempNewRatingDto.setLtTerminalBoxSizeId(CommonUtility.getStringParameter(request, "terminalBoxSize" + iRatingId));
				oTempNewRatingDto.setLtAuxTerminalBoxId(CommonUtility.getStringParameter(request, "auxTerminalBox" + iRatingId));
				oTempNewRatingDto.setLtShaftTypeId(CommonUtility.getStringParameter(request, "shaftType" + iRatingId));
				oTempNewRatingDto.setLtShaftMaterialId(CommonUtility.getStringParameter(request, "shaftMaterialType" + iRatingId));
				oTempNewRatingDto.setLtShaftGroundingId(CommonUtility.getStringParameter(request, "shaftGrounding" + iRatingId));
				oTempNewRatingDto.setLtBearingSystemId(CommonUtility.getStringParameter(request, "bearingSystem" + iRatingId));
				oTempNewRatingDto.setLtBearingNDEId(CommonUtility.getStringParameter(request, "bearingNDE" + iRatingId));
				oTempNewRatingDto.setLtBearingNDESize(CommonUtility.getStringParameter(request, "bearingNDESize" + iRatingId));
				oTempNewRatingDto.setLtBearingDEId(CommonUtility.getStringParameter(request, "bearingDE" + iRatingId));
				oTempNewRatingDto.setLtBearingDESize(CommonUtility.getStringParameter(request, "bearingDESize" + iRatingId));
				oTempNewRatingDto.setLtCableEntry(CommonUtility.getStringParameter(request, "cableEntry" + iRatingId));
				oTempNewRatingDto.setLtLubrication(CommonUtility.getStringParameter(request, "lubrication" + iRatingId));
				
				oTempNewRatingDto.setLtPerfRatedSpeed(CommonUtility.getStringParameter(request, "ratedSpeed" + iRatingId));
				oTempNewRatingDto.setLtPerfRatedCurr(CommonUtility.getStringParameter(request, "ratedCurr" + iRatingId));
				oTempNewRatingDto.setLtPerfNoLoadCurr(CommonUtility.getStringParameter(request, "noLoadCurr" + iRatingId));
				oTempNewRatingDto.setLtPerfLockRotorCurr(CommonUtility.getStringParameter(request, "lockRotorCurr" + iRatingId));
				oTempNewRatingDto.setLtPerfNominalTorque(CommonUtility.getStringParameter(request, "nominalTorque" + iRatingId));
				oTempNewRatingDto.setLtPerfMaxTorque(CommonUtility.getStringParameter(request, "maxTorque" + iRatingId));
				oTempNewRatingDto.setLtPerfLockRotorTorque(CommonUtility.getStringParameter(request, "lockRotorTorque" + iRatingId));
				oTempNewRatingDto.setLtPerfSSTHot(CommonUtility.getStringParameter(request, "sstHot" + iRatingId));
				oTempNewRatingDto.setLtPerfSSTCold(CommonUtility.getStringParameter(request, "sstCold" + iRatingId));
				oTempNewRatingDto.setLtPerfStartTimeRatedVolt(CommonUtility.getStringParameter(request, "startTimeRatedVolt" + iRatingId));
				oTempNewRatingDto.setLtPerfStartTime80RatedVolt(CommonUtility.getStringParameter(request, "startTimeAt80RatedVolt" + iRatingId));
				oTempNewRatingDto.setLtMotorGD2Value(CommonUtility.getStringParameter(request, "motorGD2" + iRatingId));
				oTempNewRatingDto.setLtLoadGD2Value(CommonUtility.getStringParameter(request, "loadGD2Value" + iRatingId));
				oTempNewRatingDto.setLtVibrationId(CommonUtility.getStringParameter(request, "vibration" + iRatingId));
				oTempNewRatingDto.setLtMotorWeight(CommonUtility.getStringParameter(request, "totalMotorWeight" + iRatingId));
				oTempNewRatingDto.setLtLoadEffPercent100Above(CommonUtility.getStringParameter(request, "effPercentAbove100Load" + iRatingId));
				oTempNewRatingDto.setLtLoadEffPercentAt100(CommonUtility.getStringParameter(request, "effPercentAt100Load" + iRatingId));
				oTempNewRatingDto.setLtLoadEffPercentAt75(CommonUtility.getStringParameter(request, "effPercentAt75Load" + iRatingId));
				oTempNewRatingDto.setLtLoadEffPercentAt50(CommonUtility.getStringParameter(request, "effPercentAt50Load" + iRatingId));
				oTempNewRatingDto.setLtLoadPFPercentAt100(CommonUtility.getStringParameter(request, "powerFactorAt100Load" + iRatingId));
				oTempNewRatingDto.setLtLoadPFPercentAt75(CommonUtility.getStringParameter(request, "powerFactorAt75Load" + iRatingId));
				oTempNewRatingDto.setLtLoadPFPercentAt50(CommonUtility.getStringParameter(request, "powerFactorAt50Load" + iRatingId));
				
				oTempNewRatingDto.setLtFlyingLeadWithoutTDId(CommonUtility.getStringParameter(request, "flyingLeadWithoutTB" + iRatingId));
				oTempNewRatingDto.setLtFlyingLeadWithTDId(CommonUtility.getStringParameter(request, "flyingLeadWithTB" + iRatingId));
				oTempNewRatingDto.setLtHardware(CommonUtility.getStringParameter(request, "hardware" + iRatingId));
				oTempNewRatingDto.setLtSpaceHeaterId(CommonUtility.getStringParameter(request, "spaceHeater" + iRatingId));
				oTempNewRatingDto.setLtThermisterId(CommonUtility.getStringParameter(request, "thermister" + iRatingId));
				oTempNewRatingDto.setLtRTDId(CommonUtility.getStringParameter(request, "rtd" + iRatingId));
				oTempNewRatingDto.setLtBTDId(CommonUtility.getStringParameter(request, "btd" + iRatingId));
				oTempNewRatingDto.setLtSpreaderBoxId(CommonUtility.getStringParameter(request, "spreaderBox" + iRatingId));
				oTempNewRatingDto.setLtCableSealingBoxId(CommonUtility.getStringParameter(request, "cableSealingBox" + iRatingId));
				oTempNewRatingDto.setLtGlandPlateId(CommonUtility.getStringParameter(request, "glandPlate" + iRatingId));
				oTempNewRatingDto.setLtDoubleCompressionGlandId(CommonUtility.getStringParameter(request, "doubleCompressGland" + iRatingId));
				oTempNewRatingDto.setLtDirectionArrowPlateId(CommonUtility.getStringParameter(request, "directionArrowPlate" + iRatingId));
				oTempNewRatingDto.setLtAddNamePlateId(CommonUtility.getStringParameter(request, "addlNamePlate" + iRatingId));
				oTempNewRatingDto.setLtVibrationProbe(CommonUtility.getStringParameter(request, "vibrationProbeMP" + iRatingId));
				oTempNewRatingDto.setLtSPMMountingProvisionId(CommonUtility.getStringParameter(request, "spmMountingProvision" + iRatingId));
				oTempNewRatingDto.setLtTechoMounting(CommonUtility.getStringParameter(request, "techoMounting" + iRatingId));
				oTempNewRatingDto.setLtMetalFanId(CommonUtility.getStringParameter(request, "metalFan" + iRatingId));
				
				oTempNewRatingDto.setLtWitnessRoutineId(CommonUtility.getStringParameter(request, "witnessRoutine" + iRatingId));
				oTempNewRatingDto.setLtAdditionalTest1Id(CommonUtility.getStringParameter(request, "additional1Test" + iRatingId));
				oTempNewRatingDto.setLtAdditionalTest2Id(CommonUtility.getStringParameter(request, "additional2Test" + iRatingId));
				oTempNewRatingDto.setLtAdditionalTest3Id(CommonUtility.getStringParameter(request, "additional3Test" + iRatingId));
				oTempNewRatingDto.setLtTypeTestId(CommonUtility.getStringParameter(request, "typeTest" + iRatingId));
				oTempNewRatingDto.setLtULCEId(CommonUtility.getStringParameter(request, "ulce" + iRatingId));
				oTempNewRatingDto.setLtQAPId(CommonUtility.getStringParameter(request, "qap" + iRatingId));
				oTempNewRatingDto.setLtSpares1(CommonUtility.getStringParameter(request, "spare1Rating" + iRatingId));
				oTempNewRatingDto.setLtSpares2(CommonUtility.getStringParameter(request, "spare2Rating" + iRatingId));
				oTempNewRatingDto.setLtSpares3(CommonUtility.getStringParameter(request, "spare3Rating" + iRatingId));
				oTempNewRatingDto.setLtSpares4(CommonUtility.getStringParameter(request, "spare4Rating" + iRatingId));
				oTempNewRatingDto.setLtSpares5(CommonUtility.getStringParameter(request, "spare5Rating" + iRatingId));
				oTempNewRatingDto.setLtDataSheet(CommonUtility.getStringParameter(request, "dataSheet" + iRatingId));
				oTempNewRatingDto.setLtMotorGA(CommonUtility.getStringParameter(request, "motorGA" + iRatingId));
				oTempNewRatingDto.setLtPerfCurves(CommonUtility.getStringParameter(request, "perfCurves" + iRatingId));
				oTempNewRatingDto.setLtTBoxGA(CommonUtility.getStringParameter(request, "tBoxGA" + iRatingId));
				oTempNewRatingDto.setLtAdditionalComments(CommonUtility.getStringParameter(request, "additionalComments" + iRatingId));
				
				oTempNewRatingDto.setLtCustomerId(String.valueOf(oNewEnquiryDto.getCustomerId()));
			}
		} catch (Exception e) {
			e.printStackTrace();
			LoggerUtility.log("ERROR: ", this.getClass().getName(), "getRatingObjectById", ExceptionUtility.getStackTraceAsString(e));
		}
		
		LoggerUtility.log("INFO", sClassName, sMethodName, "END");
		return oTempNewRatingDto;
	}
	
	public NewRatingDto getSelectedRatingFieldValues(Connection conn, QuotationDao oQuotationDao, NewRatingDto oNewRatingDto, HttpServletRequest request) {
		String sMethodName = "getSelectedRatingFieldValues";
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        NewRatingDto oTempNewRatingDto = new NewRatingDto();
        
        oTempNewRatingDto.setRatingId(oNewRatingDto.getRatingId());
        
        oTempNewRatingDto.setQuantity(Integer.parseInt(CommonUtility.getStringParameter(request, "quantity" )));
        oTempNewRatingDto.setLtProdGroupId(CommonUtility.getStringParameter(request, "productGroup" ));
		oTempNewRatingDto.setLtProdLineId(CommonUtility.getStringParameter(request, "productLine" ));
		oTempNewRatingDto.setLtMotorTypeId(CommonUtility.getStringParameter(request, "typeOfMotor" ));
		oTempNewRatingDto.setLtKWId(CommonUtility.getStringParameter(request, "kw" ));
		oTempNewRatingDto.setLtPoleId(CommonUtility.getStringParameter(request, "pole" ));
		oTempNewRatingDto.setLtFrameId(CommonUtility.getStringParameter(request, "frame" ));
		oTempNewRatingDto.setLtFrameSuffixId(CommonUtility.getStringParameter(request, "framesuffix" ));
		
		oTempNewRatingDto.setLtReplacementMotor(CommonUtility.getStringParameter(request, "isReplacementMotor" ));
		oTempNewRatingDto.setLtEarlierMotorSerialNo(CommonUtility.getStringParameter(request, "earlierSuppliedMotorSerialNo" ));
		
		oTempNewRatingDto.setLtMountingId(CommonUtility.getStringParameter(request, "mounting" ));
		oTempNewRatingDto.setLtTBPositionId(CommonUtility.getStringParameter(request, "tbPosition" ));
		oTempNewRatingDto.setLtVoltId(CommonUtility.getStringParameter(request, "volt" ));
		oTempNewRatingDto.setLtVoltAddVariation(CommonUtility.getStringParameter(request, "voltAddVariation" ));
		oTempNewRatingDto.setLtVoltRemoveVariation(CommonUtility.getStringParameter(request, "voltRemoveVariation" ));
		oTempNewRatingDto.setLtFreqId(CommonUtility.getStringParameter(request, "freq" ));
		oTempNewRatingDto.setLtFreqAddVariation(CommonUtility.getStringParameter(request, "freqAddVariation" ));
		oTempNewRatingDto.setLtFreqRemoveVariation(CommonUtility.getStringParameter(request, "freqRemoveVariation" ));
		oTempNewRatingDto.setLtCombVariation(CommonUtility.getStringParameter(request, "combinedVariation" ));
		oTempNewRatingDto.setLtServiceFactor(CommonUtility.getStringParameter(request, "serviceFactor" ));
		oTempNewRatingDto.setLtMethodOfStarting(CommonUtility.getStringParameter(request, "methodOfStarting" ));
		oTempNewRatingDto.setLtStartingCurrentOnDOLStart(CommonUtility.getStringParameter(request, "startingCurrOnDOLStarting" ));
		oTempNewRatingDto.setLtAmbTempId(CommonUtility.getStringParameter(request, "ambientTemp" ));
		oTempNewRatingDto.setLtAmbTempRemoveId(CommonUtility.getStringParameter(request, "ambientTempSub" ));
		oTempNewRatingDto.setLtTempRise(CommonUtility.getStringParameter(request, "tempRise" ));
		oTempNewRatingDto.setLtInsulationClassId(CommonUtility.getStringParameter(request, "insulationClass" ));
		oTempNewRatingDto.setLtForcedCoolingId(CommonUtility.getStringParameter(request, "methodOfCooling" ));
		oTempNewRatingDto.setLtAltitude(CommonUtility.getStringParameter(request, "altitude" ));
		oTempNewRatingDto.setLtHazardAreaId(CommonUtility.getStringParameter(request, "hazardArea" ));
		oTempNewRatingDto.setLtHazardAreaTypeId(CommonUtility.getStringParameter(request, "hazardAreaType" ));
		oTempNewRatingDto.setLtGasGroupId(CommonUtility.getStringParameter(request, "gasGroup" ));
		oTempNewRatingDto.setLtHazardZoneId(CommonUtility.getStringParameter(request, "hazardZone" ));
		oTempNewRatingDto.setLtDustGroupId(CommonUtility.getStringParameter(request, "dustGroup" ));
		oTempNewRatingDto.setLtApplicationId(CommonUtility.getStringParameter(request, "application" ));
		oTempNewRatingDto.setLtDutyId(CommonUtility.getStringParameter(request, "duty" ));
		oTempNewRatingDto.setLtCDFId(CommonUtility.getStringParameter(request, "cdf" ));
		oTempNewRatingDto.setLtStartsId(CommonUtility.getStringParameter(request, "starts" ));
		oTempNewRatingDto.setLtNoiseLevel(CommonUtility.getStringParameter(request, "noiseLevel" ));
		
		oTempNewRatingDto.setLtRVId(CommonUtility.getStringParameter(request, "rv" ));
		oTempNewRatingDto.setLtRAId(CommonUtility.getStringParameter(request, "ra" ));
		oTempNewRatingDto.setLtWindingTreatmentId(CommonUtility.getStringParameter(request, "windingTreatment" ));
		oTempNewRatingDto.setLtWindingWire(CommonUtility.getStringParameter(request, "windingWire" ));
		oTempNewRatingDto.setLtLeadId(CommonUtility.getStringParameter(request, "lead" ));
		oTempNewRatingDto.setLtWindingConfig(CommonUtility.getStringParameter(request, "windingConfiguration" ));
		oTempNewRatingDto.setLtLoadGD2Value(CommonUtility.getStringParameter(request, "loadGD2Value" ));
		oTempNewRatingDto.setLtVFDApplTypeId(CommonUtility.getStringParameter(request, "vfdType" ));
		oTempNewRatingDto.setLtVFDMotorSpeedRangeMin(CommonUtility.getStringParameter(request, "vfdSpeedRangeMin" ));
		oTempNewRatingDto.setLtVFDMotorSpeedRangeMax(CommonUtility.getStringParameter(request, "vfdSpeedRangeMax" ));
		oTempNewRatingDto.setLtOverloadingDuty(CommonUtility.getStringParameter(request, "overloadingDuty" ));
		oTempNewRatingDto.setLtConstantEfficiencyRange(CommonUtility.getStringParameter(request, "constantEfficiencyRange" ));
		
		oTempNewRatingDto.setLtShaftTypeId(CommonUtility.getStringParameter(request, "shaftType" ));
		oTempNewRatingDto.setLtShaftMaterialId(CommonUtility.getStringParameter(request, "shaftMaterialType" ));
		oTempNewRatingDto.setLtDirectionOfRotation(CommonUtility.getStringParameter(request, "directionOfRotation" ));
		oTempNewRatingDto.setLtStandardRotation(CommonUtility.getStringParameter(request, "standardRotation" ));
		oTempNewRatingDto.setLtMethodOfCoupling(CommonUtility.getStringParameter(request, "methodOfCoupling" ));
		oTempNewRatingDto.setLtPaintingTypeId(CommonUtility.getStringParameter(request, "paintingType" ));
		oTempNewRatingDto.setLtPaintShadeId(CommonUtility.getStringParameter(request, "paintShade" ));
		oTempNewRatingDto.setLtPaintShadeValue(CommonUtility.getStringParameter(request, "paintShadeVal" ));
		oTempNewRatingDto.setLtPaintThicknessId(CommonUtility.getStringParameter(request, "paintThickness" ));
		oTempNewRatingDto.setLtIPId(CommonUtility.getStringParameter(request, "ip" ));
		oTempNewRatingDto.setLtCableSizeId(CommonUtility.getStringParameter(request, "cableSize" ));
		oTempNewRatingDto.setLtNoOfRuns(CommonUtility.getStringParameter(request, "noOfRuns" ));
		oTempNewRatingDto.setLtNoOfCores(CommonUtility.getStringParameter(request, "noOfCores" ));
		oTempNewRatingDto.setLtCrossSectionAreaId(CommonUtility.getStringParameter(request, "crossSectionArea" ));
		oTempNewRatingDto.setLtConductorMaterialId(CommonUtility.getStringParameter(request, "conductorMaterial" ));
		oTempNewRatingDto.setLtCableDiameter(CommonUtility.getStringParameter(request, "cableDiameter" ));
		oTempNewRatingDto.setLtTerminalBoxSizeId(CommonUtility.getStringParameter(request, "terminalBoxSize" ));
		oTempNewRatingDto.setLtSpreaderBoxId(CommonUtility.getStringParameter(request, "spreaderBox" ));
		oTempNewRatingDto.setLtSpaceHeaterId(CommonUtility.getStringParameter(request, "spaceHeater" ));
		oTempNewRatingDto.setLtVibrationId(CommonUtility.getStringParameter(request, "vibration" ));
		oTempNewRatingDto.setLtFlyingLeadWithoutTDId(CommonUtility.getStringParameter(request, "flyingLeadWithoutTB" ));
		oTempNewRatingDto.setLtFlyingLeadWithTDId(CommonUtility.getStringParameter(request, "flyingLeadWithTB" ));
		oTempNewRatingDto.setLtMetalFanId(CommonUtility.getStringParameter(request, "metalFan" ));
		oTempNewRatingDto.setLtTechoMounting(CommonUtility.getStringParameter(request, "techoMounting" ));
		oTempNewRatingDto.setLtShaftGroundingId(CommonUtility.getStringParameter(request, "shaftGrounding" ));
		
		oTempNewRatingDto.setLtHardware(CommonUtility.getStringParameter(request, "hardware" ));
		oTempNewRatingDto.setLtGlandPlateId(CommonUtility.getStringParameter(request, "glandPlate" ));
		oTempNewRatingDto.setLtDoubleCompressionGlandId(CommonUtility.getStringParameter(request, "doubleCompressGland" ));
		oTempNewRatingDto.setLtSPMMountingProvisionId(CommonUtility.getStringParameter(request, "spmMountingProvision" ));
		oTempNewRatingDto.setLtAddNamePlateId(CommonUtility.getStringParameter(request, "addlNamePlate" ));
		oTempNewRatingDto.setLtDirectionArrowPlateId(CommonUtility.getStringParameter(request, "directionArrowPlate" ));
		oTempNewRatingDto.setLtRTDId(CommonUtility.getStringParameter(request, "rtd" ));
		oTempNewRatingDto.setLtBTDId(CommonUtility.getStringParameter(request, "btd" ));
		oTempNewRatingDto.setLtThermisterId(CommonUtility.getStringParameter(request, "thermister" ));
		oTempNewRatingDto.setLtAuxTerminalBoxId(CommonUtility.getStringParameter(request, "auxTerminalBox" ));
		oTempNewRatingDto.setLtCableSealingBoxId(CommonUtility.getStringParameter(request, "cableSealingBox" ));
		
		oTempNewRatingDto.setLtBearingSystemId(CommonUtility.getStringParameter(request, "bearingSystem" ));
		oTempNewRatingDto.setLtBearingNDEId(CommonUtility.getStringParameter(request, "bearingNDE" ));
		oTempNewRatingDto.setLtBearingDEId(CommonUtility.getStringParameter(request, "bearingDE" ));
		
		oTempNewRatingDto.setLtWitnessRoutineId(CommonUtility.getStringParameter(request, "witnessRoutine" ));
		oTempNewRatingDto.setLtAdditionalTest1Id(CommonUtility.getStringParameter(request, "additional1Test" ));
		oTempNewRatingDto.setLtAdditionalTest2Id(CommonUtility.getStringParameter(request, "additional2Test" ));
		oTempNewRatingDto.setLtAdditionalTest3Id(CommonUtility.getStringParameter(request, "additional3Test" ));
		oTempNewRatingDto.setLtTypeTestId(CommonUtility.getStringParameter(request, "typeTest" ));
		oTempNewRatingDto.setLtULCEId(CommonUtility.getStringParameter(request, "ulce" ));
		oTempNewRatingDto.setLtSpares1(CommonUtility.getStringParameter(request, "spare1Rating" ));
		oTempNewRatingDto.setLtSpares2(CommonUtility.getStringParameter(request, "spare2Rating" ));
		oTempNewRatingDto.setLtSpares3(CommonUtility.getStringParameter(request, "spare3Rating" ));
		oTempNewRatingDto.setLtSpares4(CommonUtility.getStringParameter(request, "spare4Rating" ));
		oTempNewRatingDto.setLtSpares5(CommonUtility.getStringParameter(request, "spare5Rating" ));
		
		oTempNewRatingDto.setLtDataSheet(CommonUtility.getStringParameter(request, "dataSheet" ));
		oTempNewRatingDto.setLtMotorGA(CommonUtility.getStringParameter(request, "motorGA" ));
		oTempNewRatingDto.setLtPerfCurves(CommonUtility.getStringParameter(request, "perfCurves" ));
		oTempNewRatingDto.setLtTBoxGA(CommonUtility.getStringParameter(request, "tBoxGA" ));
		
		oTempNewRatingDto.setLtCustomerId(oNewRatingDto.getLtCustomerId());
		
		// Fetch Design Page Fields.
		oTempNewRatingDto.setLtProdGroupId_MTO(CommonUtility.getStringParameter(request, "productGroup_MTO" ));
		oTempNewRatingDto.setLtProdLineId_MTO(CommonUtility.getStringParameter(request, "productLine_MTO" ));
		oTempNewRatingDto.setLtKWId_MTO(CommonUtility.getStringParameter(request, "kw_MTO" ));
		oTempNewRatingDto.setLtFrameId_MTO(CommonUtility.getStringParameter(request, "frame_MTO" ));
		oTempNewRatingDto.setLtPoleId_MTO(CommonUtility.getStringParameter(request, "pole_MTO" ));
		oTempNewRatingDto.setLtPercentCopper_MTO(CommonUtility.getStringParameter(request, "percentCopper_MTO" ));
		oTempNewRatingDto.setLtPercentStamping_MTO(CommonUtility.getStringParameter(request, "percentStamping_MTO" ));
		oTempNewRatingDto.setLtPercentAluminium_MTO(CommonUtility.getStringParameter(request, "percentAluminium_MTO" ));
		oTempNewRatingDto.setLtSpecialAddOn_MTO(CommonUtility.getStringParameter(request, "specialAddOn_MTO" ));
		oTempNewRatingDto.setLtSpecialCashExtra_MTO(CommonUtility.getStringParameter(request, "specialCashExtra_MTO" ));
		oTempNewRatingDto.setLtTotalAddOn_MTO(CommonUtility.getStringParameter(request, "totalAddOn_MTO" ));
		oTempNewRatingDto.setLtTotalCashExtra_MTO(CommonUtility.getStringParameter(request, "totalCashExtra_MTO" ));
		oTempNewRatingDto.setLtMaterialCost_MTO(CommonUtility.getStringParameter(request, "matCost_MTO" ));
		
		/*try {
			calculateNewEnquiryDesignMTOPrices(oTempNewRatingDto);
			
		} catch (Exception e) {
			e.printStackTrace();
			LoggerUtility.log("ERROR: ", sClassName, sMethodName, ExceptionUtility.getStackTraceAsString(e));
		}*/
		
		LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        return oTempNewRatingDto;
	}
	
	
	
	/**
	 * This Method is used to calculate NewEnquiry prices.
	 * @param oNewRatingDto NewRatingDto Bean which contains the required params to Calculate Price.
	 * @throws Exception if any error occurs while performing database operations, etc
	 */
	public void calculateNewEnquiryPrices(NewRatingDto oNewRatingDto) throws Exception {
		
		int quantity = 0;
		
		double standardPricePerUnit=0d, standardTotalPrice=0d, quotedPricePerUnit=0d, quotedTotalPrice=0d;
		double requestedPricePerUnit_SM=0d, requestedTotalPrice_SM=0d, requestedPricePerUnit_RSM=0d, requestedTotalPrice_RSM=0d, requestedPricePerUnit_NSM=0d, requestedTotalPrice_NSM=0d;
		double approvedPricePerUnit_RSM=0d, approvedTotalPrice_RSM=0d, approvedPricePerUnit_NSM=0d, approvedTotalPrice_NSM=0d, approvedPricePerUnit_MH=0d, approvedTotalPrice_MH=0d; 
		long standardPricePerUnitRound=0l, standardTotalPriceRound=0l, quotedPricePerUnitRound=0l, quotedTotalPriceRound=0l;
		long requestedPricePerUnitRound_SM=0l, requestedTotalPriceRound_SM=0l, requestedPricePerUnitRound_RSM=0l, requestedTotalPriceRound_RSM=0l, requestedPricePerUnitRound_NSM=0l, requestedTotalPriceRound_NSM=0l; 
		long approvedPricePerUnitRound_RSM=0l, approvedTotalPriceRound_RSM=0l, approvedPricePerUnitRound_NSM=0l, approvedTotalPriceRound_NSM=0l, approvedPricePerUnitRound_MH=0l, approvedTotalPriceRound_MH=0l; 
		
		double materialCost = 0d, loh = 0d, totalCost = 0d, profitMargin = 0d;
		double lpPerUnit = 0d, lpWithAddOnPerUnit = 0d, totalAddOnPercent = 0d, totalCashExtra = 0d;
		long lpPerUnitRound = 0l, lpWithAddOnPerUnitRound = 0l, totalAddOnPercentRound = 0l, totalCashExtraRound = 0l;
		double standardDiscount = 0d;
		
		double requestedDiscount_SM = StringUtils.isNotBlank(oNewRatingDto.getLtRequestedDiscount_SM()) ? Double.parseDouble(oNewRatingDto.getLtRequestedDiscount_SM()) : 0d;
		double requestedDiscount_RSM = StringUtils.isNotBlank(oNewRatingDto.getLtRequestedDiscount_RSM()) ? Double.parseDouble(oNewRatingDto.getLtRequestedDiscount_RSM()) : 0d;
		double requestedDiscount_NSM = StringUtils.isNotBlank(oNewRatingDto.getLtRequestedDiscount_NSM()) ? Double.parseDouble(oNewRatingDto.getLtRequestedDiscount_NSM()) : 0d;
				
		double approvedDiscount_RSM = StringUtils.isNotBlank(oNewRatingDto.getLtApprovedDiscount_RSM()) ? Double.parseDouble(oNewRatingDto.getLtApprovedDiscount_RSM()) : 0d;
		double approvedDiscount_NSM = StringUtils.isNotBlank(oNewRatingDto.getLtApprovedDiscount_NSM()) ? Double.parseDouble(oNewRatingDto.getLtApprovedDiscount_NSM()) : 0d;
		double approvedDiscount_MH = StringUtils.isNotBlank(oNewRatingDto.getLtApprovedDiscount_MH()) ? Double.parseDouble(oNewRatingDto.getLtApprovedDiscount_MH()) : 0d;
		
		double quotedDiscount = StringUtils.isNotBlank(oNewRatingDto.getLtQuotedDiscount()) ? Double.parseDouble(oNewRatingDto.getLtQuotedDiscount()) : 0d;
		
		DaoFactory oDaoFactory = null; //Creating an instance of of DaoFactory & retrieving UserDao object
        QuotationDao oQuotationDao = null;// Creating an instance of QuotationDao
        AddOnDetailsDao oAddOnDetailsDao = null;
        ListPriseDao oListPriceDao = null;
        CustomerDiscountDao oCustomerDiscountDao = null;
        DBUtility oDBUtility = new DBUtility();
		 
        try (Connection conn = oDBUtility.getDBConnection()) {
        	oDaoFactory = new DaoFactory();
        	
        	oQuotationDao = oDaoFactory.getQuotationDao();
        	oAddOnDetailsDao = oDaoFactory.getAddOnDetailsDao();
        	oListPriceDao = oDaoFactory.getListPriseDao();
        	oCustomerDiscountDao = oDaoFactory.getCustomerDiscountDao();
        	
        	//################################################ Check If Rating is DESIGN COMPLETE ########################################################
        	boolean isDesignComplete = oQuotationDao.checkIsNewRatingDesignComplete(conn, oNewRatingDto);
        	
        	if(isDesignComplete) {
        		NewRatingDto oTempNewRatingDto = oQuotationDao.fetchNewRatingByIdandNo(conn, oNewRatingDto);
        		
        		oTempNewRatingDto.setLtCustomerId(oNewRatingDto.getLtCustomerId());
        		
        		//  Set The Values.
        		lpPerUnit = StringUtils.isNotBlank(oTempNewRatingDto.getLtLPPerUnit()) ? CurrencyConvertUtility.parsePriceString(oTempNewRatingDto.getLtLPPerUnit()) : 0d;
        		lpPerUnitRound = Math.round(lpPerUnit);
        		// Material Cost, LOH.
        		
        		// Customer Discount values.
        		standardDiscount = StringUtils.isNotBlank(oTempNewRatingDto.getLtStandardDiscount()) ? CurrencyConvertUtility.parsePriceString(oTempNewRatingDto.getLtStandardDiscount()) : 0d;
        		
        		// Add-On Details.
            	totalAddOnPercent = StringUtils.isNotBlank(oTempNewRatingDto.getLtTotalAddonPercent()) ? CurrencyConvertUtility.parsePriceString(oTempNewRatingDto.getLtTotalAddonPercent()) : 0d;
    			totalAddOnPercentRound = Math.round(totalAddOnPercent);
    			totalCashExtra = StringUtils.isNotBlank(oTempNewRatingDto.getLtTotalCashExtra()) ? CurrencyConvertUtility.parsePriceString(oTempNewRatingDto.getLtTotalCashExtra()) : 0d;
    			totalCashExtraRound = Math.round(totalCashExtra);
        	}
        	
        	if(!isDesignComplete) {
        		// List Price values.
            	ListPriceDto oListPriceDto = buildListPriceSearchFilter(conn, oQuotationDao, oNewRatingDto);
            	oListPriceDto = oListPriceDao.fetchListPrice(conn, oListPriceDto);
            	lpPerUnit = oListPriceDto.getListPriceValue();
    			lpPerUnitRound = Math.round(lpPerUnit);
    			// Material Cost, LOH.
    			materialCost = StringUtils.isNotBlank(oListPriceDto.getMaterial_cost()) ? Double.parseDouble(oListPriceDto.getMaterial_cost()) : 0d;
    			loh = StringUtils.isNotBlank(oListPriceDto.getLoh()) ? Double.parseDouble(oListPriceDto.getLoh()) : 0d;
    			
    			// Customer Discount values.
    			CustomerDiscountDto  oCustomerDiscountDto = buildCustDiscountSearchFilter(conn, oQuotationDao, oNewRatingDto);
    			standardDiscount = oCustomerDiscountDao.fetchCustomerDiscount(conn, oCustomerDiscountDto);
    			
            	// Add-On Price Details.
            	AddOnDetailsDto oAddOnDetailsDto = buildAddOnDetailsSearchFilter(conn, oQuotationDao, oNewRatingDto);
            	
            	// For Final Price Check Logic.
            	if(oNewRatingDto.isFinalPriceCheck()) {
            		// Set MTO Highlight Flag to Empty String.
            		oNewRatingDto.setMtoHighlightFlag(QuotationConstants.QUOTATION_EMPTY);
            		// Setting "Refer to Design" Flag - If Standard Unit Price is not Found in DB.
        			if(lpPerUnitRound == 0) {
        				oNewRatingDto.setIsReferDesign(QuotationConstants.QUOTATION_STRING_Y);
        				oNewRatingDto.setFlg_PriceFields_MTO(QuotationConstants.QUOTATION_STRING_Y);
        				// Setting the Highlighting Flag - "0" if Price Fields Flag is "Y".
        				oNewRatingDto.setMtoHighlightFlag(QuotationConstants.QUOTATION_ZERO);
        			} else {
        				oNewRatingDto.setIsReferDesign(QuotationConstants.QUOTATION_STRING_N);
        				oNewRatingDto.setFlg_PriceFields_MTO(QuotationConstants.QUOTATION_STRING_N);
        			}
        			oAddOnDetailsDto.setMtoHighlightFlag(oNewRatingDto.getMtoHighlightFlag());
        			
            		// Fetches Total Add-On Percent / Cash Extra - Alos List of All Add-On's and MTO's
            		oAddOnDetailsDto = processAddOnDetails(conn, oAddOnDetailsDao, oAddOnDetailsDto);
            		// Assign MTO and AddOn Details to NewRatingDto.
            		oNewRatingDto.setNewMTOTechDetailsList(oAddOnDetailsDto.getNewMTOTechDetailsList());
                	oNewRatingDto.setNewMTOAddOnsList(oAddOnDetailsDto.getNewMTOAddOnsList());
                	// Assign MTO Highlight Flag to NewRatingDto.
                	oNewRatingDto.setMtoHighlightFlag(oAddOnDetailsDto.getMtoHighlightFlag());
                	
                	// Check MTO Tech Details List - If Not Empty then set the NewRatingDto.IsReferDesign = 'Y'
                	if(CollectionUtils.isNotEmpty(oNewRatingDto.getNewMTOTechDetailsList()) || QuotationConstants.QUOTATION_STRING_Y.equalsIgnoreCase(oNewRatingDto.getFlg_PriceFields_MTO()) ) {
                		oNewRatingDto.setIsReferDesign(QuotationConstants.QUOTATION_STRING_Y);
        			} else {
        				oNewRatingDto.setIsReferDesign(QuotationConstants.QUOTATION_STRING_N);
        			}
            	} else {
            		// Fetches only Total Add-On Percent / Cash Extra Details.
            		oAddOnDetailsDto = processAddOnPriceDetails(conn, oAddOnDetailsDao, oAddOnDetailsDto);
            	}
            	
            	// Add-On Details.
            	totalAddOnPercent = oAddOnDetailsDto.getTotalAddOnPercentValue();
    			totalAddOnPercentRound = Math.round(totalAddOnPercent);
    			totalCashExtra = oAddOnDetailsDto.getTotalAddOnCashExtraValue();
    			totalCashExtraRound = Math.round(totalCashExtra);
        	}
        	
			// Quantity
			quantity = oNewRatingDto.getQuantity();
			
			lpWithAddOnPerUnit = (lpPerUnit + (lpPerUnit * totalAddOnPercent/100)) + totalCashExtra;
			lpWithAddOnPerUnitRound = Math.round(lpWithAddOnPerUnit);
			
			standardPricePerUnit = ((lpPerUnit + (lpPerUnit * totalAddOnPercent/100)) * (1 - standardDiscount/100)) + totalCashExtra;
			standardPricePerUnitRound = Math.round(standardPricePerUnit);
			
			requestedPricePerUnit_SM = ((lpPerUnit + (lpPerUnit * totalAddOnPercent/100)) * (1 - requestedDiscount_SM/100)) + totalCashExtra;
			requestedPricePerUnitRound_SM = Math.round(requestedPricePerUnit_SM);
			requestedPricePerUnit_RSM = ((lpPerUnit + (lpPerUnit * totalAddOnPercent/100)) * (1 - requestedDiscount_RSM/100)) + totalCashExtra;
			requestedPricePerUnitRound_RSM = Math.round(requestedPricePerUnit_RSM);
			requestedPricePerUnit_NSM = ((lpPerUnit + (lpPerUnit * totalAddOnPercent/100)) * (1 - requestedDiscount_NSM/100)) + totalCashExtra;
			requestedPricePerUnitRound_NSM = Math.round(requestedPricePerUnit_NSM);
			
			approvedPricePerUnit_RSM = ((lpPerUnit + (lpPerUnit * totalAddOnPercent/100)) * (1 - approvedDiscount_RSM/100)) + totalCashExtra;
			approvedPricePerUnitRound_RSM = Math.round(approvedPricePerUnit_RSM);
			approvedPricePerUnit_NSM = ((lpPerUnit + (lpPerUnit * totalAddOnPercent/100)) * (1 - approvedDiscount_NSM/100)) + totalCashExtra;
			approvedPricePerUnitRound_NSM = Math.round(approvedPricePerUnit_NSM);
			approvedPricePerUnit_MH = ((lpPerUnit + (lpPerUnit * totalAddOnPercent/100)) * (1 - approvedDiscount_MH/100)) + totalCashExtra;
			approvedPricePerUnitRound_MH = Math.round(approvedPricePerUnit_MH);
			
			quotedPricePerUnit = ((lpPerUnit + (lpPerUnit * totalAddOnPercent/100)) * (1 - quotedDiscount/100)) + totalCashExtra;
			quotedPricePerUnitRound = Math.round(quotedPricePerUnit);
			
			standardTotalPrice = quantity * standardPricePerUnit; 
			standardTotalPriceRound = Math.round(standardTotalPrice);
			
			requestedTotalPrice_SM = quantity * requestedPricePerUnit_SM; requestedTotalPriceRound_SM = Math.round(requestedTotalPrice_SM);
			requestedTotalPrice_RSM = quantity * requestedPricePerUnit_RSM; requestedTotalPriceRound_RSM = Math.round(requestedTotalPrice_RSM);
			requestedTotalPrice_NSM = quantity * requestedPricePerUnit_NSM; requestedTotalPriceRound_NSM = Math.round(requestedTotalPrice_NSM);
			
			approvedTotalPrice_RSM = quantity * approvedPricePerUnit_RSM; approvedTotalPriceRound_RSM = Math.round(approvedTotalPrice_RSM);
			approvedTotalPrice_NSM = quantity * approvedPricePerUnit_NSM; approvedTotalPriceRound_NSM = Math.round(approvedTotalPrice_NSM);
			approvedTotalPrice_MH = quantity * approvedPricePerUnit_MH; approvedTotalPriceRound_MH = Math.round(approvedTotalPrice_MH);
			
			quotedTotalPrice = quantity * quotedPricePerUnit; quotedTotalPriceRound = Math.round(quotedTotalPrice);
			
			oNewRatingDto.setLtTotalAddonPercent(String.valueOf(totalAddOnPercentRound));
			oNewRatingDto.setLtTotalCashExtra(CurrencyConvertUtility.buildPriceFormat(String.valueOf(totalCashExtraRound)));
			oNewRatingDto.setLtLPPerUnit(CurrencyConvertUtility.buildPriceFormat(String.valueOf(lpPerUnitRound)));
			oNewRatingDto.setLtLPWithAddonPerUnit(CurrencyConvertUtility.buildPriceFormat(String.valueOf(lpWithAddOnPerUnitRound)));
			
			
			if(lpPerUnitRound == 0) {
				oNewRatingDto.setLtStandardDiscount("");
				oNewRatingDto.setLtPricePerUnit("");
				oNewRatingDto.setLtTotalOrderPrice("");
			} else {
				oNewRatingDto.setLtStandardDiscount(String.valueOf((double)standardDiscount));
				oNewRatingDto.setLtPricePerUnit(CurrencyConvertUtility.buildPriceFormat(String.valueOf(standardPricePerUnitRound)));
				oNewRatingDto.setLtTotalOrderPrice(CurrencyConvertUtility.buildPriceFormat(String.valueOf(standardTotalPriceRound)));
			}
			
			// ##### Total Cost = Material Cost + LOH. #############################################################################################
				totalCost = materialCost + loh;
			// ##### Profit Margin (%) =  1 - (Total Cost + (0.8 * Total-Add-on %) + (0.8 * Total Cash) ) / Total Price  ###########################
				if(totalCost > 0)
					profitMargin = (1 - ((totalCost + (0.8 * totalAddOnPercent) + (0.8 * totalCashExtra) ) / standardTotalPrice)) * 100;
			
				oNewRatingDto.setRatingMaterialCost(CommonUtility.round(materialCost, QuotationConstants.QUOTATION_PRICE_DECIMALCOUNT));
				oNewRatingDto.setRatingLOH(CommonUtility.round(loh, QuotationConstants.QUOTATION_PRICE_DECIMALCOUNT));
				oNewRatingDto.setRatingTotalCost(CommonUtility.round(totalCost, QuotationConstants.QUOTATION_PRICE_DECIMALCOUNT));
				oNewRatingDto.setRatingProfitMargin(CommonUtility.round(profitMargin, QuotationConstants.QUOTATION_PRICE_DECIMALCOUNT));
			// #####################################################################################################################################
			
			if(requestedDiscount_SM == 0) {
				oNewRatingDto.setLtRequestedDiscount_SM("");
				oNewRatingDto.setLtRequestedPricePerUnit_SM("");
				oNewRatingDto.setLtRequestedTotalPrice_SM("");
			} else {
				oNewRatingDto.setLtRequestedDiscount_SM(String.valueOf((double)requestedDiscount_SM));
				oNewRatingDto.setLtRequestedPricePerUnit_SM(CurrencyConvertUtility.buildPriceFormat(String.valueOf(requestedPricePerUnitRound_SM)));
				oNewRatingDto.setLtRequestedTotalPrice_SM(CurrencyConvertUtility.buildPriceFormat(String.valueOf(requestedTotalPriceRound_SM)));
			}
			if(requestedDiscount_RSM == 0) {
				oNewRatingDto.setLtRequestedDiscount_RSM("");
				oNewRatingDto.setLtRequestedPricePerUnit_RSM("");
				oNewRatingDto.setLtRequestedTotalPrice_RSM("");
			} else {
				oNewRatingDto.setLtRequestedDiscount_RSM(String.valueOf((double)requestedDiscount_RSM));
				oNewRatingDto.setLtRequestedPricePerUnit_RSM(CurrencyConvertUtility.buildPriceFormat(String.valueOf(requestedPricePerUnitRound_RSM)));
				oNewRatingDto.setLtRequestedTotalPrice_RSM(CurrencyConvertUtility.buildPriceFormat(String.valueOf(requestedTotalPriceRound_RSM)));
			}
			if(requestedDiscount_NSM == 0) {
				oNewRatingDto.setLtRequestedDiscount_NSM("");
				oNewRatingDto.setLtRequestedPricePerUnit_NSM("");
				oNewRatingDto.setLtRequestedTotalPrice_NSM("");
			} else {
				oNewRatingDto.setLtRequestedDiscount_NSM(String.valueOf((double)requestedDiscount_NSM));
				oNewRatingDto.setLtRequestedPricePerUnit_NSM(CurrencyConvertUtility.buildPriceFormat(String.valueOf(requestedPricePerUnitRound_NSM)));
				oNewRatingDto.setLtRequestedTotalPrice_NSM(CurrencyConvertUtility.buildPriceFormat(String.valueOf(requestedTotalPriceRound_NSM)));
			}
			
			if(approvedDiscount_RSM == 0) {
				oNewRatingDto.setLtApprovedDiscount_RSM("");
				oNewRatingDto.setLtApprovedPricePerunit_RSM("");
				oNewRatingDto.setLtApprovedTotalPrice_RSM("");
			} else {
				oNewRatingDto.setLtApprovedDiscount_RSM(String.valueOf((double)approvedDiscount_RSM));
				oNewRatingDto.setLtApprovedPricePerunit_RSM(CurrencyConvertUtility.buildPriceFormat(String.valueOf(approvedPricePerUnitRound_RSM)));
				oNewRatingDto.setLtApprovedTotalPrice_RSM(CurrencyConvertUtility.buildPriceFormat(String.valueOf(approvedTotalPriceRound_RSM)));
			}
			if(approvedDiscount_NSM == 0) {
				oNewRatingDto.setLtApprovedDiscount_NSM("");
				oNewRatingDto.setLtApprovedPricePerunit_NSM("");
				oNewRatingDto.setLtApprovedTotalPrice_NSM("");
			} else {
				oNewRatingDto.setLtApprovedDiscount_NSM(String.valueOf((double)approvedDiscount_NSM));
				oNewRatingDto.setLtApprovedPricePerunit_NSM(CurrencyConvertUtility.buildPriceFormat(String.valueOf(approvedPricePerUnitRound_NSM)));
				oNewRatingDto.setLtApprovedTotalPrice_NSM(CurrencyConvertUtility.buildPriceFormat(String.valueOf(approvedTotalPriceRound_NSM)));
			}
			if(approvedDiscount_MH == 0) {
				oNewRatingDto.setLtApprovedDiscount_MH("");
				oNewRatingDto.setLtApprovedPricePerunit_MH("");
				oNewRatingDto.setLtApprovedTotalPrice_MH("");
			} else {
				oNewRatingDto.setLtApprovedDiscount_MH(String.valueOf((double)approvedDiscount_MH));
				oNewRatingDto.setLtApprovedPricePerunit_MH(CurrencyConvertUtility.buildPriceFormat(String.valueOf(approvedPricePerUnitRound_MH)));
				oNewRatingDto.setLtApprovedTotalPrice_MH(CurrencyConvertUtility.buildPriceFormat(String.valueOf(approvedTotalPriceRound_MH)));
			}
			
			if(quotedDiscount == 0) {
				oNewRatingDto.setLtQuotedDiscount("");
				oNewRatingDto.setLtQuotedPricePerUnit("");
				oNewRatingDto.setLtQuotedTotalPrice("");
			} else {
				oNewRatingDto.setLtQuotedDiscount(String.valueOf((double)quotedDiscount));
				oNewRatingDto.setLtQuotedPricePerUnit(CurrencyConvertUtility.buildPriceFormat(String.valueOf(quotedPricePerUnitRound)));
				oNewRatingDto.setLtQuotedTotalPrice(CurrencyConvertUtility.buildPriceFormat(String.valueOf(quotedTotalPriceRound)));
			}
			
        }
	}
	
	
	public void processMTORatingPrice(NewRatingDto oNewRatingDto, NewMTORatingDto oNewMTORatingDto, ListPriceDto oListPriceDto) throws Exception {
		
		int quantity = 0;
		
		double standardPricePerUnit=0d, standardTotalPrice=0d, quotedPricePerUnit=0d, quotedTotalPrice=0d;
		double requestedPricePerUnit_SM=0d, requestedTotalPrice_SM=0d, requestedPricePerUnit_RSM=0d, requestedTotalPrice_RSM=0d, requestedPricePerUnit_NSM=0d, requestedTotalPrice_NSM=0d;
		double approvedPricePerUnit_RSM=0d, approvedTotalPrice_RSM=0d, approvedPricePerUnit_NSM=0d, approvedTotalPrice_NSM=0d, approvedPricePerUnit_MH=0d, approvedTotalPrice_MH=0d; 
		long standardPricePerUnitRound=0l, standardTotalPriceRound=0l, quotedPricePerUnitRound=0l, quotedTotalPriceRound=0l;
		long requestedPricePerUnitRound_SM=0l, requestedTotalPriceRound_SM=0l, requestedPricePerUnitRound_RSM=0l, requestedTotalPriceRound_RSM=0l, requestedPricePerUnitRound_NSM=0l, requestedTotalPriceRound_NSM=0l; 
		long approvedPricePerUnitRound_RSM=0l, approvedTotalPriceRound_RSM=0l, approvedPricePerUnitRound_NSM=0l, approvedTotalPriceRound_NSM=0l, approvedPricePerUnitRound_MH=0l, approvedTotalPriceRound_MH=0l; 
		
		double materialCost = 0d, loh = 0d, totalCost = 0d, profitMargin = 0d;
		double lpPerUnit = 0d, lpWithAddOnPerUnit = 0d, totalAddOnPercent = 0d, totalCashExtra = 0d;
		long lpPerUnitRound = 0l, lpWithAddOnPerUnitRound = 0l, totalAddOnPercentRound = 0l, totalCashExtraRound = 0l;
		double standardDiscount = 0d;
		
		double requestedDiscount_SM = StringUtils.isNotBlank(oNewRatingDto.getLtRequestedDiscount_SM()) ? Double.parseDouble(oNewRatingDto.getLtRequestedDiscount_SM()) : 0d;
		double requestedDiscount_RSM = StringUtils.isNotBlank(oNewRatingDto.getLtRequestedDiscount_RSM()) ? Double.parseDouble(oNewRatingDto.getLtRequestedDiscount_RSM()) : 0d;
		double requestedDiscount_NSM = StringUtils.isNotBlank(oNewRatingDto.getLtRequestedDiscount_NSM()) ? Double.parseDouble(oNewRatingDto.getLtRequestedDiscount_NSM()) : 0d;
				
		double approvedDiscount_RSM = StringUtils.isNotBlank(oNewRatingDto.getLtApprovedDiscount_RSM()) ? Double.parseDouble(oNewRatingDto.getLtApprovedDiscount_RSM()) : 0d;
		double approvedDiscount_NSM = StringUtils.isNotBlank(oNewRatingDto.getLtApprovedDiscount_NSM()) ? Double.parseDouble(oNewRatingDto.getLtApprovedDiscount_NSM()) : 0d;
		double approvedDiscount_MH = StringUtils.isNotBlank(oNewRatingDto.getLtApprovedDiscount_MH()) ? Double.parseDouble(oNewRatingDto.getLtApprovedDiscount_MH()) : 0d;
		
		double quotedDiscount = StringUtils.isNotBlank(oNewRatingDto.getLtQuotedDiscount()) ? Double.parseDouble(oNewRatingDto.getLtQuotedDiscount()) : 0d;
		
		DaoFactory oDaoFactory = null; //Creating an instance of of DaoFactory & retrieving UserDao object
        QuotationDao oQuotationDao = null;// Creating an instance of QuotationDao
        AddOnDetailsDao oAddOnDetailsDao = null;
        ListPriseDao oListPriceDao = null;
        CustomerDiscountDao oCustomerDiscountDao = null;
        DBUtility oDBUtility = new DBUtility();
		
        try (Connection conn = oDBUtility.getDBConnection()) {
        	oDaoFactory = new DaoFactory();
        	
        	oQuotationDao = oDaoFactory.getQuotationDao();
        	oListPriceDao = oDaoFactory.getListPriseDao();
        	oCustomerDiscountDao = oDaoFactory.getCustomerDiscountDao();
        	
        	// List Price values.
        	oListPriceDto = oListPriceDao.fetchListPrice(conn, oListPriceDto);
        	lpPerUnit = oListPriceDto.getListPriceValue();
			lpPerUnitRound = Math.round(lpPerUnit);
			// Material Cost, LOH.
			materialCost = StringUtils.isNotBlank(oListPriceDto.getMaterial_cost()) ? Double.parseDouble(oListPriceDto.getMaterial_cost()) : 0d;
			loh = StringUtils.isNotBlank(oListPriceDto.getLoh()) ? Double.parseDouble(oListPriceDto.getLoh()) : 0d;
			
        	// Customer Discount values.
        	CustomerDiscountDto  oCustomerDiscountDto = buildCustDiscountSearchFilter(conn, oQuotationDao, oNewRatingDto);
        	standardDiscount = oCustomerDiscountDao.fetchCustomerDiscount(conn, oCustomerDiscountDto);
        	
        	// Quantity
        	quantity = oNewRatingDto.getQuantity();
        	
        	if(StringUtils.isNotBlank(oNewMTORatingDto.getMtoTotalAddOnPercent())) {
        		totalAddOnPercent = Double.parseDouble(oNewMTORatingDto.getMtoTotalAddOnPercent());
        	}
			totalAddOnPercentRound = Math.round(totalAddOnPercent);
			if(StringUtils.isNotBlank(oNewMTORatingDto.getMtoTotalCashExtra())) {
				totalCashExtra = Double.parseDouble(oNewMTORatingDto.getMtoTotalCashExtra());
			}
			totalCashExtraRound = Math.round(totalCashExtra);
			
			lpWithAddOnPerUnit = (lpPerUnit + (lpPerUnit * totalAddOnPercent/100)) + totalCashExtra;
			lpWithAddOnPerUnitRound = Math.round(lpWithAddOnPerUnit);
			
			standardPricePerUnit = ((lpPerUnit + (lpPerUnit * totalAddOnPercent/100)) * (1 - standardDiscount/100)) + totalCashExtra;
			standardPricePerUnitRound = Math.round(standardPricePerUnit);
			
			standardTotalPrice = quantity * standardPricePerUnit; 
			standardTotalPriceRound = Math.round(standardTotalPrice);
			
			// Setting the values to NewRatingDto.
			oNewRatingDto.setLtTotalAddonPercent(String.valueOf(totalAddOnPercentRound));
			oNewRatingDto.setLtTotalCashExtra(CurrencyConvertUtility.buildPriceFormat(String.valueOf(totalCashExtraRound)));
			oNewRatingDto.setLtLPPerUnit(CurrencyConvertUtility.buildPriceFormat(String.valueOf(lpPerUnitRound)));
			oNewRatingDto.setLtLPWithAddonPerUnit(CurrencyConvertUtility.buildPriceFormat(String.valueOf(lpWithAddOnPerUnitRound)));
			if(lpPerUnitRound == 0) {
				oNewRatingDto.setLtStandardDiscount("");
				oNewRatingDto.setLtPricePerUnit("");
				oNewRatingDto.setLtTotalOrderPrice("");
			} else {
				oNewRatingDto.setLtStandardDiscount(String.valueOf((double)standardDiscount));
				oNewRatingDto.setLtPricePerUnit(CurrencyConvertUtility.buildPriceFormat(String.valueOf(standardPricePerUnitRound)));
				oNewRatingDto.setLtTotalOrderPrice(CurrencyConvertUtility.buildPriceFormat(String.valueOf(standardTotalPriceRound)));
			}
			
			// ##### Total Cost = Material Cost + LOH. #############################################################################################
			totalCost = materialCost + loh;
			// ##### Profit Margin (%) =  1 - (Total Cost + (0.8 * Total-Add-on %) + (0.8 * Total Cash) ) / Total Price  ###########################
			if(totalCost > 0)
				profitMargin = (1 - ((totalCost + (0.8 * totalAddOnPercent) + (0.8 * totalCashExtra) ) / standardTotalPrice)) * 100;
		
			oNewRatingDto.setRatingMaterialCost(CommonUtility.round(materialCost, QuotationConstants.QUOTATION_PRICE_DECIMALCOUNT));
			oNewRatingDto.setRatingLOH(CommonUtility.round(loh, QuotationConstants.QUOTATION_PRICE_DECIMALCOUNT));
			oNewRatingDto.setRatingTotalCost(CommonUtility.round(totalCost, QuotationConstants.QUOTATION_PRICE_DECIMALCOUNT));
			oNewRatingDto.setRatingProfitMargin(CommonUtility.round(profitMargin, QuotationConstants.QUOTATION_PRICE_DECIMALCOUNT));
			// #####################################################################################################################################
			
			// Save NewRatingDto MTO Price values and Add-On values.
			oNewRatingDto.setIsDesignComplete("Y");
			oQuotationDao.updateNewRatingMTOPriceDetails(conn, oNewRatingDto);
			
			// Save NewMTORatingDto values.
			oQuotationDao.updateNewMTORatingDetails(conn, oNewMTORatingDto);
			
        }
	}
	
	public ListPriceDto buildListPriceSearchFilter(Connection conn, QuotationDao oQuotationDao, NewRatingDto oNewRatingDto) {
		String sMethodName = "buildListPriceSearchFilter"; 
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
		
        String frameTypeVal = "";
		ListPriceDto oListPriceDto = new ListPriceDto();
	
		try {
			if (oNewRatingDto.getLtManufacturingLocation() != null) {
				if(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtManufacturingLocation())) {
					oListPriceDto.setManufacturing_location("");
				} else if(QuotationConstants.QUOTATION_STRING_ONE.equals(oNewRatingDto.getLtManufacturingLocation())) {
					oListPriceDto.setManufacturing_location("Ahmedabad");
				} else if(QuotationConstants.QUOTATION_STRING_TWO.equals(oNewRatingDto.getLtManufacturingLocation())) {
					oListPriceDto.setManufacturing_location("Kolkata");
				}
			}
			
			if(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtProdLineId())) {
				oListPriceDto.setProduct_line("");
			} else {
				oListPriceDto.setProduct_line(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_PRODLINE, oNewRatingDto.getLtProdLineId()));
			}
			
			if(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtKWId())) {
				oListPriceDto.setPower_rating_kw("");
			} else {
				oListPriceDto.setPower_rating_kw(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_KW, oNewRatingDto.getLtKWId()));
			}
			
			if(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtFrameId())) {
				oListPriceDto.setFrame_type("");
			} else {
				frameTypeVal = CommonUtility.fetchBaseFrameType(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_FRAME, oNewRatingDto.getLtFrameId()));
				oListPriceDto.setFrame_type(frameTypeVal);
			}
			
			if(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtFrameSuffixId())) {
				oListPriceDto.setFrame_suffix("");
			} else {
				oListPriceDto.setFrame_suffix(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_FRAME_SUFFIX, oNewRatingDto.getLtFrameSuffixId()));
			}
			
			if(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtPoleId())) {
				oListPriceDto.setNumber_of_poles(new Integer(0));
			} else {
				oListPriceDto.setNumber_of_poles(Integer.parseInt(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_POLE, oNewRatingDto.getLtPoleId())));
			}
			
			if(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtMountingId())) {
				oListPriceDto.setMounting_type("");
			} else {
				oListPriceDto.setMounting_type(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_MOUNTING, oNewRatingDto.getLtMountingId()));
			}
			
			if(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtTBPositionId())) {
				oListPriceDto.setTb_position("");
			} else {
				oListPriceDto.setTb_position(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_TBPOS, oNewRatingDto.getLtTBPositionId()));
			}
			
		} catch(Exception e) {
			LoggerUtility.log("ERROR: ", sClassName, sMethodName, ExceptionUtility.getStackTraceAsString(e));
		}
		
		LoggerUtility.log("INFO", sClassName, sMethodName, "END");
		return oListPriceDto;
	}
	
	
	public AddOnDetailsDto buildAddOnDetailsSearchFilter(Connection conn, QuotationDao oQuotationDao, NewRatingDto oNewRatingDto) {
		String sMethodName = "buildAddOnDetailsSearchFilter"; 
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
		
        String sAddOnFeatureKey = "";
        String sAddOnFeatureValue = "";
        String frameTypeVal = "";
        HashMap<String, String> addOnSpecialFeatureMap = new HashMap<String, String>();
        AddOnDetailsDto oAddOnDetailsDto = new AddOnDetailsDto();
		
		try {
			// Mandatory Condition Checks.
			if(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtProdLineId())) {
				oAddOnDetailsDto.setAddOnProdLine("");
			} else {
				oAddOnDetailsDto.setAddOnProdLine(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_PRODLINE, oNewRatingDto.getLtProdLineId()));
			}
			if(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtFrameId())) {
				oAddOnDetailsDto.setAddOnFrameSize("");
			} else {
				frameTypeVal = CommonUtility.fetchBaseFrameType(oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_FRAME, oNewRatingDto.getLtFrameId()));
				oAddOnDetailsDto.setAddOnFrameSize(frameTypeVal);
			}
			oAddOnDetailsDto.setAddOnKW(oNewRatingDto.getLtKWId());
			oAddOnDetailsDto.setAddOnPole(oNewRatingDto.getLtPoleId());
			oAddOnDetailsDto.setAddOnFrameSuffix(oNewRatingDto.getLtFrameSuffixId());
			oAddOnDetailsDto.setAddOnMounting(oNewRatingDto.getLtMountingId());
			oAddOnDetailsDto.setAddOnTBPos(oNewRatingDto.getLtTBPositionId());
			oAddOnDetailsDto.setAddOnMfgLoc(oNewRatingDto.getLtManufacturingLocation());
			
			/* *************************************************************************************************************************************  */
			/* :::::::::::::::::::::::::::::::::::::::::::::::::::::  Add On Values  :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::  */
			
			if(StringUtils.isNotBlank(oNewRatingDto.getLtMountingId()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtMountingId()))) {
				sAddOnFeatureKey = "Mounting";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_MOUNTING, oNewRatingDto.getLtMountingId());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtPaintingTypeId()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtPaintingTypeId()))) {
				sAddOnFeatureKey = "Painting Type";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_PAINT_TYPE, oNewRatingDto.getLtPaintingTypeId());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtPaintShadeId()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtPaintShadeId()))) {
				sAddOnFeatureKey = "Paint Shade";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_PAINT_SHADE, oNewRatingDto.getLtPaintShadeId());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtPaintThicknessId()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtPaintThicknessId()))) {
				sAddOnFeatureKey = "Paint Thickness";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_PAINT_THICKNESS, oNewRatingDto.getLtPaintThicknessId());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtInsulationClassId()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtInsulationClassId()))) {
				sAddOnFeatureKey = "Insulation Class";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_INSULATION_CLASS, oNewRatingDto.getLtInsulationClassId());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtTerminalBoxSizeId()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtTerminalBoxSizeId()))) {
				sAddOnFeatureKey = "Terminal Box Size";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_TERMINAL_BOX, oNewRatingDto.getLtTerminalBoxSizeId());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtSpreaderBoxId()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtSpreaderBoxId()))) {
				sAddOnFeatureKey = "Spreader Box";
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, oNewRatingDto.getLtSpreaderBoxId());
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtAuxTerminalBoxId()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtAuxTerminalBoxId()))) {
				sAddOnFeatureKey = "Aux TB";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_AUX_TERMINAL_BOX, oNewRatingDto.getLtAuxTerminalBoxId());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtVibrationId()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtVibrationId()))) {
				sAddOnFeatureKey = "Vibration Level";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_VIBRATION, oNewRatingDto.getLtVibrationId());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtFlyingLeadWithoutTDId()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtFlyingLeadWithoutTDId()))) {
				sAddOnFeatureKey = "Flying Lead Without TB";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_FLYING_LEAD, oNewRatingDto.getLtFlyingLeadWithoutTDId());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtFlyingLeadWithTDId()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtFlyingLeadWithTDId()))) {
				sAddOnFeatureKey = "Flying Lead With TB";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_FLYING_LEAD, oNewRatingDto.getLtFlyingLeadWithTDId());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtAddNamePlateId()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtAddNamePlateId()))) {
				sAddOnFeatureKey = "Addl Name Plate";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_ADD_NAME_PLATE, oNewRatingDto.getLtAddNamePlateId());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtDirectionArrowPlateId()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtDirectionArrowPlateId()))) {
				sAddOnFeatureKey = "Direction Arrow Plate";
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, oNewRatingDto.getLtDirectionArrowPlateId());
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtWarrantyId()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtWarrantyId()))) {
				sAddOnFeatureKey = "Warranty";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_WARRANTY, oNewRatingDto.getLtWarrantyId());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtAdditionalTest1Id()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtAdditionalTest1Id()))) {
				sAddOnFeatureKey = "Additional Testing 1";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_ADDITIONAL_TEST, oNewRatingDto.getLtAdditionalTest1Id());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtAdditionalTest2Id()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtAdditionalTest2Id()))) {
				sAddOnFeatureKey = "Additional Testing 2";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_ADDITIONAL_TEST, oNewRatingDto.getLtAdditionalTest2Id());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtAdditionalTest3Id()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtAdditionalTest3Id()))) {
				sAddOnFeatureKey = "Additional Testing 3";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_ADDITIONAL_TEST, oNewRatingDto.getLtAdditionalTest3Id());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtTypeTestId()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtTypeTestId()))) {
				sAddOnFeatureKey = "Type Test";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_TYPE_TEST, oNewRatingDto.getLtTypeTestId());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtRTDId()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtRTDId()))) {
				sAddOnFeatureKey = "RTD";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_RTD, oNewRatingDto.getLtRTDId());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtBTDId()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtBTDId()))) {
				sAddOnFeatureKey = "BTD";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_BTD, oNewRatingDto.getLtBTDId());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			
			/* :::::::::::::::::::::::::::::::::::::::::::::::::::::  Add On Values  :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::  */
			/* *************************************************************************************************************************************  */
			
			
			/* *************************************************************************************************************************************  */
			/* **************************************    Verify - Create Add-On Data - Verify ******************************************************  */
			
			if(StringUtils.isNotBlank(oNewRatingDto.getLtShaftTypeId()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtShaftTypeId()))) {
				sAddOnFeatureKey = "Shaft Type";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_SHAFT_TYPE, oNewRatingDto.getLtShaftTypeId());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtDoubleCompressionGlandId()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtDoubleCompressionGlandId()))) {
				sAddOnFeatureKey = "Double Compression Gland";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_DOUBLE_COMPRESS_GLAND, oNewRatingDto.getLtDoubleCompressionGlandId());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			/*if(StringUtils.isNotBlank(oNewRatingDto.getLtSeaworthyPackingId()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtSeaworthyPackingId()))) {
				sAddOnFeatureKey = "Seaworthy";
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, oNewRatingDto.getLtSeaworthyPackingId());
			}*/
			if(StringUtils.isNotBlank(oNewRatingDto.getLtWitnessRoutineId()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtWitnessRoutineId()))) {
				sAddOnFeatureKey = "Witness Routine";
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, oNewRatingDto.getLtWitnessRoutineId());
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtForcedCoolingId()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtForcedCoolingId()))) {
				sAddOnFeatureKey = "Method Of Cooling";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_FORCED_COOL, oNewRatingDto.getLtForcedCoolingId());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtHazardAreaTypeId()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtHazardAreaTypeId()))) {
				sAddOnFeatureKey = "Hazard Area";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_HAZARD_AREA_TYPE, oNewRatingDto.getLtHazardAreaTypeId());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtCableSealingBoxId()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtCableSealingBoxId()))) {
				sAddOnFeatureKey = "Cable Sealing Box";
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, oNewRatingDto.getLtCableSealingBoxId());		
			}
			/*if(StringUtils.isNotBlank(oNewRatingDto.getLtDualSpeedId()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtDualSpeedId()))) {
				sAddOnFeatureKey = "Dual Speed";
				if(QuotationConstants.QUOTATION_YES.equalsIgnoreCase(oNewRatingDto.getLtSpaceHeaterId())) {
					sAddOnFeatureValue = "Y";
				} else {
					sAddOnFeatureValue = "N";
				}
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}*/
			if(StringUtils.isNotBlank(oNewRatingDto.getLtGasGroupId()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtGasGroupId()))) {
				sAddOnFeatureKey = "Gas Group";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_GASGROUP, oNewRatingDto.getLtGasGroupId());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtULCEId()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtULCEId()))) {
				sAddOnFeatureKey = "Certification";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_ULCE, oNewRatingDto.getLtULCEId());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			
			
			/* **************************************    Verify - Create Add-On Data - Verify ******************************************************  */
			/* *************************************************************************************************************************************  */
			
			
			/* *************************************************************************************************************************************  */
			/* **************************************    Fields with MTO Checks ********************************************************************  */
			if(StringUtils.isNotBlank(oNewRatingDto.getLtPoleId())) {
				Integer iPoleId = Integer.parseInt(oNewRatingDto.getLtPoleId());
				if(iPoleId == 6 || iPoleId == 7 || iPoleId == 8 || iPoleId == 9 || iPoleId == 10 || iPoleId == 11) {
					sAddOnFeatureKey = "Pole";
					sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_POLE, oNewRatingDto.getLtPoleId());
					addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
				}
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtLeadId()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtLeadId()))) {
				sAddOnFeatureKey = "Leads";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_LEAD, oNewRatingDto.getLtLeadId());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtServiceFactor()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtServiceFactor()))) {
				sAddOnFeatureKey = "Service Factor";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_SERVICE_FACTOR, oNewRatingDto.getLtServiceFactor());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtMethodOfStarting()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtMethodOfStarting()))) {
				sAddOnFeatureKey = "Method of Starting";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_METHOD_OF_STARTING, oNewRatingDto.getLtMethodOfStarting());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtStartingCurrentOnDOLStart()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtStartingCurrentOnDOLStart()))) {
				sAddOnFeatureKey = "DOL Starting Current";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_START_DOL_CURRENT, oNewRatingDto.getLtStartingCurrentOnDOLStart());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtAmbTempRemoveId()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtAmbTempRemoveId()))) {
				sAddOnFeatureKey = "Ambient Temp Rem";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_AMBREMTEMP, oNewRatingDto.getLtAmbTempRemoveId());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtAltitude()) && Integer.parseInt(oNewRatingDto.getLtAltitude()) > 1000) {
				sAddOnFeatureKey = "Altitude";
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, oNewRatingDto.getLtAltitude());
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtDustGroupId()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtDustGroupId()))) {
				sAddOnFeatureKey = "Dust Group";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_DUST_GROUP, oNewRatingDto.getLtDustGroupId());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtNoiseLevel()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtNoiseLevel()))) {
				sAddOnFeatureKey = "Noise Level";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_NOISE_LEVEL, oNewRatingDto.getLtNoiseLevel());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtReplacementMotor()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtReplacementMotor()))) {
				sAddOnFeatureKey = "Replacement Motor";
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, oNewRatingDto.getLtReplacementMotor());
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtLoadGD2Value()) && oNewRatingDto.getLtLoadGD2Value().trim().length() > 0 && Integer.parseInt(oNewRatingDto.getLtLoadGD2Value()) > 0 ) {
				sAddOnFeatureKey = "Load GD2";
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, oNewRatingDto.getLtLoadGD2Value());
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtRVId()) && oNewRatingDto.getLtRVId().trim().length() > 0 && Integer.parseInt(oNewRatingDto.getLtRVId()) > 0 ) {
				sAddOnFeatureKey = "RV";
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, oNewRatingDto.getLtRVId());
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtRAId()) && oNewRatingDto.getLtRAId().trim().length() > 0 && Integer.parseInt(oNewRatingDto.getLtRAId()) > 0 ) {
				sAddOnFeatureKey = "RA";
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, oNewRatingDto.getLtRAId());
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtVFDApplTypeId()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtVFDApplTypeId()))) {
				sAddOnFeatureKey = "VFD Type";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_VFD_TYPE, oNewRatingDto.getLtVFDApplTypeId());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtVFDMotorSpeedRangeMin()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtVFDMotorSpeedRangeMin()))) {
				sAddOnFeatureKey = "VFD Min Speed";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_VFD_SPEEDRANGE_MIN, oNewRatingDto.getLtVFDMotorSpeedRangeMin());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtVFDMotorSpeedRangeMax()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtVFDMotorSpeedRangeMax()))) {
				sAddOnFeatureKey = "VFD Max Speed";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_VFD_SPEEDRANGE_MAX, oNewRatingDto.getLtVFDMotorSpeedRangeMax());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtOverloadingDuty()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtOverloadingDuty()))) {
				sAddOnFeatureKey = "Overloading Duty";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_OVERLOADING_DUTY, oNewRatingDto.getLtOverloadingDuty());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtConstantEfficiencyRange()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtConstantEfficiencyRange()))) {
				sAddOnFeatureKey = "Constant Efficiency Range";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_CONSTANT_EFF_RANGE, oNewRatingDto.getLtConstantEfficiencyRange());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtShaftMaterialId()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtShaftMaterialId()))) {
				sAddOnFeatureKey = "Shaft Material";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_SHAFT_MATERIAL, oNewRatingDto.getLtShaftMaterialId());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtMethodOfCoupling()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtMethodOfCoupling()))) {
				sAddOnFeatureKey = "Method of Coupling";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_METHOD_OF_COUPLING, oNewRatingDto.getLtMethodOfCoupling());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtIPId()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtIPId()))) {
				sAddOnFeatureKey = "IP";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_IP, oNewRatingDto.getLtIPId());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtSpaceHeaterId()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtSpaceHeaterId()))) {
				sAddOnFeatureKey = "Space Heater";
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, oNewRatingDto.getLtSpaceHeaterId());
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtMetalFanId()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtMetalFanId()))) {
				sAddOnFeatureKey = "Metal Fan";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_METAL_FAN, oNewRatingDto.getLtMetalFanId());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtTechoMounting()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtTechoMounting()))) {
				sAddOnFeatureKey = "Encoder Mounting";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_TECHO_MOUNTING, oNewRatingDto.getLtTechoMounting());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtShaftGroundingId()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtShaftGroundingId()))) {
				sAddOnFeatureKey = "Shaft Grounding";
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, oNewRatingDto.getLtShaftGroundingId());
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtHardware()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtHardware()))) {
				sAddOnFeatureKey = "Hardware";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_HARDWARE, oNewRatingDto.getLtHardware());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtGlandPlateId()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtGlandPlateId()))) {
				sAddOnFeatureKey = "Gland Plate";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_GLAND_PLATE, oNewRatingDto.getLtGlandPlateId());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtSPMMountingProvisionId()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtSPMMountingProvisionId()))) {
				sAddOnFeatureKey = "SPM Mounting";
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, oNewRatingDto.getLtSPMMountingProvisionId());
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtThermisterId()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtThermisterId()))) {
				sAddOnFeatureKey = "Thermister";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_THERMISTER, oNewRatingDto.getLtThermisterId());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtBearingSystemId()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtBearingSystemId()))) {
				sAddOnFeatureKey = "Bearing System";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_BEARING_SYSTEM, oNewRatingDto.getLtBearingSystemId());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtBearingNDEId()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtBearingNDEId()))) {
				sAddOnFeatureKey = "Bearing NDE";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_BEARING_NDE, oNewRatingDto.getLtBearingNDEId());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtBearingDEId()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtBearingDEId()))) {
				sAddOnFeatureKey = "Bearing DE";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_BEARING_DE, oNewRatingDto.getLtBearingDEId());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtPerfCurves()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtPerfCurves()))) {
				sAddOnFeatureKey = "Performance Curves";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QEM_LT_PERF_CURVES, oNewRatingDto.getLtPerfCurves());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			if(StringUtils.isNotBlank(oNewRatingDto.getLtCombVariation()) && !(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtCombVariation()))) {
				sAddOnFeatureKey = "Combined Variation";
				sAddOnFeatureValue = oQuotationDao.getOptionsbyAttributeKey(conn, QuotationConstants.QUOTATION_QEM_LT_COMBVAR, oNewRatingDto.getLtCombVariation());
				addOnSpecialFeatureMap.put(sAddOnFeatureKey, sAddOnFeatureValue);
			}
			
			/* **************************************    Fields with MTO Checks ********************************************************************  */
			/* *************************************************************************************************************************************  */
			
			// Add the Special Feature Map to AddOnDetailsDto
			oAddOnDetailsDto.setAddOnSpecialFeatureMap(addOnSpecialFeatureMap);
			
		} catch(Exception e) {
			LoggerUtility.log("ERROR: ", sClassName, sMethodName, ExceptionUtility.getStackTraceAsString(e));
		}
		
		LoggerUtility.log("INFO", sClassName, sMethodName, "END");
		return oAddOnDetailsDto;
	}
	
	
	public CustomerDiscountDto buildCustDiscountSearchFilter(Connection conn, QuotationDao oQuotationDao, NewRatingDto oNewRatingDto) {
		
		String sMethodName = "buildCustDiscountSearchFilter"; 
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        CustomerDiscountDto oCustomerDiscountDto = new CustomerDiscountDto();
        
        try {
        	if(StringUtils.isNotBlank(oNewRatingDto.getLtCustomerId())) {
        		oCustomerDiscountDto.setCustomerId(oNewRatingDto.getLtCustomerId().trim());
        	} else {
        		oCustomerDiscountDto.setCustomerId("");
        	}
        	
        	if (oNewRatingDto.getLtManufacturingLocation() != null) {
				if(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtManufacturingLocation())) {
					oCustomerDiscountDto.setManufacturing_location("");
				} else {
					oCustomerDiscountDto.setManufacturing_location(oNewRatingDto.getLtManufacturingLocation());
				}
			}
        	
			if(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtProdLineId())) {
				oCustomerDiscountDto.setProduct_line("");
			} else {
				oCustomerDiscountDto.setProduct_line(oNewRatingDto.getLtProdLineId());
			}
			
			if(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtKWId())) {
				oCustomerDiscountDto.setPower_rating_kw("");
			} else {
				oCustomerDiscountDto.setPower_rating_kw(oNewRatingDto.getLtKWId());
			}
			
			if(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtFrameId())) {
				oCustomerDiscountDto.setFrame_type("");
			} else {
				oCustomerDiscountDto.setFrame_type(oNewRatingDto.getLtFrameId());
			}
			
			if(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtPoleId())) { 
				oCustomerDiscountDto.setNumber_of_poles(new Integer(0));
			} else {
				oCustomerDiscountDto.setNumber_of_poles(Integer.parseInt(oNewRatingDto.getLtPoleId()));
			}
			
			if(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtMountingId())) {
				oCustomerDiscountDto.setMounting_type("");
			} else {
				oCustomerDiscountDto.setMounting_type(oNewRatingDto.getLtMountingId());
			}
			
			if(QuotationConstants.QUOTATION_STRING_ZERO.equals(oNewRatingDto.getLtTBPositionId())) {
				oCustomerDiscountDto.setTb_position("");
			} else {
				oCustomerDiscountDto.setTb_position(oNewRatingDto.getLtTBPositionId());
			}
			
        } catch(Exception e) {
			LoggerUtility.log("ERROR: ", sClassName, sMethodName, ExceptionUtility.getStackTraceAsString(e));
		}
		
		LoggerUtility.log("INFO", sClassName, sMethodName, "END");
		return oCustomerDiscountDto;
	}
	
	/**
	 * 
	 * @param oNewEnquiryDto	Contains Enquiry Price Details.
	 * @return NewEnquiryDto	Contains List of Ratings where Quoted Price values match with Standard values.
	 */
	public NewEnquiryDto processQuotedPrices(NewEnquiryDto oNewEnquiryDto) {
		ArrayList<NewRatingDto> alNewRatingsList = new ArrayList<>();
		ArrayList<NewRatingDto> alRatingsList = oNewEnquiryDto.getNewRatingsList();
		NewRatingDto oTempNewRatingDto = null;
		
		if(alRatingsList != null && alRatingsList.size() > 0) {
			for(int iCount = 0; iCount < alRatingsList.size(); iCount++) {
				oTempNewRatingDto = (NewRatingDto) alRatingsList.get(iCount);
				
				if( Integer.parseInt(oTempNewRatingDto.getLtQuotedDiscount()) > 0) {
					oTempNewRatingDto.setLtStandardDiscount(oTempNewRatingDto.getLtQuotedDiscount());
					oTempNewRatingDto.setLtPricePerUnit(oTempNewRatingDto.getLtQuotedPricePerUnit());
					oTempNewRatingDto.setLtTotalOrderPrice(oTempNewRatingDto.getLtQuotedTotalPrice());
				}
				alNewRatingsList.add(oTempNewRatingDto);
			}
			oNewEnquiryDto.setNewRatingsList(alNewRatingsList);
		}
		
		return oNewEnquiryDto;
	}
	
	/**
	 * This method is used to check if SM has Requested any Discount on the Enquiry or not.
	 * @param oNewEnquiryDto	NewEnquiryDto which contains the Price values.
	 * @return boolean 			Flag to notify if Discount has been Requested or not.
	 */
	public boolean verifyRequestedDiscountForRatings(NewEnquiryDto oNewEnquiryDto) {
		boolean isDiscountRequested = false;
		
		for(NewRatingDto oNewRatingDto : oNewEnquiryDto.getNewRatingsList()) {
			if(StringUtils.isNotBlank(oNewRatingDto.getLtRequestedDiscount_SM())) {
				isDiscountRequested = true;
				break;
			}
		}
		
		return isDiscountRequested;
	}
	
	
	public AddOnDetailsDto processAddOnPriceDetails(Connection conn, AddOnDetailsDao oAddOnDetailsDao, AddOnDetailsDto oAddOnDetailsDto) throws Exception {
		
		String sMethodName = "processAddOnPriceDetails"; 
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        try {
        	oAddOnDetailsDto = oAddOnDetailsDao.fetchAddOnPriceDetails(conn, oAddOnDetailsDto);
        } catch(Exception e) {
        	e.printStackTrace();
        	LoggerUtility.log("ERROR: ", sClassName, sMethodName, ExceptionUtility.getStackTraceAsString(e));
        }
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        return oAddOnDetailsDto;
	}
	
	
	public AddOnDetailsDto processAddOnDetails(Connection conn, AddOnDetailsDao oAddOnDetailsDao, AddOnDetailsDto oAddOnDetailsDto) throws Exception {
		
		String sMethodName = "processAddOnDetails"; 
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        try {
        	// 1. Process & Fetch MTO Details based on AddOnDetailsDto values.
        	oAddOnDetailsDto = oAddOnDetailsDao.fetchMTODetailsList(conn, oAddOnDetailsDto);
        	
        	// 2. Process & Fetch Add-On Details based on AddOnDetailsDto values.
        	oAddOnDetailsDto = oAddOnDetailsDao.fetchAddOnDetails(conn, oAddOnDetailsDto);
        	        	
        } catch(Exception e) {
        	e.printStackTrace();
        	LoggerUtility.log("ERROR: ", sClassName, sMethodName, ExceptionUtility.getStackTraceAsString(e));
        }
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        return oAddOnDetailsDto;
	}
	
	public String calculateQuotedTotalPrice(NewEnquiryDto oNewEnquiryDto) {
		String sMethodName = "processAddOnDetails"; 
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        String sQuotedPrice = "";
        //int quantity = 0;
        double quotedPrice = 0d, quotedTotalPrice = 0d;
        try {
        	for(NewRatingDto oNewRatingDto : oNewEnquiryDto.getNewRatingsList()) {
        		//quantity =  oNewRatingDto.getQuantity();
        		if(StringUtils.isNotBlank(oNewRatingDto.getLtQuotedTotalPrice())) {
        			quotedPrice = Double.parseDouble(CommonUtility.replaceString(oNewRatingDto.getLtQuotedTotalPrice(), ",", ""));
        		}
        		
        		quotedTotalPrice = quotedTotalPrice + quotedPrice;
        	}
        	sQuotedPrice = CommonUtility.round(quotedTotalPrice, 0);
        	if(StringUtils.isNotBlank(sQuotedPrice)) {
        		sQuotedPrice = CurrencyConvertUtility.buildPriceFormat(sQuotedPrice);
        	}
        } catch(Exception e) {
        	e.printStackTrace();
        	LoggerUtility.log("ERROR: ", sClassName, sMethodName, ExceptionUtility.getStackTraceAsString(e));
        }
        
        LoggerUtility.log("INFO", sClassName, sMethodName, "END");
        return sQuotedPrice;
	}
	
	/**
	 * Method to Convert a given Object to JSON format.
	 * @param obj		- Object which needs to be converted to JSON.
	 * @return String	- Contains the Object in JSON Format.
	 * @throws JsonProcessingException
	 */
	public String convertListToJSON(Object obj) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		String jsonStr = null;
		if(obj != null) {
			jsonStr = mapper.writeValueAsString(obj);
		}
		return jsonStr;
	}
	
	
	public ArrayList<NewMTORatingDto> getMTOTechRatingsList(Connection conn, NewEnquiryDto oNewEnquiryDto, int mtoTotalRatingIdx, HttpServletRequest request) throws Exception {
		String sMethodName = "getMTORatingsList"; 
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        ArrayList<NewMTORatingDto> alRatingsList = new ArrayList<>();
        try {
        	NewMTORatingDto oTempMTORatingDto = null;
        	for (int iRatingIndex = 1; iRatingIndex <= mtoTotalRatingIdx; iRatingIndex++) {
        		oTempMTORatingDto = new NewMTORatingDto();
        		oTempMTORatingDto.setMtoEnquiryId(oNewEnquiryDto.getEnquiryId());
        		oTempMTORatingDto.setMtoRatingId(Integer.parseInt(CommonUtility.getStringParameter(request, "mtoRatingId" + iRatingIndex)));
        		oTempMTORatingDto.setMtoRatingNo(Integer.parseInt(CommonUtility.getStringParameter(request, "mtoRatingNo" + iRatingIndex)));
        		oTempMTORatingDto.setMtoIndexId(Integer.parseInt(CommonUtility.getStringParameter(request, "mtoIndexId" + iRatingIndex)));
        		
        		oTempMTORatingDto.setMtoQuantity(Integer.parseInt(CommonUtility.getStringParameter(request, "mtoQuantity" + iRatingIndex)));
        		oTempMTORatingDto.setMtoProdLineId(CommonUtility.getStringParameter(request, "mtoProdLine" + iRatingIndex));
        		oTempMTORatingDto.setMtoKWId(CommonUtility.getStringParameter(request, "mtoKw" + iRatingIndex));
        		oTempMTORatingDto.setMtoPoleId(CommonUtility.getStringParameter(request, "mtoPole" + iRatingIndex));
        		oTempMTORatingDto.setMtoFrameId(CommonUtility.getStringParameter(request, "mtoFrame" + iRatingIndex));
        		oTempMTORatingDto.setMtoFrameSuffixId(CommonUtility.getStringParameter(request, "mtoFrameSuffix" + iRatingIndex));
        		oTempMTORatingDto.setMtoMountingId(CommonUtility.getStringParameter(request, "mtoMounting" + iRatingIndex));
        		oTempMTORatingDto.setMtoTBPositionId(CommonUtility.getStringParameter(request, "mtoTBPosition" + iRatingIndex));
        		oTempMTORatingDto.setMtoMfgLocation(CommonUtility.getStringParameter(request, "mtoMfgLocation" + iRatingIndex));
        		oTempMTORatingDto.setMtoSpecialAddOnPercent(CommonUtility.getStringParameter(request, "mtoSpecialAddOnPercent" + iRatingIndex));
        		oTempMTORatingDto.setMtoSpecialCashExtra(CommonUtility.getStringParameter(request, "mtoSpecialAddOnCash" + iRatingIndex));
        		oTempMTORatingDto.setMtoPercentCopper(CommonUtility.getStringParameter(request, "mtoPercentCopper" + iRatingIndex));
        		oTempMTORatingDto.setMtoPercentStamping(CommonUtility.getStringParameter(request, "mtoPercentStamping" + iRatingIndex));
        		oTempMTORatingDto.setMtoPercentAluminium(CommonUtility.getStringParameter(request, "mtoPercentAluminium" + iRatingIndex));
        		oTempMTORatingDto.setMtoTotalAddOnPercent(CommonUtility.getStringParameter(request, "mtoTotalAddOnPercent" + iRatingIndex));
        		oTempMTORatingDto.setMtoTotalCashExtra(CommonUtility.getStringParameter(request, "mtoTotalCash" + iRatingIndex));
        		oTempMTORatingDto.setMtoDesignComplete(CommonUtility.getStringParameter(request, "mtoDesignComplete" + iRatingIndex));
        		oTempMTORatingDto.setMtoSpecialFeatures(CommonUtility.getStringParameter(request, "splFeatures" + iRatingIndex));
        		
        		alRatingsList.add(oTempMTORatingDto);
        	}
        } catch (Exception e) {
			e.printStackTrace();
			LoggerUtility.log("ERROR: ", this.getClass().getName(), "getMTOTechRatingsList", ExceptionUtility.getStackTraceAsString(e));
		}
		
		LoggerUtility.log("INFO", sClassName, sMethodName, "END");
		return alRatingsList;
	}
	
	public NewMTORatingDto getMTOTechRatingsWithDesignAddOns(Connection conn, NewMTORatingDto oMTOTechRatingDto, int mtoTotalRatingIdx, HttpServletRequest request) throws Exception {
		String sMethodName = "getMTORatingsList"; 
        String sClassName = this.getClass().getName();
        LoggerUtility.log("INFO", sClassName, sMethodName, "START");
        
        NewMTOAddOnDto oNewMTOAddOnDto = null;
        ArrayList<NewMTOAddOnDto> alMTOAddOnList = new ArrayList<>();
        String sAddOnTypeVal = "", sAddOnPercentVal = "", sAddOnCashVal = "";
        
        try {
        	int iRatingIdx = oMTOTechRatingDto.getMtoIndexId();
        	int iAddOnRowCount = Integer.parseInt(CommonUtility.getStringParameter(request, "addRowVal" + iRatingIdx));
        	
        	for (int iAddOnIndex = 1; iAddOnIndex <= iAddOnRowCount; iAddOnIndex++) {
        		sAddOnTypeVal = CommonUtility.getStringParameter(request, "addOnType" + iRatingIdx + "_" + iAddOnIndex);
        		
        		if(StringUtils.isNotBlank(sAddOnTypeVal) && !QuotationConstants.QUOTATION_ZERO.equals(sAddOnTypeVal)) {
        			oNewMTOAddOnDto = new NewMTOAddOnDto();
        			oNewMTOAddOnDto.setMtoEnquiryId(oMTOTechRatingDto.getMtoEnquiryId());
        			oNewMTOAddOnDto.setMtoRatingNo(oMTOTechRatingDto.getMtoRatingNo());
        			oNewMTOAddOnDto.setMtoAddOnType(sAddOnTypeVal);
        			oNewMTOAddOnDto.setMtoAddOnQty(Integer.parseInt(CommonUtility.getStringParameter(request, "addOnQuantity" + iRatingIdx + "_" + iAddOnIndex)));
        			
        			sAddOnPercentVal = CommonUtility.getStringParameter(request, "addOnPercent" + iRatingIdx + "_" + iAddOnIndex);
        			sAddOnCashVal = CommonUtility.getStringParameter(request, "addOnCash" + iRatingIdx + "_" + iAddOnIndex);
        			if(StringUtils.isNotBlank(sAddOnPercentVal) && !QuotationConstants.QUOTATION_ZERO.equals(sAddOnPercentVal)) {
        				oNewMTOAddOnDto.setMtoAddOnValue(sAddOnPercentVal);
        				oNewMTOAddOnDto.setMtoAddOnCalcType(QuotationConstants.QUOTATIONLT_ADDON_PERCENT);
        			} else {
        				oNewMTOAddOnDto.setMtoAddOnValue(sAddOnCashVal);
        				oNewMTOAddOnDto.setMtoAddOnCalcType(QuotationConstants.QUOTATIONLT_ADDON_ROUNDED);
        			}
        			oNewMTOAddOnDto.setMtoAddOnReferDesign(QuotationConstants.QUOTATION_STRING_Y);
        			
        			alMTOAddOnList.add(oNewMTOAddOnDto);
        		}
        	}
        	oMTOTechRatingDto.setNewMTOAddOnsList(alMTOAddOnList);
        	
        } catch (Exception e) {
			e.printStackTrace();
			LoggerUtility.log("ERROR: ", this.getClass().getName(), "getMTOTechRatingsWithDesignAddOns", ExceptionUtility.getStackTraceAsString(e));
		}
		
		LoggerUtility.log("INFO", sClassName, sMethodName, "END");
		return oMTOTechRatingDto;
	}

	public StringBuffer fetchFileTypesString(AttachmentDto oAttachmentDto) throws Exception {
		StringBuffer sbFileType = new StringBuffer();
		// Build 'fileTypestd' value and Set to Page.
    	ArrayList<String> alFileTypesList = new ArrayList<>();
    	String sTokenVal = "";
    	String sFileTypes = oAttachmentDto.getFileTypes();
    	//System.out.println("sFileTypes = " + sFileTypes);
    	StringTokenizer stToken = new StringTokenizer(sFileTypes, ",");
		if (stToken != null && stToken.countTokens() > 0) {
			while (stToken.hasMoreTokens()) {
				sTokenVal = stToken.nextToken();
				alFileTypesList.add(sTokenVal);
				sbFileType.append(sTokenVal).append(";");
			}
		}
		
		return sbFileType;
	}
	
}
