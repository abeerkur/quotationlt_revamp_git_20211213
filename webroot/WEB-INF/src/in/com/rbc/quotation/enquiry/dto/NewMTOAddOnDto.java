package in.com.rbc.quotation.enquiry.dto;

import in.com.rbc.quotation.common.utility.CommonUtility;

public class NewMTOAddOnDto {

	private int mtoAddOnId;
	private int mtoEnquiryId;
	private int mtoRatingNo;
	
	private String mtoAddOnType;
	private int mtoAddOnQty;
	private String mtoAddOnValue;
	private String mtoAddOnCalcType;
	private String mtoAddOnReferDesign;
	private String mtoAddOnCreatedBy;
	private String mtoAddOnCreatedDate;
	
	public int getMtoAddOnId() {
		return mtoAddOnId;
	}
	public void setMtoAddOnId(int mtoAddOnId) {
		this.mtoAddOnId = mtoAddOnId;
	}
	public int getMtoEnquiryId() {
		return mtoEnquiryId;
	}
	public void setMtoEnquiryId(int mtoEnquiryId) {
		this.mtoEnquiryId = mtoEnquiryId;
	}
	public int getMtoRatingNo() {
		return mtoRatingNo;
	}
	public void setMtoRatingNo(int mtoRatingNo) {
		this.mtoRatingNo = mtoRatingNo;
	}
	public String getMtoAddOnType() {
		return CommonUtility.replaceNull(mtoAddOnType);
	}
	public void setMtoAddOnType(String mtoAddOnType) {
		this.mtoAddOnType = mtoAddOnType;
	}
	public int getMtoAddOnQty() {
		return mtoAddOnQty;
	}
	public void setMtoAddOnQty(int mtoAddOnQty) {
		this.mtoAddOnQty = mtoAddOnQty;
	}
	public String getMtoAddOnValue() {
		return CommonUtility.replaceNull(mtoAddOnValue);
	}
	public void setMtoAddOnValue(String mtoAddOnValue) {
		this.mtoAddOnValue = mtoAddOnValue;
	}
	public String getMtoAddOnCalcType() {
		return CommonUtility.replaceNull(mtoAddOnCalcType);
	}
	public void setMtoAddOnCalcType(String mtoAddOnCalcType) {
		this.mtoAddOnCalcType = mtoAddOnCalcType;
	}
	public String getMtoAddOnReferDesign() {
		return CommonUtility.replaceNull(mtoAddOnReferDesign);
	}
	public void setMtoAddOnReferDesign(String mtoAddOnReferDesign) {
		this.mtoAddOnReferDesign = mtoAddOnReferDesign;
	}
	public String getMtoAddOnCreatedBy() {
		return CommonUtility.replaceNull(mtoAddOnCreatedBy);
	}
	public void setMtoAddOnCreatedBy(String mtoAddOnCreatedBy) {
		this.mtoAddOnCreatedBy = mtoAddOnCreatedBy;
	}
	public String getMtoAddOnCreatedDate() {
		return CommonUtility.replaceNull(mtoAddOnCreatedDate);
	}
	public void setMtoAddOnCreatedDate(String mtoAddOnCreatedDate) {
		this.mtoAddOnCreatedDate = mtoAddOnCreatedDate;
	}
	
	
	
}
