/**
 * *****************************************************************************
 * Project Name: Request For Quotation
 * Document Name: PDFServlet.java
 * Package: in.com.rbc.quotation.enquiry.action
 * Desc: Servlet retrieves the PDF of the quotation 
 * *****************************************************************************
 * Author: 100006718 (Kalyan Chakravarthi B)
 * Date: October 21, 2008
 * *****************************************************************************
 */
package in.com.rbc.quotation.enquiry.action;

import in.com.rbc.quotation.common.constants.QuotationConstants;
import in.com.rbc.quotation.common.utility.DBUtility;
import in.com.rbc.quotation.common.utility.DateUtility;
import in.com.rbc.quotation.common.utility.ExceptionUtility;
import in.com.rbc.quotation.common.utility.LoggerUtility;
import in.com.rbc.quotation.common.utility.PDFUtility;
import in.com.rbc.quotation.enquiry.dao.QuotationDaoImpl;
import in.com.rbc.quotation.enquiry.dto.EnquiryDto;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PDFServlet extends HttpServlet {
    
	
	/**
	 * Method to display image in jsp -- invokes doPost
	 * @param request HttpServletRequest object to handle request operations
	 * @param response HttpServletResponse object to handle response operations 
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}
	
	/**
	 * Method displays the image in jsp
	 * @param request HttpServletRequest object to handle request operations
	 * @param response HttpServletResponse object to handle response operations
	 */
	public void doPost(HttpServletRequest request, 
                       HttpServletResponse response) throws ServletException, IOException{
		
	    // Connection object to store the database connection
	    Connection conn = null;
	    
	    // Object of DBUtility class to handle DB connection objects
	    DBUtility oDBUtility = null;
        
	    EnquiryDto oEnquiryDto = null; 
	    int iEnquiryId =0;
	    String sFinalEnquiryId=null;
	    String sFileName=null;
	    String sCustomer = null;
		String[] oCustomerArray = null;
		try{
			
			/* Instantiating the DBUtility object */
			oDBUtility = new DBUtility();
			/*Intioalization of oEnquiryDto object*/
			
			oEnquiryDto = new EnquiryDto();
			/*Getting Enquiry Id from Request/*/
			
			iEnquiryId = Integer.parseInt(request.getParameter(QuotationConstants.QUOTATION_REQUEST_ENQUIRYID));
			oEnquiryDto.setEnquiryId(iEnquiryId);
			
            /* Retrieving the database connection using DBUtility.getDBConnection method */
            conn = oDBUtility.getDBConnection();
			
            oEnquiryDto = new QuotationDaoImpl().getEnquiryDetails(conn,oEnquiryDto,QuotationConstants.QUOTATION_FORWARD_VIEW.toUpperCase());

            ByteArrayOutputStream oByteArrayOutputStream = new PDFUtility().generatePDF(oEnquiryDto,request);
            sCustomer = oEnquiryDto.getCustomerName();
            if(sCustomer!=null&&!sCustomer.equals("")){
            	oCustomerArray =sCustomer.split(" ");
            	if(oCustomerArray.length>=QuotationConstants.QUOTATION_LITERAL_2)
            		sCustomer = oCustomerArray[0]+" "+oCustomerArray[1];
            	else
            		sCustomer = oCustomerArray[0];
            } 

            sFinalEnquiryId=oEnquiryDto.getEnquiryNumber().substring(15);
             sFileName=QuotationConstants.QUOTATION_PDF+sFinalEnquiryId+QuotationConstants.QUOTATION_STRING_UNDERSC+sCustomer+QuotationConstants.QUOTATION_STRING_UNDERSC
            		+DateUtility.getCurrentYear() + QuotationConstants.QUOTATION_STRING_UNDERSC + DateUtility.getCurrentMonth()+QuotationConstants.QUOTATION_PDFEXT;

			     /* Setting the content type to PDF */
			response.setContentType("application/pdf");
			response.setContentLength(oByteArrayOutputStream.size()); 

			response.setHeader("Content-disposition","attachment; filename=" +sFileName);
			ServletOutputStream oServletOutputStream  = null;

	        if (oServletOutputStream == null)
	        	oServletOutputStream = response.getOutputStream();
	        oByteArrayOutputStream.writeTo(oServletOutputStream);
			
	        oServletOutputStream.flush();		
			
		
			
            } catch(Exception e) {
            /*
             * Logging any exception that raised during this activity in log files using Log4j
             */
            LoggerUtility.log("DEBUG", this.getClass().getName(), "doPost", ExceptionUtility.getStackTraceAsString(e));
		}  finally {
            /* Releasing/closing the connection object */
            try {
				oDBUtility.releaseResources(conn);
			} catch (SQLException e) {
                /*
                 * Logging any exception that raised during this activity in log files using Log4j
                 */
                LoggerUtility.log("DEBUG", this.getClass().getName(), "doPost", ExceptionUtility.getStackTraceAsString(e));
			}
        }
	}
}
