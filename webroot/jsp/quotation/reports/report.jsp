<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="in.com.rbc.quotation.common.vo.KeyValueVo" %>

 <script  type="text/javascript" src="html/js/multiselect.min.js"></script>
<style type="text/css">
.multiselect-wrapper {
	width: 180px;
	display: inline-block;
	white-space: nowrap;
	font-size: 12px;
	font-family: "Segoe UI", Verdana, Helvetica, Sans-Serif;
}

.multiselect-wrapper .multiselect-input {
	width: 120px;
	padding-right: 50px;
}

.multiselect-wrapper label {
	display: block;
	font-size: 12px;
	font-weight : 600;
}

.multiselect-wrapper .multiselect-list {
	z-index: 1;
	position: absolute;
	display: none;
	background-color: white;
	border: 1px solid grey;
	border-bottom-left-radius: 2px;
	border-bottom-right-radius: 2px;
	margin-top: -2px;
}

	.multiselect-wrapper .multiselect-list.active {
		display: block;
	}

	.multiselect-wrapper .multiselect-list > span {
		font-weight: bold;
	}

	.multiselect-wrapper .multiselect-list .multiselect-checkbox {
		margin-right: 2px;
	}

	.multiselect-wrapper .multiselect-list > span,
	.multiselect-wrapper .multiselect-list li {
		cursor: default;
	}

.multiselect-wrapper .multiselect-list {
	padding: 5px;
	min-width: 200px;
}

.multiselect-wrapper ul {
	list-style: none;
	display: block;
	position: relative;
	padding: 0px;
	margin: 0px;
	max-height: 200px;
	overflow-y: auto;
	overflow-x: hidden;
}

	.multiselect-wrapper ul li {
		padding-right: 20px;
		display: block;
	}

		.multiselect-wrapper ul li.active {
			background-color: rgb(0, 102, 255);
			color: white;
		}

		.multiselect-wrapper ul li:hover {
			background-color: rgb(0, 102, 255);
			color: white;
		}

.multiselect-input-div {
	height: 34px;
}

	.multiselect-input-div input{
		border: 1px solid #ababab;
		background : #fff;
		margin: 5px 0 6px 0;
		padding: 5px;
		vertical-align:middle;
	}

.multiselect-count {
	position: relative;
	text-align: center;
	border-radius: 2px;
	behavior: url(/Content/Pie/pie.htc);
	background-color: lightblue;
	display: inline-block !important;
	padding: 2px 7px;
	left: -45px;
}

.multiselect-dropdown-arrow {
	width: 0;
	height: 0;
	border-left: 5px solid transparent;
	border-right: 5px solid transparent;
	border-top: 5px solid black;
	position: absolute;
	line-height: 20px;
	text-align: center;
	display: inline-block !important;
	margin-top: 17px;
	margin-left: -42px;
}

</style>
<!--main content  table begins-->
  <html:form action="viewreport.do" method="post" ><br>
  <html:hidden property="invoke" value="viewGraph"/><br>
  <div class="sectiontitle"><bean:message key="quotation.report.report" /></div>
  <br>
  <!---------------------------- filters start ----------->
  	
		<table border="0" align="center" cellpadding="2" cellspacing="0"  style="width:95% ">
			
			<tr class="blue-light">
			  <td rowspan="3" class="working" id="divWorking" style="margin-right:5px; background-color:#FFFFFF"><img src="html/images/loading.gif" border="0" align="absmiddle" ></td>
				 <td ><bean:define name="reportForm" property="yearList"
	                	             id="yearList" type="java.util.Collection" />
              <html:select name="reportForm" property="year" styleClass="normal" styleId="year" >
                <html:option value="0">Yearly</html:option>
                <html:options name="reportForm" collection="yearList"
			 				              property="key" labelProperty="value" />
              </html:select>         
               </td>
              <td ><html:select property="month" styleId="month"> 
			  <html:option value="0">All Months</html:option> 
			  <html:option value="01">January</html:option> 
			  <html:option value="02">Febaruary</html:option> 
			  <html:option value="03">March</html:option> 
			  <html:option value="04">April</html:option>
			  <html:option value="05">May</html:option>
			  <html:option value="06">June</html:option>
			  <html:option value="07">July</html:option>
			  <html:option value="08">August</html:option>
			  <html:option value="09">September</html:option>
			  <html:option value="10">October</html:option>
			  <html:option value="11">November</html:option>
			  <html:option value="12">December</html:option>			  
			  
			  </html:select></td>
					<td>
                  <bean:define name="reportForm" property="regionList"
	                	             id="regionList" type="java.util.Collection" />
	                	<html:select name="reportForm" property="regionId" styleId="regionId"
	                	      styleClass="normal" style="width:180px " onchange="javascript:onRegionChange(this, 'salesManagerId','true','divWorking')">
	                		<html:option value="">All Regions</html:option>
			 				<html:options name="reportForm" collection="regionList"
			 				              property="key" labelProperty="value" />
						</html:select>			  </td>
				<td>
					<bean:define name="reportForm" property="salesManagerList"
	                	             id="salesManagerList" type="java.util.Collection" />
	                    	<html:select  name="reportForm" property="salesManagerId"
	                	      styleClass="normal" styleId="salesManagerId"   >
	                		<html:option value="">All Sales Managers</html:option>
	                		
			 				<html:options name="reportForm" collection="salesManagerList"
			 				              property="key" labelProperty="value" />
					    </html:select>	
					    
					    
					    			</td>
							
			</tr>
			
			<tr class="blue-light">
			 	<td >
					<bean:define name="reportForm" property="locationList"
	                	             id="locationList" type="java.util.Collection" />
	                  	<html:select name="reportForm" property="locationId" styleClass="normal" styleId="locationId"  
	                	      onchange="javascript:onLocationChange(this, 'customerId','true', 'divWorking')" >
	                		<html:option value="">All Locations</html:option>
			 				<html:options name="reportForm" collection="locationList"
			 				              property="key" labelProperty="value" />
					  	</html:select>				</td>
			  <td >
					<bean:define name="reportForm" property="customerList"
	                	             id="customerList" type="java.util.Collection" />
	                  	<html:select name="reportForm" property="customerId"
	                	      styleClass="normal" styleId="customerId"   >
	                		<html:option value="">All Customers</html:option>
			 				<html:options name="reportForm" collection="customerList"
			 				              property="key" labelProperty="value" />
					  	</html:select>		
					  	
					  			
					  	
					  	</td>
			  <td >
					<bean:define name="reportForm" property="statusList"
	                	             id="statusList" type="java.util.Collection" />
	                  	<html:select name="reportForm" property="satusId"
	                	      styleClass="normal" style="width:180px " styleId="satusId"  >
	                		<html:option value="">All Status</html:option>
			 				<html:options name="reportForm" collection="statusList"
			 				              property="key" labelProperty="value" />
					  	</html:select>				
					  	</td>
			  <td class="secondaryfilter" >
			  <input name="generate" type="submit" class="BUTTON" id="generate"  value="Graph"></td>
			 
			</tr>
		
				
			<tr >
			 
		      <td colspan="4" class="DotsTable" align="center">Click on <img src="html/images/ico_graph.gif" align="absmiddle"> to drill down to next level. Click on <img src="html/images/down_icon.gif" width="15" height="15"  border="0" align="absmiddle"> to view request table along with legend for status graph.</td>
	      </tr>
		</table>
		<br>
		
		<logic:present name="resultsGraph" scope="request">	 
		<logic:notEmpty name="resultsGraph">
	 	  <% int iCount=1; %>
		<table width="100%" border="0" cellspacing="0" cellpadding="5">
        <logic:iterate name="resultsGraph" id="oGraphDto" indexId="idx"
	                                                  type=" in.com.rbc.quotation.reports.dto.GraphDto">
	                                                 
	                                                  
        <tr>      
         <td  style="vertical-align:top">
          <logic:notEqual name="oGraphDto" property="graphName" value="">
		  <table border="0" cellpadding="0" cellspacing="0" style="background-image:url(html/images/loading.gif); background-position:center;  background-repeat:no-repeat ">
		 	<tr>
				<td class="filters1">
					<span class="blueheader">&nbsp;By <bean:write name="oGraphDto" property="graphTitle"/>  </span>
					 <logic:notEqual name="oGraphDto" property="graphName" value="nodata.gif">
					<a class="reportsview" href="<bean:write name="oGraphDto" property="graphHrefUrl"/>"> View Detailed Results</a>
					</logic:notEqual>
			  </td>
			<logic:notEmpty name="oGraphDto" property="algraphdataset">		
			    <td class="filters1" align="right" >
				  <div align="right"><a name="bus<%= iCount%>"> </a><a href="#bus<%= iCount%>"><img src="html/images/down_icon.gif"  alt="View Request Table" name="downarrow<%= iCount%>"   border="0" id="downarrow<%= iCount%>" style="cursor:hand; margin-right:1px" onClick="reportsdown('downarrow','tableid','uparrow','<%= iCount%>')"></a>
			      </div>
			    </td>
			</logic:notEmpty>	    
		 	</tr>
			
			<tr>
				<td colspan="2" align="center"><img src="<bean:write name="oGraphDto" property="graphUrl"/>" width=450 height=300 border=0  usemap="#<bean:write name="oGraphDto" property="graphName"/>"></td>
			</tr>
			<tr>
			<logic:notEmpty name="oGraphDto" property="algraphdataset">
				<td colspan="2" class="total">
					<table style="width:100%; display:none; " border="0" cellspacing="0" cellpadding="0" id="tableid<%= iCount%>">
					  <tr class="dark">
					  <logic:equal name="oGraphDto" property="graphTitle" value="Status">
					  <td><b>Legend</b></td>
					  </logic:equal>
						<td><b><bean:write name="oGraphDto" property="graphTitle"/></b></td>
						<td><b>Request</b></td>
					  </tr>
					  <logic:iterate name="oGraphDto" property="algraphdataset"
								               id="resultDetails" indexId="idx1"
								               type="in.com.rbc.quotation.reports.dto.ReportDatasetDto">
								               
  					<% if(idx.intValue() % 2 == 0) { %>
						<tr class="dark" nowrap id='item<bean:write name="idx1"/>'">
					<% } else { %>
						<tr class="dark" id='item<bean:write name="idx1"/>'>
		    		<% } %>	
		    		<logic:equal name="oGraphDto" property="graphTitle" value="Status">
					  <td><bean:write name="resultDetails" property="legend"/></td>
					</logic:equal>
						<td>
						<a class="buttongraph" style="cursor:hand;" href="<bean:write name="oGraphDto" property="graphHrefUrl"/>&sectionid=<bean:write name="resultDetails" property="sectionId"/>" >
    					<bean:write name="resultDetails" property="section"/>
    					</a>
    					</td>
						<a class="buttongraph" style="cursor:hand;" href="<bean:write name="oGraphDto" property="graphHrefUrl"/>&sectionid=<bean:write name="resultDetails" property="sectionId"/>" >
						<td style="cursor:hand;">
						<a class="buttongraph" style="cursor:hand;" href="<bean:write name="oGraphDto" property="graphHrefUrl"/>&sectionid=<bean:write name="resultDetails" property="sectionId"/>" >
							<bean:write name="resultDetails" property="openCount"/></a></td></a>
  						</tr>
  						
  					</logic:iterate> 
					
					</table>
					<div id="uparrow<%= iCount%>" align="right" class="filters1" style="display:none; text-align:right;">
					  <img src="html/images/up_icon.gif" alt="Close Request Table"  style="cursor:hand; margin-right:1px" onClick="reportsup('downarrow','tableid','uparrow','<%= iCount%>')"></div>
				</td>
			</logic:notEmpty>	
			</tr>
		 </table>
          
          </logic:notEqual>
          </td>      
          <td  style="vertical-align:top">
          <logic:notEqual name="oGraphDto" property="graphName2" value="">
		  <table cellpadding="0" cellspacing="0" style="background-image:url(html/images/loading.gif); background-position:center;  background-repeat:no-repeat ">
		 	<tr>
		 	
				<td class="filters1">
					<span class="blueheader">&nbsp;By <bean:write name="oGraphDto" property="graphTitle2"/></span> 
					 <logic:notEqual name="oGraphDto" property="graphName2" value="nodata.gif">
					<a class="reportsview" href="<bean:write name="oGraphDto" property="graphHrefUrl2"/>"> View Detailed Results</a>
					</logic:notEqual>
				</td>
				<logic:notEmpty name="oGraphDto" property="algraphdataset2">	
			    <td class="filters1" align="right">
			    <div align="right"><a name="sub<%= iCount+1%>"></a>
				<a href="#sub<%= iCount+1%>"><img src="html/images/down_icon.gif" alt="View Request Table" name="downarrow<%= iCount+1%>"   border="0" id="downarrow<%= iCount+1%>" style="cursor:hand; margin-right:1px"  onClick="reportsdown('downarrow','tableid','uparrow','<%= iCount+1%>')"></a> 
				</div></td>
			</logic:notEmpty>		
		 	</tr>
			
			<tr>
				<td colspan="2" align="center"><img src="<bean:write name="oGraphDto" property="graphUrl2"/>" width=450 height=300 border=0  usemap="#<bean:write name="oGraphDto" property="graphName2"/>"></td>
		    </tr>
			<tr>
			<logic:notEmpty name="oGraphDto" property="algraphdataset2">	
				<td colspan="2" class="total">
                  <table border="0" cellspacing="0" cellpadding="0" id="tableid<%= iCount+1%>" style="width:100%; display:none; ">
                    <tr class="dark">
                      <logic:equal name="oGraphDto" property="graphTitle2" value="Status">
					  <td><b>Legend<b></td>
					  </logic:equal>
                      <td><b><bean:write name="oGraphDto" property="graphTitle2"/></b></td>
                      <td><b>Request</b></td>
                    </tr>
                     <logic:iterate name="oGraphDto" property="algraphdataset2"
								               id="resultDetails2" indexId="idx1"
								               type="in.com.rbc.quotation.reports.dto.ReportDatasetDto">
								               
  				<% if(idx.intValue() % 2 == 0) { %>
						<tr class="dark" nowrap id='item<bean:write name="idx1"/>'">
					<% }  else { %>
						<tr class="dark" id='item<bean:write name="idx1"/>'>
		    		<% } %>	
		    		<logic:equal name="oGraphDto" property="graphTitle2" value="Status">
					  <td><bean:write name="resultDetails2" property="legend"/></td>
					</logic:equal>
						<td>
						<a class="buttongraph" href="<bean:write name="oGraphDto" property="graphHrefUrl2"/>&sectionid=<bean:write name="resultDetails2" property="sectionId"/>" >
    					<bean:write name="resultDetails2" property="section"/></td>
    					</a>
    					
							<a class="buttongraph" href="<bean:write name="oGraphDto" property="graphHrefUrl2"/>&sectionid=<bean:write name="resultDetails2" property="sectionId"/>" >
							<td style="cursor:hand;">
							<a class="buttongraph" href="<bean:write name="oGraphDto" property="graphHrefUrl2"/>&sectionid=<bean:write name="resultDetails2" property="sectionId"/>" >
							<bean:write name="resultDetails2" property="openCount"/></a></td></a>
  						</tr>  						
  				</logic:iterate> 
                  </table>
				 <div id="uparrow<%= iCount+1%>" align="right" class="filters1" style="display:none; text-align:right; ">
				  <img src="html/images/up_icon.gif" alt="Close Request Table"  style="cursor:hand; margin-right:1px" onClick="reportsup('downarrow','tableid','uparrow','<%= iCount+1%>')"></div>
				  
		      </td>
		      </logic:notEmpty>
		    </tr>
			
		 </table>
           
          </logic:notEqual>
          </td>          
        </tr>
        <% iCount=iCount+2;  %>
        </logic:iterate>            
     </table>
		</logic:notEmpty> 
		
	</logic:present>
		
</html:form>
  <!-- InstanceEndEditable -->

<!--main content  table end-->  

 

