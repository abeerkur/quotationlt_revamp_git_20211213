<%@ include file="../common/nocache.jsp" %>
<%@ include file="../common/library.jsp" %>
<%@ taglib uri="/ajax-tag" prefix="ajax" %>
<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants" %>

<!-------------------------- Main Table Start  --------------------------------->
<table width="100%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
    <tr>
        <td height="100%" valign="top">
            <br>
            <div class="sectiontitle">
            Sample
            </div>
            <form id="form1" name="form1" method="post" action="">
                <table style="height:100%" width="100%" border="0">
                    <tr>
                        <td style="vertical-align:top">
                            <!-------------------------- Right Navgation Block Start  --------------------------------->
                            <table align="right" width="90%" cellpadding="0" cellspacing="0">
		                        <!-- SAMPLE AUTOCOMPLETE CODE - TO BE REMOVED LATER -->			                                 
								<tr><td>&nbsp;</td></tr>
								<tr><td>
									Sample
									<input id="customerName" name="customerName" type="text" size="50" class="form-autocomplete"/>
									<input type="hidden" name="customerId" id="customerId" />
								</td></tr>                  		
  								<!--  END -->
                            </table>
                            <!-------------------------- Right Navgation Block End  --------------------------------->
                        </td>
                    </tr>
                </table>
            </form>
        </td>
    </tr>
</table>
<!-------------------------- Main Table End  --------------------------------->

<!-- SAMPLE AUTOCOMPLETE CODE - TO BE REMOVED LATER -->
<ajax:autocomplete
				  fieldId="customerName"
				  popupId="customer-popup"
				  targetId="customerId"
				  baseUrl="servlet/AutocompleteServlet?type=customer"
				  paramName="customer"
				  className="autocomplete"
				  progressStyle="throbbing" />

	<iframe
		 id="divShim"
		src="javascript:false;"
		scrolling="no"
		frameborder="0"
		style="position:absolute; top:0px; left:0px; display:none;">
	</iframe>
<!--  END  -->
