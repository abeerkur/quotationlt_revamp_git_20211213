<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants" %>
<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>
<%@ page import="in.com.rbc.quotation.common.utility.PropertyUtility" %>
<%@page import="in.com.rbc.quotation.common.utility.CurrencyConvertUtility" %>
<%@page import="org.apache.commons.lang.StringUtils" %>
<%@ page import="java.text.DecimalFormat,java.math.BigDecimal,java.net.URLDecoder" %>
<script type='text/javascript' src='dwr/engine.js'></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src='dwr/interface/saveNewEnquiryBasicInfo.js'></script>

<%
DecimalFormat df = new DecimalFormat("###,###");
DecimalFormat df1 = new DecimalFormat("#0");
%>

<input type="hidden" name="<%=org.apache.struts.taglib.html.Constants.TOKEN_KEY%>" value="<%=session.getAttribute(org.apache.struts.action.Action.TRANSACTION_TOKEN_KEY)%>">

<html:form action="/newViewEnquiry" method="POST">

<html:hidden property="invoke" value="processReleasedQuote"/>
<html:hidden property="enquiryId"  styleId="enquiryId" />
<html:hidden property="operationType" styleId="operationType" />
	
	<logic:present name="newEnquiryDto" scope="request">
	
		<table width="95%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
			<tr>
  				<td height="100%" valign="top">
  					<logic:equal name="newenquiryForm" property="operationType" value="viewEnquiry">
  						<fieldset>
							<legend class="blueheader">
								<bean:message key="quotation.offerdata.commercial.label.heading" />
							</legend>
							
							<logic:notEmpty name="newEnquiryDto" property="newRatingsList">
								<logic:iterate name="newEnquiryDto" property="newRatingsList" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
									
									<table width="100%" cellpadding="0" cellspacing="0">
										<tr style="font-weight: bold; background-color: #CCDAF1">
											<td style="width: 25%; text-align: right;">
												<bean:message key="quotation.search.frame" /><%=idx.intValue() + 1%>
											</td>
											<td>: <bean:write name="ratingObj" property="ltFrameId" /><bean:write name="ratingObj" property="ltFrameSuffixId" /> </td>
											<td style="width: 25%; text-align: right;">
												<bean:message key="quotation.offerdata.commercial.label.mcunit" />
											</td>
											<!-- <td style="width: 25%"> <bean:write name="ratingObj" property="commercialMCUnit" /> </td>  -->
											<td style="width: 25%"> &nbsp; </td>
										</tr>
										
										<logic:notEmpty name="ratingObj" property="addOnList">
											<logic:iterate name="ratingObject" property="addOnList" id="addOnDto" indexId="idx1" type="in.com.rbc.quotation.enquiry.dto.AddOnDto">
												<tr>
													<td class="formLabelview" style="width: 25%">
														<bean:message key="quotation.offerdata.addon.label.addontype" /><%=idx1.intValue() + 1%>
													</td>
													<td class="formContentview">
														<bean:write name="addOnDto" property="addOnTypeText" />
													</td>
													<td class="formLabelview" style="width: 25%">
														<bean:message key="quotation.offerdata.addon.label.unitprice" />
													</td>
													<td class="formContentview" style="width: 25%">:
														<bean:write name="addOnDto" property="addOnUnitPrice" />
													</td>
												</tr>
											</logic:iterate>
										</logic:notEmpty>
									</table>
										    
								</logic:iterate>
							</logic:notEmpty>
						</fieldset>
  					</logic:equal>
  					
  					<logic:equal name="newenquiryForm" property="operationType" value="consolidatedView">
  					
  						<bean:define id="teamMemberObject" name="newEnquiryDto" property="teamMemberDto" />
  						
  						<tr>
  							<td>
  							
  								<fieldset>
									<legend class="blueheader">
										<bean:message key="quotation.offerdata.commercial.label.heading" />
									</legend>
									
									<table width="100%" border="0" align="center" cellpadding="0" cellspacing="3">
													<tr>
														<td colspan="16">&nbsp;</td>
													</tr>
												
													<tr>
														<td style="width:1135px !important;overflow:scroll;float:left;">
															<table width="100%" border="0" align="center" cellpadding="0" cellspacing="3">
																<tr>
																	<td colspan="11">&nbsp;</td>
																</tr>
																
																<logic:present name="APPROVALTYPE" scope="request">
																
																	<!-- ------------------------------------ Commercial Offer : Normal : Start ----------------------------------- -->
																	<logic:equal name="APPROVALTYPE" scope="request" value="NORMAL_USER">
																		<tr>
																			<td width="3%" class="formLabelTophome"><bean:message key="quotation.create.label.itemno" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.label.admin.field.product.line" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.create.label.ratedoutput" /> ( <bean:message key="quotation.option.kw" /> )</td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.create.label.pole" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.create.label.ratedvoltage" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.search.frame" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.offerdata.addon.label.quantity" /></td>
																			<td width="5%" class="formLabelTophome">Standard Discount</td>
																			<td width="7%" class="formLabelTophome">Standard Unit Price</td>
																			<td width="7%" class="formLabelTophome">Standard Total price</td>
																		</tr>
																		<% 
																			Double dQuantity=new Double(0);
														                    Double dTotalTransportation=new Double(0);
														                    Double dTotalWarranty=new Double(0);
														                    Double dTotalTc=new Double(0);
														                    Double dMcnspRatio=new Double(0);
														                    BigDecimal bdListPrice=new BigDecimal("0");
														                    BigDecimal bdTotalPriceStd=new BigDecimal("0");
														                    String sListPrice="", sTotalPrice="", sSupervisionCharges="";
																		%>
																		<logic:iterate name="newEnquiryDto" property="newRatingsList" id="ratingObject" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
																			<bean:define id="qnt" name="ratingObject" property="quantity"/>
								                      						<bean:define id="listPriceVal" name="ratingObject" property="ltLPPerUnit"/>
								                      						<bean:define id="priceVal" name="ratingObject" property="ltTotalOrderPrice"/>
								                      						<input type="hidden" id="ratingObjId<%=idx + 1%>" name="ratingObjId<%=idx + 1%>" value='<bean:write name="ratingObject" property="ratingId" />' >
								                      						<input type="hidden" id="quantity<%=idx + 1%>" name="quantity<%=idx + 1%>" value='<%=qnt%>' >
								                      						<% 
								                      							sListPrice=String.valueOf(listPriceVal);
								                      						
									                      						if(sListPrice!="" && sListPrice!=QuotationConstants.QUOTATION_ZERO) {
									                      							if(sListPrice.indexOf(",") != -1){
									                      								sListPrice = sListPrice.replaceAll(",", "");
																					}
									                      						}
								                      						%>
								                      						<tr>
								                      							<td width="3%" class="formContentview-rating-b0x"><%=idx + 1%></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltProdLineId" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltKWId" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltPoleId" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltVoltId" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltFrameId" /><bean:write name="ratingObject" property="ltFrameSuffixId" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="quantity" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltStandardDiscount" /> %</td>
																				<td width="7%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltPricePerUnit" /></td>
																				<td width="7%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltTotalOrderPrice" /></td>
								                      						</tr>
								                      						<% 
															                    Double dQty=new Double(0);
															                    			                    
															                    sTotalPrice=String.valueOf(priceVal);
															                    
															                    if(qnt!="" && qnt != "0") {
															       					dQty = Double.parseDouble(String.valueOf(qnt));
															       					dQuantity = dQuantity + dQty;
																				}
															                    
															                    if(sTotalPrice!="" && sTotalPrice!=QuotationConstants.QUOTATION_ZERO) {
															                    	if(sTotalPrice.indexOf(",") != -1){
															                        	String unitprice=sTotalPrice.replaceAll(",", "");
															                    		BigDecimal a=new BigDecimal(unitprice);
															                    		bdTotalPriceStd=bdTotalPriceStd.add(a);      
																					} else {
															                          	BigDecimal a=new BigDecimal(sTotalPrice);
															                          	bdTotalPriceStd=bdTotalPriceStd.add(a);      
																					}
															                    }
															                    
																			%>
																		</logic:iterate>
																		
																		<tr>
																			<td colspan="10" class="formContentview-rating-b0x">&nbsp;</td>
																		</tr>
																		<logic:equal name="newEnquiryDto" property="supervisionDetails" value="Yes"> 
																			<bean:define id="superviseManDays" name="newEnquiryDto" property="superviseNoOfManDays"/>
																			<% 
																				int iSuperviseManDays = Integer.parseInt(String.valueOf(superviseManDays));
																				long supervisionCharges = iSuperviseManDays * QuotationConstants.QUOTATIONLT_MANDAY_SUPERVISE_CHARGES;
																				BigDecimal a=new BigDecimal(supervisionCharges);
																				bdTotalPriceStd = bdTotalPriceStd.add(a); 
																				sSupervisionCharges = CurrencyConvertUtility.buildPriceFormat(String.valueOf(supervisionCharges));
																			%>
																			<tr>
																				<td colspan="2" class="formContentview-rating-b0x">&nbsp;</td>
																				<td colspan="4" align="left" class="formContentview-rating-b0x"><b> Supervision and Comissioning charges</b></td>
																				<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
																				<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
																				<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
																				<td class="formContentview-rating-b0x"><b> &#8377;  <%=sSupervisionCharges%></b></td>
																			</tr>
																		</logic:equal>
																		<% 
																			sTotalPrice = CurrencyConvertUtility.buildPriceFormat(bdTotalPriceStd.toString());
																		%>
																		<tr>
																			<td colspan="5" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td align="left" class="formContentview-rating-b0x"><b><bean:message key="quotation.create.label.total"/></b></td>
							                       							<td class="formContentview-rating-b0x"> <b> <%=df1.format(dQuantity)%> </b> </td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td class="formContentview-rating-b0x"><b> &#8377;  <%=sTotalPrice%></b></td>
																		</tr>
																		<tr>
																			<td colspan="2" class="formContentview-rating-b0x"><b><bean:message key="quotation.create.label.inwords"/></b></td>
																			<td colspan="8" class="formContentview-rating-b0x" align="right">
																				<b>	<%=CurrencyConvertUtility.convertToIndianCurrency(String.valueOf(bdTotalPriceStd)) %> </b>
																			</td>
																		</tr>
																	</logic:equal>
																	<!-- ------------------------------------ Commercial Offer : Normal : End ----------------------------------- -->
																	
																	
																	<!-- ------------------------------------ Commercial Offer : SM : Start ----------------------------------- -->
																	<logic:equal name="APPROVALTYPE" scope="request" value="SM">
																		<tr>
																			<td width="3%" class="formLabelTophome"><bean:message key="quotation.create.label.itemno" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.label.admin.field.product.line" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.create.label.ratedoutput" /> ( <bean:message key="quotation.option.kw" /> )</td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.create.label.pole" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.create.label.ratedvoltage" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.search.frame" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.offerdata.addon.label.quantity" /></td>
																			<td width="5%" class="formLabelTophome">Standard Discount</td>
																			<td width="7%" class="formLabelTophome">Standard Unit Price</td>
																			<td width="7%" class="formLabelTophome">Standard Total price</td>
																			<td width="5%" class="formLabelTophome">Requested Discount (SM)</td>
																			<td width="7%" class="formLabelTophome">Requested Unit Price (SM)</td>
																			<td width="7%" class="formLabelTophome">Requested Total price (SM)</td>
																			<td width="5%" class="formLabelTophome">Approved Discount (RSM)</td>
																			<td width="7%" class="formLabelTophome">Approved Unit Price (RSM)</td>
																			<td width="7%" class="formLabelTophome">Approved Total price (RSM)</td>
																			<td width="5%" class="formLabelTophome">Quoted Discount</td>
																			<td width="7%" class="formLabelTophome">Quoted Unit Price</td>
																			<td width="7%" class="formLabelTophome">Quoted Total price</td>
																		</tr>
																		<% 
																			Double dQuantity=new Double(0);
														                    Double dTotalTransportation=new Double(0);
														                    Double dTotalWarranty=new Double(0);
														                    Double dTotalTc=new Double(0);
														                    Double dMcnspRatio=new Double(0);
														                    BigDecimal bdListPrice=new BigDecimal("0");
														                    BigDecimal bdTotalPriceStd=new BigDecimal("0");
														                    BigDecimal bdTotalPriceReq=new BigDecimal("0");
														                    BigDecimal bdTotalPriceAprv=new BigDecimal("0");
														                    BigDecimal bdTotalPriceQuoted=new BigDecimal("0");
														                    String sListPrice="", sTotalPrice="", sReqTotalPrice="", sAprvTotalPrice="", sQuotedTotalPrice="", sSupervisionCharges="";
																		%>
																		<logic:iterate name="newEnquiryDto" property="newRatingsList" id="ratingObject" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
																			<bean:define id="qnt" name="ratingObject" property="quantity"/>
								                      						<bean:define id="listPriceVal" name="ratingObject" property="ltLPPerUnit"/>
								                      						<bean:define id="priceVal" name="ratingObject" property="ltTotalOrderPrice"/>
								                      						<bean:define id="reqPriceVal" name="ratingObject" property="ltRequestedTotalPrice_SM"/>
								                      						<bean:define id="aprvPriceVal" name="ratingObject" property="ltApprovedTotalPrice_RSM"/>
								                      						<bean:define id="quotedPriceVal" name="ratingObject" property="ltQuotedTotalPrice"/>
																			<% 
								                      							sListPrice=String.valueOf(listPriceVal);
								                      						
									                      						if(sListPrice!="" && sListPrice!=QuotationConstants.QUOTATION_ZERO) {
									                      							if(sListPrice.indexOf(",") != -1){
									                      								sListPrice = sListPrice.replaceAll(",", "");
																					}
									                      						}
								                      						%>
								                      						<tr>
								                      							<td width="3%" class="formContentview-rating-b0x"><%=idx + 1%></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltProdLineId" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltKWId" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltPoleId" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltVoltId" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltFrameId" /><bean:write name="ratingObject" property="ltFrameSuffixId" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="quantity" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltStandardDiscount" /> %</td>
																				<td width="7%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltPricePerUnit" /></td>
																				<td width="7%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltTotalOrderPrice" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltRequestedDiscount_SM" /> %</td>
																				<td width="7%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltRequestedPricePerUnit_SM" /></td>
																				<td width="7%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltRequestedTotalPrice_SM" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltApprovedDiscount_RSM" /> %</td>
																				<td width="7%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltApprovedPricePerunit_RSM" /></td>
																				<td width="7%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltApprovedTotalPrice_RSM" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltQuotedDiscount" /> %</td>
																				<td width="7%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltQuotedPricePerUnit" /></td>
																				<td width="7%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltQuotedTotalPrice" /></td>
								                      						</tr>
								                      						<% 
															                    Double dQty=new Double(0);
															                    			                    
															                    sTotalPrice=String.valueOf(priceVal);
															                    sReqTotalPrice=String.valueOf(reqPriceVal);
															                    sAprvTotalPrice=String.valueOf(aprvPriceVal);
															                    sQuotedTotalPrice=String.valueOf(quotedPriceVal);
															                    
															                    if(qnt!="" && qnt != "0") {
															       					dQty = Double.parseDouble(String.valueOf(qnt));
															       					dQuantity = dQuantity + dQty;
																				}
															                    
															                    if(sTotalPrice!="" && sTotalPrice!=QuotationConstants.QUOTATION_ZERO) {
															                    	if(sTotalPrice.indexOf(",") != -1){
															                        	String unitprice=sTotalPrice.replaceAll(",", "");
															                    		BigDecimal a=new BigDecimal(unitprice);
															                    		bdTotalPriceStd=bdTotalPriceStd.add(a);      
																					} else {
															                          	BigDecimal a=new BigDecimal(sTotalPrice);
															                          	bdTotalPriceStd=bdTotalPriceStd.add(a);      
																					}
															                    }
															                    
															                    if(sReqTotalPrice!="" && sReqTotalPrice!=QuotationConstants.QUOTATION_ZERO) {
															                    	if(sReqTotalPrice.indexOf(",") != -1){
															                        	String unitprice=sReqTotalPrice.replaceAll(",", "");
															                    		BigDecimal a=new BigDecimal(unitprice);
															                    		bdTotalPriceReq=bdTotalPriceReq.add(a);      
																					} else {
															                          	BigDecimal a=new BigDecimal(sReqTotalPrice);
															                          	bdTotalPriceReq=bdTotalPriceReq.add(a);      
																					}
															                    }
															                    
															                    if(sAprvTotalPrice!="" && sAprvTotalPrice!=QuotationConstants.QUOTATION_ZERO) {
															                    	if(sAprvTotalPrice.indexOf(",") != -1){
															                        	String unitprice=sAprvTotalPrice.replaceAll(",", "");
															                    		BigDecimal a=new BigDecimal(unitprice);
															                    		bdTotalPriceAprv=bdTotalPriceAprv.add(a);      
																					} else {
															                          	BigDecimal a=new BigDecimal(sAprvTotalPrice);
															                          	bdTotalPriceAprv=bdTotalPriceAprv.add(a);      
																					}
															                    }
															                    
															                    if(sQuotedTotalPrice!="" && sQuotedTotalPrice!=QuotationConstants.QUOTATION_ZERO) {
															                    	if(sQuotedTotalPrice.indexOf(",") != -1){
															                    		String unitprice=sQuotedTotalPrice.replaceAll(",", "");
															                    		BigDecimal a=new BigDecimal(unitprice);
															                    		bdTotalPriceQuoted=bdTotalPriceQuoted.add(a);
															                    	} else {
															                    		BigDecimal a=new BigDecimal(sQuotedTotalPrice);
															                    		bdTotalPriceQuoted=bdTotalPriceQuoted.add(a);  
															                    	}
															                    }
																			%>
																		</logic:iterate>
																		<tr>
																			<td colspan="19" class="formContentview-rating-b0x">&nbsp;</td>
																		</tr>
																		<logic:equal name="newEnquiryDto" property="supervisionDetails" value="Yes"> 
																			<bean:define id="superviseManDays" name="newEnquiryDto" property="superviseNoOfManDays"/>
																			<% 
																				int iSuperviseManDays = Integer.parseInt(String.valueOf(superviseManDays));
																				long supervisionCharges = iSuperviseManDays * QuotationConstants.QUOTATIONLT_MANDAY_SUPERVISE_CHARGES;
																				BigDecimal a=new BigDecimal(supervisionCharges);
																				bdTotalPriceStd = bdTotalPriceStd.add(a);
																				if(sReqTotalPrice!="" && sReqTotalPrice!=QuotationConstants.QUOTATION_ZERO) {
																					bdTotalPriceReq = bdTotalPriceReq.add(a);
																				}
																				if(sAprvTotalPrice!="" && sAprvTotalPrice!=QuotationConstants.QUOTATION_ZERO) {
																					bdTotalPriceAprv = bdTotalPriceAprv.add(a);	
																				}
																				if(sQuotedTotalPrice!="" && sQuotedTotalPrice!=QuotationConstants.QUOTATION_ZERO) {
																					bdTotalPriceQuoted = bdTotalPriceQuoted.add(a);
																				}
																				sSupervisionCharges = CurrencyConvertUtility.buildPriceFormat(String.valueOf(supervisionCharges));
																			%>
																			<tr>
																				<td colspan="2" class="formContentview-rating-b0x">&nbsp;</td>
																				<td colspan="4" align="left" class="formContentview-rating-b0x"><b> Supervision and Comissioning charges</b></td>
																				<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
																				<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
																				<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
																				<td class="formContentview-rating-b0x"><b> &#8377;  <%=sSupervisionCharges%> </b></td>
																				<td colspan="9" class="formContentview-rating-b0x">&nbsp;</td>
																			</tr>
																		</logic:equal>
																		<% 
																			sTotalPrice = CurrencyConvertUtility.buildPriceFormat(bdTotalPriceStd.toString());
																			sReqTotalPrice = CurrencyConvertUtility.buildPriceFormat(bdTotalPriceReq.toString());
																			sAprvTotalPrice = CurrencyConvertUtility.buildPriceFormat(bdTotalPriceAprv.toString());
																			sQuotedTotalPrice = CurrencyConvertUtility.buildPriceFormat(bdTotalPriceQuoted.toString());
																		%>
																		<tr>
																			<td colspan="5" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td align="left" class="formContentview-rating-b0x"><b><bean:message key="quotation.create.label.total"/></b></td>
							                       							<td class="formContentview-rating-b0x"> <b> <%=df1.format(dQuantity)%> </b> </td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td class="formContentview-rating-b0x"><b> &#8377;  <%=sTotalPrice%></b></td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td class="formContentview-rating-b0x"><b> &#8377;  <%=sReqTotalPrice%></b></td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td class="formContentview-rating-b0x"><b> &#8377;  <%=sAprvTotalPrice%></b></td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td class="formContentview-rating-b0x"><b> &#8377;  <%=sQuotedTotalPrice%></b></td>
																		</tr>
																		<tr>
																			<td colspan="2" class="formContentview-rating-b0x"><b><bean:message key="quotation.create.label.inwords"/></b></td>
																			<td colspan="17" class="formContentview-rating-b0x" align="right">
																				<b>	<%=CurrencyConvertUtility.convertToIndianCurrency(String.valueOf(bdTotalPriceStd)) %> </b>
																			</td>
																		</tr>
																	</logic:equal>
																	<!-- ------------------------------------ Commercial Offer : SM : End -------------------------------------- -->
																	
																	
																	<!-- ------------------------------------ Commercial Offer : RSM : Start ----------------------------------- -->
																	<logic:equal name="APPROVALTYPE" scope="request" value="RSM">
																		<tr>
																			<td width="3%" class="formLabelTophome"><bean:message key="quotation.create.label.itemno" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.label.admin.field.product.line" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.create.label.ratedoutput" /> ( <bean:message key="quotation.option.kw" /> )</td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.create.label.pole" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.create.label.ratedvoltage" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.search.frame" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.offerdata.addon.label.quantity" /></td>
																			<td width="5%" class="formLabelTophome">Standard Discount</td>
																			<td width="7%" class="formLabelTophome">Standard Unit Price</td>
																			<td width="7%" class="formLabelTophome">Standard Total price</td>
																			<td width="5%" class="formLabelTophome">Requested Discount (SM)</td>
																			<td width="7%" class="formLabelTophome">Requested Unit Price (SM)</td>
																			<td width="7%" class="formLabelTophome">Requested Total Price (SM)</td>
																			<td width="5%" class="formLabelTophome">Requested Discount (RSM)</td>
																			<td width="7%" class="formLabelTophome">Requested Unit Price (RSM)</td>
																			<td width="7%" class="formLabelTophome">Requested Total Price (RSM)</td>
																			<td width="5%" class="formLabelTophome">Approved Discount (RSM)</td>
																			<td width="7%" class="formLabelTophome">Approved Unit Price (RSM)</td>
																			<td width="7%" class="formLabelTophome">Approved Total Price (RSM)</td>
																			<td width="5%" class="formLabelTophome">Approved Discount (NSM)</td>
																			<td width="5%" class="formLabelTophome">Quoted Discount</td>
																			<td width="7%" class="formLabelTophome">Quoted Unit Price</td>
																			<td width="7%" class="formLabelTophome">Quoted Total price</td>
																		</tr>
																		<% 
																			Double dQuantity=new Double(0);
														                    Double dTotalTransportation=new Double(0);
														                    Double dTotalWarranty=new Double(0);
														                    Double dTotalTc=new Double(0);
														                    Double dMcnspRatio=new Double(0);
														                    BigDecimal bdListPrice=new BigDecimal("0");
														                    BigDecimal bdTotalPriceStd=new BigDecimal("0");
														                    BigDecimal bdTotalPriceReq_SM=new BigDecimal("0");
														                    BigDecimal bdTotalPriceReq_RSM=new BigDecimal("0");
														                    BigDecimal bdTotalPriceAprv=new BigDecimal("0");
														                    BigDecimal bdTotalPriceQuoted=new BigDecimal("0");
														                    String sListPrice="", sTotalPrice="", sReqTotalPrice_SM="", sReqTotalPrice_RSM="", sAprvTotalPrice="", sQuotedTotalPrice="", sSupervisionCharges="";
																		%>
																		<logic:iterate name="newEnquiryDto" property="newRatingsList" id="ratingObject" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
								                      						<bean:define id="qnt" name="ratingObject" property="quantity"/>
								                      						<bean:define id="listPriceVal" name="ratingObject" property="ltLPPerUnit"/>
								                      						<bean:define id="priceVal" name="ratingObject" property="ltTotalOrderPrice"/>
								                      						<bean:define id="reqPriceValSM" name="ratingObject" property="ltRequestedTotalPrice_SM"/>
								                      						<bean:define id="reqPriceValRSM" name="ratingObject" property="ltRequestedTotalPrice_RSM"/>
								                      						<bean:define id="aprvPriceVal" name="ratingObject" property="ltApprovedTotalPrice_RSM"/>
								                      						<bean:define id="quotedPriceVal" name="ratingObject" property="ltQuotedTotalPrice"/>
								                      						<% 
								                      							sListPrice=String.valueOf(listPriceVal);
								                      						
									                      						if(sListPrice!="" && sListPrice!=QuotationConstants.QUOTATION_ZERO) {
									                      							if(sListPrice.indexOf(",") != -1){
									                      								sListPrice = sListPrice.replaceAll(",", "");
																					}
									                      						}
								                      						%>
								                      						<tr>
								                      							<td width="3%" class="formContentview-rating-b0x"><%=idx + 1%></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltProdLineId" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltKWId" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltPoleId" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltVoltId" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltFrameId" /><bean:write name="ratingObject" property="ltFrameSuffixId" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="quantity" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltStandardDiscount" /> %</td>
																				<td width="7%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltPricePerUnit" /></td>
																				<td width="7%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltTotalOrderPrice" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltRequestedDiscount_SM" /> %</td>
																				<td width="7%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltRequestedPricePerUnit_SM" /></td>
																				<td width="7%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltRequestedTotalPrice_SM" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltRequestedDiscount_RSM" /> %</td>
																				<td width="7%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltRequestedPricePerUnit_RSM" /></td>
																				<td width="7%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltRequestedTotalPrice_RSM" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltApprovedDiscount_RSM" /> %</td>
																				<td width="7%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltApprovedPricePerunit_RSM" /></td>
																				<td width="7%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltApprovedTotalPrice_RSM" /></td>
																				<td width="3%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltApprovedDiscount_NSM" /> %</td>
																				<td width="3%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltQuotedDiscount" /> %</td>
																				<td width="10%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltQuotedPricePerUnit" /></td>
																				<td width="10%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltQuotedTotalPrice" /></td>
																			</tr>
																			<% 
															                    Double dQty=new Double(0);
															                    			                    
															                    sTotalPrice=String.valueOf(priceVal);
															                    sReqTotalPrice_SM=String.valueOf(reqPriceValSM);
															                    sReqTotalPrice_RSM=String.valueOf(reqPriceValRSM);
															                    sAprvTotalPrice=String.valueOf(aprvPriceVal);
															                    sQuotedTotalPrice=String.valueOf(quotedPriceVal);
															                    
															                    if(qnt!="" && qnt != "0") {
															       					dQty = Double.parseDouble(String.valueOf(qnt));
															       					dQuantity = dQuantity + dQty;
																				}
															                    
															                    if(sTotalPrice!="" && sTotalPrice!=QuotationConstants.QUOTATION_ZERO) {
															                    	if(sTotalPrice.indexOf(",") != -1){
															                        	String unitprice=sTotalPrice.replaceAll(",", "");
															                    		BigDecimal a=new BigDecimal(unitprice);
															                    		bdTotalPriceStd=bdTotalPriceStd.add(a);      
																					} else {
															                          	BigDecimal a=new BigDecimal(sTotalPrice);
															                          	bdTotalPriceStd=bdTotalPriceStd.add(a);      
																					}
															                    }
															                    
															                    if(sReqTotalPrice_SM!="" && sReqTotalPrice_SM!=QuotationConstants.QUOTATION_ZERO) {
															                    	if(sReqTotalPrice_SM.indexOf(",") != -1){
															                        	String unitprice=sReqTotalPrice_SM.replaceAll(",", "");
															                    		BigDecimal a=new BigDecimal(unitprice);
															                    		bdTotalPriceReq_SM=bdTotalPriceReq_SM.add(a);      
																					} else {
															                          	BigDecimal a=new BigDecimal(sReqTotalPrice_SM);
															                          	bdTotalPriceReq_SM=bdTotalPriceReq_SM.add(a);      
																					}
															                    }
															                    
															                    if(sReqTotalPrice_RSM!="" && sReqTotalPrice_RSM!=QuotationConstants.QUOTATION_ZERO) {
															                    	if(sReqTotalPrice_RSM.indexOf(",") != -1){
															                        	String unitprice=sReqTotalPrice_RSM.replaceAll(",", "");
															                    		BigDecimal a=new BigDecimal(unitprice);
															                    		bdTotalPriceReq_RSM=bdTotalPriceReq_RSM.add(a);      
																					} else {
															                          	BigDecimal a=new BigDecimal(sReqTotalPrice_RSM);
															                          	bdTotalPriceReq_RSM=bdTotalPriceReq_RSM.add(a);      
																					}
															                    }
															                    
															                    if(sAprvTotalPrice!="" && sAprvTotalPrice!=QuotationConstants.QUOTATION_ZERO) {
															                    	if(sTotalPrice.indexOf(",") != -1){
															                        	String unitprice=sAprvTotalPrice.replaceAll(",", "");
															                    		BigDecimal a=new BigDecimal(unitprice);
															                    		bdTotalPriceAprv=bdTotalPriceAprv.add(a);      
																					} else {
															                          	BigDecimal a=new BigDecimal(sAprvTotalPrice);
															                          	bdTotalPriceAprv=bdTotalPriceAprv.add(a);      
																					}
															                    }
															                    
															                    if(sQuotedTotalPrice!="" && sQuotedTotalPrice!=QuotationConstants.QUOTATION_ZERO) {
															                    	if(sQuotedTotalPrice.indexOf(",") != -1){
															                    		String unitprice=sQuotedTotalPrice.replaceAll(",", "");
															                    		BigDecimal a=new BigDecimal(unitprice);
															                    		bdTotalPriceQuoted=bdTotalPriceQuoted.add(a);
															                    	} else {
															                    		BigDecimal a=new BigDecimal(sQuotedTotalPrice);
															                    		bdTotalPriceQuoted=bdTotalPriceQuoted.add(a);  
															                    	}
															                    }
																			%>
																		</logic:iterate>
																		<tr>
																			<td colspan="23" class="formContentview-rating-b0x">&nbsp;</td>
																		</tr>
																		<logic:equal name="newEnquiryDto" property="supervisionDetails" value="Yes"> 
																			<bean:define id="superviseManDays" name="newEnquiryDto" property="superviseNoOfManDays"/>
																			<% 
																				int iSuperviseManDays = Integer.parseInt(String.valueOf(superviseManDays));
																				long supervisionCharges = iSuperviseManDays * QuotationConstants.QUOTATIONLT_MANDAY_SUPERVISE_CHARGES;
																				BigDecimal a=new BigDecimal(supervisionCharges);
																				bdTotalPriceStd = bdTotalPriceStd.add(a);
																				if(sReqTotalPrice_SM!="" && sReqTotalPrice_SM!=QuotationConstants.QUOTATION_ZERO) {
																					bdTotalPriceReq_SM = bdTotalPriceReq_SM.add(a);
																				}
																				if(sReqTotalPrice_RSM!="" && sReqTotalPrice_RSM!=QuotationConstants.QUOTATION_ZERO) {
																					bdTotalPriceReq_RSM = bdTotalPriceReq_RSM.add(a);
																				}
																				if(sAprvTotalPrice!="" && sAprvTotalPrice!=QuotationConstants.QUOTATION_ZERO) {
																					bdTotalPriceAprv = bdTotalPriceAprv.add(a);	
																				}
																				if(sQuotedTotalPrice!="" && sQuotedTotalPrice!=QuotationConstants.QUOTATION_ZERO) {
																					bdTotalPriceQuoted = bdTotalPriceQuoted.add(a);
																				}
																				sSupervisionCharges = CurrencyConvertUtility.buildPriceFormat(String.valueOf(supervisionCharges));
																			%>
																			<tr>
																				<td colspan="2" class="formContentview-rating-b0x">&nbsp;</td>
																				<td colspan="4" align="left" class="formContentview-rating-b0x"><b> Supervision and Comissioning charges</b></td>
																				<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
																				<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
																				<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
																				<td class="formContentview-rating-b0x"><b> &#8377;  <%=sSupervisionCharges%> </b></td>
																				<td colspan="13" class="formContentview-rating-b0x">&nbsp;</td>
																			</tr>
																		</logic:equal>
																		
																		<% 
																			sTotalPrice = CurrencyConvertUtility.buildPriceFormat(bdTotalPriceStd.toString());
																			sReqTotalPrice_SM = CurrencyConvertUtility.buildPriceFormat(bdTotalPriceReq_SM.toString());
																			sReqTotalPrice_RSM = CurrencyConvertUtility.buildPriceFormat(bdTotalPriceReq_RSM.toString());
																			sAprvTotalPrice = CurrencyConvertUtility.buildPriceFormat(bdTotalPriceAprv.toString());
																			sQuotedTotalPrice = CurrencyConvertUtility.buildPriceFormat(bdTotalPriceQuoted.toString());
																		%>
																		<tr>
																			<td colspan="5" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td align="left" class="formContentview-rating-b0x"><b><bean:message key="quotation.create.label.total"/></b></td>
							                       							<td class="formContentview-rating-b0x"> <b> <%=df1.format(dQuantity)%> </b> </td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td class="formContentview-rating-b0x"><b> &#8377;  <%=sTotalPrice%></b></td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td class="formContentview-rating-b0x"><b> &#8377;  <%=sReqTotalPrice_SM%> </b></td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td class="formContentview-rating-b0x"><b> &#8377;  <%=sReqTotalPrice_RSM%> </b></td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td class="formContentview-rating-b0x"><b> &#8377;  <%=sAprvTotalPrice%> </b></td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td class="formContentview-rating-b0x"><b> &#8377;  <%=sQuotedTotalPrice%></b></td>
																		</tr>
																		<tr>
																			<td colspan="2" class="formContentview-rating-b0x"><b><bean:message key="quotation.create.label.inwords"/></b></td>
																			<td colspan="21" class="formContentview-rating-b0x" align="right">
																				<b>	<%=CurrencyConvertUtility.convertToIndianCurrency(String.valueOf(bdTotalPriceStd)) %> </b>
																			</td>
																		</tr>
																	</logic:equal>
																	<!-- ------------------------------------ Commercial Offer : RSM : End ------------------------------------- -->
																	
																	
																	<!-- ------------------------------------ Commercial Offer : NSM : Start ----------------------------------- -->
																	<logic:equal name="APPROVALTYPE" scope="request" value="NSM">
																		<tr>
																			<td width="3%" class="formLabelTophome"><bean:message key="quotation.create.label.itemno" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.label.admin.field.product.line" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.create.label.ratedoutput" /> ( <bean:message key="quotation.option.kw" /> )</td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.create.label.pole" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.create.label.ratedvoltage" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.search.frame" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.offerdata.addon.label.quantity" /></td>
																			<td width="5%" class="formLabelTophome">Standard Discount</td>
																			<td width="7%" class="formLabelTophome">Standard Unit Price</td>
																			<td width="7%" class="formLabelTophome">Standard Total price</td>
																			<td width="5%" class="formLabelTophome">Requested Discount (RSM)</td>
																			<td width="7%" class="formLabelTophome">Requested Unit Price (RSM)</td>
																			<td width="7%" class="formLabelTophome">Requested Total Price (RSM)</td>
																			<td width="5%" class="formLabelTophome">Requested Discount (NSM)</td>
																			<td width="7%" class="formLabelTophome">Requested Unit Price (NSM)</td>
																			<td width="7%" class="formLabelTophome">Requested Total Price (NSM)</td>
																			<td width="5%" class="formLabelTophome">Approved Discount (NSM)</td>
																			<td width="7%" class="formLabelTophome">Approved Unit Price (NSM)</td>
																			<td width="7%" class="formLabelTophome">Approved Total Price (NSM)</td>
																			<td width="5%" class="formLabelTophome">Approved Discount (MH)</td>
																			<td width="5%" class="formLabelTophome">Quoted Discount</td>
																			<td width="7%" class="formLabelTophome">Quoted Unit Price</td>
																			<td width="7%" class="formLabelTophome">Quoted Total Price</td>
																		</tr>
																		<% 
																			Double dQuantity=new Double(0);
														                    Double dTotalTransportation=new Double(0);
														                    Double dTotalWarranty=new Double(0);
														                    Double dTotalTc=new Double(0);
														                    Double dMcnspRatio=new Double(0);
														                    BigDecimal bdListPrice=new BigDecimal("0");
														                    BigDecimal bdTotalPriceStd=new BigDecimal("0");
														                    BigDecimal bdTotalPriceReq_RSM=new BigDecimal("0");
														                    BigDecimal bdTotalPriceReq_NSM=new BigDecimal("0");
														                    BigDecimal bdTotalPriceAprv=new BigDecimal("0");
														                    BigDecimal bdTotalPriceQuoted=new BigDecimal("0");
														                    String sListPrice="", sTotalPrice="", sReqTotalPrice_RSM="", sReqTotalPrice_NSM="", sAprvTotalPrice="", sQuotedTotalPrice="", sSupervisionCharges="";
																		%>
																		<logic:iterate name="newEnquiryDto" property="newRatingsList" id="ratingObject" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
								                      						<bean:define id="qnt" name="ratingObject" property="quantity"/>
								                      						<bean:define id="listPriceVal" name="ratingObject" property="ltLPPerUnit"/>
								                      						<bean:define id="priceVal" name="ratingObject" property="ltTotalOrderPrice"/>
								                      						<bean:define id="reqPriceValRSM" name="ratingObject" property="ltRequestedTotalPrice_RSM"/>
								                      						<bean:define id="reqPriceValNSM" name="ratingObject" property="ltRequestedTotalPrice_NSM"/>
								                      						<bean:define id="aprvPriceVal" name="ratingObject" property="ltApprovedTotalPrice_NSM"/>
								                      						<bean:define id="quotedPriceVal" name="ratingObject" property="ltQuotedTotalPrice"/>
								                      						<% 
								                      							sListPrice=String.valueOf(listPriceVal);
								                      						
									                      						if(sListPrice!="" && sListPrice!=QuotationConstants.QUOTATION_ZERO) {
									                      							if(sListPrice.indexOf(",") != -1){
									                      								sListPrice = sListPrice.replaceAll(",", "");
																					}
									                      						}
								                      						%>
								                      						<tr>
								                      							<td width="3%" class="formContentview-rating-b0x"><%=idx + 1%></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltProdLineId" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltKWId" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltPoleId" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltVoltId" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltFrameId" /><bean:write name="ratingObject" property="ltFrameSuffixId" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="quantity" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltStandardDiscount" /> %</td>
																				<td width="7%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltPricePerUnit" /></td>
																				<td width="7%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltTotalOrderPrice" /></td>
																				<td width="3%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltRequestedDiscount_RSM" /> %</td>
																				<td width="10%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltRequestedPricePerUnit_RSM" /></td>
																				<td width="10%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltRequestedTotalPrice_RSM" /></td>
																				<td width="3%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltRequestedDiscount_NSM" /> %</td>
																				<td width="10%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltRequestedPricePerUnit_NSM" /></td>
																				<td width="10%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltRequestedTotalPrice_NSM" /></td>
																				<td width="3%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltApprovedDiscount_NSM" /> %</td>
																				<td width="10%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltApprovedPricePerunit_NSM" /></td>
																				<td width="10%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltApprovedTotalPrice_NSM" /></td>
																				<td width="3%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltApprovedDiscount_MH" /> %</td>
																				<td width="3%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltQuotedDiscount" /> %</td>
																				<td width="10%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltQuotedPricePerUnit" /></td>
																				<td width="10%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltQuotedTotalPrice" /></td>
																			</tr>
																			<% 
															                    Double dQty=new Double(0);
															                    			                    
															                    sTotalPrice=String.valueOf(priceVal);
															                    sReqTotalPrice_RSM=String.valueOf(reqPriceValRSM);
															                    sReqTotalPrice_NSM=String.valueOf(reqPriceValNSM);
															                    sAprvTotalPrice=String.valueOf(aprvPriceVal);
															                    sQuotedTotalPrice=String.valueOf(quotedPriceVal);
															                    
															                    if(qnt!="" && qnt != "0") {
															       					dQty = Double.parseDouble(String.valueOf(qnt));
															       					dQuantity = dQuantity + dQty;
																				}
															                    
															                    if(sTotalPrice!="" && sTotalPrice!=QuotationConstants.QUOTATION_ZERO) {
															                    	if(sTotalPrice.indexOf(",") != -1){
															                        	String unitprice=sTotalPrice.replaceAll(",", "");
															                    		BigDecimal a=new BigDecimal(unitprice);
															                    		bdTotalPriceStd=bdTotalPriceStd.add(a);      
																					} else {
															                          	BigDecimal a=new BigDecimal(sTotalPrice);
															                          	bdTotalPriceStd=bdTotalPriceStd.add(a);      
																					}
															                    }
															                    
															                    if(sReqTotalPrice_RSM!="" && sReqTotalPrice_RSM!=QuotationConstants.QUOTATION_ZERO) {
															                    	if(sReqTotalPrice_RSM.indexOf(",") != -1){
															                        	String unitprice=sReqTotalPrice_RSM.replaceAll(",", "");
															                    		BigDecimal a=new BigDecimal(unitprice);
															                    		bdTotalPriceReq_RSM=bdTotalPriceReq_RSM.add(a);      
																					} else {
															                          	BigDecimal a=new BigDecimal(sReqTotalPrice_RSM);
															                          	bdTotalPriceReq_RSM=bdTotalPriceReq_RSM.add(a);      
																					}
															                    }
															                    
															                    if(sReqTotalPrice_NSM!="" && sReqTotalPrice_NSM!=QuotationConstants.QUOTATION_ZERO) {
															                    	if(sReqTotalPrice_NSM.indexOf(",") != -1){
															                        	String unitprice=sReqTotalPrice_NSM.replaceAll(",", "");
															                    		BigDecimal a=new BigDecimal(unitprice);
															                    		bdTotalPriceReq_NSM=bdTotalPriceReq_NSM.add(a);      
																					} else {
															                          	BigDecimal a=new BigDecimal(sReqTotalPrice_NSM);
															                          	bdTotalPriceReq_NSM=bdTotalPriceReq_NSM.add(a);      
																					}
															                    }
															                    
															                    if(sAprvTotalPrice!="" && sAprvTotalPrice!=QuotationConstants.QUOTATION_ZERO) {
															                    	if(sTotalPrice.indexOf(",") != -1){
															                        	String unitprice=sAprvTotalPrice.replaceAll(",", "");
															                    		BigDecimal a=new BigDecimal(unitprice);
															                    		bdTotalPriceAprv=bdTotalPriceAprv.add(a);      
																					} else {
															                          	BigDecimal a=new BigDecimal(sAprvTotalPrice);
															                          	bdTotalPriceAprv=bdTotalPriceAprv.add(a);      
																					}
															                    }
															                    
															                    if(sQuotedTotalPrice!="" && sQuotedTotalPrice!=QuotationConstants.QUOTATION_ZERO) {
															                    	if(sQuotedTotalPrice.indexOf(",") != -1){
															                    		String unitprice=sQuotedTotalPrice.replaceAll(",", "");
															                    		BigDecimal a=new BigDecimal(unitprice);
															                    		bdTotalPriceQuoted=bdTotalPriceQuoted.add(a);
															                    	} else {
															                    		BigDecimal a=new BigDecimal(sQuotedTotalPrice);
															                    		bdTotalPriceQuoted=bdTotalPriceQuoted.add(a);  
															                    	}
															                    }
																			%>
																		</logic:iterate>
																		<tr>
																			<td colspan="23" class="formContentview-rating-b0x">&nbsp;</td>
																		</tr>
																		<logic:equal name="newEnquiryDto" property="supervisionDetails" value="Yes"> 
																			<bean:define id="superviseManDays" name="newEnquiryDto" property="superviseNoOfManDays"/>
																			<% 
																				int iSuperviseManDays = Integer.parseInt(String.valueOf(superviseManDays));
																				long supervisionCharges = iSuperviseManDays * QuotationConstants.QUOTATIONLT_MANDAY_SUPERVISE_CHARGES;
																				BigDecimal a=new BigDecimal(supervisionCharges);
																				bdTotalPriceStd = bdTotalPriceStd.add(a);
																				if(sReqTotalPrice_RSM!="" && sReqTotalPrice_RSM!=QuotationConstants.QUOTATION_ZERO) {
																					bdTotalPriceReq_RSM = bdTotalPriceReq_RSM.add(a);
																				}
																				if(sReqTotalPrice_NSM!="" && sReqTotalPrice_NSM!=QuotationConstants.QUOTATION_ZERO) {
																					bdTotalPriceReq_NSM = bdTotalPriceReq_NSM.add(a);
																				}
																				if(sAprvTotalPrice!="" && sAprvTotalPrice!=QuotationConstants.QUOTATION_ZERO) {
																					bdTotalPriceAprv = bdTotalPriceAprv.add(a);	
																				}
																				if(sQuotedTotalPrice!="" && sQuotedTotalPrice!=QuotationConstants.QUOTATION_ZERO) {
																					bdTotalPriceQuoted = bdTotalPriceQuoted.add(a);
																				}
																				sSupervisionCharges = CurrencyConvertUtility.buildPriceFormat(String.valueOf(supervisionCharges));
																			%>
																			<tr>
																				<td colspan="2" class="formContentview-rating-b0x">&nbsp;</td>
																				<td colspan="4" align="left" class="formContentview-rating-b0x"><b> Supervision and Comissioning charges</b></td>
																				<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
																				<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
																				<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
																				<td class="formContentview-rating-b0x"><b> &#8377;  <%=sSupervisionCharges%> </b></td>
																				<td colspan="13" class="formContentview-rating-b0x">&nbsp;</td>
																			</tr>
																		</logic:equal>
																		
																		<% 
																			sTotalPrice = CurrencyConvertUtility.buildPriceFormat(bdTotalPriceStd.toString());
																			sReqTotalPrice_RSM = CurrencyConvertUtility.buildPriceFormat(bdTotalPriceReq_RSM.toString());
																			sReqTotalPrice_NSM = CurrencyConvertUtility.buildPriceFormat(bdTotalPriceReq_NSM.toString());
																			sAprvTotalPrice = CurrencyConvertUtility.buildPriceFormat(bdTotalPriceAprv.toString());
																			sQuotedTotalPrice = CurrencyConvertUtility.buildPriceFormat(bdTotalPriceQuoted.toString());
																		%>
																		<tr>
																			<td colspan="5" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td align="left" class="formContentview-rating-b0x"><b><bean:message key="quotation.create.label.total"/></b></td>
							                       							<td class="formContentview-rating-b0x"> <b> <%=df1.format(dQuantity)%> </b> </td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td class="formContentview-rating-b0x"><b> &#8377;  <%=sTotalPrice%></b></td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td class="formContentview-rating-b0x"><b> &#8377;  <%=sReqTotalPrice_RSM%></b></td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td class="formContentview-rating-b0x"><b> &#8377;  <%=sReqTotalPrice_NSM%> </b></td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td class="formContentview-rating-b0x"><b> &#8377;  <%=sAprvTotalPrice%> </b></td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td class="formContentview-rating-b0x"><b> &#8377;  <%=sQuotedTotalPrice%></b></td>
																		</tr>
																		<tr>
																			<td colspan="2" class="formContentview-rating-b0x"><b><bean:message key="quotation.create.label.inwords"/></b></td>
																			<td colspan="21" class="formContentview-rating-b0x" align="right">
																				<b>	<%=CurrencyConvertUtility.convertToIndianCurrency(String.valueOf(bdTotalPriceStd)) %> </b>
																			</td>
																		</tr>
																	</logic:equal>
																	<!-- ------------------------------------ Commercial Offer : NSM : End ------------------------------------- -->
																	
																	
																	<!-- ------------------------------------ Commercial Offer : MH : Start ----------------------------------- -->
																	<logic:equal name="APPROVALTYPE" scope="request" value="MH">
																		<tr>
																			<td width="3%" class="formLabelTophome"><bean:message key="quotation.create.label.itemno" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.label.admin.field.product.line" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.create.label.ratedoutput" /> ( <bean:message key="quotation.option.kw" /> )</td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.create.label.pole" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.create.label.ratedvoltage" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.search.frame" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.offerdata.addon.label.quantity" /></td>
																			<td width="5%" class="formLabelTophome">Standard Discount</td>
																			<td width="6%" class="formLabelTophome">Standard Unit Price</td>
																			<td width="6%" class="formLabelTophome">Standard Total price</td>
																			<td width="5%" class="formLabelTophome">Requested Discount (NSM)</td>
																			<td width="6%" class="formLabelTophome">Requested Unit Price (NSM)</td>
																			<td width="6%" class="formLabelTophome">Requested Total Price (NSM)</td>
																			<td width="5%" class="formLabelTophome">Approved Discount (MH)</td>
																			<td width="6%" class="formLabelTophome">Approved Unit Price (MH)</td>
																			<td width="6%" class="formLabelTophome">Approved Total Price (MH)</td>
																			<td width="5%" class="formLabelTophome">Quoted Discount</td>
																			<td width="6%" class="formLabelTophome">Quoted Unit Price</td>
																			<td width="6%" class="formLabelTophome">Quoted Total Price</td>
																			<td width="6%" class="formLabelTophome">Total Cost</td>
																			<td width="6%" class="formLabelTophome">Profit Margin (%)</td>
																		</tr>
																		<% 
																			Double dQuantity=new Double(0);
														                    Double dTotalTransportation=new Double(0);
														                    Double dTotalWarranty=new Double(0);
														                    Double dTotalTc=new Double(0);
														                    Double dMcnspRatio=new Double(0);
														                    Double dTotalCost=new Double(0);
														                    BigDecimal bdListPrice=new BigDecimal("0");
														                    BigDecimal bdTotalPriceStd=new BigDecimal("0");
														                    BigDecimal bdTotalPriceReq=new BigDecimal("0");
														                    BigDecimal bdTotalPriceAprv=new BigDecimal("0");
														                    BigDecimal bdTotalPriceQuoted=new BigDecimal("0");
														                    BigDecimal bdCummTotalCost=new BigDecimal("0");
														                    BigDecimal bdCummProfitMargin=new BigDecimal("0");
														                    String sListPrice="", sTotalPrice="", sReqTotalPrice="", sAprvTotalPrice="", sQuotedTotalPrice="", sSupervisionCharges="";
														                    String sCummulativeTotalCost="", sCummulativeProfitMargin="", sFormattedTotalCost="";
														                    int iCount = 0;
																		%>
																		<logic:iterate name="newEnquiryDto" property="newRatingsList" id="ratingObject" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
								                      						<bean:define id="qnt" name="ratingObject" property="quantity"/>
								                      						<bean:define id="listPriceVal" name="ratingObject" property="ltLPPerUnit"/>
								                      						<bean:define id="priceVal" name="ratingObject" property="ltTotalOrderPrice"/>
								                      						<bean:define id="reqPriceVal" name="ratingObject" property="ltRequestedTotalPrice_NSM"/>
								                      						<bean:define id="aprvPriceVal" name="ratingObject" property="ltApprovedTotalPrice_MH"/>
								                      						<bean:define id="quotedPriceVal" name="ratingObject" property="ltQuotedTotalPrice"/>
								                      						<bean:define id="totalCostVal" name="ratingObject" property="ratingTotalCost"/>
						                      								<bean:define id="profitMarginVal" name="ratingObject" property="ratingProfitMargin"/>
								                      						<% 
								                      							iCount = iCount + 1;
								                      							sListPrice=String.valueOf(listPriceVal);
								                      						
									                      						if(sListPrice!="" && sListPrice!=QuotationConstants.QUOTATION_ZERO) {
									                      							if(sListPrice.indexOf(",") != -1){
									                      								sListPrice = sListPrice.replaceAll(",", "");
																					}
									                      						}
									                      						sCummulativeTotalCost=String.valueOf(totalCostVal);
															                    sCummulativeProfitMargin=String.valueOf(profitMarginVal);
															                    dTotalCost = StringUtils.isNotBlank(sCummulativeTotalCost) ? Double.parseDouble(sCummulativeTotalCost.trim()) : 0d;
															                    sFormattedTotalCost = CurrencyConvertUtility.buildPriceFormat(String.valueOf(dTotalCost.longValue()));
								                      						%>
								                      						<tr>
								                      							<td width="3%" class="formContentview-rating-b0x"><%=idx + 1%></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltProdLineId" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltKWId" /></td>
																				<td width="4%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltPoleId" /></td>
																				<td width="4%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltVoltId" /></td>
																				<td width="4%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltFrameId" /><bean:write name="ratingObject" property="ltFrameSuffixId" /></td>
																				<td width="4%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="quantity" /></td>
																				<td width="6%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltStandardDiscount" /> %</td>
																				<td width="6%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltPricePerUnit" /></td>
																				<td width="6%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltTotalOrderPrice" /></td>
																				<td width="4%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltRequestedDiscount_NSM" /> %</td>
																				<td width="6%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltRequestedPricePerUnit_NSM" /></td>
																				<td width="6%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltRequestedTotalPrice_NSM" /></td> 
																				<td width="4%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltApprovedDiscount_MH" /> %</td>
																				<td width="6%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltApprovedPricePerunit_MH" /></td>
																				<td width="6%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltApprovedTotalPrice_MH" /></td>
																				<td width="4%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltQuotedDiscount" /> %</td>
																				<td width="6%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltQuotedPricePerUnit" /></td>
																				<td width="6%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltQuotedTotalPrice" /></td>
																				<td width="6%" class="formContentview-rating-b0x"> &#8377; <%=sFormattedTotalCost %></td>
																				<td width="6%" class="formContentview-rating-b0x"> <bean:write name="ratingObject" property="ratingProfitMargin" /> % </td>
																			</tr>
																			<% 
															                    Double dQty=new Double(0);
															                    			                    
															                    sTotalPrice=String.valueOf(priceVal);
															                    sReqTotalPrice=String.valueOf(reqPriceVal);
															                    sAprvTotalPrice=String.valueOf(aprvPriceVal);
															                    sQuotedTotalPrice=String.valueOf(quotedPriceVal);
															                    
															                    if(qnt!="" && qnt != "0") {
															       					dQty = Double.parseDouble(String.valueOf(qnt));
															       					dQuantity = dQuantity + dQty;
																				}
															                    if(sTotalPrice!="" && sTotalPrice!=QuotationConstants.QUOTATION_ZERO) {
															                    	if(sTotalPrice.indexOf(",") != -1){
															                        	String unitprice=sTotalPrice.replaceAll(",", "");
															                    		BigDecimal a=new BigDecimal(unitprice);
															                    		bdTotalPriceStd=bdTotalPriceStd.add(a);      
																					} else {
															                          	BigDecimal a=new BigDecimal(sTotalPrice);
															                          	bdTotalPriceStd=bdTotalPriceStd.add(a);      
																					}
															                    }
															                    if(sReqTotalPrice!="" && sReqTotalPrice!=QuotationConstants.QUOTATION_ZERO) {
															                    	if(sReqTotalPrice.indexOf(",") != -1){
															                        	String unitprice=sReqTotalPrice.replaceAll(",", "");
															                    		BigDecimal a=new BigDecimal(unitprice);
															                    		bdTotalPriceReq=bdTotalPriceReq.add(a);      
																					} else {
															                          	BigDecimal a=new BigDecimal(sReqTotalPrice);
															                          	bdTotalPriceReq=bdTotalPriceReq.add(a);      
																					}
															                    }
															                    if(sAprvTotalPrice!="" && sAprvTotalPrice!=QuotationConstants.QUOTATION_ZERO) {
															                    	if(sTotalPrice.indexOf(",") != -1){
															                        	String unitprice=sAprvTotalPrice.replaceAll(",", "");
															                    		BigDecimal a=new BigDecimal(unitprice);
															                    		bdTotalPriceAprv=bdTotalPriceAprv.add(a);      
																					} else {
															                          	BigDecimal a=new BigDecimal(sAprvTotalPrice);
															                          	bdTotalPriceAprv=bdTotalPriceAprv.add(a);      
																					}
															                    }
															                    if(sQuotedTotalPrice!="" && sQuotedTotalPrice!=QuotationConstants.QUOTATION_ZERO) {
															                    	if(sQuotedTotalPrice.indexOf(",") != -1){
															                    		String unitprice=sQuotedTotalPrice.replaceAll(",", "");
															                    		BigDecimal a=new BigDecimal(unitprice);
															                    		bdTotalPriceQuoted=bdTotalPriceQuoted.add(a);
															                    	} else {
															                    		BigDecimal a=new BigDecimal(sQuotedTotalPrice);
															                    		bdTotalPriceQuoted=bdTotalPriceQuoted.add(a);  
															                    	}
															                    }
															                    if(sCummulativeTotalCost!="" && sCummulativeTotalCost!=QuotationConstants.QUOTATION_ZERO) {
															                    	if(sCummulativeTotalCost.indexOf(",") != -1){
															                    		String unitprice=sCummulativeTotalCost.replaceAll(",", "");
															                    		BigDecimal a=new BigDecimal(unitprice);
															                    		bdCummTotalCost=bdCummTotalCost.add(a);
															                    	} else {
															                    		BigDecimal a=new BigDecimal(sCummulativeTotalCost);
															                    		bdCummTotalCost=bdCummTotalCost.add(a);  
															                    	}
															                    }
															                    if(sCummulativeProfitMargin!="" && sCummulativeProfitMargin!=QuotationConstants.QUOTATION_ZERO) {
															                    	if(sCummulativeProfitMargin.indexOf(",") != -1){
															                    		String unitprice=sCummulativeProfitMargin.replaceAll(",", "");
															                    		BigDecimal a=new BigDecimal(unitprice);
															                    		bdCummProfitMargin=bdCummProfitMargin.add(a);
															                    	} else {
															                    		BigDecimal a=new BigDecimal(sCummulativeProfitMargin);
															                    		bdCummProfitMargin=bdCummProfitMargin.add(a);  
															                    	}
															                    }
																			%>
																		</logic:iterate>
																		<tr>
																			<td colspan="21" class="formContentview-rating-b0x">&nbsp;</td>
																		</tr>
																		<logic:equal name="newEnquiryDto" property="supervisionDetails" value="Yes"> 
																			<bean:define id="superviseManDays" name="newEnquiryDto" property="superviseNoOfManDays"/>
																			<% 
																				int iSuperviseManDays = Integer.parseInt(String.valueOf(superviseManDays));
																				long supervisionCharges = iSuperviseManDays * QuotationConstants.QUOTATIONLT_MANDAY_SUPERVISE_CHARGES;
																				BigDecimal a=new BigDecimal(supervisionCharges);
																				bdTotalPriceStd = bdTotalPriceStd.add(a);
																				if(sReqTotalPrice!="" && sReqTotalPrice!=QuotationConstants.QUOTATION_ZERO) {
																					bdTotalPriceReq = bdTotalPriceReq.add(a);
																				}
																				if(sAprvTotalPrice!="" && sAprvTotalPrice!=QuotationConstants.QUOTATION_ZERO) {
																					bdTotalPriceAprv = bdTotalPriceAprv.add(a);	
																				}
																				if(sQuotedTotalPrice!="" && sQuotedTotalPrice!=QuotationConstants.QUOTATION_ZERO) {
																					bdTotalPriceQuoted = bdTotalPriceQuoted.add(a);
																				}
																				sSupervisionCharges = CurrencyConvertUtility.buildPriceFormat(String.valueOf(supervisionCharges));
																			%>
																			<tr>
																				<td colspan="2" class="formContentview-rating-b0x">&nbsp;</td>
																				<td colspan="4" align="left" class="formContentview-rating-b0x"><b> Supervision and Comissioning charges</b></td>
																				<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
																				<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
																				<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
																				<td class="formContentview-rating-b0x"><b> &#8377;  <%=sSupervisionCharges%> </b></td>
																				<td colspan="11" class="formContentview-rating-b0x">&nbsp;</td>
																			</tr>
																		</logic:equal>
																		
																		<% 
																			sTotalPrice = CurrencyConvertUtility.buildPriceFormat(bdTotalPriceStd.toString());
																			sReqTotalPrice = CurrencyConvertUtility.buildPriceFormat(bdTotalPriceReq.toString());
																			sAprvTotalPrice = CurrencyConvertUtility.buildPriceFormat(bdTotalPriceAprv.toString());
																			sQuotedTotalPrice = CurrencyConvertUtility.buildPriceFormat(bdTotalPriceQuoted.toString());
																			sCummulativeTotalCost = CurrencyConvertUtility.buildPriceFormat(String.valueOf(bdCummTotalCost.longValue()));
																			long lCummProfitMarginAvg = bdCummProfitMargin.longValue() / iCount;
																			sCummulativeProfitMargin = String.valueOf(lCummProfitMarginAvg);
																		%>
								                      					<tr>
																			<td colspan="5" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td align="left" class="formContentview-rating-b0x"><b><bean:message key="quotation.create.label.total"/></b></td>
							                       							<td class="formContentview-rating-b0x"> <b> <%=df1.format(dQuantity)%> </b> </td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td class="formContentview-rating-b0x"><b> &#8377;  <%=sTotalPrice%></b></td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td class="formContentview-rating-b0x"><b> &#8377;  <%=sReqTotalPrice%></b></td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td class="formContentview-rating-b0x"><b> &#8377;  <%=sAprvTotalPrice%> </b></td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td class="formContentview-rating-b0x"><b> &#8377;  <%=sQuotedTotalPrice%></b></td>
							                       							<td class="formContentview-rating-b0x"><b> &#8377;  <%=sCummulativeTotalCost%></b></td>
						                       								<td class="formContentview-rating-b0x"><b> <%=sCummulativeProfitMargin%> % </b></td>
																		</tr>
																		<tr>
																			<td colspan="2" class="formContentview-rating-b0x"><b><bean:message key="quotation.create.label.inwords"/></b></td>
																			<td colspan="19" class="formContentview-rating-b0x" align="right">
																				<b>	<%=CurrencyConvertUtility.convertToIndianCurrency(String.valueOf(bdTotalPriceStd)) %> </b>
																			</td>
																		</tr>
																	</logic:equal>
																	<!-- ------------------------------------ Commercial Offer : MH : End ------------------------------------- -->
																																	
																</logic:present>
																
																														
															</table>
														</td>
													</tr>
													
                								</table>
									
								</fieldset>
  								
  							</td>
  						</tr>
  						
  						<tr>
                			<td>&nbsp;</td>
              			</tr>
              			
              			<!----------------------- Comments and Deviations Start ------------------------------------->
              			<tr>
              				<td>
              					<fieldset style="width:100%">
									<legend class="blueheader">	<bean:message key="quotation.create.label.commentsanddeviations"/> </legend>
								 	<div>
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
						                  	<tr> <td colspan="5">&nbsp;</td> </tr>
									     	<tr style="float:left; width:100%;">				  
						                    	<td>
						                    		<textarea name="deviationComments" id="deviationComments" style="width:1135px; height:50px; BACKGROUND-COLOR: #e2e2e2" onkeyup="checkMaxLenghtText(this, 10000);" onkeydown="checkMaxLenghtText(this, 10000);" tabindex="35" readonly > <bean:write name="newEnquiryDto" property="commentsDeviations" filter="false" /> </textarea>
						                    	</td>
						                  	</tr>
							    		</table>
							    	</div>
							    </fieldset>
              				</td>
              			</tr>
              			<!----------------------- Comments and Deviations End --------------------------------------->
              			
              			<tr>
                			<td>&nbsp;</td>
              			</tr>
              			
              			<!-----------------------Notes By SM, RSM, NSM, MH Start ------------------------------------->
    					<tr>
    						<td>
    										<fieldset>
		                							<legend class="blueheader">	<bean:message key="quotation.create.label.notesection"/> </legend>
													<div>
														<table width="100%" border="0" cellspacing="0" cellpadding="0">
															<tr> <td colspan="5">&nbsp;</td> </tr>
															<tr style="float: left; width: 100%;">
																<td class="formLabelrating" style="width: 138px; margin-right: 33px;"> Note By DM </td>
																<td colspan="4">
																	<textarea name="designNote" id="designNote" style="width:1050px; height: 50px; BACKGROUND-COLOR: #e2e2e2" tabindex="35" readonly> <bean:write name="newEnquiryDto" property="designNote" filter="false" /> </textarea>
																</td>
															</tr>
															<tr style="float: left; width: 100%;">
																<td class="formLabelrating" style="width: 138px; margin-right: 33px;"> Note By QA </td>
																<td colspan="4">
																	<textarea name="qaNote" id="qaNote" style="width:1050px; height: 50px; BACKGROUND-COLOR: #e2e2e2" tabindex="35" readonly> <bean:write name="newEnquiryDto" property="qaNote" filter="false" /> </textarea>
																</td>
															</tr>
															<tr style="float: left; width: 100%;">
																<td class="formLabelrating" style="width: 138px; margin-right: 33px;"> Note By Sourcing </td>
																<td colspan="4">
																	<textarea name="scmNote" id="scmNote" style="width: 1050px; height: 50px; BACKGROUND-COLOR: #e2e2e2" tabindex="35" readonly > <bean:write name="newEnquiryDto"  property="scmNote" filter="false" /> </textarea>
																</td>
															</tr>
															<tr style="float: left; width: 100%;">
																<td class="formLabelrating" style="width: 138px; margin-right: 33px;"> Note By Mfg. Plant </td>
																<td colspan="4">
																	<textarea name="mfgNote" id="mfgNote" style="width: 1050px; height: 50px; BACKGROUND-COLOR: #e2e2e2" tabindex="35" readonly > <bean:write name="newEnquiryDto"  property="mfgNote" filter="false" /> </textarea>
																</td>
															</tr>
															
															<logic:notEqual name="newEnquiryDto" property="statusId" value="6">
															<tr style="float: left; width: 100%;">
																<td class="formLabelrating" style="width: 138px; margin-right: 33px;"><bean:message key="quotation.create.label.smnote" /></td>
																<td colspan="4">
																	<textarea name="smNote" id="smNote" style="width: 1050px; height: 50px; BACKGROUND-COLOR: #e2e2e2" tabindex="35" readonly > <bean:write name="newEnquiryDto"  property="smNote" filter="false" /> </textarea>
																</td>
															</tr>
															</logic:notEqual>
															<logic:equal name="newEnquiryDto" property="statusId" value="6">
															<tr style="float: left; width: 100%;">
																<td class="formLabelrating" style="width: 138px; margin-right: 33px;"><bean:message key="quotation.create.label.smnote" /></td>
																<td colspan="4">
																	<textarea name="smNote" id="smNote" style="width: 1050px; height: 50px;" tabindex="35" > <bean:write name="newEnquiryDto"  property="smNote" filter="false" /> </textarea>
																</td>
															</tr>
															</logic:equal>
															
															<tr style="float: left; width: 100%;">
																<td class="formLabelrating" style="width: 138px; margin-right: 33px;"><bean:message key="quotation.create.label.rsmnote" /></td>
																<td colspan="4">
																	<textarea name="rsmNote" id="rsmNote" style="width: 1050px; height: 50px; BACKGROUND-COLOR: #e2e2e2" tabindex="35" readonly > <bean:write name="newEnquiryDto"  property="rsmNote" filter="false" /> </textarea>
																</td>
															</tr>
															<tr style="float: left; width: 100%;">
																<td class="formLabelrating" style="width: 138px; margin-right: 33px;"><bean:message key="quotation.create.label.nsmnote" /></td>
																<td colspan="4">
																	<textarea name="nsmNote" id="nsmNote" style="width: 1050px; height: 50px; BACKGROUND-COLOR: #e2e2e2" tabindex="35" readonly > <bean:write name="newEnquiryDto"  property="nsmNote" filter="false" /> </textarea>
																</td>
															</tr>
															<tr style="float: left; width: 100%;">
																<td class="formLabelrating" style="width: 138px; margin-right: 33px;"><bean:message key="quotation.create.label.mhnote" /></td>
																<td colspan="4">
																	<textarea name="mhNote" id="mhNote" style="width: 1050px; height: 50px; BACKGROUND-COLOR: #e2e2e2" tabindex="35" readonly > <bean:write name="newEnquiryDto"  property="mhNote" filter="false" /> </textarea>
																</td>
															</tr>
															<tr style="float: left; width: 100%;">
																<td class="formLabelrating" style="width: 138px; margin-right: 33px;"> Note By BH </td>
																<td colspan="4">
																	<textarea name="bhNote" id="bhNote" style="width:1050px; height: 50px; BACKGROUND-COLOR: #e2e2e2" tabindex="35" readonly> <bean:write name="newEnquiryDto" property="bhNote" filter="false" /> </textarea>
																</td>
															</tr>
														</table>
													</div>
											</fieldset>
    						</td>
    					</tr>
    					<!-----------------------Notes By SM, RSM, NSM, MH End --------------------------------------->
  						
  						
  						<tr>
                			<td>&nbsp;</td>
              			</tr>
              			
              			<tr>
                			<td>
                				<fieldset>
									<legend class="blueheader">
										<bean:message key="quotation.create.label.notes" />
									</legend>

									<table  bgcolor="#FFFFFF" align="center">
										<tr>
						                	<td>&nbsp;</td>
						              	</tr>
						               	<tr>
						                	<td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_1)%></span></td>
						             	 </tr>
						              	 <tr>
						                	<td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_2)%></span></td>
						              	</tr>
						               	<tr>
						                	<td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_3)%></span></td>
						              	</tr>
						               	<tr>
						                	<td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_4)%></span></td>
						              	</tr>
						               	<tr>
						                	<td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_5)%></span></td>
						              	</tr>
						               	<tr>
						                	<td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_6)%></span></td>
						              	</tr>
						               	<tr>
						                	<td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_7)%></span></td>
						              	</tr>
						               	<tr>
						                	<td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_8)%></span></td>
						              	</tr>
						               	<tr>
						                	<td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_9)%></span></td>
						              	</tr>
						               	<tr>
						                	<td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_10)%></span></td>
						              	</tr>
						               	<tr>
						                	<td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_11)%></span></td>
						              	</tr>
						               	<tr>
						                	<td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_12)%></span></td>
						              	</tr>
						               	<tr>
						                	<td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_13)%></span></td>
						              	</tr>
						               	<tr>
						                	<td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_14)%></span></td>
										</tr>
									</table>
                				</fieldset>
                			</td>
                		</tr>
						
						<tr>
							<td>
								<fieldset>
									<legend class="blueheader">
										<bean:message key="quotation.create.label.generalnote" />
									</legend>

									<table bgcolor="#FFFFFF" align="center">
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><bean:message key="quotation.create.label.defintions" /></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_DEFINTIONS)%></span></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><bean:message key="quotation.create.label.validity" /></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_VALIDITY)%></span></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><bean:message key="quotation.create.label.scope" /></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_SCOPE)%></span></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><bean:message key="quotation.create.label.priceandbasis" /></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_PRICEANDBASIS)%></span></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><bean:message key="quotation.create.label.freightandinsurance" /></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_FREIGHT_AND_INSURANCE)%></span></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><bean:message key="quotation.create.label.packingandforwarding" /></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_PACKING_AND_FORWARDING)%></span></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><bean:message key="quotation.create.label.termsofpayments" /></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_TERMS_OF_PAYMENT)%></span></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><bean:message key="quotation.create.label.deliveries" /></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_DELIVERY)%></span></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>

										<tr>
											<td><bean:message key="quotation.create.label.drawingsanddocsapproval" /></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_DRAWINGANDDOCSAPPROVAL)%></span></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>

										<tr>
											<td><bean:message
													key="quotation.create.label.delayinmanufclearance" /></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_DELAYMANFCLEARANCE)%></span></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>


										<tr>
											<td><bean:message
													key="quotation.create.label.forcemeasures" /></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_FORCE_MAJEURE)%></span></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>



										<tr>
											<td><bean:message key="quotation.create.label.storages" /></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_STORAGE)%></span></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>



										<tr>
											<td><bean:message key="quotation.create.label.warranty" /></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_WARRANTY)%></span></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>



										<tr>
											<td><bean:message
													key="quotation.create.label.wghtsanddimension" /></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_WEIGHTS_AND_DIMENSIONS)%></span></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>


										<tr>
											<td><bean:message key="quotation.create.label.tests" /></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_TESTS)%></span></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>


										<tr>
											<td><bean:message key="quotation.create.label.standards" /></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_STANDARDS)%></span></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>



										<tr>
											<td><bean:message
													key="quotation.create.label.limitationofliability" /></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_LIMITATION_OF_LIABILITY)%></span></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>


										<tr>
											<td><bean:message
													key="quotation.create.label.consequentiallosses" /></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_CONSEQUENTIAL_LOSSES)%></span></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>


										<tr>
											<td><bean:message
													key="quotation.create.label.arbitration" /></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_ARBITRATION)%></span></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>


										<tr>
											<td><bean:message key="quotation.create.label.language" /></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_LANGUAGE)%></span></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>


										<tr>
											<td><bean:message
													key="quotation.create.label.governinglaw" /></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_GOVERNING_LAW)%></span></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>



										<tr>
											<td><bean:message
													key="quotation.create.label.variationinquantity" /></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_VARIATION_IN_QUANTITY)%></span></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>


										<tr>
											<td><bean:message
													key="quotation.create.label.transferoftitle" /></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_TRANSFER_OF_TITLE)%></span></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>



										<tr>
											<td><bean:message
													key="quotation.create.label.confidentialtreatmentsecrecy" /></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_CONFIDENTIAL_TREATMENT_AND_SECRECY)%></span></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>



										<tr>
											<td><bean:message
													key="quotation.create.label.passingofbenefitrisk" /></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_PASSING_OF_BENEFIT_AND_RISK)%></span></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>


										<tr>
											<td><bean:message
													key="quotation.create.label.compensionduetobuyersdefault" /></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_COMPENSATION_DUE_TO_BUYERS_DEFAULT)%></span></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>


										<tr>
											<td><bean:message
													key="quotation.create.label.suspensionsandtermination" /></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>



										<tr>
											<td><bean:message
													key="quotation.create.label.suspensions" /></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_SUSPENSION)%></span></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>


										<tr>
											<td><bean:message
													key="quotation.create.label.terminations" /></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_TERMINATION)%></span></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>


										<tr>
											<td><bean:message
													key="quotation.create.label.bankruptcy" /></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_BANKRUPTCY)%></span></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>




										<tr>
											<td><bean:message
													key="quotation.create.label.acceptance" /></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_ACCEPTANCE)%></span></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>



										<tr>
											<td><bean:message
													key="quotation.create.label.limitofsupply" /></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_LIMIT_OF_SUPPLY)%></span></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>



										<tr>
											<td><bean:message
													key="quotation.create.label.changeinscope" /></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_CHANGE_IN_SCOPE)%></span></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>



										<tr>
											<td><bean:message
													key="quotation.create.label.channelsforcommunications" /></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_CHANNELS_FOR_COMMUNICATION)%></span></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>



										<tr>
											<td><bean:message key="quotation.create.label.general" /></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_GENERAL)%></span></td>
										</tr>
										<tr>
											<td>&nbsp;</td>
										</tr>


										<tr>
											<td class="blue-light" align="right">

												<div class="blue-light" align="right">
													<bean:define id="statusId" name="newEnquiryDto" property="statusId" />
													<bean:define id="createdSSO" name="newEnquiryDto" property="createdBySSO" />

													<logic:equal name="userSSO" scope="request" value="<%=(String)createdSSO%>">
														<logic:equal name="newEnquiryDto" property="statusId" value="6">
															<input name="saveButton" type="button" onclick="saveReleasedQuote();" class="BUTTON" value="Save" />
														</logic:equal>
														
														<logic:present name="salesManager" scope="request">
															<logic:equal name="newEnquiryDto" property="statusId" value="6">
																<input name="viewpdf" type="button" onclick="viewNewQuotationPDF();" class="BUTTON" value="View PDF" />
															</logic:equal>
														</logic:present>
														<logic:present name="salesManager" scope="request">
															<logic:equal name="newEnquiryDto" property="statusId" value="6">
																<input name="wonButton" type="button" onclick="viewNewEnquiryIndentPage('Won');" class="BUTTON" value="Won" />
															</logic:equal>
														</logic:present>
														<logic:present name="salesManager" scope="request">
															<logic:equal name="newEnquiryDto" property="statusId" value="6">
																<input name="lostButton" type="button" onclick="javascript:viewNewEnquiryIndentPage('Lost');" class="BUTTON" value="Lost" />
															</logic:equal>
														</logic:present>
														<logic:present name="salesManager" scope="request">
															<logic:equal name="newEnquiryDto" property="statusId" value="6">
																<input name="abondentButton" type="button" onclick="javascript:viewNewEnquiryIndentPage('Abondent');" class="BUTTON" value="Abonded" />
															</logic:equal>
														</logic:present>

														<logic:equal name="newEnquiryDto" property="statusId" value="6">
															<input name="techRevision" type="button" onclick="createNewEnqTechRevision();" class="BUTTON" value="Technical Revision Required" />
															<input name="commRevision" type="button" onclick="createNewEnqCommRevision();" class="BUTTON" value="Commercial Revision Required" />
														</logic:equal>
													</logic:equal>

												</div>

											</td>
										</tr>
									</table>
								</fieldset>
							</td>
						</tr>
						
  					</logic:equal>
  				</td>
  			</tr>
		</table>
		
	</logic:present>
 </html:form>