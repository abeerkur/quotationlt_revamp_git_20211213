<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants" %>
<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>
<%@page import="java.text.DecimalFormat" %>
<%
DecimalFormat decimalFormat = new DecimalFormat("#0");
%>

<html:form  action="/viewEnquiry"  method="POST">
<html:hidden property="invoke" value="updateFactor"/>
<html:hidden property="enquiryId" />
<html:hidden property="enquiryNumber" />
<html:hidden property="createdBySSO" />
<html:hidden property="createdBy" />
<html:hidden property="pagechecking" />

<html:hidden property="operationType" value="CE" />
<input type="hidden" id="ratingMaxSize" name="ratingMaxSize"/>
<input type="hidden" id="addonMaxSize" name="addonMaxSize"/>
<input type="hidden" name='<%= org.apache.struts.taglib.html.Constants.TOKEN_KEY %>'
         value='<%= session.getAttribute(org.apache.struts.action.Action.TRANSACTION_TOKEN_KEY) %>' />


<logic:present name="enquiryDto" scope="request">
<div id="pagehide">
<table width="80%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
<tr>
  <td height="100%" valign="top">



 			<div id="estimate">
			<fieldset>
				<legend class="blueheader">
					<bean:message key="quotation.viewenquiry.estimate.label.heading"/>
				</legend>
	  <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">

				<input type="hidden" name="commonMultiplierCount" id="commonMultiplierCount">

	<tr>
<td><table width="100%" border="0" cellspacing="0" cellpadding="0">

<tbody><tr>
<td width="30%" class="label-text"><bean:message key="quotation.create.label.customer"/></td>
<td width="40%" class="formContentview-rating-b0x"><bean:write name="enquiryDto" property="customerName"/>  </td>
<td width="30%">&nbsp;&nbsp;<html:text property="customerNameMultiplier" styleId="customerNameMultiplier" styleClass="short" maxlength="4" value="" onkeypress="return validateFloatKeyPress(this,event);" onblur="return calculateCommonMultipliers()" />&nbsp;%</td>

</tr>
</tbody></table></td>
<td ></td>
<td></td>
<td><table width="100%" border="0" cellspacing="1" cellpadding="0">
<tbody><tr>
<td width="30%"  class="label-text"><bean:message key="quotation.create.label.advancepayment"/></td>
<td width="40%"  class="formContentview-rating-b0x"><bean:write name="enquiryDto" property="advancePayment"/>
<logic:notEqual name="enquiryDto" property="advancePayment" value="">
%
</logic:notEqual> </td>
<td width="30%">&nbsp;&nbsp;<html:text property="advancePaymMultiplier" styleId="advancePaymMultiplier" styleClass="short" maxlength="4" value="" onkeypress="return validateFloatKeyPress(this,event);"  onblur="return calculateCommonMultipliers()" />&nbsp;%</td>

</tr>
</tbody></table></td>
</tr>

	<tr>
<td><table width="100%" border="0" cellspacing="0" cellpadding="0">

<tbody><tr>
<td width="30%"  class="label-text"><bean:message key="quotation.create.label.customertype"/></td>
<td width="40%"  class="formContentview-rating-b0x"><bean:write name="enquiryDto" property="customerType"/></td>
<td width="30%">&nbsp;&nbsp;<html:text property="customerTypeMultiplier" styleId="customerTypeMultiplier" styleClass="short" maxlength="4" value="" onkeypress="return validateFloatKeyPress(this,event);" onblur="return calculateCommonMultipliers()" />&nbsp;%</td>

</tr>
</tbody></table></td>
<td ></td>
<td></td>
<td><table width="100%" border="0" cellspacing="1" cellpadding="0">
<tbody><tr>
<td width="30%" class="label-text"><bean:message key="quotation.create.label.paymentterms"/></td>
<td width="40%" class="formContentview-rating-b0x"><bean:write name="enquiryDto" property="paymentTerms"/>
<logic:notEqual name="enquiryDto" property="paymentTerms" value="">
% <bean:message key="quotation.create.label.beforedispatch"/>
</logic:notEqual>
</td>
<td width="30%">&nbsp;&nbsp;<html:text property="payTermMultiplier" styleId="payTermMultiplier" styleClass="short" maxlength="4" value=""  onkeypress="return validateFloatKeyPress(this,event);" onblur="return calculateCommonMultipliers()" />&nbsp;%</td>

</tr>
</tbody></table></td>
</tr>

	<tr>
<td><table width="100%" border="0" cellspacing="0" cellpadding="0">

<tbody><tr>
<td width="30%" class="label-text"><bean:message key="quotation.create.label.enduser"/></td>
<td width="40%" class="formContentview-rating-b0x"><bean:write name="enquiryDto" property="endUser"/>  </td>
<td width="30%">&nbsp;&nbsp;<html:text property="endUserMultiplier" styleId="endUserMultiplier" styleClass="short" maxlength="4" value="" onkeypress="return validateFloatKeyPress(this,event);"  onblur="return calculateCommonMultipliers()"/>&nbsp;%</td>


</tr>
</tbody></table></td>
<td ></td>
<td></td>
<td><table width="100%" border="0" cellspacing="1" cellpadding="0">
<tbody><tr>
 <td width="30%"  class="label-text"><bean:message key="quotation.create.label.deliveriesdate"/></td>
<td width="40%"  class="formContentview-rating-b0x"><bean:write name="enquiryDto" property="expectedDeliveryMonth"/></td>
<td width="30%">&nbsp;&nbsp;<html:text property="expDeliveryMultiplier" styleId="expDeliveryMultiplier" styleClass="short" maxlength="4" value=""  onkeypress="return validateFloatKeyPress(this,event);" onblur="return calculateCommonMultipliers()" />&nbsp;%</td>
 
</tr>
</tbody></table></td>
</tr>

	<tr>
<td><table width="100%" border="0" cellspacing="0" cellpadding="0">

<tbody><tr>

<td width="30%"  class="label-text"><bean:message key="quotation.create.label.warrantydispatchdates"/></td>
<td width="40%"  class="formContentview-rating-b0x"><bean:write name="enquiryDto" property="warrantyDispatchDate"/>&nbsp;<bean:message key="quotation.create.label.months"/></td>
<td width="30%">&nbsp;&nbsp;<html:text property="warrantyDispatchMultiplier" styleId="warrantyDispatchMultiplier" styleClass="short" maxlength="4" value=""  onkeypress="return validateFloatKeyPress(this,event);" onblur="return calculateCommonMultipliers()" />&nbsp;%</td>

</tr>
</tbody></table></td>
<td ></td>
<td></td>
<td><table width="100%" border="0" cellspacing="1" cellpadding="0">
<tbody><tr>
<td width="30%" class="label-text"><bean:message key="quotation.create.label.finance"/></td>
<td width="40%" class="formContentview-rating-b0x">&nbsp;</td>
<td width="30%" >&nbsp;&nbsp;<html:text property="financeMultiplier" styleId="financeMultiplier" styleClass="short" maxlength="4" value=""  onkeypress="return validateFloatKeyPress(this,event);"  onblur="return calculateCommonMultipliers()"/>&nbsp;%</td>
</tr>
</tbody></table></td>
</tr>

	<tr>
<td><table width="100%" border="0" cellspacing="0" cellpadding="0">

<tbody><tr>
<td width="30%" class="label-text"><bean:message key="quotation.create.label.warrantycommissioningdates"/></td>
<td width="40%" class="formContentview-rating-b0x"><bean:write name="enquiryDto" property="warrantyCommissioningDate"/>&nbsp;<bean:message key="quotation.create.label.months"/></td>
<td width="30%">&nbsp;&nbsp;<html:text property="warrantycommissioningMultiplier" styleId="warrantycommissioningMultiplier" styleClass="short" maxlength="4" value="" onkeypress="return validateFloatKeyPress(this,event);" onblur="return calculateCommonMultipliers()"  />&nbsp;%</td>

</tr>
</tbody></table></td>
<td ></td>
<td></td>
<td>
<table width="100%" border="0" cellspacing="1" cellpadding="0">
<tbody><tr>
<td width="30%">&nbsp;</td>
<td width="40%">&nbsp;</td>
<td width="30%">&nbsp;</td>
</tr>
</tbody>
</table>
</td>
</tr>




				
				
					<%String finalStr=""; %>		
						<logic:notEmpty name="enquiryDto" property="ratingObjects">
         <logic:iterate name="enquiryDto" property="ratingObjects"
								               id="ratingObject" indexId="idx" 
								               type="in.com.rbc.quotation.enquiry.dto.RatingDto">
<tr><td colspan="3" align="left"><legend class="blueheader"><bean:message key="quotation.viewenquiry.technical.label.heading"/> <%= idx.intValue()+1 %> </legend></td></tr>

<%

  
  
  if(finalStr=="")
  {
	  finalStr=String.valueOf(ratingObject.getRatingId());
  }
  else
  {
	  finalStr=finalStr+","+String.valueOf(ratingObject.getRatingId());
  }

  
%>

	<tr>
<td><table width="100%" border="0" cellspacing="0" cellpadding="0">

<tbody><tr>
<td width="30%" class="label-text"><bean:message key="quotation.create.label.quantityrequired"/></td>
<td width="40%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="quantity"/></td>
<td width="30%" >&nbsp;&nbsp;<input type="text" name="quantityMultiplier_${ratingObject.ratingId}" id="quantityMultiplier_${ratingObject.ratingId}" class="short" maxlength="4" value="" onkeypress="return validateFloatKeyPress(this,event);" onblur="return calculateMultipliers('${ratingObject.ratingId}')"  />&nbsp;%</td>

</tr>
</tbody></table></td>
<td ></td>
<td></td>
<td><table width="100%" border="0" cellspacing="1" cellpadding="0">
<tbody><tr>
<td width="30%" class="label-text"><bean:message key="quotation.viewenquiry.estimate.label.factor"/></td>
<td width="40%" class="formContentview-rating-b0x">&nbsp;</td>
<td width="30%" >&nbsp;&nbsp;<input type="text" name="factorMultiplier_${ratingObject.ratingId}" id="factorMultiplier_${ratingObject.ratingId}" class="short" maxlength="4" value="" onkeypress="return validateFloatKeyPress(this,event);" onblur="return calculateMultipliers('${ratingObject.ratingId}')" />&nbsp;%</td>
</tr>
</tbody></table></td>
</tr>


	<tr>
<td><table width="100%" border="0" cellspacing="0" cellpadding="0">

<tbody><tr>
<td width="30%"  class="label-text"><bean:message key="quotation.create.label.kw"/></td>
<td width="40%"  class="formContentview-rating-b0x"><bean:write name="ratingObject" property="kw"/></td>
<td width="30%">&nbsp;&nbsp;<input type="text" name="ratedOutputPNMulitplier_${ratingObject.ratingId}"  id="ratedOutputPNMulitplier_${ratingObject.ratingId}" class="short" maxlength="4" value=""  onkeypress="return validateFloatKeyPress(this,event);" onblur="return  calculateMultipliers('${ratingObject.ratingId}')" />&nbsp;%</td>

</tr>
</tbody></table></td>
<td ></td>
<td></td>
<td><table width="100%" border="0" cellspacing="1" cellpadding="0">
<tbody><tr>
 <td width="30%"  class="label-text"><bean:message key="quotation.create.label.netmc"/></td>
<td width="40%"  class="formContentview-rating-b0x">
<%String MCUnit="",AreaClassification="",frameSize=""; %>
   				<logic:notEmpty name="enquiryDto" property="ratingObjects">
		         <logic:iterate name="enquiryDto" property="ratingObjects"
								               id="ratingObject1" indexId="idsx1" 
								               type="in.com.rbc.quotation.enquiry.dto.RatingDto">	
  
  				 <bean:define id="technicalObject1" name="ratingObject1" property="technicalDto"/>
  				 <%if(idx.intValue()==idsx1.intValue()){ %>
  				 <bean:define id="tech1" name="technicalObject1" property="formattedCommercialMCUnit"/>
  				 <bean:define id="tech2" name="technicalObject1" property="areaClassification"/>
  				 <bean:define id="tech3" name="technicalObject1" property="frameSize"/>
  				 
  				 <%
  				 
  				 MCUnit=String.valueOf(tech1);
  				 
  				AreaClassification=String.valueOf(tech2);
  				frameSize=String.valueOf(tech3);
  				 } %>


  				 </logic:iterate>
  				 </logic:notEmpty>	



<%=MCUnit %>
<%
if(MCUnit!="" && MCUnit!=null && MCUnit!="0")
{
	MCUnit=MCUnit.trim().replaceAll("INR", "");
	MCUnit=MCUnit.replaceAll("\\s", "");
	MCUnit=MCUnit.replaceAll(",", "");
	
	MCUnit=decimalFormat.format(Double.parseDouble(MCUnit));
	
}
else
{
	MCUnit="0";	
}
%>

</td>
<td width="30%">&nbsp;&nbsp;<input type="text" name="netmcMultiplier_${ratingObject.ratingId}" id="netmcMultiplier_${ratingObject.ratingId}" class="short" maxlength="9" value="<%= MCUnit%>" readonly="readonly"  />&nbsp;INR</td>


</tr>
</tbody></table></td>
</tr>

	<tr>
<td><table width="100%" border="0" cellspacing="0" cellpadding="0">

<tbody><tr>
 <td width="30%"  class="label-text"><bean:message key="quotation.offer.label.areaclassification"/></td>
<td width="40%"  class="formContentview-rating-b0x">




<%=AreaClassification %>
</td>
<td width="30%">&nbsp;&nbsp;<input type="text" name="areaCMultiplier_${ratingObject.ratingId}" id="areaCMultiplier_${ratingObject.ratingId}" class="short" maxlength="4" value=""  onkeypress="return validateFloatKeyPress(this,event);" onblur="return calculateMultipliers('${ratingObject.ratingId}')" />&nbsp;%</td>
 
</tr>
</tbody></table></td>
<td ></td>
<td></td>
<td><table width="100%" border="0" cellspacing="1" cellpadding="0">
<tbody><tr>
 <td width="30%" class="label-text"><bean:message key="quotation.create.label.markup"/></td>
<td width="40%" class="formContentview-rating-b0x">&nbsp;</td>
<td width="30%" >&nbsp;&nbsp;<input type="text" name="markupMultiplier_${ratingObject.ratingId}" id="markupMultiplier_${ratingObject.ratingId}" class="short" maxlength="4" readonly="readonly"  />&nbsp;%</td>
 
 </tr>
</tbody></table></td>
</tr>



	<tr>
<td><table width="100%" border="0" cellspacing="0" cellpadding="0">

<tbody><tr>
 <td width="30%" class="label-text"><bean:message key="quotation.offerdata.technical.label.framesize"/></td>
 
 
<td width="40%" class="formContentview-rating-b0x">
 <%=frameSize %>
</td>
<td width="30%" >&nbsp;&nbsp;<input type="text" name="frameMultiplier_${ratingObject.ratingId}" id="frameMultiplier_${ratingObject.ratingId}" class="short" maxlength="4" value="" onkeypress="return validateFloatKeyPress(this,event);"  onblur="return calculateMultipliers('${ratingObject.ratingId}')" />&nbsp;%</td>
</tr>
</tbody></table></td>
<td ></td>
<td></td>
<td><table width="100%" border="0" cellspacing="1" cellpadding="0">
<tbody><tr>
 <td width="30%"  class="label-text"><bean:message key="quotation.create.label.transfercost"/></td>
<td width="40%"  class="formContentview-rating-b0x">&nbsp;</td>
<td width="30%">&nbsp;&nbsp;<input type="text"  name="transferCMultiplier_${ratingObject.ratingId}" id="transferCMultiplier_${ratingObject.ratingId}" class="short" maxlength="10" value=""  readonly="readonly" />&nbsp;INR</td>
 
 </tr>
</tbody></table></td>
</tr>


<tr>
<td><table width="100%" border="0" cellspacing="0" cellpadding="0">

<tbody><tr>
 <td width="30%" class="label-text"><bean:message key="quotation.create.label.scsr"/></td>
<td width="40%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="scsrName"/></td>
<td width="30%" >&nbsp;&nbsp;<input type="text" name="typeMultiplier_${ratingObject.ratingId}" id="typeMultiplier_${ratingObject.ratingId}" class="short" onkeypress="return validateFloatKeyPress(this,event);" maxlength="4" value="" onblur="return calculateMultipliers('${ratingObject.ratingId}')" />&nbsp;%</td>
</tr>
</tbody></table></td>
<td ></td>
<td></td>
<td><table width="100%" border="0" cellspacing="1" cellpadding="0">
<tbody><tr>
</tr>
</tbody></table></td>
</tr>

<tr>
<td><table width="100%" border="0" cellspacing="0" cellpadding="0">

<tbody><tr>
<td width="30%"  class="label-text"><bean:message key="quotation.create.label.application"/></td>
<td width="40%"  class="formContentview-rating-b0x"><bean:write name="ratingObject" property="applicationName"/></td>
<td width="30%">&nbsp;&nbsp;<input type="text" name="applicationMultiplier_${ratingObject.ratingId}" id="applicationMultiplier_${ratingObject.ratingId}" class="short" maxlength="4" value="" onkeypress="return validateFloatKeyPress(this,event);" onblur="return calculateMultipliers('${ratingObject.ratingId}')"  />&nbsp;%</td>

</tr>
</tbody></table></td>
<td ></td>
<td></td>
<td><table width="100%" border="0" cellspacing="1" cellpadding="0">
<tbody><tr>
</tr>
</tbody></table></td>
</tr>






				
				
				
								               
								               </logic:iterate>
								               </logic:notEmpty>
				
				<input type="hidden" name="finalRatingList" id="finalRatingList" value="<%=finalStr %>">
				</table>
				<br/>
				 <div align="left"><bean:message key="quotation.create.label.warrantydispatchcommissioning" /></div>
				
				
 	</fieldset>
 	
 				  
			<fieldset style="width:100%">
						       <legend class="blueheader">	
			          	<bean:message key="quotation.create.label.notesection"/>
			          </legend>
			
			     <div >
				  <table  border="0" cellspacing="0" cellpadding="0">
				  <tr><td colspan="4">&nbsp;</td></tr>
			      <tr style="float:left; width:100%;">				  
				    <td class="formLabelrating" style="width:138px; margin-right: 33px;">
				    	<bean:message key="quotation.create.label.smnote"/>
				    </td>
                    <td colspan="3" class="formLabelrating">

                    <logic:notEqual name="enquiryForm" property="smnote"  value="">
                                        <bean:write name="enquiryForm" property="smnote"/>
                                     &nbsp; By &nbsp;  <bean:write name="enquiryForm" property="sm_name"/>
                    
                    </logic:notEqual>
                    
                  </tr>
                  <tr style="float:left; width:100%;">				  
				    <td class="formLabelrating" style="width:138px; margin-right: 33px;">
				    	<bean:message key="quotation.create.label.dmnote"/>
				    </td>
                    <td colspan="3" class="formLabelrating">

                    <logic:notEqual name="enquiryForm" property="dmnote"  value="">
                                        <bean:write name="enquiryForm" property="dmnote"/>
                                     &nbsp; By &nbsp;  <bean:write name="enquiryForm" property="dm_name"/>
                    
                    </logic:notEqual>
                    
                  </tr>
                  
                  <tr><td colspan="3">&nbsp;</td></tr>
			       <tr style="float:left; width:100%;">				  
				    <td class="formLabelrating" style="width:138px; margin-right: 33px;">
				    	<bean:message key="quotation.create.label.cenote"/>
				    </td>
                    <td colspan="4">
                    	<html:textarea  property="cenote" styleId = "cenote"  style="width:600px; height:50px" onchange = "javascript:checkMaxLength(this.id, 4000);" tabindex="35"/>   </td>
                  </tr>
                  
		    </table>
		    </div>
		    </fieldset>
			  
			  
 	
 	</div>
			<div class="blue-light" align="right">      
			   <input type="button" class="BUTTON" onClick="revision()"  value="Return to Design Office">                       
               <input type="button" class="BUTTON" onclick="javascript:submitEstimate();" value="Submit" />
            </div>
		
			<!-----------------------input buttons end----------------------------> 
			<div id="return" class="showcontent">
			<fieldset>
				<legend class="blueheader">
					<bean:message key="quotation.viewenquiry.return.label.heading"/>
				</legend>
				<table  align="center" >
                  <tr>
                    <td height="23" class="formLabel">
                    	<bean:message key="quotation.viewenquiry.return.label.returnto"/> 
                    </td>
                    <td>
                   	 <bean:define name="enquiryForm" property="teamMemberList"
	                	             id="teamMemberList" type="java.util.Collection" /> 
                   	 <html:select property="returnTo">                    	                  
                        <html:options name="enquiryForm" collection="teamMemberList"
			 				              property="key" labelProperty="value" />
                     </html:select>
                    </td>
                  </tr>
                  <tr>
                    <td class="formLabel">
                    	<bean:message key="quotation.viewenquiry.return.label.comments"/>
					</td>
                    <td>
                    	<html:textarea property="returnToRemarks" styleId="returnToRemarks"/>
                    </td>
                  </tr>
                </table>
			</fieldset>
			<div class="blue-light" align="right">
			   <input name="cancel" type="button" class="BUTTON" onClick="estimate()" value="Cancel">	
               <input name="new" type="button" class="BUTTON" onClick="javascript:returnEnquiry();" value="Return">                           
            </div>
			</div>
		
  </td>
</tr>
</table>
</div>

<br><br>
</logic:present>
</html:form>


