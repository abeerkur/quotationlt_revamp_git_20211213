
<%@ include file ="../../common/library.jsp" %>
<%@ include file="../../common/nocache.jsp" %>
<logic:notEqual name="sizeOfList" value="0">

 <logic:present name="resultsList" scope="request">
	
		<table width="100%" cellpadding="2"cellspacing="0" id="table0" border="1">
        <tr>
        <td>
        <bean:message key="quotation.search.sno" />
        </td>
        <td>
        <bean:message key="quotation.search.year" />
        </td>
        <td>
        <bean:message key="quotation.search.month" />
        </td>
        <td>
        <bean:message key="quotation.search.region" />
        </td>
        <td>
        <bean:message key="quotation.search.salesmanager" />
        </td>
        <td>
        <bean:message key="quotation.search.enquirytype" />
        </td>
        <td>
        <bean:message key="quotation.search.customertype" />
        </td>
        <td>
        <bean:message key="quotation.create.label.projectname" />
        </td>
        <td>
        <bean:message key="quotation.search.rfq" />
        </td>
        <td>
        <bean:message key="quotation.create.label.enquiryreferencenumber" />
        </td>
        <td>
        <bean:message key="quotation.create.label.enduserindustry" />
        </td>
        <td>
        <bean:message key="quotation.create.label.enduser" />
        </td>
        <td>
        <bean:message key="quotation.create.label.orderclosingmonth" />
        </td>
        <td>
        <bean:message key="quotation.create.label.targeted" />
        </td>
        <td>
        <bean:message key="quotation.create.label.winningchance" />
        </td>
        <td>
        <bean:message key="quotation.create.label.deliveriestype" />
        </td>
        <td>
        <bean:message key="quotation.create.label.expecteddeliverymonth" />
        </td>
        <td><bean:message key="quotation.search.customer" /></td>
        <td><bean:message key="quotation.search.location" /></td>
        <td><bean:message key="quotation.create.label.createddate" /></td>
        <td><bean:message key="quotation.search.createdby" /></td>
        <td><bean:message key="quotation.search.designedby" /></td>
        <td><bean:message key="quotation.label.admin.update.de" /></td>
        <td><bean:message key="quotation.home.colheading.assignto" /></td>
        <td><bean:message key="quotation.create.label.ld" /></td> 
        <td><bean:message key="quotation.create.label.approval" /></td>          
        <td><bean:message key="quotation.search.status"/></td>
        <td><bean:message key="quotation.search.frame" /></td>
        <td><bean:message key="quotation.search.pole" /></td>
        <td><bean:message key="quotation.search.kw" /></td>
        <td><bean:message key="quotation.search.volts" /></td>
        <td><bean:message key="quotation.search.mounting" /></td>
        <td><bean:message key="quotation.create.label.scsr" /></td>
        <td><bean:message key="quotation.create.label.enclosure" /></td>
        <td><bean:message key="quotation.create.label.application" /></td>
        <td><bean:message key="quotation.create.label.specialfeatures" /></td>
        <td><bean:message key="quotation.create.label.smnote" /></td>
        <td>
          		<bean:message key="quotation.search.quotatedprice" />
          </td>  
              <td>
          		<bean:message key="quotation.search.orderprice" />
          </td> 
           <td>
          		<bean:message key="quotation.create.label.winreason" />
          </td>  
          <td>
          		<bean:message key="quotation.create.label.lossreason1" />
          </td> 
                    <td>
          		<bean:message key="quotation.create.label.lossreason2" />
          </td> 
                     <td>
          		<bean:message key="quotation.create.label.competitor" />
          </td> 
          
           <td>
          		<bean:message key="quotation.viewenquiry.return.label.comments" />
          </td> 
                     <td>
          		<bean:message key="quotation.create.label.wfd" />
          </td> 
                     <td>
          		<bean:message key="quotation.create.label.wfc" />
          </td> 
                     <td>
          		<bean:message key="quotation.create.label.indentno" />
          </td> 
        
        
        
        
        
        </tr>
      <logic:iterate name="resultsList" scope="request" id="searchEnquiry"
				   indexId="idx" type="in.com.rbc.quotation.enquiry.dto.SearchEnquiryDto">
	
	
		<tr  >
		<td><%=idx.intValue()+1 %>&nbsp;</td>
		   <td><bean:write name="searchEnquiry" property="year"/>&nbsp;</td>
	   <td><bean:write name="searchEnquiry" property="month"/>&nbsp;</td>
	   <td><bean:write name="searchEnquiry" property="regionName"/>&nbsp;</td>
	   <td><bean:write name="searchEnquiry" property="salesManagerName"/>&nbsp;</td>
	   <td><bean:write name="searchEnquiry" property="enquiryType"/>&nbsp;</td>
	   <td><bean:write name="searchEnquiry" property="customerType"/>&nbsp;</td>
	   <td><bean:write name="searchEnquiry" property="projectName"/>&nbsp;</td>
	   <!-- Newly Added Data End -->
		<td><bean:write name="searchEnquiry" property="enquiryNumber"/>&nbsp;</td>
       <td><bean:write name="searchEnquiry" property="enquiryReferenceNumber"/>&nbsp;</td>
       <td><bean:write name="searchEnquiry" property="endUserIndustry"/>&nbsp;</td>
        <td><bean:write name="searchEnquiry" property="endUserName"/>&nbsp;</td>
       <td><bean:write name="searchEnquiry" property="orderClosedDate"/>&nbsp;</td>
       <td><bean:write name="searchEnquiry" property="targeted"/>&nbsp;</td>
       <td><bean:write name="searchEnquiry" property="winChance"/>&nbsp;</td>
       <td><bean:write name="searchEnquiry" property="deliveryType"/>&nbsp;</td>
       <td><bean:write name="searchEnquiry" property="exDeliveryDate"/>&nbsp;</td>
       <!-- Delivery Type -->
        <td><bean:write name="searchEnquiry" property="customerName"/>&nbsp;</td>
		<td><bean:write name="searchEnquiry" property="location"/>&nbsp;</td>
		<td><bean:write name="searchEnquiry" property="createdDate"/>&nbsp;</td>
        <td><bean:write name="searchEnquiry" property="createdBy"/>&nbsp;</td>
        <td><bean:write name="searchEnquiry" property="designedBy"/>&nbsp;</td>
        <td><bean:write name="searchEnquiry" property="designEngineerName"/>&nbsp;</td>
        <td><bean:write name="searchEnquiry" property="assignTo"/>&nbsp;</td>
        <td><bean:write name="searchEnquiry" property="ld"/>&nbsp;</td>
        <td><bean:write name="searchEnquiry" property="approval"/>&nbsp;</td>
		<td><bean:write name="searchEnquiry" property="statusName"/>&nbsp;</td>
		<td><bean:write name="searchEnquiry" property="frameName"/>&nbsp;</td>
		<td><bean:write name="searchEnquiry" property="poleName"/>&nbsp;</td>
        <td><bean:write name="searchEnquiry" property="kw"/>&nbsp;</td>
		<td><bean:write name="searchEnquiry" property="volts"/>&nbsp;</td>
        <td><bean:write name="searchEnquiry" property="mounting"/>&nbsp;</td>
        <td><bean:write name="searchEnquiry" property="typeDesc"/>&nbsp;</td>
        <td><bean:write name="searchEnquiry" property="enclosureDesc"/>&nbsp;</td>
        <td><bean:write name="searchEnquiry" property="applicationDesc"/>&nbsp;</td>
        <td><bean:write name="searchEnquiry" property="splFeatures"/>&nbsp;</td>
       	<td><bean:write name="searchEnquiry" property="smnote"/>&nbsp;</td>
		<!-- Extra Data Added By Egr on 22-Mar-2019 -->
		<td><bean:write name="searchEnquiry" property="totalMotorPrice"/>&nbsp;</td>
		<td><bean:write name="searchEnquiry" property="totalOrderPrice"/>&nbsp;</td>
		<td><bean:write name="searchEnquiry" property="winReason"/>&nbsp;</td>
		<td><bean:write name="searchEnquiry" property="lossReason1"/>&nbsp;</td>
		<td><bean:write name="searchEnquiry" property="lossReason2"/>&nbsp;</td>
		<td><bean:write name="searchEnquiry" property="competitor"/>&nbsp;</td>
		<td><bean:write name="searchEnquiry" property="lossComment"/>&nbsp;</td>
		<td><bean:write name="searchEnquiry" property="warrantyFromDispatch"/>&nbsp;</td>
		<td><bean:write name="searchEnquiry" property="warrantyFromComm"/>&nbsp;</td>
	    <td><bean:write name="searchEnquiry" property="savingIndent"/>&nbsp;</td>
	
	
     </tr>
	</logic:iterate>	
</table>
</logic:present>
</logic:notEqual>

