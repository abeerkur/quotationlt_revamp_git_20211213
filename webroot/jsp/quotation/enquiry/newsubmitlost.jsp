<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>
<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants" %>
<%@ page import="in.com.rbc.quotation.common.utility.CommonUtility" %>
<%@ page import="in.com.rbc.quotation.common.utility.PropertyUtility" %>
<%@ page import="in.com.rbc.quotation.common.utility.CurrencyConvertUtility" %>

<%@ page import="java.text.DecimalFormat,java.math.BigDecimal,java.net.URLDecoder" %>

<script language="javascript" src="html/js/quotation_lt.js"></script>

<html:form action="newPlaceAnEnquiry.do" method="POST">

	<html:hidden property="invoke" value="processLostEnquiry"/>
	<html:hidden property="operationType"/>
	<html:hidden property="enquiryId" styleId="enquiryId"/>
	<html:hidden property="enquiryNumber" styleId="enquiryNumber"/>
	<html:hidden property="statusId" styleId="statusId"/>
	<html:hidden property="ratingsValidationMessage"/>
	<html:hidden property="totalRatingIndex" styleId="totalRatingIndex"/>
	<html:hidden property="dispatch" />
	
	<input type="hidden" name='<%= org.apache.struts.taglib.html.Constants.TOKEN_KEY %>' value='<%= session.getAttribute(org.apache.struts.action.Action.TRANSACTION_TOKEN_KEY) %>' />
	
	<table width="80%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
		<tr>
			<td>
 				<div class="sectiontitle"><bean:message key="quotation.viewenquiry.label.heading"/> </div>	
			</td>
		</tr>
		
		<tr>
			<td height="100%" valign="top">
				<!---------------------------------		Top Title and Status & Note block : START  -------------------------------------------------------->
				<table width="100%" border="0" cellspacing="1" cellpadding="0">
					<tr>
						<td rowspan="2">
							<table width="98%" border="0" cellspacing="2" cellpadding="0" class="DotsTable">
								<tr>
									<td rowspan="2" style="width: 70px"><img src="html/images/icon_quote.gif" width="52" height="50"></td>
									<td class="other">
										<bean:message key="quotation.viewenquiry.label.subheading1" /> <bean:write name="newenquiryForm" property="enquiryNumber" /><br> 
										<bean:message key="quotation.viewenquiry.label.subheading2" />
									</td>
								</tr>
								<tr>
									<td class="other">
										<table width="100%">
											<tr>
												<td height="16" class="section-head" align="center">
													<bean:message key="quotation.viewenquiry.label.status" /> <bean:write name="newenquiryForm" property="statusName" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
						<logic:lessThan name="newenquiryForm" property="statusId" value="6">
						<td align="right" class="other">
							<table>
								<tr>
									<td> <bean:message key="quotation.attachments.message" /> </td>
									<td>
										<a href="javascript:manageAttachments('<%=CommonUtility.getIntValue(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_ATTACHMENT_APPLICATIONID))%>','<bean:write name="newenquiryForm" property="enquiryId"/>', '<bean:write name="newenquiryForm" property="createdBySSO"/>');">
											<img src="html/images/ico_attach.gif" alt="Manage Attachments" border="0" align="right"> 
										</a>
									</td>
								</tr>
							</table>
						</td>
						</logic:lessThan>
					</tr>
					<tr>
						<td style="width: 45%">
						<logic:present name="attachmentList" scope="request">
							<logic:notEmpty name="attachmentList">
								<fieldset>
									<table width="100%" class="other">
										<logic:iterate name="attachmentList" id="attachmentData" indexId="idx" type="in.com.rbc.quotation.common.vo.KeyValueVo">
											<tr>
												<td>
													<a href="javascript:download('<bean:write name="attachmentData" property="key"/>', '<bean:write name="attachmentData" property="value"/>', '<bean:write name="attachmentData" property="value2"/>');">
														<bean:write name="attachmentData" property="value" />.<bean:write name="attachmentData" property="value2" />
													</a>
												</td>
											</tr>
										</logic:iterate>
									</table>
								</fieldset>
							</logic:notEmpty>
						</logic:present>
						</td>
					</tr>
					<logic:present name="attachmentMessage" scope="request">
					<tr>
						<td><bean:write name="attachmentMessage" /></td>
					</tr>
					</logic:present>
				</table>
				<!---------------------------------		Top Title and Status & Note block : END  ---------------------------------------------------------->
				
				<br> <br> <br>
				
				<!---------------------------------		Request Information and Lost Details Block : START  ----------------------------------------------->
				<div style="width: 100%; float: left;">
					<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
						<tr>
							<td>
								<!-- -- Request Information Block -- -->
								<fieldset>
									<legend class="blueheader"> <bean:message key="quotation.viewenquiry.label.request.heading" /> </legend>
									<table width="100%" border="0" align="center" cellpadding="0" cellspacing="3">
										<tr>
											<td><label class="blue-light"style="font-weight: bold; width: 100%;"> Basic Information </label></td>
										</tr>
										<tr>
											<th class="label-text" style="width:240px; line-height:20px;"><bean:message key="quotation.create.label.customerenqreference"/></th>
											<td class="formContentview-rating-b0x" style="width:200px"><bean:write name="newenquiryForm" property="customerEnqReference" /></td>
										</tr>
										<tr>
											<th class="label-text" style="width:240px; line-height:20px;"><bean:message key="quotation.create.label.customer" /></th>
											<td class="formContentview-rating-b0x" style="width:200px"><bean:write name="newenquiryForm" property="customerName" /></td>
										</tr>
										<tr>
											<th class="label-text" style="width:240px; line-height:20px;"><bean:message key="quotation.label.admin.customer.location" /></th>
											<td class="formContentview-rating-b0x" style="width:200px"><bean:write name="newenquiryForm" property="location" /></td>
										</tr>
										<tr>
											<th class="label-text" style="width:240px; line-height:20px;"><bean:message key="quotation.label.concerned.person" /></th>
											<td class="formContentview-rating-b0x" style="width:200px"><bean:write name="newenquiryForm" property="concernedPerson" /></td>
										</tr>
										<tr>
											<th class="label-text" style="width:240px; line-height:20px;">Project Name</th>
											<td class="formContentview-rating-b0x" style="width:200px"><bean:write name="newenquiryForm" property="projectName" /></td>
										</tr>
										<tr>
											<th class="label-text" style="width:240px; line-height:20px;">Consultant</th>
											<td class="formContentview-rating-b0x" style="width:200px"><bean:write name="newenquiryForm" property="consultantName" /></td>
										</tr>
										<tr>
											<th class="label-text" style="width:240px; line-height:20px;">End user</th>
											<td class="formContentview-rating-b0x" style="width:200px"><bean:write name="newenquiryForm" property="endUser" /></td>
										</tr>
										<tr>
											<th class="label-text" style="width:240px; line-height:20px;">End User Industry</th>
											<td class="formContentview-rating-b0x" style="width:200px"><bean:write name="newenquiryForm" property="endUserIndustry" /></td>
										</tr>
										<tr>
											<th class="label-text" style="width:240px; line-height:20px;">Location of Installation</th>
											<td class="formContentview-rating-b0x" style="width:200px"><bean:write name="newenquiryForm" property="locationOfInstallation" /></td>
										</tr>
									</table>
								</fieldset>
							</td>
							
							<!-- -- Lost Details Block -- -->
							<td valign="top">
								<fieldset>
									<legend class="blueheader"> Lost Information </legend>
									<table width="100%" border="0" align="center" cellpadding="0" cellspacing="3">
										<tr>
											<th class="label-text" style="width:320px; line-height:20px;">Lost To</th>
											<td class="formContentview-rating-b0x" style="width:200px">
												<bean:define name="newenquiryForm" property="ltLostCompetitorList" id="ltLostCompetitorList" type="java.util.Collection" />
												<html:select style="width:200px;" name="newenquiryForm" property="lostCompetitor" styleId="lostCompetitor" >
						                    		<option value="0"><bean:message key="quotation.option.select"/></option>
						                       	 	<html:options name="newenquiryForm" collection="ltLostCompetitorList" property="key" labelProperty="value" />
						                        </html:select> 
											</td>
										</tr>
										<tr>
											<th class="label-text" style="width:320px; line-height:20px;">Lost Reason 1</th>
											<td class="formContentview-rating-b0x" style="width:200px">
												<bean:define name="newenquiryForm" property="ltLostReasonList" id="ltLostReasonList" type="java.util.Collection" />
												<html:select style="width:200px;" name="newenquiryForm" property="lostReason1" styleId="lostReason1" >
						                    		<option value="0"><bean:message key="quotation.option.select"/></option>
						                       	 	<html:options name="newenquiryForm" collection="ltLostReasonList" property="key" labelProperty="value" />
						                        </html:select> 
											</td>
										</tr>
										<tr>
											<th class="label-text" style="width:320px; line-height:20px;">Lost Reason 2</th>
											<td class="formContentview-rating-b0x" style="width:200px">
												<bean:define name="newenquiryForm" property="ltLostReasonList" id="ltLostReasonList" type="java.util.Collection" />
												<html:select style="width:200px;" name="newenquiryForm" property="lostReason2" styleId="lostReason2" >
						                    		<option value="0"><bean:message key="quotation.option.select"/></option>
						                       	 	<html:options name="newenquiryForm" collection="ltLostReasonList" property="key" labelProperty="value" />
						                        </html:select> 
											</td>
										</tr>
										<tr>
											<th class="label-text" style="width:320px; line-height:20px;">Remarks</th>
											<td class="formContentview-rating-b0x" style="width:200px">
												<html:textarea property="lostRemarks" styleId="lostRemarks" style="width:200px;" onkeyup="checkMaxLenghtText(this, 4000);" onkeydown="checkMaxLenghtText(this, 4000);" tabindex="35"/>
											</td> 
										</tr>
										<tr>
											<th class="label-text" style="width:320px; line-height:20px;">Has it been discussed with Manager before loosing the Job?</th>
											<td class="formContentview-rating-b0x" style="width:200px">
												<bean:define name="newenquiryForm" property="alTargetedList" id="alTargetedList" type="java.util.Collection" />
												<html:select style="width:200px;" property="lostManagerApproval" styleId="lostManagerApproval">
													<option value="0" selected>Select</option>
						                    		<html:options name="newenquiryForm" collection="alTargetedList" property="key" labelProperty="value" />
					        	            	</html:select> 
											</td>
										</tr>
										<tr>
											<th class="label-text" style="width:320px; line-height:20px;">Total Quoted Price</th>
											<td class="formContentview-rating-b0x" style="width:200px">
												&#8377;  <html:text property="totalQuotedPrice" styleId="totalQuotedPrice" styleClass="readonlymini" readonly="true" style="width:170px" />
											</td>
										</tr>
										<tr>
											<th class="label-text" style="width:320px; line-height:20px;">Total Lost Price</th>
											<td class="formContentview-rating-b0x" style="width:200px">
												&#8377;  <html:text property="totalLostPrice" styleId="totalLostPrice" styleClass="readonlymini" readonly="true" style="width:170px" />
											</td>
										</tr>
									</table>
								</fieldset>
							</td>
						</tr>
					</table>
				</div>
				<!---------------------------------		Request Information and Lost Details Block : END  ------------------------------------------------->
				
				<br> <br> <br>
				
				<!---------------------------------		Rating Details Block : END  ----------------------------------------------------------------------->
				<logic:notEmpty name="newEnquiryDto" property="newRatingsList">
				<%  int iRatingIndex = 1;  %>
				<div style="width: 100%; float: left;">
					<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
						<tr>
							<td>
								<fieldset>
									<table width="100%" border="0" align="center" cellpadding="0" cellspacing="3">
										<tr>
											<th class="formLabelTophome"> Rating Details </th>
											<th class="formLabelTophome"> Quoted Price </th>
											<th class="formLabelTophome"> Lost Price </th>
										</tr>
										<logic:iterate property="newRatingsList" name="newEnquiryDto" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
										<tr>
											<td class="formContentview-rating-b0x">
												<bean:write name="ratingObj" property="ltProdLineId"/>  |
												<bean:write name="ratingObj" property="ltFrameId"/><bean:write name="ratingObj" property="ltFrameSuffixId"/>  |
												<bean:write name="ratingObj" property="ltPoleId"/> Pole  |
												<bean:write name="ratingObj" property="ltKWId"/>kW  |
												<bean:write name="ratingObj" property="ltVoltId"/>  |
												<bean:write name="ratingObj" property="ltFreqId"/>  |
												<bean:write name="ratingObj" property="ltMountingId"/>
												
												<input type="hidden" id="ratingNo<%=iRatingIndex%>"  name="ratingNo<%=iRatingIndex%>"  value='<bean:write name="ratingObj" property="ratingNo" filter="false" />' >
												<input type="hidden" name="quantity<%=iRatingIndex%>" id="quantity<%=iRatingIndex%>" value='<bean:write name="ratingObj" property="quantity" filter="false" />' >
											</td>
											<td class="formContentview-rating-b0x">
												&#8377; <input type="text" name="quotedPrice<%=iRatingIndex%>" id="quotedPrice<%=iRatingIndex%>" class="readonlymini" readonly="true" value='<bean:write name="ratingObj" property="ltQuotedTotalPrice" />' maxlength="15" >
											</td>
											<td class="formContentview-rating-b0x">
												&#8377; <input type="text" name="lostPrice<%=iRatingIndex%>" id="lostPrice<%=iRatingIndex%>" class="normal" value="<bean:write name="ratingObj" property="ltLostTotalPrice" />" onchange="javascript:calculateTotalLostPrice('<%=iRatingIndex%>');" maxlength="15" >
											</td>
										</tr>
										<%	iRatingIndex = iRatingIndex+1; %>
										</logic:iterate>
									</table>
								</fieldset>
							</td>
						</tr>
					</table>
				</div>
				</logic:notEmpty>
				<!---------------------------------		Rating Details Block : END  ----------------------------------------------------------------------->
				
				<br>
				
				<!---------------------------------		Buttons Section : START  -------------------------------------------------------------------------->
				<div class="blue-light" align="right" style="font-weight:bold; width:100% !important;">
					<html:submit value="Submit" styleClass="BUTTON" />
				</div>
				<!---------------------------------		Buttons Section : END  ---------------------------------------------------------------------------->
				
			</td>
		</tr>
		
	</table>
	
</html:form>