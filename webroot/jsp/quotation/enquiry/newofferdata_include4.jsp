<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants" %>
<%@ page import="in.com.rbc.quotation.common.utility.PropertyUtility" %>
<%@ page import="in.com.rbc.quotation.common.utility.CommonUtility" %>
<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>


<table width="100%" border="0" align="center" cellpadding="0" cellspacing="3">
	<tr>
		<td width="20%" class="label-text"> Hardware </td>
		<td width="20%" class="normal">
			<select name="hardware" id="hardware" title="Hardware" >
				<option value="0"><bean:message key="quotation.option.select" /></option>
				<logic:present name="newenquiryForm" property="ltHardwareList" >
					<bean:define name="newenquiryForm" property="ltHardwareList" id="ltHardwareList" type="java.util.Collection" />
					<logic:iterate name="ltHardwareList" id="ltHardware" type="in.com.rbc.quotation.common.vo.KeyValueVo">
						<option value='<bean:write name="ltHardware" property="key" />' > <bean:write name="ltHardware" property="value" /> </option>
					</logic:iterate>
				</logic:present>
			</select>
		</td>
		<td width="10%">&nbsp;</td>
		<td width="20%" class="label-text"> Gland Plate </td>
		<td width="20%" class="normal">
			<select name="glandPlate" id="glandPlate" title="Gland Plate">
				<option value="0"><bean:message key="quotation.option.select" /></option>
				<logic:present name="newenquiryForm" property="ltGlandPlateList" >
					<bean:define name="newenquiryForm" property="ltGlandPlateList" id="ltGlandPlateList" type="java.util.Collection" />
					<logic:iterate name="ltGlandPlateList" id="ltGlandPlate" type="in.com.rbc.quotation.common.vo.KeyValueVo">
						<option value='<bean:write name="ltGlandPlate" property="key" />' > <bean:write name="ltGlandPlate" property="value" /> </option>
					</logic:iterate>
				</logic:present>
			</select>
		</td>
	</tr>
	<script> selectLstItemById('hardware', '<bean:write name="ratingObj" property="ltHardware" filter="false" />'); </script>
	<script> selectLstItemById('glandPlate', '<bean:write name="ratingObj" property="ltGlandPlateId" filter="false" />'); </script>
	<tr>
		<td width="20%" class="label-text"> Double Compression Gland </td>
		<td width="20%" class="normal">
			<select name="doubleCompressGland" id="doubleCompressGland" title="Double Compression Gland" >
				<option value="0"><bean:message key="quotation.option.select" /></option>
				<logic:present name="newenquiryForm" property="ltDoubleCompressionGlandList" >
					<bean:define name="newenquiryForm" property="ltDoubleCompressionGlandList" id="ltDoubleCompressionGlandList" type="java.util.Collection" />
					<logic:iterate name="ltDoubleCompressionGlandList" id="ltDoubleCompressionGland" type="in.com.rbc.quotation.common.vo.KeyValueVo">
						<option value='<bean:write name="ltDoubleCompressionGland" property="key" />' > <bean:write name="ltDoubleCompressionGland" property="value" /> </option>
					</logic:iterate>
				</logic:present>
			</select>
		</td>
		<td width="10%">&nbsp;</td>
		<td width="20%" class="label-text"> SPM Mounting Provision </td>
		<td width="20%" class="normal">
			<select name="spmMountingProvision" id="spmMountingProvision" title="SPM Mounting Provision"  >
				<option value="0"><bean:message key="quotation.option.select" /></option>
				<logic:present name="newenquiryForm" property="alTargetedList" >
					<bean:define name="newenquiryForm" property="alTargetedList" id="alTargetedList" type="java.util.Collection" />
					<logic:iterate name="alTargetedList" id="alTargeted" type="in.com.rbc.quotation.common.vo.KeyValueVo">
						<option value='<bean:write name="alTargeted" property="key" />' > <bean:write name="alTargeted" property="value" /> </option>
					</logic:iterate>
				</logic:present>
			</select>
		</td>
	</tr>
	<script> selectLstItemById('doubleCompressGland', '<bean:write name="ratingObj" property="ltDoubleCompressionGlandId" filter="false" />'); </script>
	<script> selectLstItemById('spmMountingProvision', '<bean:write name="ratingObj" property="ltSPMMountingProvisionId" filter="false" />'); </script>
	<tr>
		<td width="20%" class="label-text"> Addl. Name Plate </td>
		<td width="20%" class="normal">
			<select name="addlNamePlate" id="addlNamePlate" title="Additional Name Plate">
				<option value="0"><bean:message key="quotation.option.select" /></option>
				<logic:present name="newenquiryForm" property="ltAddNamePlateList" >
					<bean:define name="newenquiryForm" property="ltAddNamePlateList" id="ltAddNamePlateList" type="java.util.Collection" />
					<logic:iterate name="ltAddNamePlateList" id="ltAddNamePlate" type="in.com.rbc.quotation.common.vo.KeyValueVo">
						<option value='<bean:write name="ltAddNamePlate" property="key" />' > <bean:write name="ltAddNamePlate" property="value" /> </option>
					</logic:iterate>
				</logic:present>
			</select>
		</td>
		<td width="10%">&nbsp;</td>
		<td width="20%" class="label-text"> Direction Arrow Plate </td>
		<td width="20%" class="normal">
			<select name="directionArrowPlate" id="directionArrowPlate" title="Direction Arrow Plate">
				<option value="0"><bean:message key="quotation.option.select" /></option>
				<logic:present name="newenquiryForm" property="alTargetedList" >
					<bean:define name="newenquiryForm" property="alTargetedList" id="alTargetedList" type="java.util.Collection" />
					<logic:iterate name="alTargetedList" id="alTargeted" type="in.com.rbc.quotation.common.vo.KeyValueVo">
						<option value='<bean:write name="alTargeted" property="key" />' > <bean:write name="alTargeted" property="value" /> </option>
					</logic:iterate>
				</logic:present>
			</select>
		</td>
	</tr>
	<script> selectLstItemById('addlNamePlate', '<bean:write name="ratingObj" property="ltAddNamePlateId" filter="false" />'); </script>
	<script> selectLstItemById('directionArrowPlate', '<bean:write name="ratingObj" property="ltDirectionArrowPlateId" filter="false" />'); </script>
	<tr>
		<td width="20%" class="label-text"> RTD </td>
		<td width="20%" class="normal">
			<select name="rtd" id="rtd" title="RTD" >
				<option value="0"><bean:message key="quotation.option.select" /></option>
				<logic:present name="newenquiryForm" property="ltRTDList" >
					<bean:define name="newenquiryForm" property="ltRTDList" id="ltRTDList" type="java.util.Collection" />
					<logic:iterate name="ltRTDList" id="ltRTD" type="in.com.rbc.quotation.common.vo.KeyValueVo">
						<option value='<bean:write name="ltRTD" property="key" />' > <bean:write name="ltRTD" property="value" /> </option>
					</logic:iterate>
				</logic:present>
			</select>
		</td>
		<td width="10%">&nbsp;</td>
		<td width="20%" class="label-text"> BTD </td>
		<td width="20%" class="normal">
			<select name="btd" id="btd" title="BTD" >
				<option value="0"><bean:message key="quotation.option.select" /></option>
				<logic:present name="newenquiryForm" property="ltBTDList" >
					<bean:define name="newenquiryForm" property="ltBTDList" id="ltBTDList" type="java.util.Collection" />
					<logic:iterate name="ltBTDList" id="ltBTD" type="in.com.rbc.quotation.common.vo.KeyValueVo">
						<option value='<bean:write name="ltBTD" property="key" />' > <bean:write name="ltBTD" property="value" /> </option>
					</logic:iterate>
				</logic:present>
			</select>
		</td>
	</tr>
	<script> selectLstItemById('rtd', '<bean:write name="ratingObj" property="ltRTDId" filter="false" />'); </script>
	<script> selectLstItemById('btd', '<bean:write name="ratingObj" property="ltBTDId" filter="false" />'); </script>
	<tr>
		<td width="20%" class="label-text"> Thermister </td>
		<td width="20%" class="normal">
			<select name="thermister" id="thermister" title="Thermister" >
				<option value="0"><bean:message key="quotation.option.select" /></option>
				<logic:present name="newenquiryForm" property="ltThermisterList" >
					<bean:define name="newenquiryForm" property="ltThermisterList" id="ltThermisterList" type="java.util.Collection" />
					<logic:iterate name="ltThermisterList" id="ltThermister" type="in.com.rbc.quotation.common.vo.KeyValueVo">
						<option value='<bean:write name="ltThermister" property="key" />' > <bean:write name="ltThermister" property="value" /> </option>
					</logic:iterate>
				</logic:present>
			</select>
		</td>
		<td width="10%">&nbsp;</td>
		<td width="20%" class="label-text"> Aux Terminal Box </td>
		<td width="20%" class="normal">
			<select name="auxTerminalBox" id="auxTerminalBox" title="Aux. Terminal Box">
				<option value="0"><bean:message key="quotation.option.select" /></option>
				<logic:present name="newenquiryForm" property="ltAuxTerminalBoxList" >
					<bean:define name="newenquiryForm" property="ltAuxTerminalBoxList" id="ltAuxTerminalBoxList" type="java.util.Collection" />
					<logic:iterate name="ltAuxTerminalBoxList" id="ltAuxTerminalBox" type="in.com.rbc.quotation.common.vo.KeyValueVo">
						<option value='<bean:write name="ltAuxTerminalBox" property="key" />' > <bean:write name="ltAuxTerminalBox" property="value" /> </option>
					</logic:iterate>
				</logic:present>
			</select>
		</td>
	</tr>
	<script> selectLstItemById('thermister', '<bean:write name="ratingObj" property="ltThermisterId" filter="false" />'); </script>
	<script> selectLstItemById('auxTerminalBox', '<bean:write name="ratingObj" property="ltAuxTerminalBoxId" filter="false" />'); </script>
	<tr>
		<td width="20%" class="label-text"> Cable Sealing Box </td>
		<td width="20%" class="normal">
			<select name="cableSealingBox" id="cableSealingBox" title="Cable Sealing Box" >
				<option value="0"><bean:message key="quotation.option.select" /></option>
				<logic:present name="newenquiryForm" property="alTargetedList" >
					<bean:define name="newenquiryForm" property="alTargetedList" id="alTargetedList" type="java.util.Collection" />
					<logic:iterate name="alTargetedList" id="alTargeted" type="in.com.rbc.quotation.common.vo.KeyValueVo">
						<option value='<bean:write name="alTargeted" property="key" />' > <bean:write name="alTargeted" property="value" /> </option>
					</logic:iterate>
				</logic:present>
			</select>
		</td>
		<td width="10%">&nbsp;</td>
		<td width="20%" class="label-text"> ReLubrication System </td>
		<td width="20%" class="normal">
			<select name="bearingSystem" id="bearingSystem" title="ReLubrication System">
				<option value="0"><bean:message key="quotation.option.select" /></option>
				<logic:present name="newenquiryForm" property="ltBearingSystemList" >
					<bean:define name="newenquiryForm" property="ltBearingSystemList" id="ltBearingSystemList" type="java.util.Collection" />
					<logic:iterate name="ltBearingSystemList" id="ltBearingSystem" type="in.com.rbc.quotation.common.vo.KeyValueVo">
						<option value='<bean:write name="ltBearingSystem" property="key" />' > <bean:write name="ltBearingSystem" property="value" /> </option>
					</logic:iterate>
				</logic:present>
			</select>
		</td>
	</tr>
	<script> selectLstItemById('cableSealingBox', '<bean:write name="ratingObj" property="ltCableSealingBoxId" filter="false" />'); </script>
	<script> selectLstItemById('bearingSystem', '<bean:write name="ratingObj" property="ltBearingSystemId" filter="false" />'); </script>
	<tr>
		<td width="20%" class="label-text"> Bearing NDE </td>
		<td width="20%" class="normal">
			<select name="bearingNDE" id="bearingNDE" title="Bearing NDE" >
				<option value="0"><bean:message key="quotation.option.select" /></option>
				<logic:present name="newenquiryForm" property="ltBearingNDEList" >
					<bean:define name="newenquiryForm" property="ltBearingNDEList" id="ltBearingNDEList" type="java.util.Collection" />
					<logic:iterate name="ltBearingNDEList" id="ltBearingNDE" type="in.com.rbc.quotation.common.vo.KeyValueVo">
						<option value='<bean:write name="ltBearingNDE" property="key" />' > <bean:write name="ltBearingNDE" property="value" /> </option>
					</logic:iterate>
				</logic:present>
			</select>
		</td>
		<td width="10%">&nbsp;</td>
		<logic:equal name="ratingObj" property="flg_Addon_BearingDE_MTO" value="Y">
			<td width="20%" class="label-text" style="background:#ffff00;"> Bearing DE </td>
		</logic:equal>
		<logic:notEqual name="ratingObj" property="flg_Addon_BearingDE_MTO" value="Y">
			<td width="20%" class="label-text"> Bearing DE </td>
		</logic:notEqual>
		<td width="20%" class="normal">
			<select name="bearingDE" id="bearingDE" title="Bearing DE" >
				<option value="0"><bean:message key="quotation.option.select" /></option>
				<logic:present name="newenquiryForm" property="ltBearingDEList" >
					<bean:define name="newenquiryForm" property="ltBearingDEList" id="ltBearingDEList" type="java.util.Collection" />
					<logic:iterate name="ltBearingDEList" id="ltBearingDE" type="in.com.rbc.quotation.common.vo.KeyValueVo">
						<option value='<bean:write name="ltBearingDE" property="key" />' > <bean:write name="ltBearingDE" property="value" /> </option>
					</logic:iterate>
				</logic:present>
			</select>
		</td>
	</tr>
	<script> selectLstItemById('bearingNDE', '<bean:write name="ratingObj" property="ltBearingNDEId" filter="false" />'); </script>
	<script> selectLstItemById('bearingDE', '<bean:write name="ratingObj" property="ltBearingDEId" filter="false" />'); </script>
	<tr>
		<td width="20%" class="label-text"> Bearing Size NDE </td>
		<td width="20%" class="normal">
			<input name="bearingSizeNDE" class="normal"  maxlength="10"/> &nbsp;
		</td>
		<td width="10%">&nbsp;</td>
		<td width="20%" class="label-text"> Bearing Size DE </td>
		<td width="20%" class="normal">
			<input name="bearingSizeDE" class="normal"  maxlength="10"/> &nbsp;
		</td>
	</tr>
	<tr>
		<td width="20%" class="label-text"> Vibration Probes </td>
		<td width="20%" class="normal">
			<select name="vibrationProbes" id="vibrationProbes" title="Vibration Probes" >
				<option value="0"><bean:message key="quotation.option.select" /></option>
				<logic:present name="newenquiryForm" property="alTargetedList" >
					<bean:define name="newenquiryForm" property="alTargetedList" id="alTargetedList" type="java.util.Collection" />
					<logic:iterate name="alTargetedList" id="alTargeted" type="in.com.rbc.quotation.common.vo.KeyValueVo">
						<option value='<bean:write name="alTargeted" property="key" />' > <bean:write name="alTargeted" property="value" /> </option>
					</logic:iterate>
				</logic:present>
			</select>
		</td>
		<td width="10%">&nbsp;</td>
		<td width="20%" class="label-text"> Vibration Probe Mounting Pads </td>
		<td width="20%" class="normal">
			<select name="vibrationProbesMPads" id="vibrationProbesMPads" title="Vibration Probe Mounting Pads" >
				<option value="0"><bean:message key="quotation.option.select" /></option>
				<logic:present name="newenquiryForm" property="alTargetedList" >
					<bean:define name="newenquiryForm" property="alTargetedList" id="alTargetedList" type="java.util.Collection" />
					<logic:iterate name="alTargetedList" id="alTargeted" type="in.com.rbc.quotation.common.vo.KeyValueVo">
						<option value='<bean:write name="alTargeted" property="key" />' > <bean:write name="alTargeted" property="value" /> </option>
					</logic:iterate>
				</logic:present>
			</select>
		</td>
	</tr>
	<tr>
		<td width="20%" class="label-text"> Speed Switch </td>
		<td width="20%" class="normal">
			<select name="speedSwitch" id="speedSwitch" title="Speed Switch" >
				<option value="0"><bean:message key="quotation.option.select" /></option>
				<logic:present name="newenquiryForm" property="alTargetedList" >
					<bean:define name="newenquiryForm" property="alTargetedList" id="alTargetedList" type="java.util.Collection" />
					<logic:iterate name="alTargetedList" id="alTargeted" type="in.com.rbc.quotation.common.vo.KeyValueVo">
						<option value='<bean:write name="alTargeted" property="key" />' > <bean:write name="alTargeted" property="value" /> </option>
					</logic:iterate>
				</logic:present>
			</select>
		</td>
		<td width="10%">&nbsp;</td>
		<td width="20%">&nbsp;</td>
		<td width="20%">&nbsp;</td>
	</tr>
	<tr>
		<td width="20%">&nbsp;</td>
		<td width="20%">&nbsp;</td>
		<td width="10%">&nbsp;</td>
		<td width="20%">&nbsp;</td>
		<td width="20%">&nbsp;</td>
	</tr>
</table>