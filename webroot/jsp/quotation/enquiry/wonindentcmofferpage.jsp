<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants" %>
<%@ page import="in.com.rbc.quotation.common.utility.PropertyUtility" %>
<%@ page import="in.com.rbc.quotation.common.utility.CommonUtility" %>
<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.text.DecimalFormat" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<% String hideAddOn = ""; 
DecimalFormat df = new DecimalFormat("###,###");
%>

<html:form action="createEnquiry.do" method="POST">
<html:hidden property="invoke" value="viewIndentOfferData"/>
<html:hidden property="createdBySSO" /><!--SSO Id  -->
<html:hidden property="createdBy" /><!-- SSO Name -->

<logic:equal name="enquiryForm" property="enquiryOperationType" value="commercialengineer">
<input type="hidden" name="pagechecking" id="pagechecking" value="cle" />

</logic:equal>
<logic:equal name="enquiryForm" property="enquiryOperationType" value="commercialmanager">
<input type="hidden" name="pagechecking" id="pagechecking" value="cm" />

</logic:equal>

<html:hidden property="operationType"  styleId="operationType" value="CM" />

<html:hidden property="enquiryOperationType"/>
<html:hidden property="dispatch"/>
<html:hidden property="currentRating"/>
<html:hidden property="enquiryId" />
<html:hidden property="ratingId" />
<html:hidden property="enquiryNumber" />
<html:hidden property="nextRatingId"/>
<html:hidden property="updateCreateBy" />
<html:hidden property="isDataValidated"/>
<html:hidden property="isValidated"/>
<html:hidden property="ratingsValidationMessage"/>
<html:hidden property="assignToName" />
<html:hidden property="returnEnquiry" />


<input type="hidden" 
  name='<%= org.apache.struts.taglib.html.Constants.TOKEN_KEY %>'
  value='<%= session.getAttribute(org.apache.struts.action.Action.TRANSACTION_TOKEN_KEY) %>' />
<head>
  
<script>

 window.onload = function(){  
	addonstyle();
}
</script>
<script type="text/javascript">
var globalratesList=new Array;
</script>
<style>
.lablebox-td{
width:33% !important;
    text-align: left;
    font-weight: normal;
    font-size: 10px;
    font-family: Verdana, Arial, Helvetica, sans-serif;
    padding: 2px;
    background-color: #F2F2FF;
}
/* my general stylizing for inputs, mostly comes from Twitter's
bootstrap.min.css which is distributed with Zend Framework 2*/
/*input
{
  background-color: #fff;
  border: 1px solid #ccc;
  display: inline-block;
  height: 20px;
  padding: 4px 6px;
  margin-bottom: 10px;
  font-size: 11px;
  line-height: 20px;
  color: #555;
  vertical-align: middle;
  -webkit-border-radius: 4px;
  -moz-border-radius: 4px;
  border-radius: 4px;
}*/
select
{
 
  border: 1px solid #ccc;
  color: #555;
 
}
.background-bg-box{
background: #e0e0e0;
}
/* specific customizations for the data-list */
div.data-list-input
{
	position: relative;
	height: 20px;	
	display: inline-flex;
}
select.data-list-input
{
	position: absolute;	
	height: 20px;
	margin-bottom: 0;
}
input.data-list-input
{
	position: absolute;
	top: 0px;
	left: 0px;
	height: 19px;
}
.width-select{
width:100% !important;;
}
.input-class{
width:87% !important;
padding:4px 6px;
border-width:1px;
margin:0;
}
</style>
</head>
<!-------------------------- main table start  --------------------------------->


  <%!String rowIndex="";%>
 
 <% int countval = 0; %>
 <%!String str="";%>
 <div id="estimate">
<table width="100%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
  <tr>
    <td height="100%" valign="top">     
	
	<!-------------------------- Top Title and Status block Start  --------------------------------->
	<br>
        <div class="sectiontitle">
        	<bean:message key="quotation.indentpage.label.heading"/>
	  </div><br>
        <table width="100%" border="0" cellpadding="0" cellspacing="3">
          <tr>
            <td rowspan="2" style="width:40% "><table width="98%"  border="0" cellspacing="2" cellpadding="0" class="DotsTable">
              <tr>
                <td rowspan="2" style="width:70px "><img src="html/images/icon_quote.gif" width="52" height="50"></td>
              <td class="other">
              	<bean:message key="quotation.viewenquiry.label.subheading1"/> <bean:write name="enquiryForm" property="enquiryNumber"/><br>
                <bean:message key="quotation.viewenquiry.label.subheading2"/> </td>
            </tr>
              <tr>
                <td class="other"><table width="100%">
                  <tr>
                    <td height="16" class="section-head" align="center">
                    	<bean:message key="quotation.viewenquiry.label.status"/>  
                    	<bean:write name="enquiryForm" property="statusName"/> </td>
                  </tr>
                  <logic:equal name="enquiryForm" property="enquiryOperationType" value="commercialengineer">
                  <tr>
                    <td height="16" class="section-head" align="center">
                    	<bean:message key="quotation.home.colheading.assignto"/>  
                    	:&nbsp;
                    	<bean:write name="enquiryForm" property="designEngineerName"/> 
                    </td>
                  </tr>
                  <tr>
                    <td height="16" class="section-head" align="center">
                    	<bean:message key="quotation.create.label.salesrepresentative"/> 
                    	:&nbsp; 
                    	<bean:write name="enquiryForm" property="sm_name"/> 
                    </td>
                  </tr>
                  </logic:equal>
                   <logic:equal name="enquiryForm" property="enquiryOperationType" value="commercialmanager">
                  <tr>
                    <td height="16" class="section-head" align="center">
                    	<bean:message key="quotation.home.colheading.assignto"/>  
                    	:&nbsp;
                    	<bean:write name="enquiryForm" property="createdBy"/> 
                    </td>
                  </tr>
                  <tr>
                    <td height="16" class="section-head" align="center">
                    	<bean:message key="quotation.create.label.salesrepresentative"/> 
                    	:&nbsp; 
                    	<bean:write name="enquiryForm" property="sm_name"/> 
                    </td>
                  </tr>
                  </logic:equal>
                  
                  </table>
                  <br /></td>
            </tr>
            </table></td>
        <logic:lessThan name="enquiryForm" property="statusId" value="6"> 
	        <td class="other" align="right" colspan="2">
        	<bean:message key="quotation.attachments.message"/>
    	    <a href="javascript:manageAttachments('<%= CommonUtility.getIntValue(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_ATTACHMENT_APPLICATIONID)) %>','<bean:write name="enquiryForm" property="enquiryId"/>', '<bean:write name="enquiryForm" property="createdBySSO"/>');"><img src="html/images/ico_attach.gif" alt="Manage Attachments" width="23" height="25" border="0" align="right"></a></td>
        </logic:lessThan>
      </tr>
      <logic:notEmpty name="attachmentList">
      <tr>
      <td>&nbsp;</td>
      <td class="other" align="right" colspan="2">
      <bean:message key="quotation.attachments.enquirydocument"/>
      </td></tr>
      </logic:notEmpty>
          <tr>
          <td>
          &nbsp;
          </td>
            <td align="right" colspan="2">
            <logic:present name="attachmentList" scope="request">
            <logic:notEmpty name="attachmentList">
            <fieldset>
              <table width="100%"  cellpadding="0" cellspacing="0" border="0" class="other">
              <logic:iterate name="attachmentList" id="attachmentData" indexId="idx"
								               type="in.com.rbc.quotation.common.vo.KeyValueVo">	
				<tr><td>
					<a href="javascript:download('<bean:write name="attachmentData" property="key"/>', '<bean:write name="attachmentData" property="value"/>', '<bean:write name="attachmentData" property="value2"/>');">
						<bean:write name="attachmentData" property="value"/>.<bean:write name="attachmentData" property="value2"/>
					</a>
				</td></tr>					
				</logic:iterate>
              </table>
        </fieldset>
        </logic:notEmpty>
        </logic:present>
        </td>
        
      </tr>
     <tr><td colspan="2" align="right" > 
        <!-- DM Uploading Files  -->
            <logic:lessThan name="enquiryForm" property="statusId" value="6"> 
            <table>
            <tr>
	        <td class="other" align="right">
        	<bean:message key="quotation.attachments.dmuploadedmessage"/></td>
    	    <td class="other" align="left"><a href="javascript:manageAttachments('<%= CommonUtility.getIntValue(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_DMATTACHMENT_APPLICATIONID)) %>','<bean:write name="enquiryForm" property="enquiryId"/>', '<bean:write name="enquiryForm" property="createdBySSO"/>');"><img src="html/images/ico_attach.gif" alt="Manage Attachments" width="23" height="25" border="0" align="right"></a></td>
            </tr>
            </table>
        </logic:lessThan>
      
      </td>
      </tr> 
         <logic:notEmpty name="dmattachmentList">
      <tr>
      <td>
       &nbsp;
      </td>
       <td class="other" align="right" colspan="2">
      <bean:message key="quotation.attachments.designdocument"/>
      </td></tr>
      </logic:notEmpty>
      
           <tr>
           <td>
           &nbsp;
           </td>
           <td colspan="2" align="right" style="width:45% "> 
                       <logic:present name="dmattachmentList" scope="request">
            <logic:notEmpty name="dmattachmentList">
            <fieldset>
              <table width="100%" class="other">
              <logic:iterate name="dmattachmentList" id="attachmentData" indexId="idx"
								               type="in.com.rbc.quotation.common.vo.KeyValueVo">	
				<tr><td>
					<a href="javascript:download('<bean:write name="attachmentData" property="key"/>', '<bean:write name="attachmentData" property="value"/>', '<bean:write name="attachmentData" property="value2"/>');">
						<bean:write name="attachmentData" property="value"/>.<bean:write name="attachmentData" property="value2"/>
					</a>
				</td></tr>					
				</logic:iterate>
              </table>
        </fieldset>
        </logic:notEmpty>
        </logic:present>
           
           </td>
           </tr> 
           
                <!-- Indent Page Uploads Starts Here -->
     
          <tr>
          <td>&nbsp;</td>
          <td colspan="2" align="right" > 

            <logic:greaterEqual name="enquiryForm" property="statusId" value="6"> 
            <table>
            <tr>
	        <td class="other" align="right">
        	<bean:message key="quotation.attachments.indentuploadedmessage"/></td>
    	    <td class="other" align="left"><a href="javascript:manageAttachments('<%= CommonUtility.getIntValue(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_INTENDATTACHMENT_APPLICATIONID)) %>','<bean:write name="enquiryForm" property="enquiryId"/>', '<bean:write name="enquiryForm" property="createdBySSO"/>');"><img src="html/images/ico_attach.gif" alt="Manage Attachments" width="23" height="25" border="0" align="right"></a></td>
            </tr>
            </table>
        </logic:greaterEqual>
      
      </td>
      </tr> 
           <tr>
           <td>
           &nbsp;
           </td>
           <td colspan="2" align="right" style="width:45% "> 
                       <logic:present name="intendattachmentList" scope="request">
            <logic:notEmpty name="intendattachmentList">
            <fieldset>
              <table width="100%" class="other">
              <logic:iterate name="intendattachmentList" id="attachmentData" indexId="idx2"
								               type="in.com.rbc.quotation.common.vo.KeyValueVo">	
				<tr><td>
					<a href="javascript:download('<bean:write name="attachmentData" property="key"/>', '<bean:write name="attachmentData" property="value"/>', '<bean:write name="attachmentData" property="value2"/>');">
						<bean:write name="attachmentData" property="value"/>.<bean:write name="attachmentData" property="value2"/>
					</a>
				</td></tr>					
				</logic:iterate>
              </table>
        </fieldset>
        </logic:notEmpty>
        </logic:present>
           
           </td>
           </tr> 
     
     <!-- Indent Page Uploads Ends Here -->
           
           
           
        </table>
    <br>
    

<div style="clear:both; margin-bottom:30px; float:left; width:100%;">
<div style="width:90%; float:left;">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
<tr>
<td>	

</td>
<td valign="top">
<fieldset>
 <legend class="blueheader"><bean:message key="quotation.offer.label.reassign"/></legend>
       <table style="background-color:#FFFFFF ">
         <tr>
           <td><bean:message key="quotation.offer.label.reassignto"/></td>
           <td>
           	<bean:define name="enquiryForm" property="commercialManagersList"
          	             id="commercialManagersList" type="java.util.Collection" /> 
           	<html:select property="assignToId" style="width:370px;">
             	<option value="0"><bean:message key="quotation.option.select"/></option>
                 <html:options name="enquiryForm" collection="commercialManagersList"
	              property="key" labelProperty="value" />
             </html:select>
         </tr>
         <tr>
           <td><bean:message key="quotation.offer.label.comments"/></td>
           <td>
           <textarea style="width:370px; height:55px " maxlength="250" name="assignRemarks" id="assignRemarks"></textarea>
           


           </td>
         </tr>
       </table>
       <div class="blue-light" align="right">                
         <INPUT class=BUTTON onclick="javascript:reassign();" type=button value=Re-assign name=assign>
         </div>
</fieldset></td>   

</tr>

<tr>
<td colspan="2">
<!-- Detials Start  -->
<fieldset>
    <legend class="blueheader"><bean:message key="quotation.viewenquiry.label.request.heading"/></legend>
        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
			  <tr>
			  <td width="340"><label class="blue-light" style="font-weight: bold;">
				    	<bean:message key="quotation.create.label.basicinformation"/>
				   </label></td>
                   </tr>
			  
	

	
              <tr>
                <td>
                
                <div class="tablerow">
                <div class="tablecolumn" >
                 <table width="100%" border="0" cellspacing="0" cellpadding="2">
                 	<tr>
                    <td colspan="2" >&nbsp;</td>
                    </tr>

                    <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.indentno"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                      <bean:write name="enquiryForm" property="savingIndent" />

                      </td>
                    
                    </tr>
                     <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.label.admin.customer.customer"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                        <bean:write name="enquiryForm" property="customerName" />

                      </td>
                    
                    </tr>
                     <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.sapcusomercode"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                          <bean:write name="enquiryForm" property="sapCustomerCode" />

                      </td>
                    
                    </tr>
                     <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.orderno"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                         <bean:write name="enquiryForm" property="orderNumber" />  

                      </td>
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.quotationrefno"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                               <bean:write name="enquiryForm" property="enquiryReferenceNumber" />  
                      </td>
                    
                    </tr>
                    
                      <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.pocopy"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                       <bean:write name="enquiryForm" property="poiCopy" />  
                      </td>
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.loicopy"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                          <bean:write name="enquiryForm" property="loiCopy" />  
                       
                      </td>
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.deliverytype"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                        <bean:write name="enquiryForm" property="deliveryType" /> 
                       
                      </td>
                    
                    </tr>
                    
                    
                      <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.deliveryreqcustomer"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                        <bean:write name="enquiryForm" property="deliveryReqByCust" /> 
                       
                      </td>
                    
                    </tr>
                    
                      <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.expecteddeliverymonth"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                   <bean:write name="enquiryForm" property="expectedDeliveryMonth" /> 
                       
                      </td>
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.advancepayment"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                        <bean:write name="enquiryForm" property="advancePayment" /> 
                      </td>
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.paymentterms"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                                      <bean:write name="enquiryForm" property="paymentTerms" /> 
                      </td>
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.ld"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                                                            <bean:write name="enquiryForm" property="ld" /> 
                      
                       
                      </td>
                    
                    </tr>
                      
                      <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.wfd"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                                                            <bean:write name="enquiryForm" property="warrantyDispatchDate" /> 
                      </td>
                    
                    </tr>
                    
                      <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.wfc"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                      <bean:write name="enquiryForm" property="warrantyCommissioningDate" />                        
                      </td>
                    
                    </tr>
                    
                      <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.drawingapproval"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                      <bean:write name="enquiryForm" property="drawingApproval" /> 
                       
                      </td>
                    
                    </tr>
                    
                      <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.qapapproval"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                      <bean:write name="enquiryForm" property="qapApproval" />
                       
                      </td>
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.typetest"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                       <bean:write name="enquiryForm" property="typeTest" />
                       
                      </td>
                    
                    </tr>
                    
                      <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.routinetest"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                        <bean:write name="enquiryForm" property="routineTest" />
                       
                      </td>
                    
                    </tr>
                    
                    
                    
                </table>
                </div><!-- Left Table Column  -->
                <div class="tablecolumn" >
  				   <table width="100%" border="0" cellspacing="0" cellpadding="2">
                 	<tr>
                    <td colspan="2" >&nbsp;</td>
                    </tr>

                    <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.specialtest"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
                        <bean:write name="enquiryForm" property="specialTest" />
                      </td>    
                    
                    </tr>
                    <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.specialtestdet"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
                         <bean:write name="enquiryForm" property="specialTestDetails" />

                      </td>    
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.thirdpartyinspection"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
                         <bean:write name="enquiryForm" property="thirdPartyInspection" />

                      </td>    
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.stageinspection"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
                     <bean:write name="enquiryForm" property="stageInspection" />

                      </td>    
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.enduser"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
                       <bean:write name="enquiryForm" property="endUser" />

                      </td>    
                    
                    </tr>
                      
                      <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.enduserlocation"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
                       <bean:write name="enquiryForm" property="enduserLocation" />

                      </td>    
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.enduserindustry"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
                          <bean:write name="enquiryForm" property="endUserIndustry" />

                      </td>    
                    
                    </tr>
                    
                    
                    <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.consultant"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
                                                <bean:write name="enquiryForm" property="consultantName" />

                      </td>    
                    
                    </tr>
                    
                    
                      <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.epcname"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
                     <bean:write name="enquiryForm" property="epcName" />
					
                      </td>    
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.projectname"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
                                           <bean:write name="enquiryForm" property="projectName" />

                      </td>    
                    
                    </tr>
                    
                       <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.packing"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
                       <bean:write name="enquiryForm" property="packing" />
			
                      </td>    
                    
                    </tr>
                    
                    
                    <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.insurance"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
                     <bean:write name="enquiryForm" property="insurance" />
                      </td>    
                    
                    </tr>
                    
                    <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.replacement"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
                       <bean:write name="enquiryForm" property="replacement" />
                      </td>    
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.oldserialno"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
                       <bean:write name="enquiryForm" property="oldSerialNo" />

                      </td>    
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.customerpartno"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
                        <bean:write name="enquiryForm" property="customerPartNo" />

                      </td>    
                    
                    </tr>
                      
                      <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.winreason"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
				     <bean:write name="enquiryForm" property="winlossReason" />
                      </td>    
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.commentsifany"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
                      <bean:write name="enquiryForm" property="winlossComment" />

                      </td>    
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.competitor"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
                       <bean:write name="enquiryForm" property="winningCompetitor" />
                      </td>    
                    
                    </tr>
                    </table>
				</div><!--Right Table End  -->
                </div><!--Table Row End  -->
                
                <!--Financial  Start-->
                 <fieldset>
                     <legend class="blueheader">	
			         <bean:message key="quotation.create.label.financial"/>
			        </legend>
					<div class="blue-light" >
            		<table width="100%" border="0" cellspacing="0" cellpadding="0">
				  	<tr>
					<td>
                   <bean:message key="quotation.create.label.totalmotorprice"/>
                   <bean:write name="enquiryForm" property="totalMotorPrice" />
						  
				  </td>
				  <bean:define id="totalPkgPrice" name="enquiryForm" property="totalPkgPrice"/>
				  <%
				  if(totalPkgPrice!="" && totalPkgPrice!=QuotationConstants.QUOTATION_ZERO)
				   {
					  totalPkgPrice = df.format(Double.parseDouble(String.valueOf(totalPkgPrice)));
				   }

	
				  %>
				   <td align="right">
				  <bean:message key="quotation.create.label.totalpackageprice" />
				   <%=totalPkgPrice %>
				  
				  </td>
				  
				  </tr>
				  
				  
				  </table>
				  </div>
				  		      <div class="tablerow">
                <div class="tablecolumn" >
              <fieldset style="width:100%">
              <legend class="blueheader">	
			  <bean:message key="quotation.create.label.includes"/>
			  </legend>
              				  <table  style="width: 100%" border="0" cellspacing="0" cellpadding="0">
				  <tr><td colspan="5">&nbsp;</td></tr>
				  <tr>
				  <td  colspan="3">
				  <bean:message key="quotation.create.label.freight" />
				  </td>
				  <td class="formContentview-rating-b0x" style="width:50% ">
		          <bean:write name="enquiryForm" property="includeFrieght" />
				  </td>
				  </tr>
		          <tr>
				  <td colspan="3">
				  <bean:message key="quotation.create.label.extrawarranty" />
				  </td>
				  <td class="formContentview-rating-b0x" style="width:50% ">
				  <bean:write name="enquiryForm" property="includeExtraWarrnt" />
				  
				  </td>
				  </tr>
				  <tr>
				  <td colspan="3">
				  <bean:message key="quotation.create.label.supervisionandcomm" />
				  </td>
				  <td class="formContentview-rating-b0x" style="width:50% ">
					<bean:write name="enquiryForm" property="includeSupervisionCost" />
				  </td>
				  </tr>
					<tr>
				  <td colspan="3">
				  <bean:message key="quotation.create.label.typetestcharge" />
				  </td>
				  <td class="formContentview-rating-b0x" style="width:50% ">
		           <bean:write name="enquiryForm" property="includetypeTestCharges" />
				  </td>
				  </tr>
				  
				  
                  
		    </table>
                    </fieldset>
                    </div><!-- Left Div Endings  -->
                    <div class="tablecolumn" >
              <fieldset style="width:100%">
              <legend class="blueheader">	
			  <bean:message key="quotation.create.label.extra"/>
			  </legend>
                    
				  <table  style="width: 100%" border="0" cellspacing="0" cellpadding="0">
				  <tr><td colspan="5">&nbsp;</td></tr>
				  <tr>
				  <td  colspan="3">
				  <bean:message key="quotation.create.label.freight" />
				  </td>
				  <td class="formContentview-rating-b0x" style="width:50% ">
				  <bean:write name="enquiryForm" property="extraFrieght" />
				  </td>
				  </tr>
				  
			     <tr>
				  <td  colspan="3">
				  <bean:message key="quotation.create.label.extrawarrantycharges" />
				  </td>
				  <td class="formContentview-rating-b0x" style="width:50% ">
				   <bean:write name="enquiryForm" property="extraExtraWarrnt" />
				  </td>
				  </tr>
				  <tr>
				  <td  colspan="3">
				  <bean:message key="quotation.create.label.supervisionandcomm" />
				  </td>
				  <td class="formContentview-rating-b0x" style="width:50% ">
					<bean:write name="enquiryForm" property="extraSupervisionCost" />
				  </td>
				  </tr>
				  <tr>
				  <td colspan="3">
				  <bean:message key="quotation.create.label.typetestcharge" />
				  </td>
				  <td class="formContentview-rating-b0x" style="width:50% ">
		           <bean:write name="enquiryForm" property="extratypeTestCharges" />
				  </td>
				  </tr>
				  
				  <tr>
				  <td  colspan="3">
				  <bean:message key="quotation.create.label.spares" />
				  </td>
				  <td class="formContentview-rating-b0x" style="width:50% ">
				 <bean:write name="enquiryForm" property="sparesCost" />
				  
				  </td>
				  </tr>
				  
				  
				  
		    </table>
                    </div><!--Right Div Ending  -->
                    </div><!--Final Div Ending  -->
		                    
		                    
		                    
				  
				  
				  </fieldset>
                
                <!--Financial End  -->
                
                
                
        
        </td>
        </tr></table>
        
		</fieldset>
<!--Field Set Ending  -->

</tr>
</table>
</div>
</div>
        <br>
       <logic:present name="enquiryForm" scope="request">
		<logic:notEmpty name="enquiryForm" property="ratingObjects">
         <logic:iterate name="enquiryForm" property="ratingObjects"
								               id="ratingObject" indexId="idx" 
								               type="in.com.rbc.quotation.enquiry.dto.RatingDto">	
			<bean:define id="technicalObject" name="ratingObject" property="technicalDto"/>			
			<div id="arrow<%= idx.intValue()+1 %>" > 
				<a href="javascript:;" onClick="toggledropdown('arrow<%= idx.intValue()+1 %>','fieldset<%= idx.intValue()+1 %>')"> 
				<img src="html/images/rightTriArrow.gif" width="15" height="15"  border="0" align="absmiddle"/></a> 
				<span class="other">
					<bean:message key="quotation.viewenquiry.rating.label.heading"/> <%= idx.intValue()+1 %>
				</span></div>
				<fieldset id="fieldset<%= idx.intValue()+1 %>" style="display:none; ">
				 <legend>
				 	<a href="javascript:;" onClick="toggledropdown('fieldset<%= idx.intValue()+1 %>','arrow<%= idx.intValue()+1 %>')">
				 	<img src="html/images/downTriArrow.gif"  width="15" height="15"  border="0" align="absmiddle"/></a>
				 	<span class="blueheader">
				 		<bean:message key="quotation.viewenquiry.rating.label.collapse.heading"/> <%= idx.intValue()+1 %>
				 	</span>
				 </legend>
				 <!-- Indent Rating Data Starts Here, We Have To Do Changess  -->
				             <fieldset>
			<div class="blue-light" >
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td>
				
				  <bean:message key="quotation.create.label.quantityrequired"/>
                  <bean:write name="ratingObject" property="quantity"/>					</td>
					<td>
					&nbsp;
				</td>
				  </tr>
				</table>
		        	</div> <!--Quantity Ending  -->
		        	
				 <br>
			   	  <div class="tablerow">
                  <div class="tablecolumn" >
              	  <table  style="width: 100%" border="0" cellspacing="0" cellpadding="0">
				  <tr><td colspan="2">&nbsp;</td></tr>
			      <tr>
			       <td >
                    	<bean:message key="quotation.create.label.kw"/>
                    	
                    </td>
                    <td class="formContentview-rating-b0x">

                    	 <bean:write name="ratingObject" property="kw"/>
                    	 <logic:notEqual name="ratingObject" property="kw" value="" >
              			<bean:write name="ratingObject" property="ratedOutputUnit"/>
              			</logic:notEqual>
                    	 

                    </td>
                    </tr>
                    <tr>
			       <td >
                    	<bean:message key="quotation.create.label.pole"/>
                    	
                    </td>
                    <td class="formContentview-rating-b0x">
					<bean:write name="ratingObject" property="poleName"/>
                    </td>
                    </tr>
                    
                   <tr>
			       <td >
                    	<bean:message key="quotation.create.label.scsr"/>
                    	
                    </td>
                    <td class="formContentview-rating-b0x">
					<bean:write name="ratingObject" property="scsrName"/>
                    </td>
                    </tr>
                    
                     <tr>
                    <td>
			        	<bean:message key="quotation.offerdata.technical.label.framesize"/>
			        </td>
                    <td class="formContentview-rating-b0x">
                    <bean:write name="technicalObject" property="frameSize" />
                   	</td>
                    
                    </tr>
                    
                    <tr>
			       <td >
                    	<bean:message key="quotation.create.label.volts"/>
                    	
                    </td>
                    <td class="formContentview-rating-b0x">
                    <logic:notEqual name="ratingObject" property="volts" value="">
              			<bean:write name="ratingObject" property="volts"/>
              		</logic:notEqual>
              		<logic:notEqual name="ratingObject" property="voltsVar" value="">
	              		 +/-<bean:write name="ratingObject" property="voltsVar"/> %
	              	</logic:notEqual>
                    
                    </td>
                    </tr>
                    
                   <tr>
			       <td>
                    	<bean:message key="quotation.create.label.enclosure"/>
                    	
                    </td>
                    <td class="formContentview-rating-b0x">

              			<bean:write name="ratingObject" property="enclosureName"/>
                    </td>
                    </tr>	
                    
                   <tr>
			       <td>
                    	<bean:message key="quotation.create.label.mounting"/>
                    	
                    </td>
                    <td class="formContentview-rating-b0x">

              			<bean:write name="ratingObject" property="mountingName"/>
                    </td>
                    </tr>	
                    
                   <tr>
			       <td>
                    	<bean:message key="quotation.create.label.application"/>
                    	
                    </td>
                    <td class="formContentview-rating-b0x">

              			<bean:write name="ratingObject" property="applicationName"/>
                    </td>
                    </tr>	
               <%String mainTermBoxPS="",neutralTermBox="",spaceHeater="",windingRTD="",
                 bearingRTD="",rtGD2="",strtgCurrent="",unitpriceMultiplier_rsm="",
                priceincludeFrieght="",priceincludeFrieghtVal="",priceincludeExtraWarn="",
                priceincludeExtraWarnVal="",priceincludeSupervisionCost="",priceincludeSupervisionCostVal="",
                priceincludetypeTestCharges="",priceincludetypeTestChargesVal="",priceextraFrieght="",priceextraFrieghtVal="",
                priceextraExtraWarn="",priceextraExtraWarnVal="",priceextraSupervisionCost="",priceextraSupervisionCostVal="",
                priceextratypeTestCharges="",priceextratypeTestChargesVal=""; %>     
                    
                  				<logic:notEmpty name="enquiryForm" property="ratingObjects">
		         <logic:iterate name="enquiryForm" property="ratingObjects"
								               id="ratingObject1" indexId="idsx1" 
								               type="in.com.rbc.quotation.enquiry.dto.RatingDto">	
  
  				 <bean:define id="technicalObject1" name="ratingObject1" property="technicalDto"/>
  				 <%if(idx.intValue()==idsx1.intValue()){ %>
               
                    	<bean:define id="tech1" name="technicalObject1" property="mainTermBoxPS" />
                    	<bean:define id="tech2" name="technicalObject1" property="neutralTermBox" />
                    	<bean:define id="tech3" name="technicalObject1" property="spaceHeater"/>
                    	<bean:define id="tech4" name="technicalObject1" property="windingRTD"/>
                    	<bean:define id="tech5" name="technicalObject1" property="bearingRTD"/>
                    	<bean:define id="tech6" name="technicalObject1" property="rtGD2"/>
                    	<bean:define id="tech7" name="technicalObject1" property="strtgCurrent"/>
                    	<bean:define id="tech8" name="technicalObject1" property="unitpriceMultiplier_rsm"/>
                       <bean:define id="tech9" name="technicalObject1" property="priceincludeFrieght"/>
                       <bean:define id="tech10" name="technicalObject1" property="priceincludeFrieghtVal"/>
                       <bean:define id="tech11" name="technicalObject1" property="priceincludeExtraWarn"/>
                       <bean:define id="tech12" name="technicalObject1" property="priceincludeExtraWarnVal"/>
                       
                       <bean:define id="tech13" name="technicalObject1" property="priceincludeSupervisionCost"/>
                       <bean:define id="tech14" name="technicalObject1" property="priceincludeSupervisionCostVal"/>
                        <bean:define id="tech15" name="technicalObject1" property="priceincludetypeTestCharges"/>
                       <bean:define id="tech16" name="technicalObject1" property="priceincludetypeTestChargesVal"/>
                       <bean:define id="tech17" name="technicalObject1" property="priceextraFrieght"/>
                       <bean:define id="tech18" name="technicalObject1" property="priceextraFrieghtVal"/>
                       <bean:define id="tech19" name="technicalObject1" property="priceextraExtraWarn"/>
                       <bean:define id="tech20" name="technicalObject1" property="priceextraExtraWarnVal"/>
                       <bean:define id="tech21" name="technicalObject1" property="priceextraSupervisionCost"/>
                       <bean:define id="tech22" name="technicalObject1" property="priceextraSupervisionCostVal"/>                      
                       <bean:define id="tech23" name="technicalObject1" property="priceextratypeTestCharges"/>
                       <bean:define id="tech24" name="technicalObject1" property="priceextratypeTestChargesVal"/>                      
                      
                      
                       
                       
                       
                       
                    	
                    	
                    	
                    	
                    	<%
                    	mainTermBoxPS=String.valueOf(tech1);
                    	neutralTermBox=String.valueOf(tech2);
                    	spaceHeater=String.valueOf(tech3);
                    	windingRTD=String.valueOf(tech4);
                    	bearingRTD=String.valueOf(tech5);
                    	rtGD2=String.valueOf(tech6);
                    	strtgCurrent=String.valueOf(tech7);
                    	unitpriceMultiplier_rsm=String.valueOf(tech8);
                    	priceincludeFrieght=String.valueOf(tech9);
                    	priceincludeFrieghtVal=String.valueOf(tech10);
                    	priceincludeExtraWarn=String.valueOf(tech11);
                    	priceincludeExtraWarnVal=String.valueOf(tech12);
                    	priceincludeSupervisionCost=String.valueOf(tech13);
                    	priceincludeSupervisionCostVal=String.valueOf(tech14);
                    	priceincludetypeTestChargesVal=String.valueOf(tech15);
                    	priceincludetypeTestChargesVal=String.valueOf(tech16);
                    	priceextraFrieght=String.valueOf(tech17);
                    	priceextraFrieghtVal=String.valueOf(tech18);
                    	priceextraExtraWarn=String.valueOf(tech19);
                    	priceextraExtraWarnVal=String.valueOf(tech20);
                    	priceextraSupervisionCost=String.valueOf(tech21);
                    	priceextraSupervisionCostVal=String.valueOf(tech22);
                    	priceextratypeTestCharges=String.valueOf(tech23);
                    	priceextratypeTestChargesVal=String.valueOf(tech24);
                    	%>
                    	<%} %>
                    	</logic:iterate>
                    	</logic:notEmpty>
                    	
                    	
                    <tr>
			       <td>
                    	<bean:message key="quotation.offer.label.maintbps"/>
                    	
                    </td>
                    <td class="formContentview-rating-b0x">
                   <%=mainTermBoxPS %>
              			
                    </td>
                    </tr>	
                    
                    <tr>
			        <td>
                    	<bean:message key="quotation.offer.label.neutraltb"/>
                    	
                    </td>
                    <td class="formContentview-rating-b0x">
                   <%=neutralTermBox %>
              			
                    </td>
                    </tr>
                    
                    <tr>
			        <td>
                    	<bean:message key="quotation.offer.label.spaceheater"/>
                    	
                    </td>
                    <td class="formContentview-rating-b0x">
                   <%=spaceHeater %>
              			
                    </td>
                    </tr>	
                    
                     <tr>
			        <td>
                    	<bean:message key="quotation.create.label.windingrtd"/>
                    	
                    </td>
                    <td class="formContentview-rating-b0x">
                    <%=windingRTD %>
              			
                    </td>
                    </tr>	
                    
                    <tr>
			        <td>
                    	<bean:message key="quotation.offer.label.bearingrtd"/>
                    	
                    </td>
                    <td class="formContentview-rating-b0x">
                    <%=bearingRTD %>
              			
                    </td>
                    </tr>
                    
                    <tr>
			        <td>
                    	<bean:message key="quotation.create.label.momentofInertiaload"/>
                    	
                    </td>
                    <td class="formContentview-rating-b0x">
                    <%=rtGD2 %>
              			
                    </td>
                    </tr>
                    
                    <tr>
			        <td>
                    	<bean:message key="quotation.create.label.staringcurrent"/>
                    	
                    </td>
                    <td class="formContentview-rating-b0x">
                    <%=strtgCurrent %>
              			
                    </td>
                    </tr>
                    
                    	
                    
                    	
                    
                    
                    
                    </table>
                    </div> <!--Left Column End  -->
                    <div class="tablecolumn" >
				   <table  style="width: 100%" border="0" cellspacing="0" cellpadding="0">
				  <tr><td colspan="2">&nbsp;</td></tr>
				  <tr>
				<td>
			        	<bean:message key="quotation.create.label.htlt"/>
			        </td>
                    <td class="formContentview-rating-b0x">
                    <bean:write name="ratingObject" property="htltName"/>
					</td>				
					</tr>
				     <tr>
	                 <td>
                    	<bean:message key="quotation.create.label.frequency"/>
                    </td>
                       <td class="formContentview-rating-b0x"> 
                       <logic:notEqual name="ratingObject" property="frequency" value="">
              				<bean:write name="ratingObject" property="frequency"/>
              			</logic:notEqual>
              			<logic:notEqual name="ratingObject" property="frequencyVar" value="">
	              			+/-<bean:write name="ratingObject" property="frequencyVar"/>%
	              		</logic:notEqual>
                    
					</td>
					</tr>
					
					<tr>
				     <td>
			        	<bean:message key="quotation.create.label.duty"/>
			        </td>
                    <td class="formContentview-rating-b0x">
                    <bean:write name="ratingObject" property="dutyName"/>
					</td>				
					</tr>
					
		            <tr>
				     <td>
			        	<bean:message key="quotation.create.label.ambience"/>
			        </td>
                    <td class="formContentview-rating-b0x">
                  	<logic:notEqual name="ratingObject" property="ambience" value="">
                  		<bean:write name="ratingObject" property="ambience"/> 
                  	</logic:notEqual>
                  	
                  	<logic:notEqual name="ratingObject" property="ambienceVar" value="">
	                  	+/-<bean:write name="ratingObject" property="ambienceVar"/>
	                </logic:notEqual>

					</td>				
					</tr>
					
				   <tr>
				     <td>
			        	<bean:message key="quotation.create.label.degpro"/>
			        </td>
                    <td class="formContentview-rating-b0x">
                 	<bean:write name="ratingObject" property="degreeOfProName"/> 
                    </td>				
					</tr>
					
			        <tr>
				     <td>
			        	<bean:message key="quotation.create.label.altitude"/>
			        </td>
                    <td class="formContentview-rating-b0x">
                 	<logic:notEqual name="ratingObject" property="altitude" value="">
                 		<bean:write name="ratingObject" property="altitude"/> 
                 	</logic:notEqual>
                 	<logic:notEqual name="ratingObject" property="altitudeVar" value="">
                 		+/-<bean:write name="ratingObject" property="altitudeVar"/> 
                 	</logic:notEqual>

                    </td>				
					</tr>
					
				    <tr>
				     <td>
			        	<bean:message key="quotation.create.label.stgmethod"/>
			        </td>
                    <td class="formContentview-rating-b0x">
             	 	<bean:write name="ratingObject" property="methodOfStg"/>                    
             	  </td>				
					</tr>
					
                   <tr>
				     <td>
			        	<bean:message key="quotation.create.label.temprise"/>
			        </td>
                    <td class="formContentview-rating-b0x">
	              	 	<bean:write name="ratingObject" property="tempRaise"/> 
             	  </td>				
					</tr>
					
					<tr>
				     <td>
			        	<bean:message key="quotation.create.label.coupling"/>
			        </td>
                    <td class="formContentview-rating-b0x">
					<bean:write name="ratingObject" property="coupling"/>
             	  </td>				
					</tr>
					
			        <tr>
				     <td>
              	<bean:message key="quotation.create.label.insulationclass"/>
			        </td>
                    <td class="formContentview-rating-b0x">
					<bean:write name="ratingObject" property="insulationName"/>
             	  </td>				
					</tr>
					
				<tr>
				     <td>
              	<bean:message key="quotation.create.label.termbox"/>
			        </td>
                    <td class="formContentview-rating-b0x">
              	<bean:write name="ratingObject" property="termBoxName"/> 
             	  </td>				
					</tr>
					
				<tr>
				     <td>
              	<bean:message key="quotation.create.label.rotdirection"/>
			        </td>
                    <td class="formContentview-rating-b0x">
              	<bean:write name="ratingObject" property="directionOfRotName"/> 
             	  </td>				
					</tr>
					
				<tr>
				     <td>
              	<bean:message key="quotation.create.label.shaftext"/>
			        </td>
                    <td class="formContentview-rating-b0x">
              	<bean:write name="ratingObject" property="shaftExtName"/> 
             	  </td>				
					</tr>
					
				<tr>
				     <td>
              	<bean:message key="quotation.create.label.paint"/>
			        </td>
                    <td class="formContentview-rating-b0x">
              	<bean:write name="ratingObject" property="paint"/> 
             	  </td>				
					</tr>
					
				<tr>
				     <td>
              	<bean:message key="quotation.create.label.ratingunitprice"/>
			        </td>
                    <td class="formContentview-rating-b0x">
              	<bean:write name="ratingObject" property="ratingUnitPrice"/> 
             	  </td>				
					</tr>
	
					
					
					
					
					
					
					
					</table>
					</div><!--Right Column End  -->
					</div><!--Total Div Tag Ending  -->
				</fieldset> <!-- Technical Data End  -->
				<!-- Commercial Data Starts -->
				                     <fieldset>
                     <legend class="blueheader">	
			         <bean:message key="quotation.create.label.financial"/>
			        </legend>
					<div class="blue-light" >
            		<table width="100%" border="0" cellspacing="0" cellpadding="0">
				  	<tr>
					<td>
                   <bean:message key="quotation.create.label.unitpricemotor" />                   
                   <%=unitpriceMultiplier_rsm %>

				  </td>
				  
				   <td align="right">
				  <bean:message key="quotation.create.label.totalpackageprice" /> 
				   <bean:write name="ratingObject" property="totalPackagePrice" />
				  
				  </td>
				  </tr>
				  </table>
				  </div>
				  		      <div class="tablerow">
                <div class="tablecolumn" >
              <fieldset style="width:100%">
              <legend class="blueheader">	
			  <bean:message key="quotation.create.label.priceincludes"/>
			  </legend>
              				  <table  style="width: 100%" border="0" cellspacing="0" cellpadding="0">
				  <tr><td colspan="5">&nbsp;</td></tr>
				  <tr>
				  <td  colspan="3" style="width:50%">
				  <bean:message key="quotation.create.label.freight" />
				  </td>
				  <td class="formContentview-rating-b0x" style="width:15% ">
				  <%=priceincludeFrieght.equals("0")?"No":"Yes" %>
				  </td>
				  <td class="formContentview-rating-b0x" style="width:35% ">
				  <%if(priceincludeFrieghtVal!=""){ %>
				  <%=priceincludeFrieghtVal %>
				  <%} %>

				  </td>
				   
				  </tr>
		          <tr>
				  <td colspan="3" style="width:50%">
				  <bean:message key="quotation.create.label.extrawarranty" />
				  </td>
				  <td class="formContentview-rating-b0x" style="width:15% ">
				  <%=priceincludeExtraWarn.equals("0")?"No":"Yes" %>
				 
				 </td>
				  <td class="formContentview-rating-b0x" style="width:35% ">
		         <%if(priceincludeExtraWarnVal!=""){ %>
				  <%=priceincludeExtraWarnVal %>
				  <%} %>
		     

				  </td>
				  </tr>
				  <tr>
				  <td colspan="3" style="width:50%">
				  <bean:message key="quotation.create.label.supervisionandcomm" />
				  </td>
				  <td class="formContentview-rating-b0x" style="width:15% ">
		        <%=priceincludeSupervisionCost.equals("0")?"No":"Yes" %>  
				  
				  </td>
				  <td class="formContentview-rating-b0x" style="width:35% ">
                 <% if(priceincludeSupervisionCostVal!=""){ %>
				  <%=priceincludeSupervisionCostVal %>
				  <%} %>
		     				  
				  </td>
				  </tr>
					<tr>
				  <td colspan="3 style="width:50%">
				  <bean:message key="quotation.create.label.typetestingcharges" />
				  </td>
				  <td class="formContentview-rating-b0x" style="width:15% ">
				  <%=priceincludetypeTestCharges.equals("0")?"No":"Yes" %> 
				  </td>
				  <td class="formContentview-rating-b0x" style="width:35% ">
				   <% if(priceincludetypeTestChargesVal!=""){ %>
				  <%=priceincludetypeTestChargesVal %>
				  <%} %>
				  

				  </td>
				  </tr>
				  
				  
                  
		    </table>
                    </fieldset>
                    </div><!-- Left Div Endings  -->
                    <div class="tablecolumn" >
              <fieldset style="width:100%">
              <legend class="blueheader">	
			  <bean:message key="quotation.create.label.extra"/>
			  </legend>
                    
				  <table  style="width: 100%" border="0" cellspacing="0" cellpadding="0">
				  <tr><td colspan="5">&nbsp;</td></tr>
				  <tr>
				  <td  colspan="3" style="width:50% ">
				  <bean:message key="quotation.create.label.freight" />
				  </td>
				  
				 <td class="formContentview-rating-b0x" style="width:15% ">
			    <%=priceextraFrieght.equals("0")?"No":"Yes" %> 
				  </td>
				  <td class="formContentview-rating-b0x" style="width:35% ">
				  <% if(priceextraFrieghtVal!=""){ %>
				  <%=priceextraFrieghtVal %>
				  <%} %>
				  

				  </td>
				  </tr>
				  
			     <tr>
				  <td  colspan="3" style="width:50% ">
				  <bean:message key="quotation.create.label.extrawarranty" />
				  </td>
				  <td class="formContentview-rating-b0x" style="width:15% ">
				  <%=priceextraExtraWarn.equals("0")?"No":"Yes" %>				  </td>
				  <td class="formContentview-rating-b0x" style="width:35% ">
				  <% if(priceextraExtraWarnVal!=""){ %>
				  <%=priceextraExtraWarnVal %>
				  <%} %>
				  
			  </td>
				  </tr>
				  <tr>
				  <td  colspan="3" style="width:50% ">
				  <bean:message key="quotation.create.label.supervisionandcomm" />
				  </td>
				  <td class="formContentview-rating-b0x" style="width: 15%">
				  <%=priceextraSupervisionCost.equals("0")?"No":"Yes" %>
				  </td>
				  <td class="formContentview-rating-b0x" style="width:35% ">
				 <% if(priceextraSupervisionCostVal!=""){ %>
				  <%=priceextraSupervisionCostVal %>
				  <%} %>
				  

				  </td>
				  </tr>
				  
				  <tr>
				  <td  colspan="3" style="width:50% ">
				  <bean:message key="quotation.create.label.typetestingcharges" />
				  </td>
				  <td class="formContentview-rating-b0x" style="width: 15%">
				  <%=priceextratypeTestCharges.equals("0")?"No":"Yes" %>
				  
				  </td>
				  <td class="formContentview-rating-b0x" style="width:35% ">
			     <% if(priceextratypeTestChargesVal!=""){ %>
				  <%=priceextratypeTestChargesVal %>
				  <%} %>
				  
			
				  </td>
				  </tr>
				  
				  
		    </table>
                    </div><!--Right Div Ending  -->
		                    
		                    
				  
				  
                
                <!--Financial End  -->
				
				<!-- Commercial Data Ends  -->
				
			<!-- Delivery Details Starts -->	
            <fieldset style="width:100%">
		   <legend class="blueheader">	
			<bean:message key="quotation.create.label.deliverydetails"/>
		  </legend>
		    <logic:notEmpty name="ratingObject" property="deliveryDetailsList">
		  
		  			  <table width="100%" cellpadding="0" cellspacing="0"  >
		  			  <%int k=0; %>
   	  
 	  
					  
					  	 <logic:iterate name="ratingObject" property="deliveryDetailsList"
									               id="deliveryDetails" indexId="deliveryid1" 
									               type="in.com.rbc.quotation.enquiry.dto.DeliveryDetailsDto">	
							<% k = deliveryid1.intValue()+1;
							
							if(k==1)
							{
							%>
		<tr><td colspan="4">&nbsp;</td></tr>
							
							                  <tr>
                  
			    <tr>
			     <td class="formLabelTophome">
                  	<bean:message key="quotation.offerdata.delivery.label.sno"/>
                  </td>
			    
			      <td height="100%" style="width:130px " class="formLabelTophome" >
			      	<bean:message key="quotation.create.label.deliverylocation"/> 
			      </td>
                  <td class="formLabelTophome">
                  	<bean:message key="quotation.create.label.deliveryaddress"/>
                  </td>
                  <td class="formLabelTophome">
                  	<bean:message key="quotation.create.label.noofmotors"/> 
                  </td>
                  
                </tr>
                <%} %>
               <tr> 
               <td class="formContentview-rating-b0x"><%=k %></td>    
		        <td class="formContentview-rating-b0x">
		       <bean:write name="deliveryDetails" property="deliveryLocation" />
		         </td>
		         <td align="left" class="formContentview-rating-b0x">
			   <bean:write name="deliveryDetails" property="deliveryAddress" />		         
			   </td>
		       <td class="formContentview-rating-b0x">
				<bean:write name="deliveryDetails" property="noofMotors" />		      
				</td>
                
                </logic:iterate>
		  </table>
		  </logic:notEmpty>
		  
		  <!-- Delivery Details Ends  -->
                    
                
				 
				 
</fieldset>
</div>
</fieldset>
</fieldset>

</logic:iterate>
</logic:notEmpty>
</logic:present>
</td>
</tr>
</table>

			  <!--  Note Section Starts  -->
			<fieldset style="width:90%">
		    <legend class="blueheader">	
			<bean:message key="quotation.create.label.notesection"/>
			</legend>
			
			     <div >
				  <table  border="0" cellspacing="0" cellpadding="0">
				  <tr><td colspan="4">&nbsp;</td></tr>
			      				  <tr style="float:left; width:100%;">				  
				    <td class="formLabelrating" style="width:138px; margin-right: 33px;">
				    	<bean:message key="quotation.create.label.smnote"/>
				    </td>
                    <td colspan="3" class="formLabelrating">

                    <logic:notEqual name="enquiryForm" property="indentnote"  value="">
                                        <bean:write name="enquiryForm" property="indentnote"/>
                                     &nbsp; By &nbsp;  <bean:write name="enquiryForm" property="sm_name"/>
                    
                    </logic:notEqual>
                    
                  </tr>
                  				  <tr><td colspan="3">&nbsp;</td></tr>
			      				  <tr style="float:left; width:100%;">				  
				    <td class="formLabelrating" style="width:138px; margin-right: 33px;">
				    	<bean:message key="quotation.create.label.cmnote"/>
				    </td>
                    <td colspan="4">
                    	<html:textarea  property="cmNote" styleId = "cmNote"  style="width:600px; height:50px" onkeyup="checkMaxLenghtText(this, 4000);" onkeydown="checkMaxLenghtText(this, 4000);" tabindex="35"/>   </td>
                  </tr>
                  
		    </table>
		    </div>
		    </fieldset>
			  
			  <!--  Note Section Ends-->
			  
			  		   <!-- Return Statement From DM Starts  -->
			  		   
            <logic:notEqual name="enquiryForm" property="dmToCmReturn" value="">
            		    <fieldset style="width:90%">
 		          <legend class="blueheader">	
			          	<bean:message key="quotation.create.label.commentsection"/>
			          </legend>
 		    		    
			     <div>
			     
				  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr><td colspan="3" >&nbsp;</td></tr>
			      				  <tr style="float:left; width:100%;">				  
				    <td  style="width:138px; margin-right: 33px;">
				    
				    	<bean:message key="quotation.create.label.dmcomment"/>
				    	
				    </td>
				    
                    <td colspan="3" >
                    <font color="red">
						<bean:write name="enquiryForm" property="dmToCmReturn" />
						<input type="hidden" name="dmToCmReturn" id="dmToCmReturn" value="${enquiryForm.dmToCmReturn }"/>
                    </font>	                		         	
                    </td>
                    	<td></td>	
                    <td></td>
                  </tr>
		    </table>
		    </div>
		    
		    </fieldset>
            
            </logic:notEqual>
            <!-- Return Statement From Dm Ends -->
            
            		   <!-- Return Statement From Commercial Engineer  Starts  -->
            <logic:notEqual name="enquiryForm" property="deToCmReturn" value="">
            		    <fieldset style="width:90%">
 		          <legend class="blueheader">	
			          	<bean:message key="quotation.create.label.commentsection"/>
			          </legend>
 		    		    
			     <div>
			     
				  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr><td colspan="5" >&nbsp;</td></tr>
			      				  <tr style="float:left; width:100%;">				  
				    <td  style="width:138px; margin-right: 33px;">
				    
				    	<bean:message key="quotation.create.label.decomment"/>
				    	
				    </td>
				    
                    <td colspan="4" >
                    <font color="red">
						<bean:write name="enquiryForm" property="deToCmReturn" />
						<input type="hidden" name="deToCmReturn" id="deToCmReturn" value="${enquiryForm.deToCmReturn }"/>
                    </font>	                		         	
                    </td>
                    	<td></td>	
                    <td></td>
                  </tr>
		    </table>
		    </div>
		    </fieldset>
            
            </logic:notEqual>
            <!-- Return Statement From Commercial Engineer Ends -->
			  
			  

			  			  <!-------------------------- Bottum Input buttons block Start  --------------------------------->
			<div class="blue-light-btn" align="right">
			
         		<logic:equal name="enquiryForm" property="enquiryOperationType" value="commercialengineer">  
         		<input type="button" class="BUTTON" onClick="javascript:cereturntosalesmanager()"  value="Return to Sales Manager"> 
	         		<html:button property="" styleClass="BUTTON" value="Submit to Commercial Manager"  onclick="javascript:submitToCommercialManager();"/>
	         	</logic:equal>
 	         	<logic:equal name="enquiryForm" property="enquiryOperationType" value="commercialmanager">  
 	                <input type="button" class="BUTTON"  onclick="javascript:cmreturntosalesmanager()"  value="Return to Sales Manager">
 	         		<html:button property="" styleClass="BUTTON" value="Submit to Design Manager"  onclick="javascript:submitToDesignManager();"/>
	         	 </logic:equal> 
	  </div><br>
	  			  </div>
			  <!-------------------------- Bottum Input buttons block End  --------------------------------->
			  

			  
			  			<div id="return" class="showcontent">
			  			<br>
			  			<br>
			  			<br>
			<fieldset>
				<legend class="blueheader">
					<bean:message key="quotation.viewenquiry.return.label.heading"/>
				</legend>
				<table  align="center" >
                  <tr>
                    <td height="23" class="formLabel">
                    	<bean:message key="quotation.viewenquiry.return.label.returnto"/> 
                    </td>
                    <td>
                   	 <bean:define name="enquiryForm" property="teamMemberList"
	                	             id="teamMemberList" type="java.util.Collection" /> 
                   	 <html:select property="returnTo">                    	                  
                        <html:options name="enquiryForm" collection="teamMemberList"
			 				              property="key" labelProperty="value" />
                     </html:select>
                    </td>
                  </tr>
                  <tr>
                    <td class="formLabel">
                    	<bean:message key="quotation.viewenquiry.return.label.comments"/>
					</td>
                    <td>
                    	<html:textarea property="returnToRemarks" styleId="returnToRemarks" onchange = "javascript:checkMaxLength(this.id, 4000);"/>
                    </td>
                  </tr>
                </table>
			</fieldset>
			<div class="blue-light" align="right">
			   <input name="cancel" type="button" class="BUTTON" onClick="javascript:estimateForcm();" value="Cancel">	
               <input name="new" type="button" class="BUTTON" onclick="javascript:returnEnquiryTosm();" value="Return">                           
            </div>
			</div>
			  
			  

<!-------------------------- main table End  --------------------------------->
</html:form>

<% if (hideAddOn.equalsIgnoreCase("true")){ %>
	<script>
		showhide('Addonsdummy','Addonsfield')
		</script>
<%}%>

<ajax:autocomplete
		fieldId="customerName"
		popupId="customer-popup"
		targetId="customerId"
		baseUrl="servlet/AutocompleteServlet?type=placeenquiry&valueType=id"
		paramName="customer"
		className="autocomplete"
		progressStyle="throbbing" 
		postFunc="postAutocomplete"/>		 

	<iframe
		 id="divShim"
		src="javascript:false;"
		scrolling="no"
		frameborder="0"
		style="position:absolute; top:0px; left:0px; display:none;">
	</iframe>
	
<script>
$(document).ready(function(){

	calCurrent();	
});


function updateCurrent(currValue){

	if(currValue!=null && currValue!=""){
		document.enquiryForm.pllCurr1.value =currValue;
		document.enquiryForm.flcAmps.value =currValue;
	
	}
}

jQuery(function() {
	//keep the focus on the input box so that the highlighting
  //I'm using doesn't give away the hidden select box to the user
	$('select.data-list-input').focus(function() {
		$(this).siblings('input.data-list-input').focus();
	});
  //when selecting from the select box, put the value in the input box
	$('select.data-list-input').change(function() {
		$(this).siblings('input.data-list-input').val($(this).val());
	});
  //When editing the input box, reset the select box setting to "free
  //form input". This is important to do so that you can reselect the
  //option you had selected if you want to.
	$('input.data-list-input').change(function() {
		$(this).siblings('select.data-list-input').val('');
	});
  
  
});
</script>	

<script type="text/javascript">
	function checkMaxLength(textAreaId, maxlength){
		
		var taValue = document.getElementById(textAreaId).value;
		if(taValue.length>maxlength){
			document.getElementById(textAreaId).value = taValue.substring(0, maxlength);
		}
	}
	function returnEnquiryTosm(){
		document.enquiryForm.invoke.value = "returnEnquiry";
		document.enquiryForm.submit();
	}


</script>

