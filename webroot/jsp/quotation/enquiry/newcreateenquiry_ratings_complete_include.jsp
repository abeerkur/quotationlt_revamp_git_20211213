			<!-- Rating Section : START -->
			<table id="myform">
				<tr>
					<label class="blue-light" style="font-weight: bold;">
						Ratings 
							&nbsp; <button type="button" id="btnAddCol" onclick="javascript:addNewRating();">Add New Rating</button>
							<logic:greaterEqual name="newenquiryForm" property="totalRatingIndex" value="1">
							&nbsp; <button type="button" id="btnCopyCol" onclick="javascript:copyPreviousRating();">Copy & Add Previous Rating</button> 
							</logic:greaterEqual>
							<!--  &nbsp; <button type="button" id="btnRemoveCol" onclick="javascript:removeLastRating();">Remove Last Rating</button>  -->
							<!--  &nbsp; <button type="button" id="btnCalcPrice" onclick="javascript:calculateLPPerUnitAndDiscount();">Calculate Price</button> -->
					</label>
				</tr>
			</table>
			
			<div class="zui-wrapper">
				<div id="ratingsEncloseDiv" class="zui-scroller"> 
				
				<table class="table-rating zui-table">
					 <thead>
						<tr style="width:1100px !important; float:left;" id="myrating">
							<th valign="top">
								<div id="topfields_section" class="fixme">
									<jsp:include page="newcreateenquiry_topfields_include.jsp"/>
								</div>
							</th>
						</tr>
					 <thead>
					 <tbody>
					 	<tr>
					 		<td style="width: 100%; display: block;">
								<div id="bottomfields_section">
									<div style="background-color:#CCD8F2; width:100%; float:left; z-index:999999; height:20px;border:1px solid #fff;">
										<a class="accordion-toggle" style="font-size:12px; font-weight:bold; color:#00458A; display:block;" href="javascript:void(0);" onclick="javascript:toggleBaseFieldsSection();"> Basic </a>
									</div>
									<div id="basefields_section" style="margin-top:5px;margin-bottom:5px;">
										<jsp:include page="newcreateenquiry_pricefields_include.jsp"/>
									</div>
									<div style="background-color:#CCD8F2; width:100%; float:left; z-index:999999; height:20px;border:1px solid #fff;">
										<a class="accordion-toggle" style="font-size:12px; font-weight:bold; color:#00458A; display:block;" href="javascript:void(0);" onclick="javascript:toggleElectricalSection();"> Electrical </a>
									</div>
									<div id="electricalfields_section" style="margin-top:15px;margin-bottom:5px;">
										<jsp:include page="newcreateenquiry_electrical_include.jsp"/>
									</div>
									<div style="background-color:#CCD8F2; width:100%; float:left; z-index:999999; height:20px;border:1px solid #fff;">
										<a class="accordion-toggle" style="font-size:12px; font-weight:bold; color:#00458A; display:block;" href="javascript:void(0);" onclick="javascript:toggleMechanicalSection();"> Mechanical </a>
									</div>
									<div id="mechanicalfields_section" style="margin-top:15px;margin-bottom:5px;">
										<jsp:include page="newcreateenquiry_mechanical_include.jsp"/>
									</div>
									<div style="background-color:#CCD8F2; width:100%; float:left; z-index:999999; height:20px;border:1px solid #fff;">
										<a class="accordion-toggle" style="font-size:12px; font-weight:bold; color:#00458A; display:block;" href="javascript:void(0);" onclick="javascript:toggleAccessoriesSection();"> Accessories </a>
									</div>
									<div id="accessoriesfields_section" style="margin-top:15px;margin-bottom:5px;">
										<jsp:include page="newcreateenquiry_accessories_include.jsp"/>
									</div>
									<div style="background-color:#CCD8F2; width:100%; float:left; z-index:999999; height:20px;border:1px solid #fff;">
										<a class="accordion-toggle" style="font-size:12px; font-weight:bold; color:#00458A; display:block;" href="javascript:void(0);" onclick="javascript:toggleBearingsSection();"> Bearings </a>
									</div>
									<div id="bearingsfields_section" style="margin-top:15px;margin-bottom:5px;">
										<jsp:include page="newcreateenquiry_bearings_include.jsp"/>
									</div>
									<div style="background-color:#CCD8F2; width:100%; float:left; z-index:999999; height:20px;border:1px solid #fff;">
										<a class="accordion-toggle" style="font-size:12px; font-weight:bold; color:#00458A; display:block;" href="javascript:void(0);" onclick="javascript:toggleCertificateSparesSection();"> Certificate / Spares / Testing </a>
									</div>
									<div id="certificatefields_section" style="margin-top:15px;margin-bottom:5px;">
										<jsp:include page="newcreateenquiry_certificatespares_include.jsp"/>
									</div>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
				
				</div>
			</div>
								
		<!-- Rating Section : END -->