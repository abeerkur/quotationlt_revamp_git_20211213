<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants" %>
<%@ page import="in.com.rbc.quotation.common.utility.PropertyUtility" %>
<%@ page import="in.com.rbc.quotation.common.utility.CommonUtility" %>
<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>

<link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<table width="80%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
<tr>
<td>
		<div class="sectiontitle">
			<logic:present name="pageHeadingType" scope="request">
				<logic:equal name="pageHeadingType" value="<%= QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_CE %>">
					<bean:message key="quotation.viewenquiry.estimate.label.heading"/> 		
				</logic:equal>
				<logic:equal name="pageHeadingType" value="<%= QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_RSM %>">
					<bean:message key="quotation.refactor.label.heading"/> 		
				</logic:equal>
				<logic:equal name="pageHeadingType" value="view">
					<bean:message key="quotation.viewenquiry.label.heading"/> 		
				</logic:equal>
			</logic:present>
			
		  </div>
	<logic:present name="displayPaging" scope="request">
						     
							  <table align="right"> 
							  <tr>
						      <td>
							  <fieldset  style="width:100%">
							  <table cellpadding="0" cellspacing="2" border="0"> 
							  <tr >
						            <td style="text-align:center">
	      								<a href="searchenquiry.do?invoke=viewSearchEnquiryResults&dispatch=fromViewPage" class="returnsearch">
	      								Return to search<img src="html/images/back.gif" border="0" align="absmiddle" /></a>
	      							</td>
						        </tr>
						        <tr class="other" align="right">
						            <td align="right">
							            <logic:present name="previousId" scope="request">	
							            	<bean:define name="previousId" id="previousId" scope="request"/>
											<bean:define name="previousIndex" id="previousIndex" scope="request"/>	
											<bean:define name="previousStatus" id="previousStatus" scope="request"/>				
							            	<a href="createEnquiry.do?invoke=validateRequestAction&enquiryId=<bean:write name="previousId" scope="request"/>&statusId=<bean:write name="previousStatus" scope="request"/>&index=<bean:write name="previousIndex" scope="request"/>"><img src="html/images/PrevPage.gif" alt=""  border="0" align="absmiddle" /></a>
							            </logic:present>
							
							            <logic:present name="displayMessage" scope="request">
							            	<bean:write name="displayMessage" scope="request"/>
							            </logic:present>
							
							            <logic:present name="nextId" scope="request">
							            	<a href="createEnquiry.do?invoke=validateRequestAction&enquiryId=<bean:write name="nextId" scope="request"/>&statusId=<bean:write name="nextStatus" scope="request"/>&index=<bean:write name="nextIndex" scope="request"/>"><img src="html/images/NextPage.gif" alt="Next Request"  border="0" align="absmiddle" /></a>
							            </logic:present> 
										
									</td>
						       	</tr>
						       	</table>
								</fieldset>
								</td>
								</tr>
								</table>
</logic:present>

</td>
</tr>

<tr>
  <td height="100%" valign="top">

  <script language="javascript" src="../html/js/tooltip.js"></script>

  <logic:present name="enquiryDto" scope="request">
   <table width="100%" border="0" cellpadding="0" cellspacing="3">
     <tr>
       <td rowspan="2" ><table width="98%"  border="0" cellspacing="2" cellpadding="0" class="DotsTable">
           <tr>
             <td rowspan="2" style="width:70px "><img src="html/images/icon_quote.gif" width="52" height="50"></td>
             <td class="other"><bean:message key="quotation.viewenquiry.label.subheading1"/> <bean:write name="enquiryDto" property="enquiryNumber"/><br>
            <bean:message key="quotation.viewenquiry.label.subheading2"/> </td>
           </tr>
           <tr>
             <td class="other"><table width="100%">
                 <tr>
                   <td height="16" class="section-head" align="center">
                   	<bean:message key="quotation.viewenquiry.label.status"/>  <bean:write name="enquiryDto" property="statusName"/> </td>
                 </tr>
             </table></td>
           </tr>
       </table></td>
       <logic:lessThan name="enquiryDto" property="statusId" value="6"> 
	       <td colspan="2" align="right" >
		   <table border="0" cellspacing="0" cellpadding="0" style="width:auto">
			  <tr>
				<td  class="other" align="right">
				<bean:message key="quotation.attachments.message"/>
				</td>
				<td>
				<a href="javascript:manageAttachments('<%= CommonUtility.getIntValue(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_ATTACHMENT_APPLICATIONID)) %>','<bean:write name="enquiryForm" property="enquiryId"/>', '<bean:write name="enquiryForm" property="createdBySSO"/>');"><img src="html/images/ico_attach.gif" alt="Manage Attachments" border="0" align="right"></a>
				</td>
			  </tr>
			</table>
    	   </td>
       </logic:lessThan>
       <logic:present name="attachmentList" scope="request">
       <logic:notEmpty name="attachmentList">
              <logic:equal name="enquiryDto" property="statusId" value="6"> 
	       <td colspan="2" align="right" >
		   <table border="0" cellspacing="0" cellpadding="0" style="width:auto">
			  <tr>
				<td  class="other" align="right"  colspan="2">
				<bean:message key="quotation.attachments.enquirydocument"/>
				</td>
			  </tr>
			</table>
    	   </td>
       </logic:equal>
       </logic:notEmpty>
       </logic:present>
       
     </tr>
     
     <tr>
       <td  style="width:45% ">
       <logic:present name="attachmentList" scope="request">
       <logic:notEmpty name="attachmentList">
       <fieldset>
         <table width="100%" class="other">
           <logic:iterate name="attachmentList" id="attachmentData" indexId="idx"
								               type="in.com.rbc.quotation.common.vo.KeyValueVo">	
			<tr><td>
				<a href="javascript:download('<bean:write name="attachmentData" property="key"/>', '<bean:write name="attachmentData" property="value"/>', '<bean:write name="attachmentData" property="value2"/>');">
					<bean:write name="attachmentData" property="value"/>.<bean:write name="attachmentData" property="value2"/>
				</a>
			</td></tr>					
		   </logic:iterate>
         </table>
       </fieldset>
        </logic:notEmpty>
        </logic:present>
      </td>
     </tr>
     
     <tr><td colspan="2" align="right" > 
        <!-- DM Uploading Files  -->
            <logic:lessThan name="enquiryDto" property="statusId" value="6"> 
            <table>
            <tr>
	        <td class="other" align="right">
        	<bean:message key="quotation.attachments.dmuploadedmessage"/></td>
    	    <td class="other" align="right">
    	    <a href="javascript:manageAttachments('<%= CommonUtility.getIntValue(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_DMATTACHMENT_APPLICATIONID)) %>','<bean:write name="enquiryForm" property="enquiryId"/>', '<bean:write name="enquiryForm" property="createdBySSO"/>');"><img src="html/images/ico_attach.gif" alt="Manage Attachments" width="23" height="25" border="0" align="right"></a></td>
            </tr>
            </table>
        </logic:lessThan>
        
       <logic:present name="dmattachmentList" scope="request">
       <logic:notEmpty name="dmattachmentList">
      <logic:equal name="enquiryDto" property="statusId" value="6"> 
            <table>
            <tr>
	        <td class="other" align="right" colspan="2">
        	<bean:message key="quotation.attachments.designdocument"/></td>
            </tr>
            </table>
        </logic:equal>
        </logic:notEmpty>
        </logic:present>
      </td>
    </tr> 
    
    <tr>
           <td>
           &nbsp;
           </td>
           <td colspan="2" align="right" style="width:45% "> 
                       <logic:present name="dmattachmentList" scope="request">
            <logic:notEmpty name="dmattachmentList">
            <fieldset>
              <table width="100%" class="other">
              <logic:iterate name="dmattachmentList" id="attachmentData" indexId="idx"
								               type="in.com.rbc.quotation.common.vo.KeyValueVo">	
				<tr><td>
					<a href="javascript:download('<bean:write name="attachmentData" property="key"/>', '<bean:write name="attachmentData" property="value"/>', '<bean:write name="attachmentData" property="value2"/>');">
						<bean:write name="attachmentData" property="value"/>.<bean:write name="attachmentData" property="value2"/>
					</a>
				</td></tr>					
				</logic:iterate>
              </table>
        </fieldset>
        </logic:notEmpty>
        </logic:present>
           
           </td>
    </tr> 
     
     
   </table>
   <br>
 
    <!----------------------- Employee Information block start -------------------------------------->
	<fieldset>
    <legend class="blueheader"><bean:message key="quotation.viewenquiry.label.request.heading"/></legend>
        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
			  <tr>
			  <td width="340"><label class="blue-light" style="font-weight: bold;">
				    	<bean:message key="quotation.create.label.basicinformation"/>
				   </label></td>
                   </tr>
			  
	

	
	<tr>
<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
<tbody><tr >
<td width="40%"  class="label-text" ><bean:message key="quotation.create.label.enquirytype"/></td>
<td width="60%" class="formContentview-rating-b0x"><bean:write name="enquiryDto" property="enquiryType"/>  </td>
</tr>
</tbody></table></td>
</tr>


<tr>
<td><table width="100%" border="0" cellspacing="1" cellpadding="0">
<tbody><tr>
<td width="40%" class="label-text"><bean:message key="quotation.create.label.customer"/></td>
<td width="60%" class="formContentview-rating-b0x"><bean:write name="enquiryDto" property="customerName"/></td>
</tr>
</tbody></table></td>
</tr>

<tr>
 <td><table width="100%" border="0" cellspacing="1" cellpadding="0">
<tbody><tr>
<td width="40%" class="label-text"><bean:message key="quotation.create.label.projectname"/></td>
<td width="60%" class="formContentview-rating-b0x"><bean:write name="enquiryDto" property="projectName"/></td>
</tr>
</tbody></table></td>
</tr>


<tr>
 <td><table width="100%" border="0" cellspacing="1" cellpadding="0">
<tbody><tr>
<td width="40%" class="label-text"><bean:message key="quotation.create.label.salesstage"/></td>
<td width="60%" class="formContentview-rating-b0x"><bean:write name="enquiryDto" property="salesStage"/></td>
</tr>
</tbody></table></td>
</tr>


<tr>
 <td><table width="100%" border="0" cellspacing="1" cellpadding="0">
<tbody><tr>
<td width="40%" class="label-text"><bean:message key="quotation.create.label.approval"/></td>
<td width="60%" class="formContentview-rating-b0x"><bean:write name="enquiryDto" property="approval"/></td>
</tr>
</tbody></table></td>
 
 
 </tr>

<tr>
 <td><table width="100%" border="0" cellspacing="1" cellpadding="0">
<tbody><tr>
<td width="40%" class="label-text"><bean:message key="quotation.create.label.customertype"/></td>
<td width="60%" class="formContentview-rating-b0x"><bean:write name="enquiryDto" property="customerType"/></td>
</tr>
</tbody></table></td>
 </tr>

<tr>
 <td><table width="100%" border="0" cellspacing="1" cellpadding="0">
<tbody><tr>
<td width="40%" class="label-text"><bean:message key="quotation.create.label.enduserindustry"/></td>
<td width="60%" class="formContentview-rating-b0x"><bean:write name="enquiryDto" property="endUserIndustry"/> </td>
</tr>
</tbody></table></td>
</tr>

<tr>
<td><table width="100%" border="0" cellspacing="1" cellpadding="0">
<tbody><tr>
<td width="40%" class="label-text"><bean:message key="quotation.create.label.enduser"/></td>
<td width="60%"class="formContentview-rating-b0x"><bean:write name="enquiryDto" property="endUser"/></td>
</tr>
</tbody></table></td>
 </tr>
 <bean:define id="createdSSO" name="enquiryDto" property="createdBySSO" />	
<logic:equal name="userSSO" scope="request" value="<%=(String)createdSSO%>">
<logic:notEqual name="enquiryDto" property="statusId" value="6"> 			

<tr>
<td><table width="100%" border="0" cellspacing="1" cellpadding="0">
<tbody><tr>
<td width="40%" class="label-text"><bean:message key="quotation.create.label.orderclosingmonth"/></td>
<td width="60%" class="formContentview-rating-b0x"><bean:write name="enquiryDto" property="orderClosingMonth"/></td>
</tr>
</tbody></table></td>
</tr>

<tr>
<td><table width="100%" border="0" cellspacing="1" cellpadding="0">
<tbody><tr>
<td width="40%" class="label-text"><bean:message key="quotation.create.label.targeted"/></td>
<td width="60%" class="formContentview-rating-b0x"><bean:write name="enquiryDto" property="targeted"/></td>
</tr>
</tbody></table></td> 
</tr>

<tr>
 <td><table width="100%" border="0" cellspacing="1" cellpadding="0">
<tbody><tr>
<td width="40%" class="label-text"><bean:message key="quotation.create.label.winningchance"/></td>
<td width="60%" class="formContentview-rating-b0x"><bean:write name="enquiryDto" property="winningChance"/></td>
</tr>
</tbody></table></td>
</tr>

</logic:notEqual>

<logic:equal name="enquiryDto" property="statusId" value="6"> 

<tr>
<td><table width="100%" border="0" cellspacing="1" cellpadding="0">
<tbody><tr>
<td width="40%" class="label-text"><bean:message key="quotation.create.label.orderclosingmonth"/></td>
 <td><html:text  name="enquiryDto" property="orderClosingMonth" styleId="orderClosingMonth" styleClass="readonlymini" readonly="true" style="width:170px" maxlength="12"/>	</td>
<%-- <td width="60%" class="formContentview-rating-b0x"><bean:write name="enquiryDto" property="orderClosingMonth"/></td> --%>
</tr>
</tbody></table></td>
</tr>

<tr>
<td><table width="100%" border="0" cellspacing="1" cellpadding="0">
<tbody><tr>
<td width="40%" class="label-text"><bean:message key="quotation.create.label.targeted"/></td>
<td>			       
	<bean:define name="enquiryDto" property="alTargetedList" id="alTargetedList" type="java.util.Collection" /> 
	<html:select style="width:170px;" name="enquiryDto" property="targeted"  styleId="targeted" >
		<option value=""><bean:message key="quotation.option.select"/></option>
	 	<html:options name="enquiryDto" collection="alTargetedList" property="key" labelProperty="value" />
	</html:select>                   	                      	
</td>

</tr>
</tbody></table></td> 
</tr>

<tr>
 <td><table width="100%" border="0" cellspacing="1" cellpadding="0">
<tbody><tr>
<td width="40%" class="label-text"><bean:message key="quotation.create.label.winningchance"/></td>
<td>
   <bean:define name="enquiryDto" property="alWinChanceList" id="alWinChanceList" type="java.util.Collection" /> 
	<html:select style="width:170px;" name="enquiryDto" property="winningChance" styleId="winningChance">
		<option value=""><bean:message key="quotation.option.select"/></option>
   	 	<html:options name="enquiryDto" collection="alWinChanceList" property="key" labelProperty="value" />
    </html:select>    
</td>
</tr>
</tbody></table></td>
</tr>

</logic:equal>
</logic:equal>

<logic:notEqual name="userSSO" scope="request" value="<%=(String)createdSSO%>">
<tr>
<td><table width="100%" border="0" cellspacing="1" cellpadding="0">
<tbody><tr>
<td width="40%" class="label-text"><bean:message key="quotation.create.label.orderclosingmonth"/></td>
<td width="60%" class="formContentview-rating-b0x"><bean:write name="enquiryDto" property="orderClosingMonth"/></td>
</tr>
</tbody></table></td>
</tr>

<tr>
<td><table width="100%" border="0" cellspacing="1" cellpadding="0">
<tbody><tr>
<td width="40%" class="label-text"><bean:message key="quotation.create.label.targeted"/></td>
<td width="60%" class="formContentview-rating-b0x"><bean:write name="enquiryDto" property="targeted"/></td>
</tr>
</tbody></table></td> 
</tr>

<tr>
 <td><table width="100%" border="0" cellspacing="1" cellpadding="0">
<tbody><tr>
<td width="40%" class="label-text"><bean:message key="quotation.create.label.winningchance"/></td>
<td width="60%" class="formContentview-rating-b0x"><bean:write name="enquiryDto" property="winningChance"/></td>
</tr>
</tbody></table></td>
</tr>

</logic:notEqual>


<tr>
<td><table width="100%" border="0" cellspacing="1" cellpadding="0">
<tbody><tr>
<td width="40%" class="label-text"><bean:message key="quotation.create.label.deliveriestype"/></td>
<td width="60%" class="formContentview-rating-b0x"><bean:write name="enquiryDto" property="deliveryType"/></td>
</tr>
</tbody></table></td>
</tr>




<tr>
<td><table width="100%" border="0" cellspacing="1" cellpadding="0">
<tbody><tr>
<td width="40%" class="label-text"><bean:message key="quotation.create.label.expecteddeliverymonth"/></td>
<td width="60%"class="formContentview-rating-b0x"><bean:write name="enquiryDto" property="expectedDeliveryMonth"/></td>
</tr>
</tbody></table></td>
</tr>


<tr>
<td><table width="100%" border="0" cellspacing="1" cellpadding="0">
<tbody><tr>
<td width="40%" class="label-text"><bean:message key="quotation.create.label.totalordervalue"/></td>
<td width="60%"class="formContentview-rating-b0x"><bean:write name="enquiryDto" property="totalOrderValue"/></td>
</tr>
</tbody></table></td>
</tr>



<tr>
<td><table width="100%" border="0" cellspacing="1" cellpadding="0">
<tr>
<td width="40%" class="label-text"><bean:message key="quotation.create.label.enquiryreferencenumber"/></td>
<td width="60%" class="formContentview-rating-b0x"><bean:write name="enquiryForm" property="enquiryReferenceNumber"/></td>
</tr>
</table></td>
<td class=""></td>
<td></td>
<td colspan="4"><table width="100%" border="0" cellspacing="1" cellpadding="0">
<tr>
</tr>
</table></td>
</tr>

<tr>
<td colspan="8" valign="top">
<table width="100%" border="0" cellspacing="1" cellpadding="0"><tr>
<td width="14.8%" valign="top" class="label-text"><bean:message key="quotation.create.label.description"/></td>
<td width="78%" class="formContentview-rating-b0x" style="word-break: break-all;"><bean:write name="enquiryForm" property="description"/></td>
</tr>
</table></td>
</tr>

<tr>
<td><table width="100%" border="0" cellspacing="1" cellpadding="0">
<tr>
<td width="40%" class="label-text"><bean:message key="quotation.create.label.createdby"/> </td>
<td width="60%" class="formContentview-rating-b0x"><bean:write name="enquiryForm" property="createdBy"/></td>
</tr>
</table></td>
<td class=""></td>
<td></td>
<td><table width="100%" border="0" cellspacing="1" cellpadding="0">
<tr>
<td width="50%" class="label-text"><bean:message key="quotation.create.label.createddate"/> </td>
<td width="50%" class="formContentview-rating-b0x"><bean:write name="enquiryForm" property="createdDate"/>
</td>
</tr>
</table></td>
<td class=""></td>
<td></td>
<td></td>
</tr>
</table>
        
        
        
        
        
        
        
        
		</fieldset>
        
        
        
        
        <!--  Pradip--> 
        
        <!-- Required Data Starts From Here  IDX -->
        <br>
		<logic:notEmpty name="enquiryDto" property="ratingObjects">
         <logic:iterate name="enquiryDto" property="ratingObjects"
								               id="ratingObject" indexId="idx" 
								               type="in.com.rbc.quotation.enquiry.dto.RatingDto">				
			<div id="arrow<%= idx.intValue()+1 %>" > 
				<a href="javascript:;" onClick="toggledropdown('arrow<%= idx.intValue()+1 %>','fieldset<%= idx.intValue()+1 %>')"> 
				<img src="html/images/rightTriArrow.gif" width="15" height="15"  border="0" align="absmiddle"/></a> 
				<span class="other">
					<bean:message key="quotation.viewenquiry.rating.label.heading"/> <%= idx.intValue()+1 %>
				</span></div>
				<fieldset id="fieldset<%= idx.intValue()+1 %>" style="display:none; ">
				 <legend>
				 	<a href="javascript:;" onClick="toggledropdown('fieldset<%= idx.intValue()+1 %>','arrow<%= idx.intValue()+1 %>')">
				 	<img src="html/images/downTriArrow.gif"  width="15" height="15"  border="0" align="absmiddle"/></a>
				 	<span class="blueheader">
				 		<bean:message key="quotation.viewenquiry.rating.label.collapse.heading"/> <%= idx.intValue()+1 %>
				 	</span>
				 </legend>
				 <!-- Rating Data Starts Here, We Have To Do Changess  -->
				 <table width="100%">
					<tr>
                       <td colspan="5"  >&nbsp;</td>
          			 </tr>
          			 
          			 <tr>
                      
  					 <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.create.label.quantityrequired"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%">
                      	<bean:write name="ratingObject" property="quantity"/>
                      </td>
                 
                      <td rowspan="15"   >&nbsp;</td>
                      
                     
                      <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.create.label.volts"/>
                      </td>
                      <td class="formContentview-rating-b0x">
                      	<logic:notEqual name="ratingObject" property="volts" value="">
                  			<bean:write name="ratingObject" property="volts"/> 
                  		</logic:notEqual>
                  	
                  		<logic:notEqual name="ratingObject" property="voltsVar" value="">
                  			+/-<bean:write name="ratingObject" property="voltsVar"/> %
                  		</logic:notEqual>
                      </td>    
                    </tr>
          			 
          			 
                    <tr>
                      <td class="label-text">
                      	<bean:message key="quotation.create.label.application"/>
                      </td>
                      <td class="formContentview-rating-b0x">
                      	<bean:write name="ratingObject" property="applicationName"/> 
                      </td>
                      <td class="label-text">
                      	<bean:message key="quotation.create.label.frequency"/>
                      </td>
                      <td class="formContentview-rating-b0x">
                      	<logic:notEqual name="ratingObject" property="frequency" value="">
                  			<bean:write name="ratingObject" property="frequency"/>
                  		</logic:notEqual>
                  		<logic:notEqual name="ratingObject" property="frequency" value="">
	                  		<bean:message key="quotation.units.hz"/>
	               		</logic:notEqual>
                      </td>
                    </tr>
                    <tr>
                      <td class="label-text">
                      	<bean:message key="quotation.create.label.scsr"/>
                      </td>
                      <td class="formContentview-rating-b0x">
                      	<bean:write name="ratingObject" property="scsrName"/> 
                      </td>
                      <td height="100%" class="label-text">
                      	<bean:message key="quotation.create.label.pole"/>
                      </td>
                      <td class="formContentview-rating-b0x">
                      	<bean:write name="ratingObject" property="poleName"/> 
                      </td>
                     
                    </tr>
                    <%String MCUnit="",AreaClassification="",frameSize="",rpm="",flcAmps="",nlCurrent="",strtgCurrent="",fltSQCAGE="",serviceFactor="",
                    		lrTorque="",bkw="",potFLT="",rv="",ra="",pllCurr1="",pllEff1="",pllPf1="",
                    				pllCurr2="",pllEff2="",pllPf2="",
                    				pllCurr3="",pllEff3="",pllPf3="",
                    				pllCurr4="",pllPf4="",pllCurr5="",pllEff4="",pllPf5="",sstHot="",
                            		vibrationLevel="",sstCold="",mainTermBoxPS="",
                            	    startTimeRV="",neutralTermBox="",startTimeRV80="",
                            	    cableEntry="",auxTermBox="",statorConn="",noBoTerminals="",
                            	    rotation="",coolingSystem="",motorTotalWgt="",
                            	    spaceHeater="",bearingDENDE="",lubrication="",windingRTD="",
                            	    noiseLevel="",bearingRTD="",rtGD2="",minStartVolt="",
                            	    migD2Load="",balancing="",bearingThermoDT="",spmNipple="",
                            	    airThermo="",keyPhasor="",vibeProbMPads="",encoder="",
                            	    paintColor="",speedSwitch="",surgeSuppressors="",currTransformers="",vibProbes="",
                            	    termBoxName="",zone="",gasgroup="",deviation_clarification=""; %>
   				<logic:notEmpty name="enquiryDto" property="ratingObjects">
		         <logic:iterate name="enquiryDto" property="ratingObjects"
								               id="ratingObject1" indexId="idsx1" 
								               type="in.com.rbc.quotation.enquiry.dto.RatingDto">	
    

    
  				 <bean:define id="technicalObject1" name="ratingObject1" property="technicalDto"/>
  				 <%if(idx.intValue()==idsx1.intValue()){ 
  				 %>
  				 <bean:define id="tech1" name="technicalObject1" property="formattedCommercialMCUnit"/>
  				 <bean:define id="tech2" name="technicalObject1" property="areaClassification"/>
  				 <bean:define id="tech3" name="technicalObject1" property="frameSize"/>
  				 <bean:define id="tech4" name="technicalObject1" property="rpm"/>
  				 <bean:define id="tech5" name="technicalObject1" property="flcAmps"/>
  		         <bean:define id="tech6" name="technicalObject1" property="nlCurrent"/>
  		         <bean:define id="tech7" name="technicalObject1" property="strtgCurrent"/>
  		         <bean:define id="tech8" name="technicalObject1" property="fltSQCAGE" />
  		         <bean:define id="tech9" name="technicalObject1" property="serviceFactor"/>
  		         <bean:define id="tech10" name="technicalObject1" property="lrTorque"/>
  		         <bean:define id="tech11" name="technicalObject1" property="bkw"/>
  		         <bean:define id="tech12" name="technicalObject1" property="potFLT"/>
  		         <bean:define id="tech13" name="technicalObject1" property="rv" />
  		         <bean:define id="tech14" name="technicalObject1" property="ra" />
  		         <bean:define id="tech15" name="technicalObject1" property="pllCurr1" />
  		         <bean:define id="tech16" name="technicalObject1" property="pllEff1" />
  		         <bean:define id="tech17" name="technicalObject1" property="pllPf1" />
  		         <bean:define id="tech18" name="technicalObject1" property="pllCurr2" />
  		         <bean:define id="tech19" name="technicalObject1" property="pllEff2" />
  		         <bean:define id="tech20" name="technicalObject1" property="pllPf2" />
  				 <bean:define id="tech21" name="technicalObject1" property="pllCurr3" />
  		         <bean:define id="tech22" name="technicalObject1" property="pllEff3" />
  		         <bean:define id="tech23" name="technicalObject1" property="pllpf3" />
  				 <bean:define id="tech24" name="technicalObject1" property="pllCurr4" />
  		         <bean:define id="tech25" name="technicalObject1" property="pllpf4" />
  		         <bean:define id="tech26" name="technicalObject1" property="sstHot" />
  				<bean:define id="tech27" name="technicalObject1" property="vibrationLevel" />
  				<bean:define id="tech28" name="technicalObject1" property="sstCold" />
  				<bean:define id="tech29" name="technicalObject1" property="mainTermBoxPS" />
  				 <bean:define id="tech30" name="technicalObject1" property="startTimeRV" />
  			     <bean:define id="tech31" name="technicalObject1" property="neutralTermBox" />
  			     <bean:define id="tech32" name="technicalObject1" property="startTimeRV80" />
  		  	     <bean:define id="tech33" name="technicalObject1" property="cableEntry" />	
  		  	     <bean:define id="tech34" name="technicalObject1" property="statorConn" />
  		  	     <bean:define id="tech35" name="technicalObject1" property="auxTermBox" />
  		  	     <bean:define id="tech36" name="technicalObject1" property="noBoTerminals" />
  		  	     <bean:define id="tech37" name="technicalObject1" property="rotation" />
  		  	     <bean:define id="tech38" name="technicalObject1" property="coolingSystem"/>
  		  	     <bean:define id="tech39" name="technicalObject1" property="motorTotalWgt"/>
  		  	     <bean:define id="tech40" name="technicalObject1" property="spaceHeater"/>
  		  	     <bean:define id="tech41" name="technicalObject1" property="bearingDENDE"/>
  		  	     
  		  	     <bean:define id="tech42" name="technicalObject1" property="lubrication"/>
  		  	     <bean:define id="tech43" name="technicalObject1" property="windingRTD"/>

  		  	     <bean:define id="tech44" name="technicalObject1" property="noiseLevel"/>
  		  	     <bean:define id="tech45" name="technicalObject1" property="bearingRTD"/>
  		  	     
  		  	     <bean:define id="tech46" name="technicalObject1" property="rtGD2"/>
  		  	     <bean:define id="tech47" name="technicalObject1" property="minStartVolt"/>
  		  	     
  		  	     <bean:define id="tech48" name="technicalObject1" property="migD2Load"/>
  		  	     <bean:define id="tech49" name="technicalObject1" property="balancing"/>
  		  	    
  		  	      <bean:define id="tech50" name="technicalObject1" property="bearingThermoDT"/>
  		  	     <bean:define id="tech51" name="technicalObject1" property="spmNipple"/>

  		  	      <bean:define id="tech52" name="technicalObject1" property="airThermo"/>
  		  	     <bean:define id="tech53" name="technicalObject1" property="keyPhasor"/>
  		  	     
  		  	     <bean:define id="tech54" name="technicalObject1" property="vibeProbMPads" />
  		  	      <bean:define id="tech55" name="technicalObject1" property="encoder" />
  		  	      
  		  	       <bean:define id="tech56" name="technicalObject1" property="paintColor" />
  		  	      <bean:define id="tech57" name="technicalObject1" property="speedSwitch" />
  		  	      
  		  	      <bean:define id="tech58" name="technicalObject1" property="surgeSuppressors" />
  		  	      <bean:define id="tech59" name="technicalObject1" property="currTransformers" />
  		  	      <bean:define id="tech60" name="technicalObject1" property="vibProbes" />
  		  	     
  		  	     <bean:define id="tech61" name="technicalObject1" property="termBoxName" />

				<bean:define id="tech62" name="technicalObject1" property="zone" />
  		  	    <bean:define id="tech63" name="technicalObject1" property="gasGroup" /> 
  		  	    
  		  	    <bean:define id="tech64" name="technicalObject1" property="deviation_clarification" />
  		  	    <bean:define id="tech65" name="technicalObject1" property="pllCurr5" />
  		  	    <bean:define id="tech66" name="technicalObject1" property="pllEff4" /> 
  		  	    
  		  	    <bean:define id="tech67" name="technicalObject1" property="pllPf5" />
  		  	    
  		  	    
  				 
  				 
  				 <%
  				 
  				 MCUnit=String.valueOf(tech1);
  				AreaClassification=String.valueOf(tech2);
  				frameSize=String.valueOf(tech3);
  				rpm=String.valueOf(tech4);
  				flcAmps=String.valueOf(tech5);
  				nlCurrent=String.valueOf(tech6);
  				strtgCurrent=String.valueOf(tech7);
  				fltSQCAGE=String.valueOf(tech8);
  				serviceFactor=String.valueOf(tech9);
  				lrTorque=String.valueOf(tech10);
  				bkw=String.valueOf(tech11);
  				potFLT=String.valueOf(tech12);
  				rv=String.valueOf(tech13);
  				ra=String.valueOf(tech14);
  				pllCurr1=String.valueOf(tech15);
  				pllEff1=String.valueOf(tech16);
  				pllPf1=String.valueOf(tech17);
  				
  				pllCurr2=String.valueOf(tech18);
  				pllEff2=String.valueOf(tech19);
  				pllPf2=String.valueOf(tech20);
  				
  				pllCurr3=String.valueOf(tech21);
  				pllEff3=String.valueOf(tech22);
  				pllPf3=String.valueOf(tech23);
  				
  				pllCurr4=String.valueOf(tech24);
  				
  				pllPf4=String.valueOf(tech25);
  				
  				sstHot=String.valueOf(tech26);
  				
  				vibrationLevel=String.valueOf(tech27);
  				sstCold=String.valueOf(tech28);
  				mainTermBoxPS=String.valueOf(tech29);
  				startTimeRV=String.valueOf(tech30);
  				neutralTermBox=String.valueOf(tech31);
  				startTimeRV80=String.valueOf(tech32);
  				cableEntry=String.valueOf(tech33);
  				statorConn=String.valueOf(tech34);
  				auxTermBox=String.valueOf(tech35);
  				noBoTerminals=String.valueOf(tech36);
  				rotation=String.valueOf(tech37);
  				coolingSystem=String.valueOf(tech38);
  				motorTotalWgt=String.valueOf(tech39);
  				spaceHeater=String.valueOf(tech40);
  				bearingDENDE=String.valueOf(tech41);
  				lubrication=String.valueOf(tech42);
  				windingRTD=String.valueOf(tech43);
  				noiseLevel=String.valueOf(tech44);
  				bearingRTD=String.valueOf(tech45);
  				rtGD2=String.valueOf(tech46);
  				minStartVolt=String.valueOf(tech47);
  				migD2Load=String.valueOf(tech48);
  				
  				balancing="2.5 grade as per ISO 1940";
  				bearingThermoDT=String.valueOf(tech50);
  				spmNipple=String.valueOf(tech51);
  				
  				airThermo=String.valueOf(tech52);
  				keyPhasor=String.valueOf(tech53);
  				vibeProbMPads=String.valueOf(tech54);
  				
  				encoder=String.valueOf(tech55);
  				
  				paintColor=String.valueOf(tech56);
  				speedSwitch=String.valueOf(tech57);
  				surgeSuppressors=String.valueOf(tech58);
  				currTransformers=String.valueOf(tech59);
  				vibProbes=String.valueOf(tech60);  				
  				termBoxName=String.valueOf(tech61);  				
  				zone=String.valueOf(tech62);
  				gasgroup=String.valueOf(tech63);
  				deviation_clarification=String.valueOf(tech64);
  				pllCurr5=String.valueOf(tech65);
  				pllEff4=String.valueOf(tech66);
  				pllPf5=String.valueOf(tech67);
  				
  				 } %>


  				 </logic:iterate>
  				 </logic:notEmpty>	
                    
                    <tr>
                      <td class="label-text">
                      	<bean:message key="quotation.offer.label.areaclassification"/>
                      </td>
                      <td class="formContentview-rating-b0x">
                      	<%=AreaClassification %> 
                      </td>
                    <td height="100%" class="label-text">
                      	<bean:message key="quotation.offerdata.technical.label.rpm"/>
                      </td>
                      <td class="formContentview-rating-b0x">
                      	<%=rpm %> 
                      	<%if(rpm!=""){ %>
                      	<bean:message key="quotation.units.rmin"/>
                      	<%} %>
                     
                      </td>
                      </tr>
                      
                      
          			 <tr>
          			   <td class="label-text" >
                      	<bean:message key="quotation.create.label.zone"/>
                      </td>
                      <td class="formContentview-rating-b0x" >
                      	<%=zone %> 
                      </td>
                      <td height="100%" class="label-text">
                      	<bean:message key="quotation.offerdata.technical.label.flcamps"/>
                      </td>
                      <td class="formContentview-rating-b0x">
                      
                      <%=flcAmps %>
                      <%if(flcAmps!="") {%>
                      <bean:message key="quotation.units.a"/>
                      <%} %>
                      </td>
          			 
          			 </tr>
          			
          			 <tr>
          			   <td class="label-text" >
                      	<bean:message key="quotation.create.label.gasgroup"/>
                      </td>
                      <td class="formContentview-rating-b0x" >
                      	<%=gasgroup %> 
                      </td>
                      
                      <td height="100%" class="label-text">
                      	<bean:message key="quotation.offer.label.noloadcurrent"/>
                      </td>
                      <td class="formContentview-rating-b0x">
                      
                     <%=nlCurrent %>
                     <%if(nlCurrent!=""){ %>
                     <bean:message key="quotation.units.a"/>
                     <%} %>
                      </td>
          			 
          			 </tr>
          			 
                      
                         <tr>
                      <td class="label-text">
                      	<bean:message key="quotation.create.label.enclosure"/>
                      </td>
                      <td class="formContentview-rating-b0x">
                      	
                      	<bean:write name="ratingObject" property="enclosureName"/>
                      </td>
                      <td height="100%" class="label-text">
                      	<bean:message key="quotation.offer.label.startingcurrent"/>
                      </td>
                      <td class="formContentview-rating-b0x">
                      
                     <%=strtgCurrent %>
                      </td>
                    
                      </tr>
                      
                      <tr>
                      <td class="label-text">
                      	<bean:message key="quotation.offerdata.technical.label.framesize"/>
                      </td>
                      <td class="formContentview-rating-b0x">
                      	
                      	<%=frameSize %>
                      </td>
                      <td height="100%" class="label-text">
                      	<bean:message key="quotation.offerdata.technical.label.flt"/>
                      </td>
                      <td class="formContentview-rating-b0x">
                      
                     <%=fltSQCAGE %>
                     <%if(fltSQCAGE!=""){ %>
                     <bean:message key="quotation.units.nm"/>
                     <%} %>
                      </td>
                    
                      </tr>
                      
                      <tr>
                      <td class="label-text">
                      	<bean:message key="quotation.create.label.mounting"/>
                      </td>
                      <td class="formContentview-rating-b0x">
                      	
                      <bean:write name="ratingObject" property="mountingName" />
                      </td>
                      <td height="100%" class="label-text">
                      	<bean:message key="quotation.offer.label.lockedrotortorque"/>
                      </td>
                      <td class="formContentview-rating-b0x">
                      
                     <%=lrTorque %>
                      </td>
                    
                      </tr>
                      
                      
                      
                      <tr>
                      <td class="label-text">
                      	<bean:message key="quotation.create.label.kw"/>
                      </td>
                      <td class="formContentview-rating-b0x">
                      	
                      <bean:write name="ratingObject" property="kw" />
                      <logic:notEqual  name="ratingObject" property="kw" value="">
                       <bean:write name="ratingObject" property="ratedOutputUnit" />
                      
                      </logic:notEqual>
                      </td>
                      <td height="100%" class="label-text">
                      	<bean:message key="quotation.offerdata.technical.label.potflt"/>
                      </td>
                      <td class="formContentview-rating-b0x">
                      
                     <%=potFLT %>
                      </td>
                    
                      </tr>
                      
                      
                       <tr>
                      <td class="label-text">
                      	<bean:message key="quotation.offer.label.servicefactor"/>
                      </td>
                      <td class="formContentview-rating-b0x">
                      	<%=serviceFactor %>
                      </td>
                      <td height="100%" class="label-text">
                      	<bean:message key="quotation.offerdata.technical.label.rvslip"/>
                      </td>
                      <td class="formContentview-rating-b0x">
                      
                     <%=rv %>
                      </td>
                    
                      </tr>
                      
                      <tr>
                      <td class="label-text">
                      	<bean:message key="quotation.offer.label.bkw"/>
                      </td>
                      <td class="formContentview-rating-b0x">
                      	<%=bkw %>
                      	<%if(bkw!=""){ %>
                      	<bean:message key="quotation.option.kw"/>
                      	<%} %>
                      </td>
                      <td height="100%" class="label-text">
                      	<bean:message key="quotation.offerdata.technical.label.raslip"/>
                      </td>
                      <td class="formContentview-rating-b0x">
                      
                     <%=ra %>
                      </td>
                    
                      </tr>
                      
                      
                      <tr>
                      <td class="label-text">
                      	<bean:message key="quotation.create.label.duty"/>
                      </td>
                      <td class="formContentview-rating-b0x">
                      <bean:write name="ratingObject" property="dutyName" />
                      </td>
                    
                      </tr>
                      
                      <tr>
                      <td >
                      	&nbsp;
                      </td>
                      <td >
                      &nbsp;
                      </td>
                    
                      </tr>
                      </table>
				 

				<hr />
				<table width="100%" border="0" align="center" cellpadding="0" cellspacing="3">
<tr>
<td width="30%" class="label-text"><bean:message key="quotation.offer.label.loadcharacteristics"/></td>
<td width="2%"></td>
<td width="13%" class="label-text"><bean:message key="quotation.offer.label.load"/></td>
<td width="2%"></td>
<td width="18%" class="label-text"><bean:message key="quotation.offer.label.currenta"/></td>
<td width="2%"></td>
<td width="18%" class="label-text"><bean:message key="quotation.offer.label.efficiency"/></td>
<td width="2%"></td>
<td width="18%" class="label-text"><bean:message key="quotation.offer.label.powerfactor"/></td>
</tr>
<tr>
<td class="label-text"><bean:message key="quotation.offer.label.plldesc"/></td>
<td></td>
<td class="label-text"><bean:message key="quotation.offer.label.load100"/></td>
<td></td>

<td class="formContentview-rating-b0x"><%=pllCurr1%></td>
<td></td>
<td class="formContentview-rating-b0x"><%=pllEff1%></td>
<td></td>
<td class="formContentview-rating-b0x"><%=pllPf1%></td>
</tr>

<tr>
<td>&nbsp;</td>
<td></td>
<td class="label-text"><bean:message key="quotation.offer.label.load75"/></td>
<td></td>

<td class="formContentview-rating-b0x"><%=pllCurr2%></td>
<td></td>
<td class="formContentview-rating-b0x"><%=pllEff2%></td>
<td></td>
<td class="formContentview-rating-b0x"><%=pllPf2%></td>
</tr>


<tr>
<td>&nbsp;</td>
<td></td>
<td class="label-text"><bean:message key="quotation.offer.label.load50"/></td>
<td></td>

<td class="formContentview-rating-b0x"><%=pllCurr3%></td>
<td></td>
<td class="formContentview-rating-b0x"><%=pllEff3%></td>
<td></td>
<td class="formContentview-rating-b0x"><%=pllPf3%></td>
</tr>


<tr>
<td>&nbsp;</td>
<td></td>
<td class="label-text"><bean:message key="quotation.offer.label.loadstart"/></td>
<td></td>

<td class="formContentview-rating-b0x"><%=pllCurr4%></td>
<td></td>
<td >&nbsp;</td>
<td></td>
<td class="formContentview-rating-b0x"><%=pllPf4%></td>
</tr>

<tr>
<td>&nbsp;</td>
<td></td>
<td class="label-text"><bean:message key="quotation.offer.label.loaddutypoint"/></td>
<td></td>

<td class="formContentview-rating-b0x"><%=pllCurr5%></td>
<td></td>
<td class="formContentview-rating-b0x"><%=pllEff4%></td>
<td></td>
<td class="formContentview-rating-b0x"><%=pllPf5%></td>
</tr>



                      
 
</table>
<hr/>
<div class="tablerow">
  <div class="tablecolumn" >
  <table width="100%">
  <tr><td colspan="5"></td></tr>
  <%if(sstHot!=""){ %>
  <tr>
    					 <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.offer.label.safestalltimeh"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%">
                      	<%=sstHot%>
                      	<%if(sstHot!=""){ %>
                      	<bean:message key="quotation.units.s"/>
                      	<%} %>
                      </td>
  </tr>
  <%}if(sstCold!=""){ %>
  <tr>
  <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.offer.label.safestalltimec"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%">
                      	<%=sstCold%>
                      	<%if(sstCold!=""){ %>
                      	<bean:message key="quotation.units.s"/>
                      	<%} %>
                      </td>
 
  </tr>
  <%}if(startTimeRV!=""){ %>
  <tr>
    					 <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.offer.label.startingtimerv"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%">
                      	<%=startTimeRV%>
                      	<%if(startTimeRV!=""){ %>
                      	<bean:message key="quotation.units.s"/>
                      	<%} %>
                      </td>
  
  </tr>
  <%}if(startTimeRV80!=""){ %>
  <tr>
  <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.offer.label.startingtimerv80"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%">
                      	<%=startTimeRV80%>
                      	<%if(startTimeRV80!=""){ %>
                      	<bean:message key="quotation.units.s"/>
                      	<%} %>
                      </td>
  </tr>
  <%} %>
  <logic:notEqual name="ratingObject" property="insulationName" value="">
  <tr>
    					 <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.create.label.insulationclass"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%">
                      	
                      <bean:write name="ratingObject" property="insulationName"/>
                      </td>
  
  </tr>
  </logic:notEqual>
    <logic:notEqual name="ratingObject" property="tempRaiseDesc" value="">
  <tr>
    					 <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.create.label.temprise"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%">
                      	
                      <bean:write name="ratingObject" property="tempRaiseDesc"/>
                      </td>
  
  </tr>
  </logic:notEqual>
     <logic:notEqual name="ratingObject" property="ambience" value="">
  <tr>
  <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.create.label.ambience"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%"> 	
                      <bean:write name="ratingObject" property="ambience"/>
                     <logic:notEqual name="ratingObject" property="ambience" value="">
                     &nbsp;<bean:message key="quotation.units.c"/>
                     </logic:notEqual> 
                      </td>  
  </tr>
  </logic:notEqual>
       <logic:notEqual name="ratingObject" property="altitude" value="">
  <tr>
    					 <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.create.label.altitude"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%"> 	
                      <bean:write name="ratingObject" property="altitude"/>
                    <logic:notEqual name="ratingObject" property="altitude" value="">
                    &nbsp;<bean:message key="quotation.units.masl"/>
                    </logic:notEqual>
                      
                      </td>                     
  
  </tr>
  </logic:notEqual>
         <logic:notEqual name="ratingObject" property="degreeOfProName" value="">
  <tr>
    					 <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.create.label.degpro"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%"> 	
                      <bean:write name="ratingObject" property="degreeOfProName"/>
                      
                      </td>                     
  
  </tr>
  </logic:notEqual>
  <%if(coolingSystem!=""){ %>
  <tr>
    					 <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.offer.label.coolingsystem"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%"> 	
                      <%=coolingSystem %>
                      
                      </td>                     
  
  </tr>
  <%}if(bearingDENDE!=""){ %>
  <tr>
  <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.offer.label.bearingdende"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%"> 	
                      <%=bearingDENDE %>
                      
                      </td>
  </tr>
  <%}if(lubrication!=""){ %>
  <tr>
  <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.offer.label.lubrication"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%"> 	
                      <%=lubrication %>
                      
                      </td>
  </tr>
  <%}if(!noiseLevel.equals("0") && !noiseLevel.equals("")){ %>
  <tr>
    					 <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.offerdata.technical.label.noiselevel"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%"> 	
                      <%=noiseLevel %>
                      <%if(!noiseLevel.equals("0") && !noiseLevel.equals("")){ %>
                      &nbsp;<bean:message key="quotation.units.dba"/>
                      <%} %>
                      
                      </td>                     
  
  </tr>
  <%}if(rtGD2!=""){ %>
  <tr>
    					 <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.offerdata.technical.label.rtgd"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%"> 	
                      <%=rtGD2 %>
                      <%if(rtGD2!=""){ %>
                      &nbsp;<bean:message key="quotation.units.kgm2"/>
                      <%} %>
                      
                      </td>                     
  
  </tr>
  <%}if(migD2Load!=""){ %>
  <tr>
    					 <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.offer.label.moi"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%"> 	
                      <%=migD2Load %>
                      
                      </td>                     
  
  </tr>
  <%}if(balancing!=""){ %>
                       <tr>
  					 <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.offer.label.balancing"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%"> 	
                      <%=balancing %>
                      
                      </td>                     
                    </tr> 
  </table>
</div>
<div class="tablecolumn" >
  <table width="100%">
  <tr><td colspan="5"></td></tr>
  <%if(vibrationLevel!=""){ %>
  <tr>
                        <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.offerdata.technical.label.vibrationlevel"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:25% ">
                  			<%=vibrationLevel %>
                     </td>    
  
  </tr>
  <%}if(mainTermBoxPS!=""){ %>
  <tr>
                        <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.offer.label.maintbps"/>
                      </td>
                      <td class="formContentview-rating-b0x" >
                  	<%=mainTermBoxPS %>
                     </td>    
  
  </tr>
  <%}if(neutralTermBox!=""){ %>
  <tr>
                        <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.offer.label.neutraltb"/>
                      </td>
                      <td class="formContentview-rating-b0x" >
                  	<%=neutralTermBox %>
                     </td>    
  
  </tr>
  <%}if(cableEntry!=""){ %>
  <tr>
                        <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.offer.label.cableentry"/>
                      </td>
                      <td class="formContentview-rating-b0x" >
                  	<%=cableEntry %>
                     </td>    
  
  </tr>
  <%}if(auxTermBox!=""){ %>
  <tr>
                        <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.offer.label.auxterminalbox"/>
                      </td>
                      <td class="formContentview-rating-b0x" >
                  	<%=auxTermBox %>
                     </td>    
  
  </tr>
  <%}if(statorConn!=""){ %>
  <tr>
                        <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.offer.label.statorconnection"/>
                      </td>
                      <td class="formContentview-rating-b0x" >
                  	    <%=statorConn %>
                     </td>    
  
  </tr>
  <%}if(noBoTerminals!=""){ %>
  <tr>
                        <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.offer.label.nboterminals"/>
                      </td>
                      <td class="formContentview-rating-b0x" >
                  	  
              <%=noBoTerminals %>
                     </td> 
                        
  
  </tr>
  <%}if(rotation!=""){ %>
  <tr>
                        <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.offer.label.rotation"/>
                      </td>
                      <td class="formContentview-rating-b0x" >
                  	  
              <%=rotation %>
                     </td>    
  
  </tr>
  <%} %>
  <logic:notEqual name="ratingObject" property="directionOfRotName" value="">
  <tr>
                        <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.create.label.rotdirection"/>
                      </td>
                      <td class="formContentview-rating-b0x" >
                  	  <bean:write name="ratingObject" property="directionOfRotName"/>
                     </td>    
  
  </tr>
  </logic:notEqual>
  <%if(motorTotalWgt!=""){ %>
  <tr>
                        <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.offer.label.motortotalwt"/>
                      </td>
                      <td class="formContentview-rating-b0x" >
                  	  <%=motorTotalWgt %>
                  	  <%if(motorTotalWgt!=""){ %>
                  	  <bean:message key="quotation.units.kg"/>
                  	  <%} %>
                     </td>  
                       
  
  </tr>
  <%}if(spaceHeater!=""){ %>
  <tr>
                        <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.offer.label.spaceheater"/>
                      </td>
                      <td class="formContentview-rating-b0x" >
                  	  <%=spaceHeater %>
                     </td>    
  
  </tr>
  <%}if(windingRTD!=""){ %>
  <tr>
                        <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.offer.label.windingrtd"/>
                      </td>
                      <td class="formContentview-rating-b0x" >
                  	  <%=windingRTD %>
                     </td>    
  
  </tr>
  <%}if(bearingRTD!=""){ %>
  <tr>
                        <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.offer.label.bearingrtd"/>
                      </td>
                      <td class="formContentview-rating-b0x" >
                  	  <%=bearingRTD %>
                     </td>    

  
  </tr>
  <%}if(!minStartVolt.equals("0") && !minStartVolt.equals("")){ %>
  <tr>
                        <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.offer.label.minstartvoltage"/>
                      </td>
                      <td class="formContentview-rating-b0x" >
                  	  <%=minStartVolt %>
                     </td>    
  
  </tr>
  <%} %>
  <logic:notEqual name="ratingObject" property="methodOfStg" value="">
  <tr>
                        <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.create.label.stgmethod"/>
                      </td>
                      <td class="formContentview-rating-b0x" >
                <bean:write name="ratingObject" property="methodOfStg"/>
                     </td>    
  
  </tr>
  
  </logic:notEqual>
    <%}if(termBoxName!=""){%>
            
                    <tr>
                    <td class="label-text"  style="width:25%">
                    <bean:message key="quotation.create.label.termbox"/>
                    </td>
                    <td class="formContentview-rating-b0x"  style="width:25%">
                    <%=termBoxName %>
                    </td>
                    </tr>
        <%} %>
  
  
  </table>

</div>
</div>


  <hr />  

  <div class="tablerow">
  <div class="tablecolumn" >
         <table width="100%">
				<tr>
                       <td colspan="5"  >&nbsp;</td>
          			 </tr>

          			 <%if(bearingThermoDT!=""){ %>
          			 <tr>
                      
  					 <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.offer.label.dialtypebearingthermometer"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%">
                      	<%=bearingThermoDT%>
                      	
                      </td>
                      
                      
 </tr>
 <%} if(airThermo!=""){ %>
 <tr>
   					 <td class="label-text"  style="width:40%">
                      	<bean:message key="quotation.offer.label.airthermometer"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:40%">
                      	<%=airThermo%>
                      </td>
                      
 
 </tr>
 <%} if(vibeProbMPads!=""){ %>
 <tr>
   					 <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.offer.label.vibprobemountingpads"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%">

                      	<%=vibeProbMPads %>
                      </td>
 
 </tr>
 <%} if(encoder!=""){ %>
 <tr>
   					 <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.offer.label.encoder"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%">

                      	<%=encoder %>
                      </td>
 
 </tr>
 <%} if(speedSwitch!=""){ %>
 <tr>
   					 <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.offer.label.speedswitch"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%">

                      	<%=speedSwitch %>
                      </td>
 
 </tr>
 <%} if(surgeSuppressors!=""){ %>
                     <tr>
  					 <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.offer.label.surgesuppressors"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%">

                      	<%=surgeSuppressors %>
                      </td>
                     </tr>
 <%}if(currTransformers!=""){ %>
                  <tr>
                      
  					 <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.offer.label.currenttransformers"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%">

                      	<%=currTransformers %>
                      </td>
                     </tr>
                     <%} if(vibProbes!=""){ %>  
                     <tr>
  					 <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.offer.label.vibprobes"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%">

                      	<%=vibProbes %>
                      </td>
                     </tr>
                     <%} %>

 
 </table>
  </div>
  <div class="tablecolumn" >
           <table width="100%">
				<tr>
                       <td colspan="2"  >&nbsp;</td>
          			 </tr>
          		<%if(spmNipple!=""){ %>	 
          			 <tr>
                      
                      <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.offer.label.spmnipple"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:25% ">
                  			<%=spmNipple %>
                     </td>  
                       

 </tr>
 <%} if(keyPhasor!=""){ %>	 
 <tr>
                       <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.offer.label.keyphasor"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:25% ">
                  			<%=keyPhasor %>
                     </td>   
                      
 
 </tr>
 <%} %>
 <logic:notEqual name="ratingObject" property="shaftExtName" value="">
 <tr>
                       <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.create.label.shaftext"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:25% ">
                  			
                  			<bean:write name="ratingObject" property="shaftExtName"/>
                     </td>    
 
 </tr>
 </logic:notEqual>
  <logic:notEqual name="ratingObject" property="paint" value="">
 <tr>
                       <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.create.label.paint"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:25% ">
                  			
                  			<bean:write name="ratingObject" property="paint"/>
                     </td>    
 
 </tr>
 </logic:notEqual>
 <%if(paintColor!=""){ %>
 <tr>
                       <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.offer.label.paintcolor"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:25% ">
                  			
                  		<%=paintColor %>
                     </td>    
 
 </tr>
 <%} %>
 </table>
  
  </div>
</div>

  
      <logic:notEqual name="ratingObject" property="splFeatures" value="">
            <div>
      
      <hr/>
    <table style="width: 100%">
    <tr><td colspan="5">&nbsp;</td></tr>
                  <tr>
                  
                    <td colspan="5" class="section-head">
                    <span class="blueheader">
                    <bean:message key="quotation.create.label.specialfeatures"/>
                    </span>
                    </td>
                    </tr>
                  <tr>
                  <td colspan="5" class="formContentview-rating-b0x" style="width:200px;" ><bean:write name="ratingObject" property="splFeatures"/></td>
                 </tr>
    </table>
          </div>
    
    </logic:notEqual>
    

               
			                  
  



  <div>
      <%if(!deviation_clarification.equals("")){ %>  
      <hr/>
    <table style="width: 100%">
    <tr><td colspan="5">&nbsp;</td></tr>
                  <tr>
                  
                    <td colspan="5" class="section-head">
                    <span class="blueheader">
                    <bean:message key="quotation.create.label.deviationandclarification"/>
                    </span>
                    </td>
                    </tr>
                  <tr>
                  <td colspan="5" class="formContentview-rating-b0x" style="width:200px;" ><%=deviation_clarification %> </td>
                 </tr>
    </table>
    
    
               <%} %>	

               
			                  
  
  </div>
  <div>

  <logic:notEmpty name="ratingObject" property="addOnList">
  <table style="width: 100%;">
   	  <%int j=0; %>
   	  
 	  
					  
					  	 <logic:iterate name="ratingObject" property="addOnList"
									               id="addOnDto" indexId="idx1" 
									               type="in.com.rbc.quotation.enquiry.dto.AddOnDto">	
							<% j = idx1.intValue()+1;
							
							if(j==1)
							{
							%>
							<hr/>
							   	  <tr><td colspan="5">&nbsp;</td></tr>
							
							                  <tr>
                  
                    <td colspan="5" class="section-head">
                    <span class="blueheader">
                    <bean:message key="quotation.offerdata.addon.label.heading"/>
                    </span>
                    </td>
                    </tr>
							
						   <tr>
							<td  width="10%" class="formLabelTophome"><bean:message key="quotation.create.label.addon"/></td>
							<td width="2%"></td>
							<td class="formLabelTophome" width="35%">
							<bean:message key="quotation.offerdata.addon.label.addontype"/>
							</td>
							<td width="2%"></td>
							<td class="formLabelTophome"  width="25%" >							
								<bean:message key="quotation.offerdata.addon.label.quantity"/>					  			
					 		</td>
							
							
							<td  width="2%">
							</td>
							<td class="formLabelTophome" width="25%" >							
								<bean:message key="quotation.offerdata.addon.label.unitprice"/>					  			
					 		</td>
							</tr>	
							
							<%} %>
							<tr>
							<td  width="10%" class="formContentview-rating-b0x"><%=j %></td>
							<td width="2%"></td>
							<td class="formContentview-rating-b0x" width="35%">
							
							
								<bean:write name="addOnDto" property="addOnTypeText" />
							</td>
							<td width="2%"></td>
							<td class="formContentview-rating-b0x"  width="25%" >							
								<bean:write name="addOnDto" property="addOnQuantity"/>
					  			<input type="hidden" class="short" name="quantitylist"value="<bean:write name="addOnDto" property="addOnQuantity"/>" readonly  maxlength=10>					  			
					 		</td>
							
							
							<td  width="2%">
							</td>
							<td class="formContentview-rating-b0x" width="25%" >							
								<bean:write name="addOnDto" property="formattedAddOnUnitPrice"/>
					  			<input type="hidden" class="short" name="unitlist"  value="<bean:write name="addOnDto" property="addOnUnitPrice"/>" readonly  maxlength=10>					  			
					 		</td>
							</tr>	
											  
				  		</logic:iterate>
				  	
  

  </table>
  </logic:notEmpty>
  </div>
                   

</fieldset>
</logic:iterate>
</logic:notEmpty>
</logic:present>
</td>
</tr>

</table>
<script>
$(document).ready(function () { 
		
	 
	$("#orderClosingMonth").datepicker({
	    dateFormat: 'dd-mm-yy',
	    onSelect: function (selectedDate) {
	        if (this.id == 'orderClosingMonth') {
	            var arr = selectedDate.split("-");
	            var date = new Date(arr[2]+"-"+arr[1]+"-"+arr[0]);
	            var d = date.getDate();
	            var m = date.getMonth();
	            var y = date.getFullYear();
	            var minDate = new Date(y, m + 3, d);

	        }
	    }
	});
	});
</script>
