<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>

<link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


<input type="hidden" name="<%=org.apache.struts.taglib.html.Constants.TOKEN_KEY%>" value="<%=session.getAttribute(org.apache.struts.action.Action.TRANSACTION_TOKEN_KEY)%>">
 		
<!-------------------------- main table Start  --------------------------------->
<table width="100%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
  <tr>
    <td height="100%" valign="top">
    <html:form method="post" action="searchenquiry.do" onsubmit="return validateSearchEnquiry();">
    <html:hidden property="invoke" value="viewSearchEnquiryResults" />
    <html:hidden property="enquiryId" />
    <html:hidden property="createdBySSO" styleId="createdBySSO"/>
    <html:hidden property="assignToSSO" styleId="assignToSSO"/>
    <html:hidden property="designedBySSO" styleId="designedBySSO"/>
	<html:hidden property="dispatch" value="search" />
	<br>
	
    <div class="sectiontitle"><bean:message key="quotation.search.searchenquiry" />  </div>
      <br>
	  	<!-------------------------- Search By Block Start --------------------------------->
      <fieldset>
        <legend class="blueheader"><bean:message key="quotation.search.searchby" /> </legend>
        <table align="center" cellpadding="0" cellspacing="0" border="0" style="width:80% ">
          <tr>
            <td class="formLabel" style="width:10% "  ><bean:message key="quotation.search.rfq" /></td>
            <td ><html:text property="enquiryNumber"    styleClass="long"/></td>
            <td  class="dividingdots1" rowspan="10" >&nbsp;</td>
            <td class="formLabel" style="width:15% " ><bean:message key="quotation.search.createdbetween" /> </td>
            <td>
            <input type="text" id="createdStartDate" class="readonlymini" name="createdStartDate" size="30" readonly="true"/><html:img src="html/images/popupCalen.gif" styleClass="hand"   width="19" height="22" border="0" align="absmiddle" onclick="return showCalendar('createdStartDate', '%m/%d/%Y', '24', true);" /> To <input type="text" class="readonlymini" id="createdEndDate" name="createdEndDate" size="30" readonly="true"/><html:img src="html/images/popupCalen.gif" styleClass="hand"   width="19" height="22" border="0" align="absmiddle" onclick="return showCalendar('createdEndDate', '%m/%d/%Y', '24', true);" /> </td>
          </tr>
          <tr>
            <td class="formLabel"> <bean:message key="quotation.search.customer" /></td>
            <td>
            	<logic:present name="searchCustomerName" scope="session">
		           	<input id="customerName" name="customerName" type="text" class="long" value="<bean:write name="searchCustomerName"/>" />
		        </logic:present>
		        <logic:notPresent name="searchCustomerName" scope="session">
		        	<input id="customerName" name="customerName" type="text" class="long" />
		        </logic:notPresent>
		        <div class="note hidetable" id="invalidcustomer" style="text-align:left;padding-left:20px">Invalid Customer Name</div>
		        <html:hidden property="customerId" styleId="customerId"/>
             </td>
            <td class="formLabel"><bean:message key="quotation.search.createdby" /></td>
            <td>
            
            <html:text readonly="true" property="createdBy" styleClass="long" styleId="createdBy" />
            
                 <img src="html/images/lookup.gif" alt="Lookup" width="20" height="18" class="hand" border="0" align="absmiddle" 
                onclick="javascript:lookupEmployee('<%=QuotationConstants.QUOTATION_LOOKUPSELECTTYPE_SINGLE%>', 'createdBySSO', 'createdBy', '', '', '', '');">
                
                </td>
          </tr>
          <tr>
            <%-- <td class="formLabel"><bean:message key="quotation.search.location" /></td>
            <td>
            <html:hidden name="SearchEnquiryForm" property="locationName" />
            	<html:hidden name="SearchEnquiryForm" property="locationId" />
                <bean:define name="SearchEnquiryForm" property="locationList"
	                	             id="locationList" type="java.util.Collection" />
             	<select name="location" id="location" class="long"  tabindex="2"
             	        onChange="javascript:updateCustomerLocation(this);">
             	  	<option value="0">Select</option>
                 	<logic:iterate name="locationList" id="location"  
                 	               type="in.com.rbc.quotation.common.vo.KeyValueVo">
                 		<option value="<bean:write name="location" property="key" />"><bean:write name="location" property="value" /></option>
                 	</logic:iterate>
             	</select>
             	<script>
             	 	document.forms[0].location.value = '<bean:write name="SearchEnquiryForm" property="locationId" />';
             	</script>
                
                <label class="working" id="divWorking">
					<img src="html/images/loading.gif" border="0" align="absmiddle" style="cursor:hand ">
				</label>   
            </td> --%>
             <td class="formLabel">	<bean:message key="quotation.create.label.scsr"/></td>
                    <td>
                    	<bean:define name="SearchEnquiryForm" property="alScsrList"
	                	             id="alScsrList" type="java.util.Collection" /> 
                    	<html:select property="scsrId" styleClass="long" styleId="scsrId"  tabindex="10">
                    	<option value=""><bean:message key="quotation.option.select"/></option>
                         <html:options name="SearchEnquiryForm" collection="alScsrList"
			 				              property="key" labelProperty="value" />
                   		</html:select>
                   	</td>
                        <td class="formLabel"><bean:message key="quotation.home.colheading.assignto" /></td>
            <td>
            
            <html:text readonly="true" property="assignTo" styleClass="long" styleId="assignTo" />
            
                 <img src="html/images/lookup.gif" alt="Lookup" width="20" height="18" class="hand" border="0" align="absmiddle" 
                onclick="javascript:lookupEmployee('<%=QuotationConstants.QUOTATION_LOOKUPSELECTTYPE_SINGLE%>', 'assignToSSO', 'assignTo', '', '', '', '');">
                
                </td>
            
          </tr>
          <tr>
            <td class="formLabel"><bean:message key="quotation.search.frame" /></td>
            <td><html:text property="frameName" styleId="scsrId" styleClass="long"/></td>
            
		    <td class="formLabel"><bean:message key="quotation.search.designedby" /></td>
            <td>
            
            <html:text readonly="true" property="designedBy" styleClass="long" styleId="designedBy" />
            
                 <img src="html/images/lookup.gif" alt="Lookup" width="20" height="18" class="hand" border="0" align="absmiddle" 
                onclick="javascript:lookupEmployee('<%=QuotationConstants.QUOTATION_LOOKUPSELECTTYPE_SINGLE%>', 'designedBySSO', 'designedBy', '', '', '', '');">
                
                </td>
			
			  </tr>
          <tr>
            <td class="formLabel"><bean:message key="quotation.search.pole" /></td>
            <td>
            <bean:define name="SearchEnquiryForm" property="alPoleList"
	                	             id="alPoleList" type="java.util.Collection" /> 
                        <html:select styleClass="long" property="poleId"  styleId="poleId" >
                         <option value=""><bean:message key="quotation.option.select"/></option>
                          <html:options name="SearchEnquiryForm" collection="alPoleList"
			 				              property="key" labelProperty="value" />
                       </html:select>
            
          </td>
            
             <td class="formLabel"><bean:message key="quotation.search.status" /></td>
            <td><html:select name="SearchEnquiryForm" property="statusId"  styleId="statusId" styleClass="long"  >
					<html:option value=""><bean:message key="quotation.option.select" /></html:option>
					<logic:present name="SearchEnquiryForm" property="statusList" scope="request">
						   	<bean:define name="SearchEnquiryForm" property="statusList" id="statusList" type="java.util.Collection" />
							<html:options name="SearchEnquiryForm" collection="statusList" property="key" labelProperty="value" />
						</logic:present>
				</html:select>
			</td>        
            
            
          </tr>
          <tr>
            <td class="formLabel"><bean:message key="quotation.search.kw" /></td>
            <td><html:text property="kw" styleClass="long" styleId="kw" />
            <html:select property="ratedOutputUnit" styleClass="short"  styleId="ratedOutputUnit" style="width:44px;" >
                    	<logic:equal name="SearchEnquiryForm" property="ratedOutputUnit" value="kW">
						<option value='kW'><bean:message key="quotation.option.kw"/></option>
						<option value='HP'><bean:message key="quotation.option.hp"/></option>
						</logic:equal>
						<logic:equal name="SearchEnquiryForm" property="ratedOutputUnit" value="HP">
						
						<option value='HP'><bean:message key="quotation.option.hp"/></option>
						<option value='kW'><bean:message key="quotation.option.kw"/></option>
						</logic:equal>
						<logic:equal name="SearchEnquiryForm" property="ratedOutputUnit" value="">
						<option value='kW'><bean:message key="quotation.option.kw"/></option>
						<option value='HP'><bean:message key="quotation.option.hp"/></option>
						
						</logic:equal>
			</html:select>  
            </td>
            
            
             <td class="formLabel"><bean:message key="quotation.search.volts" /></td>
             <td>
           		<bean:define name="SearchEnquiryForm" property="alVoltsList" id="alVoltsList" type="java.util.Collection" /> 
              	<html:select property="volts" styleClass="long" styleId="volts" >
              		<option value=""><bean:message key="quotation.option.select"/></option>
                  	<html:options name="SearchEnquiryForm" collection="alVoltsList"
		              property="key" labelProperty="value" />
               	</html:select>
             </td>
           
           
            
         
          </tr>
          <tr>
          	<td class="formLabel">
                    	<bean:message key="quotation.create.label.enclosure"/>  </td>
             <td>
             	<bean:define name="SearchEnquiryForm" property="alEnclosureList"
         	             id="alEnclosureList" type="java.util.Collection" /> 
            	<html:select property="enclosureId" styleClass="long" styleId="enclosureId" tabindex="31">
            		<option value=""><bean:message key="quotation.option.select"/></option>
                	<html:options name="SearchEnquiryForm" collection="alEnclosureList"
              property="key" labelProperty="value" />
             	</html:select>
           	 </td>
          <td class="formLabel"><bean:message key="quotation.search.mounting" /></td>
          <td>
            <html:select name="SearchEnquiryForm" property="mounting" styleId="mounting" styleClass="long"  >
					<html:option value=""><bean:message key="quotation.option.select" /></html:option>
					<logic:present name="SearchEnquiryForm" property="mountingList" scope="request">
						   	<bean:define name="SearchEnquiryForm" property="mountingList" id="mountingList" type="java.util.Collection" />
							<html:options name="SearchEnquiryForm" collection="mountingList" property="key" labelProperty="value" />
						</logic:present>
				</html:select>
            
            
            </td>    
          
          
          </tr>
          
           <tr>
          	<td class="formLabel">
                    	<bean:message key="quotation.create.label.application"/>  </td>
             <td>
             	<bean:define name="SearchEnquiryForm" property="alApplicationList"
	                	             id="alApplicationList" type="java.util.Collection" /> 
               	<html:select property="applicationId" styleId="applicationId" styleClass="long" tabindex="18">
               		<option value=""><bean:message key="quotation.option.select"/></option>
                    <html:options name="SearchEnquiryForm" collection="alApplicationList"
			              property="key" labelProperty="value" />
   	            </html:select>
           	 </td>
          <td class="formLabel"><bean:message key="quotation.create.label.industry" /></td>
          <td>
            <bean:define name="SearchEnquiryForm" property="alIndustryList" id="alIndustryList" type="java.util.Collection" /> 
           	<html:select name="SearchEnquiryForm" property="endUserIndustry" styleClass="long" styleId="endUserIndustry" >
           		<option value=""><bean:message key="quotation.option.select"/></option>
              	 <html:options name="SearchEnquiryForm" collection="alIndustryList" property="key" labelProperty="value" />
            </html:select>   
            
            
            </td>    
          
          
          </tr>
           <tr>
          	<td class="formLabel">
                    	<bean:message key="quotation.create.label.projectname"/>  </td>
             <td>
             	  <html:text property="projectName" styleId="projectName" styleClass="long" maxlength="30" /></td>
           	
         <td class="formLabel">
				    	<bean:message key="quotation.search.orderclosedbetween"/>				    	
				    </td>
				    <td>				    
				    <input type="text" id="orderClsStartDate" class="readonlymini" name="orderClsStartDate" size="30" readonly="true" /><html:img src="html/images/popupCalen.gif" styleClass="hand"   width="19" height="22" border="0" align="absmiddle" onclick="return showCalendar('orderClsStartDate', '%m/%d/%Y', '24', true);" /> To <input type="text" class="readonlymini" id="orderClsEndDate" name="orderClsEndDate" size="30" readonly="true"/><html:img src="html/images/popupCalen.gif" styleClass="hand"   width="19" height="22" border="0" align="absmiddle" onclick="return showCalendar('orderClsEndDate', '%m/%d/%Y', '24', true);" /> </td>
			</tr>
          
           <tr>
          		<td class="formLabel">
                    	<bean:message key="quotation.create.label.indentno"/>  </td>
             	<td> <html:text property="savingIndent" styleId="savingIndent" styleClass="long" /></td>
           	 
        	<td class="formLabel"><bean:message key="quotation.create.label.enquiryreferencenumber"/></td>
          	<td> <html:text  property="enquiryReferenceNumber" styleClass="long"   styleId="enquiryReferenceNumber" maxlength="30"/></td>           
          </tr>
          
          
          <tr>
          <td></td>
          <td></td>
          <td></td>
          <td class="formLabel" >Search Type&nbsp;</td>
           <td>
           <html:radio name="SearchEnquiryForm" property="searchType" value="E" />Enquiry&nbsp;
           <html:radio name="SearchEnquiryForm"   property="searchType" value="R" />Ratings
           </td>
          </tr>
          
        </table>
        <!-------------------------------------- search block end --------------------------->
		  <div class="blue-light" align="right">
        <html:button property="reset" onclick="clearEnquirySearch();" styleClass="BUTTON" value="Clear"/>
        <html:submit property="Search" styleClass="BUTTON" value="Search"/>
      </div>
        <!--------------------------------------buttons block start --------------------------->
        </fieldset>
        
        <!-- fieldset Ending -->
      <br>
      <!-------------------------------------- buttons Block End ------------------------->
	  	<!-------------------------- Search Results Block Start --------------------------------->
	  	<div style="width:100%; float:left;">
	  	
	<logic:present name="searchAction" scope="request">
      <div class="sectiontitle"><bean:message key="quotation.search.searchresults" /></div>    	
    
    	<%-- 
    		Listing taglib's hidden fields tag that includes the necessary hidden fields
			related to pagination, at runtime
	 	--%>
	  	<listing:hiddenfields />
		<%-- Displaying 'No Results Found' message if size of list is zero --%>
		<logic:equal name="sizeOfList" value="0">
			<table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#F7F7F7">
				<tr class="other" align="center">
					<td><bean:message key="quotation.messages.noresultsfound"/></td>
				</tr>
			</table>
	    </logic:equal>
		<logic:notEqual name="sizeOfList" value="0">
			<logic:notPresent name="oSearchEnquiryDto" property="searchResultsList" scope="request">
				<table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#F7F7F7">
					<tr align="center" class="other">
						<td><bean:message key="quotation.messages.noresultsfound"/></td>
					</tr>
					</table>
				</logic:notPresent>	
			
			<logic:present name="oSearchEnquiryDto" property="searchResultsList" scope="request">
				<logic:empty name="oSearchEnquiryDto" property="searchResultsList">
					<table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="#F7F7F7">
						<tr align="center" class="other">
								<td><bean:message key="quotation.messages.noresultsfound"/></td>
						</tr>
					</table>
				</logic:empty>
				<br>
				<logic:notEmpty name="oSearchEnquiryDto" property="searchResultsList">
					<table width="100%" cellpadding="0" cellspacing="0" border="0" class="blue-light">
				       <tr>
							<%-- 
							Listing taglib's listingsummary field tag, which displays the
							summary of records retrieved from database and those being
							displayed in this page
							 --%>
							<td align="left" bgcolor="#CCD8F2"><listing:listsummary /></td>
										
							<%-- 
							Listing taglib's pagelist field tag, which displays the list 
							of pages in dropdown selecting which, user will be redirected 
							to the respective page
							 --%>
							<td align="right" bgcolor="#CCD8F2"><listing:pagelist navigation="1" /></td>
					  </tr>
				  </table>	
					  <br>
						
      <!-------------------------------------- search results block start --------------------------->
     <div style="width:1256px; overflow-x:scroll;">
     
      <table width="100%" cellpadding="2" cellspacing="0" id="table0" >
        <tr id="top">
          <td>
          	<listing:sort dbfieldid="1" >
          		<bean:message key="quotation.search.sno" />
			</listing:sort>		
          </td>          		
          <td>
          	<listing:sort dbfieldid="2" >
          		<bean:message key="quotation.search.year" />
			</listing:sort>		
          </td> 
          <td>
          	<listing:sort dbfieldid="3" >
          		<bean:message key="quotation.search.month" />
			</listing:sort>		
          </td> 
          <td>
         <listing:sort dbfieldid="4" >
          		<bean:message key="quotation.search.region" />
			</listing:sort>		
          </td>   
          <td>
         	<listing:sort dbfieldid="5" >
          		<bean:message key="quotation.search.salesmanager" />
			</listing:sort>		
          </td>          		
          <td>
         	<listing:sort dbfieldid="6" >
          		<bean:message key="quotation.search.enquirytype" />
			</listing:sort>		
          </td>          		
          <td>
         	<listing:sort dbfieldid="7" >
          		<bean:message key="quotation.search.customertype" />
			</listing:sort>		
          </td> 
          <td>
         	<listing:sort dbfieldid="8" >
          		<bean:message key="quotation.create.label.customer" />
			</listing:sort>		
          </td> 
          <td>
         	<listing:sort dbfieldid="9" >
          		<bean:message key="quotation.create.label.projectname" />
			</listing:sort>		
          </td> 
          <td>
          	<listing:sort dbfieldid="10" >
          		<bean:message key="quotation.search.rfq" />
			</listing:sort>		
          </td>  
          <td>
          	<listing:sort dbfieldid="11" >
          		<bean:message key="quotation.create.label.enquiryreferencenumber" />
			</listing:sort>		
          </td> 
          <td>
          	<listing:sort dbfieldid="12" >
          		<bean:message key="quotation.create.label.enduserindustry" />
			</listing:sort>		
          </td>
          <td>
          	<listing:sort dbfieldid="13" >
          		<bean:message key="quotation.create.label.enduser" />
			</listing:sort>		
          </td>
          <td>
          	<listing:sort dbfieldid="14" >
          		<bean:message key="quotation.create.label.orderclosingmonth" />
			</listing:sort>		
          </td>
          <td>
          	<listing:sort dbfieldid="15" >
          		<bean:message key="quotation.create.label.targeted" />
			</listing:sort>		
          </td>   
          <td>
          	<listing:sort dbfieldid="16" >
          		<bean:message key="quotation.create.label.winningchance" />
			</listing:sort>		
          </td> 
          <td>
          	<listing:sort dbfieldid="17" >
          		<bean:message key="quotation.create.label.deliveriestype" />
			</listing:sort>		
          </td> 
          <td>
          	<listing:sort dbfieldid="18" >
          		<bean:message key="quotation.create.label.expecteddeliverymonth" />
			</listing:sort>		
          </td>                  		
          <td  >
         	 <listing:sort dbfieldid="20" >
          		 <bean:message key="quotation.search.location" />
			</listing:sort>		
          </td>          		 
          <td nowrap="nowrap"  > 
         	 <listing:sort dbfieldid="21" >
          		<bean:message key="quotation.create.label.createddate" />
			</listing:sort>		
          </td>          		
          <td  >
         	 <listing:sort dbfieldid="22" >
         		 <bean:message key="quotation.search.createdby" />
			</listing:sort>		
          </td> 
          <td>
         	 <listing:sort dbfieldid="23" >
         		 <bean:message key="quotation.search.designedby" />
			</listing:sort>		
          </td>         		 
          <td>
         	 <listing:sort dbfieldid="24" >
         		 <bean:message key="quotation.label.admin.update.de" />
			</listing:sort>		
          </td>         		 
          <td>
         	 <listing:sort dbfieldid="25" >
         		 <bean:message key="quotation.home.colheading.assignto" />
			</listing:sort>		
          </td>   
          <td>
         	 <listing:sort dbfieldid="26" >
         		 <bean:message key="quotation.create.label.ld" />
			</listing:sort>		
          </td> 
          <td>
         	 <listing:sort dbfieldid="27" >
         		 <bean:message key="quotation.create.label.approval" />
			</listing:sort>		
          </td>   
          <td >
          	<listing:sort dbfieldid="28" >
          		<bean:message key="quotation.search.status"/>
			</listing:sort>		
          </td>    
          <td>
          	<listing:sort dbfieldid="29" >
          		<bean:message key="quotation.search.frame" />
			</listing:sort>		
		  </td>
          <td>
          	<listing:sort dbfieldid="30" >
          		<bean:message key="quotation.search.pole" />
			</listing:sort>		
		  </td>
          <td>
          	<listing:sort dbfieldid="31" >
          		<bean:message key="quotation.search.kw" />
          	</listing:sort>		
          </td>
          <td>
          	<listing:sort dbfieldid="32" >
          		<bean:message key="quotation.search.volts" />
          	</listing:sort>		
          </td>
          <td>
          	<listing:sort dbfieldid="33" >
          		<bean:message key="quotation.search.mounting" />
          	</listing:sort>		
          </td>
          <td>
          	<listing:sort dbfieldid="34" >
          		<bean:message key="quotation.create.label.scsr" />
          	</listing:sort>		
          </td>
          <td>
          	<listing:sort dbfieldid="35" >
          		<bean:message key="quotation.create.label.enclosure" />
          	</listing:sort>		
          </td>
          <td >          
          	<listing:sort dbfieldid="36" >
          		<bean:message key="quotation.create.label.application" />
          	</listing:sort>          
          </td>
          <td >          
          	<listing:sort dbfieldid="37" >
          		<bean:message key="quotation.create.label.specialfeatures" />
          	</listing:sort>          
          </td>
          <td width="10%">          
          	<listing:sort dbfieldid="38" >
          		<bean:message key="quotation.create.label.smnote" />
          	</listing:sort>          
          </td>
          <td>
          	<listing:sort dbfieldid="39" >
          		<bean:message key="quotation.search.quotatedprice" />
			</listing:sort>		
          </td>  
          <td>
          	<listing:sort dbfieldid="40" >
          		<bean:message key="quotation.search.orderprice" />
			</listing:sort>		
          </td> 
          <td>
          	<listing:sort dbfieldid="41" >
          		<bean:message key="quotation.create.label.winreason" />
			</listing:sort>		
          </td>  
          <td>
          	<listing:sort dbfieldid="42" >
          		<bean:message key="quotation.create.label.lossreason1" />
			</listing:sort>		
          </td> 
          <td>
          	<listing:sort dbfieldid="43" >
          		<bean:message key="quotation.create.label.lossreason2" />
			</listing:sort>		
          </td> 
 		  <td>
          	<listing:sort dbfieldid="44" >
          		<bean:message key="quotation.create.label.competitor" />
			</listing:sort>		
          </td> 
          <td>
          	<listing:sort dbfieldid="45" >
          		<bean:message key="quotation.viewenquiry.return.label.comments" />
			</listing:sort>		
          </td> 
          <td>
          	<listing:sort dbfieldid="46" >
          		<bean:message key="quotation.create.label.wfd" />
			</listing:sort>		
          </td> 
          <td>
          	<listing:sort dbfieldid="47" >
          		<bean:message key="quotation.create.label.wfc" />
			</listing:sort>		
          </td> 
          <td>
          	<listing:sort dbfieldid="48" >
          		<bean:message key="quotation.create.label.indentno" />
			</listing:sort>		
          </td> 
          <logic:present name="salesManager" scope="request">
          	<td  style="width:5% "><bean:message key="quotation.search.copy" /></td>
            <td  style="width:5% " align="center"><bean:message key="quotation.search.edit" /></td>
            <td  style="width:5% " align="center"><bean:message key="quotation.search.delete" /></td>
       	  </logic:present>
        </tr>
        <logic:iterate name="resultsList" scope="request" id="searchEnquiry" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.SearchEnquiryDto">
			
	    <% if(idx.intValue() % 2 == 0) { %>
			<tr class="light" id='item<bean:write name="idx"/>'" onmouseover="Change(this.id)" onMouseOut="ChangeBack(this.id)">
		<% } else { %>
			<tr class="dark" id='item<bean:write name="idx"/>' onmouseover="Change(this.id)" onMouseOut="ChangeBack(this.id)">
   		<% } %>	
	<!-- Newly Added Data Starts on 20-March-2019 -->
	    <td><%=idx.intValue()+1 %>&nbsp;</td>
	   <td><bean:write name="searchEnquiry" property="year"/>&nbsp;</td>
	   <td><bean:write name="searchEnquiry" property="month"/>&nbsp;</td>
	   <td><bean:write name="searchEnquiry" property="regionName"/>&nbsp;</td>
	   <td><bean:write name="searchEnquiry" property="salesManagerName"/>&nbsp;</td>
	   <td><bean:write name="searchEnquiry" property="enquiryType"/>&nbsp;</td>
	   <td><bean:write name="searchEnquiry" property="customerType"/>&nbsp;</td>
	   <td><bean:write name="searchEnquiry" property="customerName"/>&nbsp;</td>
	   <td><bean:write name="searchEnquiry" property="projectName"/>&nbsp;</td>
	   <!-- Newly Added Data End -->
		<td><a href="javaScript:viewSearchEnquiryDetails('<bean:write name="searchEnquiry" property="enquiryId"/>','<bean:write name="searchEnquiry" property="statusId"/>','<%=idx.intValue()%>','<bean:write name="searchEnquiry" property="isNewEnquiry"/>');"><bean:write name="searchEnquiry" property="enquiryNumber"/>&nbsp;</a></td>
       <td><bean:write name="searchEnquiry" property="enquiryReferenceNumber"/>&nbsp;</td>
       <td><bean:write name="searchEnquiry" property="endUserIndustry"/>&nbsp;</td>
        <td><bean:write name="searchEnquiry" property="endUserName"/>&nbsp;</td>
       <td><bean:write name="searchEnquiry" property="orderClosedDate"/>&nbsp;</td>
       <td><bean:write name="searchEnquiry" property="targeted"/>&nbsp;</td>
       <td><bean:write name="searchEnquiry" property="winChance"/>&nbsp;</td>
       <td><bean:write name="searchEnquiry" property="deliveryType"/>&nbsp;</td>
       <td><bean:write name="searchEnquiry" property="exDeliveryDate"/>&nbsp;</td>
       <!-- Delivery Type -->        
		<td><bean:write name="searchEnquiry" property="location"/>&nbsp;</td>
		<td><bean:write name="searchEnquiry" property="createdDate"/>&nbsp;</td>
        <td><bean:write name="searchEnquiry" property="createdBy"/>&nbsp;</td>
        <td><bean:write name="searchEnquiry" property="designedBy"/>&nbsp;</td>
        <td><bean:write name="searchEnquiry" property="designEngineerName"/>&nbsp;</td>
        
        <td><bean:write name="searchEnquiry" property="assignTo"/>&nbsp;</td>
        <td><bean:write name="searchEnquiry" property="ld"/>&nbsp;</td>
        <td><bean:write name="searchEnquiry" property="approval"/>&nbsp;</td>
		<td><bean:write name="searchEnquiry" property="statusName"/>&nbsp;</td>
		<td><bean:write name="searchEnquiry" property="frameName"/>&nbsp;</td>
		<td><bean:write name="searchEnquiry" property="poleName"/>&nbsp;</td>
        <td><bean:write name="searchEnquiry" property="kw"/>&nbsp;</td>
		<td><bean:write name="searchEnquiry" property="volts"/>&nbsp;</td>
        <td><bean:write name="searchEnquiry" property="mounting"/>&nbsp;</td>
       <td><bean:write name="searchEnquiry" property="typeDesc"/>&nbsp;</td>
       <td><bean:write name="searchEnquiry" property="enclosureDesc"/>&nbsp;</td>
       <td><bean:write name="searchEnquiry" property="applicationDesc"/>&nbsp;</td>
       <td><bean:write name="searchEnquiry" property="splFeatures"/>&nbsp;</td>
       <td><bean:write name="searchEnquiry" property="smnote"/>&nbsp;</td>
		<!-- Extra Data Added By Egr on 22-Mar-2019 -->
		<logic:present name="quotedFlag" scope="request">
		<td><bean:write name="searchEnquiry" property="totalMotorPrice"/>&nbsp;</td>
		<td><bean:write name="searchEnquiry" property="totalOrderPrice"/>&nbsp;</td>
		</logic:present>
		<td><bean:write name="searchEnquiry" property="winReason"/>&nbsp;</td>
		<td><bean:write name="searchEnquiry" property="lossReason1"/>&nbsp;</td>
		<td><bean:write name="searchEnquiry" property="lossReason2"/>&nbsp;</td>
		<td><bean:write name="searchEnquiry" property="competitor"/>&nbsp;</td>
		<td><bean:write name="searchEnquiry" property="lossComment"/>&nbsp;</td>
		<td><bean:write name="searchEnquiry" property="warrantyFromDispatch"/>&nbsp;</td>
		<td><bean:write name="searchEnquiry" property="warrantyFromComm"/>&nbsp;</td>
	    <td><bean:write name="searchEnquiry" property="savingIndent"/>&nbsp;</td>
		
		<logic:present name="salesManager" scope="request">
		<td><div align="center"><a href="javascript:copyEnquiry('<bean:write name="searchEnquiry" property="enquiryId"/>','<bean:write name="searchEnquiry" property="isNewEnquiry"/>');"><img src="html/images/copy.gif" alt="Copy" width="9" height="9" border="0" ></a></div></td>
		
		<logic:equal name="searchEnquiry" property="statusId" value="1">
			<logic:equal name="userSSO" scope="request" value="<%=searchEnquiry.getCreatedBySSO()%>">
				<td>
					<div align="center"><a href="javascript:editEnquiry('<bean:write name="searchEnquiry" property="enquiryId"/>');"><img src="html/images/edit.gif" alt="Edit" width="20" height="18" border="0" ></a></div>
				</td>
				<td>
					<div align="center"><a href="javascript:deleteEnquiry('<bean:write name="searchEnquiry" property="enquiryId"/>');"><img src="html/images/deleteicon.gif" alt="Terminate" width="15" height="16" border="0" ></a></div>
				</td>
			</logic:equal>
			<logic:notEqual name="userSSO" scope="request" value="<%=searchEnquiry.getCreatedBySSO()%>">
				<td>&nbsp;</td>	
				<td>&nbsp;</td>			
			</logic:notEqual>
		</logic:equal>
		<logic:notMatch name="searchEnquiry" property="statusId" value="1">
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</logic:notMatch>
	</logic:present>
     </tr>
</logic:iterate>	
      
      </table>
      
     </div>
      <!-- Table Ending -->
    
    <br>
    <table width="100%" border="0" class="blue-light" border="0" cellspacing="0" cellpadding="0">
				       <tr>
							<%-- 
							Listing taglib's listingsummary field tag, which displays the
							summary of records retrieved from database and those being
							displayed in this page
							 --%>
							<td align="left" bgcolor="#CCD8F2"><listing:listsummary /></td>
										
							<%-- 
							Listing taglib's pagelist field tag, which displays the list 
							of pages in dropdown selecting which, user will be redirected 
							to the respective page
							 --%>
							<td align="right" bgcolor="#CCD8F2"><listing:pagelist navigation="2" /></td>
				  </tr>
				  </table>	<br>
      <!-------------------------------------- search results block end --------------------------->
	   <!-------------------------- Bottum Input buttons block Start  --------------------------------->
      <div class="blue-light" align="right">
        <input name="print" type="button" class="BUTTON" onClick="javascript:printOrExportResults('print');"  value="Print">
        <input name="export" type="button" class="BUTTON" onClick="javascript:printOrExportResults('export');"  value="Export to Excel">
      </div>
	   <!-------------------------- Bottum Input buttons block End  --------------------------------->
	   	</logic:notEmpty>
		</logic:present>
		</logic:notEqual>
    </logic:present>
         </div>
         
         </html:form></td>
  </tr>
</table>
<script language="javascript">

	searchtdshowhidereload();
</script>
<script>
document.SearchEnquiryForm.createdStartDate.value='<bean:write name="SearchEnquiryForm" property="createdStartDate"/>';
document.SearchEnquiryForm.createdEndDate.value='<bean:write name="SearchEnquiryForm" property="createdEndDate"/>';
document.SearchEnquiryForm.searchType.value='<bean:write name="SearchEnquiryForm" property="searchType"/>';
document.SearchEnquiryForm.orderClsStartDate.value='<bean:write name="SearchEnquiryForm" property="orderClsStartDate"/>';
document.SearchEnquiryForm.orderClsEndDate.value='<bean:write name="SearchEnquiryForm" property="orderClsEndDate"/>';


</script>

<ajax:autocomplete
		fieldId="customerName"
		popupId="customer-popup"
		targetId="customerId"
		baseUrl="servlet/AutocompleteServlet?type=customer&valueType=id"
		paramName="customer"
		className="autocomplete"
		progressStyle="throbbing"
		postFunc="searchPostAutocomplete" />

	<iframe
		 id="divShim"
		src="javascript:false;"
		scrolling="no"
		frameborder="0"
		style="position:absolute; top:0px; left:0px; display:none;">
	</iframe>
	
	
		
		 


<!-------------------------- main table End  --------------------------------->

