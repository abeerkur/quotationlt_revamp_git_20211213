<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants,in.com.rbc.quotation.common.utility.PropertyUtility,in.com.rbc.quotation.common.utility.CommonUtility,in.com.rbc.quotation.common.vo.KeyValueVo" %>
<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>

<table id="sticky">
	<tbody>
		<tr class="linked" style="width:1174px !important; float:left; overflow-x: scroll;">
			<td valign="top">
				<table id="tableR" align="center" cellspacing="0" cellpadding="1">
					<%	String sTextStyleClass = "textnoborder-light"; %>
					<% for(int i = 0; i <= 11; i++) {  %>
						<% if(i == 0) { %>
							<tr id="top">
								<td width="200px" class="bordertd" style="float:left; left: 0px;">&nbsp;</td>
								<%  int idRow1 = 1;  %>
								<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
								<input type="hidden" id="ratingidx<%=idRow1%>" value='<%=idRow1%>' >
								<input type="hidden" id="ratingObjId<%=idRow1%>"  name="ratingObjId<%=idRow1%>"  value='<bean:write name="ratingObj" property="ratingId" />' >
								<input type="hidden" id="ratingNo<%=idRow1%>"  name="ratingNo<%=idRow1%>"  value='<bean:write name="ratingObj" property="ratingNo" />' >
								<input type="hidden" id="ratingMTOHighlightFlag<%=idRow1%>"  name="ratingMTOHighlightFlag<%=idRow1%>"  value='<bean:write name="ratingObj" property="mtoHighlightFlag" />' >
								<td class="bordertd">
									&nbsp;&nbsp;
									<label class="normal" style="text-align:left !important;"><strong> Rating <%=idRow1%> </strong></label>
								</td>
								<%	idRow1 = idRow1+1; %>
								</logic:iterate>
							</tr>
						<% } else if(i == 1) { %>
							<tr id='item<%=i%>'>
							<% if(i % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd" > <strong>Model Number</strong> <span class="mandatory">&nbsp;</span> </td>
							<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd" > <strong>Model Number</strong> <span class="mandatory">&nbsp;</span> </td>
							<% } %>
								
								<%  int idRow2 = 1;  %>
								<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
								<td class="bordertd" >
									<input type="text" name="modelNumber<%=idRow2%>" id="modelNumber<%=idRow2%>" class="normal" style="width: 100px;" value='<bean:write name="ratingObj" property="quantity" filter="false" />' onfocus="javascript:processAutoSave('<%=idRow2%>');" title="Model Number" >
								</td>
								<%	idRow2 = idRow2+1; %>
								</logic:iterate>
							</tr>
						<% } else if(i == 2) { %>
							<tr id='item<%=i%>'>
							<% if(i % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd"> <strong>Application</strong> <span class="mandatory">&nbsp;</span> </td>
							<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd"> <strong>Application</strong> <span class="mandatory">&nbsp;</span> </td>
							<% } %>
								
								<%  int idRow4 = 1;  %>
								<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
								<td id="td_application<%=idRow4%>" class="bordertd">
									<select style="width:100px;" name="application<%=idRow4%>" id="application<%=idRow4%>" title="Application" onfocus="javascript:processAutoSave('<%=idRow4%>');" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present property="ltApplicationList" name="newmtoenquiryForm">
											<bean:define name="newmtoenquiryForm" property="ltApplicationList" id="ltApplicationList" type="java.util.Collection" />
											<logic:iterate name="ltApplicationList" id="ltApplication" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltApplication" property="key" />' > <bean:write name="ltApplication" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
								<script> selectLstItemById('application<%=idRow4%>', '<bean:write name="ratingObj" property="ltApplicationId" filter="false" />'); </script>
								<%	idRow4 = idRow4+1; %>
								</logic:iterate>
							</tr>
						<% } else if(i == 3) { %>
							<tr id='item<%=i%>'>
							<% if(i % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd"> <strong>Quantity</strong> <span class="mandatory">&nbsp;</span> </td>
							<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd"> <strong>Quantity</strong> <span class="mandatory">&nbsp;</span> </td>
							<% } %>
								
								<%  int idRow4 = 1;  %>
								<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
								<td class="bordertd" >
									<input type="text" name="quantity<%=idRow4%>" id="quantity<%=idRow4%>" class="normal" style="width: 100px;" value='<bean:write name="ratingObj" property="quantity" filter="false" />' onfocus="javascript:processAutoSave('<%=idRow4%>');" title="Quantity" >
								</td>
								<%	idRow4 = idRow4+1; %>
								</logic:iterate>
							</tr>
													
						<% } else if(i == 4) { %>
							<tr id='item<%=i%>'>
							<% if(i % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd"> <strong>Product Line</strong> <span class="mandatory">&nbsp;</span> </td>
							<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd"> <strong>Product Line</strong> <span class="mandatory">&nbsp;</span> </td>
							<% } %>
								
								<%  int idRow5 = 1;  %>
								<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
								<td id="td_productLine<%=idRow5%>" class="bordertd">
									<select style="width: 100px;" id="productLine<%=idRow5%>" name="productLine<%=idRow5%>" title="Product Line" onfocus="javascript:processAutoSave('<%=idRow5%>');" onchange="javascript:processFieldsForProductLine('<%=idRow5%>');">
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present property="ltProductLineList" name="newmtoenquiryForm">
											<bean:define name="newmtoenquiryForm" property="ltProductLineList" id="ltProductLineList" type="java.util.Collection" />
											<logic:iterate name="ltProductLineList" id="ltProductLine" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltProductLine" property="key" />'> <bean:write name="ltProductLine" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
								<script> selectLstItemById('productLine<%=idRow5%>', '<bean:write name="ratingObj" property="ltProdLineId" filter="false" />'); </script>
								<%	idRow5 = idRow5+1; %>
								</logic:iterate>
							</tr>
							
						<% } else if(i == 5) { %>
							<tr id='item<%=i%>'>
							<% if(i % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd"> <strong>Power Rating(KW)</strong> <span class="mandatory">&nbsp;</span> </td>
							<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd"> <strong>Power Rating(KW)</strong> <span class="mandatory">&nbsp;</span> </td>
							<% } %>
								
								<%  int idRow6 = 1;  %>
								<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
								<td id="td_kw<%=idRow6%>" class="bordertd">
									<select style="width:100px;" id="kw<%=idRow6%>" name="kw<%=idRow6%>" title="Power Rating(KW)" onfocus="javascript:processAutoSave('<%=idRow6%>');" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present property="ltKWList" name="newmtoenquiryForm">
											<bean:define name="newmtoenquiryForm" property="ltKWList" id="ltKWList" type="java.util.Collection" />
											<logic:iterate name="ltKWList" id="ltKW" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltKW" property="key" />' > <bean:write name="ltKW" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
								<script> selectLstItemById('kw<%=idRow6%>', '<bean:write name="ratingObj" property="ltKWId" filter="false" />'); </script>
								<%	idRow6 = idRow6+1; %>
								</logic:iterate>
							</tr>
							
						<% } else if(i == 6) { %>
							<tr id='item<%=i%>'>
							<% if(i % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd"> <strong>No. of Poles</strong> <span class="mandatory">&nbsp;</span> </td>
							<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd"> <strong>No. of Poles</strong> <span class="mandatory">&nbsp;</span> </td>
							<% } %>
								
								<%  int idRow7 = 1;  %>
								<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
								<td id="td_pole<%=idRow7%>" class="bordertd">
									<select style="width:100px;" id="pole<%=idRow7%>" name="pole<%=idRow7%>" title="No. of Poles" onfocus="javascript:processAutoSave('<%=idRow7%>');" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present property="ltPoleList" name="newmtoenquiryForm">
											<bean:define name="newmtoenquiryForm" property="ltPoleList" id="ltPoleList" type="java.util.Collection" />
											<logic:iterate name="ltPoleList" id="ltPole" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltPole" property="key" />' > <bean:write name="ltPole" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
								<script> selectLstItemById('pole<%=idRow7%>', '<bean:write name="ratingObj" property="ltPoleId" filter="false" />'); </script>
								<%	idRow7 = idRow7+1; %>
								</logic:iterate>
							</tr>
						<% } else if(i == 7) { %>
							<tr id='item<%=i%>'>
							<% if(i % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd"> <strong>Frame size</strong> <span class="mandatory">&nbsp;</span> </td>
							<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd"> <strong>Frame size</strong> <span class="mandatory">&nbsp;</span> </td>
							<% } %>
								
								<%  int idRow8 = 1;  %>
								<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
								<td id="td_frame<%=idRow8%>" class="bordertd">
									<select style="width:100px;" id="frame<%=idRow8%>" name="frame<%=idRow8%>" title="Frame size" onfocus="javascript:processAutoSave('<%=idRow8%>');" onchange="javascript:processFieldsForFrame('<%=idRow8%>');" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present property="ltFrameList" name="newmtoenquiryForm">
											<bean:define name="newmtoenquiryForm" property="ltFrameList" id="ltFrameList" type="java.util.Collection" />
											<logic:iterate name="ltFrameList" id="ltFrame" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltFrame" property="key" />' > <bean:write name="ltFrame" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
								<script> selectLstItemById('frame<%=idRow8%>', '<bean:write name="ratingObj" property="ltFrameId" filter="false" />'); </script>
								<%	idRow8 = idRow8+1; %>
								</logic:iterate>
							</tr>
						<% } else if(i == 8) { %>
							<tr id='item<%=i%>'>
							<% if(i % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd"> <strong>Frame suffix</strong> <span class="mandatory">&nbsp;</span> </td>
							<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd"> <strong>Frame suffix</strong> <span class="mandatory">&nbsp;</span> </td>
							<% } %>
								
								<%  int idRow9 = 1;  %>
								<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
								<td id="td_framesuffix<%=idRow9%>" class="bordertd">
									<select style="width:100px;" id="framesuffix<%=idRow9%>" name="framesuffix<%=idRow9%>" title="Frame suffix" onfocus="javascript:processAutoSave('<%=idRow9%>');" onchange="javascript:processFieldsForFrame('<%=idRow9%>');" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present property="ltFrameSuffixList" name="newmtoenquiryForm">
											<bean:define name="newmtoenquiryForm" property="ltFrameSuffixList" id="ltFrameSuffixList" type="java.util.Collection" />
											<logic:iterate name="ltFrameSuffixList" id="ltFrameSuffix" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltFrameSuffix" property="key" />' > <bean:write name="ltFrameSuffix" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
								<script> selectLstItemById('framesuffix<%=idRow9%>', '<bean:write name="ratingObj" property="ltFrameSuffixId" filter="false" />'); </script>
								<%	idRow9 = idRow9+1; %>
								</logic:iterate>
							</tr>
						<% } else if(i == 9) { %>
							<tr id='item<%=i%>'>
							<% if(i % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd"> <strong>Mounting</strong> <span class="mandatory">&nbsp;</span> </td>
							<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd"> <strong>Mounting</strong> <span class="mandatory">&nbsp;</span> </td>
							<% } %>
								
								<%  int idRow10 = 1;  %>
								<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
								<td id="td_mounting<%=idRow10%>" class="bordertd">
									<select style="width:100px;" id="mounting<%=idRow10%>" name="mounting<%=idRow10%>" title="Mounting" onfocus="javascript:processAutoSave('<%=idRow10%>');" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present property="ltMountingList" name="newmtoenquiryForm">
											<bean:define name="newmtoenquiryForm" property="ltMountingList" id="ltMountingList" type="java.util.Collection" />
											<logic:iterate name="ltMountingList" id="ltMounting" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltMounting" property="key" />' > <bean:write name="ltMounting" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
								<script> selectLstItemById('mounting<%=idRow10%>', '<bean:write name="ratingObj" property="ltMountingId" filter="false" />'); </script>
								<%	idRow10 = idRow10+1; %>
								</logic:iterate>
							</tr>
						<% } else if(i == 10) { %>
							<tr id='item<%=i%>'>
							<% if(i % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd"> <strong>TB Position</strong> <span class="mandatory">&nbsp;</span> </td>
							<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd"> <strong>TB Position</strong> <span class="mandatory">&nbsp;</span> </td>
							<% } %>
								
								<%  int idRow11 = 1;  %>
								<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
								<td id="td_tbPosition<%=idRow11%>" class="bordertd">
									<select style="width:100px;" id="tbPosition<%=idRow11%>" name="tbPosition<%=idRow11%>" title="TB Position" onfocus="javascript:processAutoSave('<%=idRow11%>');" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present property="ltTBPositionList" name="newmtoenquiryForm">
											<bean:define name="newmtoenquiryForm" property="ltTBPositionList" id="ltTBPositionList" type="java.util.Collection" />
											<logic:iterate name="ltTBPositionList" id="ltTBPosition" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltTBPosition" property="key" />' > <bean:write name="ltTBPosition" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
								<script> selectLstItemById('tbPosition<%=idRow11%>', '<bean:write name="ratingObj" property="ltTBPositionId" filter="false" />'); </script>
								<%	idRow11 = idRow11+1; %>
								</logic:iterate>
							</tr>
						<% } else if(i == 11) { %>
							<tr id='item<%=i%>'>
							<% if(i % 2 == 0) { %>
							<% sTextStyleClass = "textnoborder-dark"; %>
							<td width="200px" class="dark bordertd"> <strong>Efficiency Class</strong> <span class="mandatory">&nbsp;</span> </td>
							<% } else { %>
							<% sTextStyleClass = "textnoborder-light"; %>
							<td width="200px" class="light bordertd"> <strong>Efficiency Class</strong> <span class="mandatory">&nbsp;</span> </td>
							<% } %>
								
								<%  int idRow12 = 1;  %>
								<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
								<td class="bordertd">
									<select style="width:100px;" name="effClass<%=idRow12%>" id="effClass<%=idRow12%>" title="Efficiency Class" onfocus="javascript:processAutoSave('<%=idRow12%>');">
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present property="ltEfficiencyClassList" name="newmtoenquiryForm">
											<bean:define name="newmtoenquiryForm" property="ltEfficiencyClassList" id="ltEfficiencyClassList" type="java.util.Collection" />
											<logic:iterate name="ltEfficiencyClassList" id="ltEfficiencyClass" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltEfficiencyClass" property="key" />' > <bean:write name="ltEfficiencyClass" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
								<script> selectLstItemById('effClass<%=idRow12%>', '<bean:write name="ratingObj" property="ltEffClass" filter="false" />'); </script>
								<%	idRow12 = idRow12+1; %>
								</logic:iterate>
							</tr>
						<% } %>
					<% } %>
				</table>
			</td>
		</tr>
	</tbody>
</table>