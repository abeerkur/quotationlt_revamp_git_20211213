<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>
<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants" %>
<%@ page import="in.com.rbc.quotation.common.utility.PropertyUtility" %>
<%@ page import="in.com.rbc.quotation.common.utility.CommonUtility" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<html:form action="/newViewEnquiry" method="POST" >

	<html:hidden property="enquiryId" styleId="enquiryId" />
		
	<table width="80%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
		<tr>
			<td>
				<div class="sectiontitle">
					<logic:present name="pageHeadingType" scope="request">
						<logic:equal name="pageHeadingType" value="<%= QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_CE %>">
							<bean:message key="quotation.viewenquiry.estimate.label.heading"/> 		
						</logic:equal>
						<logic:equal name="pageHeadingType" value="<%= QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_RSM %>">
							<bean:message key="quotation.refactor.label.heading"/> 		
						</logic:equal>
						<logic:equal name="pageHeadingType" value="view">
							<bean:message key="quotation.viewindent.label.heading"/> 		
						</logic:equal>
					</logic:present>
		  		</div>
		  		
		  		<logic:present name="displayPaging" scope="request">
					<table align="right">
						<tr>
							<td>
								<fieldset  style="width:100%">
									<table cellpadding="0" cellspacing="2" border="0">
										<tr>
						            		<td style="text-align:center">
	      										<a href="searchenquiry.do?invoke=viewSearchEnquiryResults&dispatch=fromViewPage" class="returnsearch">
	      											Return to search<img src="html/images/back.gif" border="0" align="absmiddle" />
	      										</a>
	      									</td>
						        		</tr>
						        		<tr class="other" align="right">
								            <td align="right">
									            <logic:present name="previousId" scope="request">	
									            	<bean:define name="previousId" id="previousId" scope="request"/>
													<bean:define name="previousIndex" id="previousIndex" scope="request"/>	
													<bean:define name="previousStatus" id="previousStatus" scope="request"/>				
									            	<a href="createEnquiry.do?invoke=validateRequestAction&enquiryId=<bean:write name="previousId" scope="request"/>&statusId=<bean:write name="previousStatus" scope="request"/>&index=<bean:write name="previousIndex" scope="request"/>"><img src="html/images/PrevPage.gif" alt=""  border="0" align="absmiddle" /></a>
									            </logic:present>
									            <logic:present name="displayMessage" scope="request">
									            	<bean:write name="displayMessage" scope="request"/>
									            </logic:present>
												<logic:present name="nextId" scope="request">
									            	<a href="createEnquiry.do?invoke=validateRequestAction&enquiryId=<bean:write name="nextId" scope="request"/>&statusId=<bean:write name="nextStatus" scope="request"/>&index=<bean:write name="nextIndex" scope="request"/>"><img src="html/images/NextPage.gif" alt="Next Request"  border="0" align="absmiddle" /></a>
									            </logic:present> 
												
											</td>
								       	</tr>
									</table>
								</fieldset>
							</td>
						</tr>
					</table>
				</logic:present>
			</td>
		</tr>
		
		<tr>
			<td height="100%" valign="top">
				<script language="javascript" src="../html/js/tooltip.js"></script>
				
				<logic:present name="newEnquiryDto" scope="request">
				
					<table width="100%" border="0" cellpadding="0" cellspacing="3">
						<tr>
       						<td rowspan="2" >
       							<table width="98%"  border="0" cellspacing="2" cellpadding="0" class="DotsTable">
						           <tr>
						             	<td rowspan="2" style="width:70px "><img src="html/images/icon_quote.gif" width="52" height="50"></td>
						             	<td class="other"><bean:message key="quotation.viewenquiry.label.subheading1"/> <bean:write name="newEnquiryDto" property="enquiryNumber"/><br>
						            		<bean:message key="quotation.viewenquiry.label.subheading2"/> 
						            	</td>
						           </tr>
						           <tr>
						           		<td class="other">
						           		<table width="100%">
						                	<tr>
						                   		<td height="16" class="section-head" align="center">
						                   			<bean:message key="quotation.viewenquiry.label.status"/>  <bean:write name="newEnquiryDto" property="statusName"/> 
						                   		</td>
						                 	</tr>
						             	</table>
						             	</td>
						           </tr>
       							</table>
       						</td>
       						<logic:lessThan name="newenquiryForm" property="statusId" value="6"> 
	       						<td colspan="2" align="right" >
		   							<table border="0" cellspacing="0" cellpadding="0" style="width:auto">
									  <tr>
										<td  class="other" align="right">
										<bean:message key="quotation.attachments.message"/>
										</td>
										<td>
										<a href="javascript:manageAttachments('<%= CommonUtility.getIntValue(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_ATTACHMENT_APPLICATIONID)) %>','<bean:write name="newenquiryForm" property="enquiryId"/>', '<bean:write name="newenquiryForm" property="createdBySSO"/>');"><img src="html/images/ico_attach.gif" alt="Manage Attachments" border="0" align="right"></a>
										</td>
									  </tr>
									</table>
    	   						</td>
       						</logic:lessThan>
       						<logic:present name="attachmentList" scope="request">
       							<logic:notEmpty name="attachmentList">
              						<logic:equal name="newenquiryForm" property="statusId" value="6"> 
	       								<td colspan="2" align="right" >
		   									<table border="0" cellspacing="0" cellpadding="0" style="width:auto">
			  									<tr>
													<td  class="other" align="right"  colspan="2"><bean:message key="quotation.attachments.enquirydocument"/></td>
			  									</tr>
											</table>
    	   								</td>
       								</logic:equal>
       							</logic:notEmpty>
       						</logic:present>
						</tr>
						
						<tr>
       						<td  style="width:45% ">
       							<logic:present name="attachmentList" scope="request">
       								<logic:notEmpty name="attachmentList">
								       <fieldset>
								         <table width="100%" class="other">
								           <logic:iterate name="attachmentList" id="attachmentData" indexId="idx" type="in.com.rbc.quotation.common.vo.KeyValueVo">	
											<tr><td>
												<a href="javascript:download('<bean:write name="attachmentData" property="key"/>', '<bean:write name="attachmentData" property="value"/>', '<bean:write name="attachmentData" property="value2"/>');">
													<bean:write name="attachmentData" property="value"/>.<bean:write name="attachmentData" property="value2"/>
												</a>
											</td></tr>					
										   </logic:iterate>
								         </table>
								       </fieldset>
        							</logic:notEmpty>
        						</logic:present>
      						</td>
     					</tr>
     					
     					<tr>
     						<td colspan="2" align="right" > 
        						<!-- DM Uploading Files  -->
           						<logic:lessThan name="newenquiryForm" property="statusId" value="6"> 
            						<table>
            							<tr>
	        								<td class="other" align="right"> <bean:message key="quotation.attachments.dmuploadedmessage"/></td>
    	    								<td class="other" align="right">
    	    									<a href="javascript:manageAttachments('<%= CommonUtility.getIntValue(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_DMATTACHMENT_APPLICATIONID)) %>','<bean:write name="newenquiryForm" property="enquiryId"/>', '<bean:write name="newenquiryForm" property="createdBySSO"/>');"><img src="html/images/ico_attach.gif" alt="Manage Attachments" width="23" height="25" border="0" align="right"></a>
    	    								</td>
            							</tr>
            						</table>
        						</logic:lessThan>
       							<logic:present name="dmattachmentList" scope="request">
       								<logic:notEmpty name="dmattachmentList">
      									<logic:equal name="newenquiryForm" property="statusId" value="6"> 
            								<table>
            									<tr>
	        										<td class="other" align="right" colspan="2"><bean:message key="quotation.attachments.designdocument"/></td>
            									</tr>
            								</table>
        								</logic:equal>
        							</logic:notEmpty>
        						</logic:present>
      						</td>
      					</tr>
      					
      					<tr>
           					<td>   &nbsp;   </td>
           					<td colspan="2" align="right" style="width:45% "> 
                       			<logic:present name="dmattachmentList" scope="request">
            						<logic:notEmpty name="dmattachmentList">
							            <fieldset>
							              <table width="100%" class="other">
							              <logic:iterate name="dmattachmentList" id="attachmentData" indexId="idx" type="in.com.rbc.quotation.common.vo.KeyValueVo">	
											<tr><td>
												<a href="javascript:download('<bean:write name="attachmentData" property="key"/>', '<bean:write name="attachmentData" property="value"/>', '<bean:write name="attachmentData" property="value2"/>');">
													<bean:write name="attachmentData" property="value"/>.<bean:write name="attachmentData" property="value2"/>
												</a>
											</td></tr>					
											</logic:iterate>
							              </table>
							        	</fieldset>
        							</logic:notEmpty>
        						</logic:present>
           					</td>
           				</tr> 
     					
     					<!-- Indent Page Uploads Starts Here -->
     					<tr>
     						<td colspan="2" align="right" > 
					            <logic:equal name="newenquiryForm" property="statusId" value="6"> 
					            <table>
					            	<tr>
						        		<td class="other" align="right"><bean:message key="quotation.attachments.indentuploadedmessage"/></td>
					    	    		<td class="other" align="left"><a href="javascript:manageAttachments('<%= CommonUtility.getIntValue(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_INTENDATTACHMENT_APPLICATIONID)) %>','<bean:write name="newenquiryForm" property="enquiryId"/>', '<bean:write name="newenquiryForm" property="createdBySSO"/>');"><img src="html/images/ico_attach.gif" alt="Manage Attachments" width="23" height="25" border="0" align="right"></a></td>
					            	</tr>
					            </table>
					        	</logic:equal>
      						</td>
      					</tr>
      					
      					<tr>
           					<td> &nbsp; </td>
           					<td colspan="2" align="right" style="width:45% "> 
                       			<logic:present name="intendattachmentList" scope="request">
            						<logic:notEmpty name="intendattachmentList">
							            <fieldset>
							              <table width="100%" class="other"> 
							              <logic:iterate name="intendattachmentList" id="attachmentData" indexId="idx2" type="in.com.rbc.quotation.common.vo.KeyValueVo">	
											<tr><td>
												<a href="javascript:download('<bean:write name="attachmentData" property="key"/>', '<bean:write name="attachmentData" property="value"/>', '<bean:write name="attachmentData" property="value2"/>');">
													<bean:write name="attachmentData" property="value"/>.<bean:write name="attachmentData" property="value2"/>
												</a>
											</td></tr>					
											</logic:iterate>
							              </table>
							        	</fieldset>
        							</logic:notEmpty>
        						</logic:present>
           					</td>
           				</tr>
           				<!-- Indent Page Uploads Ends Here -->
					           				
					</table>
					
					<br>
					
					<!---------------------------------		Request Information Block : START  ------------------------------------------------------------->
					<fieldset>
    					<legend class="blueheader"><bean:message key="quotation.viewenquiry.label.request.heading"/></legend>
    					
    					<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    						<tr>
			  					<td width="340"><label class="blue-light" style="font-weight: bold;"> <bean:message key="quotation.create.label.basicinformation"/> </label></td>
                   			</tr>
                   			
                   			<tr>
                				<td>
                					<div class="tablerow">
                						<div class="tablecolumn" >
                							<table width="100%" border="0" cellspacing="0" cellpadding="2">
                								<tr>
                    								<td colspan="2" >&nbsp;</td>
                    							</tr>
                    							<tr>
                     								<td class="label-text"  style="width:50%"> <bean:message key="quotation.create.label.indentno"/> </td>
                      								<td class="formContentview-rating-b0x"  style="width:50%"> <bean:write name="newEnquiryDto" property="savingIndent" /> </td>
                    							</tr>
                     							<tr>
                     								<td class="label-text"  style="width:50%"> <bean:message key="quotation.label.admin.customer.customer"/> </td>
                      								<td class="formContentview-rating-b0x"  style="width:50%"> <bean:write name="newEnquiryDto" property="customerName" /> </td>
                    							</tr>
                    							<tr>
                     								<td class="label-text"  style="width:50%"> <bean:message key="quotation.create.label.sapcusomercode"/> </td>
                      								<td class="formContentview-rating-b0x" style="width:50%"> <bean:write name="newEnquiryDto" property="sapCustomerCode" /> </td>
                   								</tr>
                     							<tr>
                     								<td class="label-text"  style="width:50%"> <bean:message key="quotation.create.label.orderno"/> </td>
                      								<td class="formContentview-rating-b0x"  style="width:50%"> <bean:write name="newEnquiryDto" property="orderNumber" /> </td>
                    							</tr>
                    							<tr>
                     								<td class="label-text"  style="width:50%"> <bean:message key="quotation.create.label.quotationrefno"/> </td>
                      								<td class="formContentview-rating-b0x"  style="width:50%"> <bean:write name="newEnquiryDto" property="customerEnqReference" /> </td>
                    							</tr>
                      							<tr>
                     								<td class="label-text"  style="width:50%"> <bean:message key="quotation.create.label.pocopy"/> </td>
                      								<td class="formContentview-rating-b0x"  style="width:50%"> <bean:write name="newEnquiryDto" property="poiCopy" /> </td>
                    							</tr>
                     							<tr>
                     								<td class="label-text"  style="width:50%"> <bean:message key="quotation.create.label.loicopy"/> </td>
                      								<td class="formContentview-rating-b0x"  style="width:50%"> <bean:write name="newEnquiryDto" property="loiCopy" />  </td>
                    							</tr>
                    							<tr>
							                     	<td class="label-text"  style="width:50%"> <bean:message key="quotation.create.label.deliverytype"/></td>
							                      	<td class="formContentview-rating-b0x"  style="width:50%"> <bean:write name="newEnquiryDto" property="deliveryTerm" /> </td>
							                    </tr>
                      							<tr>
                     								<td class="label-text"  style="width:50%"> <bean:message key="quotation.create.label.deliveryreqcustomer"/>  </td>
                      								<td class="formContentview-rating-b0x"  style="width:50%"> <bean:write name="newEnquiryDto" property="deliveryReqByCust" />  </td>
                    							</tr>
                      							<tr>
                     								<td class="label-text"  style="width:50%"> <bean:message key="quotation.create.label.expecteddeliverymonth"/> </td>
                      								<td class="formContentview-rating-b0x"  style="width:50%"> <bean:write name="newEnquiryDto" property="closingDate" /> </td>
                    							</tr>
                     							<tr>
                     								<td class="label-text"  style="width:50%"> <bean:message key="quotation.create.label.advancepayment"/> </td>
                      								<td class="formContentview-rating-b0x"  style="width:50%"> <bean:write name="newEnquiryDto" property="advancePayment" /> </td>
                   								</tr>
                    
                     							<tr>
                     								<td class="label-text"  style="width:50%"> <bean:message key="quotation.create.label.paymentterms"/> </td>
                      								<td class="formContentview-rating-b0x"  style="width:50%"> <bean:write name="newEnquiryDto" property="paymentTerms" /> </td>
                    							</tr>
                    
                     							<tr>
                     								<td class="label-text"  style="width:50%"> <bean:message key="quotation.create.label.ld"/> </td>
                      								<td class="formContentview-rating-b0x"  style="width:50%"> <bean:write name="newEnquiryDto" property="ld" /> </td>
                    							</tr>
                      
                      							<tr>
                     								<td class="label-text"  style="width:50%"> <bean:message key="quotation.create.label.wfd"/> </td>
                      								<td class="formContentview-rating-b0x"  style="width:50%"> <bean:write name="newEnquiryDto" property="warrantyDispatchDate" /> Months </td>
                    							</tr>
                    
                      							<tr>
                     								<td class="label-text"  style="width:50%"> <bean:message key="quotation.create.label.wfc"/> </td>
                      								<td class="formContentview-rating-b0x"  style="width:50%"> <bean:write name="newEnquiryDto" property="warrantyCommissioningDate" /> Months </td>
                    							</tr>
                    
                      							<tr>
                     								<td class="label-text"  style="width:50%"> <bean:message key="quotation.create.label.drawingapproval"/> </td>
                      								<td class="formContentview-rating-b0x"  style="width:50%"> <bean:write name="newEnquiryDto" property="drawingApproval" />  </td>
                    							</tr>
                    
                      							<tr>
                     								<td class="label-text"  style="width:50%"> <bean:message key="quotation.create.label.qapapproval"/> </td>
                      								<td class="formContentview-rating-b0x"  style="width:50%"> <bean:write name="newEnquiryDto" property="qapApproval" /> </td>
                    							</tr>
                    
                     							<tr>
                     								<td class="label-text"  style="width:50%"> <bean:message key="quotation.create.label.typetest"/> </td>
                      								<td class="formContentview-rating-b0x"  style="width:50%"> <bean:write name="newEnquiryDto" property="typeTest" /> </td>
                    							</tr>
                    
                      							<tr>
                     								<td class="label-text"  style="width:50%"> <bean:message key="quotation.create.label.routinetest"/> </td>
                      								<td class="formContentview-rating-b0x"  style="width:50%"> <bean:write name="newEnquiryDto" property="routineTest" />  </td>
                    							</tr>
                							</table>
                						</div>
                						
                						<div class="tablecolumn" >
                							<table width="100%" border="0" cellspacing="0" cellpadding="2">
                								<tr><td colspan="2" >&nbsp;</td></tr>
                								<tr>
                     								<td class="label-text" style="width:50% "> <bean:message key="quotation.create.label.specialtest"/> </td>
                      								<td class="formContentview-rating-b0x" style="width:50% "> <bean:write name="newEnquiryDto" property="specialTest" /> </td>    
                    							</tr>
                    							<tr>
                     								<td class="label-text" style="width:50% "> <bean:message key="quotation.create.label.specialtestdet"/> </td>
                      								<td class="formContentview-rating-b0x" style="width:50% "> <bean:write name="newEnquiryDto" property="specialTestDetails" /> </td>    
                    							</tr>
                    
                     							<tr>
                     								<td class="label-text" style="width:50% "> <bean:message key="quotation.create.label.thirdpartyinspection"/> </td>
                      								<td class="formContentview-rating-b0x" style="width:50% "> <bean:write name="newEnquiryDto" property="thirdPartyInspection" /> </td>    
                    							</tr>
                    
                     							<tr>
                     								<td class="label-text" style="width:50% "> <bean:message key="quotation.create.label.stageinspection"/> </td>
                      								<td class="formContentview-rating-b0x" style="width:50% "> <bean:write name="newEnquiryDto" property="stageInspection" /> </td>    
                    							</tr>
                    
                     							<tr>
                     								<td class="label-text" style="width:50% "> <bean:message key="quotation.create.label.enduser"/> </td>
                      								<td class="formContentview-rating-b0x" style="width:50% "> <bean:write name="newEnquiryDto" property="endUser" /> </td>    
                    							</tr>
                      
                      							<tr>
                     								<td class="label-text" style="width:50% "> <bean:message key="quotation.create.label.enduserlocation"/> </td>
                      								<td class="formContentview-rating-b0x" style="width:50% "> <bean:write name="newEnquiryDto" property="enduserLocation" /> </td>    
                    							</tr>
                    
                     							<tr>
                     								<td class="label-text" style="width:50% "> <bean:message key="quotation.create.label.enduserindustry"/> </td>
                      								<td class="formContentview-rating-b0x" style="width:50% "> <bean:write name="newEnquiryDto" property="endUserIndustry" /> </td>    
                    							</tr>
                    
                    							<tr>
                     								<td class="label-text" style="width:50% "> <bean:message key="quotation.create.label.consultant"/> </td>
                      								<td class="formContentview-rating-b0x" style="width:50% "> <bean:write name="newEnquiryDto" property="consultantName" /> </td>    
                    							</tr>
                    
                      							<tr>
                     								<td class="label-text" style="width:50% "> <bean:message key="quotation.create.label.epcname"/> </td>
                     								<td class="formContentview-rating-b0x" style="width:50% "> <bean:write name="newEnquiryDto" property="epcName" /> </td>    
                    							</tr>
                    
                     							<tr>
                     								<td class="label-text" style="width:50% "> <bean:message key="quotation.create.label.projectname"/> </td>
                      								<td class="formContentview-rating-b0x" style="width:50% "> <bean:write name="newEnquiryDto" property="projectName" /> </td>    
                    							</tr>
                    
                       							<tr>
                     								<td class="label-text" style="width:50% "> <bean:message key="quotation.create.label.packing"/> </td>
                      								<td class="formContentview-rating-b0x" style="width:50% "> <bean:write name="newEnquiryDto" property="packaging" /> </td>    
                    							</tr>
                    
                    							<tr>
                     								<td class="label-text" style="width:50% "> <bean:message key="quotation.create.label.insurance"/> </td>
                      								<td class="formContentview-rating-b0x" style="width:50% "> <bean:write name="newEnquiryDto" property="insurance" /> </td>    
                    							</tr>
                    
                    							<tr>
                     								<td class="label-text" style="width:50% "> <bean:message key="quotation.create.label.replacement"/> </td>
                      								<td class="formContentview-rating-b0x" style="width:50% "> <bean:write name="newEnquiryDto" property="replacement" /> </td>    
                    							</tr>
                    
                     							<tr>
                     								<td class="label-text" style="width:50% "> <bean:message key="quotation.create.label.oldserialno"/> </td>
                      								<td class="formContentview-rating-b0x" style="width:50% "> <bean:write name="newEnquiryDto" property="oldSerialNo" /> </td>    
                    							</tr>
                    
                     							<tr>
                     								<td class="label-text" style="width:50% "> <bean:message key="quotation.create.label.customerpartno"/> </td>
                      								<td class="formContentview-rating-b0x" style="width:50% "> <bean:write name="newEnquiryDto" property="customerPartNo" /> </td>    
                    							</tr>
                      
                      							<tr>
                      								<logic:equal name="newEnquiryDto" property="statusName" value="ABONDENT">
                      									<td class="label-text" style="width:50% "> <bean:message key="quotation.create.label.abondentreason"/> </td>
                      									<td class="formContentview-rating-b0x" style="width:50% ">
                      										1. <bean:write name="newEnquiryDto" property="abondentReason1" /> <br>
                      										2. <bean:write name="newEnquiryDto" property="abondentReason2" />
                      									</td>
                      								</logic:equal>
                      								<logic:equal name="newEnquiryDto" property="statusName" value="LOST">
                      									<td class="label-text" style="width:50% "> <bean:message key="quotation.create.label.lostreason"/> </td>
                      									<td class="formContentview-rating-b0x" style="width:50% "> 
                      										1. <bean:write name="newEnquiryDto" property="lostReason1" /> <br>
                      										2. <bean:write name="newEnquiryDto" property="lostReason2" /> 
                      									</td>
                      								</logic:equal>
                      								<logic:equal name="newEnquiryDto" property="statusName" value="WON">
                      									<td class="label-text" style="width:50% "> <bean:message key="quotation.create.label.winreason"/> </td>
                      									<td class="formContentview-rating-b0x" style="width:50% "> 
                      										1. <bean:write name="newEnquiryDto" property="wonReason1" /> <br>
                      										2. <bean:write name="newEnquiryDto" property="wonReason2" /> 
                      									</td>
                      								</logic:equal>
                    							</tr>
                    
                     							<tr>
                     								<td class="label-text" style="width:50% "> <bean:message key="quotation.create.label.commentsifany"/> </td>
                      								<td class="formContentview-rating-b0x" style="width:50% "> <bean:write name="newEnquiryDto" property="winlossComment" /> </td>    
                    							</tr>
                    
                     							<tr>
                     								<td class="label-text" style="width:50% "> <bean:message key="quotation.create.label.competitor"/> </td>
                     								<td class="formContentview-rating-b0x" style="width:50% "> 
                     								<logic:equal name="newEnquiryDto" property="statusName" value="LOST">
                     									<bean:write name="newEnquiryDto" property="lostCompetitor" />
                     								</logic:equal>
                     								<logic:equal name="newEnquiryDto" property="statusName" value="WON">
                     									<bean:write name="newEnquiryDto" property="winningCompetitor" />
                     								</logic:equal>
                     								</td>
                    							</tr>
                    							<logic:equal name="newEnquiryDto" property="statusName" value="ABONDENT">
                    							<tr>
                    								<td class="label-text" style="width:50% "> 
                    									Has it been discussed with Customer before closing the Job?
                    								</td>
                    								<td class="formContentview-rating-b0x" style="width:50% "> <bean:write name="newEnquiryDto" property="abondentCustomerApproval" /> </td>
                    							</tr>
                    							</logic:equal>
                    							<logic:equal name="newEnquiryDto" property="statusName" value="LOST">
                    								<td class="label-text" style="width:50% "> 
                    									Has it been discussed with Manager before loosing the Job?
                    								</td>
                    								<td class="formContentview-rating-b0x" style="width:50% "> <bean:write name="newEnquiryDto" property="lostManagerApproval" /> </td>
                    							</logic:equal>
                							</table>
                						</div>
                					</div>
                				</td>
                			</tr>
                			
						</table>
					</fieldset>
					<!---------------------------------		Request Information Block : END  --------------------------------------------------------------->
					
					<!---------------------------------		Financial Information Block : START  ----------------------------------------------------------->
					<fieldset>
						<legend class="blueheader">	<bean:message key="quotation.create.label.financial"/> </legend>
						<div class="blue-light" >
            				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				  				<tr>
									<td width="40%">
										<logic:equal name="newEnquiryDto" property="statusName" value="LOST">
											Total Quoted Price :  &#8377;  <bean:write name="newEnquiryDto" property="totalQuotedPrice" />
										</logic:equal>
										<logic:notEqual name="newEnquiryDto" property="statusName" value="LOST">
											<bean:message key="quotation.create.label.totalmotorprice"/> :  &#8377;  <bean:write name="newEnquiryDto" property="totalMotorPrice" />
										</logic:notEqual>
				  					</td>
				  					<td width="40%">
				  						<logic:equal name="newEnquiryDto" property="statusName" value="LOST">
				  							Total Lost Price :  &#8377;  <bean:write name="newEnquiryDto" property="totalLostPrice" />
				  						</logic:equal>
				  						<logic:notEqual name="newEnquiryDto" property="statusName" value="LOST">
				  							&nbsp;
				  						</logic:notEqual>
				  					</td>
				   					<td align="right">
				  						<bean:message key="quotation.create.label.totalpackageprice" /> :  &#8377;  <bean:write name="newEnquiryDto" property="totalPkgPrice" />
				  					</td>
				  				</tr>
				  			</table>
				  		</div>
				  		
				  		<div class="tablerow">
				  			<div class="tablecolumn" >
				  				<fieldset style="width:100%">
              						<legend class="blueheader">	<bean:message key="quotation.create.label.includes"/> </legend>
              				  		<table  style="width: 100%" border="0" cellspacing="0" cellpadding="0">
				  						<tr>
				  							<td colspan="5">&nbsp;</td>
				  						</tr>
				  						<tr>
				  							<td  colspan="3"> <bean:message key="quotation.create.label.freight" /> </td>
				  							<td class="formContentview-rating-b0x" style="width:50% "> <bean:write name="newEnquiryDto" property="includeFrieght" /> </td>
				  						</tr>
		          						<tr>
				  							<td colspan="3"> <bean:message key="quotation.create.label.extrawarranty" /> </td>
				  							<td class="formContentview-rating-b0x" style="width:50% "> <bean:write name="newEnquiryDto" property="includeExtraWarrnt" /> </td>
				  						</tr>
				  						<tr>
				  							<td colspan="3"> <bean:message key="quotation.create.label.supervisionandcomm" /> </td>
				  							<td class="formContentview-rating-b0x" style="width:50% "> <bean:write name="newEnquiryDto" property="includeSupervisionCost" /> </td>
				  						</tr>
										<tr>
				  							<td colspan="3"> <bean:message key="quotation.create.label.typetestcharge" /> </td>
				  							<td class="formContentview-rating-b0x" style="width:50% "> <bean:write name="newEnquiryDto" property="includetypeTestCharges" /> </td>
				  						</tr>
		    						</table>
                    			</fieldset>
				  			</div>
				  			
				  			<div class="tablecolumn" >
              					<fieldset style="width:100%">
              						<legend class="blueheader">	<bean:message key="quotation.create.label.extra"/> </legend>
				  					<table  style="width: 100%" border="0" cellspacing="0" cellpadding="0">
				  						<tr><td colspan="5">&nbsp;</td></tr>
				  						<tr>
				  							<td  colspan="3"> <bean:message key="quotation.create.label.freight" /> </td>
				  							<td class="formContentview-rating-b0x" style="width:50% "> <bean:write name="newEnquiryDto" property="extraFrieght" /> </td>
				  						</tr>
			     						<tr>
				  							<td  colspan="3"> <bean:message key="quotation.create.label.extrawarrantycharges" /> </td>
				  							<td class="formContentview-rating-b0x" style="width:50% "> <bean:write name="newEnquiryDto" property="extraExtraWarrnt" /> </td>
				  						</tr>
				  						<tr>
				  							<td  colspan="3"> <bean:message key="quotation.create.label.supervisionandcomm" /> </td>
				  							<td class="formContentview-rating-b0x" style="width:50% "> <bean:write name="newEnquiryDto" property="extraSupervisionCost" /> </td>
				  						</tr>
				   						<tr>
				  							<td  colspan="3"> <bean:message key="quotation.create.label.typetestcharge" /> </td>
				  							<td class="formContentview-rating-b0x" style="width:50% "> <bean:write name="newEnquiryDto" property="extratypeTestCharges" /> </td>
				  						</tr>
				  						<tr>
				  							<td  colspan="3"> <bean:message key="quotation.create.label.spares" /> </td>
				  							<td class="formContentview-rating-b0x" style="width:50% "> <bean:write name="newEnquiryDto" property="sparesCost" /> </td>
				  						</tr>
		    						</table>
		    					</fieldset>
                    		</div>
				  		</div>
					</fieldset>
					<!---------------------------------		Financial Information Block : END  ------------------------------------------------------------->
					
					<br><br>
					
					<!---------------------------------		Ratings Block : START  ------------------------------------------------------------------------->
					<logic:notEmpty name="newEnquiryDto" property="newRatingsList">
						<%  int iRatingIndex = 1;  %>
						<logic:iterate property="newRatingsList" name="newEnquiryDto" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<div id="arrow<%=idx.intValue()+1 %>" >
								<a href="javascript:;" onClick="toggledropdown('arrow<%=idx.intValue()+1 %>','fieldset<%= idx.intValue()+1 %>')"> 
									<img src="html/images/rightTriArrow.gif" width="15" height="15"  border="0" align="absmiddle"/>
								</a>
								<span class="other">
									<bean:message key="quotation.viewenquiry.rating.label.heading"/> <%= idx.intValue()+1 %>
								</span>
							</div>
							<input type="hidden" id="ratingidx<%=iRatingIndex%>" value='<%=iRatingIndex%>' >
							<input type="hidden" id="ratingObjId<%=iRatingIndex%>"  name="ratingObjId<%=iRatingIndex%>"  value='<bean:write name="ratingObj" property="ratingId" />' >
							<input type="hidden" id="ratingNo<%=iRatingIndex%>"  name="ratingNo<%=iRatingIndex%>"  value='<bean:write name="ratingObj" property="ratingNo" />' >
													
							<fieldset id="fieldset<%=idx.intValue()+1 %>" style="display:none;">
								<legend>
									<a href="javascript:;" onClick="toggledropdown('fieldset<%=idx.intValue() + 1%>','arrow<%=idx.intValue() + 1%>')">
										<img src="html/images/downTriArrow.gif" width="15" height="15" border="0" align="absmiddle" />
									</a> 
									<span class="blueheader"> <bean:message key="quotation.viewenquiry.rating.label.collapse.heading"/> <%=idx.intValue() + 1%>
									</span>
								</legend>
								<br>
									<fieldset>
										<legend class="blueheader">	<bean:message key="quotation.create.label.financial"/> </legend>
										<div class="blue-light" >
				            				<table width="100%" border="0" cellspacing="0" cellpadding="0">
								  				<tr>
													<td width="40%">
														<logic:equal name="newEnquiryDto" property="statusName" value="LOST">
															Quoted Price :  &#8377;  <bean:write name="ratingObj" property="ltQuotedPricePerUnit" />
														</logic:equal>
														<logic:notEqual name="newEnquiryDto" property="statusName" value="LOST">
															<bean:message key="quotation.create.label.totalmotorprice"/> : 
														</logic:notEqual>
								  					</td>
								  					<td width="40%">
								  						<logic:equal name="newEnquiryDto" property="statusName" value="LOST">
								  							Lost Price :  &#8377;  <bean:write name="ratingObj" property="ltLostTotalPrice" />
								  						</logic:equal>
								  						<logic:notEqual name="newEnquiryDto" property="statusName" value="LOST">
								  							&nbsp;
								  						</logic:notEqual>
								  					</td>
								   					<td align="right">
								  						<bean:message key="quotation.create.label.totalpackageprice" /> : <bean:write name="newEnquiryDto" property="totalPkgPrice" />
								  					</td>
								  				</tr>
								  			</table>
								  		</div>
									</fieldset>				
										   				<table width="100%">
										   					<tr>
										              			<td colspan="5" class="blue-light-btn" style="height:25px " align="right" >
										              				<strong> <bean:message key="quotation.create.label.quantityrequired"/> </strong>
										              				<bean:write name="ratingObj" property="quantity"/>&nbsp;
										              			</td>
							           			 			</tr>
															<tr>
																<td colspan="5">&nbsp;</td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Tag Number# </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="tagNumber"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Ambient Temp. Deg C </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltAmbTempId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Product Line </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltProdLineId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Haz Location </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltHazardAreaId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Power Rating(KW)  </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltKWId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Gas Group </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltGasGroupId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> No. of Poles </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltPoleId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Application </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltApplicationId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Frame </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltFrameId"/><bean:write name="ratingObj" property="ltFrameSuffixId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Voltage +/- variation </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltVoltId"/> 
																	<logic:notEmpty  name="ratingObj" property="ltVoltId">
																		<logic:notEmpty  name="ratingObj" property="ltVoltAddVariation"> <b>+</b> <bean:write name="ratingObj" property="ltVoltAddVariation"/> % </logic:notEmpty>
																		<logic:notEmpty  name="ratingObj" property="ltVoltRemoveVariation"> <b>-</b> <bean:write name="ratingObj" property="ltVoltRemoveVariation"/> % </logic:notEmpty>
																	</logic:notEmpty>
																</td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Mounting </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltMountingId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Frequecy +/- variation </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltFreqId"/> 
																	<logic:notEmpty  name="ratingObj" property="ltFreqId">
																		<logic:notEmpty  name="ratingObj" property="ltFreqAddVariation"> <b>+</b> <bean:write name="ratingObj" property="ltFreqAddVariation"/> % </logic:notEmpty>
																		<logic:notEmpty  name="ratingObj" property="ltFreqRemoveVariation"> <b>-</b> <bean:write name="ratingObj" property="ltFreqRemoveVariation"/> % </logic:notEmpty>
																	</logic:notEmpty>
																</td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> TB Position </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltTBPositionId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Combined Variation </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltCombVariation"/> % </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Manufacturing Location </strong>  </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltManufacturingLocation"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Duty </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltDutyId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Efficiency Class </strong>  </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltEffClass"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> CDF </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltCDFId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Enclosure </strong>  </td>
																<td class="formContentview-rating-b0x" style="width:24%"> &nbsp; </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> No. of Starts </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltStartsId"/> </td>
															</tr>
															<tr>
																<td colspan="5">&nbsp;</td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Standard Delivery (Weeks) </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltStandardDeliveryWks"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Earlier Supplied Motor Serial No. </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltEarlierMotorSerialNo"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Customer Requested Delivery (Weeks) </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltCustReqDeliveryWks"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Replacement Motor </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltReplacementMotor"/> </td>
															</tr>
															<tr>
																<td colspan="5">&nbsp;</td>
															</tr>
															<tr>
										              			<td colspan="5" style="height:25px; font-weight:bold;">
										              				<label class="blue-light" style="font-weight:bold; width:100% !important;"> <strong> Electrical </strong> </label>
										              			</td>
							           			 			</tr>
							           			 			<tr>
																<td class="label-text" style="width:25%"> <strong> RV </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltRVId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> RA </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltRAId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> &nbsp; </td>
															<td class="label-text" style="width:24%"> &nbsp; </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Winding Treatment </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltWindingTreatmentId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Winding Wire </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltWindingWire"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Lead </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltLeadId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Starting Current on DOL Starting </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltStartingCurrentOnDOLStart"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Winding Configuration </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltWindingConfig"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Load GD2 Value referred to motor shaft </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltLoadGD2Value"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Method of Starting </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltMethodOfStarting"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> VFD Application Type </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltVFDApplTypeId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Speed Range only for VFD motor (Min.) </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltVFDMotorSpeedRangeMin"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Speed Range only for VFD motor (Max.) </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltVFDMotorSpeedRangeMax"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Service Factor </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltServiceFactor"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Overloading Duty </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltOverloadingDuty"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Constant Efficiency Range </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltConstantEfficiencyRange"/> </td>
															</tr>
															<tr>
																<td colspan="5">&nbsp;</td>
															</tr>
															<tr>
										              			<td colspan="5" style="height:25px; font-weight:bold;">
										              				<label class="blue-light" style="font-weight:bold; width:100% !important;"> <strong> Mechanical </strong> </label>
										              			</td>
							           			 			</tr>
							           			 			<tr>
																<td class="label-text" style="width:25%"> <strong> Shaft Type </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltShaftTypeId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Shaft Material </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltShaftMaterialId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Direction of Rotation </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltDirectionOfRotation"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Method of Coupling </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltMethodOfCoupling"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Painting Type </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltPaintingTypeId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Paint Shade </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltPaintShadeId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Paint Thickness </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltPaintThicknessId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> IP </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltIPId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Insulation Class </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltInsulationClassId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Terminal Box Size </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltTerminalBoxSizeId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Spreader Box </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltSpreaderBoxId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Aux Terminal Box </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltAuxTerminalBoxId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Space Heater </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltSpaceHeaterId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Vibration </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltVibrationId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Flying Lead without TB </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltFlyingLeadWithoutTDId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Flying Lead with TB </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltFlyingLeadWithTDId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Fan </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltMetalFanId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Encoder Mounting </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltTechoMounting"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Shaft Grounding </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltShaftGroundingId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Method Of Cooling </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltForcedCoolingId"/> </td>
															</tr>
															<tr>
																<td colspan="5">&nbsp;</td>
															</tr>
															<tr>
										              			<td colspan="5" style="height:25px; font-weight:bold;">
										              				<label class="blue-light" style="font-weight:bold; width:100% !important;"> <strong> Accessories </strong> </label>
										              			</td>
							           			 			</tr>
							           			 			<tr>
																<td class="label-text" style="width:25%"> <strong> Hardware </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltHardware"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Gland Plate </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltGlandPlateId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Double Compression Gland </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltDoubleCompressionGlandId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> SPM Mounting Provision </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltSPMMountingProvisionId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Additional Name Plate </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltAddNamePlateId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Direction Arrow Plate </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltDirectionArrowPlateId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> RTD </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltRTDId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> BTD </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltBTDId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Thermister </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltThermisterId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Cable Sealing Box </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltCableSealingBoxId"/> </td>
															</tr>
							           			 			<tr>
																<td colspan="5">&nbsp;</td>
															</tr>
															<tr>
										              			<td colspan="5" style="height:25px; font-weight:bold;">
										              				<label class="blue-light" style="font-weight:bold; width:100% !important;"> <strong> Bearings </strong> </label>
										              			</td>
							           			 			</tr>
							           			 			<tr>
																<td class="label-text" style="width:25%"> <strong> ReLubrication System </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltBearingSystemId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Bearing NDE </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltBearingNDEId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Bearing DE </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltBearingDEId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td style="width:25%"> &nbsp; </td>
																<td style="width:24%"> &nbsp; </td>
															</tr>
							           			 			<tr>
																<td colspan="5">&nbsp;</td>
															</tr>
															<tr>
										              			<td colspan="5" style="height:25px; font-weight:bold;">
										              				<label class="blue-light" style="font-weight:bold; width:100% !important;"> <strong> Certificate / Spares / Testing </strong> </label>
										              			</td>
							           			 			</tr>
							           			 			<tr>
																<td class="label-text" style="width:25%"> <strong> Witness Routine </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltWitnessRoutineId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Additional Test 1 </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltAdditionalTest1Id"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Additional Test 2 </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltAdditionalTest2Id"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Additional Test 3 </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltAdditionalTest3Id"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Type Test </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltTypeTestId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Certification </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltULCEId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Spares 1 </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltSpares1"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Spares 2 </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltSpares2"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Spares 3 </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltSpares3"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Spares 4 </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltSpares4"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Spares 5 </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltSpares5"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Data Sheet </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltDataSheet"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Add Drawing </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltAddDrawingId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td style="width:25%"> &nbsp; </td>
																<td style="width:24%"> &nbsp; </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Additional Comments </strong> </td>
																<td colspan="4" class="formContentview-rating-b0x" style="width:75%;word-break:break-all;height:80px;"> <bean:write name="ratingObj" property="ltAdditionalComments"/> </td>
															</tr>
														</table>
													
													<%	iRatingIndex = iRatingIndex+1; %>
													
							</fieldset>
						</logic:iterate>
					</logic:notEmpty>
					<!---------------------------------		Ratings Block : END  --------------------------------------------------------------------------->
					
										
				</logic:present>
			</td>
		</tr>
		
		<tr> <td> <br><br> </td> </tr>
		
		<tr>
			<td class="blue-light" align="right" colspan="4">
				<div class="blue-light" align="right">
					<bean:define id="statusId" name="newEnquiryDto" property="statusId" />
					<bean:define id="createdSSO" name="newEnquiryDto" property="createdBySSO" />
					<logic:present name="salesManager" scope="request">
						<logic:greaterThan name="newEnquiryDto" property="statusId" value="5">
					 		<input name="viewpdf" type="button" onclick="viewNewQuotationPDF();" class="BUTTON" value="View PDF"/>
						</logic:greaterThan>
					</logic:present>
				</div>
			</td>
		</tr>
		
	</table>
		
</html:form>