<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants,in.com.rbc.quotation.common.utility.PropertyUtility,in.com.rbc.quotation.common.utility.CommonUtility,in.com.rbc.quotation.common.vo.KeyValueVo" %>
<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.12.1/css/all.css" crossorigin="anonymous">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script type="text/javascript" src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<html:form action="newPlaceAnEnquiry.do" method="POST" enctype="multipart/form-data">
<html:hidden property="invoke" value="createEnquiry"/>
<html:hidden property="dispatch" />
<html:hidden property="createdBySSO" />
<html:hidden property="createdDate"/>
<html:hidden property="createdBy"/>
<html:hidden property="operationType" styleId="operationType"/> 
<html:hidden property="revisionNumber"/>
<html:hidden property="enquiryOperationType"/>
<html:hidden property="ratingOperationType"/>
<html:hidden property="enquiryId" styleId="enquiryId"/>
<html:hidden property="ratingId" styleId="ratingId"/>
<html:hidden property="ratingNo" styleId="ratingNo"/>
<html:hidden property="ratingsValidationMessage" styleId="ratingsValidationMessage"/>
<html:hidden property="currentRating"/>
<html:hidden property="nextRatingId"/>
<html:hidden property="isMaxRating"/>
<html:hidden property="enquiryNumber"/>
<html:hidden property="commentOperation" styleId="commentOperation"/>
<html:hidden property="totalRatingIndex" styleId="totalRatingIndex"/>
<html:hidden property="statusId" styleId="statusId" />
<html:hidden property="dmUpdateStatus" styleId="dmUpdateStatus" />
<html:hidden property="selectedRemoveRatingId" styleId="selectedRemoveRatingId"/>
<html:hidden property="locationId" styleId="locationId"/>
<html:hidden property="concernedPersonHdn" styleId="concernedPersonHdn"/>

<input type="hidden" name="lastRatingNum" id="lastRatingNum" value='<%=(Integer)request.getSession().getAttribute("lastRatingNum") %>' >
<input type="hidden" name="prevIdVal" id="prevIdVal" value='<%=(Integer)request.getSession().getAttribute("prevIdVal") %>' >
<input type="hidden" name="fileTypestd" id="fileTypestd" value='<bean:write name="newenquiryForm" property="fileTypesStr"/>' >
<input type="hidden" name='<%= org.apache.struts.taglib.html.Constants.TOKEN_KEY %>' value='<%= session.getAttribute(org.apache.struts.action.Action.TRANSACTION_TOKEN_KEY) %>' />

<!-------------------------- main table Start  --------------------------------->
<table width="80%" height="auto" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
  <tr>
    <td height="100%" valign="top">
	<!-------------------------- Top Title and Status & Note block Start  --------------------------------->
        <div class="sectiontitle">
        	<logic:equal name="newenquiryForm" property="statusId" value=""> <bean:message key="quotation.create.label.title"/>  </logic:equal>
        	
        	<logic:equal name="newenquiryForm" property="statusId" value="0"> <bean:message key="quotation.create.label.title"/> </logic:equal>
        	
        	<logic:equal name="newenquiryForm" property="statusId" value="1"> Edit Enquiry </logic:equal>
        	
        	<logic:equal name="newenquiryForm" property="statusId" value="8"> Edit Enquiry - Pending Sales, Design Complete </logic:equal>
        	
        	<logic:equal name="newenquiryForm" property="statusId" value="20"> Pending BH Approval </logic:equal>
        	
        	<logic:equal name="newenquiryForm" property="statusId" value="21"> Edit Enquiry - Pending Mfg. Plant Details </logic:equal>
        </div>
        <br><br>
        <logic:notEqual name="newenquiryForm" property="dispatch" value="edit">
	        <div class="DotsTable" align="center">
	    	   <bean:message key="quotation.create.label.title.message1"/> <b> <bean:message key="quotation.create.label.title.message2"/></b> <bean:message key="quotation.create.label.title.message3"/>
        	</div><br>
        	<div style=" width:100%; text-align:right; margin-bottom:20px;" id="attach">Attach Files&nbsp;<img src="html/images/ico_attach.gif" border="0" width="23" height="25" alt="Attachments" align="absmiddle" class="hand"  onclick="showhide('manageattachments','attach')" /></div>
			
			<fieldset style="width:100%;" class="hidetable" id="manageattachments">
			<legend class="blueheader">RFQ Documents</legend>
         		<table width="100%" border="0" cellspacing="0" cellpadding="0">
        	    	<tr>
        	      		<td style="padding-bottom:10px;">
				  			<logic:present name="oAttachmentDto" scope="session">
	    	   					Max. upload file size for attachments: <b><bean:write name="oAttachmentDto" property="maxFileSize"/> <%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_PROPERTYFILENAME, QuotationConstants.QUOTATION_ATTACHMENT_FILESIZEUNITS)%> </b>
	    	   					<br> 
	    	   					Max. attachments per enquiry: <b>10</b>	  
								<br> 
								Allowed file formats for attachments: 
								<logic:notEqual name="oAttachmentDto" property="fileTypes" value="">
									<b><bean:write name="oAttachmentDto" property="fileTypes"/></b>	
	    	   		  			</logic:notEqual>   	   		
	    	   				</logic:present>  	
    	    				<logic:equal name="newenquiryForm" property="isMaxRating" value="true"> <br> Max. Limit Exceeded, cannot add more ratings. </logic:equal>
    	    			</td>
    	    		</tr>
                	<tr>
                		<td>
                			<table width="100%">
                				<tr> 
                					<td width="20%" class="formLabel">Attach 1</td>
									<td width="11%"> <html:file property="theFile1" styleId="theFile1" style="width:218px; font:10px;"/></td>
									<td width="19%"></td>
									<td width="20%"  class="formLabel">Attach 2</td>
									<td width="15%"> <html:file property="theFile2" styleId="theFile2" style="width:218px; font:10px;"/></td>
									<td width="15%"></td>
								</tr>
                			</table>
                		</td>
					</tr>
                	<tr>
                		<td>
                			<table width="100%">
				  				<td width="20%"  class="formLabel">Attach 3</td>
				  				<td width="11%"> <html:file property="theFile3" styleId="theFile3" style="width:218px; font:10px;"/></td>
				  				<td width="19%"></td>
				   				<td width="20%"   class="formLabel">Attach 4</td>
				  				<td width="15%"> <html:file property="theFile4" styleId="theFile4" style="width:218px; font:10px;"/></td>
				  				<td width="15%"></td>
				  			</table>
				  		</td>
  					</tr>
                	<tr>
                		<td>
                			<table width="100%">
                				<td width="20%"  class="formLabel">Attach 5</td>
								<td width="11%"> <html:file property="theFile5" styleId="theFile5" style="width:218px; font:10px;"/></td>
								<td width="19%"></td>
				                <td width="20%"  class="formLabel">Attach 6</td>
								<td width="15%"> <html:file property="theFile6" styleId="theFile6" style="width:218px; font:10px;"/></td>
								<td width="15%"></td>
                  			</table>
                  		</td>              
  					</tr>
  					<tr>
                		<td>
                			<table width="100%">
	              				<td width="20%"  class="formLabel">Attach 7</td>
				  				<td width="11%"> <html:file property="theFile7" styleId="theFile7" style="width:218px; font:10px;"/></td>
							  	<td width="19%"></td>
			                  	<td width="20%"  class="formLabel">Attach 8</td>
							  	<td width="15%"> <html:file property="theFile8" styleId="theFile8" style="width:218px; font:10px;"/></td>
							  	<td width="15%"></td>
                  			</table>
                  		</td>              
  					</tr>
  					<tr>
                		<td>
                			<table width="100%">
				              <td width="20%"  class="formLabel">Attach 9</td>
							  <td width="11%"> <html:file property="theFile9" styleId="theFile9" style="width:218px; font:10px;"/></td>
							  <td width="19%"></td>
			                  <td width="20%"  class="formLabel">Attach 10</td>
							  <td width="15%"> <html:file property="theFile10" styleId="theFile10" style="width:218px; font:10px;"/></td>
							  <td width="15%"></td>
                  			</table>
                  		</td>              
  					</tr>
							<logic:present name="attachmentMessage1" scope="request">
								<tr>
									<td colspan="2"><bean:write name="attachmentMessage1" /></td>
								</tr>
							</logic:present>
							<logic:present name="attachmentMessage2" scope="request">
								<tr>
									<td colspan="2"><bean:write name="attachmentMessage2" /></td>
								</tr>
							</logic:present>
							<logic:present name="attachmentMessage3" scope="request">
								<tr>
									<td colspan="2"><bean:write name="attachmentMessage3" /></td>
								</tr>
							</logic:present>
							<logic:present name="attachmentMessage4" scope="request">
								<tr>
									<td colspan="2"><bean:write name="attachmentMessage4" /></td>
								</tr>
							</logic:present>
							<logic:present name="attachmentMessage5" scope="request">
								<tr>
									<td colspan="2"><bean:write name="attachmentMessage5" /></td>
								</tr>
							</logic:present>
							<logic:present name="attachmentMessage6" scope="request">
								<tr>
									<td colspan="2"><bean:write name="attachmentMessage6" /></td>
								</tr>
							</logic:present>
							<logic:present name="attachmentMessage7" scope="request">
								<tr>
									<td colspan="2"><bean:write name="attachmentMessage7" /></td>
								</tr>
							</logic:present>
							<logic:present name="attachmentMessage8" scope="request">
								<tr>
									<td colspan="2"><bean:write name="attachmentMessage8" /></td>
								</tr>
							</logic:present>
							<logic:present name="attachmentMessage9" scope="request">
								<tr>
									<td colspan="2"><bean:write name="attachmentMessage9" /></td>
								</tr>
							</logic:present>
							<logic:present name="attachmentMessage10" scope="request">
								<tr>
									<td colspan="2"><bean:write name="attachmentMessage10" /></td>
								</tr>
							</logic:present>
							
				<logic:present name="attachmentList" scope="request">
					<logic:notEmpty name="attachmentList">
                  		<tr>
		     				<td colspan="2">
		     					<fieldset>
		       					<table width="100%" class="other">
		         					<logic:iterate name="attachmentList" id="attachmentData" indexId="idx1" type="in.com.rbc.quotation.common.vo.KeyValueVo">	
										<tr>
											<td>
												<a href="javascript:download('<bean:write name="attachmentData" property="key"/>', '<bean:write name="attachmentData" property="value"/>', '<bean:write name="attachmentData" property="value2"/>');">
													<bean:write name="attachmentData" property="value"/>.<bean:write name="attachmentData" property="value2"/>
												</a>
											</td>
										</tr>					
									</logic:iterate>
				    			</table>
              					</fieldset>
              				</td>
              			</tr>
					</logic:notEmpty>
				</logic:present>
    	      </table>
			</fieldset>
		</logic:notEqual>
        
        <logic:equal name="newenquiryForm" property="dispatch" value="edit">
	         <table width="100%" border="0" cellpadding="0" cellspacing="0">
    			<tr>
			    	<td rowspan="2">
			    	<table width="98%"  border="0" cellspacing="2" cellpadding="0" class="DotsTable">
          				<tr>
            				<td rowspan="2" style="width:70px "><img src="html/images/icon_quote.gif" width="52" height="50"></td>
		            		<td class="other">	<bean:message key="quotation.viewenquiry.label.subheading1"/> <bean:write name="newenquiryForm" property="enquiryNumber"/><br>
               					 <bean:message key="quotation.viewenquiry.label.subheading2"/></td>
         				</tr>
         				<tr>
		            		<td class="other">
		            		<table width="100%">
		                		<tr>
		                			<logic:equal name="newenquiryForm" property="returnstatement" value="RETURNDM">
		                		    	<td height="16" class="section-head" align="center">Status : <bean:write name="newenquiryForm" property="returnstatus" /> </td>
		                		    </logic:equal>
		                		   	<logic:equal name="newenquiryForm" property="returnstatement" value="edit">      	
		                  				<td height="16" class="section-head" align="center">Status : Draft </td>
		                  			</logic:equal>
		               		 	</tr>
		            		</table>
		            		</td>
		          		</tr>
      				</table>
      				</td>
		      		<logic:lessThan name="newenquiryForm" property="statusId" value="10"> 
		      			<logic:notEqual name="newenquiryForm" property="statusId" value="7">
				      		<td class="message"><bean:message key="quotation.attachments.message"/> </td>
		     			 	<td class="message">
		     	 				<a href="javascript:manageAttachments('<%= CommonUtility.getIntValue(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_ATTACHMENT_APPLICATIONID)) %>','<bean:write name="newenquiryForm" property="enquiryId"/>', '<bean:write name="newenquiryForm" property="createdBySSO"/>');">
		     				 	<img src="html/images/ico_attach.gif" alt="Manage Attachments" width="23" height="25" border="0" align="right"></a>
		     				</td>
		     			</logic:notEqual>
		     	 	</logic:lessThan>
    			</tr>
		    	<tr>	
		    		<td align="center"> <div id="attachmentmessage"></div></td>
				</tr>        	
  			</table>
  			<br>
  		</logic:equal>
     
     	<!-------------------------- Top Title and Status & Note block End  ---------------------------------------->
     	
		<div class="note-red" style="font-weight:bold !important;" align="right"> All fields marked &nbsp;&nbsp; <img src="html/images/bullets.gif" width="7" height="7" align="absmiddle"> &nbsp;&nbsp; indicates mandatory fields. </div>
		
		<fieldset id="requestInfoFieldSet" style="min-height:1030px">
			<!-- --------------------- Employee Request Information block : Start ---------------------------------------- -->
  		    <legend class="blueheader"><bean:message key="quotation.create.label.requestinfo"/></legend>
				<table width="100%">
					<tr>
					  <td style="vertical-align:top" >
						<table width="100%">
						 	<tr>
						 		<td colspan="6" style="padding-bottom:0px !important;">			  
									<label class="blue-light" style="font-weight: bold;"> Enquiry Information </label>
								</td>				    
						  	</tr>
						  	<tr>
						  		<td class="formLabel" style="width:170px; line-height:20px;"> <bean:message key="quotation.create.label.customerenqreference"/> </td>
						  		<td>
						  			<html:text property="customerEnqReference" styleId="customerEnqReference" styleClass="normal" style="width:170px" maxlength="100" />
						  		</td>
						  		<td class="dividingdots" align="center" width="2%" height="100%" rowspan="5"></td>
						  		<td align="center" width="2%" height="100%" rowspan="5">&nbsp;</td>
						  		<td class="formLabel" style="width:120px; line-height:20px;"> <bean:message key="quotation.label.is.marathon.approved"/></td>
						     	<td>
		                    		<bean:define name="newenquiryForm" property="alTargetedList" id="alTargetedList" type="java.util.Collection" />
									<html:select property="is_marathon_approved" styleId="is_marathon_approved"  style="width:170px">
										<option value="0" selected>Select</option>
			                    		<html:options name="newenquiryForm" collection="alTargetedList" property="key" labelProperty="value" />
		        	            	</html:select>                	
								</td>
						  	</tr>
							<tr>
							    <td  class="formLabel"> <bean:message key="quotation.create.label.customer"/> <span class="mandatory">&nbsp;</span> </td>
			                    <td >
						            <input id="customerName" name="customerName" type="text" size="50" class="normal" autocomplete="off"  style="width:170px " value="<bean:write name="newenquiryForm" property="customerName"/>" />
			                     	<div class="note hidetable" id="invalidcustomer" style="text-align:left;padding-left:20px">Invalid Customer Name</div>
			                     	<html:hidden property="customerId" styleId="customerId"/>
			                     	<html:hidden property="customerType" styleId="customerType"/>
			                    </td>
			                    
			                    <td class="formLabel"> <bean:message key="quotation.label.consultant"/> </td>
						     	<td>	
						    		<html:text property="consultantName" styleClass="normal" style="width:170px" maxlength="50"/>   
								</td>
		                  	</tr>
		                  	<tr>
								<td class="formLabel"> <bean:message key="quotation.label.admin.customer.location"/> </td>
							    <td>
							    	<select style="width:170px;" name="location" id="location" onfocus="javascript:loadLocation();">
			                    		<option value="0"><bean:message key="quotation.option.select"/></option>
			                    	</select>			       
			                    </td>
			                    
			                    <td class="formLabel"> <bean:message key="quotation.create.label.enduser"/> </td>
						     	<td>				     
						     		<html:text property="endUser" styleId="endUser" styleClass="normal" style="width:170px" maxlength="50"/>                     	                      	
								</td> 
							</tr>
							<tr>
								<td class="formLabel"><bean:message key="quotation.label.concerned.person"/> <span class="mandatory">&nbsp;</span> </td>
							     <td>
							     	<!--  
							     	<select style="width:170px;" name="concernedPerson" id="concernedPerson" onfocus="javascript:loadConcernedPerson();">
			                    		<option value="0"><bean:message key="quotation.option.select"/></option>
			                    	</select>
			                    	-->
			                    	<html:text style="width:170px" property="concernedPerson" styleId="concernedPerson" styleClass="normal" maxlength="100" />
								</td>
								
								<td class="formLabel"> <bean:message key="quotation.create.label.enduserindustry"/> </td>
						    	<td>
						    		<bean:define name="newenquiryForm" property="alIndustryList" id="alIndustryList" type="java.util.Collection" /> 
			                    	<html:select style="width:170px;" name="newenquiryForm" property="endUserIndustry" styleId="endUserIndustry" >
			                    		<option value="0"><bean:message key="quotation.option.select"/></option>
			                       	 	<html:options name="newenquiryForm" collection="alIndustryList" property="key" labelProperty="value" />
			                        </html:select>  
								</td>
							</tr>
							<tr>
								<td class="formLabel">Project Name</td>
						    	<td>
						    		<html:text property="projectName" styleClass="normal" style="width:170px" maxlength="50"/>
						    	</td> 
						    	
						    	<td class="formLabel"> <bean:message key="quotation.label.location.of.installation"/> </td>
						     	<td>
						     		<input id="locationOfInstallation" name="locationOfInstallation" type="text" size="50" class="normal" autocomplete="off"  style="width:170px " value="<bean:write name="newenquiryForm" property="locationOfInstallation"/>"  />			       
									<html:hidden property="locationOfInstallationId" styleId="locationOfInstallationId"/>
								</td>
		                    </tr>
		                    <tr>
		                    	<td colspan="6"> &nbsp; </td>
		                    </tr>
		                    <tr>
		                    	<td colspan="6" nowrap >
		                    		<div class="box-nav note" style="text-align:left !important; font-weight:bold !important;">
		                    		<html:checkbox property="customerDealerLink" styleId="customerDealerLink" value="Y" />
		                    		<label for="customerDealerLink" > <bean:message key="quotation.label.create.customerDealerLink"/> </label>
		                    		</div>
		                    	</td>
		                    </tr>
		                    <tr>
		                    	<td colspan="6"> &nbsp; </td>
		                    </tr>
		                    <tr>
		                    	<td colspan="6" nowrap>
		                    		<div class="box-nav note-red" style="text-align:left !important; font-weight:bold !important;">
		                    			<label class="list-hd">NOTE :</label>
		                    			<ul style="margin-block:0em !important;">
											<li>For Customer Type "Dealer" : Maximum Payment Days is "60 days". </li>
											<li>For Customer Type "Dealer" : Retention Payment is Not Applicable. </li>
											<li>For Customer Type "Dealer" : Liquidated Damages is Not Applicable. </li>
										</ul>
		                    		</div>
		                    	</td>
		                    </tr>
						</table>
					  </td>
					  
					  <td class="dividingdots" width="2%" height="100%" align="center" style="padding:2px;"></td> 
					  <td width="2%" height="100%" align="center" style="padding:2px;">&nbsp;</td>
					  
					  <td style="vertical-align:top" >
						<table width="100%">
						 	<tr>
						 		<td colspan="2" style="padding-bottom:0px !important;">				  
						  			<label class="blue-light" style="font-weight: bold; padding-bottom:3px !important;"> Enquiry Weightage </label>
						  		</td>
						  	</tr>
						  	<tr>
						  		<td class="formLabel"> <bean:message key="quotation.create.label.enquirytype"/> <span class="mandatory">&nbsp;</span></td>
						    	<td>
						    		<bean:define name="newenquiryForm" property="alEnquiryTypeList" id="alEnquiryTypeList" type="java.util.Collection" />
						    		<html:select style="width:175px;" name="newenquiryForm" property="enquiryType" styleId="enquiryType" onclick="javascript:loadCustomerType();">
						    			<option value="0"><bean:message key="quotation.option.select"/></option>
		                       	 		<html:options name="newenquiryForm" collection="alEnquiryTypeList" property="key" labelProperty="value" />
						    		</html:select> 
								</td>
						  	</tr>
							<tr>
								<td class="formLabel"> <bean:message key="quotation.create.label.winningchance"/> <span class="mandatory">&nbsp;</span></td>
						    	<td>				     
						     		<bean:define name="newenquiryForm" property="alWinChanceList" id="alWinChanceList" type="java.util.Collection" />
						     		<html:select style="width:175px;" name="newenquiryForm" property="winningChance" styleId="winningChance">
		                    			<option value="0"><bean:message key="quotation.option.select"/></option>
		                       	 		<html:options name="newenquiryForm" collection="alWinChanceList" property="key" labelProperty="value" />
		                        	</html:select> 
								</td>
		                    </tr>
		                    <tr>
								<td class="formLabel"> Targeted <span class="mandatory">&nbsp;</span></td>
						    	<td>
						    		<bean:define name="newenquiryForm" property="alTargetedList" id="alTargetedList" type="java.util.Collection" />
									<html:select style="width:175px" property="targeted" styleId="targeted">
										<option value="0" selected>Select</option>
			                    		<html:options name="newenquiryForm" collection="alTargetedList" property="key" labelProperty="value" />
		        	            	</html:select>
								</td>
		                     </tr>
		                     
		                     <tr>
		                     	<td colspan="2" style="padding-bottom:0px !important;">
		                     		<label class="blue-light" style="font-weight: bold; padding-bottom:3px !important;"> <bean:message key="quotation.label.important.dates"/> </label>
		                     	</td>				  
				  			 </tr>
				  			 <tr>
								<td class="formLabel" style="width:190px;line-height:14px;"> <bean:message key="quotation.label.enquiry.receipt.date"/> <span class="mandatory">&nbsp;</span> </td>
							    <td>				     
							    	 <html:text property="receiptDate" styleId="receiptDate" styleClass="readonlymini" readonly="true" style="width:150px" maxlength="12" />
								</td>
							 </tr>
							 <!--
							 <tr>					
								<td class="formLabel"> <bean:message key="quotation.label.enquiry.entry.date"/> </td>
						    	<td>				     
						     		<html:text property="entryDate" styleId="entryDate" styleClass="readonlymini" readonly="true" style="width:170px" maxlength="12"/> 
								</td>
		                     </tr>
		                     -->               
		                     <tr>					
								<td class="formLabel" style="width:190px;line-height:14px;"> <bean:message key="quotation.label.create.required.offer.subission.date"/> <span class="mandatory">&nbsp;</span> </td>
						    	<td>				     
						     		<html:text property="submissionDate" styleId="submissionDate" styleClass="readonlymini" readonly="true" style="width:150px" maxlength="12"/>
								</td>
							 </tr>
							 <!--
		                     <tr>
								<td class="formLabel" style="width:200px;line-height:14px;"> <bean:message key="quotation.label.create.actual.offer.submission.date"/> </td>
						    	<td>				     
						     		<html:text property="actualSubmissionDate" styleId="actualSubmissionDate" styleClass="readonlymini" readonly="true" style="width:170px" maxlength="12"/>
								</td>
							 </tr>
							 -->
							 <tr>
								<td class="formLabel" style="width:190px;line-height:14px;"> <bean:message key="quotation.label.create.order.closing.date"/> <span class="mandatory">&nbsp;</span> </td>
						    	<td>
					    			<html:text property="closingDate" styleId="closingDate" styleClass="readonlymini" readonly="true" style="width:150px" maxlength="12"/>			     
								</td>
							 </tr>
						</table>
					  </td>
					</tr>
				</table>  
			<!-- --------------------- Employee Request Information block : End ------------------------------------------ -->
			
			<!-- --------------------- Commercial Terms and Conditions Section : Start ----------------------------------- -->
				<%@ include file="newcreateenquiry_commercialterms_include.jsp"%>
			<!-- --------------------- Commercial Terms and Conditions Section : End ------------------------------------- -->
			
			<!-- --------------------- Ratings Section : Start ----------------------------------------------------------- -->
				<%@ include file="newcreateenquiry_ratings_complete_include.jsp"%>
			<!-- --------------------- Ratings Section : End ------------------------------------------------------------- -->
			                  
		</fieldset>
				
		<br>
		<br>
		<br>		
		
		<!-- Field Section Ending -->
		
		<!-- Re-assign Section Starting -->
		<% if(QuotationConstants.QUOTATION_STRING_Y.equalsIgnoreCase((String)request.getSession().getAttribute("showReAssignSectionFlag"))) { %>
			<fieldset>
				<legend class="blueheader"> <bean:message key="quotation.offer.label.reassign" /> </legend>
				<table style="background-color: #FFFFFF">
					<tr>
						<td><bean:message key="quotation.offer.label.reassignto" /></td>
						<td>
							<select id="reassignToId" name="reassignToId" style="width:370px;">
								<option value="0"><bean:message key="quotation.option.select" /></option>
								<logic:present property="salesReassignUsersList" name="newenquiryForm">
									<bean:define name="newenquiryForm" property="salesReassignUsersList" id="salesReassignUsersList" type="java.util.Collection" />
									<logic:iterate name="salesReassignUsersList" id="salesReassignUsers" type="in.com.rbc.quotation.common.vo.KeyValueVo">
										<option value='<bean:write name="salesReassignUsers" property="key" />' > <bean:write name="salesReassignUsers" property="value" /> </option>
									</logic:iterate>
								</logic:present>
							</select>
						</td>
					</tr>
					<tr>
						<td><bean:message key="quotation.offer.label.comments" /></td>
						<td><textarea style="width: 370px; height: 55px" name="reassignRemarks" id="reassignRemarks" onkeyup="checkMaxLenghtText(this, 4000);" onkeydown="checkMaxLenghtText(this, 4000);" > <bean:write name="newenquiryForm"  property="reassignRemarks" filter="false" /> </textarea> </td>
					</tr>
				</table>
				<div class="blue-light" align="right">
					<button type="button" style="font-weight:bold; font-size:11px; color: #990000;" onclick="javascript:salesReassignTo();">Re-assign</button>
				</div>
			</fieldset>
		<% } %>
		<!-- Re-assign Section Ending   -->
		
		<!-- Notes Section Starting -->
 		<fieldset >
			<legend class="blueheader"> <bean:message key="quotation.create.label.notesection"/> </legend>
			     <div>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr> <td colspan="5">&nbsp;</td> </tr>
							<tr style="float: left; width: 100%;">
								<td class="formLabelrating" style="width: 200px; margin-right: 33px;"> <bean:message key="quotation.create.label.commentsanddeviations"/> </td>
								<td colspan="4">
									<html:textarea property="commentsDeviations" styleId="commentsDeviations" style="width:905px; height:50px;" onkeyup="checkMaxLenghtText(this, 10000);" onkeydown="checkMaxLenghtText(this, 10000);" tabindex="35"  > <bean:write name="newenquiryForm" property="commentsDeviations" /> </html:textarea>
								</td>
							</tr>
							<tr> <td colspan="5">&nbsp;</td> </tr>
							<tr style="float: left; width: 100%;">
								<td class="formLabelrating" style="width: 200px; margin-right: 33px;"> <bean:message key="quotation.create.label.smnote" /> (Internal) </td>
								<td colspan="4">
									<html:textarea property="smNote" styleId="smNote" style="width:905px; height:50px;" onkeyup="checkMaxLenghtText(this, 10000);" onkeydown="checkMaxLenghtText(this, 10000);" tabindex="35"  > <bean:write name="newenquiryForm" property="smNote" /> </html:textarea>
								</td>
							</tr>
							<logic:notEmpty name="newenquiryForm" property="designNote">
							<tr> <td colspan="5">&nbsp;</td> </tr>
							<tr style="float: left; width: 100%;">
								<td class="formLabelrating" style="width: 200px; margin-right: 33px;"> Note By DM (Internal) </td>
								<td colspan="4">
									<html:textarea property="designNote" styleId="designNote" style="width:905px; height:50px;" readonly="true" tabindex="35" > <bean:write name="newenquiryForm" property="designNote" /> </html:textarea>
								</td>
							</tr>
							</logic:notEmpty>
							
							<logic:equal name="newenquiryForm" property="statusId" value="20">
							<tr> <td colspan="5">&nbsp;</td> </tr>
							<tr style="float: left; width: 100%;">
								<td class="formLabelrating" style="width: 200px; margin-right: 33px;"> Note By BH (Internal) </td>
								<td colspan="4">
									<html:textarea property="bhNote" styleId="bhNote" style="width:905px; height:50px;" onkeyup="checkMaxLenghtText(this, 4000);" onkeydown="checkMaxLenghtText(this, 4000);" tabindex="35" > <bean:write name="newenquiryForm" property="bhNote" /> </html:textarea>
								</td>
							</tr>
							</logic:equal>
							
							<logic:equal name="newenquiryForm" property="statusId" value="21">
							<tr> <td colspan="5">&nbsp;</td> </tr>
							<tr style="float: left; width: 100%;">
								<td class="formLabelrating" style="width: 200px; margin-right: 33px;"> Note By Mfg. Plant (Internal) </td>
								<td colspan="4">
									<html:textarea property="mfgNote" styleId="mfgNote" style="width:905px; height:50px;" onkeyup="checkMaxLenghtText(this, 4000);" onkeydown="checkMaxLenghtText(this, 4000);" tabindex="35" > <bean:write name="newenquiryForm" property="mfgNote" /> </html:textarea>
								</td>
							</tr>
							</logic:equal>
						</table>
				</div>
		</fieldset>
		<!-- Notes Section Ending -->
		
 		<!-------------------------- Bottum Input buttons block Start  --------------------------------->
		<br>
		<div class="blue-light" align="right">
        		<logic:notEqual name="newenquiryForm" property="dispatch" value="edit">
        			<logic:equal name="newenquiryForm" property="statusId" value="8">
        				<html:button property="" styleClass="BUTTON" value="Submit" onclick="javascript:submitNewEnquiry('SUBMIT');"/>
        			</logic:equal>  
        			<logic:notEqual name="newenquiryForm" property="statusId" value="8">
        				<logic:notEqual name="newenquiryForm" property="statusId" value="19">
        					<logic:notEqual name="newenquiryForm" property="statusId" value="20">	
        						<html:button property="" styleClass="BUTTON" value="Refer to Design" onclick="javascript:submitNewEnquiry('REFERDESIGN', '');"/>
        						<html:button property="" styleClass="BUTTON" value="Save As Draft" onclick="javascript:saveNewEnquiryDraft('DRAFT', '');"/>
        						<html:button property="" styleClass="BUTTON" value="Submit" onclick="javascript:submitNewEnquiry('SUBMIT');"/>
        					</logic:notEqual>
        				</logic:notEqual>
        			</logic:notEqual>
        			<logic:equal name="newenquiryForm" property="statusId" value="19">
        				<input name="Button" type="button" class="BUTTON" id="lost" onclick="javascript:viewCreateEnquiryIndentPage('Lost');" value="Lost">
						<input name="Button" type="button" class="BUTTON" id="abandoned" onclick="javascript:viewCreateEnquiryIndentPage('Abondent');" value="Abondent">
        			</logic:equal>
        			<logic:equal name="newenquiryForm" property="statusId" value="20">
        				<html:button property="" styleClass="BUTTON" value="Approve Liquidated Damages" onclick="javascript:processLD('APPROVE');"/>
        				<html:button property="" styleClass="BUTTON" value="Reject Liquidated Damages" onclick="javascript:processLD('REJECT');"/>
        			</logic:equal>
         		</logic:notEqual>   
         		<!-- Last Submit Buttons Based on Conditions  -->    
	            <logic:equal name="newenquiryForm" property="dispatch" value="edit">
	                <logic:equal name="newenquiryForm" property="returnstatement" value="RETURNDM">
	                	<html:button property="" styleClass="BUTTON" value="Submit" onclick="javascript:submitNewEnquiry('RETURN');"/>
	                </logic:equal>
	                <logic:equal name="newenquiryForm" property="returnstatement" value="edit">
	                	<html:button property="" styleClass="BUTTON" value="Submit" onclick="javascript:submitNewEnquiry('SUBMIT');"/>
	            	</logic:equal>
	            </logic:equal>
		</div>
		<!-------------------------- Bottum Input buttons block End  --------------------------------->
	</td>
	</tr>
</table>
<!-------------------------- main table End  --------------------------------->
</html:form>

<div id="loadingDiv" class="modal123" style="display:none;">
	<!-- Modal content-->
	<div class="modal-content">
		<div class="note" align="center" style="color:#009900;">
			<img src="html/images/loading_big.gif" border="0" align="absmiddle" >
		</div>
	</div>
</div>

<div id="screen"></div>

<div id="modal">
	<img src="html/images/loading_big.gif" alt="" />
</div>
	
<ajax:autocomplete
		fieldId="customerName"
		popupId="customer-popup"
		targetId="customerId"
		baseUrl="servlet/AutocompleteServlet?type=placeenquiry&valueType=id"
		paramName="customer"
		className="autocomplete"
		progressStyle="throbbing" 
		postFunc="postAutocomplete"/>

<ajax:autocomplete
		fieldId="locationOfInstallation"
		popupId="customer-popup"
		targetId="locationOfInstallationId"
		baseUrl="servlet/AutocompleteServlet?type=placeenquiry&valueType=id&fieldtype=locationOfInstallation"
		paramName="customer"
		className="autocomplete"
		progressStyle="throbbing" 
		postFunc="postAutocomplete"/>
		
<iframe id="divShim" src="javascript:false;" scrolling="no" frameborder="0" style="position:absolute; top:0px; left:0px; display:none;"> </iframe>

<script>

$('#btnAddCol').click(function(){
	$('#screen, #modal').show();
});
$('#btnCopyCol').click(function(){
	$('#screen, #modal').show();
});
$('#screen, #modal button').click(function(){
	$('#screen, #modal').hide();
});
	
$(document).ready(function() {
	
	$('#screen, #modal').hide();
	
	var locationIdVal = $('#locationId').val();
	loadLocationWithValue(locationIdVal);
	
	$('.ratings-table tbody').scroll(function(e) { //detect a scroll event on the tbody
	    $('.ratings-table thead').css("left", -$(".ratings-table tbody").scrollLeft()); //fix the thead relative to the body scrolling
	    $('.ratings-table thead th').css("left", $(".ratings-table tbody").scrollLeft()); //fix the first cell of the header
	    $('.ratings-table tbody td:first-child').css("left", $(".ratings-table tbody").scrollLeft()); //fix the first column of tdbody
	});
	
	$('#ratingsEncloseDiv').find('.accordion-toggle').click(function() {
		//Expand or collapse this panel
		$(this).next().slideToggle('fast');
		var ii = $(this).find('.icon');

		$('.icon').not(ii).addClass('icon-plus');
		ii.toggleClass('icon-plus');

		//Hide the other panels
		$(".accordion-content").not($(this).next()).slideUp('fast');
	});
	
	$('#bottomfields_section').on('scroll', function () {
	    $('#topfields_section').scrollLeft($(this).scrollLeft());
	});
	
	document.getElementById("basefields_section").style.display = "none";
	document.getElementById("electricalfields_section").style.display = "none";
	document.getElementById("mechanicalfields_section").style.display = "none";
	document.getElementById("accessoriesfields_section").style.display = "none";
	document.getElementById("bearingsfields_section").style.display = "none";
	document.getElementById("certificatefields_section").style.display = "none";
		
	var totalRatingIdx = $('#totalRatingIndex').val();
	for(i=1; i<=totalRatingIdx; i++) {
		document.addEventListener('DOMContentLoaded', function(){ 
		    autosize(document.querySelectorAll('#additionalComments'+i));
		}, false);
		
		//alert('operation : ' + $('#operationType').val());
		if($('#operationType').val() != 'EDITENQUIRY' && $('#operationType').val() != 'ADDNEWRATING' && $('#operationType').val() != 'COPYPREVIOUSRATING' && $('#operationType').val() != 'REMOVERATING') {
			//alert('Inside EDIT operation : ');
			processFieldsForProductLine(i);	
		} else {
			if(i != totalRatingIdx) {
				processFieldDisplayOptions(i);
			}
		}
	}
	if($('#operationType').val() == 'ADDNEWRATING' || $('#operationType').val() != 'COPYPREVIOUSRATING' ) {
		processFieldsForProductLine(totalRatingIdx);
	}
	
	var supervisionInclusive = document.getElementById('supervisionDetails').value;
	if(supervisionInclusive === 'Yes') {
		$("#supervisionDetailsDivId").css("display","block");
	} else {
		$("#supervisionDetailsDivId").css("display","none");
	}
	
	var old_title;
	$('.title-styled').on('mouseover', function(){
		old_title = $(this).attr('title');
		$('.title-message').html($(this).attr('title')).css('visibility', 'visible');
		$(this).attr('title', '');
	});
	  
	$('.title-styled').on('mouseleave', function(){
		$(this).attr('title', old_title);
		$('.title-message').html('').css('visibility', 'hidden');
	});
	  
});

$("#receiptDate").datepicker({
    dateFormat: 'dd-mm-yy',
    onSelect: function (selectedDate) {
        if (this.id == 'receiptDate') {
        	const DATE_FORMAT = 'DD-MM-YYYY';
        	const SUNDAY = 0; // moment day index
        	const SATURDAY = 6; // moment day index
        	const WEEKENDS = [SATURDAY, SUNDAY];
        	const for_offersubmit_date = moment(selectedDate, DATE_FORMAT);
        	let offersubmit_count = 0;
            while (offersubmit_count < 3) {
            	for_offersubmit_date.add(1, 'day');
        	    // Skip weekends
        	    if (WEEKENDS.includes(for_offersubmit_date.day())) {
        	    	continue;
        	    }
        	    // Increment count
        	    offersubmit_count++;
			}
        	//alert('offer submit date = ' + for_offersubmit_date.format(DATE_FORMAT));
        	$("#submissionDate").val(for_offersubmit_date.format(DATE_FORMAT));
        	
        	const for_orderclose_date = moment(selectedDate, DATE_FORMAT);
			let orderclose_count = 0;
        	while (orderclose_count < 30) {
        		for_orderclose_date.add(1, 'day');
        	    // Skip weekends
        	    if (WEEKENDS.includes(for_orderclose_date.day())) {
        	    	continue;
        	    }
        	    // Increment count
        	    orderclose_count++;
			}
        	//alert('order close date = ' + for_orderclose_date.format(DATE_FORMAT));
        	$("#closingDate").val(for_orderclose_date.format(DATE_FORMAT));
        	
        }
    }
});

$("#submissionDate").datepicker({
    dateFormat: 'dd-mm-yy',
    onSelect: function (selectedDate) {
        if (this.id == 'submissionDate') {
            var arr = selectedDate.split("-");
            var date = new Date(arr[2]+"-"+arr[1]+"-"+arr[0]);
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();
            var minDate = new Date(y, m + 3, d);
        }
    }
});

$("#closingDate").datepicker({
    dateFormat: 'dd-mm-yy',
    onSelect: function (selectedDate) {
        if (this.id == 'closingDate') {
            var arr = selectedDate.split("-");
            var date = new Date(arr[2]+"-"+arr[1]+"-"+arr[0]);
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();
            var minDate = new Date(y, m + 3, d);
        }
    }
});

var subCatContainer = $(".linked");
subCatContainer.scroll(function() {
    subCatContainer.scrollLeft($(this).scrollLeft());
});

function sticky_relocate() {
    var window_top = $(window).scrollTop();
    var div_top = $('#sticky-anchor').offset().top;
    if (window_top > div_top) {
        $('#sticky').addClass('stick');
    } else {
        $('#sticky').removeClass('stick');
    }
}

$(function () {
    $(window).scroll(sticky_relocate);
    //sticky_relocate();
});

</script>


<style>
	.linked {
  		width: 100%;
   		overflow: hidden;
	}
	#sticky {
   		background: #fff;
	}
	#sticky.stick {
	    position: fixed;
	    top: 0;
	    z-index: 10000;
	}
	/*div.sticky {
		position: -webkit-sticky;
  		position: sticky;
  		top: 0;
  		padding: 5px;
  		border: 2px solid #4C4EAF;
	}*/
	.ratingsdiv{
		overflow:hidden;
	}
	.newFieldsMargin {
		width:127px;
		float:left;
	}
	.newRatingLabel tr td {
		float : left;
		line-height : 21px !important;
		padding : 1.6px 1.5px 1.5px 1.5px !important;
	}
	.newRatingFields tr td {
		padding : 2.5px !important;
	}
	.newRatingSparesLabel tr td {
		float : left;
		line-height : 21px !important;
		padding : 2px !important;
	}
	.newRatingSparesFields tr td {
		padding : 2.5px !important;
	}
	#calcPriceLoader { 
            border: 12px solid #f3f3f3; 
            border-radius: 50%; 
            border-top: 12px solid #444444; 
            width: 70px; 
            height: 70px; 
            animation: spin 1s linear infinite; 
	} 
	@keyframes spin { 
		100% { 
			transform: rotate(360deg); 
		} 
	} 
	.center { 
            position: absolute; 
            top: 0; 
            bottom: 0; 
            left: 0; 
            right: 0; 
            margin: auto; 
	}
	.icontemp:after {
		/* symbol for "opening" panels */
		font-family: 'Glyphicons Halflings';
		content: "\e113";
		color: #02306b;
		font-style: normal;
		padding-left: 10px;
	}
	.icontemp.collapsed:after {
		/* symbol for "collapsed" panels */
		content: "\e114"; /* adjust as needed, taken from bootstrap.css */
	}
	.icontemp {
		border-bottom: 1px solid #ccc;
	}
	
</style>
