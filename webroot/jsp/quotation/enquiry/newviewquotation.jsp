<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants" %>
<%@ page import="in.com.rbc.quotation.common.utility.CommonUtility" %>
<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>
<%@ page import="in.com.rbc.quotation.common.utility.PropertyUtility" %>
<%@page import="in.com.rbc.quotation.common.utility.CurrencyConvertUtility" %>

<%@ page import="java.text.DecimalFormat,java.math.BigDecimal,java.net.URLDecoder" %>

<%
	DecimalFormat df = new DecimalFormat("###,###");
	DecimalFormat df1 = new DecimalFormat("#0");
%>


<!----------------------------------------------------------- main table Start  ---------------------------------------------------------->

<table width="95%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
	<tr>
		<td>
		
			<logic:present name="displayPaging" scope="request">
				<table align="right">
					<tr>
						<td>
							<fieldset style="width: 100%">
								<table cellpadding="0" cellspacing="2" border="0">
									<tr>
										<td style="text-align: center"><a
											href="searchenquiry.do?invoke=viewSearchEnquiryResults&dispatch=fromViewPage"
											class="returnsearch"> Return to search<img
												src="html/images/back.gif" border="0" align="absmiddle" /></a>
										</td>
									</tr>
									<tr class="other" align="right">
										<td align="right"><logic:present name="previousId"
												scope="request">
												<bean:define name="previousId" id="previousId"
													scope="request" />
												<bean:define name="previousIndex" id="previousIndex"
													scope="request" />
												<bean:define name="previousStatus" id="previousStatus"
													scope="request" />
												<a
													href="newPlaceAnEnquiry.do?invoke=validateRequestAction&enquiryId=<bean:write name="previousId" scope="request"/>&statusId=<bean:write name="previousStatus" scope="request"/>&index=<bean:write name="previousIndex" scope="request"/>"><img
													src="html/images/PrevPage.gif" alt="" border="0"
													align="absmiddle" /></a>
											</logic:present> <logic:present name="displayMessage" scope="request">
												<bean:write name="displayMessage" scope="request" />
											</logic:present> <logic:present name="nextId" scope="request">
												<a
													href="newPlaceAnEnquiry.do?invoke=validateRequestAction&enquiryId=<bean:write name="nextId" scope="request"/>&statusId=<bean:write name="nextStatus" scope="request"/>&index=<bean:write name="nextIndex" scope="request"/>"><img
													src="html/images/NextPage.gif" alt="Next Request"
													border="0" align="absmiddle" /></a>
											</logic:present></td>
									</tr>
								</table>
							</fieldset>
						</td>
					</tr>
				</table>
			</logic:present>
			
		</td>
	</tr>
	
	<tr>
		<td valign="top" height="100%">
			<html:form  method="post" action="newPlaceAnEnquiry.do">
				<br>
				<html:hidden property="enquiryId" styleId="enquiryId"/>
				<html:hidden property="enquiryNumber" styleId="enquiryNumber"/>
				<html:hidden property="totalRatingIndex" styleId="totalRatingIndex"/>
				<html:hidden property="customerId" styleId="customerId"/>
				<html:hidden property="invoke" value="viewReturnEnquiry"/>
				<html:hidden property="dispatch" />
				
				<input type="hidden" name="<%=org.apache.struts.taglib.html.Constants.TOKEN_KEY%>" value="<%=session.getAttribute(org.apache.struts.action.Action.TRANSACTION_TOKEN_KEY)%>">
				
				<logic:present name="newEnquiryDto" scope="request">    
					    				
    				<table width="1170"  border="1" align="center" cellpadding="2" cellspacing="0" bordercolor="#003366">
    					<logic:notEmpty name="newEnquiryDto" property="newRatingsList">
    					
    					<tr>
    						<td>
    							<table width="1130"  border="0" align="center" cellpadding="1" cellspacing="0">
    								<tr>
						                <td><img src="html/images/melogo_new.jpg" style="width:350px;"></td>
						            </tr>
						            <tr>
						                <td class="blueheader" style="font-size:24px " align="right"><bean:message key="quotation.create.label.quotationtocustomer"/></td>
						            </tr>
						            <tr>
						                <td class="other" align="right"> <bean:message key="quotation.viewenquiry.label.subheading1"/> <bean:write name="newEnquiryDto" property="enquiryNumber"/> &nbsp;&nbsp;</td>
						            </tr>
						            <tr>
										<td height="16" class="section-head" align="right">
											<bean:message key="quotation.viewenquiry.label.status"/>  <bean:write name="newEnquiryDto" property="statusName"/> Approval
										</td>
									</tr>
						            <tr>
						              	<td colspan="5"  >&nbsp;</td>
						          	</tr>
						          	<tr class="section-head" >
              							<td class="grey-dark" colspan="5"> &nbsp; </td>
               						</tr>
               						
						          	<tr>
	                						<td>
	                							<fieldset>
	                								<legend class="blueheader"><bean:message key="quotation.create.label.requestinfo"/></legend>
	                								<table width="100%">
														<tr>
											  				<td valign="top" style="width:32%;" >
																<table style="width:100%;">
																 	<tr>	
																 		<td colspan="2"> <label class="blue-light" style="font-weight:bold; width:100%;"> Enquiry Information </label>
																	   	</td>				    
																  	</tr>
																  	<tr>
																  		<th width="50%" class="label-text" > <bean:message key="quotation.create.label.customerenqreference"/> </th>
																  		<td width="50%" class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="customerEnqReference"/></td>
																  	</tr>
																  	<tr>
													    				<th class="label-text" > <bean:message key="quotation.create.label.customer"/> </th>
													    				<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="customerName"/></td>
													    			</tr>
													    			<tr>
													    				<th  class="label-text" > <bean:message key="quotation.label.admin.customer.location"/> </th>
													    				<td  class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="location"/></td>
													    			</tr>
													    			<tr>
													    				<th  class="label-text" > <bean:message key="quotation.search.customer"/> <bean:message key="quotation.label.admin.field.industry"/> </th>
													    				<td  class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="customerIndustry"/></td>
													    			</tr>
													    			<tr>
													    				<th  class="label-text" > <bean:message key="quotation.label.concerned.person"/> </th>
													    				<td  class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="concernedPerson"/></td>
													    			</tr>
													    			<tr>
													    				<th  class="label-text" > Project Name </th>
													    				<td  class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="projectName"/></td>
													    			</tr>
													    			<tr>
													    				<th  class="label-text" > Is Marathon approved </th>
													    				<td  class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="is_marathon_approved"/></td>
													    			</tr>
													    			<tr>
													    				<th  class="label-text" > Consultant </th>
													    				<td  class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="consultantName"/></td>
													    			</tr>
													    			<tr>
													    				<th  class="label-text" > End user </th>
													    				<td  class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="endUser"/></td>
													    			</tr>
													    			<tr>
													    				<th  class="label-text" > End User Industry </th>
													    				<td  class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="endUserIndustry"/></td>
													    			</tr>
													    			<tr>
													    				<th  class="label-text" > Location of Installation </th>
													    				<td  class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="locationOfInstallation"/></td>
													    			</tr>
													    		</table>
															</td>
															<td width="2%" height="100%" rowspan="3" align="center"  class="dividingdots">&nbsp;</td>
															<td width="298" align="left" valign="top" style="width:300px;" >
																<table style="width:100%;">
																 	<tr>	
																 		<td colspan="2">			  
																  		<label class="blue-light" style="font-weight:bold; width:100%;"> 
																    		Enquiry Waitage
																    	</label>
																    	</td>
																  	</tr>
																  	<tr>
													    				<th width="50%"  class="label-text" > Enquiry Type </th>
													    				<td width="50%"  class="formContentview-rating-b0x"> <bean:write name="newenquiryForm" property="enquiryType"/></td>
													    			</tr>
													    			<tr>
													    				<th  class="label-text" > Winning Chance </th>
													    				<td  class="formContentview-rating-b0x"> <bean:write name="newenquiryForm" property="winningChance"/></td>
													    			</tr>
													    			<tr>
													    				<th  class="label-text" > Targeted </th>
													    				<td  class="formContentview-rating-b0x"> <bean:write name="newenquiryForm" property="targeted"/></td>
													    			</tr>
													    		</table>
															</td>
								     						<td width="2%" height="100%" rowspan="3" align="center"  class="dividingdots">&nbsp;</td>
								            				<td width="32%" align="left" valign="top" style="width:300px;" >
																<table align="left" style="width:100%;">
																	<tr>	
																		<td colspan="2">			  
																	  	<label class="blue-light" style="font-weight:bold; width:100%;">
																	    	<bean:message key="quotation.label.important.dates"/>
																	    </label>
																	    </td>
																  	</tr>
																  	<tr>
													    				<th width="50%"  class="label-text" > Enquiry Receipt date </th>
													    				<td width="50%"  class="formContentview-rating-b0x"> <bean:write name="newenquiryForm" property="receiptDate"/></td>
													    			</tr>
													    			<tr>
													    				<th  class="label-text" > Required Offer Submission date </th>
													    				<td  class="formContentview-rating-b0x"> <bean:write name="newenquiryForm" property="submissionDate"/></td>
													    			</tr>
													    			<tr>
													    				<th  class="label-text" > Order Closing date </th>
													    				<td  class="formContentview-rating-b0x"> <bean:write name="newenquiryForm" property="closingDate"/></td>
													    			</tr>
													    		</table>
															</td>
														</tr>
													</table>
													
													<table width="100%">
								                		<tr>				  
														    <th class="label-text" style="width:25% !important;"> <bean:message key="quotation.create.label.enquiryreferencenumber"/> &nbsp;&nbsp; </th>
										             		<td colspan="3" class="formContentview-rating-b0x" style="width:75% !important;word-break:break-all;height:40px;">
										             			<logic:equal name="newenquiryForm" property="enquiryReferenceNumber" value="">
																		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;				    	
																</logic:equal>
																<logic:notEqual name="newenquiryForm" property="description" value="">
																    	<bean:write name="newenquiryForm" property="enquiryReferenceNumber"/>
																</logic:notEqual>
										             		</td>
														</tr>
														<tr>				  
												    		<th class="label-text" style="width:25% !important;"> <bean:message key="quotation.create.label.description"/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; </th>
												    		<td colspan="3" class="formContentview-rating-b0x" style="width:75% !important;word-break:break-all;height:40px;">
												    			<logic:equal name="newenquiryForm" property="description" value="">
																		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;				    	
																</logic:equal>
																<logic:notEqual name="newenquiryForm" property="description" value="">
																    	<bean:write name="newenquiryForm" property="description"/>
																</logic:notEqual>
												    		</td>
														</tr>
								                 		<tr>			
										                    <th class="label-text" style="width:25% !important;"> <bean:message key="quotation.create.label.createdby"/>:&nbsp; </th>
										                    <td class="formContentview-rating-b0x" style="width:25% !important;"> <font class="formContentview-rating-b0x"><bean:write name="newenquiryForm" property="createdBy"/></font> </td>
										                    <th class="label-text" style="width:25% !important;"> <bean:message key="quotation.create.label.createddate"/>:&nbsp; </th>
										                    <td class="formContentview-rating-b0x" style="width:25% !important;"> <font class="formContentview-rating-b0x"><bean:write name="newenquiryForm" property="createdDate"/></font> </td>
								                  		</tr>	
											    	</table>
											    	
											    	<!-- Commercial Terms Section -->
													<table width="100%">
														<tr>
															<td style="vertical-align: top">
																<table width="100%">
																	<tr>
																		<td>
																			<label class="blue-light" style="font-weight:bold; width:100% !important;"> Commercial Terms and Conditions </label>
																		</td>
																	</tr>
																</table>
																<table width="100%">
																	<tr>
																		<th class="label-text" >Payment Terms </th>
																		<th class="label-text" >%age amount of Nett. order value </th>
																		<th class="label-text" >Payment Days </th>
																		<th class="label-text" >Payable Terms </th>
																		<th class="label-text" >Instrument </th>
																	</tr>
																	<tr>
																		<td class="formContentview-rating-b0x" >Advance Payment 1 </td>
																		<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt1PercentAmtNetorderValue"/> </td>
																		<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt1PaymentDays"/> </td>
																		<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt1PayableTerms"/> </td>
																		<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt1Instrument"/> </td>
																	</tr>
																	<tr>
																		<td class="formContentview-rating-b0x" >Advance Payment 2 </td>
																		<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt2PercentAmtNetorderValue"/> </td>
																		<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt2PaymentDays"/> </td>
																		<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt2PayableTerms"/> </td>
																		<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt2Instrument"/> </td>
																	</tr>
																	<tr>
																		<td class="formContentview-rating-b0x" >Main Payment </td>
																		<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt3PercentAmtNetorderValue"/> </td>
																		<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt3PaymentDays"/> </td>
																		<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt3PayableTerms"/> </td>
																		<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt3Instrument"/> </td>
																	</tr>
																	<tr>
																		<td class="formContentview-rating-b0x" >Retention Payment 1 </td>
																		<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt4PercentAmtNetorderValue"/> </td>
																		<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt4PaymentDays"/> </td>
																		<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt4PayableTerms"/> </td>
																		<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt4Instrument"/> </td>
																	</tr>
																	<tr>
																		<td class="formContentview-rating-b0x" >Retention Payment 2 </td>
																		<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt5PercentAmtNetorderValue"/> </td>
																		<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt5PaymentDays"/> </td>
																		<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt5PayableTerms"/> </td>
																		<td class="formContentview-rating-b0x" > <bean:write name="newenquiryForm" property="pt5Instrument"/> </td>
																	</tr>
																</table>
																<br>
																<table width="100%">
																	<tr>
																		<th class="label-text" style="width:25% !important;">Delivery Term </th>
																		<td class="formContentview-rating-b0x" style="width:25% !important;"> <bean:write name="newenquiryForm" property="deliveryTerm"/></td>
																		<th class="label-text" style="width:25% !important;">Order Completion Lead Time </th>
																		<td class="formContentview-rating-b0x" style="width:25% !important;"> <bean:write name="newenquiryForm" property="orderCompletionLeadTime"/> Weeks</td>
																	</tr>
																	<tr>
																		<th class="label-text" style="width:25% !important;">Warranty from the date of dispatch </th>
																		<td class="formContentview-rating-b0x" style="width:25% !important;"> <bean:write name="newenquiryForm" property="warrantyDispatchDate"/> Months</td>
																		<th class="label-text" style="width:25% !important;">Warranty from the date of comissioning </th>
																		<td class="formContentview-rating-b0x" style="width:25% !important;"> <bean:write name="newenquiryForm" property="warrantyCommissioningDate"/> Months</td>
																	</tr>
																	<tr>
																		<th class="label-text" style="width:25% !important;">Delivery </th>
																		<td class="formContentview-rating-b0x" style="width:25% !important;"> <bean:write name="newenquiryForm" property="deliveryInFull"/> </td>
																		<th class="label-text" style="width:25% !important;">Packaging </th>
																		<td class="formContentview-rating-b0x" style="width:25% !important;"> <bean:write name="newenquiryForm" property="packaging"/> </td>
																	</tr>
																	<tr>
																		<th class="label-text" style="width:25% !important;">GST </th>
																		<td class="formContentview-rating-b0x" style="width:25% !important;"> <bean:write name="newenquiryForm" property="gstValue"/> </td>
																		<th class="label-text" style="width:25% !important;">Liquidated Damages due to delayed deliveries </th>
																		<td class="formContentview-rating-b0x" style="width:25% !important;"> <bean:write name="newenquiryForm" property="liquidatedDamagesDueToDeliveryDelay"/> </td>
																	</tr>
																</table>
																<br>
																<table width="100%">
																	<tr>
																		<th class="label-text" style="width:25% !important;">Supervision for Erection and Comissioning</th>
																		<td class="formContentview-rating-b0x" style="width:25% !important;"> <bean:write name="newenquiryForm" property="supervisionDetails"/> </td>
																		<td >&nbsp;</td>
																		<td >&nbsp;</td>
																	</tr>
																</table>
																<logic:equal name="newenquiryForm" property="supervisionDetails" value="Yes">
																	<table width="50%" >
																		<tr>
																			<th class="label-text" style="width:25% !important;">No. of Man Days</th>
																			<td class="formContentview-rating-b0x" style="width:75% !important;"> <bean:write name="newenquiryForm" property="superviseNoOfManDays"/> </td>
																		</tr>
																		<tr>
																			<th class="label-text" colspan="2">&nbsp;</th>
																		</tr>
																		<tr>
																			<th class="label-text" style="width:100% !important; white-space:nowrap;" colspan="2"> Note: To & Fro travelling charges, Fooding, Lodging & Local conveyance at site are at customer's account. </th>
																		</tr>
																	</table>
																</logic:equal>
																<br>
															</td>
														</tr>
													</table>
													<!-- Commercial Terms Section -->
													
	                							</fieldset>
												             							
	                					</td>
	                				</tr>
                					
                					<!-----------------------Rating block start -------------------------------------->
                					
                					<tr>
	                						<td>
	                							
	                							<%  int iRatingIndex = 1;  %>
						
												<logic:iterate property="newRatingsList" name="newEnquiryDto" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
													<div id="arrow<%=idx.intValue()+1 %>" >
														<a href="javascript:;" onClick="toggledropdown('arrow<%=idx.intValue()+1 %>','fieldset<%= idx.intValue()+1 %>')"> 
															<img src="html/images/rightTriArrow.gif" width="15" height="15"  border="0" align="absmiddle"/>
														</a>
														<span class="other">
															<bean:message key="quotation.viewenquiry.rating.label.heading"/> <%= idx.intValue()+1 %>
														</span>
													</div>
													<input type="hidden" id="ratingidx<%=iRatingIndex%>" value='<%=iRatingIndex%>' >
													<input type="hidden" id="ratingObjId<%=iRatingIndex%>"  name="ratingObjId<%=iRatingIndex%>"  value='<bean:write name="ratingObj" property="ratingId" />' >
													<input type="hidden" id="ratingNo<%=iRatingIndex%>"  name="ratingNo<%=iRatingIndex%>"  value='<bean:write name="ratingObj" property="ratingNo" />' >
													
													<fieldset id="fieldset<%=idx.intValue()+1 %>" style="display:none;">
														<legend>
															<a href="javascript:;" onClick="toggledropdown('fieldset<%=idx.intValue() + 1%>','arrow<%=idx.intValue() + 1%>')">
																<img src="html/images/downTriArrow.gif" width="15" height="15" border="0" align="absmiddle" />
															</a> 
															<span class="blueheader"> <bean:message key="quotation.viewenquiry.rating.label.collapse.heading"/> <%=idx.intValue() + 1%>
															</span>
														</legend>
										   				<br>
										   				
										   				<table width="100%">
										   					<tr>
										              			<td colspan="5" class="blue-light-btn" style="height:25px " align="right" >
										              				<strong> <bean:message key="quotation.create.label.quantityrequired"/> </strong>
										              				<bean:write name="ratingObj" property="quantity"/>&nbsp;
										              			</td>
							           			 			</tr>
															<tr>
																<td colspan="5">&nbsp;</td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Tag Number# </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="tagNumber"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Ambient Temp. Deg C </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltAmbTempId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Product Line </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltProdLineId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Haz Location </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltHazardAreaId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Power Rating(KW)  </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltKWId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Gas Group </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltGasGroupId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> No. of Poles </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltPoleId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Application </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltApplicationId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Frame </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltFrameId"/><bean:write name="ratingObj" property="ltFrameSuffixId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Voltage +/- variation </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltVoltId"/> 
																	<logic:notEmpty  name="ratingObj" property="ltVoltId">
																		<logic:notEmpty  name="ratingObj" property="ltVoltAddVariation"> <b>+</b> <bean:write name="ratingObj" property="ltVoltAddVariation"/> % </logic:notEmpty>
																		<logic:notEmpty  name="ratingObj" property="ltVoltRemoveVariation"> <b>-</b> <bean:write name="ratingObj" property="ltVoltRemoveVariation"/> % </logic:notEmpty>
																	</logic:notEmpty>
																</td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Mounting </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltMountingId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Frequecy +/- variation </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltFreqId"/> 
																	<logic:notEmpty  name="ratingObj" property="ltFreqId">
																		<logic:notEmpty  name="ratingObj" property="ltFreqAddVariation"> <b>+</b> <bean:write name="ratingObj" property="ltFreqAddVariation"/> % </logic:notEmpty>
																		<logic:notEmpty  name="ratingObj" property="ltFreqRemoveVariation"> <b>-</b> <bean:write name="ratingObj" property="ltFreqRemoveVariation"/> % </logic:notEmpty>
																	</logic:notEmpty>
																</td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> TB Position </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltTBPositionId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Combined Variation </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltCombVariation"/> % </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Manufacturing Location </strong>  </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltManufacturingLocation"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Duty </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltDutyId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Efficiency Class </strong>  </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltEffClass"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> CDF </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltCDFId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Enclosure </strong>  </td>
																<td class="formContentview-rating-b0x" style="width:24%"> &nbsp; </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> No. of Starts </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltStartsId"/> </td>
															</tr>
															<tr>
																<td colspan="5">&nbsp;</td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Total Addon % </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <logic:notEmpty name="ratingObj" property="ltTotalAddonPercent"> <bean:write name="ratingObj" property="ltTotalAddonPercent"/> % </logic:notEmpty> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Total Cast Extra </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <logic:notEmpty name="ratingObj" property="ltTotalCashExtra"> &#8377; <bean:write name="ratingObj" property="ltTotalCashExtra"/> </logic:notEmpty> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> List Price Per Unit </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <logic:notEmpty name="ratingObj" property="ltLPPerUnit"> &#8377; <bean:write name="ratingObj" property="ltLPPerUnit"/> </logic:notEmpty> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> List Price with Addon Per Unit </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <logic:notEmpty name="ratingObj" property="ltLPWithAddonPerUnit"> &#8377; <bean:write name="ratingObj" property="ltLPWithAddonPerUnit"/> </logic:notEmpty> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Price Per Unit </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <logic:notEmpty name="ratingObj" property="ltPricePerUnit"> &#8377; <bean:write name="ratingObj" property="ltPricePerUnit"/> </logic:notEmpty> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Total Order Price </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <logic:notEmpty name="ratingObj" property="ltTotalOrderPrice"> &#8377; <bean:write name="ratingObj" property="ltTotalOrderPrice"/> </logic:notEmpty> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Standard Delivery (Weeks) </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltStandardDeliveryWks"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Earlier Supplied Motor Serial No. </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltEarlierMotorSerialNo"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Customer Requested Delivery (Weeks) </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltCustReqDeliveryWks"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Replacement Motor </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltReplacementMotor"/> </td>
															</tr>
															<tr>
																<td colspan="5">&nbsp;</td>
															</tr>
															<tr>
										              			<td colspan="5" style="height:25px; font-weight:bold;">
										              				<label class="blue-light" style="font-weight:bold; width:100% !important;"> <strong> Electrical </strong> </label>
										              			</td>
							           			 			</tr>
							           			 			<tr>
																<td class="label-text" style="width:25%"> <strong> RV </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltRVId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Winding Configuration </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltWindingConfig"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> RA </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltRAId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Winding Treatment </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltWindingTreatmentId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Method of Starting </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltMethodOfStarting"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Winding Wire </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltWindingWire"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Starting Current on DOL Starting </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltStartingCurrentOnDOLStart"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Lead </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltLeadId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> VFD Application Type </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltVFDApplTypeId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Load GD2 Value referred to motor shaft </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltLoadGD2Value"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Speed Range only for VFD motor (Min.) </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltVFDMotorSpeedRangeMin"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Overloading Duty </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltOverloadingDuty"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Speed Range only for VFD motor (Max.) </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltVFDMotorSpeedRangeMax"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Constant Efficiency Range </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltConstantEfficiencyRange"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Service Factor </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltServiceFactor"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Temperature Rise </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltTempRise"/>  </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Method Of Cooling </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltForcedCoolingId"/>  </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Insulation Class / Temp. Rise Class </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltTempClass"/>  </td>
															</tr>
															<tr>
																<td colspan="5">&nbsp;</td>
															</tr>
															<tr>
										              			<td colspan="5" style="height:25px; font-weight:bold;">
										              				<label class="blue-light" style="font-weight:bold; width:100% !important;"> <strong> Mechanical </strong> </label>
										              			</td>
							           			 			</tr>
							           			 			<tr>
																<td class="label-text" style="width:25%"> <strong> Shaft Type </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltShaftTypeId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Shaft Material </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltShaftMaterialId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Direction of Rotation </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltDirectionOfRotation"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Method of Coupling </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltMethodOfCoupling"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Painting Type </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltPaintingTypeId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Paint Shade </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltPaintShadeId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Paint Thickness </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltPaintThicknessId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> IP </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltIPId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Insulation Class </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltInsulationClassId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Terminal Box Size </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltTerminalBoxSizeId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Spreader Box </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltSpreaderBoxId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Aux Terminal Box </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltAuxTerminalBoxId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Space Heater </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltSpaceHeaterId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Vibration </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltVibrationId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Flying Lead without TB </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltFlyingLeadWithoutTDId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Flying Lead with TB </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltFlyingLeadWithTDId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Fan </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltMetalFanId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Encoder Mounting </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltTechoMounting"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Shaft Grounding </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltShaftGroundingId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Method Of Cooling </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltForcedCoolingId"/> </td>
															</tr>
															<tr>
																<td colspan="5">&nbsp;</td>
															</tr>
															<tr>
										              			<td colspan="5" style="height:25px; font-weight:bold;">
										              				<label class="blue-light" style="font-weight:bold; width:100% !important;"> <strong> Accessories </strong> </label>
										              			</td>
							           			 			</tr>
							           			 			<tr>
																<td class="label-text" style="width:25%"> <strong> Hardware </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltHardware"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Gland Plate </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltGlandPlateId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Double Compression Gland </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltDoubleCompressionGlandId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> SPM Mounting Provision </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltSPMMountingProvisionId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Additional Name Plate </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltAddNamePlateId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Direction Arrow Plate </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltDirectionArrowPlateId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> RTD </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltRTDId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> BTD </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltBTDId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Thermister </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltThermisterId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Cable Sealing Box </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltCableSealingBoxId"/> </td>
															</tr>
							           			 			<tr>
																<td colspan="5">&nbsp;</td>
															</tr>
															<tr>
										              			<td colspan="5" style="height:25px; font-weight:bold;">
										              				<label class="blue-light" style="font-weight:bold; width:100% !important;"> <strong> Bearings </strong> </label>
										              			</td>
							           			 			</tr>
							           			 			<tr>
																<td class="label-text" style="width:25%"> <strong> ReLubrication System </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltBearingSystemId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Bearing NDE </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltBearingNDEId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Bearing DE </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltBearingDEId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td style="width:25%"> &nbsp; </td>
																<td style="width:24%"> &nbsp; </td>
															</tr>
							           			 			<tr>
																<td colspan="5">&nbsp;</td>
															</tr>
															<tr>
										              			<td colspan="5" style="height:25px; font-weight:bold;">
										              				<label class="blue-light" style="font-weight:bold; width:100% !important;"> <strong> Certificate / Spares / Testing </strong> </label>
										              			</td>
							           			 			</tr>
							           			 			<tr>
																<td class="label-text" style="width:25%"> <strong> Witness Routine </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltWitnessRoutineId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Additional Test 1 </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltAdditionalTest1Id"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Additional Test 2 </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltAdditionalTest2Id"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Additional Test 3 </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltAdditionalTest3Id"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Type Test </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltTypeTestId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Certification </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltULCEId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Spares 1 </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltSpares1"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Spares 2 </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltSpares2"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Spares 3 </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltSpares3"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Spares 4 </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltSpares4"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Spares 5 </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltSpares5"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Data Sheet </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltDataSheet"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Add Drawing </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltAddDrawingId"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td style="width:25%"> &nbsp; </td>
																<td style="width:24%"> &nbsp; </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Additional Comments </strong> </td>
																<td colspan="4" class="formContentview-rating-b0x" style="width:75%;word-break:break-all;height:80px;"> <bean:write name="ratingObj" property="ltAdditionalComments"/> </td>
															</tr>
														</table>
													
													<%	iRatingIndex = iRatingIndex+1; %>
													
													</fieldset>
													
												</logic:iterate>
	                							
	                						</td>
	                					</tr>
	                					
                					<!-----------------------Rating block end ---------------------------------------------------->
                					
                					<!-----------------------Commercial Offer block start ---------------------------------------->
                					
                					<tr>
                						<td>
                							<fieldset>
                								<legend class="blueheader"> <bean:message key="quotation.offerdata.commercial.label.heading" /> </legend>
                							
                								<table width="100%" border="0" align="center" cellpadding="0" cellspacing="3">
													<tr>
														<td colspan="16">&nbsp;</td>
													</tr>
												
													<tr>
														<td style="width:1135px !important;overflow:scroll;float:left;">
															<table width="100%" border="0" align="center" cellpadding="0" cellspacing="3">
																<tr>
																	<td colspan="11">&nbsp;</td>
																</tr>
																
																<logic:present name="APPROVALTYPE" scope="request">
																
																	<!-- ------------------------------------ Commercial Offer : Normal : Start ----------------------------------- -->
																	<logic:equal name="APPROVALTYPE" scope="request" value="NORMAL_USER">
																		<tr>
																			<td width="3%" class="formLabelTophome"><bean:message key="quotation.create.label.itemno" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.label.admin.field.product.line" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.create.label.ratedoutput" /> ( <bean:message key="quotation.option.kw" /> )</td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.create.label.pole" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.create.label.ratedvoltage" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.search.frame" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.offerdata.addon.label.quantity" /></td>
																			<td width="5%" class="formLabelTophome">Standard Discount</td>
																			<td width="7%" class="formLabelTophome">Standard Unit Price</td>
																			<td width="7%" class="formLabelTophome">Standard Total price</td>
																		</tr>
																		<% 
																			Double dQuantity=new Double(0);
														                    Double dTotalTransportation=new Double(0);
														                    Double dTotalWarranty=new Double(0);
														                    Double dTotalTc=new Double(0);
														                    Double dMcnspRatio=new Double(0);
														                    BigDecimal bdListPrice=new BigDecimal("0");
														                    BigDecimal bdTotalPriceStd=new BigDecimal("0");
														                    String sListPrice="", sTotalPrice="";
																		%>
																		<logic:iterate name="newEnquiryDto" property="newRatingsList" id="ratingObject" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
																			<bean:define id="qnt" name="ratingObject" property="quantity"/>
								                      						<bean:define id="listPriceVal" name="ratingObject" property="ltLPPerUnit"/>
								                      						<bean:define id="priceVal" name="ratingObject" property="ltTotalOrderPrice"/>
								                      						<input type="hidden" id="ratingObjId<%=idx + 1%>" name="ratingObjId<%=idx + 1%>" value='<bean:write name="ratingObject" property="ratingId" />' >
								                      						<input type="hidden" id="quantity<%=idx + 1%>" name="quantity<%=idx + 1%>" value='<%=qnt%>' >
								                      						<% 
								                      							sListPrice=String.valueOf(listPriceVal);
								                      						
									                      						if(sListPrice!="" && sListPrice!=QuotationConstants.QUOTATION_ZERO) {
									                      							if(sListPrice.indexOf(",") != -1){
									                      								sListPrice = sListPrice.replaceAll(",", "");
																					}
									                      						}
								                      						%>
								                      						<tr>
								                      							<td width="3%" class="formContentview-rating-b0x"><%=idx + 1%></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltProdLineId" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltKWId" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltPoleId" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltVoltId" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltFrameId" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="quantity" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltStandardDiscount" /> %</td>
																				<td width="7%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltPricePerUnit" /></td>
																				<td width="7%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltTotalOrderPrice" /></td>
								                      						</tr>
								                      						<% 
															                    Double dQty=new Double(0);
															                    			                    
															                    sTotalPrice=String.valueOf(priceVal);
															                    
															                    if(qnt!="" && qnt != "0") {
															       					dQty = Double.parseDouble(String.valueOf(qnt));
															       					dQuantity = dQuantity + dQty;
																				}
															                    
															                    if(sTotalPrice!="" && sTotalPrice!=QuotationConstants.QUOTATION_ZERO) {
															                    	if(sTotalPrice.indexOf(",") != -1){
															                        	String unitprice=sTotalPrice.replaceAll(",", "");
															                    		BigDecimal a=new BigDecimal(unitprice);
															                    		bdTotalPriceStd=bdTotalPriceStd.add(a);      
																					} else {
															                          	BigDecimal a=new BigDecimal(sTotalPrice);
															                          	bdTotalPriceStd=bdTotalPriceStd.add(a);      
																					}
															                    }
															                    
																			%>
																		</logic:iterate>
																		<% 
																			sTotalPrice = CurrencyConvertUtility.buildPriceFormat(bdTotalPriceStd.toString());
																		%>
																		<tr>
																			<td colspan="5" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td align="left" class="formContentview-rating-b0x"><b><bean:message key="quotation.create.label.total"/></b></td>
							                       							<td class="formContentview-rating-b0x"> <b> <%=df1.format(dQuantity)%> </b> </td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td class="formContentview-rating-b0x"><b> &#8377;  <%=sTotalPrice%></b></td>
																		</tr>
																		<tr>
																			<td colspan="1" class="formContentview-rating-b0x"><b><bean:message key="quotation.create.label.inwords"/></b></td>
																			<td colspan="9" class="formContentview-rating-b0x" align="right">
																				<b>	<%=CurrencyConvertUtility.convertToIndianCurrency(String.valueOf(bdTotalPriceStd)) %> </b>
																			</td>
																		</tr>
																	</logic:equal>
																	<!-- ------------------------------------ Commercial Offer : Normal : End ----------------------------------- -->
																	
																	
																	<!-- ------------------------------------ Commercial Offer : SM : Start ----------------------------------- -->
																	<logic:equal name="APPROVALTYPE" scope="request" value="SM">
																		<tr>
																			<td width="3%" class="formLabelTophome"><bean:message key="quotation.create.label.itemno" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.label.admin.field.product.line" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.create.label.ratedoutput" /> ( <bean:message key="quotation.option.kw" /> )</td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.create.label.pole" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.create.label.ratedvoltage" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.search.frame" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.offerdata.addon.label.quantity" /></td>
																			<td width="5%" class="formLabelTophome">Standard Discount</td>
																			<td width="7%" class="formLabelTophome">Standard Unit Price</td>
																			<td width="7%" class="formLabelTophome">Standard Total price</td>
																			<td width="5%" class="formLabelTophome">Requested Discount (SM)</td>
																			<td width="7%" class="formLabelTophome">Requested Unit Price (SM)</td>
																			<td width="7%" class="formLabelTophome">Requested Total price (SM)</td>
																			<td width="5%" class="formLabelTophome">Approved Discount (RSM)</td>
																			<td width="7%" class="formLabelTophome">Approved Unit Price (RSM)</td>
																			<td width="7%" class="formLabelTophome">Approved Total price (RSM)</td>
																			<td width="5%" class="formLabelTophome">Quoted Discount</td>
																			<td width="7%" class="formLabelTophome">Quoted Unit Price</td>
																			<td width="7%" class="formLabelTophome">Quoted Total price</td>
																		</tr>
																		<% 
																			Double dQuantity=new Double(0);
														                    Double dTotalTransportation=new Double(0);
														                    Double dTotalWarranty=new Double(0);
														                    Double dTotalTc=new Double(0);
														                    Double dMcnspRatio=new Double(0);
														                    BigDecimal bdListPrice=new BigDecimal("0");
														                    BigDecimal bdTotalPriceStd=new BigDecimal("0");
														                    BigDecimal bdTotalPriceReq=new BigDecimal("0");
														                    BigDecimal bdTotalPriceAprv=new BigDecimal("0");
														                    BigDecimal bdTotalPriceQuoted=new BigDecimal("0");
														                    String sListPrice="", sTotalPrice="", sReqTotalPrice="", sAprvTotalPrice="", sQuotedTotalPrice="";
																		%>
																		<logic:iterate name="newEnquiryDto" property="newRatingsList" id="ratingObject" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
																			<bean:define id="qnt" name="ratingObject" property="quantity"/>
								                      						<bean:define id="listPriceVal" name="ratingObject" property="ltLPPerUnit"/>
								                      						<bean:define id="priceVal" name="ratingObject" property="ltTotalOrderPrice"/>
								                      						<bean:define id="reqPriceVal" name="ratingObject" property="ltRequestedTotalPrice_SM"/>
								                      						<bean:define id="aprvPriceVal" name="ratingObject" property="ltApprovedTotalPrice_RSM"/>
								                      						<bean:define id="quotedPriceVal" name="ratingObject" property="ltQuotedTotalPrice"/>
																			<% 
								                      							sListPrice=String.valueOf(listPriceVal);
								                      						
									                      						if(sListPrice!="" && sListPrice!=QuotationConstants.QUOTATION_ZERO) {
									                      							if(sListPrice.indexOf(",") != -1){
									                      								sListPrice = sListPrice.replaceAll(",", "");
																					}
									                      						}
								                      						%>
								                      						<tr>
								                      							<td width="3%" class="formContentview-rating-b0x"><%=idx + 1%></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltProdLineId" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltKWId" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltPoleId" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltVoltId" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltFrameId" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="quantity" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltStandardDiscount" /> %</td>
																				<td width="7%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltPricePerUnit" /></td>
																				<td width="7%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltTotalOrderPrice" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltRequestedDiscount_SM" /> %</td>
																				<td width="7%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltRequestedPricePerUnit_SM" /></td>
																				<td width="7%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltRequestedTotalPrice_SM" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltApprovedDiscount_RSM" /> %</td>
																				<td width="7%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltApprovedPricePerunit_RSM" /></td>
																				<td width="7%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltApprovedTotalPrice_RSM" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltQuotedDiscount" /> %</td>
																				<td width="7%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltQuotedPricePerUnit" /></td>
																				<td width="7%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltQuotedTotalPrice" /></td>
								                      						</tr>
								                      						<% 
															                    Double dQty=new Double(0);
															                    			                    
															                    sTotalPrice=String.valueOf(priceVal);
															                    sReqTotalPrice=String.valueOf(reqPriceVal);
															                    sAprvTotalPrice=String.valueOf(aprvPriceVal);
															                    sQuotedTotalPrice=String.valueOf(quotedPriceVal);
															                    
															                    if(qnt!="" && qnt != "0") {
															       					dQty = Double.parseDouble(String.valueOf(qnt));
															       					dQuantity = dQuantity + dQty;
																				}
															                    
															                    if(sTotalPrice!="" && sTotalPrice!=QuotationConstants.QUOTATION_ZERO) {
															                    	if(sTotalPrice.indexOf(",") != -1){
															                        	String unitprice=sTotalPrice.replaceAll(",", "");
															                    		BigDecimal a=new BigDecimal(unitprice);
															                    		bdTotalPriceStd=bdTotalPriceStd.add(a);      
																					} else {
															                          	BigDecimal a=new BigDecimal(sTotalPrice);
															                          	bdTotalPriceStd=bdTotalPriceStd.add(a);      
																					}
															                    }
															                    
															                    if(sReqTotalPrice!="" && sReqTotalPrice!=QuotationConstants.QUOTATION_ZERO) {
															                    	if(sReqTotalPrice.indexOf(",") != -1){
															                        	String unitprice=sReqTotalPrice.replaceAll(",", "");
															                    		BigDecimal a=new BigDecimal(unitprice);
															                    		bdTotalPriceReq=bdTotalPriceReq.add(a);      
																					} else {
															                          	BigDecimal a=new BigDecimal(sReqTotalPrice);
															                          	bdTotalPriceReq=bdTotalPriceReq.add(a);      
																					}
															                    }
															                    
															                    if(sAprvTotalPrice!="" && sAprvTotalPrice!=QuotationConstants.QUOTATION_ZERO) {
															                    	if(sAprvTotalPrice.indexOf(",") != -1){
															                        	String unitprice=sAprvTotalPrice.replaceAll(",", "");
															                    		BigDecimal a=new BigDecimal(unitprice);
															                    		bdTotalPriceAprv=bdTotalPriceAprv.add(a);      
																					} else {
															                          	BigDecimal a=new BigDecimal(sAprvTotalPrice);
															                          	bdTotalPriceAprv=bdTotalPriceAprv.add(a);      
																					}
															                    }
															                    
															                    if(sQuotedTotalPrice!="" && sQuotedTotalPrice!=QuotationConstants.QUOTATION_ZERO) {
															                    	if(sQuotedTotalPrice.indexOf(",") != -1){
															                    		String unitprice=sQuotedTotalPrice.replaceAll(",", "");
															                    		BigDecimal a=new BigDecimal(unitprice);
															                    		bdTotalPriceQuoted=bdTotalPriceQuoted.add(a);
															                    	} else {
															                    		BigDecimal a=new BigDecimal(sQuotedTotalPrice);
															                    		bdTotalPriceQuoted=bdTotalPriceQuoted.add(a);  
															                    	}
															                    }
																			%>
																		</logic:iterate>
																		<% 
																			sTotalPrice = CurrencyConvertUtility.buildPriceFormat(bdTotalPriceStd.toString());
																			sReqTotalPrice = CurrencyConvertUtility.buildPriceFormat(bdTotalPriceReq.toString());
																			sAprvTotalPrice = CurrencyConvertUtility.buildPriceFormat(bdTotalPriceAprv.toString());
																			sQuotedTotalPrice = CurrencyConvertUtility.buildPriceFormat(bdTotalPriceQuoted.toString());
																		%>
																		<tr>
																			<td colspan="5" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td align="left" class="formContentview-rating-b0x"><b><bean:message key="quotation.create.label.total"/></b></td>
							                       							<td class="formContentview-rating-b0x"> <b> <%=df1.format(dQuantity)%> </b> </td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td class="formContentview-rating-b0x"><b> &#8377;  <%=sTotalPrice%></b></td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td class="formContentview-rating-b0x"><b> &#8377;  <%=sReqTotalPrice%></b></td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td class="formContentview-rating-b0x"><b> &#8377;  <%=sAprvTotalPrice%></b></td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td class="formContentview-rating-b0x"><b> &#8377;  <%=sQuotedTotalPrice%></b></td>
																		</tr>
																		<tr>
																			<td colspan="1" class="formContentview-rating-b0x"><b><bean:message key="quotation.create.label.inwords"/></b></td>
																			<td colspan="18" class="formContentview-rating-b0x" align="right">
																				<b>	<%=CurrencyConvertUtility.convertToIndianCurrency(String.valueOf(bdTotalPriceStd)) %> </b>
																			</td>
																		</tr>
																	</logic:equal>
																	<!-- ------------------------------------ Commercial Offer : SM : End -------------------------------------- -->
																	
																	
																	<!-- ------------------------------------ Commercial Offer : RSM : Start ----------------------------------- -->
																	<logic:equal name="APPROVALTYPE" scope="request" value="RSM">
																		<tr>
																			<td width="3%" class="formLabelTophome"><bean:message key="quotation.create.label.itemno" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.label.admin.field.product.line" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.create.label.ratedoutput" /> ( <bean:message key="quotation.option.kw" /> )</td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.create.label.pole" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.create.label.ratedvoltage" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.search.frame" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.offerdata.addon.label.quantity" /></td>
																			<td width="5%" class="formLabelTophome">Standard Discount</td>
																			<td width="7%" class="formLabelTophome">Standard Unit Price</td>
																			<td width="7%" class="formLabelTophome">Standard Total price</td>
																			<td width="5%" class="formLabelTophome">Requested Discount (SM)</td>
																			<td width="7%" class="formLabelTophome">Requested Unit Price (SM)</td>
																			<td width="7%" class="formLabelTophome">Requested Total Price (SM)</td>
																			<td width="5%" class="formLabelTophome">Requested Discount (RSM)</td>
																			<td width="7%" class="formLabelTophome">Requested Unit Price (RSM)</td>
																			<td width="7%" class="formLabelTophome">Requested Total Price (RSM)</td>
																			<td width="5%" class="formLabelTophome">Approved Discount (RSM)</td>
																			<td width="7%" class="formLabelTophome">Approved Unit Price (RSM)</td>
																			<td width="7%" class="formLabelTophome">Approved Total Price (RSM)</td>
																			<td width="5%" class="formLabelTophome">Approved Discount (NSM)</td>
																			<td width="5%" class="formLabelTophome">Quoted Discount</td>
																			<td width="7%" class="formLabelTophome">Quoted Unit Price</td>
																			<td width="7%" class="formLabelTophome">Quoted Total price</td>
																		</tr>
																		<% 
																			Double dQuantity=new Double(0);
														                    Double dTotalTransportation=new Double(0);
														                    Double dTotalWarranty=new Double(0);
														                    Double dTotalTc=new Double(0);
														                    Double dMcnspRatio=new Double(0);
														                    BigDecimal bdListPrice=new BigDecimal("0");
														                    BigDecimal bdTotalPriceStd=new BigDecimal("0");
														                    BigDecimal bdTotalPriceReq_SM=new BigDecimal("0");
														                    BigDecimal bdTotalPriceReq_RSM=new BigDecimal("0");
														                    BigDecimal bdTotalPriceAprv=new BigDecimal("0");
														                    BigDecimal bdTotalPriceQuoted=new BigDecimal("0");
														                    String sListPrice="", sTotalPrice="", sReqTotalPrice_SM="", sReqTotalPrice_RSM="", sAprvTotalPrice="", sQuotedTotalPrice="";
																		%>
																		<logic:iterate name="newEnquiryDto" property="newRatingsList" id="ratingObject" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
								                      						<bean:define id="qnt" name="ratingObject" property="quantity"/>
								                      						<bean:define id="listPriceVal" name="ratingObject" property="ltLPPerUnit"/>
								                      						<bean:define id="priceVal" name="ratingObject" property="ltTotalOrderPrice"/>
								                      						<bean:define id="reqPriceValSM" name="ratingObject" property="ltRequestedTotalPrice_SM"/>
								                      						<bean:define id="reqPriceValRSM" name="ratingObject" property="ltRequestedTotalPrice_RSM"/>
								                      						<bean:define id="aprvPriceVal" name="ratingObject" property="ltApprovedTotalPrice_RSM"/>
								                      						<bean:define id="quotedPriceVal" name="ratingObject" property="ltQuotedTotalPrice"/>
								                      						<% 
								                      							sListPrice=String.valueOf(listPriceVal);
								                      						
									                      						if(sListPrice!="" && sListPrice!=QuotationConstants.QUOTATION_ZERO) {
									                      							if(sListPrice.indexOf(",") != -1){
									                      								sListPrice = sListPrice.replaceAll(",", "");
																					}
									                      						}
								                      						%>
								                      						<tr>
								                      							<td width="3%" class="formContentview-rating-b0x"><%=idx + 1%></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltProdLineId" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltKWId" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltPoleId" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltVoltId" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltFrameId" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="quantity" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltStandardDiscount" /> %</td>
																				<td width="7%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltPricePerUnit" /></td>
																				<td width="7%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltTotalOrderPrice" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltRequestedDiscount_SM" /> %</td>
																				<td width="7%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltRequestedPricePerUnit_SM" /></td>
																				<td width="7%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltRequestedTotalPrice_SM" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltRequestedDiscount_RSM" /> %</td>
																				<td width="7%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltRequestedPricePerUnit_RSM" /></td>
																				<td width="7%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltRequestedTotalPrice_RSM" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltApprovedDiscount_RSM" /> %</td>
																				<td width="7%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltApprovedPricePerunit_RSM" /></td>
																				<td width="7%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltApprovedTotalPrice_RSM" /></td>
																				<td width="3%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltApprovedDiscount_NSM" /> %</td>
																				<td width="3%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltQuotedDiscount" /> %</td>
																				<td width="10%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltQuotedPricePerUnit" /></td>
																				<td width="10%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltQuotedTotalPrice" /></td>
																			</tr>
																			<% 
															                    Double dQty=new Double(0);
															                    			                    
															                    sTotalPrice=String.valueOf(priceVal);
															                    sReqTotalPrice_SM=String.valueOf(reqPriceValSM);
															                    sReqTotalPrice_RSM=String.valueOf(reqPriceValRSM);
															                    sAprvTotalPrice=String.valueOf(aprvPriceVal);
															                    sQuotedTotalPrice=String.valueOf(quotedPriceVal);
															                    
															                    if(qnt!="" && qnt != "0") {
															       					dQty = Double.parseDouble(String.valueOf(qnt));
															       					dQuantity = dQuantity + dQty;
																				}
															                    
															                    if(sTotalPrice!="" && sTotalPrice!=QuotationConstants.QUOTATION_ZERO) {
															                    	if(sTotalPrice.indexOf(",") != -1){
															                        	String unitprice=sTotalPrice.replaceAll(",", "");
															                    		BigDecimal a=new BigDecimal(unitprice);
															                    		bdTotalPriceStd=bdTotalPriceStd.add(a);      
																					} else {
															                          	BigDecimal a=new BigDecimal(sTotalPrice);
															                          	bdTotalPriceStd=bdTotalPriceStd.add(a);      
																					}
															                    }
															                    
															                    if(sReqTotalPrice_SM!="" && sReqTotalPrice_SM!=QuotationConstants.QUOTATION_ZERO) {
															                    	if(sReqTotalPrice_SM.indexOf(",") != -1){
															                        	String unitprice=sReqTotalPrice_SM.replaceAll(",", "");
															                    		BigDecimal a=new BigDecimal(unitprice);
															                    		bdTotalPriceReq_SM=bdTotalPriceReq_SM.add(a);      
																					} else {
															                          	BigDecimal a=new BigDecimal(sReqTotalPrice_SM);
															                          	bdTotalPriceReq_SM=bdTotalPriceReq_SM.add(a);      
																					}
															                    }
															                    
															                    if(sReqTotalPrice_RSM!="" && sReqTotalPrice_RSM!=QuotationConstants.QUOTATION_ZERO) {
															                    	if(sReqTotalPrice_RSM.indexOf(",") != -1){
															                        	String unitprice=sReqTotalPrice_RSM.replaceAll(",", "");
															                    		BigDecimal a=new BigDecimal(unitprice);
															                    		bdTotalPriceReq_RSM=bdTotalPriceReq_RSM.add(a);      
																					} else {
															                          	BigDecimal a=new BigDecimal(sReqTotalPrice_RSM);
															                          	bdTotalPriceReq_RSM=bdTotalPriceReq_RSM.add(a);      
																					}
															                    }
															                    
															                    if(sAprvTotalPrice!="" && sAprvTotalPrice!=QuotationConstants.QUOTATION_ZERO) {
															                    	if(sTotalPrice.indexOf(",") != -1){
															                        	String unitprice=sAprvTotalPrice.replaceAll(",", "");
															                    		BigDecimal a=new BigDecimal(unitprice);
															                    		bdTotalPriceAprv=bdTotalPriceAprv.add(a);      
																					} else {
															                          	BigDecimal a=new BigDecimal(sAprvTotalPrice);
															                          	bdTotalPriceAprv=bdTotalPriceAprv.add(a);      
																					}
															                    }
															                    
															                    if(sQuotedTotalPrice!="" && sQuotedTotalPrice!=QuotationConstants.QUOTATION_ZERO) {
															                    	if(sQuotedTotalPrice.indexOf(",") != -1){
															                    		String unitprice=sQuotedTotalPrice.replaceAll(",", "");
															                    		BigDecimal a=new BigDecimal(unitprice);
															                    		bdTotalPriceQuoted=bdTotalPriceQuoted.add(a);
															                    	} else {
															                    		BigDecimal a=new BigDecimal(sQuotedTotalPrice);
															                    		bdTotalPriceQuoted=bdTotalPriceQuoted.add(a);  
															                    	}
															                    }
																			%>
																		</logic:iterate>
																		<% 
																			sTotalPrice = CurrencyConvertUtility.buildPriceFormat(bdTotalPriceStd.toString());
																			sReqTotalPrice_SM = CurrencyConvertUtility.buildPriceFormat(bdTotalPriceReq_SM.toString());
																			sReqTotalPrice_RSM = CurrencyConvertUtility.buildPriceFormat(bdTotalPriceReq_RSM.toString());
																			sAprvTotalPrice = CurrencyConvertUtility.buildPriceFormat(bdTotalPriceAprv.toString());
																			sQuotedTotalPrice = CurrencyConvertUtility.buildPriceFormat(bdTotalPriceQuoted.toString());
																		%>
																		<tr>
																			<td colspan="5" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td align="left" class="formContentview-rating-b0x"><b><bean:message key="quotation.create.label.total"/></b></td>
							                       							<td class="formContentview-rating-b0x"> <b> <%=df1.format(dQuantity)%> </b> </td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td class="formContentview-rating-b0x"><b> &#8377;  <%=sTotalPrice%></b></td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td class="formContentview-rating-b0x"><b> &#8377;  <%=sReqTotalPrice_SM%> </b></td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td class="formContentview-rating-b0x"><b> &#8377;  <%=sReqTotalPrice_RSM%> </b></td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td class="formContentview-rating-b0x"><b> &#8377;  <%=sAprvTotalPrice%> </b></td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td class="formContentview-rating-b0x"><b> &#8377;  <%=sQuotedTotalPrice%></b></td>
																		</tr>
																		<tr>
																			<td colspan="1" class="formContentview-rating-b0x"><b><bean:message key="quotation.create.label.inwords"/></b></td>
																			<td colspan="22" class="formContentview-rating-b0x" align="right">
																				<b>	<%=CurrencyConvertUtility.convertToIndianCurrency(String.valueOf(bdTotalPriceStd)) %> </b>
																			</td>
																		</tr>
																	</logic:equal>
																	<!-- ------------------------------------ Commercial Offer : RSM : End ------------------------------------- -->
																	
																	
																	<!-- ------------------------------------ Commercial Offer : NSM : Start ----------------------------------- -->
																	<logic:equal name="APPROVALTYPE" scope="request" value="NSM">
																		<tr>
																			<td width="3%" class="formLabelTophome"><bean:message key="quotation.create.label.itemno" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.label.admin.field.product.line" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.create.label.ratedoutput" /> ( <bean:message key="quotation.option.kw" /> )</td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.create.label.pole" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.create.label.ratedvoltage" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.search.frame" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.offerdata.addon.label.quantity" /></td>
																			<td width="5%" class="formLabelTophome">Standard Discount</td>
																			<td width="7%" class="formLabelTophome">Standard Unit Price</td>
																			<td width="7%" class="formLabelTophome">Standard Total price</td>
																			<td width="5%" class="formLabelTophome">Requested Discount (RSM)</td>
																			<td width="7%" class="formLabelTophome">Requested Unit Price (RSM)</td>
																			<td width="7%" class="formLabelTophome">Requested Total Price (RSM)</td>
																			<td width="5%" class="formLabelTophome">Requested Discount (NSM)</td>
																			<td width="7%" class="formLabelTophome">Requested Unit Price (NSM)</td>
																			<td width="7%" class="formLabelTophome">Requested Total Price (NSM)</td>
																			<td width="5%" class="formLabelTophome">Approved Discount (NSM)</td>
																			<td width="7%" class="formLabelTophome">Approved Unit Price (NSM)</td>
																			<td width="7%" class="formLabelTophome">Approved Total Price (NSM)</td>
																			<td width="5%" class="formLabelTophome">Approved Discount (MH)</td>
																			<td width="5%" class="formLabelTophome">Quoted Discount</td>
																			<td width="7%" class="formLabelTophome">Quoted Unit Price</td>
																			<td width="7%" class="formLabelTophome">Quoted Total Price</td>
																		</tr>
																		<% 
																			Double dQuantity=new Double(0);
														                    Double dTotalTransportation=new Double(0);
														                    Double dTotalWarranty=new Double(0);
														                    Double dTotalTc=new Double(0);
														                    Double dMcnspRatio=new Double(0);
														                    BigDecimal bdListPrice=new BigDecimal("0");
														                    BigDecimal bdTotalPriceStd=new BigDecimal("0");
														                    BigDecimal bdTotalPriceReq_RSM=new BigDecimal("0");
														                    BigDecimal bdTotalPriceReq_NSM=new BigDecimal("0");
														                    BigDecimal bdTotalPriceAprv=new BigDecimal("0");
														                    BigDecimal bdTotalPriceQuoted=new BigDecimal("0");
														                    String sListPrice="", sTotalPrice="", sReqTotalPrice_RSM="", sReqTotalPrice_NSM="", sAprvTotalPrice="", sQuotedTotalPrice="", sSupervisionCharges="";
																		%>
																		<logic:iterate name="newEnquiryDto" property="newRatingsList" id="ratingObject" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
								                      						<bean:define id="qnt" name="ratingObject" property="quantity"/>
								                      						<bean:define id="listPriceVal" name="ratingObject" property="ltLPPerUnit"/>
								                      						<bean:define id="priceVal" name="ratingObject" property="ltTotalOrderPrice"/>
								                      						<bean:define id="reqPriceValRSM" name="ratingObject" property="ltRequestedTotalPrice_RSM"/>
								                      						<bean:define id="reqPriceValNSM" name="ratingObject" property="ltRequestedTotalPrice_NSM"/>
								                      						<bean:define id="aprvPriceVal" name="ratingObject" property="ltApprovedTotalPrice_NSM"/>
								                      						<bean:define id="quotedPriceVal" name="ratingObject" property="ltQuotedTotalPrice"/>
								                      						<% 
								                      							sListPrice=String.valueOf(listPriceVal);
								                      						
									                      						if(sListPrice!="" && sListPrice!=QuotationConstants.QUOTATION_ZERO) {
									                      							if(sListPrice.indexOf(",") != -1){
									                      								sListPrice = sListPrice.replaceAll(",", "");
																					}
									                      						}
								                      						%>
								                      						<tr>
								                      							<td width="3%" class="formContentview-rating-b0x"><%=idx + 1%></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltProdLineId" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltKWId" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltPoleId" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltVoltId" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltFrameId" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="quantity" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltStandardDiscount" /> %</td>
																				<td width="7%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltPricePerUnit" /></td>
																				<td width="7%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltTotalOrderPrice" /></td>
																				<td width="3%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltRequestedDiscount_RSM" /> %</td>
																				<td width="10%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltRequestedPricePerUnit_RSM" /></td>
																				<td width="10%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltRequestedTotalPrice_RSM" /></td>
																				<td width="3%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltRequestedDiscount_NSM" /> %</td>
																				<td width="10%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltRequestedPricePerUnit_NSM" /></td>
																				<td width="10%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltRequestedTotalPrice_NSM" /></td>
																				<td width="3%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltApprovedDiscount_NSM" /> %</td>
																				<td width="10%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltApprovedPricePerunit_NSM" /></td>
																				<td width="10%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltApprovedTotalPrice_NSM" /></td>
																				<td width="3%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltApprovedDiscount_MH" /> %</td>
																				<td width="3%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltQuotedDiscount" /> %</td>
																				<td width="10%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltQuotedPricePerUnit" /></td>
																				<td width="10%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltQuotedTotalPrice" /></td>
																			</tr>
																			<% 
															                    Double dQty=new Double(0);
															                    			                    
															                    sTotalPrice=String.valueOf(priceVal);
															                    sReqTotalPrice_RSM=String.valueOf(reqPriceValRSM);
															                    sReqTotalPrice_NSM=String.valueOf(reqPriceValNSM);
															                    sAprvTotalPrice=String.valueOf(aprvPriceVal);
															                    sQuotedTotalPrice=String.valueOf(quotedPriceVal);
															                    
															                    if(qnt!="" && qnt != "0") {
															       					dQty = Double.parseDouble(String.valueOf(qnt));
															       					dQuantity = dQuantity + dQty;
																				}
															                    
															                    if(sTotalPrice!="" && sTotalPrice!=QuotationConstants.QUOTATION_ZERO) {
															                    	if(sTotalPrice.indexOf(",") != -1){
															                        	String unitprice=sTotalPrice.replaceAll(",", "");
															                    		BigDecimal a=new BigDecimal(unitprice);
															                    		bdTotalPriceStd=bdTotalPriceStd.add(a);      
																					} else {
															                          	BigDecimal a=new BigDecimal(sTotalPrice);
															                          	bdTotalPriceStd=bdTotalPriceStd.add(a);      
																					}
															                    }
															                    
															                    if(sReqTotalPrice_RSM!="" && sReqTotalPrice_RSM!=QuotationConstants.QUOTATION_ZERO) {
															                    	if(sReqTotalPrice_RSM.indexOf(",") != -1){
															                        	String unitprice=sReqTotalPrice_RSM.replaceAll(",", "");
															                    		BigDecimal a=new BigDecimal(unitprice);
															                    		bdTotalPriceReq_RSM=bdTotalPriceReq_RSM.add(a);      
																					} else {
															                          	BigDecimal a=new BigDecimal(sReqTotalPrice_RSM);
															                          	bdTotalPriceReq_RSM=bdTotalPriceReq_RSM.add(a);      
																					}
															                    }
															                    
															                    if(sReqTotalPrice_NSM!="" && sReqTotalPrice_NSM!=QuotationConstants.QUOTATION_ZERO) {
															                    	if(sReqTotalPrice_NSM.indexOf(",") != -1){
															                        	String unitprice=sReqTotalPrice_NSM.replaceAll(",", "");
															                    		BigDecimal a=new BigDecimal(unitprice);
															                    		bdTotalPriceReq_NSM=bdTotalPriceReq_NSM.add(a);      
																					} else {
															                          	BigDecimal a=new BigDecimal(sReqTotalPrice_NSM);
															                          	bdTotalPriceReq_NSM=bdTotalPriceReq_NSM.add(a);      
																					}
															                    }
															                    
															                    if(sAprvTotalPrice!="" && sAprvTotalPrice!=QuotationConstants.QUOTATION_ZERO) {
															                    	if(sTotalPrice.indexOf(",") != -1){
															                        	String unitprice=sAprvTotalPrice.replaceAll(",", "");
															                    		BigDecimal a=new BigDecimal(unitprice);
															                    		bdTotalPriceAprv=bdTotalPriceAprv.add(a);      
																					} else {
															                          	BigDecimal a=new BigDecimal(sAprvTotalPrice);
															                          	bdTotalPriceAprv=bdTotalPriceAprv.add(a);      
																					}
															                    }
															                    
															                    if(sQuotedTotalPrice!="" && sQuotedTotalPrice!=QuotationConstants.QUOTATION_ZERO) {
															                    	if(sQuotedTotalPrice.indexOf(",") != -1){
															                    		String unitprice=sQuotedTotalPrice.replaceAll(",", "");
															                    		BigDecimal a=new BigDecimal(unitprice);
															                    		bdTotalPriceQuoted=bdTotalPriceQuoted.add(a);
															                    	} else {
															                    		BigDecimal a=new BigDecimal(sQuotedTotalPrice);
															                    		bdTotalPriceQuoted=bdTotalPriceQuoted.add(a);  
															                    	}
															                    }
																			%>
																		</logic:iterate>
																		<tr>
																			<td colspan="23" class="formContentview-rating-b0x">&nbsp;</td>
																		</tr>
																		<logic:equal name="newEnquiryDto" property="supervisionDetails" value="Yes">
																			<bean:define id="superviseManDays" name="newEnquiryDto" property="superviseNoOfManDays"/>
																			<%
																				int iSuperviseManDays = Integer.parseInt(String.valueOf(superviseManDays));
																				long supervisionCharges = iSuperviseManDays * QuotationConstants.QUOTATIONLT_MANDAY_SUPERVISE_CHARGES;
																				BigDecimal a=new BigDecimal(supervisionCharges);
																				bdTotalPriceStd = bdTotalPriceStd.add(a);
																				if(sReqTotalPrice_RSM!="" && sReqTotalPrice_RSM!=QuotationConstants.QUOTATION_ZERO) {
																					bdTotalPriceReq_RSM = bdTotalPriceReq_RSM.add(a);
																				}
																				if(sReqTotalPrice_NSM!="" && sReqTotalPrice_NSM!=QuotationConstants.QUOTATION_ZERO) {
																					bdTotalPriceReq_NSM = bdTotalPriceReq_NSM.add(a);
																				}
																				if(sAprvTotalPrice!="" && sAprvTotalPrice!=QuotationConstants.QUOTATION_ZERO) {
																					bdTotalPriceAprv = bdTotalPriceAprv.add(a);
																				}
																				if(sQuotedTotalPrice!="" && sQuotedTotalPrice!=QuotationConstants.QUOTATION_ZERO) {
																					bdTotalPriceQuoted = bdTotalPriceQuoted.add(a);
																				}
																				sSupervisionCharges = CurrencyConvertUtility.buildPriceFormat(String.valueOf(supervisionCharges));
																			%>
																			<tr>
																				<td colspan="2" class="formContentview-rating-b0x">&nbsp;</td>
																				<td colspan="4" align="left" class="formContentview-rating-b0x"><b> Supervision and Comissioning charges </b></td>
																				<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
																				<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
																				<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
																				<td class="formContentview-rating-b0x"><b> &#8377; <%=sSupervisionCharges%> </b></td>
																				<td colspan="13" class="formContentview-rating-b0x">&nbsp;</td>
																			</tr>
																		</logic:equal>
																		<% 
																			sTotalPrice = CurrencyConvertUtility.buildPriceFormat(bdTotalPriceStd.toString());
																			sReqTotalPrice_RSM = CurrencyConvertUtility.buildPriceFormat(bdTotalPriceReq_RSM.toString());
																			sReqTotalPrice_NSM = CurrencyConvertUtility.buildPriceFormat(bdTotalPriceReq_NSM.toString());
																			sAprvTotalPrice = CurrencyConvertUtility.buildPriceFormat(bdTotalPriceAprv.toString());
																			sQuotedTotalPrice = CurrencyConvertUtility.buildPriceFormat(bdTotalPriceQuoted.toString());
																		%>
																		<tr>
																			<td colspan="5" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td align="left" class="formContentview-rating-b0x"><b><bean:message key="quotation.create.label.total"/></b></td>
							                       							<td class="formContentview-rating-b0x"> <b> <%=df1.format(dQuantity)%> </b> </td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td class="formContentview-rating-b0x"><b> &#8377;  <%=sTotalPrice%></b></td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td class="formContentview-rating-b0x"><b> &#8377;  <%=sReqTotalPrice_RSM%></b></td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td class="formContentview-rating-b0x"><b> &#8377;  <%=sReqTotalPrice_NSM%> </b></td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td class="formContentview-rating-b0x"><b> &#8377;  <%=sAprvTotalPrice%> </b></td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td class="formContentview-rating-b0x"><b> &#8377;  <%=sQuotedTotalPrice%></b></td>
																		</tr>
																		<tr>
																			<td colspan="1" class="formContentview-rating-b0x"><b><bean:message key="quotation.create.label.inwords"/></b></td>
																			<td colspan="22" class="formContentview-rating-b0x" align="right">
																				<b>	<%=CurrencyConvertUtility.convertToIndianCurrency(String.valueOf(bdTotalPriceStd)) %> </b>
																			</td>
																		</tr>
																	</logic:equal>
																	<!-- ------------------------------------ Commercial Offer : NSM : End ------------------------------------- -->
																	
																	
																	<!-- ------------------------------------ Commercial Offer : MH : Start ----------------------------------- -->
																	<logic:equal name="APPROVALTYPE" scope="request" value="MH">
																		<tr>
																			<td width="3%" class="formLabelTophome"><bean:message key="quotation.create.label.itemno" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.label.admin.field.product.line" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.create.label.ratedoutput" /> ( <bean:message key="quotation.option.kw" /> )</td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.create.label.pole" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.create.label.ratedvoltage" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.search.frame" /></td>
																			<td width="5%" class="formLabelTophome"><bean:message key="quotation.offerdata.addon.label.quantity" /></td>
																			<td width="5%" class="formLabelTophome">Standard Discount</td>
																			<td width="7%" class="formLabelTophome">Standard Unit Price</td>
																			<td width="7%" class="formLabelTophome">Standard Total price</td>
																			<td width="5%" class="formLabelTophome">Requested Discount (NSM)</td>
																			<td width="7%" class="formLabelTophome">Requested Unit Price (NSM)</td>
																			<td width="7%" class="formLabelTophome">Requested Total Price (NSM)</td>
																			<td width="5%" class="formLabelTophome">Approved Discount (MH)</td>
																			<td width="7%" class="formLabelTophome">Approved Unit Price (MH)</td>
																			<td width="7%" class="formLabelTophome">Approved Total Price (MH)</td>
																			<td width="5%" class="formLabelTophome">Quoted Discount</td>
																			<td width="7%" class="formLabelTophome">Quoted Unit Price</td>
																			<td width="7%" class="formLabelTophome">Quoted Total Price</td>
																		</tr>
																		<% 
																			Double dQuantity=new Double(0);
														                    Double dTotalTransportation=new Double(0);
														                    Double dTotalWarranty=new Double(0);
														                    Double dTotalTc=new Double(0);
														                    Double dMcnspRatio=new Double(0);
														                    BigDecimal bdListPrice=new BigDecimal("0");
														                    BigDecimal bdTotalPriceStd=new BigDecimal("0");
														                    BigDecimal bdTotalPriceReq=new BigDecimal("0");
														                    BigDecimal bdTotalPriceAprv=new BigDecimal("0");
														                    BigDecimal bdTotalPriceQuoted=new BigDecimal("0");
														                    String sListPrice="", sTotalPrice="", sReqTotalPrice="", sAprvTotalPrice="", sQuotedTotalPrice="";
																		%>
																		<logic:iterate name="newEnquiryDto" property="newRatingsList" id="ratingObject" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
								                      						<bean:define id="qnt" name="ratingObject" property="quantity"/>
								                      						<bean:define id="listPriceVal" name="ratingObject" property="ltLPPerUnit"/>
								                      						<bean:define id="priceVal" name="ratingObject" property="ltTotalOrderPrice"/>
								                      						<bean:define id="reqPriceVal" name="ratingObject" property="ltRequestedTotalPrice_NSM"/>
								                      						<bean:define id="aprvPriceVal" name="ratingObject" property="ltApprovedTotalPrice_MH"/>
								                      						<bean:define id="quotedPriceVal" name="ratingObject" property="ltQuotedTotalPrice"/>
								                      						<% 
								                      							sListPrice=String.valueOf(listPriceVal);
								                      						
									                      						if(sListPrice!="" && sListPrice!=QuotationConstants.QUOTATION_ZERO) {
									                      							if(sListPrice.indexOf(",") != -1){
									                      								sListPrice = sListPrice.replaceAll(",", "");
																					}
									                      						}
								                      						%>
								                      						<tr>
								                      							<td width="3%" class="formContentview-rating-b0x"><%=idx + 1%></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltProdLineId" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltKWId" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltPoleId" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltVoltId" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltFrameId" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="quantity" /></td>
																				<td width="5%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltStandardDiscount" /> %</td>
																				<td width="7%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltPricePerUnit" /></td>
																				<td width="7%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltTotalOrderPrice" /></td>
																				<td width="3%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltRequestedDiscount_NSM" /> %</td>
																				<td width="10%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltRequestedPricePerUnit_NSM" /></td>
																				<td width="10%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltRequestedTotalPrice_NSM" /></td> 
																				<td width="3%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltApprovedDiscount_MH" /> %</td>
																				<td width="10%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltApprovedPricePerunit_MH" /></td>
																				<td width="10%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltApprovedTotalPrice_MH" /></td>
																				<td width="3%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ltQuotedDiscount" /> %</td>
																				<td width="10%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltQuotedPricePerUnit" /></td>
																				<td width="10%" class="formContentview-rating-b0x"> &#8377; <bean:write name="ratingObject" property="ltQuotedTotalPrice" /></td>
																			</tr>
																			<% 
															                    Double dQty=new Double(0);
															                    			                    
															                    sTotalPrice=String.valueOf(priceVal);
															                    sReqTotalPrice=String.valueOf(reqPriceVal);
															                    sAprvTotalPrice=String.valueOf(aprvPriceVal);
															                    
															                    if(qnt!="" && qnt != "0") {
															       					dQty = Double.parseDouble(String.valueOf(qnt));
															       					dQuantity = dQuantity + dQty;
																				}
															                    
															                    if(sTotalPrice!="" && sTotalPrice!=QuotationConstants.QUOTATION_ZERO) {
															                    	if(sTotalPrice.indexOf(",") != -1){
															                        	String unitprice=sTotalPrice.replaceAll(",", "");
															                    		BigDecimal a=new BigDecimal(unitprice);
															                    		bdTotalPriceStd=bdTotalPriceStd.add(a);      
																					} else {
															                          	BigDecimal a=new BigDecimal(sTotalPrice);
															                          	bdTotalPriceStd=bdTotalPriceStd.add(a);      
																					}
															                    }
															                    
															                    if(sReqTotalPrice!="" && sReqTotalPrice!=QuotationConstants.QUOTATION_ZERO) {
															                    	if(sReqTotalPrice.indexOf(",") != -1){
															                        	String unitprice=sReqTotalPrice.replaceAll(",", "");
															                    		BigDecimal a=new BigDecimal(unitprice);
															                    		bdTotalPriceReq=bdTotalPriceReq.add(a);      
																					} else {
															                          	BigDecimal a=new BigDecimal(sReqTotalPrice);
															                          	bdTotalPriceReq=bdTotalPriceReq.add(a);      
																					}
															                    }
															                    
															                    if(sAprvTotalPrice!="" && sAprvTotalPrice!=QuotationConstants.QUOTATION_ZERO) {
															                    	if(sTotalPrice.indexOf(",") != -1){
															                        	String unitprice=sAprvTotalPrice.replaceAll(",", "");
															                    		BigDecimal a=new BigDecimal(unitprice);
															                    		bdTotalPriceAprv=bdTotalPriceAprv.add(a);      
																					} else {
															                          	BigDecimal a=new BigDecimal(sAprvTotalPrice);
															                          	bdTotalPriceAprv=bdTotalPriceAprv.add(a);      
																					}
															                    }
															                    
															                    if(sQuotedTotalPrice!="" && sQuotedTotalPrice!=QuotationConstants.QUOTATION_ZERO) {
															                    	if(sQuotedTotalPrice.indexOf(",") != -1){
															                    		String unitprice=sQuotedTotalPrice.replaceAll(",", "");
															                    		BigDecimal a=new BigDecimal(unitprice);
															                    		bdTotalPriceQuoted=bdTotalPriceQuoted.add(a);
															                    	} else {
															                    		BigDecimal a=new BigDecimal(sQuotedTotalPrice);
															                    		bdTotalPriceQuoted=bdTotalPriceQuoted.add(a);  
															                    	}
															                    }
																			%>
																		</logic:iterate>
																		<% 
																			sTotalPrice = CurrencyConvertUtility.buildPriceFormat(bdTotalPriceStd.toString());
																			sReqTotalPrice = CurrencyConvertUtility.buildPriceFormat(bdTotalPriceReq.toString());
																			sAprvTotalPrice = CurrencyConvertUtility.buildPriceFormat(bdTotalPriceAprv.toString());
																			sQuotedTotalPrice = CurrencyConvertUtility.buildPriceFormat(bdTotalPriceQuoted.toString());
																		%>
								                      					<tr>
																			<td colspan="5" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td align="left" class="formContentview-rating-b0x"><b><bean:message key="quotation.create.label.total"/></b></td>
							                       							<td class="formContentview-rating-b0x"> <b> <%=df1.format(dQuantity)%> </b> </td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td class="formContentview-rating-b0x"><b> &#8377;  <%=sTotalPrice%></b></td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td class="formContentview-rating-b0x"><b> &#8377;  <%=sReqTotalPrice%></b></td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td class="formContentview-rating-b0x"><b> &#8377;  <%=sAprvTotalPrice%> </b></td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
							                       							<td class="formContentview-rating-b0x"><b> &#8377;  <%=sQuotedTotalPrice%></b></td>
																		</tr>
																		<tr>
																			<td colspan="1" class="formContentview-rating-b0x"><b><bean:message key="quotation.create.label.inwords"/></b></td>
																			<td colspan="18" class="formContentview-rating-b0x" align="right">
																				<b>	<%=CurrencyConvertUtility.convertToIndianCurrency(String.valueOf(bdTotalPriceStd)) %> </b>
																			</td>
																		</tr>
																	</logic:equal>
																	<!-- ------------------------------------ Commercial Offer : MH : End ------------------------------------- -->
																																	
																</logic:present>
																
																														
															</table>
														</td>
													</tr>
													
                								</table>
                								
                							</fieldset>
                							
                						</td>
                					</tr>
                					<!-----------------------Commercial Offer block End ------------------------------------------>
                					
                					<!----------------------- Comments and Deviations Start -------------------------------------->
                					<tr>
                						<td>
                							<fieldset>
                								<legend class="blueheader"> <bean:message key="quotation.create.label.commentsanddeviations"/> </legend>
                								<div>
													<table width="100%" border="0" cellspacing="0" cellpadding="0">
														<tr>
															<td colspan="5">&nbsp;</td>
														</tr>
														<tr>
															<td width="100%">
																<textarea name="deviationComments" id="deviationComments" style="width :1050px; height: 50px; BACKGROUND-COLOR: #e2e2e2" tabindex="35" readonly> <bean:write name="newEnquiryDto" property="commentsDeviations" filter="false" /> </textarea>
															</td>
														</tr>
													</table>
												</div>
                							</fieldset>
                						</td>
                					</tr>
                					<!----------------------- Comments and Deviations End ---------------------------------------->
                					
    								<!-----------------------Notes By SM, RSM, NSM, MH Start ------------------------------------->
    								<tr>
    									<td>
    										<fieldset>
		                							<legend class="blueheader">	<bean:message key="quotation.create.label.notesection"/> </legend>
													<div>
														<table width="100%" border="0" cellspacing="0" cellpadding="0">
															<tr> <td colspan="5">&nbsp;</td> </tr>
															<tr style="float: left; width: 100%;">
																<td class="formLabelrating" style="width: 138px; margin-right: 33px;"><bean:message key="quotation.create.label.smnote" /></td>
																<td colspan="4">
																	<textarea name="smNote" id="smNote" style="width: 1050px; height: 50px; BACKGROUND-COLOR: #e2e2e2" tabindex="35" readonly > <bean:write name="newEnquiryDto"  property="smNote" filter="false" /> </textarea>
																</td>
															</tr>
															<tr style="float: left; width: 100%;">
																<td class="formLabelrating" style="width: 138px; margin-right: 33px;"><bean:message key="quotation.create.label.rsmnote" /></td>
																<td colspan="4">
																	<textarea name="rsmNote" id="rsmNote" style="width: 1050px; height: 50px; BACKGROUND-COLOR: #e2e2e2" tabindex="35" readonly > <bean:write name="newEnquiryDto"  property="rsmNote" filter="false" /> </textarea>
																</td>
															</tr>
															<tr style="float: left; width: 100%;">
																<td class="formLabelrating" style="width: 138px; margin-right: 33px;"><bean:message key="quotation.create.label.nsmnote" /></td>
																<td colspan="4">
																	<textarea name="nsmNote" id="nsmNote" style="width: 1050px; height: 50px; BACKGROUND-COLOR: #e2e2e2" tabindex="35" readonly > <bean:write name="newEnquiryDto"  property="nsmNote" filter="false" /> </textarea>
																</td>
															</tr>
															<tr style="float: left; width: 100%;">
																<td class="formLabelrating" style="width: 138px; margin-right: 33px;"><bean:message key="quotation.create.label.mhnote" /></td>
																<td colspan="4">
																	<textarea name="mhNote" id="mhNote" style="width: 1050px; height: 50px; BACKGROUND-COLOR: #e2e2e2" tabindex="35" readonly> <bean:write name="newEnquiryDto"  property="mhNote" filter="false" /> </textarea>
																</td>
															</tr>
														</table>
													</div>
											</fieldset>
    									</td>
    								</tr>
    								<!-----------------------Notes By SM, RSM, NSM, MH End --------------------------------------->
    								
    								
    								

						<!--  -----------------------------------------------------------------------------------------------------------------------------  -->
						<tr>
								<td>&nbsp;</td>
						</tr>
							<tr class="section-head">
								<td class="grey-dark" colspan="5">&nbsp;</td>
							</tr>

							<tr>
								<td class="blueheader"><bean:message key="quotation.create.label.notes" /></td>
							</tr>
							<tr class="section-head">
								<td colspan="5">&nbsp;</td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_1)%></span></td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_2)%></span></td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_3)%></span></td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_4)%></span></td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_5)%></span></td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_6)%></span></td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_7)%></span></td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_8)%></span></td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_9)%></span></td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_10)%></span></td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_11)%></span></td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_12)%></span></td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_13)%></span></td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_14)%></span></td>
							</tr>
							
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr class="section-head">
								<td class="grey-dark" colspan="5">&nbsp;</td>
							</tr>

							<tr>
								<td class="blueheader"><bean:message key="quotation.create.label.generalnote" /></td>
							</tr>

							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><bean:message key="quotation.create.label.defintions" /></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_DEFINTIONS)%></span></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><bean:message key="quotation.create.label.validity" /></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_VALIDITY)%></span></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><bean:message key="quotation.create.label.scope" /></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_SCOPE)%></span></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><bean:message
										key="quotation.create.label.priceandbasis" /></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_PRICEANDBASIS)%></span></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>



							<%--  <tr>
                <td  ><bean:message key="quotation.create.label.taxessduties"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_TAXES_DUTIES)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
              
              
               <tr>
                <td  ><bean:message key="quotation.create.label.statutoryvariation"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_STATUTORY_VARIATION)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr> --%>



							<tr>
								<td><bean:message
										key="quotation.create.label.freightandinsurance" /></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_FREIGHT_AND_INSURANCE)%></span></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>



							<tr>
								<td><bean:message
										key="quotation.create.label.packingandforwarding" /></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_PACKING_AND_FORWARDING)%></span></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>




							<tr>
								<td><bean:message
										key="quotation.create.label.termsofpayments" /></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_TERMS_OF_PAYMENT)%></span></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>



							<tr>
								<td><bean:message key="quotation.create.label.deliveries" /></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_DELIVERY)%></span></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>

							<tr>
								<td><bean:message
										key="quotation.create.label.drawingsanddocsapproval" /></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_DRAWINGANDDOCSAPPROVAL)%></span></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>

							<tr>
								<td><bean:message
										key="quotation.create.label.delayinmanufclearance" /></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_DELAYMANFCLEARANCE)%></span></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>


							<tr>
								<td><bean:message
										key="quotation.create.label.forcemeasures" /></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_FORCE_MAJEURE)%></span></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>



							<tr>
								<td><bean:message key="quotation.create.label.storages" /></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_STORAGE)%></span></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>



							<tr>
								<td><bean:message key="quotation.create.label.warranty" /></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_WARRANTY)%></span></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>



							<tr>
								<td><bean:message
										key="quotation.create.label.wghtsanddimension" /></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_WEIGHTS_AND_DIMENSIONS)%></span></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>


							<tr>
								<td><bean:message key="quotation.create.label.tests" /></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_TESTS)%></span></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>


							<tr>
								<td><bean:message key="quotation.create.label.standards" /></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_STANDARDS)%></span></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>



							<tr>
								<td><bean:message
										key="quotation.create.label.limitationofliability" /></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_LIMITATION_OF_LIABILITY)%></span></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>


							<tr>
								<td><bean:message
										key="quotation.create.label.consequentiallosses" /></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_CONSEQUENTIAL_LOSSES)%></span></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>


							<tr>
								<td><bean:message key="quotation.create.label.arbitration" /></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_ARBITRATION)%></span></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>


							<tr>
								<td><bean:message key="quotation.create.label.language" /></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_LANGUAGE)%></span></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>


							<tr>
								<td><bean:message key="quotation.create.label.governinglaw" /></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_GOVERNING_LAW)%></span></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>



							<tr>
								<td><bean:message
										key="quotation.create.label.variationinquantity" /></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_VARIATION_IN_QUANTITY)%></span></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>


							<tr>
								<td><bean:message
										key="quotation.create.label.transferoftitle" /></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_TRANSFER_OF_TITLE)%></span></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>



							<tr>
								<td><bean:message
										key="quotation.create.label.confidentialtreatmentsecrecy" /></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_CONFIDENTIAL_TREATMENT_AND_SECRECY)%></span></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>



							<tr>
								<td><bean:message
										key="quotation.create.label.passingofbenefitrisk" /></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_PASSING_OF_BENEFIT_AND_RISK)%></span></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>


							<tr>
								<td><bean:message
										key="quotation.create.label.compensionduetobuyersdefault" /></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_COMPENSATION_DUE_TO_BUYERS_DEFAULT)%></span></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>


							<tr>
								<td><bean:message
										key="quotation.create.label.suspensionsandtermination" /></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>



							<tr>
								<td><bean:message key="quotation.create.label.suspensions" /></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_SUSPENSION)%></span></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>


							<tr>
								<td><bean:message key="quotation.create.label.terminations" /></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_TERMINATION)%></span></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>


							<tr>
								<td><bean:message key="quotation.create.label.bankruptcy" /></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_BANKRUPTCY)%></span></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>




							<tr>
								<td><bean:message key="quotation.create.label.acceptance" /></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_ACCEPTANCE)%></span></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>



							<tr>
								<td><bean:message
										key="quotation.create.label.limitofsupply" /></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_LIMIT_OF_SUPPLY)%></span></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>



							<tr>
								<td><bean:message
										key="quotation.create.label.changeinscope" /></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_CHANGE_IN_SCOPE)%></span></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>



							<tr>
								<td><bean:message
										key="quotation.create.label.channelsforcommunications" /></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_CHANNELS_FOR_COMMUNICATION)%></span></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>



							<tr>
								<td><bean:message key="quotation.create.label.general" /></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td><span><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_GENERAL)%></span></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>


							<tr>
								<td class="blue-light" align="right">
									
									<!-- Adding a View PDF Button -->
									<logic:greaterEqual name="newEnquiryDto" property="statusId" value="5">
										<logic:present name="dmattachmentList" scope="request">
											<logic:notEmpty name="dmattachmentList">
												<input name="downloadattachment" type="button" onclick="viewAttachments();" class="BUTTON" value="Click To See Attachment Links"/>
											</logic:notEmpty>
										</logic:present>
									</logic:greaterEqual>
									
									<logic:greaterEqual name="newEnquiryDto" property="statusId" value="6">
										<input name="viewpdf" type="button" onclick="viewPDF();" class="BUTTON" value="View PDF"/>
									</logic:greaterEqual>
									
								</td>
							</tr>
								
						</table>
    					
    						</td>
    					</tr>						
						
						</logic:notEmpty>
    				</table>
    				
    			</logic:present>
	
			</html:form>
		</td>
	</tr>
	
</table>