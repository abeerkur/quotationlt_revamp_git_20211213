<!-- -- Design MTO  Complete Ratings Display Block -- -->
<div style="clear:both; float:left; width:100%; margin-bottom:30px;">
	<div id="sticky-anchor"></div>
	<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
		<tr>
			<td>
				<fieldset style="height:auto;">
					<legend class="blueheader"> Complete Technical Offer </legend>
					
					<div class="zui-wrapper">
						<div id="ratingsEncloseDiv" class="zui-scroller">
							<table class="table-rating zui-table">
								<thead>
									<tr style="width:1174px !important; float:left;" id="myrating">
										<th valign="top">
											<div id="topfields_mto_section">
												<jsp:include page="newmtoenquiry_topfields_include.jsp"/>
											</div>
										</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td style="width: 100%; display: block;">
											<div id="bottomfields_mto_section">
												<div style="background-color:#CCD8F2; width:100%; float:left; z-index:999999; height:20px; border:1px solid #fff;">
													<a class="accordion-toggle" style="font-size:12px; font-weight:bold; color:#00458A; display:block;" href="javascript:void(0);" onclick="javascript:toggleMTOBaseFieldsSection();"> Basic </a>
												</div>
												<div id="basefields_mto_section" style="margin-top:5px;margin-bottom:5px;">
													<jsp:include page="newmtoenquiry_basicfields_include.jsp"/>
												</div>
												<div style="background-color:#CCD8F2; width:100%; float:left; z-index:999999; height:20px; border:1px solid #fff;">
													<a class="accordion-toggle" style="font-size:12px; font-weight:bold; color:#00458A; display:block;" href="javascript:void(0);" onclick="javascript:toggleMTOElectricalSection();"> Electrical </a>
												</div>
												<div id="electricalfields_mto_section" style="margin-top:15px;margin-bottom:5px;">
													<jsp:include page="newmtoenquiry_electrical_include.jsp"/>
												</div>
												<div style="background-color:#CCD8F2; width:100%; float:left; z-index:999999; height:20px; border:1px solid #fff;">
													<a class="accordion-toggle" style="font-size:12px; font-weight:bold; color:#00458A; display:block;" href="javascript:void(0);" onclick="javascript:toggleMTOMechanicalSection();"> Mechanical </a>
												</div>
												<div id="mechanicalfields_mto_section" style="margin-top:15px;margin-bottom:5px;">
													<jsp:include page="newmtoenquiry_mechanical_include.jsp"/>
												</div>
												<div style="background-color:#CCD8F2; width:100%; float:left; z-index:999999; height:20px; border:1px solid #fff;">
													<a class="accordion-toggle" style="font-size:12px; font-weight:bold; color:#00458A; display:block;" href="javascript:void(0);" onclick="javascript:toggleMTOPerformanceSection();"> Performance </a>
												</div>
												<div id="perffields_mto_section" style="margin-top:15px;margin-bottom:5px;">
													<jsp:include page="newmtoenquiry_performance_include.jsp"/>
												</div>
												<div style="background-color:#CCD8F2; width:100%; float:left; z-index:999999; height:20px; border:1px solid #fff;">
													<a class="accordion-toggle" style="font-size:12px; font-weight:bold; color:#00458A; display:block;" href="javascript:void(0);" onclick="javascript:toggleMTOAccessoriesSection();"> Accessories </a>
												</div>
												<div id="accessoriesfields_mto_section" style="margin-top:15px;margin-bottom:5px;">
													<jsp:include page="newmtoenquiry_accessories_include.jsp"/>
												</div>
												<div style="background-color:#CCD8F2; width:100%; float:left; z-index:999999; height:20px; border:1px solid #fff;">
													<a class="accordion-toggle" style="font-size:12px; font-weight:bold; color:#00458A; display:block;" href="javascript:void(0);" onclick="javascript:toggleMTOCertificateSparesSection();"> Certificate / Spares / Testing </a>
												</div>
												<div id="certificatefields_mto_section" style="margin-top:15px;margin-bottom:5px;">
													<jsp:include page="newmtoenquiry_certificatespares_include.jsp"/>
												</div>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</fieldset>
			</td>
		</tr>
	</table>
</div>
<!-- -- Design MTO  Complete Ratings Display Block -- -->