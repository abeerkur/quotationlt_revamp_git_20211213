<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants" %>
<%@ page import="in.com.rbc.quotation.common.utility.PropertyUtility" %>
<%@ page import="in.com.rbc.quotation.common.utility.CommonUtility" %>
<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

						<table width="100%" border="0" align="center" cellpadding="0" cellspacing="3">
							<tr>
								<td width="20%" class="label-text"> Winding Treatment </td>
								<td width="20%" class="normal">
									<select name="windingTreatment" id="windingTreatment" title="Winding Treatment" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present name="newenquiryForm" property="ltWindingTreatmentList" >
											<bean:define name="newenquiryForm" property="ltWindingTreatmentList" id="ltWindingTreatmentList" type="java.util.Collection" />
											<logic:iterate name="ltWindingTreatmentList" id="ltWindingTreatment" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltWindingTreatment" property="key" />' > <bean:write name="ltWindingTreatment" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
								<td width="10%">&nbsp;</td>
								<td width="20%" class="label-text"> Winding Wire </td>
								<td width="20%" class="normal">
									<select name="windingWire" id="windingWire" title="Winding Wire" >
										<logic:present name="newenquiryForm" property="ltWindingWireList" >
											<bean:define name="newenquiryForm" property="ltWindingWireList" id="ltWindingWireList" type="java.util.Collection" />
											<logic:iterate name="ltWindingWireList" id="ltWindingWire" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltWindingWire" property="key" />' > <bean:write name="ltWindingWire" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
							</tr>
							<script> selectLstItemById('windingTreatment', '<bean:write name="ratingObj" property="ltWindingTreatmentId" filter="false" />'); </script>
							<script> selectLstItemById('windingWire', '<bean:write name="ratingObj" property="ltWindingWire" filter="false" />'); </script>
							<tr>
								<logic:equal name="ratingObj" property="flg_Addon_Leads_MTO" value="Y">
									<td width="20%" class="label-text" style="background:#ffff00;"> Leads </td>
								</logic:equal>
								<logic:notEqual name="ratingObj" property="flg_Addon_Leads_MTO" value="Y">
									<td width="20%" class="label-text"> Leads </td>
								</logic:notEqual>
								<td width="20%" class="normal">
									<select name="lead" id="lead" title="Leads" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present name="newenquiryForm" property="ltLeadList">
											<bean:define name="newenquiryForm" property="ltLeadList" id="ltLeadList" type="java.util.Collection" />
											<logic:iterate name="ltLeadList" id="ltLead" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltLead" property="key" />'> <bean:write name="ltLead" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
								<td width="10%">&nbsp;</td>
								<td width="20%" class="label-text"> Winding Configuration </td>
								<td width="20%" class="normal">
									<select name="windingConfiguration" id="windingConfiguration" title="Winding Configuration" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present name="newenquiryForm" property="ltWindingConfigurationList">
											<bean:define name="newenquiryForm" property="ltWindingConfigurationList" id="ltWindingConfigurationList" type="java.util.Collection" />
											<logic:iterate name="ltWindingConfigurationList" id="ltWindingConfiguration" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																<option value='<bean:write name="ltWindingConfiguration" property="key" />'>
																	<bean:write name="ltWindingConfiguration" property="value" />
																</option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
							</tr>
							<script> selectLstItemById('lead', '<bean:write name="ratingObj" property="ltLeadId" filter="false" />'); </script>
							<script> selectLstItemById('windingConfiguration', '<bean:write name="ratingObj" property="ltWindingConfig" filter="false" />'); </script>
							<tr>
								<td width="20%" class="label-text"> Constant Efficiency Range </td>
								<td width="20%" class="normal">
									<select name="constantEfficiencyRange" id="constantEfficiencyRange" title="Constant Efficeincy Range" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present name="newenquiryForm" property="ltConstantEffRangeList">
											<bean:define name="newenquiryForm" property="ltConstantEffRangeList" id="ltConstantEffRangeList" type="java.util.Collection" />
											<logic:iterate name="ltConstantEffRangeList" id="ltConstantEffRange" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltConstantEffRange" property="key" />'> <bean:write name="ltConstantEffRange" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
								<td width="10%">&nbsp;</td>
								<td width="20%" class="label-text"> VFD Application Type </td>
								<td width="20%" class="normal">
									<select name="vfdType" id="vfdType" title="VDF Application Type" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present name="newenquiryForm" property="ltVFDTypeList">
											<bean:define name="newenquiryForm" property="ltVFDTypeList" id="ltVFDTypeList" type="java.util.Collection" />
											<logic:iterate name="ltVFDTypeList" id="ltVFDType" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltVFDType" property="key" />'> <bean:write name="ltVFDType" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
							</tr>
							<script> selectLstItemById('constantEfficiencyRange', '<bean:write name="ratingObj" property="ltConstantEfficiencyRange" filter="false" />'); </script>
							<script> selectLstItemById('vfdType', '<bean:write name="ratingObj" property="ltVFDApplTypeId" filter="false" />'); </script>
							<tr>
								<td width="20%" class="label-text"> Speed Range only for VFD motor (Min.) </td>
								<td width="20%" class="normal">
									<select name="vfdSpeedRangeMin" id="vfdSpeedRangeMin" title="Speed Range only for VFD motor (Min.)" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present name="newenquiryForm" property="ltVFDSpeedRangeMinList">
											<bean:define name="newenquiryForm" property="ltVFDSpeedRangeMinList" id="ltVFDSpeedRangeMinList" type="java.util.Collection" />
											<logic:iterate name="ltVFDSpeedRangeMinList" id="ltVFDSpeedRangeMin" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltVFDSpeedRangeMin" property="key" />'> <bean:write name="ltVFDSpeedRangeMin" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
								<td width="10%">&nbsp;</td>
								<td width="20%" class="label-text"> Speed Range only for VFD motor (Max.) </td>
								<td width="20%" class="normal">
									<select name="vfdSpeedRangeMax" id="vfdSpeedRangeMax" title="Speed Range only for VFD motor (Max.)" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present name="newenquiryForm" property="ltVFDSpeedRangeMaxList">
											<bean:define name="newenquiryForm" property="ltVFDSpeedRangeMaxList" id="ltVFDSpeedRangeMaxList" type="java.util.Collection" />
											<logic:iterate name="ltVFDSpeedRangeMaxList" id="ltVFDSpeedRangeMax" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltVFDSpeedRangeMax" property="key" />'> <bean:write name="ltVFDSpeedRangeMax" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
							</tr>
							<script> selectLstItemById('vfdSpeedRangeMin', '<bean:write name="ratingObj" property="ltVFDMotorSpeedRangeMin" filter="false" />'); </script>
							<script> selectLstItemById('vfdSpeedRangeMax', '<bean:write name="ratingObj" property="ltVFDMotorSpeedRangeMax" filter="false" />'); </script>
							<tr>
								<td width="20%" class="label-text"> <bean:message key="quotation.offer.label.safestalltimeh"/> </td>
								<td width="20%" class="normal">
									<input name="sstHot" class="normal"  maxlength="6" /> &nbsp;<bean:message key="quotation.units.s"/>
								</td>
								<td width="10%">&nbsp;</td>
								<td width="20%" class="label-text"> <bean:message key="quotation.offerdata.technical.label.vibrationlevel"/> </td>
								<td width="20%" class="normal">
									<select name="vibration" id="vibration" title="Vibration">
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present name="newenquiryForm" property="ltVibrationList" >
											<bean:define name="newenquiryForm" property="ltVibrationList" id="ltVibrationList" type="java.util.Collection" />
											<logic:iterate name="ltVibrationList" id="ltVibration" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltVibration" property="key" />' > <bean:write name="ltVibration" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
							</tr>
							<script> selectLstItemById('vibration', '<bean:write name="ratingObj" property="ltVibrationId" filter="false" />'); </script>
							<tr>
								<td width="20%" class="label-text"> <bean:message key="quotation.offer.label.safestalltimec"/> </td>
								<td width="20%" class="normal">
									<input name="sstCold" class="normal"  maxlength="6" /> &nbsp;<bean:message key="quotation.units.s"/>
								</td>
								<td width="10%">&nbsp;</td>
								<td width="20%" class="label-text"> Neutral Terminal Box </td>
								<td width="20%" class="normal">
									<select name="neutralTermBox" id="neutralTermBox" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present name="newenquiryForm" property="alNeutralTBList" >
											<bean:define name="newenquiryForm" property="alNeutralTBList" id="alNeutralTBList" type="java.util.Collection" />
											<logic:iterate name="alNeutralTBList" id="alNeutralTB" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="alNeutralTB" property="key" />' > <bean:write name="alNeutralTB" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
							</tr>
							
							<tr>
								<td width="20%" class="label-text"> Starting time at rated voltage </td>
								<td width="20%" class="normal">
									<input name="startTimeRV" class="normal"  maxlength="6"/> &nbsp;<bean:message key="quotation.units.s"/>
								</td>
								<td width="10%">&nbsp;</td>
								<td width="20%" class="label-text"> Main TERM. Box </td>
								<td width="20%" class="normal">
									<select name="termBoxId" id="termBoxId" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present name="newenquiryForm" property="termBoxList" >
											<bean:define name="newenquiryForm" property="termBoxList" id="termBoxList" type="java.util.Collection" />
											<logic:iterate name="termBoxList" id="termBox" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="termBox" property="key" />' > <bean:write name="termBox" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
							</tr>
							
							<tr>
								<td width="20%" class="label-text"> Starting time at 80% of rated voltage </td>
								<td width="20%" class="normal">
									<input name="startTimeRV80" class="normal" maxlength="6"/> &nbsp;<bean:message key="quotation.units.s"/>
								</td>
								<td width="10%">&nbsp;</td>
								<td width="20%" class="label-text"> Cable Entry </td>
								<td width="20%" class="normal">
									<select name="cableEntry" id="cableEntry" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present name="newenquiryForm" property="alCableEntryList" >
											<bean:define name="newenquiryForm" property="alCableEntryList" id="alCableEntryList" type="java.util.Collection" />
											<logic:iterate name="alCableEntryList" id="alCableEntry" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="alCableEntry" property="key" />' > <bean:write name="alCableEntry" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
							</tr>
							
							<tr>
								<td width="20%" class="label-text"> Temperature Rise <span class="mandatory">&nbsp;</span> </td>
								<td width="20%" class="normal">
									<select name="tempRise" id="tempRise" title="Temperature Rise" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present property="ltTemperatureRiseList" name="newenquiryForm">
											<bean:define name="newenquiryForm" property="ltTemperatureRiseList" id="ltTemperatureRiseList" type="java.util.Collection" />
											<logic:iterate name="ltTemperatureRiseList" id="ltTemperatureRise" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltTemperatureRise" property="key" />' > <bean:write name="ltTemperatureRise" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
								<td width="10%">&nbsp;</td>
								<td width="20%" class="label-text"> Insulation Class / Temp. Rise Class <span class="mandatory">&nbsp;</span> </td>
								<td width="20%" class="normal">
									<select name="insulationClass" id="insulationClass" title="Insulation Class" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present name="newenquiryForm" property="ltInsulationClassList" >
											<bean:define name="newenquiryForm" property="ltInsulationClassList" id="ltInsulationClassList" type="java.util.Collection" />
											<logic:iterate name="ltInsulationClassList" id="ltInsulationClass" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltInsulationClass" property="key" />' > <bean:write name="ltInsulationClass" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
							</tr>
							<script> selectLstItemById('tempRise', '<bean:write name="ratingObj" property="ltTempRise" filter="false" />'); </script>
							<script> selectLstItemById('insulationClass', '<bean:write name="ratingObj" property="ltInsulationClassId" filter="false" />'); </script>
							<tr>
								<td width="20%" class="label-text"> Ambient Temperature </td>
								<td width="20%" class="normal">
									<select name="ambientTemp" id="ambientTemp" title="Ambient Temp. Deg C" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present property="ltAmbientTemperatureList" name="newenquiryForm">
											<bean:define name="newenquiryForm" property="ltAmbientTemperatureList" id="ltAmbientTemperatureList" type="java.util.Collection" />
											<logic:iterate name="ltAmbientTemperatureList" id="ltAmbientTemperature" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltAmbientTemperature" property="key" />' > <bean:write name="ltAmbientTemperature" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select> &nbsp;Deg C
								</td>
								<td width="10%">&nbsp;</td>
								<td width="20%" class="label-text"> Number of brought out terminals </td>
								<td width="20%" class="normal">
									<select name="noBoTerminals" id="noBoTerminals" title="Ambient Temp. Deg C" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present property="alBoTerminalsNoList" name="newenquiryForm">
											<bean:define name="newenquiryForm" property="alBoTerminalsNoList" id="alBoTerminalsNoList" type="java.util.Collection" />
											<logic:iterate name="alBoTerminalsNoList" id="alBoTerminalsNo" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="alBoTerminalsNo" property="key" />' > <bean:write name="alBoTerminalsNo" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
							</tr>
							<script> selectLstItemById('ambientTemp', '<bean:write name="ratingObj" property="ltAmbTempId" filter="false" />'); </script>
							<tr>
								<td width="20%" class="label-text"> Altitude <span class="mandatory">&nbsp;</span> </td>
								<td width="20%" class="normal">
									<input name="altitude" class="normal" maxlength="6" value="<bean:write name="ratingObj" property="ltAltitude" filter="false" />" /> 
								</td>
								<td width="10%">&nbsp;</td>
								<td width="20%" class="label-text"> Direction of Rotation <span class="mandatory">&nbsp;</span> </td>
								<td width="20%" class="normal">
									<select name="directionOfRotation" id="directionOfRotation" title="Direction of Rotation" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present name="newenquiryForm" property="ltDirectionOfRotationList" >
											<bean:define name="newenquiryForm" property="ltDirectionOfRotationList" id="ltDirectionOfRotationList" type="java.util.Collection" />
											<logic:iterate name="ltDirectionOfRotationList" id="ltDirectionOfRotation" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltDirectionOfRotation" property="key" />' > <bean:write name="ltDirectionOfRotation" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
							</tr>
							<script> selectLstItemById('directionOfRotation', '<bean:write name="ratingObj" property="ltDirectionOfRotation" filter="false" />'); </script>
							<tr>
								<td width="20%" class="label-text"> Method of Starting </td>
								<td width="20%" class="normal">
									<select name="methodOfStarting" id="methodOfStarting" title="Method of Starting"  >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present name="newenquiryForm" property="ltMethodOfStartingList">
											<bean:define name="newenquiryForm" property="ltMethodOfStartingList" id="ltMethodOfStartingList" type="java.util.Collection" />
											<logic:iterate name="ltMethodOfStartingList" id="ltMethodOfStarting" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltMethodOfStarting" property="key" />'>
													<bean:write name="ltMethodOfStarting" property="value" />
												</option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
								<td width="10%">&nbsp;</td>
								<td width="20%" class="label-text"> Standard Rotation <span class="mandatory">&nbsp;</span> </td>
								<td width="20%" class="normal">
									<select name="standardRotation" id="standardRotation" title="Standard Rotation" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present name="newenquiryForm" property="ltStandardRotationList">
											<bean:define name="newenquiryForm" property="ltStandardRotationList" id="ltStandardRotationList" type="java.util.Collection" />
											<logic:iterate name="ltStandardRotationList" id="ltStandardRotation" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltStandardRotation" property="key" />' > <bean:write name="ltStandardRotation" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
							</tr>
							<script> selectLstItemById('methodOfStarting', '<bean:write name="ratingObj" property="ltMethodOfStarting" filter="false" />'); </script>
							<script> selectLstItemById('standardRotation', '<bean:write name="ratingObj" property="ltStandardRotation" filter="false" />'); </script>
							<tr>
								<td width="20%" class="label-text"> Starting Current on DOL Starting </td>
								<td width="20%" class="normal">
									<select name="startingCurrOnDOLStarting" id="startingCurrOnDOLStarting" title="Starting Current On DOL Starting" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present name="newenquiryForm" property="ltStartingDOLCurrentList">
											<bean:define name="newenquiryForm" property="ltStartingDOLCurrentList" id="ltStartingDOLCurrentList" type="java.util.Collection" />
											<logic:iterate name="ltStartingDOLCurrentList" id="ltStartingDOLCurrent" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltStartingDOLCurrent" property="key" />'>
													<bean:write name="ltStartingDOLCurrent" property="value" />
												</option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
								<td width="10%">&nbsp;</td>
								<td width="20%" class="label-text"> Method of Cooling <span class="mandatory">&nbsp;</span> </td>
								<td width="20%" class="normal">
									<select name="methodOfCooling" id="methodOfCooling" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present name="newenquiryForm" property="ltForcedCoolingList" >
											<bean:define name="newenquiryForm" property="ltForcedCoolingList" id="ltForcedCoolingList" type="java.util.Collection" />
											<logic:iterate name="ltForcedCoolingList" id="ltForcedCooling" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltForcedCooling" property="key" />' > <bean:write name="ltForcedCooling" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
							</tr>
							<script> selectLstItemById('startingCurrOnDOLStarting', '<bean:write name="ratingObj" property="ltStartingCurrentOnDOLStart" filter="false" />'); </script>
							<script> selectLstItemById('methodOfCooling', '<bean:write name="ratingObj" property="ltForcedCoolingId" filter="false" />'); </script>
							<tr>
								<td width="20%" class="label-text"> Noise Level (Subject to Tolerance)  </td>
								<td width="20%" class="normal">
									<select name="noiseLevel" id="noiseLevel" title="Noise Level" class="normal" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present property="ltNoiseLevelList" name="newenquiryForm">
											<bean:define name="newenquiryForm" property="ltNoiseLevelList" id="ltNoiseLevelList" type="java.util.Collection" />
											<logic:iterate name="ltNoiseLevelList" id="ltNoiseLevel" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltNoiseLevel" property="key" />' > <bean:write name="ltNoiseLevel" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
								<td width="10%">&nbsp;</td>
								<td width="20%" class="label-text"> Lubrication </td>
								<td width="20%" class="normal">
									<select name="lubrication" id="lubrication" title="Method of Cooling" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present name="newenquiryForm" property="alLubricationList" >
											<bean:define name="newenquiryForm" property="alLubricationList" id="alLubricationList" type="java.util.Collection" />
											<logic:iterate name="alLubricationList" id="alLubrication" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="alLubrication" property="key" />' > <bean:write name="alLubrication" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
							</tr>
							<script> selectLstItemById('noiseLevel', '<bean:write name="ratingObj" property="ltNoiseLevel" filter="false" />'); </script>
							<tr>
								<td width="20%" class="label-text"> Moment of inertia J = � GD2 (Motor) </td>
								<td width="20%" class="normal">
									<input name="rtGD2" class="normal"  maxlength="6"/> &nbsp;
								</td>
								<td width="10%">&nbsp;</td>
								<td width="20%" class="label-text"> Type of Grease </td>
								<td width="20%" class="normal">
									<select name="typeOfGrease" id="typeOfGrease" title="Type of Grease" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present name="newenquiryForm" property="ltTypeOfGreaseList" >
											<bean:define name="newenquiryForm" property="ltTypeOfGreaseList" id="ltTypeOfGreaseList" type="java.util.Collection" />
											<logic:iterate name="ltTypeOfGreaseList" id="ltTypeOfGrease" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltTypeOfGrease" property="key" />' > <bean:write name="ltTypeOfGrease" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
							</tr>
							<tr>
								<logic:equal name="ratingObj" property="flg_Txt_LoadGD2_MTO" value="Y">
									<td width="20%" class="label-text" style="background:#ffff00;"> Moment of inertia J = � GD2 (Load) </td>
								</logic:equal>
								<logic:notEqual name="ratingObj" property="flg_Txt_LoadGD2_MTO" value="Y">
									<td width="20%" class="label-text"> Moment of inertia J = � GD2 (Load) </td>
								</logic:notEqual>
								<td width="20%" class="normal">
									<input name="miGD2Load" class="normal"  maxlength="6"/> &nbsp;
								</td>
								<td width="10%">&nbsp;</td>
								<td width="20%" class="label-text"> Balancing </td>
								<td width="20%" class="normal"> <bean:message key="quotation.create.label.balancing"/> </td>
							</tr>
							<tr>
								<td width="20%">&nbsp;</td>
								<td width="20%">&nbsp;</td>
								<td width="10%">&nbsp;</td>
								<td width="20%">&nbsp;</td>
								<td width="20%">&nbsp;</td>
							</tr>
						</table>