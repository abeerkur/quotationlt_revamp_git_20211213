<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants" %>
<%@ page import="in.com.rbc.quotation.common.utility.PropertyUtility" %>
<%@ page import="in.com.rbc.quotation.common.utility.CommonUtility" %>
<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<html:form action="/viewEnquiry" method="POST" >
<html:hidden property="invoke" value="updateFactor"/>
<html:hidden property="createdBySSO" />
<html:hidden property="operationType" styleId="operationType"/> 
<html:hidden property="revisionNumber"/>
<html:hidden property="createdDate"/>
<html:hidden property="createdBy"/>
<html:hidden property="enquiryOperationType" styleId="enquiryOperationType" />
<html:hidden property="dispatch" styleId="dispatch"/>
<html:hidden property="currentRating" styleId="currentRating"/>
<html:hidden property="enquiryId" styleId="enquiryId" />
<html:hidden property="ratingId" styleId="ratingId" />
<html:hidden property="enquiryNumber" styleId="enquiryNumber"/>
<html:hidden property="nextRatingId" styleId="nextRatingId"/>
<html:hidden property="updateCreateBy" styleId="updateCreateBy" />
<html:hidden property="isDataValidated" styleId="isDataValidated"/>
<html:hidden property="isValidated" styleId="isValidated"/>
<html:hidden property="ratingsValidationMessage" styleId="ratingsValidationMessage"/>
<html:hidden property="assignToName" styleId="assignToName" />
<html:hidden property="indentOperation" styleId="indentOperation" value="SMINDENT"/>
<html:hidden property="returnEnquiry" />


<input type="hidden" name="<%=org.apache.struts.taglib.html.Constants.TOKEN_KEY%>" 
value="<%=session.getAttribute(org.apache.struts.action.Action.TRANSACTION_TOKEN_KEY)%>">



<head>
<style>
.lablebox-td{
width:33% !important;
    text-align: left;
    font-weight: normal;
    font-size: 10px;
    font-family: Verdana, Arial, Helvetica, sans-serif;
    padding: 2px;
    background-color: #F2F2FF;
}
/* my general stylizing for inputs, mostly comes from Twitter's
bootstrap.min.css which is distributed with Zend Framework 2*/
/*input
{
  background-color: #fff;
  border: 1px solid #ccc;
  display: inline-block;
  height: 20px;
  padding: 4px 6px;
  margin-bottom: 10px;
  font-size: 11px;
  line-height: 20px;
  color: #555;
  vertical-align: middle;
  -webkit-border-radius: 4px;
  -moz-border-radius: 4px;
  border-radius: 4px;
}*/
select
{
 
  border: 1px solid #ccc;
  color: #555;
 
}
.background-bg-box{
background: #e0e0e0;
}
/* specific customizations for the data-list */
div.data-list-input
{
	position: relative;
	height: 20px;	
	display: inline-flex;
}
select.data-list-input
{
	position: absolute;	
	height: 20px;
	margin-bottom: 0;
}
input.data-list-input
{
	position: absolute;
	top: 0px;
	left: 0px;
	height: 19px;
}
.width-select{
width:100% !important;;
}
.input-class{
width:87% !important;
padding:4px 6px;
border-width:1px;
margin:0;
}
</style>
</head>

<bean:define id="latestId"  name="enquiryForm" property="currentRating"/>
<%!int count=0; %>
<%! int countval = 0; %>


 <table width="900"  border="1" align="center" cellpadding="2" cellspacing="0" bordercolor="#003366">
  <tr>
    <td height="100%" valign="top">     

	<br>
        <div class="sectiontitle">
        <logic:equal name="enquiryForm" property="dispatch" value="WON">
        	<bean:message key="quotation.indentpage.label.heading"/>
        </logic:equal>
	  </div><br>
    <!--Comment End Here  -->

   <table width="100%" border="0" cellpadding="0" cellspacing="3">
     <tr>
       <td rowspan="2" ><table width="98%"  border="0" cellspacing="2" cellpadding="0" class="DotsTable">
           <tr>
             <td rowspan="2" style="width:70px "><img src="html/images/icon_quote.gif" width="52" height="50"></td>
             <td class="other"><bean:message key="quotation.viewenquiry.label.subheading1"/> <bean:write name="enquiryForm" property="enquiryNumber"/><br>
            <bean:message key="quotation.viewenquiry.label.subheading2"/> </td>
           </tr>
           <tr>
             <td class="other"><table width="100%">
                 <tr>
                   <td height="16" class="section-head" align="center">
                   	<bean:message key="quotation.viewenquiry.label.status"/>  <bean:write name="enquiryForm" property="statusName"/> </td>
                 </tr>
             </table></td>
           </tr>
       </table></td>
       <logic:lessThan name="enquiryForm" property="statusId" value="6"> 
	       <td colspan="2" align="right" >
		   <table border="0" cellspacing="0" cellpadding="0" style="width:auto">
			  <tr>
				<td  class="other" align="right">
				<bean:message key="quotation.attachments.message"/>
				</td>
				<td>
				<a href="javascript:manageAttachments('<%= CommonUtility.getIntValue(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_ATTACHMENT_APPLICATIONID)) %>','<bean:write name="enquiryForm" property="enquiryId"/>', '<bean:write name="enquiryForm" property="createdBySSO"/>');"><img src="html/images/ico_attach.gif" alt="Manage Attachments" border="0" align="right"></a>
				</td>
			  </tr>
			</table>
    	   </td>
       </logic:lessThan>
       <logic:present name="attachmentList" scope="request">
       <logic:notEmpty name="attachmentList">
              <logic:equal name="enquiryForm" property="statusId" value="6"> 
	       <td colspan="2" align="right" >
		   <table border="0" cellspacing="0" cellpadding="0" style="width:auto">
			  <tr>
				<td  class="other" align="right"  colspan="2">
				<bean:message key="quotation.attachments.enquirydocument"/>
				</td>
			  </tr>
			</table>
    	   </td>
       </logic:equal>
       </logic:notEmpty>
       </logic:present>
       
     </tr>
     <tr>
       <td  style="width:45% ">
       <logic:present name="attachmentList" scope="request">
       <logic:notEmpty name="attachmentList">
       <fieldset>
         <table width="100%" class="other">
           <logic:iterate name="attachmentList" id="attachmentData" indexId="idx"
								               type="in.com.rbc.quotation.common.vo.KeyValueVo">	
			<tr><td>
				<a href="javascript:download('<bean:write name="attachmentData" property="key"/>', '<bean:write name="attachmentData" property="value"/>', '<bean:write name="attachmentData" property="value2"/>');">
					<bean:write name="attachmentData" property="value"/>.<bean:write name="attachmentData" property="value2"/>
				</a>
			</td></tr>					
		   </logic:iterate>
         </table>
       </fieldset>
        </logic:notEmpty>
        </logic:present>
      </td>
     </tr>
          <tr><td colspan="2" align="right" > 
        <!-- DM Uploading Files  -->
            <logic:lessThan name="enquiryForm" property="statusId" value="6"> 
            <table>
            <tr>
	        <td class="other" align="right">
        	<bean:message key="quotation.attachments.dmuploadedmessage"/></td>
    	    <td class="other" align="right">
    	    <a href="javascript:manageAttachments('<%= CommonUtility.getIntValue(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_DMATTACHMENT_APPLICATIONID)) %>','<bean:write name="enquiryForm" property="enquiryId"/>', '<bean:write name="enquiryForm" property="createdBySSO"/>');"><img src="html/images/ico_attach.gif" alt="Manage Attachments" width="23" height="25" border="0" align="right"></a></td>
            </tr>
            </table>
        </logic:lessThan>
        
       <logic:present name="dmattachmentList" scope="request">
       <logic:notEmpty name="dmattachmentList">
      <logic:equal name="enquiryForm" property="statusId" value="6"> 
            <table>
            <tr>
	        <td class="other" align="right" colspan="2">
        	<bean:message key="quotation.attachments.designdocument"/></td>
            </tr>
            </table>
        </logic:equal>
        </logic:notEmpty>
        </logic:present>
      </td>
      </tr> 
           <tr>
           <td>
           &nbsp;
           </td>
           <td colspan="2" align="right" style="width:45% "> 
                       <logic:present name="dmattachmentList" scope="request">
            <logic:notEmpty name="dmattachmentList">
            <fieldset>
              <table width="100%" class="other">
              <logic:iterate name="dmattachmentList" id="attachmentData" indexId="idx"
								               type="in.com.rbc.quotation.common.vo.KeyValueVo">	
				<tr><td>
					<a href="javascript:download('<bean:write name="attachmentData" property="key"/>', '<bean:write name="attachmentData" property="value"/>', '<bean:write name="attachmentData" property="value2"/>');">
						<bean:write name="attachmentData" property="value"/>.<bean:write name="attachmentData" property="value2"/>
					</a>
				</td></tr>					
				</logic:iterate>
              </table>
        </fieldset>
        </logic:notEmpty>
        </logic:present>
           
           </td>
           </tr> 
     <!-- Indent Page Uploads Starts Here -->
     
          <tr><td colspan="2" align="right" > 

            <logic:greaterEqual name="enquiryForm" property="statusId" value="6"> 
            <table>
            <tr>
	        <td class="other" align="right">
        	<bean:message key="quotation.attachments.indentuploadedmessage"/></td>
    	    <td class="other" align="left"><a href="javascript:manageAttachments('<%= CommonUtility.getIntValue(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_INTENDATTACHMENT_APPLICATIONID)) %>','<bean:write name="enquiryForm" property="enquiryId"/>', '<bean:write name="enquiryForm" property="createdBySSO"/>');"><img src="html/images/ico_attach.gif" alt="Manage Attachments" width="23" height="25" border="0" align="right"></a></td>
            </tr>
            </table>
        </logic:greaterEqual>
      
      </td>
      </tr> 
           <tr>
           <td>
           &nbsp;
           </td>
           <td colspan="2" align="right" style="width:45% "> 
                       <logic:present name="intendattachmentList" scope="request">
            <logic:notEmpty name="intendattachmentList">
            <fieldset>
              <table width="100%" class="other">
              <logic:iterate name="intendattachmentList" id="attachmentData" indexId="idx2"
								               type="in.com.rbc.quotation.common.vo.KeyValueVo">	
				<tr><td>
					<a href="javascript:download('<bean:write name="attachmentData" property="key"/>', '<bean:write name="attachmentData" property="value"/>', '<bean:write name="attachmentData" property="value2"/>');">
						<bean:write name="attachmentData" property="value"/>.<bean:write name="attachmentData" property="value2"/>
					</a>
				</td></tr>					
				</logic:iterate>
              </table>
        </fieldset>
        </logic:notEmpty>
        </logic:present>
           
           </td>
           </tr> 
     
     <!-- Indent Page Uploads Ends Here -->
     
   </table>
   
   <!--Attachments Ending  -->
    
    </td>
    </tr>
    
              <tr>
                <td>
                
                <div class="tablerow">
                <div class="tablecolumn" >
                 <table width="100%" border="0" cellspacing="0" cellpadding="2">
                 	<tr>
                    <td colspan="2" >&nbsp;</td>
                    </tr>

                    <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.indentno"/>
                      <span class="mandatory">&nbsp;</span>
                      </td>
                      <td   style="width:50%">
                       <html:text property="savingIndent"  styleId="savingIndent" styleClass="normal" />
                      </td>
                    
                    </tr>
                     <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.label.admin.customer.customer"/>
                      </td>
                      <td   style="width:50%">
                       <html:text property="customerName" readonly="true" styleClass="normal" />
                      </td>
                    
                    </tr>
                     <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.sapcusomercode"/>
                      </td>
                      <td   style="width:50%">
                       <html:text property="sapCustomerCode" styleId="sapCustomerCode" styleClass="normal" maxlength="50" />
                      </td>
                    
                    </tr>
                     <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.orderno"/>
                      </td>
                      <td   style="width:50%">
                       
                       <html:text property="orderNumber" styleId="orderNumber" styleClass="normal" maxlength="50"/>
                      </td>
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.quotationrefno"/>
                      </td>
                      <td   style="width:50%">
                       <html:text property="enquiryReferenceNumber" styleId="enquiryReferenceNumber" styleClass="normal" readonly="true" />
                      </td>
                    
                    </tr>
                    
                      <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.pocopy"/>
                      </td>
                      <td   style="width:50%">
                      <html:select property="poiCopy" styleId="poiCopy">
                      <html:option value="No">No</html:option>
                      <html:option value="Yes">Yes</html:option>
                      
                      </html:select>
                      </td>
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.loicopy"/>
                      </td>
                      <td   style="width:50%">
                       <html:select property="loiCopy" styleId="loiCopy">                      
                      <html:option value="No">No</html:option>
                      <html:option value="Yes">Yes</html:option>
                      </html:select>
                       
                      </td>
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.deliverytype"/>
                      </td>
                      <td   style="width:50%">
                        <bean:define name="enquiryForm" property="alDeliveryTypeList" id="alDeliveryTypeList" type="java.util.Collection" /> 
                    	<html:select  name="enquiryForm" property="deliveryType">

                       	 	<html:options name="enquiryForm" collection="alDeliveryTypeList" property="key" labelProperty="value" />
                        </html:select>                   	                      	
                       
                      </td>
                    
                    </tr>
                    
                    
                      <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.deliveryreqcustomer"/>
                      </td>
                      <td   style="width:50%">
                      <input type="hidden" name="dvryReqCust" id="dvryReqCust" value="<bean:write name="enquiryForm" property="deliveryReqByCust" />" />
                       <html:select property="deliveryReqByCust" styleId="deliveryReqByCust">
                         <option value="06 Weeks">6 Weeks</option>
                         <option value="07 Weeks">7 Weeks</option>
                         <option value="08 Weeks">8 Weeks</option>
                         <option value="09 Weeks">9 Weeks</option>
                         <option value="10 Weeks">10 Weeks</option>
                         <option value="11 Weeks">11 Weeks</option>
                         <option value="12 Weeks">12 Weeks</option>
                         <option value="13 Weeks">13 Weeks</option>
                         <option value="14 Weeks">14 Weeks</option>
                         <option value="15 Weeks">15 Weeks</option>
                         <option value="16 Weeks">16 Weeks</option>
                         <option value="17 Weeks">17 Weeks</option>
                         <option value="18 Weeks">18 Weeks</option>
                         <option value="19 Weeks">19 Weeks</option>
                         <option value="20 Weeks">20 Weeks</option>
                      </html:select>
                       
                      </td>
                    
                    </tr>
                    
                      <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.expecteddeliverymonth"/>
                      </td>
                      <td   style="width:50%">
                      <input type="hidden" name="exptedDvryDt" id="exptedDvryDt" value="<bean:write name="enquiryForm" property="expectedDeliveryMonth" />" />
                     <html:select  property="expectedDeliveryMonth" styleId="expectedDeliveryMonth" >
                         <option value="01 Month">1 Month</option>
                         <option value="02 Months">2 Months</option>
                         <option value="03 Months">3 Months</option>
                         <option value="04 Months" >4 Months</option>
                         <option value="05 Months">5 Months</option>
                         <option value="06 Months">6 Months</option>
                         <option value="07 Months">7 Months</option>
                         <option value="08 Months">8 Months</option>
                         <option value="09 Months">9 Months</option>
                         <option value="10 Months">10 Months</option>
                         <option value="11 Months">11 Months</option>
                         <option value="12 Months">12 Months</option>

                        </html:select>                   	                      	
                       
                      </td>
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.advancepayment"/>
                      </td>
                      <td   style="width:50%">
             				     <html:text property="advancePayment" styleId="advancePayment" styleClass="normal" maxlength="50"   onkeypress="return AllowAlphabet(event)"  /></td>          
                     
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.paymentterms"/>
                      </td>
                      <td   style="width:50%">
   				     <html:text property="paymentTerms" styleId="paymentTerms" styleClass="normal"  maxlength="50"  onkeypress="return AllowAlphabet(event)" /> 
                       
                      </td>
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.ld"/>
                      </td>
                      <td   style="width:50%">
                      <html:select property="ld" styleId="ld">                      
                      <html:option value="No">No</html:option>
                      <html:option value="Yes">Yes</html:option>
                      </html:select>
                       
                      </td>
                    
                    </tr>
                      
                      <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.wfd"/>
                      </td>
                      <td   style="width:50%">
                      <input type="hidden" name="wrtyDispDt" id="wrtyDispDt" value="<bean:write name="enquiryForm" property="warrantyDispatchDate" />" />
                      <html:select property="warrantyDispatchDate" styleId="warrantyDispatchDate"  >
                    		<option value="12">12</option>
                    		<option value="18" >18</option>
                    		<option value="24">24</option>
                    	    <option value="30">30</option>
                            <option value="36">36</option>
                            <option value="42">42</option>
                            <option value="48">48</option>
                            <option value="54">54</option>
                            <option value="60">60</option>
        	            </html:select>
                      </td>
                    
                    </tr>
                    
                      <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.wfc"/>
                      </td>
                      <td   style="width:50%">
                      <input type="hidden" name="wrtyCmsg" id="wrtyCmsg" value="<bean:write name="enquiryForm" property="warrantyCommissioningDate" />" />
                      <html:select property="warrantyCommissioningDate" styleId="warrantyCommissioningDate"  >
                    		<option value="12" >12</option>
                    		<option value="18">18</option>
                    		<option value="24">24</option>
                    	    <option value="30">30</option>
                            <option value="36">36</option>
                            <option value="42">42</option>
                            <option value="48">48</option>
                            <option value="54">54</option>
                            <option value="60">60</option>
        	            </html:select>
                       
                      </td>
                    
                    </tr>
                    
                      <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.drawingapproval"/>
                      </td>
                      <td   style="width:50%">
                      <html:select property="drawingApproval" styleId="drawingApproval">
                       <html:option value="No">No</html:option>
                       <html:option value="Yes">Yes</html:option>
                      </html:select>
                       
                      </td>
                    
                    </tr>
                    
                      <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.qapapproval"/>
                      </td>
                      <td   style="width:50%">
                       <html:select property="qapApproval" styleId="qapApproval">
                       <html:option value="No">No</html:option>
                       <html:option value="Yes">Yes</html:option>
                      </html:select>
                       
                      </td>
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.typetest"/>
                      </td>
                      <td   style="width:50%">
                      <html:select property="typeTest" styleId="typeTest">
                      <html:option value="No">No</html:option>
                       <html:option value="Yes">Yes</html:option>
                      </html:select>
                       
                      </td>
                    
                    </tr>
                    
                      <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.routinetest"/>
                      </td>
                      <td   style="width:50%">
                       <html:select property="routineTest" styleId="routineTest">
                       <html:option value="No">No</html:option>
                       <html:option value="Yes">Yes</html:option>
                      </html:select>
                       
                      </td>
                    
                    </tr>
                    
                    
                    
                </table>
                </div><!-- Left Table Column  -->
                <div class="tablecolumn" >
  				   <table width="100%" border="0" cellspacing="0" cellpadding="2">
                 	<tr>
                    <td colspan="2" >&nbsp;</td>
                    </tr>

                    <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.specialtest"/>
                      </td>
                      <td  style="width:50% ">
                       <html:select property="specialTest" styleId="specialTest">
                       <html:option value="No">No</html:option>
                       <html:option value="Yes">Yes</html:option>
                      </html:select>

                      </td>    
                    
                    </tr>
                    <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.specialtestdet"/>
                      </td>
                      <td  style="width:50% ">
                       <html:text property="specialTestDetails" styleId="specialTestDetails"  maxlength="50" styleClass="normal" />
                      </td>    
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.thirdpartyinspection"/>
                      </td>
                      <td  style="width:50% ">

                       <html:select property="thirdPartyInspection" styleId="thirdPartyInspection">
                       <html:option value="No">No</html:option>
                       <html:option value="Yes">Yes</html:option>
                      </html:select>

                      </td>    
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.stageinspection"/>
                      </td>
                      <td  style="width:50% ">
                       <html:select property="stageInspection" styleId="stageInspection">
                      <html:option value="No">No</html:option>
                       <html:option value="Yes">Yes</html:option>
                      </html:select>

                      </td>    
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.enduser"/>
                      </td>
                      <td  style="width:50% ">
					<html:text property="endUser" styleId="endUser"  maxlength="50" styleClass="normal" />
                      </td>    
                    
                    </tr>
                      
                      <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.enduserlocation"/>
                      </td>
                      <td  style="width:50% ">

                   <html:text property="enduserLocation" styleId="enduserLocation"  maxlength="50" styleClass="normal" />
                      </td>    
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.enduserindustry"/>
                      </td>
                      <td  style="width:50% ">
						<html:text property="endUserIndustry" styleId="endUserIndustry"  maxlength="50" styleClass="normal" />
                      </td>    
                    
                    </tr>
                    
                    
                    <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.consultant"/>
                      </td>
                      <td  style="width:50% ">
					<html:text property="consultantName" styleId="consultantName"  maxlength="50" styleClass="normal" />
                      </td>    
                    
                    </tr>
                    
                    
                      <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.epcname"/>
                      </td>
                      <td  style="width:50% ">
					<html:text property="epcName" styleId="epcName"  maxlength="50" styleClass="normal" />
                      </td>    
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.projectname"/>
                      </td>
                      <td  style="width:50% ">
					<html:text property="projectName" styleId="projectName"  maxlength="50" styleClass="normal" />
                      </td>    
                    
                    </tr>
                    
                       <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.packing"/>
                      </td>
                      <td  style="width:50% ">
			          <html:select property="packing" styleId="packing">
                      <html:option value="Standard">Standard</html:option>
                      <html:option value="Export">Export</html:option>
                      </html:select>
			
                      </td>    
                    
                    </tr>
                    
                    
                    <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.insurance"/>
                      </td>
                      <td  style="width:50% ">
					<html:select property="insurance" styleId="insurance">
                      <html:option value="Company">Company</html:option>
                      <html:option value="Customer">Customer</html:option>
                      </html:select>
                      </td>    
                    
                    </tr>
                    
                    <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.replacement"/>
                      </td>
                      <td  style="width:50% ">
					<html:select property="replacement" styleId="replacement">
                      <html:option value="No">No</html:option>
                       <html:option value="Yes">Yes</html:option>
                      </html:select>
                      </td>    
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.oldserialno"/>
                      </td>
                      <td  style="width:50% ">
					 <html:text property="oldSerialNo" styleId="oldSerialNo"  maxlength="50" styleClass="normal" />
                      </td>    
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.customerpartno"/>
                      </td>
                      <td  style="width:50% ">
					  <html:text property="customerPartNo" styleId="customerPartNo"  maxlength="50" styleClass="normal" />
                      </td>    
                    
                    </tr>
                      
                      <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.winreason"/>
                      </td>
                      <td  style="width:50% ">
				     <bean:define name="enquiryForm" property="alWinlossReasonList" id="alWinlossReasonList" type="java.util.Collection" /> 
                    	<html:select  name="enquiryForm" property="winlossReason" >
                    		<option value=""><bean:message key="quotation.option.select"/></option>
                       	 	<html:options name="enquiryForm" collection="alWinlossReasonList" property="key" labelProperty="value" />
                        </html:select>  

                      </td>    
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.commentsifany"/>
                      </td>
                      <td  style="width:50% ">
					 <html:text property="winlossComment" styleId="winlossComment" maxlength="50" styleClass="normal" />
                      </td>    
                    
                    </tr>
                    
                     <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.competitor"/>
                      </td>
                      <td  style="width:50% ">
				      <bean:define name="enquiryForm" property="alWinCompetitorList" id="alWinCompetitorList" type="java.util.Collection" /> 
                    	<html:select  name="enquiryForm" property="winningCompetitor">
                    		<option value=""><bean:message key="quotation.option.select"/></option>
                       	 	<html:options name="enquiryForm" collection="alWinCompetitorList" property="key" labelProperty="value" />
                        </html:select>  

                      </td>    
                    
                    </tr>
                    </table>
				</div><!--Right Table End  -->
                </div><!--Table Row End  -->
                
                <!--Financial  Start-->
			
                  <%--   <fieldset>
                     <legend class="blueheader">	
			         <bean:message key="quotation.create.label.financial"/>
			        </legend>
					 <div class="blue-light" >
            		<table width="100%" border="0" cellspacing="0" cellpadding="0">
				  	<tr>
					<td>
                   <bean:message key="quotation.create.label.totalmotorprice"/>
                   <logic:equal  name="enquiryForm" property="totalMotorPrice" value="0">
				   <html:text property="totalMotorPrice" styleId="totalMotorPrice" value="" styleClass="normal" />	
				   </logic:equal>	
				   <logic:notEqual  name="enquiryForm" property="totalMotorPrice" value="0">
				   <html:text property="totalMotorPrice" styleId="totalMotorPrice"  styleClass="normal" />	
				   </logic:notEqual>			  
						  
				  </td>
				   <td align="right">
				  <bean:message key="quotation.create.label.totalpackageprice" />
				  <logic:equal  name="enquiryForm" property="totalPkgPrice" value="0">
				  <html:text property="totalPkgPrice" styleId="totalPkgPrice" value="" styleClass="normal" />
				  </logic:equal>
				  <logic:notEqual  name="enquiryForm" property="totalPkgPrice" value="0">
				  <html:text property="totalPkgPrice" styleId="totalPkgPrice"  styleClass="normal" />
				  </logic:notEqual>

				  </td>
				  
				  </tr>
				  
				  
				  </table>
				  </div> --%>
				  		     <%--  <div class="tablerow">
                <div class="tablecolumn" >
              <fieldset style="width:100%">
              <legend class="blueheader">	
			  <bean:message key="quotation.create.label.includes"/>
			  </legend>
              				  <table  style="width: 100%" border="0" cellspacing="0" cellpadding="0">
				  <tr><td colspan="5">&nbsp;</td></tr>
				  <tr>
				  <td  colspan="3">
				  <bean:message key="quotation.create.label.freight" />
				  </td>
				  <td  style="width:50% ">
				   
				  <logic:equal  name="enquiryForm" property="includeFrieght" value="0">
				  <html:text property="includeFrieght" styleId="includeFrieght" value="" styleClass="normal" />
				  </logic:equal>
				  <logic:notEqual name="enquiryForm" property="includeFrieght" value="0">
				  <html:text property="includeFrieght" styleId="includeFrieght" styleClass="normal" />
				  </logic:notEqual>

				  </td>
				  </tr>
		          <tr>
				  <td colspan="3">
				  <bean:message key="quotation.create.label.extrawarranty" />
				  </td>
				  <td  style="width:50% ">
				  <logic:equal  name="enquiryForm" property="includeExtraWarrnt" value="0">
				  <html:text property="includeExtraWarrnt" styleId="includeExtraWarrnt" value="" styleClass="normal" />
				  </logic:equal>
				  <logic:notEqual name="enquiryForm" property="includeExtraWarrnt" value="0">
				  <html:text property="includeExtraWarrnt" styleId="includeExtraWarrnt" styleClass="normal" />
				  </logic:notEqual>
				  
				  </td>
				  </tr>
				  <tr>
				  <td colspan="3">
				  <bean:message key="quotation.create.label.supervisionandcomm" />
				  </td>
				  <td  style="width:50% ">
				  <logic:equal  name="enquiryForm" property="includeSupervisionCost" value="0">
				   <html:text property="includeSupervisionCost" styleId="includeSupervisionCost" value="" styleClass="normal" />
				   </logic:equal>
				  <logic:notEqual name="enquiryForm" property="includeSupervisionCost" value="0">
				   <html:text property="includeSupervisionCost" styleId="includeSupervisionCost" styleClass="normal" />
				  </logic:notEqual>
				  </td>
				  </tr>
					<tr>
				  <td colspan="3">
				  <bean:message key="quotation.create.label.typetestcharge" />
				  </td>
				  <td  style="width:50% ">
				 <logic:equal  name="enquiryForm" property="includetypeTestCharges" value="0">
				  <html:text property="includetypeTestCharges" styleId="includetypeTestCharges" value="" styleClass="normal" />
				  </logic:equal>
				  <logic:notEqual name="enquiryForm" property="includetypeTestCharges" value="0">
				  <html:text property="includetypeTestCharges" styleId="includetypeTestCharges" styleClass="normal" />
				  </logic:notEqual>
				  </td>
				  </tr>
				  
				  
                  
		    </table>
                    </fieldset>
                    </div><!-- Left Div Endings  -->
                    <div class="tablecolumn" >
              <fieldset style="width:100%">
              <legend class="blueheader">	
			  <bean:message key="quotation.create.label.extra"/>
			  </legend>
                    
				  <table  style="width: 100%" border="0" cellspacing="0" cellpadding="0">
				  <tr><td colspan="5">&nbsp;</td></tr>
				  <tr>
				  <td  colspan="3">
				  <bean:message key="quotation.create.label.freight" />
				  </td>
				  <td  style="width:50% ">
				 <logic:equal  name="enquiryForm" property="extraFrieght" value="0">
				  <html:text property="extraFrieght" styleId="extraFrieght" value="" styleClass="normal" />
				  </logic:equal>
				  <logic:notEqual name="enquiryForm" property="extraFrieght" value="0">
				  <html:text property="extraFrieght" styleId="extraFrieght" styleClass="normal" />
				  </logic:notEqual>
				  </td>
				  </tr>
				  
			     <tr>
				  <td  colspan="3">
				  <bean:message key="quotation.create.label.extrawarrantycharges" />
				  </td>
				  <td  style="width:50% ">
				  <logic:equal  name="enquiryForm" property="extraExtraWarrnt" value="0">
				  <html:text property="extraExtraWarrnt" styleId="extraExtraWarrnt" value="" styleClass="normal" />
				  </logic:equal>
				  <logic:notEqual name="enquiryForm" property="extraExtraWarrnt" value="0">
				  <html:text property="extraExtraWarrnt" styleId="extraExtraWarrnt" styleClass="normal" />
				  </logic:notEqual>
				  </td>
				  </tr>
				  <tr>
				  <td  colspan="3">
				  <bean:message key="quotation.create.label.supervisionandcomm" />
				  </td>
				  <td  style="width:50% ">
				  <logic:equal  name="enquiryForm" property="extraSupervisionCost" value="0">
				  <html:text property="extraSupervisionCost" styleId="extraSupervisionCost" value="" styleClass="normal" />
				  </logic:equal>
				  <logic:notEqual  name="enquiryForm" property="extraSupervisionCost" value="0">
				  <html:text property="extraSupervisionCost" styleId="extraSupervisionCost"  styleClass="normal" />
				  </logic:notEqual>
				  
				  </td>
				  </tr>
				  
				  <tr>
				  <td  colspan="3">
				  <bean:message key="quotation.create.label.spares" />
				  </td>
				  <td  style="width:50% ">
				  <logic:equal  name="enquiryForm" property="sparesCost" value="0">
				  <html:text property="sparesCost" styleId="sparesCost" value="" styleClass="normal" />
				  </logic:equal>
				  	<logic:notEqual  name="enquiryForm" property="sparesCost" value="0">
				  <html:text property="sparesCost" styleId="sparesCost"  styleClass="normal" />
				  </logic:notEqual>
				  
				  </td>
				  </tr>
				  
				  
				  
		    </table>
                    </div><!--Right Div Ending  -->
                    </div><!--Final Div Ending  -->
		                    
		                    
		                    
				  
				  
				  </fieldset> --%>
                
                <!--Financial End  -->
              
              <!--Rating List Starting  -->
              <div style="clear:both; float:left; width:90%; height:32px; margin-bottom:30px;">
              <div id="viewtopnav">
			  <ul>
			<logic:notEmpty name="enquiryForm" property="ratingList">

 			<logic:iterate name="enquiryForm" property="ratingList"
               id="ratingListData" indexId="idx"
               type="in.com.rbc.quotation.common.vo.KeyValueVo">
              <%  countval=countval+1;

               %>
		  </logic:iterate>
          <logic:iterate name="enquiryForm" property="ratingList"
               id="ratingListData" indexId="idx"
               type="in.com.rbc.quotation.common.vo.KeyValueVo">
          <logic:equal name="ratingListData" property="value2" value="Y">

		<li>
		<%if(countval>1){ %>
 		<a href="javascript:getNextRatingWonIndentPage('<bean:write name="ratingListData" property="key" />')"  class="valid"> 
		<%} %>

	<b>

	<logic:equal name="ratingListData" property="key" value="<%= latestId.toString() %>">
	<bean:write name="ratingListData" property="value"/>
	</logic:equal>
	<logic:notEqual name="ratingListData" property="key" value="<%= latestId.toString() %>">
	<bean:write name="ratingListData" property="value"/>
	</logic:notEqual>
	</b>

	<%if(countval>1){ %>
	</a>
	<%} %>
	</li>
	</logic:equal>
 	<logic:equal name="ratingListData" property="value2" value="N">

 	<li>
 	<%if(countval>1){ %>
 		<a href="javascript:getNextRatingWonIndentPage('<bean:write name="ratingListData" property="key" />')" class="notvalid" >
	<%} %>
 		
 			<logic:equal name="ratingListData" property="key" value="<%= latestId.toString() %>">
 				<b><bean:write name="ratingListData" property="value"/></b>
 			</logic:equal>
 			<logic:notEqual name="ratingListData" property="key" value="<%= latestId.toString() %>">
 				<bean:write name="ratingListData" property="value"/>
 			</logic:notEqual>
 			<%if(countval>1){ %>
 		</a>
 		<%} %>
 	</li>	
 	</logic:equal>	
 
	</logic:iterate>

	</logic:notEmpty>
	</ul>
	</div>
	</div>
             
             <!--Ratings List Ending  --> 
            <fieldset>
			<div class="blue-light" >
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td>
				
				  <bean:message key="quotation.create.label.quantityrequired"/>
				 <html:text property="quantity" styleId="quantity"  styleClass="normal" /> 
			
					</td>
					<td>
					&nbsp;
				</td>
				  </tr>
				</table>
		        	</div> <!--Quantity Ending  -->
		        	
		      
			    <br>
			   	  <div class="tablerow">
                  <div class="tablecolumn" >
              	  <table  style="width: 100%" border="0" cellspacing="0" cellpadding="0">
				  <tr><td colspan="2">&nbsp;</td></tr>
			      <tr>
			       <td >
                    	<bean:message key="quotation.create.label.kw"/>
                    	
                    </td>
                    <td class="formContentview-rating-b0x">
                    <bean:write name="enquiryForm" property="kw" />

						<logic:notEqual name="enquiryForm" property="kw" value="">
						<bean:write name="enquiryForm" property="ratedOutputUnit" />
						</logic:notEqual>
						                    	
                    </td>
                    </tr>	
                    <tr>
                    <td>
			        <bean:message key="quotation.create.label.pole"/>
			        </td>
                    <td class="formContentview-rating-b0x">
                    <bean:write name="enquiryForm" property="poleName" />
                    </td>
                    
                    </tr>
                    <tr>
                    <td>
			        	<bean:message key="quotation.create.label.scsr"/>
			        </td>
                    <td class="formContentview-rating-b0x">
                    <bean:write name="enquiryForm" property="scsrName" />
                   	</td>
                    
                    </tr>
                     <tr>
                    <td>
			        	<bean:message key="quotation.offerdata.technical.label.framesize"/>
			        </td>
                    <td class="formContentview-rating-b0x">
                    <bean:write name="enquiryForm" property="frameSize" />
                   	</td>
                    
                    </tr>
                    <tr>
                   <td >
                    	<bean:message key="quotation.create.label.volts"/> 
                    </td>
                    <td class="formContentview-rating-b0x">
                    <logic:notEqual name="enquiryForm" property="volts" value="">
              			<bean:write name="enquiryForm" property="volts"/>
              		</logic:notEqual>
              		<logic:notEqual name="enquiryForm" property="voltsVar" value="">
	              		 +/-<bean:write name="enquiryForm" property="voltsVar"/> %
	              	</logic:notEqual>
                    
                    </td>
                    
                    </tr>
                    <tr>
                   <td >
                    	<bean:message key="quotation.create.label.enclosure"/>
                    	
                    </td>
                    <td class="formContentview-rating-b0x">

              			<bean:write name="enquiryForm" property="enclosureName"/>
                    </td>
                    
                    </tr> 
                    <tr>
                   <td >
                    	<bean:message key="quotation.create.label.mounting"/>
                    	
                    </td>
                    <td class="formContentview-rating-b0x">

              			<bean:write name="enquiryForm" property="mountingName"/>
                    </td>
                    </tr>   
                    <tr>
                    <td>
			        	<bean:message key="quotation.create.label.application"/>
			        	
			        </td>
					<td class="formContentview-rating-b0x">

              			<bean:write name="enquiryForm" property="applicationName"/>
                    </td>
                    
                    </tr>   
                    <tr>
                 	<td><bean:message key="quotation.offer.label.maintbps"/></td>
					<td class="formContentview-rating-b0x">
              			<bean:write name="enquiryForm" property="mainTermBoxPS"/>
                    </td>
			        </tr> 
                    <tr>
                 <td><bean:message key="quotation.offer.label.neutraltb"/></td>
               <td class="formContentview-rating-b0x">
              			<bean:write name="enquiryForm" property="neutralTermBox"/>
                    </td>
                    </tr>
                    <tr>
                   <td><bean:message key="quotation.offer.label.spaceheater" />
				  </td>
				   <td class="formContentview-rating-b0x">
              			<bean:write name="enquiryForm" property="spaceHeater"/>
                    </td>
				  
                    
                    </tr>
                    <tr>
                    <td>
                    
                   <bean:message key="quotation.create.label.windingrtd" />
				  </td>
				  <td class="formContentview-rating-b0x">
              			<bean:write name="enquiryForm" property="windingRTD"/>
                    </td>

				  </tr>
				  
				   <tr>
				  <td  >
				  <bean:message key="quotation.create.label.bearingrtd" />
				  </td>
				  <td class="formContentview-rating-b0x">
              			<bean:write name="enquiryForm" property="bearingRTD"/>
                    </td>
				  </tr>
				  
				  <tr>
				  <td  >
				  <bean:message key="quotation.create.label.momentofInertiaload" />
				  </td>
				    <td class="formContentview-rating-b0x">
              			<bean:write name="enquiryForm" property="rtGD2"/>
              			&nbsp;<bean:message key="quotation.units.kgm2"/>
                    </td>
				  </tr>
				  
				  				  <tr>
				  <td>
				  <bean:message key="quotation.create.label.staringcurrent" />
				  </td>
				    <td class="formContentview-rating-b0x">
              			<bean:write name="enquiryForm" property="strtgCurrent"/>
                    </td>
							  </tr>
                    
                    
                    
				  </table>
				  </div><!--Left Column Ending  -->
				  <div class="tablecolumn" >
				   <table  style="width: 100%" border="0" cellspacing="0" cellpadding="0">
				  <tr><td colspan="2">&nbsp;</td></tr>
				  <tr>
				<td>
			        	<bean:message key="quotation.create.label.htlt"/>
			        </td>
			        <td class="formContentview-rating-b0x">
              			<bean:write name="enquiryForm" property="htltName"/>
                    </td>
					</tr>
					<tr>
					 <td>
                    	<bean:message key="quotation.create.label.frequency"/>
                    </td>
                    <td class="formContentview-rating-b0x">

              		<logic:notEqual name="enquiryForm" property="frequency" value="">
              				<bean:write name="enquiryForm" property="frequency"/>
              			</logic:notEqual>
              			<logic:notEqual name="enquiryForm" property="frequencyVar" value="">
	              			+/-<bean:write name="enquiryForm" property="frequencyVar"/>%
	              		</logic:notEqual>
              			
                    </td>
                    
					</tr>
					<tr>
					 <td >
			        	<bean:message key="quotation.create.label.duty"/>
			        </td>
                    <td class="formContentview-rating-b0x">
                    <bean:write name="enquiryForm" property="dutyName"/>
					</td>				
					
					</tr>
					<tr>
					<td >
                    	<bean:message key="quotation.create.label.ambience"/>
                    </td>
                    <td class="formContentview-rating-b0x">
                  	<logic:notEqual name="enquiryForm" property="ambience" value="">
                  		<bean:write name="enquiryForm" property="ambience"/> 
                  	</logic:notEqual>
                  	
                  	<logic:notEqual name="enquiryForm" property="ambienceVar" value="">
	                  	+/-<bean:write name="enquiryForm" property="ambienceVar"/>
	                </logic:notEqual>

					</td>				
					
					</tr>
					<tr>
				   <td  ><bean:message key="quotation.create.label.degpro"/> </td>
                    <td class="formContentview-rating-b0x">
                 	<bean:write name="enquiryForm" property="degreeOfProName"/> 
                    </td>				

					</tr>
					<tr>
					<td ><bean:message key="quotation.create.label.altitude"/></td>
                    <td class="formContentview-rating-b0x">
                 	<logic:notEqual name="enquiryForm" property="altitude" value="">
                 		<bean:write name="enquiryForm" property="altitude"/> 
                 	</logic:notEqual>
                 	<logic:notEqual name="enquiryForm" property="altitudeVar" value="">
                 		+/-<bean:write name="enquiryForm" property="altitudeVar"/> 
                 	</logic:notEqual>

                    </td>				
					
					</tr>
					<tr>
					    <td  ><bean:message key="quotation.create.label.stgmethod"/> </td>
                    <td class="formContentview-rating-b0x">
             	 	<bean:write name="enquiryForm" property="methodOfStg"/>                    
             	  </td>				
					
					</tr>
					<tr>
					 <td >
                    	<bean:message key="quotation.create.label.temprise"/> 
                    </td>
                    <td class="formContentview-rating-b0x">
	              	 	<bean:write name="enquiryForm" property="tempRaise"/> 
             	  </td>				
					
					</tr>
					<tr>
					<td  >
			        	<bean:message key="quotation.create.label.coupling"/>
			        </td>
                    <td class="formContentview-rating-b0x">
					<bean:write name="enquiryForm" property="coupling"/>
             	  </td>				
					</tr>
					<tr>
					 <td >
                    	<bean:message key="quotation.create.label.insulationclass"/> </td>
                    <td class="formContentview-rating-b0x">
					<bean:write name="enquiryForm" property="insulationName"/>
             	  </td>				
					
					</tr>
					<tr>
					<td>
			        	<bean:message key="quotation.create.label.termbox"/> 
			        </td>
                    <td class="formContentview-rating-b0x">
              	<bean:write name="enquiryForm" property="termBoxName"/> 
             	  </td>				
					</tr>
					<tr>
					<td  >
			        	<bean:message key="quotation.create.label.rotdirection"/>
			        </td>
                    <td class="formContentview-rating-b0x">
              	<bean:write name="enquiryForm" property="directionOfRotName"/> 
             	  </td>				
					
					</tr>
					<tr>
				  <td  >
			        	<bean:message key="quotation.create.label.shaftext"/>
			        </td>
                    <td class="formContentview-rating-b0x">
              	<bean:write name="enquiryForm" property="shaftExtName"/> 
             	  </td>				
					
					</tr>
					<tr>
					<td >
                    	<bean:message key="quotation.create.label.paint"/>
                    </td>
                    
                    <td class="formContentview-rating-b0x">
              	<bean:write name="enquiryForm" property="paint"/> 
             	  				
                    </td>
					
					</tr>
					<tr>
					<td ><bean:message key="quotation.create.label.ratingunitprice"/></td>
                    <td class="formContentview-rating-b0x">
              	<bean:write name="enquiryForm" property="ratingUnitPrice"/> 
             	  </td>				
					
					</tr>
				  </table>
				  
				  </div>			
				  </div><!--Table Row Ending  -->
			                  <!--Financial  Start-->
			
                     <fieldset>
                     <legend class="blueheader">	
			         <bean:message key="quotation.create.label.financial"/>
			        </legend>
					<div class="blue-light" >
            		<table width="100%" border="0" cellspacing="0" cellpadding="0">
				  	<tr>
					<td>
                   <bean:message key="quotation.create.label.unitpricemotor" />
				   <html:text property="unitpriceMultiplier_rsm" styleId="unitpriceMultiplier_rsm" styleClass="normal" />				  
				  </td>
				  </tr>
				  </table>
				  </div>
				  		      <div class="tablerow">
                <div class="tablecolumn" >
              <fieldset style="width:100%">
              <legend class="blueheader">	
			  <bean:message key="quotation.create.label.priceincludes"/>
			  </legend>
              				  <table  style="width: 100%" border="0" cellspacing="0" cellpadding="0">
				  <tr><td colspan="5">&nbsp;</td></tr>
				  <tr>
				  <td  colspan="3" style="width:50%">
				  <bean:message key="quotation.create.label.freight" />
				  </td>
				  <td  style="width:15% ">
				  
				  <bean:define id="deliveryType" name="enquiryForm" property="deliveryType" />
				 <html:select property="priceincludeFrieght" style="width:50px;" onchange="getFinancialValues(id,this.value);" styleId="priceincludeFrieght">
				
				 <% if(deliveryType.equals("FOB")||deliveryType.equals("FOR") ){%>
					<html:option value="1">Yes</html:option>
				  	<html:option value="0">No</html:option>
				  <%}else{ %>
				  <html:option value="1">No</html:option>
				  <html:option value="0">Yes</html:option>
				  <%} %>
				 
				  </html:select>
				  </td>
				  <td  style="width:35% ">
				  <html:text property="priceincludeFrieghtVal" styleId="priceincludeFrieghtVal"  style="width:70px;height:18px;" />
				  </td>
				   
				  </tr>
		          <tr>
				  <td colspan="3" style="width:50%">
				  <bean:message key="quotation.create.label.extrawarranty" />
				  </td>
				  <td  style="width:15% ">
				 
				 <html:select property="priceincludeExtraWarn" style="width:50px" onchange="getFinancialValues(id,this.value);" styleId="priceincludeExtraWarn">
				  <html:option value="1">Yes</html:option>
				  <html:option value="0">No</html:option>
				  </html:select>
				 </td>
				  <td  style="width:35% ">
		      <html:text property="priceincludeExtraWarnVal" styleId="priceincludeExtraWarnVal" style="width:70px;height:18px;" /> 
				  </td>
				  </tr>
				  <tr>
				  <td colspan="3" style="width:50%">
				  <bean:message key="quotation.create.label.supervisionandcomm" />
				  </td>
				  <td  style="width:15% ">
				 <html:select property="priceincludeSupervisionCost" style="width:50px" onchange="getFinancialValues(id,this.value);" styleId="priceincludeSupervisionCost">
				  <html:option value="1">Yes</html:option>
				  <html:option value="0">No</html:option>
				  </html:select>
				  </td>
				  <td  style="width:35% ">
				 <html:text property="priceincludeSupervisionCostVal" styleId="priceincludeSupervisionCostVal" style="width:70px;height:18px;" /> 
				  
				  </td>
				  </tr>
					<tr>
				  <td colspan="3 style="width:50%">
				  <bean:message key="quotation.create.label.typetestingcharges" />
				  </td>
				  <td  style="width:15% ">
							  <html:select property="priceincludetypeTestCharges" style="width:50px;" onchange="getFinancialValues(id,this.value);" styleId="priceincludetypeTestCharges">
				  <html:option value="1">Yes</html:option>
				  <html:option value="0">No</html:option>
				  </html:select>
				  </td>
				  <td  style="width:35% ">
			<html:text property="priceincludetypeTestChargesVal" styleId="priceincludetypeTestChargesVal" style="width:70px;height:18px;" />  
				  </td>
				  </tr>
				  
				  
                  
		    </table>
                    </fieldset>
                    </div><!-- Left Div Endings  -->
                    <div class="tablecolumn" >
              <fieldset style="width:100%">
              <legend class="blueheader">	
			  <bean:message key="quotation.create.label.extra"/>
			  </legend>
                    
				  <table  style="width: 100%" border="0" cellspacing="0" cellpadding="0">
				  <tr><td colspan="5">&nbsp;</td></tr>
				  <tr>
				  <td  colspan="3" style="width:50% ">
				  <bean:message key="quotation.create.label.freight" />
				  </td>
				 <td  style="width:15% ">
				  <html:select property="priceextraFrieght" style="width:50px;" onchange="getFinancialValues(id,this.value);" styleId="priceextraFrieght">
				  <html:option value="1">Yes</html:option>
				  <html:option value="0">No</html:option>
				  </html:select>
				  </td>
				  <td  style="width:35% ">
			<html:text property="priceextraFrieghtVal" styleId="priceextraFrieghtVal" style="width:70px;height:18px;" />  				  
				  </td>
				  </tr>
				  
			     <tr>
				  <td  colspan="3" style="width:50% ">
				  <bean:message key="quotation.create.label.extrawarranty" />
				  </td>
				  <td  style="width:15% ">
				  
				 <html:select property="priceextraExtraWarn" style="width:50px;" onchange="getFinancialValues(id,this.value);" styleId="priceextraExtraWarn">
				  <html:option value="1">Yes</html:option>
				  <html:option value="0">No</html:option>
				  </html:select>
				  </td>
				  <td  style="width:35% ">
			<html:text property="priceextraExtraWarnVal" styleId="priceextraExtraWarnVal" style="width:70px;height:18px;" />	  
				  </td>
				  </tr>
				  <tr>
				  <td  colspan="3" style="width:50% ">
				  <bean:message key="quotation.create.label.supervisionandcomm" />
				  </td>
				  <td  style="width: 15%">
		          <html:select property="priceextraSupervisionCost" style="width:50px;" onchange="getFinancialValues(id,this.value);" styleId="priceextraSupervisionCost">
				  <html:option value="1">Yes</html:option>
				  <html:option value="0">No</html:option>
				  </html:select>
				  </td>
				  <td  style="width:35% ">
				<html:text property="priceextraSupervisionCostVal" styleId="priceextraSupervisionCostVal" style="width:70px;height:18px;" />		  
				  </td>
				  </tr>
				  
				  <tr>
				  <td  colspan="3" style="width:50% ">
				  <bean:message key="quotation.create.label.typetestingcharges" />
				  </td>
				  <td  style="width: 15%">

				  <html:select property="priceextratypeTestCharges" style="width:50px;" styleId="priceextratypeTestCharges" onchange="getFinancialValues(id,this.value);">
				  <html:option value="1">Yes</html:option>
				  <html:option value="0">No</html:option>
				  </html:select>
				  
				  </td>
				  <td  style="width:35% ">
					<html:text property="priceextratypeTestChargesVal" styleId="priceextratypeTestChargesVal" style="width:70px;height:18px;" />	  
			
				  </td>
				  </tr>
				  
				  
		    </table>
                    </div><!--Right Div Ending  -->
                    </div><!--Final Div Ending  -->
		                    
		                    
		                    
				  
				  
				  </fieldset>
                
                <!--Financial End  -->
			         
           		    <fieldset style="width:100%">
		   <legend class="blueheader">	
			<bean:message key="quotation.create.label.deliverydetails"/>
		  </legend>
		  
		  
			
			  <table width="100%" cellpadding="0" cellspacing="0" id="deliverydetails" >
			    <tr>
			      <td height="100%" style="width:130px " class="formLabelTophome" >
			      	<bean:message key="quotation.create.label.deliverylocation"/> 
			      </td>
                  <td class="formLabelTophome">
                  	<bean:message key="quotation.create.label.deliveryaddress"/>
                  </td>
                  <td class="formLabelTophome">
                  	<bean:message key="quotation.create.label.noofmotors"/> 
                  </td>
                   <td class="formLabelTophome">
                  	<bean:message key="quotation.offerdata.addon.label.delete"/>
                  </td>
                  
                </tr>
                <input type="hidden" name="totalRowCount" id="totalRowCount" value="1"/>
                 <logic:notEmpty name="enquiryForm" property="deliveryDetailsList">
                	<% int k = 1; %>
                	<logic:iterate name="enquiryForm" property="deliveryDetailsList"
							               id="deliveryDetails" indexId="idx"
							               type="in.com.rbc.quotation.enquiry.dto.DeliveryDetailsDto">
						<% k = idx.intValue()+1; %> 
						
						 
			    <tr>     
		        <td>
			<input type="text" name="deliveryLocation<%=idx.intValue()+1 %>"  id = "deliveryLocation<%=idx.intValue()+1 %>" value='<bean:write name="deliveryDetails" property="deliveryLocation" />'  style="width:100px;"  maxlength="100"> 
		         </td>
		         <td align="left" >
		     <input type="text" style="width:600px; height:25px" onchange = "javascript:checkMaxLength(this.id, 4000);" id="deliveryAddress<%=idx.intValue()+1 %>" name="deliveryAddress<%=idx.intValue()+1 %>" value='<bean:write name="deliveryDetails" property="deliveryAddress" />' >
		         </td>
		       <td>
		       <input tabindex="19" class="short"  id="noOfMotors<%=idx.intValue()+1 %>" name="noOfMotors<%=idx.intValue()+1 %>" style="height:25px;" onkeypress="return isNumber(event)"  maxLength="9" value='<bean:write name="deliveryDetails" property="noofMotors" />'>
		      </td>
		                    <td>
		                    <% if (k > 0) { %>
		                    	<img src="html/images/deleteicon.gif" id="delete<%=idx.intValue()+1 %>" alt="Delete" onClick="removeDeliveryDetailsRow(<%=idx.intValue()+1 %>);"    style="cursor:hand;" />
		                    <%} %> 
		                    </td>
		                </tr>
		                
	                </logic:iterate>
	                <input type="hidden" name="hidRowIndex" id="hidRowIndex" value="<%=k%>">  
	                <div id="deleteimage1"></div>
                </logic:notEmpty>
                
               <logic:empty name="enquiryForm" property="deliveryDetailsList">
                <tr>
               	<td height="100%" style="width:130px " a class="formLabelTophome" >
  				<input type="text" name="deliveryLocation1"  id = "deliveryLocation1"  style="width:100px;"  maxlength="100">             	
			    </td>
                <td class="formLabelTophome" >
				<input type="text" style="width:600px; height:25px" onchange = "javascript:checkMaxLength(this.id, 4000);" id="deliveryAddress1" name="deliveryAddress1" />
                  </td>
                  <td class="formLabelTophome">
                   <input tabindex="19" class="short"  id="noOfMotors1" name="noOfMotors1" style="height:25px;" onkeypress="return isNumber(event)"  maxLength="9">
                  
                  	<input type="hidden" name="hidRowIndex" id="hidRowIndex" value="1">
                  </td>  
                     <td style="text-align:left;">
                     <div id="deleteimage1">
				  <img src="html/images/deleteicon.gif" alt="Delete" onClick="removeDeliveryDetailsRow(1);" id="delete1"  style="cursor:hand;" />
                  </div>
                  </td>
                               
                </tr>
		    </logic:empty>
		    
		   </table>
		            
         <div id="add1" class="blue-light" align="right" style="padding-right:25px; width:95%;">
	     <input type="button" class="BUTTON" onclick="javascript:addrowforIndentPage('deliverydetails');" value="Add" />
	     </div>	
		   
		    
             </fieldset>   <!--Delivery Details  -->
             
             
             
             </fieldset> <!-- Ratings Fieldset Ending -->
		    <!-- Field Section Ending -->
 		    		    <fieldset >
 		          <legend class="blueheader">	
			          	<bean:message key="quotation.create.label.notesection"/>
			          </legend>
 		    		    
			     <div  >
				  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr><td colspan="5">&nbsp;</td></tr>
			      				  <tr style="float:left; width:100%;">				  
				    <td class="formLabelrating" style="width:138px; margin-right: 33px;">
				    	<bean:message key="quotation.create.label.smnote"/>
				    </td>
                    <td colspan="4">
                    	<html:textarea  property="indentnote" styleId = "indentnote"  style="width:600px; height:50px" onkeyup="checkMaxLenghtText(this, 4000);" onkeydown="checkMaxLenghtText(this, 4000);" tabindex="35"/>   </td>
                    	<td></td>	
                    <td></td>
                  </tr>
		    </table>
		    </div>
		    </fieldset>
		    
		    
		    
		   <!-- Return Statement From DM Starts  -->
            <logic:notEqual name="enquiryForm" property="cmToSmReturn" value="">
            		    <fieldset >
 		          <legend class="blueheader">	
			          	<bean:message key="quotation.create.label.commentsection"/>
			          </legend>
 		    		    
			     <div>
			     
				  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr><td colspan="5" >&nbsp;</td></tr>
			      				  <tr style="float:left; width:100%;">				  
				    <td  style="width:138px; margin-right: 33px;">
				    
				    	<bean:message key="quotation.create.label.cmcomment"/>
				    	
				    </td>
				    
                    <td colspan="4" >
                    <font color="red">
						<bean:write name="enquiryForm" property="cmToSmReturn" />
						<input type="hidden" name="cmToSmReturn" id="cmToSmReturn" value="${enquiryForm.cmToSmReturn }"/>
                    </font>	                		         	
                    </td>
                    	<td></td>	
                    <td></td>
                  </tr>
		    </table>
		    </div>
		    </fieldset>
            
            </logic:notEqual>
            <!-- Return Statement From Dm Ends -->
            
            		   <!-- Return Statement From Commercial Engineer  Starts  -->
            <logic:notEqual name="enquiryForm" property="commengToSmReturn" value="">
            		    <fieldset >
 		          <legend class="blueheader">	
			          	<bean:message key="quotation.create.label.commentsection"/>
			          </legend>
 		    		    
			     <div>
			     
				  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr><td colspan="5" >&nbsp;</td></tr>
			      				  <tr style="float:left; width:100%;">				  
				    <td  style="width:138px; margin-right: 33px;">
				    
				    	<bean:message key="quotation.create.label.cecomment"/>
				    	
				    </td>
				    
                    <td colspan="4" >
                    <font color="red">
						<bean:write name="enquiryForm" property="commengToSmReturn" />
						<input type="hidden" name="commengToSmReturn" id="commengToSmReturn" value="${enquiryForm.commengToSmReturn }"/>
                    </font>	                		         	
                    </td>
                    	<td></td>	
                    <td></td>
                  </tr>
		    </table>
		    </div>
		    </fieldset>
            
            </logic:notEqual>
            <!-- Return Statement From Commercial Engineer Ends -->
            
            
            		   <!-- Return Statement From DM Starts  -->
            <logic:notEqual name="enquiryForm" property="indentDmtosmReturn" value="">
            		    <fieldset >
 		          <legend class="blueheader">	
			          	<bean:message key="quotation.create.label.commentsection"/>
			          </legend>
 		    		    
			     <div>
			     
				  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr><td colspan="5" >&nbsp;</td></tr>
			      				  <tr style="float:left; width:100%;">				  
				    <td  style="width:138px; margin-right: 33px;">
				    
				    	<bean:message key="quotation.create.label.dmcomment"/>
				    	
				    </td>
				    
                    <td colspan="4" >
                    <font color="red">
						<bean:write name="enquiryForm" property="indentDmtosmReturn" />
						<input type="hidden" name="indentDmtosmReturn" id="indentDmtosmReturn" value="${enquiryForm.indentDmtosmReturn }"/>
                    </font>	                		         	
                    </td>
                    	<td></td>	
                    <td></td>
                  </tr>
		    </table>
		    </div>
		    </fieldset>
            
            </logic:notEqual>
            <!-- Return Statement From Dm Ends -->
            
            		   <!-- Return Statement From Commercial Engineer  Starts  -->
            <logic:notEqual name="enquiryForm" property="indentDetosmReturn" value="">
            		    <fieldset >
 		          <legend class="blueheader">	
			          	<bean:message key="quotation.create.label.commentsection"/>
			          </legend>
 		    		    
			     <div>
			     
				  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr><td colspan="5" >&nbsp;</td></tr>
			      				  <tr style="float:left; width:100%;">				  
				    <td  style="width:138px; margin-right: 33px;">
				    
				    	<bean:message key="quotation.create.label.decomment"/>
				    	
				    </td>
				    
                    <td colspan="4" >
                    <font color="red">
						<bean:write name="enquiryForm" property="indentDetosmReturn" />
						<input type="hidden" name="indentDetosmReturn" id="indentDetosmReturn" value="${enquiryForm.indentDetosmReturn }"/>
                    </font>	                		         	
                    </td>
                    	<td></td>	
                    <td></td>
                  </tr>
		    </table>
		    </div>
		    </fieldset>
            
            </logic:notEqual>
            <!-- Return Statement From Commercial Engineer Ends -->
            
            
            

             <div class="blue-light-btn" style="width:100%" align="right">
              <logic:equal name="enquiryForm" property="statusId" value="6"> 
             <input type="button" class="BUTTON" value="Back"  onclick="javascript:viewEnquiryDetails('<bean:write name="enquiryForm" property="enquiryId" />','6');" />
   	         </logic:equal>
   	         <input type="button" class="BUTTON" value="Save"  onclick="javascript:getNextRatingWonIndentPage('<bean:write name="enquiryForm" property="currentRating" />');javascript:checkDeliveryData();" />
   	         <html:button property="" styleClass="BUTTON" value="Submit"  onclick="javascript:wonSubmitToEstimate(); javascript:checkDeliveryData();"/>
  
             </div>
                
                </td>
               </tr>
               
             </table>
                 


</html:form>
<script type="text/javascript">
$(document).ready(function () { 
	
	
	var poiCopy=document.getElementById("poiCopy").value;
	var loiCopy=document.getElementById("loiCopy").value;
	var deliveryReqByCust=document.getElementById("dvryReqCust").value;
	var expectedDeliveryMonth=document.getElementById("exptedDvryDt").value;
	var ld=document.getElementById("ld").value;
	var warrantyDispatchDate=document.getElementById("wrtyDispDt").value;
	var warrantyCommissioningDate=document.getElementById("wrtyCmsg").value;
	var drawingApproval=document.getElementById("drawingApproval").value;
	var qapApproval=document.getElementById("qapApproval").value;
	var typeTest=document.getElementById("typeTest").value;
	var routineTest=document.getElementById("routineTest").value;
	var specialTest=document.getElementById("specialTest").value;
	var thirdPartyInspection=document.getElementById("thirdPartyInspection").value;
	var stageInspection=document.getElementById("stageInspection").value;
	var packing=document.getElementById("packing").value;
	var insurance=document.getElementById("insurance").value;
	var replacement=document.getElementById("replacement").value;
	var priceincludeFrieght=document.getElementById("priceincludeFrieght").value;
	var priceincludeExtraWarn=document.getElementById("priceincludeExtraWarn").value;
	var priceincludeSupervisionCost=document.getElementById("priceincludeSupervisionCost").value;
	var priceincludetypeTestCharges=document.getElementById("priceincludetypeTestCharges").value;
	var priceextraFrieght=document.getElementById("priceextraFrieght").value;
	var priceextraExtraWarn=document.getElementById("priceextraExtraWarn").value;
	var priceextraSupervisionCost=document.getElementById("priceextraSupervisionCost").value;
	var priceextratypeTestCharges=document.getElementById("priceextratypeTestCharges").value;
    var delImg=document.getElementById("deleteimage1");  

	
	if(poiCopy!="")
	{
		document.getElementById("poiCopy").value=poiCopy;
	}
	if(loiCopy!="")
	{
		document.getElementById("loiCopy").value=loiCopy;
	}
	if(deliveryReqByCust!="")
	{
		document.getElementById("deliveryReqByCust").value=deliveryReqByCust;
	}
	if(expectedDeliveryMonth!="")
	{
		document.getElementById("expectedDeliveryMonth").value=expectedDeliveryMonth;
	}
	if(ld!="")
	{
		document.getElementById("ld").value=ld;
	}
	if(warrantyDispatchDate!="")
	{
		document.getElementById("warrantyDispatchDate").value=warrantyDispatchDate;	
	}
	if(warrantyCommissioningDate!="")
	{
		document.getElementById("warrantyCommissioningDate").value=warrantyCommissioningDate;
	}
	if(drawingApproval!="")
	{
		document.getElementById("drawingApproval").value=drawingApproval;
	}
	if(qapApproval!="")
    {
		document.getElementById("qapApproval").value=qapApproval;
	}
	if(typeTest!="")
	{
		document.getElementById("typeTest").value=typeTest;
	}
	if(routineTest!="")
	{
		document.getElementById("routineTest").value=routineTest;	
	}
	if(specialTest!="")
	{
		document.getElementById("specialTest").value=specialTest;
	}
	if(thirdPartyInspection!="")
	{
		document.getElementById("thirdPartyInspection").value=thirdPartyInspection;
	}
	if(stageInspection!="")
	{
		document.getElementById("stageInspection").value=stageInspection;
	}
	if(packing!="")
	{
		document.getElementById("packing").value=packing;
	}
	if(insurance!=""){
		document.getElementById("insurance").value=insurance;
	}
	if(replacement!="")
	{
		document.getElementById("replacement").value=replacement;
	}
	if(priceincludeFrieght!="" )
	{
		document.getElementById("priceincludeFrieght").value=priceincludeFrieght;
		var x = document.getElementById("priceincludeFrieghtVal");
		
		if(priceincludeFrieght=="0" || priceincludeFrieght==null)
		{
			document.getElementById("priceincludeFrieghtVal").value="";  
			  if (x.style.display === "none") {
			    x.style.display = "none";
			  } else {
			    x.style.display = "none";
			  }
	
		}
	}
	if(priceincludeExtraWarn!="" )
	{
		document.getElementById("priceincludeExtraWarn").value=priceincludeExtraWarn;
		var x = document.getElementById("priceincludeExtraWarnVal");
		
		if(priceincludeExtraWarn=="0" || priceincludeExtraWarn==null)
		{
			document.getElementById("priceincludeExtraWarnVal").value="";  
			  if (x.style.display === "none") {
			    x.style.display = "none";
			  } else {
			    x.style.display = "none";
			  }
	
		}

	}
	if(priceincludeSupervisionCost!="" )
	{
		document.getElementById("priceincludeSupervisionCost").value=priceincludeSupervisionCost;
		
		var x = document.getElementById("priceincludeSupervisionCostVal");
		
		if(priceincludeSupervisionCost=="0" || priceincludeSupervisionCost==null)
		{
			document.getElementById("priceincludeSupervisionCostVal").value="";  
			  if (x.style.display === "none") {
			    x.style.display = "none";
			  } else {
			    x.style.display = "none";
			  }
	
		}

	}
	if(priceincludetypeTestCharges!="" )
	{
		document.getElementById("priceincludetypeTestCharges").value=priceincludetypeTestCharges;
		var x = document.getElementById("priceincludetypeTestChargesVal");
		
		if(priceincludetypeTestCharges=="0" || priceincludetypeTestCharges==null)
		{
			document.getElementById("priceincludetypeTestChargesVal").value="";  
			  if (x.style.display === "none") {
			    x.style.display = "none";
			  } else {
			    x.style.display = "none";
			  }
	
		}

	}
	if(priceextraFrieght!="" )
	{
		document.getElementById("priceextraFrieght").value=priceextraFrieght;
		var x = document.getElementById("priceextraFrieghtVal");
		
		if(priceextraFrieght=="0" || priceextraFrieght==null)
		{
			document.getElementById("priceextraFrieghtVal").value="";  
			  if (x.style.display === "none") {
			    x.style.display = "none";
			  } else {
			    x.style.display = "none";
			  }
	
		}

	}
	if(priceextraExtraWarn!="" )
	{
		document.getElementById("priceextraExtraWarn").value=priceextraExtraWarn;
		
		var x = document.getElementById("priceextraExtraWarnVal");
		
		if(priceextraExtraWarn=="0" || priceextraExtraWarn==null)
		{
			document.getElementById("priceextraExtraWarnVal").value="";  
			  if (x.style.display === "none") {
			    x.style.display = "none";
			  } else {
			    x.style.display = "none";
			  }
	
		}

	}
	if(priceextraSupervisionCost!="" )
	{
		document.getElementById("priceextraSupervisionCost").value=priceextraSupervisionCost;
		var x = document.getElementById("priceextraSupervisionCostVal");
		
		if(priceextraSupervisionCost=="0" || priceextraSupervisionCost==null)
		{
			document.getElementById("priceextraSupervisionCostVal").value="";  
			  if (x.style.display === "none") {
			    x.style.display = "none";
			  } else {
			    x.style.display = "none";
			  }
	
		}


	}
	if(priceextratypeTestCharges!="" )
	{
		document.getElementById("priceextratypeTestCharges").value=priceextratypeTestCharges;
		
		var x = document.getElementById("priceextratypeTestChargesVal");
		
		if(priceextratypeTestCharges=="0" || priceextratypeTestCharges==null)
		{
			document.getElementById("priceextratypeTestChargesVal").value="";  
			  if (x.style.display === "none") {
			    x.style.display = "none";
			  } else {
			    x.style.display = "none";
			  }
	
		}

	}
	  if (delImg.style.display === "none") {
		  delImg.style.display = "none";
	  } else {
		  delImg.style.display = "none";
	  }

	
});
jQuery(function() {
	//keep the focus on the input box so that the highlighting
  //I'm using doesn't give away the hidden select box to the user
	$('select.data-list-input').focus(function() {
		$(this).siblings('input.data-list-input').focus();
	});
  //when selecting from the select box, put the value in the input box
	$('select.data-list-input').change(function() {
		$(this).siblings('input.data-list-input').val($(this).val());
	});
  //When editing the input box, reset the select box setting to "free
  //form input". This is important to do so that you can reselect the
  //option you had selected if you want to.
	$('input.data-list-input').change(function() {
		$(this).siblings('select.data-list-input').val('');
	});
  
  
});

</script>
  
  