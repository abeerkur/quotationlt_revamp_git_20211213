<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>
<%@ page import="in.com.rbc.quotation.common.utility.PropertyUtility" %>
<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants" %>
<%@ page import="in.com.rbc.quotation.common.utility.CommonUtility" %>
<%@ page import="in.com.rbc.quotation.common.utility.CurrencyConvertUtility" %>
<%@ page import="java.text.DecimalFormat,java.math.BigDecimal,java.net.URLDecoder" %>

<%
	DecimalFormat df = new DecimalFormat("###,###");
	DecimalFormat df1 = new DecimalFormat("#0");
%>

<html:form action="newPlaceAnEnquiry.do" method="POST">

<html:hidden property="invoke" value="createEnquiry"/>
<html:hidden property="operationType"/>
<html:hidden property="enquiryId" styleId="enquiryId"/>
<html:hidden property="enquiryNumber" styleId="enquiryNumber"/>
<html:hidden property="ratingsValidationMessage"/>
<html:hidden property="dispatch" value="viewSubmit"/>
<html:hidden property="customerId" styleId="customerId"/>
<html:hidden property="customerType" styleId="customerType"/>
<html:hidden property="locationId" styleId="locationId"/>
<html:hidden property="totalRatingIndex" styleId="totalRatingIndex"/>

<input type="hidden" name="lastRatingNum" id="lastRatingNum" value='<%=(Integer)request.getSession().getAttribute("lastRatingNum") %>' >
<input type="hidden" name="prevIdVal" id="prevIdVal" value='<%=(Integer)request.getSession().getAttribute("prevIdVal") %>' >
<input type="hidden" name='<%= org.apache.struts.taglib.html.Constants.TOKEN_KEY %>' value='<%= session.getAttribute(org.apache.struts.action.Action.TRANSACTION_TOKEN_KEY) %>' />
         
	<table width="80%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
		<tr>
			<td>
 				<div class="sectiontitle"><bean:message key="quotation.viewenquiry.label.heading"/> </div>	
			</td>
		</tr>

		<tr>
			<td height="100%" valign="top">
				<!-------------------------- Top Title and Status & Note block Start  --------------------------------->
				<table width="100%" border="0" cellspacing="1" cellpadding="0">
					<tr>
						<td rowspan="2"><table width="98%" border="0" cellspacing="2"
								cellpadding="0" class="DotsTable">
								<tr>
									<td rowspan="2" style="width: 70px"><img
										src="html/images/icon_quote.gif" width="52" height="50"></td>
									<td class="other"><bean:message
											key="quotation.viewenquiry.label.subheading1" /> <bean:write
											name="newenquiryForm" property="enquiryNumber" /><br> <bean:message
											key="quotation.viewenquiry.label.subheading2" /></td>
								</tr>
								<tr>
									<td class="other"><table width="100%">
											<tr>
												<td height="16" class="section-head" align="center"><bean:message
														key="quotation.viewenquiry.label.status" /> <bean:write
														name="newenquiryForm" property="statusName" /></td>
											</tr>
										</table></td>
								</tr>
							</table></td>
						<logic:lessThan name="newenquiryForm" property="statusId"
							value="6">
							<td align="right" class="other">
								<table>
									<tr>
										<td><bean:message key="quotation.attachments.message" />
										</td>
										<td><a
											href="javascript:manageAttachments('<%=CommonUtility.getIntValue(
							PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_ATTACHMENT_APPLICATIONID))%>','<bean:write name="newenquiryForm" property="enquiryId"/>', '<bean:write name="newenquiryForm" property="createdBySSO"/>');"><img
												src="html/images/ico_attach.gif" alt="Manage Attachments"
												border="0" align="right"> </a></td>
									</tr>
								</table>
							</td>
						</logic:lessThan>
					</tr>
					<tr>
						<td style="width: 45%"><logic:present name="attachmentList"
								scope="request">
								<logic:notEmpty name="attachmentList">
									<fieldset>
										<table width="100%" class="other">
											<logic:iterate name="attachmentList" id="attachmentData"
												indexId="idx"
												type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<tr>
													<td><a
														href="javascript:download('<bean:write name="attachmentData" property="key"/>', '<bean:write name="attachmentData" property="value"/>', '<bean:write name="attachmentData" property="value2"/>');">
															<bean:write name="attachmentData" property="value" />.<bean:write
																name="attachmentData" property="value2" />
													</a></td>
												</tr>
											</logic:iterate>
										</table>
									</fieldset>
								</logic:notEmpty>
							</logic:present></td>
					</tr>
					<logic:present name="attachmentMessage" scope="request">
						<tr>
							<td><bean:write name="attachmentMessage" /></td>
						</tr>
					</logic:present>
				</table> <br>
				<br> <br> 
				
				<div class="note-red" style="font-weight:bold !important;" align="right"> All fields marked &nbsp;&nbsp; <img src="html/images/bullets.gif" width="7" height="7" align="absmiddle"> &nbsp;&nbsp; indicates mandatory fields. </div>
			
				<!----------------------- Request Information : Block Start ------------------------------------>

				<fieldset>
					<legend class="blueheader"> <bean:message key="quotation.create.label.requestinfo" /> </legend>
					<table width="100%">
						<tr>
							<td style="vertical-align:top">
								<table width="100%">
									<tr>
										<td colspan="6"><label class="blue-light"style="font-weight: bold; width: 100%;"> Enquiry Information </label></td>
									</tr>
									<tr>
										<th class="label-text" style="width:170px; line-height:20px;"><bean:message key="quotation.create.label.customerenqreference"/></th>
										<td class="formContentview-rating-b0x" style="width:170px"><bean:write name="newenquiryForm" property="customerEnqReference" /></td>
										<td class="dividingdots" align="center" width="2%" height="100%" rowspan="5"></td>
						  				<td align="center" width="2%" height="100%" rowspan="5">&nbsp;</td>
						  				<th class="label-text" style="width:120px; line-height:20px;"> <bean:message key="quotation.label.is.marathon.approved"/></th>
						  				<td class="formContentview-rating-b0x" style="width:170px"><bean:write name="newenquiryForm" property="is_marathon_approved" /></td>
									</tr>
									<tr>
										<th class="label-text" style="width:170px; line-height:20px;"><bean:message key="quotation.create.label.customer" /> <img src="html/images/bullets.gif" width="7" height="7" align="absmiddle"> </th>
										<td class="formContentview-rating-b0x" style="width:170px"> 
											<bean:write name="newenquiryForm" property="customerName" /> 
											<html:hidden property="customerName" styleId="customerName"/>
										</td>
										
										<th class="label-text" style="width:120px; line-height:20px;">Consultant</th>
										<td class="formContentview-rating-b0x" style="width:170px"><bean:write name="newenquiryForm" property="consultantName" /></td>
									</tr>
									<tr>
										<th class="label-text" style="width:170px; line-height:20px;"><bean:message key="quotation.label.admin.customer.location" /> </th>
										<td class="formContentview-rating-b0x" style="width:170px"><bean:write name="newenquiryForm" property="location" /></td>
										
										<th class="label-text" style="width:120px; line-height:20px;">End user</th>
										<td class="formContentview-rating-b0x" style="width:170px"><bean:write name="newenquiryForm" property="endUser" /></td>
									</tr>
									<tr>
										<th class="label-text" style="width:170px; line-height:20px;"><bean:message key="quotation.label.concerned.person" /> <img src="html/images/bullets.gif" width="7" height="7" align="absmiddle"> </th>
										<td class="formContentview-rating-b0x" style="width:170px">
											<bean:write name="newenquiryForm" property="concernedPerson" />
											<html:hidden property="concernedPerson" styleId="concernedPerson"/>
										</td>
										
										<th class="label-text" style="width:120px; line-height:20px;">End User Industry</th>
										<td class="formContentview-rating-b0x" style="width:170px"><bean:write name="newenquiryForm" property="endUserIndustry" /></td>
									</tr>
									<tr>
										<th class="label-text" style="width:170px; line-height:20px;">Project Name</th>
										<td class="formContentview-rating-b0x" style="width:170px"><bean:write name="newenquiryForm" property="projectName" /></td>
										
										<th class="label-text" style="width:120px; line-height:20px;">Location of Installation</th>
										<td class="formContentview-rating-b0x" style="width:170px"><bean:write name="newenquiryForm" property="locationOfInstallation" /></td>
									</tr>
									<tr>
										<td colspan="6"> &nbsp; </td>
									</tr>
									<tr>
										<td colspan="6" style="width:300px !important; line-height:20px;">
											<div style="font: italic 10px Verdana, Arial, Helvetica, sans-serif; text-align:left !important; font-weight:bold !important;">
												<html:checkbox property="customerDealerLink" styleId="customerDealerLink" value="Y" disabled="true" />
		                    					<label for="customerDealerLink" > <bean:message key="quotation.label.create.customerDealerLink"/> </label>
											</div>
										</td>
									</tr>
								</table>
							</td>
							<td class="dividingdots" width="2%" height="100%" align="center" style="padding:2px;"></td> 
					  		<td width="2%" height="100%" align="center" style="padding:2px;">&nbsp;</td>
							
							<td style="vertical-align:top">
								<table width="100%">
									<tr>
										<td colspan="2"><label class="blue-light" style="font-weight: bold; padding-bottom:3px !important;"> Enquiry Weightage </label></td>
									</tr>
									<tr>
										<th class="label-text">Enquiry Type <img src="html/images/bullets.gif" width="7" height="7" align="absmiddle"> </th>
										<td class="formContentview-rating-b0x" style="width:175px;">
											<bean:write name="newenquiryForm" property="enquiryType" />
											<html:hidden property="enquiryType" styleId="enquiryType"/>
										</td>
									</tr>
									<tr>
										<th class="label-text">Winning Chance <img src="html/images/bullets.gif" width="7" height="7" align="absmiddle"> </th>
										<td class="formContentview-rating-b0x" style="width:175px;">
											<bean:write name="newenquiryForm" property="winningChance" />
											<html:hidden property="winningChance" styleId="winningChance"/>
										</td>
									</tr>
									<tr>
										<th class="label-text">Targeted <img src="html/images/bullets.gif" width="7" height="7" align="absmiddle"> </th>
										<td class="formContentview-rating-b0x" style="width:175px;">
											<bean:write name="newenquiryForm" property="targeted" />
											<html:hidden property="targeted" styleId="targeted"/>
										</td>
									</tr>
									
									<tr>
				                     	<td colspan="2"><label class="blue-light" style="font-weight: bold; padding-bottom:3px !important;"> <bean:message key="quotation.label.important.dates"/> </label></td>				  
						  			</tr>
						  			
						  			<tr>
										<th class="label-text" style="width:180px !important;" >Enquiry Receipt date <img src="html/images/bullets.gif" width="7" height="7" align="absmiddle"> </th>
										<td class="formContentview-rating-b0x" style="width:120px !important;">
											<bean:write name="newenquiryForm" property="receiptDate" />
											<html:hidden property="receiptDate" styleId="receiptDate"/>
										</td>
									</tr>
									<tr>
										<th class="label-text" style="width:180px !important;" >Required Offer Submission date <img src="html/images/bullets.gif" width="7" height="7" align="absmiddle"> </th>
										<td class="formContentview-rating-b0x" style="width:120px !important;">
											<bean:write name="newenquiryForm" property="submissionDate" />
											<html:hidden property="submissionDate" styleId="submissionDate"/>
										</td>
									</tr>
									<tr>
										<th class="label-text" style="width:180px !important;" >Order Closing date <img src="html/images/bullets.gif" width="7" height="7" align="absmiddle"></th>
										<td class="formContentview-rating-b0x" style="width:120px !important;">
											<bean:write name="newenquiryForm" property="closingDate" />
											<html:hidden property="closingDate" styleId="closingDate"/>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					
					<table width="100%">
						<tr>
							<th class="label-text" style="width: 25% !important;"><bean:message
									key="quotation.create.label.enquiryreferencenumber" />
								&nbsp;&nbsp;</th>
							<td colspan="3" class="formContentview-rating-b0x"
								style="width: 75% !important; word-break: break-all; height: 40px;">
								<logic:equal name="newenquiryForm"
									property="enquiryReferenceNumber" value="">
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;				    	
								</logic:equal> <logic:notEqual name="newenquiryForm" property="description"
									value="">
									<bean:write name="newenquiryForm"
										property="enquiryReferenceNumber" />
								</logic:notEqual>
							</td>
						</tr>
						<tr>
							<th class="label-text" style="width: 25% !important;"><bean:message
									key="quotation.create.label.description" />
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</th>
							<td colspan="3" class="formContentview-rating-b0x"
								style="width: 75% !important; word-break: break-all; height: 40px;">
								<logic:equal name="newenquiryForm" property="description"
									value="">
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;				    	
								</logic:equal> <logic:notEqual name="newenquiryForm" property="description"
									value="">
									<bean:write name="newenquiryForm" property="description" />
								</logic:notEqual>
							</td>
						</tr>
						<tr>
							<th class="label-text" style="width: 25% !important;"><bean:message
									key="quotation.create.label.createdby" />:&nbsp;</th>
							<td class="formContentview-rating-b0x"
								style="width: 25% !important;"><font
								class="formContentview-rating-b0x"><bean:write
										name="newenquiryForm" property="createdBy" /></font></td>
							<th class="label-text" style="width: 25% !important;"><bean:message
									key="quotation.create.label.createddate" />:&nbsp;</th>
							<td class="formContentview-rating-b0x"
								style="width: 25% !important;"><font
								class="formContentview-rating-b0x"><bean:write
										name="newenquiryForm" property="createdDate" /></font></td>
						</tr>
					</table>

					<!-- Commercial Terms Section -->
					<table width="100%">
						<tr>
							<td style="vertical-align: top">
								<table width="100%">
									<tr>
										<td><label class="blue-light"
											style="font-weight: bold; width: 100% !important;">
												Commercial Terms and Conditions </label></td>
									</tr>
								</table>
								<table width="100%">
									<tr>
										<th class="label-text" style="width:15% !important; color:#990000;">Payment Terms <img src="html/images/bullets.gif" width="7" height="7" align="absmiddle"> </th>
										<th class="label-text" style="width:15% !important; color:#990000;">%age amount of Nett. order value </th>
										<th class="label-text" style="width:15% !important; color:#990000;">Payment Days</th>
										<th class="label-text" style="width:35% !important; color:#990000;">Payable Terms</th>
										<th class="label-text" style="width:15% !important; color:#990000;">Instrument</th>
									</tr>
									<tr>
										<td class="formContentview-rating-b0x"> Advance Payment 1 </td>
										<td class="formContentview-rating-b0x">
											<bean:write name="newenquiryForm" property="pt1PercentAmtNetorderValue" />
											<html:hidden property="pt1PercentAmtNetorderValue" styleId="pt1PercentAmtNetorderValue" value='<bean:write name="newenquiryForm" property="pt1PercentAmtNetorderValue" />' />
										</td>
										<td class="formContentview-rating-b0x">
											<bean:write name="newenquiryForm" property="pt1PaymentDays" />
											<html:hidden property="pt1PaymentDays" styleId="pt1PaymentDays" value='<bean:write name="newenquiryForm" property="pt1PaymentDays" />' />
										</td>
										<td class="formContentview-rating-b0x">
											<bean:write name="newenquiryForm" property="pt1PayableTerms" />
											<html:hidden property="pt1PayableTerms" styleId="pt1PayableTerms" value='<bean:write name="newenquiryForm" property="pt1PayableTerms" />' />
										</td>
										<td class="formContentview-rating-b0x">
											<bean:write name="newenquiryForm" property="pt1Instrument" />
											<html:hidden property="pt1Instrument" styleId="pt1Instrument" value='<bean:write name="newenquiryForm" property="pt1Instrument" />' />
										</td>
									</tr>
									<tr>
										<td class="formContentview-rating-b0x"> Advance Payment 2 </td>
										<td class="formContentview-rating-b0x">
											<bean:write name="newenquiryForm" property="pt2PercentAmtNetorderValue" />
											<html:hidden property="pt2PercentAmtNetorderValue" styleId="pt2PercentAmtNetorderValue" value='<bean:write name="newenquiryForm" property="pt2PercentAmtNetorderValue" />' />
										</td>
										<td class="formContentview-rating-b0x">
											<bean:write name="newenquiryForm" property="pt2PaymentDays" />
											<html:hidden property="pt2PaymentDays" styleId="pt2PaymentDays" value='<bean:write name="newenquiryForm" property="pt2PaymentDays" />' />
										</td>
										<td class="formContentview-rating-b0x">
											<bean:write name="newenquiryForm" property="pt2PayableTerms" />
											<html:hidden property="pt2PayableTerms" styleId="pt2PayableTerms" value='<bean:write name="newenquiryForm" property="pt2PayableTerms" />' />
										</td>
										<td class="formContentview-rating-b0x">
											<bean:write name="newenquiryForm" property="pt2Instrument" />
											<html:hidden property="pt2Instrument" styleId="pt2Instrument" value='<bean:write name="newenquiryForm" property="pt2Instrument" />' />
										</td>
									</tr>
									<tr>
										<td class="formContentview-rating-b0x">Main Payment</td>
										<td class="formContentview-rating-b0x">
											<bean:write name="newenquiryForm" property="pt3PercentAmtNetorderValue" />
											<html:hidden property="pt3PercentAmtNetorderValue" styleId="pt3PercentAmtNetorderValue" value='<bean:write name="newenquiryForm" property="pt3PercentAmtNetorderValue" />' />
										</td>
										<td class="formContentview-rating-b0x">
											<bean:write name="newenquiryForm" property="pt3PaymentDays" />
											<html:hidden property="pt3PaymentDays" styleId="pt3PaymentDays" value='<bean:write name="newenquiryForm" property="pt3PaymentDays" />' />
										</td>
										<td class="formContentview-rating-b0x"> 
											<bean:write name="newenquiryForm" property="pt3PayableTerms" />
											<html:hidden property="pt3PayableTerms" styleId="pt3PayableTerms" value='<bean:write name="newenquiryForm" property="pt3PayableTerms" />' />
										</td>
										<td class="formContentview-rating-b0x">
											<bean:write name="newenquiryForm" property="pt3Instrument" />
											<html:hidden property="pt3Instrument" styleId="pt3Instrument" value='<bean:write name="newenquiryForm" property="pt3Instrument" />' />
										</td>
									</tr>
									<tr>
										<td class="formContentview-rating-b0x">Retention Payment 1</td>
										<td class="formContentview-rating-b0x">
											<bean:write name="newenquiryForm" property="pt4PercentAmtNetorderValue" />
											<html:hidden property="pt4PercentAmtNetorderValue" styleId="pt4PercentAmtNetorderValue" value='<bean:write name="newenquiryForm" property="pt4PercentAmtNetorderValue" />' />
										</td>
										<td class="formContentview-rating-b0x">
											<bean:write name="newenquiryForm" property="pt4PaymentDays" />
											<html:hidden property="pt4PaymentDays" styleId="pt4PaymentDays" value='<bean:write name="newenquiryForm" property="pt4PaymentDays" />' />
										</td>
										<td class="formContentview-rating-b0x">
											<bean:write name="newenquiryForm" property="pt4PayableTerms" />
											<html:hidden property="pt4PayableTerms" styleId="pt4PayableTerms" value='<bean:write name="newenquiryForm" property="pt4PayableTerms" />' />
										</td>
										<td class="formContentview-rating-b0x">
											<bean:write name="newenquiryForm" property="pt4Instrument" />
											<html:hidden property="pt4Instrument" styleId="pt4Instrument" value='<bean:write name="newenquiryForm" property="pt4Instrument" />' />
										</td>
									</tr>
									<tr>
										<td class="formContentview-rating-b0x">Retention Payment 2</td>
										<td class="formContentview-rating-b0x">
											<bean:write name="newenquiryForm" property="pt5PercentAmtNetorderValue" />
											<html:hidden property="pt5PercentAmtNetorderValue" styleId="pt5PercentAmtNetorderValue" value='<bean:write name="newenquiryForm" property="pt5PercentAmtNetorderValue" />' />
										</td>
										<td class="formContentview-rating-b0x">
											<bean:write name="newenquiryForm" property="pt5PaymentDays" />
											<html:hidden property="pt5PaymentDays" styleId="pt5PaymentDays" value='<bean:write name="newenquiryForm" property="pt5PaymentDays" />' />
										</td>
										<td class="formContentview-rating-b0x">
											<bean:write name="newenquiryForm" property="pt5PayableTerms" />
											<html:hidden property="pt5PayableTerms" styleId="pt5PayableTerms" value='<bean:write name="newenquiryForm" property="pt5PayableTerms" />' />
										</td>
										<td class="formContentview-rating-b0x">
											<bean:write name="newenquiryForm" property="pt5Instrument" />
											<html:hidden property="pt5Instrument" styleId="pt5Instrument" value='<bean:write name="newenquiryForm" property="pt5Instrument" />' />
										</td>
									</tr>
								</table> <br>
								<table width="100%">
									<tr>
										<th class="label-text" style="width:20%; color:#990000;">Delivery Term <img src="html/images/bullets.gif" width="7" height="7" align="absmiddle"> </th>
										<td class="formContentview-rating-b0x" style="width:10%; float:left;">
											<bean:write name="newenquiryForm" property="deliveryTerm" />
											<html:hidden property="deliveryTerm" styleId="deliveryTerm"/>
										</td>
										<th class="label-text" style="width:30%; color:#990000;">Delivery <img src="html/images/bullets.gif" width="7" height="7" align="absmiddle"> </th>
										<td class="formContentview-rating-b0x" style="width:10%; float:left;">
											<bean:write name="newenquiryForm" property="deliveryInFull" />
											<html:hidden property="deliveryInFull" styleId="deliveryInFull"/>
										</td>
										<th class="label-text" style="width:20%; color:#990000;">Customer Requested Delivery <img src="html/images/bullets.gif" width="7" height="7" align="absmiddle"> </th>
										<td class="formContentview-rating-b0x" style="width:10%; float:left;">
											<bean:write name="newenquiryForm" property="custRequestedDeliveryTime" /> Weeks
										</td>
									</tr>
									<tr>
										<th class="label-text" style="width:20%; color:#990000;">Warranty from the date of dispatch </th>
										<td class="formContentview-rating-b0x" style="width:10%; float:left;">
											<bean:write name="newenquiryForm" property="warrantyDispatchDate" /> Months
										</td>
										<th class="label-text" style="width:30%; color:#990000;">Warranty from the date of comissioning </th>
										<td class="formContentview-rating-b0x" style="width:10%; float:left;">
											<bean:write name="newenquiryForm" property="warrantyCommissioningDate" /> Months
										</td>
										<th class="label-text" style="width:20%; color:#990000;">Order Completion Lead Time <img src="html/images/bullets.gif" width="7" height="7" align="absmiddle"> </th>
										<td class="formContentview-rating-b0x" style="width:10%; float:left;">
											<bean:write name="newenquiryForm" property="orderCompletionLeadTime" />
											<html:hidden property="orderCompletionLeadTime" styleId="orderCompletionLeadTime"/>
											Weeks
										</td>
									</tr>
									<tr>
										<th class="label-text" style="width:20%; color:#990000;">GST <img src="html/images/bullets.gif" width="7" height="7" align="absmiddle"> </th>
										<td class="formContentview-rating-b0x" style="width:10%; float:left;">
											<bean:write name="newenquiryForm" property="gstValue" />
											<html:hidden property="gstValue" styleId="gstValue"/>
										</td>
										<th class="label-text" style="width:30%; color:#990000;">Packaging <img src="html/images/bullets.gif" width="7" height="7" align="absmiddle"> </th>
										<td class="formContentview-rating-b0x" style="width:10%; float:left;">
											<bean:write name="newenquiryForm" property="packaging" />
										</td>
										<th class="label-text" style="width:20%; color:#990000;"> Offer Validity </th>
										<td class="formContentview-rating-b0x" style="width:10%; float:left;">
											<bean:write name="newenquiryForm" property="offerValidity" /> Days
										</td>
									</tr>
									<tr>
										<th class="label-text" style="width:20%; color:#990000; padding:0px !important;"> Price Validity from PO date </th>
										<td class="formContentview-rating-b0x" style="width:10%; float:left;">
											<bean:write name="newenquiryForm" property="priceValidity" /> Months
										</td>
										<th class="label-text" style="width:30%; color:#990000; padding:0px !important;">Liquidated Damages due to delayed deliveries</th>
										<td class="formContentview-rating-b0x" style="width:40%; float:left;" colspan="3">
											<bean:write name="newenquiryForm" property="liquidatedDamagesDueToDeliveryDelay" /></td>
									</tr>
								</table> <br>
								<table width="100%">
									<tr>
										<th class="label-text" style="width: 25% !important; color:#990000;">Supervision
											for Erection and Comissioning</th>
										<td class="formContentview-rating-b0x"
											style="width: 25% !important;"><bean:write
												name="newenquiryForm" property="supervisionDetails" /></td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
									</tr>
								</table> <logic:equal name="newenquiryForm"
									property="supervisionDetails" value="Yes">
									<table width="50%">
										<tr>
											<th class="label-text" style="width: 25% !important;">No.
												of Man Days</th>
											<td class="formContentview-rating-b0x"
												style="width: 75% !important;"><bean:write
													name="newenquiryForm" property="superviseNoOfManDays" /></td>
										</tr>
										<tr>
											<th class="label-text" colspan="2">&nbsp;</th>
										</tr>
										<tr>
											<th class="label-text"
												style="width: 100% !important; white-space: nowrap;"
												colspan="2">Note: To & Fro travelling charges, Fooding,
												Lodging & Local conveyance at site are at customer's
												account.</th>
										</tr>
									</table>
								</logic:equal> <br>
							<br>
							</td>
						</tr>
					</table>
					<!-- Commercial Terms Section -->
					
				</fieldset> 
				<!----------------------- Request Information : Block End -------------------------------------->

				<!----------------------- Commercial Offer    : Block Start ------------------------------------>

				<fieldset>
					<legend class="blueheader">
						<bean:message key="quotation.offerdata.commercial.label.heading" />
					</legend>

					<table width="100%" border="0" align="center" cellpadding="0"
						cellspacing="3">
						<tr>
							<td colspan="16">&nbsp;</td>
						</tr>

						<tr>
							<logic:notEqual name="newEnquiryDto" property="dmFlag"
								value="<%=QuotationConstants.QUOTATION_STRING_TRUE%>">
								<td width="11%" class="formLabelTophome"><bean:message
										key="quotation.create.label.itemno" /></td>
								<td width="11%" class="formLabelTophome"><bean:message
										key="quotation.create.label.ratedoutput" /></td>
								<td width="11%" class="formLabelTophome"><bean:message
										key="quotation.create.label.units" /></td>
								<td width="11%" class="formLabelTophome"><bean:message
										key="quotation.create.label.pole" /></td>
								<td width="11%" class="formLabelTophome"><bean:message
										key="quotation.create.label.ratedvoltage" /></td>
								<td width="11%" class="formLabelTophome"><bean:message
										key="quotation.search.frame" /></td>
								<td width="11%" class="formLabelTophome"><bean:message
										key="quotation.offerdata.addon.label.unitprice" /></td>
								<td width="11%" class="formLabelTophome"><bean:message
										key="quotation.offerdata.addon.label.quantity" /></td>
								<td width="11%" class="formLabelTophome"><bean:message
										key="quotation.create.label.totalprice" /></td>
							</logic:notEqual>

							<logic:equal name="newEnquiryDto" property="dmFlag"
								value="<%=QuotationConstants.QUOTATION_STRING_TRUE%>">
								<td width="11%" class="formLabelTophome"><bean:message
										key="quotation.create.label.itemno" /></td>
								<td width="11%" class="formLabelTophome"><bean:message
										key="quotation.create.label.ratedoutput" /></td>
								<td width="11%" class="formLabelTophome"><bean:message
										key="quotation.create.label.units" /></td>
								<td width="11%" class="formLabelTophome"><bean:message
										key="quotation.create.label.pole" /></td>
								<td width="11%" class="formLabelTophome"><bean:message
										key="quotation.create.label.ratedvoltage" /></td>
								<td width="6%" class="formLabelTophome"><bean:message
										key="quotation.search.frame" /></td>
								<!-- <td width="6%" class="formLabelTophome"><bean:message key="quotation.create.label.unittransfercost" /></td> -->
								<!-- <td width="6%" class="formLabelTophome"><bean:message key="quotation.create.label.mcnspratio" /></td>  -->
								<td width="6%" class="formLabelTophome"><bean:message
										key="quotation.offerdata.addon.label.unitprice" /></td>
								<td width="6%" class="formLabelTophome"><bean:message
										key="quotation.offerdata.addon.label.quantity" /></td>
								<!-- <td width="6%" class="formLabelTophome"><bean:message key="quotation.create.label.totaltransportaion" /></td>
												<td width="6%" class="formLabelTophome"><bean:message key="quotation.create.label.totalwarranty" /></td>
												<td width="6%" class="formLabelTophome"><bean:message key="quotation.create.label.transfercost" /></td>  -->
								<td width="6%" class="formLabelTophome"><bean:message
										key="quotation.create.label.totalprice" /></td>
							</logic:equal>
						</tr>

						<% 
											String sTotalPrice="", sSupervisionCharges="";
											Double dQuantity=new Double(0);
						                    Double dTotalTransportation=new Double(0);
						                    Double dTotalWarranty=new Double(0);
						                    Double dTotalTc=new Double(0);
						                    Double dMcnspRatio=new Double(0);
						                    BigDecimal bdTotalPrice=new BigDecimal("0");
										%>
						<logic:iterate name="newEnquiryDto" property="newRatingsList"
							id="ratingObject" indexId="idx"
							type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<bean:define id="qnt" name="ratingObject" property="quantity" />
							<bean:define id="priceVal" name="ratingObject"
								property="ltPricePerUnit" />
							<tr>
								<logic:notEqual name="newEnquiryDto" property="dmFlag"
									value="<%=QuotationConstants.QUOTATION_STRING_TRUE%>">
									<td width="11%" class="formContentview-rating-b0x"><%=idx + 1%></td>
									<td width="11%" class="formContentview-rating-b0x"><bean:write
											name="ratingObject" property="ltKWId" /></td>
									<td width="11%" class="formContentview-rating-b0x">kW</td>
									<td width="11%" class="formContentview-rating-b0x"><bean:write
											name="ratingObject" property="ltPoleId" /></td>
									<td width="11%" class="formContentview-rating-b0x"><bean:write
											name="ratingObject" property="ltVoltId" /></td>
									<td width="11%" class="formContentview-rating-b0x"><bean:write
											name="ratingObject" property="ltFrameId" /><bean:write
											name="ratingObject" property="ltFrameSuffixId" /></td>
									<td width="11%" class="formContentview-rating-b0x">
										&#8377; <bean:write name="ratingObject"
											property="ltPricePerUnit" />
									</td>
									<td width="11%" class="formContentview-rating-b0x"><bean:write
											name="ratingObject" property="quantity" /></td>
									<td width="11%" class="formContentview-rating-b0x">
										&#8377; <bean:write name="ratingObject"
											property="ltTotalOrderPrice" />
									</td>
								</logic:notEqual>

								<logic:equal name="newEnquiryDto" property="dmFlag"
									value="<%=QuotationConstants.QUOTATION_STRING_TRUE%>">
									<td width="6%" class="formContentview-rating-b0x"><%=idx + 1%></td>
									<td width="6%" class="formContentview-rating-b0x"><bean:write
											name="ratingObject" property="ltKWId" /></td>
									<td width="11%" class="formContentview-rating-b0x">kW</td>
									<td width="11%" class="formContentview-rating-b0x"><bean:write
											name="ratingObject" property="ltPoleId" /></td>
									<td width="11%" class="formContentview-rating-b0x"><bean:write
											name="ratingObject" property="ltVoltId" /></td>
									<td width="11%" class="formContentview-rating-b0x"><bean:write
											name="ratingObject" property="ltFrameId" /><bean:write
											name="ratingObject" property="ltFrameSuffixId" /></td>
									<!-- <td width="6%" class="formLabelTophome"><bean:message key="quotation.create.label.unittransfercost" /></td> -->
									<!-- <td width="6%" class="formLabelTophome"><bean:message key="quotation.create.label.mcnspratio" /></td>  -->
									<td width="11%" class="formContentview-rating-b0x">
										&#8377; <bean:write name="ratingObject"
											property="ltPricePerUnit" />
									</td>
									<td width="11%" class="formContentview-rating-b0x"><bean:write
											name="ratingObject" property="quantity" /></td>
									<!-- <td width="6%" class="formLabelTophome"><bean:message key="quotation.create.label.totaltransportaion" /></td>
													<td width="6%" class="formLabelTophome"><bean:message key="quotation.create.label.totalwarranty" /></td>
													<td width="6%" class="formLabelTophome"><bean:message key="quotation.create.label.transfercost" /></td>  -->
									<td width="11%" class="formContentview-rating-b0x">
										&#8377; <bean:write name="ratingObject"
											property="ltTotalOrderPrice" />
									</td>
								</logic:equal>
							</tr>
							<% 
												String sUnitpriceMultiplier_rsm="",sTotalPriceMultiplier_rsm="", sTransferCMultiplier="", sMcornspratioMultiplier_rsm="",sTransportationperunit_rsm="", sWarrantyperunit_rsm="";
												Double dTotalTransportation_rsm=new Double(0);
							                    Double dTotalWarranty_rsm=new Double(0);
							                    Double dTotalTransferCost=new Double(0);
							                    Double dQty=new Double(0);
							                    							                    
							                    sTotalPriceMultiplier_rsm=String.valueOf(priceVal);
							                    
							                    if(qnt!="" && qnt != "0") {
							       					dQty = Double.parseDouble(String.valueOf(qnt));
							       					dQuantity = dQuantity + dQty;
												}
							                    
							                    if(sTotalPriceMultiplier_rsm!="" && sTotalPriceMultiplier_rsm!=QuotationConstants.QUOTATION_ZERO) {
							                    	if(sTotalPriceMultiplier_rsm.indexOf(",") != -1){
							                        	String unitprice=sTotalPriceMultiplier_rsm.replaceAll(",", "");
							                    		BigDecimal a=new BigDecimal(unitprice);
							                         	bdTotalPrice=bdTotalPrice.add(a);      
													} else {
							                          	BigDecimal a=new BigDecimal(sTotalPriceMultiplier_rsm);
							                      		bdTotalPrice=bdTotalPrice.add(a);      
													}
							                    }
											%>
						</logic:iterate>
						<tr>
							<td colspan="9" class="formContentview-rating-b0x">&nbsp;</td>
						</tr>
						<logic:equal name="newEnquiryDto" property="supervisionDetails"
							value="Yes">
							<bean:define id="superviseManDays" name="newEnquiryDto"
								property="superviseNoOfManDays" />
							<%
												int iSuperviseManDays = Integer.parseInt(String.valueOf(superviseManDays));
												long supervisionCharges = iSuperviseManDays * QuotationConstants.QUOTATIONLT_MANDAY_SUPERVISE_CHARGES;
												BigDecimal a=new BigDecimal(supervisionCharges);
												bdTotalPrice = bdTotalPrice.add(a);
												sSupervisionCharges = CurrencyConvertUtility.buildPriceFormat(String.valueOf(supervisionCharges));
											%>
							<tr>
								<td colspan="3" class="formContentview-rating-b0x">&nbsp;</td>
								<td colspan="3" align="left" class="formContentview-rating-b0x"><b>
										Supervision and Comissioning charges </b></td>
								<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
								<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
								<td class="formContentview-rating-b0x"><b> &#8377; <%=sSupervisionCharges%>
								</b></td>
							</tr>
						</logic:equal>
						<%
											sTotalPrice = CurrencyConvertUtility.buildPriceFormat(bdTotalPrice.toString());
										%>
						<logic:equal name="newEnquiryDto" property="dmFlag"
							value="<%=QuotationConstants.QUOTATION_STRING_TRUE%>">
							<tr>
								<td colspan="5" class="formContentview-rating-b0x">&nbsp;</td>
								<td align="left" class="formContentview-rating-b0x"><b><bean:message
											key="quotation.create.label.total" /></b></td>
								<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
								<td class="formContentview-rating-b0x"><b> <%=df1.format(dQuantity)%>
								</b></td>
								<td class="formContentview-rating-b0x"><b> &#8377; <%=sTotalPrice%>
								</b></td>
							</tr>
							<tr>
								<td colspan="1" class="formContentview-rating-b0x"><b><bean:message
											key="quotation.create.label.inwords" /></b></td>
								<td colspan="8" class="formContentview-rating-b0x" align="right">
									<b> <%=CurrencyConvertUtility.convertToIndianCurrency(String.valueOf(bdTotalPrice)) %>
								</b>
								</td>
							</tr>
						</logic:equal>
						<logic:notEqual name="newEnquiryDto" property="dmFlag"
							value="<%=QuotationConstants.QUOTATION_STRING_TRUE%>">
							<tr>
								<td colspan="5" class="formContentview-rating-b0x">&nbsp;</td>
								<td align="left" class="formContentview-rating-b0x"><b><bean:message
											key="quotation.create.label.total" /></b></td>
								<td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
								<td class="formContentview-rating-b0x"><b> <%=df1.format(dQuantity)%>
								</b></td>
								<td class="formContentview-rating-b0x"><b> &#8377; <%=sTotalPrice%>
								</b></td>
							</tr>
							<tr>
								<td colspan="1" class="formContentview-rating-b0x"><b><bean:message
											key="quotation.create.label.inwords" /></b></td>
								<td colspan="8" class="formContentview-rating-b0x" align="right">
									<b> <%=CurrencyConvertUtility.convertToIndianCurrency(String.valueOf(bdTotalPrice))%>
								</b>
								</td>
							</tr>
						</logic:notEqual>

					</table>

				</fieldset> 
				<!----------------------- Commercial Offer    : Block End -------------------------------------->

				<!----------------------- Comments and Deviations Start ------------------------------------->
				<fieldset>
					<legend class="blueheader"> <bean:message key="quotation.create.label.commentsanddeviations"/> </legend>
					<div>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td colspan="5">&nbsp;</td>
							</tr>
							<tr>
								<td width="100%">
									<textarea name="deviationComments" id="deviationComments" style="width :950px; height: 50px; BACKGROUND-COLOR: #e2e2e2" tabindex="35" readonly> <bean:write name="newEnquiryDto" property="commentsDeviations" filter="false" /> </textarea>
								</td>
							</tr>
						</table>
					</div>
				</fieldset>
				<!----------------------- Comments and Deviations End ------------------------------------->
				
				<!----------------------- Notes By SM, RSM, NSM, MH Start ------------------------------------->
				<fieldset>
					<legend class="blueheader"> <bean:message key="quotation.create.label.notesection" /> </legend>
					<div>
						<table width="100%" border="0" cellspacing="0" cellpadding="0">
							<tr>
								<td colspan="5">&nbsp;</td>
							</tr>
							<tr style="float: left; width: 100%;">
								<td class="formLabelrating" > Note By DM </td>
								<td colspan="4" width="75%">
									<textarea name="designNote" id="designNote" style="width :800px; height: 50px; BACKGROUND-COLOR: #e2e2e2" tabindex="35" readonly> <bean:write name="newEnquiryDto" property="designNote" filter="false" /> </textarea>
								</td>
							</tr>
							<tr style="float: left; width: 100%;">
								<td class="formLabelrating" > Note By QA </td>
								<td colspan="4" width="75%">
									<textarea name="qaNote" id="qaNote" style="width :800px; height: 50px; BACKGROUND-COLOR: #e2e2e2" tabindex="35" readonly> <bean:write name="newEnquiryDto" property="qaNote" filter="false" /> </textarea>
								</td>
							</tr>
							<tr style="float: left; width: 100%;">
								<td class="formLabelrating" > Note By Sourcing </td>
								<td colspan="4" width="75%">
									<textarea name="scmNote" id="scmNote" style="width :800px; height: 50px; BACKGROUND-COLOR: #e2e2e2" tabindex="35" readonly> <bean:write name="newEnquiryDto" property="scmNote" filter="false" /> </textarea>
								</td>
							</tr>
							<tr style="float: left; width: 100%;">
								<td class="formLabelrating" > Note By Mfg. Plant </td>
								<td colspan="4" width="75%">
									<textarea name="mfgNote" id="mfgNote" style="width :800px; height: 50px; BACKGROUND-COLOR: #e2e2e2" tabindex="35" readonly> <bean:write name="newEnquiryDto" property="mfgNote" filter="false" /> </textarea>
								</td>
							</tr>
							<tr style="float: left; width: 100%;">
								<td class="formLabelrating" ><bean:message key="quotation.create.label.smnote" /></td>
								<td colspan="4" width="75%">
									<textarea name="smNote" id="smNote" style="width :800px; height: 50px; BACKGROUND-COLOR: #e2e2e2" tabindex="35" readonly> <bean:write name="newEnquiryDto" property="smNote" filter="false" /> </textarea>
								</td>
							</tr>
							<tr style="float: left; width: 100%;">
								<td class="formLabelrating" ><bean:message key="quotation.create.label.rsmnote" /></td>
								<td colspan="4" width="75%">
									<textarea name="rsmNote" id="rsmNote" style="width :800px; height: 50px; BACKGROUND-COLOR: #e2e2e2" tabindex="35" readonly> <bean:write name="newEnquiryDto" property="rsmNote" filter="false" /> </textarea>
								</td>
							</tr>
							<tr style="float: left; width: 100%;">
								<td class="formLabelrating" ><bean:message key="quotation.create.label.nsmnote" /></td>
								<td colspan="4" width="75%">
									<textarea name="nsmNote" id="nsmNote" style="width :800px; height: 50px; BACKGROUND-COLOR: #e2e2e2" tabindex="35" readonly> <bean:write name="newEnquiryDto" property="nsmNote" filter="false" /> </textarea>
								</td>
							</tr>
							<tr style="float: left; width: 100%;">
								<td class="formLabelrating" ><bean:message key="quotation.create.label.mhnote" /></td>
								<td colspan="4" width="75%">
									<textarea name="mhNote" id="mhNote" style="width :800px; height: 50px; BACKGROUND-COLOR: #e2e2e2" tabindex="35" readonly> <bean:write name="newEnquiryDto" property="mhNote" filter="false" /> </textarea>
								</td>
							</tr>
							<tr style="float: left; width: 100%;">
								<td class="formLabelrating" >Note By BH</td>
								<td colspan="4" width="75%">
									<textarea name="bhNote" id="bhNote" style="width :800px; height: 50px; BACKGROUND-COLOR: #e2e2e2" tabindex="35" readonly> <bean:write name="newEnquiryDto" property="bhNote" filter="false" /> </textarea>
								</td>
							</tr>
						</table>
					</div>
				</fieldset>
					
				<!-----------------------Notes By SM, RSM, NSM, MH End --------------------------------------->

				<!----------------------- Ratings Details : Block Start ---------------------------------------->
				
				<logic:notEmpty name="newenquiryForm" property="newRatingsList">
					
					<%  int iRatingIndex = 1;  %>
					
					<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<input type="hidden" id="ratingidx<%=iRatingIndex%>" value='<%=iRatingIndex%>' >
						<input type="hidden" id="ratingObjId<%=iRatingIndex%>"  name="ratingObjId<%=iRatingIndex%>"  value='<bean:write name="ratingObj" property="ratingId" />' >
						<input type="hidden" id="ratingNo<%=iRatingIndex%>"  name="ratingNo<%=iRatingIndex%>"  value='<bean:write name="ratingObj" property="ratingNo" />' >
										
						<fieldset>
							<legend class="blueheader" ID="RATING1"> Rating <%=iRatingIndex%> </legend>
			   				<br>
			   				
			   				<table width="100%">
			   					<tr>
			              			<td colspan="5" class="blue-light-btn" style="height:25px " align="right" >
			              				<strong> <bean:message key="quotation.create.label.quantityrequired"/> </strong>
			              				<bean:write name="ratingObj" property="quantity"/>&nbsp;
			              				<input type="hidden" id="quantity<%=iRatingIndex%>" name="quantity<%=iRatingIndex%>" value='<bean:write name="ratingObj" property="quantity"/>' />
			              			</td>
           			 			</tr>
								<tr>
									<td colspan="5">&nbsp;</td>
								</tr>
								<tr>
																<td class="label-text" style="width:25%"> <strong> Tag Number# </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="tagNumber"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Ambient Temp. Deg C </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltAmbTempId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Product Line </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> 
																	<bean:write name="ratingObj" property="ltProdLineId"/> 
																	<input type="hidden" id="productLine<%=iRatingIndex%>" name="productLine<%=iRatingIndex%>" value='<bean:write name="ratingObj" property="ltProdLineId"/>' />
																</td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Haz Location </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltHazardAreaId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Power Rating(KW)  </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> 
																	<bean:write name="ratingObj" property="ltKWId"/> 
																	<input type="hidden" id="kw<%=iRatingIndex%>" name="kw<%=iRatingIndex%>" value='<bean:write name="ratingObj" property="ltKWId"/>' />
																</td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Gas Group </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltGasGroupId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> No. of Poles </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%">
																	<bean:write name="ratingObj" property="ltPoleId"/> 
																	<input type="hidden" id="pole<%=iRatingIndex%>" name="pole<%=iRatingIndex%>" value='<bean:write name="ratingObj" property="ltPoleId"/>' />
																</td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Application </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltApplicationId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Frame </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> 
																	<bean:write name="ratingObj" property="ltFrameId"/><bean:write name="ratingObj" property="ltFrameSuffixId"/>
																	<input type="hidden" id="frame<%=iRatingIndex%>" name="frame<%=iRatingIndex%>" value='<bean:write name="ratingObj" property="ltFrameId"/>' />
																	<input type="hidden" id="framesuffix<%=iRatingIndex%>" name="framesuffix<%=iRatingIndex%>" value='<bean:write name="ratingObj" property="ltFrameSuffixId"/>' />
																</td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Voltage +/- variation </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltVoltId"/> 
																	<logic:notEmpty  name="ratingObj" property="ltVoltId">
																		<logic:notEmpty  name="ratingObj" property="ltVoltAddVariation"> <b>+</b> <bean:write name="ratingObj" property="ltVoltAddVariation"/> % </logic:notEmpty>
																		<logic:notEmpty  name="ratingObj" property="ltVoltRemoveVariation"> <b>-</b> <bean:write name="ratingObj" property="ltVoltRemoveVariation"/> % </logic:notEmpty>
																	</logic:notEmpty>
																</td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Mounting </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> 
																	<bean:write name="ratingObj" property="ltMountingId"/> 
																	<input type="hidden" id="mounting<%=iRatingIndex%>" name="mounting<%=iRatingIndex%>" value='<bean:write name="ratingObj" property="ltMountingId"/>' />
																</td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Frequecy +/- variation </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltFreqId"/> 
																	<logic:notEmpty  name="ratingObj" property="ltFreqId">
																		<logic:notEmpty  name="ratingObj" property="ltFreqAddVariation"> <b>+</b> <bean:write name="ratingObj" property="ltFreqAddVariation"/> % </logic:notEmpty>
																		<logic:notEmpty  name="ratingObj" property="ltFreqRemoveVariation"> <b>-</b> <bean:write name="ratingObj" property="ltFreqRemoveVariation"/> % </logic:notEmpty>
																	</logic:notEmpty>
																</td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> TB Position </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> 
																	<bean:write name="ratingObj" property="ltTBPositionId"/> 
																	<input type="hidden" id="tbPosition<%=iRatingIndex%>" name="tbPosition<%=iRatingIndex%>" value='<bean:write name="ratingObj" property="ltTBPositionId"/>' />
																</td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Combined Variation </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltCombVariation"/> % </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Manufacturing Location </strong>  </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltManufacturingLocation"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> Duty </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltDutyId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Efficiency Class </strong>  </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltEffClass"/> </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> CDF </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltCDFId"/> </td>
															</tr>
															<tr>
																<td class="label-text" style="width:25%"> <strong> Enclosure </strong>  </td>
																<td class="formContentview-rating-b0x" style="width:24%"> &nbsp; </td>
																<td class="dividingdots" style="width:2%">&nbsp;</td>
																<td class="label-text" style="width:25%"> <strong> No. of Starts </strong> </td>
																<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltStartsId"/> </td>
															</tr>
								<tr>
									<td colspan="5">&nbsp;</td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Total Addon % </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <logic:notEmpty name="ratingObj" property="ltTotalAddonPercent"> <bean:write name="ratingObj" property="ltTotalAddonPercent"/> % </logic:notEmpty> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Total Cast Extra </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <logic:notEmpty name="ratingObj" property="ltTotalCashExtra"> &#8377; <bean:write name="ratingObj" property="ltTotalCashExtra"/> </logic:notEmpty> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> List Price Per Unit </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <logic:notEmpty name="ratingObj" property="ltLPPerUnit"> &#8377; <bean:write name="ratingObj" property="ltLPPerUnit"/> </logic:notEmpty> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> List Price with Addon Per Unit </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <logic:notEmpty name="ratingObj" property="ltLPWithAddonPerUnit"> &#8377; <bean:write name="ratingObj" property="ltLPWithAddonPerUnit"/> </logic:notEmpty> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Price Per Unit </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <logic:notEmpty name="ratingObj" property="ltPricePerUnit"> &#8377; <bean:write name="ratingObj" property="ltPricePerUnit"/> </logic:notEmpty> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Total Order Price </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <logic:notEmpty name="ratingObj" property="ltTotalOrderPrice"> &#8377; <bean:write name="ratingObj" property="ltTotalOrderPrice"/> </logic:notEmpty> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Standard Delivery (Weeks) </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltStandardDeliveryWks"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Earlier Supplied Motor Serial No. </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltEarlierMotorSerialNo"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Customer Requested Delivery (Weeks) </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltCustReqDeliveryWks"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Replacement Motor </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltReplacementMotor"/> </td>
								</tr>
								<tr>
									<td colspan="5">&nbsp;</td>
								</tr>
								<tr>
			              			<td colspan="5" style="height:25px; font-weight:bold;">
			              				<label class="blue-light" style="font-weight:bold; width:100% !important;"> <strong> Electrical </strong> </label>
			              			</td>
           			 			</tr>
           			 			<tr>
									<td class="label-text" style="width:25%"> <strong> RV </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltRVId"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> RA </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltRAId"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> &nbsp; </td>
									<td class="label-text" style="width:24%"> &nbsp; </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Winding Treatment </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltWindingTreatmentId"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Winding Wire </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltWindingWire"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Lead </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltLeadId"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Starting Current on DOL Starting </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltStartingCurrentOnDOLStart"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Winding Configuration </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltWindingConfig"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Load GD2 Value referred to motor shaft </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltLoadGD2Value"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Method of Starting </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltMethodOfStarting"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> VFD Application Type </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltVFDApplTypeId"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Speed Range only for VFD motor (Min.) </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltVFDMotorSpeedRangeMin"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Speed Range only for VFD motor (Max.) </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltVFDMotorSpeedRangeMax"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Service Factor </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltServiceFactor"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Overloading Duty </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltOverloadingDuty"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Constant Efficiency Range </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltConstantEfficiencyRange"/> </td>
								</tr>
								<tr>
									<td colspan="5">&nbsp;</td>
								</tr>
								<tr>
			              			<td colspan="5" style="height:25px; font-weight:bold;">
			              				<label class="blue-light" style="font-weight:bold; width:100% !important;"> <strong> Mechanical </strong> </label>
			              			</td>
           			 			</tr>
           			 			<tr>
									<td class="label-text" style="width:25%"> <strong> Shaft Type </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltShaftTypeId"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Shaft Material </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltShaftMaterialId"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Direction of Rotation </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltDirectionOfRotation"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Method of Coupling </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltMethodOfCoupling"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Painting Type </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltPaintingTypeId"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Paint Shade </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltPaintShadeId"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Paint Thickness </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltPaintThicknessId"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> IP </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltIPId"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Insulation Class </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltInsulationClassId"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Terminal Box Size </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltTerminalBoxSizeId"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Spreader Box </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltSpreaderBoxId"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Aux Terminal Box </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltAuxTerminalBoxId"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Space Heater </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltSpaceHeaterId"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Vibration </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltVibrationId"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Flying Lead without TB </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltFlyingLeadWithoutTDId"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Flying Lead with TB </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltFlyingLeadWithTDId"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Fan </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltMetalFanId"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Encoder Mounting </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltTechoMounting"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Shaft Grounding </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltShaftGroundingId"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Method Of Cooling </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltForcedCoolingId"/> </td>
								</tr>
								<tr>
									<td colspan="5">&nbsp;</td>
								</tr>
								<tr>
			              			<td colspan="5" style="height:25px; font-weight:bold;">
			              				<label class="blue-light" style="font-weight:bold; width:100% !important;"> <strong> Accessories </strong> </label>
			              			</td>
           			 			</tr>
           			 			<tr>
									<td class="label-text" style="width:25%"> <strong> Hardware </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltHardware"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Gland Plate </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltGlandPlateId"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Double Compression Gland </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltDoubleCompressionGlandId"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> SPM Mounting Provision </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltSPMMountingProvisionId"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Additional Name Plate </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltAddNamePlateId"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Direction Arrow Plate </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltDirectionArrowPlateId"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> RTD </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltRTDId"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> BTD </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltBTDId"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Thermister </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltThermisterId"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Cable Sealing Box </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltCableSealingBoxId"/> </td>
								</tr>
           			 			<tr>
									<td colspan="5">&nbsp;</td>
								</tr>
								<tr>
			              			<td colspan="5" style="height:25px; font-weight:bold;">
			              				<label class="blue-light" style="font-weight:bold; width:100% !important;"> <strong> Bearings </strong> </label>
			              			</td>
           			 			</tr>
           			 			<tr>
									<td class="label-text" style="width:25%"> <strong> ReLubrication System </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltBearingSystemId"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Bearing NDE </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltBearingNDEId"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Bearing DE </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltBearingDEId"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td style="width:25%"> &nbsp; </td>
									<td style="width:24%"> &nbsp; </td>
								</tr>
           			 			<tr>
									<td colspan="5">&nbsp;</td>
								</tr>
								<tr>
			              			<td colspan="5" style="height:25px; font-weight:bold;">
			              				<label class="blue-light" style="font-weight:bold; width:100% !important;"> <strong> Certificate / Spares / Testing </strong> </label>
			              			</td>
           			 			</tr>
           			 			<tr>
									<td class="label-text" style="width:25%"> <strong> Witness Routine </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltWitnessRoutineId"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Additional Test 1 </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltAdditionalTest1Id"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Additional Test 2 </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltAdditionalTest2Id"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Additional Test 3 </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltAdditionalTest3Id"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Type Test </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltTypeTestId"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Certification </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltULCEId"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Manufacturing Location </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltManufacturingLocation"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Spares 1 </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltSpares1"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Spares 2 </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltSpares2"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Spares 3</strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltSpares3"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Spares 4 </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltSpares4"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Spares 5 </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltSpares5"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Data Sheet </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltDataSheet"/> </td>
									<td class="dividingdots" style="width:2%">&nbsp;</td>
									<td class="label-text" style="width:25%"> <strong> Add Drawing </strong> </td>
									<td class="formContentview-rating-b0x" style="width:24%"> <bean:write name="ratingObj" property="ltAddDrawingId"/> </td>
								</tr>
								<tr>
									<td class="label-text" style="width:25%"> <strong> Additional Comments </strong> </td>
									<td colspan="4" class="formContentview-rating-b0x" style="width:75%;word-break:break-all;height:80px;"> <bean:write name="ratingObj" property="ltAdditionalComments"/> </td>
								</tr>
							</table>
			   				
						</fieldset>
						
						<%	iRatingIndex = iRatingIndex+1; %>
					</logic:iterate>
					
				</logic:notEmpty>
				
				<!----------------------- Ratings Details : Block End ------------------------------------------>
				
				<!----------------------- Bottom Input buttons : Block Start  ---------------------------------->
				
				<logic:equal name="newenquiryForm" property="statusId" value="1">
					<div class="blue-light" align="right" style="font-weight:bold; width:100% !important;">
						<logic:notEmpty name="newenquiryForm" property="newRatingsList">
							<logic:notPresent name="copy" scope="request">
								<input name="Button" type="button" class="BUTTON" id="lost" onclick="javascript:viewNewEnquiryIndentPage('Lost');" value="Lost">
								<input name="Button" type="button" class="BUTTON" id="abandoned" onclick="javascript:viewNewEnquiryIndentPage('Abondent');" value="Abondent">
				        	 	<input name="Button" type="button" class="BUTTON" id="edit" onclick="javascript:deleteNewEnquiry();"  value="Delete">
				        	 	<input name="Button" type="button" class="BUTTON" id="edit" onclick="javascript:editNewEnquiry();"  value="Edit">
				        	 	<html:button property="" styleClass="BUTTON" onclick="javascript:submitNewViewEnquiry();" value="Submit" />
							</logic:notPresent>
						</logic:notEmpty>
						<logic:empty name="newenquiryForm" property="newRatingsList">
							<input name="Button" type="button" class="BUTTON" id="edit" onclick="javascript:editRating(0);"  value="Add New Rating">        
						</logic:empty>
					</div>
        		</logic:equal>
				<logic:equal name="newenquiryForm" property="statusId" value="19">
					<logic:notEmpty name="newenquiryForm" property="newRatingsList">
					<div class="blue-light" align="right" style="font-weight:bold; width:100% !important;">
						<input name="Button" type="button" class="BUTTON" id="lost" onclick="javascript:viewNewEnquiryIndentPage('Lost');" value="Lost">
						<input name="Button" type="button" class="BUTTON" id="abandoned" onclick="javascript:viewNewEnquiryIndentPage('Abondent');" value="Abondent">
				        <input name="Button" type="button" class="BUTTON" id="edit" onclick="javascript:deleteNewEnquiry();"  value="Delete">
				        <input name="Button" type="button" class="BUTTON" id="edit" onclick="javascript:editNewEnquiry();"  value="Edit">
					</div>
					</logic:notEmpty>
				</logic:equal>				
				<!----------------------- Bottom Input buttons : Block End  ------------------------------------>
				
			</td>
		</tr>
	</table>

</html:form>

<style>
td {
    font-family: Verdana, Arial, Helvetica, sans-serif;
}
.blueheader {
    FONT-WEIGHT: bolder;
    FONT-SIZE: 15px;
    COLOR: #00458A;
    font-family: Arial, Helvetica, sans-serif;
    padding-right: 10px;
}
.blue-light {
    background: #CCD8F2;
    /* padding-left: 3px; */
    /* padding-right: 3px; */
    width: 97%;
    padding: 5px;
    float: left;
    margin: 0 0 10px 0;
    font-size: 12px;
}
.dividingdots {
    border-right: #999999 1px dotted;
}


.mandatory {
    background-image: url(../images/bullets.gif);
    background-repeat: no-repeat;
    background-position: left center;
    padding-left: 10px;
    margin-left: 2px;
}
.viewtext{
	margin-left: 5px;
    float: left;
    line-height: 21px;
	font-size:11px;
	}
</style>