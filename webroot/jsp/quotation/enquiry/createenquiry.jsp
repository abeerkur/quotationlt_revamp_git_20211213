<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants" %>
<%@ page import="in.com.rbc.quotation.common.utility.PropertyUtility" %>
<%@ page import="in.com.rbc.quotation.common.utility.CommonUtility" %>
<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>

<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
 

<html:form action="createEnquiry.do" method="POST" enctype="multipart/form-data">
<html:hidden property="invoke" value="createEnquiry"/>
<html:hidden property="dispatch" />
<html:hidden property="createdBySSO" />
<html:hidden property="operationType"/> 
<html:hidden property="ratingOperation"/>
<html:hidden property="isValidated"/>
<html:hidden property="revisionNumber"/>
<html:hidden property="enquiryOperationType"/>
<html:hidden property="ratingOperationType"/>
<html:hidden property="ratingOperationId"/>
<html:hidden property="enquiryId"/>
<html:hidden property="ratingId"/>
<html:hidden property="ratingNo"/>
<html:hidden property="ratingsValidationMessage" styleId="ratingsValidationMessage"/>
<html:hidden property="currentRating"/>
<html:hidden property="nextRatingId"/>
<html:hidden property="createdDate"/>
<html:hidden property="createdBy"/>
<html:hidden property="isMaxRating"/>
<html:hidden property="enquiryNumber"/>
<html:hidden property="commentOperation" styleId="commentOperation"/>

<input type="hidden" name='<%= org.apache.struts.taglib.html.Constants.TOKEN_KEY %>'
         value='<%= session.getAttribute(org.apache.struts.action.Action.TRANSACTION_TOKEN_KEY) %>' />


<!-------------------------- main table Start  --------------------------------->
<table width="80%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
  <tr>
    <td height="100%" valign="top">
	<!-------------------------- Top Title and Status & Note block Start  --------------------------------->
        <div class="sectiontitle">
        	<logic:equal name="enquiryForm" property="dispatch" value="edit">
        		<bean:message key="quotation.edit.label.title"/> 
        	</logic:equal>
        	<logic:notEqual name="enquiryForm" property="dispatch" value="edit">
	        	<bean:message key="quotation.create.label.title"/> 
	        </logic:notEqual>
        </div><br><br>
        <logic:notEqual name="enquiryForm" property="dispatch" value="edit">
	        <div class="DotsTable" align="center">
	    	   <bean:message key="quotation.create.label.title.message1"/> <b> <bean:message key="quotation.create.label.title.message2"/></b> <bean:message key="quotation.create.label.title.message3"/>
        	</div>
        	<br>
        	<div style=" width:100%; text-align:right; margin-bottom:20px;" id="attach">Attach Files&nbsp;<img src="html/images/ico_attach.gif" border="0" width="23" height="25" alt="Attachments" align="absmiddle" class="hand"  onclick="showhide('manageattachments','attach')" /></div>
			<fieldset style="width:100%;" class="hidetable" id="manageattachments">
			<legend class="blueheader">Manage Attachments</legend>
         	  <table width="100%" border="0" cellspacing="0" cellpadding="0">
        	     <tr>
        	      <td style="padding-bottom:10px;">
				  	    	   <logic:present name="oAttachmentDto" scope="session">
	    	   		Max. upload file size for attachments: 
	    	   		  <b><bean:write name="oAttachmentDto" property="maxFileSize"/> <%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_PROPERTYFILENAME, QuotationConstants.QUOTATION_ATTACHMENT_FILESIZEUNITS)%></b>
	    	   		<br> 
	    	   		Max. attachments per enquiry: 
	    	   		<b><bean:write name="oAttachmentDto" property="maxAttachments"/></b>	  
					<br> Allowed file formats for attachments: 
					<logic:notEqual name="oAttachmentDto" property="fileTypes" value="">
					<b><bean:write name="oAttachmentDto" property="fileTypes"/></b>	
	    	   		  </logic:notEqual>   	   		
	    	   </logic:present>  	
    	    	
    	    	<logic:equal name="enquiryForm" property="isMaxRating" value="true">
    	    		<br> 
    	    		Max. Limit Exceeded, cannot add more ratings.
    	    	</logic:equal>
    	    	</td>
    	    	</tr>
    	    	
                <tr>
                <td><table width="100%">
                <tr> <td width="20%" class="formLabel">Attach 1</td>
				  <td width="11%"> <html:file property="theFile1" style="width:218px; font:10px;"/></td>
				  <td width="19%"></td>
	              <td width="20%"   class="formLabel">Attach 4</td>
				  <td width="15%"> <html:file property="theFile2" style="width:218px; font:10px;"/></td>
				  <td width="15%"></td>	</tr>
                </table></td>
                             
  </tr>
                <tr><td><table width="100%">
	              <td width="20%"  class="formLabel">Attach 2</td>
				  <td width="11%"> <html:file property="theFile3" style="width:218px; font:10px;"/></td>
				  <td width="19%"></td>
                  <td width="20%"  class="formLabel">Attach 5</td>
				  <td width="15%"> <html:file property="theFile4" style="width:218px; font:10px;"/></td>
				  <td width="15%"></td>		              </table></td>
  </tr>
               
                <tr>
                <td><table width="100%">
	              <td width="20%"  class="formLabel">Attach 3</td>
				  <td width="11%"> <html:file property="theFile5" style="width:218px; font:10px;"/></td>
				  <td width="19%"></td>
                  <td width="32%">&nbsp;</td>
				  <td width="9%"></td>
				  <td width="9%"></td>	
                  </table></td>              
  </tr>
        	     
                  <logic:present name="attachmentMessage1" scope="request">
                  	<tr>
					<td colspan="2"><bean:write name="attachmentMessage1"/></td>
					
					</tr>                  	
                  </logic:present>
                  <logic:present name="attachmentMessage2" scope="request">
                  	<tr>
					<td colspan="2"><bean:write name="attachmentMessage2"/></td>
					
					</tr>                  	
                  </logic:present>
                  <logic:present name="attachmentMessage3" scope="request">
                  	<tr>
					<td colspan="2"><bean:write name="attachmentMessage3"/>
					</td>
					</tr>                  	
                  </logic:present>
                  <logic:present name="attachmentMessage4" scope="request">
                  	<tr>
					<td colspan="2"><bean:write name="attachmentMessage4"/></td>
					
					</tr>                  	
                  </logic:present>
                  <logic:present name="attachmentMessage5" scope="request">
                  	<tr>
					<td colspan="2"><bean:write name="attachmentMessage5"/></td>					
					</tr>                  	
                  </logic:present>
                   <logic:present name="attachmentList" scope="request">
		            <logic:notEmpty name="attachmentList">
                  <tr>
		     		<td colspan="2"><fieldset>
		       		<table width="100%" class="other">
		         	<logic:iterate name="attachmentList" id="attachmentData" indexId="idx1"
								               type="in.com.rbc.quotation.common.vo.KeyValueVo">	
					<tr><td>
						<a href="javascript:download('<bean:write name="attachmentData" property="key"/>', '<bean:write name="attachmentData" property="value"/>', '<bean:write name="attachmentData" property="value2"/>');">
							<bean:write name="attachmentData" property="value"/>.<bean:write name="attachmentData" property="value2"/>
						</a>
					</td></tr>					
				</logic:iterate>
				    </table>
              		</fieldset>
              		</td></tr>
		         	</logic:notEmpty>
           		 </logic:present>
    	      </table>
		  </fieldset>
			
        </logic:notEqual>
        <logic:equal name="enquiryForm" property="dispatch" value="edit">
	         <table width="100%" border="0" cellpadding="0" cellspacing="0">
    			<tr>
			      <td rowspan="2"><table width="98%"  border="0" cellspacing="2" cellpadding="0" class="DotsTable">
          		<tr>
            		<td rowspan="2" style="width:70px "><img src="html/images/icon_quote.gif" width="52" height="50"></td>
		            <td class="other">	<bean:message key="quotation.viewenquiry.label.subheading1"/> <bean:write name="enquiryForm" property="enquiryNumber"/><br>
               			 <bean:message key="quotation.viewenquiry.label.subheading2"/></td>
         		</tr>


          		         		<tr>
            		<td class="other">
            		<table width="100%">
                		<tr>
                		         	<logic:equal name="enquiryForm" property="returnstatement" value="RETURNDM">
                		                      			<td height="16" class="section-head" align="center">Status : <bean:write name="enquiryForm" property="returnstatus" /> </td>
                		         	</logic:equal>
                		   <logic:equal name="enquiryForm" property="returnstatement" value="edit">      	
                  			<td height="16" class="section-head" align="center">Status : Draft </td>
                  			</logic:equal>
               		 </tr>
            		</table>
            		
            		
            		</td>
          		</tr>
          		
          		
          		
          		
      		</table></td>
      		<logic:lessThan name="enquiryForm" property="statusId" value="10"> 

      		<logic:notEqual name="enquiryForm" property="statusId" value="7">
		      	<td class="message"><bean:message key="quotation.attachments.message"/> </td>
     			 <td class="message">
     	 			<a href="javascript:manageAttachments('<%= CommonUtility.getIntValue(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_ATTACHMENT_APPLICATIONID)) %>','<bean:write name="enquiryForm" property="enquiryId"/>', '<bean:write name="enquiryForm" property="createdBySSO"/>');">
     				 <img src="html/images/ico_attach.gif" alt="Manage Attachments" width="23" height="25" border="0" align="right"></a>
     			</td>
     			</logic:notEqual>
     	 	</logic:lessThan>
    	</tr>
    	<tr>	
    		<td align="center"> <div id="attachmentmessage"></div></td>
		</tr>        	
  </table>
  <br>
  </logic:equal>
     
		    <div align="right"><img src="html/images/bullets.gif" width="7" height="7" align="absmiddle"><span class="note">Indicates mandatory field</span></div>
			<!-------------------------- Top Title and Status & Note block End  --------------------------------->
	   <!----------------------- Employee Request Information block start -------------------------------------->
	    <fieldset>
  		    <legend class="blueheader"><bean:message key="quotation.create.label.requestinfo"/></legend>
			  <table width="100%">
			  <tr>
			  <td style="vertical-align:top" >
				<table width="100%">
				 <tr >				  
				  <label class="blue-light" style="font-weight: bold;">
				    	<bean:message key="quotation.create.label.basicinformation"/>
				   </label>				    
				  </tr>
				  <tr>
				  <td class="formLabel" >
				    	<bean:message key="quotation.create.label.enquirytype"/>
				    	<span class="mandatory">&nbsp;</span>
				    </td>
                   <td>			       
				     <bean:define name="enquiryForm" property="alEnquiryTypeList" id="alEnquiryTypeList" type="java.util.Collection" /> 
                    	<html:select style="width:170px;" name="enquiryForm" property="enquiryType" >
                    		<option value="0"><bean:message key="quotation.option.select"/></option>
                       	 	<html:options name="enquiryForm" collection="alEnquiryTypeList" property="key" labelProperty="value" />
                        </html:select>                   	                      	
					</td>
					</tr>
					<tr>
				    <td  class="formLabel" >
				    	<bean:message key="quotation.create.label.customer"/>
				    	<span class="mandatory">&nbsp;</span>
					</td>
                    <td >
                    	<logic:equal name="enquiryForm" property="dispatch" value="edit">
                    		<input id="customerName" name="customerName" type="text" size="50" class="normal" autocomplete="off"  style="width:170px " value="<bean:write name="enquiryForm" property="customerName"/>"  />
                    	</logic:equal>
                    	<logic:notEqual name="enquiryForm" property="dispatch" value="edit">
                    		<input id="customerName" name="customerName" type="text" size="50" class="normal" autocomplete="off"  style="width:170px " value="<bean:write name="enquiryForm" property="customerName"/>"  />
                    	</logic:notEqual>
                    	<div class="note hidetable" id="invalidcustomer" style="text-align:left;padding-left:20px">Invalid Customer Name</div>
                    	
			            <html:hidden property="customerId" styleId="customerId"/>
                     </td>
                     </tr>
                  <tr>
					<td class="formLabel">
				    	<bean:message key="quotation.create.label.projectname"/>
				    </td>
				     <td>			       
                         <html:text property="projectName" styleClass="normal" maxlength="30" style="width:170px "/></td>
					</tr>
                     
                     <tr>
					
					<td class="formLabel" >
				    	<bean:message key="quotation.create.label.salesstage"/>

				    </td>
                   <td>			       
				     <bean:define name="enquiryForm" property="alSalesStageList" id="alSalesStageList" type="java.util.Collection" /> 
                    	<html:select style="width:170px;" name="enquiryForm" property="salesStage" >
                    		<option value=""><bean:message key="quotation.option.select"/></option>
                       	 	<html:options name="enquiryForm" collection="alSalesStageList" property="key" labelProperty="value" />
                        </html:select>                   	                      	
					</td>
					</tr>
					
					<tr>
					
					<td class="formLabel">
				    	<bean:message key="quotation.create.label.approval"/>
				    	
				    </td>
				     <td>			       
				     <bean:define name="enquiryForm" property="alApprovalList" id="alApprovalList" type="java.util.Collection" /> 
                    	<html:select style="width:170px;" name="enquiryForm" property="approval" >
                    		<option value=""><bean:message key="quotation.option.select"/></option>
                       	 	<html:options name="enquiryForm" collection="alApprovalList" property="key" labelProperty="value" />
                        </html:select>                   	                      	
					</td>
					</tr>
                     
                     <tr>
                     
                     <td class="formLabel">
				    	<bean:message key="quotation.create.label.customertype"/>		
				    	<span class="mandatory">&nbsp;</span>		    	
				    </td>
				     <td>
                    	<bean:define name="enquiryForm" property="alCustomerTypeList"
	                	             id="alCustomerTypeList" type="java.util.Collection" /> 
                    	<html:select style="width:170px;" name="enquiryForm" property="customerType" >
                    		<option value=""><bean:message key="quotation.option.select"/></option>
                       	 	<html:options name="enquiryForm" collection="alCustomerTypeList"
			 				              property="key" labelProperty="value" />
                        </html:select>                      	
					</td>
					</tr>
					<tr>
					<td class="formLabel">
				    	<bean:message key="quotation.create.label.enduserindustry"/>				    	
				    </td>
				     <td>	
				    	<bean:define name="enquiryForm" property="alIndustryList" id="alIndustryList" type="java.util.Collection" /> 
                    	<html:select style="width:170px;" name="enquiryForm" property="endUserIndustry">
                    		<option value=""><bean:message key="quotation.option.select"/></option>
                       	 	<html:options name="enquiryForm" collection="alIndustryList" property="key" labelProperty="value" />
                        </html:select>    
					</td>
					</tr>
					
					<tr>
					<td class="formLabel">
				    	<bean:message key="quotation.create.label.enduser"/>
				    </td>
				     <td>				     
				     <html:text property="endUser" styleClass="normal" style="width:170px" maxlength="20"/>                     	                      	
					</td> 
                    </tr>
					
					
					
					<tr>
					
					<td class="formLabel">
				    	<bean:message key="quotation.create.label.orderclosingmonth"/>				    	
				    </td>
				    <td>
				     <html:text property="orderClosingMonth" styleId="orderClosingMonth" styleClass="readonlymini" readonly="true" style="width:170px" maxlength="12"/>			     
				   
					</td>
					</tr>
					<tr>
					<td class="formLabel">
				    	<bean:message key="quotation.create.label.targeted"/>				    	
				    </td>
				     <td>			       
				     <bean:define name="enquiryForm" property="alTargetedList" id="alTargetedList" type="java.util.Collection" /> 
                    	<html:select style="width:170px;" name="enquiryForm" property="targeted" >
                    		<option value=""><bean:message key="quotation.option.select"/></option>
                       	 	<html:options name="enquiryForm" collection="alTargetedList" property="key" labelProperty="value" />
                        </html:select>                   	                      	
					</td>
					</tr>
					<tr>
					<td class="formLabel">
				    	<bean:message key="quotation.create.label.winningchance"/>				    	
				    </td>
				     <td>
				      <bean:define name="enquiryForm" property="alWinChanceList" id="alWinChanceList" type="java.util.Collection" /> 
                    	<html:select style="width:170px;" name="enquiryForm" property="winningChance">
                    		<option value=""><bean:message key="quotation.option.select"/></option>
                       	 	<html:options name="enquiryForm" collection="alWinChanceList" property="key" labelProperty="value" />
                        </html:select>    
					</td>
					</tr>
					<tr>
					<td class="formLabel" >
				    	<bean:message key="quotation.create.label.deliveriestype"/>
				    	
				    </td>
                   <td>			       
				     <bean:define name="enquiryForm" property="alDeliveryTypeList" id="alDeliveryTypeList" type="java.util.Collection" /> 
                    	<html:select style="width:170px;" name="enquiryForm" property="deliveryType">

                       	 	<html:options name="enquiryForm" collection="alDeliveryTypeList" property="key" labelProperty="value" />
                        </html:select>                   	                      	
					</td>
					</tr>
					<tr>
					<td class="formLabel">
				    	<bean:message key="quotation.create.label.expecteddeliverymonth"/>				    	
				    </td>
				    <td>	
					 
		<input type="hidden" name="expectedDelivery" id="expectedDelivery" value="<bean:write name="enquiryForm" property="expectedDeliveryMonth"/>" /> 
					   <html:select style="width:170px;" property="expectedDeliveryMonth" styleId="expectedDeliveryMonth" >
                         <option value="01 Month">1 Month</option>
                         <option value="02 Months">2 Months</option>
                         <option value="03 Months">3 Months</option>
                         <option value="04 Months" selected>4 Months</option>
                         <option value="05 Months">5 Months</option>
                         <option value="06 Months">6 Months</option>
                         <option value="07 Months">7 Months</option>
                         <option value="08 Months">8 Months</option>
                         <option value="09 Months">9 Months</option>
                         <option value="10 Months">10 Months</option>
                         <option value="11 Months">11 Months</option>
                         <option value="12 Months">12 Months</option>

                        </html:select>                   	                      	
					
					</td>
					</tr>
					<tr>
					<td class="formLabel">
				    	<bean:message key="quotation.create.label.totalordervalue"/>
				    	
				    </td>
				    <td>				     
				     <html:text property="totalOrderValue" styleClass="normal" style="width:170px" maxlength="9"/> 
					</td>
					</tr>
					</table>
					</td>
					<td width="2%" height="100%" rowspan="3" align="center"  class="dividingdots">&nbsp;</td>
					<td style="vertical-align:top" >
					
					
					
					<table width="100%">
				 	<tr>				  
				  	<label class="blue-light" style="font-weight: bold;">
				    	<bean:message key="quotation.create.label.result"/>
				    </label>
				  </tr>
				  <tr>
				  <td class="formLabel">
				    	<bean:message key="quotation.create.label.winlossprice"/>
				    	
				    </td>
				    <td>				     
				     <html:text property="winlossPrice" styleClass="normal" style="width:170px" maxlength="9"/> 
					</td>
					</tr>
					<tr>
					<td class="formLabel">
				    	<bean:message key="quotation.create.label.winlossreason"/>
				    	
				    </td>
				    <td>				     
				     <bean:define name="enquiryForm" property="alWinlossReasonList" id="alWinlossReasonList" type="java.util.Collection" /> 
                    	<html:select style="width:170px;" name="enquiryForm" property="winlossReason" >
                    		<option value=""><bean:message key="quotation.option.select"/></option>
                       	 	<html:options name="enquiryForm" collection="alWinlossReasonList" property="key" labelProperty="value" />
                        </html:select>  
					</td>
					</tr>
					<tr>
					<td class="formLabel">
				    	<bean:message key="quotation.create.label.winlosscomment"/>
				    	
				    </td>
				    <td>				     
				     <html:text property="winlossComment" styleClass="normal" style="width:170px" maxlength="50"/> 
					</td>
                    </tr>
                   
                    <tr>
					<td class="formLabel">
				    	<bean:message key="quotation.create.label.winningcompetitor"/>
				    	
				    </td>
				    <td>
				      <bean:define name="enquiryForm" property="alWinCompetitorList" id="alWinCompetitorList" type="java.util.Collection" /> 
                    	<html:select style="width:170px;" name="enquiryForm" property="winningCompetitor">
                    		<option value=""><bean:message key="quotation.option.select"/></option>
                       	 	<html:options name="enquiryForm" collection="alWinCompetitorList" property="key" labelProperty="value" />
                        </html:select>  
					</td>
                     </tr>
                                              
                     </table>
                     </td>
                     <td width="1%" height="100%" rowspan="3" align="center"  class="dividingdots">&nbsp;</td>
          
                     <td style="vertical-align:top" >
                     <table width="100%">
				 	<tr>				  
				  	<label class="blue-light" style="font-weight: bold;">
				    	<bean:message key="quotation.create.label.financial"/>
				    </label>
				  </tr>
				  <tr>
					 	<td class="formLabel">
					    	<bean:message key="quotation.create.label.materialcost"/>
					    	
					    </td>
					    <td>				     
					    	 <html:text  property="materialCost" styleClass="normal" style="width:170px" maxlength="9"/> 
						</td>
					</tr>
					<tr>					
					<td class="formLabel">
				    	<bean:message key="quotation.create.label.mcnspratio"/>
				    	
				    	
				    </td>
				    <td>				     
				     <html:text  property="mcnspRatio" styleClass="normal" style="width:170px" maxlength="9"/> 
					</td>
                    </tr>                   
                    <tr>					
					<td class="formLabel">
				    	<bean:message key="quotation.create.label.totalprice"/>
				    	
				    </td>
				    <td>				     
				     	<html:text property="totalPrice" styleClass="normal" style="width:170px" maxlength="9"/> 
					</td>
                     </tr>
                     <tr>
					
					<td class="formLabel">
				    	<bean:message key="quotation.create.label.warrantycost"/>
				    	
				    </td>
				    <td>				     
				     <html:text property="warrantyCost" styleClass="normal" style="width:170px" maxlength="9"/> 
					</td>
					</tr>
					<tr>
					<td class="formLabel">
				    	<bean:message key="quotation.create.label.warrantydispatchdate"/>
				    </td>
				    <td>
			    				     
<input type="hidden" name="warrantyDispatch" id="warrantyDispatch" value="<bean:write name="enquiryForm" property="warrantyDispatchDate" />" />

<html:select property="warrantyDispatchDate" styleId="warrantyDispatchDate"  style="width:170px">

                    		<option value="12">12</option>
                    		<option value="18" selected>18</option>
                    		<option value="24">24</option>
                    	    <option value="30">30</option>
                            <option value="36">36</option>
                            <option value="42">42</option>
                            <option value="48">48</option>
                            <option value="54">54</option>
                            <option value="60">60</option>
        	            </html:select>
					</td>
					 </tr>              
                    <tr>
					<td class="formLabel">
				    	<bean:message key="quotation.create.label.warrantycommissioningdate"/>
				    </td>
				    <td>
				    				     

<input type="hidden" name="warrantyCommissioning" id="warrantyCommissioning" value="<bean:write name="enquiryForm" property="warrantyCommissioningDate" />" />
<html:select property="warrantyCommissioningDate" styleId="warrantyCommissioningDate"  style="width:170px">
                    		<option value="12" selected>12</option>
                    		<option value="18">18</option>
                    		<option value="24">24</option>
                    	    <option value="30">30</option>
                            <option value="36">36</option>
                            <option value="42">42</option>
                            <option value="48">48</option>
                            <option value="54">54</option>
                            <option value="60">60</option>
        	            </html:select>

					</td>
                    </tr>
                    
                    <tr>
					<td class="formLabel">
				    	<bean:message key="quotation.create.label.advancepayment"/>(%)
				    	
				    </td>
				    <td>				     
				     <html:text property="advancePayment" styleId="advancePayment" styleClass="normal" style="width:170px" maxlength="50"   onkeypress="return AllowAlphabet(event)"  /></td>
					</tr>
						<tr>
					<td class="formLabel">
				    	<bean:message key="quotation.create.label.paymentterms"/>
				    	
				    </td>
				    <td>				     
				     <html:text property="paymentTerms" styleId="paymentTerms" styleClass="normal" style="width:170px" maxlength="50"  onkeypress="return AllowAlphabet(event)" /> 
					</td>
					</tr>
                    
					
					 <tr>
					
					<td class="formLabel">
				    	<bean:message key="quotation.create.label.supervisioneccost"/>
				    	
				    </td>
				    <td>				     
				     <html:text  property="ecSupervisionCost" styleClass="normal" style="width:170px" maxlength="9"/> 
					</td>
					</tr>
					<tr>
					<td class="formLabel">
				    	<bean:message key="quotation.create.label.sparescost"/>
				    	
				    </td>
				    <td>				     
				     <html:text  property="sparesCost" styleClass="normal" style="width:170px" maxlength="9"/> 
					</td>
					</tr>
					<tr>
					<td class="formLabel">
				    	<bean:message key="quotation.create.label.transportationcost"/>
				    	
				    </td>
				    <td>				     
				     <html:text property="transportationCost" styleClass="normal" style="width:170px" maxlength="9"/> 
					</td>
                    </tr>
                    <tr>
                    <td class="formLabel">
				    	<bean:message key="quotation.create.label.hzareacertcost"/>
				    	
				    </td>
				    <td>				     
				     <html:text  property="hzAreaCertCost" styleClass="normal" style="width:170px" maxlength="9"/> 
					</td>
					</tr>
                    <tr>
					<td class="formLabel">
				    	<bean:message key="quotation.create.label.totalpackageprice"/>
				    	
				    </td>				    
				    <td>				     
				     <html:text  property="totalPkgPrice" styleClass="normal" style="width:170px" maxlength="9"/> 
					</td>
                    </tr>
                     </table>
                     </td>
                     </tr>
                     </table>     
										         
                    
				<table width="100%">
				<tr style="float:left; margin-bottom:5px;">
					<td class="formLabel" style="width:138px; margin-right: 33px; padding-left: 3px;">
                    	<bean:message key="quotation.create.label.enquiryreferencenumber"/>
                    </td>
                    <td style="margin-left:2px; float:left;'">
                    <html:text  property="enquiryReferenceNumber" styleClass="normal" style="width:168px" maxlength="30"/>                     	
                    </td>
                    <td></td>	
                    <td></td>	
                </tr>	                  
				  <tr style="float:left; width:100%;">				  
				    <td class="formLabel" style="width:138px; margin-right: 33px;">
				    	<bean:message key="quotation.create.label.description"/>
				    </td>
                    <td style="margin-left:2px; float:left;">
                    	<html:textarea  property="description" styleId = "description"  styleClass="normal" onkeyup="checkMaxLenghtText(this, 4000);" onkeydown="checkMaxLenghtText(this, 4000);" style="width:544px; height:50px "/>                    </td>
                    	<td></td>	
                    <td></td>
                  </tr>
                 <tr style="float:left; width:100%;">			
				    <td class="formLabel" style="margin-right: 33px;">
                    	<bean:message key="quotation.create.label.createdby"/>
                    </td>
                    <td style="margin-left:5px; float:left; line-height: 21px;">
                    	<bean:write name="enquiryForm" property="createdBy"/> 
                    </td>
                    <td class="formLabel" style="margin-left:134px; line-height: 21px;">
                    	<bean:message key="quotation.create.label.createddate"/> 
                    </td>
                    <td style="line-height:20px;">
                    	<bean:write name="enquiryForm" property="createdDate"/>
                    </td>
                  </tr>	
			    </table>
	    </fieldset>
		<!----------------------- Employee Request Information block End -------------------------------------->
		<!----------------------- Employee Technical Information block start -------------------------------------->
		<br>
		
		
		<table align="left" style="width:100%">
		<bean:define id="latestId" name="enquiryForm" property="currentRating"/> 

		
		<logic:notEqual name="enquiryForm" property="dispatch" value="edit">

		<tr>
			<td style="text-align:left">
				<div align="left" id="viewtopnav">
				<ul>
				<logic:iterate name="enquiryForm" property="ratingList"
								               id="ratingListData" indexId="idx"
								               type="in.com.rbc.quotation.common.vo.KeyValueVo">
					 <logic:equal name="ratingListData" property="value2" value="Y">
					 	<li>
					 		<a href="javascript:saveAsDrafts('ADDNEWRATING', '<bean:write name="ratingListData" property="key" />')" class="valid">
				 				<logic:equal name="ratingListData" property="key" value="<%= latestId.toString() %>">
					 				<b><bean:write name="ratingListData" property="value"/></b>
					 			</logic:equal>
					 			<logic:notEqual name="ratingListData" property="key" value="<%= latestId.toString() %>">
					 				<bean:write name="ratingListData" property="value"/>
					 			</logic:notEqual>
					 		</a>
					 	</li>					 	
					 </logic:equal>	
					 <logic:equal name="ratingListData" property="value2" value="N">
					 	<li> 
					 		<a href="javascript:saveAsDrafts('ADDNEWRATING', '<bean:write name="ratingListData" property="key" />')" class="notvalid">
					 			
					 			<logic:equal name="ratingListData" property="key" value="<%= latestId.toString() %>">
					 				<b><bean:write name="ratingListData" property="value"/></b>
					 			</logic:equal>
					 			<logic:notEqual name="ratingListData" property="key" value="<%= latestId.toString() %>">
					 				<bean:write name="ratingListData" property="value"/>
					 			</logic:notEqual>
					 		</a>
					 	</li>	
					 </logic:equal>					 
				</logic:iterate>
				</ul></div></td>				
					
		</tr>
		</logic:notEqual>
		<!-- Return From DM or DE -->
		<logic:equal name="enquiryForm" property="dispatch" value="edit">
		

		<logic:equal name="enquiryForm" property="returnstatement" value="RETURNDM">
		<!-- Inside Retutn DM  -->
		<tr>
			<td style="text-align:left">
				<div align="left" id="viewtopnav">
				<ul>
				<logic:iterate name="enquiryForm" property="ratingList"
								               id="ratingListData" indexId="idx"
								               type="in.com.rbc.quotation.common.vo.KeyValueVo">
					 <logic:equal name="ratingListData" property="value2" value="Y">
					 	<li>
					 		<a href="javascript:saveAsDrafts('ADDNEWRATING_RETURNDM', '<bean:write name="ratingListData" property="key" />')" class="valid">
				 				<logic:equal name="ratingListData" property="key" value="<%= latestId.toString() %>">
					 				<b><bean:write name="ratingListData" property="value"/></b>
					 			</logic:equal>
					 			<logic:notEqual name="ratingListData" property="key" value="<%= latestId.toString() %>">
					 				<bean:write name="ratingListData" property="value"/>
					 			</logic:notEqual>
					 		</a>
					 	</li>					 	
					 </logic:equal>	
					 <logic:equal name="ratingListData" property="value2" value="N">
					 	<li> 
					 		<a href="javascript:saveAsDrafts('ADDNEWRATING_RETURNDM', '<bean:write name="ratingListData" property="key" />')" class="notvalid">
					 			
					 			<logic:equal name="ratingListData" property="key" value="<%= latestId.toString() %>">
					 				<b><bean:write name="ratingListData" property="value"/></b>
					 			</logic:equal>
					 			<logic:notEqual name="ratingListData" property="key" value="<%= latestId.toString() %>">
					 				<bean:write name="ratingListData" property="value"/>
					 			</logic:notEqual>
					 		</a>
					 	</li>	
					 </logic:equal>					 
				</logic:iterate>
				</ul></div></td>				
					
		</tr>
		</logic:equal>

		<logic:equal name="enquiryForm" property="returnstatement" value="edit">
		<!-- Inside Return Edit  -->
				<tr>
			<td style="text-align:left">
				<div align="left" id="viewtopnav">
				<ul>
				<logic:iterate name="enquiryForm" property="ratingList"
								               id="ratingListData" indexId="idx"
								               type="in.com.rbc.quotation.common.vo.KeyValueVo">
					 <logic:equal name="ratingListData" property="value2" value="Y">
					 	<li>
					 		<a href="javascript:saveAsDrafts('ADDNEWRATING', '<bean:write name="ratingListData" property="key" />')" class="valid">
				 				<logic:equal name="ratingListData" property="key" value="<%= latestId.toString() %>">
					 				<b><bean:write name="ratingListData" property="value"/></b>
					 			</logic:equal>
					 			<logic:notEqual name="ratingListData" property="key" value="<%= latestId.toString() %>">
					 				<bean:write name="ratingListData" property="value"/>
					 			</logic:notEqual>
					 		</a>
					 	</li>					 	
					 </logic:equal>	
					 <logic:equal name="ratingListData" property="value2" value="N">
					 	<li> 
					 		<a href="javascript:saveAsDrafts('ADDNEWRATING', '<bean:write name="ratingListData" property="key" />')" class="notvalid">
					 			
					 			<logic:equal name="ratingListData" property="key" value="<%= latestId.toString() %>">
					 				<b><bean:write name="ratingListData" property="value"/></b>
					 			</logic:equal>
					 			<logic:notEqual name="ratingListData" property="key" value="<%= latestId.toString() %>">
					 				<bean:write name="ratingListData" property="value"/>
					 			</logic:notEqual>
					 		</a>
					 	</li>	
					 </logic:equal>					 
				</logic:iterate>
				</ul></div></td>				
					
		</tr>
		
		</logic:equal>
		</logic:equal>
		</table>
		
		<br><br>		
		
		
		    <fieldset>
			     <div class="blue-light" >
				  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr>
					<td>
				
				  <bean:message key="quotation.create.label.quantityrequired"/>
				  <logic:equal name="enquiryForm" property="quantity" value="0">
				      <html:text property="quantity" tabindex="7" styleClass="mini" maxlength="2" value="" />
				  </logic:equal>
				  <logic:notEqual name="enquiryForm" property="quantity" value="0">
				      <html:text property="quantity" tabindex="7" styleClass="mini" maxlength="2" />
				  </logic:notEqual>
				
					</td>
					<td>
					<% if (latestId.toString() != null && latestId.toString().trim().length() > 0){ %>
					 <div style="text-align:right;"> 
				   		<img src="html/images/deleteicon.gif" alt="Delete" width="15" height="16" border="0" style="cursor:hand " onClick="javascript:deleteRating('<%= latestId.toString() %>');">
				     </div>  
				   <%} %>
					</td>
				  </tr>
				</table>
		        	</div>
		      
			    <br>
			    <table width="100%">
			      <tr>
			       <td class="formLabelrating" style="width:150px ">
                    	<bean:message key="quotation.create.label.kw"/>
                    	<span class="mandatory">&nbsp;</span>
                    </td>
                    <td>
                    	<html:text tabindex="19" property="kw" styleClass="rating-box"  maxlength="25" />
                    	 
                    	<html:select property="ratedOutputUnit" styleClass="short" style="width:44px;" >
                    	<logic:equal name="enquiryForm" property="ratedOutputUnit" value="kW">
						<option value='kW'><bean:message key="quotation.option.kw"/></option>
						<option value='HP'><bean:message key="quotation.option.hp"/></option>
						</logic:equal>
						<logic:equal name="enquiryForm" property="ratedOutputUnit" value="HP">
						
						<option value='HP'><bean:message key="quotation.option.hp"/></option>
						<option value='kW'><bean:message key="quotation.option.kw"/></option>
						</logic:equal>
						<logic:equal name="enquiryForm" property="ratedOutputUnit" value="">
						<option value='kW'><bean:message key="quotation.option.kw"/></option>
						<option value='HP'><bean:message key="quotation.option.hp"/></option>
						
						</logic:equal>
				
				
						
						</html:select>  
						                    	
                    </td>			        
                    <td  rowspan="15" align="center" class="dividingdots1_new">&nbsp;</td>
                   <td height="100%"  class="formLabelrating">
			        	<bean:message key="quotation.create.label.htlt"/>
			        </td>
                    <td>
                    	<bean:define name="enquiryForm" property="htltList"
	                	             id="htltList" type="java.util.Collection" /> 
                    	<html:select styleClass="rating-box" property="htltId" tabindex="8">
                    		<option value="0">Select</option>
                       	 	<html:options name="createRequestForm" collection="htltList"
			 				              property="key" labelProperty="value" />
                        </html:select>                      	
					</td>
                  </tr>
			      <tr>
			        <td height="100%" class="formLabelrating">
			        	<bean:message key="quotation.create.label.pole"/>
			        	<span class="mandatory">&nbsp;</span>
			        </td>
                    <td>
                    	<bean:define name="enquiryForm" property="poleList"
	                	             id="poleList" type="java.util.Collection" /> 
                        <html:select styleClass="rating-box" property="poleId" tabindex="9">
                         <option value="0"><bean:message key="quotation.option.select"/></option>
                          <html:options name="createRequestForm" collection="poleList"
			 				              property="key" labelProperty="value" />
                       </html:select>
                    </td>
                    <td class="formLabelrating">
                    	<bean:message key="quotation.create.label.frequency"/>
                    </td>
                    <td>
                    <bean:define name="enquiryForm" property="alFrequencyList"
	                	             id="alFrequencyList" type="java.util.Collection" /> 
                        <html:select styleClass="short" property="frequency" >
                         <option value="0"><bean:message key="quotation.option.select"/></option>
                          <html:options name="createRequestForm" collection="alFrequencyList"
			 				              property="key" labelProperty="value" />
                       </html:select>
                    	+/-
                    	<logic:equal name="enquiryForm" property="frequencyVar" value="">
                    	<html:text tabindex="21" property="frequencyVar" styleId="frequencyVar" value ="5"   styleClass="short" maxlength="2" onkeypress="return isNumber(event)" onblur="return setNumber('frequencyVar');" />
                    	</logic:equal>
                    	<logic:notEqual name="enquiryForm" property="frequencyVar" value="">
                    	<html:text tabindex="21" property="frequencyVar" styleId="frequencyVar"  styleClass="short" maxlength="2" onkeypress="return isNumber(event)" onblur="return setNumber('frequencyVar');" />
                    	</logic:notEqual>
                      	
                    	%
                    </td>
                  </tr>
			      <tr>
			        <td class="formLabelrating">
			        	<bean:message key="quotation.create.label.scsr"/>
			        	<span class="mandatory">&nbsp;</span>
					</td>
                    <td>
                    	<bean:define name="enquiryForm" property="scsrList"
	                	             id="scsrList" type="java.util.Collection" /> 
                    	<html:select property="scsrId" styleClass="rating-box" tabindex="10">
                    	<option value="0"><bean:message key="quotation.option.select"/></option>
                         <html:options name="createRequestForm" collection="scsrList"
			 				              property="key" labelProperty="value" />
                   		</html:select>
                   	</td>
                   	
                   	 <td class="formLabelrating">
			        	<bean:message key="quotation.create.label.duty"/>
			        </td>
                    <td>
                    	<bean:define name="enquiryForm" property="dutyList"
	                	             id="dutyList" type="java.util.Collection" /> 
                    	<html:select property="dutyId"  styleClass="rating-box" tabindex="11">
                    	<option value="0"><bean:message key="quotation.option.select"/></option>
                        <html:options name="createRequestForm" collection="dutyList"
			 				              property="key" labelProperty="value" />
                    	</html:select>
                    </td>
                   
                  </tr>
			      <tr>
			       <td class="formLabelrating">
                    	<bean:message key="quotation.create.label.volts"/> 
                    	<span class="mandatory">&nbsp;</span>
                    </td>
                    <td>                    	
                    	<bean:define name="enquiryForm" property="alVoltList" id="alVoltList" type="java.util.Collection" /> 
                    	<html:select property="volts" styleClass="short" tabindex="31">
                    		<option value="0"><bean:message key="quotation.option.select"/></option>
                        	<html:options name="createRequestForm" collection="alVoltList"
			 				              property="key" labelProperty="value" />
                     	</html:select>
                      	+/-
                      	<logic:notEqual name="enquiryForm" property="voltsVar" value="">
                      	   <html:text tabindex="23" property="voltsVar" styleId="voltsVar"   styleClass="short" maxlength="2" onkeypress="return isNumber(event)" onblur="return setNumber('voltsVar');" />
                      	
                      	</logic:notEqual>
                      	<logic:equal name="enquiryForm" property="voltsVar" value="">
                     	 <html:text tabindex="23" property="voltsVar" styleId="voltsVar" value ="10"  styleClass="short" maxlength="2" onkeypress="return isNumber(event)" onblur="return setNumber('voltsVar');" />
                    </logic:equal>
                    
                    	%</td>
			       
                    <td class="formLabelrating">
                    	<bean:message key="quotation.create.label.ambience"/>
                    </td>
                    <td>
                    	<bean:define name="enquiryForm" property="alAmbienceList"
	                	             id="alAmbienceList" type="java.util.Collection" /> 
                    	<html:select property="ambience" styleClass="rating-box" tabindex="31">
                    		<option value="0"><bean:message key="quotation.option.select"/></option>
                        	<html:options name="createRequestForm" collection="alAmbienceList"
			 				              property="key" labelProperty="value" />
                     	</html:select>                	
                   	</td>
                  </tr>
			      <tr>
			      <td class="formLabelrating">
                    	<bean:message key="quotation.create.label.enclosure"/>
                    	<span class="mandatory">&nbsp;</span>
                    </td>
                     <td>
                     	<bean:define name="enquiryForm" property="enclosureList"
	                	             id="enclosureList" type="java.util.Collection" /> 
                    	<html:select property="enclosureId" styleClass="rating-box" tabindex="31">
                    		<option value="0"><bean:message key="quotation.option.select"/></option>
                        	<html:options name="createRequestForm" collection="enclosureList"
			 				              property="key" labelProperty="value" />
                     	</html:select>
                   	 </td>
			        <td  class="formLabelrating"><bean:message key="quotation.create.label.degpro"/> </td>
                    <td>
                    	<bean:define name="enquiryForm" property="degproList"
	                	             id="degproList" type="java.util.Collection" /> 
                    	<html:select property="degreeOfProId" styleClass="rating-box" tabindex="12">
                    		<option value="0"><bean:message key="quotation.option.select"/></option>
                       	   <html:options name="createRequestForm" collection="degproList"
			 				              property="key" labelProperty="value" />
                    	</html:select>
                    </td>
                    
                  </tr>
			      <tr>
			       <td class="formLabelrating">
                    	<bean:message key="quotation.create.label.mounting"/>
                    	<span class="mandatory">&nbsp;</span>
                    </td>
                    <td>
                    	<bean:define name="enquiryForm" property="mountingList"
	                	             id="mountingList" type="java.util.Collection" /> 
                    	<html:select property="mountingId" styleClass="rating-box" tabindex="32">
                    		<option value="0"><bean:message key="quotation.option.select"/></option>
	                        <html:options name="createRequestForm" collection="mountingList"
			 				              property="key" labelProperty="value" />
            	         </html:select>
            	    </td>
			      <td class="formLabelrating"><bean:message key="quotation.create.label.altitude"/></td>
                    <td>
                    	<html:text tabindex="26" property="altitude"  styleClass="rating-box" maxlength="20" />                      		
                    </td>			       
                  </tr>
                  <tr>
 
 			        <td  class="formLabelrating">
			        	<bean:message key="quotation.create.label.application"/>
			        	<span class="mandatory">&nbsp;</span>
			        </td>
                    <td>
                    	<bean:define name="enquiryForm" property="applicationList"
	                	             id="applicationList" type="java.util.Collection" /> 
                    	<html:select property="applicationId" styleClass="rating-box" tabindex="18">
                    		<option value="0"><bean:message key="quotation.option.select"/></option>
	                        <html:options name="createRequestForm" collection="applicationList"
			 				              property="key" labelProperty="value" />
        	            </html:select></td>
 
    <td  class="formLabelrating"><bean:message key="quotation.create.label.stgmethod"/> </td>
                    <td>
                    	<html:text property="methodOfStg" styleClass="rating-box" maxlength="10" tabindex="13"/>
                    </td>
                    </tr>
                    <tr>
                  <td>&nbsp;</td>
			      	<td>&nbsp; </td>
                    <td class="formLabelrating">
                    	<bean:message key="quotation.create.label.temprise"/> 
                    </td>
                    <td>
                    	        
                    	<bean:define name="enquiryForm" property="alTempRaiseList"
	                	             id="alTempRaiseList" type="java.util.Collection" /> 
                    	<html:select property="tempRaise" styleClass="rating-box" tabindex="30">
                    		<option value="0"><bean:message key="quotation.option.select"/></option>
                       		 <html:options name="createRequestForm" collection="alTempRaiseList"
			 				              property="key" labelProperty="value" />
                     	</html:select>               	
                    </td>
                    </tr>
			      <tr>
			       <td>&nbsp;</td>
			      	<td>&nbsp; </td>
			        <td  class="formLabelrating">
			        	<bean:message key="quotation.create.label.coupling"/>
			        </td>
                    <td>
                    	<html:text property="coupling" styleClass="rating-box" maxlength="10" tabindex="14" />
                    	
                    	
                    </td>
                    </tr>
                    <tr>
			      <td>&nbsp;</td>
			      	<td>&nbsp; </td>
                    <td class="formLabelrating">
                    	<bean:message key="quotation.create.label.insulationclass"/> </td>
                    <td>
                    	<bean:define name="enquiryForm" property="insulationList"
	                	             id="insulationList" type="java.util.Collection" /> 
                    	<html:select property="insulationId" styleClass="rating-box" tabindex="30">
                    		<option value="0"><bean:message key="quotation.option.select"/></option>
                       		 <html:options name="createRequestForm" collection="insulationList"
			 				              property="key" labelProperty="value" />
                     	</html:select>                        
                    </td>
                  </tr>
			      <tr>
			      <td>&nbsp;</td>
			      	<td>&nbsp; </td>
			        <td  class="formLabelrating">
			        	<bean:message key="quotation.create.label.termbox"/> 
			        </td>
			         <td>
                    	<bean:define name="enquiryForm" property="termBoxList"
	                	             id="termBoxList" type="java.util.Collection" /> 
                    	<html:select property="termBoxId" styleClass="rating-box" tabindex="15">
                    		<option value="0"><bean:message key="quotation.option.select"/></option>
	                        <html:options name="createRequestForm" collection="termBoxList"
			 				              property="key" labelProperty="value" />
                	    </html:select>
                	</td>
			        </tr>
			        
			      <tr>
			       <td>&nbsp;</td>
			      	<td>&nbsp; </td>
			        <td  class="formLabelrating">
			        	<bean:message key="quotation.create.label.rotdirection"/>
			        </td>
                    <td>
                    	<bean:define name="enquiryForm" property="rotDirectionList"
	                	             id="rotDirectionList" type="java.util.Collection" /> 
                    	<html:select property="directionOfRotId" styleClass="rating-box" tabindex="16">
                    		<option value="0"><bean:message key="quotation.option.select"/></option>
	                        <html:options name="createRequestForm" collection="rotDirectionList"
			 				              property="key" labelProperty="value" />
        	            </html:select>
        	        </td>
                   
                  </tr>
			      <tr>
			      <td>&nbsp;</td>
			      	<td>&nbsp; </td>
			      	
			        <td  class="formLabelrating">
			        	<bean:message key="quotation.create.label.shaftext"/>
			        </td>
                    <td>
                    	<bean:define name="enquiryForm" property="shaftList"
	                	             id="shaftList" type="java.util.Collection" /> 
                    	<html:select property="shaftExtId" styleClass="rating-box" tabindex="17">
                    		<option value="0"><bean:message key="quotation.option.select"/></option>
	                        <html:options name="createRequestForm" collection="shaftList"
			 				              property="key" labelProperty="value" />
        	            </html:select>
        	        </td>
        	        </tr>
        	        <tr>
        	         <td>&nbsp;</td>
			      	<td>&nbsp; </td>
                    <td class="formLabelrating">
                    	<bean:message key="quotation.create.label.paint"/>
                    </td>
                    <td>
                    	<html:text property="paint"  styleClass="rating-box" maxlength="10" tabindex="33"/>
                    </td>
                  </tr>
                  
                  
			      <tr>
			      <td>&nbsp;</td>
			      	<td>&nbsp; </td>
			      	 <td class="formLabelrating"><bean:message key="quotation.create.label.ratingunitprice"/></td>
                    <td>
                    	<html:text property="ratingUnitPrice"  styleClass="rating-box" maxlength="8" onkeypress="return isNumber(event)" />                      		
                    </td>
 
  </tr>
        	            <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
			      <tr>
			        <td  class="formLabelrating">
			        	<bean:message key="quotation.create.label.specialfeatures"/>
			        </td>
                    <td  colspan="4">
                     <html:textarea property="splFeatures"  styleId="splFeatures" style="width:600px; height:50px"  onkeyup="checkMaxLenghtText(this, 2500);" onkeydown="checkMaxLenghtText(this, 2500);" tabindex="35"/>
                    
                    </td>
                  </tr>
                  
			      </table>
		    </fieldset>
		    
		    <!-- Field Section Ending -->
 		    		    <fieldset >
 		          <legend class="blueheader">	
			          	<bean:message key="quotation.create.label.notesection"/>
			          </legend>
 		    		    
			     <div  >
				  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr><td colspan="5">&nbsp;</td></tr>
			      				  <tr style="float:left; width:100%;">				  
				    <td class="formLabelrating" style="width:138px; margin-right: 33px;">
				    	<bean:message key="quotation.create.label.smnote"/>
				    </td>
                    <td colspan="4">
                    	<html:textarea  property="smnote" styleId = "smnote"  style="width:600px; height:50px" onkeyup="checkMaxLenghtText(this, 4000);" onkeydown="checkMaxLenghtText(this, 4000);" tabindex="35"/>   </td>
                    	<td></td>	
                    <td></td>
                  </tr>
		    </table>
		    </div>
		    </fieldset>
		    

		    <logic:equal name="enquiryForm" property="dispatch" value="edit">
		   <logic:equal name="enquiryForm" property="returnstatement" value="RETURNDM">
		   <!-- Return Statement From DM Starts  -->
            <logic:notEqual name="enquiryForm" property="dmToSmReturn" value="">
		    
		    <fieldset >
 		          <legend class="blueheader">	
			          	<bean:message key="quotation.create.label.commentsection"/>
			          </legend>
 		    		    
			     <div>
			     
				  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr><td colspan="5" >&nbsp;</td></tr>
			      				  <tr style="float:left; width:100%;">				  
				    <td  style="width:138px; margin-right: 33px;">
				    
				    	<bean:message key="quotation.create.label.dmcomment"/>
				    	
				    </td>
				    
                    <td colspan="4" >
                    <font color="red">
						<bean:write name="enquiryForm" property="dmToSmReturn" />
						<input type="hidden" name="dmcomment" id="dmcomment" value="${enquiryForm.dmToSmReturn }"/>
                    </font>	                		         	
                    </td>
                    	<td></td>	
                    <td></td>
                  </tr>
		    </table>
		    </div>
		    </fieldset>
		    </logic:notEqual>
		    <!-- Return Statement From Dm End -->
		<!-- Return Statement From DE Starts  -->
            <logic:notEqual name="enquiryForm" property="deToSmReturn" value="">
		    
		    <fieldset >
 		          <legend class="blueheader">	
			          	<bean:message key="quotation.create.label.commentsection"/>
			          </legend>
 		    		    
			     <div>
			     
				  <table width="100%" border="0" cellspacing="0" cellpadding="0">
				  <tr><td colspan="5" >&nbsp;</td></tr>
			      				  <tr style="float:left; width:100%;">				  
				    <td  style="width:138px; margin-right: 33px;">
				    
				    	<bean:message key="quotation.create.label.decomment"/>
				    	
				    </td>
				    
                    <td colspan="4" >
                    <font color="red">
						<bean:write name="enquiryForm" property="deToSmReturn" />
						<input type="hidden" name="decomment" id="decomment" value="${enquiryForm.deToSmReturn }"/>
                    </font>	                		         	
                    </td>
                    	<td></td>	
                    <td></td>
                  </tr>
		    </table>
		    </div>
		    </fieldset>
		    </logic:notEqual>
		    <!-- Return Statement From DE End -->
		    
		    </logic:equal>
		    </logic:equal>
		    
		    <!--Comment Section Ends -->
 			 <!-------------------------- Bottum Input buttons block Start  --------------------------------->
			<br>
        <div class="blue-light" align="right">
        <logic:notEqual name="enquiryForm" property="dispatch" value="edit">
          <html:button property="" styleClass="BUTTON" value="Save As Draft" onclick="javascript:saveAsDraft('DRAFT', '');"/> 
          <logic:notEqual name="enquiryForm" property="isMaxRating" value="true">

	          <html:button property="" styleClass="BUTTON" value="Save &amp; Add New Rating" onclick="javascript:saveAsDraft('ADDNEWRATING', '');"/>  
	      </logic:notEqual>
           
         </logic:notEqual>   

         <logic:equal name="enquiryForm" property="dispatch" value="edit">
         <logic:equal name="enquiryForm" property="returnstatement" value="RETURNDM">
         <html:button property="" styleClass="BUTTON" value="Save" onclick="javascript:saveAsDraft('SAVEONLY_RETURNDM', '');"/>
         </logic:equal>
         <logic:equal name="enquiryForm" property="returnstatement" value="edit">
         	<html:button property="" styleClass="BUTTON" value="Save" onclick="javascript:saveAsDraft('SAVEONLY', '');"/> 
         </logic:equal>	
         	<logic:notEqual name="enquiryForm" property="isLast" value="true">
         	<logic:equal name="enquiryForm" property="returnstatement" value="RETURNDM">
         	<html:button property="" styleClass="BUTTON" value="Save &amp; Continue For Next Rating" onclick="javascript:saveAsDraft('NEXTRATING_RETURNDM', '');"/>
         	</logic:equal>
         	
         	<logic:equal name="enquiryForm" property="returnstatement" value="edit">
 				<html:button property="" styleClass="BUTTON" value="Save &amp; Continue For Next Rating" onclick="javascript:saveAsDraft('NEXTRATING', '');"/>          		
         	</logic:equal>
         	</logic:notEqual>         		
         	<logic:equal name="enquiryForm" property="isLast" value="true">
         		<logic:notEqual name="enquiryForm" property="isMaxRating" value="true">
         		<logic:equal name="enquiryForm" property="returnstatement" value="RETURNDM">
         		<html:button property="" styleClass="BUTTON" value="Save &amp; Add New Rating" onclick="javascript:saveAsDraft('ADDNEWRATING_RETURNDM', '');"/>
         		</logic:equal>
	         		<logic:equal name="enquiryForm" property="returnstatement" value="edit">

	         		<html:button property="" styleClass="BUTTON" value="Save &amp; Add New Rating" onclick="javascript:saveAsDraft('ADDNEWRATING', '');"/>  
	         	</logic:equal>
	         	</logic:notEqual>
         	</logic:equal>
         	
         </logic:equal> 
         <!-- Last Submit Buttons Based on Conditions  -->    
                  <logic:equal name="enquiryForm" property="dispatch" value="edit">
         
                                    <logic:equal name="enquiryForm" property="returnstatement" value="RETURNDM">
                                             <html:button property="" styleClass="BUTTON" value="Submit" onclick="javascript:submitEnquiry('RETURN');"/>
                                    
                		         	</logic:equal>
                		         	 <logic:equal name="enquiryForm" property="returnstatement" value="edit">

                		         	         <html:button property="" styleClass="BUTTON" value="Submit" onclick="javascript:submitEnquiry('SUBMIT');"/>
                		         	</logic:equal>
                </logic:equal>
                		         	
         <logic:notEqual name="enquiryForm" property="dispatch" value="edit">   
                     <!-- Inside Dispatch Not Edit -->
         <html:button property="" styleClass="BUTTON" value="Submit" onclick="javascript:submitEnquiry('SUBMIT');"/>
         </logic:notEqual>
         </div>
		   <!-------------------------- Bottum Input buttons block End  --------------------------------->
         </td>
  </tr>
</table>
<!-------------------------- main table End  --------------------------------->
</html:form>

<ajax:autocomplete
		fieldId="customerName"
		popupId="customer-popup"
		targetId="customerId"
		baseUrl="servlet/AutocompleteServlet?type=placeenquiry&valueType=id"
		paramName="customer"
		className="autocomplete"
		progressStyle="throbbing" 
		postFunc="postAutocomplete"/>
		
		 

	<iframe
		 id="divShim"
		src="javascript:false;"
		scrolling="no"
		frameborder="0"
		style="position:absolute; top:0px; left:0px; display:none;">
	</iframe>
	
	
	
	
	<script>
	
	 
	
	
	$(document).ready(function () { 
		
	 
	$("#orderClosingMonth").datepicker({
	    dateFormat: 'dd-mm-yy',
	    onSelect: function (selectedDate) {
	        if (this.id == 'orderClosingMonth') {
	            var arr = selectedDate.split("-");
	            var date = new Date(arr[2]+"-"+arr[1]+"-"+arr[0]);
	            var d = date.getDate();
	            var m = date.getMonth();
	            var y = date.getFullYear();
	            var minDate = new Date(y, m + 3, d);

	        }
	    }
	});
	var expectedDeliveryMonth=document.getElementById("expectedDelivery").value;
	var advancePayment=document.getElementById("advancePayment").value;
	var paymentTerms=document.getElementById("paymentTerms").value;
	
	
	var warrantyDispatchDate=document.getElementById("warrantyDispatch").value;
	var warrantyCommissioningDate=document.getElementById("warrantyCommissioning").value;
	
	if(expectedDeliveryMonth!="")
	{
		document.getElementById("expectedDeliveryMonth").value=expectedDeliveryMonth;
	}

	if(warrantyDispatchDate!="")
	{
		document.getElementById("warrantyDispatchDate").value=warrantyDispatchDate;
	}
	if(warrantyCommissioningDate!="")
	{
		document.getElementById("warrantyCommissioningDate").value=warrantyCommissioningDate;
	}

	
	if(advancePayment=="")
	{
		document.getElementById("advancePayment").value=20;
	}
	if(paymentTerms=="")
	{
		document.getElementById("paymentTerms").value=80;
	}
	

	
		

	});
	
	
	
	
	</script>