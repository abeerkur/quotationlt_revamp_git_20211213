<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants" %>
<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>
<%@page import="java.text.DecimalFormat" %>
<%
DecimalFormat decimalFormat = new DecimalFormat("#0");
%>



<html:form action="/viewEnquiry" method="POST">
<html:hidden property="invoke" value="updateFactor"/>

<html:hidden property="enquiryId" />
<html:hidden property="enquiryNumber" />
<html:hidden property="createdBySSO" />
<html:hidden property="createdBy" />
<html:hidden property="pagechecking" styleId="pagechecking" />
<html:hidden property="operationType" value="RSM" />
<input type="hidden" id="ratingMaxSize" name="ratingMaxSize"/>
<input type="hidden" id="addonMaxSize" name="addonMaxSize"/>

<input type="hidden" name='<%= org.apache.struts.taglib.html.Constants.TOKEN_KEY %>'
         value='<%= session.getAttribute(org.apache.struts.action.Action.TRANSACTION_TOKEN_KEY) %>' />


<logic:present name="enquiryDto" scope="request">
<table width="80%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
<tr>
  <td height="100%" valign="top"> 

<div id="estimate">
			<fieldset>
				<legend class="blueheader">
					<bean:message key="quotation.viewenquiry.estimate.label.heading"/>
				</legend>
	  <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">

				<input type="hidden" name="commonMultiplierCount" id="commonMultiplierCount">




<tr><td><table width="100%" border="0" cellspacing="0" cellpadding="0">

<tbody><tr>
<td>

<div class="tablerow">
  					<div class="tablecolumn" >
  					<table width="100%" border="0" cellspacing="0" cellpadding="2">
                 	<tr>
                    <td colspan="2" >&nbsp;</td>
                    </tr>
                    
                    <tr>
                      <td class="label-text"  style="width:50%">
					  <bean:message key="quotation.create.label.customer"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                       <bean:write name="enquiryDto" property="customerName"/>
                       </td>
                    
                    </tr>
                      <tr>
                      <td class="label-text"  style="width:50%">
					  <bean:message key="quotation.create.label.customertype"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                       <bean:write name="enquiryDto" property="customerType"/>
                       </td>
                    
                    </tr>
                    
                      <tr>
                      <td class="label-text"  style="width:50%">
					  <bean:message key="quotation.create.label.enduser"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                       <bean:write name="enquiryDto" property="endUser"/>
                       </td>
                    
                    </tr>
                      <tr>
                      <td class="label-text"  style="width:50%">
					  <bean:message key="quotation.create.label.warrantydispatchdates"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
					 <bean:write name="enquiryDto" property="warrantyDispatchDate"/>&nbsp;
					 <bean:message key="quotation.create.label.months"/>
					 <input type="hidden" name="warrantyAfterDispatchDate" id="warrantyAfterDispatchDate" 
					 class="short" maxlength="4" readonly="readonly" 
					 value="<bean:write name="enquiryDto" property="warrantyDispatchMultiplier"   />">
                       </td>
                    
                    </tr>
                    
                        <tr>
                      <td class="label-text"  style="width:50%">
					  <bean:message key="quotation.create.label.warrantycommissioningdates"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                    <bean:write name="enquiryDto" property="warrantyCommissioningDate"/>&nbsp;<bean:message key="quotation.create.label.months"/>
					 <input type="hidden" name="warrantyAfterCommissionDate" id="warrantyAfterCommissionDate" class="short" maxlength="4" readonly="readonly" value="<bean:write name="enquiryDto" property="warrantycommissioningMultiplier"   />">
                       </td>
                    
                    </tr>
                    
                    
                    </table>
                </div>
  				   <div class="tablecolumn" >
  				   <table width="100%" border="0" cellspacing="0" cellpadding="2">
                 	<tr>
                    <td colspan="2" >&nbsp;</td>
                    </tr>

                    <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.advancepayment"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
                      <bean:write name="enquiryDto" property="advancePayment"/>
                      <logic:notEqual name="enquiryDto" property="advancePayment" value="">
						%
					  </logic:notEqual>
                      
                      </td>    
                    
                    </tr>
                    
                      <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.paymentterms"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
                      <bean:write name="enquiryDto" property="paymentTerms"/>
                      <logic:notEqual name="enquiryDto" property="paymentTerms" value="">
						% <bean:message key="quotation.create.label.beforedispatch"/>
					  </logic:notEqual>
                      
                      </td>    
                    
                    </tr>
                    
                                          <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.deliveriesdate"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
                      <bean:write name="enquiryDto" property="expectedDeliveryMonth"/>
                      </td>    
                    
                    </tr>
                      <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.finance"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
					  &nbsp;
                      </td>    
                    
                    </tr>
                    
                   </table>
                     </div>
				</div>






</td>
</tr>
</tbody>
</table>
</td>
</tr>





				
				
					<%String finalStr=""; %>		
						<logic:notEmpty name="enquiryDto" property="ratingObjects">
         <logic:iterate name="enquiryDto" property="ratingObjects"
								               id="ratingObject" indexId="idx" 
								               type="in.com.rbc.quotation.enquiry.dto.RatingDto">
<tr><td colspan="2" align="left">
<legend class="blueheader">
<bean:message key="quotation.viewenquiry.technical.label.heading"/> 
<%= idx.intValue()+1 %> 
</legend>
</td></tr>
<%
  if(finalStr=="")
  {
	  finalStr=String.valueOf(ratingObject.getRatingId());
  }
  else
  {
	  finalStr=finalStr+","+String.valueOf(ratingObject.getRatingId());
  }

%>
<bean:define id="currentratingNo" name="ratingObject" property="ratingNo"/> 


<%String MCUnit="",AreaClassification="",frameSize="",transferCMultiplier="",quantityMultiplier="",
mcornspratioMultiplier_rsm="",transportationperunit_rsm="",warrantyperunit_rsm="",unitpriceMultiplier_rsm="",totalpriceMultiplier_rsms=""; %>
   				<logic:notEmpty name="enquiryDto" property="ratingObjects">
		         <logic:iterate name="enquiryDto" property="ratingObjects"
								               id="ratingObject1" indexId="idsx1" 
								               type="in.com.rbc.quotation.enquiry.dto.RatingDto">	
  
  				 <bean:define id="technicalObject1" name="ratingObject1" property="technicalDto"/>
  				 <%
  				 
  				 if(idx.intValue()==idsx1.intValue()){ %>
  				
				 <%--TODO Pass the Flow From CE TO RSM
				   <bean:define id="tech1" name="technicalObject1" property="transferCMultiplier"/> 
				 --%>
				 <!--TODO BiPass the Flow From DM TO RSM  -->
				 <bean:define id="tech1" name="technicalObject1" property="formattedCommercialMCUnit"/>
  				 <bean:define id="tech2" name="technicalObject1" property="areaClassification"/>
  				 <bean:define id="tech3" name="technicalObject1" property="frameSize"/>
  				 <bean:define id="tech4" name="technicalObject1" property="quantityMultiplier"/>
  				
  				<bean:define id="tech5" name="technicalObject1" property="mcornspratioMultiplier_rsm"/>
  				<bean:define id="tech6" name="technicalObject1" property="transportationperunit_rsm"/>
  				<bean:define id="tech7" name="technicalObject1" property="warrantyperunit_rsm"/>
  				<bean:define id="tech8" name="technicalObject1" property="unitpriceMultiplier_rsm"/>
  				<bean:define id="tech9" name="technicalObject1" property="totalpriceMultiplier_rsm"/> 
  				 <%
  				 
  				transferCMultiplier=String.valueOf(tech1); 
  				
  				
  				AreaClassification=String.valueOf(tech2);
  				frameSize=String.valueOf(tech3);
  				quantityMultiplier=String.valueOf(tech4);
  				
  				mcornspratioMultiplier_rsm=String.valueOf(tech5);
  				transportationperunit_rsm=String.valueOf(tech6);
  				warrantyperunit_rsm=String.valueOf(tech7);
  				unitpriceMultiplier_rsm=String.valueOf(tech8);
  				totalpriceMultiplier_rsms=String.valueOf(tech9);
  				
  				
  				
  				 } %>


  				 </logic:iterate>
  				 </logic:notEmpty>	


<!--TODO Bipass The Flow From DM TO RSM START  -->

<%
if(transferCMultiplier!="" && transferCMultiplier!=null && transferCMultiplier!="0")
{
	transferCMultiplier=transferCMultiplier.trim().replaceAll("INR", "");
	transferCMultiplier=transferCMultiplier.replaceAll("\\s", "");
	transferCMultiplier=transferCMultiplier.replaceAll(",", "");
	
	transferCMultiplier=decimalFormat.format(Double.parseDouble(transferCMultiplier));
	
}
else
{
	transferCMultiplier="0";	
}
%>
<!--TODO Bipass The Flow From DM TO RSM ENDS  -->



 
 



<tr><td><table width="100%" border="0" cellspacing="0" cellpadding="0">

<tbody><tr>
<td>



                  <div class="tablerow">
  					<div class="tablecolumn" >
  					<table width="100%" border="0" cellspacing="0" cellpadding="2">
                 	<tr>
                    <td colspan="2" >&nbsp;</td>
                    </tr>
                    
                    <tr>
                     <td class="label-text"  style="width:50%">
                      <bean:message key="quotation.create.label.quantityrequired"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                       <bean:write name="ratingObject" property="quantity"/>
                       <input type="hidden" name="quantitymultiplier_${ratingObject.ratingId}" id="quantitymultiplier_${ratingObject.ratingId}" value="<bean:write name="ratingObject" property="quantity"/>">
                      </td>
                    
                    </tr>
                   

                    <tr>
                      <td class="label-text"  style="width:50%">
                      	<bean:message key="quotation.create.label.kw"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                       	<bean:write name="ratingObject" property="kw"/>
                       </td>
                    
                    </tr>


  					<tr>
  	                  <td class="label-text"  style="width:50%">
                      	<bean:message key="quotation.create.label.volts"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                       <bean:write name="ratingObject" property="volts"/>

						<logic:notEqual name="ratingObject" property="volts" value="">
						<bean:message key="quotation.units.volts"/>
						+/-
						</logic:notEqual>
						<logic:notEqual name="ratingObject" property="voltsVar" value="">
						<bean:write name="ratingObject" property="voltsVar"/>
						%
						</logic:notEqual>
                       </td>
  					
  					</tr>
  					
  				     <tr>
                      <td class="label-text"  style="width:50%">
                      	<bean:message key="quotation.create.label.pole"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                       	<bean:write name="ratingObject" property="poleName"/>
                       </td>
                    
                    </tr>
                     <tr>
                      <td class="label-text"  style="width:50%">
                      	<bean:message key="quotation.offer.label.areaclassification"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                       	<%=AreaClassification %>
                       </td>
                    
                    </tr>
                    
                     <tr>
                      <td class="label-text"  style="width:50%">
                      	<bean:message key="quotation.offerdata.technical.label.framesize"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                       	<%=frameSize %>
                       </td>
                    
                    </tr>

                     <tr>
                      <td class="label-text"  style="width:50%">
                      	<bean:message key="quotation.create.label.scsr"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                       	<bean:write name="ratingObject" property="scsrName"/>
                       </td>
                    
                    </tr>
                    
                                    <tr>
                      <td class="label-text"  style="width:50%">
                      	<bean:message key="quotation.create.label.application"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:50%">
                       	<bean:write name="ratingObject" property="applicationName"/>
                       </td>
                    
                    </tr>
                    

					</table>
			        </div>
  				   <div class="tablecolumn" >
  				   <table width="100%" border="0" cellspacing="0" cellpadding="2">
                 	<tr>
                    <td colspan="2" >&nbsp;</td>
                    </tr>

                    <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.listprice"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
                       <input type="text" name="transferCMultiplier_${ratingObject.ratingId}" 
                       id="transferCMultiplier_${ratingObject.ratingId}" size="10" maxlength="10" 
                       value="<%=transferCMultiplier %>" onblur="return calculateMultipliersForRSM('${ratingObject.ratingId}')" >&nbsp;INR
                      </td>    
                    
                    </tr>
                    
                    <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.addonpercent"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
                       <input type="text" name="transportationperunit_rsm_${ratingObject.ratingId}" 
                       id="transportationperunit_rsm_${ratingObject.ratingId}" size="10" maxlength="10" 
                       value="<%=transportationperunit_rsm %>" onblur="return calculateMultipliersForRSM('${ratingObject.ratingId}')" >&nbsp;% 
                      </td>    
                    
                    </tr>
                    
                    <tr>
                     <td class="label-text" style="width:50% ">
                      <bean:message key="quotation.create.label.addonvalue"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
                       <input type="text" name="warrantyperunit_rsm_${ratingObject.ratingId}" 
                       id="warrantyperunit_rsm_${ratingObject.ratingId}" size="10" maxlength="10" 
                       value="<%=warrantyperunit_rsm %>" onblur="return calculateMultipliersForRSM('${ratingObject.ratingId}')" >&nbsp;INR  
                      </td>    
                    
                    </tr>
                    

                    <tr>
                     <td class="label-text" style="width:50% ">
					<bean:message key="quotation.create.label.discount"/>&nbsp;<span class="mandatory"></span>
                      <td class="formContentview-rating-b0x" style="width:50% ">
                       <%-- <bean:write name="enquiryDto" property="enquiryReferenceNumber"/> --%>
                       <input type="text" name="mcornspratioMultiplier_${ratingObject.ratingId}" id="mcornspratioMultiplier_${ratingObject.ratingId}" size="10" maxlength="4" value="<%=mcornspratioMultiplier_rsm %>" onkeypress="return validateFloatKeyPress(this,event);"  onblur="return calculateMultipliersForRSM('${ratingObject.ratingId}')"   />&nbsp;%
                      </td>    
                    
                    </tr>


                   <%--TODO  <tr>
                    
                      <td class="label-text" style="width:50% ">
						<bean:message key="quotation.create.label.transportaionperunit"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
						<input type="text" name="transportationperunit_rsm_${ratingObject.ratingId}" 
						id="transportationperunit_rsm_${ratingObject.ratingId}" 
						size="10" maxlength="9" value="<%=transportationperunit_rsm %>" 
						onkeypress="return isNumber(event)" 
						onblur="return calculateMultipliersForRSM('${ratingObject.ratingId}')"  />&nbsp;INR                      </td>    
                    
                    </tr>
                    
                     <tr>
                      <td class="label-text" style="width:50% ">
						<bean:message key="quotation.create.label.warrantyperunit"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
						<input type="text" name="warrantyperunit_rsm_${ratingObject.ratingId}" id="warrantyperunit_rsm_${ratingObject.ratingId}" size="10" 
						maxlength="9" value="<%=warrantyperunit_rsm %>" onkeypress="return isNumber(event)"  
						onblur="return calculateMultipliersForRSM('${ratingObject.ratingId}')"  />&nbsp;INR                    
                    </tr> --%>
                    
                       <tr>
                      <td class="label-text" style="width:50% ">
						<bean:message key="quotation.create.label.unitprice"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
					<input type="text" name="unitpriceMultiplier_rsm_${ratingObject.ratingId}" 
					id="unitpriceMultiplier_rsm_${ratingObject.ratingId}"  size="10" maxlength="100"  
					value="<%=unitpriceMultiplier_rsm %>"  readonly="readonly"  />&nbsp;INR 
					</td>
					</tr>
					
					<logic:notEmpty name="enquiryForm" property="previousratingList">
              <logic:iterate name="enquiryForm" property="previousratingList"
               id="previousratingListData" indexId="idx2"
               type="in.com.rbc.quotation.common.vo.KeyValueVo">
               <bean:define id="previousRatingNumber" name="previousratingListData" property="ratingNumber" />
                <%
               
               if(String.valueOf(currentratingNo).equalsIgnoreCase(String.valueOf(previousRatingNumber))){
            	   

            	   %>
			
			
			        <tr style="color: red;">
                    <td class="label-text" style="width:50% ">
					<bean:message key="quotation.create.label.previousunitprice"/><br/>
					 <bean:write name="previousratingListData" property="enquiryNumber"/>&nbsp;::
                    </td>
                    <td class="formContentview-rating-b0x" style="width:50% ">
					<br/>
					<bean:write name="previousratingListData" property="unitPriceRsm"/>&nbsp;INR</td>
					</tr>
			
            	   
               <%} %>
</logic:iterate>
</logic:notEmpty>

					
                    
                      <tr>
                      <td class="label-text" style="width:50% ">
						<bean:message key="quotation.create.label.totalprice"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:50% ">
					<input type="text" name="totalpriceMultiplier_rsm_${ratingObject.ratingId}" 
					id="totalpriceMultiplier_rsm_${ratingObject.ratingId}"  size="10" maxlength="100"  
					value="<%=totalpriceMultiplier_rsms %>"  readonly="readonly"  />&nbsp;INR
					</td>
					</tr>

                    
  					
					</table>
				  </div>
				</div> 
</td>
</tr>






				
				
				
								               
				
</tbody>
</table>
				
				</td>
	</tr>
	</logic:iterate>
	</logic:notEmpty>
</table>

					<input type="hidden" name="finalRatingList" id="finalRatingList" value="<%=finalStr %>">
			


			<fieldset style="width:100%">
						       <legend class="blueheader">	
			          	<bean:message key="quotation.create.label.notesection"/>
			          </legend>
			
			     <div >
				  <table  border="0" cellspacing="0" cellpadding="0">
				  <tr><td colspan="4">&nbsp;</td></tr>
			      <tr style="float:left; width:100%;">				  
				    <td class="formLabelrating" style="width:138px; margin-right: 33px;">
				    	<bean:message key="quotation.create.label.smnote"/>
				    </td>
                    <td colspan="3" class="formLabelrating">

                    <logic:notEqual name="enquiryForm" property="smnote"  value="">
                    
                    <html:textarea  property="smnote" readonly="true"  style="width:600px; height:50px"  tabindex="35"/>
                      &nbsp; By &nbsp;  <bean:write name="enquiryForm" property="sm_name"/>
                    
                    </logic:notEqual>
                    
                  </tr>
                  <tr style="float:left; width:100%;">				  
				    <td class="formLabelrating" style="width:138px; margin-right: 33px;">
				    	<bean:message key="quotation.create.label.dmnote"/>
				    </td>
                    <td colspan="3" class="formLabelrating">

                    <logic:notEqual name="enquiryForm" property="dmnote"  value="">
                    
                      <html:textarea  property="dmnote" readonly="true"  style="width:600px; height:50px"  tabindex="35"/>
                      &nbsp; By &nbsp;  <bean:write name="enquiryForm" property="dm_name"/>
                    
                    
                    </logic:notEqual>
                    
                  </tr>
                  <tr style="float:left; width:100%;">				  
				    <td class="formLabelrating" style="width:138px; margin-right: 33px;">
				    	<bean:message key="quotation.create.label.cenote"/>
				    </td>
                    <td colspan="3" class="formLabelrating">

                    <logic:notEqual name="enquiryForm" property="cenote"  value="">
                      <html:textarea  property="cenote" readonly="true"  style="width:600px; height:50px"  tabindex="35"/>
                      &nbsp; By &nbsp;  <bean:write name="enquiryForm" property="ce_name"/>
                    
                    
                    </logic:notEqual>
                    
                  </tr>
                  
                  
                  <tr><td colspan="3">&nbsp;</td></tr>
			       <tr style="float:left; width:100%;">				  
				    <td class="formLabelrating" style="width:138px; margin-right: 33px;">
				    	<bean:message key="quotation.create.label.rsmnote"/>
				    </td>
                    <td colspan="4">
                    	<html:textarea  property="rsmnote" styleId = "rsmnote"  style="width:600px; height:50px" onkeyup="checkMaxLenghtText(this, 4000);" onkeydown="checkMaxLenghtText(this, 4000);" tabindex="35"/>   </td>
                  </tr>
                   <logic:notEqual name="enquiryForm" property="quotationtoRsmReturn" value="">
                   <tr style="float:left; width:100%;color:red;"  >				  
				    <td class="formLabelrating"  style="width:138px; margin-right: 33px;">
				    	<bean:message key="quotation.create.label.smcomment"/>
				    </td>
                    <td class="formLabelrating"  >
                   <bean:write name="enquiryForm" property="quotationtoRsmReturn" />
                  </tr>
                  </logic:notEqual>
                  
 		    </table>
		    </div>
		    </fieldset>
		    
			  

 	
				
				
			<div class="blue-light" align="right">      
			   <input type="button" class="BUTTON" onClick="revision()"  value="Return Enquiry">                       
               <input type="button" class="BUTTON" onclick="javascript:submitEstimateForRSM();" value="Submit" />
            </div>
	</fieldset>	
	</div>

           

			<!-----------------------input buttons end----------------------------> 
			<div id="return" class="showcontent" > <!--  class="showcontent"-->
			<fieldset>
				<legend class="blueheader">
					<bean:message key="quotation.refactor.return.label.heading2"/>
				</legend>
				<table  align="center" >                  
                  <tr>
                    <td class="formLabel">
                    	<bean:message key="quotation.viewenquiry.return.label.comments"/>
					</td>
                    <td>
                    	<html:textarea property="returnToRemarks"/>
                    </td>
                  </tr>
                </table>
			</fieldset>
			<div class="blue-light" align="right">                           
              <input name="cancel" type="button" class="BUTTON" onClick="estimate()" value="Cancel">
              <input name="new" type="button" class="BUTTON" onClick="javascript:returnEnquiry();" value="Return"> 
            </div>
			</div>
  </td>
</tr>
</table><br><br>
</logic:present>
</html:form>
