<!-- Commercial Terms and Conditions : Section : Start -->

			<table width="100%">
				<tr>
					<td style="vertical-align: top">
						<table width="100%">
							<tr>
								<label class="blue-light" style="font-weight: bold;"> Commercial Terms and Conditions </label>
							</tr>
						</table>
												
						<table id="table0" width="100%" align="center" cellspacing="0" cellpadding="2">
							<tr id="top">
								<td class="bordertd" style="width:15% !important; text-align:center;" >Payment Terms <span class="mandatory">&nbsp;</span> </td>
								<td class="bordertd" style="width:15% !important; text-align:center;" >%age amount of Nett. Order value </td>
								<td class="bordertd" style="width:15% !important; text-align:center;" >Payment Days </td>
								<td class="bordertd" style="width:35% !important; text-align:center;" >Payable Terms </td>
								<td class="bordertd" style="width:15% !important; text-align:center;" >Instrument </td>
							</tr>
							
							<bean:define name="newenquiryForm" property="tcPercentNetOrderValueList" id="tcPercentNetOrderValueList" type="java.util.Collection" />
							<bean:define name="newenquiryForm" property="tcPaymentDaysList" id="tcPaymentDaysList" type="java.util.Collection" />
							<bean:define name="newenquiryForm" property="tcPayableTermsList" id="tcPayableTermsList" type="java.util.Collection" />
							<bean:define name="newenquiryForm" property="tcInstrumentList" id="tcInstrumentList" type="java.util.Collection" />
							
							<tr class="light" id="item0">
								<td class="bordertd" style="width:15% !important;"> <strong> Advance Payment 1 </strong> </td>
								<td class="bordertd" style="width:15% !important;">
									<html:select property="pt1PercentAmtNetorderValue" styleId="pt1PercentAmtNetorderValue" style="width:145px !important;" onchange="javascript:validateOrderValue('pt1PercentAmtNetorderValue');">
										<option value="0">Select</option>
										<html:options name="newenquiryForm" collection="tcPercentNetOrderValueList" property="key" labelProperty="value" />
									</html:select>
								</td>
								<td class="bordertd" style="width:15% !important;">
									<html:select property="pt1PaymentDays" styleId="pt1PaymentDays" style="width:145px !important;" onchange="javascript:validatePaymentDays('pt1PaymentDays');">
										<option value="0" selected>Select</option>
										<html:options name="newenquiryForm" collection="tcPaymentDaysList" property="key" labelProperty="value" />
									</html:select>
								</td>
								<td class="bordertd" style="width:35% !important;">
									<html:select property="pt1PayableTerms" styleId="pt1PayableTerms" style="width:380px !important;">
										<option value="0" selected>Select</option>
										<html:options name="newenquiryForm" collection="tcPayableTermsList" property="key" labelProperty="value" />
									</html:select>
								</td>
								<td class="bordertd" style="width:15% !important;">
									<html:select property="pt1Instrument" styleId="pt1Instrument" style="width:150px !important;">
										<option value="0" selected>Select</option>
										<html:options name="newenquiryForm" collection="tcInstrumentList" property="key" labelProperty="value" />
									</html:select>
								</td>
							</tr>
							<tr class="dark" id="item1">
								<td class="bordertd" style="width:15% !important;"> <strong> Advance Payment 2 </strong> </td>
								<td class="bordertd" style="width:15% !important;">
									<html:select property="pt2PercentAmtNetorderValue" styleId="pt2PercentAmtNetorderValue" style="width:145px !important;" onchange="javascript:validateOrderValue('pt2PercentAmtNetorderValue');">
										<option value="0">Select</option>
										<html:options name="newenquiryForm" collection="tcPercentNetOrderValueList" property="key" labelProperty="value" />
									</html:select>
								</td>
								<td class="bordertd" style="width:15% !important;">
									<html:select property="pt2PaymentDays" styleId="pt2PaymentDays" style="width:145px !important;" onchange="javascript:validatePaymentDays('pt2PaymentDays');">
										<option value="0" selected>Select</option>
										<html:options name="newenquiryForm" collection="tcPaymentDaysList" property="key" labelProperty="value" />
									</html:select>
								</td>
								<td class="bordertd" style="width:35% !important;">
									<html:select property="pt2PayableTerms" styleId="pt2PayableTerms" style="width:380px !important;">
										<option value="0" selected>Select</option>
										<html:options name="newenquiryForm" collection="tcPayableTermsList" property="key" labelProperty="value" />
									</html:select>
								</td>
								<td class="bordertd" style="width:15% !important;">
									<html:select property="pt2Instrument" styleId="pt2Instrument" style="width:150px !important;">
										<option value="0" selected>Select</option>
										<html:options name="newenquiryForm" collection="tcInstrumentList" property="key" labelProperty="value" />
									</html:select>
								</td>
							</tr>
							<tr class="light" id="item2">
								<td class="bordertd" style="width:15% !important;"> <strong> Main Payment </strong> </td>
								<td class="bordertd" style="width:15% !important;">
									<html:select property="pt3PercentAmtNetorderValue" styleId="pt3PercentAmtNetorderValue" style="width:145px !important;" onchange="javascript:validateOrderValue('pt3PercentAmtNetorderValue');">
										<option value="0">Select</option>
										<html:options name="newenquiryForm" collection="tcPercentNetOrderValueList" property="key" labelProperty="value" />
									</html:select>
								</td>
								<td class="bordertd" style="width:15% !important;">
									<html:select property="pt3PaymentDays" styleId="pt3PaymentDays" style="width:145px !important;" onchange="javascript:validatePaymentDays('pt3PaymentDays');">
										<option value="0" selected>Select</option>
										<html:options name="newenquiryForm" collection="tcPaymentDaysList" property="key" labelProperty="value" />
									</html:select>
								</td>
								<td class="bordertd" style="width:35% !important;">
									<html:select property="pt3PayableTerms" styleId="pt3PayableTerms" style="width:380px !important;">
										<option value="0" selected>Select</option>
										<html:options name="newenquiryForm" collection="tcPayableTermsList" property="key" labelProperty="value" />
									</html:select>
								</td>
								<td class="bordertd" style="width:15% !important;">
									<html:select property="pt3Instrument" styleId="pt3Instrument" style="width:150px !important;">
										<option value="0" selected>Select</option>
										<html:options name="newenquiryForm" collection="tcInstrumentList" property="key" labelProperty="value" />
									</html:select>
								</td>
							</tr>
							<tr class="dark" id="item3">
								<td class="bordertd" style="width:15% !important;"> <strong> Retention Payment 1 </strong> </td>
								<td class="bordertd" style="width:15% !important;">
									<html:select property="pt4PercentAmtNetorderValue" styleId="pt4PercentAmtNetorderValue" style="width:145px !important;" onchange="javascript:validateOrderValue('pt4PercentAmtNetorderValue');">
										<option value="0">Select</option>
										<html:options name="newenquiryForm" collection="tcPercentNetOrderValueList" property="key" labelProperty="value" />
									</html:select>
								</td>
								<td class="bordertd" style="width:15% !important;">
									<html:select property="pt4PaymentDays" styleId="pt4PaymentDays" style="width:145px !important;">
										<option value="0" selected>Select</option>
										<html:options name="newenquiryForm" collection="tcPaymentDaysList" property="key" labelProperty="value" />
									</html:select>
								</td>
								<td class="bordertd" style="width:35% !important;">
									<html:select property="pt4PayableTerms" styleId="pt4PayableTerms" style="width:380px !important;">
										<option value="0" selected>Select</option>
										<html:options name="newenquiryForm" collection="tcPayableTermsList" property="key" labelProperty="value" />
									</html:select>
								</td>
								<td class="bordertd" style="width:15% !important;">
									<html:select property="pt4Instrument" styleId="pt4Instrument" style="width:150px !important;">
										<option value="0" selected>Select</option>
										<html:options name="newenquiryForm" collection="tcInstrumentList" property="key" labelProperty="value" />
									</html:select>
								</td>
							</tr>
							<tr class="light" id="item3">
								<td class="bordertd" style="width:15% !important;"> <strong> Retention Payment 2 </strong> </td>
								<td class="bordertd" style="width:15% !important;">
									<html:select property="pt5PercentAmtNetorderValue" styleId="pt5PercentAmtNetorderValue" style="width:145px !important;" onchange="javascript:validateOrderValue('pt5PercentAmtNetorderValue');">
										<option value="0">Select</option>
										<html:options name="newenquiryForm" collection="tcPercentNetOrderValueList" property="key" labelProperty="value" />
									</html:select>
								</td>
								<td class="bordertd" style="width:15% !important;">
									<html:select property="pt5PaymentDays" styleId="pt5PaymentDays" style="width:145px !important;">
										<option value="0" selected>Select</option>
										<html:options name="newenquiryForm" collection="tcPaymentDaysList" property="key" labelProperty="value" />
									</html:select>
								</td>
								<td class="bordertd" style="width:35% !important;">
									<html:select property="pt5PayableTerms" styleId="pt5PayableTerms" style="width:380px !important;">
										<option value="0" selected>Select</option>
										<html:options name="newenquiryForm" collection="tcPayableTermsList" property="key" labelProperty="value" />
									</html:select>
								</td>
								<td class="bordertd" style="width:15% !important;">
									<html:select property="pt5Instrument" styleId="pt5Instrument" style="width:150px !important;">
										<option value="0" selected>Select</option>
										<html:options name="newenquiryForm" collection="tcInstrumentList" property="key" labelProperty="value" />
									</html:select>
								</td>
							</tr>
						</table>
						
						<table width="100%">
							<tr>
								<td colspan="6" style="width:100%;"> &nbsp; </td>
							</tr>
							<tr>
								<td class="formLabel" style="width:17%; color:#990000;">Delivery Term <span class="mandatory">&nbsp;</span></td>
								<td style="width:15%; float:left;">
									<bean:define name="newenquiryForm" property="tcDeliveryTypeList" id="tcDeliveryTypeList" type="java.util.Collection" />
									<html:select property="deliveryTerm" styleId="deliveryTerm" style="width:150px">
										<option value="0" selected>Select</option>
										<html:options name="newenquiryForm" collection="tcDeliveryTypeList" property="key" labelProperty="value" />			
									</html:select>
								</td>
								<td class="formLabel" style="width:18%; color:#990000;">Delivery <span class="mandatory">&nbsp;</span> </td>
								<td style="width:14%; float:left;">
									<bean:define name="newenquiryForm" property="tcDeliveryLotList" id="tcDeliveryLotList" type="java.util.Collection" /> 
									<html:select style="width:70px;" styleId="deliveryInFull" property="deliveryInFull">
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<html:options name="newenquiryForm" collection="tcDeliveryLotList" property="key" labelProperty="value" />
									</html:select>
								</td>
								<td class="formLabel" style="width:16%; color:#990000;">Customer Requested Delivery <span class="mandatory">&nbsp;</span> </td>
								<td style="width:19%; float:left;">
									<bean:define name="newenquiryForm" property="tcOrderCompleteLeadTimeList" id="tcOrderCompleteLeadTimeList" type="java.util.Collection" />
									<html:select style="width:160px;" styleId="custRequestedDeliveryTime" property="custRequestedDeliveryTime">
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<html:options name="newenquiryForm" collection="tcOrderCompleteLeadTimeList" property="key" labelProperty="value" />
									</html:select> Weeks								</td>
							</tr>
							<tr>
								<td class="formLabel" style="width:17%; color:#990000;">Warranty from Date of Dispatch </td>
								<td style="width:15%; float:left;">
									<bean:define name="newenquiryForm" property="tcWarrantyDaysList" id="tcWarrantyDaysList" type="java.util.Collection" />
									<html:select style="width:55px;" styleId="warrantyDispatchDate" property="warrantyDispatchDate">
										<html:options name="newenquiryForm" collection="tcWarrantyDaysList" property="key" labelProperty="value" />
									</html:select> Months
								</td>
								<td class="formLabel" style="width:18%; color:#990000;">Warranty from Date of Comissioning </td>
								<td style="width:14%; float:left;">
									<bean:define name="newenquiryForm" property="tcWarrantyDaysList" id="tcWarrantyDaysList" type="java.util.Collection" />
									<html:select style="width:55px;" styleId="warrantyCommissioningDate" property="warrantyCommissioningDate">
										<html:options name="newenquiryForm" collection="tcWarrantyDaysList" property="key" labelProperty="value" />
									</html:select> Months
								</td>
								<td class="formLabel" style="width:16%; color:#990000;">Order Completion Lead Time <span class="mandatory">&nbsp;</span> </td>
								<td style="width:19%; float:left;">
									<bean:define name="newenquiryForm" property="tcOrderCompleteLeadTimeList" id="tcOrderCompleteLeadTimeList" type="java.util.Collection" />
									<html:select style="width:160px;" styleId="orderCompletionLeadTime" property="orderCompletionLeadTime">
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<html:options name="newenquiryForm" collection="tcOrderCompleteLeadTimeList" property="key" labelProperty="value" />
									</html:select> Weeks
							</tr>
							<tr>
								<td class="formLabel" style="width:17%; color:#990000;">GST <span class="mandatory">&nbsp;</span></td>
								<td style="width:15%; float:left;">
									<bean:define name="newenquiryForm" property="tcGstValuesList" id="tcGstValuesList" type="java.util.Collection" />
									<html:select style="width:68px;" styleId="gstValue" property="gstValue">
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<html:options name="newenquiryForm" collection="tcGstValuesList" property="key" labelProperty="value" />
									</html:select>
								</td>
								<td class="formLabel" style="width:18%; color:#990000;">Packaging <span class="mandatory">&nbsp;</span></td>
								<td style="width:14%; float:left;">
									<bean:define name="newenquiryForm" property="tcPackagingList" id="tcPackagingList" type="java.util.Collection" />
									<html:select property="packaging" styleId="packaging" style="width:140px">
										<html:options name="newenquiryForm" collection="tcPackagingList" property="key" labelProperty="value" />
									</html:select>
								</td>
								<td class="formLabel" style="width:16%; color:#990000;">Offer Validity </td>
								<td style="width:19%; float:left;">
									<bean:define name="newenquiryForm" property="tcOfferValidityList" id="tcOfferValidityList" type="java.util.Collection" />
									<html:select style="width:60px;" styleId="offerValidity" property="offerValidity">
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<html:options name="newenquiryForm" collection="tcOfferValidityList" property="key" labelProperty="value" />
									</html:select> Days
								</td>
							</tr>
							<tr>
								<td class="formLabel" style="width:17%; color:#990000; padding:0px !important;"> Price Validity from PO date </td>
								<td style="width:15%; float:left;">
									<bean:define name="newenquiryForm" property="tcPriceValidityList" id="tcPriceValidityList" type="java.util.Collection" />
									<html:select style="width:50px;" styleId="priceValidity" property="priceValidity">
										<html:options name="newenquiryForm" collection="tcPriceValidityList" property="key" labelProperty="value" />
									</html:select> Months
								</td>
								<td class="formLabel" style="width:18%; color:#990000; padding:0px !important;"> Liquidated Damages due to delayed deliveries  </td>
								<td style="width:30%; float:left;" colspan="3">
									<bean:define name="newenquiryForm" property="tcDeliveryDamagesList" id="tcDeliveryDamagesList" type="java.util.Collection" /> 
									<html:select style="width:390px;" styleId="liquidatedDamagesDueToDeliveryDelay" property="liquidatedDamagesDueToDeliveryDelay">
										<html:options name="newenquiryForm" collection="tcDeliveryDamagesList" property="key" labelProperty="value" />
									</html:select>
								</td>
							</tr>
						</table>
						
						<table width="100%">
							<tr>
								<td class="formLabel" style="width:23%; color:#990000;">Supervision for Erection and Commissioning </td>
								<td style="width:25%; float:left;">
									<bean:define name="newenquiryForm" property="alTargetedList" id="alTargetedList" type="java.util.Collection" /> 
									<html:select style="width:60px;" styleId="supervisionDetails" property="supervisionDetails" onchange="javascript:displaySupervisionDetails()">
										<html:options name="newenquiryForm" collection="alTargetedList" property="key" labelProperty="value" />
									</html:select>
								</td>
							</tr>
						</table>
						<div id="supervisionDetailsDivId" style="display:none">
							<table width="75%" >
								<tr>
									<td style="width:17%; float:left;">&nbsp;</td>
									<td class="formLabel" style="width:12%; line-height:24px; float:left;">No. of Man Days</td>
									<td style="width:40%; float:left;"> <html:text property="superviseNoOfManDays" styleId="superviseNoOfManDays" styleClass="normal" style="width:80px" /> </td>
								</tr>
								<tr>
									<td class="note-red" style="float:left; line-height:24px; white-space:nowrap;" colspan="3"> NOTE: To & Fro Traveling charges, Fooding, Lodging & Local conveyance at Site are at Customer's Account. </td>
								</tr>
								<!--  
								<tr>
									<td style="width:25%; float:left;">&nbsp;</td>
									<td class="formLabel" style="width:25%; line-height: 24px; float:left;">To and Fro Travel</td>
									<td style="width:25%; float:left;">
										<bean:define name="newenquiryForm" property="alTargetedList" id="alTargetedList" type="java.util.Collection" />
										<html:select styleId="superviseToFroTravel" property="superviseToFroTravel" style="width:120px">
											<html:options name="newenquiryForm" collection="alTargetedList" property="key" labelProperty="value" />
										</html:select>
									</td>
								</tr>
								<tr>
									<td style="width:25%; float:left;">&nbsp;</td>
									<td class="formLabel" style="width:25%; line-height: 24px; float:left;">Lodging Boarding</td>
									<td style="width:25%; float:left;">
										<bean:define name="newenquiryForm" property="alTargetedList" id="alTargetedList" type="java.util.Collection" />
										<html:select styleId="superviseLodging" property="superviseLodging" style="width:120px">
											<html:options name="newenquiryForm" collection="alTargetedList" property="key" labelProperty="value" />
										</html:select>
									</td>
								</tr>
								-->
							</table>	
						</div>
						  
						<table width="100%">
							<tr><td style="width:100%;">&nbsp;</td></tr>
						</table>
						
						<!--
						<table width="100%">
							<tr>
								<td class="formLabel" style="width: 25%; line-height: 24px;">Commercial Purchase Specifications</td>
								<td style="width:25%; float:left;">
									<html:file property="commPurchaseSpecFile1" style="width:250px; font:10px;"/>
								</td>
							</tr>
							<logic:present name="attachmentCommPurchaseSpec" scope="request">
								<tr>
									<td colspan="2"><bean:write name="attachmentCommPurchaseSpec" /></td>
								</tr>
							</logic:present>
							<tr><td style="width:100%;">&nbsp;</td></tr>
						</table>
						-->
					</td>
				</tr>
			</table>
			
<!-- Commercial Terms and Conditions : Section : End -->