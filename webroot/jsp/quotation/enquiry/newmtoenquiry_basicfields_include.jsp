<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants,in.com.rbc.quotation.common.utility.PropertyUtility,in.com.rbc.quotation.common.utility.CommonUtility,in.com.rbc.quotation.common.vo.KeyValueVo" %>
<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>

<table>
	<tbody>
		<tr class="linked" style="width:1174px !important; float:left; overflow-x:scroll;">
			<td valign="top">
				<table id="tableR" align="center" cellspacing="0" cellpadding="1">
				<%	String sTextStyleClass = "textnoborder-light"; %>
				<% for(int j = 1; j <= 38; j++) {  %>
					
					<% if(j == 1) { %>
						<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
						<% sTextStyleClass = "textnoborder-dark"; %>
						<td width="200px" class="dark bordertd" style="float:left;"><strong>Replacement Motor</strong></td>
						<% } else { %>
						<% sTextStyleClass = "textnoborder-light"; %>
						<td width="200px" class="light bordertd" style="float:left;"><strong>Replacement Motor</strong></td>
						<% } %>
							
							<%  int idBaseRow38 = 1;  %>
							<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td id="td_isReplacementMotor<%=idBaseRow38%>" class="bordertd">
								<select style="width:100px;" name="isReplacementMotor<%=idBaseRow38%>" id="isReplacementMotor<%=idBaseRow38%>" title="Replacement Motor" onchange="javascript:processFieldsForReplaceMotor('<%=idBaseRow38%>');">
									<option value="0"><bean:message key="quotation.option.select" /></option>
									<logic:present name="newmtoenquiryForm" property="alTargetedList" >
										<bean:define name="newmtoenquiryForm" property="alTargetedList" id="alTargetedList" type="java.util.Collection" />
										<logic:iterate name="alTargetedList" id="alTargeted" type="in.com.rbc.quotation.common.vo.KeyValueVo">
											<option value='<bean:write name="alTargeted" property="key" />' > <bean:write name="alTargeted" property="value" /> </option>
										</logic:iterate>
									</logic:present>
								</select>
							</td>
							<script> selectLstItemById('isReplacementMotor<%=idBaseRow38%>', '<bean:write name="ratingObj" property="ltReplacementMotor" filter="false" />'); </script>
							<%	idBaseRow38 = idBaseRow38+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 2) { %>
						<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
						<% sTextStyleClass = "textnoborder-dark"; %>
						<td width="200px" class="dark bordertd"><strong>Earlier Supplied Motor Serial No.</strong></td>
						<% } else { %>
						<% sTextStyleClass = "textnoborder-light"; %>
						<td width="200px" class="light bordertd"><strong>Earlier Supplied Motor Serial No.</strong></td>
						<% } %>
							
							<%  int idBaseRow39 = 1;  %>
							<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td id="td_earlierSuppliedMotorSerialNo<%=idBaseRow39%>" class="bordertd">
								<input type="text" name="earlierSuppliedMotorSerialNo<%=idBaseRow39%>" id="earlierSuppliedMotorSerialNo<%=idBaseRow39%>" class="readonlymini" style="width:100px;" readonly="true" value='<bean:write name="ratingObj" property="ltEarlierMotorSerialNo" filter="false" />' title="Earlier Supplied Motor Serial No." maxlength="20" >
							</td>
							<%	idBaseRow39 = idBaseRow39+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 3) { %>
						<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
						<% sTextStyleClass = "textnoborder-dark"; %>
						<td width="200px" class="dark bordertd"><strong>Enclosure</strong></td>
						<% } else { %>
						<% sTextStyleClass = "textnoborder-light"; %>
						<td width="200px" class="light bordertd"><strong>Enclosure</strong></td>
						<% } %>
							
							<%  int idBaseRow1 = 1;  %>
							<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<input type="text" name="enclosure<%=idBaseRow1%>" id="enclosure<%=idBaseRow1%>" class="normal" style="width: 100px;" value='<bean:write name="ratingObj" property="ltEnclosure" filter="false" />' onfocus="javascript:processAutoSave('<%=idBaseRow1%>');" title="Enclosure" maxlength="20" >
							</td>
							<%	idBaseRow1 = idBaseRow1+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 4) { %>
						<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
						<% sTextStyleClass = "textnoborder-dark"; %>
						<td width="200px" class="dark bordertd"><strong>Method of Cooling</strong></td>
						<% } else { %>
						<% sTextStyleClass = "textnoborder-light"; %>
						<td width="200px" class="light bordertd"><strong>Method of Cooling</strong></td>
						<% } %>
							
							<%  int idBaseRow2 = 1;  %>
							<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td id="td_methodOfCooling<%=idBaseRow2%>" class="bordertd">
								<select style="width:100px;" name="methodOfCooling<%=idBaseRow2%>" id="methodOfCooling<%=idBaseRow2%>" title="Method of Cooling" onfocus="javascript:processAutoSave('<%=idBaseRow2%>');" >
									<option value="0"><bean:message key="quotation.option.select" /></option>
									<logic:present name="newmtoenquiryForm" property="ltForcedCoolingList" >
										<bean:define name="newmtoenquiryForm" property="ltForcedCoolingList" id="ltForcedCoolingList" type="java.util.Collection" />
										<logic:iterate name="ltForcedCoolingList" id="ltForcedCooling" type="in.com.rbc.quotation.common.vo.KeyValueVo">
											<option value='<bean:write name="ltForcedCooling" property="key" />' > <bean:write name="ltForcedCooling" property="value" /> </option>
										</logic:iterate>
									</logic:present>
								</select>
							</td>
							<script> selectLstItemById('methodOfCooling<%=idBaseRow2%>', '<bean:write name="ratingObj" property="ltForcedCoolingId" filter="false" />'); </script>
							<%	idBaseRow2 = idBaseRow2+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 5) { %>
						<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
						<% sTextStyleClass = "textnoborder-dark"; %>
						<td width="200px" class="dark bordertd"><strong>Rated Voltage</strong></td>
						<% } else { %>
						<% sTextStyleClass = "textnoborder-light"; %>
						<td width="200px" class="light bordertd"><strong>Rated Voltage</strong></td>
						<% } %>
							
							<%  int idBaseRow3 = 1;  %>
							<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:100px;" name="volt<%=idBaseRow3%>" id="volt<%=idBaseRow3%>" title="Voltage" onfocus="javascript:processAutoSave('<%=idBaseRow3%>');">
									<option value="0"><bean:message key="quotation.option.select" /></option>
									<logic:present property="ltVoltageList" name="newmtoenquiryForm">
										<bean:define name="newmtoenquiryForm" property="ltVoltageList" id="ltVoltageList" type="java.util.Collection" />
										<logic:iterate name="ltVoltageList" id="ltVoltage" type="in.com.rbc.quotation.common.vo.KeyValueVo">
											<option value='<bean:write name="ltVoltage" property="key" />' > <bean:write name="ltVoltage" property="value" /> </option>
										</logic:iterate>
									</logic:present>
								</select>
							</td>
							<script> selectLstItemById('volt<%=idBaseRow3%>', '<bean:write name="ratingObj" property="ltVoltId" filter="false" />'); </script>
							<%	idBaseRow3 = idBaseRow3+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 6) { %>
						<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
						<% sTextStyleClass = "textnoborder-dark"; %>
						<td width="200px" class="dark bordertd"><strong>Rated Voltage Variation (+)</strong></td> 
						<% } else { %>
						<% sTextStyleClass = "textnoborder-light"; %>
						<td width="200px" class="light bordertd"><strong>Rated Voltage Variation (+)</strong></td> 
						<% } %>
							
							<%  int idBaseRow4 = 1;  %>
							<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width: 40px;" name="voltAddVariation<%=idBaseRow4%>" id="voltAddVariation<%=idBaseRow4%>" title="Voltage Add Variation" onfocus="javascript:processAutoSave('<%=idBaseRow4%>');">
									<logic:present property="ltVoltVariationList" name="newmtoenquiryForm">
										<bean:define name="newmtoenquiryForm" property="ltVoltVariationList" id="ltVoltVariationList" type="java.util.Collection" />
										<logic:iterate name="ltVoltVariationList" id="ltVoltVariation" type="in.com.rbc.quotation.common.vo.KeyValueVo">
											<option value='<bean:write name="ltVoltVariation" property="key" />' > <bean:write name="ltVoltVariation" property="value" /> </option>
										</logic:iterate>
									</logic:present>
								</select> <span style="font-weight:bold;">%</span>
							</td>
							<script> selectLstItemById('voltAddVariation<%=idBaseRow4%>', '<bean:write name="ratingObj" property="ltVoltAddVariation" filter="false" />'); </script>
							<%	idBaseRow4 = idBaseRow4+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 7) { %>
						<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
						<% sTextStyleClass = "textnoborder-dark"; %>
						<td width="200px" class="dark bordertd"><strong>Rated Voltage Variation (-)</strong></td> 
						<% } else { %>
						<% sTextStyleClass = "textnoborder-light"; %>
						<td width="200px" class="light bordertd"><strong>Rated Voltage Variation (-)</strong></td> 
						<% } %>
							
							<%  int idBaseRow5 = 1;  %>
							<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width: 40px;" name="voltRemoveVariation<%=idBaseRow5%>" id="voltRemoveVariation<%=idBaseRow5%>" title="Voltage Remove Variation" onfocus="javascript:processAutoSave('<%=idBaseRow5%>');">
									<logic:present property="ltVoltVariationList" name="newmtoenquiryForm">
										<bean:define name="newmtoenquiryForm" property="ltVoltVariationList" id="ltVoltVariationList" type="java.util.Collection" />
										<logic:iterate name="ltVoltVariationList" id="ltVoltVariation" type="in.com.rbc.quotation.common.vo.KeyValueVo">
											<option value='<bean:write name="ltVoltVariation" property="key" />' > <bean:write name="ltVoltVariation" property="value" /> </option>
										</logic:iterate>
									</logic:present>
								</select> <span style="font-weight:bold;">%</span>
							</td>
							<script> selectLstItemById('voltRemoveVariation<%=idBaseRow5%>', '<bean:write name="ratingObj" property="ltVoltRemoveVariation" filter="false" />'); </script>
							<%	idBaseRow5 = idBaseRow5+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 8) { %>
						<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
						<% sTextStyleClass = "textnoborder-dark"; %>
						<td width="200px" class="dark bordertd"><strong>Rated Frequency</strong></td> 
						<% } else { %>
						<% sTextStyleClass = "textnoborder-light"; %>
						<td width="200px" class="light bordertd"><strong>Rated Frequency</strong></td> 
						<% } %>
							
							<%  int idBaseRow6 = 1;  %>
							<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:100px;" name="freq<%=idBaseRow6%>" id="freq<%=idBaseRow6%>" title="Frequency" onfocus="javascript:processAutoSave('<%=idBaseRow6%>');" >
									<option value="0"><bean:message key="quotation.option.select" /></option>
									<logic:present property="ltFrequencyList" name="newmtoenquiryForm">
										<bean:define name="newmtoenquiryForm" property="ltFrequencyList" id="ltFrequencyList" type="java.util.Collection" />
										<logic:iterate name="ltFrequencyList" id="ltFrequency" type="in.com.rbc.quotation.common.vo.KeyValueVo">
											<option value='<bean:write name="ltFrequency" property="key" />' > <bean:write name="ltFrequency" property="value" /> </option>
										</logic:iterate>
									</logic:present>
								</select>
							</td>
							<script> selectLstItemById('freq<%=idBaseRow6%>', '<bean:write name="ratingObj" property="ltFreqId" filter="false" />'); </script>
							<%	idBaseRow6 = idBaseRow6+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 9) { %>
						<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
						<% sTextStyleClass = "textnoborder-dark"; %>
						<td width="200px" class="dark bordertd"><strong>Rated Frequency Variation (+)</strong></td> 
						<% } else { %>
						<% sTextStyleClass = "textnoborder-light"; %>
						<td width="200px" class="light bordertd"><strong>Rated Frequency Variation (+)</strong></td> 
						<% } %>
							
							<%  int idBaseRow7 = 1;  %>
							<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width: 40px;" name="freqAddVariation<%=idBaseRow7%>" id="freqAddVariation<%=idBaseRow7%>" title="Frequency Add Variation" onfocus="javascript:processAutoSave('<%=idBaseRow7%>');" >
									<logic:present property="ltFreqVariationList" name="newmtoenquiryForm">
										<bean:define name="newmtoenquiryForm" property="ltFreqVariationList" id="ltFreqVariationList" type="java.util.Collection" />
										<logic:iterate name="ltFreqVariationList" id="ltFreqVariation" type="in.com.rbc.quotation.common.vo.KeyValueVo">
											<option value='<bean:write name="ltFreqVariation" property="key" />' > <bean:write name="ltFreqVariation" property="value" /> </option>
										</logic:iterate>
									</logic:present>
								</select> <span style="font-weight:bold;">%</span>
							</td>
							<script> selectLstItemById('freqAddVariation<%=idBaseRow7%>', '<bean:write name="ratingObj" property="ltFreqAddVariation" filter="false" />'); </script>
							<%	idBaseRow7 = idBaseRow7+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 10) { %>
						<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
						<% sTextStyleClass = "textnoborder-dark"; %>
						<td width="200px" class="dark bordertd"><strong>Rated Frequency Variation (-)</strong></td> 
						<% } else { %>
						<% sTextStyleClass = "textnoborder-light"; %>
						<td width="200px" class="light bordertd"><strong>Rated Frequency Variation (-)</strong></td> 
						<% } %>
							
							<%  int idBaseRow8 = 1;  %>
							<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width: 40px;" name="freqRemoveVariation<%=idBaseRow8%>" id="freqRemoveVariation<%=idBaseRow8%>" title="Frequency Remove Variation" onfocus="javascript:processAutoSave('<%=idBaseRow8%>');" >
									<logic:present property="ltFreqVariationList" name="newmtoenquiryForm">
										<bean:define name="newmtoenquiryForm" property="ltFreqVariationList" id="ltFreqVariationList" type="java.util.Collection" />
										<logic:iterate name="ltFreqVariationList" id="ltFreqVariation" type="in.com.rbc.quotation.common.vo.KeyValueVo">
											<option value='<bean:write name="ltFreqVariation" property="key" />' > <bean:write name="ltFreqVariation" property="value" /> </option>
										</logic:iterate>
									</logic:present>
								</select> <span style="font-weight:bold;">%</span>
							</td>
							<script> selectLstItemById('freqRemoveVariation<%=idBaseRow8%>', '<bean:write name="ratingObj" property="ltFreqRemoveVariation" filter="false" />'); </script>
							<%	idBaseRow8 = idBaseRow8+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 11) { %>
						<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
						<% sTextStyleClass = "textnoborder-dark"; %>
						<td width="200px" class="dark bordertd"><strong>Combined Variation</strong></td> 
						<% } else { %>
						<% sTextStyleClass = "textnoborder-light"; %>
						<td width="200px" class="light bordertd"><strong>Combined Variation</strong></td> 
						<% } %>
							
							<%  int idBaseRow9 = 1;  %>
							<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td id="td_combinedVariation<%=idBaseRow9%>" class="bordertd">
								<select style="width:60px;" name="combinedVariation<%=idBaseRow9%>" id="combinedVariation<%=idBaseRow9%>" title="Combined Variation" onfocus="javascript:processAutoSave('<%=idBaseRow9%>');" >
									<option value="0"><bean:message key="quotation.option.select" /></option>
									<logic:present property="ltCombinedVariationList" name="newmtoenquiryForm">
										<bean:define name="newmtoenquiryForm" property="ltCombinedVariationList" id="ltCombinedVariationList" type="java.util.Collection" />
										<logic:iterate name="ltCombinedVariationList" id="ltCombinedVariation" type="in.com.rbc.quotation.common.vo.KeyValueVo">
											<option value='<bean:write name="ltCombinedVariation" property="key" />' > <bean:write name="ltCombinedVariation" property="value" /> </option>
										</logic:iterate>
									</logic:present>
								</select> <span style="font-weight:bold;">%</span>
							</td>
							<script> selectLstItemById('combinedVariation<%=idBaseRow9%>', '<bean:write name="ratingObj" property="ltCombVariation" filter="false" />'); </script>
							<%	idBaseRow9 = idBaseRow9+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 12) { %>
						<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
						<% sTextStyleClass = "textnoborder-dark"; %>
						<td width="200px" class="dark bordertd"><strong>Area Classification</strong></td> 
						<% } else { %>
						<% sTextStyleClass = "textnoborder-light"; %>
						<td width="200px" class="light bordertd"><strong>Area Classification</strong></td> 
						<% } %>
							
							<%  int idBaseRow10 = 1;  %>
							<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:100px;" name="hazardAreaType<%=idBaseRow10%>" id="hazardAreaType<%=idBaseRow10%>" title="Area Classification" onchange="javascript:processFieldsForHazard('<%=idBaseRow10%>');" onfocus="javascript:processAutoSave('<%=idBaseRow10%>');" >
									<option value="0"><bean:message key="quotation.option.select" /></option>
									<logic:present property="ltHazAreaTypeList" name="newmtoenquiryForm">
										<bean:define name="newmtoenquiryForm" property="ltHazAreaTypeList" id="ltHazAreaTypeList" type="java.util.Collection" />
										<logic:iterate name="ltHazAreaTypeList" id="ltHazAreaType" type="in.com.rbc.quotation.common.vo.KeyValueVo">
											<option value='<bean:write name="ltHazAreaType" property="key" />' > <bean:write name="ltHazAreaType" property="value" /> </option>
										</logic:iterate>
									</logic:present>
								</select>
							</td>
							<script> selectLstItemById('hazardAreaType<%=idBaseRow10%>', '<bean:write name="ratingObj" property="ltHazardAreaTypeId" filter="false" />'); </script>
							<%	idBaseRow10 = idBaseRow10+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 13) { %>
						<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
						<% sTextStyleClass = "textnoborder-dark"; %>
						<td width="200px" class="dark bordertd"><strong>Hazard Protection</strong></td> 
						<% } else { %>
						<% sTextStyleClass = "textnoborder-light"; %>
						<td width="200px" class="light bordertd"><strong>Hazard Protection</strong></td> 
						<% } %>
							
							<%  int idBaseRow11 = 1;  %>
							<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:100px;" name="hazardArea<%=idBaseRow11%>" id="hazardArea<%=idBaseRow11%>" title="Hazard Protection" onfocus="javascript:processAutoSave('<%=idBaseRow11%>');" >
									<option value="0"><bean:message key="quotation.option.select" /></option>
									<logic:present property="ltHazAreaList" name="newmtoenquiryForm">
										<bean:define name="newmtoenquiryForm" property="ltHazAreaList" id="ltHazAreaList" type="java.util.Collection" />
										<logic:iterate name="ltHazAreaList" id="ltHazArea" type="in.com.rbc.quotation.common.vo.KeyValueVo">
											<option value='<bean:write name="ltHazArea" property="key" />' > <bean:write name="ltHazArea" property="value" /> </option>
										</logic:iterate>
									</logic:present>
								</select>
							</td>
							<script> selectLstItemById('hazardArea<%=idBaseRow11%>', '<bean:write name="ratingObj" property="ltHazardAreaId" filter="false" />'); </script>
							<%	idBaseRow11 = idBaseRow11+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 14) { %>
						<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
						<% sTextStyleClass = "textnoborder-dark"; %>
						<td width="200px" class="dark bordertd"><strong>Gas Group</strong></td> 
						<% } else { %>
						<% sTextStyleClass = "textnoborder-light"; %>
						<td width="200px" class="light bordertd"><strong>Gas Group</strong></td> 
						<% } %>
							
							<%  int idBaseRow12 = 1;  %>
							<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:100px;" name="gasGroup<%=idBaseRow12%>" id="gasGroup<%=idBaseRow12%>" title="Gas Group" onchange="javascript:processGasGroupChange('<%=idBaseRow12%>');" onfocus="javascript:processAutoSave('<%=idBaseRow12%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present property="ltGasGroupList" name="newmtoenquiryForm">
																	<bean:define name="newmtoenquiryForm" property="ltGasGroupList" id="ltGasGroupList" type="java.util.Collection" />
																	<logic:iterate name="ltGasGroupList" id="ltGasGroup" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltGasGroup" property="key" />' > <bean:write name="ltGasGroup" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
								</select>
							</td>
							<script> selectLstItemById('gasGroup<%=idBaseRow12%>', '<bean:write name="ratingObj" property="ltGasGroupId" filter="false" />'); </script>
							<%	idBaseRow12 = idBaseRow12+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 15) { %>
						<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
						<% sTextStyleClass = "textnoborder-dark"; %>
						<td width="200px" class="dark bordertd"><strong>Hazard Zone</strong></td> 
						<% } else { %>
						<% sTextStyleClass = "textnoborder-light"; %>
						<td width="200px" class="light bordertd"><strong>Hazard Zone</strong></td> 
						<% } %>
							
							<%  int idBaseRow13 = 1;  %>
							<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:100px;" name="hazardZone<%=idBaseRow13%>" id="hazardZone<%=idBaseRow13%>" title="Hazard Zone" onfocus="javascript:processAutoSave('<%=idBaseRow13%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present property="ltHazardZoneList" name="newmtoenquiryForm">
																	<bean:define name="newmtoenquiryForm" property="ltHazardZoneList" id="ltHazardZoneList" type="java.util.Collection" />
																	<logic:iterate name="ltHazardZoneList" id="ltHazardZone" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltHazardZone" property="key" />' > <bean:write name="ltHazardZone" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
								</select>
							</td>
							<script> selectLstItemById('hazardZone<%=idBaseRow13%>', '<bean:write name="ratingObj" property="ltHazardZoneId" filter="false" />'); </script>
							<%	idBaseRow13 = idBaseRow13+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 16) { %>
						<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
						<% sTextStyleClass = "textnoborder-dark"; %>
						<td width="200px" class="dark bordertd"><strong>Dust Group</strong></td> 
						<% } else { %>
						<% sTextStyleClass = "textnoborder-light"; %>
						<td width="200px" class="light bordertd"><strong>Dust Group</strong></td> 
						<% } %>
							
							<%  int idBaseRow14 = 1;  %>
							<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td id="td_dustGroup<%=idBaseRow14%>" class="bordertd">
								<select style="width:100px;" name="dustGroup<%=idBaseRow14%>" id="dustGroup<%=idBaseRow14%>" title="Dust Group" onchange="javascript:processDustGroupChange('<%=idBaseRow14%>');" onfocus="javascript:processAutoSave('<%=idBaseRow14%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present property="ltDustGroupList" name="newmtoenquiryForm">
																	<bean:define name="newmtoenquiryForm" property="ltDustGroupList" id="ltDustGroupList" type="java.util.Collection" />
																	<logic:iterate name="ltDustGroupList" id="ltDustGroup" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltDustGroup" property="key" />' > <bean:write name="ltDustGroup" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
								</select>
							</td>
							<script> selectLstItemById('dustGroup<%=idBaseRow14%>', '<bean:write name="ratingObj" property="ltDustGroupId" filter="false" />'); </script>
							<%	idBaseRow14 = idBaseRow14+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 17) { %>
						<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
						<% sTextStyleClass = "textnoborder-dark"; %>
						<td width="200px" class="dark bordertd"><strong>Temperature Class</strong></td> 
						<% } else { %>
						<% sTextStyleClass = "textnoborder-light"; %>
						<td width="200px" class="light bordertd"><strong>Temperature Class</strong></td> 
						<% } %>
							
							<%  int idBaseRow15 = 1;  %>
							<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:100px;" name="tempClass<%=idBaseRow15%>" id="tempClass<%=idBaseRow15%>" title="Temperature Class" onfocus="javascript:processAutoSave('<%=idBaseRow15%>');" >
									<option value="0"><bean:message key="quotation.option.select" /></option>
									<logic:present property="ltTemperatureClassList" name="newmtoenquiryForm">
										<bean:define name="newmtoenquiryForm" property="ltTemperatureClassList" id="ltTemperatureClassList" type="java.util.Collection" />
										<logic:iterate name="ltTemperatureClassList" id="ltTemperatureClass" type="in.com.rbc.quotation.common.vo.KeyValueVo">
											<option value='<bean:write name="ltTemperatureClass" property="key" />' > <bean:write name="ltTemperatureClass" property="value" /> </option>
										</logic:iterate>
									</logic:present>
								</select>
							</td>
							<script> selectLstItemById('tempClass<%=idBaseRow15%>', '<bean:write name="ratingObj" property="ltTempClass" filter="false" />'); </script>
							<%	idBaseRow15 = idBaseRow15+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 18) { %>
						<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
						<% sTextStyleClass = "textnoborder-dark"; %>
						<td width="200px" class="dark bordertd"><strong>Ambient Temp. Deg C (+)</strong></td> 
						<% } else { %>
						<% sTextStyleClass = "textnoborder-light"; %>
						<td width="200px" class="light bordertd"><strong>Ambient Temp. Deg C (+)</strong></td> 
						<% } %>
							
							<%  int idBaseRow16 = 1;  %>
							<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:100px;" name="ambientTemp<%=idBaseRow16%>" id="ambientTemp<%=idBaseRow16%>" title="Ambient Temp. Deg C (+)" onfocus="javascript:processAutoSave('<%=idBaseRow16%>');" >
									<option value="0"><bean:message key="quotation.option.select" /></option>
									<logic:present property="ltAmbientTemperatureList" name="newmtoenquiryForm">
										<bean:define name="newmtoenquiryForm" property="ltAmbientTemperatureList" id="ltAmbientTemperatureList" type="java.util.Collection" />
										<logic:iterate name="ltAmbientTemperatureList" id="ltAmbientTemperature" type="in.com.rbc.quotation.common.vo.KeyValueVo">
											<option value='<bean:write name="ltAmbientTemperature" property="key" />' > <bean:write name="ltAmbientTemperature" property="value" /> </option>
										</logic:iterate>
									</logic:present>
								</select>
							</td>
							<script> selectLstItemById('ambientTemp<%=idBaseRow16%>', '<bean:write name="ratingObj" property="ltAmbTempId" filter="false" />'); </script>
							<%	idBaseRow16 = idBaseRow16+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 19) { %>
						<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
						<% sTextStyleClass = "textnoborder-dark"; %>
						<td width="200px" class="dark bordertd"><strong>Ambient Temp. Deg C (-)</strong></td> 
						<% } else { %>
						<% sTextStyleClass = "textnoborder-light"; %>
						<td width="200px" class="light bordertd"><strong>Ambient Temp. Deg C (-)</strong></td> 
						<% } %>
							
							<%  int idBaseRow17 = 1;  %>
							<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td id="td_ambientTempSub<%=idBaseRow17%>" class="bordertd">
								<select style="width:100px;" name="ambientTempSub<%=idBaseRow17%>" id="ambientTempSub<%=idBaseRow17%>" title="Ambient Temp. Deg C (-)" onfocus="javascript:processAutoSave('<%=idBaseRow17%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present property="ltAmbientRemTempList" name="newmtoenquiryForm">
																	<bean:define name="newmtoenquiryForm" property="ltAmbientRemTempList" id="ltAmbientRemTempList" type="java.util.Collection" />
																	<logic:iterate name="ltAmbientRemTempList" id="ltAmbientRemTemp" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltAmbientRemTemp" property="key" />' > <bean:write name="ltAmbientRemTemp" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
								</select>
							</td>
							<script> selectLstItemById('ambientTempSub<%=idBaseRow17%>', '<bean:write name="ratingObj" property="ltAmbTempRemoveId" filter="false" />'); </script>
							<%	idBaseRow17 = idBaseRow17+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 20) { %>
						<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
						<% sTextStyleClass = "textnoborder-dark"; %>
						<td width="200px" class="dark bordertd"><strong>Insulation Class / Temp. Rise Class</strong></td> 
						<% } else { %>
						<% sTextStyleClass = "textnoborder-light"; %>
						<td width="200px" class="light bordertd"><strong>Insulation Class / Temp. Rise Class</strong></td> 
						<% } %>
							
							<%  int idBaseRow18 = 1;  %>
							<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:100px;" name="insulationClass<%=idBaseRow18%>" id="insulationClass<%=idBaseRow18%>" title="Insulation Class" onfocus="javascript:processAutoSave('<%=idBaseRow18%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newmtoenquiryForm" property="ltInsulationClassList" >
																	<bean:define name="newmtoenquiryForm" property="ltInsulationClassList" id="ltInsulationClassList" type="java.util.Collection" />
																	<logic:iterate name="ltInsulationClassList" id="ltInsulationClass" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltInsulationClass" property="key" />' > <bean:write name="ltInsulationClass" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
								</select>
							</td>
							<script> selectLstItemById('insulationClass<%=idBaseRow18%>', '<bean:write name="ratingObj" property="ltInsulationClassId" filter="false" />'); </script>
							<%	idBaseRow18 = idBaseRow18+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 21) { %>
						<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
						<% sTextStyleClass = "textnoborder-dark"; %>
						<td width="200px" class="dark bordertd"><strong>Temperature Rise</strong></td> 
						<% } else { %>
						<% sTextStyleClass = "textnoborder-light"; %>
						<td width="200px" class="light bordertd"><strong>Temperature Rise</strong></td> 
						<% } %>
							
							<%  int idBaseRow19 = 1;  %>
							<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:100px;" name="tempRise<%=idBaseRow19%>" id="tempRise<%=idBaseRow19%>" title="Temperature Rise" onfocus="javascript:processAutoSave('<%=idBaseRow19%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present property="ltTemperatureRiseList" name="newmtoenquiryForm">
																	<bean:define name="newmtoenquiryForm" property="ltTemperatureRiseList" id="ltTemperatureRiseList" type="java.util.Collection" />
																	<logic:iterate name="ltTemperatureRiseList" id="ltTemperatureRise" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltTemperatureRise" property="key" />' > <bean:write name="ltTemperatureRise" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
								</select>
							</td>
							<script> selectLstItemById('tempRise<%=idBaseRow19%>', '<bean:write name="ratingObj" property="ltTempRise" filter="false" />'); </script>
							<%	idBaseRow19 = idBaseRow19+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 22) { %>
						<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
						<% sTextStyleClass = "textnoborder-dark"; %>
						<td width="200px" class="dark bordertd"><strong>Altitude</strong></td>
						<% } else { %>
						<% sTextStyleClass = "textnoborder-light"; %>
						<td width="200px" class="light bordertd"><strong>Altitude</strong></td>
						<% } %>
							 
							<%  int idBaseRow20 = 1;  %>
							<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td id="td_altitude<%=idBaseRow20%>" class="bordertd">
								<input type="text" name="altitude<%=idBaseRow20%>" id="altitude<%=idBaseRow20%>" class="normal" style="width:100px;" value='<bean:write name="ratingObj" property="ltAltitude" filter="false" />' onfocus="javascript:processAutoSave('<%=idBaseRow20%>');" title="Altitude" maxlength="10" >
							</td>
							<%	idBaseRow20 = idBaseRow20+1; %>
							</logic:iterate>
						</tr>
					<%-- 
					<% } else if(j == 21) { %>
						<% if(j % 2 == 0) { %>
						<% sTextStyleClass = "textnoborder-dark"; %>
						<tr class="dark" id='item<%=j%>'>
						<% } else { %>
						<% sTextStyleClass = "textnoborder-light"; %>
						<tr class="light" id='item<%=j%>'>
						<% } %>
							<td width="200px" class="bordertd"><strong>Altitude</strong></td> 
							<%  int idBaseRow21 = 1;  %>
							<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								&nbsp;&nbsp;
								
							</td>
							<%	idBaseRow21 = idBaseRow21+1; %>
							</logic:iterate>
						</tr>
					--%>
					<% } else if(j == 23) { %>
						<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
						<% sTextStyleClass = "textnoborder-dark"; %>
						<td width="200px" class="dark bordertd"><strong>Degree of Protection (IP)</strong></td> 
						<% } else { %>
						<% sTextStyleClass = "textnoborder-light"; %>
						<td width="200px" class="light bordertd"><strong>Degree of Protection (IP)</strong></td> 
						<% } %>
							
							<%  int idBaseRow22 = 1;  %>
							<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td id="td_ip<%=idBaseRow22%>" class="bordertd">
								<select style="width:100px;" name="ip<%=idBaseRow22%>" id="ip<%=idBaseRow22%>" title="IP" onfocus="javascript:processAutoSave('<%=idBaseRow22%>');" >
									<option value="0"><bean:message key="quotation.option.select" /></option>
									<logic:present name="newmtoenquiryForm" property="ltIPList" >
										<bean:define name="newmtoenquiryForm" property="ltIPList" id="ltIPList" type="java.util.Collection" />
										<logic:iterate name="ltIPList" id="ltIP" type="in.com.rbc.quotation.common.vo.KeyValueVo">
											<option value='<bean:write name="ltIP" property="key" />' > <bean:write name="ltIP" property="value" /> </option>
										</logic:iterate>
									</logic:present>
								</select>
							</td>
							<script> selectLstItemById('ip<%=idBaseRow22%>', '<bean:write name="ratingObj" property="ltIPId" filter="false" />'); </script>
							<%	idBaseRow22 = idBaseRow22+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 24) { %>
						<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
						<% sTextStyleClass = "textnoborder-dark"; %>
						<td width="200px" class="dark bordertd"><strong>Method of Starting</strong></td> 
						<% } else { %>
						<% sTextStyleClass = "textnoborder-light"; %>
						<td width="200px" class="light bordertd"><strong>Method of Starting</strong></td> 
						<% } %>
							
							<%  int idBaseRow23 = 1;  %>
							<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td id="td_methodOfStarting<%=idBaseRow23%>" class="bordertd">
								<select style="width: 100px;" name="methodOfStarting<%=idBaseRow23%>" id="methodOfStarting<%=idBaseRow23%>" title="Method of Starting" onfocus="javascript:processAutoSave('<%=idBaseRow23%>');" onchange="javascript:processFieldsForStartingMethod('<%=idBaseRow23%>');" >
									<option value="0"><bean:message key="quotation.option.select" /></option>
									<logic:present name="newmtoenquiryForm" property="ltMethodOfStartingList">
										<bean:define name="newmtoenquiryForm" property="ltMethodOfStartingList" id="ltMethodOfStartingList" type="java.util.Collection" />
										<logic:iterate name="ltMethodOfStartingList" id="ltMethodOfStarting" type="in.com.rbc.quotation.common.vo.KeyValueVo">
											<option value='<bean:write name="ltMethodOfStarting" property="key" />'>
												<bean:write name="ltMethodOfStarting" property="value" />
											</option>
										</logic:iterate>
									</logic:present>
								</select>
							</td>
							<script> selectLstItemById('methodOfStarting<%=idBaseRow23%>', '<bean:write name="ratingObj" property="ltMethodOfStarting" filter="false" />'); </script>
							<%	idBaseRow23 = idBaseRow23+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 25) { %>
						<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
						<% sTextStyleClass = "textnoborder-dark"; %>
						<td width="200px" class="dark bordertd"><strong>Direction of Rotation</strong></td> 
						<% } else { %>
						<% sTextStyleClass = "textnoborder-light"; %>
						<td width="200px" class="light bordertd"><strong>Direction of Rotation</strong></td> 
						<% } %>
							
							<%  int idBaseRow24 = 1;  %>
							<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:100px;" name="directionOfRotation<%=idBaseRow24%>" id="directionOfRotation<%=idBaseRow24%>" title="Direction of Rotation" onfocus="javascript:processAutoSave('<%=idBaseRow24%>');" >
																	<option value="0"><bean:message key="quotation.option.select" /></option>
																	<logic:present name="newmtoenquiryForm" property="ltDirectionOfRotationList" >
																		<bean:define name="newmtoenquiryForm" property="ltDirectionOfRotationList" id="ltDirectionOfRotationList" type="java.util.Collection" />
																		<logic:iterate name="ltDirectionOfRotationList" id="ltDirectionOfRotation" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																			<option value='<bean:write name="ltDirectionOfRotation" property="key" />' > <bean:write name="ltDirectionOfRotation" property="value" /> </option>
																		</logic:iterate>
																	</logic:present>
								</select>
							</td>
							<script> selectLstItemById('directionOfRotation<%=idBaseRow24%>', '<bean:write name="ratingObj" property="ltDirectionOfRotation" filter="false" />'); </script>
							<%	idBaseRow24 = idBaseRow24+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 26) { %>
						<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
						<% sTextStyleClass = "textnoborder-dark"; %>
						<td width="200px" class="dark bordertd"><strong>Standard Rotation</strong></td> 
						<% } else { %>
						<% sTextStyleClass = "textnoborder-light"; %>
						<td width="200px" class="light bordertd"><strong>Standard Rotation</strong></td> 
						<% } %>
							
							<%  int idBaseRow25 = 1;  %>
							<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:100px;" name="standardRotation<%=idBaseRow25%>" id="standardRotation<%=idBaseRow25%>" title="Standard Rotation" onfocus="javascript:processAutoSave('<%=idBaseRow25%>');" >
									<option value="0"><bean:message key="quotation.option.select" /></option>
									<logic:present name="newmtoenquiryForm" property="ltStandardRotationList" >
										<bean:define name="newmtoenquiryForm" property="ltStandardRotationList" id="ltStandardRotationList" type="java.util.Collection" />
										<logic:iterate name="ltStandardRotationList" id="ltStandardRotation" type="in.com.rbc.quotation.common.vo.KeyValueVo">
											<option value='<bean:write name="ltStandardRotation" property="key" />' > <bean:write name="ltStandardRotation" property="value" /> </option>
										</logic:iterate>
									</logic:present>
								</select>
							</td>
							<script> selectLstItemById('standardRotation<%=idBaseRow25%>', '<bean:write name="ratingObj" property="ltStandardRotation" filter="false" />'); </script>
							<%	idBaseRow25 = idBaseRow25+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 27) { %>
						<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
						<% sTextStyleClass = "textnoborder-dark"; %>
						<td width="200px" class="dark bordertd"><strong>Type of Grease</strong></td> 
						<% } else { %>
						<% sTextStyleClass = "textnoborder-light"; %>
						<td width="200px" class="light bordertd"><strong>Type of Grease</strong></td> 
						<% } %>
							
							<%  int idBaseRow26 = 1;  %>
							<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:100px;" name="typeOfGrease<%=idBaseRow26%>" id="typeOfGrease<%=idBaseRow26%>" title="Type Of Grease" onfocus="javascript:processAutoSave('<%=idBaseRow26%>');" >
									<option value="0"><bean:message key="quotation.option.select" /></option>
									<logic:present name="newmtoenquiryForm" property="ltTypeOfGreaseList" >
										<bean:define name="newmtoenquiryForm" property="ltTypeOfGreaseList" id="ltTypeOfGreaseList" type="java.util.Collection" />
										<logic:iterate name="ltTypeOfGreaseList" id="ltTypeOfGrease" type="in.com.rbc.quotation.common.vo.KeyValueVo">
											<option value='<bean:write name="ltTypeOfGrease" property="key" />' > <bean:write name="ltTypeOfGrease" property="value" /> </option>
										</logic:iterate>
									</logic:present>
								</select>
							</td>
							<script> selectLstItemById('typeOfGrease<%=idBaseRow26%>', '<bean:write name="ratingObj" property="ltStandardRotation" filter="false" />'); </script>
							<%	idBaseRow26 = idBaseRow26+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 28) { %>
						<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
						<% sTextStyleClass = "textnoborder-dark"; %>
						<td width="200px" class="dark bordertd"><strong>Paint Type</strong></td> 
						<% } else { %>
						<% sTextStyleClass = "textnoborder-light"; %>
						<td width="200px" class="light bordertd"><strong>Paint Type</strong></td> 
						<% } %>
							
							<%  int idBaseRow27 = 1;  %>
							<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:100px;" name="paintingType<%=idBaseRow27%>" id="paintingType<%=idBaseRow27%>" title="Painting Type" onfocus="javascript:processAutoSave('<%=idBaseRow27%>');" >
																	<option value="0"><bean:message key="quotation.option.select" /></option>
																	<logic:present name="newmtoenquiryForm" property="ltPaintingTypeList" >
																		<bean:define name="newmtoenquiryForm" property="ltPaintingTypeList" id="ltPaintingTypeList" type="java.util.Collection" />
																		<logic:iterate name="ltPaintingTypeList" id="ltPaintingType" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																			<option value='<bean:write name="ltPaintingType" property="key" />' > <bean:write name="ltPaintingType" property="value" /> </option>
																		</logic:iterate>
																	</logic:present>
								</select>
							</td>
							<script> selectLstItemById('paintingType<%=idBaseRow27%>', '<bean:write name="ratingObj" property="ltPaintingTypeId" filter="false" />'); </script>
							<%	idBaseRow27 = idBaseRow27+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 29) { %>
						<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
						<% sTextStyleClass = "textnoborder-dark"; %>
						<td width="200px" class="dark bordertd"><strong>Paint Shade</strong></td> 
						<% } else { %>
						<% sTextStyleClass = "textnoborder-light"; %>
						<td width="200px" class="light bordertd"><strong>Paint Shade</strong></td> 
						<% } %>
							
							<%  int idBaseRow28 = 1;  %>
							<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td id="td_paintShade<%=idBaseRow28%>" class="bordertd">
								<select style="width:100px;" name="paintShade<%=idBaseRow28%>" id="paintShade<%=idBaseRow28%>" title="Paint Shade" onfocus="javascript:processAutoSave('<%=idBaseRow28%>');" onchange="javascript:processFieldsForPaintShade('<%=idBaseRow28%>');" >
																	<option value="0"><bean:message key="quotation.option.select" /></option>
																	<logic:present name="newmtoenquiryForm" property="ltPaintShadeList" >
																		<bean:define name="newmtoenquiryForm" property="ltPaintShadeList" id="ltPaintShadeList" type="java.util.Collection" />
																		<logic:iterate name="ltPaintShadeList" id="ltPaintShade" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																			<option value='<bean:write name="ltPaintShade" property="key" />' > <bean:write name="ltPaintShade" property="value" /> </option>
																		</logic:iterate>
																	</logic:present>
								</select>
							</td>
							<script> selectLstItemById('paintShade<%=idBaseRow28%>', '<bean:write name="ratingObj" property="ltPaintShadeId" filter="false" />'); </script>
							<%	idBaseRow28 = idBaseRow28+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 30) { %>
						<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
						<% sTextStyleClass = "textnoborder-dark"; %>
						<td width="200px" class="dark bordertd"><strong>Non Standard Paint Shade Value</strong></td> 
						<% } else { %>
						<% sTextStyleClass = "textnoborder-light"; %>
						<td width="200px" class="light bordertd"><strong>Non Standard Paint Shade Value</strong></td> 
						<% } %>
							
							<%  int idBaseRow29 = 1;  %>
							<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td id="td_paintShadeVal<%=idBaseRow29%>" class="bordertd">
								<input type="text" name="paintShadeVal<%=idBaseRow29%>" id="paintShadeVal<%=idBaseRow29%>" class="readonlymini" style="width:100px;" readonly="true" value='<bean:write name="ratingObj" property="ltPaintShadeValue" filter="false" />' title="Non Standard Paint Shade Value" onfocus="javascript:processAutoSave('<%=idBaseRow29%>');" maxlength="20" >
							</td>
							<%	idBaseRow29 = idBaseRow29+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 31) { %>
						<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
						<% sTextStyleClass = "textnoborder-dark"; %>
						<td width="200px" class="dark bordertd"><strong>Paint Thickness</strong></td> 
						<% } else { %>
						<% sTextStyleClass = "textnoborder-light"; %>
						<td width="200px" class="light bordertd"><strong>Paint Thickness</strong></td> 
						<% } %>
							
							<%  int idBaseRow30 = 1;  %>
							<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td id="td_paintThickness<%=idBaseRow30%>" class="bordertd">
								<select style="width:100px;" name="paintThickness<%=idBaseRow30%>" id="paintThickness<%=idBaseRow30%>" title="Paint Thickness" onfocus="javascript:processAutoSave('<%=idBaseRow30%>');" >
																	<option value="0"><bean:message key="quotation.option.select" /></option>
																	<logic:present name="newmtoenquiryForm" property="ltPaintThicknessList" >
																		<bean:define name="newmtoenquiryForm" property="ltPaintThicknessList" id="ltPaintThicknessList" type="java.util.Collection" />
																		<logic:iterate name="ltPaintThicknessList" id="ltPaintThickness" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																			<option value='<bean:write name="ltPaintThickness" property="key" />' > <bean:write name="ltPaintThickness" property="value" /> </option>
																		</logic:iterate>
																	</logic:present>
								</select>
							</td>
							<script> selectLstItemById('paintThickness<%=idBaseRow30%>', '<bean:write name="ratingObj" property="ltPaintThicknessId" filter="false" />'); </script>
							<%	idBaseRow30 = idBaseRow30+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 32) { %>
						<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
						<% sTextStyleClass = "textnoborder-dark"; %>
						<td width="200px" class="dark bordertd"><strong>Cable Size</strong></td>
						<% } else { %>
						<% sTextStyleClass = "textnoborder-light"; %>
						<td width="200px" class="light bordertd"><strong>Cable Size</strong></td>
						<% } %>
							 
							<%  int idBaseRow31 = 1;  %>
							<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:100px;" name="cableSize<%=idBaseRow31%>" id="cableSize<%=idBaseRow31%>" title="Cable Size" onfocus="javascript:processAutoSave('<%=idBaseRow31%>');" onchange="javascript:processFieldsForCableSize('<%=idBaseRow31%>');" >
																	<option value="0"><bean:message key="quotation.option.select" /></option>
																	<logic:present name="newmtoenquiryForm" property="ltCableSizeList" >
																		<bean:define name="newmtoenquiryForm" property="ltCableSizeList" id="ltCableSizeList" type="java.util.Collection" />
																		<logic:iterate name="ltCableSizeList" id="ltCableSize" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																			<option value='<bean:write name="ltCableSize" property="key" />' > <bean:write name="ltCableSize" property="value" /> </option>
																		</logic:iterate>
																	</logic:present>
								</select>
							</td>
							<script> selectLstItemById('cableSize<%=idBaseRow31%>', '<bean:write name="ratingObj" property="ltCableSizeId" filter="false" />'); </script>
							<%	idBaseRow31 = idBaseRow31+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 33) { %>
						<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
						<% sTextStyleClass = "textnoborder-dark"; %>
						<td width="200px" class="dark bordertd"><strong>No. Of Runs</strong></td> 
						<% } else { %>
						<% sTextStyleClass = "textnoborder-light"; %>
						<td width="200px" class="light bordertd"><strong>No. Of Runs</strong></td> 
						<% } %>
							
							<%  int idBaseRow32 = 1;  %>
							<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:100px;" name="noOfRuns<%=idBaseRow32%>" id="noOfRuns<%=idBaseRow32%>" title="No. Of Runs" onfocus="javascript:processAutoSave('<%=idBaseRow32%>');" >
																	<option value="0"><bean:message key="quotation.option.select" /></option>
																	<logic:present name="newmtoenquiryForm" property="ltNoOfRunsList" >
																		<bean:define name="newmtoenquiryForm" property="ltNoOfRunsList" id="ltNoOfRunsList" type="java.util.Collection" />
																		<logic:iterate name="ltNoOfRunsList" id="ltNoOfRuns" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																			<option value='<bean:write name="ltNoOfRuns" property="key" />' > <bean:write name="ltNoOfRuns" property="value" /> </option>
																		</logic:iterate>
																	</logic:present>
								</select>
							</td>
							<script> selectLstItemById('noOfRuns<%=idBaseRow32%>', '<bean:write name="ratingObj" property="ltNoOfRuns" filter="false" />'); </script>
							<%	idBaseRow32 = idBaseRow32+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 34) { %>
						<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
						<% sTextStyleClass = "textnoborder-dark"; %>
						<td width="200px" class="dark bordertd"><strong>No. Of Cores</strong></td> 
						<% } else { %>
						<% sTextStyleClass = "textnoborder-light"; %>
						<td width="200px" class="light bordertd"><strong>No. Of Cores</strong></td> 
						<% } %>
							
							<%  int idBaseRow33 = 1;  %>
							<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:100px;" name="noOfCores<%=idBaseRow33%>" id="noOfCores<%=idBaseRow33%>" title="No. Of Cores" onfocus="javascript:processAutoSave('<%=idBaseRow33%>');" >
																	<option value="0"><bean:message key="quotation.option.select" /></option>
																	<logic:present name="newmtoenquiryForm" property="ltNoOfCoresList" >
																		<bean:define name="newmtoenquiryForm" property="ltNoOfCoresList" id="ltNoOfCoresList" type="java.util.Collection" />
																		<logic:iterate name="ltNoOfCoresList" id="ltNoOfCores" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																			<option value='<bean:write name="ltNoOfCores" property="key" />' > <bean:write name="ltNoOfCores" property="value" /> </option>
																		</logic:iterate>
																	</logic:present>
								</select>
							</td>
							<script> selectLstItemById('noOfCores<%=idBaseRow33%>', '<bean:write name="ratingObj" property="ltNoOfCores" filter="false" />'); </script>
							<%	idBaseRow33 = idBaseRow33+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 35) { %>
						<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
						<% sTextStyleClass = "textnoborder-dark"; %>
						<td width="200px" class="dark bordertd"><strong>Cross-Sectional Area (mm2)</strong></td> 
						<% } else { %>
						<% sTextStyleClass = "textnoborder-light"; %>
						<td width="200px" class="light bordertd"><strong>Cross-Sectional Area (mm2)</strong></td> 
						<% } %>
							
							<%  int idBaseRow34 = 1;  %>
							<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:100px;" name="crossSectionArea<%=idBaseRow34%>" id="crossSectionArea<%=idBaseRow34%>" title="Cross-Sectional Area (mm2)" onfocus="javascript:processAutoSave('<%=idBaseRow34%>');" >
																	<option value="0"><bean:message key="quotation.option.select" /></option>
																	<logic:present name="newmtoenquiryForm" property="ltCrossSectionAreaList" >
																		<bean:define name="newmtoenquiryForm" property="ltCrossSectionAreaList" id="ltCrossSectionAreaList" type="java.util.Collection" />
																		<logic:iterate name="ltCrossSectionAreaList" id="ltCrossSectionArea" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																			<option value='<bean:write name="ltCrossSectionArea" property="key" />' > <bean:write name="ltCrossSectionArea" property="value" /> </option>
																		</logic:iterate>
																	</logic:present>
								</select>
							</td>
							<script> selectLstItemById('crossSectionArea<%=idBaseRow34%>', '<bean:write name="ratingObj" property="ltCrossSectionAreaId" filter="false" />'); </script>
							<%	idBaseRow34 = idBaseRow34+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 36) { %>
						<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
						<% sTextStyleClass = "textnoborder-dark"; %>
						<td width="200px" class="dark bordertd"><strong>Material of Conductor</strong></td> 
						<% } else { %>
						<% sTextStyleClass = "textnoborder-light"; %>
						<td width="200px" class="light bordertd"><strong>Material of Conductor</strong></td> 
						<% } %>
							
							<%  int idBaseRow35 = 1;  %>
							<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:100px;" name="conductorMaterial<%=idBaseRow35%>" id="conductorMaterial<%=idBaseRow35%>" title="Material of Conductor" onfocus="javascript:processAutoSave('<%=idBaseRow35%>');" >
																	<option value="0"><bean:message key="quotation.option.select" /></option>
																	<logic:present name="newmtoenquiryForm" property="ltConductorMaterialList" >
																		<bean:define name="newmtoenquiryForm" property="ltConductorMaterialList" id="ltConductorMaterialList" type="java.util.Collection" />
																		<logic:iterate name="ltConductorMaterialList" id="ltConductorMaterial" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																			<option value='<bean:write name="ltConductorMaterial" property="key" />' > <bean:write name="ltConductorMaterial" property="value" /> </option>
																		</logic:iterate>
																	</logic:present>
								</select>
							</td>
							<script> selectLstItemById('conductorMaterial<%=idBaseRow35%>', '<bean:write name="ratingObj" property="ltConductorMaterialId" filter="false" />'); </script>
							<%	idBaseRow35 = idBaseRow35+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 37) { %>
						<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
						<% sTextStyleClass = "textnoborder-dark"; %>
						<td width="200px" class="dark bordertd"><strong>Cable Diameter</strong></td> 
						<% } else { %>
						<% sTextStyleClass = "textnoborder-light"; %>
						<td width="200px" class="light bordertd"><strong>Cable Diameter</strong></td> 
						<% } %>
							
							<%  int idBaseRow36 = 1;  %>
							<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<input type="text" name="cableDiameter<%=idBaseRow36%>" id="cableDiameter<%=idBaseRow36%>" class="normal" style="width:100px;" readonly="false" value='<bean:write name="ratingObj" property="ltCableDiameter" filter="false" />' title="Cable Diameter" onfocus="javascript:processAutoSave('<%=idBaseRow36%>');" maxlength="5" >
							</td>
							<%	idBaseRow36 = idBaseRow36+1; %>
							</logic:iterate>
						</tr>
					<% } else if(j == 38) { %>
						<tr id='item<%=j%>'>
						<% if(j % 2 == 0) { %>
						<% sTextStyleClass = "textnoborder-dark"; %>
						<td width="200px" class="dark bordertd"><strong>Noise Level (Subject to Tolerance)</strong></td> 
						<% } else { %>
						<% sTextStyleClass = "textnoborder-light"; %>
						<td width="200px" class="light bordertd"><strong>Noise Level (Subject to Tolerance)</strong></td> 
						<% } %>
							
							<%  int idBaseRow37 = 1;  %>
							<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td id="td_noiseLevel<%=idBaseRow37%>" class="bordertd">
								<select style="width:100px;" class="normal" name="noiseLevel<%=idBaseRow37%>" id="noiseLevel<%=idBaseRow37%>" title="Noise Level" onfocus="javascript:processAutoSave('<%=idBaseRow37%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present property="ltNoiseLevelList" name="newmtoenquiryForm">
																	<bean:define name="newmtoenquiryForm" property="ltNoiseLevelList" id="ltNoiseLevelList" type="java.util.Collection" />
																	<logic:iterate name="ltNoiseLevelList" id="ltNoiseLevel" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltNoiseLevel" property="key" />' > <bean:write name="ltNoiseLevel" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
								</select>
							</td>
							<script> selectLstItemById('noiseLevel<%=idBaseRow37%>', '<bean:write name="ratingObj" property="ltNoiseLevel" filter="false" />'); </script>
							<%	idBaseRow37 = idBaseRow37+1; %>
							</logic:iterate>
						</tr>
					<% } %>	
				<% } %>
				</table>
			</td>
		</tr>
	</tbody>
</table>