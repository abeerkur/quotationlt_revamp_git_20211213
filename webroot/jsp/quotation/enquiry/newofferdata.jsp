<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants" %>
<%@ page import="in.com.rbc.quotation.common.utility.PropertyUtility" %>
<%@ page import="in.com.rbc.quotation.common.utility.CommonUtility" %>
<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<% String hideAddOn = ""; %>

<html:form action="newPlaceAnEnquiry.do" method="POST">

	<html:hidden property="invoke" value="viewOfferData"/>
	<html:hidden property="createdBySSO" /><!--SSO Id  -->
	<html:hidden property="createdBy" /><!-- SSO Name -->

	<logic:equal name="newenquiryForm" property="enquiryOperationType" value="engineer">
		<input type="hidden" name="pagechecking" id="pagechecking" value="de" />
	</logic:equal>
	
	<logic:equal name="newenquiryForm" property="enquiryOperationType" value="manager">
		<input type="hidden" name="pagechecking" id="pagechecking" value="dm" />
	</logic:equal>
	
	<html:hidden property="operationType" styleId="operationType" value="DM" />

	<html:hidden property="enquiryOperationType"/>
	<html:hidden property="dispatch"/>
	<html:hidden property="currentRating"/>
	<html:hidden property="enquiryId" />
	<html:hidden property="ratingId" />
	<html:hidden property="enquiryNumber" />
	<html:hidden property="nextRatingId"/>
	<html:hidden property="ratingsValidationMessage"/>
	<html:hidden property="assignToName" />
	<html:hidden property="statusId" styleId="statusId" />
	<html:hidden property="customerId" styleId="customerId" />
	
	<input type="hidden" name='<%= org.apache.struts.taglib.html.Constants.TOKEN_KEY %>'
         value='<%= session.getAttribute(org.apache.struts.action.Action.TRANSACTION_TOKEN_KEY) %>' />
         
	<head>
		
		<script>
		 	window.onload = function(){  
				addonstyle();
			}
		</script>
		<script type="text/javascript">
			var globalratesList=new Array;
		</script>
		
		<style>
			.lablebox-td{
				width:33% !important;
			    text-align: left;
			    font-weight: normal;
			    font-size: 10px;
			    font-family: Verdana, Arial, Helvetica, sans-serif;
			    padding: 2px;
			    background-color: #F2F2FF;
			}
			/* my general stylizing for inputs, mostly comes from Twitter's
			bootstrap.min.css which is distributed with Zend Framework 2*/
			/*input
			{
			  background-color: #fff;
			  border: 1px solid #ccc;
			  display: inline-block;
			  height: 20px;
			  padding: 4px 6px;
			  margin-bottom: 10px;
			  font-size: 11px;
			  line-height: 20px;
			  color: #555;
			  vertical-align: middle;
			  -webkit-border-radius: 4px;
			  -moz-border-radius: 4px;
			  border-radius: 4px;
			}*/
			select
			{
			  	border: 1px solid #ccc;
			  	color: #555;
			}
			.background-bg-box{
				background: #e0e0e0;
			}
			/* specific customizations for the data-list */
			div.data-list-input
			{
				position: relative;
				height: 20px;	
				display: inline-flex;
			}
			select.data-list-input
			{
				position: absolute;	
				height: 20px;
				margin-bottom: 0;
			}
			input.data-list-input
			{
				position: absolute;
				top: 0px;
				left: 0px;
				height: 19px;
			}
			.width-select{
				width:100% !important;;
			}
			.input-class{
				width:87% !important;
				padding:4px 6px;
				border-width:1px;
				margin:0;
			}
		</style>
		
	</head>
	
	<!-- ---------------------------------------------------------- Main Table Start  ------------------------------------------------------------------- -->
	
	<%!String rowIndex="";%>
 	<% int countval = 0; %>
 	<%!String str="";%>
 	
	<!-- ------------------------------------------------------	Top Table Section : START --------------------------------------------------------------- -->	
	
		<table width="100%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
			<tr>
    			<td height="100%" valign="top">
				
				<!----------------------------------------------- Top Title and Status block Start  -------------------------------------------------------->
				<br>
				
				<div class="sectiontitle"> <bean:message key="quotation.offerdata.label.heading"/> </div>
				
				<br>
				
				<table width="100%" border="0" cellpadding="0" cellspacing="3">
					<tr>
            			<td rowspan="2" style="width:40% ">
            			<table width="98%"  border="0" cellspacing="2" cellpadding="0" class="DotsTable">
            				<tr>
            					<td rowspan="2" style="width:70px "><img src="html/images/icon_quote.gif" width="52" height="50"></td>
              					<td class="other">
					              	<bean:message key="quotation.viewenquiry.label.subheading1"/> <bean:write name="newenquiryForm" property="enquiryNumber"/><br>
					                <bean:message key="quotation.viewenquiry.label.subheading2"/> 
                				</td>
            				</tr>
            				<tr>
            					<td class="other">
            					<table width="100%">
											<tr>
												<td height="16" class="section-head" align="center"><bean:message key="quotation.viewenquiry.label.status" /> <bean:write name="newenquiryForm" property="statusName" /></td>
											</tr>
											<logic:equal name="newenquiryForm" property="enquiryOperationType" value="engineer">
											<tr>
												<td height="16" class="section-head" align="center"><bean:message key="quotation.home.colheading.assignto" /> :&nbsp; <bean:write name="newenquiryForm" property="designEngineerName" /></td>
											</tr>
											<tr>
												<td height="16" class="section-head" align="center"><bean:message key="quotation.create.label.salesrepresentative" /> :&nbsp; <bean:write name="newenquiryForm" property="sm_name" /> </td>
											</tr>
											</logic:equal>
											<logic:equal name="newenquiryForm" property="enquiryOperationType" value="manager">
											<tr>
												<td height="16" class="section-head" align="center"><bean:message key="quotation.home.colheading.assignto" /> :&nbsp; <bean:write name="newenquiryForm" property="createdBy" /></td>
											</tr>
											<tr>
												<td height="16" class="section-head" align="center"><bean:message key="quotation.create.label.salesrepresentative" /> :&nbsp; <bean:write name="newenquiryForm" property="sm_name" /> </td>
											</tr>
											</logic:equal>
								</table>
            					<br>
            					</td>
            				</tr>
            			</table>
            			</td>
            			<logic:lessThan name="newenquiryForm" property="statusId" value="6"> 
					        <td class="other" align="right" colspan="2">
				        	<bean:message key="quotation.attachments.message"/>
				    	    <a href="javascript:manageAttachments('<%= CommonUtility.getIntValue(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_ATTACHMENT_APPLICATIONID)) %>','<bean:write name="newenquiryForm" property="enquiryId"/>', '<bean:write name="newenquiryForm" property="createdBySSO"/>');"><img src="html/images/ico_attach.gif" alt="Manage Attachments" width="23" height="25" border="0" align="right"></a></td>
				        </logic:lessThan>
            		</tr>
            		
            		<tr>
            			<td colspan="2" style="width:45% ">
				            <logic:present name="attachmentList" scope="request">
					            <logic:notEmpty name="attachmentList">
						            <fieldset>
						              <table width="100%"  cellpadding="0" cellspacing="0" border="0" class="other">
						              <logic:iterate name="attachmentList" id="attachmentData" indexId="idx" type="in.com.rbc.quotation.common.vo.KeyValueVo">	
										<tr><td>
											<a href="javascript:download('<bean:write name="attachmentData" property="key"/>', '<bean:write name="attachmentData" property="value"/>', '<bean:write name="attachmentData" property="value2"/>');">
												<bean:write name="attachmentData" property="value"/>.<bean:write name="attachmentData" property="value2"/>
											</a>
										</td></tr>					
										</logic:iterate>
						              </table>
						        	</fieldset>
						        </logic:notEmpty>
					        </logic:present>
				    	</td>
            		</tr>
            		<tr>
            			<td colspan="2" align="right" > 
			        		<!-- DM Uploading Files  -->
				            <logic:lessThan name="newenquiryForm" property="statusId" value="6">
								<table>
									<tr>
										<td class="other" align="right"><bean:message key="quotation.attachments.dmuploadedmessage" /></td>
										<td class="other" align="left">
										<a href="javascript:manageAttachments('<%=CommonUtility.getIntValue(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_DMATTACHMENT_APPLICATIONID))%>','<bean:write name="newenquiryForm" property="enquiryId"/>', '<bean:write name="newenquiryForm" property="createdBySSO"/>');">
											<img src="html/images/ico_attach.gif" alt="Manage Attachments" width="23" height="25" border="0" align="right">
										</a>
										</td>
									</tr>
								</table>
							</logic:lessThan>
						</td>
			      	</tr>
			      	<tr>
						<td>&nbsp;</td>
						<td colspan="2" align="right" style="width: 45%"><logic:present
								name="dmattachmentList" scope="request">
								<logic:notEmpty name="dmattachmentList">
									<fieldset>
										<table width="100%" class="other">
											<logic:iterate name="dmattachmentList" id="attachmentData"
												indexId="idx"
												type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<tr>
													<td><a href="javascript:download('<bean:write name="attachmentData" property="key"/>', '<bean:write name="attachmentData" property="value"/>', '<bean:write name="attachmentData" property="value2"/>');">
															<bean:write name="attachmentData" property="value" />.<bean:write name="attachmentData" property="value2" />
													</a></td>
												</tr>
											</logic:iterate>
										</table>
									</fieldset>
								</logic:notEmpty>
							</logic:present>
						</td>
					</tr>
				</table>
				
				<br>
				
				<div style="clear: both; margin-bottom: 30px; float: left; width: 100%;">
					<div style="width: 100%; float: left;">
						<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
							<tr>
								<td>
									<fieldset>
										<legend class="blueheader"> <bean:message key="quotation.viewenquiry.label.request.heading" /> </legend>
										<table width="100%" border="0" align="center" cellpadding="0" cellspacing="3">
											<tr>
												<td class="normal"><bean:message key="quotation.create.label.customer" /></td>
												<td class="normal">
													<input id="customerName" name="customerName" type="text" size="50" class="normal" autocomplete="off" value="<bean:write name="newenquiryForm" property="customerName"/>" />
													<div class="note hidetable" id="invalidcustomer" style="text-align: left; padding-left: 20px">Invalid Customer Name </div> 
													<html:hidden property="customerId" styleId="customerId" />
												</td>
												<td class="dividingdots">&nbsp;</td>
												<td>&nbsp;</td>
												<td class="normal"><bean:message key="quotation.create.label.deptauth" /></td>
												<td class="normal"><bean:write name="newenquiryForm" property="deptAndAuth" /></td>
											</tr>
											<tr>
												<td class="normal"><bean:message key="quotation.create.label.enduser" /></td>
												<td><input id="endUser" name="endUser" type="text" class="normal" maxlength="20" value='<bean:write name="newenquiryForm" property="endUser" filter="false" />' /></td>
												<td class="dividingdots">&nbsp;</td>
												<td>&nbsp;</td>
												<td class="normal"><bean:message key="quotation.create.label.projectname" /></td>
												<td><input id="projectName" name="projectName" class="normal" maxlength="30" value='<bean:write name="newenquiryForm" property="projectName" filter="false" />' /></td>
											</tr>
											<tr>
												<td class="normal"><bean:message key="quotation.create.label.revision" /></td>
												<td><input id="revision" name="revision" class="readonlymini" readonly="true" maxlength="5" value='<bean:write name="ratingObj" property="ratingNo" filter="false" />' /></td>
												<td class="dividingdots">&nbsp;</td>
												<td>&nbsp;</td>
												<td class="normal"><bean:message key="quotation.create.label.enquiryreferencenumber" /></td>
												<td><input id="enquiryReferenceNumber" name="enquiryReferenceNumber"class="normal" maxlength="30" value='<bean:write name="newenquiryForm" property="enquiryReferenceNumber" filter="false" />' /></td>
											</tr>
											<tr>
												<td class="normal"><bean:message key="quotation.create.label.issuedate" /></td>
												<td>
													<input id="lastModifiedDate" name="lastModifiedDate" class="readonlymini" readonly="true" maxlength="30" />
													<img src="html/images/popupCalen.gif" class="hand" width="19" height="22" border="0" align="absmiddle" onclick="return showCalendar('lastModifiedDate', '%m/%d/%Y', '24', true);" />
												</td>
												<td class="dividingdots">&nbsp;</td>
												<td>&nbsp;</td>
												<td class="normal">Quotation Required By Date</td>
												<td>
													<input id="submissionDate" name="submissionDate" class="readonlymini" readonly="true" maxlength="30" />
													<img src="html/images/popupCalen.gif" class="hand" width="19" height="22" border="0" align="absmiddle" onclick="return showCalendar('lastModifiedDate', '%m/%d/%Y', '24', true);" />
												</td>
											</tr>
											<tr>
												<td class="normal"><bean:message key="quotation.create.label.savingindent" /></td>
												<td><input id="savingIndent" name="savingIndent" class="readonlymini" readonly="true" maxlength="30" /></td>
												<td class="dividingdots">&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
											</tr>
											<tr>
												<td colspan="6"> &nbsp; </td>
											</tr>
											<tr>
												<td style="background:#ffff00;"> Datasheet </td>
												<td>
													<select name="dataSheet" id="dataSheet" title="Datasheet" class="normal" >
														<option value="0"><bean:message key="quotation.option.select" /></option>
														<logic:present name="newenquiryForm" property="ltDataSheetList" >
															<bean:define name="newenquiryForm" property="ltDataSheetList" id="ltDataSheetList" type="java.util.Collection" />
															<logic:iterate name="ltDataSheetList" id="ltDataSheet" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																<option value='<bean:write name="ltDataSheet" property="key" />' > <bean:write name="ltDataSheet" property="value" /> </option>
															</logic:iterate>
														</logic:present>
													</select>
												</td>
												<script> selectLstItemById('dataSheet', '<bean:write name="ratingObj" property="ltDataSheet" filter="false" />'); </script>
												<td class="dividingdots">&nbsp;</td>
												<td>&nbsp;</td>
												<td style="background:#ffff00;"> Motor GA </td>
												<td>
													<select name="motorGA" id="motorGA" title="Motor GA" class="normal" >
														<option value="0"><bean:message key="quotation.option.select" /></option>
														<logic:present name="newenquiryForm" property="alTargetedList" >
															<bean:define name="newenquiryForm" property="alTargetedList" id="alTargetedList" type="java.util.Collection" />
															<logic:iterate name="alTargetedList" id="alTargeted" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																<option value='<bean:write name="alTargeted" property="key" />' > <bean:write name="alTargeted" property="value" /> </option>
															</logic:iterate>
														</logic:present>
													</select>
												</td>
												<script> selectLstItemById('motorGA', '<bean:write name="ratingObj" property="ltMotorGA" filter="false" />'); </script>
											</tr>
											<tr>
												<td style="background:#ffff00;"> Performance Curves </td>
												<td>
													<select name="perfCurves" id="perfCurves" title="Performance Curves" >
														<option value="0"><bean:message key="quotation.option.select" /></option>
														<logic:present name="newenquiryForm" property="ltPerfCurvesList" >
															<bean:define name="newenquiryForm" property="ltPerfCurvesList" id="ltPerfCurvesList" type="java.util.Collection" />
															<logic:iterate name="ltPerfCurvesList" id="ltPerfCurves" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																<option value='<bean:write name="ltPerfCurves" property="key" />' > <bean:write name="ltPerfCurves" property="value" /> </option>
															</logic:iterate>
														</logic:present>
													</select>
												</td>
												<script> selectLstItemById('perfCurves', '<bean:write name="ratingObj" property="ltPerfCurves" filter="false" />'); </script>
												<td class="dividingdots">&nbsp;</td>
												<td>&nbsp;</td>
												<td style="background:#ffff00;"> T.Box GA </td>
												<td>
													<select name="tBoxGA" id="tBoxGA" title="T.Box GA" >
														<option value="0"><bean:message key="quotation.option.select" /></option>
														<logic:present name="newenquiryForm" property="alTargetedList" >
															<bean:define name="newenquiryForm" property="alTargetedList" id="alTargetedList" type="java.util.Collection" />
															<logic:iterate name="alTargetedList" id="alTargeted" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																<option value='<bean:write name="alTargeted" property="key" />' > <bean:write name="alTargeted" property="value" /> </option>
															</logic:iterate>
														</logic:present>
													</select>
												</td>
												<script> selectLstItemById('tBoxGA', '<bean:write name="ratingObj" property="ltTBoxGA" filter="false" />'); </script>
											</tr>
											<tr>
												<td colspan="6"> &nbsp; </td>
											</tr>
											<input type="hidden" name="showReplaceMotor" id="showReplaceMotor" value='<bean:write name="ratingObj" property="ltReplacementMotor" filter="false" />' />
											<div id="showReplaceMotorDivId" style="display:none">
											<logic:equal name="ratingObj" property="ltReplacementMotor" value="Yes">
											<tr>
												<td style="background:#ffff00;"> Replacement Motor </td>
												<td>
													<select name="isReplacementMotor" id="isReplacementMotor" title="Replacement Motor" style="pointer-events:none; font-weight:normal !important; BACKGROUND-COLOR: #e2e2e2;">
														<option value="0"><bean:message key="quotation.option.select" /></option>
														<logic:present name="newenquiryForm" property="alTargetedList" >
															<bean:define name="newenquiryForm" property="alTargetedList" id="alTargetedList" type="java.util.Collection" />
															<logic:iterate name="alTargetedList" id="alTargeted" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																<option value='<bean:write name="alTargeted" property="key" />' > <bean:write name="alTargeted" property="value" /> </option>
															</logic:iterate>
														</logic:present>
													</select>
												</td>
												<script> selectLstItemById('isReplacementMotor', '<bean:write name="ratingObj" property="ltReplacementMotor" filter="false" />'); </script>
												<td class="dividingdots">&nbsp;</td>
												<td>&nbsp;</td>
												<td style="background:#ffff00;"> Earlier Supplied Motor Serial No. </td>
												<td>
													<input type="text" name="earlierSuppliedMotorSerialNo" id="earlierSuppliedMotorSerialNo" class="normal" style="BACKGROUND-COLOR: #e2e2e2;" readonly value='<bean:write name="ratingObj" property="ltEarlierMotorSerialNo" filter="false" />' title="Earlier Supplied Motor Serial No." >
												</td>
											</tr>
											</logic:equal>
											</div>
										</table>
									</fieldset>
								</td>
								<td valign="top">
									<fieldset>
										<legend class="blueheader"> <bean:message key="quotation.offer.label.reassign" /> </legend>
										<table style="background-color: #FFFFFF">
											<tr>
												<td><bean:message key="quotation.offer.label.reassignto" /></td>
												<td>
													<select id="assignToId" name="assignToId" style="width:370px;">
														<option value="0"><bean:message key="quotation.option.select" /></option>
													</select>
												</td>
											</tr>
											<tr>
												<td><bean:message key="quotation.offer.label.comments" /></td>
												<td><textarea style="width: 370px; height: 55px" maxlength="250" name="assignRemarks" id="assignRemarks"></textarea> </td>
											</tr>
										</table>
										<div class="blue-light" align="right">
											<INPUT class=BUTTON onclick="javascript:reassign();" type=button value=Re-assign name=assign>
										</div>
									</fieldset>
								</td>

							</tr>
						</table>
					</div>
				</div> 
				
				<div style="clear:both; float:left; width:100%; height:32px; margin-bottom:30px;">
					<bean:define id="latestId"  name="newenquiryForm" property="currentRating"/>
					<div id="viewtopnav">
						<ul>
							<logic:notEmpty name="newenquiryForm" property="newRatingsList">
								<%!int count=0; %> <% String displayRatingStyle = ""; String displayRatingBorder = "";%>
								<logic:iterate name="newenquiryForm" property="newRatingsList" id="ratingListData" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
									<%  countval=countval+1; %>
									
									<logic:equal name="ratingListData" property="isReferDesign" value="Y">
										<% displayRatingStyle = "referDesignValid"; %>
									</logic:equal>
									<logic:notEqual name="ratingListData" property="isReferDesign" value="Y">
										<% displayRatingStyle = "referDesignNotValid"; %>
									</logic:notEqual>
									
									<bean:define id="selectedRatingId" name="ratingObj" property="ratingId"/>
									<bean:define id="currentRatingId" name="ratingListData" property="ratingId"/>
									
									<% 	
										//System.out.println("selectedRatingId = "+selectedRatingId);
										//System.out.println("currentRatingId = "+currentRatingId);
										if(selectedRatingId.equals(currentRatingId)) { 
											displayRatingBorder = " activeBtn";
										} else { 
											displayRatingBorder = " ";
										} 
									%>
									
									<a id="rating<%=idx.intValue()+1 %>" href="javascript:getNextNewRating('<bean:write name="ratingListData" property="ratingId" />', '<%=idx.intValue()+1 %>')"  class="<%=displayRatingStyle %> <%=displayRatingBorder %>">
										Rating <%=idx.intValue()+1 %>
									</a>
									&nbsp;&nbsp;
								</logic:iterate>
								
							</logic:notEmpty>
						</ul>
					</div>
				</div>
				
				<div style="width:100%; class="note" align="right" style="float:right; line-height:16px;">
					<font color="red"><bean:message key="quotation.label.manadatory"/></font>
				</div>
				
<!-- ---------------------------------------------------	Commercial offer Section : START	---------------------------------------------------------- -->
				
				<div style="width:100%; float:left; margin-bottom:30px; float:left;">
					<fieldset>
			            <legend class="blueheader"> <bean:message key="quotation.offerdata.commercial.label.heading"/> </legend>
			            <table width="100%" cellpadding="0" cellspacing="0" id="commercial_mto">
			            	<tr>
			            		<td style="width:10%" class="formLabelTophome"> Product Group <span class="mandatory">&nbsp;</span> </td>
			            		<td style="width:10%; padding:4px; border-bottom: 1px solid black;" >
			            			<select style="width:100px;" id="productGroup_MTO" name="productGroup_MTO" title="Product Group">
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present property="ltProductGroupList" name="newenquiryForm">
											<bean:define name="newenquiryForm" property="ltProductGroupList" id="ltProductGroupList" type="java.util.Collection" />
											<logic:iterate name="ltProductGroupList" id="ltProductGroup" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltProductGroup" property="key" />' > <bean:write name="ltProductGroup" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
			            		</td>
			            		<script> selectLstItemById('productGroup_MTO', '<bean:write name="ratingObj" property="ltProdGroupId_MTO" filter="false" />'); </script>
			            		<td style="width:1%; border-bottom: 1px solid black;" class="dividingdots">&nbsp;</td>
			            		<td style="width:1%; border-bottom: 1px solid black;">&nbsp;</td>
			            		<td style="width:9%" class="formLabelTophome"> Product Line <span class="mandatory">&nbsp;</span> </td>
			            		<td style="width:10%; padding:2px; border-bottom: 1px solid black; white-space: nowrap;	">
			            			&nbsp; &nbsp;
			            			<select style="width:100px;" id="productLine_MTO" name="productLine_MTO" title="Product Line" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present property="ltProductLineList" name="newenquiryForm">
											<bean:define name="newenquiryForm" property="ltProductLineList" id="ltProductLineList" type="java.util.Collection" />
											<logic:iterate name="ltProductLineList" id="ltProductLine" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltProductLine" property="key" />' > <bean:write name="ltProductLine" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
			            		</td>
			            		<script> selectLstItemById('productLine_MTO', '<bean:write name="ratingObj" property="ltProdLineId_MTO" filter="false" />'); </script>
			            		<td style="width:1%; border-bottom: 1px solid black;" class="dividingdots">&nbsp;</td>
			            		<td style="width:1%; border-bottom: 1px solid black;">&nbsp;</td>
			            		<td style="width:8%" class="formLabelTophome"> KW <span class="mandatory">&nbsp;</span> </td>
			            		<td style="width:10%; padding:2px; border-bottom: 1px solid black; white-space: nowrap;	">
			            			&nbsp; &nbsp;
			            			<select style="width:100px;" id="kw_MTO" name="kw_MTO" title="KW" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present property="ltKWList" name="newenquiryForm">
											<bean:define name="newenquiryForm" property="ltKWList" id="ltKWList" type="java.util.Collection" />
											<logic:iterate name="ltKWList" id="ltKW" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltKW" property="key" />' > <bean:write name="ltKW" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
			            		</td>
			            		<script> selectLstItemById('kw_MTO', '<bean:write name="ratingObj" property="ltKWId_MTO" filter="false" />'); </script>
			            		<td style="width:1%; border-bottom: 1px solid black;" class="dividingdots">&nbsp;</td>
			            		<td style="width:1%; border-bottom: 1px solid black;">&nbsp;</td>
			            		<td style="width:8%" class="formLabelTophome"> Frame <span class="mandatory">&nbsp;</span> </td>
			            		<td style="width:10%; padding:4px; border-bottom: 1px solid black;">
			            			<select style="width:100px;" id="frame_MTO" name="frame_MTO" title="Frame" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present property="ltFrameList" name="newenquiryForm">
											<bean:define name="newenquiryForm" property="ltFrameList" id="ltFrameList" type="java.util.Collection" />
											<logic:iterate name="ltFrameList" id="ltFrame" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltFrame" property="key" />' > <bean:write name="ltFrame" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
			            		</td>
			            		<script> selectLstItemById('frame_MTO', '<bean:write name="ratingObj" property="ltFrameId_MTO" filter="false" />'); </script>
			            		<td style="width:1%; border-bottom: 1px solid black;" class="dividingdots">&nbsp;</td>
			            		<td style="width:1%; border-bottom: 1px solid black;">&nbsp;</td>
			            		<td style="width:8%" class="formLabelTophome"> Pole <span class="mandatory">&nbsp;</span> </td>
			            		<td style="width:10%; padding:4px; border-bottom: 1px solid black;">
			            			<select style="width:100px;" id="pole_MTO" name="pole_MTO" title="No. of Poles" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present property="ltPoleList" name="newenquiryForm">
											<bean:define name="newenquiryForm" property="ltPoleList" id="ltPoleList" type="java.util.Collection" />
											<logic:iterate name="ltPoleList" id="ltPole" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltPole" property="key" />' > <bean:write name="ltPole" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
			            		</td>
			            		<script> selectLstItemById('pole_MTO', '<bean:write name="ratingObj" property="ltPoleId_MTO" filter="false" />'); </script>
			            	</tr>
			            	<tr>
			            		<td style="width:10%" class="formLabelTophome"> Special Add On % </td>
			            		<td style="width:10%; padding:4px; border-bottom: 1px solid black;">
			            			<input name="specialAddOn_MTO" id="specialAddOn_MTO" style="width:100px;" />
			            		</td>
			            		<td style="width:1%; border-bottom: 1px solid black;" class="dividingdots">&nbsp;</td>
			            		<td style="width:1%; border-bottom: 1px solid black;">&nbsp;</td>
			            		<td style="width:10%" class="formLabelTophome"> Special Cash Extra </td>
			            		<td style="width:10%; padding:2px; border-bottom: 1px solid black; white-space: nowrap;	">
			            			&#8377; &nbsp; <input name="specialCashExtra_MTO" id="specialCashExtra_MTO" style="width:100px;" />
			            		</td>
			            		<td style="width:1%; border-bottom: 1px solid black;" class="dividingdots">&nbsp;</td>
			            		<td style="width:1%; border-bottom: 1px solid black;">&nbsp;</td>
			            		<td style="width:8%" class="formLabelTophome"> % Copper </td>
			            		<td style="width:10%; padding:2px; border-bottom: 1px solid black;">
			            			&nbsp; &nbsp; <input name="percentCopper_MTO" id="percentCopper_MTO" style="width:100px;" />
			            		</td>
			            		<td style="width:1%; border-bottom: 1px solid black;" class="dividingdots">&nbsp;</td>
			            		<td style="width:1%; border-bottom: 1px solid black;">&nbsp;</td>
			            		<td style="width:8%" class="formLabelTophome"> % Stamping </td>
			            		<td style="width:10%; padding:4px; border-bottom: 1px solid black;">
			            			<input name="percentStamping_MTO" id="percentStamping_MTO" style="width:100px;" />
			            		</td>
			            		<td style="width:1%; border-bottom: 1px solid black;" class="dividingdots">&nbsp;</td>
			            		<td style="width:1%; border-bottom: 1px solid black;">&nbsp;</td>
			            		<td style="width:8%" class="formLabelTophome"> % Aluminium </td>
			            		<td style="width:10%; padding:4px; border-bottom: 1px solid black;">
			            			<input name="percentAluminium_MTO" id="percentAluminium_MTO" style="width:100px;" />
			            		</td>
			            	</tr>
			            	<tr>
								<td style="width:9%" class="formLabelTophome"> Total Add On % </td>
			            		<td style="width:10%; padding:4px; border-bottom: 1px solid black;">
			            			<input type="text" name="totalAddOn_MTO" id="totalAddOn_MTO" style="width:100px;" value='<bean:write name="ratingObj" property="ltTotalAddOn_MTO" filter="false" />' title='Actual Add-on % : <bean:write name="ratingObj" property="ltTotalAddonPercent" filter="false" /> ' />
			            			<input type="hidden" name="totalAddOn_MTO_H" id="totalAddOn_MTO_H" value='<bean:write name="ratingObj" property="ltTotalAddonPercent" filter="false" />' />
			            		</td>
			            		<td style="width:1%; border-bottom: 1px solid black;" class="dividingdots">&nbsp;</td>
			            		<td style="width:1%; border-bottom: 1px solid black;">&nbsp;</td>
			            		<td style="width:9%" class="formLabelTophome"> Total Cash Extra </td>
			            		<td style="width:10%; padding:2px; border-bottom: 1px solid black; white-space: nowrap;">
			            			&#8377; &nbsp; <input type="text" name="totalCashExtra_MTO" id="totalCashExtra_MTO" style="width:100px;" value='<bean:write name="ratingObj" property="ltTotalCashExtra_MTO" filter="false" />' title='Actual Cash Extra : <bean:write name="ratingObj" property="ltTotalCashExtra" filter="false" /> ' />
			            			<input type="hidden" name="totalCashExtra_MTO_H" id="totalCashExtra_MTO_H" value='<bean:write name="ratingObj" property="ltTotalCashExtra" filter="false" />' />
			            		</td>
			            		<td style="width:1%; border-bottom: 1px solid black;" class="dividingdots">&nbsp;</td>
			            		<td style="width:1%; border-bottom: 1px solid black;">&nbsp;</td>	
			            		<td style="width:8%" class="formLabelTophome"> Material Cost </td>	
			            		<td style="width:10%; padding:2px; border-bottom: 1px solid black; white-space: nowrap;">
			            			&#8377; &nbsp; <input name="matCost_MTO" id="matCost_MTO" style="width:100px;" />
			            		</td>
			            		<td style="width:1%; border-bottom: 1px solid black;" class="dividingdots">&nbsp;</td>
			            		<td style="width:1%; border-bottom: 1px solid black;">&nbsp;</td>
			            		<td style="width:8%" class="formLabelTophome"> &nbsp; </td>	
			            		<td style="width:10%; padding:4px; border-bottom: 1px solid black;"> &nbsp; </td> 
			            		<td style="width:1%; border-bottom: 1px solid black;" class="dividingdots">&nbsp;</td>
			            		<td style="width:1%; border-bottom: 1px solid black;">&nbsp;</td>
			            		<td style="width:8%" class="formLabelTophome"> &nbsp; </td>	
			            		<td style="width:10%; padding:4px; border-bottom: 1px solid black;"> &nbsp; </td>           		
			            	</tr>
			            	<tr>
			            	<td style="width:100%" colspan="18"> &nbsp; </td>
			            	</tr>
			            </table>
			    	</fieldset>
<!-- ---------------------------------------------------	Commercial offer Section : END	-------------------------------------------------------------- -->

<!-- ---------------------------------------------------	Add On Section : START	---------------------------------------------------------------------- -->			    	
			    	<fieldset>
			    		<legend class="blueheader"> <bean:message key="quotation.offerdata.addon.label.heading"/> </legend>
						<table width="100%" cellpadding="0" cellspacing="0" id="addons">
							<tr>
								<td style="width:35%" class="formLabelTophome"> <bean:message key="quotation.offerdata.addon.label.addontype" /> </td>
								<td style="width:15%" class="formLabelTophome"> <bean:message key="quotation.offerdata.addon.label.quantity" /> </td>
								<td style="width:15%" class="formLabelTophome"> Add-on % </td>
								<td style="width:20%" class="formLabelTophome"> Add-on Cash Extra </td>
								<td style="width:15%" class="formLabelTophome"> <bean:message key="quotation.offerdata.addon.label.delete" /> </td>
							</tr>
							
							<logic:notEmpty name="newenquiryForm" property="addOnList">
								<input type="hidden" name="totalRowCount" id="totalRowCount" value="1"/>
								<% int k = 1; %>
								<logic:iterate name="newenquiryForm" property="addOnList" id="addOnDetails" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.AddOnDto">
									<% k = idx.intValue() + 1; %>
									<tr>
										<td style="width:35%; padding:4px; border-bottom: 1px solid black;">
											<div class="data-list-input" style="width:300px; float:left;">
												<select name="addOnType<%=idx.intValue() + 1%>" id="addOnType<%=idx.intValue() + 1%>" class="normal data-list-input width-select">
													<option value=""><bean:message key="quotation.option.select" /></option>
													<option value="Load Test">Load Test</option>
													<option value="Surge& Impulse test on sample coil">Surge&Impulse test on sample coil</option>
													<option value="Tan Delta Test">Tan Delta Test</option>
													<option value="Ring loop Flux test">Ring loop Flux test</option>
												</select> 
												<input name="addOnTypeText<%=idx.intValue() + 1%>" id="addOnTypeText<%=idx.intValue() + 1%>" class="normal data-list-input input-class" maxlength="100"
													value='<bean:write name="addOnDetails" property="addOnTypeText" />' />
											</div>
										</td>
										<td style="width:15%; padding:4px; border-bottom: 1px solid black;"> 
											<input name="addOnQuantity<%=idx.intValue() + 1%>" id="addOnQuantity<%=idx.intValue() + 1%>" class='short' maxlength="2" onkeypress="return isNumber(event)" value='<bean:write name="addOnDetails" property="addOnQuantity" />' />
										</td>
										<td style="width:15%; padding:4px; border-bottom: 1px solid black;">
											<input name="addOnPercent<%=idx.intValue() + 1%>" id="addOnPercent<%=idx.intValue() + 1%>" class='short' maxlength="2" onkeypress="return isNumber(event)" onblur="javascript:calcTotalAddOnMTO('<%=idx.intValue() + 1%>');" value='<bean:write name="addOnDetails" property="addOnPercent" />' />
										</td>
										<td style="width:20%; padding:4px; border-bottom: 1px solid black;">
											&#8377; &nbsp; <input name="addOnUnitPrice<%=idx.intValue() + 1%>" id="addOnUnitPrice<%=idx.intValue() + 1%>" class='normal' maxlength="8" onkeypress="return isNumber(event)" onblur="javascript:calcTotalCashExtraMTO('<%=idx.intValue() + 1%>');" value='<bean:write name="addOnDetails" property="addOnUnitPrice" />' />
										</td>
										<td style="width:15%; padding:4px; border-bottom: 1px solid black;">
											<% if (k > 0) { %> 
												<img src="html/images/deleteicon.gif" id="delete<%=idx.intValue() + 1%>" alt="Delete" onClick="removeRow(this);" style="cursor: hand;" /> 
											<% } %>
										</td>
									</tr>
								</logic:iterate>
								<input type="hidden" name="hidRowIndex" id="hidRowIndex" value="<%=k%>">
							</logic:notEmpty>
							<logic:empty name="newenquiryForm" property="addOnList">
								<input type="hidden" name="totalRowCount" id="totalRowCount" value="1"/>
								<tr>
									<td style="width:35%; padding:4px; border-bottom: 1px solid black;">
										<div class="data-list-input" style="width:300px; float:left;">
											<select name="addOnType1" id="addOnType1" class="normal data-list-input width-select" onchange="javascript:resetAddOnTypeText('1');">
												<option value=""><bean:message key="quotation.option.select" /></option>
												<option value="Load Test">Load Test</option>
												<option value="Surge and Impulse test on sample coil">Surge & Impulse test on sample coil</option>
												<option value="Tan Delta Test">Tan Delta Test</option>
												<option value="Ring loop Flux test">Ring loop Flux test</option>
											</select> 
											<input type="text" name="addOnTypeText1" id="addOnTypeText1" class="normal data-list-input input-class" maxlength="100">
										</div>
									</td>
									<td style="width:15%; padding:4px; border-bottom: 1px solid black;">
										<input id="addOnQuantity1" name="addOnQuantity1" onkeypress="return isNumber(event)" maxLength="2" class="short" >
									</td>
									<td style="width:15%; padding:4px; border-bottom: 1px solid black;">
										<input id="addOnPercent1" name="addOnPercent1" onkeypress="return isNumber(event)" maxLength="2" class="short" >
									</td>
									<td style="width:20%; padding:4px; border-bottom: 1px solid black;">
										&#8377; &nbsp; <input id="addOnUnitPrice1" name="addOnUnitPrice1" onkeypress="return isNumber(event)" maxLength="8" class="normal" > 
									</td>
									<td style="width:15%; padding:4px; border-bottom: 1px solid black;">
										<img src="html/images/deleteicon.gif" alt="Delete" onClick="removeRow(this);" id="delete1" style="cursor: hand;" />
									</td>
								</tr>
								<input type="hidden" name="hidRowIndex" id="hidRowIndex" value="1">
							</logic:empty>
						</table>
						<br/>
						<div id="add1" class="blue-light" align="right" style="padding-right:25px; width:100%;">
			      			<input style="width:100px; font-weight:bold; font-size:11px; color: #990000;" type="button" class="BUTTON" onclick="javascript:addrowcreateoffer('addons');" value="Add" />
			      		</div>
			    	</fieldset>
<!-- ---------------------------------------------------	Add On Section : END	---------------------------------------------------------------------- -->

<!-- ---------------------------------------------------	Special Features Section : START	---------------------------------------------------------- -->			    	
			    	<fieldset style="width:100%">
						<legend class="blueheader">	<bean:message key="quotation.create.label.specialfeatures"/> </legend>
						<div>
							<table  border="0" cellspacing="0" cellpadding="0">
			               		<tr><td>&nbsp;</td></tr>
						   		<tr style="float:left; width:100%;">				  
				                    <td>
				                    	<textarea name="splFeatures" id="splFeatures"  style="width:1135px; height:50px" onkeyup="checkMaxLenghtText(this, 2500);" onkeydown="checkMaxLenghtText(this, 2500);" tabindex="35">   </textarea>
				                    </td>
			                  	</tr>
					    	</table>
					    </div>
		    		</fieldset>
<!-- ---------------------------------------------------	Special Features Section : END	-------------------------------------------------------------- -->			    		
		    		
<!-- ---------------------------------------------------	Deviation And Clarification Section : START	-------------------------------------------------- -->			  
					<fieldset style="width:100%">
						<legend class="blueheader">	<bean:message key="quotation.create.label.commentsanddeviations"/> </legend>
					 	<div>
							<table  border="0" cellspacing="0" cellpadding="0">
			                  	<tr><td>&nbsp;</td></tr>
						     	<tr style="float:left; width:100%;">				  
			                    	<td>
			                    		<textarea name="deviationComments" id="deviationComments" style="width:1135px; height:50px" onkeyup="checkMaxLenghtText(this, 10000);" onkeydown="checkMaxLenghtText(this, 10000);" tabindex="35"> <bean:write name="newenquiryForm" property="commentsDeviations" filter="false" /> </textarea>
			                    	</td>
			                  	</tr>
				    		</table>
				    	</div>
				    </fieldset>
<!-- ---------------------------------------------------	Deviation And Clarification Section : END	-------------------------------------------------- -->
				    
<!-- ---------------------------------------------------	Notes Section : START	---------------------------------------------------------------------- -->				    
				    <fieldset style="width:100%">
				    	<legend class="blueheader">	<bean:message key="quotation.create.label.notesection"/> </legend>
				    	<div>
				  			<table  border="0" cellspacing="0" cellpadding="0">
								<tr><td colspan="4">&nbsp;</td></tr>
							    <tr style="float:left; width:100%;">				  
									<td class="formLabelrating" style="width:138px; margin-right:33px;">
								    	<bean:message key="quotation.create.label.smnote"/>
								    </td>
				                    <td colspan="3">
										<textarea  name="smNote" id="smNote"  style="width:1055px; height:50px; BACKGROUND-COLOR: #e2e2e2;" readonly="true" tabindex="35" /> <bean:write name="newenquiryForm"  property="smNote" filter="false" />  </textarea>         
				               		</td>
				           		</tr>
				                <tr><td colspan="4">&nbsp;</td></tr>
				                <tr style="float:left; width:100%;">
				                	<td class="formLabelrating" style="width:138px; margin-right:33px;">
								    	<bean:message key="quotation.create.label.rsmnote"/>
								    </td>
				                    <td colspan="3">
										<textarea  name="rsmNote" id="rsmNote"  style="width:1055px; height:50px; BACKGROUND-COLOR: #e2e2e2;" readonly="true" tabindex="35" /> <bean:write name="newenquiryForm"  property="rsmNote" filter="false" /> </textarea>         
				               		</td>
				               	</tr>
				                <tr><td colspan="4">&nbsp;</td></tr>
				                <tr style="float:left; width:100%;">
				                	<td class="formLabelrating" style="width:138px; margin-right:33px;">
								    	<bean:message key="quotation.create.label.nsmnote"/>
								    </td>
				                    <td colspan="3">
										<textarea  name="nsmNote" id="nsmNote"  style="width:1055px; height:50px; BACKGROUND-COLOR: #e2e2e2;" readonly="true" tabindex="35" /> <bean:write name="newenquiryForm"  property="nsmNote" filter="false" /> </textarea>         
				               		</td>
				               	</tr>
				                <tr><td colspan="4">&nbsp;</td></tr>
				                <tr style="float:left; width:100%;">
				                	<td class="formLabelrating" style="width:138px; margin-right:33px;">
								    	<bean:message key="quotation.create.label.mhnote"/>
								    </td>
				                    <td colspan="3">
										<textarea  name="mhNote" id="mhNote"  style="width:1055px; height:50px; BACKGROUND-COLOR: #e2e2e2;" readonly="true" tabindex="35" /> <bean:write name="newenquiryForm"  property="mhNote" filter="false" />  </textarea>         
				               		</td>
				               	</tr>
				                <tr><td colspan="4">&nbsp;</td></tr>
							    <tr style="float:left; width:100%;">				  
									<td class="formLabelrating" style="width:138px; margin-right:33px;">
								    	<bean:message key="quotation.create.label.dmnote"/>
								    </td>
				                    <td colspan="3">
				                    	<textarea name="dmNote" id="dmNote" style="width:1055px; height:50px" onkeyup="checkMaxLenghtText(this, 4000);" onkeydown="checkMaxLenghtText(this, 4000);" tabindex="35"/> </textarea>  
				                    </td>
				            	</tr>
				                  
						    </table>
		    			</div>
				    </fieldset>
<!-- ---------------------------------------------------	Notes Section : END	-------------------------------------------------------------------------- -->
				    
				</div>


<!-- ---------------------------------------------------	Technical Offer Section : START	-------------------------------------------------------------- -->				
				
				<div style="width:100%; float:left; margin-bottom:10px;">
				
					<fieldset>
						<legend class="blueheader"><bean:message key="quotation.offerdata.technical.label.heading"/></legend>
						
						<table width="100%" border="0" align="center" cellpadding="0" cellspacing="3">
							<tr>
								<td width="20%" class="label-text" style="background: #CCD8F2;"> Model Number</td>
								<td width="20%" class="normal" > <input id="modelNumber" name="modelNumber" class="normal" maxlength="30" /></td>
								<td width="10%">&nbsp;</td>
								<td width="20%">&nbsp;</td>
								<td width="20%">&nbsp;</td>
							</tr>
							<tr>
								<td width="20%" class="label-text"><bean:message key="quotation.offerdata.addon.label.quantity"/></td>
								<td width="20%" class="normal"><input id="quantity" name="quantity" class="normal" maxlength="2" value="<bean:write name="ratingObj" property="quantity" filter="false" />" /></td>
								<td width="10%">&nbsp;</td>
								<td width="20%" class="label-text" style="background:#ffff00;">Actual Product Group <span class="mandatory">&nbsp;</span></td>
								<td width="20%" class="normal">
									<select style="width:150px;" id="productGroup" name="productGroup" title="Product Group">
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present property="ltProductGroupList" name="newenquiryForm">
											<bean:define name="newenquiryForm" property="ltProductGroupList" id="ltProductGroupList" type="java.util.Collection" />
											<logic:iterate name="ltProductGroupList" id="ltProductGroup" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltProductGroup" property="key" />' > <bean:write name="ltProductGroup" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
							</tr>
							<script> selectLstItemById('productGroup', '<bean:write name="ratingObj" property="ltProdGroupId" filter="false" />'); </script>
							<tr>
								<td width="20%" class="label-text" style="background:#ffff00;">Actual Product Line <span class="mandatory">&nbsp;</span></td>
								<td width="20%" class="normal">
									<select style="width:150px;" id="productLine" name="productLine" title="Product Line" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present property="ltProductLineList" name="newenquiryForm">
											<bean:define name="newenquiryForm" property="ltProductLineList" id="ltProductLineList" type="java.util.Collection" />
											<logic:iterate name="ltProductLineList" id="ltProductLine" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltProductLine" property="key" />' > <bean:write name="ltProductLine" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
								<td width="10%">&nbsp;</td>
								<td width="20%" class="label-text" style="background:#ffff00;">Actual KW <span class="mandatory">&nbsp;</span></td>
								<td width="20%" class="normal">
									<select style="width:150px;" id="kw" name="kw" title="KW" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present property="ltKWList" name="newenquiryForm">
											<bean:define name="newenquiryForm" property="ltKWList" id="ltKWList" type="java.util.Collection" />
											<logic:iterate name="ltKWList" id="ltKW" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltKW" property="key" />' > <bean:write name="ltKW" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
							</tr>
							<script> selectLstItemById('productLine', '<bean:write name="ratingObj" property="ltProdLineId" filter="false" />'); </script>
							<script> selectLstItemById('kw', '<bean:write name="ratingObj" property="ltKWId" filter="false" />'); </script>
							<tr>
								<td width="20%" class="label-text" style="background:#ffff00;">Actual Pole <span class="mandatory">&nbsp;</span></td>
								<td width="20%" class="normal">
									<select style="width:150px;" id="pole" name="pole" title="No. of Poles" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present property="ltPoleList" name="newenquiryForm">
											<bean:define name="newenquiryForm" property="ltPoleList" id="ltPoleList" type="java.util.Collection" />
											<logic:iterate name="ltPoleList" id="ltPole" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltPole" property="key" />' > <bean:write name="ltPole" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
								<td width="10%">&nbsp;</td>
								<td width="20%" class="label-text" style="background:#ffff00;">Actual Frame <span class="mandatory">&nbsp;</span></td>
								<td width="20%" class="normal">
									<select style="width:150px;" id="frame" name="frame" title="Frame" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present property="ltFrameList" name="newenquiryForm">
											<bean:define name="newenquiryForm" property="ltFrameList" id="ltFrameList" type="java.util.Collection" />
											<logic:iterate name="ltFrameList" id="ltFrame" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltFrame" property="key" />' > <bean:write name="ltFrame" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
							</tr>
							<script> selectLstItemById('pole', '<bean:write name="ratingObj" property="ltPoleId" filter="false" />'); </script>
							<script> selectLstItemById('frame', '<bean:write name="ratingObj" property="ltFrameId" filter="false" />'); </script>
							<tr>
								<td width="20%" class="label-text">Mounting <span class="mandatory">&nbsp;</span></td>
								<td width="20%" class="normal">
									<select style="width:150px;" id="mounting" name="mounting" title="Mounting" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present property="ltMountingList" name="newenquiryForm">
											<bean:define name="newenquiryForm" property="ltMountingList" id="ltMountingList" type="java.util.Collection" />
											<logic:iterate name="ltMountingList" id="ltMounting" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltMounting" property="key" />' > <bean:write name="ltMounting" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
								<td width="10%">&nbsp;</td>
								<td width="20%" class="label-text">T.Box Position <span class="mandatory">&nbsp;</span></td>
								<td width="20%" class="normal">
									<select style="width:150px;" id="tbPosition" name="tbPosition" title="TB Position" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present property="ltTBPositionList" name="newenquiryForm">
											<bean:define name="newenquiryForm" property="ltTBPositionList" id="ltTBPositionList" type="java.util.Collection" />
											<logic:iterate name="ltTBPositionList" id="ltTBPosition" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltTBPosition" property="key" />' > <bean:write name="ltTBPosition" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
							</tr>
							<script> selectLstItemById('mounting', '<bean:write name="ratingObj" property="ltMountingId" filter="false" />'); </script>
							<script> selectLstItemById('tbPosition', '<bean:write name="ratingObj" property="ltTBPositionId" filter="false" />'); </script>
							<tr>
								<td width="20%" class="label-text">Motor Type</td>
								<td width="20%" class="normal">
									<select style="width:150px;" id="typeOfMotor" name="typeOfMotor" title="Type of Motor" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present property="ltMotorTypeList" name="newenquiryForm">
											<bean:define name="newenquiryForm" property="ltMotorTypeList" id="ltMotorTypeList" type="java.util.Collection" />
											<logic:iterate name="ltMotorTypeList" id="ltMotorType" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltMotorType" property="key" />' > <bean:write name="ltMotorType" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
								<td width="10%">&nbsp;</td>
								<td width="20%" class="label-text"><bean:message key="quotation.create.label.application"/></td>
								<td width="20%" class="normal">
									<select style="width:150px;" name="application" id="application" title="Application" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present property="ltApplicationList" name="newenquiryForm">
											<bean:define name="newenquiryForm" property="ltApplicationList" id="ltApplicationList" type="java.util.Collection" />
											<logic:iterate name="ltApplicationList" id="ltApplication" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltApplication" property="key" />' > <bean:write name="ltApplication" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
							</tr>
							<script> selectLstItemById('typeOfMotor', '<bean:write name="ratingObj" property="ltMotorTypeId" filter="false" />'); </script>
							<script> selectLstItemById('application', '<bean:write name="ratingObj" property="ltApplicationId" filter="false" />'); </script>
							<tr>
								<td width="20%" class="label-text"><bean:message key="quotation.create.label.volts"/> <span class="mandatory">&nbsp;</span></td>	
								<td width="20%" class="normal" width="20%">
									<select name="volt" id="volt" title="Voltage" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present property="ltVoltageList" name="newenquiryForm">
											<bean:define name="newenquiryForm" property="ltVoltageList" id="ltVoltageList" type="java.util.Collection" />
											<logic:iterate name="ltVoltageList" id="ltVoltage" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltVoltage" property="key" />' > <bean:write name="ltVoltage" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
								<td width="10%">&nbsp;</td>
								<td width="20%" class="label-text"> <bean:message key="quotation.create.label.frequency"/> <span class="mandatory">&nbsp;</span> </td>
								<td width="20%" class="normal" >
									<select name="freq" id="freq" title="Frequency">
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present property="ltFrequencyList" name="newenquiryForm">
											<bean:define name="newenquiryForm" property="ltFrequencyList" id="ltFrequencyList" type="java.util.Collection" />
											<logic:iterate name="ltFrequencyList" id="ltFrequency" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltFrequency" property="key" />' > <bean:write name="ltFrequency" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>  
								</td>
							</tr>
							<script> selectLstItemById('volts', '<bean:write name="ratingObj" property="ltVoltId" filter="false" />'); </script>
							<script> selectLstItemById('freq', '<bean:write name="ratingObj" property="ltFreqId" filter="false" />'); </script>
							<tr>
								<td width="20%" class="label-text"> <bean:message key="quotation.create.label.volts"/> Var. +/- <span class="mandatory">&nbsp;</span></td>
								<td width="20%" class="normal">
									<select style="width: 40px;" name="voltAddVariation" id="voltAddVariation" title="Voltage Add Variation">
										<logic:present property="ltVoltVariationList" name="newenquiryForm">
											<bean:define name="newenquiryForm" property="ltVoltVariationList" id="ltVoltVariationList" type="java.util.Collection" />
											<logic:iterate name="ltVoltVariationList" id="ltVoltVariation" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltVoltVariation" property="key" />' > <bean:write name="ltVoltVariation" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select> <span style="font-weight:bold;">%</span>
									<select style="width: 40px;" name="voltRemoveVariation" id="voltRemoveVariation" title="Voltage Remove Variation">
										<logic:present property="ltVoltVariationList" name="newenquiryForm">
											<bean:define name="newenquiryForm" property="ltVoltVariationList" id="ltVoltVariationList" type="java.util.Collection" />
											<logic:iterate name="ltVoltVariationList" id="ltVoltVariation" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltVoltVariation" property="key" />' > <bean:write name="ltVoltVariation" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select> <span style="font-weight:bold;">%</span>
								</td>
								<td width="10%">&nbsp;</td>
								<td width="20%" class="label-text"> <bean:message key="quotation.create.label.frequency"/>  Var. +/- <span class="mandatory">&nbsp;</span></td>
								<td width="20%" class="normal">
									<select style="width: 40px;" name="freqAddVariation" id="freqAddVariation" title="Frequency Add Variation">
										<logic:present property="ltFreqVariationList" name="newenquiryForm">
										<bean:define name="newenquiryForm" property="ltFreqVariationList" id="ltFreqVariationList" type="java.util.Collection" />
											<logic:iterate name="ltFreqVariationList" id="ltFreqVariation" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltFreqVariation" property="key" />' > <bean:write name="ltFreqVariation" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select> <span style="font-weight:bold;">%</span>
									<select style="width: 40px;" name="freqRemoveVariation" id="freqRemoveVariation" title="Frequency Remove Variation" >
										<logic:present property="ltFreqVariationList" name="newenquiryForm">
											<bean:define name="newenquiryForm" property="ltFreqVariationList" id="ltFreqVariationList" type="java.util.Collection" />
											<logic:iterate name="ltFreqVariationList" id="ltFreqVariation" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltFreqVariation" property="key" />' > <bean:write name="ltFreqVariation" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select> <span style="font-weight:bold;">%</span>
								</td>
							</tr>
							<script> selectLstItemById('voltAddVariation', '<bean:write name="ratingObj" property="ltVoltAddVariation" filter="false" />'); </script>
							<script> selectLstItemById('voltRemoveVariation', '<bean:write name="ratingObj" property="ltVoltRemoveVariation" filter="false" />'); </script>
							<script> selectLstItemById('freqAddVariation', '<bean:write name="ratingObj" property="ltFreqAddVariation" filter="false" />'); </script>
							<script> selectLstItemById('freqRemoveVariation', '<bean:write name="ratingObj" property="ltFreqRemoveVariation" filter="false" />'); </script>
							<tr>
								<td width="20%" class="label-text"> Combined Variation </td>
								<td width="20%" class="normal">
									<select name="combinedVariation" id="combinedVariation" title="Combined Variation">
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present property="ltCombinedVariationList" name="newenquiryForm">
											<bean:define name="newenquiryForm" property="ltCombinedVariationList" id="ltCombinedVariationList" type="java.util.Collection" />
											<logic:iterate name="ltCombinedVariationList" id="ltCombinedVariation" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltCombinedVariation" property="key" />' > <bean:write name="ltCombinedVariation" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select> <span style="font-weight:bold;">%</span>
								</td>
								<script> selectLstItemById('combinedVariation', '<bean:write name="ratingObj" property="ltCombVariation" filter="false" />'); </script>
								<td width="10%">&nbsp;</td>
								<td width="20%" class="label-text"> <bean:message key="quotation.offer.label.servicefactor"/> <span class="mandatory">&nbsp;</span></td>
								<td width="20%">
									<select name="serviceFactor" id="serviceFactor" title="Service Factor">
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present name="newenquiryForm" property="ltServiceFactorList">
											<bean:define name="newenquiryForm" property="ltServiceFactorList" id="ltServiceFactorList" type="java.util.Collection" />
											<logic:iterate name="ltServiceFactorList" id="ltServiceFactor" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltServiceFactor" property="key" />'> <bean:write name="ltServiceFactor" property="value" /> </option>
											</logic:iterate>
										</logic:present>
								</select>
								</td>
								<script> selectLstItemById('serviceFactor', '<bean:write name="ratingObj" property="ltServiceFactor" filter="false" />'); </script>
							</tr>
							<tr>
								<td width="20%" class="label-text"><bean:message key="quotation.offer.label.areaclassification"/> <span class="mandatory">&nbsp;</span> </td>
								<td width="20%" class="normal">
									<select name="hazardAreaType" id="hazardAreaType" title="Area Classification" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present property="ltHazAreaTypeList" name="newenquiryForm">
											<bean:define name="newenquiryForm" property="ltHazAreaTypeList" id="ltHazAreaTypeList" type="java.util.Collection" />
											<logic:iterate name="ltHazAreaTypeList" id="ltHazAreaType" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltHazAreaType" property="key" />' > <bean:write name="ltHazAreaType" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
								<script> selectLstItemById('hazardAreaType', '<bean:write name="ratingObj" property="ltHazardAreaTypeId" filter="false" />'); </script>
								<td width="10%">&nbsp;</td>
								<td width="20%" class="label-text"><bean:message key="quotation.offerdata.technical.label.rpm"/></td>
								<td width="20%" class="normal">
									<input name="rpm" class="normal" onchange="javascript:calculateNTorque_NewEnquiry();" maxlength="30"/>
									&nbsp;<bean:message key="quotation.units.rmin"/>
								</td>
							</tr>
							<tr>
								<td width="20%" class="label-text"><bean:message key="quotation.create.label.zone"/></td>
								<td width="20%">
									<select name="hazardArea" id="hazardArea" title="Hazard Location" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present property="ltHazAreaList" name="newenquiryForm">
											<bean:define name="newenquiryForm" property="ltHazAreaList" id="ltHazAreaList" type="java.util.Collection" />
											<logic:iterate name="ltHazAreaList" id="ltHazArea" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltHazArea" property="key" />' > <bean:write name="ltHazArea" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
								<script> selectLstItemById('hazardArea', '<bean:write name="ratingObj" property="ltHazardAreaId" filter="false" />'); </script>
								<td width="10%">&nbsp;</td>
								<td width="20%" class="label-text"><bean:message key="quotation.offerdata.technical.label.flcamps"/> </td>
								<td width="20%" class="normal">
									<input name="flcAmps" class="normal"  onchange="javascript:updateCurrent_NewEnquiry(this.value)" maxlength="30"/>&nbsp;<bean:message key="quotation.units.a"/>
								</td>
							</tr>
							<tr>
								<td width="20%" class="label-text"><bean:message key="quotation.create.label.gasgroup"/></td>
								<td width="20%" class="normal">
									<select name="gasGroup" id="gasGroup" title="Gas Group">
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present property="ltGasGroupList" name="newenquiryForm">
											<bean:define name="newenquiryForm" property="ltGasGroupList" id="ltGasGroupList" type="java.util.Collection" />
											<logic:iterate name="ltGasGroupList" id="ltGasGroup" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltGasGroup" property="key" />' > <bean:write name="ltGasGroup" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
								<script> selectLstItemById('gasGroup', '<bean:write name="ratingObj" property="ltGasGroupId" filter="false" />'); </script>
								<td width="10%">&nbsp;</td>
								<td width="20%" class="label-text"><bean:message key="quotation.offer.label.noloadcurrent"/></td>
								<td width="20%" class="normal"> 
									<input name="nlCurrent" class="normal"  maxlength="10"/>&nbsp;<bean:message key="quotation.units.a"/>
								</td>
							</tr>
							<tr>
								<td width="20%" class="label-text">Temperature class</td>
								<td width="20%" class="normal">
									<select name="tempClass" id="tempClass" class="normal" >
										<option value=" "><bean:message key="quotation.option.select"/></option>
										<logic:present property="ltTemperatureClassList" name="newenquiryForm">
											<bean:define name="newenquiryForm" property="ltTemperatureClassList" id="ltTemperatureClassList" type="java.util.Collection" />
											<logic:iterate name="ltTemperatureClassList" id="ltTemperatureClass" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltTemperatureClass" property="key" />' > <bean:write name="ltTemperatureClass" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
								<td width="10%">&nbsp;</td>
								<!--
								<td width="20%" class="label-text"><bean:message key="quotation.offer.label.startingcurrent"/></td>
								<td width="20%" class="normal">
									<input name="strtgCurrentText" id="strtgCurrentText" class="normal "  maxlength="10"/>
								</td>
								-->
								<td width="20%" class="label-text"><bean:message key="quotation.offer.label.bkw"/></td>
								<td width="20%" class="normal">
									<input name="bkw" class="normal" maxlength="6"/> &nbsp;<bean:message key="quotation.option.kw"/>
								</td>
							</tr>
							<tr>
								<td width="20%" class="label-text"><bean:message key="quotation.create.label.enclosure"/></td>
								<td width="20%" class="normal">
									<select name="enclosureId" class="normal" >
										<option value=" "><bean:message key="quotation.option.select"/></option>
										<logic:present property="enclosureList" name="newenquiryForm">
											<bean:define name="newenquiryForm" property="enclosureList" id="enclosureList" type="java.util.Collection" />
											<logic:iterate name="enclosureList" id="enclosure" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="enclosure" property="key" />' > <bean:write name="enclosure" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
								<td width="10%">&nbsp;</td>
								<td width="20%" class="label-text"><bean:message key="quotation.offerdata.technical.label.flt"/></td>
								<td width="20%" class="normal">
									<input name="fltSQCAGE" class="normal"  maxlength="10"/>&nbsp;<bean:message key="quotation.units.nm"/>
								</td>
							</tr>
							<tr>
								<td width="20%" class="label-text"> No. of Starts <span class="mandatory">&nbsp;</span> </td>
								<td width="20%" class="normal">
									<select name="starts" id="starts" title="No. of Starts" class="normal" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present property="ltStartsList" name="newenquiryForm">
											<bean:define name="newenquiryForm" property="ltStartsList" id="ltStartsList" type="java.util.Collection" />
											<logic:iterate name="ltStartsList" id="ltStarts" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltStarts" property="key" />' > <bean:write name="ltStarts" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
								<script> selectLstItemById('starts', '<bean:write name="ratingObj" property="ltStartsId" filter="false" />'); </script>
								<td width="10%">&nbsp;</td>
								<td width="20%"class="label-text"><bean:message key="quotation.offer.label.lockedrotortorque"/></td>
								<td width="20%">
									<input name="lrTorque" class="normal"  maxlength="10"/>
								</td>
							</tr>
							<tr>
								<td width="20%" class="label-text"> No. of Starts/Hr. (Intermittent duty) <span class="mandatory">&nbsp;</span> </td>
								<td width="20%" class="normal">
									<input name="startsPerHour" id="startsPerHour" class="normal"  maxlength="10"/>
								</td>
								<td width="10%">&nbsp;</td>
								<td width="20%" class="label-text"><bean:message key="quotation.offerdata.technical.label.potflt"/></td>
								<td width="20%" class="normal">
									<input name="potFLT" id="potFLT" class="normal"  maxlength="10"/>
								</td>
							</tr>
							<tr>
								<td width="20%" class="label-text"><bean:message key="quotation.create.label.duty"/><span class="mandatory">&nbsp;</span></td>
								<td width="20%" class="normal">
									<select name="duty" id="duty" title="Duty" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present property="ltDutyList" name="newenquiryForm">
											<bean:define name="newenquiryForm" property="ltDutyList" id="ltDutyList" type="java.util.Collection" />
											<logic:iterate name="ltDutyList" id="ltDuty" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltDuty" property="key" />' > <bean:write name="ltDuty" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
								<script> selectLstItemById('duty', '<bean:write name="ratingObj" property="ltDutyId" filter="false" />'); </script>
								<td width="10%">&nbsp;</td>
								<logic:equal name="ratingObj" property="flg_Txt_RV_MTO" value="Y">
									<td width="20%" class="label-text" style="background:#ffff00;"> <bean:message key="quotation.offerdata.technical.label.rvslip"/> </td>
								</logic:equal>
								<logic:notEqual name="ratingObj" property="flg_Txt_RV_MTO" value="Y">
									<td width="20%" class="label-text"> <bean:message key="quotation.offerdata.technical.label.rvslip"/> </td>
								</logic:notEqual>
								<td width="20%">
									<input name="rv" class="normal"  maxlength="15"/>
								</td>
							</tr>
							<tr>
								<td width="20%" class="label-text">CDF </td>
								<td width="20%" class="normal">
									<select style="width:150px;" name="cdf" id="cdf" title="CDF" class="normal" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present property="ltCDFList" name="newenquiryForm">
											<bean:define name="newenquiryForm" property="ltCDFList" id="ltCDFList" type="java.util.Collection" />
											<logic:iterate name="ltCDFList" id="ltCDF" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltCDF" property="key" />' > <bean:write name="ltCDF" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
								<script> selectLstItemById('cdf', '<bean:write name="ratingObj" property="ltCDFId" filter="false" />'); </script>
								<td width="10%">&nbsp;</td>
								<logic:equal name="ratingObj" property="flg_Txt_RA_MTO" value="Y">
									<td width="20%" class="label-text" style="background:#ffff00;"> <bean:message key="quotation.offerdata.technical.label.raslip"/> </td>
								</logic:equal>
								<logic:notEqual name="ratingObj" property="flg_Txt_RA_MTO" value="Y">
									<td width="20%" class="label-text"> <bean:message key="quotation.offerdata.technical.label.raslip"/> </td>
								</logic:notEqual>
								<td width="20%">
									<input name="ra" class="normal"  maxlength="15"/>
								</td>
							</tr>
							<tr>
								<td width="20%" class="label-text">Overloading Duty</td>
								<td width="20%" class="normal">
									<select name="overloadingDuty" id="overloadingDuty" title="Overloading Duty" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present name="newenquiryForm" property="ltOverloadingDutyList">
											<bean:define name="newenquiryForm" property="ltOverloadingDutyList" id="ltOverloadingDutyList" type="java.util.Collection" />
											<logic:iterate name="ltOverloadingDutyList" id="ltOverloadingDuty" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltOverloadingDuty" property="key" />'>
													<bean:write name="ltOverloadingDuty" property="value" />
												</option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
								<script> selectLstItemById('overloadingDuty', '<bean:write name="ratingObj" property="ltOverloadingDuty" filter="false" />'); </script>
								<td width="10%">&nbsp;</td>
								<td width="20%" class="label-text">Efficiency Class</td>
								<td width="20%" class="normal">
									<select name="efficiencyClass" id="efficiencyClass" title="Efficiency Class" >
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present name="newenquiryForm" property="ltEfficiencyClassList">
											<bean:define name="newenquiryForm" property="ltEfficiencyClassList" id="ltEfficiencyClassList" type="java.util.Collection" />
											<logic:iterate name="ltEfficiencyClassList" id="ltEfficiencyClass" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltEfficiencyClass" property="key" />'> <bean:write name="ltEfficiencyClass" property="value" /> </option>
											</logic:iterate>
										</logic:present>
									</select>
								</td>
							</tr>
							<tr>
								<td width="20%">&nbsp;</td>
								<td width="20%">&nbsp;</td>
								<td width="10%">&nbsp;</td>
								<td width="20%">&nbsp;</td>
								<td width="20%">&nbsp;</td>
							</tr>
						</table>
						
						<hr />
						
						<table width="100%" border="0" align="center" cellpadding="0" cellspacing="3">
							<tr>
								<td width="25%" class="label-text"><bean:message key="quotation.offer.label.loadcharacteristics"/></td>
								<td width="2%"> </td>
								<td width="13%" class="label-text"><bean:message key="quotation.offer.label.load"/></td>
								<td width="2%"> </td>
								<td width="18%" class="label-text"><bean:message key="quotation.offer.label.currenta"/></td>
								<td width="2%"> </td>
								<td width="18%" class="label-text"><bean:message key="quotation.offer.label.efficiency"/></td>
								<td width="2%"> </td>
								<td width="18%" class="label-text"><bean:message key="quotation.offer.label.powerfactor"/></td>
							</tr>
							<tr>
								<td class="label-text"><bean:message key="quotation.offer.label.plldesc"/></td>
								<td width="2%"> </td>
								<td class="label-text"><bean:message key="quotation.offer.label.load100"/></td>
								<td width="2%"> </td>	
								<td>
									<bean:define name="newenquiryForm" property="flcAmps" id="flcAmpsValue" type="java.lang.String"/>
									<input name="pllCurr1" class="normal"  maxlength="6" value="<%=flcAmpsValue %>" onchange="javascript:updateCurrent_NewEnquiry(this.value)" />
								</td>
								<td width="2%"> </td>
								<td>
									<input name="pllEff1" class="normal"  maxlength="6" onchange="javascript:calCurrent_NewEnquiry()" />
								</td>
								<td width="2%"> </td>
								<td>
									<input name="pllPf1" class="normal"  maxlength="6" onchange="javascript:calCurrent_NewEnquiry()" />
								</td>
							</tr>
							<tr>
								<td class="label-text"></td>
								<td width="2%"> </td>
								<td class="label-text"><bean:message key="quotation.offer.label.load75"/></td>
								<td width="2%"> </td>
								<td>
									<input name="pllCurr2" class="normal"  maxlength="6" />
								</td>
								<td width="2%"> </td>
								<td>
									<input name="pllEff2" class="normal"  maxlength="6" onchange="javascript:calCurrent_NewEnquiry()" />
								</td>
								<td width="2%"> </td>
								<td>
									<input name="pllPf2" class="normal"  maxlength="6" onchange="javascript:calCurrent_NewEnquiry()" />
								</td>
							</tr>
							<tr>
								<td class="label-text"></td>
								<td width="2%"> </td>
								<td class="label-text"><bean:message key="quotation.offer.label.load50"/></td>
								<td width="2%"> </td>
								<td>
									<input name="pllCurr3" class="normal"  maxlength="6" />
								</td>
								<td width="2%"> </td>
								<td>
									<input name="pllEff3" class="normal"  maxlength="6" onchange="javascript:calCurrent_NewEnquiry()" />
								</td>
								<td width="2%"> </td>
								<td>
									<input name="pllpf3" class="normal"  maxlength="6" onchange="javascript:calCurrent_NewEnquiry()" />
								</td>
							</tr>
							<tr>
								<td class="label-text"></td>
								<td width="2%"> </td>
								<td class="label-text"><bean:message key="quotation.offer.label.loadstart"/></td>
								<td width="2%"> </td>
								<td>
									<input name="pllCurr4" class="normal"  maxlength="6" />
								</td>
								<td width="2%"> </td>
								<td></td>
								<td width="2%"> </td>
								<td>
									<input name="pllpf4" class="normal"  maxlength="6" />
								</td>
							</tr>
							<tr>
								<td class="label-text"></td>
								<td width="2%"> </td>
								<td class="label-text"><bean:message key="quotation.offer.label.loaddutypoint"/></td>
								<td width="2%"> </td>
								<td>
									<input name="pllCurr5" class="normal"  maxlength="6" />
								</td>
								<td width="2%"> </td>
								<td>
									<input name="pllEff4" class="normal"  maxlength="6" />
								</td>
								<td width="2%"> </td>
								<td>
									<input name="pllPf5" class="normal"  maxlength="6" />
								</td>
							</tr>
							<tr>
								<td colspan="9">&nbsp;</td>
							</tr>
						</table>
						
						<hr />
						
						<jsp:include page="newofferdata_include2.jsp"/>
						
						<hr />
						
						<jsp:include page="newofferdata_include3.jsp"/>
						
						<hr />
						
						<jsp:include page="newofferdata_include4.jsp"/>
						
						<hr />
						
						<jsp:include page="newofferdata_include5.jsp"/>
						
					</fieldset>
				</div>
								
				
<!-- ---------------------------------------------------	Technical Offer Section : END	-------------------------------------------------------------- -->


<!-- ---------------------------------------------------	Save & Submit Buttons Section : START	------------------------------------------------------ -->

				<div style="width:100%; float:left;" class="blue-light-btn" align="right">
					
					<input type="button" class="BUTTON" value="Save" onclick="javascript:mto_UpdateData();" />
					
					<input type="button" class="BUTTON" value="Return to Sales Manager" onClick="javascript:dereturntosm();" /> 
					
	         		<input type="button" class="BUTTON" value="Submit to Sales Manager" onclick="javascript:submitToManager()" />
				</div>
				
				<br/>

<!-- ---------------------------------------------------	Save & Submit Buttons Section : END	---------------------------------------------------------- -->
				
				</td>
			</tr>
		</table>
	
	
<!-- ---------------------------------------------------	Top Table Section : END	---------------------------------------------------------------------- -->	
	
	
	
</html:form>

<script>
	$(document).ready(function(){
		calCurrent_NewEnquiry();	
		calculateNTorque_NewEnquiry();
		
		var showReplaceMotorVal = document.getElementById('showReplaceMotor').value;
		if(showReplaceMotorVal === 'Yes') {
			$("#showReplaceMotorDivId").css("display","block");
		} else {
			$("#showReplaceMotorDivId").css("display","none");
		}
		
	});
	
	function updateCurrent_NewEnquiry(currValue){
		if(currValue != null && currValue != ""){
			document.newenquiryForm.pllCurr1.value = currValue;
			document.newenquiryForm.flcAmps.value = currValue;
		}
	}
	
</script>
