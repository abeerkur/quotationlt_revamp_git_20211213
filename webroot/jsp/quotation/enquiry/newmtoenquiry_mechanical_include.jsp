<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants,in.com.rbc.quotation.common.utility.PropertyUtility,in.com.rbc.quotation.common.utility.CommonUtility,in.com.rbc.quotation.common.vo.KeyValueVo" %>
<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>

<table>
	<tr class="linked" style="width:1174px !important; float:left; overflow-x: scroll;">
		<td valign="top">
			<table id="tableR" align="center" cellspacing="0" cellpadding="1">
			<%	String sTextStyleClass = "textnoborder-light"; %>
			<% for(int j = 1; j <= 14; j++) {  %>
				<% if(j == 1) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd" style="float:left;"><strong>Method of coupling</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd" style="float:left;"><strong>Method of coupling</strong></td>
					<% } %>
						
						<%  int idMechRow1 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td id="td_methodOfCoupling<%=idMechRow1%>" class="bordertd">
							<select style="width:100px;" name="methodOfCoupling<%=idMechRow1%>" id="methodOfCoupling<%=idMechRow1%>" title="Method of Coupling" onfocus="javascript:processAutoSave('<%=idMechRow1%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newmtoenquiryForm" property="ltMethodOfCouplingList" >
																	<bean:define name="newmtoenquiryForm" property="ltMethodOfCouplingList" id="ltMethodOfCouplingList" type="java.util.Collection" />
																	<logic:iterate name="ltMethodOfCouplingList" id="ltMethodOfCoupling" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltMethodOfCoupling" property="key" />' > <bean:write name="ltMethodOfCoupling" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('methodOfCoupling<%=idMechRow1%>', '<bean:write name="ratingObj" property="ltMethodOfCoupling" filter="false" />'); </script>
						<%	idMechRow1 = idMechRow1+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 2) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Main Terminal Box</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Main Terminal Box</strong></td>
					<% } %>
						
						<%  int idMechRow2 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width: 100px;" name="mainTermBox<%=idMechRow2%>" id="mainTermBox<%=idMechRow2%>" title="Service Factor" onfocus="javascript:processAutoSave('<%=idMechRow2%>');" >
								<option value="0"><bean:message key="quotation.option.select" /></option>
								<logic:present name="newmtoenquiryForm" property="ltMainTBList" >
									<bean:define name="newmtoenquiryForm" property="ltMainTBList" id="ltMainTBList" type="java.util.Collection" />
									<logic:iterate name="ltMainTBList" id="ltMainTB" type="in.com.rbc.quotation.common.vo.KeyValueVo">
										<option value='<bean:write name="ltMainTB" property="key" />' > <bean:write name="ltMainTB" property="value" /> </option>
									</logic:iterate>
								</logic:present>
							</select>
						</td>
						
						<%	idMechRow2 = idMechRow2+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 3) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Terminal Box Size</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Terminal Box Size</strong></td>
					<% } %>
						
						<%  int idMechRow3 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td id="td_terminalBoxSize<%=idMechRow3%>" class="bordertd">
							<select style="width:100px;" name="terminalBoxSize<%=idMechRow3%>" id="terminalBoxSize<%=idMechRow3%>" title="Terminal Box Size" onfocus="javascript:processAutoSave('<%=idMechRow3%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newmtoenquiryForm" property="ltTerminalBoxList" >
																	<bean:define name="newmtoenquiryForm" property="ltTerminalBoxList" id="ltTerminalBoxList" type="java.util.Collection" />
																	<logic:iterate name="ltTerminalBoxList" id="ltTerminalBox" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltTerminalBox" property="key" />' > <bean:write name="ltTerminalBox" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('terminalBoxSize<%=idMechRow3%>', '<bean:write name="ratingObj" property="ltTerminalBoxSizeId" filter="false" />'); </script>
						<%	idMechRow3 = idMechRow3+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 4) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Aux. Terminal Box</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Aux. Terminal Box</strong></td>
					<% } %>
						
						<%  int idMechRow4 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="auxTerminalBox<%=idMechRow4%>" id="auxTerminalBox<%=idMechRow4%>" title="Aux. Terminal Box" onfocus="javascript:processAutoSave('<%=idMechRow4%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newmtoenquiryForm" property="ltAuxTerminalBoxList" >
																	<bean:define name="newmtoenquiryForm" property="ltAuxTerminalBoxList" id="ltAuxTerminalBoxList" type="java.util.Collection" />
																	<logic:iterate name="ltAuxTerminalBoxList" id="ltAuxTerminalBox" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltAuxTerminalBox" property="key" />' > <bean:write name="ltAuxTerminalBox" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('auxTerminalBox<%=idMechRow4%>', '<bean:write name="ratingObj" property="ltAuxTerminalBoxId" filter="false" />'); </script>
						<%	idMechRow4 = idMechRow4+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 5) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Shaft Type</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Shaft Type</strong></td>
					<% } %>
						
						<%  int idMechRow5 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="shaftType<%=idMechRow5%>" id="shaftType<%=idMechRow5%>" title="Shaft Type" onfocus="javascript:processAutoSave('<%=idMechRow5%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newmtoenquiryForm" property="ltShaftTypeList" >
																	<bean:define name="newmtoenquiryForm" property="ltShaftTypeList" id="ltShaftTypeList" type="java.util.Collection" />
																	<logic:iterate name="ltShaftTypeList" id="ltShaftType" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltShaftType" property="key" />' > <bean:write name="ltShaftType" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('shaftType<%=idMechRow5%>', '<bean:write name="ratingObj" property="ltShaftTypeId" filter="false" />'); </script>
						<%	idMechRow5 = idMechRow5+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 6) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Shaft Material</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Shaft Material</strong></td>
					<% } %>
						
						<%  int idMechRow6 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td id="td_shaftMaterialType<%=idMechRow6%>" class="bordertd">
							<select style="width:100px;" name="shaftMaterialType<%=idMechRow6%>" id="shaftMaterialType<%=idMechRow6%>" title="Shaft Material" onfocus="javascript:processAutoSave('<%=idMechRow6%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newmtoenquiryForm" property="ltShaftMaterialTypeList" >
																	<bean:define name="newmtoenquiryForm" property="ltShaftMaterialTypeList" id="ltShaftMaterialTypeList" type="java.util.Collection" />
																	<logic:iterate name="ltShaftMaterialTypeList" id="ltShaftMaterialType" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltShaftMaterialType" property="key" />' > <bean:write name="ltShaftMaterialType" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('shaftMaterialType<%=idMechRow6%>', '<bean:write name="ratingObj" property="ltShaftMaterialId" filter="false" />'); </script>
						<%	idMechRow6 = idMechRow6+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 7) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Shaft Grounding</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Shaft Grounding</strong></td>
					<% } %>
						
						<%  int idMechRow7 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td id="td_shaftGrounding<%=idMechRow7%>" class="bordertd">
							<select style="width:100px;" name="shaftGrounding<%=idMechRow7%>" id="shaftGrounding<%=idMechRow7%>" title="Shaft Grounding" onfocus="javascript:processAutoSave('<%=idMechRow7%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newmtoenquiryForm" property="alTargetedList" >
																	<bean:define name="newmtoenquiryForm" property="alTargetedList" id="alTargetedList" type="java.util.Collection" />
																	<logic:iterate name="alTargetedList" id="alTargeted" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="alTargeted" property="key" />' > <bean:write name="alTargeted" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('shaftGrounding<%=idMechRow7%>', '<bean:write name="ratingObj" property="ltShaftGroundingId" filter="false" />'); </script>
						<%	idMechRow7 = idMechRow7+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 8) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>ReLubrication System</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>ReLubrication System</strong></td>
					<% } %>
						
						<%  int idMechRow8 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td id="td_bearingSystem<%=idMechRow8%>" class="bordertd">
							<select style="width:100px;" name="bearingSystem<%=idMechRow8%>" id="bearingSystem<%=idMechRow8%>" title="ReLubrication System" onfocus="javascript:processAutoSave('<%=idMechRow8%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newmtoenquiryForm" property="ltBearingSystemList" >
																	<bean:define name="newmtoenquiryForm" property="ltBearingSystemList" id="ltBearingSystemList" type="java.util.Collection" />
																	<logic:iterate name="ltBearingSystemList" id="ltBearingSystem" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltBearingSystem" property="key" />' > <bean:write name="ltBearingSystem" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('bearingSystem<%=idMechRow8%>', '<bean:write name="ratingObj" property="ltBearingSystemId" filter="false" />'); </script>
						<%	idMechRow8 = idMechRow8+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 9) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Bearing NDE</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Bearing NDE</strong></td>
					<% } %>
						
						<%  int idMechRow9 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width:100px;" name="bearingNDE<%=idMechRow9%>" id="bearingNDE<%=idMechRow9%>" title="Bearing NDE" onfocus="javascript:processAutoSave('<%=idMechRow9%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newmtoenquiryForm" property="ltBearingNDEList" >
																	<bean:define name="newmtoenquiryForm" property="ltBearingNDEList" id="ltBearingNDEList" type="java.util.Collection" />
																	<logic:iterate name="ltBearingNDEList" id="ltBearingNDE" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltBearingNDE" property="key" />' > <bean:write name="ltBearingNDE" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('bearingNDE<%=idMechRow9%>', '<bean:write name="ratingObj" property="ltBearingNDEId" filter="false" />'); </script>
						<%	idMechRow9 = idMechRow9+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 10) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Bearing NDE Size</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Bearing NDE Size</strong></td>
					<% } %>
						
						<%  int idMechRow10 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<input type="text" name="bearingNDESize<%=idMechRow10%>" id="bearingNDESize<%=idMechRow10%>" class="normal" style="width:100px;" value='<bean:write name="ratingObj" property="ltBearingNDESize" filter="false" />' title="Bearing NDE Size" onfocus="javascript:processAutoSave('<%=idMechRow10%>');" maxlength="20" >
						</td>
						<%	idMechRow10 = idMechRow10+1; %>
						</logic:iterate>
					</tr>
				
				<% } else if(j == 11) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Bearing DE</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Bearing DE</strong></td>
					<% } %>
						
						<%  int idMechRow11 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td id="td_bearingDE<%=idMechRow11%>" class="bordertd">
							<select style="width:100px;" name="bearingDE<%=idMechRow11%>" id="bearingDE<%=idMechRow11%>" title="Bearing DE" onchange="javascript:processBearingDEChange('<%=idMechRow11%>');" onfocus="javascript:processAutoSave('<%=idMechRow11%>');" >
																<option value="0"><bean:message key="quotation.option.select" /></option>
																<logic:present name="newmtoenquiryForm" property="ltBearingDEList" >
																	<bean:define name="newmtoenquiryForm" property="ltBearingDEList" id="ltBearingDEList" type="java.util.Collection" />
																	<logic:iterate name="ltBearingDEList" id="ltBearingDE" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																		<option value='<bean:write name="ltBearingDE" property="key" />' > <bean:write name="ltBearingDE" property="value" /> </option>
																	</logic:iterate>
																</logic:present>
							</select>
						</td>
						<script> selectLstItemById('bearingDE<%=idMechRow11%>', '<bean:write name="ratingObj" property="ltBearingDEId" filter="false" />'); </script>
						<%	idMechRow11 = idMechRow11+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 12) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Bearing DE Size</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Bearing DE Size</strong></td>
					<% } %>
						
						<%  int idMechRow12 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<input type="text" name="bearingDESize<%=idMechRow12%>" id="bearingDESize<%=idMechRow12%>" class="normal" style="width:100px;" value='<bean:write name="ratingObj" property="ltBearingDESize" filter="false" />' title="Bearing NDE Size" onfocus="javascript:processAutoSave('<%=idMechRow12%>');" maxlength="20" >
						</td>
						<%	idMechRow12 = idMechRow12+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 13) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Cable Entry</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Cable Entry</strong></td>
					<% } %>
						
						<%  int idMechRow13 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width: 100px;" name="cableEntry<%=idMechRow13%>" id="cableEntry<%=idMechRow13%>" title="Cable Entry" onfocus="javascript:processAutoSave('<%=idMechRow13%>');" >
								<option value="0"><bean:message key="quotation.option.select" /></option>
								<logic:present name="newmtoenquiryForm" property="ltBearingDEList" >
									<bean:define name="newmtoenquiryForm" property="ltCableEntryList" id="ltCableEntryList" type="java.util.Collection" />
									<logic:iterate name="ltCableEntryList" id="ltCableEntry" type="in.com.rbc.quotation.common.vo.KeyValueVo">
										<option value='<bean:write name="ltCableEntry" property="key" />' > <bean:write name="ltCableEntry" property="value" /> </option>
									</logic:iterate>
								</logic:present>
							</select>
						</td>
						
						<%	idMechRow13 = idMechRow13+1; %>
						</logic:iterate>
					</tr>
				<% } else if(j == 14) { %>
					<tr id='item<%=j%>'>
					<% if(j % 2 == 0) { %>
					<% sTextStyleClass = "textnoborder-dark"; %>
					<td width="200px" class="dark bordertd"><strong>Lubrication</strong></td>
					<% } else { %>
					<% sTextStyleClass = "textnoborder-light"; %>
					<td width="200px" class="light bordertd"><strong>Lubrication</strong></td>
					<% } %>
						
						<%  int idMechRow14 = 1;  %>
						<logic:iterate property="newRatingsList" name="newmtoenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
						<td class="bordertd">
							<select style="width: 100px;" name="lubrication<%=idMechRow14%>" id="lubrication<%=idMechRow14%>" title="Lubrication" onfocus="javascript:processAutoSave('<%=idMechRow14%>');" >
								<option value="0"><bean:message key="quotation.option.select" /></option>
								<logic:present name="newmtoenquiryForm" property="ltBearingDEList" >
									<bean:define name="newmtoenquiryForm" property="ltLubricationList" id="ltLubricationList" type="java.util.Collection" />
									<logic:iterate name="ltLubricationList" id="ltLubrication" type="in.com.rbc.quotation.common.vo.KeyValueVo">
										<option value='<bean:write name="ltLubrication" property="key" />' > <bean:write name="ltLubrication" property="value" /> </option>
									</logic:iterate>
								</logic:present>
							</select>
						</td>
						
						<%	idMechRow14 = idMechRow14+1; %>
						</logic:iterate>
					</tr>
				<% } %>
			<% } %>
			</table>
		</td>
	</tr>
</table>