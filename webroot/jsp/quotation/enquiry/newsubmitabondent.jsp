<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>
<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants" %>
<%@ page import="in.com.rbc.quotation.common.utility.CommonUtility" %>
<%@ page import="in.com.rbc.quotation.common.utility.PropertyUtility" %>
<%@ page import="in.com.rbc.quotation.common.utility.CurrencyConvertUtility" %>

<%@ page import="java.text.DecimalFormat,java.math.BigDecimal,java.net.URLDecoder" %>

<script language="javascript" src="html/js/quotation_lt.js"></script>

<html:form action="newPlaceAnEnquiry.do" method="POST">

	<html:hidden property="invoke" value="processAbandonEnquiry"/>
	<html:hidden property="operationType"/>
	<html:hidden property="enquiryId" styleId="enquiryId"/>
	<html:hidden property="enquiryNumber" styleId="enquiryNumber"/>
	<html:hidden property="statusId" styleId="statusId"/>
	<html:hidden property="ratingsValidationMessage"/>
	<html:hidden property="dispatch" />
	
	<input type="hidden" name='<%= org.apache.struts.taglib.html.Constants.TOKEN_KEY %>' value='<%= session.getAttribute(org.apache.struts.action.Action.TRANSACTION_TOKEN_KEY) %>' />
	
	<table width="80%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
		<tr>
			<td>
 				<div class="sectiontitle"><bean:message key="quotation.viewenquiry.label.heading"/> </div>	
			</td>
		</tr>
		
		<tr>
			<td height="100%" valign="top">
				<!---------------------------------		Top Title and Status & Note block : START  -------------------------------------------------------->
				<table width="100%" border="0" cellspacing="1" cellpadding="0">
					<tr>
						<td rowspan="2">
							<table width="98%" border="0" cellspacing="2" cellpadding="0" class="DotsTable">
								<tr>
									<td rowspan="2" style="width: 70px"><img src="html/images/icon_quote.gif" width="52" height="50"></td>
									<td class="other">
										<bean:message key="quotation.viewenquiry.label.subheading1" /> <bean:write name="newenquiryForm" property="enquiryNumber" /><br> 
										<bean:message key="quotation.viewenquiry.label.subheading2" />
									</td>
								</tr>
								<tr>
									<td class="other">
										<table width="100%">
											<tr>
												<td height="16" class="section-head" align="center">
													<bean:message key="quotation.viewenquiry.label.status" /> <bean:write name="newenquiryForm" property="statusName" />
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
						<logic:lessThan name="newenquiryForm" property="statusId" value="6">
						<td align="right" class="other">
							<table>
								<tr>
									<td> <bean:message key="quotation.attachments.message" /> </td>
									<td>
										<a href="javascript:manageAttachments('<%=CommonUtility.getIntValue(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_ATTACHMENT_APPLICATIONID))%>','<bean:write name="newenquiryForm" property="enquiryId"/>', '<bean:write name="newenquiryForm" property="createdBySSO"/>');">
											<img src="html/images/ico_attach.gif" alt="Manage Attachments" border="0" align="right"> 
										</a>
									</td>
								</tr>
							</table>
						</td>
						</logic:lessThan>
					</tr>
					<tr>
						<td style="width: 45%">
						<logic:present name="attachmentList" scope="request">
							<logic:notEmpty name="attachmentList">
								<fieldset>
									<table width="100%" class="other">
										<logic:iterate name="attachmentList" id="attachmentData" indexId="idx" type="in.com.rbc.quotation.common.vo.KeyValueVo">
											<tr>
												<td>
													<a href="javascript:download('<bean:write name="attachmentData" property="key"/>', '<bean:write name="attachmentData" property="value"/>', '<bean:write name="attachmentData" property="value2"/>');">
														<bean:write name="attachmentData" property="value" />.<bean:write name="attachmentData" property="value2" />
													</a>
												</td>
											</tr>
										</logic:iterate>
									</table>
								</fieldset>
							</logic:notEmpty>
						</logic:present>
						</td>
					</tr>
					<logic:present name="attachmentMessage" scope="request">
					<tr>
						<td><bean:write name="attachmentMessage" /></td>
					</tr>
					</logic:present>
				</table>
				<!---------------------------------		Top Title and Status & Note block : END  ---------------------------------------------------------->
				
				<br> <br> <br>
				
				<!---------------------------------		Request Information and Abandon Details Block : START  -------------------------------------------->
				<div style="width: 100%; float: left;">
					<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
						<tr>
							<td>
								<!-- -- Request Information Block -- -->
								<fieldset>
									<legend class="blueheader"> <bean:message key="quotation.viewenquiry.label.request.heading" /> </legend>
									<table width="100%" border="0" align="center" cellpadding="0" cellspacing="3">
										<tr>
											<td><label class="blue-light"style="font-weight: bold; width: 100%;"> Basic Information </label></td>
										</tr>
										<tr>
											<th class="label-text" style="width:240px; line-height:20px;"><bean:message key="quotation.create.label.customerenqreference"/></th>
											<td class="formContentview-rating-b0x" style="width:200px"><bean:write name="newenquiryForm" property="customerEnqReference" /></td>
										</tr>
										<tr>
											<th class="label-text" style="width:240px; line-height:20px;"><bean:message key="quotation.create.label.customer" /></th>
											<td class="formContentview-rating-b0x" style="width:200px"><bean:write name="newenquiryForm" property="customerName" /></td>
										</tr>
										<tr>
											<th class="label-text" style="width:240px; line-height:20px;"><bean:message key="quotation.label.admin.customer.location" /></th>
											<td class="formContentview-rating-b0x" style="width:200px"><bean:write name="newenquiryForm" property="location" /></td>
										</tr>
										<tr>
											<th class="label-text" style="width:240px; line-height:20px;"><bean:message key="quotation.label.concerned.person" /></th>
											<td class="formContentview-rating-b0x" style="width:200px"><bean:write name="newenquiryForm" property="concernedPerson" /></td>
										</tr>
										<tr>
											<th class="label-text" style="width:240px; line-height:20px;">Project Name</th>
											<td class="formContentview-rating-b0x" style="width:200px"><bean:write name="newenquiryForm" property="projectName" /></td>
										</tr>
										<tr>
											<th class="label-text" style="width:240px; line-height:20px;">Consultant</th>
											<td class="formContentview-rating-b0x" style="width:200px"><bean:write name="newenquiryForm" property="consultantName" /></td>
										</tr>
										<tr>
											<th class="label-text" style="width:240px; line-height:20px;">End user</th>
											<td class="formContentview-rating-b0x" style="width:200px"><bean:write name="newenquiryForm" property="endUser" /></td>
										</tr>
										<tr>
											<th class="label-text" style="width:240px; line-height:20px;">End User Industry</th>
											<td class="formContentview-rating-b0x" style="width:200px"><bean:write name="newenquiryForm" property="endUserIndustry" /></td>
										</tr>
										<tr>
											<th class="label-text" style="width:240px; line-height:20px;">Location of Installation</th>
											<td class="formContentview-rating-b0x" style="width:200px"><bean:write name="newenquiryForm" property="locationOfInstallation" /></td>
										</tr>
									</table>
								</fieldset>
							</td>
							
							<!-- -- Abandon Details Block -- -->
							<td valign="top">
								<fieldset>
									<legend class="blueheader"> Abandon Information </legend>
									<table width="100%" border="0" align="center" cellpadding="0" cellspacing="3">
										<tr>
											<th class="label-text" style="width:320px; line-height:20px;">Reason 1</th>
											<td class="formContentview-rating-b0x" style="width:200px">
												<bean:define name="newenquiryForm" property="ltAbondentReasonList" id="ltAbondentReasonList" type="java.util.Collection" />
												<html:select style="width:200px;" name="newenquiryForm" property="abondentReason1" styleId="abondentReason1" >
						                    		<option value="0"><bean:message key="quotation.option.select"/></option>
						                       	 	<html:options name="newenquiryForm" collection="ltAbondentReasonList" property="key" labelProperty="value" />
						                        </html:select> 
											</td>
										</tr>
										<tr>
											<th class="label-text" style="width:320px; line-height:20px;">Reason 2</th>
											<td class="formContentview-rating-b0x" style="width:200px">
												<bean:define name="newenquiryForm" property="ltAbondentReasonList" id="ltAbondentReasonList" type="java.util.Collection" />
												<html:select style="width:200px;" name="newenquiryForm" property="abondentReason2" styleId="abondentReason2" >
						                    		<option value="0"><bean:message key="quotation.option.select"/></option>
						                       	 	<html:options name="newenquiryForm" collection="ltAbondentReasonList" property="key" labelProperty="value" />
						                        </html:select> 
											</td>
										</tr>
										<tr>
											<th class="label-text" style="width:320px; line-height:20px;">Remarks</th>
											<td class="formContentview-rating-b0x" style="width:200px">
												<html:textarea property="abondentRemarks" styleId="abondentRemarks" style="width:200px;" onkeyup="checkMaxLenghtText(this, 4000);" onkeydown="checkMaxLenghtText(this, 4000);" tabindex="35"/>
											</td> 
										</tr>
										<tr>
											<th class="label-text" style="width:320px; line-height:20px;">Has it been discussed with Customer before closing the Job?</th>
											<td class="formContentview-rating-b0x" style="width:200px">
												<bean:define name="newenquiryForm" property="alTargetedList" id="alTargetedList" type="java.util.Collection" />
												<html:select style="width:200px;" property="abondentCustomerApproval" styleId="abondentCustomerApproval">
													<option value="0" selected>Select</option>
						                    		<html:options name="newenquiryForm" collection="alTargetedList" property="key" labelProperty="value" />
					        	            	</html:select> 
											</td>
										</tr>
									</table>
								</fieldset>
							</td>
						</tr>
					</table>
				</div>
				<!---------------------------------		Request Information and Abandon Details Block : END  -------------------------------------------->
				
				<br>
				
				<!---------------------------------		Buttons Section : START  ------------------------------------------------------------------------>
				<div class="blue-light" align="right" style="font-weight:bold; width:100% !important;">
					<html:submit value="Submit" styleClass="BUTTON" />
				</div>
				<!---------------------------------		Buttons Section : END  -------------------------------------------------------------------------->
				
			</td>
		</tr>
		
	</table>

</html:form>