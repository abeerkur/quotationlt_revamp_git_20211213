
<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>

<body>
<!-------------------------- main table Start  --------------------------------->
<table width="80%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
  <tr>
    <td height="100%" valign="top">      
    
    <html:form  method="post" action="createEnquiry.do"><br>
	<html:hidden property="invoke" value="returnEnquiry"/>
	<html:hidden property="dispatch" />
	<html:hidden property="enquiryId" name="enquiryForm"/>
	<html:hidden property="enquiryNumber" name="enquiryForm"/>
	<html:hidden property="operationType" name="enquiryForm" value="RFQ" />
	
	<input type="hidden" name='<%= org.apache.struts.taglib.html.Constants.TOKEN_KEY %>'
         value='<%= session.getAttribute(org.apache.struts.action.Action.TRANSACTION_TOKEN_KEY) %>' />
	
	<br>
        <div class="sectiontitle">Return Enquiry </div>
	    <br>
      		<fieldset>
				<legend class="blueheader">
					<bean:message key="quotation.viewenquiry.return.label.heading"/>
				</legend>
				<table  align="center" >
                  <tr>
                    <td height="23" class="formLabel">
                    	<bean:message key="quotation.viewenquiry.return.label.returnto"/> 
                    </td>
                    <td>
                   	 <bean:define name="enquiryForm" property="teamMemberList"
	                	             id="teamMemberList" type="java.util.Collection" /> 
                   	 <html:select property="returnTo">                    	                  
                        <html:options name="enquiryForm" collection="teamMemberList"
			 				              property="key" labelProperty="value" />
                     </html:select>
                    </td>
                  </tr>
                  <tr>
                    <td class="formLabel">
                    	<bean:message key="quotation.viewenquiry.return.label.comments"/>
					</td>
                    <td>
                    	<html:textarea property="returnToRemarks"/>
                    </td>
                  </tr>
                </table>
			
		    <br>
          <div class="blue-light" align="right">
	        <input name="new" type="button" class="BUTTON" id="new" onClick="backToViewQuotation();" value="Cancel">
            <input name="new" type="button" class="BUTTON" id="new" onClick="returnEnquiry('RFQ');" value="Return">
            </div>
	    </fieldset>
      </html:form>    </td>
  </tr>
</table>
<!-------------------------- main table End  --------------------------------->
</body>
</html>