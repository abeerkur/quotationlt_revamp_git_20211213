<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants,in.com.rbc.quotation.common.utility.PropertyUtility,in.com.rbc.quotation.common.utility.CommonUtility,in.com.rbc.quotation.common.vo.KeyValueVo" %>
<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>

<div id="sticky-anchor"></div>
<table id="sticky">
	<tbody>
		<tr class="linked" style="width:1100px !important; float:left; overflow-x: scroll;">
			<td valign="top">
				<table id="tableR" align="center" cellspacing="0" cellpadding="1">
				<%	String sTextStyleClass = "textnoborder-light"; %>
				<% for(int i = 0; i <= 8; i++) {  %>
					<% if(i == 0) { %>
						<tr id="top">
							<td width="200px" class="bordertd" style="float:left;">&nbsp;</td>
							<%  int idRow1 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<input type="hidden" id="ratingidx<%=idRow1%>" value='<%=idRow1%>' >
							<input type="hidden" id="ratingObjId<%=idRow1%>"  name="ratingObjId<%=idRow1%>"  value='<bean:write name="ratingObj" property="ratingId" />' >
							<input type="hidden" id="ratingNo<%=idRow1%>"  name="ratingNo<%=idRow1%>"  value='<bean:write name="ratingObj" property="ratingNo" />' >
							<td class="bordertd">
								<label class="normal" style="text-align:left !important;"><strong> Rating <%=idRow1%> </strong></label>
								<a href="javascript:removeRating('<%=idRow1%>');" id="removeRating<%=idRow1%>" style="font-size:0px !important;"> <i class="fas fa-trash" style="float:right; padding-right:12px; font-size:14px; color:#000;"></i> </a> 
							</td>
							<%	idRow1 = idRow1+1; %>
							</logic:iterate>
						</tr>
					<% } else if(i == 1) { %>
						<tr id='item<%=i%>'>
							<% if(i % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd" > <strong>Quantity</strong> <span class="mandatory">&nbsp;</span> </td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd" > <strong>Quantity</strong> <span class="mandatory">&nbsp;</span> </td>
							<% } %>
							
							<%  int idRow2 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<input type="text" name="quantity<%=idRow2%>" id="quantity<%=idRow2%>" class="normal" style="width: 100px;" value='<bean:write name="ratingObj" property="quantity" filter="false" />' onfocus="javascript:processAutoSave('<%=idRow2%>');" title="Quantity" >
							</td>
							<%	idRow2 = idRow2+1; %>
							</logic:iterate>
						</tr>
					<%-- 
					<% } else if(i == 2) { %>
						<% if(i % 2 == 0) { %>
						<% sTextStyleClass = "textnoborder-dark"; %>
						<tr class="dark" id='item<%=i%>'>
						<% } else { %>
						<% sTextStyleClass = "textnoborder-light"; %>
						<tr class="light" id='item<%=i%>'>
						<% } %>
							<td width="200px" class="bordertd"> <strong>Product Group</strong> <span class="mandatory">&nbsp;</span> </td>
							<%  int idRow3 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								&nbsp;&nbsp;
								<select style="width:100px;" id="productGroup<%=idRow3%>" name="productGroup<%=idRow3%>" title="Product Group">
									<option value="0"><bean:message key="quotation.option.select" /></option>
									<logic:present property="ltProductGroupList" name="newenquiryForm">
										<bean:define name="newenquiryForm" property="ltProductGroupList" id="ltProductGroupList" type="java.util.Collection" />
										<logic:iterate name="ltProductGroupList" id="ltProductGroup" type="in.com.rbc.quotation.common.vo.KeyValueVo">
											<option value='<bean:write name="ltProductGroup" property="key" />' > <bean:write name="ltProductGroup" property="value" /> </option>
										</logic:iterate>
									</logic:present>
								</select>
							</td>
							<script> selectLstItemById('productGroup<%=idRow3%>', '<bean:write name="ratingObj" property="ltProdGroupId" filter="false" />'); </script>
							<%	idRow3 = idRow3+1; %>
							</logic:iterate>
						</tr>
					--%>
					
					<% } else if(i == 2) { %>
						<tr id='item<%=i%>'>
							<% if(i % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd" > <strong>Product Line</strong> <span class="mandatory">&nbsp;</span> </td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd" > <strong>Product Line</strong> <span class="mandatory">&nbsp;</span> </td>
							<% } %>
							
							<%  int idRow4 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width: 100px;"
									id="productLine<%=idRow4%>"
									name="productLine<%=idRow4%>" title="Product Line"
									onfocus="javascript:processAutoSave('<%=idRow4%>');" 
									onchange="javascript:processFieldsForProductLine('<%=idRow4%>');">
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<logic:present property="ltProductLineList" name="newenquiryForm">
											<bean:define name="newenquiryForm" property="ltProductLineList" id="ltProductLineList" type="java.util.Collection" />
											<logic:iterate name="ltProductLineList" id="ltProductLine" type="in.com.rbc.quotation.common.vo.KeyValueVo">
												<option value='<bean:write name="ltProductLine" property="key" />'>
													<bean:write name="ltProductLine" property="value" />
												</option>
											</logic:iterate>
										</logic:present>
								</select>
							</td>
							<script> selectLstItemById('productLine<%=idRow4%>', '<bean:write name="ratingObj" property="ltProdLineId" filter="false" />'); </script>
							<%	idRow4 = idRow4+1; %>
							</logic:iterate>
						</tr>
					<%-- 
					<% } else if(i == 4) { %>
						<% if(i % 2 == 0) { %>
						<% sTextStyleClass = "textnoborder-dark"; %>
						<tr class="dark" id='item<%=i%>'>
						<% } else { %>
						<% sTextStyleClass = "textnoborder-light"; %>
						<tr class="light" id='item<%=i%>'>
						<% } %>
							<td width="200px" class="bordertd"> <strong>Type of Motor</strong> <span class="mandatory">&nbsp;</span> </td>
							<%  int idRow5 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								&nbsp;&nbsp;
								<select style="width:100px;" id="typeOfMotor<%=idRow5%>" name="typeOfMotor<%=idRow5%>" title="Type of Motor" >
									<option value="0"><bean:message key="quotation.option.select" /></option>
									<logic:present property="ltMotorTypeList" name="newenquiryForm">
										<bean:define name="newenquiryForm" property="ltMotorTypeList" id="ltMotorTypeList" type="java.util.Collection" />
										<logic:iterate name="ltMotorTypeList" id="ltMotorType" type="in.com.rbc.quotation.common.vo.KeyValueVo">
											<option value='<bean:write name="ltMotorType" property="key" />' > <bean:write name="ltMotorType" property="value" /> </option>
										</logic:iterate>
									</logic:present>
								</select>
							</td>
							<script> selectLstItemById('typeOfMotor<%=idRow5%>', '<bean:write name="ratingObj" property="ltMotorTypeId" filter="false" />'); </script>
							<%	idRow5 = idRow5+1; %>
							</logic:iterate>
						</tr>
					--%>
					
					<% } else if(i == 3) { %>
						<tr id='item<%=i%>'>
							<% if(i % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd" > <strong>Power Rating(KW)</strong> <span class="mandatory">&nbsp;</span> </td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd" > <strong>Power Rating(KW)</strong> <span class="mandatory">&nbsp;</span> </td>
							<% } %>
							
							<%  int idRow6 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:100px;" id="kw<%=idRow6%>" name="kw<%=idRow6%>" title="Power Rating(KW)" onfocus="javascript:processAutoSave('<%=idRow6%>');" >
									<option value="0"><bean:message key="quotation.option.select" /></option>
									<logic:present property="ltKWList" name="newenquiryForm">
										<bean:define name="newenquiryForm" property="ltKWList" id="ltKWList" type="java.util.Collection" />
										<logic:iterate name="ltKWList" id="ltKW" type="in.com.rbc.quotation.common.vo.KeyValueVo">
											<option value='<bean:write name="ltKW" property="key" />' > <bean:write name="ltKW" property="value" /> </option>
										</logic:iterate>
									</logic:present>
								</select>
							</td>
							<script> selectLstItemById('kw<%=idRow6%>', '<bean:write name="ratingObj" property="ltKWId" filter="false" />'); </script>
							<%	idRow6 = idRow6+1; %>
							</logic:iterate>
						</tr>
					<% } else if(i == 4) { %>
						<tr id='item<%=i%>'>
							<% if(i % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd" > <strong>No. of Poles</strong> <span class="mandatory">&nbsp;</span> </td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd" > <strong>No. of Poles</strong> <span class="mandatory">&nbsp;</span> </td>
							<% } %>
							
							<%  int idRow7 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:100px;" id="pole<%=idRow7%>" name="pole<%=idRow7%>" title="No. of Poles" onfocus="javascript:processAutoSave('<%=idRow7%>');" >
									<option value="0"><bean:message key="quotation.option.select" /></option>
									<logic:present property="ltPoleList" name="newenquiryForm">
										<bean:define name="newenquiryForm" property="ltPoleList" id="ltPoleList" type="java.util.Collection" />
										<logic:iterate name="ltPoleList" id="ltPole" type="in.com.rbc.quotation.common.vo.KeyValueVo">
											<option value='<bean:write name="ltPole" property="key" />' > <bean:write name="ltPole" property="value" /> </option>
										</logic:iterate>
									</logic:present>
								</select>
							</td>
							<script> selectLstItemById('pole<%=idRow7%>', '<bean:write name="ratingObj" property="ltPoleId" filter="false" />'); </script>
							<%	idRow7 = idRow7+1; %>
							</logic:iterate>
						</tr>
					<% } else if(i == 5) { %> 
						<tr id='item<%=i%>'>
							<% if(i % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd" > <strong>Frame size</strong> <span class="mandatory">&nbsp;</span> </td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd" > <strong>Frame size</strong> <span class="mandatory">&nbsp;</span> </td>
							<% } %>
							
							<%  int idRow8 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:100px;" id="frame<%=idRow8%>" name="frame<%=idRow8%>" title="Frame size" onfocus="javascript:processAutoSave('<%=idRow8%>');" onchange="javascript:processFieldsForFrame('<%=idRow8%>');" >
									<option value="0"><bean:message key="quotation.option.select" /></option>
									<logic:present property="ltFrameList" name="newenquiryForm">
										<bean:define name="newenquiryForm" property="ltFrameList" id="ltFrameList" type="java.util.Collection" />
										<logic:iterate name="ltFrameList" id="ltFrame" type="in.com.rbc.quotation.common.vo.KeyValueVo">
											<option value='<bean:write name="ltFrame" property="key" />' > <bean:write name="ltFrame" property="value" /> </option>
										</logic:iterate>
									</logic:present>
								</select>
							</td>
							<script> selectLstItemById('frame<%=idRow8%>', '<bean:write name="ratingObj" property="ltFrameId" filter="false" />'); </script>
							<%	idRow8 = idRow8+1; %>
							</logic:iterate>
						</tr>
					<% } else if(i == 6) { %>
						<tr id='item<%=i%>'>
							<% if(i % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd" > <strong>Frame suffix</strong> <span class="mandatory">&nbsp;</span> </td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd" > <strong>Frame suffix</strong> <span class="mandatory">&nbsp;</span> </td>
							<% } %>
							
							<%  int idRow9 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:100px;" id="framesuffix<%=idRow9%>" name="framesuffix<%=idRow9%>" title="Frame suffix" onfocus="javascript:processAutoSave('<%=idRow9%>');" onchange="javascript:processFieldsForFrame('<%=idRow9%>');" >
									<option value="0"><bean:message key="quotation.option.select" /></option>
									<logic:present property="ltFrameSuffixList" name="newenquiryForm">
										<bean:define name="newenquiryForm" property="ltFrameSuffixList" id="ltFrameSuffixList" type="java.util.Collection" />
										<logic:iterate name="ltFrameSuffixList" id="ltFrameSuffix" type="in.com.rbc.quotation.common.vo.KeyValueVo">
											<option value='<bean:write name="ltFrameSuffix" property="key" />' > <bean:write name="ltFrameSuffix" property="value" /> </option>
										</logic:iterate>
									</logic:present>
								</select>
							</td>
							<script> selectLstItemById('framesuffix<%=idRow9%>', '<bean:write name="ratingObj" property="ltFrameSuffixId" filter="false" />'); </script>
							<%	idRow9 = idRow9+1; %>
							</logic:iterate>
						</tr>
					<% } else if(i == 7) { %>
						<tr id='item<%=i%>'>
							<% if(i % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd" > <strong>Mounting</strong> <span class="mandatory">&nbsp;</span> </td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd" > <strong>Mounting</strong> <span class="mandatory">&nbsp;</span> </td>
							<% } %>
							
							<%  int idRow10 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:100px;" id="mounting<%=idRow10%>" name="mounting<%=idRow10%>" title="Mounting" onfocus="javascript:processAutoSave('<%=idRow10%>');" >
									<option value="0"><bean:message key="quotation.option.select" /></option>
									<logic:present property="ltMountingList" name="newenquiryForm">
										<bean:define name="newenquiryForm" property="ltMountingList" id="ltMountingList" type="java.util.Collection" />
										<logic:iterate name="ltMountingList" id="ltMounting" type="in.com.rbc.quotation.common.vo.KeyValueVo">
											<option value='<bean:write name="ltMounting" property="key" />' > <bean:write name="ltMounting" property="value" /> </option>
										</logic:iterate>
									</logic:present>
								</select>
							</td>
							<script> selectLstItemById('mounting<%=idRow10%>', '<bean:write name="ratingObj" property="ltMountingId" filter="false" />'); </script>
							<%	idRow10 = idRow10+1; %>
							</logic:iterate>
						</tr>
					<% } else if(i == 8) { %>
						<tr id='item<%=i%>'>
							<% if(i % 2 == 0) { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<td width="200px" class="dark bordertd" > <strong>TB Position</strong> <span class="mandatory">&nbsp;</span> </td>
							<% } else { %>
								<% sTextStyleClass = "textnoborder-light"; %>
								<td width="200px" class="light bordertd" > <strong>TB Position</strong> <span class="mandatory">&nbsp;</span> </td>
							<% } %>
							
							<%  int idRow11 = 1;  %>
							<logic:iterate property="newRatingsList" name="newenquiryForm" id="ratingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewRatingDto">
							<td class="bordertd">
								<select style="width:100px;" id="tbPosition<%=idRow11%>" name="tbPosition<%=idRow11%>" title="TB Position" onfocus="javascript:processAutoSave('<%=idRow11%>');" >
									<option value="0"><bean:message key="quotation.option.select" /></option>
									<logic:present property="ltTBPositionList" name="newenquiryForm">
										<bean:define name="newenquiryForm" property="ltTBPositionList" id="ltTBPositionList" type="java.util.Collection" />
										<logic:iterate name="ltTBPositionList" id="ltTBPosition" type="in.com.rbc.quotation.common.vo.KeyValueVo">
											<option value='<bean:write name="ltTBPosition" property="key" />' > <bean:write name="ltTBPosition" property="value" /> </option>
										</logic:iterate>
									</logic:present>
								</select>
							</td>
							<script> selectLstItemById('tbPosition<%=idRow11%>', '<bean:write name="ratingObj" property="ltTBPositionId" filter="false" />'); </script>
							<%	idRow11 = idRow11+1; %>
							</logic:iterate>
						</tr>
					<% } %>	
				<% } %>
				</table>
			</td>
		</tr>
	</tbody>
</table>
