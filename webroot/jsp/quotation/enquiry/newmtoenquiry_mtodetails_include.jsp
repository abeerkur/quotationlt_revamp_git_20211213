<!-- -- Design MTO Ratings : Search Block -- -->
	<div style="clear:both; float:left; width:100%; margin-bottom:30px;">
		<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
			<tr>
				<td>
					<fieldset>
						<legend class="blueheader"> Design (MTO) Details - Search </legend>
						<table width="100%" border="0" align="center" cellpadding="0" cellspacing="3">
							<tr>
								<td class="normal">Mfg. Location</td>
								<td>
									<div class="col-sm-8">
										<bean:define name="newmtoenquiryForm" property="ltMfgLocationList" id="ltMfgLocationList" type="java.util.Collection" />
										<html:select property="searchMfgLocation" styleId="searchMfgLocation"  style="width:100px;">
											<option value="0"><bean:message key="quotation.option.select"/></option>
			                    			<html:options name="newmtoenquiryForm" collection="ltMfgLocationList" property="key" labelProperty="value" />
										</html:select>
									</div>
								</td>
								<td class="normal">Product Line</td>
								<td>
									<div class="col-sm-8">
										<bean:define name="newmtoenquiryForm" property="ltProductLineList" id="ltProductLineList" type="java.util.Collection" />
										<html:select property="searchProdLine" styleId="searchProdLine"  style="width:100px;">
											<option value="0"><bean:message key="quotation.option.select"/></option>
			                    			<html:options name="newmtoenquiryForm" collection="ltProductLineList" property="key" labelProperty="value" />
										</html:select>
									</div>
								</td>
								<td class="normal">KW</td>
								<td>
									<div class="col-sm-8">
										<bean:define name="newmtoenquiryForm" property="ltKWList" id="ltKWList" type="java.util.Collection" />
										<html:select property="searchKW" styleId="searchKW"  style="width:100px;">
											<option value="0"><bean:message key="quotation.option.select"/></option>
			                    			<html:options name="newmtoenquiryForm" collection="ltKWList" property="key" labelProperty="value" />
										</html:select>
									</div>
								</td>
								<td class="normal">Pole</td>
								<td>
									<div class="col-sm-8">
										<bean:define name="newmtoenquiryForm" property="ltPoleList" id="ltPoleList" type="java.util.Collection" />
										<html:select property="searchPole" styleId="searchPole"  style="width:100px;">
											<option value="0"><bean:message key="quotation.option.select"/></option>
			                    			<html:options name="newmtoenquiryForm" collection="ltPoleList" property="key" labelProperty="value" />
										</html:select>
									</div>
								</td>
							</tr>
							<br>
							<tr>
								<td class="normal">Frame</td>
								<td>
									<div class="col-sm-8">
										<bean:define name="newmtoenquiryForm" property="ltFrameList" id="ltFrameList" type="java.util.Collection" />
										<html:select property="searchFrame" styleId="searchFrame"  style="width:100px;">
											<option value="0"><bean:message key="quotation.option.select"/></option>
			                    			<html:options name="newmtoenquiryForm" collection="ltFrameList" property="key" labelProperty="value" />
										</html:select>
									</div>
								</td>
								<td class="normal">Frame Suffix</td>
								<td>
									<div class="col-sm-8">
										<bean:define name="newmtoenquiryForm" property="ltFrameSuffixList" id="ltFrameSuffixList" type="java.util.Collection" />
										<html:select property="searchFrameSuffix" styleId="searchFrameSuffix"  style="width:100px;">
											<option value="0"><bean:message key="quotation.option.select"/></option>
			                    			<html:options name="newmtoenquiryForm" collection="ltFrameSuffixList" property="key" labelProperty="value" />
										</html:select>
									</div>
								</td>
								<td class="normal">Mounting</td>
									<td>
									<div class="col-sm-8">
										<bean:define name="newmtoenquiryForm" property="ltMountingList" id="ltMountingList" type="java.util.Collection" />
										<html:select property="searchMounting" styleId="searchMounting"  style="width:100px;">
											<option value="0"><bean:message key="quotation.option.select"/></option>
			                    			<html:options name="newmtoenquiryForm" collection="ltMountingList" property="key" labelProperty="value" />
										</html:select>
									</div>
								</td>
								<td class="normal">TB Position</td>
									<td>
									<div class="col-sm-8">
										<bean:define name="newmtoenquiryForm" property="ltTBPositionList" id="ltTBPositionList" type="java.util.Collection" />
										<html:select property="searchTBPosition" styleId="searchTBPosition"  style="width:100px;">
											<option value="0"><bean:message key="quotation.option.select"/></option>
			                    			<html:options name="newmtoenquiryForm" collection="ltTBPositionList" property="key" labelProperty="value" />
										</html:select>
									</div>
								</td>
							</tr>
						</table>
						<br/>
						<div class="blue-light" align="right" style="padding-right:25px; width:100%;">
							<input style="width:100px; font-weight:bold; font-size:11px; color: #990000;" type="button" class="BUTTON" value="Search" onclick="javascript:searchMTORatingDetails();" />
						</div>
					</fieldset>
				</td>
			</tr>
		</table>
	</div>
<!-- -- Design MTO Ratings : Search Block -- -->

<!-- -- Design MTO Ratings : Search Results Block -- -->
	<div style="width:100%; float:left; margin-bottom:30px;">
		<fieldset>
			<legend class="blueheader"> Design (MTO) Details </legend>
			
			<!-- -- Design MTO Commercial Details Block -- -->
			<div class="blue-light" align="left" style="padding-right:25px; width:100%;">
				<label style="font-weight: bold;">  MTO Commercial Details  </label>
			</div>
			<div style="width:100%; float:left; margin-bottom:30px;">
				<table width="100%" border="0" align="center" cellpadding="0" cellspacing="3" style="overflow-x:scroll;">
					<tr>
						<td style="width:auto;overflow:scroll;float:left;">
							<table style="width:100%; float:left; border: 2px solid black !important;" align="center" cellpadding="0" cellspacing="3">
								<thead>
									<tr>
										<th style="border: 1px solid black !important; width: 26px !important;" class="formLabelTophome">S.No.</th>
										<th style="border: 1px solid black !important; width: 85px !important;" class="formLabelTophome">Mfg. Location</th>
										<th style="border: 1px solid black !important; width: 70px !important;" class="formLabelTophome">Prod Line</th>
										<th style="border: 1px solid black !important; width: 50px !important;" class="formLabelTophome">KW</th>
										<th style="border: 1px solid black !important; width: 50px !important;" class="formLabelTophome">Pole</th>
										<th style="border: 1px solid black !important; width: 50px !important;" class="formLabelTophome">Frame</th>
										<th style="border: 1px solid black !important; width: 50px !important;" class="formLabelTophome">Frame suffix</th>
										<th style="border: 1px solid black !important; width: 50px !important;" class="formLabelTophome">Mounting</th>
										<th style="border: 1px solid black !important; width: 60px !important;" class="formLabelTophome">TB Position</th>
										<th style="border: 1px solid black !important; width: 60px !important;" class="formLabelTophome">Standard Add-On %</th>
										<th style="border: 1px solid black !important; width: 60px !important;" class="formLabelTophome">Standard Cash Extra</th>
										<th style="border: 1px solid black !important; width: 60px !important;" class="formLabelTophome">% Copper</th>
										<th style="border: 1px solid black !important; width: 60px !important;" class="formLabelTophome">% Stamping</th>
										<th style="border: 1px solid black !important; width: 60px !important;" class="formLabelTophome">% Aluminium</th>
										<th style="border: 1px solid black !important; width: 60px !important;" class="formLabelTophome">Total AddOn%</th>
										<th style="border: 1px solid black !important; width: 60px !important;" class="formLabelTophome">Total Cash Extra</th>
										<th style="border: 1px solid black !important; width: 60px !important;" class="formLabelTophome">Material Cost</th>
										<th style="border: 1px solid black !important; width: 60px !important;" class="formLabelTophome">Is Design Complete?</th>
									</tr>
								</thead>
								<tbody>
									<%  int idRow = 1;  %>
									<logic:iterate property="newMTORatingsList" name="newmtoenquiryForm" id="mtoRatingObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewMTORatingDto">
									<tr>
										<td style="border: 1px solid black !important; width: 26px !important;" class="formContentview-rating-b0x"> <%=idRow%>
											<input type="hidden" id="mtoIndexId<%=idRow%>"  name="mtoIndexId<%=idRow%>"  value='<%=idRow%>' >
											<input type="hidden" id="mtoRatingId<%=idRow%>"  name="mtoRatingId<%=idRow%>"  value='<bean:write name="mtoRatingObj" property="mtoRatingId" />' >
											<input type="hidden" id="mtoRatingNo<%=idRow%>"  name="mtoRatingNo<%=idRow%>"  value='<bean:write name="mtoRatingObj" property="mtoRatingNo" />' >
											<input type="hidden" id="mtoQuantity<%=idRow%>"  name="mtoQuantity<%=idRow%>"  value='<bean:write name="mtoRatingObj" property="mtoQuantity" />' >
										</td>
										<td style="border: 1px solid black !important; width: 85px !important;" class="formContentview-rating-b0x">
											<select style="width:85px !important;" id="mtoMfgLocation<%=idRow%>" name="mtoMfgLocation<%=idRow%>" >
												<logic:present property="ltMfgLocationList" name="newmtoenquiryForm">
													<bean:define name="newmtoenquiryForm" property="ltMfgLocationList" id="ltMfgLocationList" type="java.util.Collection" />
													<logic:iterate name="ltMfgLocationList" id="ltMfgLocation" type="in.com.rbc.quotation.common.vo.KeyValueVo">
														<option value='<bean:write name="ltMfgLocation" property="key" />' > <bean:write name="ltMfgLocation" property="value" /> </option>
													</logic:iterate>
												</logic:present>
											</select>
											<script> selectLstItemById('mtoMfgLocation<%=idRow%>', '<bean:write name="mtoRatingObj" property="mtoMfgLocation" filter="false" />'); </script>
										</td>
										<td style="border: 1px solid black !important; width: 70px !important;" class="formContentview-rating-b0x">
											<select style="width:70px !important;" id="mtoProdLine<%=idRow%>" name="mtoProdLine<%=idRow%>" >
												<logic:present property="ltProductLineList" name="newmtoenquiryForm">
													<bean:define name="newmtoenquiryForm" property="ltProductLineList" id="ltProductLineList" type="java.util.Collection" />
													<logic:iterate name="ltProductLineList" id="ltProductLine" type="in.com.rbc.quotation.common.vo.KeyValueVo">
														<option value='<bean:write name="ltProductLine" property="key" />' > <bean:write name="ltProductLine" property="value" /> </option>
													</logic:iterate>
												</logic:present>
											</select>
											<script> selectLstItemById('mtoProdLine<%=idRow%>', '<bean:write name="mtoRatingObj" property="mtoProdLineId" filter="false" />'); </script>
										</td>
										<td style="border: 1px solid black !important; width: 50px !important;" class="formContentview-rating-b0x">
											<select style="width:50px !important;" id="mtoKw<%=idRow%>" name="mtoKw<%=idRow%>" >
												<logic:present property="ltKWList" name="newmtoenquiryForm">
													<bean:define name="newmtoenquiryForm" property="ltKWList" id="ltKWList" type="java.util.Collection" />
													<logic:iterate name="ltKWList" id="ltKW" type="in.com.rbc.quotation.common.vo.KeyValueVo">
														<option value='<bean:write name="ltKW" property="key" />' > <bean:write name="ltKW" property="value" /> </option>
													</logic:iterate>
												</logic:present>
											</select>
											<script> selectLstItemById('mtoKw<%=idRow%>', '<bean:write name="mtoRatingObj" property="mtoKWId" filter="false" />'); </script>
										</td>
										<td style="border: 1px solid black !important; width: 50px !important;" class="formContentview-rating-b0x">
											<select style="width:50px !important;" id="mtoPole<%=idRow%>" name="mtoPole<%=idRow%>" >
												<logic:present property="ltPoleList" name="newmtoenquiryForm">
													<bean:define name="newmtoenquiryForm" property="ltPoleList" id="ltPoleList" type="java.util.Collection" />
													<logic:iterate name="ltPoleList" id="ltPole" type="in.com.rbc.quotation.common.vo.KeyValueVo">
														<option value='<bean:write name="ltPole" property="key" />' > <bean:write name="ltPole" property="value" /> </option>
													</logic:iterate>
												</logic:present>
											</select>
											<script> selectLstItemById('mtoPole<%=idRow%>', '<bean:write name="mtoRatingObj" property="mtoPoleId" filter="false" />'); </script>
										</td>
										<td style="border: 1px solid black !important; width: 50px !important;" class="formContentview-rating-b0x">
											<select style="width:50px !important;" id="mtoFrame<%=idRow%>" name="mtoFrame<%=idRow%>" >
												<logic:present property="ltFrameList" name="newmtoenquiryForm">
													<bean:define name="newmtoenquiryForm" property="ltFrameList" id="ltFrameList" type="java.util.Collection" />
													<logic:iterate name="ltFrameList" id="ltFrame" type="in.com.rbc.quotation.common.vo.KeyValueVo">
														<option value='<bean:write name="ltFrame" property="key" />' > <bean:write name="ltFrame" property="value" /> </option>
													</logic:iterate>
												</logic:present>
											</select>
											</select>
											<script> selectLstItemById('mtoFrame<%=idRow%>', '<bean:write name="mtoRatingObj" property="mtoFrameId" filter="false" />'); </script>
										</td>
										<td style="border: 1px solid black !important; width: 50px !important;" class="formContentview-rating-b0x">
											<select style="width:50px !important;" id="mtoFrameSuffix<%=idRow%>" name="mtoFrameSuffix<%=idRow%>" >
												<logic:present property="ltFrameSuffixList" name="newmtoenquiryForm">
													<bean:define name="newmtoenquiryForm" property="ltFrameSuffixList" id="ltFrameSuffixList" type="java.util.Collection" />
													<logic:iterate name="ltFrameSuffixList" id="ltFrameSuffix" type="in.com.rbc.quotation.common.vo.KeyValueVo">
														<option value='<bean:write name="ltFrameSuffix" property="key" />' > <bean:write name="ltFrameSuffix" property="value" /> </option>
													</logic:iterate>
												</logic:present>
											</select>
											<script> selectLstItemById('mtoFrameSuffix<%=idRow%>', '<bean:write name="mtoRatingObj" property="mtoFrameSuffixId" filter="false" />'); </script>
										</td>
										<td style="border: 1px solid black !important; width: 50px !important;" class="formContentview-rating-b0x">
											<select style="width:50px !important;" id="mtoMounting<%=idRow%>" name="mtoMounting<%=idRow%>" >
												<logic:present property="ltMountingList" name="newmtoenquiryForm">
													<bean:define name="newmtoenquiryForm" property="ltMountingList" id="ltMountingList" type="java.util.Collection" />
													<logic:iterate name="ltMountingList" id="ltMounting" type="in.com.rbc.quotation.common.vo.KeyValueVo">
														<option value='<bean:write name="ltMounting" property="key" />' > <bean:write name="ltMounting" property="value" /> </option>
													</logic:iterate>
												</logic:present>
											</select>
											<script> selectLstItemById('mtoMounting<%=idRow%>', '<bean:write name="mtoRatingObj" property="mtoMountingId" filter="false" />'); </script>
										</td>
										<td style="border: 1px solid black !important; width: 60px !important;" class="formContentview-rating-b0x">
											<select style="width:60px !important;" id="mtoTBPosition<%=idRow%>" name="mtoTBPosition<%=idRow%>" >
												<logic:present property="ltTBPositionList" name="newmtoenquiryForm">
													<bean:define name="newmtoenquiryForm" property="ltTBPositionList" id="ltTBPositionList" type="java.util.Collection" />
													<logic:iterate name="ltTBPositionList" id="ltTBPosition" type="in.com.rbc.quotation.common.vo.KeyValueVo">
														<option value='<bean:write name="ltTBPosition" property="key" />' > <bean:write name="ltTBPosition" property="value" /> </option>
													</logic:iterate>
												</logic:present>
											</select>
											<script> selectLstItemById('mtoTBPosition<%=idRow%>', '<bean:write name="mtoRatingObj" property="mtoTBPositionId" filter="false" />'); </script>
										</td>
										<td style="border: 1px solid black !important; width: 60px !important; background-color: #e2e2e2;" class="formContentview-rating-b0x">
											<input type="text" name="mtoStandardAddOnPercent<%=idRow%>" id="mtoStandardAddOnPercent<%=idRow%>" class="normal" style="width: 40px !important; background-color: #e2e2e2;" value='<bean:write name="mtoRatingObj" property="mtoStandardAddOnPercent" filter="false" />' readonly > %
										</td>
										<td style="border: 1px solid black !important; width: 60px !important; background-color: #e2e2e2;" class="formContentview-rating-b0x">
											&#8377; &nbsp; <input type="text" name="mtoStandardAddOnCash<%=idRow%>" id="mtoStandardAddOnCash<%=idRow%>" class="normal" style="width: 60px !important; background-color: #e2e2e2;" value='<bean:write name="mtoRatingObj" property="mtoStandardCashExtra" filter="false" />' readonly >
										</td>
										<td style="border: 1px solid black !important; width: 60px !important;" class="formContentview-rating-b0x">
											<input type="text" name="mtoPercentCopper<%=idRow%>" id="mtoPercentCopper<%=idRow%>" class="normal" style="width: 40px !important;" maxlength="5" value='<bean:write name="mtoRatingObj" property="mtoPercentCopper" filter="false" />' onchange="javascript:calculateMTOTechValues('<%=idRow%>');" > %
										</td>
										<td style="border: 1px solid black !important; width: 60px !important;" class="formContentview-rating-b0x">
											<input type="text" name="mtoPercentStamping<%=idRow%>" id="mtoPercentStamping<%=idRow%>" class="normal" style="width: 40px !important;" maxlength="5" value='<bean:write name="mtoRatingObj" property="mtoPercentStamping" filter="false" />' onchange="javascript:calculateMTOTechValues('<%=idRow%>');" > %
										</td>
										<td style="border: 1px solid black !important; width: 60px !important;" class="formContentview-rating-b0x">
											<input type="text" name="mtoPercentAluminium<%=idRow%>" id="mtoPercentAluminium<%=idRow%>" class="normal" style="width: 40px !important;" maxlength="5" value='<bean:write name="mtoRatingObj" property="mtoPercentAluminium" filter="false" />' onchange="javascript:calculateMTOTechValues('<%=idRow%>');" > %
										</td>
										<td style="border: 1px solid black !important; width: 60px !important;" class="formContentview-rating-b0x">
											<input type="text" name="mtoTotalAddOnPercent<%=idRow%>" id="mtoTotalAddOnPercent<%=idRow%>" class="normal" style="width: 40px !important; background-color: #e2e2e2;" value='<bean:write name="mtoRatingObj" property="mtoTotalAddOnPercent" filter="false" />' readonly > %
										</td>
										<td style="border: 1px solid black !important; width: 60px !important;" class="formContentview-rating-b0x">
											&#8377; &nbsp; <input type="text" name="mtoTotalCash<%=idRow%>" id="mtoTotalCash<%=idRow%>" class="normal" style="width: 60px !important; background-color: #e2e2e2;" value='<bean:write name="mtoRatingObj" property="mtoTotalCashExtra" filter="false" />' readonly >
										</td>
										<td style="border: 1px solid black !important; width: 60px !important; background-color: #e2e2e2;" class="formContentview-rating-b0x">
											&#8377; &nbsp; <input type="text" name="mtoMaterialCost<%=idRow%>" id="mtoMaterialCost<%=idRow%>" class="normal" style="width: 60px !important; background-color: #e2e2e2;" value='<bean:write name="mtoRatingObj" property="mtoMaterialCost" filter="false" />' readonly >
										</td>
										<td style="border: 1px solid black !important; width: 60px !important;" class="formContentview-rating-b0x">
											<select style="width:60px !important;" id="mtoDesignComplete<%=idRow%>" name="mtoDesignComplete<%=idRow%>" onchange="javascript:validateMTOPriceCheck('<%=idRow%>');" >
												<option value="0"><bean:message key="quotation.option.select"/></option>
												<logic:present name="newmtoenquiryForm" property="alTargetedList" >
													<bean:define name="newmtoenquiryForm" property="alTargetedList" id="alTargetedList" type="java.util.Collection" />
													<logic:iterate name="alTargetedList" id="alTargeted" type="in.com.rbc.quotation.common.vo.KeyValueVo">
														<option value='<bean:write name="alTargeted" property="key" />' > <bean:write name="alTargeted" property="value" /> </option>
													</logic:iterate>
												</logic:present>
											</select>
											<script> selectLstItemById('mtoDesignComplete<%=idRow%>', '<bean:write name="mtoRatingObj" property="mtoDesignComplete" filter="false" />'); </script>
										</td>
									</tr>
									
									<%	idRow = idRow+1; %>
									</logic:iterate>
								</tbody>
							</table>
						</td>
					</tr>
				</table>
			</div>
			<!-- -- Design MTO Commercial Details Block -- -->
			
			<!-- -- Design MTO Technical & Add-On Details Block -- -->
			<div class="blue-light" align="left" style="padding-right:25px; width:100%;">
				<label style="font-weight: bold;">  MTO Technical & Add-On Details  </label>
			</div>
			<div style="width:100%; float:left; margin-bottom:30px;">
				<table width="100%" border="0" align="center" cellpadding="0" cellspacing="3" >
					<%  int idRowTech = 1;  %>
					<logic:iterate property="newMTORatingsList" name="newmtoenquiryForm" id="mtoRatingObjTech" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewMTORatingDto">
					
					<bean:define name="mtoRatingObjTech" property="newMTOTechDetailsList" id="newMTOTechDetailsList" type="java.util.Collection" />
					<bean:define name="mtoRatingObjTech" property="newMTOAddOnsList" id="newMTOAddOnsList" type="java.util.Collection" />
					
					<tr>
						<td>
							<input type="hidden" id="ratingNo<%=idRowTech%>"  name="ratingNo<%=idRowTech%>"  value='<bean:write name="mtoRatingObjTech" property="mtoRatingNo" />' >
							<div id="mtorating<%=idRowTech%>" >
								<a href="javascript:;" onClick="toggledropdown('mtorating<%=idRowTech%>','mtofieldset<%=idRowTech%>')"> 
									<img src="html/images/rightTriArrow.gif" width="15" height="15"  border="0" align="absmiddle"/>
								</a>
								<span class="other"> 
									View MTO Technical & Add-On Details - Rating <%=idRowTech%> 
									( 
										<bean:write name="mtoRatingObjTech" property="actualProdLineId" filter="false" /> &nbsp; - &nbsp;
										<bean:write name="mtoRatingObjTech" property="actualKWId" filter="false" /> kW &nbsp; - &nbsp;
										<bean:write name="mtoRatingObjTech" property="actualPoleId" filter="false" /> P &nbsp; - &nbsp;
										<bean:write name="mtoRatingObjTech" property="actualFrameId" filter="false" /><bean:write name="mtoRatingObjTech" property="actualFrameSuffixId" filter="false" /> &nbsp; - &nbsp;
										<bean:write name="mtoRatingObjTech" property="actualMountingId" filter="false" /> &nbsp; - &nbsp;
										<bean:write name="mtoRatingObjTech" property="actualTBPositionId" filter="false" />
									)
								</span>
							</div>
							<fieldset id="mtofieldset<%=idRowTech%>" style="display:none;">
								<legend>
									<a href="javascript:;" onClick="toggledropdown('mtofieldset<%=idRowTech%>','mtorating<%=idRowTech%>')">
										<img src="html/images/downTriArrow.gif" width="15" height="15" border="0" align="absmiddle" />
									</a> 
									<span class="blueheader"> 
										Collapse MTO Technical & Add-On Details - Rating <%=idRowTech%>
										( 
											<bean:write name="mtoRatingObjTech" property="actualProdLineId" filter="false" /> &nbsp; - &nbsp;
											<bean:write name="mtoRatingObjTech" property="actualKWId" filter="false" /> kW &nbsp; - &nbsp;
											<bean:write name="mtoRatingObjTech" property="actualPoleId" filter="false" /> P &nbsp; - &nbsp;
											<bean:write name="mtoRatingObjTech" property="actualFrameId" filter="false" /><bean:write name="mtoRatingObjTech" property="actualFrameSuffixId" filter="false" /> &nbsp; - &nbsp;
											<bean:write name="mtoRatingObjTech" property="actualMountingId" filter="false" /> &nbsp; - &nbsp;
											<bean:write name="mtoRatingObjTech" property="actualTBPositionId" filter="false" />
										)
									</span>
								</legend>
								<br>
								
								<table style="width:100%; float:left; border: 1px solid black !important;" align="center" cellpadding="0" cellspacing="3">
									<tr>
										<td style="width:10% !important; border: 0px;" class="formLabelTophome">Actual Mfg. Location</td>
										<td style="width:15% !important;"> <bean:write name="mtoRatingObjTech" property="actualMfgLocation" filter="false" /> </td>
										<td style="width:10% !important; border: 0px;" class="formLabelTophome">Actual Product Line</td>
										<td style="width:15% !important;"> <bean:write name="mtoRatingObjTech" property="actualProdLineId" filter="false" /> </td>
										<td style="width:10% !important; border: 0px;" class="formLabelTophome">Actual KW</td>
										<td style="width:15% !important;"> <bean:write name="mtoRatingObjTech" property="actualKWId" filter="false" /> </td>
										<td style="width:10% !important; border: 0px;" class="formLabelTophome">Actual Pole</td>
										<td style="width:15% !important;"> <bean:write name="mtoRatingObjTech" property="actualPoleId" filter="false" /> </td>
									</tr>
									<tr>
										<td style="width:10% !important; border: 0px;" class="formLabelTophome">Actual Frame</td>
										<td style="width:15% !important;"> <bean:write name="mtoRatingObjTech" property="actualFrameId" filter="false" /> </td>
										<td style="width:10% !important; border: 0px;" class="formLabelTophome">Actual Frame Suffix</td>
										<td style="width:15% !important;"> <bean:write name="mtoRatingObjTech" property="actualFrameSuffixId" filter="false" /> </td>
										<td style="width:10% !important; border: 0px;" class="formLabelTophome">Actual Mounting</td>
										<td style="width:15% !important;"> <bean:write name="mtoRatingObjTech" property="actualMountingId" filter="false" /> </td>
										<td style="width:10% !important; border: 0px;" class="formLabelTophome">Actual TB Position</td>
										<td style="width:15% !important;"> <bean:write name="mtoRatingObjTech" property="actualTBPositionId" filter="false" /> </td>
									</tr>
									<tr>
										<td> <br><br> </td>
									</tr>
									
									<!-- Iterate the MTO Technical Details List. -->
									<%  int idxTech1 = 1, idxTech2 = 0;  %>
									<logic:iterate property="newMTOTechDetailsList" name="mtoRatingObjTech" id="mtoTechDetailObj" indexId="idx" type="in.com.rbc.quotation.common.vo.KeyValueVo">
									<% if(idxTech2 % 4  == 0) { %>
									<tr>
									<% } %>
									<% idxTech2 = idxTech2+1; %>
									
										<td style="width:10% !important; border: 0px;" class="formLabelTophome"> <bean:write name="mtoTechDetailObj" property="key" /></td>
										<td style="width:15% !important;">
											<bean:write name="mtoTechDetailObj" property="value" />
										</td>
									<% if(idxTech2 % 4  == 0) { %>
									</tr>
									<% } %>
									<%	idxTech1 = idxTech1+1; %>
									
									</logic:iterate>
									<!-- Iterate the MTO Technical Details List. -->
									
									<tr>
										<td> <br> </td>
									</tr>
								</table>
								
								<br><br>
								
								<table style="width:100%; float:left; border: 1px solid black !important;" cellpadding="0" cellspacing="0">
									<tr>
										<td colspan="20">
											<label class="blue-light" style="font-weight: bold;">
												&nbsp; <button type="button" id="btnCalcMTOAddOns<%=idRowTech%>" style="width:200px; font-weight:bold; font-size:11px; color: #990000;" onclick="javascript:calculateMTOAddOns('<%=idRowTech%>');">Calculate Add-on values</button>
												&nbsp;&nbsp;&nbsp;&nbsp; <span class="formLabelTophome" style="font-weight: bold;"> Special Add-On % </span> 
												&nbsp; : &nbsp; <input type="text" name="mtoSpecialAddOnPercent<%=idRowTech%>" id="mtoSpecialAddOnPercent<%=idRowTech%>" class="normal" style="width: 40px !important;" value='<bean:write name="mtoRatingObjTech" property="mtoSpecialAddOnPercent" filter="false" />' maxlength="5" > %
												&nbsp;&nbsp;&nbsp;&nbsp; <span class="formLabelTophome" style="font-weight: bold;"> Special Cash Extra </span>
												&nbsp; : &nbsp;&nbsp; &#8377; &nbsp; <input type="text" name="mtoSpecialAddOnCash<%=idRowTech%>" id="mtoSpecialAddOnCash<%=idRowTech%>" class="normal" style="width: 60px !important;" value='<bean:write name="mtoRatingObjTech" property="mtoSpecialCashExtra" filter="false" />' maxlength="15" >
											</label>
										</td>
									</tr>
									<tr>
										<td>
											<table id="mto_addons_table<%=idRowTech%>"style="width:100%; float:left; border: 0px;" cellpadding="0" cellspacing="0" >
												<tr>
													<td> <br> </td>
												</tr>
												<tr>
													<td style="width:35%" class="formLabelTophome"> Add-on Type</td>
													<td style="width:15%" class="formLabelTophome"> Quantity</td>
													<td style="width:15%" class="formLabelTophome"> Add-on % </td>
													<td style="width:20%" class="formLabelTophome"> Add-on Cash Extra </td>
													<td style="width:15%" class="formLabelTophome"> Delete</td>
												</tr>
												<!-- Iterate the MTO AddOn Details List. -->
												<%  int idxAddOn = 1;  %>
												
												<logic:present property="newMTOAddOnsList" name="mtoRatingObj">
												<logic:iterate property="newMTOAddOnsList" name="mtoRatingObj" id="mtoAddOnDetailObj" indexId="idx" type="in.com.rbc.quotation.enquiry.dto.NewMTOAddOnDto">
												<tr>
													<td style="width:35%; padding:4px; border-bottom: 1px solid black;">
														<select name="addOnType<%=idRowTech%>_<%=idxAddOn%>" id="addOnType<%=idRowTech%>_<%=idxAddOn%>" style="width:300px; float:left;">
															<option value="0">Select</option>
															<logic:present property="newCompleteAddOnsList" name="mtoRatingObj">
																<bean:define name="mtoRatingObj" property="newCompleteAddOnsList" id="newCompleteAddOnsList" type="java.util.Collection" />
																<logic:iterate name="newCompleteAddOnsList" id="newCompleteAddOn" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																	<option value='<bean:write name="newCompleteAddOn" property="key" />' > <bean:write name="newCompleteAddOn" property="value" /> </option>
																</logic:iterate>
															</logic:present>
														</select>
														<script> selectLstItemByLabel('addOnType<%=idRowTech%>_<%=idxAddOn%>', '<bean:write name="mtoAddOnDetailObj" property="mtoAddOnType" filter="false" />'); </script>
													</td>
													<td style="width:15%; padding:4px; border-bottom: 1px solid black;">
														<input id="addOnQuantity<%=idRowTech%>_<%=idxAddOn%>" name="addOnQuantity<%=idRowTech%>_<%=idxAddOn%>" onkeypress="return isNumber(event)" maxLength="2" class="short" value="" >
													</td>
													<td style="width:15%; padding:4px; border-bottom: 1px solid black;">
														<logic:equal name="mtoAddOnDetailObj" property="mtoAddOnCalcType" value="PERCENT">
															<input id="addOnPercent<%=idRowTech%>_<%=idxAddOn%>" name="addOnPercent<%=idRowTech%>_<%=idxAddOn%>" onkeypress="return isNumber(event)" maxLength="5" class="short" value='<bean:write name="mtoAddOnDetailObj" property="mtoAddOnValue" filter="false" />' >								
														</logic:equal>
														<logic:notEqual name="mtoAddOnDetailObj" property="mtoAddOnCalcType" value="PERCENT">
															<input id="addOnPercent<%=idRowTech%>_<%=idxAddOn%>" name="addOnPercent<%=idRowTech%>_<%=idxAddOn%>" onkeypress="return isNumber(event)" maxLength="5" class="short" >
														</logic:notEqual>
													</td>
													<td style="width:20%; padding:4px; border-bottom: 1px solid black;">
														<logic:equal name="mtoAddOnDetailObj" property="mtoAddOnCalcType" value="ROUNDED">
															&#8377; &nbsp; <input id="addOnCash<%=idRowTech%>_<%=idxAddOn%>" name="addOnCash<%=idRowTech%>_<%=idxAddOn%>" onkeypress="return isNumber(event)" maxLength="15" class="normal" value='<bean:write name="mtoAddOnDetailObj" property="mtoAddOnValue" filter="false" />' >
														</logic:equal>
														<logic:notEqual name="mtoAddOnDetailObj" property="mtoAddOnCalcType" value="ROUNDED">
															&#8377; &nbsp; <input id="addOnCash<%=idRowTech%>_<%=idxAddOn%>" name="addOnCash<%=idRowTech%>_<%=idxAddOn%>" onkeypress="return isNumber(event)" maxLength="15" class="normal" >
														</logic:notEqual>
													</td>
													<td style="width:15%; padding:4px; border-bottom: 1px solid black;">
														<img src="html/images/deleteicon.gif" alt="Delete" onclick="javascript:removeAddOn(this,'<%=idRowTech%>');" id="delete<%=idRowTech%>_<%=idxAddOn%>" style="cursor: hand;" />
													</td>
												</tr>
												<%	idxAddOn = idxAddOn+1; %>
												</logic:iterate>
												</logic:present>
												
												<%  if( idxAddOn != 1 ) { idxAddOn = idxAddOn + 1; }  %>
												<tr>
													<td style="width:35%; padding:4px; border-bottom: 1px solid black;">
														<select name="addOnType<%=idRowTech%>_<%=idxAddOn%>" id="addOnType<%=idRowTech%>_<%=idxAddOn%>" style="width:300px; float:left;">
															<option value="0">Select</option>
															<logic:present property="newCompleteAddOnsList" name="mtoRatingObj">
																<bean:define name="mtoRatingObj" property="newCompleteAddOnsList" id="newCompleteAddOnsList" type="java.util.Collection" />
																<logic:iterate name="newCompleteAddOnsList" id="newCompleteAddOn" type="in.com.rbc.quotation.common.vo.KeyValueVo">
																	<option value='<bean:write name="newCompleteAddOn" property="key" />' > <bean:write name="newCompleteAddOn" property="value" /> </option>
																</logic:iterate>
															</logic:present>
														</select>
														
													</td>
													<td style="width:15%; padding:4px; border-bottom: 1px solid black;">
														<input id="addOnQuantity<%=idRowTech%>_<%=idxAddOn%>" name="addOnQuantity<%=idRowTech%>_<%=idxAddOn%>" onkeypress="return isNumber(event)" maxLength="2" class="short" value="" >
													</td>
													<td style="width:15%; padding:4px; border-bottom: 1px solid black;">
														<input id="addOnPercent<%=idRowTech%>_<%=idxAddOn%>" name="addOnPercent<%=idRowTech%>_<%=idxAddOn%>" onkeypress="return isNumber(event)" maxLength="6" class="short" >
													</td>
													<td style="width:20%; padding:4px; border-bottom: 1px solid black;">
														&#8377; &nbsp; <input id="addOnCash<%=idRowTech%>_<%=idxAddOn%>" name="addOnCash<%=idRowTech%>_<%=idxAddOn%>" onkeypress="return isNumber(event)" maxLength="8" class="normal" >
													</td>
													<td style="width:15%; padding:4px; border-bottom: 1px solid black;">
														<img src="html/images/deleteicon.gif" alt="Delete" onclick="javascript:removeAddOn(this,'<%=idRowTech%>');" id="delete<%=idRowTech%>_<%=idxAddOn%>" style="cursor: hand;" />
													</td>
												</tr>
											</table>
											<br/>
											<div id="add1" class="blue-light" align="right" style="padding-right:25px; width:100%;">
												<input type="hidden" id="addRowVal<%=idRowTech%>" name="addRowVal<%=idRowTech%>" value="<%=idxAddOn%>" >
												<input type="button" class="BUTTON" style="width:100px; font-weight:bold; font-size:11px; color: #990000;" onclick="javascript:createAddOn('<%=idRowTech%>');" value="Add" />
											</div>
										</td>
									</tr>
								</table>
								
								<br><br>
								
								<table style="width:100%; float:left; border:0px;" cellspacing="0" cellpadding="0">
									<tr><td colspan="4">&nbsp;</td></tr>
									<tr style="float:left; width:100%;">
										<td class="formLabelTophome" style="width:138px; margin-right:33px; border-bottom:0px !important;"> MTO Special Features </td>
										<td colspan="3">
											<textarea name="splFeatures<%=idRowTech%>" id="splFeatures<%=idRowTech%>"  style="width:1000px; height:50px" onkeyup="checkMaxLenghtText(this, 2500);" onkeydown="checkMaxLenghtText(this, 2500);" tabindex="35">   </textarea>
										</td>
									</tr>
									<tr><td colspan="4">&nbsp;</td></tr>
								</table>
							</fieldset>
						</td>
					</tr>
					
					<%	idRowTech = idRowTech+1; %>
					</logic:iterate>
					
				</table>
			</div>						
		</fieldset>
	</div>
<!-- -- Design MTO Ratings : Search Results Block -- -->

