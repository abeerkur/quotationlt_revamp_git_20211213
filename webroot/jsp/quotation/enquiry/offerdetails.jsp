<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants" %>
<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>
<%@ page import="in.com.rbc.quotation.common.utility.PropertyUtility" %>
<%@page import="in.com.rbc.quotation.common.utility.CurrencyConvertUtility" %>
<%@ page import="java.text.DecimalFormat,java.math.BigDecimal,java.net.URLDecoder" %>
<script type='text/javascript' src='dwr/engine.js'></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src='dwr/interface/saveEnquiryBasicInfo.js'></script>

<%
DecimalFormat df = new DecimalFormat("###,###");
DecimalFormat df1 = new DecimalFormat("#0");
%>

<input type="hidden" name="<%=org.apache.struts.taglib.html.Constants.TOKEN_KEY%>" value="<%=session.getAttribute(org.apache.struts.action.Action.TRANSACTION_TOKEN_KEY)%>">
 		
<html:form action="/viewEnquiry" method="POST">
<html:hidden property="invoke" value="updateFactor"/>
<html:hidden property="enquiryId"  styleId="enquiryId" />
<html:hidden property="operationType" styleId="operationType" />

<logic:present name="enquiryDto" scope="request">
<table width="80%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
<tr>
  <td height="100%" valign="top">
  	<logic:equal name="enquiryForm" property="operationType" value="viewEnquiry">
  	
	<fieldset>
		<legend class="blueheader">
			<bean:message key="quotation.offerdata.commercial.label.heading"/>
		</legend>
		
		
		
		<logic:notEmpty name="enquiryDto" property="ratingObjects">
		
         <logic:iterate name="enquiryDto" property="ratingObjects"
								               id="ratingObject" indexId="idx" 
								               type="in.com.rbc.quotation.enquiry.dto.RatingDto">	
			<bean:define id="technicalObject" name="ratingObject" property="technicalDto"/>	
			
			
			<table width="100%" cellpadding="0" cellspacing="0">
			<tr style=" font-weight:bold; background-color:#CCDAF1">
				<td  style="width:25%; text-align:right;">
					<bean:message key="quotation.search.frame"/><%= idx.intValue()+1 %>
				</td>
				<td >:
					<bean:write name="technicalObject" property="frameSize"/>
				</td>
				
				<td style="width:25%; text-align:right;">
					<bean:message key="quotation.offerdata.commercial.label.mcunit"/>
				</td>
				<td style="width:25%">
					<bean:write name="technicalObject" property="commercialMCUnit"/>
				</td>
			</tr>
			
			  <logic:notEmpty name="ratingObject" property="addOnList">
			  	 <logic:iterate name="ratingObject" property="addOnList"
							               id="addOnDto" indexId="idx1" 
							               type="in.com.rbc.quotation.enquiry.dto.AddOnDto">
					<tr>								
					<td class="formLabelview"  style="width:25%" >
						<bean:message key="quotation.offerdata.addon.label.addontype"/><%= idx1.intValue()+1 %>
					</td>
					<td class="formContentview" >
						<bean:write name="addOnDto" property="addOnTypeText"/>
					</td>
					
					<td class="formLabelview" style="width:25%">
						<bean:message key="quotation.offerdata.addon.label.unitprice"/>
					</td>
					<td class="formContentview" style="width:25% ">
						:<bean:write name="addOnDto" property="addOnUnitPrice"/>
			  		</td>
					</tr>						  
		  		</logic:iterate>
		  	</logic:notEmpty>
			</table>
			</logic:iterate>
		</logic:notEmpty>
			
	</fieldset>
	
	</logic:equal>
	
	<logic:equal name="enquiryForm" property="operationType" value="consolidatedView">
	<bean:define id="teamMemberObject" name="enquiryDto" property="teamMemberDto" />
	
 		  <tr>
			  <td>
			  	<fieldset>
		<legend class="blueheader">
			<bean:message key="quotation.offerdata.commercial.label.heading"/>
		</legend>
			  
                                   <table width="100%" border="0" align="center" cellpadding="0" cellspacing="3">
				<tr>
                       <td colspan="16"  >&nbsp;</td>
          	   </tr>
                                   
<%
Double dQuantity=new Double(0);
Double dTotalTransportation=new Double(0);
Double dTotalWarranty=new Double(0);
Double dTotalTc=new Double(0);
Double dMcnspRatio=new Double(0);
BigDecimal bdTotalPrice=new BigDecimal("0");
%>
<tr>
<logic:notEqual name="enquiryDto" property="dmFlag" value="<%=QuotationConstants.QUOTATION_STRING_TRUE%>">
<td  width="11%" class="formLabelTophome"><bean:message key="quotation.create.label.itemno"/></td>

<td width="11%" class="formLabelTophome"><bean:message key="quotation.create.label.ratedoutput"/></td>

<td width="11%" class="formLabelTophome"><bean:message key="quotation.create.label.units"/></td>

<td width="11%" class="formLabelTophome"><bean:message key="quotation.create.label.pole"/></td>

<td width="11%" class="formLabelTophome"><bean:message key="quotation.create.label.ratedvoltage"/></td>

<td width="11%" class="formLabelTophome"><bean:message key="quotation.search.frame"/></td>

<td width="11%" class="formLabelTophome"><bean:message key="quotation.offerdata.addon.label.unitprice"/></td>

<td width="11%" class="formLabelTophome"><bean:message key="quotation.offerdata.addon.label.quantity"/></td>

<td width="11%" class="formLabelTophome"><bean:message key="quotation.create.label.totalprice"/></td>
</logic:notEqual>


<logic:equal name="enquiryDto" property="dmFlag" value="<%=QuotationConstants.QUOTATION_STRING_TRUE%>">

<td  width="6%" class="formLabelTophome"><bean:message key="quotation.create.label.itemno"/></td>

<td width="6%" class="formLabelTophome"><bean:message key="quotation.create.label.ratedoutput"/></td>

<td width="6%" class="formLabelTophome"><bean:message key="quotation.create.label.units"/></td>

<td width="6%" class="formLabelTophome"><bean:message key="quotation.create.label.pole"/></td>

<td width="6%" class="formLabelTophome"><bean:message key="quotation.create.label.ratedvoltage"/></td>

<td width="6%" class="formLabelTophome"><bean:message key="quotation.search.frame"/></td>
<td width="6%" class="formLabelTophome"><bean:message key="quotation.create.label.unittransfercost"/></td>
<td width="6%" class="formLabelTophome"><bean:message key="quotation.create.label.mcnspratio"/></td>
<td width="6%" class="formLabelTophome"><bean:message key="quotation.offerdata.addon.label.unitprice"/></td>
<td width="6%" class="formLabelTophome"><bean:message key="quotation.offerdata.addon.label.quantity"/></td>
<td width="6%" class="formLabelTophome"><bean:message key="quotation.create.label.totaltransportaion"/></td>
<td width="6%" class="formLabelTophome"><bean:message key="quotation.create.label.totalwarranty"/></td>
<td width="6%" class="formLabelTophome"><bean:message key="quotation.create.label.transfercost"/></td>	
<td width="6%" class="formLabelTophome"><bean:message key="quotation.create.label.totalprice"/></td>
</logic:equal>


</tr>  
				
				
				    <logic:iterate name="enquiryDto" property="ratingObjects"
								               id="ratingObject" indexId="idx" 
								               type="in.com.rbc.quotation.enquiry.dto.RatingDto">	
				
                      <bean:define id="technicalObject" name="ratingObject" property="technicalDto"/>
                      <bean:define id="qnt" name="ratingObject" property="quantity"/>
       
       
                      	
                      <tr>
 <logic:notEqual name="enquiryDto" property="dmFlag" value="<%=QuotationConstants.QUOTATION_STRING_TRUE%>">                     
<td   width="11%" class="formContentview-rating-b0x"><%=idx+1 %></td>

<td width="11%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="kw" /></td>

<td width="11%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ratedOutputUnit" /></td>

<td width="11%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="poleName" /></td>

<td width="11%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="volts"/></td>
</logic:notEqual>
<logic:equal name="enquiryDto" property="dmFlag" value="<%=QuotationConstants.QUOTATION_STRING_TRUE%>">
<td   width="6%" class="formContentview-rating-b0x"><%=idx+1 %></td>

<td width="6%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="kw" /></td>

<td width="6%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="ratedOutputUnit" /></td>

<td width="6%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="poleName" /></td>

<td width="6%" class="formContentview-rating-b0x"><bean:write name="ratingObject" property="volts"/></td>
</logic:equal>


                    <%String sFrameSize="",sUnitpriceMultiplier_rsm="",sTotalPriceMultiplier_rsm="", sTransferCMultiplier="", sMcornspratioMultiplier_rsm="",sTransportationperunit_rsm="", sWarrantyperunit_rsm="";
                    Double dTotalTransportation_rsm=new Double(0);
                    Double dTotalWarranty_rsm=new Double(0);
                    Double dTotalTransferCost=new Double(0);
                    Double dQty=new Double(0);
                    
                    %>
   				<logic:notEmpty name="enquiryDto" property="ratingObjects">
		         <logic:iterate name="enquiryDto" property="ratingObjects"
								               id="ratingObject1" indexId="idsx1" 
								               type="in.com.rbc.quotation.enquiry.dto.RatingDto">	
				 
  
  				 <bean:define id="technicalObject1" name="ratingObject1" property="technicalDto"/>
  				 <%if(idx.intValue()==idsx1.intValue()){ %>
  				 <bean:define id="tech1" name="technicalObject1" property="frameSize"/>
  				 <bean:define id="tech2" name="technicalObject1" property="unitpriceMultiplier_rsm"/>
  	  			 <bean:define id="tech3" name="technicalObject1" property="totalpriceMultiplier_rsm"/>	  	
  	  			 <bean:define id="tech4" name="technicalObject1" property="formattedCommercialMCUnit"/>    
  	  			 <bean:define id="tech5" name="technicalObject1" property="mcornspratioMultiplier_rsm"/>
  				 <bean:define id="tech6" name="technicalObject1" property="transportationperunit_rsm"/>
  				 <bean:define id="tech7" name="technicalObject1" property="warrantyperunit_rsm"/> 
  				 
  				 <%
  				 
  				sFrameSize=String.valueOf(tech1);
  				sUnitpriceMultiplier_rsm=String.valueOf(tech2);
  				sTotalPriceMultiplier_rsm=String.valueOf(tech3);
  				sTransferCMultiplier=String.valueOf(tech4);
  				sMcornspratioMultiplier_rsm =String.valueOf(tech5);
  				sTransportationperunit_rsm= String.valueOf(tech6);
  				sWarrantyperunit_rsm=String.valueOf(tech7);

  				
  				 } %>


  				 </logic:iterate>
  				 </logic:notEmpty>	
  				 
  			<%
  				 
  			if(qnt!="" && qnt != "0")
			   {
  					dQty = Double.parseDouble(String.valueOf(qnt));
  					dQuantity=dQuantity+dQty;
			   }
			if(sTransferCMultiplier!="" && sTransferCMultiplier!=null && sTransferCMultiplier!="0")
			{
				sTransferCMultiplier=sTransferCMultiplier.trim().replaceAll(QuotationConstants.QUOTATION_INR, "");
				sTransferCMultiplier=sTransferCMultiplier.replaceAll(QuotationConstants.QUOTATION_SPACE, "");
				sTransferCMultiplier=sTransferCMultiplier.replaceAll(",", "");
				
				sTransferCMultiplier=df1.format(Double.parseDouble(sTransferCMultiplier));
				
				
			}
			else
			{
				sTransferCMultiplier= QuotationConstants.QUOTATION_ZERO;	
			}
			if(sTransferCMultiplier!="" && sTransferCMultiplier!=QuotationConstants.QUOTATION_ZERO)
			   {
				dTotalTransferCost=Double.parseDouble(sTransferCMultiplier)*dQty;
				dTotalTc=dTotalTc+dTotalTransferCost;
			   }
			if(sTransportationperunit_rsm!="" && sTransportationperunit_rsm!=QuotationConstants.QUOTATION_ZERO)
			   {
				
				sTransportationperunit_rsm=sTransportationperunit_rsm.trim().replaceAll(QuotationConstants.QUOTATION_INR, "");
				sTransportationperunit_rsm=sTransportationperunit_rsm.replaceAll(QuotationConstants.QUOTATION_SPACE, "");
				sTransportationperunit_rsm=sTransportationperunit_rsm.replaceAll(",", "");
				
				sTransportationperunit_rsm=df1.format(Double.parseDouble(sTransportationperunit_rsm));
			   }
			else{
				sTransportationperunit_rsm = QuotationConstants.QUOTATION_ZERO;
			}
			if(sTransportationperunit_rsm!="" && sTransportationperunit_rsm!=QuotationConstants.QUOTATION_ZERO)
			   {
				dTotalTransportation_rsm=Double.parseDouble(sTransportationperunit_rsm)*dQty;
				
				dTotalTransportation = dTotalTransportation+dTotalTransportation_rsm;
			   }
			
			if(sWarrantyperunit_rsm!="" && sWarrantyperunit_rsm!=QuotationConstants.QUOTATION_ZERO)
			   {
				sWarrantyperunit_rsm=sWarrantyperunit_rsm.trim().replaceAll(QuotationConstants.QUOTATION_INR, "");
				sWarrantyperunit_rsm=sWarrantyperunit_rsm.replaceAll(QuotationConstants.QUOTATION_SPACE, "");
				sWarrantyperunit_rsm=sWarrantyperunit_rsm.replaceAll(",", "");
				
				sWarrantyperunit_rsm=df1.format(Double.parseDouble(sWarrantyperunit_rsm));
			   }
			else{
				sWarrantyperunit_rsm = QuotationConstants.QUOTATION_ZERO;
			}
			
			if(sWarrantyperunit_rsm!="" && sWarrantyperunit_rsm!=QuotationConstants.QUOTATION_ZERO)
			   {
				dTotalWarranty_rsm=Double.parseDouble(sWarrantyperunit_rsm)*dQty;
				
				dTotalWarranty=dTotalWarranty+dTotalWarranty_rsm;
				
			   }


%>
<logic:notEqual name="enquiryDto" property="dmFlag" value="<%=QuotationConstants.QUOTATION_STRING_TRUE%>">  
<td width="11%" class="formContentview-rating-b0x"><%=sFrameSize %></td>

<td width="11%" class="formContentview-rating-b0x"><%=sUnitpriceMultiplier_rsm %></td>

<td width="11%" class="formContentview-rating-b0x"><strong><bean:write name="ratingObject" property="quantity"/></strong></td>

<td width="11%" class="formContentview-rating-b0x"><strong><%=sTotalPriceMultiplier_rsm %></strong></td>
</logic:notEqual>

<logic:equal name="enquiryDto" property="dmFlag" value="<%=QuotationConstants.QUOTATION_STRING_TRUE%>">
<td width="6%" class="formContentview-rating-b0x"><%=sFrameSize %></td>
<td width="6%" class="formContentview-rating-b0x"><%=df.format(Double.parseDouble(sTransferCMultiplier)) %></td>
<td width="6%" class="formContentview-rating-b0x"><%=sMcornspratioMultiplier_rsm %>%</td>
<td width="6%" class="formContentview-rating-b0x"><%=sUnitpriceMultiplier_rsm %></td>
<td width="6%" class="formContentview-rating-b0x"><strong><bean:write name="ratingObject" property="quantity"/></strong></td>
<td width="6%" class="formContentview-rating-b0x"><%=df.format(dTotalTransportation_rsm) %></td>
<td width="6%" class="formContentview-rating-b0x"><%=df.format(dTotalWarranty_rsm) %></td>
<td width="6%" class="formContentview-rating-b0x"><%=df.format(dTotalTransferCost)%></td>	
<td width="6%" class="formContentview-rating-b0x"><strong><%=sTotalPriceMultiplier_rsm %></strong></td>
</logic:equal>
</tr>  
   <%
   
    if(sTotalPriceMultiplier_rsm!="" && sTotalPriceMultiplier_rsm!=QuotationConstants.QUOTATION_ZERO)
    {
        if(sTotalPriceMultiplier_rsm.indexOf(",") != -1){
      	 String unitprice=sTotalPriceMultiplier_rsm.replaceAll(",", "");
      	 BigDecimal a=new BigDecimal(unitprice);
      	bdTotalPrice=bdTotalPrice.add(a);      
      	
        }
        else{
       	 BigDecimal a=new BigDecimal(sTotalPriceMultiplier_rsm);
       	bdTotalPrice=bdTotalPrice.add(a);      
       
        }

    	
    }
  
   
    %>                   
</logic:iterate>

<logic:equal name="enquiryDto" property="dmFlag" value="<%=QuotationConstants.QUOTATION_STRING_TRUE%>">	 
		<% 		
		dMcnspRatio = (dTotalTc/(bdTotalPrice.doubleValue()-dTotalTransportation-dTotalWarranty))*100;
		%>
				<tr>
				       <td colspan="6" class="formContentview-rating-b0x">&nbsp;</td>
                       <td align="left" class="formContentview-rating-b0x"><b><bean:message key="quotation.create.label.total"/></b></td>
                       <td class="formContentview-rating-b0x"><b><%=df1.format(dMcnspRatio)%>%</b></td>                       
                        <td colspan="1" class="formContentview-rating-b0x">&nbsp;</td>
                       <td class="formContentview-rating-b0x">
                       <b>
                       <%=df1.format(dQuantity)%>
                       </b>
                       </td>
                       <td class="formContentview-rating-b0x"><b><%=df.format(dTotalTransportation)%></b></td>
                       <td class="formContentview-rating-b0x"><b><%=df.format(dTotalWarranty)%></b></td>
                        <td class="formContentview-rating-b0x"><b><%=df.format(dTotalTc)%></b></td>
                       <td class="formContentview-rating-b0x"><b><%=df.format(bdTotalPrice)%></b></td>
          	   </tr>
          	   	<tr>
				       <td colspan="1" class="formContentview-rating-b0x"><b><bean:message key="quotation.create.label.inwords"/></b></td>
                       <td colspan="13" class="formContentview-rating-b0x" align="right">
                       <b>   
                       <%=CurrencyConvertUtility.convertToIndianCurrency(String.valueOf(bdTotalPrice)) %>
                       </b>
                       </td>
          	   </tr>
          	   
</logic:equal>
<logic:notEqual name="enquiryDto" property="dmFlag" value="<%=QuotationConstants.QUOTATION_STRING_TRUE%>">	 
				<tr>
				       <td colspan="6" class="formContentview-rating-b0x">&nbsp;</td>
                       <td align="left" class="formContentview-rating-b0x"><b><bean:message key="quotation.create.label.total"/></b></td>
                       <td class="formContentview-rating-b0x">
                       <b>
                       <%=df1.format(dQuantity)%>
                       </b>
                       </td>
                       <td class="formContentview-rating-b0x"><b><%=df.format(bdTotalPrice) %></b></td>
          	   </tr>
          	   	<tr>
				       <td colspan="2" class="formContentview-rating-b0x"><b><bean:message key="quotation.create.label.inwords"/></b></td>
                       <td colspan="11" class="formContentview-rating-b0x" align="right">
                       <b>   
                       <%=CurrencyConvertUtility.convertToIndianCurrency(String.valueOf(bdTotalPrice)) %>
                       </b>
                       </td>
          	   </tr>
          </logic:notEqual>
          	   

</table>
</fieldset>

			  </td>
			  </tr>
			  <tr>
			  <td>
			  			  	<fieldset>
			  
			   <table width="100%">
				<tr>
                       <td colspan="5"  >&nbsp;</td>
          			 </tr>
          			 
          			 <tr>
                      
  					 <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.create.label.expecteddeliverymonth"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%">
                       	<bean:write name="enquiryDto" property="expectedDeliveryMonth"/>
                       </td>
                 
                      <td rowspan="15"   >&nbsp;</td>
                      
                     
                      <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.create.label.deliveriestype"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:23% ">
                       <bean:write name="enquiryDto" property="deliveryType"/>
                      </td>    
                    </tr>
                    

                    
                    
                    
                        <tr>
                      
  					 <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.create.label.warrantydispatchdate"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%">
                       	<bean:write name="enquiryDto" property="warrantyDispatchDate"/>
                       	&nbsp;<bean:message key="quotation.create.label.months"/>
                       </td>
                 
                      
                     
                      <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.create.label.warrantycommissioningdate"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:23% ">
                       <bean:write name="enquiryDto" property="warrantyCommissioningDate"/>
                       &nbsp;<bean:message key="quotation.create.label.months"/>
                      </td>    
                    </tr>
                    

                    
                                            <tr>
                      
  					 <td class="label-text"  style="width:25%">
                      	<bean:message key="quotation.create.label.advancepayment"/>
                      </td>
                      <td class="formContentview-rating-b0x"  style="width:25%">
                       	<bean:write name="enquiryDto" property="advancePayment"/>
                       	%
                       </td>
                 
                      
                     
                      <td class="label-text" style="width:25% ">
                      	<bean:message key="quotation.create.label.paymentterms"/>
                      </td>
                      <td class="formContentview-rating-b0x" style="width:23% ">
                       <bean:write name="enquiryDto" property="paymentTerms"/>
                       %<bean:message key="quotation.create.label.beforedispatch" />
                      </td>    
                    </tr>
                    
                    
                    
                    
                    </table>
			  			  	</fieldset>
			  
			  </td>
			  </tr>
           


              <tr>
                <td>&nbsp;</td>
              </tr>
              
              <tr>
                <td >
       <fieldset>
		<legend class="blueheader">
			<bean:message key="quotation.create.label.notes"/>
		</legend>
                
                <table  bgcolor="#FFFFFF" align="center">
               <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_1)%></span></td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_2)%></span></td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_3)%></span></td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_4)%></span></td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_5)%></span></td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_6)%></span></td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_7)%></span></td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_8)%></span></td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_9)%></span></td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_10)%></span></td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_11)%></span></td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_12)%></span></td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_13)%></span></td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_NOTES_14)%></span></td>
              </tr>
              
              
              </table>
              </fieldset>
              </td>
              </tr>
              
              
              <tr>

                <td >
       <fieldset>
		<legend class="blueheader">
			<bean:message key="quotation.create.label.generalnote"/>
		</legend>
                
                <table  bgcolor="#FFFFFF" align="center">
               <tr>
                <td>&nbsp;</td>
              </tr>
              
                           
              
              <tr>
              
                <td  ><bean:message key="quotation.create.label.defintions"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
             <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_DEFINTIONS)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
             
               
               <tr>
                <td  ><bean:message key="quotation.create.label.validity"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               
              <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_VALIDITY)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              

               <tr>
                <td  ><bean:message key="quotation.create.label.scope"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_SCOPE)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
               <tr>
                <td  ><bean:message key="quotation.create.label.priceandbasis"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_PRICEANDBASIS)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
              
             <%--  <tr>
                <td  ><bean:message key="quotation.create.label.taxessduties"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_TAXES_DUTIES)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
              
              
               <tr>
                <td  ><bean:message key="quotation.create.label.statutoryvariation"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_STATUTORY_VARIATION)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               --%>
              
              
               <tr>
                <td  ><bean:message key="quotation.create.label.freightandinsurance"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_FREIGHT_AND_INSURANCE)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
              
              <tr>
                <td  ><bean:message key="quotation.create.label.packingandforwarding"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_PACKING_AND_FORWARDING)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
             
             
             
              
              <tr>
                <td><bean:message key="quotation.create.label.termsofpayments"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_TERMS_OF_PAYMENT)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
              
               <tr>
                <td><bean:message key="quotation.create.label.deliveries"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_DELIVERY)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
                <tr>
                <td><bean:message key="quotation.create.label.drawingsanddocsapproval"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_DRAWINGANDDOCSAPPROVAL)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
                <tr>
                <td><bean:message key="quotation.create.label.delayinmanufclearance"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_DELAYMANFCLEARANCE)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
               <tr>
                <td><bean:message key="quotation.create.label.forcemeasures"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_FORCE_MAJEURE)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
              
              <tr>
                <td><bean:message key="quotation.create.label.storages"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_STORAGE)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
              
               <tr>
                <td><bean:message key="quotation.create.label.warranty"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_WARRANTY)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
              
               <tr>
                <td><bean:message key="quotation.create.label.wghtsanddimension"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_WEIGHTS_AND_DIMENSIONS)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
               <tr>
                <td><bean:message key="quotation.create.label.tests"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
                <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_TESTS)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
               <tr>
                <td><bean:message key="quotation.create.label.standards"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_STANDARDS)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
              
               <tr>
                <td><bean:message key="quotation.create.label.limitationofliability"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_LIMITATION_OF_LIABILITY)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
               <tr>
                <td><bean:message key="quotation.create.label.consequentiallosses"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_CONSEQUENTIAL_LOSSES)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
               <tr>
                <td><bean:message key="quotation.create.label.arbitration"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_ARBITRATION)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
                <tr>
                <td><bean:message key="quotation.create.label.language"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
                <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_LANGUAGE)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              

                <tr>
                <td><bean:message key="quotation.create.label.governinglaw"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
                <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_GOVERNING_LAW)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
              
              <tr>
                <td><bean:message key="quotation.create.label.variationinquantity"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_VARIATION_IN_QUANTITY)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
                <tr>
                <td><bean:message key="quotation.create.label.transferoftitle"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
                <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_TRANSFER_OF_TITLE)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
              
              <tr>
                <td><bean:message key="quotation.create.label.confidentialtreatmentsecrecy"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
                <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_CONFIDENTIAL_TREATMENT_AND_SECRECY)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
              
               <tr>
                <td><bean:message key="quotation.create.label.passingofbenefitrisk"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_PASSING_OF_BENEFIT_AND_RISK)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
                <tr>
                <td><bean:message key="quotation.create.label.compensionduetobuyersdefault"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_COMPENSATION_DUE_TO_BUYERS_DEFAULT)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
              <tr>
                <td><bean:message key="quotation.create.label.suspensionsandtermination"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
              
               <tr>
                <td><bean:message key="quotation.create.label.suspensions"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
                <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_SUSPENSION)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              

               <tr>
                <td><bean:message key="quotation.create.label.terminations"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
                 <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_TERMINATION)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              

				<tr>
                <td><bean:message key="quotation.create.label.bankruptcy"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
                <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_BANKRUPTCY)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              


<tr>
                <td><bean:message key="quotation.create.label.acceptance"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
                <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_ACCEPTANCE)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
              
              <tr>
                <td><bean:message key="quotation.create.label.limitofsupply"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_LIMIT_OF_SUPPLY)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
              
                 <tr>
                <td><bean:message key="quotation.create.label.changeinscope"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_CHANGE_IN_SCOPE)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              

              <tr>
                <td><bean:message key="quotation.create.label.channelsforcommunications"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
               <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_CHANNELS_FOR_COMMUNICATION)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
              
              
              
              <tr>
                <td><bean:message key="quotation.create.label.general"/></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>
                <tr>
                <td><span ><%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATIONTERMS_PROPERTYFILENAME, QuotationConstants.QUOTATION_GENERAL)%></span></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
              </tr>


              <tr>
                <td class="blue-light" align="right">

                			<div class="blue-light" align="right">
				<bean:define id="statusId" name="enquiryDto" property="statusId" />				
				<bean:define id="createdSSO" name="enquiryDto" property="createdBySSO" />				
				
				 <logic:equal name="userSSO" scope="request" value="<%=(String)createdSSO%>">
				  <logic:equal name="enquiryDto" property="statusId"  value="6">
				  <input name="save1" type="button" onclick="update1(<bean:write name="enquiryDto" property="enquiryId"/>);" class="BUTTON" value="Save"/>
				  </logic:equal>
					 <logic:present name="salesManager" scope="request">
					  <logic:equal name="enquiryDto" property="statusId" value="6">
					 	<input name="viewpdf" type="button" onclick="viewPDF();" class="BUTTON" value="View PDF"/>
					 	</logic:equal>
					 </logic:present>
	                  <logic:present name="salesManager" scope="request">
					  <logic:equal name="enquiryDto" property="statusId" value="6">
					 	<input name="viewpdf" type="button" onclick="viewIndentPage('Won', <bean:write name="enquiryDto" property="enquiryId"/>);" class="BUTTON" value="Won"/>
					 	</logic:equal>
					 </logic:present>
					
					
					<logic:present name="salesManager" scope="request">
					  <logic:equal name="enquiryDto" property="statusId" value="6">
					 	<input name="viewpdf" type="button" onclick="viewIndentPage('Lost',<bean:write name="enquiryDto" property="enquiryId"/>);" class="BUTTON" value="Lost"/>
					 	</logic:equal>
					 </logic:present>
					 <logic:present name="salesManager" scope="request">
					  <logic:equal name="enquiryDto" property="statusId" value="6">
					 	<input name="viewpdf" type="button" onclick="viewIndentPage('Abonded',<bean:write name="enquiryDto" property="enquiryId"/>);" class="BUTTON" value="Abonded"/>
					 	</logic:equal>
					 </logic:present>
					 
					    
					
						 <logic:equal name="enquiryDto" property="statusId" value="6">
					 		<input name="revision" type="button" onclick="createRevision();" class="BUTTON" value="Revision Required"/>
					 	</logic:equal>
					 </logic:equal>
					    
				</div>
			
               </td>
               </tr>
               </table>
               </fieldset>
               </logic:equal> 
               </td>
               </tr>
               </table>
               
 
 
 
 
</logic:present>
</html:form>

