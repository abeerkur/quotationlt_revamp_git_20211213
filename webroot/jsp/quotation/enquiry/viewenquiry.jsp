<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants" %>
<%@ page import="in.com.rbc.quotation.common.utility.PropertyUtility" %>
<%@ page import="in.com.rbc.quotation.common.utility.CommonUtility" %>
<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>

<html:form action="createEnquiry.do" method="POST">
<html:hidden property="invoke" value="createEnquiry"/>
<html:hidden property="enquiryId"/>
<html:hidden property="enquiryNumber"/>
<html:hidden property="ratingsValidationMessage"/>
<html:hidden property="dispatch" value="viewSubmit"/>
<input type="hidden" name='<%= org.apache.struts.taglib.html.Constants.TOKEN_KEY %>'
         value='<%= session.getAttribute(org.apache.struts.action.Action.TRANSACTION_TOKEN_KEY) %>' />

<!-------------------------- main table Start  --------------------------------->
<table width="80%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
<tr>
<td>
 <div class="sectiontitle"><bean:message key="quotation.viewenquiry.label.heading"/> </div>
 
<logic:present name="displayPaging" scope="request">
							 
						      <table align="right"> 
						       <tr>
						            <td>
									<fieldset style="width:100%">
									 <table cellpadding="0" cellspacing="2" border="0"> 
						      		 <tr>
						            <td style="text-align:center">
	      								<a href="searchenquiry.do?invoke=viewSearchEnquiryResults&dispatch=fromViewPage" class="returnsearch">
	      								Return to search<img src="html/images/back.gif" width="16" height="17" 	border="0" align="absmiddle" /></a>
	      							</td>
						        </tr>
						        <tr class="other" align="right">
						            <td align="right">
							            <logic:present name="previousId" scope="request">	
							            	<bean:define name="previousId" id="previousId" scope="request"/>
											<bean:define name="previousIndex" id="previousIndex" scope="request"/>	
											<bean:define name="previousStatus" id="previousStatus" scope="request"/>				
							            	<a href="createEnquiry.do?invoke=validateRequestAction&enquiryId=<bean:write name="previousId" scope="request"/>&statusId=<bean:write name="previousStatus" scope="request"/>&index=<bean:write name="previousIndex" scope="request"/>"><img src="html/images/PrevPage.gif" alt="" width="17" height="17" border="0" align="absmiddle" /></a>
							            </logic:present>
							
							            <logic:present name="displayMessage" scope="request">
							            	<bean:write name="displayMessage" scope="request"/>
							            </logic:present>
							
							            <logic:present name="nextId" scope="request">
							            	<a href="createEnquiry.do?invoke=validateRequestAction&enquiryId=<bean:write name="nextId" scope="request"/>&statusId=<bean:write name="nextStatus" scope="request"/>&index=<bean:write name="nextIndex" scope="request"/>"><img src="html/images/NextPage.gif" alt="Next Request" width="17" height="17" border="0" align="absmiddle" /></a>
							            </logic:present>
								  </td>
						       		</tr>
						       		</table> 										
								</fieldset>           	
									</td>
						       	</tr>
						       	</table>
</logic:present>
</td>
</tr>




  <tr>
    <td height="100%" valign="top">
       <!-------------------------- Top Title and Status & Note block Start  --------------------------------->
	   
        <table width="100%" border="0" cellspacing="1" cellpadding="0">
          <tr>
            <td rowspan="2"><table width="98%"  border="0" cellspacing="2" cellpadding="0" class="DotsTable">
              <tr>
                <td rowspan="2" style="width:70px "><img src="html/images/icon_quote.gif" width="52" height="50"></td>
                  <td class="other"><bean:message key="quotation.viewenquiry.label.subheading1"/> <bean:write name="enquiryForm" property="enquiryNumber"/><br>
                  <bean:message key="quotation.viewenquiry.label.subheading2"/> </td>
                </tr>
              <tr>
                <td class="other"><table width="100%">
                  <tr>
                    <td height="16" class="section-head" align="center">
                    	<bean:message key="quotation.viewenquiry.label.status"/> <bean:write name="enquiryForm" property="statusName"/> </td>
                      </tr>
                  </table></td>
                </tr>
            </table></td>
            <logic:lessThan name="enquiryForm" property="statusId" value="6"> 
	            <td align="right" class="other" >
				<table><tr><td>
				<bean:message key="quotation.attachments.message"/>
				</td><td>
        	    	<a href="javascript:manageAttachments('<%= CommonUtility.getIntValue(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_ATTACHMENT_APPLICATIONID)) %>','<bean:write name="enquiryForm" property="enquiryId"/>', '<bean:write name="enquiryForm" property="createdBySSO"/>');"><img src="html/images/ico_attach.gif" alt="Manage Attachments"  border="0" align="right">
   		      </a></td></tr></table>            	</td>
   	        </logic:lessThan>
          </tr>
          <tr>
            <td style="width:45% ">
            <logic:present name="attachmentList" scope="request">
            <logic:notEmpty name="attachmentList">
              <fieldset>
              <table width="100%" class="other">
				<logic:iterate name="attachmentList" id="attachmentData" indexId="idx"
								               type="in.com.rbc.quotation.common.vo.KeyValueVo">	
					<tr><td>
						<a href="javascript:download('<bean:write name="attachmentData" property="key"/>', '<bean:write name="attachmentData" property="value"/>', '<bean:write name="attachmentData" property="value2"/>');">
							<bean:write name="attachmentData" property="value"/>.<bean:write name="attachmentData" property="value2"/>
						</a>
					</td></tr>					
				</logic:iterate>
              </table>
              </fieldset>              
            </logic:notEmpty>
            </logic:present>
           </td>
          </tr>
          <logic:present name="attachmentMessage" scope="request">
          	<tr><td>
            	<bean:write name="attachmentMessage"/>
            </td></tr>                  	
          </logic:present>
        </table>
	     <br><br>
        <br>
		<!-------------------------- Top Title and Status & Note block End  --------------------------------->
		 <!----------------------- Employee Request Information block start -------------------------------------->
        <fieldset>
          <legend class="blueheader"><bean:message key="quotation.viewenquiry.label.request.heading"/></legend>

 
 			  <table width="100%">
			  <tr>
			  <td style="vertical-align:top" >
				<table width="100%">
				 <tr >				  
				  <label class="blue-light" style="font-weight: bold;">
				    	<bean:message key="quotation.create.label.basicinformation"/>
				   </label>				    
				  </tr>
				  <tr>
				  <td class="label-text" >
				    	<bean:message key="quotation.create.label.enquirytype"/>
				    	
				    </td>
				    <td  class="formContentview-rating-b0x"><bean:write name="enquiryForm" property="enquiryType"/></td>
					</tr>
					<tr>
				    <td  class="label-text" >
				    	<bean:message key="quotation.create.label.customer"/>
				    	
					</td>
					<td  class="formContentview-rating-b0x"><bean:write name="enquiryForm" property="customerName"/></td>
                     </tr>
                  <tr>
					<td class="label-text">
				    	<bean:message key="quotation.create.label.projectname"/>
				    </td>
				    <td  class="formContentview-rating-b0x"><bean:write name="enquiryForm" property="projectName"/></td>
					</tr>
                     
                     <tr>
					
					<td class="label-text" >
				    	<bean:message key="quotation.create.label.salesstage"/>
				    	
				    </td>
				     <td  class="formContentview-rating-b0x"><bean:write name="enquiryForm" property="salesStage"/></td>
					</tr>
					
					<tr>
					
					<td class="label-text">
				    	<bean:message key="quotation.create.label.approval"/>
				    	
				    </td>
				    <td  class="formContentview-rating-b0x"><bean:write name="enquiryForm" property="approval"/></td>
					</tr>
                     
                     <tr>
                     
                     <td class="label-text">
				    	<bean:message key="quotation.create.label.customertype"/>				    	
				    </td>
				    <td  class="formContentview-rating-b0x"><bean:write name="enquiryForm" property="customerType"/></td>
					</tr>
					<tr>
					<td class="label-text">
				    	<bean:message key="quotation.create.label.enduserindustry"/>				    	
				    </td>
				    <td  class="formContentview-rating-b0x"><bean:write name="enquiryForm" property="endUserIndustry"/></td>
					</tr>
					
					<tr>
					<td class="label-text">
				    	<bean:message key="quotation.create.label.enduser"/>
				    </td>
				      <td  class="formContentview-rating-b0x"><bean:write name="enquiryForm" property="endUser"/></td>                 	                      	
                    </tr>				
					
					<tr>
					
					<td class="label-text">
				    	<bean:message key="quotation.create.label.orderclosingmonth"/>				    	
				    </td>
				    <td  class="formContentview-rating-b0x"><bean:write name="enquiryForm" property="orderClosingMonth"/></td>
					</tr>
					<tr>
					<td class="label-text">
				    	<bean:message key="quotation.create.label.targeted"/>				    	
				    </td>
				    <td  class="formContentview-rating-b0x"><bean:write name="enquiryForm" property="targeted"/></td>
					</tr>
					<tr>
					<td class="label-text">
				    	<bean:message key="quotation.create.label.winningchance"/>				    	
				    </td>
				    <td  class="formContentview-rating-b0x"><bean:write name="enquiryForm" property="winningChance"/></td>
					</tr>
					<tr>
					<td class="label-text" >
				    	<bean:message key="quotation.create.label.deliveriestype"/>
				    	
				    </td>
				    <td  class="formContentview-rating-b0x"><bean:write name="enquiryForm" property="deliveryType"/></td>
					</tr>
					<tr>
					<td class="label-text">
				    	<bean:message key="quotation.create.label.expecteddeliverymonth"/>				    	
				    </td>
				    <td  class="formContentview-rating-b0x"><bean:write name="enquiryForm" property="expectedDeliveryMonth"/></td>
					</tr>
					<tr>
					<td class="label-text">
				    	<bean:message key="quotation.create.label.totalordervalue"/>
				    	
				    </td>
				    <td  class="formContentview-rating-b0x"><bean:write name="enquiryForm" property="totalOrderValue"/></td>
					</tr>
					</table>
					</td>
					<td width="2%" height="100%" rowspan="3" align="center"  class="dividingdots">&nbsp;</td>
					<td style="vertical-align:top" >
					
					
					
					<table width="100%">
				 	<tr>				  
				  	<label class="blue-light" style="font-weight: bold;">
				    	<bean:message key="quotation.create.label.result"/>
				    </label>
				  </tr>
				  <tr>
				  <td class="label-text" style="width: 50%;">
				    	<bean:message key="quotation.create.label.winlossprice"/>
				    	
				    </td>
				    <td  class="formContentview-rating-b0x" style="width: 50%;"><bean:write name="enquiryForm" property="winlossPrice"/></td>
					</tr>
					<tr>
					<td class="label-text">
				    	<bean:message key="quotation.create.label.winlossreason"/>
				    	
				    </td>
				    <td  class="formContentview-rating-b0x"  ><bean:write name="enquiryForm" property="winlossReason"/></td>
					</tr>
					<tr>
					<td class="label-text">
				    	<bean:message key="quotation.create.label.winlosscomment"/>
				    	
				    </td>
				    <td  class="formContentview-rating-b0x"><bean:write name="enquiryForm" property="winlossComment"/></td>
                    </tr>
                   
                    <tr>
					<td class="label-text">
				    	<bean:message key="quotation.create.label.winningcompetitor"/>
				    	
				    </td>
				    <td  class="formContentview-rating-b0x"><bean:write name="enquiryForm" property="winningCompetitor"/></td>
                     </tr>
                                              
                     </table>
                     </td>
                     <td width="1%" height="100%" rowspan="3" align="center"  class="dividingdots">&nbsp;</td>
          
                     <td style="vertical-align:top" >
                     <table width="100%">
				 	<tr>				  
				  	<label class="blue-light" style="font-weight: bold;">
				    	<bean:message key="quotation.create.label.financial"/>
				    </label>
				  </tr>
				  <tr>
					 	<td class="label-text" style="width: 50%;">
					    	<bean:message key="quotation.create.label.materialcost"/>
					    	
					    </td>
					    <td  class="formContentview-rating-b0x" style="width: 50%;"><bean:write name="enquiryForm" property="materialCost"/></td>
					</tr>
					<tr>					
					<td class="label-text">
				    	<bean:message key="quotation.create.label.mcnspratio"/>
				    	
				    	
				    </td>
				    <td  class="formContentview-rating-b0x"><bean:write name="enquiryForm" property="mcnspRatio"/></td>
                    </tr>                   
                    <tr>					
					<td class="label-text">
				    	<bean:message key="quotation.create.label.totalprice"/>
				    	
				    </td>
				    <td  class="formContentview-rating-b0x"><bean:write name="enquiryForm" property="totalPrice"/></td>
                     </tr>
                     <tr>
					
					<td class="label-text">
				    	<bean:message key="quotation.create.label.warrantycost"/>
				    	
				    </td>
				    <td  class="formContentview-rating-b0x"><bean:write name="enquiryForm" property="warrantyCost"/></td>
					</tr>
					<tr>
					<td class="label-text">
				    	<bean:message key="quotation.create.label.warrantydispatchdate"/>
				    </td>
				    <td  class="formContentview-rating-b0x"><bean:write name="enquiryForm" property="warrantyDispatchDate"/></td>
					 </tr>              
                    <tr>
					<td class="label-text">
				    	<bean:message key="quotation.create.label.warrantycommissioningdate"/>
				    </td>
				    <td  class="formContentview-rating-b0x"><bean:write name="enquiryForm" property="warrantyCommissioningDate"/></td>
                    </tr>
                    
                    <tr>
					<td class="label-text">
				    	<bean:message key="quotation.create.label.advancepayment"/>(%)
				    	
				    </td>
				    <td  class="formContentview-rating-b0x"><bean:write name="enquiryForm" property="advancePayment"/></td>
					</tr>
						<tr>
					<td class="label-text">
				    	<bean:message key="quotation.create.label.paymentterms"/>
				    	
				    </td>
				    <td  class="formContentview-rating-b0x"><bean:write name="enquiryForm" property="paymentTerms"/></td>
					</tr>
                    
					
					 <tr>
					
					<td class="label-text">
				    	<bean:message key="quotation.create.label.supervisioneccost"/>
				    	
				    </td>
				    <td  class="formContentview-rating-b0x"><bean:write name="enquiryForm" property="ecSupervisionCost"/></td>
					</tr>
					<tr>
					<td class="label-text">
				    	<bean:message key="quotation.create.label.sparescost"/>
				    	
				    </td>
				    <td  class="formContentview-rating-b0x"><bean:write name="enquiryForm" property="sparesCost"/></td>
					</tr>
					<tr>
					<td class="label-text">
				    	<bean:message key="quotation.create.label.transportationcost"/>
				    	
				    </td>
				    <td  class="formContentview-rating-b0x"><bean:write name="enquiryForm" property="transportationCost"/></td>
                    </tr>
                    <tr>
                    <td class="label-text">
				    	<bean:message key="quotation.create.label.hzareacertcost"/>
				    	
				    </td>
					<td  class="formContentview-rating-b0x"><bean:write name="enquiryForm" property="hzAreaCertCost"/></td>
				    
					</tr>
                    <tr>
					<td class="label-text">
				    	<bean:message key="quotation.create.label.totalpackageprice"/>
				    	
				    </td>
				    <td  class="formContentview-rating-b0x"><bean:write name="enquiryForm" property="totalPkgPrice"/></td>				    
                    </tr>
                     </table>
                     </td>
                     </tr>
                     </table>     
                 
                     
										         
                  
				<table width="100%">
                
                				  <tr >				  
				    <td class="label-text" style="width: 100%;" colspan="2">
				    	<bean:message key="quotation.create.label.enquiryreferencenumber"/>
				    	&nbsp;&nbsp;
             			<font class="formContentview-rating-b0x">
				    	<logic:equal name="enquiryForm" property="enquiryReferenceNumber" value="">
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;				    	
				    	</logic:equal>
				    	<logic:notEqual name="enquiryForm" property="description" value="">
				    	<bean:write name="enquiryForm" property="enquiryReferenceNumber"/>
				    	
				    	</logic:notEqual>
				    	</font>
                    	
                   </td>
                  </tr>
                                  
				  <tr >				  
				    <td class="label-text" style="width: 100%;" colspan="2">
				    	<bean:message key="quotation.create.label.description"/>
				    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				    	&nbsp;
				    	<font class="formContentview-rating-b0x">
				    	<logic:equal name="enquiryForm" property="description" value="">
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;				    	
				    	</logic:equal>
				    	<logic:notEqual name="enquiryForm" property="description" value="">
				    	<bean:write name="enquiryForm" property="description"/>
				    	
				    	</logic:notEqual>
				    	
                    	</font>	
                   </td>
                  </tr>
                  
                 <tr >			
                    <td style="width: 100%;" colspan="2">
                    	<bean:message key="quotation.create.label.createdby"/>:
                    	&nbsp;
                    	<font class="formContentview-rating-b0x"><bean:write name="enquiryForm" property="createdBy"/> </font>
                    	<bean:message key="quotation.create.label.createddate"/>:
                    	&nbsp;
                    	<font class="formContentview-rating-b0x"><bean:write name="enquiryForm" property="createdDate"/>
                    	</font>
                    </td>
                  </tr>	
                  
			    </table>
 
 
 
  </fieldset>
	   <!----------------------- Employee Request Information block End -------------------------------------->
	   
	   <!-----------------------Rating block start -------------------------------------->
	   <logic:notEmpty name="enquiryForm" property="ratingObjects">
	   		<logic:iterate name="enquiryForm" property="ratingObjects"
								               id="ratingData" indexId="idx"
								               type="in.com.rbc.quotation.enquiry.dto.RatingDto">	
				<fieldset>
          			<legend class="blueheader" ID="RATING1">
          				<bean:write name="ratingData" property="ratingTitle"/>
          			</legend>
			   		<br>
			   		
			   		 <table width="100%">
           			 <tr>
              			<td colspan="5" class="blue-light-btn" style="height:25px " align="right" >
              				<bean:message key="quotation.create.label.quantityrequired"/>
              				<bean:write name="ratingData" property="quantity"/>&nbsp;
              			</td>
           			 </tr>
            		 <tr>
                       <td colspan="5"  >&nbsp;</td>
          			 </tr>
            		<tr>
            		<td class="label-text" style="width:25%">
              			<bean:message key="quotation.create.label.kw"/>
              		</td>
              		<td class="formContentview-rating-b0x" style="width:25%">
              			<bean:write name="ratingData" property="kw"/>
              			<logic:notEqual name="ratingData" property="kw" value="" >
              			<bean:write name="ratingData" property="ratedOutputUnit"/>
              			</logic:notEqual>
              			
              		</td>
             		 
              		<td rowspan="15"  class="dividingdots" >&nbsp;</td>
              		<td  class="label-text" style="width:25% ">
             		 	<bean:message key="quotation.create.label.htlt"/>
             		 </td>
             		 <td class="formContentview-rating-b0x">
             		 	<bean:write name="ratingData" property="htltName"/>
					 </td>
              		
            	</tr>
           		<tr>
              		<td class="label-text">
              			<bean:message key="quotation.create.label.pole"/>
              		</td>
              		<td class="formContentview-rating-b0x">
              			<bean:write name="ratingData" property="poleName"/> 
              		</td>
              		<td class="label-text">
              			<bean:message key="quotation.create.label.frequency"/>
              		</td>
              		<td class="formContentview-rating-b0x">
              			<logic:notEqual name="ratingData" property="frequency" value="">
              				<bean:write name="ratingData" property="frequency"/>
              			</logic:notEqual>
              			<logic:notEqual name="ratingData" property="frequencyVar" value="">
	              			+/-<bean:write name="ratingData" property="frequencyVar"/>%
	              		</logic:notEqual>
              		</td>
           		</tr>
            	<tr>
              		<td class="label-text">
              			<bean:message key="quotation.create.label.scsr"/>
              		</td>
              		<td class="formContentview-rating-b0x">
              			<bean:write name="ratingData" property="scsrName"/> 
              		</td>
              	
              	
              	 <td height="100%" class="label-text">
              	  	<bean:message key="quotation.create.label.duty"/>
              	  </td>
              	  <td class="formContentview-rating-b0x">
              	  	<bean:write name="ratingData" property="dutyName"/> 
              	  </td>
              	
           	   </tr>
           	   <tr>
           	   <td class="label-text">
              		<bean:message key="quotation.create.label.volts"/>
              	</td>
              	 <td class="formContentview-rating-b0x">
              		<logic:notEqual name="ratingData" property="volts" value="">
              			<bean:write name="ratingData" property="volts"/>
              		</logic:notEqual>
              		<logic:notEqual name="ratingData" property="voltsVar" value="">
	              		 +/-<bean:write name="ratingData" property="voltsVar"/> %
	              	</logic:notEqual>
              	</td>	
              	  	
                  <td class="label-text">
                  	<bean:message key="quotation.create.label.ambience"/>
                  </td>
                  <td class="formContentview-rating-b0x">
                  	<logic:notEqual name="ratingData" property="ambience" value="">
                  		<bean:write name="ratingData" property="ambience"/> 
                  	</logic:notEqual>
                  	
                  	<logic:notEqual name="ratingData" property="ambienceVar" value="">
	                  	+/-<bean:write name="ratingData" property="ambienceVar"/>
	                </logic:notEqual>
                  </td>
               </tr>
           	   <tr>
           	   <td class="label-text">
              	<bean:message key="quotation.create.label.enclosure"/>
              </td>
              <td class="formContentview-rating-b0x">
              	<bean:write name="ratingData" property="enclosureName"/> 
              </td>
                 <td  class="label-text">
                 	<bean:message key="quotation.create.label.degpro"/>
                 </td>
                 <td class="formContentview-rating-b0x">
                 	<bean:write name="ratingData" property="degreeOfProName"/> 
                 </td>
                
              </tr>
              
              <tr>
              
              <td class="label-text">
              	<bean:message key="quotation.create.label.mounting"/>
              </td>
              <td class="formContentview-rating-b0x">
              	<bean:write name="ratingData" property="mountingName"/> 
              </td>
              
               <td class="label-text">
                 	<bean:message key="quotation.create.label.altitude"/>
				 </td>
                 <td class="formContentview-rating-b0x">
                 	<logic:notEqual name="ratingData" property="altitude" value="">
                 		<bean:write name="ratingData" property="altitude"/> 
                 	</logic:notEqual>
                 	<logic:notEqual name="ratingData" property="altitudeVar" value="">
                 		+/-<bean:write name="ratingData" property="altitudeVar"/> 
                 	</logic:notEqual>
                 </td>
              </tr>
              <tr>
               <td height="100%"  class="label-text">
              	<bean:message key="quotation.create.label.application"/>
              </td>
              <td class="formContentview-rating-b0x">
              	<bean:write name="ratingData" property="applicationName"/>  
              </td>
              
                 <td  class="label-text">
                 	<bean:message key="quotation.create.label.stgmethod"/>
              	 </td>
             	 <td class="formContentview-rating-b0x">
             	 	<bean:write name="ratingData" property="methodOfStg"/> 
             	 </td>
             	 </tr>
             	 <tr>
             	 <td>&nbsp;</td>
			      	<td>&nbsp; </td>
             	 
            	 <td class="label-text">
            	 	<bean:message key="quotation.create.label.temprise"/>
            	 </td>
              	 <td class="formContentview-rating-b0x">
              	 	<logic:notEqual name="ratingData" property="tempRaise" value="">
	              	 	<bean:write name="ratingData" property="tempRaise"/> 
	              	</logic:notEqual>
	              	
	              	              	 </td>
             </tr>
            <tr>
            <td>&nbsp;</td>
			 <td>&nbsp; </td>
              <td  class="label-text">
              	<bean:message key="quotation.create.label.coupling"/>
              </td>
              <td class="formContentview-rating-b0x">
              	<bean:write name="ratingData" property="coupling"/> 
              </td>
              </tr>
              
           <tr>
            <td>&nbsp;</td>
			 <td>&nbsp; </td>              
              <td class="label-text">
              	<bean:message key="quotation.create.label.insulationclass"/>
              </td>
              <td class="formContentview-rating-b0x">
              	<bean:write name="ratingData" property="insulationName"/> 
              </td>
            </tr>
            <tr>
            	<td>&nbsp;</td>
				<td>&nbsp; </td>  
              <td height="100%"  class="label-text">
              	<bean:message key="quotation.create.label.termbox"/>
              </td>
              <td class="formContentview-rating-b0x">
              	<bean:write name="ratingData" property="termBoxName"/> 
              </td>
              
            </tr>
            <tr>
            <td>&nbsp;</td>
			 <td>&nbsp; </td>  
              <td height="100%"  class="label-text">
              	<bean:message key="quotation.create.label.rotdirection"/>
              </td>
              <td class="formContentview-rating-b0x">
              	<bean:write name="ratingData" property="directionOfRotName"/> 
              </td>
              
            </tr>
            <tr>
            <td>&nbsp;</td>
			 <td>&nbsp; </td> 
              <td  class="label-text">
              	<bean:message key="quotation.create.label.shaftext"/>
              </td>
              <td class="formContentview-rating-b0x">
              	<bean:write name="ratingData" property="shaftExtName"/> 
              </td>
              </tr>
              <tr>
              <td>&nbsp;</td>
			 <td>&nbsp; </td> 
              <td class="label-text">
              	<bean:message key="quotation.create.label.paint"/>
              </td>
              <td class="formContentview-rating-b0x">
              	<bean:write name="ratingData" property="paint"/>  
              </td>
            </tr>
            <tr>
            <td>&nbsp;</td>
			 <td>&nbsp; </td> 
			  <td  class="label-text">
                 	<bean:message key="quotation.create.label.ratingunitprice"/>
              	 </td>
             	 <td class="formContentview-rating-b0x">
             	 	<bean:write name="ratingData" property="ratingUnitPrice"/> 
             	 </td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td  class="label-text">
              	<bean:message key="quotation.create.label.specialfeatures"/>
              </td>
              <td colspan="4" class="formContentview-rating-b0x" style="word-break:break-all;height: 40px">
              	<bean:write name="ratingData" property="splFeatures"/>  
			  </td>
            </tr>
                       </table>
			    <br>
			    
		<logic:equal name="enquiryForm" property="statusId" value="1">
          <div class="blue-light" align="right">
          	<bean:define id="ratingId" name="ratingData" property="ratingId"/>                    
            <input name="Button" type="button" class="BUTTON" id="delete" value="Delete"  onclick="javascript:deleteRating('<bean:write name="ratingData" property="ratingId" />');" />
            
        </div>
        </logic:equal>
			   	</fieldset>
								               
			</logic:iterate>
	   </logic:notEmpty>
	   
       
			
			 <!-----------------------Rating block End -------------------------------------->
			 <!-------------------------- Bottum Input buttons block Start  --------------------------------->
		<logic:equal name="enquiryForm" property="statusId" value="1">
    	    <div class="blue-light" align="right">
    	     <logic:notEmpty name="enquiryForm" property="ratingObjects">
	        	 <logic:notPresent name="copy" scope="request">
	        	 	<input name="Button" type="button" class="BUTTON" id="edit" onclick="javascript:editRating('<bean:write name="ratingData" property="ratingId" />');"  value="Edit">
	        	 	<html:button property="" styleClass="BUTTON" value="Submit" onclick="javascript:submitViewEnquiry();"/>
	        	 </logic:notPresent>
	       	 </logic:notEmpty>
	       	 
	       	  <logic:empty name="enquiryForm" property="ratingObjects">
	       	  	<input name="Button" type="button" class="BUTTON" id="edit" onclick="javascript:editRating(0);"  value="Add New Rating">        
	       	  </logic:empty>
          	</div>
        </logic:equal>
        
		  <!-------------------------- Bottum Input buttons block End  --------------------------------->
    </td>
  </tr>
</table>
</html:form>
<style>
td {
    font-family: Verdana, Arial, Helvetica, sans-serif;
}
.blueheader {
    FONT-WEIGHT: bolder;
    FONT-SIZE: 15px;
    COLOR: #00458A;
    font-family: Arial, Helvetica, sans-serif;
    padding-right: 10px;
}
.blue-light {
    background: #CCD8F2;
    /* padding-left: 3px; */
    /* padding-right: 3px; */
    width: 97%;
    padding: 5px;
    float: left;
    margin: 0 0 10px 0;
    font-size: 12px;
}
.dividingdots {
    border-right: #999999 1px dotted;
}


.mandatory {
    background-image: url(../images/bullets.gif);
    background-repeat: no-repeat;
    background-position: left center;
    padding-left: 10px;
    margin-left: 2px;
}
.viewtext{
	margin-left: 5px;
    float: left;
    line-height: 21px;
	font-size:11px;
	}
</style>