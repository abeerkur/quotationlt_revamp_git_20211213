<!-- Commercial Terms and Conditions : Section : Start -->

			<table width="100%">
				<tr>
					<td style="vertical-align: top">
						<table width="100%">
							<tr>
								<label class="blue-light" style="font-weight: bold;"> Commercial Terms and Conditions </label>
							</tr>
						</table>
												
						<table id="table0" width="100%" align="center" cellspacing="0" cellpadding="2">
							<tr id="top">
								<td class="label-text" style="width:15% !important; color:#990000;" >Payment Terms <span class="mandatory">&nbsp;</span> </td>
								<td class="label-text" style="width:15% !important; color:#990000;" >%age amount of Nett. Order value </td>
								<td class="label-text" style="width:15% !important; color:#990000;" >Payment Days </td>
								<td class="label-text" style="width:35% !important; color:#990000;" >Payable Terms </td>
								<td class="label-text" style="width:15% !important; color:#990000;" >Instrument </td>
							</tr>
														
							<tr>
								<td class="formContentview-rating-b0x"> Advance Payment 1 </td>
								<td class="formContentview-rating-b0x">
									<bean:write name="newenquiryForm" property="pt1PercentAmtNetorderValue" />
									<html:hidden property="pt1PercentAmtNetorderValue" styleId="pt1PercentAmtNetorderValue" value='<bean:write name="newenquiryForm" property="pt1PercentAmtNetorderValue" />' />
								</td>
								<td class="formContentview-rating-b0x">
									<bean:write name="newenquiryForm" property="pt1PaymentDays" />
									<html:hidden property="pt1PaymentDays" styleId="pt1PaymentDays" value='<bean:write name="newenquiryForm" property="pt1PaymentDays" />' />
								</td>
								<td class="formContentview-rating-b0x">
									<bean:write name="newenquiryForm" property="pt1PayableTerms" />
									<html:hidden property="pt1PayableTerms" styleId="pt1PayableTerms" value='<bean:write name="newenquiryForm" property="pt1PayableTerms" />' />
								</td>
								<td class="formContentview-rating-b0x">
									<bean:write name="newenquiryForm" property="pt1Instrument" />
									<html:hidden property="pt1Instrument" styleId="pt1Instrument" value='<bean:write name="newenquiryForm" property="pt1Instrument" />' />
								</td>
							</tr>
							<tr>
								<td class="formContentview-rating-b0x"> Advance Payment 2 </td>
								<td class="formContentview-rating-b0x">
									<bean:write name="newenquiryForm" property="pt2PercentAmtNetorderValue" />
									<html:hidden property="pt2PercentAmtNetorderValue" styleId="pt2PercentAmtNetorderValue" value='<bean:write name="newenquiryForm" property="pt2PercentAmtNetorderValue" />' />
								</td>
								<td class="formContentview-rating-b0x">
									<bean:write name="newenquiryForm" property="pt2PaymentDays" />
									<html:hidden property="pt2PaymentDays" styleId="pt2PaymentDays" value='<bean:write name="newenquiryForm" property="pt2PaymentDays" />' />
								</td>
								<td class="formContentview-rating-b0x">
									<bean:write name="newenquiryForm" property="pt2PayableTerms" />
									<html:hidden property="pt2PayableTerms" styleId="pt2PayableTerms" value='<bean:write name="newenquiryForm" property="pt2PayableTerms" />' />
								</td>
								<td class="formContentview-rating-b0x">
									<bean:write name="newenquiryForm" property="pt2Instrument" />
									<html:hidden property="pt2Instrument" styleId="pt2Instrument" value='<bean:write name="newenquiryForm" property="pt2Instrument" />' />
								</td>
							</tr>
							<tr>
								<td class="formContentview-rating-b0x">Main Payment</td>
								<td class="formContentview-rating-b0x">
									<bean:write name="newenquiryForm" property="pt3PercentAmtNetorderValue" />
									<html:hidden property="pt3PercentAmtNetorderValue" styleId="pt3PercentAmtNetorderValue" value='<bean:write name="newenquiryForm" property="pt3PercentAmtNetorderValue" />' />
								</td>
								<td class="formContentview-rating-b0x">
									<bean:write name="newenquiryForm" property="pt3PaymentDays" />
									<html:hidden property="pt3PaymentDays" styleId="pt3PaymentDays" value='<bean:write name="newenquiryForm" property="pt3PaymentDays" />' />
								</td>
								<td class="formContentview-rating-b0x"> 
									<bean:write name="newenquiryForm" property="pt3PayableTerms" />
									<html:hidden property="pt3PayableTerms" styleId="pt3PayableTerms" value='<bean:write name="newenquiryForm" property="pt3PayableTerms" />' />
								</td>
								<td class="formContentview-rating-b0x">
									<bean:write name="newenquiryForm" property="pt3Instrument" />
									<html:hidden property="pt3Instrument" styleId="pt3Instrument" value='<bean:write name="newenquiryForm" property="pt3Instrument" />' />
								</td>
							</tr>
							<tr>
								<td class="formContentview-rating-b0x">Retention Payment 1</td>
								<td class="formContentview-rating-b0x">
									<bean:write name="newenquiryForm" property="pt4PercentAmtNetorderValue" />
									<html:hidden property="pt4PercentAmtNetorderValue" styleId="pt4PercentAmtNetorderValue" value='<bean:write name="newenquiryForm" property="pt4PercentAmtNetorderValue" />' />
								</td>
								<td class="formContentview-rating-b0x">
									<bean:write name="newenquiryForm" property="pt4PaymentDays" />
									<html:hidden property="pt4PaymentDays" styleId="pt4PaymentDays" value='<bean:write name="newenquiryForm" property="pt4PaymentDays" />' />
								</td>
								<td class="formContentview-rating-b0x">
									<bean:write name="newenquiryForm" property="pt4PayableTerms" />
									<html:hidden property="pt4PayableTerms" styleId="pt4PayableTerms" value='<bean:write name="newenquiryForm" property="pt4PayableTerms" />' />
								</td>
								<td class="formContentview-rating-b0x">
									<bean:write name="newenquiryForm" property="pt4Instrument" />
									<html:hidden property="pt4Instrument" styleId="pt4Instrument" value='<bean:write name="newenquiryForm" property="pt4Instrument" />' />
								</td>
							</tr>
							<tr>
								<td class="formContentview-rating-b0x">Retention Payment 2</td>
								<td class="formContentview-rating-b0x">
									<bean:write name="newenquiryForm" property="pt5PercentAmtNetorderValue" />
									<html:hidden property="pt5PercentAmtNetorderValue" styleId="pt5PercentAmtNetorderValue" value='<bean:write name="newenquiryForm" property="pt5PercentAmtNetorderValue" />' />
								</td>
								<td class="formContentview-rating-b0x">
									<bean:write name="newenquiryForm" property="pt5PaymentDays" />
									<html:hidden property="pt5PaymentDays" styleId="pt5PaymentDays" value='<bean:write name="newenquiryForm" property="pt5PaymentDays" />' />
								</td>
								<td class="formContentview-rating-b0x">
									<bean:write name="newenquiryForm" property="pt5PayableTerms" />
									<html:hidden property="pt5PayableTerms" styleId="pt5PayableTerms" value='<bean:write name="newenquiryForm" property="pt5PayableTerms" />' />
								</td>
								<td class="formContentview-rating-b0x">
									<bean:write name="newenquiryForm" property="pt5Instrument" />
									<html:hidden property="pt5Instrument" styleId="pt5Instrument" value='<bean:write name="newenquiryForm" property="pt5Instrument" />' />
								</td>
							</tr>
						</table>
						
						<table width="100%">
							<tr>
								<th class="label-text" style="width:20%; color:#990000;">Delivery Term <img src="html/images/bullets.gif" width="7" height="7" align="absmiddle"> </th>
								<td class="formContentview-rating-b0x" style="width:10%; float:left;">
									<bean:write name="newenquiryForm" property="deliveryTerm" />
								</td>
								<th class="label-text" style="width:30%; color:#990000;">Delivery <img src="html/images/bullets.gif" width="7" height="7" align="absmiddle"> </th>
								<td class="formContentview-rating-b0x" style="width:10%; float:left;">
									<bean:write name="newenquiryForm" property="deliveryInFull" />
									<html:hidden property="deliveryInFull" styleId="deliveryInFull"/>
								</td>
								<th class="label-text" style="width:20%; color:#990000;">Customer Requested Delivery <img src="html/images/bullets.gif" width="7" height="7" align="absmiddle"> </th>
								<td class="formContentview-rating-b0x" style="width:10%; float:left;">
									<bean:write name="newenquiryForm" property="custRequestedDeliveryTime" /> Weeks
								</td>
							</tr>
							<tr>
								<th class="label-text" style="width:20%; color:#990000;">Warranty from the date of dispatch </th>
								<td class="formContentview-rating-b0x" style="width:10%; float:left;">
									<bean:write name="newenquiryForm" property="warrantyDispatchDate" /> Months
								</td>
								<th class="label-text" style="width:30%; color:#990000;">Warranty from the date of comissioning </th>
								<td class="formContentview-rating-b0x" style="width:10%; float:left;">
									<bean:write name="newenquiryForm" property="warrantyCommissioningDate" /> Months
								</td>
								<th class="label-text" style="width:20%; color:#990000;">Order Completion Lead Time <img src="html/images/bullets.gif" width="7" height="7" align="absmiddle"> </th>
								<td class="formContentview-rating-b0x" style="width:10%; float:left;">
									<bean:define name="newenquiryForm" property="tcOrderCompleteLeadTimeList" id="tcOrderCompleteLeadTimeList" type="java.util.Collection" />
									<html:select style="width:160px;" styleId="orderCompletionLeadTime" property="orderCompletionLeadTime">
										<option value="0"><bean:message key="quotation.option.select" /></option>
										<html:options name="newenquiryForm" collection="tcOrderCompleteLeadTimeList" property="key" labelProperty="value" />
									</html:select> Weeks
								</td>
							</tr>
							<tr>
								<th class="label-text" style="width:20%; color:#990000;">GST <img src="html/images/bullets.gif" width="7" height="7" align="absmiddle"> </th>
								<td class="formContentview-rating-b0x" style="width:10%; float:left;">
									<bean:write name="newenquiryForm" property="gstValue" />
									<html:hidden property="gstValue" styleId="gstValue"/>
								</td>
								<th class="label-text" style="width:30%; color:#990000;">Packaging <img src="html/images/bullets.gif" width="7" height="7" align="absmiddle"> </th>
								<td class="formContentview-rating-b0x" style="width:10%; float:left;">
									<bean:write name="newenquiryForm" property="packaging" />
								</td>
								<th class="label-text" style="width:20%; color:#990000;"> Offer Validity </th>
								<td class="formContentview-rating-b0x" style="width:10%; float:left;">
									<bean:write name="newenquiryForm" property="offerValidity" /> Days
								</td>
							</tr>
							<tr>
								<th class="label-text" style="width:20%; color:#990000; padding:0px !important;"> Price Validity from PO date </th>
								<td class="formContentview-rating-b0x" style="width:10%; float:left;">
									<bean:write name="newenquiryForm" property="priceValidity" /> Months
								</td>
								<th class="label-text" style="width:30%; color:#990000; padding:0px !important;">Liquidated Damages due to delayed deliveries</th>
								<td class="formContentview-rating-b0x" style="width:40%; float:left;" colspan="3">
									<bean:write name="newenquiryForm" property="liquidatedDamagesDueToDeliveryDelay" /></td>
							</tr>
						</table> <br>
						<table width="100%">
							<tr>
								<th class="label-text" style="width: 25% !important; color:#990000;">Supervision for Erection and Comissioning</th>
								<td class="formContentview-rating-b0x" style="width: 25% !important;">
									<bean:write name="newenquiryForm" property="supervisionDetails" />
								</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							</tr>
						</table> 
						<logic:equal name="newenquiryForm" property="supervisionDetails" value="Yes">
						<table width="50%">
							<tr>
								<th class="label-text" style="width: 25% !important;">No. of Man Days</th>
								<td class="formContentview-rating-b0x" style="width: 75% !important;">
									<bean:write name="newenquiryForm" property="superviseNoOfManDays" />
								</td>
							</tr>
							<tr>
								<th class="label-text" colspan="2">&nbsp;</th>
							</tr>
							<tr>
								<th class="label-text"
									style="width: 100% !important; white-space: nowrap;"
									colspan="2">Note: To & Fro travelling charges, Fooding,
									Lodging & Local conveyance at site are at customer's account.</th>
							</tr>
						</table>
						</logic:equal> <br>
												
					</td>
				</tr>
			</table>
			
<!-- Commercial Terms and Conditions : Section : End -->