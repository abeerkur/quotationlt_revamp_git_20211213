<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants" %>
<%@ page import="in.com.rbc.quotation.common.utility.PropertyUtility" %>
<%@ page import="in.com.rbc.quotation.common.utility.CommonUtility" %>
<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<% String hideAddOn = ""; %>

<html:form action="createEnquiry.do" method="POST">
<html:hidden property="invoke" value="viewOfferData"/>
<html:hidden property="createdBySSO" /><!--SSO Id  -->
<html:hidden property="createdBy" /><!-- SSO Name -->

<logic:equal name="enquiryForm" property="enquiryOperationType" value="engineer">
<input type="hidden" name="pagechecking" id="pagechecking" value="de" />

</logic:equal>
<logic:equal name="enquiryForm" property="enquiryOperationType" value="manager">
<input type="hidden" name="pagechecking" id="pagechecking" value="dm" />

</logic:equal>

<html:hidden property="operationType"  styleId="operationType" value="DM" />

<html:hidden property="enquiryOperationType"/>
<html:hidden property="dispatch"/>
<html:hidden property="currentRating"/>
<html:hidden property="enquiryId" />
<html:hidden property="ratingId" />
<html:hidden property="enquiryNumber" />
<html:hidden property="nextRatingId"/>
<html:hidden property="updateCreateBy" />
<html:hidden property="isDataValidated"/>
<html:hidden property="isValidated"/>
<html:hidden property="ratingsValidationMessage"/>
<html:hidden property="assignToName" />


<input type="hidden" name='<%= org.apache.struts.taglib.html.Constants.TOKEN_KEY %>'
         value='<%= session.getAttribute(org.apache.struts.action.Action.TRANSACTION_TOKEN_KEY) %>' />
<head>
  
<script>

 window.onload = function(){  
	addonstyle();
}
</script>
<script type="text/javascript">
var globalratesList=new Array;
</script>
<style>
.lablebox-td{
width:33% !important;
    text-align: left;
    font-weight: normal;
    font-size: 10px;
    font-family: Verdana, Arial, Helvetica, sans-serif;
    padding: 2px;
    background-color: #F2F2FF;
}
/* my general stylizing for inputs, mostly comes from Twitter's
bootstrap.min.css which is distributed with Zend Framework 2*/
/*input
{
  background-color: #fff;
  border: 1px solid #ccc;
  display: inline-block;
  height: 20px;
  padding: 4px 6px;
  margin-bottom: 10px;
  font-size: 11px;
  line-height: 20px;
  color: #555;
  vertical-align: middle;
  -webkit-border-radius: 4px;
  -moz-border-radius: 4px;
  border-radius: 4px;
}*/
select
{
 
  border: 1px solid #ccc;
  color: #555;
 
}
.background-bg-box{
background: #e0e0e0;
}
/* specific customizations for the data-list */
div.data-list-input
{
	position: relative;
	height: 20px;	
	display: inline-flex;
}
select.data-list-input
{
	position: absolute;	
	height: 20px;
	margin-bottom: 0;
}
input.data-list-input
{
	position: absolute;
	top: 0px;
	left: 0px;
	height: 19px;
}
.width-select{
width:100% !important;;
}
.input-class{
width:87% !important;
padding:4px 6px;
border-width:1px;
margin:0;
}
</style>
</head>
<!-------------------------- main table start  --------------------------------->


  <%!String rowIndex="";%>
 
 <% int countval = 0; %>
 <%!String str="";%>
 <div id="estimate">
<table width="100%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
  <tr>
    <td height="100%" valign="top">     
	
	<!-------------------------- Top Title and Status block Start  --------------------------------->
	<br>
        <div class="sectiontitle">
        	<bean:message key="quotation.offerdata.label.heading"/>
	  </div><br>
        <table width="100%" border="0" cellpadding="0" cellspacing="3">
          <tr>
            <td rowspan="2" style="width:40% "><table width="98%"  border="0" cellspacing="2" cellpadding="0" class="DotsTable">
              <tr>
                <td rowspan="2" style="width:70px "><img src="html/images/icon_quote.gif" width="52" height="50"></td>
              <td class="other">
              	<bean:message key="quotation.viewenquiry.label.subheading1"/> <bean:write name="enquiryForm" property="enquiryNumber"/><br>
                <bean:message key="quotation.viewenquiry.label.subheading2"/> </td>
            </tr>
              <tr>
                <td class="other"><table width="100%">
                  <tr>
                    <td height="16" class="section-head" align="center">
                    	<bean:message key="quotation.viewenquiry.label.status"/>  
                    	<bean:write name="enquiryForm" property="statusName"/> </td>
                  </tr>
                  <logic:equal name="enquiryForm" property="enquiryOperationType" value="engineer">
                  <tr>
                    <td height="16" class="section-head" align="center">
                    	<bean:message key="quotation.home.colheading.assignto"/>  
                    	:&nbsp;
                    	<bean:write name="enquiryForm" property="designEngineerName"/> 
                    </td>
                  </tr>
                  <tr>
                    <td height="16" class="section-head" align="center">
                    	<bean:message key="quotation.create.label.salesrepresentative"/> 
                    	:&nbsp; 
                    	<bean:write name="enquiryForm" property="sm_name"/> 
                    </td>
                  </tr>
                  </logic:equal>
                   <logic:equal name="enquiryForm" property="enquiryOperationType" value="manager">
                  <tr>
                    <td height="16" class="section-head" align="center">
                    	<bean:message key="quotation.home.colheading.assignto"/>  
                    	:&nbsp;
                    	<bean:write name="enquiryForm" property="createdBy"/> 
                    </td>
                  </tr>
                  <tr>
                    <td height="16" class="section-head" align="center">
                    	<bean:message key="quotation.create.label.salesrepresentative"/> 
                    	:&nbsp; 
                    	<bean:write name="enquiryForm" property="sm_name"/> 
                    </td>
                  </tr>
                  </logic:equal>
                  
                  </table>
                  <br /></td>
            </tr>
            </table></td>
        <logic:lessThan name="enquiryForm" property="statusId" value="6"> 
	        <td class="other" align="right" colspan="2">
        	<bean:message key="quotation.attachments.message"/>
    	    <a href="javascript:manageAttachments('<%= CommonUtility.getIntValue(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_ATTACHMENT_APPLICATIONID)) %>','<bean:write name="enquiryForm" property="enquiryId"/>', '<bean:write name="enquiryForm" property="createdBySSO"/>');"><img src="html/images/ico_attach.gif" alt="Manage Attachments" width="23" height="25" border="0" align="right"></a></td>
        </logic:lessThan>
      </tr>
          <tr>
            <td colspan="2" style="width:45% ">
            <logic:present name="attachmentList" scope="request">
            <logic:notEmpty name="attachmentList">
            <fieldset>
              <table width="100%"  cellpadding="0" cellspacing="0" border="0" class="other">
              <logic:iterate name="attachmentList" id="attachmentData" indexId="idx"
								               type="in.com.rbc.quotation.common.vo.KeyValueVo">	
				<tr><td>
					<a href="javascript:download('<bean:write name="attachmentData" property="key"/>', '<bean:write name="attachmentData" property="value"/>', '<bean:write name="attachmentData" property="value2"/>');">
						<bean:write name="attachmentData" property="value"/>.<bean:write name="attachmentData" property="value2"/>
					</a>
				</td></tr>					
				</logic:iterate>
              </table>
        </fieldset>
        </logic:notEmpty>
        </logic:present>
        </td>
        
      </tr>
     <tr><td colspan="2" align="right" > 
        <!-- DM Uploading Files  -->
            <logic:lessThan name="enquiryForm" property="statusId" value="6"> 
            <table>
            <tr>
	        <td class="other" align="right">
        	<bean:message key="quotation.attachments.dmuploadedmessage"/></td>
    	    <td class="other" align="left"><a href="javascript:manageAttachments('<%= CommonUtility.getIntValue(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_DMATTACHMENT_APPLICATIONID)) %>','<bean:write name="enquiryForm" property="enquiryId"/>', '<bean:write name="enquiryForm" property="createdBySSO"/>');"><img src="html/images/ico_attach.gif" alt="Manage Attachments" width="23" height="25" border="0" align="right"></a></td>
            </tr>
            </table>
        </logic:lessThan>
      
      </td>
      </tr> 
           <tr>
           <td>
           &nbsp;
           </td>
           <td colspan="2" align="right" style="width:45% "> 
                       <logic:present name="dmattachmentList" scope="request">
            <logic:notEmpty name="dmattachmentList">
            <fieldset>
              <table width="100%" class="other">
              <logic:iterate name="dmattachmentList" id="attachmentData" indexId="idx"
								               type="in.com.rbc.quotation.common.vo.KeyValueVo">	
				<tr><td>
					<a href="javascript:download('<bean:write name="attachmentData" property="key"/>', '<bean:write name="attachmentData" property="value"/>', '<bean:write name="attachmentData" property="value2"/>');">
						<bean:write name="attachmentData" property="value"/>.<bean:write name="attachmentData" property="value2"/>
					</a>
				</td></tr>					
				</logic:iterate>
              </table>
        </fieldset>
        </logic:notEmpty>
        </logic:present>
           
           </td>
           </tr> 
        </table>
    <br>
    

<div style="clear:both; margin-bottom:30px; float:left; width:100%;">
<div style="width:90%; float:left;">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
<tr>
<td><fieldset>
<legend class="blueheader"><bean:message key="quotation.viewenquiry.label.request.heading"/></legend>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="3">
<tr>
<td class="background-bg-box"><bean:message key="quotation.create.label.customer"/></td>
<td class="background-bg-box">
<input id="customerName" name="customerName" type="text" size="50" class="normal" autocomplete="off" value="<bean:write name="enquiryForm" property="customerName"/>"  />
<div class="note hidetable" id="invalidcustomer" style="text-align:left;padding-left:20px">Invalid Customer Name</div>
<html:hidden property="customerId" styleId="customerId"/>
</td>
<td class="dividingdots">&nbsp;</td>
<td>&nbsp;</td>
<td class=""><bean:message key="quotation.create.label.deptauth"/></td>
<td class=""><bean:write name="enquiryForm" property="deptAndAuth"/></td>
</tr>
<tr>
<td class=""><bean:message key="quotation.create.label.enduser"/></td>

<td> <html:text property="endUser" styleClass="normal" maxlength="20"/> 
					</td>
<td class="dividingdots">&nbsp;</td>
<td>&nbsp;</td>
<td class=""><bean:message key="quotation.create.label.projectname"/></td>
<td ><html:text property="projectName" styleClass="normal" maxlength="30"/> </td>
</tr>
<tr>
<td class=""><bean:message key="quotation.create.label.itemname"/></td>
<td class=""><bean:write name="enquiryForm" property="scsrName"/></td>
<td class="dividingdots">&nbsp;</td>
<td>&nbsp;</td>
<td class=""><bean:message key="quotation.create.label.enquiryreferencenumber"/></td>
<td ><html:text property="enquiryReferenceNumber" styleClass="normal" maxlength="30"/> </td>
</tr>
<tr>
<td class=""><bean:message key="quotation.create.label.revision"/></td>

<td><html:text property="revision" styleClass="normal" maxlength="5"/> </td>
<td class="dividingdots">&nbsp;</td>
<td>&nbsp;</td>
<td class=""><bean:message key="quotation.create.label.issuedate"/></td>
<td><html:text property="lastModifiedDate" styleId="lastModifiedDate" styleClass="readonlymini"  readonly="true" maxlength="30"/><html:img src="html/images/popupCalen.gif" styleClass="hand"   width="19" height="22" border="0" align="absmiddle" onclick="return showCalendar('lastModifiedDate', '%m/%d/%Y', '24', true);" /> </td>
</tr>
<tr>
<td class=""><bean:message key="quotation.create.label.savingindent"/></td>
<td><html:text property="savingIndent" styleClass="normal" maxlength="30"/> </td>
</tr>
</table>
</fieldset></td>
<td valign="top">
<fieldset>
 <legend class="blueheader"><bean:message key="quotation.offer.label.reassign"/></legend>
       <table style="background-color:#FFFFFF ">
         <tr>
           <td><bean:message key="quotation.offer.label.reassignto"/></td>
           <td>
           	<bean:define name="enquiryForm" property="directManagersList"
          	             id="directManagersList" type="java.util.Collection" /> 
           	<html:select property="assignToId" style="width:370px;">
             	<option value="0"><bean:message key="quotation.option.select"/></option>
                 <html:options name="enquiryForm" collection="directManagersList"
	              property="key" labelProperty="value" />
             </html:select>
         </tr>
         <tr>
           <td><bean:message key="quotation.offer.label.comments"/></td>
           <td>
           <textarea style="width:370px; height:55px " maxlength="250" name="assignRemarks" id="assignRemarks"></textarea>
           


           </td>
         </tr>
       </table>
       <div class="blue-light" align="right">                
         <INPUT class=BUTTON onclick="javascript:reassign();" type=button value=Re-assign name=assign>
         </div>
</fieldset></td>   

</tr>
</table>
</div>
</div>
<div  style="clear:both;">
<div style="clear:both; float:left; width:90%; height:32px; margin-bottom:30px;">
<bean:define id="latestId"  name="enquiryForm" property="currentRating"/>
<div id="viewtopnav">
<ul>
<logic:notEmpty name="enquiryForm" property="ratingList">
<%!int count=0; %>

 <logic:iterate name="enquiryForm" property="ratingList"
               id="ratingListData" indexId="idx"
               type="in.com.rbc.quotation.common.vo.KeyValueVo">
              <%  countval=countval+1;

               %>
</logic:iterate>
         <logic:iterate name="enquiryForm" property="ratingList"
               id="ratingListData" indexId="idx"
               type="in.com.rbc.quotation.common.vo.KeyValueVo">
               <logic:equal name="ratingListData" property="value2" value="Y">

<li>
<%if(countval>1){ %>
 <a href="javascript:getNextRating('<bean:write name="ratingListData" property="key" />')"  class="valid"> 
<%} %>

<b>

	<logic:equal name="ratingListData" property="key" value="<%= latestId.toString() %>">
	<bean:write name="ratingListData" property="value"/>
	</logic:equal>
	<logic:notEqual name="ratingListData" property="key" value="<%= latestId.toString() %>">
	<bean:write name="ratingListData" property="value"/>
	</logic:notEqual>
</b>

<%if(countval>1){ %>
</a>
<%} %>
</li>
</logic:equal>
 <logic:equal name="ratingListData" property="value2" value="N">

 	<li>
 	<%if(countval>1){ %>
 		<a href="javascript:getNextRating('<bean:write name="ratingListData" property="key" />')" class="notvalid" >
<%} %>
 		
 			<logic:equal name="ratingListData" property="key" value="<%= latestId.toString() %>">
 				<b><bean:write name="ratingListData" property="value"/></b>
 			</logic:equal>
 			<logic:notEqual name="ratingListData" property="key" value="<%= latestId.toString() %>">
 				<bean:write name="ratingListData" property="value"/>
 			</logic:notEqual>
 			<%if(countval>1){ %>
 		</a>
 		<%} %>
 	</li>	
 </logic:equal>	
 
 </logic:iterate>

</logic:notEmpty>
</ul>
</div>
<%


%>
</div>
<div style="clear:both;">
<div style="width:90%; class="note" align="right" style="float:right; line-height:16px;"><font  color="red"><bean:message key="quotation.label.manadatory"/></font></div>
<div style="width:90%; float:left; margin-bottom:30px; float:left;">

<fieldset>
<legend class="blueheader"><bean:message key="quotation.offerdata.technical.label.heading"/></legend>


<table width="100%" border="0" align="center" cellpadding="0" cellspacing="3">
<tr>
<td width="20%" class="label-text background-bg-box"><bean:message key="quotation.offerdata.addon.label.quantity"/></td>
<td width="20%" class="background-bg-box"><html:text property="quantity" styleClass="normal" maxlength="2"/></td>
<td width="10%">&nbsp;</td>
<td width="20%" class="label-text background-bg-box"><bean:message key="quotation.create.label.volts"/>
<span class="mandatory">&nbsp;</span>
</td>
<td  class="background-bg-box" width="20%">
<bean:define name="enquiryForm" property="alVoltList" id="alVoltList1" type="java.util.Collection" /> 
	<html:select property="volts" styleClass="normal"  onchange="javascript:calCurrent()" >
	<option value=""><bean:message key="quotation.option.select"/></option>
	  <html:options name="enquiryForm" collection="alVoltList1"
	property="key" labelProperty="value" />
	</html:select>
	<bean:message key="quotation.units.volts"/>
	+/-
	<html:text tabindex="23" property="voltsVar" value ="10" readonly="true" style="width:28px;" styleClass="short" maxlength="10" />
	%</td>
</tr>
<tr>
<td class="label-text  background-bg-box"><bean:message key="quotation.create.label.application"/></td>
<td  class="background-bg-box">
<bean:define name="enquiryForm" property="applicationList" id="applicationList" type="java.util.Collection" /> 
<html:select property="applicationId" styleClass="normal" >
	<option value=""><bean:message key="quotation.option.select"/></option>
	  <html:options name="enquiryForm" collection="applicationList"	property="key" labelProperty="value" />
	</html:select></td>
<td>&nbsp;</td>
<td class="label-text  background-bg-box"><bean:message key="quotation.create.label.frequency"/>
<span class="mandatory">&nbsp;</span>
</td>
<td class="background-bg-box" ><bean:define name="enquiryForm" property="alFrequencyList" id="alFrequencyList" type="java.util.Collection" /> 
<html:select property="frequency" styleClass="normal" >
	<option value=""><bean:message key="quotation.option.select"/></option>
	  <html:options name="enquiryForm" collection="alFrequencyList"	property="key" labelProperty="value" />
	</html:select>
	&nbsp;<bean:message key="quotation.units.hz"/>
	</td>
</tr>
<tr>
<td class="label-text background-bg-box"><bean:message key="quotation.create.label.scsr"/></td>
<td  class="background-bg-box"><bean:define name="enquiryForm" property="scsrList" id="scsrList" type="java.util.Collection" /> 
<html:select property="scsrId" styleClass="normal" >
	<option value=" "><bean:message key="quotation.option.select"/></option>
	  <html:options name="enquiryForm" collection="scsrList"	property="key" labelProperty="value" />
	</html:select></td>
<td>&nbsp;</td>
<td class="label-text background-bg-box"><bean:message key="quotation.create.label.pole"/>
<span class="mandatory">&nbsp;</span></td>
<td  class="background-bg-box"><bean:define name="enquiryForm" property="poleList" id="poleList" type="java.util.Collection" /> 
<html:select property="poleId" styleClass="normal" >
	<option value=" "><bean:message key="quotation.option.select"/></option>
	  <html:options name="enquiryForm" collection="poleList"	property="key" labelProperty="value" />
	</html:select></td>
</tr>
<tr>
<td class="label-text"><bean:message key="quotation.offer.label.areaclassification"/>
<span class="mandatory">&nbsp;</span>
</td>
<td><bean:define name="enquiryForm" property="alAreaClassificationList" id="alAreaClassificationList" type="java.util.Collection" /> 
<html:select property="areaClassification" styleClass="normal" >
	<option value=" "><bean:message key="quotation.option.select"/></option>
	  <html:options name="enquiryForm" collection="alAreaClassificationList"	property="key" labelProperty="value" />
	</html:select></td>
<td>&nbsp;</td>
<td class="label-text background-bg-box"><bean:message key="quotation.offerdata.technical.label.rpm"/>
</td>
<td  class="background-bg-box"><html:text property="rpm" styleClass="normal" onchange="javascript:calculateNTorque();" maxlength="30"/>
&nbsp;<bean:message key="quotation.units.rmin"/>
</td>
</tr>


<tr>
<td class="label-text"><bean:message key="quotation.create.label.zone"/>

</td>
<td><bean:define name="enquiryForm" property="alZoneList" id="alZoneList" type="java.util.Collection" /> 
<html:select property="zone" styleClass="normal" >
	<option value=" "><bean:message key="quotation.option.select"/></option>
	  <html:options name="enquiryForm" collection="alZoneList"	property="key" labelProperty="value" />
	</html:select></td>
<td>&nbsp;</td>
<td class="label-text background-bg-box"><bean:message key="quotation.offerdata.technical.label.flcamps"/>

</td>
<td  class="background-bg-box"><html:text property="flcAmps" styleClass="normal"  onchange="javascript:updateCurrent(this.value)" maxlength="30"/>&nbsp;<bean:message key="quotation.units.a"/></td>

</tr>

<tr>
<td class="label-text"><bean:message key="quotation.create.label.gasgroup"/>

</td>
<td><bean:define name="enquiryForm" property="alGasGroupList" id="alGasGroupList" type="java.util.Collection" /> 
<html:select property="gasGroup" styleClass="normal" >
	<option value=" "><bean:message key="quotation.option.select"/></option>
	  <html:options name="enquiryForm" collection="alGasGroupList"	property="key" labelProperty="value" />
	</html:select></td>
<td>&nbsp;</td>
<td class="label-text"><bean:message key="quotation.offer.label.noloadcurrent"/></td>
<td><html:text property="nlCurrent" styleClass="normal"  maxlength="10"/>&nbsp;<bean:message key="quotation.units.a"/></td>
</tr>



<tr>
<td class="label-text  background-bg-box"><bean:message key="quotation.create.label.enclosure"/></td>
<td class="background-bg-box"><bean:define name="enquiryForm" property="enclosureList" id="enclosureList" type="java.util.Collection" /> 
<html:select property="enclosureId" styleClass="normal" >
	<option value=" "><bean:message key="quotation.option.select"/></option>
	  <html:options name="enquiryForm" collection="enclosureList"	property="key" labelProperty="value" />
	</html:select></td>
<td>&nbsp;</td>
<td class="label-text"><bean:message key="quotation.offer.label.startingcurrent"/>

</td>
<td><bean:define name="enquiryForm" property="alStrtgCurrentList" id="alStrtgCurrentList" type="java.util.Collection" /> 

<div class="data-list-input" style="width:151px;">

<html:select property="strtgCurrent" styleId = "strtgCurrentSelect" styleClass="normal data-list-input width-select" >
	<option value=" "><bean:message key="quotation.option.select"/></option>
	<html:options name="enquiryForm" collection="alStrtgCurrentList"	property="key" labelProperty="value" />
</html:select>
<html:text property="strtgCurrent" styleId = "strtgCurrentText" styleClass="normal data-list-input input-class"  maxlength="10"/>
</div>




</td>

</tr>
<tr>
<td class="label-text background-bg-box"><bean:message key="quotation.offerdata.technical.label.framesize"/>
<span class="mandatory">&nbsp;</span>
</td>
<td  class="background-bg-box"><html:text property="frameSize" styleClass="normal"  maxlength="20"/></td>
<td>&nbsp;</td>
<td class="label-text background-bg-box"><bean:message key="quotation.offerdata.technical.label.flt"/></td>
<td  class="background-bg-box"><html:text property="fltSQCAGE" styleClass="normal"  maxlength="10"/>&nbsp;<bean:message key="quotation.units.nm"/></td>
</tr>
<tr>
<td class="label-text  background-bg-box"><bean:message key="quotation.create.label.mounting"/>
<span class="mandatory">&nbsp;</span>
</td>
<td  class="background-bg-box"><bean:define name="enquiryForm" property="mountingList" id="mountingList" type="java.util.Collection" /> 
<html:select property="mountingId" styleClass="normal" >
	<option value=" "><bean:message key="quotation.option.select"/></option>
	<html:options name="enquiryForm" collection="mountingList"	property="key" labelProperty="value" />
</html:select></td>
<td>&nbsp;</td>
<td class="label-text"><bean:message key="quotation.offer.label.lockedrotortorque"/>

</td>
<td><html:text property="lrTorque" styleClass="normal"  maxlength="10"/></td>
</tr>
<tr>
<td class="label-text background-bg-box"><bean:message key="quotation.create.label.kw"/>
<span class="mandatory">&nbsp;</span>
</td>
<td  class="background-bg-box" ><html:text property="kw" styleClass="normal" onchange="javascript:calCurrent();javascript:calculateNTorque();" maxlength="25"/>


<html:select property="ratedOutputUnit" styleClass="short"  onchange="javascript:calCurrent();javascript:calculateNTorque();" style="width:44px;" >
<logic:equal  name="enquiryForm" property="ratedOutputUnit" value="kW">
<option value='kW'><bean:message key="quotation.option.kw"/></option>
<option value='HP'><bean:message key="quotation.option.hp"/></option>
</logic:equal>
<logic:equal  name="enquiryForm" property="ratedOutputUnit" value="HP">
<option value='HP'><bean:message key="quotation.option.hp"/></option>
<option value='kW'><bean:message key="quotation.option.kw"/></option>
</logic:equal>
<logic:equal  name="enquiryForm" property="ratedOutputUnit" value="">

<option value='kW'><bean:message key="quotation.option.kw"/></option>
<option value='HP'><bean:message key="quotation.option.hp"/></option>

</logic:equal>


</html:select>
</td>
<td>&nbsp;</td>
<td class="label-text background-bg-box"><bean:message key="quotation.offerdata.technical.label.potflt"/>
<!-- TODO <span class="mandatory">&nbsp;</span> -->
</td>
<td  class="background-bg-box"><html:text property="potFLT" styleClass="normal"  maxlength="10"/></td></tr>
<tr>
<td class="label-text"><bean:message key="quotation.offer.label.servicefactor"/>
<span class="mandatory">&nbsp;</span>
</td>
<td><bean:define name="enquiryForm" property="alServiceFactorList" id="alServiceFactorList" type="java.util.Collection" /> 
<html:select property="serviceFactor" styleClass="normal" >
	<option value=" "><bean:message key="quotation.option.select"/></option>
	<html:options name="enquiryForm" collection="alServiceFactorList"	property="key" labelProperty="value" />
</html:select></td>
<td>&nbsp;</td>
 <td class="label-text"><bean:message key="quotation.offerdata.technical.label.rvslip"/></td>
<td><html:text property="rv" styleClass="normal"  maxlength="15"/></td>
 
 
 </tr>
<tr>
<td class="label-text"><bean:message key="quotation.offer.label.bkw"/></td>
<td><html:text property="bkw" styleClass="normal"  maxlength="6"/> &nbsp;<bean:message key="quotation.option.kw"/>  </td>
<td>&nbsp;</td>
<td class="label-text"><bean:message key="quotation.offerdata.technical.label.raslip"/></td>
<td><html:text property="ra" styleClass="normal"  maxlength="15"/></td>
 
 </tr>
<tr>
<td class="label-text background-bg-box"><bean:message key="quotation.create.label.duty"/>
<span class="mandatory">&nbsp;</span>
</td>
<td  class="background-bg-box"><bean:define name="enquiryForm" property="dutyList" id="dutyList" type="java.util.Collection" /> 
<html:select property="dutyId" styleClass="normal" >
	<option value=" "><bean:message key="quotation.option.select"/></option>
	<html:options name="enquiryForm" collection="dutyList"	property="key" labelProperty="value" />
</html:select></td>
<td>&nbsp;</td>
</tr>
<tr>
<td class="label-text"></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
</table>
<hr />
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="3">
<tr>
<td width="25%" class="label-text"><bean:message key="quotation.offer.label.loadcharacteristics"/></td>
<td width="2%"></td>
<td width="13%" class="label-text"><bean:message key="quotation.offer.label.load"/></td>
<td width="2%"></td>
<td width="18%" class="label-text"><bean:message key="quotation.offer.label.currenta"/></td>
<td width="2%"></td>
<td width="18%" class="label-text"><bean:message key="quotation.offer.label.efficiency"/></td>
<td width="2%"></td>
<td width="18%" class="label-text"><bean:message key="quotation.offer.label.powerfactor"/></td>
</tr>
<tr>
<td class="label-text"><bean:message key="quotation.offer.label.plldesc"/></td>
<td></td>
<td class="label-text"><bean:message key="quotation.offer.label.load100"/></td>
<td></td>
<bean:define name="enquiryForm" property="flcAmps" id="flcAmpsValue" type="java.lang.String"/>

<td><html:text property="pllCurr1" styleClass="normal"  value='<%=flcAmpsValue %>' maxlength="6" onchange="javascript:updateCurrent(this.value)" /></td>
<td></td>
<td><html:text property="pllEff1" styleClass="normal"  onchange="javascript:calCurrent()" maxlength="6"/><!--TODO <span class="mandatory">&nbsp;</span> --></td>
<td></td>
<td><html:text property="pllPf1" styleClass="normal" onchange="javascript:calCurrent()"  maxlength="6"/><!--TODO <span class="mandatory">&nbsp;</span> --></td>
</tr>
<tr>
<td class="label-text"></td>
<td></td>
<td class="label-text"><bean:message key="quotation.offer.label.load75"/></td>
<td></td>
<td><html:text property="pllCurr2" styleClass="normal"  maxlength="6"/></td>
<td></td>
<td><html:text property="pllEff2" styleClass="normal"  onchange="javascript:calCurrent()" maxlength="6"/><!--TODO <span class="mandatory">&nbsp;</span> --></td>
<td></td>
<td><html:text property="pllPf2" styleClass="normal"  onchange="javascript:calCurrent()" maxlength="6"/><!--TODO <span class="mandatory">&nbsp;</span> --></td>
</tr>
<tr>
<td class="label-text"></td>
<td></td>
<td class="label-text"><bean:message key="quotation.offer.label.load50"/></td>
<td></td>
<td><html:text property="pllCurr3" styleClass="normal"  maxlength="6"/></td>
<td></td>
<td><html:text property="pllEff3" styleClass="normal" onchange="javascript:calCurrent()" maxlength="6"/><!--TODO <span class="mandatory">&nbsp;</span> --></td>
<td></td>
<td><html:text property="pllpf3" styleClass="normal" onchange="javascript:calCurrent()" maxlength="6"/><!--TODO <span class="mandatory">&nbsp;</span> --></td>
</tr>
<tr>
<td class="label-text"></td>
<td></td>
<td class="label-text"><bean:message key="quotation.offer.label.loadstart"/></td>
<td></td>

<td><html:text property="pllCurr4" styleClass="normal"  maxlength="6"/></td>
<td></td>
<td></td>
<td></td>
<td><html:text property="pllpf4" styleClass="normal"  maxlength="6"/></td>
</tr>
<tr>
<td class="label-text"></td>
<td></td>
<td class="label-text"><bean:message key="quotation.offer.label.loaddutypoint"/></td>
<td></td>
<td><html:text property="pllCurr5" styleClass="normal"  maxlength="6"/></td>
<td></td>
<td><html:text property="pllEff4" styleClass="normal"  maxlength="6"/></td>
<td></td>
<td><html:text property="pllPf5" styleClass="normal"  maxlength="6"/></td>
</tr>
</table>
<hr />
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="3">
<tr>
<td width="20%" class="label-text"><bean:message key="quotation.offer.label.safestalltimeh"/><!--TODO <span class="mandatory">&nbsp;</span> --></td>
<td width="20%"><html:text property="sstHot" styleClass="normal"  maxlength="6"/>&nbsp;<bean:message key="quotation.units.s"/></td>
<td width="10%">&nbsp;</td>
<td width="20%" class="label-text"><bean:message key="quotation.offerdata.technical.label.vibrationlevel"/><!-- TODO <span class="mandatory">&nbsp;</span> --></td>
<td width="20%"><bean:define name="enquiryForm" property="alVibrationList" id="alVibrationList" type="java.util.Collection" /> 

<div class="data-list-input" style="width:151px; float:left;">

<html:select property="vibrationLevel" styleId="vibrationLevelSel" styleClass="normal data-list-input width-select" >
	<option value=" "><bean:message key="quotation.option.select"/></option>
	<html:options name="enquiryForm" collection="alVibrationList" property="key" labelProperty="value" />
</html:select>
<html:text property="vibrationLevel" styleId="vibrationLevelText" styleClass="normal data-list-input input-class"  maxlength="10"/>
</div>



</td>
</tr>
<tr>
<td class="label-text"><bean:message key="quotation.offer.label.safestalltimec"/><!--TODO <span class="mandatory">&nbsp;</span> --></td>
<td><html:text property="sstCold" styleClass="normal"  maxlength="6"/>&nbsp;<bean:message key="quotation.units.s"/></td>
<td>&nbsp;</td>
<td class="label-text"><bean:message key="quotation.offer.label.maintbps"/></td>
<td><bean:define name="enquiryForm" property="alMainTermBoxPSList" id="alMainTermBoxPSList" type="java.util.Collection" /> 
	<html:select property="mainTermBoxPS" styleClass="normal" >
	<option value=" "><bean:message key="quotation.option.select"/></option>
	<html:options name="enquiryForm" collection="alMainTermBoxPSList"	property="key" labelProperty="value" />
</html:select></td>
</tr>
<tr>
<td class="label-text"><bean:message key="quotation.offer.label.startingtimerv"/></td>
<td><html:text property="startTimeRV" styleClass="normal"  maxlength="6"/>&nbsp;<bean:message key="quotation.units.s"/></td>
<td>&nbsp;</td>
<td class="label-text"><bean:message key="quotation.offer.label.neutraltb"/></td>
<td><bean:define name="enquiryForm" property="alNeutralTBList" id="alNeutralTBList" type="java.util.Collection" /> 
	<html:select property="neutralTermBox" styleClass="normal" >
	<option value=" "><bean:message key="quotation.option.select"/></option>
	<html:options name="enquiryForm" collection="alNeutralTBList"	property="key" labelProperty="value" />
</html:select>
</td>
</tr>
<tr>
<td class="label-text"><bean:message key="quotation.offer.label.startingtimerv80"/></td>
<td><html:text property="startTimeRV80" styleClass="normal"  maxlength="6"/>&nbsp;<bean:message key="quotation.units.s"/></td>
<td>&nbsp;</td>
<td class="label-text"><bean:message key="quotation.offer.label.cableentry"/></td>
<td>
<bean:define name="enquiryForm" property="alCableEntryList" id="alCableEntryList" type="java.util.Collection" /> 
	<html:select property="cableEntry" styleClass="normal" >
	<option value=" "><bean:message key="quotation.option.select"/></option>
	<html:options name="enquiryForm" collection="alCableEntryList"	property="key" labelProperty="value" />
</html:select>
</td>
</tr>
<tr>
<td class="label-text background-bg-box"><bean:message key="quotation.create.label.insulationclass"/><span class="mandatory">&nbsp;</span></td>
<td  class="background-bg-box">
<bean:define name="enquiryForm" property="insulationList" id="insulationList" type="java.util.Collection" /> 
	<html:select property="insulationId" styleClass="normal" >
	<option value=" "><bean:message key="quotation.option.select"/></option>
	<html:options name="enquiryForm" collection="insulationList" property="key" labelProperty="value" />
</html:select>
</td>
<td>&nbsp;</td>
<td class="label-text"><bean:message key="quotation.offer.label.auxterminalbox"/></td>
<td><bean:define name="enquiryForm" property="alAuxTermBoxList" id="alAuxTermBoxList" type="java.util.Collection" /> 
	<html:select property="auxTermBox" styleClass="normal" >
	<option value=" "><bean:message key="quotation.option.select"/></option>
	<html:options name="enquiryForm" collection="alAuxTermBoxList" property="key" labelProperty="value" />
	</html:select>
</td>
</tr>
<tr>
<td class="label-text background-bg-box"><bean:message key="quotation.create.label.temprise"/><span class="mandatory">&nbsp;</span></td>
<td  class="background-bg-box">
<bean:define name="enquiryForm" property="alTempRaiseList" id="alTempRaiseList" type="java.util.Collection" /> 
	<html:select property="tempRaise" styleClass="normal" >
	<option value=" "><bean:message key="quotation.option.select"/></option>
	<html:options name="enquiryForm" collection="alTempRaiseList" property="key" labelProperty="value" />
	</html:select>

</td>
<td>&nbsp;</td>
<td class="label-text"><bean:message key="quotation.offer.label.statorconnection"/></td>
<td>
<bean:define name="enquiryForm" property="alStatorConnList" id="alStatorConnList" type="java.util.Collection" /> 
	<html:select property="statorConn" styleClass="normal" >
	<option value=" "><bean:message key="quotation.option.select"/></option>
	<html:options name="enquiryForm" collection="alStatorConnList" property="key" labelProperty="value" />
	</html:select>
</td>
</tr>
<tr>

<td class="label-text background-bg-box"><bean:message key="quotation.create.label.ambience"/><span class="mandatory">&nbsp;</span></td>
<td  class="background-bg-box">
<bean:define name="enquiryForm" property="alAmbienceList" id="alAmbienceList" type="java.util.Collection" /> 
	
<div class="data-list-input" style="width:151px; float:left;">

<html:select property="ambience" styleId="ambienceSel" styleClass="normal data-list-input width-select" >
	<option value=" "><bean:message key="quotation.option.select"/></option>
	<html:options name="enquiryForm" collection="alAmbienceList"	property="key" labelProperty="value" />
</html:select>

 <html:text property="ambience" styleId="ambienceText"  styleClass="normal data-list-input input-class"  maxlength="10"/> 


</div>
<div style="padding-top:5px; line-height:14px; float:left;">&nbsp;<bean:message key="quotation.units.c"/></div>
</td>
<td>&nbsp;</td>
<td class="label-text"><bean:message key="quotation.offer.label.nboterminals"/></td>
<td>
<bean:define name="enquiryForm" property="alBoTerminalsNoList" id="alBoTerminalsNoList" type="java.util.Collection" /> 
<html:select property="noBoTerminals" styleClass="normal" >
	<option value=" "><bean:message key="quotation.option.select"/></option>
	<html:options name="enquiryForm" collection="alBoTerminalsNoList" property="key" labelProperty="value" />
</html:select>

</td>
</tr>
<tr>
<td class="label-text background-bg-box"><bean:message key="quotation.create.label.altitude"/><span class="mandatory">&nbsp;</span></td>
<td  class="background-bg-box"><html:text property="altitude" styleClass="normal"  maxlength="6"/>&nbsp;<bean:message key="quotation.units.masl"/></td>
<td>&nbsp;</td>
<td class="label-text"><bean:message key="quotation.offer.label.rotation"/></td>
<td>
<bean:define name="enquiryForm" property="alRotationList" id="alRotationList" type="java.util.Collection" /> 
<html:select property="rotation" styleClass="normal" >
	<option value=" "><bean:message key="quotation.option.select"/></option>
	<html:options name="enquiryForm" collection="alRotationList" property="key" labelProperty="value" />
</html:select>
</td>
</tr>
<tr>
<td class="label-text background-bg-box"><bean:message key="quotation.create.label.degpro"/><span class="mandatory">&nbsp;</span></td>
<td  class="background-bg-box">
<bean:define name="enquiryForm" property="degproList" id="degproList" type="java.util.Collection" /> 
<html:select property="degreeOfProId" styleClass="normal" >
	<option value=" "><bean:message key="quotation.option.select"/></option>
	<html:options name="enquiryForm" collection="degproList" property="key" labelProperty="value" />
</html:select>
</td>
<td>&nbsp;</td>
<td class="label-text background-bg-box"><bean:message key="quotation.create.label.rotdirection"/><!-- TODO <span class="mandatory">&nbsp;</span> --></td>
<td class="background-bg-box"> 
<bean:define name="enquiryForm" property="rotDirectionList" id="rotDirectionList" type="java.util.Collection" /> 
<html:select property="directionOfRotId" styleClass="normal" >
	<option value=" "><bean:message key="quotation.option.select"/></option>
	<html:options name="enquiryForm" collection="rotDirectionList" property="key" labelProperty="value" />
</html:select>
</td>
</tr>
<tr>
<td class="label-text"><bean:message key="quotation.offer.label.coolingsystem"/><span class="mandatory">&nbsp;</span></td>
<td>
<bean:define name="enquiryForm" property="alClgSystemList" id="alClgSystemList" type="java.util.Collection" /> 
<html:select property="coolingSystem" styleClass="normal" >
	<option value=" "><bean:message key="quotation.option.select"/></option>
	<html:options name="enquiryForm" collection="alClgSystemList" property="key" labelProperty="value" />
</html:select>
</td>
<td>&nbsp;</td>
<td class="label-text"><bean:message key="quotation.offer.label.motortotalwt"/></td>
<td><html:text property="motorTotalWgt" styleClass="normal"  maxlength="6"/>&nbsp;<bean:message key="quotation.units.kg"/></td>
</tr>
<tr>
<td class="label-text"><bean:message key="quotation.offer.label.bearingdende"/><!-- TODO <span class="mandatory">&nbsp;</span> --></td>
<td>
<bean:define name="enquiryForm" property="alBearingDENDEList" id="alBearingDENDEList" type="java.util.Collection" /> 
<html:select property="bearingDENDE" styleClass="normal" >
	<option value=" "><bean:message key="quotation.option.select"/></option>
	<html:options name="enquiryForm" collection="alBearingDENDEList" property="key" labelProperty="value" />
</html:select>

</td>
<td>&nbsp;</td>
<td class="label-text"><bean:message key="quotation.offer.label.spaceheater"/><span class="mandatory">&nbsp;</span></td>
<td>
<bean:define name="enquiryForm" property="alYesNoList" id="alYesNoList" type="java.util.Collection" /> 
<html:select property="spaceHeater" styleClass="normal" >
	<option value=" "><bean:message key="quotation.option.select"/></option>
	<html:options name="enquiryForm" collection="alYesNoList" property="key" labelProperty="value" />
</html:select>
</td>
</tr>
<tr>
<td class="label-text"><bean:message key="quotation.offer.label.lubrication"/></td>
<td>
<bean:define name="enquiryForm" property="alLubricationList" id="alLubricationList" type="java.util.Collection" /> 
<html:select property="lubrication" styleClass="normal" >
	<option value=" "><bean:message key="quotation.option.select"/></option>
	<html:options name="enquiryForm" collection="alLubricationList" property="key" labelProperty="value" />
</html:select>
</td>
<td>&nbsp;</td>
<td class="label-text"><bean:message key="quotation.offer.label.windingrtd"/><span class="mandatory">&nbsp;</span></td>
<td>
<bean:define name="enquiryForm" property="alWindingRTDList" id="alWindingRTDList" type="java.util.Collection" /> 
<div class="data-list-input" style="width:151px; float:left;">
<html:select property="windingRTD" styleId = "windingRTDSel" styleClass="normal data-list-input width-select" >
	<option value=" "><bean:message key="quotation.option.select"/></option>
	<html:options name="enquiryForm" collection="alWindingRTDList" property="key" labelProperty="value" />
</html:select>
<html:text property="windingRTD"  styleId = "windingRTDText" styleClass="normal data-list-input input-class"  maxlength="10"/>
</div>
</td>
</tr>
<tr>
<td class="label-text"><bean:message key="quotation.offerdata.technical.label.noiselevel"/><!-- TODO <span class="mandatory">&nbsp;</span> --></td>
<td>
 <bean:define name="enquiryForm" property="alnoiseLevelList" id="alnoiseLevelList" type="java.util.Collection" /> 
<div class="data-list-input" style="width:151px; float:left;">
<html:select property="noiseLevel" styleClass="normal data-list-input width-select" >
	<option value="As Per IS12065"><bean:message key="quotation.option.select"/></option>
	  <html:options name="enquiryForm" collection="alnoiseLevelList"	property="key" labelProperty="value" />
	</html:select>
 <html:text property="noiseLevel" styleId = "noiseLevelText" styleClass="normal data-list-input input-class" value="As Per IS12065"  maxlength="10"/>
&nbsp;<bean:message key="quotation.units.dba"/>
 </div>
 </td>




<td>&nbsp;</td>
<td class="label-text"><bean:message key="quotation.offer.label.bearingrtd"/><span class="mandatory">&nbsp;</span></td>
<td>
<bean:define name="enquiryForm" property="alBearingRTDList" id="alBearingRTDList" type="java.util.Collection" /> 
<div class="data-list-input" style="width:151px; float:left;">
<html:select property="bearingRTD" styleId = "bearingRTDSel"  styleClass="normal data-list-input width-select" >
	<option value=" "><bean:message key="quotation.option.select"/></option>
	<html:options name="enquiryForm" collection="alBearingRTDList" property="key" labelProperty="value" />
</html:select>
<html:text property="bearingRTD" styleId = "bearingRTDText" styleClass="normal data-list-input input-class"  maxlength="10"/>
</div>

</td>
</tr>
<tr>
<td class="label-text"><bean:message key="quotation.offerdata.technical.label.rtgd"/><!-- TODO <span class="mandatory">&nbsp;</span> --></td>
<td><html:text property="rtGD2" styleClass="normal"  maxlength="10"/>&nbsp;<bean:message key="quotation.units.kgm2"/></td>
<td>&nbsp;</td>
<td class="label-text"><bean:message key="quotation.offer.label.minstartvoltage"/></td>
<td>
<bean:define name="enquiryForm" property="alMinStartVoltList" id="alMinStartVoltList" type="java.util.Collection" /> 
<div class="data-list-input" style="width:151px; float:left;">

<html:select property="minStartVolt"  styleId = "minStartVoltSel" styleClass="normal data-list-input width-select" >
	<option value=" "><bean:message key="quotation.option.select"/></option>
	<html:options name="enquiryForm" collection="alMinStartVoltList" property="key" labelProperty="value" />
</html:select>
<html:text property="minStartVolt" styleId = "minStartVoltText" styleClass="normal data-list-input input-class"   maxlength="10"/>
</div>
</td>
</tr>
<tr>
<td class="label-text"><bean:message key="quotation.offer.label.moi"/></td>
<td><html:text property="migD2Load" styleClass="normal"  maxlength="10"/></td>
<td>&nbsp;</td>
<td class="label-text"><bean:message key="quotation.create.label.stgmethod"/><span class="mandatory">&nbsp;</span></td>
<td>
<bean:define name="enquiryForm" property="alMethodOfStgList" id="alMethodOfStgList" type="java.util.Collection" /> 


<div class="data-list-input" style="width:151px; float:left;">

<html:select property="methodOfStg"  styleId = "methodOfStgSel" styleClass="normal data-list-input width-select" >
	<option value=" "><bean:message key="quotation.option.select"/></option>
	<html:options name="enquiryForm" collection="alMethodOfStgList" property="key" labelProperty="value" />
</html:select>
<html:text property="methodOfStg"  styleId = "methodOfStgText" styleClass="normal data-list-input input-class"  maxlength="10"/>
</div>


</td>
</tr>
<tr>
<td class="label-text"><bean:message key="quotation.offer.label.balancing"/></td>
<td>
<bean:define name="enquiryForm" property="alBalancingList" id="alBalancingList" type="java.util.Collection" /> 

<%--TODO <logic:iterate name="alBalancingList" id="balancing" type="in.com.rbc.quotation.common.vo.KeyValueVo" >
<bean:write name="balancing" property="key" />
</logic:iterate>
 --%>
 <bean:message key="quotation.create.label.balancing"/>
 <%-- TODO <html:select property="balancing" styleClass="normal" >
	<option value="0"><bean:message key="quotation.option.select"/></option>
	<html:options name="enquiryForm" collection="alBalancingList" property="key" labelProperty="value" />
</html:select> --%>
</td>
 
 <td>&nbsp;</td>
  <td  class="label-text">
			        	<bean:message key="quotation.create.label.termbox"/> 
			        </td>
			         <td>
                
 <bean:define name="enquiryForm" property="termBoxList" id="termBoxList" type="java.util.Collection" /> 
<html:select property="termBoxId" styleClass="normal" >
	<option value="0"><bean:message key="quotation.option.select"/></option>
	  <html:options name="enquiryForm" collection="termBoxList"	property="key" labelProperty="value" />
	</html:select>
 
 	</td>
 
 </tr>
</table>


<hr />
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="3">
<tr>
<td class="label-text" width="20%"><bean:message key="quotation.offer.label.dialtypebearingthermometer"/></td>
<td width="20%">
<bean:define name="enquiryForm" property="alBearingThermoDtList" id="alBearingThermoDtList" type="java.util.Collection" /> 
<html:select property="bearingThermoDT" styleClass="normal" >
	<option value=" "><bean:message key="quotation.option.select"/></option>
	<html:options name="enquiryForm" collection="alBearingThermoDtList" property="key" labelProperty="value" />
</html:select>

</td>
<td width="10%">&nbsp;</td>
<td class="label-text" width="20%"><bean:message key="quotation.offer.label.spmnipple"/></td>
<td width="20%">
<bean:define name="enquiryForm" property="alYesNoList" id="alYesNoList" type="java.util.Collection" /> 
<html:select property="spmNipple" styleClass="normal" >
	<option value=" "><bean:message key="quotation.option.select"/></option>
	<html:options name="enquiryForm" collection="alYesNoList" property="key" labelProperty="value" />
</html:select>
</td>
</tr>
<tr>
<td class="label-text"><bean:message key="quotation.offer.label.airthermometer"/></td>
<td>
<bean:define name="enquiryForm" property="alYesNoList" id="alYesNoList" type="java.util.Collection" /> 
<html:select property="airThermo" styleClass="normal" >
	<option value=" "><bean:message key="quotation.option.select"/></option>
	<html:options name="enquiryForm" collection="alYesNoList" property="key" labelProperty="value" />
</html:select>
</td>
<td>&nbsp;</td>
<td class="label-text"><bean:message key="quotation.offer.label.keyphasor"/></td>
<td>
<bean:define name="enquiryForm" property="alYesNoList" id="alYesNoList" type="java.util.Collection" /> 
<html:select property="keyPhasor" styleClass="normal" >
	<option value=" "><bean:message key="quotation.option.select"/></option>
	<html:options name="enquiryForm" collection="alYesNoList" property="key" labelProperty="value" />
</html:select>
</td>
</tr>
<tr>
<td class="label-text"><bean:message key="quotation.offer.label.vibprobemountingpads"/></td>
<td>
<bean:define name="enquiryForm" property="alYesNoList" id="alYesNoList" type="java.util.Collection" /> 
<html:select property="vibeProbMPads" styleClass="normal" >
	<option value=" "><bean:message key="quotation.option.select"/></option>
	<html:options name="enquiryForm" collection="alYesNoList" property="key" labelProperty="value" />
</html:select>
</td>
<td>&nbsp;</td>
<td class="label-text"><bean:message key="quotation.create.label.shaftext"/></td>
<td>
<bean:define name="enquiryForm" property="shaftList" id="shaftList" type="java.util.Collection" /> 
<html:select property="shaftExtId" styleClass="normal" >
	<option value=" "><bean:message key="quotation.option.select"/></option>
	<html:options name="enquiryForm" collection="shaftList" property="key" labelProperty="value" />
</html:select>
</td>
</tr>
<tr>
<td class="label-text"><bean:message key="quotation.offer.label.encoder"/></td>
<td>
<bean:define name="enquiryForm" property="alYesNoList" id="alYesNoList" type="java.util.Collection" /> 
<html:select property="encoder" styleClass="normal" >
	<option value=" "><bean:message key="quotation.option.select"/></option>
	<html:options name="enquiryForm" collection="alYesNoList" property="key" labelProperty="value" />
</html:select>

</td>
<td>&nbsp;</td>
<td class="label-text"><bean:message key="quotation.create.label.paint"/></td>
<td><html:text property="paint" styleClass="normal"  maxlength="10"/></td>
</tr>
<tr>
<td class="label-text"><bean:message key="quotation.offer.label.speedswitch"/></td>
<td>
<bean:define name="enquiryForm" property="alYesNoList" id="alYesNoList" type="java.util.Collection" /> 
<html:select property="speedSwitch" styleClass="normal" >
	<option value=" "><bean:message key="quotation.option.select"/></option>
	<html:options name="enquiryForm" collection="alYesNoList" property="key" labelProperty="value" />
</html:select>
</td>
<td>&nbsp;</td>
<td class="label-text"><bean:message key="quotation.offer.label.paintcolor"/></td>
<td><html:text property="paintColor" styleClass="normal"  maxlength="30"/></td>
</tr>
<tr>
<td class="label-text"><bean:message key="quotation.offer.label.surgesuppressors"/></td>
<td>
<bean:define name="enquiryForm" property="alYesNoList" id="alYesNoList" type="java.util.Collection" /> 
<html:select property="surgeSuppressors" styleClass="normal" >
	<option value=" "><bean:message key="quotation.option.select"/></option>
	<html:options name="enquiryForm" collection="alYesNoList" property="key" labelProperty="value" />
</html:select>
</td>
<td>&nbsp;</td>

</tr>
<tr>
<td class="label-text"><bean:message key="quotation.offer.label.currenttransformers"/></td>
<td>
<bean:define name="enquiryForm" property="alYesNoList" id="alYesNoList" type="java.util.Collection" /> 
<html:select property="currTransformers" styleClass="normal" >
	<option value=" "><bean:message key="quotation.option.select"/></option>
	<html:options name="enquiryForm" collection="alYesNoList" property="key" labelProperty="value" />
</html:select>
</td>
<td>&nbsp;</td>
</tr>
<tr>
<td class="label-text"><bean:message key="quotation.offer.label.vibprobes"/></td>
<td>
<bean:define name="enquiryForm" property="alYesNoList" id="alYesNoList" type="java.util.Collection" /> 
<html:select property="vibProbes" styleClass="normal" >
	<option value=" "><bean:message key="quotation.option.select"/></option>
	<html:options name="enquiryForm" collection="alYesNoList" property="key" labelProperty="value" />
</html:select>

</td>
<td>&nbsp;</td>
<td class="label-text">&nbsp;</td>
<td>&nbsp;</td>
</tr>
</table>

</fieldset>
</div>



			<fieldset style="width:90%">
						       <legend class="blueheader">	
			          	<bean:message key="quotation.create.label.specialfeatures"/>
			          </legend>
			
			     <div >
				  <table  border="0" cellspacing="0" cellpadding="0">
				  <tr><td colspan="4">&nbsp;</td></tr>
                  				  <tr><td colspan="5">&nbsp;</td></tr>
			      				  <tr style="float:left; width:100%;">				  
                    <td colspan="4">
                    	<html:textarea  property="splFeatures" styleId = "splFeatures"  style="width:600px; height:50px" onkeyup="checkMaxLenghtText(this, 2500);" onkeydown="checkMaxLenghtText(this, 2500);" tabindex="35"/>   </td>
                  </tr>
                  
		    </table>
		    </div>
		    </fieldset>
			  

			<fieldset style="width:90%">
						       <legend class="blueheader">	
			          	<bean:message key="quotation.create.label.deviationandclarification"/>
			          </legend>
			
			     <div >
				  <table  border="0" cellspacing="0" cellpadding="0">
				  <tr><td colspan="4">&nbsp;</td></tr>
                  				  <tr><td colspan="5">&nbsp;</td></tr>
			      				  <tr style="float:left; width:100%;">				  
                    <td colspan="4">
                    	<html:textarea  property="deviation_clarification" styleId = "deviation_clarification"  style="width:600px; height:50px" onkeyup="checkMaxLenghtText(this, 10000);" onkeydown="checkMaxLenghtText(this, 10000);" tabindex="35"/>   </td>
                  </tr>
                  
		    </table>
		    </div>
		    </fieldset>
			  

<table style="width:90%">
		<tr>
		      <td style="width:46%; vertical-align:top ">
		        <fieldset>
	            <legend class="blueheader">
	            	<bean:message key="quotation.offerdata.commercial.label.heading"/>
				</legend>
          <table width="100%">
            <tr>
              <td class="formLabel" >&nbsp;</td>
              <td >&nbsp;</td>
            </tr>
            <tr>
              <td class="formLabel"  >
              	<bean:message key="quotation.offerdata.commercial.label.offertype"/>
              </td>
              <logic:equal name="enquiryForm" property="commercialOfferType" value="P">
              	<% hideAddOn = "true"; %>
              </logic:equal>
              <td >
                <html:radio tabindex="16" property="commercialOfferType" value="D" onclick="showhide('Addonsfield','Addonsdummy')" /><bean:message key="quotation.offerdata.commercial.label.offertype.initial"/>                                             	
              	<html:radio tabindex="16" property="commercialOfferType" value="P" onclick="showhide('Addonsdummy','Addonsfield')" /><bean:message key="quotation.offerdata.commercial.label.offertype.final"/>             
              </td>
            </tr>
            <tr>
              <td class="formLabel" >
              	<bean:message key="quotation.offerdata.commercial.label.mcunit"/>              	
              </td>
              <td >&nbsp;
              	<logic:equal name="enquiryForm" property="commercialMCUnit" value="0">
              		<html:text tabindex="17" property="commercialMCUnit" styleId="commercialMCUnit" onkeypress="return isNumber(event);" onblur="return ChangeMCUnit(this.value);"  styleClass="short" style=" width:125px;" value="" maxlength="8" />
              	</logic:equal>
              	
              	<logic:notEqual name="enquiryForm" property="commercialMCUnit" value="0">
              		<html:text tabindex="17" property="commercialMCUnit" styleId="commercialMCUnit" styleClass="short" onkeypress="return isNumber(event);" onblur="return ChangeMCUnit(this.value);" style=" width:125px;" maxlength="8"/>
              	</logic:notEqual>
              	&nbsp;INR
              </td>
              </tr>
              <tr><td class="formLabel">In Words</td><td><div id="amountconversiontowords"></div></td></tr>
              <%String ratingNumber="",previousrationNumber=""; %>
              <logic:notEmpty name="enquiryForm" property="previousratingList">
              <logic:notEmpty name="enquiryForm" property="ratingList">
              <logic:iterate name="enquiryForm" property="ratingList"
               id="ratingListData" indexId="idx"
               type="in.com.rbc.quotation.common.vo.KeyValueVo">
               
               
             <logic:equal name="ratingListData" property="key" value="<%= latestId.toString() %>"> 
                            
              <bean:define id="tech1" name="ratingListData" property="ratingNumber"/>

              
              <%
              ratingNumber=String.valueOf(tech1);

              
              %>
               
              <logic:iterate name="enquiryForm" property="previousratingList"
               id="previousratingListData" indexId="idx1"
               type="in.com.rbc.quotation.common.vo.KeyValueVo">
               <bean:define id="tech2" name="previousratingListData" property="ratingNumber" />
               <%
               previousrationNumber=String.valueOf(tech2);
               
               if(ratingNumber.equalsIgnoreCase(previousrationNumber)){
            	   
            	   
            	   %>
              <tr style="color: red;">
              <td class="formLabel">Previous MC/Unit</td>
              <td >&nbsp;<bean:write name="previousratingListData" property="enquiryNumber"/>&nbsp;::&nbsp;<bean:write name="previousratingListData" property="mcValue"/>&nbsp;</td>
              </tr>
              <%} %>
              </logic:iterate> 
              </logic:equal>
              </logic:iterate> 
              </logic:notEmpty> 
              </logic:notEmpty> 
            </table>
		  </fieldset>			    </td>
			    <td>
				<div id="Addonsdummy" class="showcontent"></div>
				   
				      <fieldset id="Addonsfield">
			          <legend class="blueheader">	
			          	<bean:message key="quotation.offerdata.addon.label.heading"/>
			          </legend>
			  <table width="100%" cellpadding="0" cellspacing="0" id="addons" >
			    <tr>
			      <td height="100%" style="width:130px " class="formLabelTophome" >
			      	<bean:message key="quotation.offerdata.addon.label.addontype"/> 
			      </td>
                  <td class="formLabelTophome">
                  	<bean:message key="quotation.offerdata.addon.label.quantity"/>
                  </td>
                  <td class="formLabelTophome">
                  	<bean:message key="quotation.offerdata.addon.label.unitprice"/> 
                  </td>
                  <td class="formLabelTophome">
                  	<bean:message key="quotation.offerdata.addon.label.delete"/>
                  </td>
                </tr>
                <input type="hidden" name="totalRowCount" id="totalRowCount" value="1"/>  
                <logic:notEmpty name="enquiryForm" property="addOnList">
                	<% int k = 1; %>
                	<logic:iterate name="enquiryForm" property="addOnList"
							               id="addOnDetails" indexId="idx"
							               type="in.com.rbc.quotation.enquiry.dto.AddOnDto">
						<% k = idx.intValue()+1; %> 
						
						 
						<tr>     
		                   	<td>
		                   	
		                  <div class="data-list-input" style="width:200px; float:left;">
						  <select name="addOnType<%=idx.intValue()+1 %>" id="addOnType<%=idx.intValue()+1 %>" class="normal data-list-input width-select">
						   <option value=""><bean:message key="quotation.option.select"/></option> 
						  <option value="Load Test">Load Test</option>
						  <option value="Surge& Impulse test on sample coil">Surge& Impulse test on sample coil</option>
						  <option value="Tan Delta Test">Tan Delta Test</option>
						  <option value="Ring loop Flux test">Ring loop Flux test</option>
						 </select>
						 
		                   	
		                 <input name="addOnTypeText<%=idx.intValue()+1 %>" id="addOnTypeText<%=idx.intValue()+1 %>" class="normal data-list-input input-class"  maxlength="100" value='<bean:write name="addOnDetails" property="addOnTypeText" />' />
		                 </div>

		                   	</td>
		                    <td align="left" ><input name="addOnQuantity<%=idx.intValue()+1 %>"  id="addOnQuantity<%=idx.intValue()+1 %>" class='short' maxlength="2" onkeypress="return isNumber(event)" value='<bean:write name="addOnDetails" property="addOnQuantity" />' /></td>
		                    <td><input name="addOnUnitPrice<%=idx.intValue()+1 %>" id="addOnUnitPrice<%=idx.intValue()+1 %>" class='short' maxlength="8" onkeypress="return isNumber(event)" value='<bean:write name="addOnDetails" property="addOnUnitPrice" />'  /></td>
		                    <td>
		                    <% if (k > 0) { %>
		                    	<img src="html/images/deleteicon.gif" id="delete<%=idx.intValue()+1 %>" alt="Delete"  onClick="removeRow(this);" style="cursor:hand;" />
		                    <%} %> 
		                    </td>
		                </tr>
		                
	                </logic:iterate>
	                <input type="hidden" name="hidRowIndex" id="hidRowIndex" value="<%=k%>">  
                </logic:notEmpty>
                 <logic:empty name="enquiryForm" property="addOnList">
				 
                  <tr>
			      <td align="left"> 
			      
			      
<div class="data-list-input" style="width:200px; float:left;">
<select name="addOnType1" id="addOnType1" class="normal data-list-input width-select">
<option value=""><bean:message key="quotation.option.select"/></option>
<option value="Load Test">Load Test</option>
<option value="Surge& Impulse test on sample coil">Surge& Impulse test on sample coil</option>
<option value="Tan Delta Test">Tan Delta Test</option>
<option value="Ring loop Flux test">Ring loop Flux test</option>
</select>
<input type="text" name="addOnTypeText1"  id = "addOnTypeText1" class="normal data-list-input input-class"  maxlength="100">
</div>
			      	
			      </td>
                  <td align="left">
                  	<input tabindex="19" class="short"  id="addOnQuantity1" name="addOnQuantity1" onkeypress="return isNumber(event)"  maxLength="2">
                  </td>
                  <td>
                  	<input tabindex="20" class="short" id="addOnUnitPrice1" name="addOnUnitPrice1" onkeypress="return isNumber(event)"  maxLength="8">
                  	<input type="hidden" name="hidRowIndex" id="hidRowIndex" value="1">
                  </td>
                  <td style="text-align:left;">
				  <img src="html/images/deleteicon.gif" alt="Delete" onClick="removeRow(this);" id="delete1"  style="cursor:hand;" />
                  </td>
                </tr>
                </logic:empty>
			    
			    </table>
			    <div id="add1" class="blue-light" align="right" style="padding-right:25px; width:95%;">
			      <input type="button" class="BUTTON" onclick="javascript:addrowcreateoffer('addons');" value="Add" />
			      </div>	
			          </fieldset>
		      </td>
		    </tr>
        </table>
			  <!-------------------------- Commercial offer & Add-ons block End  --------------------------------->
    </td>
  </tr>
</table>
			  <!--  Note Section Starts  -->
			<fieldset style="width:90%">
						       <legend class="blueheader">	
			          	<bean:message key="quotation.create.label.notesection"/>
			          </legend>
			
			     <div >
				  <table  border="0" cellspacing="0" cellpadding="0">
				  <tr><td colspan="4">&nbsp;</td></tr>
			      				  <tr style="float:left; width:100%;">				  
				    <td class="formLabelrating" style="width:138px; margin-right: 33px;">
				    	<bean:message key="quotation.create.label.smnote"/>
				    </td>
                    <td colspan="3" class="formLabelrating">

                    <logic:notEqual name="enquiryForm" property="smnote"  value="">
<%--                                         <bean:write name="enquiryForm" property="smnote"/>
                                     &nbsp; By &nbsp;  <bean:write name="enquiryForm" property="sm_name"/>
 --%>                         <html:textarea  property="smnote"  style="width:600px; height:50px" readonly="true" tabindex="35" />            
                    &nbsp; By &nbsp;  <bean:write name="enquiryForm" property="sm_name" />
                    </logic:notEqual>
                    
                  </tr>
                  				  <tr><td colspan="3">&nbsp;</td></tr>
			      				  <tr style="float:left; width:100%;">				  
				    <td class="formLabelrating" style="width:138px; margin-right: 33px;">
				    	<bean:message key="quotation.create.label.dmnote"/>
				    </td>
                    <td colspan="4">
                    	<html:textarea  property="dmnote" styleId = "dmnote"  style="width:600px; height:50px" onkeyup="checkMaxLenghtText(this, 4000);" onkeydown="checkMaxLenghtText(this, 4000);" tabindex="35"/>   </td>
                  </tr>
                  
		    </table>
		    </div>
		    </fieldset>
			  
			  <!--  Note Section Ends-->

			  			  <!-------------------------- Bottum Input buttons block Start  --------------------------------->
			<div class="blue-light-btn" align="right">
			
				 <input name="save" type="button" class="BUTTON" value="Save" onclick="javascript:updateRatingsData();">
				 <logic:notEqual name="enquiryForm" property="isLast" value="true">				 	
 					<html:button property="" styleClass="BUTTON" value="Save &amp; Continue For Next Rating"  onclick="javascript:nextRating()"/>          		
         		</logic:notEqual> 
         		<logic:equal name="enquiryForm" property="enquiryOperationType" value="engineer">  
         			<input type="button" class="BUTTON" onClick="javascript:dereturntosm()"  value="Return to Sales Manager"> 
	         		<html:button property="" styleClass="BUTTON" value="Submit to Manager"  onclick="javascript:submitToManager()"/>
	         	</logic:equal>
	         	<logic:equal name="enquiryForm" property="enquiryOperationType" value="manager"> 
 	                <input type="button" class="BUTTON" onClick="javascript:revisionForsm()"  value="Return to Sales Manager">
 	         		<html:button property="" styleClass="BUTTON" value="Submit to Estimate &amp; Advise"  onclick="javascript:submitToEstimate()"/>
	         	</logic:equal>
	  </div><br>
	  			  </div>
			  <!-------------------------- Bottum Input buttons block End  --------------------------------->
			  

			  
			  			<div id="return" class="showcontent">
			  			<br>
			  			<br>
			  			<br>
			<fieldset>
				<legend class="blueheader">
					<bean:message key="quotation.viewenquiry.return.label.heading"/>
				</legend>
				<table  align="center" >
                  <tr>
                    <td height="23" class="formLabel">
                    	<bean:message key="quotation.viewenquiry.return.label.returnto"/> 
                    </td>
                    <td>
                   	 <bean:define name="enquiryForm" property="teamMemberList"
	                	             id="teamMemberList" type="java.util.Collection" /> 
                   	 <html:select property="returnTo">                    	                  
                        <html:options name="enquiryForm" collection="teamMemberList"
			 				              property="key" labelProperty="value" />
                     </html:select>
                    </td>
                  </tr>
                  <tr>
                    <td class="formLabel">
                    	<bean:message key="quotation.viewenquiry.return.label.comments"/>
					</td>
                    <td>
                    	<html:textarea property="returnToRemarks" styleId="returnToRemarks" onchange = "javascript:checkMaxLength(this.id, 4000);"/>
                    </td>
                  </tr>
                </table>
			</fieldset>
			<div class="blue-light" align="right">
			   <input name="cancel" type="button" class="BUTTON" onClick="javascript:estimateForsm();" value="Cancel">	
               <input name="new" type="button" class="BUTTON" onClick="javascript:returnEnquiry();" value="Return">                           
            </div>
			</div>
			  
			  

<!-------------------------- main table End  --------------------------------->
</html:form>

<% if (hideAddOn.equalsIgnoreCase("true")){ %>
	<script>
		showhide('Addonsdummy','Addonsfield')
		</script>
<%}%>

<ajax:autocomplete
		fieldId="customerName"
		popupId="customer-popup"
		targetId="customerId"
		baseUrl="servlet/AutocompleteServlet?type=placeenquiry&valueType=id"
		paramName="customer"
		className="autocomplete"
		progressStyle="throbbing" 
		postFunc="postAutocomplete"/>		 

	<iframe
		 id="divShim"
		src="javascript:false;"
		scrolling="no"
		frameborder="0"
		style="position:absolute; top:0px; left:0px; display:none;">
	</iframe>
	
<script>
$(document).ready(function(){

	calCurrent();	
	calculateNTorque();
});


function updateCurrent(currValue){

	if(currValue!=null && currValue!=""){
		document.enquiryForm.pllCurr1.value =currValue;
		document.enquiryForm.flcAmps.value =currValue;
	
	}
}

jQuery(function() {
	//keep the focus on the input box so that the highlighting
  //I'm using doesn't give away the hidden select box to the user
	$('select.data-list-input').focus(function() {
		$(this).siblings('input.data-list-input').focus();
	});
  //when selecting from the select box, put the value in the input box
	$('select.data-list-input').change(function() {
		$(this).siblings('input.data-list-input').val($(this).val());
	});
  //When editing the input box, reset the select box setting to "free
  //form input". This is important to do so that you can reselect the
  //option you had selected if you want to.
	$('input.data-list-input').change(function() {
		$(this).siblings('select.data-list-input').val('');
	});
  
  
});
</script>	


