<%@ include file ="../../common/library.jsp"%>
<%@ include file="../../common/nocache.jsp" %>
<html:form action="mastersdata.do" method="post" >
<html:hidden property="invoke" value="viewSearchMasterList"/>
<html:hidden property="masterId" value=""/>
<html:hidden property="master" value=""/>
<!--main content  table begins-->
<table width="80%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
<tr>
  <td height="100%" valign="top"><!-- InstanceBeginEditable name="content area" -->
  <div class="sectiontitle"><bean:message key="quotation.admin.managemasterheading"/></div>
  <br>
  <fieldset>
    <legend class="blueheader" ID="RATING1"><bean:message key="quotation.admin.enggdatalist"/> </legend>
  		<logic:equal name="sizeOfList" value="0">				
			<table width="100%" cellspacing="0" bgcolor="#F7F7F7">
				<tr class="color3" align="center">
					<td class="note"><bean:message key="quotation.messages.noresultsfound"/></td>
				</tr>
			</table>
		</logic:equal>
  		<logic:notEqual name="sizeOfList" value="0">
    		<table width="70%" align="center">
       			<tr>
		            <td>&nbsp;</td>
		            <td width="50px" height="100%" rowspan="5">
		            	<table width="1%" height="100%" border="0" align="center" cellpadding="0" cellspacing="0">
			                <tr>
			                  <td background="../html/images/dot.gif"><img src="../html/images/dot.gif" width="1" height="5" /> </td>
	                		</tr>
          				</table>
          			</td>
           			 <td>&nbsp;</td>
            		 <td width="50px" height="100%" rowspan="13" align="center">
            			<table width="1%" height="100%" border="0" align="center" cellpadding="0" cellspacing="0">
			                <tr>
			                  <td background="../html/images/dot.gif"><img src="../html/images/dot.gif" width="1" height="5" /> </td>
			                </tr>
          				</table>
          			</td>
           			 <td>&nbsp;</td>
            		 <td width="50px" height="100%" rowspan="13" align="center">
            			<table width="1%" height="100%" border="0" align="center" cellpadding="0" cellspacing="0">
			                <tr>
			                  <td background="../html/images/dot.gif"><img src="../html/images/dot.gif" width="1" height="5" /> </td>
			                </tr>
          				</table>
          			</td>
            		<td>&nbsp;</td>
        		</tr>
        
        <%int iColumnCount=0; %>
        <tr>
        <logic:iterate name="resultsList" scope="request" id="searchMasterList"
				   indexId="idx" type="in.com.rbc.quotation.common.vo.KeyValueVo">
        <%if(iColumnCount==4){
        	iColumnCount=0;
        %>
        </tr><tr>
        <% }
       
        %>
        <td><a href="javaScript:loadEngineeringData('<bean:write name="searchMasterList" property="key"/>','<bean:write name="searchMasterList" property="value"/>')"><bean:write name="searchMasterList" property="value"/></a></td>

        <%iColumnCount++;%>
        </logic:iterate>
        </tr>
       	
      </tr>

    </table>
    </logic:notEqual>
    
    </fieldset>
	<br>


  </html:form>
  <!-- InstanceEndEditable --></td>
</tr>
</table>
<!--main content  table end--> 
    