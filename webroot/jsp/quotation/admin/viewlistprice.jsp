<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants" %>
<%@ page import="in.com.rbc.quotation.common.utility.PropertyUtility" %>
<%@ page import="in.com.rbc.quotation.common.utility.CommonUtility" %>
<%@ include file ="../../common/library.jsp"%>
<%@ include file="../../common/nocache.jsp" %>

<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<html:form action="listprice.do" method="POST" enctype="multipart/form-data">
<html:hidden property="invoke"/>
<html:hidden property="serial_id" styleId="serial_id"/>
<html:hidden property="isFileLoaded" value="YES"/>

<input type="hidden" name="<%=org.apache.struts.taglib.html.Constants.TOKEN_KEY%>" value="<%=session.getAttribute(org.apache.struts.action.Action.TRANSACTION_TOKEN_KEY)%>">

<table width="80%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
	<tr>
    	<td height="100%" valign="top">
      				<br>
	    			<!-------------------------- Title & Search By Block start  --------------------------------->
      				<div class="sectiontitle"><bean:message key="quotation.label.admin.list.price"/> </div>
     				<br>
						
					<fieldset>
							<legend class="blueheader"> <bean:message key="quotation.label.admin.searchby" /> </legend>
							<table align="center" border="0" cellpadding="0" cellspacing="2">
								<tr>
									<td colspan="5">&nbsp;</td>
								</tr>
								<tr>
									<td class="formLabel" style="width:155px !important;"> <bean:message key="quotation.label.admin.field.manufacturing.location" /> </td>
									<td>
					            		<html:select property="manufacturing_location" styleId="manufacturing_location" styleClass="normal">
					            			<html:option value="0">Select</html:option>
							            	<logic:notEmpty name="listPriceForm" property="maufacturingList">
							            	<bean:define name="listPriceForm" property="maufacturingList" id="maufacturingList" type="java.util.Collection" />
							            	<html:options name="listPriceForm" collection="maufacturingList" property="value" labelProperty="value" />
							            	</logic:notEmpty>
					            		</html:select>
					            	</td>
									<td style="width:55px !important;"> &nbsp; </td>
									<td class="formLabel" style="width:135px !important;"><bean:message key="quotation.label.admin.field.Power.Rating_KW"/> </td>
					            	<td>
					            		<html:select property="power_rating_kw" styleId="power_rating_kw" styleClass="normal">
							            	<html:option value="0">Select</html:option>
							            	<logic:notEmpty name="listPriceForm" property="powerratingkwList">
							            	<bean:define name="listPriceForm" property="powerratingkwList" id="powerratingkwList" type="java.util.Collection" />
							            	<html:options name="listPriceForm" collection="powerratingkwList" property="value" labelProperty="value" />
							            	</logic:notEmpty>
					            		</html:select>
					            	</td>
								</tr>
								<tr>
									<td class="formLabel" style="width:155px !important;"><bean:message key="quotation.label.admin.field.product.group"/> </td>
					            	<td>
					            		<html:select property="product_group" styleId="product_group" styleClass="normal">
					            			<html:option value="0">Select</html:option>
							            	<logic:notEmpty name="listPriceForm" property="productGroupList">
							            	<bean:define name="listPriceForm" property="productGroupList" id="productGroupList" type="java.util.Collection" />
							            	<html:options name="listPriceForm" collection="productGroupList" property="value" labelProperty="value" />
							            	</logic:notEmpty>
					            		</html:select>
					            	</td>
					            	<td style="width:55px !important;"> &nbsp; </td>
					            	<td class="formLabel" style="width:135px !important;"><bean:message key="quotation.label.admin.field.product.line"/> </td>
					                <td>
					                	<html:select property="product_line" styleId="product_line" styleClass="normal">
							            	<html:option value="0">Select</html:option>
							            	<logic:notEmpty name="listPriceForm" property="productLineList">
							            	<bean:define name="listPriceForm" property="productLineList" id="productLineList" type="java.util.Collection" />
							            	<html:options name="listPriceForm" collection="productLineList" property="value" labelProperty="value" />
							            	</logic:notEmpty>
					            		</html:select>
					            	</td>						
								</tr>
								<tr>
									<td class="formLabel" style="width:155px !important;"><bean:message key="quotation.label.admin.field.frame.type"/> </td>
					            	<td>
					            		<html:select property="frame_type" styleId="frame_type" styleClass="normal">
							            	<html:option value="0">Select</html:option>
							            	<logic:notEmpty name="listPriceForm" property="frametypelist">
							            	<bean:define name="listPriceForm" property="frametypelist" id="frametypelist" type="java.util.Collection" />
							            	<html:options name="listPriceForm" collection="frametypelist" property="value" labelProperty="value" />
							            	</logic:notEmpty>
					            		</html:select>
					            	</td>
					            	<td style="width:55px !important;"> &nbsp; </td>
					            	<td class="formLabel" style="width:135px !important;"><bean:message key="quotation.label.admin.field.frame.suffix"/> </td>
					            	<td>
					            		<html:select property="frame_suffix" styleId="frame_suffix" styleClass="normal">
					            			<html:option value="0">Select</html:option>
							            	<logic:notEmpty name="listPriceForm" property="framesuffixlist">
							            	<bean:define name="listPriceForm" property="framesuffixlist" id="framesuffixlist" type="java.util.Collection" />
							            	<html:options name="listPriceForm" collection="framesuffixlist" property="value" labelProperty="value" />
							            	</logic:notEmpty>
					            		</html:select>
					            	</td>
								</tr>
							</table>
							<br>
							<div class="blue-light" align="right">
								<html:button property="" styleClass="BUTTON" value="Clear" onclick="javascript:resetListPriceSearch();" />
								<html:button property="" styleClass="BUTTON" value="Search" onclick="javascript:searchListPrice();" />
							</div>
					</fieldset>
					
					<br>
				  	<div class="sectiontitle"><bean:message key="quotation.search.searchresults" /></div>
			      	<br>
			      	<%-- 
						Listing taglib's hidden fields tag that includes the necessary hidden fields
						related to pagination, at runtime
				   	--%>
				 	<listing:hiddenfields />
			  	 	<%-- Displaying 'No Results Found' message if size of list is zero --%>

					<logic:equal name="sizeOfList" value="0">
						<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#F7F7F7">
							<tr class="other" align="center">
								<td><bean:message key="quotation.label.admin.noresultsfound" /></td>
							</tr>
						</table>
					</logic:equal>

					<logic:notEqual name="sizeOfList" value="0">
						<logic:notPresent name="oListPriceDto" property="searchResultsList" scope="request">
							<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#F7F7F7">
								<tr align="center" class="other">
										<td><bean:message key="quotation.label.admin.noresultsfound" /></td>
								</tr>
							</table>
						</logic:notPresent>
						<logic:present name="oListPriceDto" property="searchResultsList" scope="request">
							<logic:empty name="oListPriceDto" property="searchResultsList">
								<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#F7F7F7">
										<tr align="center" class="other">
											<td><bean:message key="quotation.label.admin.noresultsfound" /></td>
										</tr>
								</table>
							</logic:empty>
							<logic:notEmpty name="oListPriceDto" property="searchResultsList">
								<table width="100%" border="0" cellspacing="0" cellpadding="0" class="blue-light">
									<tr>
										<td align="right" bgcolor="#CCD8F2"><listing:pagelist navigation="1" /></td>
									</tr>
								</table>
								<br>
								<!-------------------------------------- search results block end --------------------------->
								<!-------------------------------------- buttons block start -------------------------------->
								
								<table cellspacing="0" cellpadding="2" width="100%" ID="table0" class="sortable">
									<tr id="top">
										<td class="bordertd">
											<listing:sort dbfieldid="1"> <bean:message key="quotation.label.admin.field.manufacturing.location" /> </listing:sort>
										</td>
										<td class="bordertd">
											<listing:sort dbfieldid="2"> <bean:message key="quotation.label.admin.field.product.group" /> </listing:sort>
										</td>
										<td class="bordertd">
											<listing:sort dbfieldid="3"> <bean:message key="quotation.label.admin.field.product.line" /> </listing:sort>
										</td>
										<td class="bordertd" style="width: 5%" align="center"><bean:message key="quotation.label.admin.field.Standard" /></td>
										<td class="bordertd" style="width: 10%" align="center"><bean:message key="quotation.label.admin.field.Power.Rating_KW" /></td>
										<td class="bordertd" style="width: 5%" align="center"><bean:message key="quotation.label.admin.field.frame.type" /></td>
										<td class="bordertd" style="width: 5%" align="center"><bean:message key="quotation.label.admin.field.frame.suffix" /></td>
										<td class="bordertd" style="width: 5%" align="center"><bean:message key="quotation.label.admin.field.number.of.poles" /></td>
										<td class="bordertd" style="width: 10%" align="center"><bean:message key="quotation.label.admin.field.mounting.type" /></td>
										<td class="bordertd" style="width: 5%" align="center"><bean:message key="quotation.label.admin.field.tB.position" /></td>
										<td class="bordertd" style="width: 10%" align="center"><bean:message key="quotation.label.admin.field.list.price" /></td>
										<td class="bordertd" style="width: 5%" align="center"><bean:message key="quotation.label.admin.field.material.cost" /></td>
										<td class="bordertd" style="width: 5%" align="center"><bean:message key="quotation.label.admin.field.loh" /></td>
										<td style="width: 5%" align="center"><bean:message key="quotation.search.edit" /></td>
										<td style="width: 5%"><bean:message key="quotation.search.delete" /></td>
									</tr>
									<%
										String sTextStyleClass = "textnoborder-light";
									%>
									<logic:iterate name="oListPriceDto" property="searchResultsList" id="listPriceForm" indexId="idx" type="in.com.rbc.quotation.admin.dto.ListPriceDto">
										<%
											int id1 = idx.intValue();
											id1 = (int) id1 + 1;
										%>
										<%
											if (idx.intValue() % 2 == 0) {
										%>
										<%
											sTextStyleClass = "textnoborder-light";
										%>
										<tr class="light" id='item<bean:write name="idx"/>' onmouseover="Change(this.id);ChangeTextbox('loc_<%=id1%>_3','loc_<%=id1%>_4');"
												onMouseOut="ChangeBack(this.id);ChangeBackTextbox('loc_<%=id1%>_3','loc_<%=id1%>_4','<%=sTextStyleClass%>');">
										<%
											} else {
										%>
										<%
											sTextStyleClass = "textnoborder-dark";
										%>
											
										<tr class="dark" id='item<bean:write name="idx"/>' onmouseover="Change(this.id);ChangeTextbox('loc_<%=id1%>_3','loc_<%=id1%>_4');"
												onMouseOut="ChangeBack(this.id);ChangeBackTextbox('loc_<%=id1%>_3','loc_<%=id1%>_4','<%=sTextStyleClass%>');">
										<%
											}
										%>
										
											<td class="bordertd"><bean:write name="listPriceForm" property="manufacturing_location" /></td>
											<td class="bordertd"><bean:write name="listPriceForm" property="product_group" /></td>
											<td class="bordertd"><bean:write name="listPriceForm" property="product_line" /></td>
											
											<logic:equal name="listPriceForm" property="standard" value="Y">
												<td class="bordertd"><bean:message key="quotation.label.yes" /></td>
											</logic:equal>
											<logic:equal name="listPriceForm" property="standard" value="N">
												<td class="bordertd"><bean:message key="quotation.label.no" /></td>
											</logic:equal>
								
											<td class="bordertd"><bean:write name="listPriceForm" property="power_rating_kw" /></td>
											<td class="bordertd"><bean:write name="listPriceForm" property="frame_type" /></td>
											<td class="bordertd"><bean:write name="listPriceForm" property="frame_suffix" /></td>
											<td class="bordertd"><bean:write name="listPriceForm" property="number_of_poles" /></td>
											<td class="bordertd"><bean:write name="listPriceForm" property="mounting_type" /></td>
											<td class="bordertd"><bean:write name="listPriceForm" property="tb_position" /></td>
											<td class="bordertd"><bean:write name="listPriceForm" property="list_price" /></td>
											<td class="bordertd"><bean:write name="listPriceForm" property="material_cost" /></td>
											<td class="bordertd"><bean:write name="listPriceForm" property="loh" /></td>
											<td>
												<div align="center">
													<img src="html/images/edit.gif" alt="Edit" width="20" height="18" border="0" style="cursor: hand"
															onClick="javascript:editListPrice('<bean:write name="listPriceForm" property="serial_id"/>')">
												</div>
											</td>
											<td align="center">
												<img src="html/images/deleteicon.gif" alt="Delete" width="15" height="16" border="0" style="cursor: hand"
														onClick="javascript:deleteListPrice('<bean:write name="listPriceForm" property="serial_id"/>', '<bean:write name="listPriceForm" property="manufacturing_location"/>')">
											</td>
										</tr>
									</logic:iterate>
								</table>

								<!-------------------------------------- search results block end --------------------------->
							
								<table width="100%" cellpadding="0" cellspacing="0" border="0" class="blue-light">
									<tr>
										<td align="left" bgcolor="#CCD8F2"><listing:listsummary /></td>
										<td align="right" bgcolor="#CCD8F2"><listing:pagelist navigation="2" /></td>
									</tr>
								</table>
								<br>
								
							</logic:notEmpty>
						</logic:present>
					</logic:notEqual>

					<table width="100%" class="blue-light" cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td>
								<html:button property="" value="Add New" styleClass="BUTTON" onclick="javascript:addNewListPrice();" />
							</td>
							<td align="right">
								<html:button property="" value="Export to Excel" styleClass="BUTTON" onclick="javascript:exportListPriceResult();" />
							</td>
						</tr>
					</table>
					
					<table width="100%" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td>
								<br>
								<div class="sectiontitle"><bean:message key="quotation.label.admin.list.price" /> <bean:message key="quotation.search.bulkupload" /></div>
						      	<br>
						      	
						      	<fieldset>
						      		<table width="100%" border="0" cellpadding="0" cellspacing="0">
						      			<tr>
						      				<td nowrap>
						      					<div class="box-nav">
						      						<p class="list-hd">Instructions for Bulk Loading QuotationLT List Price values:</p>
						      						<ul>
									                	<li>List Prices information should be provided in a specific format.</li>
									       				<li>Please  <a href="html/docs/ListPriceBulkload.xls" target="_blank">click here</a> to download the template to load List Prices in the Tool.</li>
									                	<li>Please ensure all mandatory columns are filled in to avoid disturbances in data</li>
														<!--  <li>Please note that this feature is limited to only < % =PropertyUtility.getKeyValue(QuotationConstants.EHS_BULKUPLOAD_RECORD_COUNT) % > records. </li> -->
														<li>Please note that this feature is limited to only 500 records.
							                	 	</ul>
						      					</div>
						      				</td>
						      			</tr>
						      		</table>
						      		<table width="100%" border="0" cellpadding="0" cellspacing="0">
						      			<tr>
						      				<td align="center">Select the file to be uploaded</td>
						      				<td align="left"> <html:file property="listpriceBulkupload" style="width:218px; font:10px;" /> </td>
						      			</tr>
						      		</table>
						      		<br>
						      		<div class="blue-light" align="right">
										<html:button property="" value="Bulk Upload" styleClass="button" onclick="validateListPriceBulkLoad();" />
									</div>
						      	</fieldset>
							</td>
						</tr>
					</table>
								      	
		</td>
	</tr>
</table>


</html:form>