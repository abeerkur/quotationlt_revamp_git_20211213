<!-------------------------- main table Start  --------------------------------->
<%@ include file ="../../common/library.jsp"%>
<%@ include file="../../common/nocache.jsp" %>
<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants" %>
<html:form action="/updatemanagers.do" method="post" onsubmit="return saveValue();">
	<html:hidden property="invoke" value="saveValues" />
	<html:hidden property="operation" styleId="operation"/>
	<html:hidden property="cheifExecutiveSSO" styleId="cheifExecutiveSSO"/>
	<html:hidden property="designManagerSSO" styleId="designManagerSSO"/>
	<html:hidden property="regionalSalesManagerSSO" styleId="regionalSalesManagerSSO"/>
	<html:hidden property="salesManagerSSO" styleId="salesManagerSSO"/>
	<html:hidden property="commercialManagerSSO" styleId="commercialManagerSSO"/>
	
	<table width="80%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
  		<tr>
    		<td height="100%" valign="top">
    			<br>
		  		<!-------------------------- Title & Note Block start  --------------------------------->
        		<div class="sectiontitle"><bean:message key="quotation.label.admin.update.updatemanager"/> </div>
			    <br>
			        <div class="DotsTable"><bean:message key="quotation.label.admin.update.message"/> </div>
			    <br>
				<div class="note" align="right" style="width:100%">
				<font color="red"><bean:message key="quotation.label.manadatory"/></font>
				</div>
		  		<!-------------------------- Title & Note Block End  --------------------------------->
		  		<!-------------------------- Select Manager Block Start  --------------------------------->
        		<fieldset>
        			<legend class="blueheader"><bean:message key="quotation.label.admin.update.selectmanager"/> </legend>
					    <br>
					    <table width="80%"  border="0" cellspacing="2" cellpadding="0" align="center">
						    <tr> 
						    	<td>
						      		<html:radio name="updateManagerForm"  property="managertype"  
						      			onclick="showreplacemanagers('CEO','dm','rsm','sm','de','cm')" 
						      			value="<%=QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_CE %>" >
						      		<bean:message key="quotation.label.admin.update.ce"/> 
						      		</html:radio>
						      	</td>
					  	      	<td>
					  	      		<html:radio name="updateManagerForm"  property="managertype" 
						  	      		onclick="showreplacemanagers('dm','CEO','rsm','sm','de','cm')" 
						  	      		value="<%=QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_DM %>">
						  	      		<bean:message key="quotation.label.admin.update.dm"/>  
					  	      		</html:radio>
					  	      	</td>
					  	      	<td>
					  	      		<html:radio name="updateManagerForm"  property="managertype"
					  	      		 	onclick="showreplacemanagers('rsm','CEO','sm','dm','de','cm')" 
					  	      		 	value="<%= QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_RSM %>">
					  	        	<bean:message key="quotation.label.admin.update.rsm"/>
					  	        	</html:radio>
					  	        </td>
					  	      	<td>
					  	      		<html:radio name="updateManagerForm"  property="managertype" 
						  	      		onclick="showreplacemanagers('sm','CEO','rsm','dm','de','cm')" 
						  	      		value="<%= QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_SM %>">
					  	      		<bean:message key="quotation.label.admin.sales.salesmanager"/>  
					  	      		</html:radio>
					  	      	</td>
					  	      	<td>
					  	      		<html:radio name="updateManagerForm"  property="managertype" 
						  	      		onclick="showreplacemanagers('de','CEO','rsm','sm','dm','cm')" 
						  	      		value="<%=QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_DE %>">
						  	      		<bean:message key="quotation.label.admin.update.de"/>  
					  	      		</html:radio>
					  	      	</td>
					  	      	
					  	      	<td>
					  	      		<html:radio name="updateManagerForm"  property="managertype" 
						  	      		onclick="showreplacemanagers('cm','CEO','rsm','sm','dm','de')" 
						  	      		value="<%=QuotationConstants.QUOTATION_WORKFLOW_NEXTLEVEL_CM %>">
						  	      		<bean:message key="quotation.label.admin.update.cm"/>  
					  	      		</html:radio>
					  	      	</td>
					  	    </tr>
	      				</table>
	        		<br>
	        		<div id="CEO" style="display:none" name="ceo">
				    	<table width="60%"  border="0" align="center" cellpadding="2" cellspacing="2">
				        	<tr>
				          		<td colspan="2" class="message"><bean:message key="quotation.label.admin.update.cemessage"/> </td>
		                  	</tr>
					      	<tr>
					      		<td class="formLabel"><bean:message key="quotation.label.admin.update.oldce"/></td>
			              		<td>
			              			<bean:write name="updateManagerForm" property="cheifExecutiveOldName" />
									<html:hidden name="updateManagerForm" property="cheifExecutiveOldSSO"/>
			              	</tr>
					      	<tr>
					      		<td class="formLabel"><bean:message key="quotation.label.admin.update.newce"/></td>
			              		<td>
			              			<html:text property="cheifExecutiveName" styleId="cheifExecutiveName" styleClass="readonly" readonly="true" />
			              			<img src="html/images/lookup.gif" class="lookup" width="20" height="18" border="0" align="absmiddle" style="cursor:hand " onclick="javascript:lookupEmployee('<%=QuotationConstants.QUOTATION_LOOKUPSELECTTYPE_SINGLE%>', 'cheifExecutiveSSO', 'cheifExecutiveName', '', '', '', '');">
			              	</tr>
					        <tr>
					          <td class="formLabel">&nbsp;</td>
			                  <td>&nbsp;</td>
			                </tr>
				        </table>
			    	</div>
			    	<div id="de" style="display:none" name="de">
				    	<table width="60%"  border="0" align="center" cellpadding="2" cellspacing="2">
				  	      	<tr>
				  	        	<td colspan="2"><span class="message"><bean:message key="quotation.label.admin.update.demessage"/></span></td>
		                    </tr>
		                    <tr>
						        <td class="formLabel"><bean:message key="quotation.label.admin.update.existde"/> </td>
		                        <td>
			                        <html:select property="oldDesignEngineerSSO" styleClass="normal">
						                  	<html:option value="0">Select</html:option>
						                  	<logic:present name="updateManagerForm" property="designEngineerList">
						                  	<bean:define name="updateManagerForm" property="designEngineerList"
							                	             id="designEngineerList" type="java.util.Collection" />
						                  	<html:options name="updateManagerForm" collection="designEngineerList"
									 				              property="key" labelProperty="value" />
						                  	</logic:present>
						        	</html:select> 
		                        </td>
                      		</tr>
				  	      	<tr>
					  	        <td class="formLabel"> <bean:message key="quotation.label.admin.update.newde"/> </td>
			                    <td>
			                    	<html:select property="newDesignEngineerSSO" styleClass="normal">
						                  	<html:option value="0">Select</html:option>
						                  	<logic:present name="updateManagerForm" property="designEngineerList">
						                  	<bean:define name="updateManagerForm" property="designEngineerList"
							                	             id="designEngineerList" type="java.util.Collection" />
						                  	<html:options name="updateManagerForm" collection="designEngineerList"
									 				              property="key" labelProperty="value" />
						                  	</logic:present>
						        	</html:select> 
			              		</td>
		                  	</tr>
				  	      	<tr>
				  	        	<td class="formLabel">&nbsp;</td>
		                    <td>&nbsp;</td>
		                  	</tr>
			  	      	</table>
			    	</div>
			    	
			    	<div id="dm" class="showcontent" style="display:none" name="dm">
			  	    	<table width="60%"  border="0" align="center" cellpadding="2" cellspacing="2">
				  	      	<tr>
				  	        	<td colspan="2"><span class="message"><bean:message key="quotation.label.admin.update.dmmessage"/></span></td>
		                    </tr>
				  	      	<tr>
					  	        <td class="formLabel"> <bean:message key="quotation.label.admin.update.olddm"/> </td>
			                    <td>
			                    	<bean:write name="updateManagerForm" property="designManagerOldName" />
			                    	<html:hidden name="updateManagerForm" property="designManagerOldSSO"/>
			              		</td>
		                  	</tr>
				  	      	<tr>
					  	        <td class="formLabel"> <bean:message key="quotation.label.admin.update.newdm"/> </td>
			                    <td>
			                    	<html:text property="designManagerName" styleId="designManagerName" styleClass="readonly" readonly="true" />
			              			<img src="html/images/lookup.gif" class="lookup" width="20" height="18" border="0" align="absmiddle" style="cursor:hand " onclick="javascript:lookupEmployee('<%=QuotationConstants.QUOTATION_LOOKUPSELECTTYPE_SINGLE%>', 'designManagerSSO', 'designManagerName', '', '', '', '');">
			              		</td>
		                  	</tr>
				  	      	<tr>
				  	        	<td class="formLabel">&nbsp;</td>
		                    <td>&nbsp;</td>
		                  	</tr>
			  	      	</table>
			    	</div>
			    	<div id="rsm" class="showcontent" style="display:none" name="rsm">
			  	    	<table width="60%"  border="0" align="center" cellpadding="2" cellspacing="2">
				  	      	<tr>
				  	        	<td colspan="2"><span class="message"><bean:message key="quotation.label.admin.update.rsmmessage"/></span></td>
		                    </tr>
		                  	<tr>
						        <td class="formLabel"><bean:message key="quotation.label.admin.update.existrsm"/> </td>
		                        <td>
		                        	<html:select property="regionalSalesManagerID" styleClass="normal">
					                  	<html:option value="0">Select</html:option>
					                  	<logic:present name="updateManagerForm" property="regionalSalesManagerList">
					                  	<bean:define name="updateManagerForm" property="regionalSalesManagerList"
						                	             id="regionalSalesManagerList" type="java.util.Collection" />
					                  	<html:options name="updateManagerForm" collection="regionalSalesManagerList"
								 				              property="key" labelProperty="value" />
					                  	</logic:present>
					                </html:select>  
		                        </td>
                      		</tr>
						    <tr>
						    	<td class="formLabel"><bean:message key="quotation.label.admin.update.newrsm"/>  </td>
		                        <td><html:text property="regionalSalesManagerName" styleId="regionalSalesManagerName" styleClass="readonly" readonly="true" />
		                          <img src="html/images/lookup.gif" class="lookup" width="20" height="18" border="0" align="absmiddle" style="cursor:hand " onclick="javascript:lookupEmployee('<%=QuotationConstants.QUOTATION_LOOKUPSELECTTYPE_SINGLE%>', 'regionalSalesManagerSSO', 'regionalSalesManagerName', '', '', '', '');">
		                         </td>
							</tr>
				  	      	<tr>
				  	        	<td class="formLabel">&nbsp;</td>
		                    <td>&nbsp;</td>
		                  	</tr>
			  	      	</table>
			    	</div>
			    	<div id="sm" class="showcontent" style="display:none" name="sm">
			  	    	<table width="60%"  border="0" align="center" cellpadding="2" cellspacing="2">
				  	      	<tr>
				  	        	<td colspan="2"><span class="message"><bean:message key="quotation.label.admin.update.smmessage"/></span></td>
		                    </tr>
		                    <tr>
						        <td class="formLabel"><bean:message key="quotation.label.admin.update.existsm"/> </td>
		                        <td>
			                        <html:select property="oldSalesManagerSSO" styleClass="normal">
						                  	<html:option value="0">Select</html:option>
						                  	<logic:present name="updateManagerForm" property="salesManagerList">
						                  	<bean:define name="updateManagerForm" property="salesManagerList"
							                	             id="salesManagerList" type="java.util.Collection" />
						                  	<html:options name="updateManagerForm" collection="salesManagerList"
									 				              property="key" labelProperty="value" />
						                  	</logic:present>
						        	</html:select> 
		                        </td>
                      		</tr>
				  	      	<tr>
					  	        <td class="formLabel"> <bean:message key="quotation.label.admin.update.newsm"/> </td>
			                    <td>
			                    	<html:select property="newSalesManagerSSO" styleClass="normal">
						                  	<html:option value="0">Select</html:option>
						                  	<logic:present name="updateManagerForm" property="salesManagerList">
						                  	<bean:define name="updateManagerForm" property="salesManagerList"
							                	             id="salesManagerList" type="java.util.Collection" />
						                  	<html:options name="updateManagerForm" collection="salesManagerList"
									 				              property="key" labelProperty="value" />
						                  	</logic:present>
						        	</html:select> 
			              		</td>
		                  	</tr>
		                  	</table>
		                  	</div>
		                  	
		                  	<div id="cm" class="showcontent" style="display:none" name="cm">
			  	    	<table width="60%"  border="0" align="center" cellpadding="2" cellspacing="2">
				  	      	<tr>
				  	        	<td colspan="2"><span class="message"><bean:message key="quotation.label.admin.update.cmmessage"/></span></td>
		                    </tr>
				  	      	<tr>
					  	        <td class="formLabel"> <bean:message key="quotation.label.admin.update.oldcm"/> </td>
			                    <td>
			                    	<bean:write name="updateManagerForm" property="commercialManagerOldName" />
			                    	<html:hidden name="updateManagerForm" property="commercialManagerOldSSO"/>
			              		</td>
		                  	</tr>
				  	      	<tr>
					  	        <td class="formLabel"> <bean:message key="quotation.label.admin.update.newcm"/> </td>
			                    <td>
			                    	<html:text property="commercialManagerName" styleId="commercialManagerName" styleClass="readonly" readonly="true" />
			              			<img src="html/images/lookup.gif" class="lookup" width="20" height="18" border="0" align="absmiddle" style="cursor:hand " onclick="javascript:lookupEmployee('<%=QuotationConstants.QUOTATION_LOOKUPSELECTTYPE_SINGLE%>', 'commercialManagerSSO', 'commercialManagerName', '', '', '', '');">
			              		</td>
		                  	</tr>
				  	      	<tr>
				  	        	<td class="formLabel">&nbsp;</td>
		                    <td>&nbsp;</td>
		                  	</tr>
			  	      	</table>
			    	</div>
				  	      	
			    	
				 <!-------------------------- Select Manager Block End  --------------------------------->
				   <!--------------------------------------buttons block start --------------------------->

  				    <div class="blue-light" align="right">
  				    	<html:submit styleClass="BUTTON" value="Save" />
		        	</div>
			  <!--------------------------------------buttons block start --------------------------->

	  			</fieldset>
        	</td>
		</tr>
	</table>
</html:form>

