<!---------------- custdiscountbulkuploadreport.jsp ------------>

<%@ include file ="../../common/library.jsp"%>
<%@ include file="../../common/nocache.jsp" %>
<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants" %>
<%@ page import="in.com.rbc.quotation.common.utility.PropertyUtility" %>
<%@ page import="in.com.rbc.quotation.common.utility.CommonUtility" %>

<table width="80%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
	<tr>
		<td height="100%" valign="top">
			<div>&nbsp;</div>
	  		<div class="bulkloadsectiontitle"><bean:message key="quotation.label.admin.field.customer.discount" /> <bean:message key="quotation.label.admin.bulkupload.reportheader" /></div>
	  		<br>
	  		
	  		<table width="100%" border="0" align="center" cellpadding="3" cellspacing="3">
	  			<tr>
					<td>
						<fieldset>
							<legend class="bulkloadblueheader">Overall Result</legend>
							<br>
							<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
								<logic:present name="<%=QuotationConstants.APP_ERRORMESSAGE %>" scope="request">
									<tr>
										<td colspan="2" class="message"> error:
											<bean:write name="<%=QuotationConstants.APP_ERRORMESSAGE%>" scope="request" filter="false" />
										</td>
									</tr>
								</logic:present>
								<tr>
									<logic:present name="recordCount" scope="request">
										<td width="50%" class="bulkloadformLabelview"> Total records in input files </td>
										<td class="bulkloadformContentview">
											<bean:write name="recordCount" scope="request" />
											&nbsp;
										</td>
									</logic:present>
								</tr>
								<tr>
									<logic:present name="insertedRecordsCount" scope="request">
										<td class="bulkloadformLabelview"> Total records inserted in Customer Discount </td>
										<td class="bulkloadformContentview">
											<bean:write name="insertedRecordsCount" scope="request" />
											&nbsp;
										</td>
									</logic:present>
								</tr>
								<tr>
									<logic:present name="existingsRecordsCount" scope="request">
										<td class="bulkloadformLabelview"> Total records not processed because they already exists in Database </td>
										<td class="bulkloadformContentview">
											<bean:write name="existingsRecordsCount" scope="request" />
											&nbsp;
										</td>
									</logic:present>
								</tr>
								<tr>
									<logic:present name="invalidRecordsCount" scope="request">
										<td class="bulkloadformLabelview"> Total records not processed because of incorrect format </td>
										<td class="bulkloadformContentview">
											<bean:write name="invalidRecordsCount" scope="request" />
											&nbsp;
										</td>
									</logic:present>
								</tr>
								<tr> <td class="bulkloadformLabelview"> <br> &nbsp; <br>  </td> </tr>
								<tr>
				            		<td class="bulkloadformLabelview" align="center">
			            				<logic:present name="adminSuccessMessage" scope="request">
			            					<strong> <bean:write name="adminSuccessMessage" filter="false" /> </strong>
			            				</logic:present>
				            		</td>
				            		<td style="width:30%">
				            			<table class="other">
					            			<tr>
					            				<logic:present name="backToSearchUrl" scope="request">
					            					<td>
					            					<logic:present name="backToSearchMsg" scope="request">
					            						<a href="<bean:write name="backToSearchUrl" />"><bean:write name="backToSearchMsg" scope="request" /> 
					            						<img src="html/images/back.gif" width="16" height="17" border="0" align="absmiddle"></a>
					            					</logic:present>
					            					<logic:notPresent name="backToSearchMsg" scope="request">
					            						<a href="<bean:write name="backToSearchUrl" />">Return to Search 
					            						<img src="html/images/back.gif" width="16" height="17" border="0" align="absmiddle"></a>
					            					</logic:notPresent>
					            					</td>
					            				</logic:present>
						         			</tr>
							            </table>
				         			</td>
				         		</tr>
							</table>
						</fieldset>
					</td>s
				</tr>
	  		</table>
		</td>
	</tr>
</table>