<%@ include file ="../../common/library.jsp"%>
<%@ include file="../../common/nocache.jsp" %>
<%@ taglib uri="/ajax-tag" prefix="ajax" %>

<html:form action="searchcustomer.do" method="post" >
<html:hidden property="invoke" styleId="invoke" value="updateCustomer"/>
<html:hidden property="customerId" styleId="customerId"/>
<html:hidden property="operation" styleId="operation"/>
<input type="hidden" name="existingCustomer" id="existingCustomer"  value="<bean:write name="customerForm" property="customerName"/>">
<input type="hidden" name="<%=org.apache.struts.taglib.html.Constants.TOKEN_KEY%>" value="<%=session.getAttribute(org.apache.struts.action.Action.TRANSACTION_TOKEN_KEY)%>">

<!-------------------------- main table start  --------------------------------->
<table width="80%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
  <tr>
    <td height="100%" valign="top"><br>
      
	    <div class="sectiontitle">
	    	<logic:equal name="customerForm" property="operation"  value="update">
          		<bean:message key="quotation.label.admin.customer.updatecustomer"/>
          	</logic:equal>
          	<logic:notEqual name="customerForm"	property="operation" value="update">
		  		<bean:message key="quotation.label.admin.customer.addnewcustomer"/>
            </logic:notEqual>
        </div>
	    <br>
		<div class="note" align="right" style="width:100%"><font color="red"><bean:message key="quotation.label.manadatory"/></font></div>
        <fieldset>
        	<table  align="center" >
            	<tr>
            		<td height="23" class="formLabel" style="width:210px !important;"><bean:message key="quotation.label.admin.sales.salesengineer"/> <span class="mandatory">&nbsp;</span> </td>
            		<td>
            			<html:select property="salesengineerId" styleId="salesengineerId" styleClass="long">
	                  		<html:option value="0">Select</html:option>
	                  		<logic:present name="customerForm" property="salesengineerList">
	                  			<bean:define name="customerForm" property="salesengineerList" id="salesengineerList" type="java.util.Collection" />
	                  			<html:options name="customerForm" collection="salesengineerList" property="key" labelProperty="value" />
	                  		</logic:present>
	                	</html:select>   
             		</td>
             		<td style="width:55px !important;"> &nbsp; </td>
             		<td height="23" class="formLabel" style="width:210px !important;">Sales Group <span class="mandatory">&nbsp;</span> </td>
             		<td>
             			<html:select property="customerSalesGroup" styleId="customerSalesGroup" styleClass="long">
             				<html:option value="0">Select</html:option>
             				
             			</html:select>
             		</td>
            	</tr>
            	<tr>
              		<td height="23" class="formLabel" style="width:210px !important;"><bean:message key="quotation.label.admin.customer.customer"/> <span class="mandatory">&nbsp;</span> </td>
		      		<td>
		      			<input id="customerName" name="customerName" maxlength="150" type="text" size="29" class="form-autocomplete long" autocomplete="off" value="<bean:write name="customerForm" property="customerName"/>"/>
		      			<html:hidden property="customer" styleId="customer"/>
		      			<label class="working"> <img src="html/images/loading.gif" border="0" align="absmiddle" style="cursor:hand "> </label> 
			  		</td>
		      		<td rowspan="6" class="dividingdots" style="width:50px ">&nbsp;</td>
		      <td rowspan="6" class="formLabel" style="padding-left:10px;"><bean:message key="quotation.label.admin.customer.address"/></td>
		      <td rowspan="6" >
		      	<html:textarea property="address" styleId="address" onchange = "javascript:checkMaxLength(this.id, 500);" style="width:220px; height:150px;" />
		      </td>
			</tr>
			<tr>
			<td height="23" class="formLabel"><bean:message key="quotation.search.customer"/> Type</td>
			  <td><span class="mandatory">&nbsp;</span><html:text property="customerType" styleId="customerType" maxlength="150" styleClass="long"/></td>
			</tr>
			<tr>
			<td height="23" class="formLabel"><bean:message key="quotation.label.admin.field.industry"/></td>
			  <td><span class="mandatory">&nbsp;</span><html:text property="industry" styleId="industry" maxlength="150" styleClass="long"/></td>
			</tr>
			<tr>
			<td height="23" class="formLabel"><bean:message key="quotation.label.admin.field.sapcode"/></td>
			  <td>&nbsp;&nbsp;&nbsp;&nbsp;</span><html:text property="sapcode" styleId="sapcode" maxlength="150" styleClass="long"/></td>
			</tr>
			<tr>
			<td height="23" class="formLabel"><bean:message key="quotation.label.admin.field.oraclecode"/></td>
			  <td>&nbsp;&nbsp;&nbsp;&nbsp;</span><html:text property="oraclecode" styleId="oraclecode" maxlength="150" styleClass="long"/></td>
			</tr>
			<tr>
			<td height="23" class="formLabel"><bean:message key="quotation.label.admin.field.gstnumber"/></td>
			  <td>&nbsp;&nbsp;&nbsp;&nbsp;</span><html:text property="gstnumber" styleId="gstnumber" maxlength="150" styleClass="long"/></td>
			</tr>
            <tr>
              <td height="23" class="formLabel"><bean:message key="quotation.label.admin.customer.location"/></td>
              <td><span class="mandatory">&nbsp;</span><html:select property="locationId" styleId="locationId" styleClass="long">
	                  	<html:option value="0">Select</html:option>
	                  	<logic:present name="customerForm" property="locationList">
	                  	<bean:define name="customerForm" property="locationList"
		                	             id="locationList" type="java.util.Collection" />
	                  	<html:options name="customerForm" collection="locationList"
				 				              property="key" labelProperty="value" />
	                  	</logic:present>
	                </html:select>   
                </td>
            </tr>
            <tr>
              <td height="23" class="formLabel"><bean:message key="quotation.label.admin.customer.contactperson"/> </td>
              <td><span class="mandatory">&nbsp;</span><html:text property="contactperson" styleId="contactperson" maxlength="150" styleClass="long"/></td>
            </tr>
            <tr>
              <td height="23" class="formLabel"><bean:message key="quotation.label.admin.customer.email"/></td>
		      <td>&nbsp;&nbsp;&nbsp;&nbsp;<html:text property="email" styleId="email" maxlength="120" styleClass="long"/></td>
			    </tr>
            <tr>
              <td height="23" class="formLabel"><bean:message key="quotation.label.admin.customer.phone"/></td>
		      <td>&nbsp;&nbsp;&nbsp;&nbsp;<html:text property="phone" styleId="phone" maxlength="15" styleClass="long"/></td>
			</tr>
			<tr>
              <td height="23" class="formLabel"><bean:message key="quotation.label.admin.customer.mobile"/></td>
		      <td>&nbsp;&nbsp;&nbsp;&nbsp;<html:text property="mobile" styleId="mobile" maxlength="15" styleClass="long"/></td>
			</tr>
			<tr>
			</tr>
			<tr>
			<td height="23" class="formLabel"><bean:message key="quotation.label.admin.field.state"/></td>
			  <td>&nbsp;&nbsp;&nbsp;&nbsp;</span><html:text property="state" styleId="state" maxlength="150" styleClass="long"/></td>
			</tr>
			<tr>
			<td height="23" class="formLabel"><bean:message key="quotation.label.admin.field.country"/></td>
			  <td>&nbsp;&nbsp;&nbsp;&nbsp;</span><html:text property="country" styleId="country" maxlength="150" styleClass="long"/></td>
			</tr>
           <%--  <tr>
              <td height="23" class="formLabel"><bean:message key="quotation.label.admin.customer.fax"/></td>
		      <td>&nbsp;&nbsp;&nbsp;&nbsp;<html:text property="fax" styleId="fax" maxlength="15" styleClass="long"/></td>
			    </tr> --%>
            </table>
		    <br>
          <div class="blue-light" align="right">
          	<logic:equal name="customerForm" property="operation" value="update">
          	    <%--TODO <html:reset property="" styleClass="BUTTON" value="Reset" /> --%>
          		<html:submit value="Update" styleClass="BUTTON" onclick="javascript:return validateCustomer('update');" />
          	</logic:equal>
          	<logic:notEqual name="customerForm"	property="operation" value="update">
			  	<html:button property="" styleClass="BUTTON" value="Clear" onclick="javascript:clearCustomerForm();" />
	            <html:submit value="Add" styleClass="BUTTON" onclick="javascript:return validateCustomer('insert');" />
            </logic:notEqual>

        </div>
	    </fieldset>
      </form>    </td>
  </tr>
</table>
<!-------------------------- main table End  --------------------------------->
</html:form>

<!-- AUTOCOMPLETE CODE FOR FILLING THE CUSTOMER NAME IN CUSTOMER TEXT FIELD- TO BE REMOVED LATER -->

<ajax:autocomplete
				  fieldId="customerName"
				  popupId="customer-popup"
				  targetId="customer"
				  baseUrl="servlet/AutocompleteServlet?type=customer"
				  paramName="customer"
				  className="autocomplete"
				  progressStyle="throbbing" />

	<iframe
		 id="divShim"
		src="javascript:false;"
		scrolling="no"
		frameborder="0"
		style="position:absolute; top:0px; left:0px; display:none;">
	</iframe>
	<script type="text/javascript">
		function checkMaxLength(textAreaId, maxlength){	
		var taValue = document.getElementById(textAreaId).value;
		if(taValue.length>maxlength){
			document.getElementById(textAreaId).value = taValue.substring(0, maxlength);
		}
	}
		</script>
	
<!--  END  -->