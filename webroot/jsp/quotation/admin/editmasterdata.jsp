<html>
<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>

<!--main content  table begins-->
<table width="80%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
<tr>
  <td height="100%" valign="top">  
    <html:form  action="mastersdata.do"  method="post" >
 		<html:hidden property="invoke" value="addorUpdateMasterData"/>
 		<html:hidden property="addOrUpdate" value="add"/>
 		<html:hidden name="MastersDataForm" property="masterId" styleId="masterId"/>
 		<html:hidden name="MastersDataForm" property="master" styleId="masterName"  />
 		
 		<input type="hidden" name="<%=org.apache.struts.taglib.html.Constants.TOKEN_KEY%>" value="<%=session.getAttribute(org.apache.struts.action.Action.TRANSACTION_TOKEN_KEY)%>">
 		
 <br>
      <div class="sectiontitle">
      <logic:match name="sPage" value="addMastersData" scope="request">
      	Add&nbsp;<bean:write name="MastersDataForm" property="master"/>
      </logic:match>
      <logic:match name="sPage" value="editMastersData" scope="request">
      	Edit&nbsp;<bean:write name="MastersDataForm" property="master"/>
      </logic:match>
         		
	  </div>
  	  <br>
	  <div class="note" align="right" style="width:100%">
	  <font color="red">
        	<bean:message key="quotation.label.manadatory"/>
        	</font>
		</div>
      <fieldset>
        
		<table border="0" align="center" cellpadding="2" cellspacing="2">
			
	    	<tr>
  				<html:hidden name="MastersDataForm"  property="engineeringId" styleClass="normal" />
			    <td class="formLabel" >
			    <bean:write name="MastersDataForm" property="master" />
			    <span class="mandatory">&nbsp;</span>
			    </td>
                <td>
                	<html:text  name="MastersDataForm" property="engineeringName" maxlength="150" styleId="engineeringName" styleClass="normal" />
            <input type="hidden" name="editmastervalue" id="editmastervalue" value="<bean:write name="MastersDataForm" property="engineeringName"/>">
                	
                </td>
                        
            </tr>
		                    	     
        
		</table>
  		<br>
        
        <div class="blue-light" align="right">
        <logic:match name="sPage" value="addMaster" scope="request">
        <html:reset value="Clear" styleId="BUTTON" styleClass="BUTTON" />
      		 <input name="add" type="button" class="BUTTON" id="add" value="Add" onClick="addorupdateEngineeringData('add');">
      	</logic:match>
      	<logic:match name="sPage" value="editMaster" scope="request">
      		 <input name="update" type="button" class="BUTTON" id="update" value="Submit" onClick="addorupdateEngineeringData('update');">
      	</logic:match>
        
         </div>
	  </fieldset>
    </html:form>    </td>
</tr>
</table>
<!--main content  table end-->
</html>
