<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants" %>
<%@ page import="in.com.rbc.quotation.common.utility.PropertyUtility" %>
<%@ page import="in.com.rbc.quotation.common.utility.CommonUtility" %>
<%@ include file ="../../common/library.jsp"%>
<%@ include file="../../common/nocache.jsp" %>
<%@ taglib uri="/ajax-tag" prefix="ajax" %>

<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<html:form action="searchcustomer.do" method="post" enctype="multipart/form-data">
<html:hidden property="invoke" styleId="invoke"/>
<html:hidden property="customerId" styleId="customerId" />
<html:hidden property="operation" styleId="operation"/>

<input type="hidden" name="<%=org.apache.struts.taglib.html.Constants.TOKEN_KEY%>" value="<%=session.getAttribute(org.apache.struts.action.Action.TRANSACTION_TOKEN_KEY)%>">

<!-------------------------- main table start  --------------------------------->
<table width="80%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
	<tr>
		<td height="100%" valign="top">
			<br>
		   	<!-------------------------- Title & Search By Block start  --------------------------------->
	      	<div class="sectiontitle"><bean:message key="quotation.label.admin.customer.searchcustomer"/> </div>
	      	<br>
	
	      	<fieldset>
	        	<legend class="blueheader"><bean:message key="quotation.label.admin.searchby"/></legend>
	        	<table style="width:1100px !important;" border="0" align="center" cellpadding="0" cellspacing="2">
	        		<tr>
		            	<td colspan="2">&nbsp;</td>
		          	</tr>
			    	<tr>
			    		<td class="formLabel" style="width:400px !important; text-align:right;"> <bean:message key="quotation.label.admin.customer.customer"/> </td>
			            <td style="width:600px !important; text-align:left;">
							<input id="customerName" name="customerName" class="normal" type="text" size="29"  autocomplete="off" maxlength="150" value="<bean:write name="customerForm" property="customerName" />" />
			            	<html:hidden property="customer" styleId="customer"/>
			            	<label class="working"> <img src="html/images/loading.gif" border="0" align="absmiddle" style="cursor:hand "> </label>  
			            </td>
					</tr>
				</table>
	        	<br>
	        	<div class="blue-light" align="right">
					<html:button property="" styleClass="BUTTON" value="Clear" onclick="javascript:resetCustomerSearch();" />
	        		<html:button property="" styleClass="BUTTON" value="Search" onclick="javascript:searchCustomer();" />
	        	</div>
	        </fieldset>
	      	<br>
		  	<!-------------------------- Title & Search By Block End  --------------------------------->
		  	 
			<!-------------------------- Search Results Block start  --------------------------------->
		    <div style="width:1100px !important;" class="sectiontitle"><bean:message key="quotation.search.searchresults" /></div>
			<br>
			<%-- Listing taglib's hidden fields tag that includes the necessary hidden fields related to pagination, at runtime. --%>
			<listing:hiddenfields />
			<%-- Displaying 'No Results Found' message if size of list is zero --%>
			
			<logic:equal name="sizeOfList" value="0">
				<table style="width:1100px !important;" border="0" cellspacing="0" cellpadding="0" bgcolor="#F7F7F7">
					<tr class="other" align="center">
						<td><bean:message key="quotation.label.admin.noresultsfound"/></td>
					</tr>
				</table>
			</logic:equal>
				 
			<logic:notEqual name="sizeOfList" value="0">
				<logic:notPresent name="oCustomerDto" property="searchResultsList" scope="request">
					<table style="width:1100px !important;" border="0" cellspacing="0" cellpadding="0" bgcolor="#F7F7F7">
						<tr align="center" class="other">
							<td><bean:message key="quotation.label.admin.noresultsfound"/></td>
						</tr>
					</table>
				</logic:notPresent>
				<logic:present name="oCustomerDto" property="searchResultsList" scope="request">
					<logic:empty name="oCustomerDto" property="searchResultsList">
						<table style="width:1100px !important;" border="0" cellspacing="0" cellpadding="0" bgcolor="#F7F7F7">
							<tr align="center" class="other">
									<td><bean:message key="quotation.label.admin.noresultsfound" /></td>
							</tr>
						</table>
					</logic:empty>
				 	<logic:notEmpty name="oCustomerDto" property="searchResultsList">
				 		<table style="width:1120px !important;" border="0" cellspacing="0" cellpadding="0" class="blue-light">
			       			<tr>
								<%-- 
									Listing taglib's listingsummary field tag, which displays the summary of records 
									retrieved from database and those being displayed in this page
							 	--%>
								<td align="left" bgcolor="#CCD8F2"><listing:listsummary /></td>
								<%-- 
									Listing taglib's pagelist field tag, which displays the list of pages in dropdown 
									selecting which, user will be redirected to the respective page
						 		--%>
								<td align="right" bgcolor="#CCD8F2"><listing:pagelist navigation="1" /></td>
							</tr>
				  		</table>	
				  		<br>
	    
						<table style="width:1100px !important;" cellpadding="2"cellspacing="0" id="table0" >
							<tr id="top"> 
					       		<td class="bordertd"> <listing:sort dbfieldid="1" > <bean:message key="quotation.label.admin.customer.customer"/> </listing:sort> </td>   
					        	<td class="bordertd"> <listing:sort dbfieldid="2" > <bean:message key="quotation.label.admin.customer.contactperson"/> </listing:sort> </td>
					      		<td class="bordertd"> <listing:sort dbfieldid="3" > <bean:message key="quotation.label.admin.customer.location"/> </listing:sort> </td>
					        	<td class="bordertd"> <listing:sort dbfieldid="4" > <bean:message key="quotation.label.admin.customer.email"/> </listing:sort> </td>
					        	<td class="bordertd"> <listing:sort dbfieldid="5" > <bean:message key="quotation.label.admin.customer.phone"/> </listing:sort> </td>
					        	<td class="bordertd"><bean:message key="quotation.label.admin.sales.salesengineer"/></td>
					        	<td class="bordertd"><bean:message key="quotation.label.admin.customer.salesgroup"/></td>
					        	<td class="bordertd"><bean:message key="quotation.label.admin.customer.customerrsm"/></td>
					        	<td class="bordertd"><bean:message key="quotation.search.customer"/> Type</td>
					        	<td class="bordertd"><bean:message key="quotation.label.admin.customer.customerclass"/></td>
					        	<td class="bordertd"><bean:message key="quotation.label.admin.customer.customerindustry"/></td>
					        	<td class="bordertd"><bean:message key="quotation.label.admin.field.sapcode"/></td>
					        	<td class="bordertd"><bean:message key="quotation.label.admin.field.oraclecode"/></td>
					        	<td class="bordertd"><bean:message key="quotation.label.admin.field.gstnumber"/></td>
					        	<td class="bordertd"><bean:message key="quotation.label.admin.field.state"/></td>
					        	<td class="bordertd"><bean:message key="quotation.label.admin.field.country"/></td>
					        
					        	<td  style="width:5%;" align="center"> Edit </td>
					        	<td  style="width:5%;"> Delete </td>
				        	</tr>
				        	<logic:iterate name="oCustomerDto" property="searchResultsList" id="customerDetails" indexId="idx" type="in.com.rbc.quotation.admin.dto.CustomerDto">
					        	<% if(idx.intValue() % 2 == 0) { %>
									<tr class="light" id='item<bean:write name="idx"/>' onmouseover="Change(this.id)" onMouseOut="ChangeBack(this.id)">
								<% } else { %>
									<tr class="dark" id='item<bean:write name="idx"/>' onmouseover="Change(this.id)" onMouseOut="ChangeBack(this.id)">
					   			<% } %>	
						   		  		<td class="bordertd"><bean:write name="customerDetails" property="customer"/></td>		  
									  	<td class="bordertd"><bean:write name="customerDetails" property="contactperson"/></td>		
									  	<td class="bordertd"><bean:write name="customerDetails" property="locationname"/></td>	
									  	<td class="bordertd"><bean:write name="customerDetails" property="email"/></td>
									  	<td class="bordertd"><bean:write name="customerDetails" property="phone"/></td>
								  		<td class="bordertd"><bean:write name="customerDetails" property="salesengineer"/></td>	
								  		<td class="bordertd"><bean:write name="customerDetails" property="customerSalesGroup"/></td>
								  		<td class="bordertd"><bean:write name="customerDetails" property="customerRSM"/></td>	
								  		<td class="bordertd"><bean:write name="customerDetails" property="customerType"/></td>
								  		<td class="bordertd"><bean:write name="customerDetails" property="customerClass"/></td>	
								  		<td class="bordertd"><bean:write name="customerDetails" property="industry"/></td>
								  		<td class="bordertd"><bean:write name="customerDetails" property="sapcode"/></td>
								  		<td class="bordertd"><bean:write name="customerDetails" property="oraclecode"/></td>		
								  		<td class="bordertd"><bean:write name="customerDetails" property="gstnumber"/></td>	
								  		<td class="bordertd"><bean:write name="customerDetails" property="state"/></td>		
								  		<td class="bordertd"><bean:write name="customerDetails" property="country"/></td>	
																	  
						          		<td><div align="center"><img src="html/images/edit.gif" alt="Edit" width="20" height="18" border="0" style="cursor:hand " onClick="javascript:editCustomer('<bean:write name="customerDetails" property="customerId"/>')"></div></td>
						          		<td align="center"><img src="html/images/deleteicon.gif" alt="Delete" width="15" height="16" border="0" style="cursor:hand " onClick="javascript:deleteCustomer('<bean:write name="customerDetails" property="customerId"/>', '<bean:write name="customerDetails" property="customer"/>')"></td>
					        		</tr>
				        	</logic:iterate>
				      	</table>
					
				      	<table class="blue-light" style="width:1120px !important;" cellpadding="0" cellspacing="0" border="0">
							<tr>
								<%-- 
									Listing taglib's listingsummary field tag, which displays the summary of records 
									retrieved from database and those being displayed in this page
								--%>
								<td align="left" bgcolor="#CCD8F2"><listing:listsummary /></td>
								<%-- 
									Listing taglib's pagelist field tag, which displays the list of pages in dropdown 
									selecting which, user will be redirected to the respective page
								--%>
								<td align="right" bgcolor="#CCD8F2"><listing:pagelist navigation="2" /></td>
							</tr>
					  	</table>	
					  	<br>
					  	
					  	<table class="blue-light" style="width:1120px !important;" cellpadding="0" cellspacing="0" border="0">
				        	<tr>
				          		<td> <html:button property="" value="Add New Customer" styleClass="BUTTON" onclick="javascript:addNewCustomer();"/> </td>
				          		<td align="right">
				          			<!-- <html:button property="" value="Print" styleClass="BUTTON" onclick="javascript:printCustomerResult();"/> -->
				          			<html:button property="" value="Export to Excel" styleClass="BUTTON" onclick="javascript:exportCustomerResult();"/>
				          		</td>
				        	</tr>
						</table>
						
					</logic:notEmpty>
				</logic:present>
			</logic:notEqual>
			
			<!-------------------------------------- search results block end --------------------------->
	        
			<logic:equal name="sizeOfList" value="0">
				<table class="blue-light" style="width:1100px !important" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td> <html:button property="" value="Add New Customer" styleClass="BUTTON" onclick="javascript:addNewCustomer();"/> </td>
					</tr>
				</table>
			</logic:equal>
			     
			</td>
		</tr>
	</table>
</html:form>

<!-- AUTOCOMPLETE CODE FOR FILLING THE CUSTOMER NAME IN CUSTOMER TEXT FIELD- TO BE REMOVED LATER -->

<ajax:autocomplete
	fieldId="customerName"
	popupId="customer-popup"
	targetId="customer"
	baseUrl="servlet/AutocompleteServlet?type=customer"
	paramName="customer"
	className="autocomplete"
	progressStyle="throbbing" />

<iframe id="divShim"
		src="javascript:false;"
		scrolling="no"
		frameborder="0"
		style="position:absolute; top:0px; left:0px; display:none;">
</iframe>

<!--  END  -->