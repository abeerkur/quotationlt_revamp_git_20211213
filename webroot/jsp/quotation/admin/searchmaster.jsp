<%@ include file ="../../common/library.jsp"%>
<%@ include file="../../common/nocache.jsp" %>
<html:form action="masters.do" method="post" >
<html:hidden property="invoke" value="viewSearchMasterList"/>
<html:hidden property="masterId" styleId="masterId" value=""/>


 		<input type="hidden" name="<%=org.apache.struts.taglib.html.Constants.TOKEN_KEY%>" value="<%=session.getAttribute(org.apache.struts.action.Action.TRANSACTION_TOKEN_KEY)%>">
	<table width="75%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
		<tr>
			<td height="100%" valign="top">
				
					<script language="javascript" src="html/js/tooltip.js"></script>
				
				<div class="sectiontitle"><bean:message key="quotation.admin.searchmaster"/></div><br>
				<!------------------------------search block Start ----------------------------------->
				<fieldset>
					<legend class="blueheader"><bean:message key="quotation.admin.searchby"/></legend>
					<table  border="0" align="center" cellpadding="2" cellspacing="5">
						<tr>
							<td>&nbsp;&nbsp;</td>
							<td class="formLabel"><bean:message key="quotation.admin.master" /></td>
							<td>
								<html:text property="master" styleId="master"  maxlength="100" styleClass="long"/>
							</td>
							
						</tr>
				</table>
				<div class="blue-light" align="right">
					<html:submit property="" value="Clear"  styleClass="BUTTON" onclick="clearMasterSearch();"/>
				  		<html:submit property="" value="Search" styleClass="BUTTON" onclick="return validateMasters();"/>
				  </div>
				</fieldset>
				
					<br>
				
	<div class="sectiontitle"><bean:message key="quotation.admin.mastersearchresults"/></div><br>
	
	<logic:notEqual name="sizeOfList" value="0">	
  	 	
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="blue-light" >
	<tr>
	<%-- 
	Listing taglib's listingsummary field tag, which displays the
	summary of records retrieved from database and those being
	displayed in this page
	 --%>
	<td align="left" bgcolor="#CCD8F2"><listing:listsummary /></td>
				
	<%-- 
	Listing taglib's pagelist field tag, which displays the list 
	of pages in dropdown selecting which, user will be redirected 
	to the respective page
	 --%>
	<td align="right" bgcolor="#CCD8F2"><listing:pagelist navigation="1" /></td>
	</tr>
</table><br />

	</logic:notEqual>
<table width="100%" cellpadding="1" cellspacing="0" id="table0"  align="center">
	
	<%-- 
	Listing taglib's hidden fields tag that includes the necessary hidden fields
	related to pagination, at runtime
	 --%>
	<listing:hiddenfields />
	
  <%-- Displaying 'No Results Found' message if size of list is zero --%>
			<logic:equal name="sizeOfList" value="0">				
				<table width="100%" cellspacing="0" bgcolor="#F7F7F7">
					<tr class="color3" align="center">
						<td class="other" style="text-align:center"><bean:message key="quotation.messages.noresultsfound"/><br />
</td>
					</tr>
				</table>
			</logic:equal>

			<logic:notEqual name="sizeOfList" value="0">
			 <tr id="top">
                
                <td  align="center" width="15%">
                	<listing:sort dbfieldid="1"  >
	                			<bean:message key="quotation.admin.masterid"/>								
				    </listing:sort> 
		       </td>
				    <td  align="center">
				    	<listing:sort dbfieldid="2" >
	                			<bean:message key="quotation.admin.master"/>								
				    </listing:sort> 
				</td>
				 
				<td  style="width:5%;text-align:center">Edit</td>
				<td  style="width:5%;text-align:center">Delete</td>
			 </tr>
	
	<logic:iterate name="resultsList" scope="request" id="searchMasterList"
				   indexId="idx" type="in.com.rbc.quotation.common.vo.KeyValueVo">
	
	 <% if(idx.intValue() % 2 == 0) { %>
		<tr nowrap class="light" id='item<bean:write name="idx"/>'" onmouseover="Change(this.id)" onmouseout="ChangeBack(this.id)">
	<% } else { %>
		<tr class="dark" id='item<bean:write name="idx"/>' onmouseover="Change(this.id)" onmouseout="ChangeBack(this.id)">
	<% } %>
	          	<td class="bordertd"><bean:write name="searchMasterList" property="key"/>
				</td>
              	<td ><a href="javaScript:loadMasterData('<bean:write name="searchMasterList" property="key"/>','<bean:write name="searchMasterList" property="value"/>')"><bean:write name="searchMasterList" property="value"/></a>
				</td>
				<td ><div align="center"><a href="javascript:editMaster('<bean:write name="searchMasterList" property="key"/>');"><img src="html/images/edit.gif" alt="Edit" width="20" height="18" border="0" ></a></div></td>
     			<td ><div align="center"><a href="javascript:deleteMaster('<bean:write name="searchMasterList" property="key"/>','<bean:write name="searchMasterList" property="value"/>');"><img src="html/images/deleteicon.gif" alt="Delete" width="15" height="16" border="0" ></a></div></td>
     </tr>
	</logic:iterate>		

	</table>
	
	
			<!-------------------------------------- search results block end --------------------------->
			<table width="100%" class="blue-light" border="0" cellspacing="0" cellpadding="0">
	<tr>
	<%-- 
	Listing taglib's listingsummary field tag, which displays the
	summary of records retrieved from database and those being
	displayed in this page
	 --%>
	<td align="left" bgcolor="#CCD8F2"><listing:listsummary /></td>
				
	<%-- 
	Listing taglib's pagelist field tag, which displays the list 
	of pages in dropdown selecting which, user will be redirected 
	to the respective page
	 --%>
	<td align="right" bgcolor="#CCD8F2"><listing:pagelist navigation="2" /></td>
	</tr>
</table>
			<br />
			</logic:notEqual>
<table width="100%" class="blue-light" border="0" cellspacing="0" cellpadding="0">
				<tr>
				
					<td align="right" style="width:20% ">
						<input name="Add Master"  value="Add New Master" type="button" class="BUTTON" onClick="addMaster()">
					</td>
					<td>&nbsp;</td>
					<logic:notEqual name="sizeOfList" value="0">
					<td style="width:30%; text-align:right;">
						<input name="Print" type="button" class="BUTTON"  onClick="printOrExportMaster('print')" value="Print">
						<input name="Export" type="button" class="BUTTON"  onClick="printOrExportMaster('export')" value="Export To Excel">
					</td>
					</logic:notEqual>
				</tr>
			</table>


 </html:form><br>

 </td>
</tr>
</table>

<!--main content  table end--> 

