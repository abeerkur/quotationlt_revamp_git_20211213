<%@ include file ="../../common/library.jsp"%>
<%@ include file="../../common/nocache.jsp" %>

<table width="80%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
	<tr>
		<td height="100%" valign="top">
			<logic:present name="alAdminSearchResultsList" scope="request">
				<logic:empty name="alAdminSearchResultsList">
					<table width="100%" cellspacing="0" bgcolor="#F7F7F7">
						<tr class="other">
							<td><bean:message key="quotation.label.admin.noresultsfound" /></td>
						</tr>
					</table>
				</logic:empty>
				<logic:notEmpty name="alAdminSearchResultsList">
					<!-------------------------------------- search results block start --------------------------->
					<table width="100%" cellpadding="2" cellspacing="0" border=1>
						<tr>
							<td class="formLabelTop"><b><bean:message key="quotation.label.admin.field.manufacturing.location" /></b></td>
							<td class="formLabelTop"><b><bean:message key="quotation.label.admin.field.product.group" /></b></td>
							<td class="formLabelTop"><b><bean:message key="quotation.label.admin.field.product.line" /></b></td>
							<td class="formLabelTop"><b><bean:message key="quotation.label.admin.field.Standard" /></b></td>
							<td class="formLabelTop"><b><bean:message key="quotation.label.admin.field.Power.Rating_KW" /></b></td>
							<td class="formLabelTop"><b><bean:message key="quotation.label.admin.field.frame.type" /></b></td>
							<td class="formLabelTop"><b><bean:message key="quotation.label.admin.field.frame.suffix" /></b></td>
							<td class="formLabelTop"><b><bean:message key="quotation.label.admin.field.number.of.poles" /></b></td>
							<td class="formLabelTop"><b><bean:message key="quotation.label.admin.field.mounting.type" /></b></td>
							<td class="formLabelTop"><b><bean:message key="quotation.label.admin.field.tB.position" /></b></td>
							<td class="formLabelTop"><b><bean:message key="quotation.label.admin.field.list.price" /></b></td>
							<td class="formLabelTop"><b><bean:message key="quotation.label.admin.field.material.cost" /></b></td>
							<td class="formLabelTop"><b><bean:message key="quotation.label.admin.field.loh" /></b></td>
						</tr>
						<logic:iterate name="alAdminSearchResultsList" id="listPriceDetails" indexId="idx" type="in.com.rbc.quotation.admin.dto.ListPriceDto">
							<tr>
								<td class="bordertd"><bean:write name="listPriceDetails" property="manufacturing_location" />&nbsp;</td>
								<td class="bordertd"><bean:write name="listPriceDetails" property="product_group" />&nbsp;</td>
								<td class="bordertd"><bean:write name="listPriceDetails" property="product_line" />&nbsp;</td>
																
								<logic:equal name="listPriceDetails" property="standard" value="Y">
									<td class="bordertd"><bean:message key="quotation.label.yes" /></td>
								</logic:equal>
								<logic:equal name="listPriceDetails" property="standard" value="N">
									<td class="bordertd"><bean:message key="quotation.label.no" /></td>
								</logic:equal>
								
								<td class="bordertd"><bean:write name="listPriceDetails" property="power_rating_kw" />&nbsp;</td>
								<td class="bordertd"><bean:write name="listPriceDetails" property="frame_type" />&nbsp;</td>
								<td class="bordertd"><bean:write name="listPriceDetails" property="frame_suffix" />&nbsp;</td>
								<td class="bordertd"><bean:write name="listPriceDetails" property="number_of_poles" />&nbsp;</td>
								<td class="bordertd"><bean:write name="listPriceDetails" property="mounting_type" />&nbsp;</td>
								<td class="bordertd"><bean:write name="listPriceDetails" property="tb_position" />&nbsp;</td>
								<td class="bordertd"><bean:write name="listPriceDetails" property="list_price" />&nbsp;</td>
								<td class="bordertd"><bean:write name="listPriceDetails" property="material_cost" />&nbsp;</td>
								<td class="bordertd"><bean:write name="listPriceDetails" property="loh" />&nbsp;</td>
							</tr>
						</logic:iterate>
					</table>
				</logic:notEmpty>
			</logic:present>
			
		</td>
	</tr>
</table>