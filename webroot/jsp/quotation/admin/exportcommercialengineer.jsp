<%@ include file ="../../common/library.jsp"%>
<%@ include file="../../common/nocache.jsp" %>

<table width="80%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
  <tr>
    <td height="100%" valign="top">
     <logic:present name="alAdminSearchResultsList" scope="request">
	 <logic:empty name="alAdminSearchResultsList">
	 	<table width="100%" cellspacing="0" bgcolor="#F7F7F7">
			<tr class="other">
				<td><bean:message key="quotation.label.admin.noresultsfound"/></td>
			</tr>
		</table>
	 </logic:empty>
	 <logic:notEmpty name="alAdminSearchResultsList">	 
      <!-------------------------------------- search results block start --------------------------->
      <table width="100%" cellpadding="2" cellspacing="0" border=1>
        <tr>        	   
	        <td class="formLabelTop">
				<b><bean:message key="quotation.label.admin.commercialengineer"/></b>
			</td>
        </tr>
       <logic:iterate name="alAdminSearchResultsList" 
					         id="designEngineerDetails" indexId="idx"
					         type="in.com.rbc.quotation.admin.dto.CommercialEngineerDto">
	       	<tr>	   		   	
	   		   <td class="bordertd"><bean:write name="designEngineerDetails" property="designEngineerName"/>&nbsp;</td>			  
	        </tr>
        </logic:iterate>
      </table>
	  </logic:notEmpty>
	  </logic:present>
    </td>
  </tr>
</table>