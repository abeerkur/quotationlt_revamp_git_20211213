<%@ include file ="../../common/library.jsp"%>
<%@ include file="../../common/nocache.jsp" %>
<!-------------------------- main table Start  --------------------------------->
<html:form action="/technicaldata.do" method="post" onsubmit="return validateaddTechnicalData()">
<html:hidden property="invoke" value="updateTechnicalData"/>
<html:hidden property="operation" styleId="operation"/>
<html:hidden property="SERIAL_ID" styleId="SERIAL_ID"/>

<table width="80%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
  <tr>
    <td height="100%" valign="top"><br>
    <logic:equal name="technicalDataForm" property="operation" value="update">
        <div class="sectiontitle">Update Data </div>
     </logic:equal>   
     <logic:notEqual name="technicalDataForm"	property="operation" value="update">
        <div class="sectiontitle">Add New Data </div>
        </logic:notEqual>
         <br>
		<div class="note" align="right" style="width:100%">
        	<font color="red"><bean:message key="quotation.label.manadatory"/></font>
		</div>
        <fieldset>
          <table  align="center" >
            <tr>
              	<td class="formLabel"><bean:message key="quotation.label.admin.field.manufacturing.location"/><span class="mandatory">&nbsp;</span></td>
            	<td ><html:text property="MANUFACTURING_LOCATION" styleId="MANUFACTURING_LOCATION" styleClass="normal"  maxlength="100" /> </td>
            	
            	<td class="formLabel"><bean:message key="quotation.label.admin.field.product.group"/><span class="mandatory">&nbsp;</span></td>
            	<td ><html:text property="PRODUCT_GROUP" styleId="PRODUCT_GROUP" styleClass="normal"  maxlength="100" /> </td>
		    </tr>
            <tr>
            
             	<td height="23" class="formLabel"><bean:message key="quotation.label.admin.field.product.line"/><span class="mandatory">&nbsp;</span></td>
             	<%-- <td><html:select property="PRODUCT_GROUP" styleId="PRODUCT_GROUP" styleClass="normal"><html:option value="0">Select</html:option></html:select></td> --%>
                <td><html:text property="PRODUCT_LINE" styleId="PRODUCT_LINE" styleClass="normal"  maxlength="100" /> </td> 
            
             <td class="formLabel"><bean:message key="quotation.label.admin.field.Standard"/><span class="mandatory">&nbsp;</span></td>
             <td><html:text property="STANDARD" styleId="STANDARD" styleClass="normal"  maxlength="100" /> </td>
		     </tr>
		     
		     <tr>
              	<td class="formLabel"><bean:message key="quotation.label.admin.field.Power.Rating_KW"/><span class="mandatory">&nbsp;</span></td>
            	<td ><html:text property="POWER_RATING_KW" styleId="POWER_RATING_KW" styleClass="normal"  maxlength="100" /> </td>
            	
            	<td class="formLabel"><bean:message key="quotation.label.admin.field.frame.type"/><span class="mandatory">&nbsp;</span></td>
            	<td ><html:text property="FRAME_TYPE" styleId="FRAME_TYPE" styleClass="normal"  maxlength="100" /> </td>
		    </tr>
		    
		    <tr>
              	<td class="formLabel"><bean:message key="quotation.label.admin.field.number.of.poles"/><span class="mandatory">&nbsp;</span></td>
            	<td ><html:text property="NUMBER_OF_POLES" styleId="NUMBER_OF_POLES" styleClass="normal"  maxlength="100" /> </td>
            	
            	<td class="formLabel"><bean:message key="quotation.label.admin.field.mounting.type"/><span class="mandatory">&nbsp;</span></td>
            	<td ><html:text property="MOUNTING_TYPE" styleId="MOUNTING_TYPE" styleClass="normal"  maxlength="100" /> </td>
		    </tr>
		    
		    <tr>
              	<td class="formLabel"><bean:message key="quotation.label.admin.field.tB.position"/><span class="mandatory">&nbsp;</span></td>
            	<td ><html:text property="TB_POSITION" styleId="TB_POSITION" styleClass="normal"  maxlength="100" /> </td>
            	
            	<td class="formLabel"><bean:message key="quotation.label.admin.field.model.no"/><span class="mandatory">&nbsp;</span></td>
            	<td ><html:text property="MODEL_NO" styleId="MODEL_NO" styleClass="normal"  maxlength="100" /> </td>
		    </tr>
		    
		    <tr>
              	<td class="formLabel"><bean:message key="quotation.label.admin.field.voltage.v"/><span class="mandatory">&nbsp;</span></td>
            	<td ><html:text property="VOLTAGE_V" styleId="VOLTAGE_V" styleClass="normal"  maxlength="100" /> </td>
            	
            	<td class="formLabel"><bean:message key="quotation.label.admin.field.power.rating.hp"/></td>
            	<td ><html:text property="POWER_RATING_HP" styleId="POWER_RATING_HP" styleClass="normal"  maxlength="100" /> </td>
		    </tr>
		    
		    <tr>
              	<td class="formLabel"><bean:message key="quotation.label.admin.field.frequency.hz"/></td>
            	<td ><html:text property="FREQUENCY_HZ" styleId="FREQUENCY_HZ" styleClass="normal"  maxlength="100" /> </td>
            	
            	<td class="formLabel"><bean:message key="quotation.label.admin.field.current.a"/></td>
            	<td ><html:text property="CURRENT_A" styleId="CURRENT_A" styleClass="normal"  maxlength="100" /> </td>
		    </tr>
		    
		    <tr>
              	<td class="formLabel"><bean:message key="quotation.label.admin.field.speed.max"/></td>
            	<td ><html:text property="SPEED_MAX" styleId="SPEED_MAX" styleClass="normal"  maxlength="100" /> </td>
            	
            	<td class="formLabel"><bean:message key="quotation.label.admin.field.power.factor"/></td>
            	<td ><html:text property="POWER_FACTOR" styleId="POWER_FACTOR" styleClass="normal"  maxlength="100" /> </td>
		    </tr>
		    
		    <tr>
              	<td class="formLabel"><bean:message key="quotation.label.admin.field.efficiency"/></td>
            	<td ><html:text property="EFFICIENCY" styleId="EFFICIENCY" styleClass="normal"  maxlength="100" /> </td>
            	
            	<td class="formLabel"><bean:message key="quotation.label.admin.field.electrical.type"/></td>
            	<td ><html:text property="ELECTRICAL_TYPE" styleId="ELECTRICAL_TYPE" styleClass="normal"  maxlength="100" /> </td>
		    </tr>
		    
		    <tr>
              	<td class="formLabel"><bean:message key="quotation.label.admin.field.ip.code"/></td>
            	<td ><html:text property="MANUFACTURING_LOCATION" styleId="MANUFACTURING_LOCATION" styleClass="normal"  maxlength="100" /> </td>
            	
            	<td class="formLabel"><bean:message key="quotation.label.admin.field.insulation.class"/></td>
            	<td ><html:text property="IP_CODE" styleId="IP_CODE" styleClass="normal"  maxlength="100" /> </td>
		    </tr>
		    
		    <tr>
              	<td class="formLabel"><bean:message key="quotation.label.admin.field.service.factor"/></td>
            	<td ><html:text property="SERVICE_FACTOR" styleId="SERVICE_FACTOR" styleClass="normal"  maxlength="100" /> </td>
            	
            	<td class="formLabel"><bean:message key="quotation.label.admin.field.duty"/></td>
            	<td ><html:text property="DUTY" styleId="DUTY" styleClass="normal"  maxlength="100" /> </td>
		    </tr>
		    
		    <tr>
              	<td class="formLabel"><bean:message key="quotation.label.admin.field.number.of.phases"/></td>
            	<td ><html:text property="NUMBER_OF_PHASES" styleId="NUMBER_OF_PHASES" styleClass="normal"  maxlength="100" /> </td>
            	
            	<td class="formLabel"><bean:message key="quotation.label.admin.field.max.ambient.c"/></td>
            	<td ><html:text property="MAX_AMBIENT_C" styleId="MAX_AMBIENT_C" styleClass="normal"  maxlength="100" /> </td>
		    </tr>
		    
		    <tr>
              	<td class="formLabel"><bean:message key="quotation.label.admin.field.de.bearing.size"/></td>
            	<td ><html:text property="DE_BEARING_SIZE" styleId="DE_BEARING_SIZE" styleClass="normal"  maxlength="100" /> </td>
            	
            	<td class="formLabel"><bean:message key="quotation.label.admin.field.de.bearing.type"/></td>
            	<td ><html:text property="DE_BEARING_TYPE" styleId="DE_BEARING_TYPE" styleClass="normal"  maxlength="100" /> </td>
		    </tr>
		    
		    <tr>
              	<td class="formLabel"><bean:message key="quotation.label.admin.field.enclosure.type"/></td>
            	<td ><html:text property="ENCLOSURE_TYPE" styleId="ENCLOSURE_TYPE" styleClass="normal"  maxlength="100" /> </td>
            	
            	<td class="formLabel"><bean:message key="quotation.label.admin.field.motor.orientation"/></td>
            	<td ><html:text property="MOTOR_ORIENTATION" styleId="MOTOR_ORIENTATION" styleClass="normal"  maxlength="100" /> </td>
		    </tr>
		    
		    <tr>
              	<td class="formLabel"><bean:message key="quotation.label.admin.field.frame.material"/></td>
            	<td ><html:text property="FRAME_MATERIAL" styleId="FRAME_MATERIAL" styleClass="normal"  maxlength="100" /> </td>
            	
            	<td class="formLabel"><bean:message key="quotation.label.admin.field.frame.length"/></td>
            	<td ><html:text property="FRAME_LENGTH" styleId="FRAME_LENGTH" styleClass="normal"  maxlength="100" /> </td>
		    </tr>
		    
		    <tr>
              	<td class="formLabel"><bean:message key="quotation.label.admin.field.rotation"/></td>
            	<td ><html:text property="ROTATION" styleId="ROTATION" styleClass="normal"  maxlength="100" /> </td>
            	
            	<td class="formLabel"><bean:message key="quotation.label.admin.field.number.of.speeds"/></td>
            	<td ><html:text property="NUMBER_OF_SPEEDS" styleId="NUMBER_OF_SPEEDS" styleClass="normal"  maxlength="100" /> </td>
		    </tr>
		    
		    <tr>
              	<td class="formLabel"><bean:message key="quotation.label.admin.field.shaft.type"/></td>
            	<td ><html:text property="SHAFT_TYPE" styleId="SHAFT_TYPE" styleClass="normal"  maxlength="100" /> </td>
            	
            	<td class="formLabel"><bean:message key="quotation.label.admin.field.shaft.diameter.mm"/></td>
            	<td ><html:text property="SHAFT_DIAMETER_MM" styleId="SHAFT_DIAMETER_MM" styleClass="normal"  maxlength="100" /> </td>
		    </tr>
		    
		    <tr>
              	<td class="formLabel"><bean:message key="quotation.label.admin.field.shaft.extension.mm"/></td>
            	<td ><html:text property="SHAFT_EXTENSION_MM" styleId="SHAFT_EXTENSION_MM" styleClass="normal"  maxlength="100" /> </td>
            	
            	<td class="formLabel"><bean:message key="quotation.label.admin.field.outline.dwg"/></td>
            	<td ><html:text property="OUTLINE_DWG_NO" styleId="OUTLINE_DWG_NO" styleClass="normal"  maxlength="100" /> </td>
		    </tr>
		    
		    <tr>
              	<td class="formLabel"><bean:message key="quotation.label.admin.field.connection.drawing.no"/></td>
            	<td ><html:text property="CONNECTION_DRAWING_NO" styleId="CONNECTION_DRAWING_NO" styleClass="normal"  maxlength="100" /> </td>
            	
            	<td class="formLabel"><bean:message key="quotation.label.admin.field.overall.length.mm"/></td>
            	<td ><html:text property="OVERALL_LENGTH_MM" styleId="OVERALL_LENGTH_MM" styleClass="normal"  maxlength="100" /> </td>
		    </tr>
		    
		    <tr>
              	<td class="formLabel"><bean:message key="quotation.label.admin.field.starting.type"/></td>
            	<td ><html:text property="STARTING_TYPE" styleId="STARTING_TYPE" styleClass="normal"  maxlength="100" /> </td>
            	
            	<td class="formLabel"><bean:message key="quotation.label.admin.field.thru.bolts.extension"/></td>
            	<td ><html:text property="THRU_BOLTS_EXTENSION" styleId="THRU_BOLTS_EXTENSION" styleClass="normal"  maxlength="100" /> </td>
		    </tr>
		    
		    <tr>
              	<td class="formLabel"><bean:message key="quotation.label.admin.field.type.of.overload.protection"/></td>
            	<td ><html:text property="TYPE_OF_OVERLOAD_PROTECTION" styleId="TYPE_OF_OVERLOAD_PROTECTION" styleClass="normal"  maxlength="100" /> </td>
            	
            	<td class="formLabel"><bean:message key="quotation.label.admin.field.ce"/></td>
            	<td ><html:text property="CE" styleId="CE" styleClass="normal"  maxlength="100" /> </td>
		    </tr>
		    
		    <tr>
              	<td class="formLabel"><bean:message key="quotation.label.admin.field.csa"/></td>
            	<td ><html:text property="CSA" styleId="CSA" styleClass="normal"  maxlength="100" /> </td>
            	
            	<td class="formLabel"><bean:message key="quotation.label.admin.field.ul"/></td>
            	<td ><html:text property="UL" styleId="UL" styleClass="normal"  maxlength="100" /> </td>
		    </tr>
            </table>
		    <br>
		    <logic:notEqual name="technicalDataForm"	property="operation" value="update">
          	<div class="blue-light" align="right">
		  		<html:button property="" styleClass="BUTTON" value="Clear" onclick="javascript:clearTechnicalDataForm();" />
            	<html:submit value="Add" styleClass="BUTTON" />
         </div>
         </logic:notEqual>
         <logic:equal name="technicalDataForm" property="operation" value="update">
        <html:submit value="Update" styleClass="BUTTON" />
     	</logic:equal>
	    </fieldset>
        </td>
  </tr>
</table>

</html:form>