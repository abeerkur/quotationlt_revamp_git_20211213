<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants" %>
<%@ page import="in.com.rbc.quotation.common.utility.PropertyUtility" %>
<%@ page import="in.com.rbc.quotation.common.utility.CommonUtility" %>
<%@ include file ="../../common/library.jsp"%>
<%@ include file="../../common/nocache.jsp" %>
<!-------------------------- main table Start  --------------------------------->
<html:form action="/listprice.do" method="post" onsubmit="return validateaddListPrise()">
	<html:hidden property="invoke" value="addupdateListPrice"/>
	<html:hidden property="operation" styleId="operation"/>
	<html:hidden property="serial_id" styleId="serial_id"/>

	<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

	<input type="hidden" name='<%= org.apache.struts.taglib.html.Constants.TOKEN_KEY %>' value='<%= session.getAttribute(org.apache.struts.action.Action.TRANSACTION_TOKEN_KEY) %>' />

	<table width="80%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
	  <tr>
	    <td height="100%" valign="top"><br>
	    <logic:equal name="listPriceForm" property="operation" value="update">
	        <div class="sectiontitle"> <bean:message key="quotation.label.admin.field.list.price"/> - Update Data </div>
	    </logic:equal>   
	    <logic:notEqual name="listPriceForm"	property="operation" value="update">
	        <div class="sectiontitle"> <bean:message key="quotation.label.admin.field.list.price"/> - Add New Data </div>
	    </logic:notEqual>
	        <br>
			<div class="note" align="right" style="width:100%">
	        	<font color="red"><bean:message key="quotation.label.manadatory"/></font>
			</div>
	        <fieldset>
	          <table align="center">
				<tr>
	              	<td class="formLabel" style="width:155px !important;"><bean:message key="quotation.label.admin.field.manufacturing.location"/><span class="mandatory">&nbsp;</span></td>
	            	<td>
	            		<html:select property="manufacturing_location" styleId="manufacturing_location" styleClass="normal">
	            			<html:option value="0">Select</html:option>
			            	<logic:notEmpty name="listPriceForm" property="maufacturingList">
			            	<bean:define name="listPriceForm" property="maufacturingList" id="maufacturingList" type="java.util.Collection" />
			            	<html:options name="listPriceForm" collection="maufacturingList" property="value" labelProperty="value" />
			            	</logic:notEmpty>
	            		</html:select>
	            	</td>
					<td style="width:55px !important;"> &nbsp; </td>       	
	            	<td class="formLabel" style="width:135px !important;"><bean:message key="quotation.label.admin.field.product.group"/><span class="mandatory">&nbsp;</span></td>
	            	<td>
	            		<html:select property="product_group" styleId="product_group" styleClass="normal">
	            			<html:option value="0">Select</html:option>
			            	<logic:notEmpty name="listPriceForm" property="productGroupList">
			            	<bean:define name="listPriceForm" property="productGroupList" id="productGroupList" type="java.util.Collection" />
			            	<html:options name="listPriceForm" collection="productGroupList" property="value" labelProperty="value" />
			            	</logic:notEmpty>
	            		</html:select>
	            	</td>
				</tr>
	            <tr>
	             	<td height="23" class="formLabel" style="width:155px !important;"><bean:message key="quotation.label.admin.field.product.line"/><span class="mandatory">&nbsp;</span></td>
	                <td>
	                	<html:select property="product_line" styleId="product_line" styleClass="normal">
			            	<html:option value="0">Select</html:option>
			            	<logic:notEmpty name="listPriceForm" property="productLineList">
			            	<bean:define name="listPriceForm" property="productLineList" id="productLineList" type="java.util.Collection" />
			            	<html:options name="listPriceForm" collection="productLineList" property="value" labelProperty="value" />
			            	</logic:notEmpty>
	            		</html:select>
	            	</td>
	            	<td style="width:55px !important;"> &nbsp; </td>
	             	<td class="formLabel" style="width:135px !important;"><bean:message key="quotation.label.admin.field.Standard"/></td>
	              	<td>
	              		<html:select property="standard" styleId="standard" styleClass="normal">
	            			<html:option value="0">Select</html:option>
			            	<logic:notEmpty name="listPriceForm" property="standardList">
			            	<bean:define name="listPriceForm" property="standardList" id="standardList" type="java.util.Collection" />
			            	<html:options name="listPriceForm" collection="standardList" property="key" labelProperty="value" />
			            	</logic:notEmpty>
	            		</html:select>
	            	</td>
				</tr>
				<tr>
	              	<td class="formLabel" style="width:155px !important;"><bean:message key="quotation.label.admin.field.Power.Rating_KW"/><span class="mandatory">&nbsp;</span></td>
	            	<td>
	            		<html:select property="power_rating_kw" styleId="power_rating_kw" styleClass="normal">
			            	<html:option value="0">Select</html:option>
			            	<logic:notEmpty name="listPriceForm" property="powerratingkwList">
			            	<bean:define name="listPriceForm" property="powerratingkwList" id="powerratingkwList" type="java.util.Collection" />
			            	<html:options name="listPriceForm" collection="powerratingkwList" property="value" labelProperty="value" />
			            	</logic:notEmpty>
	            		</html:select>
	            	</td>
	            	<td style="width:55px !important;"> &nbsp; </td>
	            	<td class="formLabel" style="width:135px !important;"><bean:message key="quotation.label.admin.field.number.of.poles"/><span class="mandatory">&nbsp;</span></td>
	            	<td>
	            		<html:select property="number_of_poles" styleId="number_of_poles" styleClass="normal">
			            	<html:option value="0">Select</html:option>
			            	<logic:notEmpty name="listPriceForm" property="numberofpoleslist">
			            	<bean:define name="listPriceForm" property="numberofpoleslist" id="numberofpoleslist" type="java.util.Collection" />
			            	<html:options name="listPriceForm" collection="numberofpoleslist" property="value" labelProperty="value" />
			            	</logic:notEmpty>
	            		</html:select>
	            	</td>
				</tr>
			    <tr>
	              	<td class="formLabel" style="width:155px !important;"><bean:message key="quotation.label.admin.field.frame.type"/><span class="mandatory">&nbsp;</span></td>
	            	<td>
	            		<html:select property="frame_type" styleId="frame_type" styleClass="normal">
			            	<html:option value="0">Select</html:option>
			            	<logic:notEmpty name="listPriceForm" property="frametypelist">
			            	<bean:define name="listPriceForm" property="frametypelist" id="frametypelist" type="java.util.Collection" />
			            	<html:options name="listPriceForm" collection="frametypelist" property="value" labelProperty="value" />
			            	</logic:notEmpty>
	            		</html:select>
	            	</td>
	            	<td style="width:55px !important;"> &nbsp; </td>
	            	<td class="formLabel" style="width:135px !important;"><bean:message key="quotation.label.admin.field.frame.suffix"/><span class="mandatory">&nbsp;</span></td>
	            	<td>
	            		<html:select property="frame_suffix" styleId="frame_suffix" styleClass="normal">
	            			<html:option value="0">Select</html:option>
			            	<logic:notEmpty name="listPriceForm" property="framesuffixlist">
			            	<bean:define name="listPriceForm" property="framesuffixlist" id="framesuffixlist" type="java.util.Collection" />
			            	<html:options name="listPriceForm" collection="framesuffixlist" property="value" labelProperty="value" />
			            	</logic:notEmpty>
	            		</html:select>
	            	</td>
			    </tr>
			    <tr>
			    	<td class="formLabel" style="width:155px !important;"><bean:message key="quotation.label.admin.field.mounting.type"/><span class="mandatory">&nbsp;</span></td>
	            	<td>
	            		<html:select property="mounting_type" styleId="mounting_type" styleClass="normal">
			            	<html:option value="0">Select</html:option>
			            	<logic:notEmpty name="listPriceForm" property="mountingyypelist">
			            	<bean:define name="listPriceForm" property="mountingyypelist" id="mountingyypelist" type="java.util.Collection" />
			            	<html:options name="listPriceForm" collection="mountingyypelist" property="value" labelProperty="value" />
			            	</logic:notEmpty>
	            		</html:select>
	            	</td>
	            	<td style="width:55px !important;"> &nbsp; </td>
	              	<td class="formLabel" style="width:135px !important;"><bean:message key="quotation.label.admin.field.tB.position"/><span class="mandatory">&nbsp;</span></td>
	            	<td>
	            		<html:select property="tb_position" styleId="tb_position" styleClass="normal">
			            	<html:option value="0">Select</html:option>
			            	<logic:notEmpty name="listPriceForm" property="tbpositionlist">
			            	<bean:define name="listPriceForm" property="tbpositionlist" id="tbpositionlist" type="java.util.Collection" />
			            	<html:options name="listPriceForm" collection="tbpositionlist" property="value" labelProperty="value" />
			            	</logic:notEmpty>
	            		</html:select>
	            	</td>
			    </tr>
			    <tr>
			    	<td class="formLabel" style="width:155px !important;"><bean:message key="quotation.label.admin.field.list.price"/>&nbsp;</td>
	            	<td>
	            		<html:text property="list_price" styleId="list_price" styleClass="normal"  maxlength="100" /> 
	            	</td>
	            	<td style="width:55px !important;"> &nbsp; </td>
	              	<td class="formLabel" style="width:135px !important;"><bean:message key="quotation.label.admin.field.material.cost"/>&nbsp;</td>
	            	<td>
	            		<html:text property="material_cost" styleId="material_cost" styleClass="normal"  maxlength="100" /> 
	            	</td>
				</tr>
			    <tr>
			    	<td class="formLabel" style="width:155px !important;"><bean:message key="quotation.label.admin.field.loh"/>&nbsp;</td>
	            	<td>
	            		<html:text property="loh" styleId="loh" styleClass="normal"  maxlength="100" /> 
	            	</td>
	            	<td style="width:55px !important;"> &nbsp; </td>
	            	<td class="formLabel" style="width:135px !important;"> &nbsp; </td>
	            	<td> &nbsp; </td>
			    </tr>
			</table>
			
			<br>
			
			<logic:notEqual name="listPriceForm"	property="operation" value="update">
	          	<div class="blue-light" align="right">
	          		<html:button property="" styleClass="BUTTON" value="Back" onclick="javascript:returnSearchListPrice();" />
			  		<html:button property="" styleClass="BUTTON" value="Clear" onclick="javascript:clearListPriceForm();" />
	            	<html:submit value="Add" styleClass="BUTTON" />
				</div>
	        </logic:notEqual>
	        <logic:equal name="listPriceForm" property="operation" value="update">
	        	<div class="blue-light" align="right">
	        		<html:button property="" styleClass="BUTTON" value="Back" onclick="javascript:returnSearchListPrice();" />
	        		<html:submit value="Update" styleClass="BUTTON" />
	        	</div>
	     	</logic:equal>
	     	
		    </fieldset>
	        </td>
		</tr>
		
	</table>

</html:form>