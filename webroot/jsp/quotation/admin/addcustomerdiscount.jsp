<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants" %>
<%@ page import="in.com.rbc.quotation.common.utility.PropertyUtility" %>
<%@ page import="in.com.rbc.quotation.common.utility.CommonUtility" %>
<%@ include file ="../../common/library.jsp"%>
<%@ include file="../../common/nocache.jsp" %>
<!-------------------------- main table Start  --------------------------------->
<html:form action="/customerdiscount.do" method="post" onsubmit="return validateaddCustomerDiscount()">
	<html:hidden property="invoke" value="addupdateCustomerDiscount"/>
	<html:hidden property="operation" styleId="operation"/>
	<html:hidden property="serial_id" styleId="serial_id"/>

	<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  	
  	<input type="hidden" name='<%= org.apache.struts.taglib.html.Constants.TOKEN_KEY %>' value='<%= session.getAttribute(org.apache.struts.action.Action.TRANSACTION_TOKEN_KEY) %>' />
  	
	<table width="80%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
	  <tr>
	    <td height="100%" valign="top"><br>
	    <logic:equal name="customerDiscountForm" property="operation" value="update">
	        <div class="sectiontitle"> <bean:message key="quotation.label.admin.field.customer.discount"/> - Update Data </div>
	    </logic:equal>   
	    <logic:notEqual name="customerDiscountForm"	property="operation" value="update">
	        <div class="sectiontitle"> <bean:message key="quotation.label.admin.field.customer.discount"/> - Add New Data </div>
	    </logic:notEqual>
	        <br>
			<div class="note" align="right" style="width:100%">
	        	<font color="red"><bean:message key="quotation.label.manadatory"/></font>
			</div>
			<fieldset>
			  <table  align="center" >
	            <tr>
	              	<td class="formLabel" style="width:210px !important;"><bean:message key="quotation.label.admin.field.manufacturing.location"/><span class="mandatory">&nbsp;</span></td>
	            	<td>
	            		<html:select property="manufacturing_location" styleId="manufacturing_location" styleClass="normal">
	            			<html:option value="0">Select</html:option>
			            	<logic:notEmpty name="customerDiscountForm" property="maufacturingList">
			            	<bean:define name="customerDiscountForm" property="maufacturingList" id="maufacturingList" type="java.util.Collection" />
			            	<html:options name="customerDiscountForm" collection="maufacturingList" property="key" labelProperty="value" />
			            	</logic:notEmpty>
	            		</html:select>
	            	</td>
	            	<td style="width:55px !important;"> &nbsp; </td> 
	            	<td class="formLabel" style="width:210px !important;"><bean:message key="quotation.label.admin.field.product.group"/><span class="mandatory">&nbsp;</span></td>
	            	<td>
	            		<html:select property="product_group" styleId="product_group" styleClass="normal">
			            	<html:option value="0">Select</html:option>
			            	<logic:notEmpty name="customerDiscountForm" property="productGroupList">
			            	<bean:define name="customerDiscountForm" property="productGroupList" id="productGroupList" type="java.util.Collection" />
			            	<html:options name="customerDiscountForm" collection="productGroupList" property="key" labelProperty="value" />
			            	</logic:notEmpty>
	            		</html:select>
	            	</td>
			    </tr>
	            <tr>
	             	<td height="23" class="formLabel" style="width:210px !important;"><bean:message key="quotation.label.admin.field.product.line"/><span class="mandatory">&nbsp;</span></td>
	                <td>
	                	<html:select property="product_line" styleId="product_line" styleClass="normal">
			            	<html:option value="0">Select</html:option>
			            	<logic:notEmpty name="customerDiscountForm" property="productLineList">
			            	<bean:define name="customerDiscountForm" property="productLineList" id="productLineList" type="java.util.Collection" />
			            	<html:options name="customerDiscountForm" collection="productLineList" property="key" labelProperty="value" />
			            	</logic:notEmpty>
	            		</html:select>
	            	</td> 
	            	<td style="width:55px !important;"> &nbsp; </td>
	             	<td class="formLabel" style="width:210px !important;"><bean:message key="quotation.label.admin.field.Standard"/><span class="mandatory">&nbsp;</span></td>
	             	<td>
	             		<html:select property="standard" styleId="standard" styleClass="normal">
			            	<html:option value="0">Select</html:option>
			            	<logic:notEmpty name="customerDiscountForm" property="standardList">
			            	<bean:define name="customerDiscountForm" property="standardList" id="standardList" type="java.util.Collection" />
			            	<html:options name="customerDiscountForm" collection="standardList" property="key" labelProperty="value" />
			            	</logic:notEmpty>
	            		</html:select>
	            	</td>
			     </tr>
			     <tr>
	              	<td class="formLabel" style="width:210px !important;"><bean:message key="quotation.label.admin.field.Power.Rating_KW"/><span class="mandatory">&nbsp;</span></td>
	            	<td>
	            		<html:select property="power_rating_kw" styleId="power_rating_kw" styleClass="normal">
			            	<html:option value="0">Select</html:option>
			            	<logic:notEmpty name="customerDiscountForm" property="powerratingkwList">
			            	<bean:define name="customerDiscountForm" property="powerratingkwList" id="powerratingkwList" type="java.util.Collection" />
			            	<html:options name="customerDiscountForm" collection="powerratingkwList" property="key" labelProperty="value" />
	            			</logic:notEmpty>
	            		</html:select>
	            	</td>
	            	<td style="width:55px !important;"> &nbsp; </td>
	            	<td class="formLabel" style="width:210px !important;"><bean:message key="quotation.label.admin.field.frame.type"/><span class="mandatory">&nbsp;</span></td>
	            	<td>
	            		<html:select property="frame_type" styleId="frame_type" styleClass="normal">
			            	<html:option value="0">Select</html:option>
			            	<logic:notEmpty name="customerDiscountForm" property="frametypelist">
			            	<bean:define name="customerDiscountForm" property="frametypelist" id="frametypelist" type="java.util.Collection" />
			            	<html:options name="customerDiscountForm" collection="frametypelist" property="key" labelProperty="value" />
			            	</logic:notEmpty>
	            		</html:select>
	            	</td>
			    </tr>
				<tr>
	              	<td class="formLabel" style="width:210px !important;"><bean:message key="quotation.label.admin.field.number.of.poles"/><span class="mandatory">&nbsp;</span></td>
	            	<td>
	            		<html:select property="number_of_poles" styleId="number_of_poles" styleClass="normal">
			            	<html:option value="0">Select</html:option>
			            	<logic:notEmpty name="customerDiscountForm" property="numberofpoleslist">
			            	<bean:define name="customerDiscountForm" property="numberofpoleslist" id="numberofpoleslist" type="java.util.Collection" />
			            	<html:options name="customerDiscountForm" collection="numberofpoleslist" property="key" labelProperty="value" />
			            	</logic:notEmpty>
	            		</html:select> 
	            	</td>
	            	<td style="width:55px !important;"> &nbsp; </td>
	            	<td class="formLabel" style="width:210px !important;"><bean:message key="quotation.label.admin.field.mounting.type"/><span class="mandatory">&nbsp;</span></td>
	            	<td>
	            		<html:select property="mounting_type" styleId="mounting_type" styleClass="normal">
			            	<html:option value="0">Select</html:option>
			            	<logic:notEmpty name="customerDiscountForm" property="mountingyypelist">
			            	<bean:define name="customerDiscountForm" property="mountingyypelist" id="mountingyypelist" type="java.util.Collection" />
			            	<html:options name="customerDiscountForm" collection="mountingyypelist" property="key" labelProperty="value" />
			            	</logic:notEmpty>
	            		</html:select> 
	            	</td>
			    </tr>
			    <tr>
	              	<td class="formLabel" style="width:210px !important;"><bean:message key="quotation.label.admin.field.tB.position"/><span class="mandatory">&nbsp;</span></td>
	            	<td>
	            		<html:select property="tb_position" styleId="tb_position" styleClass="normal">
			            	<html:option value="0">Select</html:option>
			            	<logic:notEmpty name="customerDiscountForm" property="tbpositionlist">
			            	<bean:define name="customerDiscountForm" property="tbpositionlist" id="tbpositionlist" type="java.util.Collection" />
			            	<html:options name="customerDiscountForm" collection="tbpositionlist" property="key" labelProperty="value" />
			            	</logic:notEmpty>
	            		</html:select> 
	            	</td>
	            	<td style="width:55px !important;"> &nbsp; </td>
	            	<td class="formLabel" style="width:210px !important;"><bean:message key="quotation.label.admin.sales.salesengineer"/><span class="mandatory">&nbsp;</span></td>
	            	<td>
	            		<html:select property="salesengineerid" styleId="salesengineerid" styleClass="normal">
			            	<html:option value="0">Select</html:option>
			            	<logic:notEmpty name="customerDiscountForm" property="salesengineerList">
			            	<bean:define name="customerDiscountForm" property="salesengineerList" id="salesengineerList" type="java.util.Collection" />
			            	<html:options name="customerDiscountForm" collection="salesengineerList" property="key" labelProperty="value" />
			            	</logic:notEmpty>
	            		</html:select>
	            	</td>
			    </tr>
			    <tr>
			    	<td class="formLabel" style="width:210px !important;"><bean:message key="quotation.label.admin.customer.customer"/><span class="mandatory">&nbsp;</span></td>
	            	<td>
	            		<html:select property="customer_name" styleId="customer_name" styleClass="normal">
			            	<html:option value="0">Select</html:option>
			            	<logic:notEmpty name="customerDiscountForm" property="customerlist">
			            	<bean:define name="customerDiscountForm" property="customerlist" id="customerlist" type="java.util.Collection" />
			            	<html:options name="customerDiscountForm" collection="customerlist" property="key" labelProperty="value" />
			            	</logic:notEmpty>
	            		</html:select>
	            	</td>
	            	<td style="width:55px !important;"> &nbsp; </td>
	            	<td class="formLabel" style="width:210px !important;"><bean:message key="quotation.label.admin.field.customer.discount"/><span class="mandatory">&nbsp;</span></td>
	            	<td>
	            		<html:text property="customer_discount" styleId="customer_discount" styleClass="normal"  maxlength="100" /> &nbsp; %
	            	</td>
			    </tr>
			    <tr>
	              	<td class="formLabel" style="width:210px !important;"><bean:message key="quotation.label.admin.field.sm.discount"/><span class="mandatory">&nbsp;</span></td>
	            	<td>
	            		<html:text property="sm_discount" styleId="sm_discount" styleClass="normal"  maxlength="100" /> &nbsp; %
	            	</td>
	            	<td style="width:55px !important;"> &nbsp; </td>
	            	<td class="formLabel" style="width:210px !important;"><bean:message key="quotation.label.admin.field.rsm.discount"/><span class="mandatory">&nbsp;</span></td>
	            	<td>
	            		<html:text property="rsm_discount" styleId="rsm_discount" styleClass="normal"  maxlength="100" /> &nbsp; %
	            	</td>
			    </tr>
			    <tr>
			    	<td class="formLabel" style="width:210px !important;"><bean:message key="quotation.label.admin.field.nsm.discount"/><span class="mandatory">&nbsp;</span></td>
	            	<td>
	            		<html:text property="nsm_discount" styleId="nsm_discount" styleClass="normal"  maxlength="100" /> &nbsp; %
	            	</td>
	            	<td style="width:55px !important;"> &nbsp; </td>
			    	<td class="formLabel" style="width:210px !important;"><bean:message key="quotation.label.admin.field.gstnumber"/>&nbsp;</td>
	            	<td>
	            		<html:select property="gstnumber" styleId="gstnumber" styleClass="normal">
			            	<html:option value="0">Select</html:option>
			            	<logic:notEmpty name="customerDiscountForm" property="gstnumberlist">
			            	<bean:define name="customerDiscountForm" property="gstnumberlist" id="gstnumberlist" type="java.util.Collection" />
			            	<html:options name="customerDiscountForm" collection="gstnumberlist" property="key" labelProperty="value" />
			            	</logic:notEmpty>
	            		</html:select>
	            	</td>
			    </tr>
			    <tr>
			    	<td class="formLabel" style="width:210px !important;"><bean:message key="quotation.label.admin.field.sapcode"/>&nbsp;</td>
	            	<td>
	            		<html:select property="sapcode" styleId="sapcode" styleClass="normal">
			            	<html:option value="0">Select</html:option>
			            	<logic:notEmpty name="customerDiscountForm" property="sapcodelist">
			            	<bean:define name="customerDiscountForm" property="sapcodelist" id="sapcodelist" type="java.util.Collection" />
			            	<html:options name="customerDiscountForm" collection="sapcodelist" property="key" labelProperty="value" />
			            	</logic:notEmpty>
	            		</html:select>
	            	</td>
	            	<td style="width:55px !important;"> &nbsp; </td>
	              	<td class="formLabel" style="width:210px !important;"><bean:message key="quotation.label.admin.field.oraclecode"/>&nbsp;</td>
	            	<td>
	            		<html:select property="oraclecode" styleId="oraclecode" styleClass="normal">
			            	<html:option value="0">Select</html:option>
			            	<logic:notEmpty name="customerDiscountForm" property="oraclecodelist">
			            	<bean:define name="customerDiscountForm" property="oraclecodelist" id="oraclecodelist" type="java.util.Collection" />
			            	<html:options name="customerDiscountForm" collection="oraclecodelist" property="key" labelProperty="value" />
			            	</logic:notEmpty>
	            		</html:select>
	            	</td>
			    </tr>
			  </table>
	            
			  <br>
			  
			  <logic:notEqual name="customerDiscountForm" property="operation" value="update">
	         	<div class="blue-light" align="right">
	         		<html:button property="" styleClass="BUTTON" value="Back" onclick="javascript:returnSearchCustomerDiscount();" />
			  		<html:button property="" styleClass="BUTTON" value="Clear" onclick="javascript:clearCustomerDiscountForm();" />
	            	<html:submit value="Add" styleClass="BUTTON" />
	          	</div>
	          </logic:notEqual>
	          <logic:equal name="customerDiscountForm" property="operation" value="update">
	            <div class="blue-light" align="right">
	            	<html:button property="" styleClass="BUTTON" value="Back" onclick="javascript:returnSearchCustomerDiscount();" />
	        		<html:submit value="Update" styleClass="BUTTON" />
	        	</div>
	     	  </logic:equal>
		    
		    </fieldset>
			</td>
		</tr>
		
	</table>

</html:form>