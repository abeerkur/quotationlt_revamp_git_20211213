<html>
<%@ include file="../../common/nocache.jsp" %>
<%@ include file="../../common/library.jsp" %>


<!--main content  table begins-->
<table width="80%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
<tr>
  <td height="100%" valign="top">  
    <html:form  action="masters.do"  method="post" >
 		<html:hidden property="invoke" value="addorUpdateMaster"/>
 		<html:hidden property="addOrUpdate" value="add"/>
 		<input type="hidden" name="existingMaster" id="existingMaster"  value="<bean:write  name="MasterForm" property="master" />"/> 
 		
 		<input type="hidden" name="<%=org.apache.struts.taglib.html.Constants.TOKEN_KEY%>" value="<%=session.getAttribute(org.apache.struts.action.Action.TRANSACTION_TOKEN_KEY)%>">
       <div class="sectiontitle">
      <logic:match name="sPage" value="addMaster" scope="request">
      	<bean:message key="quotation.create.addmaster"/>
      </logic:match>
      <logic:match name="sPage" value="editMaster" scope="request">
      	<bean:message key="quotation.create.editmaster"/>
      </logic:match>
         		
	  </div>
  	  <br>
	  <div class="note" align="right" style="width:100%">
        	<font color="red"><bean:message key="quotation.label.manadatory"/></font>
		</div>
      <fieldset>
        
		<table border="0" align="center" cellpadding="2" cellspacing="2">
			
	    	<tr  hidden="true">
  				 <td class="formLabel" ><bean:message key="quotation.admin.masterid"/></td>
                <td>
	                <logic:match name="sPage" value="addMaster" scope="request">
	      				<html:text readonly="true" name="MasterForm"  property="masterId" styleId="masterId" styleClass="normal" />
	      			</logic:match>
	      			<logic:match name="sPage" value="editMaster" scope="request">
	      				<html:text name="MasterForm" readonly="true" property="masterId" styleId="masterId" styleClass="normal" />
			      	</logic:match>
		     	 </td>
		     	 </tr>
		     	 <tr>
                <td class="formLabel" ><bean:message key="quotation.admin.master"/><span class="mandatory">&nbsp;</span></td>
                <td>
                	<html:text  name="MasterForm" property="master" styleId="master" maxlength="100" styleClass="normal" />
                </td>
                        
            </tr>
		                    	     
        
		</table>
  		<br>
        
        <div class="blue-light" align="right">
        <logic:match name="sPage" value="addMaster" scope="request">
        	<html:reset value="Clear" styleId="BUTTON" styleClass="BUTTON" />
      		 <input name="add" type="button" class="BUTTON" id="add" value="Add" onClick="addorupdateMaster('add');">
      	</logic:match>
      	<logic:match name="sPage" value="editMaster" scope="request">
      		 <input name="update" type="button" class="BUTTON" id="update" value="Update" onClick="addorupdateMaster('update');">
      	</logic:match>
        
         </div>
	  </fieldset>
    </html:form>    </td>
</tr>
</table>
<!--main content  table end-->
</html>
