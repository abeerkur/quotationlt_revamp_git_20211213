<%@ include file ="../../common/library.jsp"%>
<%@ include file="../../common/nocache.jsp" %>
<!-------------------------- main table Start  --------------------------------->
<html:form action="/searchlocation.do" method="post" onsubmit="return validateLocation()">
<html:hidden property="invoke" value="updateLocation"/>
<html:hidden property="operation" styleId="operation"/>
<html:hidden property="locationId" styleId="locationId"/>
<input type="hidden" name="<%=org.apache.struts.taglib.html.Constants.TOKEN_KEY%>" value="<%=session.getAttribute(org.apache.struts.action.Action.TRANSACTION_TOKEN_KEY)%>">

<table width="80%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
  <tr>
    <td height="100%" valign="top"><br>
        <div class="sectiontitle">
        	<logic:equal name="locationForm" property="operation" value="update">
          		<bean:message key="quotation.label.admin.location.updatedlocation"/>
          	</logic:equal>
          	<logic:notEqual name="locationForm"	property="operation" value="update">
		  		<bean:message key="quotation.label.admin.location.addnewlocation"/>
            </logic:notEqual> </div>
	    <br>
		<div class="note" align="right" style="width:100%">
        	<font color="red"><bean:message key="quotation.label.manadatory"/></font>
		</div>
        <fieldset>
          <table  align="center" >
            <tr>
              	<td class="formLabel"><bean:message key="quotation.label.admin.location.location"/><span class="mandatory">&nbsp;</span></td>
            	<td ><html:text property="locationName" styleId="locationName" styleClass="normal"  maxlength="100" /> </td>
		    </tr>
            <tr>
            
            <td height="23" class="formLabel"><bean:message key="quotation.label.admin.location.region"/><span class="mandatory">&nbsp;</span></td>
            <td>
	                <html:select property="regionId" styleId="regionId" styleClass="normal">
	                  	<html:option value="0">Select</html:option>
	                  	<logic:present name="locationForm" property="regionList">
	                  	<bean:define name="locationForm" property="regionList"
		                	             id="regionList" type="java.util.Collection" />
	                  	<html:options name="locationForm" collection="regionList"
				 				              property="key" labelProperty="value" />
	                  	</logic:present>
	                </html:select>   
                </td>
		      </tr>
		      <tr>
		      	<td height="23" class="formLabel">
		      		<bean:message  key="quotation.label.admin.location.status"/></td>
				<td>
					<html:radio name="locationForm" property="status" styleId="status" value="A"> <bean:message key="quotation.label.admin.location.active"/>  </html:radio>
				    <html:radio name="locationForm" property="status" styleId="status" value="I"> <bean:message key="quotation.label.admin.location.inactive"/>  </html:radio>
				</td>
		      </tr>
            </table>
		    <br>
          <div class="blue-light" align="right">
            <logic:equal name="locationForm" property="operation" value="update">
            <%--TODO <html:reset property="" styleClass="BUTTON" value="Reset" /> --%>
          		<html:submit value="Update" styleClass="BUTTON" />
          	</logic:equal>
          	<logic:notEqual name="locationForm"	property="operation" value="update">
		  		<html:button property="" styleClass="BUTTON" value="Clear" onclick="javascript:clearLocationForm();" />
            	<html:submit value="Add" styleClass="BUTTON" />
            </logic:notEqual>
         </div>
	    </fieldset>
	</td>
  </tr>
</table>
<!-------------------------- main table End  --------------------------------->
</html:form>