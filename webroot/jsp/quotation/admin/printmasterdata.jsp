<%@ include file ="../../common/library.jsp" %>
<%@ include file="../../common/nocache.jsp" %>

<logic:notEqual name="sizeOfList" value="0">
	<logic:present name="resultsList" scope="request">
		<table width="100%" cellpadding="2"cellspacing="0" id="table0" border="1">
        <tr>
	          <td>
	          	<b><bean:write name="MastersDataForm" property="master"/>				</b>
	          </td>          		
	          <td> 
	          		<b><bean:message key="quotation.admin.status"/></b>
	          </td>          		
        </tr>
      	<logic:iterate name="resultsList" scope="request" id="searchMaster"
				   indexId="idx" type="in.com.rbc.quotation.admin.dto.MastersDataDto">
			<tr>
				<td ><bean:write name="searchMaster" property="engineeringName"/></td>
		        <td><bean:write name="searchMaster" property="status"/></td>
	     	</tr>
		</logic:iterate>	
		</table>
	</logic:present>
</logic:notEqual>


<script>
  window.print();
</script>