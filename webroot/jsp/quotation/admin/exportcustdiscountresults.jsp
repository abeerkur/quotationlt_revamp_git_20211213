<%@ include file ="../../common/library.jsp"%>
<%@ include file="../../common/nocache.jsp" %>

<table width="80%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
	<tr>
		<td height="100%" valign="top">
			<logic:present name="alAdminSearchResultsList" scope="request">
				<logic:empty name="alAdminSearchResultsList">
					<table width="100%" cellspacing="0" bgcolor="#F7F7F7">
						<tr class="other">
							<td><bean:message key="quotation.label.admin.noresultsfound" /></td>
						</tr>
					</table>
				</logic:empty>
				<logic:notEmpty name="alAdminSearchResultsList">
					<!-------------------------------------- search results block start --------------------------->
					<table width="100%" cellpadding="2" cellspacing="0" border=1>
						<tr>
							<td class="formLabelTop"><b><bean:message key="quotation.label.admin.field.manufacturing.location" /></b></td>
							<td class="formLabelTop"><b><bean:message key="quotation.label.admin.field.product.group" /></b></td>
							<td class="formLabelTop"><b><bean:message key="quotation.label.admin.field.product.line" /></b></td>
							<td class="formLabelTop"><b><bean:message key="quotation.label.admin.field.Standard" /></b></td>
							<td class="formLabelTop"><b><bean:message key="quotation.label.admin.field.Power.Rating_KW" /></b></td>
							<td class="formLabelTop"><b><bean:message key="quotation.label.admin.field.frame.type" /></b></td>
							<td class="formLabelTop"><b><bean:message key="quotation.label.admin.field.number.of.poles" /></b></td>
							<td class="formLabelTop"><b><bean:message key="quotation.label.admin.field.mounting.type" /></b></td>
							<td class="formLabelTop"><b><bean:message key="quotation.label.admin.field.tB.position" /></b></td>
							<td class="formLabelTop"><b><bean:message key="quotation.label.admin.customer.customer"/></b></td>
							<td class="formLabelTop"><b><bean:message key="quotation.label.admin.field.sapcode"/></b></td>
							<td class="formLabelTop"><b><bean:message key="quotation.label.admin.field.oraclecode"/></b></td>
							<td class="formLabelTop"><b><bean:message key="quotation.label.admin.field.gstnumber"/></b></td>
							<td class="formLabelTop"><b><bean:message key="quotation.label.admin.sales.salesengineer"/></b></td>
							<td class="formLabelTop"><b><bean:message key="quotation.label.admin.field.customer.discount"/> % </b></td>
							<td class="formLabelTop"><b><bean:message key="quotation.label.admin.field.sm.discount"/> % </b></td>
							<td class="formLabelTop"><b><bean:message key="quotation.label.admin.field.rsm.discount"/> % </b></td>
							<td class="formLabelTop"><b><bean:message key="quotation.label.admin.field.nsm.discount"/> % </b></td>
						</tr>
						<logic:iterate name="alAdminSearchResultsList" id="custDiscountDetails" indexId="idx" type="in.com.rbc.quotation.admin.dto.CustomerDiscountDto">
							<tr>
								<td class="bordertd"><bean:write name="custDiscountDetails" property="manufacturing_location" />&nbsp;</td>
								<td class="bordertd"><bean:write name="custDiscountDetails" property="product_group" />&nbsp;</td>
								<td class="bordertd"><bean:write name="custDiscountDetails" property="product_line" />&nbsp;</td>
																
								<logic:notEqual name="custDiscountDetails" property="standard" value="N">
									<td class="bordertd"><bean:message key="quotation.label.yes" /></td>
								</logic:notEqual>
								<logic:equal name="custDiscountDetails" property="standard" value="N">
									<td class="bordertd"><bean:message key="quotation.label.no" /></td>
								</logic:equal>
								
								<td class="bordertd"><bean:write name="custDiscountDetails" property="power_rating_kw" />&nbsp;</td>
								<td class="bordertd"><bean:write name="custDiscountDetails" property="frame_type" />&nbsp;</td>
								<td class="bordertd"><bean:write name="custDiscountDetails" property="number_of_poles" />&nbsp;</td>
								<td class="bordertd"><bean:write name="custDiscountDetails" property="mounting_type" />&nbsp;</td>
								<td class="bordertd"><bean:write name="custDiscountDetails" property="tb_position" />&nbsp;</td>
								<td class="bordertd"><bean:write name="custDiscountDetails" property="customer_name" />&nbsp;</td>
								<td class="bordertd"><bean:write name="custDiscountDetails" property="sapcode"/>&nbsp;</td>
								<td class="bordertd"><bean:write name="custDiscountDetails" property="oraclecode"/>&nbsp;</td>
								<td class="bordertd"><bean:write name="custDiscountDetails" property="gstnumber"/>&nbsp;</td>
								<td class="bordertd"><bean:write name="custDiscountDetails" property="salesengineer"/>&nbsp;</td>
								<td class="bordertd"><bean:write name="custDiscountDetails" property="customer_discount"/>&nbsp;</td>
								<td class="bordertd"><bean:write name="custDiscountDetails" property="sm_discount"/>&nbsp;</td>
								<td class="bordertd"><bean:write name="custDiscountDetails" property="rsm_discount"/>&nbsp;</td>
								<td class="bordertd"><bean:write name="custDiscountDetails" property="nsm_discount"/>&nbsp;</td>
							</tr>
						</logic:iterate>
					</table>
				</logic:notEmpty>
			</logic:present>
			
		</td>
	</tr>
</table>