<!-------------------------- main table Start  --------------------------------->
<%@ include file ="../../common/library.jsp"%>
<%@ include file="../../common/nocache.jsp" %>

<html:form action="/searchlocation.do" method="post">
<html:hidden property="invoke"/>
<html:hidden property="locationId"/>
<input type="hidden" name="<%=org.apache.struts.taglib.html.Constants.TOKEN_KEY%>" value="<%=session.getAttribute(org.apache.struts.action.Action.TRANSACTION_TOKEN_KEY)%>">
<table width="80%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
  <tr>
    <td height="100%" valign="top"><form id="form1" name="form1" method="post" action="">
      <br>
	    <!-------------------------- Title & Search By Block start  --------------------------------->
      <div class="sectiontitle"><bean:message key="quotation.label.admin.location.searchlocation"/> </div>
     <br>

      <fieldset>
        <legend class="blueheader"><bean:message key="quotation.label.admin.searchby"/>  </legend>
        <table  border="0" align="center" cellpadding="0" cellspacing="2">
          <tr>
            <td colspan="5">&nbsp;</td>
          </tr>
          <tr>
            <td class="formLabel"><bean:message key="quotation.label.admin.location.location"/></td>
            <td ><html:text property="locationName" styleClass="normal" maxlength="100" /> </td>
            <td  style="width:20px ">&nbsp;</td>
            <td class="formLabel"><bean:message key="quotation.label.admin.location.region"/></td>
            <%-- <td height="23" class="formLabel"><bean:message key="quotation.label.admin.customer.location"/></td> --%>
            <td>
	                <html:select property="regionId" styleClass="normal">
	                  	<html:option value="0">Select</html:option>
	                  	<logic:present name="locationForm" property="regionList">
	                  	<bean:define name="locationForm" property="regionList"
		                	             id="regionList" type="java.util.Collection" />
	                  	<html:options name="locationForm" collection="regionList"
				 				              property="key" labelProperty="value" />
	                  	</logic:present>
	                </html:select>   
            </td>
          </tr>
        </table>
        <br>
	        <div class="blue-light" align="right">
	          <html:button property="" styleClass="BUTTON" value="Clear" onclick="javascript:resetLocationSearch();" />
		      <html:button property="" styleClass="BUTTON" value="Search" onclick="javascript:searchLocation();" />
	        </div>
        </fieldset>
		 <!-------------------------- Title & Search By Block End  --------------------------------->
		  <!-------------------------- Search Results Block start  --------------------------------->
      			<div class="sectiontitle"><bean:message key="quotation.search.searchresults" /></div>
			      <br>
			      <%-- 
					Listing taglib's hidden fields tag that includes the necessary hidden fields
					related to pagination, at runtime
				   --%>
				 <listing:hiddenfields />
			  	 <%-- Displaying 'No Results Found' message if size of list is zero --%>
			
				 <logic:equal name="sizeOfList" value="0">
					<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#F7F7F7">
						<tr class="other" align="center">
							<td><bean:message key="quotation.label.admin.noresultsfound"/></td>
						</tr>
					</table>
				 </logic:equal>
				 
				 <logic:notEqual name="sizeOfList" value="0">
				 <logic:notPresent name="oLocationDto" property="searchResultsList" scope="request">
					<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#F7F7F7">
						<tr align="center" class="other">
								<td><bean:message key="quotation.label.admin.noresultsfound"/></td>
						</tr>
					</table>
				 </logic:notPresent>
				 <logic:present name="oLocationDto" property="searchResultsList" scope="request">
				 <logic:empty name="oLocationDto" property="searchResultsList">
				 	<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#F7F7F7">
						<tr align="center" class="other">
							<td><bean:message key="quotation.label.admin.noresultsfound"/></td>
						</tr>
					</table>
				 </logic:empty>
				 <logic:notEmpty name="oLocationDto" property="searchResultsList">
				 <table width="100%" border="0" cellspacing="0" cellpadding="0" class="blue-light">
			       <tr>
						<%-- 
						Listing taglib's listingsummary field tag, which displays the
						summary of records retrieved from database and those being
						displayed in this page
						 --%>
						<td align="left" bgcolor="#CCD8F2"><listing:listsummary /></td>
									
						<%-- 
						Listing taglib's pagelist field tag, which displays the list 
						of pages in dropdown selecting which, user will be redirected 
						to the respective page
						 --%>
						<td align="right" bgcolor="#CCD8F2"><listing:pagelist navigation="1" /></td>
					</tr>
				  </table>	
				  <br>
     <!-------------------------------------- search results block end --------------------------->
	        <!--------------------------------------buttons block start --------------------------->

       			<table  cellspacing="0" cellpadding="2" width="100%" ID="table0" class="sortable" >
				   		
				    	<tr id="top"> 
					       	<td >
					       		<listing:sort dbfieldid="1" >
									<bean:message key="quotation.label.admin.location.location"/>
								</listing:sort>	
					        </td>   
					        <td  >
					        	<listing:sort dbfieldid="2" >
									<bean:message key="quotation.label.admin.location.region"/>
								</listing:sort>	
					        </td>
					        <td >
					        	<listing:sort dbfieldid="3" >
									<bean:message key="quotation.search.status"/>
								</listing:sort>	
							</td>
						
							<td  style="width:20% " align="center"><bean:message key="quotation.label.admin.location.changestatus"/></td>						
							<td  style="width:5% " align="center">&nbsp;</td>
					        <td  style="width:5% " align="center"><bean:message key="quotation.search.edit"/></td>
					        <td  style="width:5% " ><bean:message key="quotation.search.delete"/></td>
				        </tr>
				        <% String sTextStyleClass = "textnoborder-light"; %>
				        <logic:iterate name="oLocationDto" property="searchResultsList"
					         id="locationDetails" indexId="idx"
					         type="in.com.rbc.quotation.admin.dto.LocationDto">
					   		<% int id1= idx.intValue();
					   		       id1=(int)id1+1; %>
					        <% if(idx.intValue() % 2 == 0) { %>
					        	<% sTextStyleClass = "textnoborder-light"; %>
								<tr class="light" id='item<bean:write name="idx"/>'" onmouseover="Change(this.id);ChangeTextbox('loc_<%=id1%>_3', 'loc_<%=id1%>_4');" onMouseOut="ChangeBack(this.id);ChangeBackTextbox('loc_<%=id1%>_3', 'loc_<%=id1%>_4', '<%=sTextStyleClass%>');">
							<% } else { %>
								<% sTextStyleClass = "textnoborder-dark"; %>
								<tr class="dark" id='item<bean:write name="idx"/>' onmouseover="Change(this.id);ChangeTextbox('loc_<%=id1%>_3', 'loc_<%=id1%>_4');" onMouseOut="ChangeBack(this.id);ChangeBackTextbox('loc_<%=id1%>_3', 'loc_<%=id1%>_4', '<%=sTextStyleClass%>');">
					   		<% } %>	
						   		  <td class="bordertd"  id="loc_<%=id1 %>_1"><bean:write name="locationDetails" property="locationName"/></td>		  
								  <td  id="loc_<%=id1 %>_2"><bean:write name="locationDetails" property="regionName"/></td>		
								  
								  	<logic:equal name="locationDetails" property="status" value="A">
								  		<td >
								  			<input type="text" name="loc_<%=id1%>_3" id="loc_<%=id1%>_3" readonly="true" class="<%=sTextStyleClass%>"
								  			       value="<bean:message key="quotation.label.admin.location.active"/>">
								  		</td>
								  		<td style="border:0px;">
								  			<input type="text" name="loc_<%=id1%>_4" id="loc_<%=id1 %>_4" readonly="true" class="<%=sTextStyleClass%>-a" 
								  			       onClick="javascript:changeStatus('loc_<%=id1%>_3', 'loc_<%=id1%>_4', '<bean:write name="locationDetails" property="locationId"/>', 'working_<%=id1%>');"
								  			       value="<bean:message key="quotation.label.admin.location.turninactive"/>">
								  		</td>
								  	</logic:equal>
								  	<logic:equal name="locationDetails" property="status" value="I">
								  		<td >
								  			<input type="text" name="loc_<%=id1%>_3" id="loc_<%=id1 %>_3" readonly="true" class="<%=sTextStyleClass%>"
								  			       value="<bean:message key="quotation.label.admin.location.inactive"/>">
								  		</td>
								  		<td style="border:0px;">
								  			<input type="text" name="loc_<%=id1%>_4" id="loc_<%=id1 %>_4" readonly="true" class="<%=sTextStyleClass%>-a"
								  			       onClick="javascript:changeStatus('loc_<%=id1%>_3', 'loc_<%=id1%>_4', '<bean:write name="locationDetails" property="locationId"/>', 'working_<%=id1%>');"
								  			       value="<bean:message key="quotation.label.admin.location.turnactive"/>">
								  		</td>
								  	</logic:equal>
								  <td >
								  		<div id="working_<%=id1%>" class="working">
											<img src="html/images/loading.gif" border="0" align="absmiddle" style="cursor:hand;">
								  		</div>
								  </td>
						          <td ><div align="center"><img src="html/images/edit.gif" alt="Edit" width="20" height="18" border="0" style="cursor:hand " onClick="javascript:editLocation('<bean:write name="locationDetails" property="locationId"/>')"></div></td>
						          <td align="center"><img src="html/images/deleteicon.gif" alt="Delete" width="15" height="16" border="0" style="cursor:hand " onClick="javascript:deleteLocation('<bean:write name="locationDetails" property="locationId"/>', '<bean:write name="locationDetails" property="locationName"/>')"></td>
					        </tr>
				        </logic:iterate>
				      </table>
					
			      <!-------------------------------------- search results block end --------------------------->
				      <table width="100%" cellpadding="0" cellspacing="0" border="0" class="blue-light"  >
						 <tr>
							<%-- 
							Listing taglib's listingsummary field tag, which displays the
							summary of records retrieved from database and those being
							displayed in this page
							 --%>
							<td align="left" bgcolor="#CCD8F2"><listing:listsummary /></td>
										
							<%-- 
							Listing taglib's pagelist field tag, which displays the list 
							of pages in dropdown selecting which, user will be redirected 
							to the respective page
							 --%>
							<td align="right" bgcolor="#CCD8F2"><listing:pagelist navigation="2" /></td>
						</tr>
					  </table>	
					  <BR>
				      <table width="100%" class="blue-light" cellpadding="0" cellspacing="0" border="0">
				        <tr>
				          <td>
				          	<html:button property="" value="Add New Location" styleClass="BUTTON" onclick="javascript:addNewLocation();"/>          	
				          </td>
				          <td align="right">
				            <html:button property="" value="Print" styleClass="BUTTON" onclick="javascript:printLocationResult();"/>
				          	<html:button property="" value="Export to Excel" styleClass="BUTTON" onclick="javascript:exportLocationResult();"/>
				          
				          </td>
				        </tr>
				      </table>
					</logic:notEmpty>
				</logic:present>
			</logic:notEqual>
		  
				<logic:equal name="sizeOfList" value="0">
				  	<table width="100%" class="blue-light" cellpadding="0" cellspacing="0" border="0">
				      <tr>
				        <td>
				        	<html:button property="" value="Add New Location" styleClass="BUTTON" onclick="javascript:addNewLocation();"/>          	
				        </td>
				       </tr>
				       </table>
				 </logic:equal>
	 <!--------------------------------------buttons block End --------------------------->
    </td>
  </tr>
</table>
<!-------------------------- main table End  --------------------------------->
</html:form>