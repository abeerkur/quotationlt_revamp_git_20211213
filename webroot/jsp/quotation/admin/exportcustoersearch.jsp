<%@ include file ="../../common/library.jsp"%>
<%@ include file="../../common/nocache.jsp" %>

<table width="80%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
  <tr>
    <td height="100%" valign="top">
     <logic:present name="alAdminSearchResultsList" scope="request">
	 <logic:empty name="alAdminSearchResultsList">
	 	<table width="100%" cellspacing="0" bgcolor="#F7F7F7">
			<tr class="other">
				<td><bean:message key="quotation.label.admin.noresultsfound"/></td>
			</tr>
		</table>
	 </logic:empty>
	 <logic:notEmpty name="alAdminSearchResultsList">	 
      <!-------------------------------------- search results block start --------------------------->
      <table width="100%" cellpadding="2" cellspacing="0" border=1>
   		
        <tr>
        	<td class="formLabelTop">
				<b><bean:message key="quotation.label.admin.customer.customer"/></b>
	        </td>   
	        <td class="formLabelTop">
				<b><bean:message key="quotation.label.admin.customer.location"/></b>
			</td>
	        <td class="formLabelTop" >
				<b><bean:message key="quotation.label.admin.customer.contactperson"/></b>
	        </td>
	        <td class="formLabelTop">
				<b><bean:message key="quotation.label.admin.customer.email"/></b>
	        </td>
	        <td class="formLabelTop">
				<b><bean:message key="quotation.label.admin.customer.phone"/></b>
	        </td>
	        <td class="formLabelTop">
				<b><bean:message key="quotation.label.admin.customer.mobile"/></b>
	        </td>
	        <td class="formLabelTop">
				<b><bean:message key="quotation.label.admin.customer.fax"/></b>
	        </td>
	        <td class="formLabelTop">
				<b><bean:message key="quotation.label.admin.customer.address"/></b>
	        </td>
	        <td class="formLabelTop"><bean:message key="quotation.label.admin.sales.salesengineer"/></td>
			<td class="formLabelTop"><bean:message key="quotation.search.customer"/> Type</td>
			<td class="formLabelTop"><bean:message key="quotation.label.admin.field.industry"/></td>
			<td class="formLabelTop"><bean:message key="quotation.label.admin.field.sapcode"/></td>
			<td class="formLabelTop"><bean:message key="quotation.label.admin.field.oraclecode"/></td>
			<td class="formLabelTop"><bean:message key="quotation.label.admin.field.gstnumber"/></td>
			<td class="formLabelTop"><bean:message key="quotation.label.admin.field.state"/></td>
			<td class="formLabelTop"><bean:message key="quotation.label.admin.field.country"/></td>
	        
        </tr>
        <logic:iterate name="alAdminSearchResultsList" id="customerDetails" indexId="idx"
	         type="in.com.rbc.quotation.admin.dto.CustomerDto">
       	<tr>
   		   	<td class="bordertd"><bean:write name="customerDetails" property="customer"/>&nbsp;</td>	
   		   	<td class="bordertd"><bean:write name="customerDetails" property="locationname"/>&nbsp;</td>		  
		  	<td class="bordertd"><bean:write name="customerDetails" property="contactperson"/>&nbsp;</td>		
		  	<td class="bordertd"><bean:write name="customerDetails" property="email"/>&nbsp;</td>
		  	<td class="bordertd"><bean:write name="customerDetails" property="phone"/>&nbsp;</td>
			<td class="bordertd"><bean:write name="customerDetails" property="mobile"/>&nbsp;</td>
			<td class="bordertd"><bean:write name="customerDetails" property="fax"/>&nbsp;</td>
		 	<td class="bordertd"><bean:write name="customerDetails" property="address"/>&nbsp;</td>
		 	
		 	<td class="bordertd"><bean:write name="customerDetails" property="salesengineerId"/></td>		
			<td class="bordertd"><bean:write name="customerDetails" property="customerType"/></td>	
			<td class="bordertd"><bean:write name="customerDetails" property="industry"/></td>
			<td class="bordertd"><bean:write name="customerDetails" property="sapcode"/></td>
			<td class="bordertd"><bean:write name="customerDetails" property="oraclecode"/></td>		
			<td class="bordertd"><bean:write name="customerDetails" property="gstnumber"/></td>	
			<td class="bordertd"><bean:write name="customerDetails" property="state"/></td>		
			<td class="bordertd"><bean:write name="customerDetails" property="country"/></td>
		 
        </tr>
        </logic:iterate>
      </table>
	  </logic:notEmpty>
	  </logic:present>
    </td>
  </tr>
</table>