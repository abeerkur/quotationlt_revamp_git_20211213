<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants" %>
<%@ page import="in.com.rbc.quotation.common.utility.PropertyUtility" %>
<%@ page import="in.com.rbc.quotation.common.utility.CommonUtility" %>
<%@ include file ="../../common/library.jsp"%>
<%@ include file="../../common/nocache.jsp" %>
<%@ taglib uri="/ajax-tag" prefix="ajax" %>

<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<html:form action="customerdiscount.do" method="POST" enctype="multipart/form-data">
<html:hidden property="invoke"/>
<html:hidden property="serial_id" styleId="serial_id"/>
<html:hidden property="isFileLoaded" value="YES"/>

<input type="hidden" name="<%=org.apache.struts.taglib.html.Constants.TOKEN_KEY%>" value="<%=session.getAttribute(org.apache.struts.action.Action.TRANSACTION_TOKEN_KEY)%>">

<table width="80%" height="73%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" align="center">
	<tr>
    	<td height="100%" valign="top">
			<br>
	    	<!-------------------------- Title & Search By Block start  --------------------------------->
      		<div class="sectiontitle"><bean:message key="quotation.label.admin.field.customer.discount"/> </div>
    		<br>
     
	     	<fieldset>
	        	<legend class="blueheader"><bean:message key="quotation.label.admin.searchby"/>  </legend>
		        <table  border="0" align="center" cellpadding="0" cellspacing="2">
		        	<tr>
		            	<td colspan="5">&nbsp;</td>
		          	</tr>
					<tr>
						<td class="formLabel" style="width: 155px !important;"> <bean:message key="quotation.label.admin.customer.customer" /> </td>
						<td>
							<input id="customerName" name="customerName" class="normal" type="text" size="29"  autocomplete="off" maxlength="150" />
				            <html:hidden property="customerId" styleId="customerId"/>
				            <label class="working">
								<img src="html/images/loading.gif" border="0" align="absmiddle" style="cursor:hand ">
							</label>
						</td>
						<td style="width: 55px !important;">&nbsp;</td>
						<td class="formLabel" style="width: 155px !important;"> <bean:message key="quotation.label.admin.field.manufacturing.location" /> </td>
						<td>
							<html:select property="manufacturing_location" styleId="manufacturing_location" styleClass="normal">
								<html:option value="0">Select</html:option>
								<logic:notEmpty name="customerDiscountForm" property="maufacturingList">
									<bean:define name="customerDiscountForm" property="maufacturingList" id="maufacturingList" type="java.util.Collection" />
									<html:options name="customerDiscountForm" collection="maufacturingList" property="key" labelProperty="value" />
								</logic:notEmpty>
							</html:select>
						</td>
					</tr>
					<tr>
						<td class="formLabel" style="width:155px !important;"><bean:message key="quotation.label.admin.field.product.group"/> </td>
						<td>
							<html:select property="product_group" styleId="product_group" styleClass="normal">
						    	<html:option value="0">Select</html:option>
								<logic:notEmpty name="customerDiscountForm" property="productGroupList">
									<bean:define name="customerDiscountForm" property="productGroupList" id="productGroupList" type="java.util.Collection" />
								    <html:options name="customerDiscountForm" collection="productGroupList" property="key" labelProperty="value" />
								</logic:notEmpty>
							</html:select>
						</td>
						<td style="width:55px !important;"> &nbsp; </td>
						<td class="formLabel" style="width:155px !important;"><bean:message key="quotation.label.admin.field.product.line"/> </td>
						<td>
							<html:select property="product_line" styleId="product_line" styleClass="normal">
								<html:option value="0">Select</html:option>
								<logic:notEmpty name="customerDiscountForm" property="productLineList">
									<bean:define name="customerDiscountForm" property="productLineList" id="productLineList" type="java.util.Collection" />
								    <html:options name="customerDiscountForm" collection="productLineList" property="key" labelProperty="value" />
								</logic:notEmpty>
							</html:select>
						</td>						
					</tr>
					<tr>
						<td class="formLabel" style="width:155px !important;"><bean:message key="quotation.label.admin.field.frame.type"/> </td>
						<td>
							<html:select property="frame_type" styleId="frame_type" styleClass="normal">
								<html:option value="0">Select</html:option>
								<logic:notEmpty name="customerDiscountForm" property="frametypelist">
									<bean:define name="customerDiscountForm" property="frametypelist" id="frametypelist" type="java.util.Collection" />
									<html:options name="customerDiscountForm" collection="frametypelist" property="key" labelProperty="value" />
								</logic:notEmpty>
							</html:select>
						</td>
						<td style="width:55px !important;"> &nbsp; </td>
						<td class="formLabel" style="width: 155px !important;"><bean:message key="quotation.label.admin.field.Power.Rating_KW" /> </td>
						<td>
							<html:select property="power_rating_kw" styleId="power_rating_kw" styleClass="normal">
								<html:option value="0">Select</html:option>
								<logic:notEmpty name="customerDiscountForm" property="powerratingkwList">
									<bean:define name="customerDiscountForm" property="powerratingkwList" id="powerratingkwList" type="java.util.Collection" />
									<html:options name="customerDiscountForm" collection="powerratingkwList" property="key" labelProperty="value" />
								</logic:notEmpty>
							</html:select>
						</td>
					</tr>
				</table>
	        	<br>
		        <div class="blue-light" align="right">
		        	<html:button property="" styleClass="BUTTON" value="Clear" onclick="javascript:resetCustomerDiscountSearch();" />
			      	<html:button property="" styleClass="BUTTON" value="Search" onclick="javascript:searchCustomerDiscount();" />
		        </div>
	        </fieldset>
        
	        <br>
	        <div class="sectiontitle"><bean:message key="quotation.search.searchresults" /></div>
			<br>
			<%-- Listing taglib's hidden fields tag that includes the necessary hidden fields related to pagination, at runtime --%>
			<listing:hiddenfields />
			<%-- Displaying 'No Results Found' message if size of list is zero --%>
			
			<logic:equal name="sizeOfList" value="0">
				<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#F7F7F7">
					<tr class="other" align="center">
						<td><bean:message key="quotation.label.admin.noresultsfound"/></td>
					</tr>
				</table>
			</logic:equal>
				 
			<logic:notEqual name="sizeOfList" value="0">
				<logic:notPresent name="oCustomerDiscountDto" property="searchResultsList" scope="request">
					<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#F7F7F7">
						<tr align="center" class="other">
								<td><bean:message key="quotation.label.admin.noresultsfound"/></td>
						</tr>
					</table>
				</logic:notPresent>
				<logic:present name="oCustomerDiscountDto" property="searchResultsList" scope="request">
					<logic:empty name="oCustomerDiscountDto" property="searchResultsList">
					 	<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#F7F7F7">
							<tr align="center" class="other">
								<td><bean:message key="quotation.label.admin.noresultsfound"/></td>
							</tr>
						</table>
					</logic:empty>
					<logic:notEmpty name="oCustomerDiscountDto" property="searchResultsList">
						<table width="100%" border="0" cellspacing="0" cellpadding="0" class="blue-light">
							<tr>
								<%-- 
									Listing taglib's listingsummary field tag, which displays the summary of records
									retrieved from database and those being displayed in this page.
							 	--%>
								<td align="left" bgcolor="#CCD8F2"><listing:listsummary /></td>
								<%-- 
									Listing taglib's pagelist field tag, which displays the list of pages in dropdown
								 	selecting which, user will be redirected to the respective page.
								--%>
								<td align="right" bgcolor="#CCD8F2"><listing:pagelist navigation="1" /></td>
							</tr>
						</table>	
				  		<br>
											
       					<table  cellspacing="0" cellpadding="2" width="100%" ID="table0" class="sortable" >
							<tr id="top"> 
						       	<td class="bordertd"><listing:sort dbfieldid="1" ><bean:message key="quotation.label.admin.field.manufacturing.location"/></listing:sort></td>   
						        <td class="bordertd"><listing:sort dbfieldid="2" ><bean:message key="quotation.label.admin.field.product.group"/></listing:sort></td>
						        <td class="bordertd"><listing:sort dbfieldid="3" ><bean:message key="quotation.label.admin.field.product.line"/></listing:sort></td>
								<td class="bordertd" style="width:20% " align="center"><bean:message key="quotation.label.admin.field.Standard"/></td>						
								<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.Power.Rating_KW"/></td>
								<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.frame.type"/></td>
								<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.number.of.poles"/></td>
								<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.mounting.type"/></td>
								<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.tB.position"/></td>
								<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.customer.customer"/></td>
								<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.sapcode"/></td>
								<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.oraclecode"/></td>
								<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.gstnumber"/></td>
								<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.sales.salesengineer"/></td>
								<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.customer.discount"/> % </td>
								<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.sm.discount"/> % </td>
								<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.rsm.discount"/> % </td>
								<td class="bordertd" style="width:5% " align="center"><bean:message key="quotation.label.admin.field.nsm.discount"/> % </td>
								<td  style="width:5% " align="center"><bean:message key="quotation.search.edit"/></td>
						        <td  style="width:5% " ><bean:message key="quotation.search.delete"/></td>
							</tr>
					        <% 
					        	String sTextStyleClass = "textnoborder-light"; 
					        %>
				        	<logic:iterate name="oCustomerDiscountDto" property="searchResultsList" id="customerDiscountForm" indexId="idx" type="in.com.rbc.quotation.admin.dto.CustomerDiscountDto">
					   			<% 
					   				int id1= idx.intValue();
					   				id1=(int)id1+1; 
					   			%>
						        <% if(idx.intValue() % 2 == 0) { %>
						        	<% sTextStyleClass = "textnoborder-light"; %>
									<tr class="light" id='item<bean:write name="idx"/>' onmouseover="Change(this.id);ChangeTextbox('loc_<%=id1%>_3', 'loc_<%=id1%>_4');" onMouseOut="ChangeBack(this.id);ChangeBackTextbox('loc_<%=id1%>_3', 'loc_<%=id1%>_4', '<%=sTextStyleClass%>');">
								<% } else { %>
									<% sTextStyleClass = "textnoborder-dark"; %>
									<tr class="dark" id='item<bean:write name="idx"/>' onmouseover="Change(this.id);ChangeTextbox('loc_<%=id1%>_3', 'loc_<%=id1%>_4');" onMouseOut="ChangeBack(this.id);ChangeBackTextbox('loc_<%=id1%>_3', 'loc_<%=id1%>_4', '<%=sTextStyleClass%>');">
						   		<% } %> 
							   			<td class="bordertd"><bean:write name="customerDiscountForm" property="manufacturing_location"/></td>
								   		<td class="bordertd"><bean:write name="customerDiscountForm" property="product_group"/></td>
								   		<td class="bordertd"><bean:write name="customerDiscountForm" property="product_line"/></td>
								   		
								   		<logic:notEqual name="customerDiscountForm" property="standard" value="N">
								   			<td class="bordertd"><bean:message key="quotation.label.yes" /></td>
								   		</logic:notEqual>
								   		<logic:equal name="customerDiscountForm" property="standard" value="N">
								   			<td class="bordertd"><bean:message key="quotation.label.no" /></td>
								   		</logic:equal>
								   		
								   		<td class="bordertd"><bean:write name="customerDiscountForm" property="power_rating_kw"/></td>
								   		<td class="bordertd"><bean:write name="customerDiscountForm" property="frame_type"/></td>
								   		<td class="bordertd"><bean:write name="customerDiscountForm" property="number_of_poles"/></td>
								   		<td class="bordertd"><bean:write name="customerDiscountForm" property="mounting_type"/></td>
								   		<td class="bordertd"><bean:write name="customerDiscountForm" property="tb_position"/></td>
								   		<td class="bordertd"><bean:write name="customerDiscountForm" property="customer_name"/></td>
								   		<td class="bordertd"><bean:write name="customerDiscountForm" property="sapcode"/></td>
								   		<td class="bordertd"><bean:write name="customerDiscountForm" property="oraclecode"/></td>
								   		<td class="bordertd"><bean:write name="customerDiscountForm" property="gstnumber"/></td>
								   		<td class="bordertd"><bean:write name="customerDiscountForm" property="salesengineer"/></td>
								   		<td class="bordertd"><bean:write name="customerDiscountForm" property="customer_discount"/> % </td>
								   		<td class="bordertd"><bean:write name="customerDiscountForm" property="sm_discount"/> % </td>
								   		<td class="bordertd"><bean:write name="customerDiscountForm" property="rsm_discount"/> % </td>
								   		<td class="bordertd"><bean:write name="customerDiscountForm" property="nsm_discount"/> % </td>
															   		  
						          		<td >
						          			<div align="center">
						          				<img src="html/images/edit.gif" alt="Edit" width="20" height="18" border="0" style="cursor:hand " 
						          					onClick="javascript:editCustomerDiscount('<bean:write name="customerDiscountForm" property="serial_id"/>')">
						          			</div>
						          		</td>
						          		<td align="center">
						          			<img src="html/images/deleteicon.gif" alt="Delete" width="15" height="16" border="0" style="cursor:hand " 
						          				onClick="javascript:deleteCustomerDiscount('<bean:write name="customerDiscountForm" property="serial_id"/>', '<bean:write name="customerDiscountForm" property="manufacturing_location"/>')">
						          		</td>
									</tr>
							
							</logic:iterate>
						</table>
						
						<table width="100%" cellpadding="0" cellspacing="0" border="0" class="blue-light">
							<tr>
								<%-- 
									Listing taglib's listingsummary field tag, which displays the summary of records 
									retrieved from database and those being displayed in this page
							 	--%>
								<td align="left" bgcolor="#CCD8F2"><listing:listsummary /></td>
								<%-- 
									Listing taglib's pagelist field tag, which displays the list of pages in dropdown 
									selecting which, user will be redirected to the respective page
							 	--%>
								<td align="right" bgcolor="#CCD8F2"><listing:pagelist navigation="2" /></td>
							</tr>
						</table>
						<br>
						
					</logic:notEmpty>
				</logic:present>
			</logic:notEqual>
			
			<!-------------------------------------- search results block end --------------------------->
	        
	        <!-------------------------------------- buttons block start -------------------------------->
	        			 
			<table width="100%" class="blue-light" cellpadding="0" cellspacing="0" border="0">
				<tr>
					<td>
						<html:button property="" value="Add New" styleClass="BUTTON" onclick="javascript:addNewCustomerDiscount();"/>          	
					</td>
					<td align="right">
						<html:button property="" value="Export to Excel" styleClass="BUTTON" onclick="javascript:exportCustomerDiscountResult();"/>
					</td>
				</tr>
			</table>
			
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<br>
						<div class="sectiontitle"><bean:message key="quotation.label.admin.field.customer.discount" /> <bean:message key="quotation.search.bulkupload" /></div>
						<br>
						
						<fieldset>
							<table width="100%" border="0" cellpadding="0" cellspacing="0">
								<tr>
						      		<td nowrap>
						      			<div class="box-nav">
						      				<p class="list-hd">Instructions for Bulk Loading QuotationLT Customer Discount values:</p>
						      				<ul>
												<li>Customer Discounts information should be provided in a specific format.</li>
									       		<li>Please  <a href="html/docs/CustomerDiscountBulkload.xls" target="_blank">click here</a> to download the template to load Customer Discounts in the Tool.</li>
									            <li>Please ensure all mandatory columns are filled in to avoid disturbances in data</li>
												<!--  <li>Please note that this feature is limited to only < % =PropertyUtility.getKeyValue(QuotationConstants.EHS_BULKUPLOAD_RECORD_COUNT) % > records. </li> -->
												<li>Please note that this feature is limited to only 500 records.
											</ul>
										</div>
									</td>
						      	</tr>
							</table>
							<table width="100%" border="0" cellpadding="0" cellspacing="0">
								<tr>
						      		<td align="center">Select the file to be uploaded</td>
						      		<td align="left"> <html:file property="custDiscountBulkupload" style="width:218px; font:10px;" /> </td>
						      	</tr>
							</table>
							<br>
							<div class="blue-light" align="right">
								<html:button property="" value="Bulk Upload" styleClass="button" onclick="javascript:validateCustomerBulkupLoad();" />
							</div>
						</fieldset>
					</td>
				</tr>
			</table>
			
		</td>
	</tr>
</table>

</html:form>

<!-- AUTOCOMPLETE CODE FOR FILLING THE CUSTOMER NAME IN CUSTOMER TEXT FIELD- TO BE REMOVED LATER -->

<ajax:autocomplete
				  fieldId="customerName"
				  popupId="customer-popup"
				  targetId="customerId"
				  baseUrl="servlet/AutocompleteServlet?type=customer"
				  paramName="customer"
				  className="autocomplete"
				  progressStyle="throbbing" />
