<%@ include file="../common/nocache.jsp" %>
<%@ taglib uri="/struts-tiles" prefix="tiles" %>
<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants" %>
<%@ page import="in.com.rbc.quotation.common.utility.PropertyUtility" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<html>
	<head>
		<title><tiles:getAsString name="title"/></title>
		<link rel="stylesheet" href="html/css/ajax.css" type="text/css" />
		<link rel="stylesheet" href="html/css/main.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="html/css/print.css" type="text/css" media="print" />		
		<link rel="stylesheet" href="html/css/menu.css" type="text/css" />
		<link rel="stylesheet" href="html/css/results.css" type="text/css" />
		<link rel="stylesheet" href="html/css/theme.css" type="text/css" />
				
		<script type="text/javascript">qmad=new Object();qmad.bvis="";qmad.bhide="";</script>
		<script language="javascript" type="text/javascript" src="html/js/quotation_lt.js"></script>
		<script language="javascript" type="text/javascript" src="html/js/quotation.js"></script>
		<script language="javascript" type="text/javascript" src="html/js/quotationadmin.js"></script>
		<script language="javascript" src="html/js/common.js"></script>
		<script language="javascript" src="html/js/listing.js"></script>
		<script language="javascript" src="html/js/jquery.js"></script>
		<script language="javascript" src="html/js/menu.js"></script>
		<script language="javascript" src="html/js/results.js"></script>		
		<script language="javascript" type="text/javascript" src="html/js/jquery.min.js"></script>
		<script language="javascript" src="html/js/jquery-ui.min.js"></script>
		<script language="javascript" src="html/js/calendar_part1.js"></script>
		<script language="javascript" src="html/js/calendar_part2.js"></script>
		<script language="javascript" src="html/js/calendar-en.js"></script>
		<script language="javascript" src="html/js/ajax.js"></script>
		<script language="javascript" src="html/js/lookup.js"></script>
		<script language="javascript" src="html/js/show_hide.js"></script>
		<script language="javascript" src="html/js/ajaxtags.js"></script>
		<script language="javascript" src="html/js/signuptoggle.js"></script>
		<script language="javascript" src="html/js/calculator.js"></script>
		
	</head>
	<body>
	<input type="hidden" id ="quotationUrl" name="quotationUrl" value ="<%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_PROPERTYFILENAME, QuotationConstants.QUOTATION_APP_URL)%>"/>
	<tiles:insert attribute="header" />			  
		     <table width="100%" border="0" cellpadding="0" cellspacing="0" class="adbanner">
				   <tr>
						<td class="bg-left">&nbsp;</td>
						<td style="vertical-align:top"><tiles:insert attribute="menu" /></td>
						<td class="bg-rt">&nbsp;</td>
			      </tr>
				  </table>
				  <table width="100%" border="0" cellpadding="0" cellspacing="0">
			     	<tr>
						<td class="bg-left">&nbsp;</td>
						<td style="vertical-align:top"><tiles:insert attribute="body" /></td>
						<td class="bg-rt" ><img src="html/images/spacer.gif" width="1" height="390px"></td>
			      </tr>
			    </table>
		  <tiles:insert attribute="footer" />
		
	</body>
</html>