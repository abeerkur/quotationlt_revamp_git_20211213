<%@ include file="../common/library.jsp" %>
<%@ page import="in.com.rbc.quotation.common.utility.CommonUtility" %>
<%@ page import="in.com.rbc.quotation.common.utility.PropertyUtility" %>
<%@ page import="in.com.rbc.quotation.common.constants.QuotationConstants" %>

<!--top menu start -->
<table style="width:100%; border:0; background-color:#ffffff "  cellpadding="0" cellspacing="0" >
	<tr>
		<td class="bg-menu" style="background-image:url(html/images/bg_topnav.gif); background-repeat:repeat-x; background-position:bottom;"><img src="html/images/spacer.gif" width="1" height="1" />
				<script type="text/javascript">
				document.write('<ul id="qm0" class="qmmc">\n')
				document.write('<li><a class="qmparent" href="javascript:home()">Home</a></li>\n')
				<logic:present name="isSalesManager" scope="session">
				<logic:equal name="isSalesManager" value="true">
				document.write('<li><a class="qmparent" href="javascript:placeAnEnquiry()">Place an Enquiry</a>\n')
				</logic:equal>
				</logic:present>
				document.write('<li><a class="qmparent" href="javascript:searchEnquiry()">Search Enquiry</a>\n')
				document.write('<li><a class="qmparent" href="javascript:viewReport()">Reports</a>\n')
				<security:role id="<%= CommonUtility.getIntValue(PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_ROLE_ADMIN)) %>" >
					document.write('<li><a class="qmparent" href="#">Administration</a>\n')
						document.write('<ul>\n')
						document.write('<li><a class="dropdown" href="javascript:viewLocations()">Manage Locations</a></li>\n')
						document.write('<li><a class="dropdown" href="javascript:viewCustomers()">Manage Customers</a></li>\n')
						document.write('<li><a class="dropdown" href="javascript:viewSalesManagers()">Manage Sales Managers</a></li>\n')
						document.write('<li><a class="dropdown" href="javascript:viewDesignEngineers()">Manage Design Engineers</a></li>\n')
						document.write('<li><a class="dropdown" href="javascript:viewReplaceMasters()">Replace Managers</a></li>\n')
						document.write('<li><a class="dropdown" href="javascript:viewMasterData()">Manage Master Data</a></li>\n')
						document.write('<li><a class="dropdown" href="javascript:viewMasters()">Manage Masters</a></li>\n')
					document.write('</ul></li>\n')
				</security:role>
				document.write('<li><a class="qmparent" href="javascript:openViewFeedbackForm(\'<%=PropertyUtility.getKeyValue(QuotationConstants.QUOTATION_PROPERTYFILENAME, QuotationConstants.QUOTATION_FEEDBACK_LINK)%>\')">Feedback</a>\n')	
				document.write('<li class="qmclear">&nbsp;</li></ul>\n')
			</script>
         	<script type="text/javascript">qm_create(0,false,0,500,false,false,false,false,false);</script>
		</td>
	</tr>
</table>
<!--top menu end -->
<!--black line under menu -->
<table style="width:100%; border:0 "cellpadding="0" cellspacing="0">
	<tr>
	  	<td style="height:1px"><img src="html/images/spacer.gif" width="1" height="1" /></td>
	</tr>
	<tr>
	  	<td style="height:1px; background-color:#000000"><img src="html/images/spacer.gif" width="1" height="1" /></td>
	</tr>
</table>