/**
 * This function brings the user to the selected page. This method is called by
 * ListModel.getPageList()
 * @author sfwang
 * @param oFormName form name (eg. 'document.onlinesubForm'), 
 *	  oActionName action object (eg. 'document.onlinesubForm.action'), 
 * 	  sActionName action name (eg. 'CompleteCollection'), 
 *	  InputField input field to be passed over from the calling methods
 *
 */
function goToPage(oFormName, oActionName, sActionName, InputField)
    {
       
      oFormName.sCurrentPage.value=InputField.value;
       oActionName.value = sActionName;
      oFormName.submit();
    }

/**
 * This function brings the user to the next or previous page. This method is called by
 * ListModel.getPageList()
 * @author sfwang
 * @param oFormName form name (eg. 'document.onlinesubForm'), 
 *	  oActionName action object (eg. 'document.onlinesubForm.action'), 
 * 	  sActionName action name (eg. 'CompleteCollection'), 
 *	  sNextPrevNo the next or previous page number to be passed over from the calling methods
 *
 */    
function goNextPrev(oFormName, oActionName, sActionName, sNextPrevNo)
    {
     oFormName.sNextPrevNo.value = sNextPrevNo;
     oActionName.value = sActionName;
     oFormName.submit();
    }

/**
 * This function sorts the listing according to the fields & sort order according to user selection
 * @author sfwang
 * @param oFormName form name (eg. 'document.onlinesubForm'), 
 * 	  oActionName action object (eg. 'document.onlinesubForm.action'), 
 *	  sActionName action name (eg. 'CompleteCollection'), 
 *	  sSortBy the sorting field (sent in as number defined in getDBField in Dao)
 *	  sSortOrder the sorting order ('A' - ascending or 'D' - descending)
 *
 */ 
function goSort(oFormName, oActionName, sActionName, sSortBy, sSortOrder)
	{
     	oFormName.sSortBy.value = sSortBy;
     	oFormName.sSortOrder.value = sSortOrder;
     	oActionName.value = sActionName;
        oFormName.submit();
    	}