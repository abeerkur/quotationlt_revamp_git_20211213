
		/*******  Menu 0 Add-On Settings *******/
		var a = qmad.qm0 = new Object();

		// Slide Animation Add On
		a.slide_animation_frames = 15;
		a.slide_accelerator = 1;
		a.slide_sub_subs_left_right = true;
		a.slide_offxy = 1;

		// Item Bullets (CSS - Imageless) Add On
		a.ibcss_apply_to = "parent";
		a.ibcss_main_type = "none";
		a.ibcss_main_direction = "down";
		a.ibcss_main_size = 5;
		a.ibcss_main_position_x = -16;
		a.ibcss_main_position_y = -5;
		a.ibcss_main_align_x = "right";
		a.ibcss_main_align_y = "middle";
		a.ibcss_sub_type = "arrow-head";
		a.ibcss_sub_direction = "right";
		a.ibcss_sub_size = 5;
		a.ibcss_sub_position_x = -2;
		a.ibcss_sub_align_x = "right";
		a.ibcss_sub_align_y = "middle";

		// Rounded Corners Add On
		a.rcorner_size = 6;
		a.rcorner_border_color = "#336699";
		a.rcorner_bg_color = "#5286BB";
		a.rcorner_apply_corners = new Array(false,false,false,false);
		a.rcorner_top_line_auto_inset = true;

		// Rounded Items Add On
		
		a.ritem_apply = "main";
		a.ritem_main_apply_corners = new Array(false,false,false,false);
		a.ritem_show_on_actives = true;

		// IE Over Select Fix Add On
		a.overselects_active = true;