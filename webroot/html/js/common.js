/*************** 	Pop Window center related items  :  Start	*******************/
	var width = 950;
    var height = 550;
    var left = parseInt((screen.availWidth/2) - (width/2));
    var top = parseInt((screen.availHeight/2) - (height/2));
    var windowFeatures = "width=" + width + ",height=" + height + ",status,resizable,scrollbars,left=" + left + ",top=" + top + "screenX=" + left + ",screenY=" + top;
    
/*************** 	Pop Window center related items  :	End		*******************/

/*  User Lookup - PopUp Window.  */ 
function searchLookup(nameObjid, ssoObjId, emailObj){
    	//alert('hello')
    var params = 'invoke=lookupEmployee'
                  + '&selectType=1'
                  + '&parentNameObj=' + nameObjid
                  + '&parentSsoObj=' + ssoObjId                  
                  + '&parentEmailObj=' + emailObj
                  + '&allowMultipleEmailIds=false'
                  + '&isSingleSelect=true';   
       
    window.open('../attachments/lookupEmployee.do?'+params, '', windowFeatures);
}
    
    
/*
 * PURPOSE: Remove leading blanks from our string.
 * IN: str - the string we want to LTrim
 */
function LTrim(str) {
    var whitespace = new String(" \t\n\r");
    var s = new String(str);
    if (whitespace.indexOf(s.charAt(0)) != -1) {
        // Case when we have a string with leading blank(s)...
        var j=0, i = s.length;
        // Iterate from the far left of string until we don't have any more whitespace...
        while (j < i && whitespace.indexOf(s.charAt(j)) != -1) j++;
        // Get the substring from the first non-whitespace character to the end of the string...
        s = s.substring(j, i);
    }
    return s;
}
/*
 * RTrim(string) : Returns a copy of a string without trailing spaces.
 * PURPOSE: Remove trailing blanks from our string.
 * IN: str - the string we want to RTrim
 */
function RTrim(str) {
    // We don't want to trip JUST spaces, but also tabs, line feeds, etc.  Add anything else you want to "trim" here in Whitespace
    var whitespace = new String(" \t\n\r");
    var s = new String(str);
    if (whitespace.indexOf(s.charAt(s.length-1)) != -1) {
        // Case when we have a string with trailing blank(s)...
        var i = s.length - 1;       // Get length of string
        // Iterate from the far right of string until we don't have any more whitespace...
        while (i >= 0 && whitespace.indexOf(s.charAt(i)) != -1) i--;
        // Get the substring from the front of the string to where the last non-whitespace character is...
        s = s.substring(0, i+1);
    }
    return s;
}
/*
 * Trim(string) : Returns a copy of a string without leading or trailing spaces
 * PURPOSE: Remove trailing and leading blanks from our string.
 * IN: str - the string we want to Trim
 * RETVAL: A Trimmed string!
 */
function Trim(str) {
    return RTrim(LTrim(str));
}
function selectLstItem(field,value) {
	for(i=0;i<field.options.length; i++) {
		if(field.options[i].value==value)
			field.options[i].selected=true;
	}
}
function isNumeric(val){
    return(parseFloat(val,10)==(val*1));
}
function isRequired(id, name) {
	if ( document.getElementById(id).value == '' ) {
		alert(name + " is a required field!");
		document.getElementById(id).className = 'invalid';
		isValid = false;
	}
}
function checkNumeric(id, name) {
	if ( !isNumeric(document.getElementById(id).value) && document.getElementById(id).value != '') {
		alert(name +  " is not a valid numeric value!");
		document.getElementById(id).className = 'invalid';
		isValid = false;
	}	
}
function chkSingleQuote(e){
	var keynum;
	var keychar;
	var singlequotecheck;
	
	if(window.event) // IE
		keynum = e.keyCode;
	else if(e.which) // Netscape/Firefox/Opera
		keynum = e.which;
	keychar = String.fromCharCode(keynum);
	singlequotecheck = /\'/;
	return !singlequotecheck.test(keychar);
}
function stripSingleQuote(target, str){
	var stripped = str.replace(/[\'\ ]/g, '');
	document.getElementById(target).value = stripped;
}
function checkTxtNull(obj){
  if (obj != null)
    if (obj.value != null && Trim(obj.value) != '')
    	return false;
  else 
    return true;  
}
function checkComboNull(obj){
  if (obj != null)
    if (obj.value != null && obj.value != 0 && Trim(obj.value) != '')
    return false;
  else 
    return true;  
}
function checkUndefined(value){
    if (value == undefined || value == "undefined" || Trim(value) == '')
    	return true;
  	else 
    	return false;  
}

function checkValidText(obj){
	
	var specialCharacter = /^[a-zA-Z0-9()-]+(\s([a-zA-Z0-9()-])+)*$/;
	if (obj != null) {
		if (Trim(obj.value) != "" && ! specialCharacter.test(Trim(obj.value))) {
			return false;
		}else
			return true;
	}
}

function getSelectedRadioValue(formName, radioFieldName) {
    var oRadioFieldArray = eval("document." + formName + "." + radioFieldName);
    var selectedValue = "";
    selectedValue = oRadioFieldArray.value;
    if ( Trim(selectedValue) == "" || selectedValue != "undefined" ) {
        for ( i=0 ; i<oRadioFieldArray.length ; i++ ) {
            if ( oRadioFieldArray[i].checked ) {
                selectedValue = oRadioFieldArray[i].value;
                break;
            }
        }
    }
    return selectedValue;
}
/*
 * Function Name : isEmail
 * Descriptiion  : If email is having error then focus is laid on that field.
 * Inputs        : Email - object of the email field
 * Returns	     : It returns false if the email is invalid
 */
function isValidEmail(objEmailField) {
    var emailValue = Trim(objEmailField.value) ;
    var strFirstChar = emailValue.charAt(0) ;
    if ( emailValue=="" ) {
        alert("Please Enter email id") ;
        objEmailField.focus() ;
        objEmailField.select() ;
        return false ;
    }
    if ( !isvalid("Email Id", emailValue, "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.-_@") ) {
        alert("Please Enter valid email id") ;
        objEmailField.focus() ;
        objEmailField.select() ;
        return false ;
    }
    if ( (emailValue.indexOf("@")==-1) 
           || (emailValue.indexOf(".")==-1) 
           || ((emailValue.indexOf("@")+1) == emailValue.length)
           || (emailValue.indexOf("@")!=emailValue.lastIndexOf("@")) ) {
        alert("Please Enter valid email id") ;
        objEmailField.focus() ;
        objEmailField.select() ;
        return false ;
    }
//    if ( (strFirstChar<"A" || strFirstChar>"Z") && (strFirstChar<"a" || strFirstChar>"z") ) {
//        alert("Email id should start with an alphabet") ;
//        objEmailField.focus() ;
//        objEmailField.select() ;
//        return false ;
//    }
    if ( ( (emailValue.indexOf("@")+1)==emailValue.indexOf(".") ) || ( (emailValue.indexOf("@")-1)==emailValue.indexOf(".") ) ) {
        alert("Please Enter valid email id") ;
        objEmailField.focus() ;
        objEmailField.select() ;
        return false ;
    }
    if ( ( (emailValue.indexOf("-"))>emailValue.indexOf("@") ) || ( (emailValue.indexOf("@")-1)==emailValue.indexOf("-") ) ) {
        alert("Please Enter valid email id") ;
        objEmailField.focus() ;
        objEmailField.select() ;
        return false ;
    }
    return true ;
}
/*
 * Function Name : isvalid
 * Description   : This function validates for the valid characters in the provided string
 * Input         : ctrlname - Passes the control name on which the validation is to be performed
 *                            str - Passes the string on which the validation is to be performed
 *                            chars - Represent the valid set of characters allowed
 * Returns       : The flag which is set to true or false is returned accordingly
 * Sample Usage  : 
 *                 isUserValid = isvalid('Username', 'shanthi', 'abcdefghijklmnopqrstuvwxyz01234567890') ;
 *                 --  In this case, isvalid function returns true
 *                 isUserValid = isvalid('Username', 'shanthi123', 'abcdefghijklmnopqrstuvwxyz01234567890') ;
 *                 --  In this case, isvalid function returns true
 *                 isUserValid = isvalid('Username', 'shanthi#1%3', 'abcdefghijklmnopqrstuvwxyz01234567890') ;
 *                 --  In this case, isvalid function returns false
 */
function isvalid(ctrlname, str, chars) {	
    var bool_valid = true;	
	var validchars = chars;
	for (var int_i=0; int_i < str.length; int_i++) {
		var letter = str.charAt(int_i).toLowerCase();
        //if ( escape(letter) != "%0D" && escape(letter) != "%0A") {
            if (validchars.indexOf(letter) != -1)
		        continue;
            if ( escape(letter) == "%0D" ) {
                letter = "New Line";
            } // If - To check if 
            if ( escape(letter) == " " ) {
                letter = "Space";
            } // If - To check if 
            //alert("Invalid character in " + ctrlname + " : " + letter + "\nValid characters are " + chars);
            bool_valid=false;
            break;
        //}		
	}
	// Returns the valid status of isvalid
	return bool_valid;
}
/*
 * Function      : isFileAcceptable(oFileField, sFileTypes)
 * Description   : This function checks weather the specified file is acceptable or not
 * Inputs        : Argument    : oFileField
 *                 Datatype    : Object
 *                 Description : File field object, which contains the file path and name and has to be validated
 *                 Argument    : sFileTypes
 *                 Datatype    : String
 *                 Description : A semi-comma separated string containing the acceptable file types/extensions separated by semi-commas
 * Outputs       : Argument    : None
 * Returns       : Boolean value, true/false depending on the validation done
 *                 Return Value : true
 *                 Condition    : When all the parameters are valid and file type is acceptable
 *                 Return Value : false
 *                 Condition    : When any of the inputs are not valid or file type is not acceptable
 */
function isFileAcceptable(oFileField, sFileTypes) {
	var strFilePath = Trim(oFileField);
	var intPos=0;
	var strFileType="";
	var strFileTypeArray;
	
	// Checking whether the file is specified/selected or not
	if (strFilePath == "") {
		alert("Please specify the file to proceed further");
		return false;
	}
	// Checking whether the semi-comma separated string containing the acceptable file types are specified or not
	if (sFileTypes == "") {
		alert("Acceptable file types should be specified for verification");
		return false;
	}
	// Retrieving the specified file type/extension
	intPos = strFilePath.lastIndexOf(".") + 1;
	strFileType = strFilePath.substring(intPos);	
	// Spliting the semi-comma separated string containing the acceptable file types/extensions into an array
	strFileTypeArray = sFileTypes.split(";");
	sFileTypes = "";
	// Checking whether the specified file type exists in the array or not
	if (strFileTypeArray.length > 0) {
		sFileTypes = sFileTypes + " " + strFileTypeArray[0];
		if (strFileTypeArray[0].toLowerCase() == strFileType.toLowerCase()) {
			return true;
		}
		
		for (var i=1; i < strFileTypeArray.length; i++) {
			sFileTypes = sFileTypes + ", " + strFileTypeArray[i];
			if (strFileTypeArray[i].toLowerCase() == strFileType.toLowerCase()) {
				return true;
			}
	   }
	}
	
	
	alert("Files with extensions" + sFileTypes + " can only be uploaded");
	oFileField.focus();
	oFileField.select();
	return false;
}
/*
 * Function Name     :  isValidDate
 * Description       :  This function checks the date entered is valid and checks for future date
 * Inputs            :  day - Represents the date part of the date
 *                      month - Represents the month part of the date
 *                      year - Represents the year part of the date
 * Returns           :  Returns the error number 
 *                      1. Invalid Month
 *                      2. Invalid Date
 *                      3. Invalid Year
 * Sample Usage      :  isValidCharacter=isDate(10,12,2005) 
 *                          In this case returns 0
 *                      isValidCharacter=isDate(10,42,2005) 
 *                          In this case returns 1
 */
function isDate(day, month, year) {
	// parse date into variables
	int_month = Trim(month.value);
	int_day = Trim(day.value);
	int_year = Trim(year.value);
	int_err = 0;
    
	// check month range
	if (int_month < 1 || int_month > 12) { 
		alert("Invalid Month");
		int_err = 1;
	}
	// check date range
	if (int_day < 1 || int_day > 31) {
		alert("Invalid Date");
		int_err = 2;
	}
	// check date range for even months
	else if ((int_month==4 || int_month==6 || int_month==9 || int_month==11) && int_day==31) {
		alert("Invalid Date");
		int_err = 2;
	}
	// check year range
	else if (int_year < 1800) {
		alert("Year must not be lesser then 1800");
		int_err = 3;
	}
	// check for february 29th
	else if (int_month == 2) {
		var isleap = false;		
		if (int_year % 100 == 0) {
			if (int_year % 400 == 0) { 
				isleap =  true; 
			}
		} else {
			if ((int_year % 4) == 0) {
				isleap = true; 
			}
		}		
		if (int_day > 29 || (int_day==29 && !isleap)) {
			alert("Invalid Date");
			int_err = 2;
		}		
	}
	else {
        var dtObj = new Date();
        //var Dt_Today = "" + dtObj.getYear() + (parseInt(dtObj.getMonth())+1) + dtObj.getDate();
        var Dt_Today = "" ;
        Dt_Today = Dt_Today + dtObj.getYear() ;
        if ( (dtObj.getMonth()+1) < 10 ) {
            Dt_Today = Dt_Today + "0" + (parseInt(dtObj.getMonth())+1) ;
        }
        else {
            Dt_Today = Dt_Today + (parseInt(dtObj.getMonth())+1) ;
        }
        if ( dtObj.getDate() < 10 ) {
            Dt_Today = Dt_Today + "0" + dtObj.getDate() ;
        }
        else {
            Dt_Today = Dt_Today + dtObj.getDate() ;
        }
        
        var Dt_Check = "" ;
        Dt_Check = Dt_Check + int_year ;
        if ( int_month.length == 1 ) {
            Dt_Check = Dt_Check + "0" + int_month ;
        }
        else {
            Dt_Check = Dt_Check + int_month ;
        }
        if ( int_day.length == 1 ) {
            Dt_Check = Dt_Check + "0" + int_day ;
        }
        else {
            Dt_Check = Dt_Check + int_day ;
        }
        //var Dt_Check = "" +  int_year + int_month + int_day ;
        
        //alert("Dt_Today = " + Dt_Today + "\nDt_Check = " + Dt_Check) ;
        if ( Dt_Today < Dt_Check ) {
            alert("Future date is not allowed") ;
            int_err = 1 ;
        }
    }
    // Returns the valid error number
	// 1. Invalid Month
	// 2. Invalid Date
	// 3. Invalid Year
	return int_err;
}
/*
 * Function Name :  isValidDate
 * Description       :  This function checks the date entered is valid and does  not check for future date
 * Inputs              :  day - Represents the date part of the date
 *                            month - Represents the month part of the date
 *                            year - Represents the year part of the date
 * Returns            :  Returns the error number 
 *                            1. Invalid Month
 *                             2. Invalid Date
 *                             3. Invalid Year
 * Sample Usage:  isValidCharacter=isValidDate(10,12,2005) 
 *                               In this case returns 0
 *                           isValidCharacter=isValidDate(10,42,2005) 
 *                               In this case returns 1
 */
function isValidDate(day, month, year) {
	// parse date into variables
	int_month = Trim(month.value);
	int_day = Trim(day.value);
	int_year = Trim(year.value);
	int_err = 0;

	// check month range
	if (int_month < 1 || int_month > 12) { 
		alert("Invalid Month");
		int_err = 1;
	}
	// check date range
	if (int_day < 1 || int_day > 31) {
		alert("Invalid Date");
		int_err = 2;
	}
	// check date range for even months
	else if ((int_month==4 || int_month==6 || int_month==9 || int_month==11) && int_day==31) {
		alert("Invalid Date");
		int_err = 2;
	}
	// check year range
	else if (isNaN(int_year) && int_year!="") {
		alert("Year must be a valid Number");
		int_err = 3;
	}
	// check year range
	else if (int_year < 1600) {
		alert("Year must not be lesser then 1800");
		int_err = 3;
	}
    else if (int_year > 2100) {
		alert("Year must not be greater then 2100");
		int_err = 3;
	}
	// check for february 29th
	else if (int_month == 2) {
		var isleap = false;		
		if (int_year % 100 == 0) {
			if (int_year % 400 == 0) { 
				isleap =  true; 
			}
		} else {
			if ((int_year % 4) == 0) {
				isleap = true; 
			}
		}		
		if (int_day > 29 || (int_day==29 && !isleap)) {
			alert("Invalid Date");
			int_err = 2;
		}		
	}
	else {
        var dtObj = new Date();
        //var Dt_Today = "" + dtObj.getYear() + (parseInt(dtObj.getMonth())+1) + dtObj.getDate();
        var Dt_Today = "" ;
        Dt_Today = Dt_Today + dtObj.getYear() ;
        if ( (dtObj.getMonth()+1) < 10 ) {
            Dt_Today = Dt_Today + "0" + (parseInt(dtObj.getMonth())+1) ;
        }
        else {
            Dt_Today = Dt_Today + (parseInt(dtObj.getMonth())+1) ;
        }
        if ( dtObj.getDate() < 10 ) {
            Dt_Today = Dt_Today + "0" + dtObj.getDate() ;
        }
        else {
            Dt_Today = Dt_Today + dtObj.getDate() ;
        }
        
        var Dt_Check = "" ;
        Dt_Check = Dt_Check + int_year ;
        if ( int_month.length == 1 ) {
            Dt_Check = Dt_Check + "0" + int_month ;
        }
        else {
            Dt_Check = Dt_Check + int_month ;
        }
        if ( int_day.length == 1 ) {
            Dt_Check = Dt_Check + "0" + int_day ;
        }
        else {
            Dt_Check = Dt_Check + int_day ;
        }
        //var Dt_Check = "" +  int_year + int_month + int_day ;
        
        //alert("Dt_Today = " + Dt_Today + "\nDt_Check = " + Dt_Check) ;
        //if ( Dt_Today < Dt_Check ) {
        //    alert("Future date is not allowed") ;
        //    int_err = 1 ;
        //}
    }
    // Returns the valid error number
	// 1. Invalid Month
	// 2. Invalid Date
	// 3. Invalid Year
	return int_err;
}
/* 
 *   Function Name :  formatDate
 *   Description       : This function splits the date parts and rearranges them as MM-DD-YYYY format
 *                              If the date is null then it is set as 01-01-0001
 *   Input                : 	strDate - Date String which is to be formatted
 *   Returns            : The flag which is set to true or false is returned accordingly
 *   Sample Usage  : isValidCharacter=formatDate('2005-03-29') returns 03-29-2005 
 */
function formatDate(strDate) {
    var strTempArray ;
    var strReturnDate ;
    
    if ( strDate != "" ) {
        strTempArray = strDate.split("-") ;
        strReturnDate = strTempArray[1] + "-" + strTempArray[2] + "-" + strTempArray[0] ;
        document.tdDispDate.innerHTML = strReturnDate ;
    }
    else {
    	document.tdDispDate.innerHTML = "01-01-0001" ;
    }
} //End of Function
/* 
 *   Function Name : compareDates
 *   Description   : This function is used to compare the difference between the two dates
 *   Input         : pDate1 - Passes the value on which the check is to be performed
                     pDate2 - Passes the value on which the check is to be performed
                     Note: Date format should be MM/dd/yyyy (delimiter can change)
 *   Returns       :  0 - If pDate1 and pDate2 are equal
                     -1 - If pDate1 is less than pDate2
                      1 - If pDate1 is greater than pDate2
 */
function compareDates(pDate1, pDate2, delimiter) {
    var tempDate1 ;
    var tempDate2 ;
    var date1Year=0 ;
    var date2Year=0 ;
    var date1Month=0 ;
    var date2Month=0 ;
    var date1Day=0 ;
    var date2Day=0 ;
    var strDate1 ;
    var strDate2 ;
    var returnValue=2 ;
    
    pDate1 = (pDate1.split(" "))[0];
    pDate2 = (pDate2.split(" "))[0];
    
    tempDate1=pDate1.split(delimiter) ;
    tempDate2=pDate2.split(delimiter) ;
    
    date1Month = Trim(tempDate1[0]);
    date1Day = Trim(tempDate1[1]);
    date1Year = Trim(tempDate1[2]);
    strDate1 = "" ;
    strDate1 = strDate1 + date1Year;
    
    if ( date1Month.length == 3 ) {
        date1Month = getMonthNumber(date1Month);
    }
    if ( date1Month.length == 1 ) strDate1 = strDate1 + "0" + date1Month ;
    else strDate1 = strDate1 + date1Month ;
    if ( date1Day.length == 1 ) strDate1 = strDate1 + "0" + date1Day ;
    else strDate1 = strDate1 + date1Day ;
    
    date2Month = Trim(tempDate2[0]);
    date2Day = Trim(tempDate2[1]);
    date2Year = Trim(tempDate2[2]);
    strDate2 = "" ;
    strDate2 = strDate2 + date2Year;
    
    if ( date2Month.length == 3 ) {
        date2Month = getMonthNumber(date2Month);
    }
    if ( date2Month.length == 1 ) strDate2 = strDate2 + "0" + date2Month ;
    else strDate2 = strDate2 + date2Month ;
    if ( date2Day.length == 1 ) strDate2 = strDate2 + "0" + date2Day ;
    else strDate2 = strDate2 + date2Day ;
    
    // if date1 is less than date2 then return -1
    if ( strDate1 < strDate2 ) {
        returnValue = -1;
    }
    // if date1 is greater than date2 then return 1
    if ( strDate1 > strDate2 ) {
        returnValue= 1;
    }
    // if date1 and date2 are equal return 0;
    if ( strDate1 == strDate2 ) {
        returnValue= 0;
    }
    
    //returns the value
    return returnValue;
}// End of Function

function getMonthNumber(monthName) {
    var monthNumber = '00';
    
    if ( monthName.length == 3 ) {
        if ( monthName.toUpperCase()=='JAN' ) {
            monthNumber = '01';
        }
        if ( monthName.toUpperCase()=='FEB' ) {
            monthNumber = '02';
        }
        if ( monthName.toUpperCase()=='MAR' ) {
            monthNumber = '03';
        }
        if ( monthName.toUpperCase()=='APR' ) {
            monthNumber = '04';
        }
        if ( monthName.toUpperCase()=='MAY' ) {
            monthNumber = '05';
        }
        if ( monthName.toUpperCase()=='JUN' ) {
            monthNumber = '06';
        }
        if ( monthName.toUpperCase()=='JUL' ) {
            monthNumber = '07';
        }
        if ( monthName.toUpperCase()=='AUG' ) {
            monthNumber = '08';
        }
        if ( monthName.toUpperCase()=='SEP' ) {
            monthNumber = '09';
        }
        if ( monthName.toUpperCase()=='OCT' ) {
            monthNumber = '10';
        }
        if ( monthName.toUpperCase()=='NOV' ) {
            monthNumber = '11';
        }
        if ( monthName.toUpperCase()=='DEC' ) {
            monthNumber = '12';
        }
    }
    return monthNumber;
}

/* 
 *   Function Name : compareDateWithToday
 *   Description   : This function is used to compare the difference between the two dates
 *   Input         : pDate - Passes the value on which the check is to be performed
 *                   Note: Date format should be MM/dd/yyyy (delimiter can change)
 *   Returns       :  0 - If pDate is equal to current date
 *                   -1 - If pDate is greater than current date
 *                    1 - If pDate is less than current date
 */
function compareDateWithToday(pDate, delimiter) {
    var tempDate2 ;
    var date1Year=0 ;
    var date2Year=0 ;
    var date1Month=0 ;
    var date2Month=0 ;
    var date1Day=0 ;
    var date2Day=0 ;
    var strDate1 ;
    var strDate2 ;
    var returnValue=2 ;
    
    var current_date = new Date();
    date1Month = current_date.getMonth()+1;
    date1Day = current_date.getDate();
    date1Year = current_date.getFullYear();
    
    strDate1 = "" ;
    strDate1 = strDate1 + date1Year ;
    if ( date1Month < 10 ) strDate1 = strDate1 + "0" + date1Month ;
    else strDate1 = strDate1 + date1Month ;
    if ( date1Day < 10 ) strDate1 = strDate1 + "0" + date1Day ;
    else strDate1 = strDate1 + date1Day ;
    
    pDate = (pDate.split(" "))[0];
    tempDate2=pDate.split(delimiter);
    date2Month = Trim(tempDate2[0]);
    date2Day = Trim(tempDate2[1]);
    date2Year = Trim(tempDate2[2]);
    strDate2 = "" ;
    strDate2 = strDate2 + date2Year ;
    if ( date2Month.length == 1 ) strDate2 = strDate2 + "0" + date2Month ;
    else strDate2 = strDate2 + date2Month ;
    if ( date2Day.length == 1 ) strDate2 = strDate2 + "0" + date2Day ;
    else strDate2 = strDate2 + date2Day ;
    
    // if date1 is less than date2 then return -1
    if ( strDate1 < strDate2 ) {
        returnValue = -1;
    }
    // if date1 is greater than date2 then return 1
    if ( strDate1 > strDate2 ) {
        returnValue= 1;
    }
    // if date1 and date2 are equal return 0;
    if ( strDate1 == strDate2 ) {
        returnValue= 0;
    }
    //returns the value
    return returnValue;
}// End of Function
function selectAllOptions(objSelect) {
    if ( objSelect!=null && objSelect.options.length>0 ) {
        for (i=0; i<objSelect.options.length; i++) {
            objSelect.options[i].selected = true;
        }
    }
}
function getHelp(helpId) {
	PopWin = window.open('help.do?invoke=viewHelp&helpId='+helpId,'','height=282,width=513,resizable=yes,menubar=no,scrollbars=yes,toolbar=no,left=100,top=160,status=yes');
	PopWin.focus();
}
function openViewFeedbackForm(sUrl){
	document.location = sUrl ;
}

