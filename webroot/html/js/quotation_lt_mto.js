function processMTOHighlightFlagValues() {
	var totalRatingIndex = $('#totalRatingIndex').val();
	
	for(i=1; i<=totalRatingIndex; i++) {
		var index = i;
		var mtoHighlightFlgVal = $("#ratingMTOHighlightFlag" + index).val();
		
		if(mtoHighlightFlgVal != null && mtoHighlightFlgVal != '') {
			const mtoHighlightArray = mtoHighlightFlgVal.split("|");
			
			if(mtoHighlightArray.indexOf('0') >= 0) {
				$("#td_productLine" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#productLine" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
				$("#td_kw" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#kw" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
				$("#td_pole" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#pole" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
				$("#td_frame" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#frame" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
				$("#td_framesuffix" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#framesuffix" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
				$("#td_mounting" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#mounting" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
				$("#td_tbPosition" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#tbPosition" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
			} 
			if(mtoHighlightArray.indexOf('1') >= 0) {
				$("#td_pole" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#pole" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
			} 
			if(mtoHighlightArray.indexOf('2') >= 0) {
				$("#td_serviceFactor" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#serviceFactor" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
			} 
			if(mtoHighlightArray.indexOf('3') >= 0) {
				$("#td_methodOfStarting" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#methodOfStarting" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
			} 
			if(mtoHighlightArray.indexOf('4') >= 0) {
				$("#td_startingCurrOnDOLStarting" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#startingCurrOnDOLStarting" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
			} 
			if(mtoHighlightArray.indexOf('5') >= 0) {
				$("#td_ambientTempSub" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#ambientTempSub" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
			} 
			if(mtoHighlightArray.indexOf('6') >= 0) {
				$("#td_altitude" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#altitude" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
			} 
			if(mtoHighlightArray.indexOf('7') >= 0) {
				$("#td_dustGroup" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#dustGroup" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
			} 
			if(mtoHighlightArray.indexOf('8') >= 0) {
				$("#td_noiseLevel" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#noiseLevel" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
			} 
			if(mtoHighlightArray.indexOf('9') >= 0) {
				$("#td_isReplacementMotor" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#isReplacementMotor" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
				$("#td_earlierSuppliedMotorSerialNo" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#earlierSuppliedMotorSerialNo" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
			} 
			if(mtoHighlightArray.indexOf('10') >= 0) {
				$("#td_lead" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#lead" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
			} 
			if(mtoHighlightArray.indexOf('11') >= 0) {
				$("#td_loadGD2" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#loadGD2" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
			} 
			if(mtoHighlightArray.indexOf('12') >= 0) {
				$("#td_vfdType" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#vfdType" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
			} 
			if(mtoHighlightArray.indexOf('13') >= 0) {
				$("#td_vfdSpeedRangeMin" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#vfdSpeedRangeMin" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
			} 
			if(mtoHighlightArray.indexOf('14') >= 0) {
				$("#td_vfdSpeedRangeMax" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#vfdSpeedRangeMax" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
			} 
			if(mtoHighlightArray.indexOf('15') >= 0) {
				$("#td_overloadingDuty" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#overloadingDuty" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
			} 
			if(mtoHighlightArray.indexOf('16') >= 0) {
				$("#td_constantEfficiencyRange" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#constantEfficiencyRange" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
			} 
			if(mtoHighlightArray.indexOf('17') >= 0) {
				$("#td_shaftMaterialType" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#shaftMaterialType" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
			} 
			if(mtoHighlightArray.indexOf('18') >= 0) {
				$("#td_methodOfCoupling" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#methodOfCoupling" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
			} 
			if(mtoHighlightArray.indexOf('19') >= 0) {
				$("#td_paintShade" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#paintShade" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
				$("#td_paintShadeVal" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#paintShadeVal" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
			} 
			if(mtoHighlightArray.indexOf('20') >= 0) {
				$("#td_paintThickness" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#paintThickness" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
			} 
			if(mtoHighlightArray.indexOf('21') >= 0) {
				$("#td_ip" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#ip" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
			} 
			if(mtoHighlightArray.indexOf('22') >= 0) {
				$("#td_terminalBoxSize" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#terminalBoxSize" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
			} 
			if(mtoHighlightArray.indexOf('23') >= 0) {
				$("#td_spaceHeater" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#spaceHeater" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
			} 
			if(mtoHighlightArray.indexOf('24') >= 0) {
				$("#td_flyingLeadWithoutTB" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#flyingLeadWithoutTB" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
			} 
			if(mtoHighlightArray.indexOf('25') >= 0) {
				$("#td_flyingLeadWithTB" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#flyingLeadWithTB" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
			} 
			if(mtoHighlightArray.indexOf('26') >= 0) {
				$("#td_metalFan" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#metalFan" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
			} 
			if(mtoHighlightArray.indexOf('27') >= 0) {
				$("#td_techoMounting" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#techoMounting" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
			} 
			if(mtoHighlightArray.indexOf('28') >= 0) {
				$("#td_shaftGrounding" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#shaftGrounding" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
			} 
			if(mtoHighlightArray.indexOf('29') >= 0) {
				$("#td_hardware" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#hardware" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
			} 
			if(mtoHighlightArray.indexOf('30') >= 0) {
				$("#td_glandPlate" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#glandPlate" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
			} 
			if(mtoHighlightArray.indexOf('31') >= 0) {
				$("#td_spmMountingProvision" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#spmMountingProvision" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
			} 
			if(mtoHighlightArray.indexOf('32') >= 0) {
				$("#td_rtd" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#rtd" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
			} 
			if(mtoHighlightArray.indexOf('33') >= 0) {
				$("#td_btd" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#btd" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
			} 
			if(mtoHighlightArray.indexOf('34') >= 0) {
				$("#td_thermister" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#thermister" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
			} 
			if(mtoHighlightArray.indexOf('35') >= 0) {
				$("#td_bearingSystem" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#bearingSystem" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
			} 
			if(mtoHighlightArray.indexOf('36') >= 0) {
				$("#td_bearingDE" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#bearingDE" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
			} 
			if(mtoHighlightArray.indexOf('37') >= 0) {
				$("#td_combinedVariation" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#combinedVariation" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
			} 
			if(mtoHighlightArray.indexOf('38') >= 0) {
				$("#td_perfCurves" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#perfCurves" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
			} 
			if(mtoHighlightArray.indexOf('39') >= 0) {
				$("#td_dataSheet" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#dataSheet" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
			} 
			if(mtoHighlightArray.indexOf('40') >= 0) {
				$("#td_motorGA" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#motorGA" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
			} 
			if(mtoHighlightArray.indexOf('41') >= 0) {
				$("#td_tBoxGA" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#tBoxGA" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
			}
			if(mtoHighlightArray.indexOf('42') >= 0) {
				$("#td_rv" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#rv" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
				$("#td_ra" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#ra" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
			}
			if(mtoHighlightArray.indexOf('43') >= 0) {
				$("#td_methodOfCooling" + index).attr("style", "BACKGROUND-COLOR:#aff299;");
				$("#methodOfCooling" + index).attr("style", "width:100px;BACKGROUND-COLOR:#aff299;");
			}
		}
	}
}

function reassignTo() {
	if($("#reassignToId").val() == '' || $("#reassignToId").val() == '0') {
		alert("Please select Re-assign to User.");
		$("#reassignToId").focus();
		return false;
	}
	if($("#reassignRemarks").val() == null || $("#reassignRemarks").val().trim() == '') {
		alert("Please enter Comments before Re-assigning to a different User.");
		$("#reassignRemarks").focus();
		return false;
	}
	
	document.newmtoenquiryForm.action="newMTOEnquiry.do";
	document.newmtoenquiryForm.invoke.value="reassignMTOEnquiry";
	document.newmtoenquiryForm.submit();
}

function searchMTORatingDetails() {
	document.newmtoenquiryForm.action="newMTOEnquiry.do";
	document.newmtoenquiryForm.invoke.value="searchMTORatings";
	document.newmtoenquiryForm.submit();
}

function removeAddOn(src,idx) {
	var oRow = src.parentElement.parentElement;	
	var tbl=document.getElementById("mto_addons_table"+idx);
	var Rowlength = tbl.rows.length;
	
	document.all("mto_addons_table"+idx).deleteRow(oRow.rowIndex);
}

function createAddOn(tblIdx) {
	var tbl = document.getElementById("mto_addons_table"+tblIdx);
	var rowIdx =  $("#addRowVal"+tblIdx).val();
	
	var addOnTypeVal = $("#addOnType" + tblIdx + "_" + rowIdx).val();
	var addOnQtyVal = $("#addOnQuantity" + tblIdx + "_" + rowIdx).val();
	var addOnPercentVal = $("#addOnPercent" + tblIdx + "_" + rowIdx).val();
	var addOnCashVal = $("#addOnCash" + tblIdx + "_" + rowIdx).val();
	
	var mtoAddOnsData = jsonMTOAddOnsJS;
	//alert('mtoAddOnsData ...' + mtoAddOnsData);
	
	var onlyNumericChars = /^[0-9]+$/;
	
	if (addOnTypeVal != null && addOnTypeVal == '0') {
		alert("Please enter Add-on Type value before adding a new Add-on.");
		$("#addOnType" + tblIdx + "_" + rowIdx).focus();
		return false;
	}
	
	if (addOnQtyVal != null && (addOnQtyVal == '' || !onlyNumericChars.test(Trim(addOnQtyVal))) ) {
		alert("Please enter Add-on Quantity before adding a new Add-on.. Quantity should be a Number.");
		$("#addOnQuantity" + tblIdx + "_" + rowIdx).focus();
		return false;
	}
	
	if (addOnPercentVal != null && addOnCashVal != null && (addOnPercentVal == '' && addOnCashVal == '')) {
		alert("Please enter either Add-on Percent or Add-on Cash value before adding a new Add-on.");
		if(addOnPercentVal == '') {
			$("#addOnPercent" + tblIdx + "_" + rowIdx).focus();
		} else {
			$("#addOnCash" + tblIdx + "_" + rowIdx).focus();
		}
		return false;
	}
	
	var newRow = tbl.insertRow();
	var nextRow = parseInt(rowIdx) + 1;
	
	// Set nextRow value to "addRowVal".
	$("#addRowVal"+tblIdx).val(nextRow);
	
	var oCell  = newRow.insertCell();
	var oCell1 = newRow.insertCell();
	var oCell2 = newRow.insertCell();
	var oCell3 = newRow.insertCell();
	var oCell4 = newRow.insertCell();
	
	// 1st column : CSS and HTML
	oCell.style.cssText = "width:35%; padding:4px; border-bottom: 1px solid black;";
	oCell.innerHTML	= '<select name="addOnType'+tblIdx+'_'+nextRow+'" id="addOnType'+tblIdx+'_'+nextRow+'" style="width:300px; float:left;"> </select> ';
	$("#addOnType"+tblIdx+"_"+nextRow).append('<option value="0">Select</option>');
	$.each(mtoAddOnsData, function(index, value) {
		$("#addOnType"+tblIdx+"_"+nextRow).append('<option value='+index+'>'+value+'</option>');
	});
	
	// 2nd column : CSS and HTML
	oCell1.style.cssText = "width:15%; padding:4px; border-bottom: 1px solid black;";
	oCell1.innerHTML = '<input id="addOnQuantity'+tblIdx+'_'+nextRow+'" name="addOnQuantity'+tblIdx+'_'+nextRow+'" onkeypress="return isNumber(event)" maxlength="2" class="short" >';
					  
	// 3rd column : CSS and HTML	  
	oCell2.style.cssText = "width:15%; padding:4px; border-bottom: 1px solid black;";		  
	oCell2.innerHTML = '<input id="addOnPercent'+tblIdx+'_'+nextRow+'" name="addOnPercent'+tblIdx+'_'+nextRow+'" onkeypress="return isNumber(event)" maxlength="8" class="short" >';
	
	// 4th column : CSS and HTML
	oCell3.style.cssText = "width:20%; padding:4px; border-bottom: 1px solid black;";
	oCell3.innerHTML = '&#8377; &nbsp; <input id="addOnCash'+tblIdx+'_'+nextRow+'" name="addOnCash'+tblIdx+'_'+nextRow+'" onkeypress="return isNumber(event)" maxlength="8" class="normal" >';
	
	// 5th column : CSS and HTML
	oCell4.style.cssText = "width:15%; padding:4px; border-bottom: 1px solid black;";
	oCell4.innerHTML = '<img src="html/images/deleteicon.gif" alt="Delete" onclick="javascript:removeAddOn(this,'+tblIdx+');"  id="delete'+tblIdx+'_'+nextRow+'" style="cursor:hand;" >';
	
}

function calculateMTOAddOns(tblIdx) {
	var tbl = document.getElementById("mto_addons_table"+tblIdx);
	var tblRowLength = $("#addRowVal"+tblIdx).val();
	
	var stdAddOnPercent = 0, stdAddOnCash = 0, splAddOnPercent = 0, splAddOnCash = 0;
		
	for(var idx=1; idx <= tblRowLength; idx++) {
		var addOnTypeVal = $("#addOnType" + tblIdx + "_" + idx).val();
		var addOnQtyVal = $("#addOnQuantity" + tblIdx + "_" + idx).val();
		var addOnPercentVal = $("#addOnPercent" + tblIdx + "_" + idx).val();
		var addOnCashVal = $("#addOnCash" + tblIdx + "_" + idx).val(); 
		
		if (addOnTypeVal != null && addOnTypeVal != '' && addOnTypeVal != '0') {
			
			if(addOnQtyVal != '') {
				addOnQtyVal = parseInt(addOnQtyVal);
			}
			
			if(addOnPercentVal != '') {
				splAddOnPercent = splAddOnPercent + (parseInt(addOnPercentVal) * addOnQtyVal);
			}
			if(addOnCashVal != '') {
				splAddOnCash = splAddOnCash + (parseInt(addOnCashVal) * addOnQtyVal);
			}
		}
	}
	
	// Setting Special Add-On values.
	$("#mtoSpecialAddOnPercent" + tblIdx).val(splAddOnPercent);
	$("#mtoSpecialAddOnCash" + tblIdx).val(splAddOnCash);
	
	if($("#mtoStandardAddOnPercent"+tblIdx).val() != '') {
		stdAddOnPercent = parseFloat($("#mtoStandardAddOnPercent"+tblIdx).val());
	}
	if($("#mtoStandardAddOnCash"+tblIdx).val() != '') {
		stdAddOnCash = parseFloat($("#mtoStandardAddOnCash"+tblIdx).val());
	}
	
	// Calculate Total Add-On values.
	var totalAddOnPercent = 0, totalAddOnCash = 0;
	var stampPercentVal = 0;
	var copperPercentVal = 0;
	var aluminiumPercentVal = 0;
	
	if($("#mtoPercentStamping"+tblIdx).val() != '') {
		stampPercentVal = parseFloat($("#mtoPercentStamping"+tblIdx).val());
	}
	if($("#mtoPercentCopper"+tblIdx).val() != '') {
		copperPercentVal = parseFloat($("#mtoPercentCopper"+tblIdx).val());
	}
	if($("#mtoPercentAluminium"+tblIdx).val() != '') {
		aluminiumPercentVal = parseFloat($("#mtoPercentAluminium"+tblIdx).val());
	}
	
	// Applying Copper/Stamping/Aluminium Markup Rule.
	totalAddOnPercent = (stampPercentVal * 0.35) + (copperPercentVal * 0.35) + (aluminiumPercentVal * 0.1);
	
	// Applying Ceiling value.
	totalAddOnPercent = Math.ceil(totalAddOnPercent);
	
	if(totalAddOnPercent > 0 && totalAddOnPercent <= 5) {
		totalAddOnPercent = 5;
	} else if(totalAddOnPercent > 5 && totalAddOnPercent <= 10) {
		totalAddOnPercent = 10;
	} else if(totalAddOnPercent > 10 && totalAddOnPercent <= 15) {
		totalAddOnPercent = 15;
	} else if(totalAddOnPercent > 15 && totalAddOnPercent <= 20) {
		totalAddOnPercent = 20;
	} else if(totalAddOnPercent > 20 && totalAddOnPercent <= 25) {
		totalAddOnPercent = 25;
	} else if(totalAddOnPercent > 25 && totalAddOnPercent <= 30) {
		totalAddOnPercent = 30;
	}
	
	totalAddOnPercent = totalAddOnPercent + splAddOnPercent + stdAddOnPercent;
	totalAddOnCash = totalAddOnCash + splAddOnCash + stdAddOnCash;
	
	$("#mtoTotalAddOnPercent" + tblIdx).val(totalAddOnPercent);
	$("#mtoTotalCash" + tblIdx).val(totalAddOnCash);
	
}

function calculateMTOTechValues(tblIdx) {
	var stdAddOnPercent = 0, stdAddOnCash = 0, splAddOnPercent = 0, splAddOnCash = 0;
	var totalAddOnPercent = 0, totalAddOnCash = 0;
	var stampPercentVal = 0;
	var copperPercentVal = 0;
	var aluminiumPercentVal = 0;
	
	if($("#mtoStandardAddOnPercent"+tblIdx).val() != '') {
		stdAddOnPercent = parseFloat($("#mtoStandardAddOnPercent"+tblIdx).val());
	}
	if($("#mtoStandardAddOnCash"+tblIdx).val() != '') {
		stdAddOnCash = parseFloat($("#mtoStandardAddOnCash"+tblIdx).val());
	}
	if($("#mtoSpecialAddOnPercent"+tblIdx).val() != '') {
		splAddOnPercent = parseFloat($("#mtoSpecialAddOnPercent"+tblIdx).val());
	}
	if($("#mtoSpecialAddOnCash"+tblIdx).val() != '') {
		splAddOnCash = parseFloat($("#mtoSpecialAddOnCash"+tblIdx).val());
	}
	if($("#mtoPercentStamping"+tblIdx).val() != '') {
		stampPercentVal = parseFloat($("#mtoPercentStamping"+tblIdx).val());
	}
	if($("#mtoPercentCopper"+tblIdx).val() != '') {
		copperPercentVal = parseFloat($("#mtoPercentCopper"+tblIdx).val());
	}
	if($("#mtoPercentAluminium"+tblIdx).val() != '') {
		aluminiumPercentVal = parseFloat($("#mtoPercentAluminium"+tblIdx).val());
	}
	
	// Applying Copper/Stamping/Aluminium Markup Rule.
	totalAddOnPercent = (stampPercentVal * 0.35) + (copperPercentVal * 0.35) + (aluminiumPercentVal * 0.1);
	
	// Applying Ceiling value.
	totalAddOnPercent = Math.ceil(totalAddOnPercent);
	
	if(totalAddOnPercent > 0 && totalAddOnPercent <= 5) {
		totalAddOnPercent = 5;
	} else if(totalAddOnPercent > 5 && totalAddOnPercent <= 10) {
		totalAddOnPercent = 10;
	} else if(totalAddOnPercent > 10 && totalAddOnPercent <= 15) {
		totalAddOnPercent = 15;
	} else if(totalAddOnPercent > 15 && totalAddOnPercent <= 20) {
		totalAddOnPercent = 20;
	} else if(totalAddOnPercent > 20 && totalAddOnPercent <= 25) {
		totalAddOnPercent = 25;
	} else if(totalAddOnPercent > 25 && totalAddOnPercent <= 30) {
		totalAddOnPercent = 30;
	}
	
	totalAddOnPercent = totalAddOnPercent + splAddOnPercent + stdAddOnPercent;
	totalAddOnCash = totalAddOnCash + splAddOnCash + stdAddOnCash;
	
	$("#mtoTotalAddOnPercent" + tblIdx).val(totalAddOnPercent);
	$("#mtoTotalCash" + tblIdx).val(totalAddOnCash);
}

function applyMTOMaterialCost(tblIdx) {
	
	// If Special Add-on Percent or Cash or Markup(Cu, Al and Stamping) is not Empty - Then Show Alert that Material Cost will override Special Add-on Percent/Cash.
	if($("#mtoSpecialAddOnPercent"+tblIdx).val() != '' || $("#mtoSpecialAddOnCash"+tblIdx).val() != '' || $("#mtoPercentStamping"+tblIdx).val() != ''
		|| $("#mtoPercentCopper"+tblIdx).val() != '' || $("#mtoPercentAluminium"+tblIdx).val() != '') {
		
		// alert("The value for Special Add-on Percent, Cash or Markup(% Copper, Aluminium and Stamping) is already provided. \n If Material Cost is entered then Special Add-on Percent or Cash will be overridden.");
		var acptMessage = confirm("The value for Special Add-on Percent, Cash or Markup(% Copper, Aluminium and Stamping) is already provided. \n Are you sure you want to override Special Add-on Percent or Cash or Markup with Material Cost ?");
		
		if (acptMessage == false) {
			return false;
		}
	}
		
	// Here : Write Logic for Applying Material Cost.
	
}

function validateMTOPriceCheck(mtoId) {
	
	var customerId = $("#customerId").val(); 
	var enquiryId = $("#enquiryId").val();
	var ratingNo = $("#mtoRatingNo"+mtoId).val();
	var quantity = $("#mtoQuantity"+mtoId).val();
	var mtoMfgLocation = $("#mtoMfgLocation"+mtoId).val();
	var mtoProdLineVal = $("#mtoProdLine"+mtoId).val();
	var mtoKWVal = $("#mtoKw"+mtoId).val();
	var mtoPoleVal = $("#mtoPole"+mtoId).val();
	var mtoFrameVal = $("#mtoFrame"+mtoId).val();
	var mtoFrameSuffixVal = $("#mtoFrameSuffix"+mtoId).val();
	var mtoMountingVal = $("#mtoMounting"+mtoId).val();
	var mtoTBPosVal = $("#mtoTBPosition"+mtoId).val();
	
	var specialAddOnPercentVal = $("#mtoSpecialAddOnPercent"+mtoId).val();
	var specialAddOnCashVal = $("#mtoSpecialAddOnCash"+mtoId).val();
	var percentStampingVal = $("#mtoPercentStamping"+mtoId).val();
	var percentCopperVal = $("#mtoPercentCopper"+mtoId).val();
	var percentAluminiumVal = $("#mtoPercentAluminium"+mtoId).val();
	var totalAddOnPercentVal = $("#mtoTotalAddOnPercent"+mtoId).val();
	var totalAddOnCashVal = $("#mtoTotalCash"+mtoId).val();
	
	var mtoMaterialCostVal = $("#mtoMaterialCost"+mtoId).val();
	var mtoDesignCompleteVal = $("#mtoDesignComplete"+mtoId).val();
	
	// If Design Complete = 'Yes' : Need to Check if ListPrice is available for ProductLine parameters combination.
	// *** NOTE : If Material Cost has specific value then Need not Do this check. ***
	if((mtoMaterialCostVal == '' || mtoMaterialCostVal == '0') && mtoDesignCompleteVal == 'Yes') {
		
		$.ajax({
			type : "POST",
			url : "newMTOEnquiry.do",
			dataType : "XML",
			data : {
				customerId : customerId,
				enquiryId : enquiryId,
				ratingNo : ratingNo,
				quantity : quantity,
				mtoMfgLocation : mtoMfgLocation,
				mtoProdLineVal : mtoProdLineVal,
				mtoKWVal : mtoKWVal,
				mtoPoleVal : mtoPoleVal,
				mtoFrameVal : mtoFrameVal,
				mtoFrameSuffixVal : mtoFrameSuffixVal,
				mtoMountingVal : mtoMountingVal,
				mtoTBPosVal : mtoTBPosVal,
				specialAddOnPercentVal : specialAddOnPercentVal,
				specialAddOnCashVal : specialAddOnCashVal,
				percentStampingVal : percentStampingVal,
				percentCopperVal : percentCopperVal,
				percentAluminiumVal : percentAluminiumVal,
				totalAddOnPercentVal : totalAddOnPercentVal,
				totalAddOnCashVal : totalAddOnCashVal,
				"invoke" : "validateMTOPriceCheckAjax"
			},
			success : function(data) {
				console.log($(data).find('msg').text());
				console.log($(data).find('returnValue1').text());
				if($(data).find('msg').text() == 'failure' ) {
					alert('The List Price for this Rating and ProductLine parameter combination is not available. Please choose Rating combination which has appropriate List Price value.');
					$("#mtoDesignComplete"+mtoId).val('No');
					return false;
				} else if($(data).find('msg').text() == 'success' ) {
					alert('List Price for this Rating has been verified successfully !');
				}
			},
			error : function(xhr, ajaxOptions, thrownError) {
				alert('The List Price for this Rating and ProductLine parameter combination could not be verified. Please choose Rating combination which has appropriate List Price value.');
				$("#mtoDesignComplete"+mtoId).val('No');
				return false;
			}
		});
		
	} else {
		return false;
	}
	
	
}

function submitMTOEnquiry(opType) {
	var enquiryId = $('#enquiryId').val();
	
	if(opType == 'DM') {
		$.ajax({
			type : "POST",
			url : "newMTOEnquiry.do",
			dataType : "XML",
			data : {
				enquiryId : enquiryId,
				"invoke" : "verifyMTODesignCompleteAjax"
			},
			success : function(data) {
				console.log($(data).find('msg').text());
				
				if($(data).find('msg').text() == 'success') {
					document.newmtoenquiryForm.action="newMTOEnquiry.do";
					document.newmtoenquiryForm.operationType.value=opType;
					document.newmtoenquiryForm.invoke.value="submitMTOEnquiryToSM";
					document.newmtoenquiryForm.submit();
				} else if($(data).find('msg').text() == 'failure' ) {
					alert("This Enquiry cannot be Submitted to Sales Manager as all MTO Ratings have not been Design Complete. \n Please move all MTO Ratings to Design Complete Status.");
					return false;
				}
			},
			error : function(xhr, ajaxOptions, thrownError) {
				return false;
			}
		});
	}
	else {
		document.newmtoenquiryForm.action="newMTOEnquiry.do";
		document.newmtoenquiryForm.operationType.value=opType;
		document.newmtoenquiryForm.invoke.value="submitMTOEnquiryToSM";
		document.newmtoenquiryForm.submit();
	}
	
}

function returnMTOEnquiry() {
	var dmComments = $('#designNote').val();
	
	if(dmComments == '') {
		alert('Please enter comments from Design Team, before returning the Enquiry to Sales Manager.');
		$('#designNote').focus();
		return false;
	}
	
	document.newmtoenquiryForm.action="newMTOEnquiry.do";
	document.newmtoenquiryForm.invoke.value="returnMTOEnquiryToSM";
	document.newmtoenquiryForm.submit();
}

function regretMTOEnquiry() {
	var dmComments = $('#designNote').val();
	
	if(dmComments == '') {
		alert('Please enter comments from Design Team, before moving the Enquiry to Regret.');
		$('#designNote').focus();
		return false;
	}
	
	var acptMessage = confirm("Are you sure you want this MTO Enquiry to move to Regret mode ?");
	
	if (acptMessage == false) {
		return false;
	}
	
	document.newmtoenquiryForm.action="newMTOEnquiry.do";
	document.newmtoenquiryForm.invoke.value="regretMTOEnquiry";
	document.newmtoenquiryForm.submit();
}

function saveMTOEnquiry() {
	document.newmtoenquiryForm.action="newMTOEnquiry.do";
	document.newmtoenquiryForm.invoke.value="saveMTOEnquiry";
	document.newmtoenquiryForm.submit();
}