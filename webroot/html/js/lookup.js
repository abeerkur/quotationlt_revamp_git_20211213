
function lookupEmployee(selectionType, userIdObjId, nameObjid, emailObjId, locObjId, parentListObj, employeeFilter) {
	var url = 'lookupEmployee.do?'
    	          + 'invoke=lookupEmployee'
                  + '&selectType=' + selectionType
                  + '&parentUserIdObj=' + userIdObjId
                  + '&parentNameObj=' + nameObjid
                  + '&parentEmailObj=' + emailObjId
                  + '&parentLocObj=' + locObjId
                  + '&parentListObj=' + parentListObj
                  + '&employeeFilter=' + employeeFilter;
	var windowFeatures = 'height=500,width=800,resizable=yes,menubar=no,scrollbars=yes,toolbar=no,left=100,top=125,status=yes';
	
	lookupWindow = window.open(url, '', windowFeatures);
	if (lookupWindow.opener == null) lookupWindow.opener = self;
	lookupWindow.focus();
}

function loadingLookup() {
   document.forms[0].employeeNameFilter.focus();
}

function validateSearchEmployee(){
    var empName1 = Trim(document.forms[0].employeeNameFilter.value);
    if (empName1 == "") {
        alert("Please enter employee name to search");
        document.forms[0].employeeNameFilter.focus();
        return false;
    } else {
        var specialCharacter = /^[a-zA-Z()-]+(\s([a-zA-Z()-])+)*$/;
        var specialCharacter = "abcdefghijklmnopqrstuvwxyz0123456789";
		//if (!specialCharacter.test(empName1)) {
		if ( !isvalid('Name', document.forms[0].employeeNameFilter.value, specialCharacter) ) {
			alert("Please enter valid employee name");
			document.forms[0].employeeNameFilter.focus();
			document.forms[0].employeeNameFilter.select();
			return false;
		}
    }
    document.forms[0].submitted.value = 1;
    
    return true;
}

function returnEmployeeInfo(objField) {
    var employeeInfo = objField.value.split(":");
    
    if ( employeeInfo.length>1 ) {
        var userId = employeeInfo[0];
        var fullName = employeeInfo[1];
        var emailId = employeeInfo[2];
        var location = employeeInfo[3];
        if ( userId!='' && Trim(document.forms[0].parentUserIdObj.value)!='' ) {
            var parentSsoObj = opener.document.getElementById(document.forms[0].parentUserIdObj.value);
            //alert(parentSsoObj);
            if (parentSsoObj != null) {
                parentSsoObj.value = userId;
            }
        }
        if ( fullName!='' && Trim(document.forms[0].parentNameObj.value)!='' ) {
            var parentNameObj = opener.document.getElementById(document.forms[0].parentNameObj.value);
            if (parentNameObj != null) {
                parentNameObj.value = fullName;
            }
        }
        if ( emailId!='' && Trim(document.forms[0].parentEmailObj.value)!='' ) {
            var parentEmailObj = opener.document.getElementById(document.forms[0].parentEmailObj.value);
            if (parentEmailObj != null) {
                if ( Trim(parentEmailObj.value).length > 0){
        	        parentEmailObj.value = Trim(parentEmailObj.value) + ", " + emailId;
        	    } else {
        		    parentEmailObj.value = emailId;
        	    }
            }
        }
        if ( location!='' && Trim(document.forms[0].parentLocObj.value)!='' ) {
            var parentLocObj = opener.document.getElementById(document.forms[0].parentLocObj.value);
            if (parentLocObj != null) {
                parentLocObj.value = location;
            }
        }
    }
    
    if ( Trim(document.forms[0].parentListObj.value)!='' ) {
        var selectedEmployees = document.forms[0].selectedEmployee;
        var parentListObj = opener.document.getElementById(document.forms[0].parentListObj.value);
        var resultCount = document.forms[0].sizeOfList.value;
        var prevlength = parentListObj.length;
        
        if ( resultCount == 1 ) {
            if (selectedEmployees.checked) {
                var employeeInfo = selectedEmployees.value.split(":");
                
                //opener.setEmployees(UserId, FullName, EmailId, Location, PreviousLengthOfListObj);
                opener.setEmployees(employeeInfo[0], employeeInfo[1], employeeInfo[2], employeeInfo[3], parentListObj);
            }
        } else {
            for ( i=0 ; i<selectedEmployees.length ; i++ ) {
                if (selectedEmployees[i].checked) {
                    var employeeInfo = selectedEmployees[i].value.split(":");
                    
                    //opener.setEmployees(UserId, FullName, EmailId, Location, PreviousLengthOfListObj);
                    opener.setEmployees(employeeInfo[0], employeeInfo[1], employeeInfo[2], employeeInfo[3], parentListObj);
                }
            }
        }
    }
    
    self.close();
}

function setEmployees(userId, fullName, emailId, location, parentListObj) {
    if ( isAlreadyExist(userId, parentListObj) ) {
        parentListObj.options[parentListObj.length] = new Option(fullName, userId);
    }
}

function isAlreadyExist(userId, parentListObj) {
    /*var defUserObj = opener.document.getElementById("defaultUsersList");
    if (defUserObj != null && defUserObj.value != "") {
    	for ( i=0; i<defUserObj.length; i++ ) {
        	if (defUserObj[i].value == Trim(userId)) {
            alert("Employee is already added in the list.");
            return false;
        	}
    	}
      
    }*/
    for ( i=0; i<parentListObj.length; i++ ) {
        if (parentListObj[i].value == Trim(userId)) {
            //alert("Employee is already added in the list.");
            return false;
        }
    }
    return true;
}

function removeEmployee(employeeListObj) {
    for (i=0; i<employeeListObj.length; i++) {
        if (employeeListObj[i].selected)
            employeeListObj.remove(i);
    }
}

function clearAll(employeeListObj) {
    if (employeeListObj != null)
        employeeListObj.length=0;
}

function clearForm() {
    document.forms[0].employeeNameFilter.value="";
    document.getElementById("divResults").style.display='none';
    document.forms[0].employeeNameFilter.focus();
}