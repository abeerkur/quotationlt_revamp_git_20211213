/**********************************************************************************/
function Loading() {
	self.focus();
	document.getElementById("qFOLDER").focus();
}
/**********************************************************************************/
function showInstruct() {
	var inst = document.getElementById('instructions');

	if ( inst.style.display == 'block' ){
		inst.style.display = 'none';
	}else{
		inst.style.display = 'block';
	}
}
/**********************************************************************************/
function Change(element) {
	var itemChange = document.getElementById(element);
	if (itemChange.className == 'light'){
		itemChange.className = 'highlight';
	}
	else{
		itemChange.className = 'highdark';
	}
}
/**********************************************************************************/
function ChangeBack(element) {
	var itemBack = document.getElementById(element);
	if (itemBack.className == 'highlight'){
		itemBack.className = 'light';
	}
	else{
		itemBack.className = 'dark';
	}
}
/**********************************************************************************/
function ChangeTextbox(element1, element2) {
	var itemChange1 = document.getElementById(element1);
	var itemChange2 = document.getElementById(element2);
	if (itemChange1.className == 'light'){
		itemChange1.className = 'highlight';
	} else{
		itemChange1.className = 'highdark';
	}
	if (itemChange2.className == 'light'){
		itemChange2.className = 'highlight-a';
	} else{
		itemChange2.className = 'highdark-a';
	}
}
/**********************************************************************************/
function ChangeBackTextbox(element1, element2, oldStyle) {
	var itemBack1 = document.getElementById(element1);
	var itemBack2 = document.getElementById(element2);
	if (itemBack1.className == 'highlight'){
		itemBack1.className = oldStyle;
	} else{
		itemBack1.className = oldStyle;
	}
	if (itemBack2.className == 'highlight'){
		itemBack2.className = oldStyle + '-a';
	} else{
		itemBack2.className = oldStyle + '-a';
	}
}
/**********************************************************************************/
