function showhide(show,hide){
obj=document.getElementById(show).style.display="block";
obj1=document.getElementById(hide).style.display="none";
}

function searchdashboard(maintablename,colum,id){

	var tablename=document.getElementById(maintablename);
	var lengthoftablerows =tablename.rows.length;
	var checkboxid=document.getElementById(id);
	if(checkboxid.checked==true){
	
	for(i=0;i<lengthoftablerows;i++){
		document.getElementById(colum+i).style.display="block";
		}}
		else{
			for(i=0;i<lengthoftablerows;i++){
		document.getElementById(colum+i).style.display="none";
		}
			}
}
function show(num){
    obj=document.getElementById('showdataApplication')
    if(num=='Application'){
        obj.style.display="block";
    }else if(num=='Document'){
    	obj.style.display="none";
	}
}

function hide(num){
    obj=document.getElementById('showdata'+num)
    obj.style.display="none"
}

function homedashboardmain(maintablename, colum, id){
	var tablename=document.getElementById(maintablename);
	var lengthoftablerows =tablename.rows.length-1;
	var checkboxId = document.getElementById(id);
	if(checkboxId != null && checkboxId.checked==true){		
		for(i=0;i<lengthoftablerows;i++){
			document.getElementById(colum).style.display='block';
			document.getElementById(colum+i).style.display='block';
		}
	}
	else{
		
		
		for(i=0;i<lengthoftablerows;i++){
			document.getElementById(colum).style.display='none';
			document.getElementById(colum+i).style.display='none';
		}
	}
}
function searchtdshowhidereload(valid){		
	var pole = document.getElementById("poleCheck");
	var kw = document.getElementById("kwCheck");
	
	
	
	var frame = document.getElementById("frameCheck");
	var volts = document.getElementById("voltsCheck");
	var mounting = document.getElementById("mountingCheck");
	
	
	if(pole != null && pole.checked==true){
		var colum = "poletd";
	
		document.getElementById("poletd0").style.display = 'block';
		var tablename=document.getElementById("table0");
		var lengthoftablerows =tablename.rows.length-1;		
		for(i=0;i<=lengthoftablerows;i++){			
			document.getElementById(colum+i).style.display='block';
		}		
	}
	
	if(kw != null && kw.checked==true){
		var colum = "KWtd";
		document.getElementById("KWtd0").style.display = 'block';
		var tablename=document.getElementById("table0");
		var lengthoftablerows =tablename.rows.length-1;		
		for(i=0;i<=lengthoftablerows;i++){			
			document.getElementById(colum+i).style.display='block';
		}		
	}
	
	if(frame != null && frame.checked==true){
		var colum = "Frametd";
		document.getElementById("Frametd0").style.display = 'block';
		var tablename=document.getElementById("table0");
		var lengthoftablerows =tablename.rows.length-1;		
		for(i=0;i<=lengthoftablerows;i++){			
			document.getElementById(colum+i).style.display='block';
		}		
	}
	
	if(volts != null && volts.checked==true){
		var colum = "Voltstd";
		document.getElementById("Voltstd0").style.display = 'block';
		var tablename=document.getElementById("table0");
		var lengthoftablerows =tablename.rows.length-1;		
		for(i=0;i<=lengthoftablerows;i++){			
			document.getElementById(colum+i).style.display='block';
		}		
	}
	
	if(mounting != null && mounting.checked==true){
		var colum = "Mountingtd";
		document.getElementById("Mountingtd0").style.display = 'block';
		var tablename=document.getElementById("table0");
		var lengthoftablerows =tablename.rows.length-1;		
		for(i=0;i<=lengthoftablerows;i++){			
			document.getElementById(colum+i).style.display='block';
		}		
	}
}
function MM_goToURL() { //v3.0
  var i, args=MM_goToURL.arguments; document.MM_returnValue = false;
  for (i=0; i<(args.length-1); i+=2) eval(args[i]+".location='"+args[i+1]+"'");
}

function f1()
{window.open('request.html','','height=500,width=800,resizable=yes,menubar=no,scrollbars=yes,toolbar=no,left=150,top=150');}
function f2()
{window.open('help.html','','height=400,width=800,resizable=yes,menubar=no,scrollbars=yes,toolbar=no,left=150,top=150');}

/********* Allows only Numbers **********/
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode < 48 || charCode > 57) {
        return false;
    }
    return true;
}

/* ******************************************************** Add-on Add Row function ****************************************************************** */
function addrowcreateoffer(tablename){
	//alert("Coming To addrowcreateoffer");

	var tbl = document.getElementById(tablename);
	var currentRow = tbl.rows.length-1;		//parseInt(document.forms[0].hidRowIndex.value);
	
	var currentValue = "", currentValue2="";

	if (document.getElementById("addOnTypeText" + currentRow) != null) {
	
		currentValue2 = Trim(document.getElementById("addOnTypeText" + currentRow).value);
		
		if(currentValue2 == "" || currentValue2 == 0) {
			alert("Please enter Add-on type value before adding a new Add-on");
			document.getElementById("addOnTypeText"+currentRow).focus();
			document.getElementById("addOnTypeText"+currentRow).value="";
			return false;
		}
		if(currentValue2 != "" && (currentValue2.toUpperCase()=="NULL" || currentValue2.toUpperCase()=="NA")) {
			alert("Please enter valid Add-on type Value");
			document.getElementById("addOnTypeText"+currentRow).focus();
			document.getElementById("addOnTypeText"+currentRow).value="";
			return false;
		}
	}
	
	if (document.getElementById("addOnQuantity"+currentRow) != null) {
		var currentQty = Trim(document.getElementById("addOnQuantity"+currentRow).value);
		if (currentQty == "" || currentQty.length == 0){
			alert("Please enter Quantity value before adding a new Add-on");
			document.getElementById("addOnQuantity"+currentRow).focus();
			return false;
		}	
	}

	if (document.getElementById("addOnPercent"+currentRow) != null && document.getElementById("addOnUnitPrice"+currentRow) != null) {
		var addOn_Percent_val = Trim(document.getElementById("addOnPercent"+currentRow).value);
		var addOn_Price_val = Trim(document.getElementById("addOnUnitPrice"+currentRow).value);
		if ( (addOn_Percent_val == null || addOn_Percent_val == "") && (addOn_Price_val == null || addOn_Price_val == "") ) {
			alert("Please enter Add-on % (or) Add-on Cash Extra value before adding a new Add-on");
			return false;	
		}
	}
	
	for (var i=1; i<currentRow;i++) {
		if (document.getElementById("addOnTypeText"+i) != null){		
			if ( currentValue2 != "" && currentValue2 == document.getElementById("addOnTypeText"+i).value){
				alert("Add-on type values cannot be same, please enter a different value.");
				return false;
			}			
		}
	}

	var newRow = tbl.insertRow();
	var Rowlength = tbl.rows.length;
	var Rowlengthid = Rowlength-1;
	var lastRow = parseInt(document.forms[0].hidRowIndex.value)+1;
	document.forms[0].hidRowIndex.value = lastRow;
	
	var oCell  = newRow.insertCell();
	var oCell1 = newRow.insertCell();
	var oCell2 = newRow.insertCell();
	var oCell3 = newRow.insertCell();
	var oCell4 = newRow.insertCell();
	
	
	if(Rowlength < 2){
		tbl.className="addonshide"
	}else{
		tbl.className="addonsshow"
	}
	
	oCell.innerHTML	= '<div class="data-list-input" style="width:300px; float:left;">' +
							'<select name="addOnType'+Rowlengthid+'" id="addOnType'+Rowlengthid+'" class="normal data-list-input width-select" onchange="javascript:resetAddOnTypeText('+Rowlengthid+');">' +
								'<option value=""> <bean:message key="quotation.option.select"/> </option>' +
								'<option value="Load Test">	Load Test </option>' +
								'<option value="Surge and Impulse test on sample coil">Surge & Impulse test on sample coil</option>' +
								'<option value="Tan Delta Test">Tan Delta Test</option>' +
								'<option value="Ring loop Flux test">Ring loop Flux test</option>' +
							'</select> <input type="text" name="addOnTypeText'+Rowlengthid+'" id = "addOnTypeText'+Rowlengthid+'" class="normal data-list-input input-class" maxlength="100"> ' +
					  '</div> ';
	oCell.style.cssText = "width:35%; padding:4px; border-bottom: 1px solid black;";
	
	oCell1.innerHTML = '<input id="addOnQuantity'+Rowlengthid+'" name="addOnQuantity'+Rowlengthid+'" onkeypress="return isNumber(event)" maxlength="2" class="short" >';
	oCell1.style.cssText = "width:15%; padding:4px; border-bottom: 1px solid black;";
	
	oCell2.innerHTML = '<input id="addOnPercent'+Rowlengthid+'" name="addOnPercent'+Rowlengthid+'" onkeypress="return isNumber(event)" maxlength="8" class="short" >';
	oCell2.style.cssText = "width:15%; padding:4px; border-bottom: 1px solid black;";
	
	oCell3.innerHTML = '&#8377; &nbsp; <input id="addOnUnitPrice'+Rowlengthid+'" name="addOnUnitPrice'+Rowlengthid+'" onkeypress="return isNumber(event)" maxlength="8" class="normal" >';
	oCell3.style.cssText = "width:20%; padding:4px; border-bottom: 1px solid black;";

	oCell4.innerHTML = '<img src="html/images/deleteicon.gif" style="cursor:hand;" onClick="removeRow(this)" id="delete'+Rowlengthid+'" alt="Delete">';
	oCell4.style.cssText = "width:15%; padding:4px; border-bottom: 1px solid black;";
	
}


function loadTextBoxValue(value,rowvalue)
{
	document.getElementById("addOnTypeText"+rowvalue).value=value;
}

function removeRow(src) {
	var oRow = src.parentElement.parentElement;	
	var tbl=document.getElementById("addons");
	var Rowlength = tbl.rows.length;
	document.all("addons").deleteRow(oRow.rowIndex);

		if(Rowlength==3){
			tbl.className="addonshide"
		}else{
			tbl.className="addonsshow"
		}
}

// JavaScript Document
if (document.getElementById) {
	document.write('<style type="text/css">\n')
	document.write('.showcontent{display:none;}\n')
	document.write('</style>\n')
}

function show(num){
	obj=document.getElementById('showdata'+num)
	obj.style.display="block";
	if(num==4){
		document.getElementById('multiple').style.display="none";
		document.getElementById('remove').style.display="block";
	}
}

function hide(num){
	obj=document.getElementById('showdata'+num)
	obj.style.display="none"
}

function estimate(){
	obj=document.getElementById('estimate')
	obj.style.display="block"
	obj1=document.getElementById('return')
	obj1.style.display="none"
}

function revision(){

	obj=document.getElementById("return")
	obj.style.display="block"
	obj1=document.getElementById("estimate")
	obj1.style.display="none"
		
}

function reportsdown(downarrow,tableid,uparrow,hiddenvalue){
	document.getElementById(downarrow+hiddenvalue).style.display="none";
	document.getElementById(tableid+hiddenvalue).style.display="block";
	document.getElementById(uparrow+hiddenvalue).style.display="block";
}

function reportsup(downarrow,tableid,uparrow,hiddenvalue){
	document.getElementById(downarrow+hiddenvalue).style.display="block";
	document.getElementById(tableid+hiddenvalue).style.display="none";
	document.getElementById(uparrow+hiddenvalue).style.display="none";
}

function addonstyle(){	
	var tbl=document.getElementById("addons");
	var Rowlength = tbl.rows.length;
	if(Rowlength==2){
		tbl.className="addonshide"
	}else{
		tbl.className="addonsshow"
	}
}


/*
 * This Function is Created By Gangadhar On 14-Feb-2019
 *  For Delivery Details.
 */
function addrowforIndentPage(tablename){

	var tbl=document.getElementById(tablename);

	var currentRow = tbl.rows.length-1;//parseInt(document.forms[0].hidRowIndex.value);

	var currentValue = "";
	var currentValue2="";
	var currentQty ="";
	var currentPrice ="";

	if (document.getElementById("deliveryLocation"+currentRow) != null) {
		currentValue2=Trim(document.getElementById("deliveryLocation"+currentRow).value);
		if(currentValue2 == "" || currentValue2 == 0 ) {
			alert("Please enter Delivery Location value before adding a new Delivery Details");
			document.getElementById("deliveryLocation"+currentValue2).focus();
			document.getElementById("deliveryLocation"+currentValue2).value="";
			return false;
		}
		if(currentValue2!="" && (currentValue2.toUpperCase()=="NULL" || currentValue2.toUpperCase()=="NA")) {
			alert("Please enter Valid Delivery Location Value");
			document.getElementById("deliveryLocation"+currentValue2).focus();
			document.getElementById("deliveryLocation"+currentValue2).value="";
			return false;
	   
		}
	}
	
	if (document.getElementById("deliveryAddress"+currentRow) != null) {
		currentQty = Trim(document.getElementById("deliveryAddress"+currentRow).value);
		if (currentQty == "" || currentQty.length == 0){
			alert("Please enter Delivery Address value before adding a new Delivery Details");
			document.getElementById("deliveryAddress"+currentRow).focus();
			return false;
		}	
	}

	if (document.getElementById("noOfMotors"+currentRow) != null) {
		currentPrice = Trim(document.getElementById("noOfMotors"+currentRow).value);
		if (currentPrice == "" || currentPrice.length == 0){
			alert("Please enter No Of Motors value before adding a new Delivery Details");
			document.getElementById("noOfMotors"+currentRow).focus();
			return false;
		}	
	}
	
	for (var i=1; i<currentRow;i++) {
		if ((document.getElementById("deliveryLocation"+i) != null && document.getElementById("deliveryAddress"+i) != null)
		    &&
		   ((currentValue2 != "" && currentValue2 == document.getElementById("deliveryLocation"+i).value) &&(currentQty != "" && currentQty == document.getElementById("deliveryAddress"+i).value))
		   ){
				alert("Delivery Location and Delivery Address values should not be same, please enter a different value");
				document.getElementById("deliveryLocation"+currentRow).value="";
				document.getElementById("deliveryAddress"+currentRow).value="";
				document.getElementById("noOfMotors"+currentRow).value="";
				return false;
			}
		
	} //For Loop Ending


	var newRow = tbl.insertRow();
	var Rowlength = tbl.rows.length;
	var Rowlengthid = Rowlength-1;
	var lastRow = parseInt(document.getElementById("hidRowIndex").value)+1;


	var oCell  = newRow.insertCell();
	var oCell1 = newRow.insertCell();
	var oCell2 = newRow.insertCell();
	var oCell3 = newRow.insertCell();
	oCell1.style.textAlign="center";


   //Show Delete Image
    var delImg=document.getElementById("deleteimage1");  
	  if (delImg.style.display === "none") {
		  delImg.style.display = "block";
	  } else {
		  delImg.style.display = "block";
	  }


     
	oCell.innerHTML  ='<input type="text" name="deliveryLocation'+Rowlengthid+'"  id = "deliveryLocation'+Rowlengthid+'"  style="width:100px;"  maxlength="100">';
	oCell.style.textAlign = "left";
	oCell1.innerHTML ='	<input type="text" style="width:600px; height:25px" onchange = "javascript:checkMaxLength(this.id, 4000);" id="deliveryAddress'+Rowlengthid+'" name="deliveryAddress'+Rowlengthid+'" />';	
	oCell1.style.textAlign = "center";
	oCell2.innerHTML ='<input tabindex="19" class="short"  id="noOfMotors'+Rowlengthid+'" name="noOfMotors'+Rowlengthid+'" style="height:25px;" onkeypress="return isNumber(event)"  maxLength="9">';	
	oCell3.innerHTML ='<div id="deleteimage'+Rowlengthid+'"><img src="html/images/deleteicon.gif" style="cursor:hand;" onClick="removeDeliveryDetailsRow('+Rowlengthid+')" id="delete'+Rowlengthid+'" alt="Delete"></div>';
	oCell3.style.textAlign = "left";

	document.getElementById("hidRowIndex").value = lastRow;

}


/*
 * This Function is Created By Gangadhar On 14-Feb-2019
 *  For Delivery Details.
 */
function removeDeliveryDetailsRow(src) {
	var tbl=document.getElementById("deliverydetails");
	var Rowlength = tbl.rows.length;
	document.all("deliverydetails").deleteRow(src);

			if(Rowlength==3){
				tbl.className="addonshide"
				}else{
					tbl.className="addonsshow"
					}
}

