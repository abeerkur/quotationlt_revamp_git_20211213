/*
 * Manage Customer 
 */
function searchCustomer() {
	var customerValue = Trim(document.customerForm.customerName.value);
	if (customerValue == "") {
		alert("Please enter  customer name");
		document.customerForm.customerName.focus();

	} else {
		document.customerForm.submit();
	}
}

// resetCustomerSearch function is used to reset the customer search value ....
function resetCustomerSearch(){
    window.location = "searchcustomer.do?invoke=viewcustomer";
}

function clearMasterSearch() {
	document.getElementById("master").value = "";
}

function addNewCustomer() {
	document.customerForm.invoke.value = "addNewCustomerpage";
	document.customerForm.submit();
}

function validateCustomer(value) {

	var specialCharacter = /^[a-zA-Z0-9()-.]+(\s([a-zA-Z0-9()-.])+)*$/;
	var customerName = Trim(document.customerForm.customerName.value);
	var contactPersonName = Trim(document.customerForm.contactperson.value);

	if (customerName == "") {
		alert("Please enter the Customer Name ");
		document.customerForm.customerName.focus();
		return false;
	}
	if (customerName.length > 150) {
		alert("Please enter the Customer Name with in 150 Charecters ");
		document.customerForm.customerName.focus();
		return false;
	}
	if (document.customerForm.locationId.value == 0) {
		alert("Please select the Customer Location");
		return false;
	}
	if (contactPersonName == "" || !specialCharacter.test(contactPersonName)) {
		alert("Please enter the Contact Person name");
		document.customerForm.contactperson.focus();
		return false;
	}
	if (contactPersonName.length > 150) {
		alert("Please enter the Contact Person Name with in 150 Charecters ");
		document.customerForm.customerName.focus();
		return false;
	}
	if (!isValidEmail(document.customerForm.email)) {
		return false;
	}
	if (document.customerForm.email.value.length > 120) {
		alert("Please enter the Email with in 120 Charecters ");
		document.customerForm.customerName.focus();
		return false;
	}

	var Phone = Trim(document.customerForm.phone.value);
	if (isNaN(Phone)) {
		alert("Please enter valid Phone Number");
		document.customerForm.phone.focus();
		return false;
	}
	if (Phone.length > 15) {
		alert("Please enter the Phone Number with in 15 Charecters ");
		document.customerForm.customerName.focus();
		return false;
	}
	
	var Mobile = Trim(document.customerForm.mobile.value);
	if (isNaN(Mobile)) {
		alert("Please enter valid Mobile Number");
		document.customerForm.mobile.focus();
		return false;
	}
	if (Mobile.length > 15) {
		alert("Please enter the Mobile Number with in 15 Charecters ");
		document.customerForm.customerName.focus();
		return false;
	}

	/*
	 * var Fax = Trim(document.customerForm.fax.value); if (isNaN(Fax)) {
	 * alert("Please enter valid Fax Number");
	 * document.customerForm.fax.focus(); return false; } if(Fax.length > 15){
	 * alert("Please enter the Fax Number with in 15 Charecters ");
	 * document.customerForm.customerName.focus(); return false; }
	 */
	var Address = Trim(document.customerForm.address.value);
	if (contactPersonName != "" && Address.length > 500) {
		alert("Please enter the Address with in 500 Charecters ");
		document.customerForm.customerName.focus();
		return false;
	}

	document.getElementById("operation").value = value;

	return true;
}

function clearCustomerForm(){
	document.customerForm.customer.value ="";
	document.customerForm.customerName.value ="";
	document.customerForm.locationId.value = 0;
	document.customerForm.contactperson.value="";
    document.customerForm.email.value="";
    document.customerForm.phone.value="";
    document.customerForm.mobile.value="";
    document.customerForm.fax.value="";
    document.customerForm.address.value="";
    
    document.customerForm.salesengineerId.value="0";
    document.customerForm.customerType.value="";
    document.customerForm.industry.value="";
    document.customerForm.sapcode.value="";
    document.customerForm.oraclecode.value="";
    document.customerForm.gstnumber.value="";
    document.customerForm.contactname.value="";
    document.customerForm.city.value="";
    document.customerForm.state.value="";
    document.customerForm.country.value="";
}

function editCustomer(id){
    window.location = "searchcustomer.do?invoke=editCustomerPage&customerId="+id;
}

function deleteCustomer(id, cName) {
	if (window.confirm("Are you sure to delete the Customer ?")) {
		document.customerForm.customer.value = cName;
		document.customerForm.customerId.value = id;
		document.customerForm.invoke.value = "deleteCustomer";
		document.customerForm.submit();
	}
}

function exportCustomerResult() {
	var epxortcustomerresult = "searchcustomer.do?invoke=exportCustomerSearch&functionType=export";
	props = 'toolbar=0,scrollbars=0,location=0,statusbar=1,menubar=0,resizable=0';
	window.location = epxortcustomerresult;
}

function printCustomerResult() {
	var printCustomerResult = "searchcustomer.do?invoke=exportCustomerSearch&functionType=print";
	props = 'toolbar=0,scrollbars=0,location=0,statusbar=1,menubar=0,resizable=0';
	window.open(printCustomerResult)
}


/*
 * Manage Locations
 */

function resetLocationSearch() {
	window.location = "searchlocation.do?invoke=viewlocationsearch";
}

function resetManufacturingSearch() {
	window.location = "technicaldata.do?invoke=viewbulkdata";
}

function searchLocation() {
	var specialCharacter = /^[a-zA-Z0-9()-]+(\s([a-zA-Z0-9()-.])+)*$/;
	var locationValue = Trim(document.locationForm.locationName.value);

	if (locationValue != "" && !specialCharacter.test(locationValue)) {
		alert("Please enter valid Location name");
		document.locationForm.locationName.focus();
	} else {
		document.locationForm.submit();
	}
}

function changeStatus(objStatusName, objStatusActionName, locationId, workingDiv) {
	var objStatus = document.getElementById(objStatusName);
	var objStatusAction = document.getElementById(objStatusActionName);

	workingDivId = workingDiv;

	if (objStatus.value == "Inactive") {
		updateStatus(locationId, 'A', objStatusName, objStatusActionName, workingDiv);
	} else if (objStatus.value == "Active") {
		updateStatus(locationId, 'I', objStatusName, objStatusActionName, workingDiv);
	}
}

function updateStatus(locationId, action, objStatusName, objStatusActionName, workingDiv) {
	if (Trim(locationId) != "" && Trim(action) != "") {
		var url = "searchlocation.do?invoke=updateLocationStatus";
		data = "";
		data += "&locid=" + locationId;
		data += "&action=" + action;
		data += "&targetObjects=" + objStatusName + "," + objStatusActionName;
		url += data;

		// invokeCommonAjaxFunctionality(url, data);
		invokeAjaxFunctionality(url, data, workingDiv);
	} else {
		alert("Invalid inputs.");
	}
}

function addNewLocation() {
	document.locationForm.invoke.value = "addNewLocationPage";
	document.locationForm.submit();
}

function validateLocation() {
	var specialCharacter = /^[a-zA-Z0-9()-.]+(\s([a-zA-Z0-9()-.])+)*$/;
	var locationname = Trim(document.locationForm.locationName.value);

	if (locationname == "" || !specialCharacter.test(locationname)) {
		alert("Please enter the valid Location name ");
		document.locationForm.locationName.focus();
		return false;
	}

	if (document.locationForm.regionId.value == 0) {
		alert("Please select any one of Region Name from drop down");
		document.locationForm.regionId.focus();
		return false;
	}
	selectedValue = getSelectedRadioValue("locationForm", "status");
	if (checkUndefined(selectedValue)) {
		alert("Please Select the Status Value");
		return false;
	}
	return true;
}

function clearLocationForm() {
	document.forms[0].reset();
}

function editLocation(id) {
	window.location = "searchlocation.do?invoke=editLocationPage&locationId=" + id;
}

function deleteLocation(id, lName) {
	if (window.confirm("Are you sure to delete the Location ?")) {
		document.locationForm.locationName.value = lName;
		document.locationForm.locationId.value = id;
		document.locationForm.invoke.value = "deleteLocation";
		document.locationForm.submit();
	}
}

function exportLocationResult() {
	var exportLocationResult = "searchlocation.do?invoke=exportLocationSearch&functionType=export";
	props = 'toolbar=0,scrollbars=0,location=0,statusbar=1,menubar=0,resizable=0';
	window.location = exportLocationResult;
}

function printLocationResult() {
	var printLocatrionResult = "searchlocation.do?invoke=exportLocationSearch&functionType=print";
	props = 'toolbar=0,scrollbars=0,location=0,statusbar=1,menubar=0,resizable=0';
	window.open(printLocatrionResult)
}


/*
 * Manage Sales Managers Screens
 */

function resetSalesManagerSearch(){
    window.location = "searchsalesmanager.do?invoke=viewsalesmanagerSearchResult";
}

function addNewSalesManager(){
    
    document.salesManagerForm.invoke.value="addNewSalesManagerPage";
    document.salesManagerForm.submit();
}
function validateMasters()
{
	
	document.getElementById("master").value=Trim(document.getElementById("master").value);
	var MasterName=Trim(document.getElementById("master").value);
	
    if(MasterName =="" ){
        alert("Please Enter the Master name");
        document.getElementById("master").focus();
        return false;
    }
    else
    {
    	document.MastersDataForm.submit();
    }


}
function validateSalesManager(){
    
    document.salesManagerForm.salesManager.value=Trim(document.salesManagerForm.salesManager.value);

      var salesManagerName=Trim(document.salesManagerForm.salesManager.value );

    if(salesManagerName =="" ){
        alert("Please Enter the Sales Manager name");
        document.salesManagerForm.salesManager.focus();
        return false;
    }
    if(document.salesManagerForm.regionId.value ==0){
        alert("Please select any one of Region Name from the drop down");
         document.salesManagerForm.regionId.focus();
        return false;
    }
    if(document.salesManagerForm.primaryRSM.value ==""){
        alert("Please select the Primary RSM name");
         document.salesManagerForm.primaryRSM.focus();
        return false;
    }
    return true;
}

function clearSalesManagerForm(){
    document.salesManagerForm.salesManager.value ="";
    document.salesManagerForm.regionId.value=0;
    document.salesManagerForm.primaryRSM.value ="";
    document.salesManagerForm.additionalRSM.value="";
}

function editSalesManager(salesMngrId){
    window.location = "searchsalesmanager.do?invoke=editSalesManagerPage&salesManagerId="+salesMngrId;
}

function updateSalesManagerValue(){
    if(document.salesManagerForm.salesManager.value ==""){
        alert("Please select the Sales Manager name");
    }else if(document.salesManagerForm.regionId.value ==0){
        alert("Please select any one of Region Name from the drop down");
    }else if(document.salesManagerForm.primaryRSM.value ==""){
        alert("Please select the PrimaryRSM name");
    }else if(document.salesManagerForm.additionalRSM.value ==""){
        alert("Please select the AdditionalRSM name");
    }else{
      document.salesManagerForm.operation.value="update";
      document.salesManagerForm.submit();
    }
}

function deleteSalesManager(salesManagerSSO,salesManagerName,Region){
      if (window.confirm("Are you sure to delete the Sales Manager ?")){
        document.salesManagerForm.salesManager.value =salesManagerName;
        document.salesManagerForm.salesManagerSSO.value =salesManagerSSO;
        var regionId=0;
        if(Region=="East")
        {
        	regionId=1;
        }
        if(Region=="West")
        {
        	regionId=2;
        }
        if(Region=="North")
        {
        	regionId=3;
        }
        if(Region=="South")
        {
        	regionId=4;
        }

        
        document.getElementById("regionIds").value =regionId;
        document.salesManagerForm.invoke.value="deleteSalesManager";
        document.salesManagerForm.submit();
    }
}

function exportSalesManagerSearchResult(){
    
    var exportSalesManagerResult = "searchsalesmanager.do?invoke=exportSalesManagerSearch&functionType=export";
     props='toolbar=0,scrollbars=0,location=0,statusbar=1,menubar=0,resizable=0';	
	 window.location = exportSalesManagerResult;
}

function printSalesManagerSearchResult(){
    var printSalesManagerResult = "searchsalesmanager.do?invoke=exportSalesManagerSearch&functionType=print";
    props='toolbar=0,scrollbars=0,location=0,statusbar=1,menubar=0,resizable=0';	
	window.open (printSalesManagerResult)
}



/*
 * Manage add/replace managers screens ....
 */

/****  replacemanagers  ***/	
function showreplacemanagers(show,hide1,hide2,hide3,hide4,hide5){
	obj=document.getElementById(show).style.display="block";
	obj1=document.getElementById(hide1).style.display="none"
	obj2=document.getElementById(hide2).style.display="none"
	obj3=document.getElementById(hide3).style.display="none"
	obj4=document.getElementById(hide4).style.display="none"
	obj5=document.getElementById(hide5).style.display="none"

   document.updateManagerForm.cheifExecutiveSSO.value="";
   document.updateManagerForm.designManagerSSO.value="";
   document.updateManagerForm.regionalSalesManagerSSO.value="";
   document.updateManagerForm.oldSalesManagerSSO.value=0;
   document.updateManagerForm.newSalesManagerSSO.value=0;
   document.updateManagerForm.commercialManagerSSO.value="";
}

function saveValue() {
	if ( getSelectedRadioValue(document.forms[0].name, "managertype")=="CM" ) {
        document.updateManagerForm.operation.value="CM";
        
        if ( document.updateManagerForm.commercialManagerSSO.value=="" ) {
            alert("Please specify the new Commercial Manager to add/replace");
            return false;
        }
        if ( document.updateManagerForm.commercialManagerSSO.value == document.updateManagerForm.commercialManagerOldSSO.value ) {
            alert("Please select a different user to replace with existing Commercial Manager");
            return false;
        }
    }
	
    if ( getSelectedRadioValue(document.forms[0].name, "managertype")=="CE" ) {
        document.updateManagerForm.operation.value="CE";
        
        if ( document.updateManagerForm.cheifExecutiveSSO.value=="" ) {
            alert("Please specify the new Chief Executive to add/replace");
            return false;
        }
        if ( document.updateManagerForm.cheifExecutiveSSO.value == document.updateManagerForm.cheifExecutiveOldSSO.value ) {
            alert("Please select a different user to replace with existing Chief Executive");
            return false;
        }
    }
    if ( getSelectedRadioValue(document.forms[0].name, "managertype")=="DM" ) {
        document.updateManagerForm.operation.value="DM";
        
        if ( document.updateManagerForm.designManagerSSO.value=="" ) {
            alert("Please specify the new Design Manager to add/replace");
            return false;
        }
        if ( document.updateManagerForm.designManagerSSO.value == document.updateManagerForm.designManagerOldSSO.value ) {
            alert("Please select a different user to replace with existing Design Manager");
            return false;
        }
    }
    if ( getSelectedRadioValue(document.forms[0].name, "managertype")=="RSM" ) {
        document.updateManagerForm.operation.value="RSM";
        
        if ( document.updateManagerForm.regionalSalesManagerSSO.value==""
                || document.updateManagerForm.regionalSalesManagerID.value==0 ) {
            alert("Please specify both existing and new RSM to replace managers");
            return false;
        }
        if ( document.updateManagerForm.regionalSalesManagerSSO.value == document.updateManagerForm.regionalSalesManagerID.value ) {
            alert("Please select a different user to replace with existing RSM");
            return false;
        }
    }
    if ( getSelectedRadioValue(document.forms[0].name, "managertype")=="SM" ) {
        document.updateManagerForm.operation.value="SM";
        
        if ( document.updateManagerForm.oldSalesManagerSSO.value==0
                || document.updateManagerForm.newSalesManagerSSO.value==0  ) {
            alert("Please specify both existing and new Sales Manager to replace managers");
            return false;
        }
        if ( document.updateManagerForm.oldSalesManagerSSO.value == document.updateManagerForm.newSalesManagerSSO.value ) {
            alert("Please select a different user to replace with existing Sales Manager");
            return false;
        }
    }

    if ( getSelectedRadioValue(document.forms[0].name, "managertype")=="DE" ) {
        document.updateManagerForm.operation.value="DE";

       
        if ( document.updateManagerForm.oldDesignEngineerSSO.value==0
                || document.updateManagerForm.newDesignEngineerSSO.value==0  ) {
            alert("Please specify both existing and new Design Engineer to replace managers");
            return false;
        }
        if ( document.updateManagerForm.oldDesignEngineerSSO.value == document.updateManagerForm.newDesignEngineerSSO.value ) {
            alert("Please select a different user to replace with existing Design Engineer");
            return false;
        }
    }
    
   // return false;
}



/*
 * Manage Design Engineers Screens ...
 */

function resetDesignEngineerSearch(){
    window.location = "searchdesignengineer.do?invoke=viewDesignEngineer";
}

function addNewDesignEngineer(){
    document.designEngineerForm.invoke.value="addNewDesignEngineerPage"
    document.designEngineerForm.submit();
}

function clearDesignEngineerForm(){
    document.designEngineerForm.designEngineerName.value="";
}

function validateDesignEngineerValue(){
    
      var specialCharacter = /^[a-zA-Z0-9()-.]+(\s([a-zA-Z0-9()-.])+)*$/;	
      var designManagerName=Trim(document.designEngineerForm.designEngineerName.value );

   
    if(designManagerName==""){
        alert("Select desin engineer name from Lookup icon");
        return false;
    }
    return true;
}

function editDesignEngineer(DesignEngineerId){
     window.location = "searchdesignengineer.do?invoke=editDesignEngineerPage&designEngineerId="+DesignEngineerId;
}

function deleteDesignEngineer(designEngineerSSOValue, designengineerName){    
    var deName = (document.getElementById(designengineerName)).innerHTML;
      if (window.confirm("Are you sure to delete the Design Engineer ?")){
        document.designEngineerForm.designEngineerSSO.value =designEngineerSSOValue;
        document.designEngineerForm.designEngineerName.value =deName;
        document.designEngineerForm.invoke.value="deleteDesignEngineerValues";
        document.designEngineerForm.submit();
    }
}

function exportDesginEngineerResult(){
    var exportDesignEngineerResult = "searchdesignengineer.do?invoke=exportDesignEngineerSearchResult&functionType=export";
     props='toolbar=0,scrollbars=0,location=0,statusbar=1,menubar=0,resizable=0';	
	 window.location = exportDesignEngineerResult;
}

function printDesginEngineerResult(){
    var printDesignEngineerResult = "searchdesignengineer.do?invoke=exportDesignEngineerSearchResult&functionType=print";
    props='toolbar=0,scrollbars=0,location=0,statusbar=1,menubar=0,resizable=0';	
	window.open (printDesignEngineerResult)
}



/*
 * Manage Commercial Engineers Screens ...
 */

function resetCommercialEngineerSearch(){
    window.location = "searchcommercialengineer.do?invoke=viewDesignEngineer";
}

function addNewCommercialEngineer(){
    document.commercialEngineerForm.invoke.value="addNewDesignEngineerPage"
    document.commercialEngineerForm.submit();
}

function clearCommercialEngineerForm(){
    document.commercialEngineerForm.designEngineerName.value="";
}

function validateCommercialEngineerValue(){
    
      var specialCharacter = /^[a-zA-Z0-9()-.]+(\s([a-zA-Z0-9()-.])+)*$/;	
      var designManagerName=Trim(document.commercialEngineerForm.designEngineerName.value );

   
    if(designManagerName==""){
        alert("Select desin engineer name from Lookup icon");
        return false;
    }
    return true;
}

function editCommercialEngineer(DesignEngineerId){
     window.location = "searchcommercialengineer.do?invoke=editCommercialEngineerPage&designEngineerId="+DesignEngineerId;
}

function deleteCommercialEngineer(designEngineerSSOValue, designengineerName){    
    var deName = (document.getElementById(designengineerName)).innerHTML;
      if (window.confirm("Are you sure to delete the Design Engineer ?")){
        document.commercialEngineerForm.designEngineerSSO.value =designEngineerSSOValue;
        document.commercialEngineerForm.designEngineerName.value =deName;
        document.commercialEngineerForm.invoke.value="deleteCommercialEngineerValues";
        document.commercialEngineerForm.submit();
    }
}

function exportCommercialEngineerResult(){
    var exportCommercialEngineerResult = "searchcommercialengineer.do?invoke=exportCommercialEngineerSearchResult&functionType=export";
     props='toolbar=0,scrollbars=0,location=0,statusbar=1,menubar=0,resizable=0';	
	 window.location = exportCommercialEngineerResult;
}

function printCommercialEngineerResult(){
    var printCommercialEngineerResult = "searchcommercialengineer.do?invoke=exportCommercialEngineerSearchResult&functionType=print";
    props='toolbar=0,scrollbars=0,location=0,statusbar=1,menubar=0,resizable=0';	
	window.open (printCommercialEngineerResult)
}



/*
 * Manage Technical Data Screens ...
 */

function searchTechnicalData(){
	alert("Page in progress..");
}

function addNewTechnicalData(){
    document.technicalDataForm.invoke.value="addNewTechnicalData";
    document.technicalDataForm.submit();
}

function vaidateTechnicalDataBulkLoad(){
	 if ( Trim(document.technicalDataForm.technicalDataFile.value) == "" ) {
		 	document.technicalDataForm.technicalDataFile.value="";
	        alert("Please specify the input file to load records.");
	        document.technicalDataForm.technicalDataFile.focus();
	        return false;
	    } else {
	        if ( Trim(document.technicalDataForm.technicalDataFile.value) == "" ) {
	        	document.technicalDataForm.technicalDataFile.value="";
		        alert("Please select a Site");
		        document.technicalDataForm.technicalDataFile.focus();
		        return false;
	    	}else{
	    		var file_name = document.technicalDataForm.technicalDataFile.value.split(".");
	    		if(file_name[1]!="xls") {
	    		   document.technicalDataForm.technicalDataFile.value="";
				   alert("Please upload only .xls extention files only");
				   return false;
				}else{
					document.technicalDataForm.invoke.value="bulkUploadTechnicalData";
	    			document.technicalDataForm.submit();
				}
	    	}
	    } 
}

function validateaddTechnicalData(){
	
	if(document.getElementById("MANUFACTURING_LOCATION").value ==""){
        alert("Please Enter MANUFACTURING LOCATION ");
        return false;
    }
	
	if(document.getElementById("PRODUCT_GROUP").value ==""){
        alert("Please Enter PRODUCT GROUP ");
        return false;
    }
	
	if(document.getElementById("PRODUCT_LINE").value==""){
        alert("Please Enter PRODUCT LINE ");
        return false;
    }
	
	if(document.getElementById("STANDARD").value==""){
        alert("Please Enter STANDARD ");
        return false;
    }
	
	if(document.getElementById("POWER_RATING_KW").value==""){
        alert("Please Enter POWER RATING_KW ");
        return false;
    }
	
	if(document.getElementById("FRAME_TYPE").value==""){
        alert("Please Enter FRAME TYPE ");
        return false;
    }
	
	if(document.getElementById("NUMBER_OF_POLES").value==""){
        alert("Please Enter NUMBER OF POLES ");
        return false;
    }
	
	if(document.getElementById("MOUNTING_TYPE").value==""){
        alert("Please Enter MOUNTING TYPE ");
        return false;
    }
	
	if(document.getElementById("TB_POSITION").value==""){
        alert("Please Enter TB POSITION ");
        return false;
    }
	
	if(document.getElementById("MODEL_NO").value==""){
        alert("Please Enter MODEL NO ");
        return false;
    }
	
	if(document.getElementById("VOLTAGE_V").value==""){
        alert("Please Enter VOLTAGE V ");
        return false;
    }
	
	return true;
		
}

function clearTechnicalDataForm(){
	document.salesManagerForm.MANUFACTURING_LOCATION.value =="";
}

function editTechnicalData(id){
    window.location = "technicaldata.do?invoke=editTechnicalData&TECHNICAL_DATA_SERIAL_ID="+id;
}

function deleteTechnicalData(id ,lName){
	if (window.confirm("Are you sure to delete the Technica Data ?")){
     document.technicalDataForm.MANUFACTURING_LOCATION.value =lName;
     document.technicalDataForm.SERIAL_ID.value =id;
     document.technicalDataForm.invoke.value="deleteTechnicalData";
     document.technicalDataForm.submit();
	}
}



/*
 * Manage List Price Screens ...
 */

function searchListPrice(){
	document.listPriceForm.submit();
}

function resetListPriceSearch(){
	document.listPriceForm.manufacturing_location.value = "0";
	document.listPriceForm.power_rating_kw.value = "0";
	document.listPriceForm.product_group.value = "0";
	document.listPriceForm.product_line.value = "0";
	document.listPriceForm.frame_type.value = "0";
	document.listPriceForm.frame_suffix.value = "0";
	
    //window.location = "listprice.do?invoke=viewListPrice";
}

function exportListPriceResult() {
	var exportListPriceResult = "listprice.do?invoke=exportListPriceSearch&functionType=export";
	props = 'toolbar=0,scrollbars=0,location=0,statusbar=1,menubar=0,resizable=0';
	window.location = exportListPriceResult;
}

function clearListPriceForm() {
	document.listPriceForm.manufacturing_location.value = "0";
	document.listPriceForm.product_group.value = "0";
	document.listPriceForm.product_line.value = "0";
	document.listPriceForm.standard.value = "0";
	document.listPriceForm.power_rating_kw.value = "0";
	document.listPriceForm.number_of_poles.value = "0";
	document.listPriceForm.frame_type.value = "0";
	document.listPriceForm.frame_suffix.value = "0";
	document.listPriceForm.mounting_type.value = "0";
	document.listPriceForm.tb_position.value = "0";
	document.listPriceForm.list_price.value = "";
	document.listPriceForm.material_cost.value = "";
	document.listPriceForm.loh.value = "";
}

function validateaddListPrise(){
	
	if(document.getElementById("manufacturing_location").value =="0"){
        alert("Please select Manufacturing Location. ");
        return false;
    }
	if(document.getElementById("product_group").value =="0"){
        alert("Please select Product Group. ");
        return false;
    }
	if(document.getElementById("product_line").value=="0"){
        alert("Please select Product Line. ");
        return false;
    }
	if(document.getElementById("power_rating_kw").value=="0"){
        alert("Please select Power Rating (KW). ");
        return false;
    }
	if(document.getElementById("frame_type").value=="0"){
        alert("Please select Frame Type. ");
        return false;
    }
	if(document.getElementById("frame_suffix").value=="0"){
        alert("Please select Frame Suffix. ");
        return false;
    }
	if(document.getElementById("number_of_poles").value=="0"){
        alert("Please select Number of Poles. ");
        return false;
    }
	/*if(document.getElementById("mounting_type").value=="0"){
        alert("Please Enter Mounting Type. ");
        return false;
    }
	if(document.getElementById("tb_position").value=="0"){
        alert("Please Enter TB Position ");
        return false;
    }*/
	if(document.getElementById("list_price").value == ""){
		alert("Please enter value for List Price. ");
        return false;
	}
	
	return true;
}

function addNewListPrice(){
	document.listPriceForm.invoke.value="addNewListPrice";
    document.listPriceForm.submit();
}

function editListPrice(id) {
	window.location = "listprice.do?invoke=editListPrice&serial_id=" + id;
}

function returnSearchListPrice() {
	window.location = "listprice.do?invoke=viewListPrice";
}

function deleteListPrice(id, lName) {
	if (window.confirm("Are you sure to delete the List Price ?")) {
		document.listPriceForm.manufacturing_location.value = lName;
		document.listPriceForm.serial_id.value = id;
		document.listPriceForm.invoke.value = "deleteListPrice";
		document.listPriceForm.submit();
	}
}

function validateListPriceBulkLoad() {
	if ( Trim(document.listPriceForm.listpriceBulkupload.value) == "" ) {
		document.listPriceForm.listpriceBulkupload.value="";
		alert("Please specify the input file to load records.");
		document.listPriceForm.listpriceBulkupload.focus();
		return false;
   	 } else {
   		var file_name = document.listPriceForm.listpriceBulkupload.value.split(".");
		if(file_name[1] != "xls") {
			document.listPriceForm.listpriceBulkupload.value="";
			alert("Please upload only .xls extention files only");
			return false;
		} else {
			document.listPriceForm.invoke.value="processBulkUploadListPrice";
			document.listPriceForm.submit();
		}
   	 } 
}



/*
 * Manage Customer Discount Screens ...
 */

function searchCustomerDiscount(){
	document.customerDiscountForm.submit();
}

function resetCustomerDiscountSearch(){
	document.customerDiscountForm.customerName.value = "";
	document.customerDiscountForm.manufacturing_location.value = "0";
	document.customerDiscountForm.power_rating_kw.value = "0";
	document.customerDiscountForm.product_group.value = "0";
	document.customerDiscountForm.product_line.value = "0";
	document.customerDiscountForm.frame_type.value = "0";	
}

function exportCustomerDiscountResult() {
	var exportCustDiscountResult = "customerdiscount.do?invoke=exportCustDiscountSearch&functionType=export";
	props = 'toolbar=0,scrollbars=0,location=0,statusbar=1,menubar=0,resizable=0';
	window.location = exportCustDiscountResult;
}

function clearCustomerDiscountForm() {
	document.customerDiscountForm.manufacturing_location.value = "0";
	document.customerDiscountForm.product_group.value = "0";
	document.customerDiscountForm.product_line.value = "0";
	document.customerDiscountForm.standard.value = "0";
	document.customerDiscountForm.power_rating_kw.value = "0";
	document.customerDiscountForm.frame_type.value = "0";
	document.customerDiscountForm.number_of_poles.value = "0";
	document.customerDiscountForm.mounting_type.value = "0";
	document.customerDiscountForm.tb_position.value = "0";
	document.customerDiscountForm.salesengineerid.value = "0";
	document.customerDiscountForm.customer_name.value = "0";
	document.customerDiscountForm.customer_discount.value = "";
	document.customerDiscountForm.sm_discount.value = "";
	document.customerDiscountForm.rsm_discount.value = "";
	document.customerDiscountForm.nsm_discount.value = "";
	document.customerDiscountForm.gstnumber.value = "0";
	document.customerDiscountForm.sapcode.value = "0";
	document.customerDiscountForm.oraclecode.value = "0";
}

function validateaddCustomerDiscount() {
	if(document.getElementById("manufacturing_location").value =="0"){
        alert("Please select Manufacturing Location. ");
        return false;
    }
	if(document.getElementById("product_group").value =="0"){
        alert("Please select Product Group. ");
        return false;
    }
	if(document.getElementById("product_line").value=="0"){
        alert("Please select Product Line. ");
        return false;
    }
	if(document.getElementById("standard").value=="0"){
        alert("Please select Standard value. ");
        return false;
    }
	if(document.getElementById("power_rating_kw").value=="0"){
        alert("Please select Power Rating (KW). ");
        return false;
    }
	if(document.getElementById("frame_type").value=="0"){
        alert("Please select Frame Type. ");
        return false;
    }
	if(document.getElementById("number_of_poles").value=="0"){
        alert("Please select Number of Poles. ");
        return false;
    }
	if(document.getElementById("mounting_type").value=="0"){
        alert("Please select Mounting Type. ");
        return false;
    }
	if(document.getElementById("tb_position").value=="0"){
        alert("Please select TB Position. ");
        return false;
    }
	if(document.getElementById("customer_name").value=="0"){
        alert("Please select Customer Name. ");
        return false;
    }
	if(document.getElementById("customer_discount").value == ""){
        alert("Please select Customer Discount. ");
        return false;
    }
	if(document.getElementById("sm_discount").value == ""){
        alert("Please select Sales Manager Discount. ");
        return false;
    }
	if(document.getElementById("rsm_discount").value == ""){
        alert("Please select Regional Sales Manager Discount. ");
        return false;
    }
	if(document.getElementById("nsm_discount").value == ""){
        alert("Please select National Sales Manager Discount. ");
        return false;
    }
	
	return true;
}

function addNewCustomerDiscount() {
    document.customerDiscountForm.invoke.value="addNewCustomerDiscount";
    document.customerDiscountForm.submit();
}

function editCustomerDiscount(id) {
    window.location = "customerdiscount.do?invoke=editcustomerdiscount&serial_id="+id;
}

function returnSearchCustomerDiscount() {
	window.location = "customerdiscount.do?invoke=viewdiscount";
}

function deleteCustomerDiscount(id, lName) {

	if (window.confirm("Are you sure to delete the Customer Discount Data ?")) {
		document.customerDiscountForm.manufacturing_location.value = lName;
		document.customerDiscountForm.serial_id.value = id;
		document.customerDiscountForm.invoke.value = "deleteCustomerDiscount";
		document.customerDiscountForm.submit();
	}
}

function validateCustomerBulkupLoad(){
  	 if ( Trim(document.customerDiscountForm.custDiscountBulkupload.value) == "" ) {
  		 	document.customerDiscountForm.custDiscountBulkupload.value="";
  	        alert("Please specify the input file to load records.");
  	        document.customerDiscountForm.custDiscountBulkupload.focus();
  	        return false;
  	 } else {
  	    	var file_name = document.customerDiscountForm.custDiscountBulkupload.value.split(".");
	    	if(file_name[1] != "xls") {
	    		document.customerDiscountForm.custDiscountBulkupload.value="";
	    		alert("Please upload only .xls extention files only");
	    		return false;
			} else {
				document.customerDiscountForm.invoke.value="processBulkUploadCustDiscount";
				document.customerDiscountForm.submit();
			}
  	 }
}