/* Quotation LT - Script Changes */

// Function to Set User - Roles. 
function setUser() {
	window.location = "home.do?invoke=setRoles&accesstoId="+document.getElementById("accesstoId").value;
	//document.forms[0].action = "home.do";
	//document.forms[0].method = "POST";
	//document.forms[0].invoke.value = "setRoles";
	//document.forms[0].submit();
}


function checkTxtNullByName(objName){
	if (objName != null && objName != '') {
		var objValue = $('#'+objName).val();
		if(objValue != null && Trim(objValue) != '') {
			return false;
		} else {
			return true;
		}
	} else {
		return true;
	}
}

/* Function to view List Price list page */
function viewListPrice() {
    var url = 'listprice.do?invoke=viewListPrice';
	window.location = url;
}

/* Function to view Enquiry Creation page */
function newplaceAnEnquiry() {
    var url = 'newPlaceAnEnquiry.do?invoke=viewCreateEnquiry';
	window.location = url;
}

function editNewEnquiry() {
	var enquiryId = document.newenquiryForm.enquiryId.value;
	window.location = 'newPlaceAnEnquiry.do?invoke=getEnquiryDetails&type=edit&enquiryId='+enquiryId;
}

function deleteNewEnquiry() {
	var acptMessage = confirm("Are you sure you want to delete this Enquiry?");
	
	if (acptMessage == true) {
		var enquiryId = document.newenquiryForm.enquiryId.value;
		window.location = 'newPlaceAnEnquiry.do?invoke=deleteDraftEnquiry&enquiryId='+enquiryId;
	} else {
		return false;
	}
}

function addRatingChecks() {
	if($("#customerId").val() == null || $("#customerId").val() == '' || $("#customerId").val() == '0') {
		alert(' Please select a valid Customer Name to Add a New Rating.');
		$("#customerId").val('');
		$('#customerName').val('');
		$('#customerName').focus();
		return false;
	}
	
	return true;
}
function addNewRating() {
	var isCheckPassed = addRatingChecks();
	if(!isCheckPassed) {
		return false;
	}
	document.newenquiryForm.operationType.value = "ADDNEWRATING";
	document.newenquiryForm.submit();
}

function copyPreviousRating() {
	var isCheckPassed = addRatingChecks();
	if(!isCheckPassed) {
		return false;
	}
	document.newenquiryForm.operationType.value = "COPYPREVIOUSRATING";
	document.newenquiryForm.submit();
}

function removeRating(ratingId) {
	if($("#customerId").val() == null || $("#customerId").val() == '' || $("#customerId").val() == '0') {
		alert(' Please select a valid Customer Name to perform this operation.');
		$("#customerId").val('');
		$('#customerName').val('');
		$('#customerName').focus();
		return false;
	}
	var selectedRatingId = $('#ratingObjId'+ratingId).val();
	$('#selectedRemoveRatingId').val(selectedRatingId);
	$('#lastRatingNum').val(ratingId);
	var selectedRatingNo = $('#ratingNo'+ratingId).val();
	$('#prevIdVal').val(selectedRatingNo);
	document.newenquiryForm.operationType.value = "REMOVERATING";
	$('#screen, #modal').show();
	document.newenquiryForm.submit();
}

function saveNewEnquiryDraft(type, ratingId) {
	
	// Validate Attachment Files.
	if (document.getElementById("fileTypestd") != null){
		for(var i=1;i<=10;i++) {
			if (!checkTxtNullByName('theFile'+i)){
				if (!isFileAcceptable($("#theFile"+i).val(), $("#fileTypestd").val()) ) {		
		      		 return false;    
		  		}
			}
		}
	}
	
	var ratingsSize = $('#totalRatingIndex').val();
	
	if(ratingsSize != null && ratingsSize != '') {
		if(parseInt(ratingsSize) == 0 ) {
			alert(' Please enter/add atleast one Rating to Save the Enquiry.');
			return false;
		}
		document.newenquiryForm.operationType.value = "SAVEASDRAFT";
		document.newenquiryForm.submit();
		
	} else {
		alert(' Please enter/add atleast one Rating to Save the Enquiry.');
		return false;
	}
}

function submitNewViewEnquiry() {
	loadCustomerType();
	var customerTypeVal = $('#customerType').val();
	
	var ratingsSize = $('#totalRatingIndex').val();
	
	var hasValidFormFields = true;
	
	// Check If any Ratings are present in the Enquiry.
	if(ratingsSize == null && ratingsSize == '') {
		alert(' Please click Edit to enter/add atleast one Rating to Submit the Enquiry.');
		return false;
	} else {
		if(parseInt(ratingsSize) == 0 ) {
			alert(' Please click Edit to enter/add atleast one Rating to Submit the Enquiry.');
			return false;
		}
	}
	
	if($("#customerId").val() == null || $("#customerId").val() == '' || $("#customerId").val() == '0') {
		hasValidFormFields = false;
	}
	/*if($("#location").val() == null || $("#location").val() == '' || $("#location").val() == '0') {
		hasValidFormFields = false;
	}*/
	if($("#concernedPerson").val() == null || $("#concernedPerson").val() == '') {
		hasValidFormFields = false;
	}
	if($("#enquiryType").val() == null || $("#enquiryType").val() == '' || $("#enquiryType").val() == '0') {
		hasValidFormFields = false;
	}
	if($("#winningChance").val() == null || $("#winningChance").val() == '' || $("#winningChance").val() == '0') {
		hasValidFormFields = false;
	}
	if($("#targeted").val() == null || $("#targeted").val() == '' || $("#targeted").val() == '0') {
		hasValidFormFields = false;
	}
	if($("#receiptDate").val() == null || $("#receiptDate").val() == '') {
		hasValidFormFields = false;
	}
	if($("#submissionDate").val() == null || $("#submissionDate").val() == '') {
		hasValidFormFields = false;
	}
	if($("#closingDate").val() == null || $("#closingDate").val() == '') {
		hasValidFormFields = false;
	}
	
	// Validate Payment Terms Total Value.
	var totalPercentValue = 0;
	var pt1Value = $("#pt1PercentAmtNetorderValue").val();
	var pt2Value = $("#pt2PercentAmtNetorderValue").val();
	var pt3Value = $("#pt3PercentAmtNetorderValue").val();
	var pt4Value = $("#pt4PercentAmtNetorderValue").val();
	var pt5Value = $("#pt5PercentAmtNetorderValue").val();
	
	if(pt1Value != '' || pt1Value != 'Select') {	totalPercentValue = totalPercentValue + parseInt(pt1Value);		}
	if(pt2Value != '' || pt2Value != 'Select') {	totalPercentValue = totalPercentValue + parseInt(pt2Value);		}
	if(pt3Value != '' || pt3Value != 'Select') {	totalPercentValue = totalPercentValue + parseInt(pt3Value);		}
	if(pt4Value != '' || pt4Value != 'Select') {	totalPercentValue = totalPercentValue + parseInt(pt4Value);		}
	if(pt5Value != '' || pt5Value != 'Select') {	totalPercentValue = totalPercentValue + parseInt(pt5Value);		}
	
	if(totalPercentValue < 100) {
		hasValidFormFields = false;
	}
	
	if(pt1Value != '') {
		var pt1PaymentDays = $("#pt1PaymentDays").val();
		var pt1PayableTerms = $("#pt1PayableTerms").val();
		var pt1Instrument = $("#pt1Instrument").val();
		if((pt1PaymentDays == '' || pt1PaymentDays == '0') || (pt1PayableTerms == '' || pt1PayableTerms == '0') || (pt1Instrument == '' || pt1Instrument == '0')) {
			hasValidFormFields = false;
		}
		if(customerTypeVal != null && customerTypeVal == '1') {
			var paymentDaysValue = $("#pt1PaymentDays").val();
			if(parseInt(paymentDaysValue) > 60) {
				hasValidFormFields = false;
			}
		}
	}
	if(pt2Value != '') {
		var pt2PaymentDays = $("#pt2PaymentDays").val();
		var pt2PayableTerms = $("#pt2PayableTerms").val();
		var pt2Instrument = $("#pt2Instrument").val();
		if((pt2PaymentDays == '' || pt2PaymentDays == '0') || (pt2PayableTerms == '' || pt2PayableTerms == '0') || (pt2Instrument == '' || pt2Instrument == '0')) {
			hasValidFormFields = false;
		}
		if(customerTypeVal != null && customerTypeVal == '1') {
			var paymentDaysValue = $("#pt2PaymentDays").val();
			if(parseInt(paymentDaysValue) > 60) {
				hasValidFormFields = false;
			}
		}
	}
	if(pt3Value != '') {
		var pt3PaymentDays = $("#pt3PaymentDays").val();
		var pt3PayableTerms = $("#pt3PayableTerms").val();
		var pt3Instrument = $("#pt3Instrument").val();
		if((pt3PaymentDays == '' || pt3PaymentDays == '0') || (pt3PayableTerms == '' || pt3PayableTerms == '0') || (pt3Instrument == '' || pt3Instrument == '0')) {
			hasValidFormFields = false;
		}
		if(customerTypeVal != null && customerTypeVal == '1') {
			var paymentDaysValue = $("#pt3PaymentDays").val();
			if(parseInt(paymentDaysValue) > 60) {
				hasValidFormFields = false;
			}
		}
	}
	// Check Retention Payment Terms - If Customer Type IS NOT 'Dealer'. 
	if(customerTypeVal != null && (customerTypeVal == '' || customerTypeVal != '1')) {
		if(pt4Value != '') {
			var pt4PaymentDays = $("#pt4PaymentDays").val();
			var pt4PayableTerms = $("#pt4PayableTerms").val();
			var pt4Instrument = $("#pt4Instrument").val();
			if((pt4PaymentDays == '' || pt4PaymentDays == '0') || (pt4PayableTerms == '' || pt4PayableTerms == '0') || (pt4Instrument == '' || pt4Instrument == '0')) {
				hasValidFormFields = false;
			}
		}
		if(pt5Value != '') {
			var pt5PaymentDays = $("#pt5PaymentDays").val();
			var pt5PayableTerms = $("#pt5PayableTerms").val();
			var pt5Instrument = $("#pt5Instrument").val();
			if((pt5PaymentDays == '' || pt5PaymentDays == '0') || (pt5PayableTerms == '' || pt5PayableTerms == '0') || (pt5Instrument == '' || pt5Instrument == '0')) {
				hasValidFormFields = false;
			}
		}
	}

	// Validate Dropdown Fields in Commercial T&C Section
	if($("#deliveryTerm").val() == null || $("#deliveryTerm").val() == '' || $("#deliveryTerm").val() == '0') {
		hasValidFormFields = false;
	}
	if($("#deliveryInFull").val() == null || $("#deliveryInFull").val() == '' || $("#deliveryInFull").val() == '0') {
		hasValidFormFields = false;
	}
	/*if($("#custRequestedDeliveryTime").val() == null || $("#custRequestedDeliveryTime").val() == '' || $("#custRequestedDeliveryTime").val() == '0') {
		hasValidFormFields = false;
	}*/
	if($("#orderCompletionLeadTime").val() == null || $("#orderCompletionLeadTime").val() == '' || $("#orderCompletionLeadTime").val() == '0') {
		hasValidFormFields = false;
	}
	if($("#gstValue").val() == null || $("#gstValue").val() == '' || $("#gstValue").val() == '0') {
		hasValidFormFields = false;
	}
	
	var totalRatingIndex = $("#totalRatingIndex").val();
	
	// For Mandatory Checks
	for(i=1; i<=totalRatingIndex; i++) {
		var index = i;
		
		// Mandatory Fields - Validations
		var qtyValue = $('#quantity'+index).val();
		var prodLineValue = $('#productLine'+index).val();
		var kwValue = $('#kw'+index).val();
		var poleValue = $('#pole'+index).val();
		var frameSizeValue = $('#frame'+index).val();
		var frameSuffixValue = $('#framesuffix'+index).val();
		var mountingValue = $('#mounting'+index).val();
		var tbPosValue = $('#tbPosition'+index).val();
		
		if(qtyValue == null || qtyValue == '') {
			hasValidFormFields = false;
		}
		if(prodLineValue == null || prodLineValue == '' || prodLineValue == '0') {
			hasValidFormFields = false;
		}
		if(kwValue == null || kwValue == '' || kwValue == '0') {
			hasValidFormFields = false;
		}
		if(poleValue == null || poleValue == '' || poleValue == '0') {
			hasValidFormFields = false;
		}
		if(frameSizeValue == null || frameSizeValue == '' || frameSizeValue == '0') {
			hasValidFormFields = false;
		}
		if(frameSuffixValue == null || frameSuffixValue == '' || frameSuffixValue == '0') {
			hasValidFormFields = false;
		}
		if(mountingValue == null || mountingValue == '' || mountingValue == '0') {
			hasValidFormFields = false;
		}
		if(tbPosValue == null || tbPosValue == '' || tbPosValue == '0') {
			hasValidFormFields = false;
		}
		
		/*
		var startingMethodVal = $('#methodOfStarting'+index).find("option:selected").text().trim();
		var dutyValue = $('#duty'+index).find("option:selected").text().trim();
		
		if(startingMethodVal.includes("VFD") ) {
			if($('#vfdType'+index).val() == '0') {
				hasValidFormFields = false;
			}
		}
		if(startingMethodVal.includes("VFD") && dutyValue == 'S1') {
			if($('#vfdSpeedRangeMin'+index).val() == '0') {
				hasValidFormFields = false;
			}
			if($('#vfdSpeedRangeMax'+index).val() == '0') {
				hasValidFormFields = false;
			}
		}
		var glandPlateValue = $('#glandPlate'+index).find("option:selected").text().trim();
		if(glandPlateValue == 'Drilled') {
			if($('#cableSize'+index).val() == null || $('#cableSize'+index).val() == '' || $('#cableSize'+index).val() == '0') {
				hasValidFormFields = false;
			}
		}
		*/
	}
	
	// If 'hasValidFormFields' = false, Show alert and prevent from Submit.
	if(!hasValidFormFields) {
		alert('All the fields for this Enquiry does not have valid values. \n Please click on Edit to enter valid values. ');
		return false;
	}
	
	//document.newenquiryForm.dispatch.value = '';	
	document.newenquiryForm.operationType.value = 'SUBMIT';	
	document.newenquiryForm.submit();
}

function submitNewEnquiry(type) {
	
	// Validate Attachment Files.
	if (document.getElementById("fileTypestd") != null){
		for(var i=1;i<=10;i++) {
			if (!checkTxtNullByName('theFile'+i)){
				if (!isFileAcceptable($("#theFile"+i).val(), $("#fileTypestd").val()) ) {		
		      		 return false;    
		  		}
			}
		}
	}
	
	loadCustomerType();
	var customerTypeVal = $('#customerType').val();
	
	var ratingsSize = $('#totalRatingIndex').val();
	
	var onlyAlphaChars = /^[a-zA-Z]+(\s([a-zA-Z])+)*$/;
	var onlyAlphaNumericChars = /^[0-9a-zA-Z]+(\s([0-9a-zA-Z])+)*$/;
	var onlyNumericChars = /^[0-9]+$/;
	
	// Check If any Ratings are present in the Enquiry.
	if(ratingsSize == null && ratingsSize == '') {
		alert(' Please enter/add atleast one Rating to Submit the Enquiry.');
		return false;
	} else {
		if(parseInt(ratingsSize) == 0 ) {
			alert(' Please enter/add atleast one Rating to Submit the Enquiry.');
			return false;
		}
	}
	
	if($("#customerId").val() == null || $("#customerId").val() == '' || $("#customerId").val() == '0') {
		alert(' Please select a valid Customer Name to Submit the Enquiry.');
		$("#customerId").val('');
		$('#customerName').val('');
		$('#customerName').focus();
		return false;
	}
	/*if($("#location").val() == null || $("#location").val() == '' || $("#location").val() == '0') {
		alert(' Please select a valid Location to Submit the Enquiry.');
		$('#location').val('');
		return false;
	}*/
	if($("#concernedPerson").val() == null || $("#concernedPerson").val() == '') {
		alert(' Please enter a valid Customer Contact to Submit the Enquiry.');
		$('#concernedPerson').focus();
		return false;
	}
	if($("#concernedPerson").val() != '' && !onlyAlphaChars.test(Trim($("#concernedPerson").val())) ) {
		alert("Please enter valid Customer Contact, only Alphabets are allowed.");
		$("#concernedPerson").focus();
		return false;
	}
	if($("#projectName").val() != '' && !onlyAlphaChars.test(Trim($("#projectName").val())) ) {
		alert("Please enter valid Project Name, only Alphabets are allowed.");
		$("#projectName").focus();
		return false;
	}
	if($("#consultantName").val() != '' && !onlyAlphaChars.test(Trim($("#consultantName").val())) ) {
		alert("Please enter valid Consultant Name, only Alphabets are allowed.");
		$("#consultantName").focus();
		return false;
	}
	if($("#endUser").val() != '' && !onlyAlphaChars.test(Trim($("#endUser").val())) ) {
		alert("Please enter valid End User name, only Alphabets are allowed.");
		$("#endUser").focus();
		return false;
	}
	/*if($("#endUserIndustry").val() == null || $("#endUserIndustry").val() == '' || $("#endUserIndustry").val() == '0') {
		alert(' Please select a valid End User Industry to Submit the Enquiry.');
		$('#endUserIndustry').val('');
		return false;
	}*/
	if($("#enquiryType").val() == null || $("#enquiryType").val() == '' || $("#enquiryType").val() == '0') {
		alert(' Please select a valid Enquiry Type to Submit the Enquiry.');
		$('#enquiryType').focus();
		return false;
	}
	if($("#winningChance").val() == null || $("#winningChance").val() == '' || $("#winningChance").val() == '0') {
		alert(' Please select a valid Winning Chance value, to Submit the Enquiry.');
		$('#winningChance').focus();
		return false;
	}
	if($("#targeted").val() == null || $("#targeted").val() == '' || $("#targeted").val() == '0') {
		alert(' Please select a valid Targeted value, to Submit the Enquiry.');
		$('#targeted').focus();
		return false;
	}
	if($("#receiptDate").val() == null || $("#receiptDate").val() == '') {
		alert(' Please enter a valid Enquiry Receipt date value, to Submit the Enquiry.');
		$('#receiptDate').focus();
		return false;
	}
	if($("#submissionDate").val() == null || $("#submissionDate").val() == '') {
		alert(' Please enter a valid Required offer Submission date value, to Submit the Enquiry.');
		$('#submissionDate').focus();
		return false;
	}
	if($("#closingDate").val() == null || $("#closingDate").val() == '') {
		alert(' Please enter a valid Order Closing date value, to Submit the Enquiry.');
		$('#closingDate').focus();
		return false;
	}
	
	
	// Validate Payment Terms Total Value.
	var totalPercentValue = 0;
	var pt1Value = $("#pt1PercentAmtNetorderValue option:selected").text().trim();
	var pt2Value = $("#pt2PercentAmtNetorderValue option:selected").text().trim();
	var pt3Value = $("#pt3PercentAmtNetorderValue option:selected").text().trim();
	var pt4Value = $("#pt4PercentAmtNetorderValue option:selected").text().trim();
	var pt5Value = $("#pt5PercentAmtNetorderValue option:selected").text().trim();
	
	if(pt1Value != 'Select') {
		totalPercentValue = totalPercentValue + parseInt(pt1Value);
	}
	if(pt2Value != 'Select') {
		totalPercentValue = totalPercentValue + parseInt(pt2Value);
	}
	if(pt3Value != 'Select') {
		totalPercentValue = totalPercentValue + parseInt(pt3Value);
	}
	if(pt4Value != 'Select') {
		totalPercentValue = totalPercentValue + parseInt(pt4Value);
	}
	if(pt5Value != 'Select') {
		totalPercentValue = totalPercentValue + parseInt(pt5Value);
	}
	
	if(totalPercentValue < 100) {
		alert(' The overall Sum of %age amount of Nett. order value should be equal to 100%.\n Please change the Payment Terms accordingly.');
		return false;
	}
	
	if(pt1Value != 'Select') {
		var pt1PaymentDays = $("#pt1PaymentDays").val();
		var pt1PayableTerms = $("#pt1PayableTerms").val();
		var pt1Instrument = $("#pt1Instrument").val();
		if((pt1PaymentDays == '' || pt1PaymentDays == '0') || (pt1PayableTerms == '' || pt1PayableTerms == '0') || (pt1Instrument == '' || pt1Instrument == '0')) {
			alert(' Please select appropriate values for Payment Days, Payable Terms and Instrument for Advance Payment 1 - Payment Term.');
			return false;
		}
		if(customerTypeVal != null && customerTypeVal == '1') {
			var paymentDaysValue = $("#pt1PaymentDays option:selected").text().trim();
			if(parseInt(paymentDaysValue) > 60) {
				alert("The Payment Days for Customer Type Dealer cannot be more than 60 Days. \n Please select value less than or equal to 60 days.");
				$('#pt1PaymentDays').val('0');
				$('#pt1PaymentDays').focus();
				return false;
			}
		}
	}
	if(pt2Value != 'Select') {
		var pt2PaymentDays = $("#pt2PaymentDays").val();
		var pt2PayableTerms = $("#pt2PayableTerms").val();
		var pt2Instrument = $("#pt2Instrument").val();
		if((pt2PaymentDays == '' || pt2PaymentDays == '0') || (pt2PayableTerms == '' || pt2PayableTerms == '0') || (pt2Instrument == '' || pt2Instrument == '0')) {
			alert(' Please select appropriate values for Payment Days, Payable Terms and Instrument for Advance Payment 2 - Payment Term.');
			return false;
		}
		if(customerTypeVal != null && customerTypeVal == '1') {
			var paymentDaysValue = $("#pt2PaymentDays option:selected").text().trim();
			if(parseInt(paymentDaysValue) > 60) {
				alert("The Payment Days for Customer Type Dealer cannot be more than 60 Days. \n Please select value less than or equal to 60 days.");
				$('#pt2PaymentDays').val('0');
				$('#pt2PaymentDays').focus();
				return false;
			}
		}
	}
	if(pt3Value != 'Select') {
		var pt3PaymentDays = $("#pt3PaymentDays").val();
		var pt3PayableTerms = $("#pt3PayableTerms").val();
		var pt3Instrument = $("#pt3Instrument").val();
		if((pt3PaymentDays == '' || pt3PaymentDays == '0') || (pt3PayableTerms == '' || pt3PayableTerms == '0') || (pt3Instrument == '' || pt3Instrument == '0')) {
			alert(' Please select appropriate values for Payment Days, Payable Terms and Instrument for Main Payment - Payment Term.');
			return false;
		}
		if(customerTypeVal != null && customerTypeVal == '1') {
			var paymentDaysValue = $("#pt3PaymentDays option:selected").text().trim();
			if(parseInt(paymentDaysValue) > 60) {
				alert("The Payment Days for Customer Type Dealer cannot be more than 60 Days. \n Please select value less than or equal to 60 days.");
				$('#pt3PaymentDays').val('0');
				$('#pt3PaymentDays').focus();
				return false;
			}
		}
	}
	// Check Retention Payment Terms - If Customer Type IS NOT 'Dealer'. 
	if(customerTypeVal != null && (customerTypeVal == '' || customerTypeVal != '1')) {
		if(pt4Value != 'Select') {
			var pt4PaymentDays = $("#pt4PaymentDays").val();
			var pt4PayableTerms = $("#pt4PayableTerms").val();
			var pt4Instrument = $("#pt4Instrument").val();
			if((pt4PaymentDays == '' || pt4PaymentDays == '0') || (pt4PayableTerms == '' || pt4PayableTerms == '0') || (pt4Instrument == '' || pt4Instrument == '0')) {
				alert(' Please select appropriate values for Payment Days, Payable Terms and Instrument for Retention Payment 1 - Payment Term.');
				return false;
			}
		}
		if(pt5Value != 'Select') {
			var pt5PaymentDays = $("#pt5PaymentDays").val();
			var pt5PayableTerms = $("#pt5PayableTerms").val();
			var pt5Instrument = $("#pt5Instrument").val();
			if((pt5PaymentDays == '' || pt5PaymentDays == '0') || (pt5PayableTerms == '' || pt5PayableTerms == '0') || (pt5Instrument == '' || pt5Instrument == '0')) {
				alert(' Please select appropriate values for Payment Days, Payable Terms and Instrument for Retention Payment 2 - Payment Term.');
				return false;
			}
		}
	}

	// Validate Dropdown Fields in Commercial T&C Section
	if($("#deliveryTerm").val() == null || $("#deliveryTerm").val() == '' || $("#deliveryTerm").val() == '0') {
		alert(' Please select a valid Delivery Term value, to Submit the Enquiry.');
		$('#deliveryTerm').focus();
		return false;
	}
	if($("#deliveryInFull").val() == null || $("#deliveryInFull").val() == '' || $("#deliveryInFull").val() == '0') {
		alert(' Please select a valid Delivery value, to Submit the Enquiry.');
		$('#deliveryInFull').focus();
		return false;
	}
	if($("#custRequestedDeliveryTime").val() == null || $("#custRequestedDeliveryTime").val() == '' || $("#custRequestedDeliveryTime").val() == '0') {
		alert(' Please select a valid Customer Requested Delivery value, to Submit the Enquiry.');
		$('#custRequestedDeliveryTime').focus();
		return false;
	}
	if($("#orderCompletionLeadTime").val() == null || $("#orderCompletionLeadTime").val() == '' || $("#orderCompletionLeadTime").val() == '0') {
		alert(' Please select a valid Order Completion Lead Time value, to Submit the Enquiry.');
		$('#orderCompletionLeadTime').focus();
		return false;
	}
	if($("#gstValue").val() == null || $("#gstValue").val() == '' || $("#gstValue").val() == '0') {
		alert(' Please select a valid GST value, to Submit the Enquiry.');
		$('#gstValue').focus();
		return false;
	}
	
	
	var totalRatingIndex = $("#totalRatingIndex").val();
	
	// For Mandatory Checks
	for(i=1; i<=totalRatingIndex; i++) {
		var index = i;
		
		// Mandatory Fields - Validations
		var qtyValue = $('#quantity'+index).val();
		var prodLineValue = $('#productLine'+index).val();
		var kwValue = $('#kw'+index).val();
		var poleValue = $('#pole'+index).val();
		var frameSizeValue = $('#frame'+index).val();
		var frameSuffixValue = $('#framesuffix'+index).val();
		var mountingValue = $('#mounting'+index).val();
		var tbPosValue = $('#tbPosition'+index).val();
		
		if(qtyValue == null || qtyValue == '') {
			alert(' Please enter a valid Quantity value, for Rating - ' + index);
			$('#quantity'+index).focus();
			return false;
		}
		if(prodLineValue == null || prodLineValue == '' || prodLineValue == '0') {
			alert(' Please select a valid Product Line, for Rating - ' + index);
			$('#productLine'+index).focus();
			return false;
		}
		if(kwValue == null || kwValue == '' || kwValue == '0') {
			alert(' Please select a valid Power Rating(KW) value, for Rating - ' + index);
			$('#kw'+index).focus();
			return false;
		}
		if(poleValue == null || poleValue == '' || poleValue == '0') {
			alert(' Please select valid No. of Poles, for Rating - ' + index);
			$('#pole'+index).focus();
			return false;
		}
		if(frameSizeValue == null || frameSizeValue == '' || frameSizeValue == '0') {
			alert(' Please select valid Frame size, for Rating - ' + index);
			$('#frame'+index).focus();
			return false;
		}
		if(frameSuffixValue == null || frameSuffixValue == '' || frameSuffixValue == '0') {
			alert(' Please select valid Frame suffix, for Rating - ' + index);
			$('#framesuffix'+index).focus();
			return false;
		}
		if(mountingValue == null || mountingValue == '' || mountingValue == '0') {
			alert(' Please select valid Mounting, for Rating - ' + index);
			$('#mounting'+index).focus();
			return false;
		}
		if(tbPosValue == null || tbPosValue == '' || tbPosValue == '0') {
			alert(' Please select valid TB Position, for Rating - ' + index);
			$('#tbPosition'+index).focus();
			return false;
		}

		
		// Text Fields - Alphabetic or Alpha-Numeric - Validations.
		if($("#tagNumber"+index).val() != '' && !onlyAlphaNumericChars.test(Trim($("#tagNumber"+index).val())) ) {
			alert("Please enter valid Tag Number# value, only Alphabets and Numbers are allowed.");
			$("#tagNumber"+index).focus();
			return false;
		}
		
		var startingMethodVal = $('#methodOfStarting'+index).find("option:selected").text().trim();
		var dutyValue = $('#duty'+index).find("option:selected").text().trim();
		
		if(startingMethodVal.includes("VFD") ) {
			if($('#vfdType'+index).val() == '0') {
				alert("Please select 'VFD Application Type' if Method of Starting is 'VFD' for Rating - " + index);
				return false;
			}
		}
		
		if(startingMethodVal.includes("VFD") && dutyValue == 'S1') {
			if($('#vfdSpeedRangeMin'+index).val() == '0') {
				alert("Please select 'Speed Range only for VFD motor (Min.)' if Method of Starting is 'VFD' and Duty is 'S1' : for Rating - " + index);
				return false;
			}
			if($('#vfdSpeedRangeMax'+index).val() == '0') {
				alert("Please select 'Speed Range only for VFD motor (Max.)' if Method of Starting is 'VFD' and Duty is 'S1' : for Rating - " + index);
				return false;
			}
		}
		
		if($("#altitude"+index).val() != '' && !onlyNumericChars.test(Trim($("#altitude"+index).val())) ) {
			alert("Please enter valid Altitude value, only Numbers are allowed.");
			$("#altitude"+index).focus();
			return false;
		}
		if($("#earlierSuppliedMotorSerialNo"+index).val() != '' && !onlyAlphaNumericChars.test(Trim($("#earlierSuppliedMotorSerialNo"+index).val())) ) {
			alert("Please enter valid Earlier Supplied Motor Serial No., only Alphabets and Numbers are allowed.");
			$("#earlierSuppliedMotorSerialNo"+index).focus();
			return false;
		}
		if($("#standardDelivery"+index).val() != '' && !onlyNumericChars.test(Trim($("#standardDelivery"+index).val())) ) {
			alert("Please enter valid Standard Delivery (Weeks), only Numbers are allowed.");
			$("#standardDelivery"+index).focus();
			return false;
		}
		if($("#customerRequestedDelivery"+index).val() != '' && !onlyNumericChars.test(Trim($("#customerRequestedDelivery"+index).val())) ) {
			alert("Please enter valid Customer Requested Delivery (Weeks), only Numbers are allowed.");
			$("#customerRequestedDelivery"+index).focus();
			return false;
		}
		
		if($("#rv"+index).val() != '' && !onlyAlphaNumericChars.test(Trim($("#rv"+index).val())) ) {
			alert("Please enter valid RV value, only Alphabets and Numbers are allowed.");
			$("#rv"+index).focus();
			return false;
		}
		if($("#ra"+index).val() != '' && !onlyAlphaNumericChars.test(Trim($("#ra"+index).val())) ) {
			alert("Please enter valid RA value, only Alphabets and Numbers are allowed.");
			$("#ra"+index).focus();
			return false;
		}
		
		var glandPlateValue = $('#glandPlate'+index).find("option:selected").text().trim();
		if(glandPlateValue == 'Drilled') {
			if($('#cableSize'+index).val() == null || $('#cableSize'+index).val() == '' || $('#cableSize'+index).val() == '0') {
				alert(' Please select a valid Cable Size value, Cable Size cannot be empty when Gland Plate value is Drilled.');
				$('#cableSize'+index).focus();
				return false;
			}
		}
		
	}
	
	if(type == 'REFERDESIGN' && $('#smNote').val() == '') {
		alert("Please enter Comments in Note By SM (Internal) field before Refering the Enquiry to Design Team.");
		return false;
	}
	
	document.newenquiryForm.operationType.value = type;	
	document.newenquiryForm.submit();
}


var iProgress = 0;
function makeProgress() {
	if (iProgress < 100) {
		iProgress = iProgress + 1;
		$(".progress-bar").css("width", iProgress + "%").text(" Progress");
	}
	// Wait for sometime before running this script again
	//setTimeout("makeProgress()", 100);
}

function calculateLPPerUnitAndDiscount() {
	
	//$("#calcPriceDiv").css("display", "block");
	document.getElementById('calcPriceDiv').style.display = "block";
	
	//setTimeout(function(){ alert("Hello"); }, 3000);
	
	var totalRatingIndex = $("#totalRatingIndex").val();
	
	for(i=1; i<=totalRatingIndex; i++) {
		var fieldId = i;
		var quantityVal = $("#quantity"+fieldId).val();
		var customerIdVal = $("#customerId").val();
		var mfgLocationVal = $("#mfgLocation"+fieldId).val();
		var prodGroupVal = $("#productGroup"+fieldId).val();
		var prodLineVal = $("#productLine"+fieldId).val();
		var kwVal = $("#kw"+fieldId).val();
		var frameVal = $("#frame"+fieldId).val();
		var poleVal = $("#pole"+fieldId).val();
		var mountingVal = $("#mounting"+fieldId).val();
		var tbPositionVal = $("#tbPosition"+fieldId).val();
		
		$.ajax({
			type : "POST",
			url : "newPlaceAnEnquiry.do",
			dataType : "xml",
			data : {
				mfgLocationVal : mfgLocationVal,		// AMD/KOL
				prodGroupVal : prodGroupVal,
				prodLineVal : prodLineVal,
				kwVal : kwVal,
				frameVal : frameVal,
				poleVal : poleVal,
				mountingVal : mountingVal,
				tbPositionVal : tbPositionVal,
				quantityVal : quantityVal,
				customerIdVal : customerIdVal,
				"invoke" : "calculateLPPerUnitValueAndDiscountByAjax"
			},
			success : function(data) {
				console.log($(data).find('msg').text());
				console.log($(data).find('returnValue1').text());
				if($(data).find('returnValue1').text() != '' && $(data).find('returnValue1').text() != '0') {
					$('#lpPerUnit'+fieldId).val($(data).find('returnValue1').text());
				} else {
					$('#lpPerUnit'+fieldId).val('');
				}
				if($(data).find('returnValue2').text() != '' && $(data).find('returnValue2').text() != '0') {
					$('#standardDiscount'+fieldId).val($(data).find('returnValue2').text());
				} else {
					$('#standardDiscount'+fieldId).val('');
				}
			},
			error : function(xhr, ajaxOptions, thrownError) {
				return false;
			}
		});
		
		// Invoke CalculateTotalAddOn function.
		calculateTotalAddOn(fieldId);
		
		// Invoke CalculatePriceValues function.
		calculatePriceValues(fieldId);
	}
	
	document.getElementById('calcPriceDiv').style.display = "none";
	//$("#calcPriceDiv").css("display", "none");
}

function calculateTotalAddOn(fieldId) {
	var prodGroupVal = $("#productGroup"+fieldId).val();
	var prodLineVal = $("#productLine"+fieldId).val();
	var frameVal = $("#frame"+fieldId).val();
	var nonStdVoltageVal = $("#nonStandardVoltage"+fieldId).val();
	var windingTreatmentVal = $("#windingTreatment"+fieldId).val();
	var shaftTypeVal = $("#shaftType"+fieldId).val();
	var shaftMaterialVal = $("#shaftMaterialType"+fieldId).val();
	var spaceHeaterVal = $("#spaceHeater"+fieldId).val();
	var hardwareVal = $("#hardware"+fieldId).val();
	var doubleCompressGlandVal = $("#doubleCompressGland"+fieldId).val();
	var mountingVal = $("#mounting"+fieldId).val();
	var paintTypeVal = $("#paintingType"+fieldId).val();
	var paintShadeVal = $("#paintShade"+fieldId).val();
	var paintThicknessVal = $("#paintThickness"+fieldId).val();
	var insulationClassVal = $("#insulationClass"+fieldId).val();
	var terminalBoxVal = $("#terminalBoxSize"+fieldId).val();
	var spreaderBoxVal = $("#spreaderBox"+fieldId).val();
	var auxTBVal = $("#auxTerminalBox"+fieldId).val();
	var vibrationLevelVal = $("#vibration"+fieldId).val();
	var flyingLeadWithoutTBVal = $("#flyingLeadWithoutTB"+fieldId).val();
	var flyingLeadWithTBVal = $("#flyingLeadWithTB"+fieldId).val();
	var seaworthyVal = $("#seaworthyPacking"+fieldId).val();
	var addlNamePlateVal = $("#addNamePlate"+fieldId).val();
	var directionArrowPlateVal = $("#directionArrowPlate"+fieldId).val();
	var witnessRoutineVal = $("#witnessRoutine"+fieldId).val();
	var warrantyVal = $("#warranty"+fieldId).val();
	var additionalTest1Val = $("#additional1Test"+fieldId).val();
	var additionalTest2Val = $("#additional2Test"+fieldId).val();
	var additionalTest3Val = $("#additional3Test"+fieldId).val();
	var typeTestVal = $("#typeTest"+fieldId).val();
	var rtdVal = $("#rtd"+fieldId).val();
	var thermisterVal = $("#thermister"+fieldId).val();
	var btdVal = $("#btd"+fieldId).val();
	var techoMountingVal = $("#techoMounting"+fieldId).val();
	var metalFanVal = $("#metalFan"+fieldId).val();
	var methodOfCoolVal = $("#methodOfCooling"+fieldId).val();
	var hazardAreaVal = $("#hazardArea"+fieldId).val();
	var cableSealingBoxVal = $("#cableSealingBox"+fieldId).val();
	var bearingNDEVal = $("#bearingNDE"+fieldId).val();
	var bearingDEVal = $("#bearingDE"+fieldId).val();
	var gasGroupVal = $("#gasGroup"+fieldId).val();
	var shaftGroundingVal = $("#shaftGrounding"+fieldId).val();
	var spmMountingVal = $("#spmMountingProvision"+fieldId).val();
	var vfdTypeVal = $("#vfdType"+fieldId).val();
	var certificationVal = $("#ulce"+fieldId).val();
	var ipVal = $("#ip"+fieldId).val();
	var leadVal = $("#lead"+fieldId).val();
	
	$.ajax({
		type : "POST",
		url : "newPlaceAnEnquiry.do",
		dataType : "xml",
		data : {
			prodGroupVal : prodGroupVal,
			prodLineVal : prodLineVal,
			frameVal : frameVal,
			nonStdVoltageVal : nonStdVoltageVal,
			windingTreatmentVal : windingTreatmentVal,
			shaftTypeVal : shaftTypeVal,
			shaftMaterialVal : shaftMaterialVal,
			spaceHeaterVal : spaceHeaterVal,
			hardwareVal : hardwareVal,
			doubleCompressGlandVal : doubleCompressGlandVal,
			mountingVal : mountingVal,
			paintTypeVal : paintTypeVal,
			paintShadeVal : paintShadeVal,
			paintThicknessVal : paintThicknessVal,
			insulationClassVal : insulationClassVal,
			terminalBoxVal : terminalBoxVal,
			spreaderBoxVal : spreaderBoxVal,
			auxTBVal : auxTBVal,
			vibrationLevelVal : vibrationLevelVal,
			flyingLeadWithoutTBVal : flyingLeadWithoutTBVal,
			flyingLeadWithTBVal : flyingLeadWithTBVal,
			seaworthyVal : seaworthyVal,
			addlNamePlateVal : addlNamePlateVal,
			directionArrowPlateVal : directionArrowPlateVal,
			witnessRoutineVal : witnessRoutineVal,
			warrantyVal : warrantyVal,
			additionalTest1Val : additionalTest1Val,
			additionalTest2Val : additionalTest2Val,
			additionalTest3Val : additionalTest3Val,
			typeTestVal : typeTestVal,
			rtdVal : rtdVal,
			thermisterVal : thermisterVal,
			btdVal : btdVal,
			techoMountingVal : techoMountingVal,
			metalFanVal : metalFanVal,
			methodOfCoolVal : methodOfCoolVal,
			hazardAreaVal : hazardAreaVal,
			cableSealingBoxVal : cableSealingBoxVal,
			bearingNDEVal : bearingNDEVal,
			bearingDEVal : bearingDEVal,
			gasGroupVal : gasGroupVal,
			shaftGroundingVal : shaftGroundingVal,
			spmMountingVal : spmMountingVal,
			vfdTypeVal : vfdTypeVal,
			certificationVal : certificationVal,
			ipVal : ipVal,
			leadVal : leadVal,
			"invoke" : "calculateTotalAddOnByAjax"
		},
		success : function(data) {
			console.log($(data).find('msg').text());
			console.log($(data).find('returnValue1').text());
			if($(data).find('returnValue1').text() != '' && $(data).find('returnValue1').text() != '0') {
				$('#totalAddonPercent'+fieldId).val($(data).find('returnValue1').text());
			} else {
				$('#totalAddonPercent'+fieldId).val('');
			}
			if($(data).find('returnValue2').text() != '' && $(data).find('returnValue2').text() != '0') {
				$('#totalCashExtra'+fieldId).val($(data).find('returnValue2').text());
			} else {
				$('#totalCashExtra'+fieldId).val('');
			}
		},
		error : function(xhr, ajaxOptions, thrownError) {
			return false;
		}
	});
}

function calculatePriceValues(fieldId) {
	var quantityVal = '';
	var totalAddOnPercentVal = '';
	var totalAddOnCashExtraVal = '';
	var lpPerUnitVal = '';
	var discountVal = '';
	var lpWithAddOnPerUnitVal = 0;
	var pricePerUnitVal = 0;
	var totalOrderPriceVal = 0;
	
	if($("#quantity"+fieldId).val() != '') {
		quantityVal = parseInt($("#quantity"+fieldId).val());
	} else {
		quantityVal = 1;
	}
	
	if($("#totalAddonPercent"+fieldId).val() != '') {
		totalAddOnPercentVal = parseFloat($("#totalAddonPercent"+fieldId).val());
	} else {
		totalAddOnPercentVal = 0;
	}
	
	if($("#totalCashExtra"+fieldId).val() != '') {
		totalAddOnCashExtraVal = parseFloat($("#totalCashExtra"+fieldId).val());
	} else {
		totalAddOnCashExtraVal = 0;
	}
	
	if($("#lpPerUnit"+fieldId).val() != '') {
		lpPerUnitVal = parseInt($("#lpPerUnit"+fieldId).val());
	} else {
		lpPerUnitVal = 0;
	}
	
	if($("#standardDiscount"+fieldId).val() != '') {
		discountVal = parseInt($("#standardDiscount"+fieldId).val());
	} else {
		discountVal = 0;
	}
	
	// Calculation Starts
	if(lpPerUnitVal > 0 && discountVal > 0) {
		lpWithAddOnPerUnitVal = (lpPerUnitVal + (lpPerUnitVal * totalAddOnPercentVal/100)) + totalAddOnCashExtraVal;
		if(lpWithAddOnPerUnitVal != 0) { lpWithAddOnPerUnitVal = Math.round(lpWithAddOnPerUnitVal); }
		
		pricePerUnitVal = ((lpPerUnitVal + (lpPerUnitVal * totalAddOnPercentVal/100)) * (1 - discountVal/100)) + totalAddOnCashExtraVal;
		if(pricePerUnitVal != 0) { pricePerUnitVal = Math.round(pricePerUnitVal); }
		
		totalOrderPriceVal = quantityVal * pricePerUnitVal;
		if(totalOrderPriceVal != 0) { totalOrderPriceVal = Math.round(totalOrderPriceVal); }
	}
	
	if(totalAddOnPercentVal == 0) { $("#totalAddonPercent"+fieldId).val(''); } else { $("#totalAddonPercent"+fieldId).val(totalAddOnPercentVal); }
	
	if(totalAddOnCashExtraVal == 0) { 
		$("#totalCashExtra"+fieldId).val(''); 
	} else { 
		totalAddOnCashExtraVal = Math.round(totalAddOnCashExtraVal);
		$("#totalCashExtra"+fieldId).val(formatNumber(totalAddOnCashExtraVal));
	}
	
	if(lpPerUnitVal == 0) { 
		$("#lpPerUnit"+fieldId).val(''); 
	} else { 
		lpPerUnitVal = Math.round(lpPerUnitVal);
		$("#lpPerUnit"+fieldId).val(formatNumber(lpPerUnitVal)); 
	}
	
	if(discountVal == 0) { $("#standardDiscount"+fieldId).val(''); } else { $("#standardDiscount"+fieldId).val(discountVal); }
	
	if(lpWithAddOnPerUnitVal == 0) { $("#lpWithAddonPerUnit"+fieldId).val(''); } else { $("#lpWithAddonPerUnit"+fieldId).val(formatNumber(lpWithAddOnPerUnitVal)); }
	
	if(pricePerUnitVal == 0) { $("#pricePerUnit"+fieldId).val(''); } else { $("#pricePerUnit"+fieldId).val(formatNumber(pricePerUnitVal)); }
	
	if(totalOrderPriceVal == 0) { $("#totalOrderPrice"+fieldId).val(''); } else { $("#totalOrderPrice"+fieldId).val(formatNumber(totalOrderPriceVal)); }
}

function calculateSMApprovePrices() {
	var totalRatingIndex = $("#totalRatingIndex").val();
	var quoted_TotalPrice_Cummulative = 0;
	var requested_TotalPrice_Cummulative = 0;
	
	for(i=1; i<=totalRatingIndex; i++) {
		var q_pricePerUnit = 0;
		var q_totalPrice = 0;
		var r_pricePerUnit = 0;
		var r_totalPrice = 0;
		
		var quantityVal = parseInt($("#quantity"+i).val());
		var lpPerUnitVal = parseInt($("#lpPerUnit"+i).val());
		var totalAddOnPercentVal = parseInt($("#totalAddOnPercent"+i).val());
		if(totalAddOnPercentVal == null || totalAddOnPercentVal == '' || Number.isNaN(totalAddOnPercentVal)) { totalAddOnPercentVal = 0; }
		var totalCashExtraVal = parseInt($("#totalCashExtra"+i).val());
		if(totalCashExtraVal == null || totalCashExtraVal == '' || Number.isNaN(totalCashExtraVal) ) { totalCashExtraVal = 0; }
		
		var quotedDiscountVal = parseInt($("#quotedDiscount"+i).val());
		if(quotedDiscountVal == null || quotedDiscountVal =='' || Number.isNaN(quotedDiscountVal)) { quotedDiscountVal = 0; }
		if(quotedDiscountVal > 0) {
			q_pricePerUnit = ((lpPerUnitVal + (lpPerUnitVal * totalAddOnPercentVal/100)) * (1 - quotedDiscountVal/100)) + totalCashExtraVal;
			q_pricePerUnit = Math.round(q_pricePerUnit);
			q_totalPrice = quantityVal * q_pricePerUnit;
			quoted_TotalPrice_Cummulative = quoted_TotalPrice_Cummulative + q_totalPrice;
			$("#quotedPricePerUnit"+i).val(formatNumber(q_pricePerUnit));
			$("#quotedTotalPrice"+i).val(formatNumber(q_totalPrice));
		}
		
		var requestedDiscountVal = parseInt($("#requestedDiscount"+i).val());
		if(requestedDiscountVal == null || requestedDiscountVal =='' || Number.isNaN(requestedDiscountVal)) { requestedDiscountVal = 0; }
		if(requestedDiscountVal > 0) {
			r_pricePerUnit = ((lpPerUnitVal + (lpPerUnitVal * totalAddOnPercentVal/100)) * (1 - requestedDiscountVal/100)) + totalCashExtraVal;
			r_pricePerUnit = Math.round(r_pricePerUnit);
			r_totalPrice = quantityVal * r_pricePerUnit;
			requested_TotalPrice_Cummulative = requested_TotalPrice_Cummulative + r_totalPrice;
			$("#requestedPricePerUnit"+i).val(formatNumber(r_pricePerUnit));
			$("#requestedTotalPrice"+i).val(formatNumber(r_totalPrice));
		}
	}
	$("#q_TotalPrice_Cumm").val(formatNumber(quoted_TotalPrice_Cummulative));
	$("#r_TotalPrice_Cumm").val(formatNumber(requested_TotalPrice_Cummulative));
}

function calculateApprovePrices(userType) {
	var totalRatingIndex = $("#totalRatingIndex").val();
	var r_CummPrice = 0;
	var a_CummPrice = 0;
	
	for(i=1; i<=totalRatingIndex; i++) {
		var r_DiscountVal = 0;
		var r_pricePerUnit = 0;
		var r_totalPrice = 0;
		var a_DiscountVal = 0;
		var a_pricePerUnit = 0;
		var a_totalPrice = 0;
		
		var quantityVal = parseInt($("#quantity"+i).val());
		var lpPerUnitVal = parseInt($("#lpPerUnit"+i).val());
		var totalAddOnPercentVal = parseInt($("#totalAddOnPercent"+i).val());
		if(totalAddOnPercentVal == null || totalAddOnPercentVal == '' || Number.isNaN(totalAddOnPercentVal)) { totalAddOnPercentVal = 0; }
		var totalCashExtraVal = parseInt($("#totalCashExtra"+i).val());
		if(totalCashExtraVal == null || totalCashExtraVal == '' || Number.isNaN(totalCashExtraVal) ) { totalCashExtraVal = 0; }
		
		if(userType == 'RSM') {
			r_DiscountVal = parseInt($("#reqDiscount_RSM"+i).val());
			a_DiscountVal = parseInt($("#aprvDiscount_RSM"+i).val());
		} else if(userType == 'NSM') {
			r_DiscountVal = parseInt($("#reqDiscount_NSM"+i).val());
			a_DiscountVal = parseInt($("#aprvDiscount_NSM"+i).val());
		} else if(userType == 'MH') {
			a_DiscountVal = parseInt($("#aprvDiscount_MH"+i).val());
		}
		
		if(r_DiscountVal == null || r_DiscountVal =='' || Number.isNaN(r_DiscountVal)) { r_DiscountVal = 0; }
		if(r_DiscountVal > 0) {
			r_pricePerUnit = ((lpPerUnitVal + (lpPerUnitVal * totalAddOnPercentVal/100)) * (1 - r_DiscountVal/100)) + totalCashExtraVal;
			r_pricePerUnit = Math.round(r_pricePerUnit);
			r_totalPrice = quantityVal * r_pricePerUnit;
			r_CummPrice = r_CummPrice + r_totalPrice;
		}
		
		if(a_DiscountVal == null || a_DiscountVal =='' || Number.isNaN(a_DiscountVal)) { a_DiscountVal = 0; }
		if(a_DiscountVal > 0) {
			a_pricePerUnit = ((lpPerUnitVal + (lpPerUnitVal * totalAddOnPercentVal/100)) * (1 - a_DiscountVal/100)) + totalCashExtraVal;
			a_pricePerUnit = Math.round(a_pricePerUnit);
			a_totalPrice = quantityVal * a_pricePerUnit;
			a_CummPrice = a_CummPrice + a_totalPrice;
		}
		
		if(userType == 'RSM') {
			if(r_DiscountVal > 0) {
				$("#reqPricePerUnit_RSM"+i).val(formatNumber(r_pricePerUnit));
				$("#reqTotalPrice_RSM"+i).val(formatNumber(r_totalPrice));
			}
			if(a_DiscountVal > 0) {
				$("#aprvPricePerUnit_RSM"+i).val(formatNumber(a_pricePerUnit));
				$("#aprvTotalPrice_RSM"+i).val(formatNumber(a_totalPrice));
			}
		} else if(userType == 'NSM') {
			if(r_DiscountVal > 0) {
				$("#reqPricePerUnit_NSM"+i).val(formatNumber(r_pricePerUnit));
				$("#reqTotalPrice_NSM"+i).val(formatNumber(r_totalPrice));
			}
			if(a_DiscountVal > 0) {
				$("#aprvPricePerUnit_NSM"+i).val(formatNumber(a_pricePerUnit));
				$("#aprvTotalPrice_NSM"+i).val(formatNumber(a_totalPrice));
			}
		} else if(userType == 'MH') {
			if(a_DiscountVal > 0) {
				$("#aprvPricePerUnit_MH"+i).val(formatNumber(a_pricePerUnit));
				$("#aprvTotalPrice_MH"+i).val(formatNumber(a_totalPrice));
			}
		}
	}
	if(userType != 'MH') {
		if(r_CummPrice > 0) {
			$("#r_CummPrice").val(formatNumber(r_CummPrice));
		}
	}
	if(a_CummPrice > 0) {
		$("#a_CummPrice").val(formatNumber(a_CummPrice));
	}
	
}

function formatNumber(num) {
	num = num.toString();
	var lastThree = num.substring(num.length-3);
	var otherNumbers = num.substring(0,num.length-3);
    if(otherNumbers != '')
        lastThree = ',' + lastThree;
    var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
    console.log("formatNumber result: " + res);
    return res;
}


function toggleBaseFieldsSection() { 
	var basefieldsSectionId = document.getElementById("basefields_section");
	if (basefieldsSectionId.style.display === "none") {
		document.getElementById("basefields_section").style.display = "block";
	} else {
		document.getElementById("basefields_section").style.display = "none";
	}
}

function toggleElectricalSection() {
	var electricalSectionId = document.getElementById("electricalfields_section");
	if (electricalSectionId.style.display === "none") {
		document.getElementById("electricalfields_section").style.display = "block";
	} else {
		document.getElementById("electricalfields_section").style.display = "none";
	}
}

function toggleMechanicalSection() {
	var mechanicalSectionId = document.getElementById("mechanicalfields_section");
	if (mechanicalSectionId.style.display === "none") {
		document.getElementById("mechanicalfields_section").style.display = "block";
	} else {
		document.getElementById("mechanicalfields_section").style.display = "none";
	}
}

function toggleAccessoriesSection() {
	var accessoriesSectionId = document.getElementById("accessoriesfields_section");
	if (accessoriesSectionId.style.display === "none") {
		document.getElementById("accessoriesfields_section").style.display = "block";
	} else {
		document.getElementById("accessoriesfields_section").style.display = "none";
	}
}

function toggleBearingsSection() {
	var bearingsSectionId = document.getElementById("bearingsfields_section");
	if (bearingsSectionId.style.display === "none") {
		document.getElementById("bearingsfields_section").style.display = "block";
	} else {
		document.getElementById("bearingsfields_section").style.display = "none";
	}
}

function toggleCertificateSparesSection() {
	var certificateSparesId = document.getElementById("certificatefields_section");
	if (certificateSparesId.style.display === "none") {
		document.getElementById("certificatefields_section").style.display = "block";
	} else {
		document.getElementById("certificatefields_section").style.display = "none";
	}
}

/* Design Page : Toggle Sections */
function toggleMTOBaseFieldsSection() {
	var basefieldsSectionId = document.getElementById("basefields_mto_section");
	if (basefieldsSectionId.style.display === "none") {
		document.getElementById("basefields_mto_section").style.display = "block";
	} else {
		document.getElementById("basefields_mto_section").style.display = "none";
	}
}
function toggleMTOElectricalSection() {
	var electricalSectionId = document.getElementById("electricalfields_mto_section");
	if (electricalSectionId.style.display === "none") {
		document.getElementById("electricalfields_mto_section").style.display = "block";
	} else {
		document.getElementById("electricalfields_mto_section").style.display = "none";
	}
}
function toggleMTOMechanicalSection() {
	var mechanicalSectionId = document.getElementById("mechanicalfields_mto_section");
	if (mechanicalSectionId.style.display === "none") {
		document.getElementById("mechanicalfields_mto_section").style.display = "block";
	} else {
		document.getElementById("mechanicalfields_mto_section").style.display = "none";
	}
}
function toggleMTOPerformanceSection() {
	var perfSectionId = document.getElementById("perffields_mto_section");
	if (perfSectionId.style.display === "none") {
		document.getElementById("perffields_mto_section").style.display = "block";
	} else {
		document.getElementById("perffields_mto_section").style.display = "none";
	}
}
function toggleMTOAccessoriesSection() {
	var accessoriesSectionId = document.getElementById("accessoriesfields_mto_section");
	if (accessoriesSectionId.style.display === "none") {
		document.getElementById("accessoriesfields_mto_section").style.display = "block";
	} else {
		document.getElementById("accessoriesfields_mto_section").style.display = "none";
	}
}
function toggleMTOCertificateSparesSection() {
	var certificateSparesId = document.getElementById("certificatefields_mto_section");
	if (certificateSparesId.style.display === "none") {
		document.getElementById("certificatefields_mto_section").style.display = "block";
	} else {
		document.getElementById("certificatefields_mto_section").style.display = "none";
	}
}
/* Design Page : Toggle Sections */


function selectLstItemById(fieldVal, value) {
	var field = document.getElementById(fieldVal);
	if (field != null) {
		for(i=0; i<field.options.length; i++) {
			if(field.options[i].value == value)
				field.options[i].selected=true;
		}
	}
}

function selectLstItemByLabel(fieldVal, value) {
	var field = document.getElementById(fieldVal);
	if (field != null) {
		for(i=0; i<field.options.length; i++) {
			if(field.options[i].text == value)
				field.options[i].selected=true;
		}
	}
}

function displaySupervisionDetails() {
	var supervisionInclusive = document.getElementById('supervisionDetails').value;
	
	if(supervisionInclusive === 'Yes') {
		document.getElementById("supervisionDetailsDivId").style.display = "block";
	} else {
		document.getElementById("supervisionDetailsDivId").style.display = "none";
	}
}

function showTitle(objId) {
	$('.title-message').html($('#'+objId).attr('title')).css('visibility', 'visible');
}

function hideTitle() {
	$('.title-message').html('').css('visibility', 'hidden');
}

function submitToRSM()
{
	var totalRatingIndex = $("#totalRatingIndex").val();
	
	// For Mandatory Checks
	for(i=1; i<=totalRatingIndex; i++) {
		var index = i;
		
		if($('#requestedDiscount'+index).val() == null || $('#requestedDiscount'+index).val() == '' || $('#requestedDiscount'+index).val() == undefined) {
			alert('Please enter appropriate value for Requested Discount(SM).');
			$('#requestedDiscount'+index).focus();
			return false;
		}
		
		var requestedDiscount_SM = parseInt($('#requestedDiscount'+index).val());
		var standardDiscount = parseInt($('#standardDiscountHid'+index).val());
		
		if(requestedDiscount_SM <= standardDiscount) {
			alert('The Requested Discount(SM) should be greater than Standard Discount. Please enter appropriate value.');
			$('#requestedDiscount'+index).focus();
			return false;
		}
	}
	
	document.newenquiryForm.action="newPlaceAnEnquiry.do";
	document.newenquiryForm.invoke.value="submitToRSM";
	document.newenquiryForm.submit();
}

function approveByRSM()
{
	var totalRatingIndex = $("#totalRatingIndex").val();
	var customerIdVal = $("#customerId").val();
	
	// Validation : Mandatory and Approval Limit Check.
	for(i=1; i<=totalRatingIndex; i++) {
		var index = i;
		
		if($('#aprvDiscount_RSM'+index).val() == null || $('#aprvDiscount_RSM'+index).val() == '' || $('#aprvDiscount_RSM'+index).val() == undefined) {
			alert('Please enter appropriate value for Approved Discount(RSM).');
			$('#aprvDiscount_RSM'+index).focus();
			return false;
		}
		
		var aprvDiscountRSMVal = $('#aprvDiscount_RSM'+index).val();
		var prodLineVal = $("#prodLine"+index).val();
		var frameTypeVal = $("#frameType"+index).val();
		var aprvLimitRSM = '';
		var aprvDiscountNSMVal = $('#approvedDiscount_NSM_Hid'+index).val();
		
		if(aprvDiscountNSMVal != null && (aprvDiscountNSMVal != '' || aprvDiscountNSMVal != '0') ) {
			if (parseFloat(parseFloat(aprvDiscountRSMVal).toFixed(2)) > parseFloat(parseFloat(aprvDiscountNSMVal).toFixed(2))) {
				alert('NSM Approved value is ' + aprvDiscountNSMVal + '%. Please enter appropriate value below NSM Approved value.');
				$('#aprvDiscount_RSM'+index).focus();
				return false;
			}
		} else {
			var data = {
				customerIdVal : customerIdVal,
				prodLineVal : prodLineVal,
				frameTypeVal : frameTypeVal
			}
				
			$.ajax({
					type : "POST",
					url : "/quotationlttool/newPlaceAnEnquiry.do?invoke=fetchRSMApprovalLimitAjax&reqparams="+JSON.stringify(data),
					dataType : "json",
					success : function(response) {
						$.each(response, function(index) {
							aprvLimitRSM = response[index];
						});
					},
					error : function(xhr, ajaxOptions, thrownError) {
						return false;
					}
			});
			
			if(aprvLimitRSM != null && aprvLimitRSM != '') {
				if (parseFloat(parseFloat(aprvDiscountRSMVal).toFixed(2)) > parseFloat(parseFloat(aprvLimitRSM).toFixed(2))) {
					alert('The RSM Approval Limit is ' + aprvLimitRSM + '%. Please enter appropriate value below Approval Limit.');
					$('#aprvDiscount_RSM'+index).focus();
					return false;
				}
			}
		}
	}
	
	document.newenquiryForm.action="newPlaceAnEnquiry.do";
	document.newenquiryForm.invoke.value="approveByRSM";
	document.newenquiryForm.submit();
}

function submitToNSM()
{
	var totalRatingIndex = $("#totalRatingIndex").val();
	
	// For Mandatory Checks
	for(i=1; i<=totalRatingIndex; i++) {
		var index = i;
		
		if($('#reqDiscount_RSM'+index).val() == null || $('#reqDiscount_RSM'+index).val() == '' || $('#reqDiscount_RSM'+index).val() == undefined) {
			alert('Please enter appropriate value for Requested Discount(RSM).');
			$('#reqDiscount_RSM'+index).focus();
			return false;
		}
		
		var requestedDiscount_RSM = parseInt($('#reqDiscount_RSM'+index).val());
		var requestedDiscount_SM = parseInt($('#reqDiscount_SM_Hid'+index).val());
		
		if(requestedDiscount_RSM <= requestedDiscount_SM) {
			alert('The Requested Discount(RSM) should be greater than Requested Discount(SM). Please enter appropriate value.');
			$('#reqDiscount_RSM'+index).focus();
			return false;
		}
	}
	
	document.newenquiryForm.action="newPlaceAnEnquiry.do";
	document.newenquiryForm.invoke.value="submitToNSM";
	document.newenquiryForm.submit();
}

function approveByNSM()
{
	var totalRatingIndex = $("#totalRatingIndex").val();
	var customerIdVal = $("#customerId").val();
	var isNSPCheckPassed = true;
	var nspPercentVal = '';
	var nspValues = [];
	var idxNSPArr = 0;
	
	// For Mandatory Checks
	for(i=1; i<=totalRatingIndex; i++) {
		var index = i;
		
		if($('#aprvDiscount_NSM'+index).val() == null || $('#aprvDiscount_NSM'+index).val() == '' || $('#aprvDiscount_NSM'+index).val() == undefined) {
			alert('Please enter appropriate value for Approved Discount(NSM).');
			$('#aprvDiscount_NSM'+index).focus();
			return false;
		}
		
		var aprvDiscountNSMVal = $('#aprvDiscount_NSM'+index).val();
		var prodLineVal = $("#prodLine"+index).val();
		var frameTypeVal = $("#frameType"+index).val();
		var aprvLimitNSM = '';
		var aprvDiscountMHVal = $('#approvedDiscount_MH_Hid'+index).val();
		
		if(aprvDiscountMHVal != null && (aprvDiscountMHVal != '' || aprvDiscountMHVal != '0') ) {
			if (parseFloat(parseFloat(aprvDiscountNSMVal).toFixed(2)) > parseFloat(parseFloat(aprvDiscountMHVal).toFixed(2))) {
				alert('MH Approved value is ' + aprvDiscountMHVal + '%. Please enter appropriate value below MH Approved value.');
				$('#aprvDiscount_NSM'+index).focus();
				return false;
			}
		} else {
			var data = {
					customerIdVal : customerIdVal,
					prodLineVal : prodLineVal,
					frameTypeVal : frameTypeVal
			}
				
			$.ajax({
				type : "POST",
				url : "/quotationlttool/newPlaceAnEnquiry.do?invoke=fetchNSMApprovalLimitAjax&reqparams="+JSON.stringify(data),
				dataType : "json",
				success : function(response) {
					$.each(response, function(index) {
						aprvLimitNSM = response[index];
					});
				},
				error : function(xhr, ajaxOptions, thrownError) {
					return false;
				}
			});
			
			if(aprvLimitNSM != null && aprvLimitNSM != '') {
				if (parseFloat(parseFloat(aprvDiscountNSMVal).toFixed(2)) > parseFloat(parseFloat(aprvLimitNSM).toFixed(2))) {
					alert('The NSM Approval Limit is ' + aprvLimitNSM + '%. Please enter appropriate value below Approval Limit.');
					$('#aprvDiscount_NSM'+index).focus();
					return false;
				}
			}
		}
	}
	
	document.newenquiryForm.action="newPlaceAnEnquiry.do";
	document.newenquiryForm.invoke.value="approveByNSM";
	document.newenquiryForm.submit();
}

function submitToMH()
{
	var totalRatingIndex = $("#totalRatingIndex").val();
	
	// For Mandatory Checks
	for(i=1; i<=totalRatingIndex; i++) {
		var index = i;
		
		if($('#reqDiscount_NSM'+index).val() == null || $('#reqDiscount_NSM'+index).val() == '' || $('#reqDiscount_NSM'+index).val() == undefined) {
			alert('Please enter appropriate value for Requested Discount(NSM).');
			$('#reqDiscount_NSM'+index).focus();
			return false;
		}
		
		var requestedDiscount_NSM = parseInt($('#reqDiscount_NSM'+index).val());
		var requestedDiscount_RSM = parseInt($('#reqDiscount_RSM_Hid'+index).val());
		
		if(requestedDiscount_NSM <= requestedDiscount_RSM) {
			alert('The Requested Discount(NSM) should be greater than Requested Discount(RSM). Please enter appropriate value.');
			$('#reqDiscount_NSM'+index).focus();
			return false;
		}
	}
	
	document.newenquiryForm.action="newPlaceAnEnquiry.do";
	document.newenquiryForm.invoke.value="submitToMH";
	document.newenquiryForm.submit();
}

function approveByMH()
{
	var totalRatingIndex = $("#totalRatingIndex").val();
	
	// For Mandatory Checks
	for(i=1; i<=totalRatingIndex; i++) {
		var index = i;
		
		if($('#aprvDiscount_MH'+index).val() == null || $('#aprvDiscount_MH'+index).val() == '' || $('#aprvDiscount_MH'+index).val() == undefined) {
			alert('Please enter appropriate value for Approved Discount(MH).');
			$('#aprvDiscount_MH'+index).focus();
			return false;
		}
		
	}
	
	document.newenquiryForm.action="newPlaceAnEnquiry.do";
	document.newenquiryForm.invoke.value="approveByMH";
	document.newenquiryForm.submit();
}

function reviseEnquiry(userType)
{
	if(userType == 'SM' && ($('#smNote').val() == null || Trim($('#smNote').val()) == '') ) {
		alert('Please enter appropriate comments before submitting the Enquiry for Review.');
		$('#smNote').focus();
		return false;
	}
	if(userType == 'RSM' && ($('#rsmNote').val() == null || Trim($('#rsmNote').val()) == '') ) {
		alert('Please enter appropriate comments before submitting the Enquiry for Review.');
		$('#rsmNote').focus();
		return false;
	}
	if(userType == 'NSM' && ($('#nsmNote').val() == null || Trim($('#nsmNote').val()) == '') ) {
		alert('Please enter appropriate comments before submitting the Enquiry for Review.');
		$('#nsmNote').focus();
		return false;
	}
	if(userType == 'MH' && ($('#mhNote').val() == null || Trim($('#mhNote').val()) == '') ) {
		alert('Please enter appropriate comments before submitting the Enquiry for Review.');
		$('#mhNote').focus();
		return false;
	}
	document.newenquiryForm.action="newPlaceAnEnquiry.do";
	document.newenquiryForm.invoke.value="processReviseEnquiry";
	document.newenquiryForm.reviseUserType.value=userType;
	document.newenquiryForm.submit();
}

function emailQuoteToCustomerNewEnq()
{
	var totalRatingIndex = $("#totalRatingIndex").val();
	
	// For Mandatory Checks
	for(i=1; i<=totalRatingIndex; i++) {
		var index = i;
		
		if($('#quotedDiscount'+index).val() == null || $('#quotedDiscount'+index).val() == '' || $('#quotedDiscount'+index).val() == undefined) {
			alert(' Please enter appropriate value for Quoted Discount.');
			$('#quotedDiscount'+index).focus();
			return false;
		}
		
		if($('#approvedDiscountRSMHid'+index).val() != null || $('#approvedDiscountRSMHid'+index).val() != '') {
			var approvedDiscount_RSM = parseFloat($('#approvedDiscountRSMHid'+index).val());
			var quotedDiscount = parseFloat($('#quotedDiscount'+index).val());
			
			if(quotedDiscount == 0) {
				alert('Quoted Discount should be greater than zero. Please enter appropriate value.');
				$('#quotedDiscount'+index).focus();
				return false;
			}
			if(quotedDiscount > approvedDiscount_RSM) {
				alert('The Quoted Discount should be less than or equal to Approved Discount (RSM). Please enter appropriate value.');
				$('#quotedDiscount'+index).focus();
				return false;
			}
		}
	}
	
	document.newenquiryForm.action="newPlaceAnEnquiry.do";
	document.newenquiryForm.invoke.value="emailQuoteToCustomerNewEnquiry";
	document.newenquiryForm.submit();
}

function showRSMApproveLimit(index) {
	var customerIdVal = $("#customerId").val();
	var prodLineVal = $("#prodLine"+index).val();
	var frameTypeVal = $("#frameType"+index).val();
	var aprvLimitRSM = '';
	
	var data = {
		customerIdVal : customerIdVal,
		prodLineVal : prodLineVal,
		frameTypeVal : frameTypeVal
	}
		
	$.ajax({
		type : "POST",
		url : "/quotationlttool/newPlaceAnEnquiry.do?invoke=fetchRSMApprovalLimitAjax&reqparams="+JSON.stringify(data),
		dataType : "json",
		success : function(response) {
			$.each(response, function(index) {
				aprvLimitRSM = response[index];
			});
			//var approvalLimitText = "RSM Approval Limit : "+aprvLimitRSM+"%";
			$('#aprvDiscount_RSM'+index).attr('title', "RSM Approval Limit : "+aprvLimitRSM+"%");
		},
		error : function(xhr, ajaxOptions, thrownError) {
			return false;
		}
	});
	
}
function hideRSMApproveLimit(index) {
	var hasTooltip = $('#hasTooltip').val();
	if (hasTooltip == 'true') {
		$('#aprvDiscount_RSM'+index).tooltip("disable");
		$('#hasTooltip').val('false');
	}
}

function showNSMApproveLimit(index) {
	var customerIdVal = $("#customerId").val();
	var prodLineVal = $("#prodLine"+index).val();
	var frameTypeVal = $("#frameType"+index).val();
	var aprvLimitNSM = '';
	
	var data = {
		customerIdVal : customerIdVal,
		prodLineVal : prodLineVal,
		frameTypeVal : frameTypeVal
	}
	
	$.ajax({
		type : "POST",
		url : "/quotationlttool/newPlaceAnEnquiry.do?invoke=fetchNSMApprovalLimitAjax&reqparams="+JSON.stringify(data),
		dataType : "json",
		success : function(response) {
			$.each(response, function(index) {
				aprvLimitNSM = response[index];
			});
		},
		error : function(xhr, ajaxOptions, thrownError) {
			return false;
		}
	});
	
	$('#aprvDiscount_NSM'+index).attr('title', "NSM Approval Limit : "+aprvLimitNSM+"%");
}

function loadCustomerType() 
{
	var customerIdVal = $("#customerId").val();
	var customerNameVal = $("#customerName").val();
	if(customerNameVal == null || customerIdVal == null || customerNameVal == '' || customerIdVal == '') {
		customerIdVal = '0';
	}
	var data = {
		customerIdVal : customerIdVal
	}
	
	$.ajax({
		type : "GET",
		url : "/quotationlttool/newPlaceAnEnquiry.do?invoke=fetchCustomerTypeByAjax&reqparams="+JSON.stringify(data),
		dataType : "json",
		cache : false,
		success : function(response) {
			console.log('success', response);
			$.each(response, function(index) {
				$('#customerType').val(response[index]);
			});
			var customerTypeVal = $('#customerType').val();
			if(customerTypeVal != null && customerTypeVal == '1') {
				// Disable Retention Payment 1 Fields.
				$("#pt4PercentAmtNetorderValue").attr("style", "pointer-events:none; font-weight:normal !important; width:145px !important; BACKGROUND-COLOR: #e2e2e2;");
				$("#pt4PercentAmtNetorderValue").val('0');
				$("#pt4PaymentDays").attr("style", "pointer-events:none; font-weight:normal !important; width:145px !important; BACKGROUND-COLOR: #e2e2e2;");
				$("#pt4PaymentDays").val('0');
				$("#pt4PayableTerms").attr("style", "pointer-events:none; font-weight:normal !important; width:380px !important; BACKGROUND-COLOR: #e2e2e2;");
				$("#pt4PayableTerms").val('0');
				$("#pt4Instrument").attr("style", "pointer-events:none; font-weight:normal !important; width:150px !important; BACKGROUND-COLOR: #e2e2e2;");
				$("#pt4Instrument").val('0');
				// Disable Retention Payment 2 Fields.
				$("#pt5PercentAmtNetorderValue").attr("style", "pointer-events:none; font-weight:normal !important; width:145px !important; BACKGROUND-COLOR: #e2e2e2;");
				$("#pt5PercentAmtNetorderValue").val('0');
				$("#pt5PaymentDays").attr("style", "pointer-events:none; font-weight:normal !important; width:145px !important; BACKGROUND-COLOR: #e2e2e2;");
				$("#pt5PaymentDays").val('0');
				$("#pt5PayableTerms").attr("style", "pointer-events:none; font-weight:normal !important; width:380px !important; BACKGROUND-COLOR: #e2e2e2;");
				$("#pt5PayableTerms").val('0');
				$("#pt5Instrument").attr("style", "pointer-events:none; font-weight:normal !important; width:150px !important; BACKGROUND-COLOR: #e2e2e2;");
				$("#pt5Instrument").val('0');
				// Disable Liquidated Damages Field.
				$("#liquidatedDamagesDueToDeliveryDelay").attr("style", "pointer-events:none; font-weight:normal !important; width:390px !important; BACKGROUND-COLOR: #e2e2e2;");
				$("#liquidatedDamagesDueToDeliveryDelay").val('1');
				
				// Set Dealer Default values.
				$("#pt3PercentAmtNetorderValue").val('20');
				$("#pt3PaymentDays").val('5');
				$("#pt3PayableTerms").val('1');
				$("#pt3Instrument").val('2');
			} else {
				// Enable Retention Payment 1 Fields.
				$("#pt4PercentAmtNetorderValue").attr("style", "width:145px !important;");
				$("#pt4PaymentDays").attr("style", "width:145px !important;");
				$("#pt4PayableTerms").attr("style", "width:380px !important;");
				$("#pt4Instrument").attr("style", "width:150px !important;");
				// Enable Retention Payment 2 Fields.
				$("#pt5PercentAmtNetorderValue").attr("style", "width:145px !important;");
				$("#pt5PaymentDays").attr("style", "width:145px !important;");
				$("#pt5PayableTerms").attr("style", "width:380px !important;");
				$("#pt5Instrument").attr("style", "width:150px !important;");
				// Enable Liquidated Damages Field.
				$("#liquidatedDamagesDueToDeliveryDelay").attr("style", "width:390px !important;");
				
			}
		},
		error : function(xhr, ajaxOptions, thrownError) {
			$('#loadingDiv').hide();
			return false;
		}
	});
}


function loadLocation()
{
	var customerIdVal = $("#customerId").val();
	
	var data = {
		customerIdVal : customerIdVal
	}
	$('#location').empty();
	var option = new Option(0,0);
	$(option).html('Select');
	$('#location').append(option);
	
	if(customerIdVal != null && customerIdVal != '') {
		$.ajax({
			type : "GET",
			url : "/quotationlttool/newPlaceAnEnquiry.do?invoke=loadCustomerLocationByAjax&reqparams="+JSON.stringify(data),
			dataType : "json",
			cache : false,
			success : function(response) {
				console.log('success',response);
				$.each(response, function(index) {
					var option = new Option(index,index);
					$(option).html(response[index]);
					$('#location').append(option);
				});
			},
			error : function(xhr, ajaxOptions, thrownError) {
				return false;
			}
		});
	}
}

function loadLocationWithValue(locationId) {
	var customerIdVal = $("#customerId").val();
	
	var data = {
		customerIdVal : customerIdVal
	}
	$('#location').empty();
	var option = new Option(0,0);
	$(option).html('Select');
	$('#location').append(option);
	
	if(customerIdVal != null && customerIdVal != '') {
		$.ajax({
			type : "GET",
			url : "/quotationlttool/newPlaceAnEnquiry.do?invoke=loadCustomerLocationByAjax&reqparams="+JSON.stringify(data),
			dataType : "json",
			cache : false,
			success : function(response) {
				console.log('success',response);
				$.each(response, function(index) {
					var option = new Option(index,index);
					$(option).html(response[index]);
					$('#location').append(option);
				});
				if(locationId != '') {
					var field = document.getElementById('location');
					if (field != null) {
						for(i=0; i<field.options.length; i++) {
							if(field.options[i].value == locationId)
								field.options[i].selected=true;
						}
					}
				}
			},
			error : function(xhr, ajaxOptions, thrownError) {
				return false;
			}
		});
	}
	
	var customerTypeVal = $('#customerType').val();
	if(customerTypeVal != null && customerTypeVal == '1') {
		// Disable Retention Payment 1 Fields.
		$("#pt4PercentAmtNetorderValue").attr("style", "pointer-events:none; font-weight:normal !important; width:145px !important; BACKGROUND-COLOR: #e2e2e2;");
		$("#pt4PercentAmtNetorderValue").val('0');
		$("#pt4PaymentDays").attr("style", "pointer-events:none; font-weight:normal !important; width:145px !important; BACKGROUND-COLOR: #e2e2e2;");
		$("#pt4PaymentDays").val('0');
		$("#pt4PayableTerms").attr("style", "pointer-events:none; font-weight:normal !important; width:380px !important; BACKGROUND-COLOR: #e2e2e2;");
		$("#pt4PayableTerms").val('0');
		$("#pt4Instrument").attr("style", "pointer-events:none; font-weight:normal !important; width:150px !important; BACKGROUND-COLOR: #e2e2e2;");
		$("#pt4Instrument").val('0');
		// Disable Retention Payment 2 Fields.
		$("#pt5PercentAmtNetorderValue").attr("style", "pointer-events:none; font-weight:normal !important; width:145px !important; BACKGROUND-COLOR: #e2e2e2;");
		$("#pt5PercentAmtNetorderValue").val('0');
		$("#pt5PaymentDays").attr("style", "pointer-events:none; font-weight:normal !important; width:145px !important; BACKGROUND-COLOR: #e2e2e2;");
		$("#pt5PaymentDays").val('0');
		$("#pt5PayableTerms").attr("style", "pointer-events:none; font-weight:normal !important; width:380px !important; BACKGROUND-COLOR: #e2e2e2;");
		$("#pt5PayableTerms").val('0');
		$("#pt5Instrument").attr("style", "pointer-events:none; font-weight:normal !important; width:150px !important; BACKGROUND-COLOR: #e2e2e2;");
		$("#pt5Instrument").val('0');
		// Disable Liquidated Damages Field.
		$("#liquidatedDamagesDueToDeliveryDelay").attr("style", "pointer-events:none; font-weight:normal !important; width:390px !important; BACKGROUND-COLOR: #e2e2e2;");
		$("#liquidatedDamagesDueToDeliveryDelay").val('1');
		
		// Set Dealer Default values.
		$("#pt3PercentAmtNetorderValue").val('20');
		$("#pt3PaymentDays").val('5');
		$("#pt3PayableTerms").val('1');
		$("#pt3Instrument").val('2');
	} else {
		// Enable Retention Payment 1 Fields.
		$("#pt4PercentAmtNetorderValue").attr("style", "width:145px !important;");
		$("#pt4PaymentDays").attr("style", "width:145px !important;");
		$("#pt4PayableTerms").attr("style", "width:380px !important;");
		$("#pt4Instrument").attr("style", "width:150px !important;");
		// Enable Retention Payment 2 Fields.
		$("#pt5PercentAmtNetorderValue").attr("style", "width:145px !important;");
		$("#pt5PaymentDays").attr("style", "width:145px !important;");
		$("#pt5PayableTerms").attr("style", "width:380px !important;");
		$("#pt5Instrument").attr("style", "width:150px !important;");
		// Enable Liquidated Damages Field.
		$("#liquidatedDamagesDueToDeliveryDelay").attr("style", "width:390px !important;");
		
	}
}


function loadConcernedPerson()
{
	var customerIdVal = $("#customerId").val();
	
	var data = {
			customerIdVal : customerIdVal
	}
	$('#concernedPerson').empty();
	var option = new Option(0,0);
	$(option).html('Select');
	$('#concernedPerson').append(option);
	
	if(customerIdVal != null && customerIdVal != '') {
		$.ajax({
			type : "POST",
			url : "/quotationlttool/newPlaceAnEnquiry.do?invoke=loadCustomerContactByAjax&reqparams="+JSON.stringify(data),
			dataType : "json",
			cache : false,
			success : function(response) {
				console.log('success', response);
				$.each(response, function(index) {
					var option = new Option(index,index);
					$(option).html(response[index]);
					$('#concernedPerson').append(option);
				});
			},
			error : function(xhr, ajaxOptions, thrownError) {
				return false;
			}
		});
	}
}

function loadCustomerIndustry()
{
	var customerIdVal = $("#customerId").val();
	
	$('#customerIndustry').val('');
	
	var data = {
			customerIdVal : customerIdVal
	}
	if(customerIdVal != null && customerIdVal != '') {
		$.ajax({
			type : "POST",
			url : "/quotationlttool/newPlaceAnEnquiry.do?invoke=loadCustomerIndustryByAjax&reqparams="+JSON.stringify(data),
			dataType : "json",
			cache : false,
			success : function(response) {
				$.each(response, function(index) {
					$('#customerIndustry').val(response[index]);
				});
			},
			error : function(xhr, ajaxOptions, thrownError) {
				return false;
			}
		});
	}
}

function validatePaymentDays(field)
{
	var customerIdVal = $("#customerId").val();
	var customerNameVal = $("#customerName").val();
	if(customerNameVal == null || customerIdVal == null || customerNameVal == '' || customerIdVal == '') {
		customerIdVal = '0';
	}
	var data = {
		customerIdVal : customerIdVal
	}
	
	$.ajax({
		type : "GET",
		url : "/quotationlttool/newPlaceAnEnquiry.do?invoke=fetchCustomerTypeByAjax&reqparams="+JSON.stringify(data),
		dataType : "json",
		cache : false,
		success : function(response) {
			console.log('success', response);
			$.each(response, function(index) {
				$('#customerType').val(response[index]);
			});
			var customerTypeVal = $('#customerType').val();
			if(customerTypeVal != null && customerTypeVal == '1') {
				// Disable Retention Payment 1 Fields.
				$("#pt4PercentAmtNetorderValue").attr("style", "pointer-events:none; font-weight:normal !important; width:145px !important; BACKGROUND-COLOR: #e2e2e2;");
				$("#pt4PercentAmtNetorderValue").val('0');
				$("#pt4PaymentDays").attr("style", "pointer-events:none; font-weight:normal !important; width:145px !important; BACKGROUND-COLOR: #e2e2e2;");
				$("#pt4PaymentDays").val('0');
				$("#pt4PayableTerms").attr("style", "pointer-events:none; font-weight:normal !important; width:380px !important; BACKGROUND-COLOR: #e2e2e2;");
				$("#pt4PayableTerms").val('0');
				$("#pt4Instrument").attr("style", "pointer-events:none; font-weight:normal !important; width:150px !important; BACKGROUND-COLOR: #e2e2e2;");
				$("#pt4Instrument").val('0');
				// Disable Retention Payment 2 Fields.
				$("#pt5PercentAmtNetorderValue").attr("style", "pointer-events:none; font-weight:normal !important; width:145px !important; BACKGROUND-COLOR: #e2e2e2;");
				$("#pt5PercentAmtNetorderValue").val('0');
				$("#pt5PaymentDays").attr("style", "pointer-events:none; font-weight:normal !important; width:145px !important; BACKGROUND-COLOR: #e2e2e2;");
				$("#pt5PaymentDays").val('0');
				$("#pt5PayableTerms").attr("style", "pointer-events:none; font-weight:normal !important; width:380px !important; BACKGROUND-COLOR: #e2e2e2;");
				$("#pt5PayableTerms").val('0');
				$("#pt5Instrument").attr("style", "pointer-events:none; font-weight:normal !important; width:150px !important; BACKGROUND-COLOR: #e2e2e2;");
				$("#pt5Instrument").val('0');
				// Disable Liquidated Damages Field.
				$("#liquidatedDamagesDueToDeliveryDelay").attr("style", "pointer-events:none; font-weight:normal !important; width:390px !important; BACKGROUND-COLOR: #e2e2e2;");
				$("#liquidatedDamagesDueToDeliveryDelay").val('1');
				
				// Set Dealer Default values.
				$("#pt3PercentAmtNetorderValue").val('20');
				$("#pt3PaymentDays").val('5');
				$("#pt3PayableTerms").val('1');
				$("#pt3Instrument").val('2');
				
				var paymentDaysValue = $("#"+field+" option:selected").text().trim();
				if(paymentDaysValue != 'null' && paymentDaysValue != '') {
					if(parseInt(paymentDaysValue) > 60) {
						alert("The Payment Days for Customer Type Dealer cannot be more than 60 Days. \n Please select value less than or equal to 60 days.");
						$('#'+field).val('0');
						$('#'+field).focus();
						return false;
					}
				}
			} else {
				// Enable Retention Payment 1 Fields.
				$("#pt4PercentAmtNetorderValue").attr("style", "width:145px !important;");
				$("#pt4PaymentDays").attr("style", "width:145px !important;");
				$("#pt4PayableTerms").attr("style", "width:380px !important;");
				$("#pt4Instrument").attr("style", "width:150px !important;");
				// Enable Retention Payment 2 Fields.
				$("#pt5PercentAmtNetorderValue").attr("style", "width:145px !important;");
				$("#pt5PaymentDays").attr("style", "width:145px !important;");
				$("#pt5PayableTerms").attr("style", "width:380px !important;");
				$("#pt5Instrument").attr("style", "width:150px !important;");
				// Enable Liquidated Damages Field.
				$("#liquidatedDamagesDueToDeliveryDelay").attr("style", "width:390px !important;");
				
			}
		},
		error : function(xhr, ajaxOptions, thrownError) {
			$('#loadingDiv').hide();
			return false;
		}
	});
	
}

function validateOrderValue(field)
{
	var customerTypeValue = $("#customerType").val();
	
	var pt1Value = $("#pt1PercentAmtNetorderValue option:selected").text().trim();
	var pt2Value = $("#pt2PercentAmtNetorderValue option:selected").text().trim();
	var pt3Value = $("#pt3PercentAmtNetorderValue option:selected").text().trim();
	var pt4Value = '0';
	var pt5Value = '0';
		
	if(customerTypeValue != null && (customerTypeValue == '' || customerTypeValue != '1')) {
		pt4Value = $("#pt4PercentAmtNetorderValue option:selected").text().trim();
		pt5Value = $("#pt5PercentAmtNetorderValue option:selected").text().trim();
	}
	
	var totalPercentValue = 0;
	
	if(pt1Value != 'Select') {
		totalPercentValue = totalPercentValue + parseInt(pt1Value);
	}
	if(pt2Value != 'Select') {
		totalPercentValue = totalPercentValue + parseInt(pt2Value);
	}
	if(pt3Value != 'Select') {
		totalPercentValue = totalPercentValue + parseInt(pt3Value);
	}
	if(pt4Value != 'Select') {
		totalPercentValue = totalPercentValue + parseInt(pt4Value);
	}
	if(pt5Value != 'Select') {
		totalPercentValue = totalPercentValue + parseInt(pt5Value);
	}
	
	if(totalPercentValue > 100) {
		alert(' The overall Sum of %age amount of Nett. order value should not be more than 100%.\n Please change the Payment Terms accordingly.');
		$('#'+field).val('0');
		return false;
	}
	
	return true;
}

/*  ***************************************************************************************************************************************  */
/*  Exexcute this Script for Saved Ratings - This Script Only handles Display Options - Show/Hide, Enable/Disable, Background Color Options  */
/*  **IMP** :: Do not Set Default values - As this will overide existing values.                                                             */
function processFieldDisplayOptions(index) {
	var prodLineValue = $('#productLine'+index).find("option:selected").text().trim();
	var frameValue = $('#frame'+index).find("option:selected").text().trim();
	
	// Slip Ring
	if(prodLineValue == 'KS' || prodLineValue == 'KS-CMR' ) {		
		$("#rv"+index).removeClass("readonlymini");
		$("#rv"+index).addClass("normal");
		$("#rv"+index).prop("readonly", false);
		$("#ra"+index).removeClass("readonlymini");
		$("#ra"+index).addClass("normal");
		$("#ra"+index).prop("readonly", false);
		// Disable Flying Lead
		$("#flyingLeadWithoutTB"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#flyingLeadWithoutTB"+index).val('1');
		$("#flyingLeadWithTB"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#flyingLeadWithTB"+index).val('1');
		
	} else {
		$("#rv"+index).val('');
		$("#rv"+index).removeClass("normal");
		$("#rv"+index).addClass("readonlymini");
		$("#rv"+index).prop("readonly", true);
		$("#ra"+index).val('');
		$("#ra"+index).removeClass("normal");
		$("#ra"+index).addClass("readonlymini");
		$("#ra"+index).prop("readonly", true);
		// Enable Flying Lead
		$("#flyingLeadWithoutTB"+index).attr("style", "width:100px;");
		$("#flyingLeadWithTB"+index).attr("style", "width:100px;");
	}

	// Method of Starting - Enabling/Disabling Fields.
	var startingMethodVal = $('#methodOfStarting'+index).find("option:selected").text().trim();
	if(!startingMethodVal.includes("DOL")) {
		$("#startingCurrOnDOLStarting"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#startingCurrOnDOLStarting"+index).val('0');
	} else {
		$("#startingCurrOnDOLStarting"+index).attr("style", "width:100px;");
	}
	if(!startingMethodVal.includes("VFD")) {
		$("#vfdType"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#vfdType"+index).val('0');
		$("#vfdSpeedRangeMin"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#vfdSpeedRangeMin"+index).val('0');
		$("#vfdSpeedRangeMax"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#vfdSpeedRangeMax"+index).val('0');
	} else {
		$("#vfdType"+index).attr("style", "width:100px;");
		$("#vfdSpeedRangeMin"+index).attr("style", "width:100px;");
		$("#vfdSpeedRangeMax"+index).attr("style", "width:100px;");
		// Set Winding Wire = "Inverter Grade"
		//$("#windingWire"+index).val('2');
	}
	
	// Hazard Area - Enabling/Disabling Fields.
	var hazardAreaTypeValue = $('#hazardAreaType'+index).find("option:selected").text().trim();
	if(hazardAreaTypeValue == 'Safe Area') {
		$("#hazardArea"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#hazardArea"+index).val('1');
		$("#gasGroup"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#gasGroup"+index).val('1');
		$("#hazardZone"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#hazardZone"+index).val('1');
		$("#dustGroup"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#dustGroup"+index).val('1');
		$("#tempClass"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#tempClass"+index).val('1');
		// Enable Flying Lead
		$("#flyingLeadWithoutTB"+index).attr("style", "width:100px;");
		$("#flyingLeadWithTB"+index).attr("style", "width:100px;");
	} else {
		$("#hazardArea"+index).attr("style", "width:100px;");
		$("#gasGroup"+index).attr("style", "width:100px;");
		$("#hazardZone"+index).attr("style", "width:100px;");
		$("#dustGroup"+index).attr("style", "width:100px;");
		$("#tempClass"+index).attr("style", "width:100px;");
		// Disable Flying Lead
		$("#flyingLeadWithoutTB"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#flyingLeadWithoutTB"+index).val('1');
		$("#flyingLeadWithTB"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#flyingLeadWithTB"+index).val('1');
	}
	
	// Gas Group - Enabling / DIsabling Fields.
	var gasGroupValue = $('#gasGroup'+index).find("option:selected").text().trim();
	if(gasGroupValue == 'I') {
		$("#cableSealingBox"+index).attr("style", "width:100px;");
		$("#cableSealingBox"+index).val('yes');
		$("#ulce"+index).val('2');
	} else {
		$("#cableSealingBox"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#cableSealingBox"+index).val('0');
		$("#ulce"+index).val('0');
	}
	
	// Duty - Enabling/Disabling Fields.
	var dutyValue = $('#duty'+index).find("option:selected").text().trim();
	if(dutyValue == 'S1' || dutyValue == 'S2'){
		$("#cdf"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#starts"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#cdf"+index).val('0');
		$("#starts"+index).val('0');
	} else {
		$("#cdf"+index).attr("style", "width:100px;");
		$("#starts"+index).attr("style", "width:100px;");
	}
	
	// Cable Size - Enabling/Disabling Fields.
	var cableSizeVal = $('#cableSize'+index).val();
	if(cableSizeVal == '2') {
		$("#noOfRuns"+index).attr("style", "width:100px;");
		$("#noOfCores"+index).attr("style", "width:100px;");
		$("#crossSectionArea"+index).attr("style", "width:100px;");
		$("#conductorMaterial"+index).attr("style", "width:100px;");
		$("#cableDiameter"+index).removeClass("readonlymini");
		$("#cableDiameter"+index).addClass("normal");
		$("#cableDiameter"+index).prop("readonly", false);
	} else {
		$("#noOfRuns"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#noOfRuns"+index).val('0');
		$("#noOfCores"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#noOfCores"+index).val('0');
		$("#crossSectionArea"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#crossSectionArea"+index).val('0');
		$("#conductorMaterial"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#conductorMaterial"+index).val('0');
		$("#cableDiameter"+index).val('');
		$("#cableDiameter"+index).removeClass("normal");
		$("#cableDiameter"+index).addClass("readonlymini");
		$("#cableDiameter"+index).prop("readonly", true);
	}
	
	// Flameproof.
	if(prodLineValue == 'KF-IE1' || prodLineValue == 'KF-IE2') {
		$("#rtd"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#rtd"+index).val('0');
		$("#btd"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#btd"+index).val('0');
		$("#auxTerminalBox"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#auxTerminalBox"+index).val('0');
		$("#spreaderBox"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#spreaderBox"+index).val('1');
		
		if(parseInt(frameValue) == 80 || parseInt(frameValue) == 90 || parseInt(frameValue) == 100) {
			$("#spaceHeater"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
			$("#spaceHeater"+index).val('1');
		}
	}
	
	// Non-Std. Paint Shade.
	var paintShadeVal = $('#paintShade'+index).find("option:selected").text().trim();
	if(paintShadeVal.includes("Non-std")){
		$("#paintShadeVal"+index).removeClass("readonlymini");
		$("#paintShadeVal"+index).addClass("normal");
		$("#paintShadeVal"+index).prop("readonly", false);
	} else {
		$("#paintShadeVal"+index).val('');
		$("#paintShadeVal"+index).removeClass("normal");
		$("#paintShadeVal"+index).addClass("readonlymini");
		$("#paintShadeVal"+index).prop("readonly", true);
	}
	
	// Replacement Motor.
	var replaceMotorValue = $('#isReplacementMotor'+index).val();
	if(replaceMotorValue == 'Yes') {
		$("#earlierSuppliedMotorSerialNo"+index).removeClass("readonlymini");
		$("#earlierSuppliedMotorSerialNo"+index).addClass("normal");
		$("#earlierSuppliedMotorSerialNo"+index).prop("readonly", false);
	} else {
		$("#earlierSuppliedMotorSerialNo"+index).val('');
		$("#earlierSuppliedMotorSerialNo"+index).removeClass("normal");
		$("#earlierSuppliedMotorSerialNo"+index).addClass("readonlymini");
		$("#earlierSuppliedMotorSerialNo"+index).prop("readonly", true);
	}
	
}
/*  ***************************************************************************************************************************************  */


/*  ************************************************************************************* */
/*  On Ratings Section : Scripts to verify Dynamic Validations and Setting Default Values */

function processFieldsForProductLine(index)
{
	//alert('Inside Dynamic Validations and Default Values Setting');
	var prodLineValue = $('#productLine'+index).find("option:selected").text().trim();
	var frameValue = $('#frame'+index).find("option:selected").text().trim();
	
	if(prodLineValue == 'TCA' || prodLineValue == 'SCA' ) {
		if(frameValue.includes("315") || frameValue.includes("355")) {
			$("#mfgLocation"+index).val('2');
		} else {
			$("#mfgLocation"+index).val('1');
		}
	} else {
		$("#mfgLocation"+index).val('2');
	}
	
	processMfgLocationOnChange(index);
	
	if(prodLineValue == 'TCA' || prodLineValue == 'SCA' || prodLineValue == 'SE !E2' || prodLineValue == 'KD' ||
		prodLineValue == 'IE1' || prodLineValue == 'KF-IE1' || prodLineValue == 'KF-IE2' || prodLineValue == 'PTSC-I' || 
		prodLineValue == 'SZ250' || prodLineValue == 'SZ300' || prodLineValue == 'SZ400' || prodLineValue == 'PTSC-I-FF' ) {
			$("#methodOfStarting"+index).val('1');
	} else if(prodLineValue == 'VD' || prodLineValue == 'VKD' ) {
			$("#methodOfStarting"+index).val('2');
	}
	
	// Slip Ring
	if(prodLineValue == 'KS' || prodLineValue == 'KS-CMR' ) {
		$("#methodOfStarting"+index).val('6');
		
		$("#rv"+index).removeClass("readonlymini");
		$("#rv"+index).addClass("normal");
		$("#rv"+index).prop("readonly", false);
		$("#ra"+index).removeClass("readonlymini");
		$("#ra"+index).addClass("normal");
		$("#ra"+index).prop("readonly", false);
		
		// Disable Flying Lead
		$("#flyingLeadWithoutTB"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#flyingLeadWithoutTB"+index).val('1');
		$("#flyingLeadWithTB"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#flyingLeadWithTB"+index).val('1');
		
	} else {
		$("#rv"+index).val('');
		$("#rv"+index).removeClass("normal");
		$("#rv"+index).addClass("readonlymini");
		$("#rv"+index).prop("readonly", true);
		$("#ra"+index).val('');
		$("#ra"+index).removeClass("normal");
		$("#ra"+index).addClass("readonlymini");
		$("#ra"+index).prop("readonly", true);
		
		// Enable Flying Lead
		$("#flyingLeadWithoutTB"+index).attr("style", "width:100px;");
		$("#flyingLeadWithTB"+index).attr("style", "width:100px;");
	}
	
	processFieldsForStartingMethod(index);
	
	if(prodLineValue == 'TCA' || prodLineValue == 'SCA' || prodLineValue == 'SE !E2' || prodLineValue == 'KD' ||
		prodLineValue == 'KS' || prodLineValue == 'IE1' || prodLineValue == 'KF-IE1' || prodLineValue == 'KF-IE2' || 
		prodLineValue == 'SZ250' || prodLineValue == 'SZ300' || prodLineValue == 'SZ400' || prodLineValue == 'VD' || prodLineValue == 'VKD' ) {
			$("#methodOfCooling"+index).val('1');
			$("#ip"+index).val('3');
	} else if(prodLineValue == 'PTSC-I' || prodLineValue == 'PTSC-I-FF' ) {
			$("#methodOfCooling"+index).val('5');
			$("#ip"+index).val('1');
	}
	
	processFieldsForHazard(index);
	processFieldsForDuty(index);
	processFieldsForCableSize(index);
	
	if(prodLineValue == 'KF-IE1' || prodLineValue == 'KF-IE2') {
		$("#rtd"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#rtd"+index).val('0');
		$("#btd"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#btd"+index).val('0');
		$("#auxTerminalBox"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#auxTerminalBox"+index).val('0');
		$("#spreaderBox"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#spreaderBox"+index).val('1');
		
		if(parseInt(frameValue) == 80 || parseInt(frameValue) == 90 || parseInt(frameValue) == 100) {
			$("#spaceHeater"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
			$("#spaceHeater"+index).val('1');
		}
	}
}

function processFieldsForFrame(index)
{
	var prodLineValue = $('#productLine'+index).find("option:selected").text().trim();
	var frameValue = $('#frame'+index).find("option:selected").text().trim();
	
	if(prodLineValue == 'TCA' || prodLineValue == 'SCA' ) {
		if(frameValue.includes("315") || frameValue.includes("355")) {
			$("#mfgLocation"+index).val('2');
		} else {
			$("#mfgLocation"+index).val('1');
		}
	} else {
		$("#mfgLocation"+index).val('2');
	}
	
	processMfgLocationOnChange(index);
	
	if(prodLineValue == 'KF-IE1' || prodLineValue == 'KF-IE2') {
		if(parseInt(frameValue) == 80 || parseInt(frameValue) == 90 || parseInt(frameValue) == 100) {
			$("#spaceHeater"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
			$("#spaceHeater"+index).val('1');
		}
	}
	
	if(parseInt(frameValue) < 160) { 
		$("#bearingSystem"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#bearingSystem"+index).val('0');
		$("#bearingNDE"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#bearingNDE"+index).val('0');
	}
}

function processMfgLocationOnChange(index)
{
	var mfgLocValue = $("#mfgLocation"+index).val();
	
	if(mfgLocValue == '2') {
		$("#auxTerminalBox"+index).attr("style", "width:100px;");
		$("#auxTerminalBox"+index).val('1');
	}
}

function processFieldsForHazard(index)
{
	var hazardAreaTypeValue = $('#hazardAreaType'+index).find("option:selected").text().trim();
	
	if(hazardAreaTypeValue == 'Safe Area') {
		$("#hazardArea"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#hazardArea"+index).val('1');
		$("#gasGroup"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#gasGroup"+index).val('1');
		$("#hazardZone"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#hazardZone"+index).val('1');
		$("#dustGroup"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#dustGroup"+index).val('1');
		$("#tempClass"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#tempClass"+index).val('1');
		// Enable Flying Lead
		$("#flyingLeadWithoutTB"+index).attr("style", "width:100px;");
		$("#flyingLeadWithTB"+index).attr("style", "width:100px;");
	} else {
		$("#hazardArea"+index).attr("style", "width:100px;");
		$("#gasGroup"+index).attr("style", "width:100px;");
		$("#hazardZone"+index).attr("style", "width:100px;");
		$("#dustGroup"+index).attr("style", "width:100px;");
		$("#tempClass"+index).attr("style", "width:100px;");
		// Disable Flying Lead
		$("#flyingLeadWithoutTB"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#flyingLeadWithoutTB"+index).val('1');
		$("#flyingLeadWithTB"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#flyingLeadWithTB"+index).val('1');
	}
}

function processGasGroupChange(index)
{
	var gasGroupValue = $('#gasGroup'+index).find("option:selected").text().trim();
	
	if(gasGroupValue == 'I') {
		$("#cableSealingBox"+index).attr("style", "width:100px;");
		$("#cableSealingBox"+index).val('yes');
		$("#ulce"+index).val('2');
	} else {
		$("#cableSealingBox"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#cableSealingBox"+index).val('0');
		$("#ulce"+index).val('0');
	}
}

function processDustGroupChange(index)
{
	var dustGroupValue = $('#dustGroup'+index).find("option:selected").text().trim();
	
	if(dustGroupValue == 'Zone21') {
		$("#ip"+index).attr("style", "width:100px;");
		$("#ip"+index).val('6');
	}
}

function processFieldsForDuty(index)
{
	var dutyValue = $('#duty'+index).find("option:selected").text().trim();
	
	if(dutyValue == 'S1' || dutyValue == 'S2'){
		$("#cdf"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#starts"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#cdf"+index).val('0');
		$("#starts"+index).val('0');
	} else {
		$("#cdf"+index).attr("style", "width:100px;");
		$("#starts"+index).attr("style", "width:100px;");
		/*
		if(dutyValue == 'S3') {
			$("#starts"+index).val('2');
		}
		*/
	}
	return true;
}

function processFieldsForPaintShade(index)
{
	var paintShadeVal = $('#paintShade'+index).find("option:selected").text().trim();
	
	if(paintShadeVal.includes("Non-std")){
		$("#paintShadeVal"+index).removeClass("readonlymini");
		$("#paintShadeVal"+index).addClass("normal");
		$("#paintShadeVal"+index).prop("readonly", false);
	} else {
		$("#paintShadeVal"+index).val('');
		$("#paintShadeVal"+index).removeClass("normal");
		$("#paintShadeVal"+index).addClass("readonlymini");
		$("#paintShadeVal"+index).prop("readonly", true);
	}
	return true;
}

function processFieldsForStartingMethod(index)
{
	var startingMethodVal = $('#methodOfStarting'+index).find("option:selected").text().trim();
	
	if(!startingMethodVal.includes("DOL")) {
		$("#startingCurrOnDOLStarting"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#startingCurrOnDOLStarting"+index).val('0');
	} else {
		$("#startingCurrOnDOLStarting"+index).attr("style", "width:100px;");
	}
	
	if(!startingMethodVal.includes("VFD")) {
		$("#vfdType"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#vfdType"+index).val('0');
		$("#vfdSpeedRangeMin"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#vfdSpeedRangeMin"+index).val('0');
		$("#vfdSpeedRangeMax"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#vfdSpeedRangeMax"+index).val('0');
	} else {
		$("#vfdType"+index).attr("style", "width:100px;");
		$("#vfdSpeedRangeMin"+index).attr("style", "width:100px;");
		$("#vfdSpeedRangeMax"+index).attr("style", "width:100px;");
		
		// Set Winding Wire = "Inverter Grade"
		$("#windingWire"+index).val('2');
	}
}

function processFieldsForReplaceMotor(index)
{
	var replaceMotorValue = $('#isReplacementMotor'+index).val();
	
	if(replaceMotorValue == 'Yes') {
		$("#earlierSuppliedMotorSerialNo"+index).removeClass("readonlymini");
		$("#earlierSuppliedMotorSerialNo"+index).addClass("normal");
		$("#earlierSuppliedMotorSerialNo"+index).prop("readonly", false);
	} else {
		$("#earlierSuppliedMotorSerialNo"+index).val('');
		$("#earlierSuppliedMotorSerialNo"+index).removeClass("normal");
		$("#earlierSuppliedMotorSerialNo"+index).addClass("readonlymini");
		$("#earlierSuppliedMotorSerialNo"+index).prop("readonly", true);
	}
}

function processFieldsForCableSize(index)
{
	var cableSizeVal = $('#cableSize'+index).val();
	
	if(cableSizeVal == '2') {
		$("#noOfRuns"+index).attr("style", "width:100px;");
		$("#noOfCores"+index).attr("style", "width:100px;");
		$("#crossSectionArea"+index).attr("style", "width:100px;");
		$("#conductorMaterial"+index).attr("style", "width:100px;");
		
		$("#cableDiameter"+index).removeClass("readonlymini");
		$("#cableDiameter"+index).addClass("normal");
		$("#cableDiameter"+index).prop("readonly", false);
	} else {
		$("#noOfRuns"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#noOfRuns"+index).val('0');
		$("#noOfCores"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#noOfCores"+index).val('0');
		$("#crossSectionArea"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#crossSectionArea"+index).val('0');
		$("#conductorMaterial"+index).attr("style", "pointer-events:none; font-weight:normal !important; width:100px;BACKGROUND-COLOR: #e2e2e2;");
		$("#conductorMaterial"+index).val('0');
		
		$("#cableDiameter"+index).val('');
		$("#cableDiameter"+index).removeClass("normal");
		$("#cableDiameter"+index).addClass("readonlymini");
		$("#cableDiameter"+index).prop("readonly", true);
	}
}

function processBearingDEChange(index)
{
	var bearingDEVal = $('#bearingDE'+index).find("option:selected").text().trim();
	
	if(bearingDEVal == 'Roller Bearing') {
		$("#bearingSystem"+index).val('2');
	}
}

/*  On Ratings Section : Scripts to verify Dynamic Validations and Setting Default Values */
/*  ************************************************************************************* */


function createNewEnqTechRevision() {
	document.newenquiryForm.invoke.value="createNewEnqTechRevision";
	document.newenquiryForm.method="POST";
	document.newenquiryForm.action="newPlaceAnEnquiry.do"
	document.newenquiryForm.submit();
}

function createNewEnqCommRevision() {
	document.newenquiryForm.invoke.value="createNewEnqCommRevision";
	document.newenquiryForm.method="POST";
	document.newenquiryForm.action="newPlaceAnEnquiry.do"
	document.newenquiryForm.submit();
}

function createNewEnqRevision()
{
	document.newenquiryForm.invoke.value="createNewEnquiryRevision";
	document.newenquiryForm.method="POST";
	document.newenquiryForm.action="newPlaceAnEnquiry.do"
	document.newenquiryForm.submit();
}

function getNextNewRating(ratingId, rowId)
{
	if (ratingId != '') {
		document.newenquiryForm.action="newPlaceAnEnquiry.do";
		document.newenquiryForm.method="POST";
		document.newenquiryForm.invoke.value="viewOfferData";
		document.newenquiryForm.operationType.value = 'updaterating';
		document.newenquiryForm.currentRating.value = ratingId;	
		document.newenquiryForm.submit();
	}
}

function calculateNTorque_NewEnquiry()
{
	if(document.newenquiryForm.kw.value != null) {
		var ratedOutput = parseFloat(document.newenquiryForm.kw.value);		
		if(document.newenquiryForm.rpm.value != null && !isNaN(document.newenquiryForm.rpm.value)){			
			var speed = parseFloat(document.newenquiryForm.rpm.value);
			if(document.newenquiryForm.ratedOutputUnit.value == 'kW')
				document.newenquiryForm.fltSQCAGE.value = Math.ceil(9.81*(973 * ratedOutput/speed));
			else
				document.newenquiryForm.fltSQCAGE.value = Math.ceil(0.746*9.81*(973 * ratedOutput/speed));
			
			if(isNaN(document.newenquiryForm.fltSQCAGE.value)) {
				document.newenquiryForm.fltSQCAGE.value="";
			}
		}
	}
}

function calCurrent_NewEnquiry()
{
	if(document.newenquiryForm.kw.value != null){
		var ratedOutput = parseFloat(document.newenquiryForm.kw.value);
		var ratedVoltage = document.newenquiryForm.volt.value;	
		
		if(ratedVoltage.indexOf('KV') != -1){
			ratedVoltage = parseFloat(ratedVoltage.substring(0,ratedVoltage.length-2));
		} else{
			ratedVoltage = parseFloat(ratedVoltage.substring(0,ratedVoltage.length-1))/1000;
		}
	
		if( ratedVoltage != 0.0 ) {
			
			// *****************************************************	for 100% Load *****************************************************
			var efficiency =  document.newenquiryForm.pllEff1.value;
			var powerFactor = document.newenquiryForm.pllPf1.value;
			
            if(efficiency == "") {
            	efficiency = 0.0;
            } else {
            	efficiency = parseFloat(document.newenquiryForm.pllEff1.value);
            }
            if(powerFactor == "")
            {
            	powerFactor = 0.0;
            } else {
            	powerFactor = parseFloat(document.newenquiryForm.pllPf1.value);
            }
            
            if(efficiency != 0.0 && powerFactor != 0.0) {	
				if(document.newenquiryForm.ratedOutputUnit.value == 'kW')
					document.newenquiryForm.pllCurr1.value = Math.ceil(ratedOutput/(1.732*ratedVoltage*efficiency*powerFactor)*100);
				else
					document.newenquiryForm.pllCurr1.value = Math.ceil(0.745 * (ratedOutput/(1.732*ratedVoltage*efficiency*powerFactor)) * 100);
				
				efficiency = 0.0;
				powerFactor = 0.0;
			}
            
			efficiency =  document.newenquiryForm.pllEff2.value;
			powerFactor = document.newenquiryForm.pllPf2.value;
            if(efficiency == "") {
            	efficiency = 0.0;
            } else {
            	efficiency = parseFloat(document.newenquiryForm.pllEff2.value);	
            }
            if(powerFactor == "") {
    			powerFactor = 0.0;
            } else {
    			powerFactor = parseFloat(document.newenquiryForm.pllPf2.value);
            }
            
			if(efficiency != 0.0 && powerFactor != 0.0){				
				if(document.newenquiryForm.ratedOutputUnit.value == 'kW')
					document.newenquiryForm.pllCurr2.value = Math.ceil(0.75*(ratedOutput/(1.732*ratedVoltage*efficiency*powerFactor))*100);
				else
					document.newenquiryForm.pllCurr2.value = Math.ceil(0.745*0.75*(ratedOutput/(1.732*ratedVoltage*efficiency*powerFactor))*100);
				
				efficiency = 0.0;
				powerFactor = 0.0;
			}
			
			// *****************************************************	for 50% Load ******************************************************
			efficiency = document.newenquiryForm.pllEff3.value;
			powerFactor = document.newenquiryForm.pllpf3.value;
			if(efficiency == "") {
				efficiency = 0.0;
			} else {
				efficiency = parseFloat(document.newenquiryForm.pllEff3.value);
			}
			if(powerFactor == "") {
				powerFactor = 0.0;
			} else {
				powerFactor = parseFloat(document.newenquiryForm.pllpf3.value);
			}
			
			if(efficiency != 0.0 && powerFactor != 0.0){
				if(document.newenquiryForm.ratedOutputUnit.value == 'kW')
					document.newenquiryForm.pllCurr3.value = Math.ceil(0.50*(ratedOutput/(1.732*ratedVoltage*efficiency*powerFactor))*100);
				else 
					document.newenquiryForm.pllCurr3.value = Math.ceil(0.745*0.50*(ratedOutput/(1.732*ratedVoltage*efficiency*powerFactor))*100);
				
				efficiency = 0.0;
				powerFactor = 0.0;
			}
			
			if(document.newenquiryForm.pllCurr1.value != "") {
				updateCurrent_NewEnquiry(document.newenquiryForm.pllCurr1.value);
			}
			
		}
	}
	
}

function resetAddOnTypeText(index)
{
	var addOnTypeVal = $('#addOnType'+index).find("option:selected").text().trim();
	
	$('#addOnTypeText'+index).val(addOnTypeVal);
}

function calcTotalAddOnMTO(index)
{
	var totalAddOnValActual = parseInt($('#totalAddOn_MTO_H').val());
	
	var specialAddOnVal = '';
	var percentCopper = '';
	var percentStamping = '';
	var percentAluminium = '';
	
	if($("#specialAddOn_MTO").val() != '') {
		specialAddOnVal = parseFloat($("#specialAddOn_MTO").val());
	} else {
		specialAddOnVal = 0;
	}
	if($("#percentCopper_MTO").val() != '') {
		percentCopper = parseFloat($("#percentCopper_MTO").val());
	} else {
		percentCopper = 0;
	}
	if($("#percentStamping_MTO").val() != '') {
		percentStamping = parseFloat($("#percentStamping_MTO").val());
	} else {
		percentStamping = 0;
	}
	if($("#percentAluminium_MTO").val() != '') {
		percentAluminium = parseFloat($("#percentAluminium_MTO").val());
	} else {
		percentAluminium = 0;
	}
	
	var addons_Table = document.getElementById('addons');
	for(i = 1; i <= addons_Table.rows.length-1; i++) {
		if( Trim(document.getElementById("addOnPercent"+i).value) != "" ) {
			totalAddOnValActual = totalAddOnValActual + parseInt(document.getElementById("addOnPercent"+i).value);
		}
	}
	
	var max_percent = 0;
	if(percentCopper > percentStamping) { 
		max_percent = percentCopper;
	} else {
		max_percent = percentStamping;
	}
	if (percentAluminium > max_percent) {
		max_percent = percentAluminium;
	}
	
	// Check Range and Set the Actual Percent value.
	if(max_percent > 0 && max_percent < 5) {
		max_percent  = 5;
	} else if(max_percent > 5 && max_percent < 10) {
		max_percent  = 10;
	} else if(max_percent > 10 && max_percent < 15) {
		max_percent  = 15;
	} else if(max_percent > 15 && max_percent < 20) {
		max_percent  = 20;
	} else if(max_percent > 20 && max_percent < 25) {
		max_percent  = 25;
	}
	
	totalAddOnValActual = totalAddOnValActual + max_percent;
	
	$('#totalAddOn_MTO').val(totalAddOnValActual);
}

function calcTotalCashExtraMTO(index)
{
	var totalCashExtraActual = parseInt($('#totalCashExtra_MTO_H').val());
	
	var materialCostVal = '';
	
	if($("#matCost_MTO").val() != '') {
		materialCostVal = parseFloat($("#matCost_MTO").val());
	} else {
		materialCostVal = 0;
	}
	
	var addons_Table = document.getElementById('addons');
	for(i = 1; i <= addons_Table.rows.length-1; i++) {
		if( Trim(document.getElementById("addOnUnitPrice"+i).value) != "" ) {
			totalCashExtraActual = totalCashExtraActual + parseInt(document.getElementById("addOnUnitPrice"+i).value);
		}
	}
	
	totalCashExtraActual = totalCashExtraActual + materialCostVal;
	
	$('#totalCashExtra_MTO').val(totalCashExtraActual);
}

function mto_UpdateData()
{
	document.newenquiryForm.action="newPlaceAnEnquiry.do";
	document.newenquiryForm.method="POST";
	document.newenquiryForm.invoke.value="viewOfferData";
	document.newenquiryForm.operationType.value = 'updaterating';
	document.newenquiryForm.submit();
}

function viewNewEnquiryIndentPage(indentValue) 
{
	if(indentValue == "Lost" || indentValue == "Won") {
		var totalRatingIndex = parseInt($("#totalRatingIndex").val());
		if(totalRatingIndex <= 0) {
			alert("This Enquiry cannot be submitted to " + indentValue + " Status, as it has no Ratings.");
			return false;
		}
	}
	
	if(indentValue == "Won") {
		document.newenquiryForm.operationType.value="WON";
		document.newenquiryForm.action="newPlaceAnEnquiry.do"
    	document.newenquiryForm.submit();
	}
	if(indentValue == "Lost") {
		document.newenquiryForm.operationType.value="LOST";
		document.newenquiryForm.action="newPlaceAnEnquiry.do"
    	document.newenquiryForm.submit();
    }
    if(indentValue == "Abondent") {
    	document.newenquiryForm.operationType.value="ABONDENT";
    	document.newenquiryForm.action="newPlaceAnEnquiry.do"
    	document.newenquiryForm.submit();
    }
}

function viewCreateEnquiryIndentPage(indentValue) {
	if(indentValue == "Lost" || indentValue == "Won") {
		var totalRatingIndex = parseInt($("#totalRatingIndex").val());
		if(totalRatingIndex <= 0) {
			alert("This Enquiry cannot be submitted to " + indentValue + " Status, as it has no Ratings.");
			return false;
		}
	}
	
	if(indentValue == "Lost") {
		document.newenquiryForm.operationType.value="LOST";
		document.newenquiryForm.dispatch.value="viewSubmit";
		document.newenquiryForm.action="newPlaceAnEnquiry.do"
    	document.newenquiryForm.submit();
    }
    if(indentValue == "Abondent") {
    	document.newenquiryForm.operationType.value="ABONDENT";
    	document.newenquiryForm.dispatch.value="viewSubmit";
    	document.newenquiryForm.action="newPlaceAnEnquiry.do"
    	document.newenquiryForm.submit();
    }
}

function processAutoSave(idVal)
{
	var ajaxLoading = false;
	var previousId = parseInt($('#prevIdVal').val());
	var currentId = parseInt($('#ratingNo'+idVal).val());
	
	var customerIdVal = $("#customerId").val();
	var locationIdVal = $("#locationId").val();
	var locationVal = $("#location").val();
	var revisonNumberVal = $("#revisionNumber").val();
	var createdBySSOVal = $("#createdBySSO").val();
	var currEnquiryTypeVal = $("#enquiryType").val();
	var currEnquiryId = $('#enquiryId').val();
	var currRatingId = $('#ratingObjId'+idVal).val();
	var currentRatingNum = $('#ratingNo'+idVal).val();
	var currQuantity = $('#quantity'+idVal).val();
	var currProdLine = $("#productLine"+idVal).val();
	var currKW = $("#kw"+idVal).val();
	var currPoles = $("#pole"+idVal).val();
	var currFrame = $("#frame"+idVal).val();
	var currFrameSuffix = $("#framesuffix"+idVal).val();
	var currMounting = $("#mounting"+idVal).val();
	var currTBPosition = $("#tbPosition"+idVal).val();
	var currTagNo = $("#tagNumber"+idVal).val();
	var currMfgLoc = $("#mfgLocation"+idVal).val();
	var currEffClass = $("#effClass"+idVal).val();
	var currVoltage = $("#volt"+idVal).val();
	var currVoltAddVar = $("#voltAddVariation"+idVal).val();
	var currVoltRemVar = $("#voltRemoveVariation"+idVal).val();
	var currFrequency = $("#freq"+idVal).val();
	var currFreqAddVar = $("#freqAddVariation"+idVal).val();
	var currFreqRemVar = $("#freqRemoveVariation"+idVal).val();
	var currCombinedVar = $("#combinedVariation"+idVal).val();
	var currServiceFactor = $("#serviceFactor"+idVal).val();
	var currMethodOfStarting = $("#methodOfStarting"+idVal).val();
	var currStartingCurr = $("#startingCurrOnDOLStarting"+idVal).val();
	var currAmbTempAdd = $("#ambientTemp"+idVal).val();
	var currAmbTempRem = $("#ambientTempSub"+idVal).val();
	var currTempRise = $("#tempRise"+idVal).val();
	var currInsClass = $("#insulationClass"+idVal).val();
	var currMethodOfCool = $("#methodOfCooling"+idVal).val();
	var currAltitude = $("#altitude"+idVal).val();
	var currHazAreaType = $("#hazardAreaType"+idVal).val();
	var currHazProtection = $("#hazardArea"+idVal).val();
	var currGasGroup = $("#gasGroup"+idVal).val();
	var currHazZone = $("#hazardZone"+idVal).val();
	var currDustGroup = $("#dustGroup"+idVal).val();
	var currApplication = $("#application"+idVal).val();
	var currDuty = $("#duty"+idVal).val();
	var currCDF = $("#cdf"+idVal).val();
	var currNoOfStarts = $("#starts"+idVal).val();
	var currNoiseLevel = $("#noiseLevel"+idVal).val();
	var currReplaceMotor = $("#isReplacementMotor"+idVal).val();
	var currEarlierMotorNo = $("#earlierSuppliedMotorSerialNo"+idVal).val();
	var currStdDelivery = $("#standardDelivery"+idVal).val();
	var currCustomerDelivery = $("#customerRequestedDelivery"+idVal).val();
	var currRV = $("#rv"+idVal).val();
	var currRA = $("#ra"+idVal).val();
	var currWindingConn = $("#windingConfiguration"+idVal).val();
	var currLeads = $("#lead"+idVal).val();
	var currLoadGD2 = $("#loadGD2Value"+idVal).val();
	var currVFDAppType = $("#vfdType"+idVal).val();
	var currVFDSpeedMin = $("#vfdSpeedRangeMin"+idVal).val();
	var currVFDSpeedMax = $("#vfdSpeedRangeMax"+idVal).val();
	var currOverloadDuty = $("#overloadingDuty"+idVal).val();
	var currConstEffRange = $("#constantEfficiencyRange"+idVal).val();
	var currShaftType = $("#shaftType"+idVal).val();
	var currShaftMaterial = $("#shaftMaterialType"+idVal).val();
	var currDirOfRotation = $("#directionOfRotation"+idVal).val();
	var currCouplingMethod = $("#methodOfCoupling"+idVal).val();
	var currPaintType = $("#paintingType"+idVal).val();
	var currPaintShade = $("#paintShade"+idVal).val();
	var currPaintShadeValue = $("#paintShadeVal"+idVal).val();
	var currPaintThickness = $("#paintThickness"+idVal).val();
	var currIPVal = $("#ip"+idVal).val();
	var currCableSize = $("#cableSize"+idVal).val();
	var currNoOfRuns = $("#noOfRuns"+idVal).val();
	var currNoOfCores = $("#noOfCores"+idVal).val();
	var currCrossSectionArea = $("#crossSectionArea"+idVal).val();
	var currConductorMat = $("#conductorMaterial"+idVal).val();
	var currCableDiameter = $("#cableDiameter"+idVal).val();
	var currTBoxSize = $("#terminalBoxSize"+idVal).val();
	var currSpreaderBox = $("#spreaderBox"+idVal).val();
	var currSpaceHeater = $("#spaceHeater"+idVal).val();
	var currVibration = $("#vibration"+idVal).val();
	var currFlyLeadWithoutTB = $("#flyingLeadWithoutTB"+idVal).val();
	var currFlyLeadWithTB = $("#flyingLeadWithTB"+idVal).val();
	var currFan = $("#metalFan"+idVal).val();
	var currEncoderMounting = $("#techoMounting"+idVal).val();
	var currShaftGrounding = $("#shaftGrounding"+idVal).val();
	var currHardware = $("#hardware"+idVal).val();
	var currGlandPlate = $("#glandPlate"+idVal).val();
	var currDoubleCompressGland = $("#doubleCompressGland"+idVal).val();
	var currSPMMountProvision = $("#spmMountingProvision"+idVal).val();
	var currAddlNamePlate = $("#addlNamePlate"+idVal).val();
	var currDirArrowPlate = $("#directionArrowPlate"+idVal).val();
	var currRTD = $("#rtd"+idVal).val();
	var currBTD = $("#btd"+idVal).val();
	var currThermister = $("#thermister"+idVal).val();
	var currAuxTBox = $("#auxTerminalBox"+idVal).val();
	var currCableSealingBox = $("#cableSealingBox"+idVal).val();
	var currBearingSystem = $("#bearingSystem"+idVal).val();
	var currBearingNDE = $("#bearingNDE"+idVal).val();
	var currBearingDE = $("#bearingDE"+idVal).val();
	var currWitnessTest = $("#witnessRoutine"+idVal).val();
	var currAddlTest1 = $("#additional1Test"+idVal).val();
	var currAddlTest2 = $("#additional2Test"+idVal).val();
	var currAddlTest3 = $("#additional3Test"+idVal).val();
	var currTypeTest = $("#typeTest"+idVal).val();
	var currCertification = $("#ulce"+idVal).val();
	var currSpare1Val = $("#spare1Rating"+idVal).val();
	var currSpare2Val = $("#spare2Rating"+idVal).val();
	var currSpare3Val = $("#spare3Rating"+idVal).val();
	var currSpare4Val = $("#spare4Rating"+idVal).val();
	var currSpare5Val = $("#spare5Rating"+idVal).val();
	var currDatasheet = $("#dataSheet"+idVal).val();
	var currMotorGA = $("#motorGA"+idVal).val();
	var currPerfCurve = $("#perfCurves"+idVal).val();
	var currTBoxGA = $("#tBoxGA"+idVal).val();
	var currAddlComments = $("#additionalComments"+idVal).val();
	
	if(previousId == currentId) {
		// Cursor still pointing to the Same Rating.
		return false;
	} else {
		// Cursor has moved on to a New Rating.
	
		if(!ajaxLoading) {
			$.ajax({
				type : "POST",
				url : "newPlaceAnEnquiry.do",
				dataType : "XML",
				data : {
					customerIdVal : customerIdVal,
					locationIdVal : locationIdVal,
					locationVal : locationVal,
					revisonNumberVal : revisonNumberVal,
					createdBySSOVal : createdBySSOVal,
					currEnquiryTypeVal : currEnquiryTypeVal,
					currentRatingId : currRatingId,
					currentRatingNum : currentRatingNum,
					enquiryId : currEnquiryId,
					currQuantity : currQuantity,
					currProdLine : currProdLine,
					currKW : currKW,
					currPoles : currPoles,
					currFrame : currFrame,
					currFrameSuffix : currFrameSuffix,
					currMounting : currMounting,
					currTBPosition : currTBPosition,
					currTagNo : currTagNo,
					currMfgLoc : currMfgLoc,
					currEffClass : currEffClass,
					currVoltage : currVoltage,
					currVoltAddVar : currVoltAddVar,
					currVoltRemVar : currVoltRemVar,
					currFrequency : currFrequency,
					currFreqAddVar : currFreqAddVar,
					currFreqRemVar : currFreqRemVar,
					currCombinedVar : currCombinedVar,
					currServiceFactor : currServiceFactor,
					currMethodOfStarting : currMethodOfStarting,
					currStartingCurr : currStartingCurr,
					currAmbTempAdd : currAmbTempAdd,
					currAmbTempRem : currAmbTempRem,
					currTempRise : currTempRise,
					currInsClass : currInsClass,
					currMethodOfCool : currMethodOfCool,
					currAltitude : currAltitude,
					currHazAreaType : currHazAreaType,
					currHazProtection : currHazProtection,
					currGasGroup : currGasGroup,
					currHazZone : currHazZone,
					currDustGroup : currDustGroup,
					currApplication : currApplication,
					currDuty : currDuty,
					currCDF : currCDF,
					currNoOfStarts : currNoOfStarts,
					currNoiseLevel : currNoiseLevel,
					currReplaceMotor : currReplaceMotor,
					currEarlierMotorNo : currEarlierMotorNo,
					currStdDelivery : currStdDelivery,
					currCustomerDelivery : currCustomerDelivery,
					currRV : currRV,
					currRA : currRA,
					currWindingConn : currWindingConn,
					currLeads : currLeads,
					currLoadGD2 : currLoadGD2,
					currVFDAppType : currVFDAppType,
					currVFDSpeedMin : currVFDSpeedMin,
					currVFDSpeedMax : currVFDSpeedMax,
					currOverloadDuty : currOverloadDuty,
					currConstEffRange : currConstEffRange,
					currShaftType : currShaftType,
					currShaftMaterial : currShaftMaterial,
					currDirOfRotation : currDirOfRotation,
					currCouplingMethod : currCouplingMethod,
					currPaintType : currPaintType, 
					currPaintShade : currPaintShade,
					currPaintShadeValue : currPaintShadeValue,
					currPaintThickness : currPaintThickness,
					currIPVal : currIPVal,
					currCableSize : currCableSize,
					currNoOfRuns : currNoOfRuns,
					currNoOfCores : currNoOfCores,
					currCrossSectionArea : currCrossSectionArea,
					currConductorMat : currConductorMat,
					currCableDiameter : currCableDiameter,
					currTBoxSize : currTBoxSize,
					currSpreaderBox : currSpreaderBox,
					currSpaceHeater : currSpaceHeater,
					currVibration : currVibration,
					currFlyLeadWithoutTB : currFlyLeadWithoutTB,
					currFlyLeadWithTB : currFlyLeadWithTB,
					currFan : currFan,
					currEncoderMounting : currEncoderMounting,
					currShaftGrounding : currShaftGrounding,
					currHardware : currHardware,
					currGlandPlate : currGlandPlate,
					currDoubleCompressGland : currDoubleCompressGland,
					currSPMMountProvision : currSPMMountProvision,
					currAddlNamePlate : currAddlNamePlate,
					currDirArrowPlate : currDirArrowPlate,
					currRTD : currRTD,
					currBTD : currBTD,
					currThermister : currThermister,
					currAuxTBox : currAuxTBox,
					currCableSealingBox : currCableSealingBox,
					currBearingSystem : currBearingSystem,
					currBearingNDE : currBearingNDE,
					currBearingDE : currBearingDE,
					currWitnessTest : currWitnessTest,
					currAddlTest1 : currAddlTest1,
					currAddlTest2 : currAddlTest2,
					currAddlTest3 : currAddlTest3,
					currTypeTest : currTypeTest,
					currCertification : currCertification,
					currSpare1Val : currSpare1Val,
					currSpare2Val : currSpare2Val,
					currSpare3Val : currSpare3Val,
					currSpare4Val : currSpare4Val,
					currSpare5Val : currSpare5Val,
					currDatasheet : currDatasheet,
					currMotorGA : currMotorGA,
					currPerfCurve : currPerfCurve,
					currTBoxGA : currTBoxGA,
					currAddlComments : currAddlComments,
					"invoke" : "processRatingsAutoSaveAjax"
				},
				beforeSend: function() {
					ajaxLoading = true;
					$('#modal').show(); 
				},
				complete: function() { 
					$('#modal').hide(); 
				},
				success : function(data) {
					console.log($(data).find('msg').text());
					console.log($(data).find('returnValue1').text());
					if($(data).find('returnValue1').text() != '' && $(data).find('returnValue1').text() != '0') {
						$('#enquiryId').val($(data).find('returnValue1').text());
					} else {
						$('#enquiryId').val('');
					}
					$('#prevIdVal').val(currentId);
					$('#lastRatingNum').val(idVal);
				},
				error : function(xhr, ajaxOptions, thrownError) {
					return false;
				}
			});
		}
	}
}

function calculateTotalLostPrice(fieldId) 
{
	var totalRatingIndex = $("#totalRatingIndex").val();
	var totalLostPrice = 0;
	var lostPrice = 0;
	var quantity = 0;
	
	for(i=1; i<=totalRatingIndex; i++) {
		var index = i;
		quantity = parseInt($("#quantity"+index).val());
		lostPrice = parseFloat($("#lostPrice"+index).val());
		if(Number.isNaN(lostPrice) ) {
			totalLostPrice  = totalLostPrice;
		} else {
			totalLostPrice = totalLostPrice + lostPrice;
		}
	}
	
	$("#totalLostPrice").val(totalLostPrice);
}

function calculateTotalWonPrice(fieldId)
{
	var totalRatingIndex = $("#totalRatingIndex").val();
	var totalWonPrice = 0;
	var wonPrice = 0;
	var quantity = 0;
	
	for(i=1; i<=totalRatingIndex; i++) {
		var index = i;
		quantity = parseInt($("#quantity"+index).val());
		wonPrice = parseFloat($("#wonPrice"+index).val());
		if(Number.isNaN(wonPrice) ) {
			totalWonPrice  = totalWonPrice;
		} else {
			totalWonPrice = totalWonPrice + wonPrice;
		}
	}
	
	$("#totalWonPrice").val(totalWonPrice);
}

function saveReleasedQuote()
{
	var enquiryIdVal = $("#enquiryId").val();
	var winChanceVal = $("#winningChance").val();
	var targetedVal =  $("#targeted").val();
	var orderClosingDt = $("#closingDate").val();
	var smNoteVal = $("#smNote").val();
	
	var mat = saveNewEnquiryBasicInfo.updateNewEnquiryBasicInfo(enquiryIdVal, orderClosingDt, targetedVal,winChanceVal,smNoteVal, callBackFun);
	function callBackFun(str){
		if(str != '' && str =='YES'){					
			alert('Enquiry Information is Updated Successfully!');
			return;			
		}
	}  
	
}

function processLD(approvalStatus) 
{
	document.newenquiryForm.action="newPlaceAnEnquiry.do";
	document.newenquiryForm.invoke.value="processLDApproval";
	document.newenquiryForm.operationType.value=approvalStatus;
	document.newenquiryForm.submit();
}

function processMfgPlantDetails()
{
	document.newenquiryForm.action="newPlaceAnEnquiry.do";
	document.newenquiryForm.invoke.value="processMfgPlantDetails";
	document.newenquiryForm.submit();
}

function salesReassignTo()
{
	if($("#reassignToId").val() == '' || $("#reassignToId").val() == '0') {
		alert("Please select Re-assign to User.");
		$("#reassignToId").focus();
		return false;
	}
	if($("#reassignRemarks").val() == null || $("#reassignRemarks").val().trim() == '') {
		alert("Please enter Comments before Re-assigning to a different User.");
		$("#reassignRemarks").focus();
		return false;
	}
	
	document.newenquiryForm.action="newPlaceAnEnquiry.do";
	document.newenquiryForm.invoke.value="reassignSalesEnquiry";
	document.newenquiryForm.submit();
}
